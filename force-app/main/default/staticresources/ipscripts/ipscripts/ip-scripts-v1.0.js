// JavaScript Document

		var mPos = [0,0];
		YAHOO.util.Event.onDOMReady(function() {
			/* downshift the event execution */
			var _timeoutId = null;
			YAHOO.util.Event.on (document.body, 'mousemove', function(e){
				mPos = YAHOO.util.Event.getXY(e);
				clearTimeout(_timeoutId);
				_timeoutId = setTimeout(function(){
								//console.log ('Downshift: ', mPos);
							}, 150);
			});
		});

		function popItToggler(t) {
			if(document.getElementById(t).style.display!='block')
				{document.getElementById(t).style.display='block';}
			else
				document.getElementById(t).style.display='none';
		}

		function toggleIpTab(t,p)
		{
			hideAllIpTabs(p);
			if(document.getElementById(t).className!='ipTabShow')
				document.getElementById(t).className='ipTabShow';
			else
				document.getElementById(t).className='ipTabHide';
		}		
		
		
		function hideAllIpTabs(p){
			var childNodeArray = YAHOO.util.Dom.getChildren(p);
			for(i=0; i < childNodeArray.length; i++){
				if(childNodeArray[i].className == 'ipTabShow')
					{childNodeArray[i].className='ipTabHide';}
				}
		}
		
		/*	@t = target element id, where the tabs (divs) are
			@c = container element id, where the menu will be created*/
		function buildIpTabManeu(t,c){
			var childNodeArray = YAHOO.util.Dom.getChildren(t);
			var v= "<ul>";
			for(i=0; i < childNodeArray.length; i++)
				{v +=	"<li><a href=\"#\" onclick=\"javascript:toggleIpTab('" + childNodeArray[i].id + "','" + t +"');return false;\">" + childNodeArray[i].title + "</a></li>";}
			v += "<ul>";
			document.getElementById(c).innerHTML = v;
		}
			
		        
		function popIt(t){
			document.getElementById(t).style.display='block';
			updatepopIt(t);
		}
        
		function updatepopIt(t){
			var mPosOffset = mPos;
			mPosOffset[0] = mPosOffset[0]+5;
			mPosOffset[1] = mPosOffset[1]+5;
			YAHOO.util.Dom.setXY(t, mPosOffset); 
		}       
		
		function unpopIt(t){
			document.getElementById(t).style.display='none';
		}
		
	/* 	Show hide a element @el based on a option selected on a picklist @sel against a list of value @searchIn
			@displayType is optional and define the css display property of the element @el, when not declared defaults to block */
		function toggleAreaOnSelection(sel,el,searchIn,displayType){

			if(sel.value==null || sel.value=='')
				{document.getElementById(el).style.display="none";
				return false;}
		
			if(displayType==null)
				displayType="block";
			
			if(searchIn.toLowerCase().indexOf(sel.value.toLowerCase())>=0)
				document.getElementById(el).style.display=displayType;
			else
				document.getElementById(el).style.display="none";
		}



		function numbersonly(e, decimal) {
		    var key;
		    var keychar;

		    if (window.event) {
		        key = window.event.keyCode;
		    }
		    else if (e) {
		        key = e.which;
		    }
		    else {
		        return true;
		    }
	
		    keychar = String.fromCharCode(key);

		    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27)) {
    	    }
		    else if ((("0123456789").indexOf(keychar) > -1)) {
		        return true;
		    }
		    else if (decimal && (keychar == ".")) {
		            return true;
		    }
		    else
		        return false;
		}


		function IsDateGreater(Date1, Date2, msg) {
		    var dv1 = document.getElementById(Date1).value.split("/");
		    var dv2 = document.getElementById(Date2).value.split("/");
		
		    var DateValue1 = new Date(dv1[1] + "/" + dv1[0] + "/" + dv1[2]);
		    var DateValue2 = new Date(dv2[1] + "/" + dv2[0] + "/" + dv2[2]);
		    
		    if (DateValue1 == '' || DateValue2 == '')
		        return;

		    if (DateValue2 <= DateValue1) {
		        alert(msg);
		    }

	    
		}