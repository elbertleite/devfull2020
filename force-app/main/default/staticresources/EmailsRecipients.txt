{
	"SchoolEmails": {
		"amendCommission": "Finance - Commissions",
		"groupPayment": "Finance - Payments",
		"payment": "Finance - Payments",
		"sendInvoice": "Finance - Commissions",
		"resendInvoice": "Finance - Commissions",
		"requestCOE": "Admissions;Admissions - CoE",
		"requestConfirm": "Finance - Commissions",
		"resendRequest": "Finance - Commissions",
		"requestLOO": "Admissions;Admissions - LoO",
		"cancelCourse": "Admissions;Finance - Refunds"		
	}
}