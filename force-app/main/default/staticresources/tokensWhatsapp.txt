{
	"Current_Agency": {
		"fdType": "string",
		"fddName": "Current_Agency__r.name",
		"fdDesc": "NAme of the agency currently looking after the client."
	},
	"Original_Agency": {
		"fdType": "string",
		"fddName": "Original_Agency__r.name",
		"fdDesc": "Name of the agency that has created the client"
	},
	"name": {
		"fdType": "string",
		"fddName": "Name",
		"fdDesc": "Full name of the client."
	},
	"firstName": {
		"fdType": "string",
		"fddName": "firstName",
		"fdDesc": "First name of the client."
	},
	"Visa_Expiry_Date": {
		"fdType": "date",
		"fddName": "visa_Expiry_Date__c",
		"fdDesc": "Current Visa Expire Date. The date that shows up on the page header."
	},
	"Country_of_Birth": {
		"fdType": "string",
		"fddName": "Nationality__c",
		"fdDesc": "Nationality of the client."
	},
	"Birthdate": {
		"fdType": "date",
		"fddName": "Birthdate",
		"fdDesc": "Birthdate of the client."
	},
	"Email": {
		"fdType": "string",
		"fddName": "Email",
		"fdDesc": "Email address of the client."
	},
	"Owner": {
		"fdType": "string",
		"fddName": "Owner__r.name",
		"fdDesc": "Name of the client's current owner."
	},
	"OwnerDepartment": {
		"fdType": "string",
		"fddName": "Owner__r.Contact.Department__r.name",
		"fdDesc": "Name of the department of the client's current owner."
	},
	"Status": {
		"fdType": "string",
		"fddName": "Status__c",
		"fdDesc": "The last status marked on the client's checklist."
	},
	"Arrival_Date": {
		"fdType": "date",
		"fddName": "Arrival_Date__c",
		"fdDesc": "The arrival date entered on the client's cycle."
	},
	"Expected_Travel_Date": {
		"fdType": "date",
		"fddName": "Expected_Travel_Date__c",
		"fdDesc": "Date in which the client is expected to take off to the destination."
	},
	"Destination_Country": {
		"fdType": "string",
		"fddName": "Destination_Country__c",
		"fdDesc": "Country chosen by the client to travel. It corresponds to the field Main Destination on the client's cycle."
	},
	"createdDate": {
		"fdType": "dateTime",
		"fddName": "createdDate",
		"fdDesc": "Date in which the client was created on HiFy."
	}
}

