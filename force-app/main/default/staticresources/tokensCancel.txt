{
	"name": {
		"fdType": "string",
		"fddName": "Client__r.name",
		"fdDesc": "Full name of the client."
	},
	"CurrentAgency": {
		"fdType": "string",
		"fddName": "Client__r.Current_Agency__r.Name",
		"fdDesc": "Agency currently looking after the client."		
	},
	"CancelDescription": {
		"fdType": "string",
		"fddName": "Credit_Description__c",
		"fdDesc": "The reason you filled up when requesting the cancellation of the course."		
	},
	"CancelReason": {
		"fdType": "string",
		"fddName": "Credit_Reason__c",
		"fdDesc": "The reason you've chosen on the drop-down list when requesting the cancellation of the course."		
	},
	"EnrolmentAgency": {
		"fdType": "string",
		"fddName": "Enroled_by_Agency__r.Name",
		"fdDesc": "Agency that has enrolled the student at the school."		
	}
}