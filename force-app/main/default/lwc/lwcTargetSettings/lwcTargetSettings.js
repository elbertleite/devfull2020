/* eslint-disable no-console */
import { LightningElement, track, wire, api } from 'lwc';
//import getCurrentUser from '@salesforce/apex/lwcDashboard.getCurrentUser'; 
import getDepartmentTargets from '@salesforce/apex/lwcDashboardSetting.getDepartmentTargets';
import updateSettingTarget from '@salesforce/apex/lwcDashboardSetting.updateSettingTarget';

export default class LwcTargetSettings extends LightningElement {
    @api accoId;
    @track isValid = false;
    @track crsDepartment = [];
    @track crsDepartmentAux = [];
    //@track currentAgency = '';
    //@track currentAgencyId = '';
    @track isToSave = false;

    @track toast = false;    
    @track msgToast = '';
    @track typeToast = '';

    // //CurrentUser
    // @wire(getCurrentUser)
    // currentUserValues ({error, data}) {          
    //      if (data) {         
    //         this.currentAgency = data.Contact.Account.Name; 
    //         this.agencyId = this.accoId;    
    //         console.log(this.currentAgency + ' data ' + this.currentAgencyId); 
    //         this.getDepartmentsTarget();                    
    //      } else {
    //         this.currentAgency = '';
    //         this.currentAgencyId = '';             
    //         if (error !== undefined){                   
    //              this.error = error;     
    //         }        
    //      }       
    // }     

    //getDepartmentsTarget(){
    @wire(getDepartmentTargets, {selectedAgency: '$accoId'})
    getDepTargets ({error, data}) {        
            if (data) {                               
                this.crsDepartment = data; 
                this.crsDepartmentAux = data;                
            } else{
                this.error = error; 
            }                                        
    }  

    handleChange(){
        this.isToSave = true;
    }

    handleSubmit(){
        
        let sDeparts = [];
        this.template.querySelectorAll('lightning-input').forEach(el => {              
            let id = el.dataset.item;                                                                                              
            let vl = el.value;                    
            sDeparts.push({id: id, value: vl});             
        })             
        
        const objJson = JSON.stringify(sDeparts);
        console.log('objJson ' + objJson);
        
        updateSettingTarget({objJson, agencyId: this.accoId}).then(result => {               
            if(result !== 'success'){                 
                this.msgToast = 'Error update record';
                this.typeToast = 'error'; 
                this.toast = true;                    
            }
            else{
                this.isToSave = false;            
            }            
        }) 
    }
}