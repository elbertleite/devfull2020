/* eslint-disable no-console */
/* eslint-disable @lwc/lwc/no-async-operation */
import { LightningElement, track, wire, api } from 'lwc';
import getNationality from '@salesforce/apex/lwcFrontDesk.getNationality';
import getVisaType from '@salesforce/apex/lwcFrontDesk.getVisaType';

import getEmailTemplate from '@salesforce/apex/lwcSettingsForm.getEmailTemplate';
import getUserPicklist from '@salesforce/apex/lwcSettingsForm.getUserPicklist';
import getSettings from '@salesforce/apex/lwcSettingsForm.getSettings';
import insertFormSettings from '@salesforce/apex/lwcSettingsForm.insertFormSettings';
import getSettingsFormPassword from '@salesforce/apex/lwcSettingsForm.getSettingsFormPassword';
import updatePasswordFormSettings from '@salesforce/apex/lwcSettingsForm.updatePasswordFormSettings';
import getClientStage from '@salesforce/apex/lwcSettingsForm.getClientStage';
import { refreshApex } from '@salesforce/apex';

export default class Settings extends LightningElement {
    @api accoId;

    @track crsUser= [];
    @track crsUserAux= [];
    @track searchUser= '';    
    @track searchUserFull='';
    @track isUse = false;

    @track crsUserSender= [];
    @track crsUserSenderAux= [];
    @track searchUserSender= '';    
    @track searchUserSenderFull='';
    @track isUserSender = false;
    
    @track crsAssigning= [];
    @track crsAssigningAux= [];
    @track searchAssigning= '';    
    @track searchAssigningFull='';
    @track isAssigning = false;
    
    @track crsNationality= [];
    @track crsNationalityAux= [];    
    @track searchNationality ='';    
    @track isNat = false;   

    @track crsClientStages= [];
    @track crsClientStagesAux= [];    
    @track searchClientStages ='';    
    @track isClientStages = false;    
    
    @track isNewOrAll = true;
    @track crsTemplate= [];
    @track crsTemplateAux= [];
    @track searchTemplate ='';
    @track searchTemplateFull = '';
    @track isTem = false;

    @track isExistingOrAll = true;
    @track crsTemplateExist= [];
    @track crsTemplateExistAux= [];
    @track searchTemplateExist ='';
    @track searchTemplateExistFull = '';
    @track isTemExist = false;
    
    @track isPswChanged = false;
    @track error; 
    @track isTab = false;
    @track isValid = false;    
    @track arrayParentAux = [];
    @track arrayParent = [];       

    @track crsTypeVisa;
    @track crsTypeVisaAux;  
    @track isVisa = false;
    @track searchVisaType= '';

    @track crsTemplateOffshore= [];
    @track crsTemplateOffshoreAux= [];
    @track searchTemplateOffshore= '';    
    @track searchTemplateOffshoreFull='';
    @track isOffshore = false;
    @track isOffshoreOrAll = true;
    
    @track passwordSelected = '';
    
    @track isChange = false;

    @track toast = false;    
    @track msgToast = '';
    @track typeToast = '';

    @track searchClient = '';
    @track searchClientFull = '';
    @track isClient = false;
    @track reqClientStages = false;
    
    @track reqNew = false;
    @track reqExisting = false;
    @track reqOffshore = false
    @track reqClient = true;
    @track reqOwner = true;
    @track reqNat = true;
    @track reqVisa = true;
    @track reqSender = true; 

    @track duoDateSelected = '';
    
    indexToDelete = -1;

    @track tpClient = [        
        { label: 'All', value: 'All' },
        { label: 'New Lead', value: 'New' },
        { label: 'Existing Contacts (not clients)', value: 'Existing' },
        { label: 'Offshore Arrival', value: 'Offshore' }        
    ]; 
  
    @track tpClientAux = []; 

    @track bShowModal = false;

    @wire(getSettings, {agencyId: '$accoId'})
    wiredArrayParent;
     
    refresh() {
        this.tpClientAux = this.tpClient;        
        //console.log(this.arrayParent + ' refresh Set ' + JSON.stringify(this.wiredArrayParent.data));
        if (this.wiredArrayParent.data !== undefined){
            if (this.wiredArrayParent.data.length > 0){ 
                this.arrayParentAux = [];   
                this.arrayParent = [];            
                this.arrayParentAux = this.wiredArrayParent.data;             
                this.onAddExistent();                                                                         
            }
        }else{
            this.refreshData();
           // console.log(JSON.stringify(this.arrayParent)  + ' refresh Set else ' + JSON.stringify(this.wiredArrayParent.data));            
        }
    }

    refreshData(){
        getSettings({agencyId: this.accoId}).then(result => {
            if(result.length > 0){    
                this.arrayParentAux = [];   
                this.arrayParent = [];            
                this.arrayParentAux = result;             
                this.onAddExistent();                          
                console.log(JSON.stringify(this.arrayParent) + ' refreshData: ' );
            }else{
                console.log('refreshDataerror: ' + result);                       
            }            
        })           
    }

    @wire(getNationality)
    NationalityPicklistValues ({error, data}) {          
        if (data) {                
            //console.log('getNationality ' + JSON.stringify(data));
            this.crsNationality = data;                           
            this.crsNationalityAux = data;
            this.crsNationalityRefresh = data;            
            this.isNat = false;
        } else {
            this.crsNationality = [];
            this.crsNationalityAux = [];  
            this.error = error;             
        }       
    }  
    
    handleNationalityFocus(){       
        this.crsNationality = this.crsNationalityAux;    
        this.reqNat = false;  
        this.onCloseSearch();  
        this.isNat = true;        
    }

    handleNationalityChange(event){                 
        this.searchNationality = event.target.value;  
        console.log('handleNationalityChange' + this.searchNationality);             
        this.isNat = false;        
        if (this.searchNationality.length > 0){
            this.crsNationality = this.crsNationalityAux;             
            this.isNat = true;
            const filterItems = (l) => {
                return this.crsNationality.filter(x => x.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }
            this.crsNationality = filterItems(this.searchNationality); 
            this.reqNat = true;           
        }              
    }

    handleNationalityClicked(event){
        this.searchNationality = event.target.dataset.item;            
        this.isNat = false;        
    }

    @wire(getEmailTemplate, {agencyId: '$accoId'})
    TemplatePicklistValues ({error, data}) {                 
        if (data) {            
            //console.log('getEmailTemplate/TemplateExist ' + JSON.stringify(data));
            this.crsTemplate = data;            
            this.crsTemplateAux = data;           
            this.isTem = false;    

            this.crsTemplateExist = data;            
            this.crsTemplateExistAux = data;           
            this.isTemExist = false;    

            this.crsTemplateOffshore = data;       
            this.crsTemplateOffshoreAux = data;
            this.isOffshore = false;

        } else {
            this.crsTemplate = [];
            this.crsTemplateAux = [];
            this.crsTemplateExist = [];
            this.crsTemplateExistAux = [];   
            this.crsTemplateOffshore = [];
            this.crsTemplateOffshoreAux = []; 
            this.error = error;             
        }       
    }
    
    handleTemplateChange(event){           
        this.searchTemplate = event.target.value;  
        //console.log('handleTemplateChange' + this.searchTemplate);
        this.isTem = false;
        if (this.searchTemplate.length > 0){
            this.crsTemplate = this.crsTemplateAux;         
            const filterItems = (l) => {
                return this.crsTemplate.filter(x => x.value.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }
            this.crsTemplate = filterItems(this.searchTemplate);
            this.isTem = true;              
        }      
    }

    handleTemplateClicked(event){
        this.searchTemplate = event.target.dataset.item;
        this.searchTemplateFull = {label: event.target.dataset.value , value: event.target.dataset.item};
        this.isTem = false;        
    }

    handleTemplateFocus(){
        this.crsTemplate = this.crsTemplateAux;  
        this.reqNew = false;
        this.onCloseSearch();
        this.isTem = true;                
    }
    
    handleTemplateOffshoreClicked(event){
        this.searchTemplateOffshore = event.target.dataset.item;
        this.searchTemplateOffshoreFull = {label: event.target.dataset.value , value: event.target.dataset.item};
        this.isOffshore = false; 
    }

    handleTemplateOffshoreChange(event){
        this.searchTemplateOffshore = event.target.value;  
        //console.log('handleTemplateOffshoreChange' + this.searchTemplateOffshore);
        this.isOffshore = false;
        if (this.searchTemplateOffshore.length > 0){
            this.crsTemplateOffshore = this.crsTemplateOffshoreAux;         
            const filterItems = (l) => {
                return this.crsTemplateOffshore.filter(x => x.value.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }
            this.crsTemplateOffshore = filterItems(this.searchTemplateOffshore);
            this.isOffshore = true;              
        }   
    }

    handleTemplateOffshoreFocus(){
        this.crsTemplateOffshore = this.crsTemplateOffshoreAux;  
        this.reqOffshore = false;
        this.onCloseSearch();
        this.isOffshore = true;    
    }

    handleTemplateExistChange(event){           
        this.searchTemplateExist = event.target.value;          
        this.isTemExist = false;
        if (this.searchTemplateExist.length > 0){
            this.crsTemplateExist = this.crsTemplateExistAux;         
            const filterItems = (l) => {
                return this.crsTemplateExist.filter(x => x.value.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }
            this.crsTemplateExist = filterItems(this.searchTemplateExist);
            this.isTemExist = true;              
        }      
    }    

    handleTemplateExistClicked(event){
        this.searchTemplateExist = event.target.dataset.item;
        this.searchTemplateExistFull = {label: event.target.dataset.value , value: event.target.dataset.item};
        this.isTemExist = false;        
    }

    handleTemplateExistFocus(){
        this.crsTemplateExist = this.crsTemplateExistAux; 
        this.reqExisting = false;
        this.onCloseSearch();
        this.isTemExist = true;         
    }    
    
    @wire(getClientStage)
    ClientStagePicklistValues ({error, data}) {          
        if (data) {                
            //console.log('getClientStage ' + JSON.stringify(data));
            this.crsClientStages = data;                           
            this.crsClientStagesAux = data;            
            this.isClientStages = false;
        } else {
            this.crsClientStages = [];
            this.crsClientStagesAux = [];  
            this.error = error;             
        }       
    } 

    handleClientStagesChange(event){
        this.searchClientStages = event.target.value;          
        this.isClientStages = false;
        if (this.searchClientStages.length > 0){
            this.crsClientStages = this.crsClientStagesAux;         
            const filterItems = (l) => {
                return this.crsClientStages.filter(x => x.value.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }
            this.crsClientStages = filterItems(this.searchClientStages);
            this.isClientStages = true;              
        } 
    }
    
    handleClientStagesFocus(){
        this.crsClientStages = this.crsClientStagesAux;         
        this.onCloseSearch();
        this.isClientStages = true; 
        this.reqClientStages = false;
    }

    handleClientStagesClicked(event){
        this.searchClientStages = event.target.dataset.item;
        this.searchClientStagesFull = {label: event.target.dataset.value , value: event.target.dataset.item};
        this.isClientStages = false;    
    }

    @wire(getUserPicklist, {agencyId: '$accoId'})
    userPicklistValues ({error, data}) {               
        if (data) {            
            //console.log('getUser/ sender ' + JSON.stringify(data)); 
            this.crsUser = data;
            this.crsUserAux = data;           
            this.isUse = false; 

            this.crsUserSender = data;
            this.crsUserSenderAux = data;           
            this.isUserSender = false; 
            
            this.crsAssigning = data;
            this.crsAssigningAux = data;
            this.isAssigning = false;
                             
        } else {
            this.crsUser = [];
            this.crsUserAux = [];  
            this.crsTypeVisa = []; 
            this.crsTypeVisaAux = []; 
            this.crsAssigning = [];
            this.crsAssigningAux = []; 
            this.error = error;             
        }               
    }    

    handleUserChange(event){                
        this.searchUser = event.target.value;      
        this.isUse = false;
        this.searchNationality = '';
        this.searchVisaType = '';
        this.searchTemplate = '';
        this.searchTemplateFull = '';
        this.searchTemplateExist = '';
        this.searchTemplateExistFull = '';
        this.searchTemplateOffshore = '';
        this.searchTemplateOffshoreFull = '';
        this.searchAssigning = '';
        this.searchAssigningFull = '';        
        this.duoDateSelected = '';        
        this.searchClientStages = '';
        
        if (this.searchUser.length > 0){
            this.crsUser = this.crsUserAux;                  
            this.isUse = true;  
            const filterItems = (l) => {                
                return this.crsUser.filter(x => x.value.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }            
            this.crsUser = filterItems(this.searchUser);   
            this.onEditUserIfExits();                      
        }              
    }

    onEditUserIfExits(){      
        //if user exists fill all fields        
        const fu = this.arrayParent.filter(x => x.user.value.indexOf(this.searchUser) > -1);         
        if (fu.length > 0){            
            console.log('fu  ' + JSON.stringify(fu));
              
            const ftCliFull = fu[0].tpclient;            
            this.searchClient = ftCliFull.label;
            this.searchClientFull = ftCliFull;
            this.isClient = false; 
            this.validClientType();
            console.log('onEditUserIfExits ' + this.searchClient);

            const ftEmFull = fu[0].emailsender;            
            this.searchUserSender = ftEmFull.value;
            this.searchUserSenderFull = ftEmFull;
            this.isUserSender = false;             
            
            const ftFull = fu[0].template;  
            if (ftFull !== undefined){         
                this.searchTemplate = ftFull.value;
                this.searchTemplateFull = ftFull;
                this.isTem = false; 
            }

            const ftExFull = fu[0].templateExist;  
            if (ftExFull !== undefined){         
                this.searchTemplateExist = ftExFull.value;
                this.searchTemplateExistFull = ftExFull;
                this.isTemExist = false; 
            }             

            const ftOffFull = fu[0].templateOffshore;  
            if (ftOffFull !== undefined){         
                this.searchTemplateOffshore = ftOffFull.value;
                this.searchTemplateOffshoreFull = ftOffFull;
                this.isOffshore = false; 
            }

            const ftAssigFull = fu[0].AssigningTo;  
            if (ftAssigFull !== undefined){         
                this.searchAssigning = ftAssigFull.value;
                this.searchAssigningFull = ftAssigFull;
                this.isAssigning = false; 
            }

            const ftClientStagesFull = fu[0].ClientStages;  
            if (ftClientStagesFull !== undefined){         
                this.searchClientStages = ftClientStagesFull.value;
                this.searchClientStagesFull = ftClientStagesFull;
                this.isClientStages = false; 
            }

            this.duoDateSelected = fu[0].DuoDate;
        }
        else{          
            this.searchNationality = '';
            this.searchVisaType = '';
            
            this.searchUserSender = '';
            this.searchUserSenderFull = '';
            this.isUserSender = false;
            
            this.searchTemplate = '';
            this.searchTemplateFull = '';
            this.isTem = false; 

            this.searchTemplateExist = '';
            this.searchTemplateExistFull = '';
            this.isTemExist = false;
            
            this.searchTemplateOffshore = '';
            this.searchTemplateOffshoreFull = '';
            this.isOffshore = false; 

            this.searchAssigning = '';
            this.searchAssigningFull = '';
            this.isAssigning = false;

            this.searchClientStages = '';
            this.searchClientStagesFull = '';
            this.isClientStages = false; 
        }
    }

    handleUserFocus(){
        this.crsUser = this.crsUserAux;     
        this.reqOwner = false;  
        this.onCloseSearch();           
        this.isUse = true;        
    }
   
    handleUserClicked(event){          
        this.searchUser = event.target.dataset.item;        
        this.searchUserFull = {label: event.target.dataset.value , value: event.target.dataset.item};                
        this.isUse = false; 
        this.duoDateSelected = '';      
        this.onEditUserIfExits();   
    }

    @wire(getVisaType)
    TypeVisaPicklistValues ({error, data}) {           
        if (data) {            
            //console.log('getVisaType' + JSON.stringify(data));   
            this.crsTypeVisa = data;          
            this.crsTypeVisaAux = data;           
            this.isVisa = false;               
            this.refresh();      
        } else {
            this.crsTypeVisa = undefined;
            this.crsTypeVisaAux = undefined;  
            this.error = error;             
        }       
    } 

    handleVisaTypeChange(event){
        this.searchVisaType = event.target.value;        
        this.isVisa = false;
        if (this.searchVisaType.length > 0){
            this.crsTypeVisa = this.crsTypeVisaAux;      
        
            const filterItems = (l) => {
                return this.crsTypeVisa.filter(x => x.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }
            this.isVisa = true;  
            this.crsTypeVisa = filterItems(this.searchVisaType);
        }  
    }

    handleVisaTypeClicked(event){
        this.searchVisaType = event.target.dataset.item;
        this.isVisa = false;
    }

    handleTypeVisaFocus(){
        this.crsTypeVisa = this.crsTypeVisaAux;    
        this.reqVisa = false;
        this.onCloseSearch();
        this.isVisa = true;        
    }    
    
    handleUserSenderChange(event){                  
        this.searchUserSender = event.target.value;        
        this.isUserSender = false;

        if (this.searchUserSender.length > 0){
            this.crsUserSender = this.crsUserSenderAux;                  
            this.isUserSender = true;  
            const filterItems = (l) => {                
                return this.crsUserSender.filter(x => x.value.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }            
            this.crsUserSender = filterItems(this.searchUserSender);              
        }      
    }

    handleUserSenderFocus(){
        this.crsUserSender = this.crsUserSenderAux; 
        this.reqSender = false;                 
        this.onCloseSearch();
        this.isUserSender = true;         
    }
   
    handleUserSenderClicked(event){               
        this.searchUserSender = event.target.dataset.item;        
        this.searchUserSenderFull = {label: event.target.dataset.value , value: event.target.dataset.item};                
        this.isUserSender = false;          
    }

    handleAssigningFocus(){
        this.crsAssigning = this.crsAssigningAux;                          
        this.onCloseSearch();
        this.isAssigning = true; 
    }

    handleAssigningChange(event){                  
        this.searchAssigning = event.target.value;        
        this.isAssigning = false;

        if (this.searchAssigning.length > 0){
            this.crsAssigning = this.crsAssigningAux;                  
            this.isAssigning = true;  
            const filterItems = (l) => {                
                return this.crsAssigning.filter(x => x.value.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }            
            this.crsAssigning = filterItems(this.searchAssigning);              
        }
        console.log('handleAssigningChange ' + this.searchAssigning);      
    }

    handleAssigningClicked(event){
        
        this.searchAssigning =  event.target.dataset.item;
        this.searchAssigningFull = {label: event.target.dataset.value , value: event.target.dataset.item};                
        this.isAssigning = false; 
        console.log('handleAssigningClicked ' + this.searchAssigning);       
    }

    handleClientChange(event){
                  
        this.searchClient = event.target.value;                 
        this.isClient = false;        
        if (this.searchClient.length > 0){
            this.tpClient = this.tpClientAux;                  
            this.isClient = true;  
            const filterItems = (l) => {                
                return this.tpClient.filter(x => x.value.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }            
            this.tpClient = filterItems(this.searchClient);  
            this.validClientType();              
        }      
    }

    validClientType(){
        this.isOffshoreOrAll = true; 
        this.isExistingOrAll = true;
        this.isNewOrAll = true;
        this.reqExisting = false;
        this.reqNew = false;
        this.reqOffshore = false;
        this.reqClientStages = false;

        if (this.searchClient.length > 0){
            if (this.searchClient === 'All'){
                this.isExistingOrAll = false;
                this.isNewOrAll = false;
                this.isOffshoreOrAll = false;
                this.reqExisting = true;
                this.reqNew = true;
                this.reqOffshore = true;
                this.reqClientStages = true;
            } else{
                let n = this.searchClient.indexOf("New");                
                if (n >= 0){                   
                    this.isNewOrAll = false;
                    this.reqNew = true
                    this.searchTemplateExist = '';
                    this.searchTemplateExistFull = '';
                    this.searchTemplateOffshore = '';
                    this.searchTemplateOffshoreFull = '';
                    this.searchClientStages = '';
                    this.searchClientStagesFull = '';
                }else{
                    let e = this.searchClient.indexOf("Existing");
                    if (e >= 0){
                        this.isExistingOrAll = false; 
                        this.reqExisting = true;  
                        this.searchTemplate = '';
                        this.searchTemplateFull = '';
                        this.searchTemplateOffshore = '';
                        this.searchTemplateOffshoreFull = '';
                        this.searchClientStages = '';
                        this.searchClientStagesFull = '';                     
                    }else{
                        let o = this.searchClient.indexOf("Offshore");
                        if (o >= 0){
                            this.isOffshoreOrAll = false; 
                            this.reqOffshore = true;  
                            this.reqClientStages = true;
                            this.searchTemplateExist = '';
                            this.searchTemplateExistFull = '';
                            this.searchTemplate = '';
                            this.searchTemplateFull = '';
                        }
                    }    
                }
            }           
        }
    }

    handleUserBlur(){
        this.reqOwner = true;        
    }

    handleNatBlur(){
        this.reqNat = true;        
    }

    handleVisaBlur(){
        this.reqVisa = true;        
    }

    handleSenderBlur(){
        this.reqSender = true;        
    }

    handleNewBlur(){
        this.reqNew = true;              
    }

    handleExistingBlur(){
        this.reqExisting = true;        
    }
    
    handleOffshoreBlur(){
        this.reqOffshore = true;
    }    
    
    handleClientStagesBlur(){
       this.reqClientStages = true;
    }

    handleClientFocus(){          
        this.tpClient = this.tpClientAux;
        this.reqClient = false; 
        this.onCloseSearch();                 
        this.isClient = true;             
    }

    handleDuoDateChange(event){
        this.duoDateSelected = event.target.value;             
    }

    onCloseSearch(){
        this.isClient = false;
        this.isUse = false;
		this.isNat = false;
		this.isVisa = false;
		this.isUserSender = false;
		this.isTem = false;
        this.isTemExist = false;  
        this.isOffshore = false;
        this.isAssigning = false;
        this.isClientStages = false;
    }
   
    handleExit(){        
        this.onCloseSearch();
        Array.from(
            this.template.querySelectorAll('lightning-input')
        ).map(element => {
                return element.blur();
        });
                   
    }

    handleClientBlur(){       
        this.reqClient = true;        
    }

    handleClientClicked(event){           
        this.searchClient = event.target.dataset.value;         
        this.searchClientFull = {label: event.target.dataset.item , value: event.target.dataset.value};           
        this.isClient = false;          
        this.validClientType();
    }

    @wire(getSettingsFormPassword, {agencyId: '$accoId'})
    wirePassword ({error, data}) {                   
        if (data) {                           
            this.passwordSelected = data;                                            
        } else {
            this.passwordSelected = '';
            this.error = error;             
        }           
    }        

    handleFieldsChange(event){
        this.passwordSelected = event.detail.value;
        this.isPswChanged = true;
    }

    handleSave(){        
        this.toast = false;
        let password = this.passwordSelected;
        let agencyId = this.accoId;
        updatePasswordFormSettings({password, agencyId}).then(result => {
            if(result === 'success'){    
                this.isPswChanged = false;
            }else{
                console.log('Error: ' + result);                    
                this.msgToast = 'Error update record';
                this.typeToast = 'error'; 
                this.toast = true;
            }            
        })  
    }

    onAddExistent(){        

        for (let n = 0; n < this.arrayParentAux.length; n++) {
            const data = this.arrayParentAux[n];

            let sClientFull = {label: data.tpclient.label , value: data.tpclient.value};
            
            let sUser = data.user.value;            
            let sUserFull = {label: data.user.label, value: sUser};             
            
            let sSenderFull = {};            
            if (data.emailsender !== undefined){
                sSenderFull = {label: data.emailsender.label, value: data.emailsender.value};  
            }

            let sTemplateFull = {};   
            if (data.template !== undefined){
                sTemplateFull = {label: data.template.label, value: data.template.value};  
            }

            let sTemplateExistFull = {};
            if (data.templateExist !== undefined){
                sTemplateExistFull = {label: data.templateExist.label, value: data.templateExist.value};  
            }

            let stemplateOffshoreFull = {};
            if (data.templateOffshore !== undefined){
                stemplateOffshoreFull = {label: data.templateOffshore.label, value: data.templateOffshore.value};  
            }

            let sAssigningToFull = {};
            if (data.AssigningTo !== undefined){
                sAssigningToFull = {label: data.AssigningTo.label, value: data.AssigningTo.value};  
            }
            
            let sClientStagesFull = {};
            if (data.ClientStages !== undefined){
                sClientStagesFull = {label: data.ClientStages.label, value: data.ClientStages.value};  
            }

            let sDuoDate = '';            
            if (data.DuoDate !== undefined){
                sDuoDate = data.DuoDate;  
            }

            let dataP = this.arrayParentAux[n].paises;
            let dataV = this.arrayParentAux[n].visas;
            
            let dataFor = [];                                   
            if (dataP.length > dataV.length){
                dataFor = dataP;
            }
            else
            {
                dataFor = dataV;
            }            
            
            for (let z = 0; z < dataFor.length; z++) {
                let sNat = dataP[z];    
                let sVisaType = dataV[z];                            
                this.isTab = true;                                
                let indexAdd = -1;
                for (let i = 0; i < this.arrayParent.length; i++) {
                    const parent = this.arrayParent[i];                        
                    if(parent.user.value === sUser)
                    {
                        indexAdd = i;
                    }
                }

                if(indexAdd > -1){
                    //user exists -> add Nationalidades, visas
                    if (sNat !== undefined){
                        this.arrayParent[indexAdd].paises.push(sNat); 
                    }
                    if (sVisaType !== undefined){
                        this.arrayParent[indexAdd].visas.push(sVisaType);                                        
                    }
                }
                else{
                    //new user                    
                    this.arrayParent.push({tpclient: sClientFull, 
                                               user: sUserFull, 
                                        emailsender: sSenderFull, 
                                           template: sTemplateFull,
                                      templateExist: sTemplateExistFull,
                                   templateOffshore: stemplateOffshoreFull,
                                        AssigningTo: sAssigningToFull,
                                       ClientStages: sClientStagesFull, 
                                            DuoDate: sDuoDate,
                                             paises: [sNat],
                                              visas: [sVisaType]
                   })
                }               
            }
        }        
    }
   
    handleDeleteNationality(event){                
        this.indexToDelete = -1;
        let natSelected = event.target.value;      
        let ownerSelected = event.target.dataset.item;        

        for (let i = 0; i < this.arrayParent.length; i++) {
            const parent = this.arrayParent[i];            
            if(parent.user.value === ownerSelected){                
                const parentNat = this.arrayParent[i].paises;
                let indexNat = parentNat.indexOf(natSelected);                          
                if (indexNat > -1){                 
                    parentNat.splice(indexNat, 1);
                    this.isChange = true;                            
                    if (parentNat.length === 0)
                    {                   
                        this.indexToDelete = i;
                        this.bShowModal = true;                         
                    }                
                }
                break;
            }            
        }                               
    }

    confirmModal(){
        this.bShowModal = false;                 
        this.arrayParent.splice(this.indexToDelete, 1); 
        if (this.arrayParent.length === 0){
            this.isTab = false;                   
        }        
    }

    closeModal(){
        this.bShowModal = false;
        this.handleCancelSettings();
    }

    handleDeleteVisa(event){
        this.indexToDelete = -1;
        let visaSelected = event.target.value;      
        let ownerSelected = event.target.dataset.item;        
                
        for (let i = 0; i < this.arrayParent.length; i++) {
            const parent = this.arrayParent[i];            
            if(parent.user.value === ownerSelected){                
                const parentVisa = this.arrayParent[i].visas;
                let indexVisa = parentVisa.indexOf(visaSelected);                          
                if (indexVisa > -1){                 
                    parentVisa.splice(indexVisa, 1);
                    this.isChange = true;                            
                    if (parentVisa.length === 0)
                    {                   
                        this.indexToDelete = i;
                        this.bShowModal = true;                         
                    }                
                }
                break;
            }            
        }                    
    }   

    onValidation(){
        this.isValid = false;
        const allValidLI = [...this.template.querySelectorAll('lightning-input')]
        .reduce((validSoFar, inputCmp) => {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);       

        if (allValidLI){
            this.isValid = true;
            console.log('allValidLI');
        }

        //Client
        const filterItems = (l) => {                
            return this.tpClient.filter(x => x.label === l);
        }            
        const clientValid = filterItems(this.searchClient);        
        if (clientValid.length > 0)
        {
            this.searchClientFull = {label: clientValid[0].label, value: clientValid[0].value};       
        }else{                       
            this.isValid = false;   
            console.log('Client');             
            return;     
        }

        //Owner
        const filterItems1 = (l) => {                
            return this.crsUser.filter(x => x.value === l);
        }            
        const userValid = filterItems1(this.searchUser);        
        if (userValid.length > 0)
        {
            this.searchUserFull = {label: userValid[0].label, value: userValid[0].value};       
        }else{                       
            this.isValid = false;    
            console.log('Owner');            
            return;     
        }

        //Nationality
        const filterItems2 = (l) => {
            return this.crsNationality.filter(x => x === l);
        }
        const natValid = filterItems2(this.searchNationality);         
        if (natValid.length > 0)
        {
            this.searchNationality = natValid[0];
        }     
        else{        
            this.isValid = false; 
            console.log('Nationality');  
            return;                         
        } 

        //Visa
        const filterItems3 = (l) => {
            return this.crsTypeVisa.filter(x => x === l);
        }
        const visaValid = filterItems3(this.searchVisaType);        
        if (visaValid.length > 0)
        {
            this.searchVisaType = visaValid[0];            
        }else{            
            this.isValid = false;
            console.log('Visa');                
            return;
        }
        
        //Sender
        const filterItems4 = (l) => {                
            return this.crsUserSender.filter(x => x.value === l);
        }            
        const senderValid = filterItems4(this.searchUserSender);        
        if (senderValid.length > 0)
        {
            this.searchUserSenderFull = {label: senderValid[0].label, value: senderValid[0].value};       
        }else{                       
            this.isValid = false; 
            console.log('Sender');               
            return;     
        }

        if (!this.isNewOrAll){
            //Template
            const filterItems5 = (l) => {
                return this.crsTemplate.filter(x => x.value === l);
            }
            const tmpValid = filterItems5(this.searchTemplate);        
            if (tmpValid.length > 0)
            {
                this.searchTemplateFull = {label: tmpValid[0].label, value: tmpValid[0].value};            
            }else{            
                this.isValid = false; 
                console.log('Template');               
                return;
            }
        }

        if (!this.isExistingOrAll){
            //TemplateExist
            const filterItems6 = (l) => {
                return this.crsTemplateExist.filter(x => x.value === l);
            }
            const tmpExValid = filterItems6(this.searchTemplateExist);        
            if (tmpExValid.length > 0)
            {
                this.searchTemplateExistFull = {label: tmpExValid[0].label, value: tmpExValid[0].value};            
            }else{            
                this.isValid = false;    
                console.log('TemplateExist');                        
            }              
        }

        if (!this.isOffshoreOrAll){
            //TemplateOffshore
            const filterItems6 = (l) => {
                return this.crsTemplateOffshore.filter(x => x.value === l);
            }
            const tmpOffValid = filterItems6(this.searchTemplateOffshore);        
            if (tmpOffValid.length > 0)
            {
                this.searchTemplateOffshoreFull = {label: tmpOffValid[0].label, value: tmpOffValid[0].value};            
            }else{            
                this.isValid = false;    
                console.log('TemplateOffshore');                        
            }              
        }

        this.searchAssigningFull = '';        
        if ((this.searchAssigning !== '') && (this.searchAssigning !== undefined)){
            //AssigningTo
            const filterItems6 = (l) => {
                return this.crsAssigning.filter(x => x.value === l);
            }
            const tmpAssigValid = filterItems6(this.searchAssigning);        
            if (tmpAssigValid.length > 0)
            {
                this.searchAssigningFull = {label: tmpAssigValid[0].label, value: tmpAssigValid[0].value};            
            }else{            
                this.isValid = false;    
                console.log('AssigningTo');
            }              
        }
        
        this.searchClientStagesFull = '';
        if ((this.searchClientStages !== '') && (this.searchClientStages !== undefined)){
            //ClientStages
            const filterItems6 = (l) => {
                return this.crsClientStages.filter(x => x.value === l);
            }
            const tmpStagesValid = filterItems6(this.searchClientStages);        
            if (tmpStagesValid.length > 0)
            {
                this.searchClientStagesFull = {label: tmpStagesValid[0].label, value: tmpStagesValid[0].value};            
            }else{            
                this.isValid = false;    
                console.log('ClientStages');
            }              
        }        
    }

    handleAdd(){                
        this.handleClose(); 
        this.onValidation();
        if (this.isValid){
            this.isChange = true;
            this.isTab = true;            
            //add to the arrayParent
            let indexAdd = -1;
            for (let i = 0; i < this.arrayParent.length; i++) {
                const parent = this.arrayParent[i];                        
                if(parent.user.value === this.searchUser)
                {
                    indexAdd = i;
                    break;
                }                
            }
            if(indexAdd > -1){
                //user exists -> add Nationalidades, visas
                const filterItems = (l) => {
                    return this.arrayParent[indexAdd].paises.filter(x => x === l);
                }
                const natExist = filterItems(this.searchNationality);         
                if (natExist.length > 0){               
                    //this.msgToast = 'Nationality already exist.';
                    //this.typeToast = 'info'; 
                    //this.toast = true;                    
                }
                else{
                    this.arrayParent[indexAdd].paises.push(this.searchNationality);
                }

                const filterItems1 = (l) => {
                    return this.arrayParent[indexAdd].visas.filter(x => x === l);
                }                
                const visExist = filterItems1(this.searchVisaType);         
                if (visExist.length > 0){                
                    //this.msgToast = 'Visa type already exist.';
                    //this.typeToast = 'info'; 
                    //this.toast = true;                    
                }                     
                else{ 
                    this.arrayParent[indexAdd].visas.push(this.searchVisaType);
                }

                //update all other fields                
                if (this.arrayParent[indexAdd].tpclient.value !== this.searchClient)
                {
                    this.arrayParent[indexAdd].tpclient = this.searchClientFull;                    
                } 
                if (this.arrayParent[indexAdd].template.value !== this.searchTemplate)
                {
                    this.arrayParent[indexAdd].template = this.searchTemplateFull;                    
                } 
                if (this.arrayParent[indexAdd].templateExist.value !== this.searchTemplateExist)
                {
                    this.arrayParent[indexAdd].templateExist = this.searchTemplateExistFull;                    
                } 
                if (this.arrayParent[indexAdd].emailsender.value !== this.searchUserSender)
                {
                    this.arrayParent[indexAdd].emailsender = this.searchUserSenderFull;                    
                } 
                if (this.arrayParent[indexAdd].templateOffshore.value !== this.searchTemplateOffshore)
                {
                    this.arrayParent[indexAdd].templateOffshore = this.searchTemplateOffshoreFull;                    
                } 
                if (this.arrayParent[indexAdd].AssigningTo.value !== this.searchAssigningFull)
                {
                    this.arrayParent[indexAdd].AssigningTo = this.searchAssigningFull;                    
                } 
                if (this.arrayParent[indexAdd].ClientStages.value !== this.searchClientStagesFull)
                {
                    this.arrayParent[indexAdd].ClientStages = this.searchClientStagesFull;                    
                }                      
                if (this.arrayParent[indexAdd].DuoDate.value !== this.duoDateSelected)
                {
                    this.arrayParent[indexAdd].DuoDate = this.duoDateSelected;                    
                }                
            }
            else{
                //new user               
                this.arrayParent.push({tpclient: this.searchClientFull, 
                                           user: this.searchUserFull, 
                                    emailsender: this.searchUserSenderFull, 
                                       template: this.searchTemplateFull,
                                  templateExist: this.searchTemplateExistFull, 
                               templateOffshore: this.searchTemplateOffshoreFull,
                                    AssigningTo: this.searchAssigningFull,
                                   ClientStages: this.searchClientStagesFull, 
                                        DuoDate: this.duoDateSelected,
                                         paises: [this.searchNationality],
                                          visas: [this.searchVisaType]
                                      })
            } 
            this.searchNationality = '';
            this.searchVisaType = '';                                          
        }
        else{            
            this.msgToast = 'Please fill up all the mandatory fields.';
            this.typeToast = 'info'; 
            this.toast = true;
        }
    }   

    //called by child toastMessage.js    
    handleClose(){        
        this.toast = false;
        this.msgToast ='';
        this.typeToast ='';
    }
    
    onClean(){        
        this.searchClientFull = ''; 
        this.searchUserFull = '';
        this.searchUserSenderFull = '';
        this.searchTemplateFull = '';
        this.searchTemplateExistFull = '';
        this.searchNationality = '';
        this.searchVisaType = '';     
        this.isChange = false;   
        return refreshApex(this.wiredArrayParent);                
    }

    handleCancelSettings(){
        this.onClean();
        this.refresh(); 
    }    

    handleSubmit(){
        this.toast = false;             
        const objJson = JSON.stringify(this.arrayParent);           
        console.log('objJson: ' + objJson);
        insertFormSettings({objJson, agencyId: this.accoId}).then(result => {
            if(result === 'success'){                      
                this.onClean();                              
            }else{
                console.log('Error: ' + result);
                this.msgToast = 'Error update record';
                this.typeToast = 'error'; 
                this.toast = true;                    
            }            
        })        
    }
}