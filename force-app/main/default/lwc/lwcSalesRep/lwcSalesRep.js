/* eslint-disable no-console */
import { LightningElement, api, track } from 'lwc';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import chartjs from '@salesforce/resourceUrl/chart';

export default class LwcSalesRep extends LightningElement {
    @api crData;    
    vChart;
    initialized = false;       
    @track crData1 = {};
    @track showNoResult = false;

    @api
    refresh() {                                    
        this.initialized = false; 
        this.renderedCallback();              
    }
    
    renderedCallback() {              
        if (this.initialized) {
            return;
        }
       
        this.initialized = true;      
        this.crData1 = {};         
        if (this.crData !== undefined){ 
            this.crData1 = JSON.parse(JSON.stringify(this.crData));              
            if (this.crData1.dtsets !== undefined){
                if (this.crData1.dtsets.length > 0){       
                    let config = {
                        type: 'bar',
                        data: {
                            labels: this.crData1.stages,
                            datasets: this.crData1.dtsets                
                        },
                        options: {               
                            responsive: true,                   
                            legend: {  display: true },                           
                            tooltips: {     
                                enabled: true,                                                                                                 
                                callbacks: {
                                    label: function(tooltipItem, data) {                                    
                                        if (Number(tooltipItem.yLabel) > 0) {
                                            return Number(tooltipItem.yLabel).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " ( " + data.datasets[tooltipItem.datasetIndex].labels[tooltipItem.index] + " )";
                                        }                        
                                        return '';
                                    }
                                }
                            },
                            scales: {
                                xAxes: [{
                                    ticks: { autoSkip: false,
                                    }
                                }],
                                yAxes: [{
                                    scaleLabel: {
                                        display: true                            
                                    },
                                    ticks: {
                                        beginAtZero: true,
                                        precision:0                                                        
                                    }
                                }]
                            },              
                        }
                    };                 
                    
                    if (this.vChart !== undefined){
                        this.vChart.destroy();
                    }
                    
                    loadScript(this, chartjs + '/package/dist/Chart.min.js').then(() => {                                        
                        const canvas = this.template.querySelector('canvas.doch');
                        const ctx = canvas.getContext('2d');                                                            
                        this.vChart = new window.Chart(ctx, config);                             
                    }).catch(error => {
                        console.log('error ' + error);
                    });
                    loadStyle(this, chartjs + '/package/dist/Chart.min.css'); 
                }
                else{
                    this.showNoResult = true;
                } 
            }
            else{
                this.showNoResult = true;
            }   
        }
        else{
            this.showNoResult = true;
        }                      
    }
}