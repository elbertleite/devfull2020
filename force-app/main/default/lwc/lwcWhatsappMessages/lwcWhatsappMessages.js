/* eslint-disable no-console */
import { LightningElement, wire, api, track } from 'lwc';
import init from '@salesforce/apex/lwcWhatsappMessages.init';
import openChat from '@salesforce/apex/lwcWhatsappMessages.openChat';
import sendMessage from '@salesforce/apex/lwcWhatsappMessages.sendMessage';
import retrieveMessages from '@salesforce/apex/lwcWhatsappMessages.retrieveMessages';
import retrieveLatestMessages from '@salesforce/apex/lwcWhatsappMessages.retrieveLatestMessages';
import retrieveMessageFromDocument from '@salesforce/apex/lwcWhatsappMessages.retrieveMessageFromDocument';
import retrieveMessageFromTemplate from '@salesforce/apex/lwcWhatsappMessages.retrieveMessageFromTemplate';

import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import emoji from '@salesforce/resourceUrl/emojiButton';
import jquery from '@salesforce/resourceUrl/up_jQuery';


export default class LwcWhatsappMessages extends LightningElement {
    @api idClient;

    @track totalChatMessagesToload;

    @track totalClientMessages;
    @track chatLoadMoreIndex;
    @track newMessage = '';
    @track contactChat = {};
    @track lastMessage;   
    @track chatMessages;
    @track chatHasLastMessage; 
    @track chatDocuments = [];  
    @track chatTemplates = [];  
    @track chatInterval;

    @track totalLatestMessages;
    @track latestMessagesInterval;
    @track latestMessages = [];
    @track hasLatestMessages = false;
    @track latestMessagesOpen = false;
    @track showLoadMore = false;
    @track chatClientId;
    
    @track chatOpen = false;
    @track hasNewMessage = false;
    @track hasDocuments =  false;
    @track hasTemplates = false;
    @track showAttachModal = false;
    @track showTemplateModal = false;

    scrollWhenStart = true;

    connectedCallback() {} 
    renderedCallback() { 
        loadScript(this, emoji + '/emoij.js').then(() => {                                        
            if(this.template){
                const input = this.template.querySelector('.slds-textarea');
            }     
        }).catch(error => {
            console.log('error ' + error);
        });
    }

    @wire(init, {id: '$idClient'})
    init({error, data}) {    
        if (data) { 
            this.totalChatMessagesToload = data.totalChatMessagesToload;
            this.chatTemplates = data.templates;
            this.hasTemplates = this.chatTemplates.length > 0;  
            if(this.idClient){
                this.chatClientId = this.idClient;
                this.chatLoadMoreIndex = 1;
                this.openClientChat(true);            
            }else{
                this.latestMessagesOpen = true;
                this.retrieveClientsLastMessages();
            }                               
        }          
    }
    
    retrieveClientChat(event){
        this.chatClientId = event.currentTarget.dataset.key;
        this.retrieveClientsLastMessages();
    }

    openWhatsappEmojis(event){
        this.template.querySelector(".inputMesage").focus();
    }
    
    openClientChat(scrollToBottom){
        if(this.chatInterval){window.clearInterval(this.chatInterval);}
        this.chatOpen = true;
        openChat({id : this.chatClientId}).then((chatData) => {
            this.contactChat = chatData.contact; 
            this.chatDocuments = chatData.documents;
            this.hasDocuments = this.chatDocuments.length > 0;
            retrieveMessages({id : this.chatClientId, index : this.chatLoadMoreIndex}).then((messagesData) => {
                this.lastMessage = messagesData.lastMessage;
                this.totalClientMessages = messagesData.totalClientMessages;
                this.showLoadMore = this.totalClientMessages > this.chatLoadMoreIndex * this.totalChatMessagesToload;
                this.chatMessages = messagesData.messages;
                this.chatHasLastMessage = messagesData.hasLastMessage;
                this.chatInterval = this.retrieveChatMessagesInterval(this);
                if(scrollToBottom){
                    this.scrollToBottom();
                }
            });
        });
    }

    loadOlderMessages(){
        this.chatLoadMoreIndex = this.chatLoadMoreIndex + 1;
        if(this.latestMessagesOpen){
            this.retrieveClientsLastMessages();
        }else{
            this.openClientChat(false);
        }
    }
    
    retrieveChatMessagesInterval(that){
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        return window.setInterval(function(){
            retrieveMessages({id : that.chatClientId, index : that.chatLoadMoreIndex}).then((messagesData) => {
                that.hasNewMessage = that.chatHasLastMessage && that.lastMessage !== messagesData.lastMessage;
                that.lastMessage = messagesData.lastMessage;
                that.totalClientMessages = messagesData.totalClientMessages;
                that.showLoadMore = that.totalClientMessages > that.chatLoadMoreIndex * that.totalChatMessagesToload;
                that.chatMessages = messagesData.messages;
                that.chatHasLastMessage = messagesData.hasLastMessage; 
                if(that.hasNewMessage){
                    that.scrollToBottom();
                }
            });
        }, 20000);
    }
    
    retrieveClientsLastMessages(){  
        if(this.latestMessagesInterval){
            window.clearInterval(this.latestMessagesInterval);
        }
        retrieveLatestMessages().then((data) => {
            this.latestMessages = data;
            this.totalLatestMessages = this.latestMessages.length;
            this.hasLatestMessages = this.totalLatestMessages > 0;
            
            this.latestMessagesInterval = this.retrieveClientsMessagesInterval(this);
            if(this.chatClientId){
                this.chatLoadMoreIndex = 1;
                this.openClientChat(true);
            }   
        });
    }

    retrieveClientsMessagesInterval(that){
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        return window.setInterval(function(){
            retrieveLatestMessages().then((response) => {
                that.latestMessages = response;
                that.totalLatestMessages = that.latestMessages.length;
                that.hasLatestMessages = that.totalLatestMessages > 0;  
            });        
        }, 20000);
    }

    scrollToBottom(){
        if(this.template && this.template.querySelector('.chat-messages')){
            this.template.querySelector('.chat-messages').scrollTop = 10000000;
            return false;
        }
        return true;
    }

    markMessageAsRead(){
        console.log("Clickou no certo");
    }

    dismissNotification(){
        this.hasNewMessage = false;
    }
    
    handleChange(event) {
        this.newMessage = event.target.value;
    }

    openCloseAttach(){
        this.showAttachModal = !this.showAttachModal;
    }

    openCloseTemplate(){
        this.showTemplateModal = !this.showTemplateModal;
    }

    sendTemplate(event){
        let template = this.chatTemplates[event.currentTarget.dataset.index];
        retrieveMessageFromTemplate({clientID : this.chatClientId, template : template}).then((response) => {
            this.newMessage += response;
            this.showTemplateModal = false;
        });
    }

    sendMessageToClient(){
        let message = {message : this.newMessage, contact : this.contactChat, latestMessagesOpen : this.latestMessagesOpen};
        sendMessage(message).then((data) => {
            this.newMessage = '';
            this.template.querySelector(".inputMesage").value = '';
            if(this.latestMessagesOpen){
                this.retrieveClientsLastMessages();
            }else{
                this.openClientChat(true);
            }
        });
    }
    
    attachDocument(event){
        console.log("Chegou aqui pelo menso?");
        let request = {documents : this.chatDocuments, parentIndex : event.currentTarget.dataset.parentindex, index : event.currentTarget.dataset.index}
        retrieveMessageFromDocument(request).then((response) => {
            console.log(response);
            this.newMessage += response;
            this.showAttachModal = false;
        });
    }
}