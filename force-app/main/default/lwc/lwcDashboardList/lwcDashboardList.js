/* eslint-disable no-console */
/* eslint-disable @lwc/lwc/no-async-operation */
import { api, LightningElement } from 'lwc';

export default class LwcDashboardList extends LightningElement {
    @api tasklist;
    @api category;
    @api hasEl;    
    @api element;
    @api filterData;  
    @api applyData;        
    
    handleDragOver(evt) {           
        
        evt.preventDefault();
    }   

    handleItemDrag(evt) {          
    
        const event = new CustomEvent('listitemdrag', {
            detail: evt.detail,           
        });
        
        this.dispatchEvent(event);
    }

    handleItemApply(){
        
        const event = new CustomEvent('listapplyreturn', {
            detail: false,           
        });
        
        this.dispatchEvent(event);
    }

    handleDrop() {  
          
        const event = new CustomEvent('itemdrop', {
            detail: this.category,
            hasEl: this.hasEl,  
            filterData: this.filterData, 
            applyData: this.applyData        
        });
        
        this.dispatchEvent(event);
    }   
}