/* eslint-disable no-console */
/* eslint-disable @lwc/lwc/no-async-operation */
import { LightningElement, api, track } from 'lwc'; 
export default class ToastMessage extends LightningElement {
 
    @api vmsg; 
    @api vtype;  
    @track message = '';
  
    get typeSuccess(){
        let result = false;       
        if (this.vtype === 'success'){
            this.handleLoad();
            result = true;
        }        
        return result;        
    }

    get typeError(){
        let result = false;       
        if (this.vtype === 'error'){
            this.handleLoad();
            result = true;
        }        
        return result;        
    }

    get typeInfo(){
        let result = false;       
        if (this.vtype === 'info'){
            this.handleLoad();
            result = true;
        }        
        return result;
    }

    get typeWarning(){
        let result = false;       
        if (this.vtype === 'warning'){
            this.handleLoad();
            result = true;
        }        
        return result;        
    }

    handleLoad(){
        console.log('handleLoad: ' + this.vtype + ' ' + this.vmsg);      
        this.message = this.vmsg;        
        window.setTimeout(() => {            
            this.handleClose();
        }, 3000);
    } 
    
    handleClose(){        
        this.message = '';
        this.vmsg = ''; 
        this.vtype = '';
        //call method (onclose) on parent
        this.dispatchEvent(new CustomEvent('close'));
    }
}