/* eslint-disable no-console */
/* eslint-disable @lwc/lwc/no-async-operation */
import { LightningElement, track, wire, api } from 'lwc';
import getFrontDeskQueue from '@salesforce/apex/lwcFrontDesk.getFrontDeskQueue';
import removeContactFromDeskQueue from '@salesforce/apex/lwcFrontDesk.removeContactFromDeskQueue';
import { refreshApex } from '@salesforce/apex';

const columns1 = [
    { label: 'Name', fieldName: 'Name' },    
    { label: 'Mobile', fieldName: 'MobilePhone' },
    { label: 'Email', fieldName: 'Email' },
    { label: 'Current Agency', fieldName: 'CurrentAgencyName' },
    {
        type: "button",     
        initialWidth: 105,  
        typeAttributes: {   
            label: 'Edit',                     
            name: 'open_form',
            title: 'Open Form',
            variant: 'brand',             
            iconName: 'utility:edit_form',
            iconPosition: 'left'
        }
    },
];

const columns = [
    { label: 'Name', fieldName: 'Name' },    
    { label: 'Mobile', fieldName: 'MobilePhone' },
    { label: 'Email', fieldName: 'Email' },
    { label: 'Current Agency', fieldName: 'CurrentAgencyName' },
    {
        type: "button",     
        initialWidth: 120,                  
        typeAttributes: {  
            label: 'Delete',                                    
            name: 'remove',
            title: 'Delete from the Queue',
            variant: 'destructive',             
            iconName: 'utility:close',
            iconPosition: 'left',                         
        }   
    },
];

export default class FrontDeskQueue extends LightningElement {
    @api fromQueue = false;
    @api agency;
    @api form = false;
    @track data = [];
    @track isInicial = true;
    @track isnewstudent = false;
    @track editForm = false;
    @track columns = columns;         
    @track columns1 = columns1;  
    @track contactToUpdate;    
    @track contactRec;
    @track error;
    @track tableLoadingState = true;
    @track bShowModal = false;

    @wire(getFrontDeskQueue, {agencyId: "$agency"})
    wiredQueue;    

    @api
    refresh() { 
        console.log('refresh');
        this.handleRefresh();
    }

    handleRowAction(event) {          
        const actionName = event.detail.action.name;
        const row = event.detail.row;
        this.contactToUpdate = row;
        switch (actionName) {
            case 'remove':                
                this.bShowModal = true;              
                break;
            case 'open_form':                        
                this.openEditForm();
                break;
            default:
        }       
    }  
    
    handleRefresh(){
        return refreshApex(this.wiredQueue); 
    }    

    confirmModal(){
        this.bShowModal = false;
        this.deleteRow(); 
    }

    closeModal(){
        this.bShowModal = false;
    }

    openEditForm(){
        this.contactRec = this.contactToUpdate;
        console.log('openEditForm ' + JSON.stringify(this.contactToUpdate));
        this.editForm = true;         
        this.isnewstudent = false;
        this.isInicial = false;
        window.setTimeout(() => {
            //call method refresh on child (c-front-desk-form)
            this.template.querySelector('c-front-desk-form').openToEdit();                                 
        }, 2000); 
    }       

    deleteRow() {               
        const contactId = this.contactToUpdate.Id;
        const agencyId = this.agency;
        removeContactFromDeskQueue({ contactId, agencyId }).then(() => {   
            this.handleRefresh();             
        })
        .catch(error => {
            this.error = error;
        });
    }    
       
    handleNewForm(){
        //open form clean to insert         
        this.contactRec = undefined;
        this.editForm = true;
        this.form = true;
        this.isnewstudent = true;
        this.isInicial = false;         
    }     
}