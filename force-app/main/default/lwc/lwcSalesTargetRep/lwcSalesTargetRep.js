/* eslint-disable no-console */
import { LightningElement, api, track } from 'lwc';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import chartjs from '@salesforce/resourceUrl/chart';

export default class LwcSalesTargetRep extends LightningElement {
    @api crData;    
    vChart;
    initialized = false;       
    @track crData1 = {};    
    @track showNoResult = false;

    @api
    refresh() {                    
        this.initialized = false; 
        this.renderedCallback();              
    }
    
    renderedCallback() {              
        if (this.initialized) {
            return;
        }
       
        this.initialized = true;      
        this.crData1 = {};                
        if (this.crData !== undefined){ 
            this.crData1 = JSON.parse(JSON.stringify(this.crData));
                             
            if (this.crData1.qtd !== undefined){
                if (this.crData1.qtd.length > 0){      
                    console.log(this.crData1);                
                    const config = {
                        type: 'doughnut', 
                        data: {            
                            labels : this.crData1.label, 
                            datasets: [{                
                                data : this.crData1.qtd,
                                backgroundColor: ['rgb(154, 208, 245)', 'rgb(255, 157, 177)'],
                                label:["Achievement","To Achieve"]
                            }]            
                        },
                        options: {
                            circumference: Math.PI,
                            rotation : Math.PI,                                                           
                            legend: {
                                display: true
                            },
                            tooltips: { 
                                callbacks: {
                                    label: function(tooltipItem, data) {                    
                                        let label = data.labels[tooltipItem.index];
                                        let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                        return ' ' + label + ': ' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");                    
                                    }
                                }                                   
                            }
                        }        
                    }                  

                    if (this.vChart !== undefined){
                        this.vChart.destroy();
                    }
                
                    loadScript(this, chartjs + '/package/dist/Chart.min.js').then(() => {                                        
                        const canvas = this.template.querySelector('canvas.tar');
                        const ctx = canvas.getContext('2d');                                                            
                        this.vChart = new window.Chart(ctx, config);              
                    }).catch(error => {
                        console.log('error ' + error);
                    });
                    loadStyle(this, chartjs + '/package/dist/Chart.min.css');
                }
                else{
                    this.showNoResult = true;
                }   
            }
            else{
                this.showNoResult = true;
            }     
        }
        else{
            this.showNoResult = true;
        }                 
    }
}