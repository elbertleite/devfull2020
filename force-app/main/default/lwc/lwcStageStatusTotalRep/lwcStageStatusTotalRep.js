/* eslint-disable no-console */
import { LightningElement, api, track } from 'lwc';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import chartjs from '@salesforce/resourceUrl/chart';

export default class lwcStageStatusTotalRep extends LightningElement {
    @api crData;    
    vChart;
    initialized = false;       
    @track crData1 = {};
    @track showNoResult = false;
    
    @api
    refresh() {                                      
        this.initialized = false; 
        this.renderedCallback();              
    }
    
    renderedCallback() {              
        if (this.initialized) {
            return;
        }        
      
        this.initialized = true;      
        this.crData1 = {};                
       // console.log(' crData ' + JSON.stringify(this.crData)); 
        if (this.crData !== undefined){ 
          this.crData1 = JSON.parse(JSON.stringify(this.crData));         
          if (this.crData1.dtsets.length > 0){      
                        
            let config = {
              type: 'horizontalBar',
              data:{
                  labels: this.crData1.stages,                  
                  datasets: this.crData1.dtsets
                },             
                options: {                
                  responsive: true,                
                  legend: {
                    display: false
                  },
                  tooltips: {
                    enabled: false
                  }, 
                  hover: {
                    animationDuration: 0
                  },
                  animation: {
                    duration: 1,
                    onComplete: function() {
                      var chartInstance = this.chart,
                      ctx = chartInstance.ctx;          
                      ctx.textAlign = 'right';
                      ctx.textBaseline = 'middle';                     
                      this.data.datasets.forEach(function(dataset, i) {                      
                        var meta = chartInstance.controller.getDatasetMeta(i);                      
                        meta.data.forEach(function(bar, index) {                        
                          var data = dataset.data[index];   
                          var label = dataset.label[index];                         
                          if (data > 0){
                              ctx.fillText(label + " (" + data + ") ", 240, bar._model.y); 
                          }
                        });
                      });
                    }
                  },
                  scales:{                       
                    xAxes: [{
                      display:true,                      
                      ticks: {
                        beginAtZero: true,
                        precision:0                                               
                      }
                    }],
                    yAxes: [{
                      display:true,
                      gridLines:{                      
                        zeroLineColor:'black',
                        color:"black",
                        tickMarkLength:195,                      
                        drawTicks:true,                      
                      },
                      ticks:{      
                        autoSkip: false,                    
                        fontSize:11,
                        fontStyle:"normal", 
                        fontFamily:"sans-serif",                               
                        fontColor: "#666",
                        padding:4                      
                      }                    
                    }]
                }
              }               
            };  
          
            if (this.vChart !== undefined){
                this.vChart.destroy();
            }
          
            loadScript(this, chartjs + '/package/dist/Chart.min.js').then(() => {                                        
                const canvas = this.template.querySelector('canvas.doch');
                if (this.crData1.stages.length >= 3)
                    canvas.height = 400;                  
                const ctx = canvas.getContext('2d');              
                this.vChart = new window.Chart(ctx, config);         
            }).catch(error => {
                console.log('error ' + error);
            });
            loadStyle(this, chartjs + '/package/dist/Chart.min.css'); 
          }
          else{
            this.showNoResult = true;
          }         
        }
        else{
          this.showNoResult = true;
        }
  }
}