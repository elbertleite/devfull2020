/* eslint-disable no-console */
import { LightningElement } from 'lwc';

export default class FrontDesckQueue extends LightningElement {



    handleQueueRowAction(event) {       
        //open Form to fill up
        
        const row = event.detail.row; 
        this.contactToUpdate = row;        
        this.contactId = this.contactToUpdate.Id;
        console.log("id " + this.contactToUpdate.Id);            
        console.log("Name " + this.contactToUpdate.Name);
        console.log("Email " + this.contactToUpdate.Email);
        console.log("MobilePhone " + this.contactToUpdate.MobilePhone);
        console.log("Current_Agency__c " + this.contactToUpdate.Current_Agency__r.Name);
    }

}