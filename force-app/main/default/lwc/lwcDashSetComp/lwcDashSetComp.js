/* eslint-disable no-console */
import { LightningElement, api, track, wire } from 'lwc';
import getFilterCriteria from '@salesforce/apex/lwcDashboardSetting.getFilterCriteria';

export default class LwcDashSetComp extends LightningElement {
    @api repId;
    @api repName;
    @api reports;
    @api empId;
    @api empName;
    @api empReports = {};      
    @track criteriaValue = '';
    @track isCriteria = false;
    @track crsCriteria;
    @track crsCriteriaAux;
    @track eName = '';
    @track category = 'LIST';
    isValid = true;
    error = '';
    @track arrayParent = [];
    Data1=[];

    @wire(getFilterCriteria)
    CriteriaValues ({error, data}) {        
        if (data) {                           
            this.crsCriteria = data;
            this.crsCriteriaAux = data;           
            this.isCriteria = false;               
        } else {
            this.crsCriteria = undefined;
            this.crsCriteriaAux = undefined;  
            this.error = error;             
        }       
    }

    connectedCallback(){       
        
        if (this.empReports.length !== undefined){                         
            let r = this.empReports.filter(x => x.id === this.repId); 
            //console.log(r.length + ' r.length');           
            if (r.length > 0){            
                this.criteriaValue = r[0].period; 
                this.category = r[0].category;             
            }
        }
        //10 = Enrollments Confirmed / 13 = Enrollments FinalCommission use different label/value
        if ((this.repId === '10') || (this.repId === '13')){    
            this.data1= [{label:'Enrolment_Date',value:'Enrolment Date'}, {label:'Payment_Date',value:'Payment Date'}];                        
            this.crsCriteria = this.data1;
            this.crsCriteriaAux = this.data1;                       
        }                 
        
        this.seaName = this.repId + '(sea)' + this.repName;    
        this.eName = this.empId + '(sea)' + this.empName; 
    }
    
    handleExit(){
        this.isCriteria = false;              
        Array.from(
            this.template.querySelectorAll('lightning-input')
        ).map(element => {
            return element.blur();
        });              
    }

    handleCriteriaChange(event) {
        console.log(JSON.stringify(event.currentTarget.key) + ' handleCriteriaChange: ' + event.target.value);
        this.isCriteria = false;
        this.criteriaValue = event.target.value;
        if (this.criteriaValue.length > 0){
            this.crsCriteria = this.crsCriteriaAux;               
            const filterItems = (l) => {
                return this.crsCriteria.filter(x => x.value.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }
            this.isCriteria = true;  
            this.crsCriteria = filterItems(this.criteriaValue);                        
        }        
        
        if (this.crsCriteria.length > 0){
            this.itemReturnChange();          
        }        
    }

    itemReturnChange(){
        
        let sendCriteriaValue = null;
        if (this.empId === undefined){
            sendCriteriaValue = this.criteriaValue;
        }
        const event = new CustomEvent('itemchange', {          
            detail: {                
                changed: true,
                hasValue: sendCriteriaValue, 
                hasRepName: this.repName
            }            
        });
       
        this.dispatchEvent(event);        
    }
 
    handleCriteriaClicked(event){
        this.criteriaValue = event.target.dataset.item;        
        this.isCriteria = false;       
        console.log(JSON.stringify(event.currentTarget.id) + ' handleCriteriaClicked: ' + this.criteriaValue); 
        this.itemReturnChange();        
    }

    handleCriteriaFocus(){
        this.crsCriteria = this.crsCriteriaAux;
        this.isCriteria = true;        
    } 
    
    handleUserRetrieve(){

        this.crsCriteria = this.crsCriteriaAux;               
        const filterItems = (l) => {
            return this.crsCriteria.filter(x => x.value.toLowerCase().indexOf(l.toLowerCase()) > -1);
        }
        this.isCriteria = true;  
        this.crsCriteria = filterItems(this.criteriaValue);
        this.handleExit();        
    }

    @api
    AllUsersRetrieve(perValue, repName){
        this.template.querySelectorAll('.iptLine').forEach(el => { 
            if (perValue.length > 0) { 
                let e = el.dataset.item.split('(sea)');                                                                                              
                let r = el.name.split('(sea)');                                                                                         
                if ((r.length > 1) && (e.length > 1)){                      
                    if ((r[1] === repName) && (e[0] !== undefined)){             
                        //console.log(repName + ' call AllUsersRetrieve ' + e[0]);                                    
                        this.criteriaValue = perValue;
                        this.handleUserRetrieve();
                    }
                } 
            } 
        })          
    }

    @api
    Retrieve(){          
        
        this.template.querySelectorAll('.iptLine').forEach(el => {              
            if (el.value.length > 0) { 
                let e = el.dataset.item.split('(sea)');                                                                                              
                let r = el.name.split('(sea)');        
              
                if ((r.length > 1) && (e.length > 1)){  
                    //console.log(el.value + ' call Retrieve ' + el.dataset.value);
                    let c = el.dataset.value;                                 
                    let sClient = {id: e[0], 
                                 name: e[1],
                              reports:[{id: r[0], 
                                      name: r[1], 
                                    period: el.value,
                                  criteria: 'criteriavalue',
                                  category: c}]
                                    };   
                    const event = new CustomEvent('itemretrieved', {                                      
                        detail: sClient
                    });                    
                    this.dispatchEvent(event);                                                            
                } 
            } 
        })          
    }    
}