/* eslint-disable @lwc/lwc/no-async-operation */
/* eslint-disable no-console */
import { LightningElement,track, wire, api } from 'lwc';
import updateSettingDashboard from '@salesforce/apex/lwcDashboardSetting.updateSettingDashboard';
import getDashReports from '@salesforce/apex/lwcDashboardSetting.getDashReports';
import getDashboardSettingsUsers from '@salesforce/apex/lwcDashboardSetting.getDashboardSettingsUsers';
import getFilterCriteria from '@salesforce/apex/lwcDashboardSetting.getFilterCriteria';

export default class LwcDashSettings extends LightningElement {
    @api accoId;
    @track arrayParent = [];
    @track reports = [];
    @track emplooyes = [];        
    emplooyesAux = [];        
    crSetData = [];
    @track isToSave = false;    
    @track toast = false;    
    @track msgToast = '';
    @track typeToast = '';    
    error = '';
    isValid = true;
    crsCriteria;
    rCriteria;
    searchName = '';
    
    @wire(getFilterCriteria)
    CriteriaValues ({error, data}) {        
        if (data) {            
            this.crsCriteria = data;
        } else {
            this.crsCriteria = undefined;              
            this.error = error;                     
        }                     
    }       
    
    @wire(getDashboardSettingsUsers, {accoId: '$accoId'})
    SettingsDash ({error, data}) {               
        if (data) {                        
            this.emplooyes = data;            
            this.emplooyesAux = data; 
            console.log('emplooyes ' + JSON.stringify(this.emplooyes));                                
        } else {
            this.emplooyes = []; 
            this.emplooyesAux =[];
            this.crSetData = [];
            this.error = error; 
            //console.log('emplooyes ' + JSON.stringify(this.error));                          
        }               
    }  

    @wire(getDashReports)
    reportDash ({error, data}) {   
            if (data) {                                                                    
               this.reports = data;                
               //console.log('reports ' + JSON.stringify(this.reports));                                
            } else
            {
                this.reports = []; 
                this.error = error;                
            }     
    }  

    handleItemChange(evt){   
        console.log(' handleItemChange');    
        //console.log('handleItemChange - hasValue ' + evt.detail.hasValue + " detail " + evt.detail.changed +  " hasRepName " + evt.detail.hasRepName);
        this.isToSave = evt.detail.changed;        
        if (evt.detail.hasValue !== null){
            Array.from( this.template.querySelectorAll("td") ).forEach(row => {
                const f = row.querySelector("c-lwc-dash-set-comp");
                if (f != null){                                                                                 
                    f.AllUsersRetrieve(evt.detail.hasValue, evt.detail.hasRepName);
                }             
            });
        }        
    }

    handleClose(){
        
        this.toast = false;
        this.msgToast ='';
        this.typeToast ='';
    }    

    handleItemRetrieved(event){ 
        
        let sReportAux = [];
        sReportAux.push(event.detail);  
      
        this.handleValidation(sReportAux[0].reports[0].period);
        if (!this.isValid){ 
            this.rCriteria = undefined;                
            this.crSetData = undefined;
            //console.log(' invalid ');
        }
        
        if (this.crSetData !== undefined){
            //console.log(' ok ');
            let sReport = {id: sReportAux[0].reports[0].id, 
                         name: sReportAux[0].reports[0].name, 
                       period: sReportAux[0].reports[0].period,
                     criteria: this.rCriteria,
                     category: sReportAux[0].reports[0].category};
            
            if (this.crSetData.length > 0){                   
                for (let i = 0; i < this.crSetData.length; i++) {                                       
                    let nd = this.crSetData[i].reports.filter(x => x.id === sReportAux[0].reports[0].id);                                       
                    let id = this.crSetData.filter(x => x.id === sReportAux[0].id);                       
                    if (id.length !== 0){                                        
                        if ((sReportAux[0].id === this.crSetData[i].id) && (nd.length === 0)){                        
                            this.crSetData[i].reports.push(sReport);                             
                            break;
                        }                     
                    }else{                                                                         
                        this.crSetData.push(event.detail);                         
                        let index = (this.crSetData.length - 1);
                        this.crSetData[index].reports = [sReport];                             
                    }              
                }            
            }
            else{                     
                this.crSetData.push(event.detail); 
                this.crSetData[0].reports = [sReport];                  
            }
        }                    
    }

    handleValidation(p){
        //period               
        if ((p === 'Payment Date') || (p === 'Enrolment Date')){
            if (p === 'Payment Date'){
                this.rCriteria = 'Payment_Date';
            }else{
                if (p === 'Enrolment Date'){
                    this.rCriteria = 'Enrolment_Date';
                }
            }
        }
        else
        {            
            const filterItems = (l) => {
                return this.crsCriteria.filter(x => x.value === l);
            }
            const criteriaValid = filterItems(p);            
            if (criteriaValid.length === 0) {                 
                this.isValid = false;                 
            }else{                       
                this.rCriteria = criteriaValid[0].label;
            }
        }        
    }    

    addReports2(p){               
        let crAux = [];                                                                        
        for (let i = 0; i < this.emplooyes.length; i++) {
            let emp = this.emplooyes[i];              
            let sClient = {depId: emp.depId, 
                depName: emp.depName,
                     id: emp.id,
                   name: emp.name,
                reports:[]};

            crAux.push(sClient);
            let index = (crAux.length - 1);

            for (let s = 0; s < this.crSetData.length; s++) {                     
                if (this.crSetData[s].id === emp.id){    
                    for (let c = 0; c < this.crSetData[s].reports.length; c++) { 
                        let rep = this.crSetData[s].reports[c];                 
                        let sReport = {id: rep.id, 
                                     name: rep.name, 
                                   period: rep.period,
                                 category: rep.category};
                        
                        if (crAux[index].reports.length === 0){                            
                            crAux[index].reports = [sReport];                            
                        }else{
                            crAux[index].reports.push(sReport);
                        }
                    }   
                    break;
                }        
            }
        }
        this.emplooyes =[];
        this.emplooyes = crAux;     
        console.log('addReports2 - emplooyes ' + JSON.stringify(this.emplooyes)); 
        if (p){
            window.setTimeout(() => {     
                this.handleValidSubmit();                        
            }, 300);            
        }                  
    }

    addReports(){               
        //if this 2 reports are on some dashboard spot, it is added to avoid lost it from spot
        for (let i = 0; i < this.emplooyes.length; i++) {
            let idEmplooye = this.emplooyes[i].id;            
            let c = this.emplooyes[i].reports.filter(x => x.name.indexOf('Sales Target') > -1); 
            if (c.length > 0){                        
                for (let z = 0; z < this.crSetData.length; z++) {                     
                    if (this.crSetData[z].id === idEmplooye){   
                        let inst = this.crSetData[z].reports.filter(x => x.name.indexOf('Sales Overview - First Instalments') > -1);
                        if (inst.length > 0){  
                            this.handleValidation(c[0].period);
                            let cReport = {id: c[0].id, 
                                         name: c[0].name, 
                                       period: c[0].period,
                                     criteria: this.rCriteria,
                                     category: c[0].category};                        
                            this.crSetData[z].reports.push(cReport);                            
                            break;
                        }
                    }
                }       
                break;                        
            }
            
            let l = this.emplooyes[i].reports.filter(x => x.name.indexOf('New Leads Evolution') > -1); 
            if (l.length > 0){                
                for (let s = 0; s < this.crSetData.length; s++) {                     
                    if (this.crSetData[s].id === idEmplooye){                                                                    
                        let lReport = {id: l[0].id, 
                                     name: l[0].name, 
                                   period: 'Last 3 Years',
                                 criteria: 'LAST_N_YEARS:3',
                                 category: l[0].category};                        
                        this.crSetData[s].reports.push(lReport);                        
                        break;
                    }
                }  
                break;
            }
        }     
        //console.log('addReports: ' + JSON.stringify(this.crSetData));       
    }

    callItensRetrieve(){
        Array.from( this.template.querySelectorAll("td") ).forEach(row => {
            const f = row.querySelector("c-lwc-dash-set-comp");
             if (f != null){                                 
                 f.Retrieve();
             }             
        });
    }

    handleSubmit(){          
        if (this.searchName.length > 0){           
            this.searchName = '';
            this.handleSearchNameChange('');
            
        }else{                                 
            this.handleValidSubmit();            
        }        
    }

    handleValidSubmit(){  
        this.isValid = true;                
        this.crSetData = [];
        this.callItensRetrieve();        
        console.log('handleSubmit - crSetData: ' + JSON.stringify(this.crSetData)); 
        //this.crSetData = undefined;        //TEST
        if (this.crSetData !== undefined){
            this.addReports();
            const objJson = JSON.stringify(this.crSetData);           
            console.log('salvar objJson: ' + objJson);            
            updateSettingDashboard({objJson, agencyId: this.accoId}).then(result => {               
                if(result !== 'success'){                 
                    this.msgToast = 'Error update record';
                    this.typeToast = 'error'; 
                    this.toast = true;                    
                }
                else{
                    this.isToSave = false;
                }            
            })      
        }    
        else{            
            this.msgToast = 'Please fill up the fields correctly.';            
            this.typeToast = 'info';
            this.toast = true; 
        }  
    }

    handleSearchNameChange(event){      
        let preSubmit = true;
        if (event.target !== undefined){      
            this.searchName = event.target.value; 
            preSubmit = false;                       
        }
        this.callItensRetrieve(); 
        this.emplooyes = this.emplooyesAux;
        if (this.crSetData !== undefined){                               
            this.addReports2(preSubmit);
            if (this.searchName.length > 0){  
                const filterItems = (l) => {
                    return this.emplooyes.filter(x => x.name.toLowerCase().indexOf(l.toLowerCase()) > -1);
                }
                this.emplooyes = filterItems(this.searchName);                                 
            }                
        }
    }
}