/* eslint-disable no-console */
/* eslint-disable @lwc/lwc/no-async-operation */
import { LightningElement, track, wire } from 'lwc';
import getAgencyGroup from '@salesforce/apex/lwcDashboard.getAgencyGroup';
import getAgency from '@salesforce/apex/lwcDashboard.getAgency';
import getDepartment from '@salesforce/apex/lwcDashboard.getDepartment';
import getUser from '@salesforce/apex/lwcDashboard.getUser';
import getCurrentUser from '@salesforce/apex/lwcDashboard.getCurrentUser';
import getDashboardReportsUser from '@salesforce/apex/lwcDashboard.getDashboardReportsUser';
import getDashboardReports from '@salesforce/apex/lwcDashboard.getDashboardReports';
import updateDashboardReport from '@salesforce/apex/lwcDashboard.updateDashboardReport';

const NUM_OF_TABS = 3;
const tabs = [];
export default class LwcDashboard extends LightningElement {
    
    @track resultFilter;
    @track elementList = [];
    @track draggingEl = false;
       
    @track isClicked = false;
    @track spotListAux = [];
    @track isTypeClicked = false;

    @track spotListF = [];     
    @track spotListA = [];
    @track spotList = [];

    @track spotOne = [];
    @track spotTwo = [];
    @track spotThree = [];    
    @track spotFour = [];
    @track spotFive = [];
    @track spotSix = [];
    @track spotSeven = [];    
    @track spotEight = []; 
    @track spotNine = []; 
    @track spotTen = []; 
    @track spotEleven = []; 
    @track spotTwelve = []; 
    @track spotThirteen = []; 
    @track spotFourteen = [];
    @track spotFifteen = [];
    @track spotSixteen = [];
    @track spotSeventeen = [];
    @track spotEighteen = [];
    @track spotNineteen = [];    

    @track showSpinner1 = false;
    @track showSpinner2 = false;
    @track showSpinner3 = false;
    @track showSpinner4 = false;
    @track showSpinner5 = false;
    @track showSpinner6 = false;
    @track showSpinner7 = false;
    @track showSpinner8 = false;
    @track showSpinner9 = false;
    @track showSpinner10 = false;
    @track showSpinner11 = false;
    @track showSpinner12 = false;
    @track showSpinner13 = false;
    @track showSpinner14 = false;
    @track showSpinner15 = false;
    @track showSpinner16 = false;
    @track showSpinner17 = false;
    @track showSpinner18 = false;
    @track showSpinner19 = false;

    @track draggingId = ""; 
    @track isChange = false;   
    hasItem = false;
    error = undefined;

    @track crsGroup;
    @track crsGroupAux;
    @track reqGroup = true;
    @track isGroup = false;
    @track groupSelected = '';
    groupSelectedId = '';

    @track crsAgency;
    @track crsAgencyAux;
    @track reqAgency = true;
    @track isAgency = false;
    @track agencySelected = '';
    agencySelectedId = '';

    @track crsDepartment;
    @track crsDepartmentAux;    
    @track isDepartment = false;
    @track departmentSelected = '';
    departmentSelectedId = '';

    @track crsUsers;
    @track crsUsersAux;    
    @track isUsers = false;
    @track usersSelected = '';
    usersSelectedId = '';  
            
    @track crsCurrentUser;
    @track showSpinner = false;
    @track applyClick = false;
    
    @track toast = false;    
    @track msgToast = '';
    @track typeToast = '';        
    //_wiredResult;
    catNumber = '';
    retCatNumber = 0;

    //CurrentUser
    @wire(getCurrentUser)
    currentUserValues ({error, data}) {          
        if (data) {            
            this.crsCurrentUser = data;    
            //console.log('this.crsCurrentUser ' + JSON.stringify(this.crsCurrentUser));
            this.usersSelectedId = this.crsCurrentUser.Id;
            this.groupSelectedId = this.crsCurrentUser.Contact.Account.ParentId;            
            this.agencySelectedId = this.crsCurrentUser.Contact.AccountId;
            this.departmentSelectedId = this.crsCurrentUser.Contact.Department__c;            
        } else {
            this.crsCurrentUser = undefined;             
            if (error !== undefined){
                console.log('this.crsCurrentUser-error ' + JSON.stringify(error))           
                this.error = error;     
            }        
        }     
    }  

    // Group
    @wire(getAgencyGroup)    
    AgencyGroupValues ({error, data}) {          
        if (data) {            
            this.crsGroup = data;            
            this.crsGroupAux = data;           
            const filterItems = (l) => {
                return this.crsGroup.filter(x => x.label.indexOf(l) > -1);
            }            
            this.crsGroup = filterItems(this.groupSelectedId);
            if (this.crsGroup.length > 0){
                this.groupSelected = this.crsGroup[0].value;
            }            
            this.crsGroup = this.crsGroupAux;                        
            this.getAgencyValues(); 
            this.getReportsList(true);                       
        } else {
            this.crsGroup = undefined; 
            this.crsGroupAux = undefined;    
            this.error = error;             
        }       
    }  
    
    handleGroupFocus(){     
        this.crsGroup = this.crsGroupAux;        
        this.isGroup = true;
        this.reqGroup = false;    
    }

    handleGroupBlur(){
        this.reqGroup = true;    
    }

    handleGroupChange(event){    
        this.groupSelected = event.detail.value; 
        
        this.isGroup = false;
        if (this.groupSelected.length > 0){
            this.crsGroup = this.crsGroupAux;     
        
            const filterItems = (l) => {
                return this.crsGroup.filter(x => x.value.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }
            this.isGroup = true;  
            this.crsGroup = filterItems(this.groupSelected);
            if (this.crsGroup.length > 0){
                this.groupSelectedId = event.detail.label;
                this.agencySelectedId =undefined;                
                this.getAgencyValues(); 
                this.getReportsList(false);                 
            }            
        }          
    }
    
    handleGroupClicked(event){    
        this.groupSelected = event.target.dataset.item;
        this.groupSelectedId = event.target.dataset.value;
        this.isGroup = false;
        this.agencySelectedId = undefined;   
        this.getAgencyValues();  
        this.getReportsList(false);     
    }

    //Agency
    getAgencyValues(){         
        getAgency({selectedAgencyGroup:this.groupSelectedId}).then(result => {             
            if(result !== null){
                this.crsAgency = result;
                this.crsAgencyAux = result;                
                const filterItems = (l) => {
                    return this.crsAgency.filter(x => x.label.indexOf(l) > -1);
                }            
                this.crsAgency = filterItems(this.agencySelectedId);                
                if (this.crsAgency.length > 0){
                    this.agencySelected = this.crsAgency[0].value;                    
                }else{                                      
                    if (this.crsAgencyAux.length > 0){
                        this.agencySelectedId = this.crsAgencyAux[0].label;
                        this.agencySelected = this.crsAgencyAux[0].value;                
                    }
                }
                this.crsAgency = this.crsAgencyAux;                
                this.getDepartmentValues();
            }else{
                this.agencySelectedId = undefined;
                this.agencySelected = '';
            }
        }).catch(error => {
            this.crsAgency = undefined; 
            this.crsAgencyAux = undefined;    
            this.error = error;
        });
    }

    handleAgencyFocus(){     
        this.crsAgency = this.crsAgencyAux;        
        this.isAgency = true;
        this.reqAgency = false;          
    }

    handleAgencyBlur(){
        this.reqAgency = true;         
    }

    handleAgencyChange(event){    
        this.agencySelected = event.detail.value;         
        this.isAgency = false;
        if (this.agencySelected.length > 0){
            this.crsAgency = this.crsAgencyAux;      
        
            const filterItems = (l) => {
                return this.crsAgency.filter(x => x.value.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }
            this.isAgency = true;              
            this.crsAgency = filterItems(this.agencySelected);
            if (this.crsAgency.length > 0){
                this.agencySelectedId = event.detail.label;
                this.departmentSelectedId =undefined;                
                this.getDepartmentValues();  
            } 
        }    
    }
    
    handleAgencyClicked(event){    
        this.agencySelected = event.target.dataset.item;  
        this.agencySelectedId = event.target.dataset.value;      
        this.isAgency = false; 
        this.departmentSelectedId =undefined;        
        this.getDepartmentValues();      
    }
    
    //Department
    getDepartmentValues(){                             
        getDepartment({ selectedAgency: this.agencySelectedId}).then(result => {
            
            if(result !== null){
                this.crsDepartment = result;
                this.crsDepartmentAux = result;
                //console.log('this.crsDepartment ' + JSON.stringify(this.crsDepartment));
                const filterItems = (l) => {
                    return this.crsDepartment.filter(x => x.label.indexOf(l) > -1);
                }            
                this.crsDepartment = filterItems(this.departmentSelectedId);
                if (this.crsDepartment.length > 0){
                    this.departmentSelected = this.crsDepartment[0].value;
                }else{                    
                    if (this.crsDepartmentAux.length > 0){
                        this.departmentSelectedId = this.crsDepartmentAux[0].label;
                        this.departmentSelected = this.crsDepartmentAux[0].value;
                    }
                }
                this.crsDepartment = this.crsDepartmentAux;                
                this.getUserValues();
            }
        }).catch(error => {
            console.log('error ' + JSON.stringify(error));
            this.crsDepartment = undefined; 
            this.crsDepartmentAux = undefined;    
            this.error = error;
        }); 
    }  

    handleDepartmentFocus(){     
        this.crsDepartment = this.crsDepartmentAux;        
        this.isDepartment = true;        
    }

    handleDepartmentChange(event){            
        this.departmentSelected = event.detail.value;          
        this.isDepartment = false;
        if (this.departmentSelected.length > 0){
            this.crsDepartment = this.crsDepartmentAux;      
            //console.log(this.crsDepartment);
            const filterItems = (l) => {
                return this.crsDepartment.filter(x => x.value.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }
            this.isDepartment = true;  
            this.crsDepartment = filterItems(this.departmentSelected);            
            if (this.crsDepartment.length > 0){
                this.departmentSelectedId = event.detail.label;
                this.usersSelectedId = undefined;
                this.getUserValues();  
            } 
        }//else{
        //     this.departmentSelectedId = 'all';
        //     this.departmentSelected = '-- All --';
        // }    
    }
    
    handleDepartmentClicked(event){    
        this.departmentSelected = event.target.dataset.item;
        this.departmentSelectedId = event.target.dataset.value;
        this.isDepartment = false;
        this.usersSelectedId = undefined; 
        this.getUserValues();      
    }

    //Users
    getUserValues(){              
        getUser({ selectedAgency: this.agencySelectedId, 
                  selectedDepartment: this.departmentSelectedId }).then(result => {
            if(result !== null){
                this.crsUsers = result;
                this.crsUsersAux = result;    
                //console.log('this.crsUsers ' + JSON.stringify(this.crsUsers));
                const filterItems = (l) => {
                    return this.crsUsers.filter(x => x.label.indexOf(l) > -1);
                }            
                this.crsUsers = filterItems(this.usersSelectedId);
                if (this.crsUsers.length > 0){
                    this.usersSelected = this.crsUsers[0].value;
                }else{                  
                    
                    if (this.crsUsersAux.length > 0){
                        this.usersSelectedId = this.crsUsersAux[0].label;
                        this.usersSelected = this.crsUsersAux[0].value;
                    }
                }                  
                this.crsUsers = this.crsUsersAux;                
            }
        }).catch(error => {
            this.crsUsers = undefined; 
            this.crsUsersAux = undefined;    
            this.error = error;
        });
    } 

    handleUsersFocus(){     
        this.crsUsers = this.crsUsersAux;        
        this.isUsers = true;        
    }  

    handleUsersChange(event){          
        this.usersSelected = event.detail.value; 
        this.usersSelectedId = event.detail.label; 
        this.isUsers = false;
        if (this.usersSelected.length > 0){
            this.crsUsers = this.crsUsersAux;      
        
            const filterItems = (l) => {
                return this.crsUsers.filter(x => x.value.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }
            this.isUsers = true;  
            this.crsUsers = filterItems(this.usersSelected);
        }
        // else{
        //     this.usersSelectedId = 'all';
        //     this.usersSelected = '-- All --';
        // }    
    }
    
    handleUsersClicked(event){           
        this.usersSelected = event.target.dataset.item;
        this.usersSelectedId = event.target.dataset.value;
        this.isUsers = false;       
    }  

    handleExit(){     
        this.isGroup = false; 
        this.isAgency = false; 
        this.isDepartment = false; 
        this.isUsers = false;        

        Array.from(
            this.template.querySelectorAll('lightning-input')
        ).map(element => {
            return element.blur();
        });        
    }

    // @wire(getDashboardReportsUser, {selectedAgencyGroup:'$groupSelectedId'})
    // wiredCallback(result) { 
    //     this._wiredResult = result;        
    //     this.elementList = undefined; 
    //     this.error = undefined;      
    //     if (result.data) {
    //         this.elementList = result.data;             
    //         //Sort List
    //         const elementListSorted = Array.from(this.elementList).sort((a, b) => 
    //         {
    //             let x = a.name.toLowerCase();
    //             let y = b.name.toLowerCase();
    //             if (x < y) {return -1;}
    //             if (x > y) {return 1;}
    //             return 0;
    //         });            
    //         this.elementList = elementListSorted;
    //         let qtdReports = 0;
    //         if (this.elementList !== undefined){                                                         
    //             if (this.elementList.length > 0){
    //                 qtdReports = this.elementList.length;
    //                 this.distributeTasks();                          
    //                 if ((this.spotOne.length > 0) || (this.spotTwo.length > 0) || (this.spotThree.length > 0) || 
    //                     (this.spotFour.length > 0) || (this.spotFive.length > 0) || (this.spotSix.length > 0) || 
    //                     (this.spotSeven.length > 0) || (this.spotEight.length > 0) || (this.spotNine.length > 0) ||
    //                     (this.spotTen.length > 0) || (this.spotEleven.length > 0) || (this.spotTwelve.length > 0) ||
    //                     (this.spotThirteen.length > 0) || (this.spotFourteen.length > 0) || (this.spotFifteen.length > 0)||
    //                     (this.spotSixteen.length > 0) || (this.spotSeventeen.length > 0)){
    //                     this.draggingEl = true;
    //                 }   
    //                 this.handlseResize();
    //             }
    //             this.applyClick = false;
    //             this.showSpinner = true;
    //             this.getReportsData();  
    //             this.handleSpotsVisibility(qtdReports);
    //         }
            
    //     } else if (result.error) {
    //         this.error = result.error;            
    //     }
    // }

    //ReportsUserGroup
    
    getReportsList(first){         
        this.elementList = [];
        getDashboardReportsUser({selectedAgencyGroup:this.groupSelectedId}).then(result => {  
            console.log(' result ' + JSON.stringify(result));           
            //this._wiredResult = result;                     
            this.error = undefined;      
            if(result !== null){
                this.elementList = result;                               
                //Sort List
                const elementListSorted = Array.from(this.elementList).sort((a, b) => 
                {
                    let x = a.name.toLowerCase();
                    let y = b.name.toLowerCase();
                    if (x < y) {return -1;}
                    if (x > y) {return 1;}
                    return 0;
                });            
                this.elementList = elementListSorted;                    
                let qtdReports = (this.elementList !== undefined) ? this.elementList.length : 0;
                this.verifyQtdReports();
                this.distributeTasks();                          
                if ((this.spotOne.length > 0) || (this.spotTwo.length > 0) || (this.spotThree.length > 0) || 
                    (this.spotFour.length > 0) || (this.spotFive.length > 0) || (this.spotSix.length > 0) || 
                    (this.spotSeven.length > 0) || (this.spotEight.length > 0) || (this.spotNine.length > 0) ||
                    (this.spotTen.length > 0) || (this.spotEleven.length > 0) || (this.spotTwelve.length > 0) ||
                    (this.spotThirteen.length > 0) || (this.spotFourteen.length > 0) || (this.spotFifteen.length > 0)||
                    (this.spotSixteen.length > 0) || (this.spotSeventeen.length > 0) || (this.spotEighteen.length > 0) ||
                    (this.spotNineteen.length > 0) ){
                    this.draggingEl = true;
                }   
                this.handleResize();                
                if (first){               
                    this.applyClick = false;
                    this.showSpinner = true;
                    this.getReportsData();                      
                }
                this.handleSpotVisibility(qtdReports);

            } else if (result.error) {
                this.error = result.error;            
            }
        });
    }

    handleSpotVisibility(qtdReports){
      //  console.log('qtdReports ' + qtdReports)
        let s1 = this.template.querySelector('.lSpot1');  
        if (s1 !== null){
            s1.style.display = (qtdReports >= 1) ? 'inline' : 'none';
        }
        
        let s2 = this.template.querySelector('.lSpot2');  
        if (s2 !== null){
            s2.style.display = (qtdReports >= 2) ? 'inline' : 'none';
        }

        let s3 = this.template.querySelector('.lSpot3');  
        if (s3 !== null){
            s3.style.display = (qtdReports >= 3) ? 'inline' : 'none';
        }
        let s4 = this.template.querySelector('.lSpot4');  
        if (s4 !== null){
            s4.style.display = (qtdReports >= 4) ? 'inline' : 'none';
        }
        let s5 = this.template.querySelector('.lSpot5');  
        if (s5 !== null){
            s5.style.display = (qtdReports >= 5) ? 'inline' : 'none';
        }
        let s6 = this.template.querySelector('.lSpot6');  
        if (s6 !== null){
            s6.style.display = (qtdReports >= 6) ? 'inline' : 'none';
        }
        let s7 = this.template.querySelector('.lSpot7');  
        if (s7 !== null){
            s7.style.display = (qtdReports >= 7) ? 'inline' : 'none';           
        }
        let s8 = this.template.querySelector('.lSpot8');  
        if (s8 !== null){
            s8.style.display = (qtdReports >= 8) ? 'inline' : 'none';
        }
        let s9 = this.template.querySelector('.lSpot9');  
        if (s9 !== null){
            s9.style.display = (qtdReports >= 9) ? 'inline' : 'none'; 
        }
        let s10 = this.template.querySelector('.lSpot10');  
        if (s10 !== null){
            s10.style.display = (qtdReports >= 10) ? 'inline' : 'none'; 
        }
        let s11 = this.template.querySelector('.lSpot11');  
        if (s11 !== null){
            s11.style.display = (qtdReports >= 11) ? 'inline' : 'none'; 
        }
        let s12 = this.template.querySelector('.lSpot12');  
        if (s12 !== null){
            s12.style.display = (qtdReports >= 12) ? 'inline' : 'none'; 
        }
        let s13 = this.template.querySelector('.lSpot13');  
        if (s13 !== null){
            s13.style.display = (qtdReports >= 13) ? 'inline' : 'none'; 
        }
        let s14 = this.template.querySelector('.lSpot14');  
        if (s14 !== null){
            s14.style.display = (qtdReports >= 14) ? 'inline' : 'none'; 
        }
        let s15 = this.template.querySelector('.lSpot15');  
        if (s15 !== null){
            s15.style.display = (qtdReports >= 15) ? 'inline' : 'none';         
        }
        let s16 = this.template.querySelector('.lSpot16');  
        if (s16 !== null){
            s16.style.display = (qtdReports >= 16) ? 'inline' : 'none'; 
        }
        let s17 = this.template.querySelector('.lSpot17');  
        if (s17 !== null){
            s17.style.display = (qtdReports >= 17) ? 'inline' : 'none'; 
        }

        let s18 = this.template.querySelector('.lSpot18');  
        if (s18 !== null){
            s18.style.display = (qtdReports >= 18) ? 'inline' : 'none'; 
        }

        let s19 = this.template.querySelector('.lSpot19');  
        if (s19 !== null){
            s19.style.display = (qtdReports >= 19) ? 'inline' : 'none'; 
        }
    }

    handleResize(){         
        let s1 = this.template.querySelector('.lSpot1');         
        if (s1 !== null){
            s1.style.height = (this.spotOne.length === 0)? '50px' : 'auto';            
        }

        let s2 = this.template.querySelector('.lSpot2');         
        if (s2 !== null){
            s2.style.height = (this.spotTwo.length === 0) ? '50px' : 'auto';
        }
        
        let s3 = this.template.querySelector('.lSpot3');         
        if (s3 !== null){
            s3.style.height = (this.spotThree.length === 0) ? '50px' : 'auto';
        }
        
        let s4 = this.template.querySelector('.lSpot4');        
        if (s4 !== null){
             s4.style.height = (this.spotFour.length === 0) ? '50px' : 'auto';
        }

        let s5 = this.template.querySelector('.lSpot5');         
        if (s5 !== null){
            s5.style.height = (this.spotFive.length === 0) ? '50px' : 'auto';
        }
        
        let s6 = this.template.querySelector('.lSpot6');          
        if (s6 !== null){
            s6.style.height = (this.spotSix.length === 0) ? '50px' : 'auto';
        }

        let s7 = this.template.querySelector('.lSpot7');        
        if (s7 !== null){
            s7.style.height = (this.spotSeven.length === 0) ? '50px' : 'auto';           
        }

        let s8 = this.template.querySelector('.lSpot8');         
        if (s8 !== null){
            s8.style.height = (this.spotEight.length === 0) ? '50px' : 'auto'; 
        }

        let s9 = this.template.querySelector('.lSpot9');         
        if (s9 !== null){
            s9.style.height = (this.spotNine.length === 0) ? '50px' : 'auto';   
        }
        
        let s10 = this.template.querySelector('.lSpot10');         
        if (s10 !== null){
            s10.style.height = (this.spotTen.length === 0) ? '50px' : 'auto';  
        }
        
        let s11 = this.template.querySelector('.lSpot11');          
        if (s11 !== null){
            s11.style.height = (this.spotEleven.length === 0) ? '50px' : 'auto'; 
        }

        let s12 = this.template.querySelector('.lSpot12');          
        if (s12 !== null){
            s12.style.height = (this.spotTwelve.length === 0) ? '50px' : 'auto'; 
        }

        let s13 = this.template.querySelector('.lSpot13');         
        if (s13 !== null){
            s13.style.height = (this.spotThirteen.length === 0) ? '50px' : 'auto';
        }

        let s14 = this.template.querySelector('.lSpot14');  
        if (s14 !== null){
            s14.style.height = (this.spotFourteen.length === 0) ? '50px' : 'auto';
        }

        let s15 = this.template.querySelector('.lSpot15');  
        if (s15 !== null){
            s15.style.height = (this.spotFifteen.length === 0) ? '50px' : 'auto';
        }
        
        let s16 = this.template.querySelector('.lSpot16');          
        if (s16 !== null){
            s16.style.height = (this.spotSixteen.length === 0) ? '50px' : 'auto';
        }

        let s17 = this.template.querySelector('.lSpot17');         
        if (s17 !== null){
            s17.style.height = (this.spotSeventeen.length === 0) ? '50px' : 'auto'; 
        }

        let s18 = this.template.querySelector('.lSpot18');         
        if (s18 !== null){
            s18.style.height = (this.spotEighteen.length === 0) ? '50px' : 'auto'; 
        }

        let s19 = this.template.querySelector('.lSpot19');         
        if (s19 !== null){
            s19.style.height = (this.spotNineteen.length === 0) ? '50px' : 'auto'; 
        }
    }

    handleSpinner(){     
        let s1 = this.template.querySelector('.lSpot1');  
        if (s1 !== null){
            if (this.spotOne.length > 0){
                this.showSpinner1 = true;
            }
        }

        let s2 = this.template.querySelector('.lSpot2');  
        if (s2 !== null){
           if (this.spotTwo.length > 0){
               this.showSpinner2 = true;
           }
        }

        let s3 = this.template.querySelector('.lSpot3');  
        if (s3 !== null){
            if (this.spotThree.length > 0){
                this.showSpinner3 = true;
            }
        }

        let s4 = this.template.querySelector('.lSpot4');  
        if (s4 !== null){
           if (this.spotFour.length > 0){
               this.showSpinner4 = true;
           }
        }

        let s5 = this.template.querySelector('.lSpot5');  
        if (s5 !== null){
            if (this.spotFive.length > 0){
                this.showSpinner5 = true;
            }
        }

        let s6 = this.template.querySelector('.lSpot6');  
        if (s6 !== null){
           if (this.spotSix.length > 0){
               this.showSpinner6 = true;
           }
        }

        let s7 = this.template.querySelector('.lSpot7');  
        if (s7 !== null){
            if (this.spotSeven.length > 0){
                this.showSpinner7 = true;
            }
        }

        let s8 = this.template.querySelector('.lSpot8');  
        if (s8 !== null){
           if (this.spotEight.length > 0){
               this.showSpinner8 = true;
           }
        }

        let s9 = this.template.querySelector('.lSpot9');  
        if (s9 !== null){
            if (this.spotNine.length > 0){
                this.showSpinner9 = true;
            }
        }

        let s10 = this.template.querySelector('.lSpot10');  
        if (s10 !== null){
           if (this.spotTen.length > 0){
               this.showSpinner10 = true;
           }
        }

        let s11 = this.template.querySelector('.lSpot11');  
        if (s11 !== null){
            if (this.spotEleven.length > 0){
                this.showSpinner11 = true;
            }
        }

        let s12 = this.template.querySelector('.lSpot12');  
        if (s12 !== null){
           if (this.spotTwelve.length > 0){
               this.showSpinner12 = true;
           }
        }

        let s13 = this.template.querySelector('.lSpot13');  
        if (s13 !== null){
            if (this.spotThirteen.length > 0){
                this.showSpinner13 = true;
            }
        }

        let s14 = this.template.querySelector('.lSpot14');  
        if (s14 !== null){
           if (this.spotFourteen.length > 0){
               this.showSpinner14 = true;
           }
        }

        let s15 = this.template.querySelector('.lSpot15');  
        if (s15 !== null){
            if (this.spotFifteen.length > 0){
                this.showSpinner15 = true;
            }
        }

        let s16 = this.template.querySelector('.lSpot16');  
        if (s16 !== null){
           if (this.spotSixteen.length > 0){
               this.showSpinner16 = true;
           }
        }

        let s17 = this.template.querySelector('.lSpot17');  
        if (s17 !== null){
            if (this.spotSeventeen.length > 0){
                this.showSpinner17 = true;
            }
        }
        
        let s18 = this.template.querySelector('.lSpot18');  
        if (s18 !== null){
            if (this.spotEighteen.length > 0){
                this.showSpinner18 = true;
            }
        }

        let s19 = this.template.querySelector('.lSpot19');  
        if (s19 !== null){
            if (this.spotNineteen.length > 0){
                this.showSpinner19 = true;
            }
        }
    }
    
    cleanSpinner(){
        this.showSpinner1 = false;
        this.showSpinner2 = false;
        this.showSpinner3 = false;
        this.showSpinner4 = false;
        this.showSpinner5 = false;
        this.showSpinner6 = false;
        this.showSpinner7 = false;
        this.showSpinner8 = false;
        this.showSpinner9 = false;
        this.showSpinner10 = false;
        this.showSpinner11 = false;
        this.showSpinner12 = false;
        this.showSpinner13 = false;
        this.showSpinner14 = false;
        this.showSpinner15 = false;
        this.showSpinner16 = false;
        this.showSpinner17 = false;
        this.showSpinner18 = false;
        this.showSpinner19 = false;        
    }

    distributeTasks() {        
        let curSpotList = [];
        let curSpotListF = [];
        let curSpotListA = [];
        let curSpotOne = [];
        let curSpotTwo = [];
        let curSpotThree = [];        
        let curSpotFour = []; 
        let curSpotFive = []; 
        let curSpotSix = [];
        let curSpotSeven = [];
        let curSpotEight = [];
        let curSpotNine = [];
        let curSpotTen = [];
        let curSpotEleven = [];
        let curSpotTwelve = [];
        let curSpotThirteen = [];
        let curSpotFourteen = [];
        let curSpotFifteen = [];
        let curSpotSixteen = [];
        let curSpotSeventeen = [];       
        let curSpotEighteen = [];
        let curSpotNineteen = [];

        this.elementList.forEach(function(t){         
            switch (t.category) {
                case "LIST":
                    //distribute TYPE LIST by Id                    
                    if ((t.id === "5") || (t.id === "6") ||
                        (t.id === "7") || (t.id === "8") ||                                               
                        (t.id === "11") || (t.id === "13")){         
                            curSpotListF.push(t); //finance Menu
                    }else{
                        if ((t.id === "1") || (t.id === "2") ||
                            (t.id === "3") || (t.id === "9") ||                            
                            (t.id === "10") || (t.id === "12") ||
                            (t.id === "14") || (t.id === "15") || 
                            (t.id === "16") || (t.id === "17") || 
                            (t.id === "18") || (t.id === "19")  
                           ){
                                curSpotList.push(t); //pipeline Menu
                        }else{
                            if ((t.id === "4")){
                                    curSpotListA.push(t); //Admission Menu
                                }
                        }
                    }
                    break;
                case "ONE":                    
                    curSpotOne.push(t);                                        
                    break;
                case "TWO":
                    curSpotTwo.push(t);  
                    break;
                case "THREE":
                    curSpotThree.push(t);  
                    break;
                case "FOUR":
                    curSpotFour.push(t);  
                    break;
                case "FIVE":
                    curSpotFive.push(t);  
                    break;
                case "SIX":
                    curSpotSix.push(t);  
                    break;
                case "SEVEN":
                    curSpotSeven.push(t);  
                    break;
                case "EIGHT":
                    curSpotEight.push(t);
                    break;  
                case "NINE":
                    curSpotNine.push(t);
                    break;  
                case "TEN":
                    curSpotTen.push(t);
                    break; 
                case "ELEVEN":
                    curSpotEleven.push(t);
                    break;  
                case "TWELVE":
                    curSpotTwelve.push(t);
                    break;   
                case "THIRTEEN":
                    curSpotThirteen.push(t);
                    break;
                case "FOURTEEN":
                    curSpotFourteen.push(t);
                    break;
                case "FIFTEEN":
                    curSpotFifteen.push(t);
                    break;
                case "SIXTEEN":
                    curSpotSixteen.push(t);
                    break;  
                case "SEVENTEEN":
                    curSpotSeventeen.push(t);
                    break; 
                case "EIGHTEEN":
                    curSpotEighteen.push(t);
                    break; 
                default:
                    curSpotNineteen.push(t);
                    break;                    
            }            
        });

        this.spotList = curSpotList; 
        this.spotListF = curSpotListF;        
        this.spotListA = curSpotListA;        
        this.spotOne = curSpotOne;
        this.spotTwo = curSpotTwo;       
        this.spotThree = curSpotThree;
        this.spotFour = curSpotFour; 
        this.spotFive = curSpotFive; 
        this.spotSix = curSpotSix; 
        this.spotSeven = curSpotSeven;         
        this.spotEight = curSpotEight;
        this.spotNine = curSpotNine;
        this.spotTen = curSpotTen;
        this.spotEleven = curSpotEleven;
        this.spotTwelve = curSpotTwelve;
        this.spotThirteen = curSpotThirteen;
        this.spotFourteen = curSpotFourteen;        
        this.spotFifteen = curSpotFifteen;
        this.spotSixteen = curSpotSixteen;
        this.spotSeventeen = curSpotSeventeen;         
        this.spotEighteen = curSpotEighteen;   
        this.spotNineteen = curSpotNineteen;   
        this.verifyTypeClicked();        
    }

    verifyTypeClicked(){
        
        let f = this.template.querySelector('lightning-tabset');
        if (f !== null){
            //console.log(' -activeTab: ' + f.activeTabValue);
            if (f.activeTabValue === '0'){            
                this.handleReportTypeFClick();                        
            }else{
                if (f.activeTabValue === '1'){                
                    this.handleReportTypePClick();
                }else{
                    if (f.activeTabValue === '2'){                
                        this.handleReportTypeAClick();
                    }
                }
            }
        }    
    }

    handleDragOver(evt) { 
        //console.log(evt + ' handleDragOver ' + JSON.stringify(evt));    
        evt.dataTransfer.dropEffect = 'move';           
        evt.preventDefault();
    }

    handleListItemDrag(evt) {   
        //console.log(evt + ' handleListItemDrag ' + JSON.stringify(evt.detail));           
        this.draggingId = evt.detail;               
    }

    get tabs() {                
        if (tabs.length === 0){
            for (let i = 0; i < NUM_OF_TABS; i++) {
                tabs.push({
                    value: i,
                    label: (i === 0) ? 'Finance' : (i === 1) ? 'Pipeline' : 'Admission',
                    content: (i === 0) ? this.handleReportTypeFClick() : '',
                });
            }
        }
        return tabs;
    }

    handleActive(event) {        
        
        if (event.target.value === '0'){            
            this.handleReportTypeFClick();                        
        }else{
            if (event.target.value === '1'){                
                this.handleReportTypePClick();
            }else{
                if (event.target.value === '2'){                
                    this.handleReportTypeAClick();
                }
            }
        }
    }    

    handleReportTypeFClick(){          
        //console.log('spotListF ' + JSON.stringify(this.spotListF)); 
        this.spotListAux = [];
        this.spotListAux = this.spotListF;                        
    }

    handleReportTypePClick(){       
        //console.log('spotListS ' + JSON.stringify(this.spotList));         
        this.spotListAux = [];       
        this.spotListAux = this.spotList;  
    }

    handleReportTypeAClick(){          
        //console.log('spotListA ' + JSON.stringify(this.spotListA)); 
        this.spotListAux = [];
        this.spotListAux = this.spotListA;                        
    }

    handleReportClick(){    
        this.isTypeClicked = false;    
        if (this.isClicked === true){
            this.isClicked = false;
        }else{
            this.isClicked = true;            
        }   
    }

    handleItemDrop(evt) {
        this.isChange = true;
        let id = this.draggingId;
        let catSelected = evt.detail;              
        
        this.onVerifyItens(catSelected);       
        let elementListAux =[];
        for (let i = 0; i < this.elementList.length; i++) {
            let parent = this.elementList[i];                        
            if(parent.id === id)
            {                
                parent = {   
                    id: id,
                    name:parent.name,    
                    period: parent.period,
                    criteria: parent.criteria,    
                    category:catSelected
                } 
                this.draggingEl = true;                 
                if(parent.category !== "LIST") {                
                    if (this.hasItem === true){
                        parent = {   
                            id: id,
                            name:parent.name, 
                            period: parent.period,
                            criteria: parent.criteria,       
                            category:"LIST"
                        }
                    }
                }                                
            }                      
            elementListAux.push(parent);
        }    
    
        this.elementList = elementListAux;                
        this.distributeTasks();
        this.handleResize();        
    }   

    verifyQtdReports(){
        let elementListAux =[];
        //if some reports are on spot number > than qt spots visible, move then to LIST. 
        for (let i = 0; i < this.elementList.length; i++) {
            let parent = this.elementList[i];                           
            if (parent.category !== "LIST") { 
                this.catNumber = parent.category;
                this.getCategoryNumber();               
                if (this.retCatNumber > this.elementList.length) {
                    parent = {   
                        id: parent.id,
                        name:parent.name, 
                        period: parent.period,
                        criteria: parent.criteria,       
                        category:"LIST"
                    }
                }
            }                  
            elementListAux.push(parent);
        }    
    
        this.elementList = elementListAux;  
    }

    getCategoryNumber(){

        switch (this.catNumber) {            
            case "ONE":                    
            this.retCatNumber = 1;                                        
               break;
            case "TWO":
                this.retCatNumber = 2;  
                break;
            case "THREE":
                this.retCatNumber = 3;
                break;
            case "FOUR":
                this.retCatNumber = 4;  
                break;
            case "FIVE":
                this.retCatNumber = 5;  
                break;
            case "SIX":
                this.retCatNumber = 6;  
                break;
            case "SEVEN":
                this.retCatNumber = 7;  
                break;
            case "EIGHT":
                this.retCatNumber = 8;
                break;  
            case "NINE":
                this.retCatNumber = 9;
                break;  
            case "TEN":
                this.retCatNumber = 10;
                break; 
            case "ELEVEN":
                this.retCatNumber = 11;
                break;  
            case "TWELVE":
                this.retCatNumber = 12;
                break;   
            case "THIRTEEN":
                this.retCatNumber = 13;
                break;
            case "FOURTEEN":
                this.retCatNumber = 14;
                break;
            case "FIFTEEN":
                this.retCatNumber = 15;
                break;
            case "SIXTEEN":
                this.retCatNumber = 16;
                break;                                                 
            case "SEVENTEEN":
                this.retCatNumber = 17;
                break; 
            case "EIGHTEEN":
                this.retCatNumber = 18;
                break; 
            default:
                this.retCatNumber = 19;
                break;                                 
        }             
    }

    handleListApplyReturn(evt){                       
         this.applyClick = evt.detail;                      
    }

    onVerifyItens(cat)    
    {              
        this.hasItem = false;        
        let l = this.elementList.filter(x => x.category.indexOf(cat) > -1);                        
        if (l.length > 0){
            this.hasItem = true;        
        }           
    }   

    handleCancelSettings(){          
        //this.wiredCallback(this._wiredResult);
        this.getReportsList(true);
    }

    handleApplyFilter(){   
        this.onValidation();
        if (this.isValid){  
            this.cleanSpinner();
            this.handleSpinner();
            this.showSpinner = true;   
            this.applyClick = true;  
            this.getReportsData(); 
        }else{
           // console.log('ApplyFilter ');
            this.msgToast = "Please fill up all the mandatory fields.";
            this.typeToast = 'info'; 
            this.toast = true;    
            this.showSpinner = false;    
            this.cleanSpinner();
        }                     
    }

    onValidation() {
    
        this.isValid = false;

        const allValidLI = [...this.template.querySelectorAll('lightning-input')]
        .reduce((validSoFar, inputCmp) => {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);                     

        //group
        const filterItems = (l) => {
            return this.crsGroup.filter(x => x.value.toLowerCase().indexOf(l.toLowerCase()) > -1);
        }
        const grpValid = filterItems(this.groupSelected);         
        if (grpValid.length > 0)
        {
            this.groupSelected = grpValid[0].value;
            this.groupSelectedId = grpValid[0].label;
            //console.log(JSON.stringify(grpValid));            
        }     
        else{                              
            this.groupSelectedId = undefined;
            return;
        }

        // agency
        const filterItems1 = (l) => {
           return this.crsAgency.filter(x => x.value.toLowerCase().indexOf(l.toLowerCase()) > -1);
        }
        const agcValid = filterItems1(this.agencySelected);         
        if (agcValid.length > 0)
        {
            this.agencySelected = agcValid[0].value; 
            this.agencySelectedId = agcValid[0].label;
            //console.log(JSON.stringify(agcValid));             
        }     
        else{                              
            this.agencySelectedId = undefined;
            return;
        }

        // department
        const filterItems3 = (l) => {
            return this.crsDepartment.filter(x => x.value.toLowerCase().indexOf(l.toLowerCase()) > -1);
        }
        const dptValid = filterItems3(this.departmentSelected);         
        if (dptValid.length > 0)
        {
            this.departmentSelected = dptValid[0].value; 
            this.departmentSelectedId = dptValid[0].label; 
            //console.log(JSON.stringify(dptValid));           
        }     
        else{                              
            this.departmentSelectedId = undefined;
            return;
        }

        // user
        const filterItems4 = (l) => {
            return this.crsUsers.filter(x => x.value.toLowerCase().indexOf(l.toLowerCase()) > -1);
        }
        const useValid = filterItems4(this.usersSelected);         
        if (useValid.length > 0)
        {
            this.usersSelected = useValid[0].value;
            this.usersSelectedId = useValid[0].label;
            //console.log(JSON.stringify(useValid));                
        }     
        else{                              
            this.usersSelectedId = undefined;
            return;
        }

        if (allValidLI){
            this.isValid = true;   
        }
    }

    getReportsData(){   
        console.log('this.groupSelectedId ' + this.groupSelectedId);
        console.log('this.agencySelectedId ' + this.agencySelectedId);
        console.log('this.elementList ' + JSON.stringify(this.elementList));
            
        if ((this.groupSelectedId !== undefined) && (this.groupSelectedId !== '') &&
            (this.agencySelectedId !== undefined) && (this.agencySelectedId !== '') &&
            (this.usersSelectedId !== undefined) && (this.usersSelectedId !== '') &&
            (this.departmentSelectedId !== undefined) && (this.departmentSelectedId !== '') &&
            (this.elementList.length > 0)  //(JSON.stringify(this.elementList) !== undefined) && (JSON.stringify(this.elementList) !== '') 
           ){            
            this.resultFilter = undefined; 
            console.log('this.departmentSelectedId ' + this.departmentSelectedId);
            console.log('this.usersSelectedId ' + this.usersSelectedId);

            getDashboardReports({ selectedAgencyGroup: this.groupSelectedId, selectedAgency: this.agencySelectedId, 
                                selectedDepartment: this.departmentSelectedId, selectedUser: this.usersSelectedId,
                                reportSettings: JSON.stringify(this.elementList)
                                }).then(result => {            
                                if(result !== null){                                                      
                                    this.resultFilter = result;
                                    this.showSpinner = false;                                                                        
                                }   
                                console.log('resultFilter : ' + JSON.stringify(this.resultFilter));
                                this.cleanSpinner();                                        
            }).catch(error => {                                
                this.msgToast = "Error creating charts!";
                this.typeToast = 'error'; 
                this.toast = true;             
                console.log('getReports Error: ' + JSON.stringify(error));      
                this.error = error;
                this.showSpinner = false;  
                this.cleanSpinner();
            });     
        }
        else{
            //console.log('Please fill up');
            this.msgToast = "Please fill up all the mandatory fields.";
            this.typeToast = 'info'; 
            this.toast = true;    
            this.showSpinner = false;  
            this.cleanSpinner();  
        }         
    }
    
    handleClose(){        
        this.toast = false;
        this.msgToast ='';
        this.typeToast ='';
    }

    handleSubmit(){
        this.toast = false;     
        if (this.elementList.length > 0){
            const objJson = JSON.stringify(this.elementList);             
            console.log('objJson: ' + objJson);
            updateDashboardReport({objJson}).then(result => {               
                if(result === 'success'){
                    this.isChange = false;
                   // this._wiredResult = {data:this.elementList};
                }else{                                                             
                    this.msgToast = 'Error update record';
                    this.typeToast = 'error'; 
                    this.toast = true;                    
                }            
            })                  
        }   
    }  

    handleover(event){
        event.preventDefault();           
       
        if ((event.currentTarget.classList.contains('spotsCol1')) || (event.currentTarget.classList.contains('spotsCol2'))) {                                   
           event.currentTarget.style.background= "rgb(45, 43, 182)";
        }
    }    
    
    dropped(){
               
        Array.from(
            this.template.querySelectorAll('.spotsCol1')
        ).map(element => {            
            element.style.background= "";            
            return element;
        }); 

        Array.from(
            this.template.querySelectorAll('.spotsCol2')
        ).map(element => {            
            element.style.background= "";            
            return element;
        });        
    }    
}