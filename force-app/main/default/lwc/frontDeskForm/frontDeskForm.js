/* eslint-disable @lwc/lwc/no-async-operation */
/* eslint-disable no-console */
import imageResource from '@salesforce/resourceUrl/ipadfiles';
import { LightningElement, track, api, wire } from 'lwc';
//Contact
import CONTACT_OBJECT from '@salesforce/schema/Contact';
import FIRSTNAME_FIELD from '@salesforce/schema/Contact.FirstName';              
import LASTNAME_FIELD from '@salesforce/schema/Contact.LastName'; 
import NATIONALITY_FIELD from '@salesforce/schema/Contact.Nationality__c';  
import BIRTHDATE_FIELD from '@salesforce/schema/Contact.Birthdate';  
import EMAIL_FIELD from '@salesforce/schema/Contact.Email';
//import MOBILEPHONE_FIELD from '@salesforce/schema/Contact.MobilePhone';
import CONTACTPREFERENCE_FIELD from '@salesforce/schema/Contact.Form_ContactPreference__c';               
import CONTACTBY_FIELD from '@salesforce/schema/Contact.Form_ContactBy__c';
import SCHOOL_FIELD from '@salesforce/schema/Contact.Form_School__c';                 
import COURSE_FIELD from '@salesforce/schema/Contact.Form_Course__c';                                                    
import LEADSOURCE_FIELD from '@salesforce/schema/Contact.LeadSource';
import LEADSOURCESPECIFIC_FIELD from '@salesforce/schema/Contact.Lead_Source_Specific__c'; 
import AGREEMENTIMAGE_FIELD from '@salesforce/schema/Contact.Form_AgreementImg__c';             
//Client_Document__c
import CLIENTDOCUMENT_OBJECT from '@salesforce/schema/Client_Document__c';
import VISATYPE_FIELD from '@salesforce/schema/Client_Document__c.Visa_type__c';
import EXPIRYDATEVISA_FIELD from '@salesforce/schema/Client_Document__c.Expiry_Date__c';
//methods
import getClientDocument from '@salesforce/apex/lwcFrontDesk.getClientDocument'; 
import getWrapperFormsOfContact from '@salesforce/apex/lwcMergeForm.getWrapperFormsOfContact';
import createContactFromDeskForm from '@salesforce/apex/lwcFrontDesk.createContactFromDeskForm';
import updateContactFromDeskForm from '@salesforce/apex/lwcFrontDesk.updateContactFromDeskForm';
import verifyPassword from '@salesforce/apex/lwcFrontDesk.verifyPassword';
import getNationality from '@salesforce/apex/lwcFrontDesk.getNationality';
import getVisaType from '@salesforce/apex/lwcFrontDesk.getVisaType';
import getChannel from '@salesforce/apex/lwcFrontDesk.getChannel';
import getLeadSourceSpecific from '@salesforce/apex/lwcFrontDesk.getLeadSourceSpecific';

export default class FrontDeskForm extends LightningElement {
    @api isnewstudent;
    @api recContact;
    today = new Date();
    @track isSubmited = false;
    @track img = imageResource + '/thankyou.jpg';  
    @track isTime = false;
    @track isInicial = true;
    @track isFinish = false;
    @track viewQueue = false;
    @track contact;
    @track recContactId;
    @track isValid = false;
    @track error;        
    @track password='';
    @track firstNameSelected ='';
    @track lastNameSelected ='';
    @track crsNationality;
    @track crsNationalityAux; 
    @track nationalitySelected ='';
    @track isNat = false;
    @track dtBirthSelected ='';   
    @track emailSelected = '';    
    @track facebookSelected ='';
    @track instagramSelected ='';
    @track mobileSelected ='';    
    @track isDiffChecked = false;
    @track isFacebookChecked = false;
    @track disFacebook = false;
    @track reqFacebook = false;
    @track reqInstagram = false;
    @track reqImageUsage = false;
    @track reqEmail = true;
    @track reqPhone = true;
    @track reqWhatsApp = false;
    @track disInstagram = false;
    @track isInstagramChecked = false;
    @track whatsAppSelected = '';
    @track crsTypeVisa;
    @track crsTypeVisaAux;  
    @track isVisa = false;
    @track typeVisaSelected= '';
    @track dtVisaExpirySelected= '';
    @track optPreferenceSelected= '';
    @track optConBySelected= '';   
    @track schoolSelected= '';
    @track courseSelected= '';
    @track isChannel = false
    @track crsChannel;
    @track crsChannelAux;
    @track channelSelected= '';
    @track imgUsageChecked = false;    
    @track hasEmails = false;
    @track hasPhones = false;    
    @track listEmail = [];
    @track listPhone = [];   
    @track titleForm = "Well Done!";
    @track titleIcon = "standard:task2";
    @track isCanceled = false;
    @track studentName = '';
    @track formRec=[];
    @track bShowModal = false;
    @track toast = false;    
    @track msgToast = '';
    @track typeToast = '';
    @track reqNat = true;
    @track reqChannel = true;
    @track reqVisa = true;        
    @track emailNav = '';
    @track crsLsSpecific;
    @track crsLsSpecificAux;
    @track islsSpecific= false; 
    @track lsSpecificSelected= '';
    @track reqlsSpecific = false;
    @track disLsSpecific = true;

    @track imgUsageAgree = "<p style=\"text-align: justify;font-size: 11px;\">Information Planet would like to request your permission for use of photographs that might contain your image.<strong> If you are a IP Member</strong>, photos from parties and IP events where you might participate will be posted on social media and used " +
        "for some marketing purposes such as on our website. By signing this contract you authorize Information Planet to utilize your image as mentioned above.</p>" +
        "<p style=\"font-size: 11px;\">Conditions of use:</p>" +
        "<p style=\"text-align: justify;font-size: 11px;\">1. This permission is valid for one year from the date of signing, which means that even if you return to your home country, Information Planet can stil use your photos and data.</p>" +
        "<p style=\"text-align: justify;font-size: 11px;\">2. Information Planet will not use the photographs or any other information provided for any other purposes.</p>" +
        "<p style=\"text-align: justify;font-size: 11px;\">3. Your consent (to the publication of your photos) can be withdraw at any time.</p>";
    
    @track optBy = [
        { label: 'Email', value: 'Email' },
        { label: 'Phone Call', value: 'Phone Call' },
        { label: 'SMS', value: 'SMS' },
        { label: 'WhatsApp', value: 'WhatsApp' }
    ];

    @track optPref = [
        { label: 'Morning', value: 'Morning' },
        { label: 'Afternoon', value: 'Afternoon' },
        { label: 'Evening', value: 'Evening' },
    ];   
    
    @wire(getChannel)
    ChannelPicklistValues ({error, data}) {        
        if (data) {            
            this.crsChannel = data;
            this.crsChannelAux = data;           
            this.isChannel = false;               
        } else {
            this.crsChannel = undefined;
            this.crsChannelAux = undefined;  
            this.error = error;             
        }       
    } 

    @wire(getNationality)
    NationalityPicklistValues ({error, data}) {        
        if (data) {            
            this.crsNationality = data;
            this.crsNationalityAux = data;           
            this.isNat = false;    
        } else {
            this.crsNationality = undefined;
            this.crsNationalityAux = undefined;  
            this.error = error;             
        }       
    }   

    @wire(getVisaType)
    TypeVisaPicklistValues ({error, data}) {        
        if (data) {            
            this.crsTypeVisa = data;          
            this.crsTypeVisaAux = data;           
            this.isVisa = false;               
        } else {
            this.crsTypeVisa = undefined;
            this.crsTypeVisaAux = undefined;  
            this.error = error;             
        }       
    }    

    //called by parent frontDeskQueue.js
    @api
    openToEdit(){        
        this.contact = this.recContact;      
        console.log('openToEdit ' + JSON.stringify(this.contact));  
        if (this.contact !== undefined){            
            this.recContactId = this.contact.Id;
            this.firstNameSelected = this.contact.FirstName;
            this.lastNameSelected = this.contact.LastName;            
            this.nationalitySelected = this.contact.Nationality;                        
            this.dtBirthSelected = this.contact.Birthdate;    

            if ((this.contact.Email === undefined) || ((this.contact.Email === null)))
            {     
                this.emailSelected  = '';       
            }else{
                this.emailSelected = this.contact.Email;   
            }

            if ((this.contact.MobilePhone === undefined) || ((this.contact.MobilePhone === null)))
            {
                this.mobileSelected = ''; 
            }
            else{
                this.mobileSelected = this.contact.MobilePhone; 
            }     
            this.optPreferenceSelected = this.contact.FormContactPreference;
            this.optConBySelected = this.contact.FormContactBy; 
            this.schoolSelected = this.contact.FormSchool;
            this.courseSelected = this.contact.FormCourse;
            this.channelSelected = this.contact.LeadSourceChannel; 
            this.lsSpecificSelected = this.contact.LeadSourceSpecific;  /// ?????????????                
            this.imgUsageChecked = this.contact.FormAgreementImage;

            this.getContactForm();
            this.getClientForm();
        }   
    }      

    getClientForm(){
        console.log('getClientDocument - contactId ' + this.recContactId);
        getClientDocument({contactId: this.recContactId})
                .then(result => {
                    console.log('getClientDocument result ' + JSON.stringify(result));
                    if (result) {                               
                        console.log('Expiry_Date__c ' + result.Expiry_Date__c);
                        if (result.Expiry_Date__c !== undefined) {                       
                            this.dtVisaExpirySelected = result.Expiry_Date__c;
                        }
                        console.log('Visa_type__c ' + result.Visa_type__c);
                        if (result.Visa_type__c !== undefined) { 
                            this.typeVisaSelected = result.Visa_type__c;
                        }                        
                    }                                   
                });
    }

    getContactForm(){

        getWrapperFormsOfContact({contactId: this.recContactId})
        .then(result => {
            //console.log('getContactForm result --> ' + JSON.stringify(result));       
            if (result) {                                                 
                if (result.email !== undefined) {                
                    //console.log('result.email ' + JSON.stringify(result.email));    
                    if (result.email.length > 0){     
                        this.hasEmails = true; 
                        this.reqEmail = false;
                        for(let i=0;i < result.email.length;i++){
                            let res = result.email[i];
                            if (this.emailSelected !== res){  
                                this.listEmail.push(res);
                            }
                        }                        
                    }
                } 
                console.log('result.mobile ' + JSON.stringify(result.mobile)); 
                if (result.mobile !== undefined) {                       
                    if (result.mobile.length > 0){                       
                        this.hasPhones = true;
                        this.reqPhone = false;
                        for(let i=0;i < result.mobile.length;i++){
                            let res = result.mobile[i];
                            if (this.mobileSelected !== res){  
                                this.listPhone.push(res);
                            }
                        }                                             
                    }
                }       
                this.mobileSelected = this.mobileSelected;//.replace('+61','').trim();        
                //console.log('result.whatsapp ' + JSON.stringify(result.whatsapp)); 
                if (result.whatsapp !== undefined) {                
                    if (result.whatsapp.length > 0){
                        this.whatsAppSelected = result.whatsapp[0];
                        this.onShowsWhatsApp();
                    }
                }
                //console.log('result.instagram ' + JSON.stringify(result.instagram)); 
                if (result.instagram !== undefined) {
                    if (result.instagram.length > 0){
                        this.instagramSelected = result.instagram[0];
                    }
                }
                //console.log('result.facebook ' + JSON.stringify(result.facebook));
                if (result.facebook !== undefined) {
                    if (result.facebook.length > 0){
                        this.facebookSelected = result.facebook[0];
                    }
                }              
            }                          
        })
    } 

    onShowsWhatsApp(){
        if ((this.mobileSelected !== undefined) && (this.whatsAppSelected !== undefined)){    
            if (this.mobileSelected !== this.whatsAppSelected){
                this.isDiffChecked = true;                
            }             
        }else{
            if (this.whatsAppSelected !== undefined){
                this.isDiffChecked = true;
            }
        }
    }

    handleFaceEndFocus(){
        
        if ((this.facebookSelected !== undefined) && (this.facebookSelected !== null)){            
            if ((this.facebookSelected.trim().length === 0) && (this.isFacebookChecked === false)){
                this.reqFacebook = true;                
            }
            else
            {                
                this.reqFacebook = false; 
            }
        }else{                        
           if (this.isFacebookChecked === false){
               this.reqFacebook = true; 
            }else
            {
                this.reqFacebook = false; 
            }
        }
    }
    // onblur={handleMobileEndFocus} 
    // handleEmailEndFocus(){
        // if ((this.emailSelected !== undefined) && (this.emailSelected !== null) && (this.listEmail !== undefined)){              
        //     if ((this.emailSelected.trim().length === 0) && (this.listEmail.length === 0)){
        //         this.reqEmail = true;                      
        //     }
        //     else
        //     {                   
        //         this.reqEmail = false; 
        //     }
        // }else{   
        //     if (this.listEmail !== undefined) {                                      
        //         if (this.listEmail.length === 0){                        
        //                 this.reqEmail = true; 
        //             }else
        //             {                        
        //                 this.reqEmail = false; 
        //             }
        //     }
        //     else{                
        //         this.reqEmail = true; 
        //     }
        // }
    // }

    handleInsEndFocus(){
        
        if ((this.instagramSelected !== undefined) && (this.instagramSelected !== null)){            
            if ((this.instagramSelected.trim().length === 0) && (this.isInstagramChecked === false)){
                this.reqInstagram = true;                
            }
            else
            {                
                this.reqInstagram = false; 
            }
        }else{                        
           if (this.isInstagramChecked === false){
               this.reqInstagram = true; 
            }else
            {
                this.reqInstagram = false; 
            }
        }
    }
    // onblur={handleEmailEndFocus}
    // handleMobileEndFocus(){

        // if ((this.mobileSelected !== undefined) && (this.mobileSelected !== null) && (this.listPhone !== undefined)){  
            
        //     if ((this.mobileSelected.trim().length === 0) && (this.listPhone.length === 0)){
        //         this.reqPhone = true;                      
        //     }
        //     else
        //     {                            
        //         this.reqPhone = false; 
        //     }
        // }else{   
        //     if (this.listPhone !== undefined) {               
        //         if (this.listPhone.length === 0){            
        //                 this.reqPhone = true; 
        //             }else
        //             {            
        //                 this.reqPhone = false; 
        //             }
        //     }
        //     else{            
        //         this.reqPhone = true; 
        //     }
        // }       
    //}

    handleNationalityFocus(){       
        this.crsNationality = this.crsNationalityAux;        
        this.isNat = true;
        this.reqNat = false;
    }

    handleBirthFocus(){
        this.isNat = false;
    }

    handleEmailFocus(){
        this.isNat = false;
    }

    handleTypeVisaFocus(){
        this.crsTypeVisa = this.crsTypeVisaAux;    
        this.isVisa = true;
        this.reqVisa = false;
    }

    handleTypeVisaBlur(){       
        this.reqVisa = true;         
    }

    handleDtVisaExpiryFocus(){
        this.isVisa = false;
    }

    handlelsSpecificFocus(){
        this.crsLsSpecific = this.crsLsSpecificAux;        
        this.islsSpecific = true;       
        this.reqlsSpecific = false; 
    }    
    
    handleSpecificBlur(){
        this.reqlsSpecific = true; 
    }

    getLeadSourceSpecification(){

        this.isSpecific = false;        
        this.disLsSpecific = true;
        this.reqlsSpecific = false; 
        this.lsSpecificSelected = '';  
        this.crsLsSpecific = [];
        this.crsLsSpecificAux = []; 
        console.log('getLeadSourceeSpecification ' + this.channelSelected);             
        getLeadSourceSpecific({ leadSource :this.channelSelected}).then(result => {
            console.log('result ' +  JSON.stringify(result));
            if (result) {
                this.crsLsSpecific = result;
                this.crsLsSpecificAux = result;  
                if (this.crsLsSpecific.length > 0){
                    console.log('result 1');
                    this.isSpecific = true;
                    this.disLsSpecific = false;
                    this.reqlsSpecific = true;
                }            
            }                                
        })
        .catch(error => {                  
            this.error = error;            
        });                       
    }

    handleChannelFocus(){
        this.crsChannel = this.crsChannelAux;
        this.isChannel = true;
        this.reqChannel = false;
    }

    handleChannelBlur(){       
        this.reqChannel = true;         
    }

    handleImgUsageFocus(){
        this.isChannel = false;
    }

    handleNationalityChange(){           
        //this.searchNationality = event.target.value;       
        this.isNat = false;
        if (this.nationalitySelected.length > 0){
            this.crsNationality = this.crsNationalityAux;      
        
            const filterItems = (l) => {
                return this.crsNationality.filter(x => x.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }
            this.isNat = true;  
            this.crsNationality = filterItems(this.nationalitySelected);
        }      
    }
    
    handleNationalityClicked(event){
        this.nationalitySelected = event.target.dataset.item;
        this.isNat = false;        
    }

    handleNationalityBlur(){       
        this.reqNat = true;         
    }

    handleVisaTypeChange(){           
        
        this.isVisa = false;
        if (this.typeVisaSelected.length > 0){
            this.crsTypeVisa = this.crsTypeVisaAux;      
        
            const filterItems = (l) => {
                return this.crsTypeVisa.filter(x => x.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }
            this.isVisa = true;  
            this.crsTypeVisa = filterItems(this.typeVisaSelected);
        }      
    }
    
    handleVisaTypeClicked(event){
        this.typeVisaSelected = event.target.dataset.item;
        this.isVisa = false;
    }

    handleChannelClicked(event){
        this.channelSelected = event.target.dataset.item;
        this.isChannel = false;
        this.getLeadSourceSpecification();
    }
    
    handlelsSpecificClicked(event){        
        this.lsSpecificSelected = event.target.dataset.item;
        this.islsSpecific = false;        
    }

    handleSpecificChange(){
        this.islsSpecific = false;
        console.log('crsLsSpecific: ' + this.crsLsSpecific);
        if (this.lsSpecificSelected.length > 0){
            this.crsLsSpecific = this.crsLsSpecificAux;      
        
            const filterItems = (l) => {
                return this.crsLsSpecific.filter(x => x.value.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }
            this.islsSpecific= true;  
            this.crsLsSpecific = filterItems(this.lsSpecificSelected);            
        }
    }

    handleChannelChange(){           
        
        this.isChannel = false;
        
        if (this.channelSelected.length > 0){
            this.crsChannel = this.crsChannelAux;      
        
            const filterItems = (l) => {
                return this.crsChannel.filter(x => x.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }
            this.isChannel= true;  
            this.crsChannel = filterItems(this.channelSelected);            
        }
        this.getLeadSourceSpecification();      
    }    

    handleFieldsChange(event) {        
        const fieldName = event.target.name;

        // this[event.target.name] = event.target.value;
        
        switch (fieldName) {            
            case 'txtFstName':
                this.firstNameSelected = event.detail.value;
                break;
            case 'txtLstName':
                this.lastNameSelected = event.detail.value;
                break;
            case 'dtpDtBirth':
                this.dtBirthSelected = event.detail.value;
                break;
            case 'txtEmail':
                this.emailSelected = event.target.value;                                
                break;            
            case 'txtFacebook':
                this.facebookSelected = event.detail.value;               
                break;
            case 'txtInstagram':
                this.instagramSelected = event.detail.value;                
                break;
            case 'txtMobileNro':
                this.mobileSelected = event.target.value;                 
                break;
            case 'chkDiffWhatAppNro':
                this.isDiffChecked = event.target.checked;                
                if (!this.isDiffChecked){
                    this.whatsAppSelected = '';
                    this.reqWhatsApp = false;
                }else
                {
                    this.reqWhatsApp = true;
                }
                break;
            case 'chkFacebook':
                this.isFacebookChecked = event.target.checked;                
                if(this.isFacebookChecked === true){                                        
                    this.facebookSelected = '';                                      
                    this.reqFacebook = false;                    
                    this.disFacebook = true;                   
                }       
                else
                {                 
                    this.disFacebook = false;  
                    this.reqFacebook = true;                 
                }         
                break;
            case 'chkInstagram':
                this.isInstagramChecked = event.target.checked;                
                if(this.isInstagramChecked === true){ 
                    this.instagramSelected = '';                    
                    this.reqInstagram = false;
                    this.disInstagram = true;                     
                }                
                else{
                    this.disInstagram = false; 
                    this.reqInstagram = true;                    
                }
                break;                
            case 'txtWhatsAppFone':
                this.whatsAppSelected = event.detail.value;
                this.reqWhatsApp = false;
                break;
            case 'schVisa':
                this.typeVisaSelected = event.target.value;
                this.handleVisaTypeChange();
                break;
            case 'dtpVisaExpiry':
                this.dtVisaExpirySelected = event.target.value;
                break;
            case 'rdbPrefGroup':
                this.optPreferenceSelected = event.target.value;
                break;
            case 'rdbByGroup':
                this.optConBySelected = event.target.value;
                break;            
            case 'chkSchool':
                this.schoolSelected = event.target.value;
                break;
            case 'chkCourse':
                this.courseSelected = event.target.value;
                break;
            case 'schChannels':
                this.channelSelected = event.target.value;
                this.handleChannelChange();
                break;
            case 'schlsSpecifics':
                    this.lsSpecificSelected = event.target.value;
                    this.handleSpecificChange();
                    break;   
            case 'schNationality':
                this.nationalitySelected = event.detail.value; 
                this.handleNationalityChange();                               
                break; 
            case 'txtPassword':  
                this.password = event.detail.value;  
                break;   
            default: //chkImageUsage
                this.imgUsageChecked = event.target.checked;
                if (this.imgUsageChecked){
                    this.reqImageUsage = false;
                }else{
                    this.reqImageUsage = true;
                }
                
                break;
        }
    }         
    
    handleAddEmail(event){        
        this.handleClose(); 
        let emailSel = event.target.value;

        Array.from(
            this.template.querySelectorAll('lightning-input')
        ).filter(element => element.name === 'txtEmail')         
        .map(element => {
               if (!element.validity.valid){
                emailSel = undefined;
               }
               return emailSel;
        });

        if ((emailSel !== undefined) && (emailSel !== null)){  
            if (emailSel.trim().length > 0){
                let index = this.listEmail.indexOf(emailSel);                
                if (index !== -1){ 
                    this.msgToast = "Email already exists!";
                    this.typeToast = 'info'; 
                    this.toast = true;   
                    console.log('handleAddEmail');                           
                }                
                else {                    
                    this.listEmail.push(emailSel);
                    this.hasEmails = true;    
                    this.reqEmail = false;       
                    this.emailSelected = '';                        
                }
            } 
        }
    }

    handleClose(){
        console.log('handleClose'); 
        this.toast = false;
        this.msgToast ='';
        this.typeToast ='';
    }

    handleAddPhone(event){            
        this.handleClose();      
        let foneSelected = event.target.value;        
        
        Array.from(
            this.template.querySelectorAll('lightning-input')
        ).filter(element => element.name === 'txtMobileNro')         
        .map(element => {
               if (!element.validity.valid){
                   foneSelected = undefined;
               }
               return foneSelected;
        });
        
           
        if ((foneSelected !== undefined) && (foneSelected !== null)){             
            if (foneSelected.trim().length > 0)
            {                
                let index = this.listPhone.indexOf('+61 ' + foneSelected);                
                if (index !== -1){                   
                    this.msgToast = "Mobile already exists!";
                    this.typeToast = 'info'; 
                    this.toast = true;    
                    console.log('handleAddPhone');         
                }
                else {                    
                    this.listPhone.push('+61 ' + foneSelected);            
                    this.hasPhones = true;                         
                    this.mobileSelected = '';  
                    this.reqPhone = false;  
                }            
            }
        }
    
    }
    
    handleDeletePhone(event){    
        
        let foneSelected = event.target.value;        
        
        let index = this.listPhone.indexOf(foneSelected);
        if (index !== -1){                    
            this.listPhone.splice(index, 1);
        }        
        if (this.listPhone.length === 0){
            this.hasPhones = false; 
            this.reqPhone = true;       
        }  
        else{
            this.hasPhones = true;  
            this.reqPhone = false;               
        }      
    }  

    handleDeleteEmail(event){
        let emailSelected = event.target.value;

        let index = this.listEmail.indexOf(emailSelected);
        if (index !== -1){                      
            this.listEmail.splice(index, 1);
        }    
        if (this.listEmail.length === 0){
            this.hasEmails = false;
            this.reqEmail = true; 
        }
        else {
            this.hasEmails = true; 
            this.reqEmail = false; 
        }    
    }

    onValidation() {
    
        this.isValid = false;

        const allValidLI = [...this.template.querySelectorAll('lightning-input')]
        .reduce((validSoFar, inputCmp) => {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);        
 
        const allValidLCB = [...this.template.querySelectorAll('lightning-radio-group')]
        .reduce((validSoFar, inputCmp) => {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);

        const filterItems2 = (l) => {
            return this.crsNationality.filter(x => x === l);
        }
        const natValid = filterItems2(this.nationalitySelected);         
        if (natValid.length > 0)
        {
            this.nationalitySelected = natValid[0];
        }     
        else{                  
            this.isValid = false;                
            return;
        }  

        const filterItems1 = (l) => {
            return this.crsTypeVisa.filter(x => x === l);
        }
        const tpVisa = filterItems1(this.typeVisaSelected);         
        if (tpVisa.length > 0)
        {
            this.typeVisaSelected = tpVisa[0];
        }     
        else{                        
            this.isValid = false;                
            return;
        }              

        if (this.isnewstudent){
            const filterItems3 = (l) => {
                return this.crsChannel.filter(x => x === l);
            }
            const chan = filterItems3(this.channelSelected);         
            if (chan.length > 0)
            {
                this.channelSelected = chan[0];
            }     
            else{                 
                this.isValid = false;                
                return;
            }              
        
            if (!this.disLsSpecific){            
                const filterItems4 = (l) => {
                    return this.crsLsSpecific.filter(x => x.value === l);
                }
                const spec = filterItems4(this.lsSpecificSelected);         
                if (spec.length > 0)
                {
                    this.lsSpecificSelected = spec[0].value;
                }     
                else{                 
                    this.isValid = false;                
                    return;
                }
            }  
        }
        // //mobile
        // let validPhone = true;
        // if (this.listPhone !== undefined) {
        //     if ((this.listPhone.length === 0) && 
        //         ((this.mobileSelected === undefined) || (this.mobileSelected === null))){           
        //             validPhone = false;                   
        //     }          
        //     if ((this.listPhone.length === 0) && 
        //         ((this.mobileSelected !== undefined) && (this.mobileSelected !== null))){      
        //             if(this.mobileSelected.trim().length === 0){
        //                 validPhone = false;                        
        //             }                
        //     }              
        // }else{
        //     if ((this.mobileSelected === undefined) || (this.mobileSelected === null)){           
        //         validPhone = false;  
        //     }     
        //     else{
        //         if ((this.mobileSelected !== undefined) && (this.mobileSelected !== null)){
        //             if(this.mobileSelected.trim().length === 0){
        //                 validPhone = false;                        
        //             } 
        //         }
        //     }         
        // }     
        
        // if (!validPhone){
        //     this.reqPhone = true;
        //     return;
        // }

        //email
        let validEmail = true;
        if (this.listEmail !== undefined) {
            if ((this.listEmail.length === 0) && 
                ((this.emailSelected === undefined) || (this.emailSelected === null))){           
                validEmail = false;                
            }          
            if ((this.listEmail.length === 0) && 
                ((this.emailSelected !== undefined) && (this.emailSelected !== null))){      
                    if(this.emailSelected.trim().length === 0){
                        validEmail = false;                        
                    }                
            }              
        } else{
            if ((this.emailSelected === undefined) || (this.emailSelected === null)){           
                validEmail = false;  
            }     
            else{
                if ((this.emailSelected !== undefined) && (this.emailSelected !== null)){
                    if(this.emailSelected.trim().length === 0){
                        validEmail = false;                        
                    } 
                }
            }         
        }          
        
        if (!validEmail){
            this.reqEmail = true;
            return;
        }

        //facebook
        let validFacebook = true;
        if(this.isFacebookChecked === false){           
            if ((this.facebookSelected === undefined) || (this.facebookSelected === null)){
                validFacebook = false;                
            }
            else{
                if(this.facebookSelected.trim().length === 0){
                    validFacebook = false;
                }                
            }
        }
        if (!validFacebook){
            this.reqFacebook= true;
            return;
        }

        //instagram
        let validInstagram = true;
        if(this.isInstagramChecked === false){
            if ((this.instagramSelected === undefined) || (this.instagramSelected === null)){
                validInstagram = false;
            }else{
                if(this.instagramSelected.trim().length === 0){
                    validInstagram = false;
                }                
            }
        }
        
        if (!validInstagram){
            this.reqInstagram = true;
            return;
        }

        // //WhatsApp
        // let validWhatsApp = true;
        // if (this.isDiffChecked){
        //     if ((this.whatsAppSelected === undefined) || (this.whatsAppSelected === null)){
        //         validWhatsApp = false;
        //     }else{                
        //         if(this.whatsAppSelected.trim().length === 0){                    
        //             validWhatsApp = false;
        //         }              
        //     }
        // } 

        // if (!validWhatsApp){
        //     this.reqWhatsApp = true;
        //     return;
        // }    
        
        if (!this.imgUsageChecked){
            this.reqImageUsage = true;
        }

        let birthDt = new Date(this.dtBirthSelected);
        let validbirthDt = true;
        if (birthDt > this.today){
            validbirthDt = false;
        }                
        //&& validPhone && validWhatsApp
        if (allValidLI && allValidLCB && validEmail && validFacebook && validInstagram && this.imgUsageChecked && validbirthDt) {
            this.isValid = true;    
        }
    }   

    handleClearFields() {

        this.recContactId = undefined;    
        this.contact = undefined; 
        this.isValid = false;
        this.error ='';       
        this.password='';
        this.firstNameSelected ='';
        this.lastNameSelected ='';
        this.nationalitySelected ='';
        this.dtBirthSelected ='';   
        this.emailSelected = '';
        this.auxMob =''; 
        this.auxEma = '';
        this.facebookSelected ='';
        this.instagramSelected ='';
        this.mobileSelected ='';    
        this.isDiffChecked = false;
        this.isFacebookChecked = false;
        this.disFacebook = false;
        this.reqFacebook = false;
        this.reqInstagram = false;
        this.reqImageUsage = false;
        this.reqEmail = false;
        this.reqPhone = false;
        this.reqlsSpecific = false;
        this.reqWhatsApp = false;
        this.disInstagram = false;
        this.isInstagramChecked = false;
        this.whatsAppSelected = '';
        this.typeVisaSelected= '';
        this.dtVisaExpirySelected= '';
        this.optPreferenceSelected= '';
        this.optConBySelected= '';   
        this.schoolSelected= '';
        this.courseSelected= '';
        this.channelSelected= '';
        this.lsSpecificSelected= '';
        this.disLsSpecific = true;
        this.imgUsageChecked = false;    
        this.hasEmails = false;
        this.hasPhones = false;
        this.listEmail = [];
        this.listPhone = [];
        this.formRec = [];
        this.handleClose();
    }

    handleExit(){   
        this.isNat = false; 
        this.isVisa = false; 
        this.isChannel = false;  
        this.islsSpecific = false;           
        Array.from(
            this.template.querySelectorAll('lightning-input')
        ).map(element => {
                return element.blur();
        });
                   
    }

    handleQueue(){
        this.handleClose();
        const password = this.password;
        verifyPassword({password}).then(res => 
        { 
            console.log(res);
            if (res === 'success'){
                this.isFinish = false;
                this.isInicial = false;
                this.isTime = false;   
                this.viewQueue = true; 
                window.setTimeout(() => {
                    //call method refresh on child (c-front-desk-queue)
                    this.template.querySelector('c-front-desk-queue').refresh();                                 
                }, 2000);                               
            }
            else
            {
                if (res === 'error'){
                    this.msgToast = "Please try again!";
                    this.typeToast = 'error'; 
                    this.toast = true;                      
                }
                else {   
                    this.msgToast = res;
                    this.typeToast = 'info'; 
                    this.toast = true;                                    
                }
            }        
        });       
    }     

    onSuccess(){
        
        this.isFinish = true;
        this.isInicial = false;   
        this.viewQueue = false;
        this.isNavigation = false;
        this.isTime = false;
        this.titleForm = "Well Done!";
        this.titleIcon ="standard:task2";
        window.setTimeout(() => {
            //console.log("time");
            this.isTime = true;
        }, 10000);
        
    }

    handleCancelForm(){        
        this.bShowModal = true;         
    }

    confirmModal(){
        this.bShowModal = false;
        this.isCanceled = true; 
        this.studentName = this.firstNameSelected + ' ' + this.lastNameSelected;
        this.handleClearFields();
        this.onSuccess();
        this.titleIcon = "standard:default";
        this.titleForm = "Cancelled!";
    }

    closeModal(){
        this.bShowModal = false;
    }

    navigateToWebPage(){    
        if (this.contact !== undefined){ 
            if ((this.contact.Email !== undefined) && ((this.contact.Email !== null))){
                if(this.contact.Email.toLowerCase().indexOf('null') === -1)
                {
                    this.emailNav = this.contact.Email;
                }                
            }
        }               
        // let url = 'https://devfull-educationhify.cs57.force.com/ip/apex/visitsmobile?email='+ this.emailNav;
        let url = 'visitsmobile?email='+ this.emailNav;
        
        window.open(url,'_self');                
    }

    handleSubmit() {
        this.handleClose();
        this.onValidation();        
        if (this.isValid) {
            this.isSubmited = true;
            //Contact          
            const fieldsCon = {};
            fieldsCon[FIRSTNAME_FIELD.fieldApiName] = this.firstNameSelected;
            fieldsCon[LASTNAME_FIELD.fieldApiName] = this.lastNameSelected;
            fieldsCon[NATIONALITY_FIELD.fieldApiName] =this.nationalitySelected;             
            fieldsCon[BIRTHDATE_FIELD.fieldApiName] = this.dtBirthSelected;       
            fieldsCon[CONTACTPREFERENCE_FIELD.fieldApiName] = this.optPreferenceSelected;
            fieldsCon[CONTACTBY_FIELD.fieldApiName] =  this.optConBySelected;
            if (this.contact === undefined){ 
                fieldsCon[SCHOOL_FIELD.fieldApiName] = this.schoolSelected;
                fieldsCon[COURSE_FIELD.fieldApiName] = this.courseSelected;
                fieldsCon[LEADSOURCE_FIELD.fieldApiName] =  this.channelSelected;
                fieldsCon[LEADSOURCESPECIFIC_FIELD.fieldApiName] =  this.lsSpecificSelected;
            }
            fieldsCon[AGREEMENTIMAGE_FIELD.fieldApiName] = this.imgUsageChecked;            
            
            //Forms_of_Contact__c                
            this.formRec = [];

            //console.log('-- Email --');
            // let first = 0;
            // if ((this.emailSelected !== undefined) && (this.emailSelected !== null)){
            //     if(this.emailSelected.trim().length > 0){ 
            //         fieldsCon[EMAIL_FIELD.fieldApiName] = this.emailSelected; 
            //         let it;
            //         if (this.contact === undefined){ 
            //             it = { 'Type__c':'Email', 'Detail__c': this.emailSelected, 'Contact__c':'NewContact' };                                                        
            //         } else{
            //             it = { 'Type__c':'Email', 'Detail__c': this.emailSelected};                              
            //         } 
            //         this.formRec.push(it);                       
            //         first = 1;
            //     }
            // }

            // if ((this.listEmail !== undefined) && (this.listEmail.length > 0)){ 
            //     for(let i=0;i < this.listEmail.length;i++)
            //     {        
            //         let auxEma = String(this.listEmail[i]);
            //         if (first === 0){
            //             if (i === 0){
            //                 fieldsCon[EMAIL_FIELD.fieldApiName] = auxEma;
            //             }
            //         }    
            //         let it;
            //         if (this.contact === undefined){ 
            //             it = { 'Type__c':'Email', 'Detail__c': auxEma, 'Contact__c':'NewContact' };                                                        
            //         } else{
            //             it = { 'Type__c':'Email', 'Detail__c': auxEma};                              
            //         } 
            //         this.formRec.push(it);                                               
            //     }
            // }


            let first = 0;
            if ((this.emailSelected !== undefined) && (this.emailSelected !== null)){
                if(this.emailSelected.trim().length > 0){ 
                    fieldsCon[EMAIL_FIELD.fieldApiName] = this.emailSelected;   
                    this.emailNav = this.emailSelected;             
                    first = 1;
                }
            }

            if ((this.listEmail !== undefined) && (this.listEmail.length > 0)){ 
                for(let i=0;i < this.listEmail.length;i++)
                {        
                    let auxEma = String(this.listEmail[i]);
                    if (first === 0){
                        if (i === 0){
                            fieldsCon[EMAIL_FIELD.fieldApiName] = (this.emailSelected.trim().length > 0) ? this.emailSelected : auxEma; 
                            this.emailNav = (this.emailSelected.trim().length > 0) ? this.emailSelected : auxEma;                             
                        }else{
                            let it;
                            if (this.contact === undefined){ 
                                it = { 'Type__c':'Secondary e-mail', 'Detail__c': auxEma, 'Contact__c':'NewContact' };                                                        
                            } else{
                                it = { 'Type__c':'Secondary e-mail', 'Detail__c': auxEma};                              
                            } 
                            this.formRec.push(it);                           
                        }
                    }
                    else
                    {
                        if ((this.emailSelected !== auxEma)){
                            let it;
                            if (this.contact === undefined){ 
                                it = { 'Type__c':'Secondary e-mail', 'Detail__c': auxEma, 'Contact__c':'NewContact' };                                                        
                            } else{
                                it = { 'Type__c':'Secondary e-mail', 'Detail__c': auxEma};                              
                            } 
                            this.formRec.push(it);                             
                        }
                    }
                }
            }
           
            //console.log('-- Phone --');   
            if ((this.mobileSelected !== undefined) && (this.mobileSelected !== null)){
                if(this.mobileSelected.trim().length > 0){                     
                    let it; 
                    if (this.contact === undefined){ 
                        it = { 'Type__c':'Mobile', 'Detail__c': '+61 ' + this.mobileSelected, 'Contact__c':'NewContact', 'Country__c':'Australia' };                                                        
                    } else{
                        it = { 'Type__c':'Mobile', 'Detail__c': '+61 ' + this.mobileSelected, 'Country__c':'Australia'};                              
                    } 
                    this.formRec.push(it);                                        
                }
            }

            if ((this.listPhone !== undefined) && (this.listPhone.length > 0)){ 
                for(let i=0;i < this.listPhone.length;i++)
                {        
                    let auxMob = String(this.listPhone[i]);
                    if ('+61 ' + this.mobileSelected !== auxMob){
                        let it;
                        if (this.contact === undefined){ 
                            it = { 'Type__c':'Mobile', 'Detail__c': auxMob, 'Contact__c':'NewContact', 'Country__c':'Australia' };                                                        
                        } else{
                            it = { 'Type__c':'Mobile', 'Detail__c': auxMob, 'Country__c':'Australia'};                              
                        } 
                        this.formRec.push(it);              
                    }                             
                }
            }
//----------------------------------------------------------------------------------------------------------------------------
            // let firstP = 0;
            // if ((this.mobileSelected !== undefined) && (this.mobileSelected !== null)){
            //     if(this.mobileSelected.trim().length > 0){ 
            //         fieldsCon[MOBILEPHONE_FIELD.fieldApiName] = '+61' + this.mobileSelected;                    
            //         firstP = 1;
            //     }
            // }

            // if ((this.listPhone !== undefined) && (this.listPhone.length > 0)){ 
            //     for(let i=0;i < this.listPhone.length;i++)
            //     {        
            //         let auxMob = String(this.listPhone[i]);
            //         if (firstP === 0){
            //             if (i === 0){
            //                 fieldsCon[MOBILEPHONE_FIELD.fieldApiName] = (this.mobileSelected.trim().length > 0) ? this.mobileSelected : auxMob;
                            
            //             }else{
            //         let it;
            //         if (this.contact === undefined){ 
            //             it = { 'Type__c':'Mobile', 'Detail__c': auxMob, 'Contact__c':'NewContact', 'Country__c':'BillingCountry' };                                                        
            //         } else{
            //             it = { 'Type__c':'Mobile', 'Detail__c': auxMob, 'Country__c':'BillingCountry'};                              
            //         } 
            //         this.formRec.push(it);
            //                 console.log('1.FoPhone -> ' + JSON.stringify(it));  
            //             }
            //         }
            //         else
            //         {
            //             if ((this.mobileSelected !== auxMob)){
            //                 let it;
            //                 if (this.contact === undefined){ 
            //                     it = { 'Type__c':'Mobile', 'Detail__c': auxMob, 'Contact__c':'NewContact', 'Country__c':'BillingCountry' };                                                        
            //                 } else{
            //                     it = { 'Type__c':'Mobile', 'Detail__c': auxMob, 'Country__c':'BillingCountry'};                              
            //                 } 
            //                 this.formRec.push(it);
                            
            //             }
            //         }
            //     }
            // }

            const objCon = { apiName: CONTACT_OBJECT.objectApiName, fieldsCon };
            const objContact = JSON.stringify(objCon);

            if (!this.isInstagramChecked) {              
                 
                if (this.contact === undefined){   
                   const item = {'Type__c':'Instagram', 'Detail__c': this.instagramSelected, 'Contact__c':'NewContact'};
                   this.formRec.push(item); 
                }
                else{
                    const item = {'Type__c':'Instagram', 'Detail__c': this.instagramSelected}; 
                    this.formRec.push(item); 
                }                                
            }

            if (!this.isFacebookChecked) {
                if (this.contact === undefined){  
                    const item = {'Type__c':'Facebook', 'Detail__c': this.facebookSelected, 'Contact__c':'NewContact'};
                    this.formRec.push(item);   
                }else{
                    const item = {'Type__c':'Facebook', 'Detail__c': this.facebookSelected};
                    this.formRec.push(item);                  
                }
            }

            if (this.isDiffChecked) {
                if (this.contact === undefined){
                    const items = {'Type__c':'WhatsApp', 'Detail__c': this.whatsAppSelected, 'Contact__c':'NewContact'};
                    this.formRec.push(items);      
                }
                else{
                    const items = {'Type__c':'WhatsApp', 'Detail__c': this.whatsAppSelected};
                    this.formRec.push(items);  
                }       
            }
            
            let objForm = null;
            if ((this.formRec !== undefined) && (this.formRec.length > 0)){
                //const objfieldsForm = JSON.stringify(this.formRec);
                //objForm = { apiName: FORMCONTACT_OBJECT.objectApiName, objfieldsForm };
                objForm = JSON.stringify(this.formRec);              
            }
            //console.log('objForm ->>' + objForm);  
            //Client_Document__c
            const fieldsCli = {};
            fieldsCli[VISATYPE_FIELD.fieldApiName] =  this.typeVisaSelected;   
            //console.log('typeVisaSelected ' + this.typeVisaSelected);

            fieldsCli[EXPIRYDATEVISA_FIELD.fieldApiName] = this.dtVisaExpirySelected;

            const objConClient = { apiName: CLIENTDOCUMENT_OBJECT.objectApiName, fieldsCli };
            const objClient = JSON.stringify(objConClient);           
            this.isCanceled = false;
            if (this.contact !== undefined){           
                //UPDATE JSON on Form_UpdatedContact__c 
                let objJson = objContact + ";" + objClient;
                if (objForm !== null){
                    objJson = objJson + ";" + objForm;                   
                }      
                console.log('objJson ->> ' + objJson);                                          
                const contactId = this.contact.Id;
                updateContactFromDeskForm({contactId, objJson}).then(result => {
                    this.isSubmited = false;
                    if(result === 'success'){      
                        console.log('success');                                                 
                        //this.onSuccess();
                        this.navigateToWebPage();
                        this.handleClearFields();
                    }else{
                        console.log('Error: ' + result);
                        this.msgToast = result;
                        this.typeToast = 'error'; 
                        this.toast = true;                         
                    }            
                }).catch(error => {                                               
                    this.msgToast = "Error updating record!";
                    this.typeToast = 'error'; 
                    this.toast = true;                       
                    console.log('Error: ' + error);      
                    this.error = error;
                });  
                
            } else {
                //INSERT                                
                console.log('objClient ->>' + objClient);
                console.log('objContact ->>' + objContact);
                console.log('objForm ->>' + objForm);
                createContactFromDeskForm({objContact, objForm, objClient}).then(result => {
                    this.isSubmited = false;
                    if(result === 'success'){
                        console.log('success');                        
                        //this.onSuccess();                        
                        this.navigateToWebPage();
                        this.handleClearFields();
                    }else{    
                        console.log('ErrorRe: ' + result);
                        this.error = result;                                              
                        this.msgToast = this.error;
                        this.typeToast = 'error'; 
                        this.toast = true;                                                                               
                    }            
                }).catch(error => {                                
                    this.msgToast = "Error creating record!";
                    this.typeToast = 'error'; 
                    this.toast = true;                       
                    console.log('Error: ' + error);      
                    this.error = error;
                });                 
            }            
        }     
        else
        {
            this.msgToast = "Please fill up all the mandatory fields.";
            this.typeToast = 'info'; 
            this.toast = true;           
        }   
    }
}