/* eslint-disable no-console */
import { LightningElement, api, track } from 'lwc';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import chartjs from '@salesforce/resourceUrl/chart';

var randomColorGenerator = function () { 
    return '#' + (Math.random().toString(16) + '0000000').slice(2, 8); 
};

const columns = [
    { label: 'Subject', fieldName: 'label' },    
    { label: 'Total', fieldName: 'value' , resizeColumnDisabled:"true", fixedWidth:90}
];

export default class LwcTotalTasksRep extends LightningElement {
    @api crDataS;    
    @api crDataN;
    @api crDataP;
    @api titleS;    
    @api titleN;
    @api titleP;
    vChartN;
    vChartP;
    initialized = false;            
    @track columns = columns;  
    @track dataSubject = [];
    @track dataNext = [];
    @track dataPriority = [];
    @track showNoResultS = false;
    @track showNoResultP = false;
    @track showNoResultT = false;    

   
    @api
    refresh() {                   
        console.log('refresh');          
        this.initialized = false; 
        this.renderedCallback();         
    }
    
    renderedCallback() { 
        //console.log('renderedCallback');       
        if (this.initialized) {
            return;
        }                       
        
        this.initialized = true;
        this.handleSubject();  
        this.handleNext7();        
        this.handlePriority();                      
    }

    handleNext7(){

        let colors = [];
        this.dataNext = {};
        let nm = 0;
        if (this.crDataN !== undefined){      
            this.dataNext = JSON.parse(JSON.stringify(this.crDataN));   
            if (this.crDataN.qtd !== undefined){                           
                nm = this.dataNext.qtd.length;        
                for (let index = 0; index < nm; index++) {
                    colors.push(randomColorGenerator());                                       
                }
                if (nm > 0){ 
                    let config = {
                        type: 'doughnut',                      
                        data: {
                            labels: this.dataNext.label, 
                            tooltips: {                    
                                enabled: true
                            }, 
                            datasets: [
                                {                    
                                    data: this.dataNext.qtd,                                         
                                    backgroundColor: colors,
                                    borderAlign: 'inner'                
                                }                     
                            ]           
                        },
                        options: { 
        
                            responsive: true,                              
                            legend: {
                                display: false
                            }                                                                
                        }
                    };       
                    
                    if (this.vChartN !== undefined){
                        this.vChartN.destroy();
                    }
        
                    loadScript(this, chartjs + '/package/dist/Chart.min.js').then(() => {
                        const canvas = this.template.querySelector('canvas.dochNext7');
                        const ctx = canvas.getContext('2d');                                                      
                        this.vChartN = new window.Chart(ctx, config);   
                        this.handleResize();         
                    }).catch(error => {
                        console.log('error ' + error);
                    });
                    loadStyle(this, chartjs + '/package/dist/Chart.min.css'); 
                }
            }
        }        
        
        if (nm === 0){        
            this.showNoResultS = true;            
        }      
    }

    handleSubject(){           
        this.dataSubject = this.crDataS; 
        if ((this.dataSubject !== undefined) && (this.dataSubject.length > 0)){
            this.showNoResultT = true;                    
        }                            
    }

    handlePriority(){
        let colors = [];

        this.dataPriority = {};
        let nm = 0;
        if (this.crDataP !== undefined){      
            this.dataPriority = JSON.parse(JSON.stringify(this.crDataP));   
            if (this.crDataP.qtd !== undefined){             
                nm = this.dataPriority.qtd.length;        
                for (let index = 0; index < nm; index++) {
                    colors.push(randomColorGenerator());                                       
                }

                if (nm > 0){
                    let config = {
                        type: 'pie',            
                        data: {
                            labels: this.dataPriority.label, 
                            datasets: [
                                {                    
                                    data: this.dataPriority.qtd,                                         
                                    backgroundColor: colors,
                                    borderAlign: 'inner'                 
                                }                     
                            ]           
                        },
                        options: {   
                            responsive: true,                             
                            legend: {
                                display: false
                            }                                               
                        }
                    };         
                    
                    if (this.vChartP !== undefined){
                        this.vChartP.destroy();
                    }
        
                    loadScript(this, chartjs + '/package/dist/Chart.min.js').then(() => {
                        const canvas = this.template.querySelector('canvas.dochPriority');
                        const ctx = canvas.getContext('2d');                                                      
                        this.vChartP = new window.Chart(ctx, config); 
                        this.handleResize();                        
                    }).catch(error => {
                        console.log('error ' + error);
                    });
                    loadStyle(this, chartjs + '/package/dist/Chart.min.css');
                }
            }
        }
        
        if (nm === 0){        
            this.showNoResultP = true;
        }       
    }

    handleResize(){
        //console.log('handleResize ');
        const canvas = this.template.querySelector('canvas.dochPriority');
        const c = this.template.querySelector('div.divT');
        if(c != null){
            c.style.height = canvas.height + 'px';
        }
    }
}