/* eslint-disable no-console */
import { LightningElement, track, wire, api } from 'lwc';
import { refreshApex } from '@salesforce/apex';

import getPosFrontDeskQueue from '@salesforce/apex/lwcMergeForm.getPosFrontDeskQueue';
import getContactValuestoMerge from '@salesforce/apex/lwcMergeForm.getContactValuestoMerge';
import updateMergeQueue from '@salesforce/apex/lwcMergeForm.updateMergeQueue';
import updateFromMergeAllDisabled from '@salesforce/apex/lwcMergeForm.updateFromMergeAllDisabled';
import getVisaType from '@salesforce/apex/lwcFrontDesk.getVisaType';
import getNationality from '@salesforce/apex/lwcFrontDesk.getNationality';
import getChannel from '@salesforce/apex/lwcFrontDesk.getChannel';
import getLeadSourceSpecific from '@salesforce/apex/lwcFrontDesk.getLeadSourceSpecific';

const columns = [
    { fieldName: 'Id', fixedWidth:1, resizeColumnDisabled:"true"},
    { label: 'Name', fieldName: 'Name',  type: 'text', cellAttributes: { class: { fieldName: 'NameCSSClass' }}}
]

export default class FrontDeskMerge extends LightningElement {
    @api agency;
    @track preSelectedRows= [];
    @track isToMerge = false;  
    @track isValid = true;   
    @track contactColumns = columns;
    @track lstContact = [];   
    @track lstForm = [];   
    @track lstDocument = [];   
    @track wiredNew = [];  
    @track error = '';
    @track contactSelected = '';    
    @track ltsDisabled = [];
    @track allDisabled = true;    
    @track crsTypeVisa;
    @track crsTypeVisaAux;  
    @track isVisa = false;
    @track typeVisaSelected= '';
    @track crsNationality;
    @track crsNationalityAux; 
    @track nationalitySelected ='';
    @track isNat = false;
    @track toast = false;    
    @track msgToast = '';
    @track typeToast = '';    
    
    @track crsLeadSpecific;
    @track crsLeadSpecificAux;
    @track isLeadSpecific = false;
    @track leadSpecificSelected ='';

    @track crsLead;
    @track crsLeadAux;
    @track isLead = false;
    @track leadSelected ='';

    @wire(getPosFrontDeskQueue, {agencyId: "$agency"})
    wiredPosQueue;
     
    @wire(getNationality)
    NationalityPicklistValues ({error, data}) {        
        if (data) {            
            this.crsNationality = data;
            this.crsNationalityAux = data;           
            this.isNat = false;               
        } else {
            this.crsNationality = undefined;
            this.crsNationalityAux = undefined;  
            this.error = error;             
        }       
    } 

    @wire(getVisaType)
    TypeVisaPicklistValues ({error, data}) {        
        if (data) {            
            this.crsTypeVisa = data;                  
            this.crsTypeVisaAux = data;           
            this.isVisa = false;               
        } else {
            this.crsTypeVisa = undefined;
            this.crsTypeVisaAux = undefined;  
            this.error = error;             
        }       
    } 
    
    @wire(getChannel)
    LeadPicklistValues ({error, data}) {        
        if (data) {            
            this.crsLead = data;                  
            this.crsLeadAux = data;              
            this.isLead = false;          
        } else {
            this.crsLead = undefined;
            this.crsLeadAux = undefined;  
            this.error = error;                         
        }             
    }   

    getLeadSourceSpecification(){       
        
        this.leadSpecificSelected = '';  
        this.crsLeadSpecific = [];
        this.crsLeadSpecificAux = []; 
        //console.log('getLeadSourceSpecification ' + this.leadSelected);             
        getLeadSourceSpecific({ leadSource :this.leadSelected}).then(result => {
            //console.log('result ' +  JSON.stringify(result));
            if (result) {
                this.crsLeadSpecific = result;
                this.crsLeadSpecificAux = result;  
                if (this.crsLsSpecific.length > 0){                   
                    this.isLeadSpecific = true;                        
                }            
            }                                
        })
        .catch(error => {                  
            this.error = error;            
        });                       
     }

    handleRefresh(){               
        this.handleClear();
        return refreshApex(this.wiredPosQueue);  
    }

    handleClear(){
        this.preSelectedRows= [];
        this.isToMerge = false;                 
        this.wiredNew = [];         
        this.lstContact = []; 
        this.lstForm = []; 
        this.lstDocument = [];    
        this.contactSelected = ''; 
        this.typeVisaSelected= '';        
        this.nationalitySelected =''; 
        this.leadSelected ='';            
        this.leadSpecificSelected ='';        
        this.isVisa = false;                      
    }
    
    handleLeadChange(event){
        this.leadSelected = event.target.value;                 
        this.isLead = false;
        if (this.leadSelected.length > 0){
            this.crsLead = this.crsLeadAux;      
        
            const filterItems = (l) => {
                return this.crsLead.filter(x => x.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }
            this.isLead = true;  
            this.crsLead = filterItems(this.leadSelected);            
        } 
    }
    
    handleLeadClicked(event){
        this.leadSelected = event.target.dataset.item;
        this.isLead = false;
        this.getLeadSourceSpecification();
    }
        
    handleLeadSpecificChange(event){
        this.leadSpecificSelected = event.target.value;                 
        this.isLeadSpecific = false;
        if (this.leadSpecificSelected.length > 0){
            this.crsLeadSpecific = this.crsLeadSpecificAux;      
        
            const filterItems = (l) => {
                return this.crsLeadSpecific.filter(x => x.value.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }
            this.isLeadSpecific = true;  
            this.crsLeadSpecific = filterItems(this.leadSpecificSelected);  
            this.getLeadSourceSpecification();          
        } 
    }
    
    handleLeadSpecificClicked(event){
        this.leadSpecificSelected = event.target.dataset.item;
        this.isLeadSpecific = false;
    }

    handleVisaTypeChange(event){   
        this.typeVisaSelected = event.target.value;                 
        this.isVisa = false;
        if (this.typeVisaSelected.length > 0){
            this.crsTypeVisa = this.crsTypeVisaAux;      
        
            const filterItems = (l) => {
                return this.crsTypeVisa.filter(x => x.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }
            this.isVisa = true;  
            this.crsTypeVisa = filterItems(this.typeVisaSelected);            
        }      
    }
    
    handleExit(){   
        this.isNat = false; 
        this.isVisa = false; 
        this.isLead = false;  
        this.isLeadSpecific = false;           
        Array.from(
            this.template.querySelectorAll('lightning-input')
        ).map(element => {
                return element.blur();
        });
                   
    }

    handleVisaTypeClicked(event){
        this.typeVisaSelected = event.target.dataset.item;
        this.isVisa = false;
    }

    handleNationalityChange(event){           
        this.nationalitySelected = event.target.value;       
        this.isNat = false;
        if (this.nationalitySelected.length > 0){
            this.crsNationality = this.crsNationalityAux;      
        
            const filterItems = (l) => {
                return this.crsNationality.filter(x => x.toLowerCase().indexOf(l.toLowerCase()) > -1);
            }
            this.isNat = true;  
            this.crsNationality = filterItems(this.nationalitySelected);
        }      
    }
    
    handleTypeVisaFocus(){
        this.crsTypeVisa = this.crsTypeVisaAux;    
        this.isVisa = true;        
    }

    handleNationalityFocus(){       
        this.crsNationality = this.crsNationalityAux;        
        this.isNat = true;        
    }

    handleNationalityClicked(event){
        this.nationalitySelected = event.target.dataset.item;
        this.isNat = false;        
    }

    handlelsSpecificFocus(){
        this.crsLeadSpecific = this.crsLeadSpecificAux;        
        this.isLeadSpecific = true;        
    }  

    handleChannelFocus(){
        this.crsLead = this.crsLeadAux;
        this.isLead = true;        
    }
  
    handleRowSelection(event){

        this.handleClear();        
        const selectedRows = event.detail.selectedRows;    
        console.log(selectedRows.length + ' handleRowSelection ' + JSON.stringify(selectedRows));       
        if (selectedRows.length > 0)
        {
            this.contactSelected = selectedRows[0].Id;                
            getContactValuestoMerge({ contactId :this.contactSelected}).then(result => {
                if (result) {                                                                    
                    this.wiredNew = result.values;                           
                    const filterDisables = () => {
                        return this.wiredNew.filter(x => x.fieldDisable === false);
                    }                                        
                    this.ltsDisabled = filterDisables();
                    this.allDisabled = (this.ltsDisabled.length === 0) ? true : false;                    
                    this.isToMerge = true;                            

                    const filterVisaType = () => {
                        return this.wiredNew.filter(x => x.picklistVisa === true);
                    }                                        
                    let retFilter = filterVisaType();                      
                    if ((retFilter != null) && (retFilter.length > 0))
                    {
                        this.typeVisaSelected = retFilter[0].jsonValue;                    
                    }

                    const filterNationality = () => {
                        return this.wiredNew.filter(x => x.picklistNati === true);
                    }                                        
                    let retNFilter = filterNationality();                      
                    if ((retNFilter != null) && (retNFilter.length > 0))
                    {
                        this.nationalitySelected = retNFilter[0].jsonValue;                    
                    }

                    const filterChannel = () => {
                        return this.wiredNew.filter(x => x.picklistLead === true);
                    }                                        
                    let retLFilter = filterChannel();
                    if ((retLFilter != null) && (retLFilter.length > 0))
                    {
                        this.leadSelected = retLFilter[0].jsonValue;
                        this.getLeadSourceSpecification();
                    }

                    const filterSpec = () => {
                        return this.wiredNew.filter(x => x.picklistLeadSpec === true);
                    }                                        
                    let retSFilter = filterSpec();
                    if ((retSFilter != null) && (retSFilter.length > 0))
                    {
                        this.leadSpecificSelected = retSFilter[0].jsonValue;
                    }

                } else
                {
                    this.contactSelected = '';  
                    this.handleRefresh();
                    console.log('handleRowSelectionC ' + this.contactSelected);              
                }                                
            })
            .catch(error => {            
                this.contactSelected ='';                        
                this.error = error;
                this.handleRefresh();
                console.log('handleRowSelectionE ' +  JSON.stringify(this.error)); 
            }); 
        }        
    }    

    handleClose(){
        //console.log('handleClose'); 
        this.toast = false;
        this.msgToast ='';
        this.typeToast ='';
    }

    onValidation(elObject, elName, elValue){
        this.handleClose(); 
        console.log(elObject + ' elObject - elValue ' + elValue + " onnValidation "  + elName);
        if ((elValue !== null) && (elValue.trim().length > 0)){                                                
            elValue = elValue.trim();
            if ((elName === "Visa Expire Date:") || (elName === "Date of birth:"))
            {   
                let dt = Date.parse(elValue);                
                if (isNaN(dt))
                {                    
                    this.msgToast = 'Invalid Date!';
                    this.typeToast = 'info'; 
                    this.toast = true;
                    this.isValid = false;
                    console.log('data'); 
                    return;
                }                                
            }

            if (elName === "Nationality:"){
                
                const filterItems2 = (l) => {
                    return this.crsNationality.filter(x => x === l);
                }
                const natValid = filterItems2(elValue);         
                if (natValid.length > 0)
                {
                    this.nationalitySelected = natValid[0];
                }     
                else{                                              
                    this.msgToast = 'Invalid Nationality!';
                    this.typeToast = 'info'; 
                    this.toast = true;
                    this.isValid = false;       
                    console.log('Nationality');          
                    return;
                }  
            }
        
            if (elName === "Visa Type:"){
                const filterItems1 = (l) => {
                    return this.crsTypeVisa.filter(x => x === l);
                }
                const tpVisa = filterItems1(elValue);         
                if (tpVisa.length > 0)
                {
                    this.typeVisaSelected = tpVisa[0];                    
                }     
                else{            
                    this.msgToast = 'Invalid Visa Type!';
                    this.typeToast = 'info'; 
                    this.toast = true;
                    this.isValid = false;     
                    console.log('visa');              
                    return;
                }     
            }
            
            if (elName === "Channel:"){
                const filterItems3 = (l) => {
                    return this.crsLead.filter(x => x === l);
                }
                const tpChan = filterItems3(elValue);         
                if (tpChan.length > 0)
                {
                    console.log("tpChan[0W] " + tpChan[0]);
                    this.leadSelected = tpChan[0];                    
                }     
                else{                          
                    console.log('Channel');        
                    this.msgToast = 'Invalid Channel!';
                    this.typeToast = 'info'; 
                    this.toast = true;
                    this.isValid = false;                
                    return;
                }     
            }

            if (elName === "Specific Channel:"){
               // console.log(' -elValue ' + elValue + ' -this.leadSelected ' +  this.leadSelected + " -crsLeadSpecific " + JSON.stringify(this.crsLeadSpecific));
                const filterItems4 = (l) => {
                    return this.crsLeadSpecific.filter(x => x.value === l);
                }
                const tpChan = filterItems4(elValue);         
                if (tpChan.length > 0)
                {
                    console.log("tpChan[0] " + tpChan[0]);
                    this.leadSpecificSelected = tpChan[0]; 
                                      
                }     
                else{            
                    console.log("Invalid Channel Specific");
                    this.msgToast = 'Invalid Channel Specific!';
                    this.typeToast = 'info'; 
                    this.toast = true;
                    this.isValid = false; 
                    console.log('esp');                       
                    return;
                }     
            }

            if (elName === "Contact Preference:"){
                let eleValue = elValue.toLowerCase();
                if ((eleValue !== "morning") && 
                    (eleValue !== "afternoon") && 
                    (eleValue !== "evening")){
                    this.isValid = false;
                    this.msgToast = "Contact Preference accepts just 'Morning','Afternoon' OR 'Evening'";
                    this.typeToast = 'info'; 
                    this.toast = true;      
                    console.log('Preference');          
                    return;
                }                                    
            }

            if (elName === "Contact By:"){
                let eleValue = elValue.toLowerCase();
               
                if ((eleValue !== "email") &&
                    (eleValue !== "phone call") &&
                    (eleValue !== "sms") &&
                    (eleValue !== "whatsapp")){
                    this.isValid = false;                    
                    this.msgToast = "Contact Preference accepts just 'Email','Phone Call','SMS' OR 'WhatsApp'.";
                    this.typeToast = 'info'; 
                    this.toast = true;  
                    console.log('by '+ eleValue);          
                    return;
                }                                    
            }

            if (elName === "Email:"){                              
                if ((!elValue.includes('@')) || (!elValue.includes('.')))
                {
                    this.isValid = false;
                    this.msgToast = "Invalid Email!";
                    this.typeToast = 'info'; 
                    this.toast = true;    
                    console.log('email');                           
                    return;
                }                
            }
            
            if (elName === "Agreement Image:"){
                let vl = elValue.toLowerCase();                  
                if (vl !== "true"){
                    this.isValid = false;                    
                    this.msgToast = "Agreement Image accepts just 'true'!";
                    this.typeToast = 'info'; 
                    this.toast = true; 
                    console.log('Agreement');
                    return;
                }   
            }

            if ((elName === "Mobile:") || (elName === "WhatsApp:")){                
                let phoneValue = Number(parseFloat(elValue));                
                if (isNaN(phoneValue)){
                    this.isValid = false;                    
                    this.msgToast = "Mobile OR WhatsApp accept just Numbers!";
                    this.typeToast = 'info'; 
                    this.toast = true; 
                    console.log('Mobile');
                    return;
                } 
                if (elValue.length > 40) {
                    this.isValid = false;
                    this.msgToast = "Mobile OR WhatsApp accept just less or equal 40 positions!";
                    this.typeToast = 'info'; 
                    this.toast = true;       
                    console.log('Mobile1');             
                    return;
                }                           
            } 
                  
            if (this.isValid === true){
                if (elObject === "Contact"){
                    this.lstContact.push(elName + elValue);
                }else{
                    if (elObject === "Client_Document__c"){
                        this.lstDocument.push(elName + elValue);
                    }else
                    {
                        if (elObject === "Forms_of_Contact__c"){
                            this.lstForm.push(elName + elValue);
                        }
                    }
                }                                                    
            }            
        }
        else
        {
            this.isValid = false;
            this.msgToast = "Please complete selected fields to update!";
            this.typeToast = 'info'; 
            this.toast = true;   
        }
    }

    handleSubmit(){
        console.log('handleSubmit'); 
        this.handleClose(); 
        const contactId = this.contactSelected;
        if (this.allDisabled === true){            
            updateFromMergeAllDisabled({contactId}).then(result => {
                if(result === 'success'){  
                    console.log('SuccessAllDisabled');                                         
                    this.handleRefresh();
                }else{
                    console.log('Error: ' + result);                  
                    this.msgToast = "Error update Contact record!";
                    this.typeToast = 'error'; 
                    this.toast = true; 
                }            
            })
        }        
        else{
            this.lstContact = [];   
            this.lstForm = [];   
            this.lstDocument = []; 
            this.isValid = true;
            this.template.querySelectorAll('.chkLine').forEach(chkElement => {                     
                if (chkElement.checked === true){                                      
                    this.template.querySelectorAll('.iptLine').forEach(element => {                        
                        if (chkElement.value === element.name) {  
                            let elName = chkElement.name.trim();
                            let elValue = element.value;                            
                            let elObject = chkElement.dataset.item.trim(); 
                            this.onValidation(elObject, elName, elValue);                                                                                  
                        }
                    });
                }
            });                
                   
            console.log('isValid ' + this.isValid);
            if(this.isValid === true)
            {
                if ((this.lstContact.length > 0) || (this.lstForm.length > 0) || (this.lstDocument.length > 0) && (this.contactSelected !== '')) {
                    console.log('lstContact: ' + JSON.stringify(this.lstContact));
                    console.log('lstForm: ' + JSON.stringify(this.lstForm)); 
                    console.log('lstDocument: ' + JSON.stringify(this.lstDocument)); 
                    const fieldsContact = JSON.stringify(this.lstContact);
                    const fieldsForm = JSON.stringify(this.lstForm); 
                    const fieldsDocument = JSON.stringify(this.lstDocument);
                    updateMergeQueue({ contactId, fieldsContact, fieldsForm, fieldsDocument }).then(() => {
                        console.log('Success');                        
                        this.handleRefresh();                                              
                    })
                    .catch(error => {
                        this.error = error;
                        console.log('error: ' + this.error); 
                        this.msgToast = "Error update record!";
                        this.typeToast = 'error'; 
                        this.toast = true;
                    });                    
                }
                else{
                    this.msgToast = "Please select fields to update!";
                    this.typeToast = 'info'; 
                    this.toast = true;                  
                }
            }   
        }                
    }
}