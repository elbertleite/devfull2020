/* eslint-disable no-console */
/* eslint-disable @lwc/lwc/no-async-operation */
import { LightningElement, api, track } from 'lwc';

export default class LwcDashboardItem extends LightningElement {
    @api task;
    @api hasElement;
    @api rFilter;
    @api rApply;    
    has = false;
    @track rFilterEl1;
    @track rFilterEl2;
    @track rFilterEl3;
    @track rFilterEl4;  
    @track rFilterEl5; 
    @track rFilterEl6;
    @track rFilterEl7;
    @track rFilterEl8;
    @track rFilterEl9;
    @track rFilterEl10;
    @track rFilterEl11;
    @track rFilterEl12;
    @track rFilterEl13;
    @track rFilterEl14;
    @track rFilterEl15;
    @track rFilterEl16;
    @track rFilterEl17;
    @track rFilterEl18;
    @track rFilterEl19;
    @track rFilterEl20;
    @track rFilterEl21;

    @track settingFilter1;
    @track settingFilter2;
    @track settingFilter3;
    @track settingFilter4;
    @track settingFilter5;
    @track settingFilter6;
    @track settingFilter7;
    @track settingFilter8;
    @track settingFilter9;
    @track settingFilter10;
    @track settingFilter11;
    @track settingFilter12;
    @track settingFilter13;
    @track settingFilter14;
    @track settingFilter15;
    @track settingFilter16;
    @track settingFilter17;
    @track settingFilter18;
    @track settingFilter19;
    @track settingFilter20;
    @track settingFilter21;

    @api 
    get hasElement1(){           
        let ret = false;          
        if (this.rFilter !== undefined){                         
            ret = (this.task.id === '1') ? true : false;            
            if (ret)
            {                       
                this.settingFilter1 = "New Leads Evolution (Last 3 years)"; //" + this.task.period + ")"; 
                let f1 = JSON.stringify(this.rFilter.l1);
                let f2 = JSON.stringify(this.rFilterEl1);                       
                this.rFilterEl1 = this.rFilter.l1;     
                if (this.rApply){                        
                    if (f1 !== f2){
                        console.log('apply1');
                        window.setTimeout(() => {                    
                            this.template.querySelector('c-lwc-sales-rep').refresh();                               
                        }, 1000);
                        this.itemReturnApply(); 
                    }
                }                               
            }            
        }        
        return ret;
    }

    @api 
    get hasElement2(){    
                
        let ret = false;
        if (this.rFilter !== undefined){                        
            ret = (this.task.id === '2') ? true : false;
            if (ret)
            {                                               
                this.settingFilter2 = "Pipeline Overview Australia";// (" + this.task.period + ")";                 
                let f1 = JSON.stringify(this.rFilter.l2);
                let f2 = JSON.stringify(this.rFilterEl2);                                           
                this.rFilterEl2 = this.rFilter.l2;                     
                if (this.rApply){                                                  
                    if (f1 !== f2) {
                        console.log('apply2');                            
                        window.setTimeout(() => {                    
                            this.template.querySelector('c-lwc-stage-status-total-rep').refresh();                               
                        }, 1000); 
                        this.itemReturnApply();                            
                    }
                }                                      
            }           
        }     
        
        return ret;
    }

    @api 
    get hasElement3(){        
        
        let ret = false;        
        if (this.rFilter !== undefined){
                      
            ret = (this.task.id === '3') ? true : false;
            if (ret)
            {               
                this.settingFilter3 = "Tasks (Next 7 Days)";  //n
                this.settingFilter4 = "Tasks By Priority (" + this.task.period + ")";  //p
                this.settingFilter5 = "Tasks By Subject (" + this.task.period + ")"; //s
                let f1 = JSON.stringify(this.rFilter.l3);
                let f2 = JSON.stringify(this.rFilterEl3);
                this.rFilterEl3 = this.rFilter.l3;  
                
                let f3 = JSON.stringify(this.rFilter.l4);
                let f4 = JSON.stringify(this.rFilterEl4);
                this.rFilterEl4 = this.rFilter.l4;

                let f5 = JSON.stringify(this.rFilter.l5);
                let f6 = JSON.stringify(this.rFilterEl5);
                this.rFilterEl5 = this.rFilter.l5;

                if (this.rApply){                       
                    if ((f1 !== f2) || (f3 !== f4) || (f5 !== f6)){
                        console.log('apply3');                            
                        window.setTimeout(() => {                    
                            this.template.querySelector('c-lwc-total-tasks-rep').refresh();                               
                        }, 1000); 
                        this.itemReturnApply();                            
                    }
                }
            }                            
        }       
       
        return ret;
    }

    @api 
    get hasElement4(){        
        let ret = false;   
        if (this.rFilter !== undefined){                   
            ret = (this.task.id === '4') ? true : false;
            if (ret)
            {                       
                this.settingFilter6 = "Quantity of Enrolments (" + this.task.period + ")"; 
                let f1 = JSON.stringify(this.rFilter.l6);
                let f2 = JSON.stringify(this.rFilterEl6);                       
                this.rFilterEl6 = this.rFilter.l6;     
                if (this.rApply){                        
                    if (f1 !== f2){
                        console.log('apply4');
                        window.setTimeout(() => {                    
                            this.template.querySelector('c-lwc-sales-rep').refresh();                               
                        }, 1000); 
                        this.itemReturnApply();
                    }
                }                                
            }    
        }       
        
        return ret;
    }
    
    @api 
    get hasElement5(){        
        let ret = false;   
        if (this.rFilter !== undefined){                             
            ret = (this.task.id === '5') ? true : false;
            if (ret)
            {                   
                this.settingFilter7 = "Sales Overview - First Instalments (" + this.task.period + ")"; 
                let f1 = JSON.stringify(this.rFilter.l7);
                let f2 = JSON.stringify(this.rFilterEl7);                       
                this.rFilterEl7 = this.rFilter.l7;    
                
                if (this.rApply){                        
                    if (f1 !== f2){
                        console.log('apply7');
                        window.setTimeout(() => {                    
                            this.template.querySelector('c-lwc-sales-rep').refresh();                               
                        }, 1000); 
                        this.itemReturnApply();
                    }
                }                                
            }            
        }       
        
        return ret;
    }
    
    @api 
    get hasElement6(){        
        let ret = false;   
        if (this.rFilter !== undefined){                             
            ret = (this.task.id === '6') ? true : false;
            if (ret)
            {                       
                this.settingFilter8 = "Sales Overview - First Payments (" + this.task.period + ")"; 
                let f1 = JSON.stringify(this.rFilter.l8);
                let f2 = JSON.stringify(this.rFilterEl8);                       
                this.rFilterEl8 = this.rFilter.l8;     
                if (this.rApply){                        
                    if (f1 !== f2){
                        console.log('apply8');
                        window.setTimeout(() => {                    
                            this.template.querySelector('c-lwc-sales-rep').refresh();                               
                        }, 1000); 
                        this.itemReturnApply();
                    }
                }                                
            }            
        }       
        
        return ret;
    }
        
    @api 
    get hasElement7(){        
        let ret = false;   
        if (this.rFilter !== undefined){                             
            ret = (this.task.id === '7') ? true : false;
            if (ret)
            {                   
                this.settingFilter9 = "Sales Overview - Repayments (" + this.task.period + ")"; 
                let f1 = JSON.stringify(this.rFilter.l9);
                let f2 = JSON.stringify(this.rFilterEl9);                       
                this.rFilterEl9 = this.rFilter.l9;     
                if (this.rApply){                        
                    if (f1 !== f2){
                        console.log('apply9');
                        window.setTimeout(() => {                    
                            this.template.querySelector('c-lwc-sales-rep').refresh();                               
                        }, 1000); 
                        this.itemReturnApply();
                    }
                }                                
            }            
        }       
        
        return ret;
    }

    @api 
    get hasElement8(){        
        let ret = false;   
        if (this.rFilter !== undefined){                             
            ret = (this.task.id === '8') ? true : false;
            if (ret)
            {                 
                let targetValue = "0.00";
                //console.log('l10 ' + this.rFilter.l10);
                if (this.rFilter.l10 !== undefined){
                    targetValue = this.rFilter.l10.targetValue;
                }
                this.settingFilter10 =  "Sales Target (A$ " + targetValue + ") (" + this.task.period + ")"; 
                let f1 = JSON.stringify(this.rFilter.l10);
                let f2 = JSON.stringify(this.rFilterEl10);                       
                this.rFilterEl10 = this.rFilter.l10;     
                if (this.rApply){                        
                    if (f1 !== f2){
                        console.log('apply10');
                        window.setTimeout(() => {                    
                            this.template.querySelector('c-lwc-sales-target-rep').refresh();                               
                        }, 1000); 
                        this.itemReturnApply();
                    }
                }                                
            }            
        }       
        
        return ret;
    }

    @api 
    get hasElement9(){        
        let ret = false;   
        if (this.rFilter !== undefined){                             
            ret = (this.task.id === '9') ? true : false;
            if (ret)
            {                  
                this.settingFilter11 = "Pipeline Overview New Zealand";// (" + this.task.period + ")"; 
                let f1 = JSON.stringify(this.rFilter.l11);
                let f2 = JSON.stringify(this.rFilterEl11);                       
                this.rFilterEl11 = this.rFilter.l11;     
                if (this.rApply){                        
                    if (f1 !== f2){
                        console.log('apply11');
                        window.setTimeout(() => {                    
                            this.template.querySelector('c-lwc-stage-status-total-rep').refresh();                               
                        }, 1000); 
                        this.itemReturnApply();
                    }
                }                                
            }            
        }       
        
        return ret;
    }

    @api 
    get hasElement10(){        
        let ret = false;   
        if (this.rFilter !== undefined){                             
            ret = (this.task.id === '10') ? true : false;
            if (ret)
            {                  
                this.settingFilter12 = "Quantity of Enrolments (" + this.task.period + " = Last 3 years)"; 
                let f1 = JSON.stringify(this.rFilter.l12);
                let f2 = JSON.stringify(this.rFilterEl12);                       
                this.rFilterEl12 = this.rFilter.l12;     
                if (this.rApply){                        
                    if (f1 !== f2){
                        console.log('apply12');
                        window.setTimeout(() => {                    
                            this.template.querySelector('c-lwc-sales-rep').refresh();                               
                        }, 1000); 
                        this.itemReturnApply();
                    }
                }                                
            }            
        }       
        
        return ret;
    }

    @api 
    get hasElement11(){        
        let ret = false;   
        if (this.rFilter !== undefined){                             
            ret = (this.task.id === '11') ? true : false;
            if (ret)
            {                   
                this.settingFilter13 = "Commission Invoice Overview (" + this.task.period + ")"; 
                let f1 = JSON.stringify(this.rFilter.l13);
                let f2 = JSON.stringify(this.rFilterEl13);                       
                this.rFilterEl13 = this.rFilter.l13;     
                if (this.rApply){                        
                    if (f1 !== f2){
                        console.log('apply13');
                        window.setTimeout(() => {                    
                            this.template.querySelector('c-lwc-sales-rep').refresh();                               
                        }, 1000); 
                        this.itemReturnApply();
                    }
                }                                
            }            
        }       
        
        return ret;
    }

    @api 
    get hasElement12(){        
        let ret = false;   
        if (this.rFilter !== undefined){                             
            ret = (this.task.id === '12') ? true : false;
            if (ret)
            {                   
                this.settingFilter14 = "Pipeline Overview Brazil";// (" + this.task.period + ")"; 
                let f1 = JSON.stringify(this.rFilter.l14);
                let f2 = JSON.stringify(this.rFilterEl14);                       
                this.rFilterEl14 = this.rFilter.l14;     
                if (this.rApply){                        
                    if (f1 !== f2){
                        console.log('apply14');
                        window.setTimeout(() => {                    
                            this.template.querySelector('c-lwc-stage-status-total-rep').refresh();                               
                        }, 1000); 
                        this.itemReturnApply();
                    }
                }                                
            }            
        }       
        
        return ret;
    }

    @api 
    get hasElement13(){        
        let ret = false;   
        if (this.rFilter !== undefined){                             
            ret = (this.task.id === '13') ? true : false;
            if (ret)
            {                   
                this.settingFilter15 = "Sales Revenue (" + this.task.period + " = Last 3 years)"; 
                let f1 = JSON.stringify(this.rFilter.l15);
                let f2 = JSON.stringify(this.rFilterEl15);                       
                this.rFilterEl15 = this.rFilter.l15;     
                if (this.rApply){                        
                    if (f1 !== f2){
                        console.log('apply15');
                        window.setTimeout(() => {                    
                            this.template.querySelector('c-lwc-sales-rep').refresh();                               
                        }, 1000); 
                        this.itemReturnApply();
                    }
                }                           
            }            
        }       
        
        return ret;
    }

    @api 
    get hasElement14(){        
        let ret = false;   
        if (this.rFilter !== undefined){                             
            ret = (this.task.id === '14') ? true : false;
            if (ret)
            {                   
                this.settingFilter16 = "Pipeline Overview Canada";// (" + this.task.period + ")"; 
                let f1 = JSON.stringify(this.rFilter.l16);
                let f2 = JSON.stringify(this.rFilterEl16);                       
                this.rFilterEl16 = this.rFilter.l16;     
                if (this.rApply){                        
                    if (f1 !== f2){
                        console.log('apply16');
                        window.setTimeout(() => {                    
                            this.template.querySelector('c-lwc-stage-status-total-rep').refresh();                               
                        }, 1000); 
                        this.itemReturnApply();
                    }
                }                           
            }            
        }       
        
        return ret;
    }


    @api 
    get hasElement15(){        
        let ret = false;   
        if (this.rFilter !== undefined){                             
            ret = (this.task.id === '15') ? true : false;
            if (ret)
            {                   
                this.settingFilter17 = "Pipeline Overview Czech Republic";// (" + this.task.period + ")";  
                let f1 = JSON.stringify(this.rFilter.l17);
                let f2 = JSON.stringify(this.rFilterEl17);                       
                this.rFilterEl17 = this.rFilter.l17;     
                if (this.rApply){                        
                    if (f1 !== f2){
                        console.log('apply17');
                        window.setTimeout(() => {                    
                            this.template.querySelector('c-lwc-stage-status-total-rep').refresh();                               
                        }, 1000); 
                        this.itemReturnApply();
                    }
                }                           
            }            
        }       
        
        return ret;
    }


    @api 
    get hasElement16(){        
        let ret = false;   
        if (this.rFilter !== undefined){                             
            ret = (this.task.id === '16') ? true : false;
            if (ret)
            {                   
                this.settingFilter18 = "Pipeline Overview Ireland";// (" + this.task.period + ")"; 
                let f1 = JSON.stringify(this.rFilter.l18);
                let f2 = JSON.stringify(this.rFilterEl18);                       
                this.rFilterEl18 = this.rFilter.l18;     
                if (this.rApply){                        
                    if (f1 !== f2){
                        console.log('apply18');
                        window.setTimeout(() => {                    
                            this.template.querySelector('c-lwc-stage-status-total-rep').refresh();                               
                        }, 1000); 
                        this.itemReturnApply();
                    }
                }                           
            }            
        }       
        
        return ret;
    }


    @api 
    get hasElement17(){        
        let ret = false;   
        if (this.rFilter !== undefined){                             
            ret = (this.task.id === '17') ? true : false;
            if (ret)
            {                   
                this.settingFilter19 = "Pipeline Overview United Kingdom";// (" + this.task.period + ")"; 
                let f1 = JSON.stringify(this.rFilter.l19);
                let f2 = JSON.stringify(this.rFilterEl19);                       
                this.rFilterEl19 = this.rFilter.l19;     
                if (this.rApply){                        
                    if (f1 !== f2){
                        console.log('apply19');
                        window.setTimeout(() => {                    
                            this.template.querySelector('c-lwc-stage-status-total-rep').refresh();                               
                        }, 1000); 
                        this.itemReturnApply();
                    }
                }                           
            }            
        }       
        
        return ret;
    }

    @api 
    get hasElement18(){        
        let ret = false;   
        if (this.rFilter !== undefined){                             
            ret = (this.task.id === '18') ? true : false;
            if (ret)
            {                   
                this.settingFilter20 = "Contacts with Visa Expiry (" + this.task.period + ")"; 
                let f1 = JSON.stringify(this.rFilter.l20);
                let f2 = JSON.stringify(this.rFilterEl20);                       
                this.rFilterEl20 = this.rFilter.l20;     
                if (this.rApply){                        
                    if (f1 !== f2){
                        console.log('apply20');
                        window.setTimeout(() => {                    
                            this.template.querySelector('c-lwc-stage-status-total-rep').refresh();                               
                        }, 1000); 
                        this.itemReturnApply();
                    }
                }                           
            }            
        }       
        
        return ret;
    }

    @api 
    get hasElement19(){        
        let ret = false;   
        if (this.rFilter !== undefined){                             
            ret = (this.task.id === '19') ? true : false;
            if (ret)
            {                   
                this.settingFilter21 = "Contacts with Travel Date (" + this.task.period + ")"; 
                let f1 = JSON.stringify(this.rFilter.l21);
                let f2 = JSON.stringify(this.rFilterEl21);                       
                this.rFilterEl21 = this.rFilter.l21;     
                if (this.rApply){                        
                    if (f1 !== f2){
                        console.log('apply21');
                        window.setTimeout(() => {                    
                            this.template.querySelector('c-lwc-stage-status-total-rep').refresh();                               
                        }, 1000); 
                        this.itemReturnApply();
                    }
                }                           
            }            
        }       
        
        return ret;
    }


    itemReturnApply(){
        const event = new CustomEvent('itemapply', {          
            detail: false
        });
       
        this.dispatchEvent(event);
    }

    itemDragStart(evt) {         
        evt.dataTransfer.dropEffect = 'move';  
        evt.dataTransfer.setData('itemdrag', this.task.id);
        const event = new CustomEvent('itemdrag', {            
            detail: this.task.id            
        });
       
        this.dispatchEvent(event);
    }
}