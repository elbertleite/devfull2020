<apex:page controller="report_leadConversion" readOnly="true" action="{!checkUserAccess}" >
	<apex:include pageName="xCourseSearchStyle" />
	<apex:include pageName="xStylePageV2" />
	<!-- Chosen -->
	<apex:styleSheet value="{!URLFOR($Resource.chosen,'chosen.css')}" />    
	<apex:includeScript value="{!URLFOR($Resource.chosen,'chosen.jquery.js')}" />
	<apex:includeScript value="{!$Resource.floatThead}"/>
	
	<apex:includeScript value="{!URLFOR($Resource.amcharts,'amcharts/amcharts.js')}" />
	<apex:includeScript value="{!URLFOR($Resource.amcharts,'amcharts/serial.js')}" />
	<apex:includeScript value="{!URLFOR($Resource.amcharts,'amcharts/themes/light.js')}" />
	
	<style>	
		.floatThead-container { z-index: 2 !important; }
        .left { text-align: left !important; }
		.noBackground {background-color: #FFF;}
		.noBorder { border: none !important;}
		.agency td:first-child { padding-left: 20px; }
		.user td:first-child { padding-left: 35px; }
		
		.user td { color: #555; }
		
		.agency td { background-color: #eee; font-size: 1.1em; }
		
		.agencyGroup td {
			background-color: #ddd;
			font-weight: bold;
			font-size: 1.4em;
			padding: 0.3em !important;			
		}
		
		.reportHeader {
			font-weight: bold; 
			padding: 5px;
			width: 99.4%;
			border-top: 1px solid #999;
			border-left: 1px solid #999;
			border-right: 1px solid #999;
			border-radius: 1px;
			box-shadow: 0 0 0 #CCCCCC;
		}
		table.result tr td { 
			border: 1px solid #f5f5f5;
			padding: 5px;
			vertical-align: middle;
        	text-align: center;
		}
		table.result th {
        	background: #999;
        	color: #fff;
        	font-size: 1.2em;
        	text-align: center;
        	padding: 5px;
        }
		
		table.result {
			border-collapse: collapse;
			width: 100%;
			margin-top: 1%;
		}
		.box-searh {
			background-color: #F3F3F3;
			border-left: 1px solid #999999 !important;
			border-bottom: 1px solid #999999 !important;
			border-right: 1px solid #999999 !important;			
			border-radius: 1px;
			box-shadow: 0 0 0 #CCCCCC;
			display: block;
			padding: 4px;
		}
	
		.SearchField {
			border: 0 none;
			float: left;
			width: 100%;
		}
		
		.searchBtn {
			color: #8FAACD;
			position: relative;
			text-decoration: none;
			top: -11px;
		}
		.sourceRow:hover .sourceColumn{
			background-color: #E3F3FF !important;
		}
		.spacing { padding: 5px 0; }
		.hide { display: none; }
		.outer { width: 100%; }
		.inner { margin: 0 auto; width: 45%; text-align: center; font-size: 2em; color: #666; }
		.helpIcon.custom { float: right; position: relative; top: 2px; }
	</style>
	<script>
   		function calcTotal(theClass){
        	var tot = 0;
        	$('.'+theClass).each(function(){
        		tot += $(this).html() * 1;
        	});	
        	return tot;
        }
        function getConversionRate(total, converted){
            return Math.round( ((converted/total) * 100) * 100) / 100;
        }
    	function calcAverage(theClass){
        	var avg = 0;
            var i = 0;
        	$('.'+theClass).each(function(){
        		avg += $(this).html() * 1
                i++;
        	});	 
        	
            return Math.round( (avg/i) * 100) / 100;
            	
        }
        function showLoadingArea(){
	      	jQuery('.chartResult').block({  message: '<H1 style="font-size:1.3em">Loading...</H1>',
		       css: { 
		         border: 'none', 
		         padding: '15px', 
		         backgroundColor: '#000', 
		         '-webkit-border-radius': '10px', 
		         '-moz-border-radius': '10px', 
		         opacity: 30, 
		         color: '#fff',
		         top:  ($(window).height() - 200) /2 + 'px', 
		         left: ($(window).width() - 200) /2 + 'px', 
		         width: '200px'
	       		} 
	    	});
	  	}
	  
	    
   		function hideLoading(){
        	jQuery.unblockUI();
    	}
    </script>	
	
	
	<apex:form >
		<apex:actionStatus id="areaStatus" onstart="showLoadingArea();" onstop="hideLoading();" />
		<a href="javascript: history.go(-1)" class="title" style="text-decoration:none;">		
			<apex:outputText value="Back to Reports" styleClass="title" />
		</a>
		
		<br/><br/>
		
		<div class="agencyBackground agencyFont reportHeader">
			<apex:outputText value="Average Lead Conversion Time" style="margin-left:10px;" /> <!--<apex:outputText value=" (Check progress, status and important dates of clients)" style="font-size:0.75em;"/>-->
		</div>	
		
		<apex:outputPanel id="filters" layout="block" styleClass="box-searh" style="width:99.5%;">
			<table id="resultData">
				<tr>
					<td>
						<apex:outputPanel styleClass="SearchField">
							<div class="SearchFooter"><span class="SearchLabel">Agency Group</span></div>
							<div class="SearchInput">
								<apex:selectList value="{!selectedAgencyGroup}" style="width: 200px;" size="1" id="selAgencyGroup" styleclass="chosen" disabled="{!NOT(user.Contact.Group_View_Permission__c)}">
									<apex:SelectOptions value="{!agencyGroupOptions}" />
									<apex:actionSupport event="onchange" action="{!refreshAgencies}" status="statusLoad" reRender="filters" oncomplete="jQuery('.chosen').chosen();" />
								</apex:selectList>
							</div>
						</apex:outputPanel>
					</td>
					
					<td style="padding-left: 25px;">
						<apex:outputPanel styleClass="SearchField">
							<div class="SearchFooter"><span class="SearchLabel">Agency</span></div>
							<div class="SearchInput">
								<apex:outputPanel id="selAgency" >
									<apex:selectList value="{!selectedAgency}" style="width: 200px;" size="1"  styleclass="chosen" disabled="{!AND(NOT(user.Contact.Group_View_Permission__c),ISNULL(user.Aditional_Agency_Managment__c))}">
										<apex:SelectOptions value="{!agencyOptions}" />										
									</apex:selectList>
								</apex:outputPanel>
							</div>
						</apex:outputPanel>
					</td>
                    
                    <td style="padding-left: 25px;">
						<apex:outputPanel styleClass="SearchField">
							<div class="SearchInput">
                                <apex:selectRadio value="{!selectedSearchOption}" style="width: 200px;" layout="pageDirection" styleClass="searchOptions" >
                                    <apex:SelectOptions value="{!searchOptions}" />										
                                </apex:selectRadio>
							</div>							
						</apex:outputPanel>
					</td>
					
					<td style="padding-left: 25px;">
						<apex:outputPanel styleClass="SearchField">							
							<div class="SearchInput" style="padding-top: 20px;" >								
								<apex:inputField value="{!range.Arrival_Date__c}"/>
								<apex:outputText value="and" style="margin: 0 5px" styleClass="SearchLabel" /> 
								<apex:inputField value="{!range.Expected_Travel_Date__c}" />
							</div>
						</apex:outputPanel>
					</td>
				
					<td style="vertical-align: middle; padding-left: 25px;">
						<apex:outputPanel styleClass="SearchField">
							<div class="SearchInput">
								<apex:commandLink status="statusLoad" action="{!search}" value="Search" styleClass="agencyButton " title="Search!" reRender="result, resultExcel, chartResult" />
							</div>
						</apex:outputPanel>
					</td>
                    
				</tr>
			</table>
			<script>
				$('.chosen').chosen();
				$('span.dateFormat').hide();
				$('.searchOptions input[value="conversion_rate"]').parent().append('<img class="helpIcon custom" alt="" src="/s.gif" title="An overview of how many leads were created, the amount converted and the average conversion time within the specified period." />');
				$('.searchOptions input[value="conversion_date"]').parent().append('<img class="helpIcon custom" alt="" src="/s.gif" title="How many leads were converted into clients during the specified period, despite the date of creation of the lead in the system." />');
			</script>
            
            <apex:outputPanel id="resultExcel">
                <apex:outputPanel rendered="{!myDetails.Export_Report_Permission__c}">
                    <apex:commandLink value="Export to  Excel" onclick="genExcel()" style="float: right;margin:8px 0;" rerender="resultExcel" target="_blank" immediate="true" status="statusLoad" styleClass="agencyButton" />
                </apex:outputPanel>
            </apex:outputPanel>
            
            
            <apex:actionFunction name="genExcel" action="{!generateExcel}" />
            
		</apex:outputPanel>
		
		<apex:actionFunction action="{!refreshChart}" name="refreshChart" rerender="chartResult" status="statusLoad">
			<apex:param name="subjectType" value="" />
			<apex:param name="subjectId" value="" />
		</apex:actionFunction>
		
	</apex:form>	
	
	
	<apex:outputPanel id="chartResult" styleClass="chartResult" style="padding: 1em; display:block; clear:both; height: auto;" layout="block">
			
			
		<div class="outer">
			<div class="inner">
				<apex:outputText value="{!chartTitle}" />
			</div>
		</div>
		
		<div id="chartdiv" style="height: 400px;"></div>
		
		
		<apex:outputPanel rendered="{!searchByCreatedDate}">
		
			<script>
				if(chart != null){
					chart.clear();
					chart = null;
				}
				
			
				var data = [];
	        	<apex:repeat value="{!chartData}" var="period">
	                var st = {"period" : '{!chartData[period].name}', "totalLeads" : '{!chartData[period].totalLeads}', "convertedLeads" : '{!chartData[period].convertedLeads}', "avgConversionInDays" : '{!ROUND(chartData[period].avgConversionInDays,0)}'};
	        		data.push(st);
	            </apex:repeat>
	            
	           var chart = AmCharts.makeChart("chartdiv", {
						    "theme": "light",
						    "type": "serial",														    
						    "dataProvider": data,
						    "legend": {
								        "equalWidths": false,
								        "useGraphSettings": true,
								        "valueAlign": "left",
								        "valueWidth": 120
								      },
						    "valueAxes": [{
									        "position": "left",
									        "title": "Leads Created",
									        "integersOnly": true
						    			 },{
									        "id": "durationAxis",
									        "duration": "DD",
									        "durationUnits": {
									            "DD": " days"
									        },
									        "integersOnly": true,
									        "axisAlpha": 0,
									        "gridAlpha": 0,
									        "inside": true,
									        "position": "right",
									        "title": "Average Conversion Time"
									    }],
						    "startDuration": 1,
						    "graphs": [{
						        "balloonText": "total leads: <b>[[value]]</b>",
						        "fillAlphas": 0.9,
						        "lineAlpha": 0.2,
						        "title": "Leads Created",
						        "type": "column",
						        "valueField": "totalLeads"
						    }, {
						        "balloonText": "converted leads: <b>[[value]]</b>",
						        "fillAlphas": 0.9,
						        "lineAlpha": 0.2,
						        "title": "Leads Converted",
						        "type": "column",
						        "clustered":false,
						        "columnWidth":0.5,
						        "valueField": "convertedLeads"
						    }, {
						        "bullet": "square",
						        "bulletBorderAlpha": 1,
						        "bulletBorderThickness": 1,
						        "dashLengthField": "dashLength",
						        "legendValueText": "[[value]]",
						        "title": "Average Conversion Time",
						        "fillAlphas": 0,
						        "valueField": "avgConversionInDays",
						        "valueAxis": "durationAxis"														         
						    }],
						    "plotAreaFillAlphas": 0.1,
						    "categoryField": "period",
						    "categoryAxis": {
						        "gridPosition": "start"														        
						    },
						    "chartCursor": {
						        "categoryBalloonDateFormat": "DD",
						        "cursorAlpha": 0.1,
						        "cursorColor":"#000000",
						         "fullWidth":true,
						        "valueBalloonsEnabled": false,
						        "zoomable": false
					    	}														
						});	            
	            
			</script>		
		</apex:outputPanel>
		
		<apex:outputPanel rendered="{!NOT(searchByCreatedDate)}">
		
			<script>
				if(chart != null){
					chart.clear();
					chart = null;
				}
				
			
				var data = [];
	        	<apex:repeat value="{!chartData}" var="period">
	                var st = {"period" : '{!chartData[period].name}', "convertedLeads" : '{!chartData[period].totalLeads}', "avgConversionInDays" : '{!ROUND(chartData[period].avgConversionInDays,0)}'};
	        		data.push(st);
	            </apex:repeat>
	            
	           var chart = AmCharts.makeChart("chartdiv", {
						    "theme": "light",
						    "type": "serial",														    
						    "dataProvider": data,
						    "legend": {
								        "equalWidths": false,
								        "useGraphSettings": true,
								        "valueAlign": "left",
								        "valueWidth": 120
								      },
						    "valueAxes": [{
									        "position": "left",
									        "title": "Leads Converted",
									        "integersOnly": true																	        
						    			 },{
									        "id": "durationAxis",
									        "duration": "DD",
									        "durationUnits": {
									            "DD": " days"
									        },
									        "axisAlpha": 0,
									        "gridAlpha": 0,
									        "inside": true,
									        "position": "right",
									        "title": "Average Conversion Time"
									    }],
						    "startDuration": 1,
						    "graphs": [{
						        "balloonText": "converted leads: <b>[[value]]</b>",
						        "fillAlphas": 0.9,
						        "lineAlpha": 0.2,
						        "title": "Leads Converted",
						        "type": "column",
						        "clustered":false,
						        "columnWidth":0.5,
						        "valueField": "convertedLeads",
						        "fillColors" : "#EFD216"
						    }, {
						        "bullet": "square",
						        "bulletBorderAlpha": 1,
						        "bulletBorderThickness": 1,
						        "dashLengthField": "dashLength",
						        "legendValueText": "[[value]]",
						        "title": "Average Conversion Time",
						        "fillAlphas": 0,
						        "valueField": "avgConversionInDays",
						        "valueAxis": "durationAxis",
						        "lineColor" : "#84B761"												         
						    }],
						    "plotAreaFillAlphas": 0.1,
						    "categoryField": "period",
						    "categoryAxis": {
						        "gridPosition": "start"														        
						    },
						    "chartCursor": {
						        "categoryBalloonDateFormat": "DD",
						        "cursorAlpha": 0.1,
						        "cursorColor":"#000000",
						         "fullWidth":true,
						        "valueBalloonsEnabled": false,
						        "zoomable": false
					    	}														
						});	            
	            
			</script>
		
		
		</apex:outputPanel>
	</apex:outputPanel>
	
	
	
	
	
	
	
	<apex:outputPanel id="result" style="padding: 1em;">

		<apex:pageMessages />
		
		<table class="result">
			<thead>
				<tr>
					<th class="noBackground"></th>
					<th class="noBackground">
						Total Leads {!IF(searchByCreatedDate, '', 'Converted')}
						<span class="tl-header"></span>
					</th>
					<th class="noBackground {!IF(searchByCreatedDate, '', 'hide')}">
						Converted Leads
						<span class="cl-header"></span>
					</th>
					<th class="noBackground {!IF(searchByCreatedDate, '', 'hide')}">
						Conversion Rate
						<span class="cr-header"></span>
					</th>
					<th class="noBackground">
						Average Conversion Time
						<span class="act-header">({!ROUND(agencyGroupConversion['overall'], 0)} days)</span>
					</th>
				</tr>
			</thead>
			<tbody>
				<apex:repeat value="{!conversionMap}" var="group" >
					<apex:variable value="{!SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(group, '(', ''), ')', ''), ' ', '-')}" var="jsgroup" />
					
					<tr class="agencyGroup sourceRow" style="{!IF(selectedAgency == 'all', '', 'display: none;')}">
						<td class="sourceColumn left group">
							<apex:outputLink onclick="refreshChart('agencyGroup', '{!group}'); document.body.scrollTop = document.documentElement.scrollTop = 0; return false;" >{!group}</apex:outputLink>
						</td>
						<td class="sourceColumn">
                            <apex:outputText value="" styleClass="total-{!jsgroup} tl-group" />
						</td>
						<td class="sourceColumn {!IF(searchByCreatedDate, '', 'hide')}">
                            <apex:outputText value="" styleClass="converted-{!jsgroup} cl-group" />
						</td>
						<td class="sourceColumn {!IF(searchByCreatedDate, '', 'hide')}">
                            <apex:outputText value="" styleClass="cr-{!jsgroup} cr-group" />
                            <apex:outputText value="%" />
						</td>
						<td class="sourceColumn">						
							<apex:outputText value="{!ROUND(agencyGroupConversion[group], 0)} days"  /> <!-- styleClass="act-{!jsgroup} act-group" -->
						</td>
					</tr>
					
					<apex:repeat value="{!conversionMap[group]}" var="agency" >
						<apex:variable value="{!SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(agency, '(', ''), ')', ''), ' ', '-')}" var="jsagency" />
						<tr><td class="spacing noBackground noBorder" colspan="5"></td></tr>
						<tr class="agency sourceRow">
							<td class="sourceColumn left agency">
								<apex:outputLink onclick="refreshChart('agency', '{!agency}'); document.body.scrollTop = document.documentElement.scrollTop = 0; return false;">{!agency}</apex:outputLink>
							</td>
							<td class="sourceColumn">
                                <apex:outputText value="" styleClass="total-{!jsagency}" />
							</td>
							<td class="sourceColumn {!IF(searchByCreatedDate, '', 'hide')}">
                                <apex:outputText value="" styleClass="converted-{!jsagency}" />
							</td>
							<td class="sourceColumn {!IF(searchByCreatedDate, '', 'hide')}">
                            	<apex:outputText value="" styleClass="cr-{!jsagency}" />
                            </td>
							<td class="sourceColumn">
								<apex:outputText value="{!ROUND(agencyGroupConversion[agency], 0)} days" />
							</td>
						</tr>
						
						<apex:repeat value="{!conversionMap[group][agency]}" var="user" >
                            
							<tr class="user sourceRow">
								<td class="sourceColumn left">
									<apex:outputLink onclick="refreshChart('user', '{!user}'); document.body.scrollTop = document.documentElement.scrollTop = 0; return false;" >{!user}</apex:outputLink>									
								</td>
								<td class="sourceColumn">
                                    <apex:outputText value="{!conversionMap[group][agency][user].totalLeads}" rendered="{!conversionMap[group][agency][user].totalLeads > 0}" 
                                                     styleClass="user-total-{!jsagency} user-total-{!jsgroup}" />
                                    
                                    <apex:outputText value="-" rendered="{!conversionMap[group][agency][user].totalLeads = 0}" />
                               	</td>
								<td class="sourceColumn {!IF(searchByCreatedDate, '', 'hide')}">
	                                <apex:outputText value="{!conversionMap[group][agency][user].convertedLeads}" rendered="{!conversionMap[group][agency][user].convertedLeads > 0}"
                                                     styleClass="user-converted-{!jsagency} user-converted-{!jsgroup}" />
                                    
	                                <apex:outputText value="-" rendered="{!conversionMap[group][agency][user].convertedLeads = 0}" />
	                           	</td>
								<td class="sourceColumn {!IF(searchByCreatedDate, '', 'hide')}">
	                                 <apex:outputText value="{! ROUND( (conversionMap[group][agency][user].convertedLeads/conversionMap[group][agency][user].totalLeads)*100 ,2)}%" 
	                                                  rendered="{!conversionMap[group][agency][user].convertedLeads > 0}" styleClass="user-cr-{!jsagency} user-cr-{!jsgroup}" />
	                               	<apex:outputText value="-" rendered="{!conversionMap[group][agency][user].convertedLeads = 0}" />
	                           	</td>
								<td class="sourceColumn">
									
	                                <apex:outputText value="-" rendered="{!searchByCreatedDate && conversionMap[group][agency][user].convertedLeads = 0}" />
                                    <apex:outputPanel rendered="{!not(searchByCreatedDate) || searchByCreatedDate && conversionMap[group][agency][user].convertedLeads > 0}" >
                                    	<apex:outputText value="1 day" rendered="{!conversionMap[group][agency][user].avgConversionInDays <= 1}" />	                                	
                                        <apex:outputText value="{!ROUND(conversionMap[group][agency][user].avgConversionInDays, 0)}" rendered="{!conversionMap[group][agency][user].avgConversionInDays > 1}" />
                                        <apex:outputText value="{!conversionMap[group][agency][user].avgConversionInDays}" styleClass="user-act-{!jsagency} user-act-{!jsgroup}" style="display: none;" />                                    
                                    	<apex:outputText value=" days" rendered="{!conversionMap[group][agency][user].avgConversionInDays > 1}" />
                                    </apex:outputPanel>                                    
	                           	</td>
							</tr>
						</apex:repeat>
						
                        <tr style="display: none;">
                            <td>
                            	<script>
                                    var totalAgency = calcTotal('user-total-{!jsagency}');
                                    $('.total-{!jsagency}').html(totalAgency);
                                    var convertedAgency = calcTotal('user-converted-{!jsagency}');
                                    $('.converted-{!jsagency}').html(convertedAgency);
                                    $('.cr-{!jsagency}').html( getConversionRate(totalAgency,convertedAgency) + '%');                                   
                            	</script>
                            </td>
                        </tr>
                        
					</apex:repeat>
                    
                    <tr style="display: none;">
                    	<td>
                        	<script>                                                	
                                var totalGroup = calcTotal('user-total-{!jsgroup}');							
                                $('.total-{!jsgroup}').html(totalGroup);
                                var convertedGroup = calcTotal('user-converted-{!jsgroup}');
                                $('.converted-{!jsgroup}').html(convertedGroup);                                
                                $('.cr-{!jsgroup}').html( getConversionRate(totalGroup,convertedGroup) );
                            </script>
                        </td>
                    </tr>
                      
				</apex:repeat>				
			</tbody>
			<tfoot>
			</tfoot>
		</table>
		
		<script>
			$('.result').floatThead();			
			$('.tl-header').html('(' + calcTotal('tl-group') + ')');
			$('.cl-header').html('(' + calcTotal('cl-group') + ')');
			$('.cr-header').html('(' + calcAverage('cr-group') + '%)');
		</script>
		
	</apex:outputPanel>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
</apex:page>