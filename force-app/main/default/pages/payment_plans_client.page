<apex:page standardController="contact" extensions="payment_plans_client" showHeader="false" sidebar="false">
	<apex:slds />
    <!-- <apex:includeScript value="{!$Resource.jquery}"/> -->
    <apex:includeScript value="{!$Resource.jQueryCurrency}"/>
    <style>
        .dateFormat{
            display: none;
        }

        .statusButton{
		     border-radius: 3px;
		     //box-shadow: 1px 1px 3px #b5b5b5;
		     color: #fff;
		     //font-size: 0.8em;
		     font-weight: bold;
		     text-align: center;
		     width: 100px !important;
		     display: block;
             line-height: 22px;
		}

		.statusGreen {
			background: none repeat scroll 0 0 #6EB300;
	    }

	    .statusYellow {
	         background: none repeat scroll 0 0 #FAA82A;
	     }

	    .statusRed {
	         background: none repeat scroll 0 0 #DD5A46;
	    }

	    .statusBlue {
	         background: none repeat scroll 0 0 #0792D5;
	    }

	   .statusGrey {
	         background: none repeat scroll 0 0 #ADB1B4;
	    }

    </style>

    <div class="slds-page-header">
        <div class="slds-media">
            <div class="slds-media__figure">
                <span class="slds-icon_container slds-icon-standard-opportunity" title="Payment Plan">
                    <svg class="slds-icon slds-page-header__icon" aria-hidden="true">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#list')}" />
                    </svg>
                </span>
            </div>
            <div class="slds-media__body">
                <h1 class="slds-page-header__title slds-truncate slds-align-middle" title="Payment Plan">Payment Plan</h1>
            </div>
            <apex:outputLink target="_top" styleClass="slds-button slds-button_brand" value="/contact_new_payment_plan?id={!$CurrentPage.parameters.id}">Create Payment Plan</apex:outputLink>
        </div>
    </div>
    <apex:repeat value="{!listPaymentPlans}" var="item">
        <div style="display: block; padding:5px;font-weight: bold;background: #c1c1c1;"><apex:outputText value="{!IF(Item = 'bucket','This is Bucket','This Instalment')}" /></div>
        <table class="slds-table slds-table_bordered slds-table_striped" style="margin-top: 10px;">
            <thead>
                <tr class="slds-text-title_caps">
                    <th scope="col">
                        <div class="slds-truncate" title="Client">Status</div>
                    </th>
                    <th scope="col">
                        <div class="slds-truncate" title="Client">Description</div>
                    </th>
                    <th scope="col">
                        <div class="slds-truncate" title="Client">Payment Plan</div>
                    </th>
                    <th scope="col">
                        <div class="slds-truncate" title="Remaining Value">Remaining Value</div>
                    </th>
                    <apex:outputPanel layout="none" rendered="{!item = 'instalments'}">
                        <th scope="col">
                            <div class="slds-truncate" title="Remaining Value">Status</div>
                        </th>
                    </apex:outputPanel>
                    <apex:outputPanel layout="none" rendered="{!item = 'bucket'}">
                        <th scope="col">
                            <div class="slds-truncate" title="Notes">Total Available</div>
                        </th>
                        <th scope="col">
                            <div class="slds-truncate" title="Action">Reminder</div>
                        </th>
                    </apex:outputPanel>
                    <th scope="col">
                        <div class="slds-truncate" title="Action">Action</div>
                    </th>
                </tr>
            </thead>
            <tbody>
                <apex:repeat value="{!listPaymentPlans[item]}" var="lPlan">
                    <tr>
                        <td data-label="Client" class="slds-cell-wrap" width="100px">
                            <apex:outputtext styleclass="statusButton statusRed" value="Unpaid" rendered="{!lPlan.itemPLan.Plan_Type__c = 'bucket' && lPlan.itemPlan.Value_Remaining__c = lPlan.itemPlan.Value__c}" />
                            <apex:outputtext styleclass="statusButton statusRed" value="Unpaid" rendered="{!lPlan.itemPLan.Plan_Type__c = 'instalments' && lPlan.itemPlan.Total_Paid__c = 0}" />
                            <apex:outputtext styleclass="statusButton statusYellow" value="Partially Paid" rendered="{!lPlan.itemPLan.Plan_Type__c="instalments" && lPlan.itemPlan.Total_Paid__c > 0}" />
                            <apex:outputtext styleclass="statusButton statusYellow" value="Partially Paid" rendered="{!lPlan.itemPLan.Plan_Type__c = 'bucket' && lPlan.itemPlan.Value__c - lPlan.itemPlan.Value_Remaining__c != lPlan.itemPlan.Value__c}" />
                            <apex:outputtext styleclass="statusButton statusGreen" value="Paid" rendered="{!lPlan.itemPlan.Value_Remaining__c = 0}" />
                        </td>
                        <td data-label="Client" class="slds-cell-wrap">
                            <apex:outputField value="{!lPlan.itemPlan.Description__c}" />
                        </td>
                        <td data-label="Client" class="slds-cell-wrap"> 
                            <div class="slds-truncate" title="Value">
                                 <apex:outputtext value="{0,number,currency}">
                                    <apex:param value="{!lPlan.itemPlan.Value__c}" />
                                </apex:outputtext> 
                                <p><span style="color:#0070d2; font-size:0.8em">({!lPlan.itemPlan.Currency_Code__c})</span></p>
                                {!lPlan.itemPlan.Total_Paid__c}
                            </div>
                        </td>
                        <td data-label="Plan Value" class="slds-cell-wrap">
                            <div class="slds-truncate" title="Value">
                                <apex:outputtext value="{0,number,currency}">
                                    <apex:param value="{!lPlan.itemPlan.Value_Remaining__c}" />
                                </apex:outputtext> 
                                <p><span style="color:#0070d2; font-size:0.8em">({!lPlan.itemPlan.Currency_Code__c})</span></p>
                            </div>
                        </td>
                        <apex:outputPanel layout="none" rendered="{!item = 'instalments'}">
                            <td data-label="Plan Value" class="slds-cell-wrap">
                                <apex:outputText value="Pending Approval" style="color:orange; font-weight: bold;" rendered="{!ISNULL(lPlan.itemPlan.Loan_Approved_On__c)}"/>
                                <apex:outputText value="Approved" style="color:green; font-weight: bold;" rendered="{!lPlan.itemPlan.loan_status__c = 'approved'}"/>
                                <apex:outputText value="Denied" style="color:red; font-weight: bold;" rendered="{!lPlan.itemPlan.loan_status__c = 'denied'}"/>
                            </td>
                        </apex:outputPanel>
                        <apex:outputPanel layout="none" rendered="{!item = 'bucket'}">
                            <td data-label="Plan Value" class="slds-cell-wrap">
                                <apex:outputField value="{!lPlan.itemPlan.Total_Available__c}" />
                            </td>
                            <td data-label="Plan Value" class="slds-cell-wrap">
                                <apex:outputField value="{!lPlan.itemPlan.Reminder_Date__c}" />
                            </td>
                        </apex:outputPanel>
                        <td data-label="Plan Value" class="slds-cell-wrap">
                            <apex:outputLink target="_top" value="/contact_new_payment_plan?id={!lPlan.itemPlan.Client__c}&pl={!lPlan.itemPlan.id}">View</apex:outputLink>
                        </td>
                        
                    </tr>
                </apex:repeat>
            </tbody>
        </table>
    </apex:repeat>
</apex:page>