<apex:page standardController="client_product_service__c" extensions="client_course_instalment_receipt" showHeader="false" sideBar="false" renderAs="pdf" applyBodyTag="false" applyHtmlTag="false" language="{!language}">
<!-- renderAs="pdf"  -->

<head>
    <style>
        @media print
            {
                body { font-family: Sans-serif; font-size: 0.7em; }

            }
            @page {
                    size: A4;
                    margin:0.15in;
                    margin-bottom: 0.3in;
                    border: 1px solid black;

                @bottom-center {
                    //content: "{!$Label.Quotation}: No.: ";
                    //font-size: 8pt;
                }
                @bottom-right {
                    content: "Page " counter(page) "/" counter(pages);
                    font-size: 8pt;
                }
            }

        .clientInvoice{
            padding:10px;
        }

        .contactDetails{
            display:block;
            width:100%;
            color:#999;
            font-size:16px;
        }

        .labelInvoice{
            font-weight: 900 !important;
            text-align: right !important;
            font-size: 1.1em !important;
        }

        .detailsTable{
            width:100%;
            border-spacing: 0px;
            line-height:15px;
        }

        .detailsTable th{
            border-bottom:1px solid black;
            font-size: 1.2em;
            text-align:left;
            font-weight: 900;
        }


        .detailsTable tr td{
            border-bottom:1px solid #ccc;
            padding:5px;
        }

        .detailsTable tr:last-child td{
            border-bottom:0px !important;
        }

        .paymentsTable{
            width:100%;
            border-spacing: 0px;
            line-height:15px;
        }

        .paymentsTable th{
            border-bottom:1px solid #ececec;
            font-size: 0.9em;
            text-align:left;
            font-weight: 300 !important;
            color: #787878 !important;
        }


        .paymentsTable tr td{
            border-bottom:0px !important;
            padding:5px;
            font-weight: 300 !important;
            font-size:0.9em;
        }

        .greyFont{
            color:#666 !important;
        }

        .totalsCell{
            border-top:1px solid black;
        }
    
    </style>
</head>

<div class="clientInvoice">
    <!-- INVOICE HEADER -->
    <div style="width:100%;display:block;">
        <div style="float:left; width:70%">
            <apex:image id="logo" value="{!URLFOR(agencyDetails.Logo__c)}" height="60px" />
        </div>
        <div style="float:left; width:30%; font-size:1em;">
            <apex:outputfield value="{!agencyDetails.Name}" /> <br/>
            <apex:outputfield value="{!agencyDetails.BillingStreet}" /> <br/>
            <apex:outputfield value="{!agencyDetails.BillingCountry}" />,&nbsp;<apex:outputfield value="{!agencyDetails.BillingCity}" />, &nbsp;<apex:outputfield value="{!agencyDetails.BillingPostalCode}" /><br/>
        </div>
    </div>
    <!-- END -- INVOICE HEADER -->

    <div style="clear:both;height:20px;"><br /></div>

    <div style="color:black;font-weight:bold; text-align:center;">
        <div style="width:100%">
            <h2>{!$Label.Payment_Receipt}</h2>
        </div>
    </div>

    <div style="clear:both;height:20px;"><br /></div>
    <!-- INVOICE NUMBER -->
    <div>
        <table style="width:400px;" cellspacing="3px">
            <tr>
                <td class="labelInvoice" style="width:40%">{!$Label.Client_Name}</td>
                <td style="width:60%">
                    <apex:outputtext value="{!clientName}" />
                </td>
            </tr>
            <tr>
                <td class="labelInvoice" style="width:40%">{!$Label.Invoice_Number}</td>
                <td style="width:60%">
                    <apex:outputtext value="{!invoiceNumber}" />
                </td>
            </tr>
            <!-- <tr>
                <td class="labelInvoice" style="width:40%">Instalment Nº</td>
                <td style="width:60%">
                    {!instNumber}
                </td>
            </tr> -->
            <tr>
                <td class="labelInvoice" style="width:40%">{!$Label.Issue_Date}</td>
                <td style="width:60%">
                    <apex:outputText value="{0, date, dd/MM/YYYY}">
                        <apex:param value="{!Today()}" />
                    </apex:outputText>
                </td>
            </tr>
            <apex:outputpanel layout="none" rendered="{!NOT(ISNULL(invoiceDueDate))}">
                <tr>
                    <td class="labelInvoice">{!$ObjectType.Invoice__c.fields.Due_Date__c.label}</td>
                    <td>
                        <apex:outputText value="{0, date, dd/MM/YYYY}">
                            <apex:param value="{!invoiceDueDate}" />
                        </apex:outputText>
                    </td>
                </tr>
            </apex:outputpanel>
        </table>
    </div>
    <!-- END -- INVOICE NUMBER -->


    <div style="clear:both;height:20px;"><br /></div>

    <!-- PRODUCTS LIST -->
        <table class="detailsTable">
            <thead>
                <tr >
                    <th style="width:300px;"> {!$Label.Description} </th>
                    
                    <th> {!$Label.Start_date} </th>

                    <th> {!$Label.End_date} </th>

                    <th> {!$Label.Qty} </th>

                    <th> {!$Label.Total} </th>
                </tr>
            </thead>
            <apex:repeat value="{!prodInvoice}" var="pd">
                <tbody>
                    <tr>
                        <td >
                            <b>{!pd.Product_Name__c}</b>
                            <div style="font-size:0.8em;font-weight:normal;color:grey;">{!pd.Products_Services__r.Provider__r.Provider_Name__c} </div>
                            <apex:outputText style="color:red;font-size: 0.9em; font-style: italic;" value="Product not paid yet" rendered="{!pd.Received_Date__c == NULL}"/>

                        </td>
                        <td>
                            <apex:outputField value="{!pd.Start_Date__c}"/>
                        </td>
                        <td>
                            <apex:outputField value="{!pd.End_Date__c}"/>
                        </td>
                        <td>
                            {!pd.Quantity__c} {!pd.Unit_Description__c}
                        </td>
                        <td>
                            <apex:outputText value="{0,number, currency}" >
                                <apex:param value="{!pd.Price_Total__c}" />
                            </apex:outputText> &nbsp;{!pd.Currency__c}
                        </td>
                    </tr>
                    <apex:repeat value="{!pd.paid_products__r}" var="rp">
                        <tr>
                            <td>
                                <b>{!rp.Product_Name__c}</b>
                            </td>
                            <td>
                                <apex:outputField value="{!rp.Start_Date__c}"/>
                            </td>
                            <td>
                                <apex:outputField value="{!rp.End_Date__c}"/>
                            </td>
                            <td>
                                {!rp.Quantity__c} {!rp.Unit_Description__c}
                            </td>
                            <td>
                                <apex:outputText value="{0,number, currency}" >
                                    <apex:param value="{!rp.Price_Total__c}" />
                                </apex:outputText> &nbsp;{!pd.Currency__c}
                            </td>
                        </tr>
                    </apex:repeat>
                    <tr style="display:{!IF(pd.client_course_instalment_payments__r.size >0, '','none')}">
                        <td colspan="3" style="padding-right:0px; padding-left:0px; ">
                            <div style="padding: 5px; margin:2px 2px 5px 2px; background-color:#f3f8f3; border-radius:5px;">

                                <!-- <span style="font-weight: 300; font-size: 1em; color: green;">Payment details</span> -->
                                <table class="paymentsTable" style="">
                                    <thead>
                                        <tr class="slds-line-height_reset">
                                            <th style="width:300px;"> {!$Label.Payment_Type} </th>
                                            
                                            <th> {!$Label.Payment_Date} </th>

                                            <th>{!$Label.Amount}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <apex:repeat value="{!pd.client_course_instalment_payments__r}" var="pay">
                                            <tr class="">
                                                <td >
                                                    {!pay.Payment_Type__c}
                                                </td>

                                                <td>
                                                    <apex:outputField value="{!pay.Date_Paid__c}" />
                                                </td>

                                                <td class="">
                                                    <apex:outputText value="{0,number, currency}" styleClass="price" >
                                                        <apex:param value="{!pay.Value__c}" />
                                                    </apex:outputText>
                                                </td>
                                            </tr>
                                        </apex:repeat>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                        <td colspan="2">
                        </td>

                    </tr>
                </tbody>
            </apex:repeat>

        </table>

	    <div style="clear:both;height:20px;"><br /></div>

        <div style="width:100%; text-align:right;font-size:1.4em !important;margin-top:30px;" class="labelInvoice">
            {!$Label.Balance_Due}&nbsp;
            <apex:outputtext value="({!invoiceCurrency})" rendered="{!NOT(ISNULL(invoiceCurrency))}"/>

            &nbsp;
            <apex:outputText value="{0, number, currency}" >
                <apex:param value="{!invoiceTotal - totalPaid}" />
            </apex:outputText>
        </div>

</div>




</apex:page>