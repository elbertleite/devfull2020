<apex:page controller="PaymentInvoiceController" showHeader="false" sideBar="false" renderAs="pdf" applyBodyTag="false" applyHtmlTag="false" language="{!language}">

<head>
	<style>
		@media print
			{
				body { font-family: Sans-serif; font-size: 0.7em; }

			}
			@page {
				 size: A4;
				 margin:0.15in;
				 margin-bottom: 0.3in;
				 border: 1px solid black;

				@bottom-center {
					//content: "{!$Label.Quotation}: No.: ";
					//font-size: 8pt;
				}
				@bottom-right {
					content: "Page " counter(page) "/" counter(pages);
					font-size: 8pt;
				}
			}

		.clientInvoice{
			padding:10px;
		}

		.contactDetails{
			display:block;
			width:100%;
			color:#999;
			font-size:16px;
		}

		.labelInvoice{
			font-weight: 900 !important;
			text-align: right !important;
			font-size: 1.1em !important;
		}

		.detailsTable{
			width:100%;
			border-spacing: 0px;
			line-height:15px;
		}

		.detailsTable th{
			border-bottom:1px solid black;
			font-size: 1.2em;
			text-align:center;
			font-weight: 900;
		}


		.detailsTable tr td{
			border-bottom:1px solid #ccc;
			padding:5px;
		}

		.detailsTable tr:last-child td{
			border-bottom:0px !important;
		}

		.greyFont{
			color:#666 !important;
		}

		.totalsCell{
			border-top:1px solid black;
		}



	</style>

</head>
	{!emailBody}
	<!-- Payment Invoice -->
	<!--{!language}-->
	
	<apex:outputpanel rendered="{!invoiceType=='payment'}" >
		<!-- INVOICE HEADER -->
		<div style="width:100%;display:block;">
			<apex:outputpanel layout="block" style="float:left; width:80%" rendered="{!agencyDetails.Logo__c!=null}" >
				<apex:image value="{!URLFOR(agencyDetails.Logo__c)}" height="60px" />
			</apex:outputpanel>
			<div style="float:left; width:20%; font-size:1em;">
				<apex:outputfield value="{!agencyDetails.Registration_name__c}" /> <br/>
				<apex:outputpanel layout="block" rendered="{!NOT(ISNULL(agencyDetails.Trading_Name__c))}" style="color:grey !important;"><apex:outputtext value="trading as {!agencyDetails.Trading_Name__c}" /></apex:outputpanel>
				<apex:outputfield value="{!agencyDetails.Name}" /> <br/>
				<apex:outputfield value="{!agencyDetails.BillingStreet}" /> <br/>
				<apex:outputfield value="{!agencyDetails.BillingCountry}" />,&nbsp;<apex:outputfield value="{!agencyDetails.BillingCity}" />,&nbsp;<apex:outputfield value="{!agencyDetails.BillingPostalCode}" /><br/>
				<apex:outputfield value="{!agencyDetails.Type_of_Business_Number__c}" />&nbsp;<apex:outputfield value="{!agencyDetails.Business_Number__c}" /><br/>
			</div>
		</div>
		<!-- END -- INVOICE HEADER -->

		<div style="clear:both;height:20px;"><br /></div>

		<!-- SCHOOL DETAILS -->
		<div style="float:left; width:40%; font-size:1em;margin-left:55px;">
			<apex:outputfield value="{!campusDetails.Registration_name__c}" /> <br/>
			<apex:repeat value="{!campusDetails.childAccounts}" var="ca">
				<apex:outputfield value="{!ca.ShippingStreet}" /> <br/>
				<apex:outputfield value="{!ca.ShippingCountry}" /> &nbsp;<apex:outputfield value="{!ca.ShippingCity}" />, &nbsp;<apex:outputfield value="{!ca.ShippingPostalCode}" /><br/>
			</apex:repeat>
			<apex:outputfield value="{!campusDetails.Type_of_Business_Number__c}" />&nbsp;<apex:outputfield value="{!campusDetails.Business_Number__c}" /><br/>
		</div>

		<div style="clear:both;height:10px;"><br /></div>
		<!-- END SCHOOL DETAILS -->


		<div style="color:black;font-weight:bold; width:100%; text-align:center;">
			<div style="width:100%; text-align:center;">
				<apex:outputpanel layout="block" rendered="{!AND(NOT(isInstAmendment), NOT(isCommissionAmend), NOT(isOverPaidAmend))}">
					<h2>TAX INVOICE</h2>
				</apex:outputpanel>
				<apex:outputpanel layout="block" rendered="{!OR(isInstAmendment, isCommissionAmend, isOverPaidAmend)}">
					<h2>TAX INVOICE - Amendment </h2>
				</apex:outputpanel>
			</div>
		</div>

		<div style="clear:both;height:20px;"><br /></div>

		<!-- <div style="float:left; width:80%; margin-left:20px;">
			<apex:outputtext value="{!campusDetails.Parent.Name} - {!campusDetails.Name} " />
		</div> -->

		<br/><br/>
		<!-- INVOICE INFORMATION -->
		<div style="float:left; width:50%; font-size:1em;">

			<table style="width:100%;border-spacing:5px;" >
				<tr>
					<td style="font-weight:bold;text-align:right;">{!$Label.Invoice_Number}</td>
					<td>
						<apex:outputtext value="{!instInvoice.School_Invoice_Number__c}" rendered="{!NOT(isInstAmendment)}"/>
						<apex:outputtext value="{!instInvoice.Original_Instalment__r.School_Invoice_Number__c}" rendered="{!isInstAmendment}"/>
					</td>
				</tr>

				<tr>
					<td style="font-weight:bold;text-align:right;">{!$Label.Issue_Date}</td>
					<td>
						<apex:outputText value="{0, date, dd/MM/YYYY}">
						    <apex:param value="{!now()}" />
						</apex:outputText>
					</td>
				</tr>

				<tr>
					<td style="font-weight:bold;text-align:right;">{!$ObjectType.Invoice__c.fields.Due_Date__c.label}</td>
					<td>
						<apex:outputtext value="Paid" rendered="{!NOT(instInvoice.isPFS__c)}" />

						<apex:outputText value="{0, date, dd/MM/YYYY}" rendered="{!instInvoice.isPFS__c}">
						    <apex:param value="{!instInvoice.Commission_Due_Date__c}" />
						</apex:outputText>

					</td>
				</tr>

				<tr>
					<td style="font-weight:bold;text-align:right;">Summary</td>
					<td>
						<apex:outputtext value="Commission for {!instInvoice.client_course__r.client__r.Name}" />
					</td>
				</tr>
			</table>

		</div>
		<!-- END INVOICE INFORMATION -->

		<div style="clear:both;height:20px;"><br /><br /></div>


		<!-- INVOICE DETAILS -->
		<div style="width:100%; border-bottom:1px solid black;text-align:left;padding:3px;">
			<table style="width:90%;font-weight:bold;">
				<tr>
					<td style="width:80%;padding-left:50px;">
						{!$Label.Description}
					</td>

					<td style="width:20%; text-align:center">
						{!$Label.Value} <br/> (Exc {!taxName})
					</td>
				</tr>
			</table>
		</div>

		<div style="clear:both;height:5px;"><br /><br /></div>

			<table style="width:100%">
				<tr>
					<td style="width:80%">
						<table style="border-spacing:5px;"  >
							<tr>
								<td style="text-align:right;">Student Name</td>
								<td>{!instInvoice.client_course__r.client__r.Name}</td>
							</tr>

							<tr>
								<td style="text-align:right;">Student DOB</td>
								<td><apex:outputfield value="{!instInvoice.client_course__r.client__r.Birthdate}" /></td>
							</tr>

							<tr>
								<td style="text-align:right;">Student Number</td>
								<td>
									{!instInvoice.client_course__r.School_Student_Number__c}
									<apex:outputpanel layout="none" rendered="{!ISNULL(instInvoice.client_course__r.School_Student_Number__c)}">-</apex:outputpanel>
								</td>
							</tr>

							<tr>
								<td style="text-align:right;">Course</td>
								<td>{!instInvoice.client_course__r.Course_Name__c}</td>
							</tr>

							<tr>
								<td style="text-align:right;">Start Date</td>
								<td><apex:outputfield value="{!instInvoice.client_course__r.Start_Date__c}" /> </td>
							</tr>

							<tr>
								<td style="text-align:right;">Instalment Number</td>
								<td>
									<apex:outputtext value="{!instInvoice.Number__c}" />
									<apex:outputText value="." rendered="{!NOT(ISNULL(instInvoice.Split_Number__c))}"/>
									<apex:outputText value="{!instInvoice.Split_Number__c}" rendered="{!NOT(ISNULL(instInvoice.Split_Number__c))}" />
								</td>
							</tr>

							<tr>
								<td style="text-align:right;">Tuition</td>
								<td>

									<apex:outputText value="{0, number, currency}" rendered="{!AND(NOT(isInstAmendment), NOT(isCommissionAmend), NOT(isOverPaidAmend)) || (isInstAmendment && instInvoice.Original_Instalment__r.isAmended_BeforeSchoolPayment__c)}">
										<apex:param value="{!instInvoice.Tuition_Value__c}" />
									</apex:outputText>

									<apex:outputText value="{0, number, currency}" rendered="{!(isInstAmendment || isCommissionAmend || isOverPaidAmend) && NOT(instInvoice.Original_Instalment__r.isAmended_BeforeSchoolPayment__c)}">
										<apex:param value="{!instInvoice.Tuition_Value__c + instInvoice.Original_Instalment__r.Tuition_Value__c}" />
									</apex:outputText>
								</td>
							</tr>

							<tr style="{!IF(NOT(ISNULL(allFees))&& allFees.size>0, '','display:none;')}">
								<td style="text-align:right;">Extra Fees</td>
								<td>
									<apex:repeat value="{!allFees}" var="f">
										<div style="display:block">
											<apex:outputText value="{0, number, currency}">
												<apex:param value="{!f.feeValue}" />
											</apex:outputText>
											&nbsp;<span>({!f.feeName})</span>
										</div>
									</apex:repeat>
								</td>
							</tr>
							<tr style="{!IF(NOT(ISNULL(lFees))&& lFees.size>0, '','display:none;')}">
								<td style="text-align:right;">Extra Fees Kept</td>
								<td>
									<apex:repeat value="{!lFees}" var="f">
										<div style="display:block">
											<apex:outputText value="{0, number, currency}">
												<apex:param value="{!f.keepFeeValue}" />
											</apex:outputText>
											&nbsp;<span>({!f.feeName})</span>
										</div>
									</apex:repeat>
								</td>
							</tr>

							<tr style="{!IF(instInvoice.Scholarship_Taken__c<1,'display:none', '')}">
								<td style="text-align:right;">Scholarship Taken</td>
								<td>
									<apex:outputText value="{0, number, currency}">
										<apex:param value="{!instInvoice.Scholarship_Taken__c}" />
									</apex:outputText>
								</td>
							</tr>

							<tr style="{!IF(instInvoice.Agent_Deal_Value__c<1,'display:none', '')}">
								<td style="text-align:right;">Agent Deal</td>
								<td>
									<apex:outputText value="{0, number, currency}">
										<apex:param value="{!instInvoice.Agent_Deal_Value__c}" />
									</apex:outputText>

									&nbsp;({!instInvoice.Agent_Deal_Name__c})
								</td>
							</tr>

							<tr>
								<td style="text-align:right;">Commission &nbsp;<apex:outputtext value="{!IF(instInvoice.client_course__r.Commission_Type__c==0, '%', '$')}" /> </td>
								<td>
									<apex:outputfield value="{!instInvoice.Commission__c}" />
								</td>
							</tr>



							<tr style="{!IF(schoolCredit<1,'display:none', '')}">
								<td style="text-align:right;">School Credit Used</td>
								<td>
									<apex:outputText value="{0, number, currency}">
										<apex:param value="{!schoolCredit}" />
									</apex:outputText>
								</td>
							</tr>


						</table>
					</td>

					<td style="width:20%;vertical-align:bottom;" >
						<apex:outputText value="{0, number, currency}" >
							<apex:param value="{!commissionValue}" />
						</apex:outputText>
					</td>
				</tr>
			</table>
			<div style="width:100%; border-bottom:1px solid black;text-align:left;padding:3px;"></div>

			<div style="width:50%; font-size:1em;float:right;margin-top:10px;">

				<table style="width:100%;border-spacing:5px;text-align:right;" >
					<tr>
						<td style="font-weight:bold;">Subtotal Exc Tax ({!taxName}) <apex:outputtext value=" ({!instInvoice.client_Course__r.CurrencyIsoCode__c})" /></td>
						<td>
							<apex:outputText value="{0, number, currency}" >
								<apex:param value="{!commissionValue}" />
							</apex:outputText>
						</td>
					</tr>

					<tr>
						<td style="font-weight:bold;">Plus Tax ({!taxName}) <apex:outputtext value=" ({!instInvoice.client_Course__r.CurrencyIsoCode__c})" /></td>
						<td>
							<apex:outputText value="{0, number, currency}" >
								<apex:param value="{!taxValue}" />
							</apex:outputText>
						</td>
					</tr>

					<tr style="{!IF(OR(totalKeepFee>0,originalKeepFee >0), '', 'display:none;')}">
						<td style="font-weight:bold;">Plus Keep Fee <apex:outputtext value=" ({!instInvoice.client_Course__r.CurrencyIsoCode__c})" /></td>
						<td>
							<apex:outputText value="{0, number, currency}" >
								<apex:param value="{!totalKeepFee + originalKeepFee}" />
							</apex:outputText>
						</td>
					</tr>

					<tr>
						<td style="font-weight:bold;">Total <apex:outputtext value=" ({!instInvoice.client_Course__r.CurrencyIsoCode__c})" /></td>
						<td>
							<apex:outputText value="{0, number, currency}" >
								<apex:param value="{!totalValue}" />
							</apex:outputText>
						</td>
					</tr>

					<tr>
						<td style="font-weight:bold;">{!$Label.Amount_Paid} <apex:outputtext value=" ({!instInvoice.client_Course__r.CurrencyIsoCode__c})" /></td>
						<td>
							<apex:outputText value="{0, number, currency}" >
								<apex:param value="{!amountPaid}" />
							</apex:outputText>
						</td>
					</tr>



					<!-- Tuition OverPaid -->
					<tr style="{!IF(AND(isOverPaidAmend, instInvoice.Amendment_Adjustment_Confirmed_On__c ==null ), '', 'display:none')}">
						<td style="font-weight:bold;border-top:1px solid black;font-size:1.1em;">Tuition Overpaid <apex:outputtext value=" ({!instInvoice.client_Course__r.CurrencyIsoCode__c})" /></td>
						<td style="border-top:1px solid black;font-size:1.1em;">
							<apex:outputText value="{0, number, currency}" >
								<apex:param value="{!ABS(instInvoice.Tuition_Value__c)}" />
							</apex:outputText>
						</td>
					</tr>


					<!-- Agency OverRetained Commission -->
					<tr style="{!IF(AND(isOverPaidAmend, instInvoice.Amendment_Adjustment_Confirmed_On__c ==null ), '', 'display:none')}">
						<td style="font-weight:bold;font-size:1.1em;">Over Retained Commission <apex:outputtext value=" ({!instInvoice.client_Course__r.CurrencyIsoCode__c})" /></td>
						<td style="font-size:1.1em;">
							<apex:outputText value="{0, number, currency}" rendered="{!NOT(instInvoice.isPFS__c)}">
								<apex:param value="{!amountPaid - totalValue}" />
							</apex:outputText>
						</td>
					</tr>


					<!-- Total Refund Agency -->
					<tr style="{!IF(AND(isOverPaidAmend, instInvoice.Amendment_Adjustment_Confirmed_On__c ==null ), '', 'display:none')}">
						<td style="font-weight:bold;font-size:1.1em;">Total Pending<apex:outputtext value=" ({!instInvoice.client_Course__r.CurrencyIsoCode__c})" /></td>
						<td style="font-size:1.1em;">
							<!--<apex:outputText value="{0, number, currency}" rendered="{!amountPaid=0}">
								<apex:param value="{!instInvoice.Amendment_Receive_From_School__c + totalValue}" />
							</apex:outputText>-->
							<apex:outputText value="{0, number, currency}">
								<apex:param value="{!instInvoice.Amendment_Receive_From_School__c}" />
							</apex:outputText>
						</td>
					</tr>



					<tr style="{!IF(AND(isOverPaidAmend, instInvoice.Amendment_Adjustment_Confirmed_On__c ==null), 'display:none', '')}">
						<td style="font-weight:bold;border-top:1px solid black;font-size:1.1em;">{!$Label.Balance_Due} <apex:outputtext value=" ({!instInvoice.client_Course__r.CurrencyIsoCode__c})" /></td>
						<td style="border-top:1px solid black;font-size:1.1em;">
							<apex:outputText value="{0, number, currency}" >
								<apex:param value="{!balanceDue}" />
							</apex:outputText>
						</td>
					</tr>
				</table>

			</div>

		<!-- END INVOICE DETAILS -->

		<div style="clear:both;height:20px;"><br /><br /></div>


		<!-- INFORMATION -->
		<apex:outputpanel layout="Block" style="margin-left:20px;" rendered="{!NOT(instInvoice.isPFS__c)}">
			<span style="font-weight:bold;">FOR YOUR INFORMATION ONLY:</span>
			<span>The total amount was debited from out payment for above mentioned student's tuition fee.</span>
			<div style="clear:both;height:20px;"><br /><br /></div>
		</apex:outputpanel>
		<!-- END -- INFORMATION -->


		<!-- BANK DETAILS -->
		<div style="margin-left:20px;">
			<span style="font-weight:bold;">{!$Label.Bank_Account_Details}</span>

			<table style="margin-top:10px;">
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.Bank__c.label}: </td>
					<td> {!agencyBankDetails.Bank} </td>
				</tr>
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.Branch_Address__c.label}: </td>
					<td> {!agencyBankDetails.Branch_Address} </td>
				</tr>
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.Account_Name__c.label}: </td>
					<td> {!agencyBankDetails.Account_Name} </td>
				</tr>
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.BSB__c.label}: </td>
					<td> {!agencyBankDetails.BSB} </td>
				</tr>
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.Account_Number__c.label}: </td>
					<td> {!agencyBankDetails.Account_Number} </td>
				</tr>
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.Swift_Code__c.label}: </td>
					<td> {!agencyBankDetails.Swift_Code} </td>
				</tr>
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.IBAN__c.label}: </td>
					<td> {!agencyBankDetails.IBAN} </td>
				</tr>

				<tr>
					<td style="text-align:right;"></td>
					<td></td>
				</tr>

				<tr>
					<td style="text-align:right;"> {!$ObjectType.Invoice__c.fields.CurrencyIsoCode__c.label}: </td>
					<td> {!instInvoice.client_Course__r.CurrencyIsoCode__c} </td>
				</tr>

				<tr>
					<td style="text-align:right;"> {!$Label.Payment_Reference}: </td>
					<td> {!instInvoice.School_Invoice_Number__c} </td>
				</tr>
			</table>
			
		</div>
		<!-- END -- BANK DETAILS -->




	</apex:outputpanel>
	<!-- END -- Payment Invoice -->


	<!-- PDS Invoice -->
	<apex:outputpanel rendered="{!invoiceType=='pdsInvoice'}" >
		<!-- INVOICE HEADER -->
		<div style="width:100%;display:block;">
			<apex:outputpanel layout="block" style="float:left; width:80%" rendered="{!agencyDetails.Logo__c!=null}" >
				<apex:image value="{!URLFOR(agencyDetails.Logo__c)}" height="60px" />
			</apex:outputpanel>
			<div style="float:left; width:20%; font-size:1em;">
				<apex:outputfield value="{!agencyDetails.Registration_name__c}" /> <br/>
				<apex:outputpanel layout="block" rendered="{!NOT(ISNULL(agencyDetails.Trading_Name__c))}" style="color:grey !important;"><apex:outputtext value="trading as {!agencyDetails.Trading_Name__c}" /></apex:outputpanel>
				<apex:outputfield value="{!agencyDetails.Name}" /> <br/>
				<apex:outputfield value="{!agencyDetails.BillingStreet}" /> <br/>
				<apex:outputfield value="{!agencyDetails.BillingCountry}" />,&nbsp;<apex:outputfield value="{!agencyDetails.BillingCity}" />,&nbsp;<apex:outputfield value="{!agencyDetails.BillingPostalCode}" /><br/>
				<apex:outputfield value="{!agencyDetails.Type_of_Business_Number__c}" />&nbsp;<apex:outputfield value="{!agencyDetails.Business_Number__c}" /><br/>
			</div>
		</div>
		<!-- END -- INVOICE HEADER -->

		<div style="clear:both;height:20px;"><br /></div>

		<!-- SCHOOL DETAILS -->
		<div style="float:left; width:40%; font-size:1em;margin-left:55px;">
			<apex:outputfield value="{!campusDetails.Registration_name__c}" /> <br/>
			<apex:repeat value="{!campusDetails.childAccounts}" var="ca">
				<apex:outputfield value="{!ca.ShippingStreet}" /> <br/>
				<apex:outputfield value="{!ca.ShippingCountry}" /> &nbsp;<apex:outputfield value="{!ca.ShippingCity}" />, &nbsp;<apex:outputfield value="{!ca.ShippingPostalCode}" /><br/>
			</apex:repeat>
			<apex:outputfield value="{!campusDetails.Type_of_Business_Number__c}" />&nbsp;<apex:outputfield value="{!campusDetails.Business_Number__c}" /><br/>
		</div>

		<div style="clear:both;height:10px;"><br /></div>
		<!-- END SCHOOL DETAILS -->


		<div style="color:black;font-weight:bold; width:100%; text-align:center;">
			<div style="width:100%; text-align:center;">
				<h2>TAX INVOICE</h2>
			</div>
		</div>

		<div style="clear:both;height:20px;"><br /></div>


		<!-- INVOICE INFORMATION -->
		<div style="float:left; width:50%; font-size:1em;">

			<table style="width:100%;border-spacing:5px;" >
				<tr>
					<td style="font-weight:bold;text-align:right;">{!$Label.Invoice_Number}</td>
					<td>{!groupInvoice.School_Invoice_Number__c}</td>
				</tr>

				<tr>
					<td style="font-weight:bold;text-align:right;">{!$Label.Issue_Date}</td>
					<td>
						<apex:outputText value="{0, date, dd/MM/YYYY}">
						    <apex:param value="{!now()}" />
						</apex:outputText>
					</td>
				</tr>

				<tr>
					<td style="font-weight:bold;text-align:right;">{!$ObjectType.Invoice__c.fields.Due_Date__c.label}</td>
					<td>
						<apex:outputtext value="{!dueDate}" rendered="{!NOT(ISNULL(dueDate)) && NOT(ISBLANK(dueDate))}" />
					</td>
				</tr>

				<tr>
					<td style="font-weight:bold;text-align:right;">Summary</td>
					<td>
						<apex:outputtext value="Commission - Information Planet" />
					</td>
				</tr>
			</table>

		</div>
		<!-- END INVOICE INFORMATION -->

		<div style="clear:both;height:20px;"><br /></div>


		<apex:variable value="{!0}" var="subtotal" />
		<apex:variable value="{!0}" var="subtotalPlus" />
		<apex:variable value="{!0}" var="Total" />

		<!-- INVOICE DETAIS -->
		<table class="massInvoice" style="border-spacing:0px;width:100%;padding:5px;">

			<tr>
				<td style="border-bottom:1px solid black;width:80%;"> <b>{!$Label.Description}</b> </td>
				<td style="text-align:right;border-bottom:1px solid black;width:20%;"> <b>Value</b> </td>
			</tr>
				<apex:repeat value="{!massInvoice}" var="i">
					<tr>
						<td style="width:70%;border-bottom:1px solid black;">

							<table style="border-spacing:0px; width:100%;">
								<tr>
									<td style="text-align:right;width:30%;padding-right:10px;">Student Name</td>
									<td><apex:outputtext value="{!i.client_course__r.client__r.Name}"/></td>
								</tr>

								<tr>
									<td style="text-align:right;width:30%;padding-right:10px;">Student DOB</td>
									<td><apex:outputfield value="{!i.client_course__r.client__r.Birthdate}" /></td>
								</tr>

								<tr>
									<td style="text-align:right;width:30%;padding-right:10px;">Student Number</td>
									<td><apex:outputfield value="{!i.client_course__r.School_Student_Number__c}" /></td>
								</tr>

								<tr>
									<td style="text-align:right;width:30%;padding-right:10px;">Course</td>
									<td><apex:outputtext value="{!i.client_course__r.Course_Name__c}" /></td>
								</tr>

								<tr>
									<td style="text-align:right;width:30%;padding-right:10px;">Instalment Number</td>
									<td>
										<apex:outputtext value="{!i.Number__c}"/>
										<apex:outputText value="." rendered="{!NOT(ISNULL(i.Split_Number__c))}" />
    									<apex:outputText value="{!i.Split_Number__c}" rendered="{!NOT(ISNULL(i.Split_Number__c))}" />
									</td>
								</tr>

								<tr>
									<td style="text-align:right;width:30%;padding-right:10px;">Instalment {!$ObjectType.Invoice__c.fields.Due_Date__c.label}</td>
									<td><apex:outputfield value="{!i.Due_Date__c}" /></td>
								</tr>

								<tr>
									<td style="text-align:right;width:30%;padding-right:10px;">Tuition (Instalment value)</td>
									<td>
										<apex:outputText value="{0, number, currency}" >
											<apex:param value="{!i.Tuition_Value__c}" />
										</apex:outputText>
									</td>
								</tr>

								<tr>
									<td style="text-align:right;width:30%;padding-right:10px;">Commission <apex:outputText value="{!IF(i.client_course__r.Commission_Type__c==1,'Cash','%')}" />	</td>
									<td>
										<apex:outputfield value="{!i.Commission__c}" />
									</td>
								</tr>

								<tr>
									<td style="text-align:right;width:30%;padding-right:10px;">

										{!i.client_course__r.Commission_Tax_Name__c}


									</td>
									<td><apex:outputfield value="{!i.Commission_Tax_Rate__c}" /></td>
								</tr>
							</table>
						</td>


						<td style="vertical-align:bottom;width:30%; text-align:right;border-bottom:1px solid black;">
							<div style="width:60%; text-align:right;">
								<apex:outputText value="{0, number, currency}">
									<apex:param value="{!i.Commission_Value__c}" />
								</apex:outputText>
							</div>
							<div style="width:60%; text-align:right;">
								<apex:outputText value="{0, number, currency}">
									<apex:param value="{!i.Commission_Tax_Value__c}" />
								</apex:outputText>
							</div>
						</td>
					</tr>

					<apex:variable value="{!subtotal + i.Commission_Value__c}" var="subtotal" />
					<apex:variable value="{!subtotalPlus + i.Commission_Tax_Value__c}" var="subtotalPlus" />
					<apex:variable value="{!Total + i.Commission_Value__c + i.Commission_Tax_Value__c}" var="Total" />
				</apex:repeat>

			<tr>
				<td style="width:70%; text-align:right;font-weight:bold;">
					Subtotal Exc. Tax ({!taxName})
				</td>

				<td style="width:30%;text-align:right;">
					<div style="width:60%; text-align:right;">
						<apex:outputText value="{0, number, currency}" style="font-weight:bold">
							<apex:param value="{!subtotal}" />
						</apex:outputText>
					</div>
				</td>
			</tr>

			<tr>
				<td style="width:70%; text-align:right;font-weight:bold;">
					Plus Tax ({!taxName})
				</td>

				<td style="width:30%;text-align:right;">
					<div style="width:60%; text-align:right;">
						<apex:outputText value="{0, number, currency}" style="font-weight:bold">
							<apex:param value="{!subtotalPlus}" />
						</apex:outputText>
					</div>
				</td>
			</tr>

			<tr>
				<td style="width:70%; text-align:right;font-weight:bold;font-size:1.2em;">
					{!$Label.Balance_Due}
				</td>

				<td style="width:30%;font-size:1.2em;text-align:right;border-top:1.5px solid black;">
					<div style="width:60%; text-align:right;">
						<apex:outputText value="{0, number, currency}" style="font-weight:bold">
							<apex:param value="{!Total}" />
						</apex:outputText>
					</div>
				</td>
			</tr>
		</table>

		<!-- END -- INVOICE DETAIS -->
		<div style="clear:both;height:20px;"><br /></div>
		<!-- BANK DETAILS -->
		<div style="margin-left:20px;">
			<span style="font-weight:bold;">{!$Label.Bank_Account_Details}</span>

			<table style="margin-top:10px;">
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.Bank__c.label}: </td>
					<td> {!agencyBankDetails.Bank} </td>
				</tr>
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.Branch_Address__c.label}: </td>
					<td> {!agencyBankDetails.Branch_Address} </td>
				</tr>
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.Account_Name__c.label}: </td>
					<td> {!agencyBankDetails.Account_Name} </td>
				</tr>
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.BSB__c.label}: </td>
					<td> {!agencyBankDetails.BSB} </td>
				</tr>
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.Account_Number__c.label}: </td>
					<td> {!agencyBankDetails.Account_Number} </td>
				</tr>
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.Swift_Code__c.label}: </td>
					<td> {!agencyBankDetails.Swift_Code} </td>
				</tr>
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.IBAN__c.label}: </td>
					<td> {!agencyBankDetails.IBAN} </td>
				</tr>

				<tr>
					<td style="text-align:right;"></td>
					<td></td>
				</tr>

				<!-- <tr>
					<td style="text-align:right;"> {!$ObjectType.Invoice__c.fields.CurrencyIsoCode__c.label}: </td>
					<td> {!instInvoice.client_Course__r.CurrencyIsoCode__c} </td>
				</tr>-->

				<tr>
					<td style="text-align:right;"> {!$Label.Payment_Reference}: </td>
					<td> {!groupInvoice.School_Invoice_Number__c} </td>
				</tr>
			</table>
		</div>
		<!-- END -- BANK DETAILS -->

	</apex:outputpanel>

	<!-- END -- PDS Invoice -->



	<!-- Client Invoice -->
	<apex:outputpanel layout="block" styleclass="clientInvoice" rendered="{!invoiceType=='client'}" >

		<!-- INVOICE HEADER -->
		<div style="width:100%;display:block;">
			<apex:outputpanel layout="block" style="float:left; width:80%" rendered="{!agencyDetails.Logo__c!=null}" >
				<apex:image value="{!URLFOR(agencyDetails.Logo__c)}" height="60px" />
			</apex:outputpanel>
			<div style="float:left; width:20%; font-size:1em;">
				<apex:outputfield value="{!agencyDetails.Name}" /> <br/>
				<apex:outputfield value="{!agencyDetails.BillingStreet}" /> <br/>
				<apex:outputfield value="{!agencyDetails.BillingCountry}" />,&nbsp;<apex:outputfield value="{!agencyDetails.BillingCity}" />, &nbsp;<apex:outputfield value="{!agencyDetails.BillingPostalCode}" /><br/>
			</div>
		</div>
		<!-- END -- INVOICE HEADER -->


		<div style="clear:both;height:20px;"><br /></div>

		<!-- CLIENT DETAILS
		<div class="contactDetails">
			To:&nbsp;
			<apex:outputtext value="{!clientName}" />
		</div>
		<!-- END -- CLIENT DETAILS -->


		<div style="clear:both;height:20px;"><br /></div>

		<div style="color:black;font-weight:bold;text-align:center;">
			<div style="width:100%">
				<h2>{!$Label.Tax_Invoice}</h2>
			</div>
		</div>

		<div style="clear:both;height:20px;"><br /></div>

		<!-- {!$Label.Invoice_Number} -->
		<div>
			<table style="width:400px;" >
				<tr>
					<td class="labelInvoice" style="width:40%">{!$Label.Client_Name}</td>
					<td style="width:60%">
						<apex:outputtext value="{!clientName}" />
					</td>
				</tr>
				<tr>
					<td class="labelInvoice" style="width:40%">{!$Label.Invoice_Number}</td>
					<td style="width:60%">
						<apex:outputtext value="{!invoiceNumber}" />
					</td>
				</tr>

				<tr>
					<td class="labelInvoice" style="width:40%">{!$Label.Issue_Date}</td>
					<td style="width:60%">
						<apex:outputText value="{0, date, dd/MM/YYYY}">
						    <apex:param value="{!Today()}" />
						</apex:outputText>
					</td>
				</tr>
				<apex:outputpanel layout="none" rendered="{!NOT(ISNULL(invoiceDueDate))}">
					<tr>
						<td class="labelInvoice">{!$ObjectType.Invoice__c.fields.Due_Date__c.label}</td>
						<td>
							<apex:outputText value="{0, date, dd/MM/YYYY}">
							    <apex:param value="{!invoiceDueDate}" />
							</apex:outputText>
						</td>
					</tr>
				</apex:outputpanel>
			</table>
		</div>
		<!-- END -- {!$Label.Invoice_Number} -->

		<apex:outputpanel layout="none" >
			<div style="clear:both;height:40px;"><br /></div>

			<!-- <div style="color:black;font-weight:bold">
				<div style="width:100%">
					<h2>COURSE FEES</h2>
				</div>
			</div>
			 -->
			<div style="clear:both;height:10px;"><br /></div>
			<table class="detailsTable" >
				<tr>
					<th>{!$Label.Description}</th>
					<th>{!$ObjectType.client_course_instalment_payment__c.fields.client_course_instalment__c.label} Nº</th>
					<th>{!$ObjectType.client_product_service__c.fields.Quantity__c.label}</th>
					<th>{!$Label.Price_per_Unit}</th>
					<th style="text-align:right;">{!$Label.Value} <br/> <span style="font-size:0.9em;">
						<apex:outputpanel layout="none" rendered="{!NOT(ISNULL(taxName))}">
							({!$Label.Exclude_short} {!taxName}) 
						</apex:outputpanel>
					</span></th>
				</tr>

				<apex:repeat value="{!lInstInvoice}" var="i">
					<tr>
						<td style="width:50%;">
							<span class="greyFont"><apex:outputfield value="{!i.client_course__r.Campus_Name__c}"   style="font-weight:bold;"/></span> -&nbsp;<apex:outputfield value="{!i.client_course__r.Course_Name__c}" />
						</td>
						<td style="text-align:center;width:10%;">
							<apex:outputText value="{!i.Number__c}" />
							<apex:outputText value="." rendered="{!NOT(ISNULL(i.Split_Number__c))}"/>
							<apex:outputText value="{!i.Split_Number__c}" rendered="{!NOT(ISNULL(i.Split_Number__c))}" />
						</td>
						<td style="text-align:center;width:15%;">
							<span class="greyFont">-</span>
						</td>
						<td style="text-align:center;width:15%;">
							<span class="greyFont">-</span>
						</td>
						<td style="text-align:right;width:20%;">
							<apex:outputText value="{0, number, currency}" >
								<apex:param value="{!i.Tuition_Value__c}" />
							</apex:outputText>

							<apex:outputpanel layout="block" style="color:green !important;font-size:0.8em !important;" rendered="{!AND(NOT(ISNULL(i.Client_Scholarship__c)), i.Client_Scholarship__c>0, NOT(ISNULL($CurrentPage.parameters.inv)))}">
								<apex:outputText value="{0, number, currency}" >
									<apex:param value="{!i.Client_Scholarship__c}" />
								</apex:outputText>
								Client Scholarship
							</apex:outputpanel>
						</td>
					</tr>
				</apex:repeat>

				<apex:repeat value="{!lFees}" var="fee">
					<tr>
						<td style="width:50%;">
							<apex:outputtext value="{!fee.feeName}" />
						</td>
						<td style="text-align:center;width:15%;">
							<span class="greyFont">-</span>
						</td>
						<td style="text-align:center;width:15%;">
							<span class="greyFont">-</span>
						</td>
						<td style="text-align:center;width:15%;">
							<span class="greyFont">-</span>
						</td>
						<td style="text-align:right;width:20%;">
							<apex:outputText value="{0, number, currency}" >
								<apex:param value="{!fee.feeValue}" />
							</apex:outputText>
						</td>
					</tr>
				</apex:repeat>

				<apex:repeat value="{!prodInvoice}"	var="pd">
					<tr>
						<td style="width:25%;">
							<apex:outputfield value="{!pd.Product_Name__c}" />
						</td>
						<!-- <td style="width:25%;">
							<apex:outputfield value="{!pd.Category__c}" />
						</td> -->
						<td style="width:25%;text-align:center;">
							-
						</td>
						
						<td style="width:25%;text-align:center;">
							<apex:outputfield value="{!pd.Quantity__c}" />
							&nbsp;{!pd.Unit_Description__c} (s)
						</td>
						<td style="width:25%;text-align:center;">
							-
						</td>
						<td style="text-align:right;width:20%;">
							<apex:outputText value="{0, number, currency}" >
								<apex:param value="{!pd.Price_Total__c}" />
							</apex:outputText>&nbsp;{!invoiceCurrency}
						</td>
					</tr>
					<apex:repeat value="{!mProdFees[pd.Id]}" var="rp">
						<tr>
							<td style="width:25%;">
								<apex:outputfield value="{!rp.Product_Name__c}" />
							</td>
							<!-- <td style="width:25%;">
								<apex:outputfield value="{!rp.Category__c}" />
							</td> -->
							<td style="width:25%;text-align:center;">
								-
							</td>
							
							<td style="width:25%;text-align:center;">
								<apex:outputfield value="{!rp.Quantity__c}" />
								&nbsp;{!rp.Unit_Description__c} (s)
							</td>
							<td style="width:25%;text-align:center;">
								-
							</td>
							<td style="text-align:right;width:20%;">
								<apex:outputText value="{0, number, currency}" >
									<apex:param value="{!rp.Price_Total__c}" />
								</apex:outputText>&nbsp;{!invoiceCurrency}
							</td>
						</tr>
					</apex:repeat>
				</apex:repeat>
				
			</table>
		</apex:outputpanel>

		<div style="clear:both;height:20px;"><br /></div>

		<!-- TOTAL -->
		<table style="width:100%;border-top:1px solid black;margin-top:20px;" >
			<tr>
				<td style="width:50%;"></td>
				<td style="width:50%;">
					<table style="width:300px;border-spacing:0px;float:right;" >
						<tr>
							<td class="labelInvoice">{!$ObjectType.Invoice__c.fields.Total_Value__c.label} &nbsp;
								<apex:outputtext value="({!invoiceCurrency})" rendered="{!NOT(ISNULL(invoiceCurrency))}"/>
							</td>
							<td style="text-align:right;">
								<apex:outputText value="{0, number, currency}">
									<apex:param value="{!invoiceTotal}" />
								</apex:outputText>
							</td>
						</tr>


						<tr style="{!IF(AND(NOT(ISNULL(clientScholarship)),clientScholarship>0),'','display:none;')}">
							<td class="labelInvoice" style="padding-top:5px;">Client Scholarship &nbsp;
								<apex:outputtext value="({!invoiceCurrency})" rendered="{!NOT(ISNULL(invoiceCurrency))}"/>
							</td>
							<td style="text-align:right;padding-top:5px;">
								<apex:outputText value="{0, number, currency}">
									<apex:param value="{!clientScholarship}" />
								</apex:outputText>
							</td>
						</tr>



						<tr>
							<td class="labelInvoice" style="padding-top:5px;">{!$Label.Amount_Paid} &nbsp;
								<apex:outputtext value="({!invoiceCurrency})" rendered="{!NOT(ISNULL(invoiceCurrency))}"/>
							</td>
							<td style="text-align:right;padding-top:5px;">
								<apex:outputText value="{0, number, currency}">
									<apex:param value="{!totalReceived}" />
								</apex:outputText>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td colspan="2"> &nbsp;</td></tr>
			<tr><td colspan="2"> &nbsp;</td></tr>
			<tr>
				<td style="width:50%;"></td>
				<td style="width:50%;">
					<table style="width:300px;border-spacing:0px;float:right;border-top:1px solid black;">
						<tr>
							<td class="labelInvoice" style="font-size:1.3em !important;">
								{!$Label.Balance_Due}  &nbsp;
								<apex:outputtext value="({!invoiceCurrency})" rendered="{!NOT(ISNULL(invoiceCurrency))}"/>
							</td>
							<td class="" style="text-align:right;font-size:1.3em !important;">
								<apex:outputText value="{0, number, currency}" >
									<apex:param value="{! invoiceTotal - totalReceived - clientScholarship}" />
								</apex:outputText>
							</td>

						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- END -- TOTAL -->

		<!-- BANK DETAILS -->
		<div style="margin-left:20px;margin-top:10px;">
			<span style="font-weight:bold;">{!$Label.Bank_Account_Details}</span>
			<table style="margin-top:10px;">
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.Bank__c.label}: </td>
					<td> {!agencyDetails.Bank_Client_Invoice__r.Bank__c} </td>
				</tr>
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.Branch_Address__c.label}: </td>
					<td> {!agencyDetails.Bank_Client_Invoice__r.Branch_Address__c} </td>
				</tr>
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.Account_Name__c.label}: </td>
					<td> {!agencyDetails.Bank_Client_Invoice__r.Account_Name__c} </td>
				</tr>
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.BSB__c.label}: </td>
					<td> {!agencyDetails.Bank_Client_Invoice__r.BSB__c} </td>
				</tr>
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.Account_Number__c.label}: </td>
					<td> {!agencyDetails.Bank_Client_Invoice__r.Account_Number__c} </td>
				</tr>
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.Swift_Code__c.label}: </td>
					<td> {!agencyDetails.Bank_Client_Invoice__r.Swift_Code__c} </td>
				</tr>
				<tr>
					<td style="text-align:right;"> {!$ObjectType.Bank_Detail__c.fields.IBAN__c.label}: </td>
					<td> {!agencyDetails.Bank_Client_Invoice__r.IBAN__c} </td>
				</tr>

				<tr>
					<td style="text-align:right;"></td>
					<td></td>
				</tr>

				<tr>
					<td style="text-align:right;"> {!$ObjectType.Invoice__c.fields.CurrencyIsoCode__c.label}: </td>
					<td> {!invoiceCurrency} </td>
				</tr>

				<tr>
					<td style="text-align:right;"> {!$Label.Payment_Reference}: </td>
					<td> {!invoiceNumber} </td>
				</tr>
			</table>


		</div>

		<!-- END -- BANK DETAILS -->

		<div style="margin-left:20px;margin-top:10px;">
			<b>{!$Label.Note}:</b> {!$Label.Client_Invoice_Note}
		</div>


	</apex:outputpanel>




	<!-- END -- Client Invoice -->
</apex:page>