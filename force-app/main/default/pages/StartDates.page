<apex:page controller="StartDates" showHeader="false" sidebar="false" >
	<apex:include pageName="headerInclude" />
	<style>
		.titleHeader {
		    background: url("/resource/1367818070000/icons/icons/home_page/gradientBlue.png") repeat scroll 0 0 transparent;
		    display: block;
			font-weight: bold;
		    height: 17px;
		   	color: white;
		    padding: 3px;
		}
		
		.dateFormat {
			display: none;
		}
		
		.apexp {
			padding: 5px 10px;			
		}
		.newRange {
			border: 1px solid lightgrey;
			border-radius: 5px;
		}
		
		.relatedFees th {
			background: url("/resource/1367818070000/icons/icons/home_page/gradientBlue.png") repeat scroll 0 -63px transparent;
			font-weight: bold;		
		}
		
		.relatedFees {
			font-size: 0.8em;
			border: 1px solid grey;
			width: 100%;
		}
		
		.relatedFees tr {
			line-height: 23px;
		}
		
		table.relatedFees tr:nth-child(odd) { background-color:#EEEEEE; }
		table.relatedFees tr:nth-child(even) { background-color: white; }
		
		
		table.results tr:nth-child(odd) { background-color:#EEEEEE; }
		table.results tr:nth-child(even) { background-color: white; }
		
		.results {
			border: 1px solid lightgrey;
			border-radius: 2px;
		}
		
		.feeInfo {
			font-size: 0.8em;
			padding-left: 2em;
		}
		
		.buttons {
			float:right;
			padding-right: 13px;
		}
		.fileRow {
			line-height: 15px !important;
		}
		.expired {
			color:red;
		}
	</style>
	
	<script>
		$ = jQuery.noConflict();
		function selectAll(checked, styleClass){
			if(checked)
				$('.'+styleClass).attr('checked', 'true');
			else 
				$('.'+styleClass).removeAttr('checked');
		}
	</script>
	
	<apex:form >
		<div class="titleHeader">
			<apex:outputText value="Add New Start Dates"  />
		</div>
		
		<div>
			
			<apex:outputPanel layout="block" rendered="{!type == EXTRA_FEE || type == RELATED_FEE}">
				<apex:outputText value="{!extraFee.Product__r.Name__c}" style="color:#191970; font-size: 1.5em; padding: 10px 10px 2px 10px; display:block;" />
				<apex:outputText value="{!extraFee.Nationality__c}" styleClass="feeInfo" />
				<apex:outputText value="From (Unit): {!extraFee.From__c}" styleClass="feeInfo" />
				<apex:outputText value="Value: ${!extraFee.Value__c}" styleClass="feeInfo" />
				<apex:outputText value="Pay Date From: {!extraFee.date_paid_from__c}" styleClass="feeInfo" />
				<apex:outputText value="Pay Date To: {!extraFee.date_paid_to__c}" styleClass="feeInfo {!IF(NOT(ISNULL(extraFee.date_paid_to__c)) && extraFee.date_paid_to__c < TODAY(), 'expired', '')}"  />
			</apex:outputPanel>
			
			<apex:outputPanel style="padding: 15px 30px 5px 30px; display:block;" rendered="{!type == RELATED_FEE}">
				
				<table class="relatedFees" cellspacing="0">
					
					<tr style="line-height: 20px;">
						<th width="3%"> <apex:inputCheckbox title="Select All" onclick="selectAll(this.checked, 'relatedCheck');" /> </th>
						<th>Related Fee</th>
						<th width="15%">From (Unit)</th>
						<th width="15%">Value</th>
						<th width="15%">Pay Date From</th>
						<th width="15%">Pay Date To</th>
					</tr>							
					
					<tbody>
						<apex:repeat value="{!extraFee.Course_Extra_Fees_Dependent__r}" var="dep">
							<tr>
								<td>
									<apex:inputField value="{!dep.Selected__c}" styleClass="relatedCheck" />
								</td>
								<td>
									<apex:outputField value="{!dep.Product__r.Name__c}" />
								</td>
								<td>
									<apex:outputField value="{!dep.From__c}" />
								</td>
								<td>
									$<apex:outputField value="{!dep.Value__c}" />
								</td>
								<td>
									<apex:outputField value="{!dep.Date_Paid_From__c}" />
								</td>
								<td>
									<span class="{!IF(NOT(ISNULL(dep.Date_Paid_To__c)) && dep.Date_Paid_To__c < TODAY(), 'expired', '')}" >
										<apex:outputField value="{!dep.Date_Paid_To__c}" />
									</span>
								</td>
							</tr>
						</apex:repeat>
					</tbody>
				</table>
			</apex:outputPanel>
			
			
			
			<apex:outputPanel layout="block" rendered="{!type == PROMOTION}">
				<apex:outputText value="{!deal.Promotion_Name__c}" style="color:#191970; font-size: 1.5em; padding: 10px 10px 2px 10px; display:block;" />
				<apex:outputText value="Extra Fee: {!deal.Product__r.Name__c}" styleClass="feeInfo" rendered="{!deal.Promotion_Type__c == PROMOTION_EXTRA_FEE_DISCOUNT}" />
				<apex:outputText value="Nationality: {!deal.Nationality_Group__c}" styleClass="feeInfo" />				
				<apex:outputText value="Buy: {!deal.Promotion_Weeks__c}" styleClass="feeInfo" rendered="{!deal.Promotion_Type__c == PROMOTION_FREE_UNITS}" />
				<apex:outputText value="Get Free: {!deal.Number_of_Free_Weeks__c}" styleClass="feeInfo" rendered="{!deal.Promotion_Type__c == PROMOTION_FREE_UNITS}" />
				<apex:outputText value="From: {!deal.From__c}" styleClass="feeInfo" rendered="{!deal.Promotion_Type__c == PROMOTION_EXTRA_FEE_DISCOUNT}" />
				<apex:outputText value="Value: ${!deal.Extra_Fee_Value__c}" styleClass="feeInfo" rendered="{!deal.Promotion_Type__c == PROMOTION_EXTRA_FEE_DISCOUNT && deal.Extra_Fee_Type__c = 'Cash'}" />
				<apex:outputText value="Value: {!deal.Extra_Fee_Value__c}%" styleClass="feeInfo" rendered="{!deal.Promotion_Type__c == PROMOTION_EXTRA_FEE_DISCOUNT && deal.Extra_Fee_Type__c = 'Percentage'}" />
				
				<apex:outputText value="Pay Date From: {!deal.From_Date__c}" styleClass="feeInfo" />
				<apex:outputText value="Pay Date To: {!deal.To_Date__c}" styleClass="feeInfo {!IF(NOT(ISNULL(deal.To_Date__c)) && deal.To_Date__c < TODAY(), 'expired', '')}"/>
			</apex:outputPanel>
			
			
			<apex:outputPanel layout="block" rendered="{!type == COURSE_PRICE}">
				<apex:outputText value="{!coursePrice.Nationality__c}" style="color:#191970; font-size: 1.5em; padding: 10px 10px 2px 10px; display:block;" />
				<apex:outputText value="From: {!coursePrice.From__c} {!coursePrice.Campus_Course__r.Course__r.Course_Unit_Type__c}s" styleClass="feeInfo"  />
				<apex:outputText value="Value: ${!coursePrice.Price_per_week__c}" styleClass="feeInfo" />
				<apex:outputText value="Pay Date From: {!coursePrice.Price_valid_from__c}" styleClass="feeInfo" />
				<apex:outputText value="Pay Date To: {!coursePrice.Price_valid_until__c}" styleClass="feeInfo {!IF(NOT(ISNULL(coursePrice.Price_valid_until__c)) && coursePrice.Price_valid_until__c < TODAY(), 'expired', '')}"/>
			</apex:outputPanel>
			
			
		</div>
				
		<apex:pageBlock id="newRange" >	
			<div class="newRange">
				<table style="width:100%; padding: 5px;">
					
					<apex:outputPanel rendered="{!deal.Promotion_Type__c == PROMOTION_EXTRA_FEE_DISCOUNT}" layout="none" >
						<tr>
							<td colspan="4">
								<div class="SearchInput">
									<div class="requiredInput">
										<div class="requiredBlock"></div>
										<apex:inputField value="{!newStartDate.Promotion_Name__c}" style="width: 350px;" />
									</div>
								</div>
								 <div class="SearchFooter">
									<span class="SearchLabel">Promotion Name</span>
								</div>
							</td>
						</tr>
					</apex:outputPanel>
					
					<tr>							
						<td width="150px;padding-left:10px;">
							<div class="SearchInput">
								<div class="requiredInput">
									<div class="requiredBlock"></div>
									<apex:inputField value="{!newStartDate.From_Date__c}" />
								</div>
							</div>
							 <div class="SearchFooter">
								<span class="SearchLabel">From Date</span>
							</div>
						</td>
						<td width="150px;">
							<div class="SearchInput">
								<div class="requiredInput">
									<div class="requiredBlock"></div>
									<apex:inputField value="{!newStartDate.To_Date__c}" />
								</div>
							</div>
							 <div class="SearchFooter">
								<span class="SearchLabel">To Date</span>
							</div>
						</td>							
						<td width="150px;" style="{!IF(type != 'promotion', '', 'display:none;')}" >
							<div class="SearchInput">
								<div class="requiredInput">
									<div class="requiredBlock"></div>
									<apex:inputField value="{!newStartDate.Value__c}" style="width:68px;"  />
								</div>
							</div>
							 <div class="SearchFooter">
								<span class="SearchLabel">Value</span>
							</div>
						</td>
						
						<apex:outputPanel rendered="{!deal.Promotion_Type__c == PROMOTION_EXTRA_FEE_DISCOUNT}" layout="none" >
										
							<td width="150px;">
								<div class="SearchInput">
									<div class="requiredInput">
										<div class="requiredBlock"></div>
											<input type="text" class="promotionType" value="{!deal.Extra_Fee_Type__c}" disabled="disabled" style="width:6em;" />
											<script>
												jQuery('.promotionType').val(selectedType);
											</script>
									</div>
								</div>
								 <div class="SearchFooter">
									<span class="SearchLabel">Promotion Type</span>
								</div>
							</td>
							
							<td width="150px;">
								<div class="SearchInput">
									<div class="requiredInput">
										<div class="requiredBlock"></div>
											<apex:inputField value="{!newStartDate.Value__c}" style="width:6em;"  />
									</div>
								</div>
								 <div class="SearchFooter">
									<span class="SearchLabel">Value</span>
								</div>
							</td>
							
						</apex:outputPanel>
												
						<apex:outputPanel rendered="{!deal.Promotion_Type__c == PROMOTION_FREE_UNITS}" layout="none" >
							<td width="150px;" style="{!IF(type == 'promotion', '', 'display:none;')}" >
								<div class="SearchInput">
									<div class="requiredInput">
										<div class="requiredBlock"></div>
										<apex:inputField value="{!newStartDate.Number_of_Free_Weeks__c}" style="width:68px;"  />
									</div>
								</div>
								 <div class="SearchFooter">
									<span class="SearchLabel">Get Free</span>
								</div>
							</td>
						</apex:outputPanel>
							
						<td width="250px;">
							<div class="SearchInput">								
								<apex:inputField value="{!newStartDate.Comments__c}" style="width:200px;height: 28px;"  />								
							</div>
							 <div class="SearchFooter">
								<span class="SearchLabel">Comments</span>
							</div>
						</td>
						
						<td style="text-align:right;">
							<apex:commandButton status="ipStatus" rerender="newRange,listFees, errorPanel, buttons" action="{!addStartDateRange}" value="Add Start Date Range" oncomplete="sizeFrame(); window.opener.refreshExtraFees();" />
						</td>
						
					</tr>
					
					<tr>
						<td colspan="5">
							<!--========================  File Details ============================================-->
							<div style="width:100%; margin-top:5px;text-align:left;">
								
								<div class="SearchInput">
		                        	<apex:selectList value="{!newStartDate.Account_Document_File__c}" size="1" style="width:700px;" styleClass="files">
										<apex:SelectOptions value="{!files}" /> 
									</apex:selectList>
									<script>
										jQuery('.files option[value!="Folder"]').attr('style','color:orangered;');
										jQuery('.files option[value="Folder"]').attr('style','color:black;background-image: url("/resource/1367818070000/icons/icons/home_page/gradientBlue.png") !important');
										jQuery('.files option[value="Folder"]').attr('disabled','disabled');
										jQuery('.files option[value="disabled"]').attr('disabled','disabled');
										
										jQuery('.files option[value!="Folder"]').each(function(){
											var str = jQuery(this).html();
											str = str.replace('[*', '<span style="color:#999999;font-size:0.85em;margin-left:5px;"> ( ');
											str = str.replace('*]', ') </span>');										
											jQuery(this).html(str);
										});
									</script>
		                        </div>
		                        <div class="SearchFooter">
		                        	<span class="SearchLabel">Related File</span>
		                        </div>
							</div>
						</td>
					</tr>
				
				</table>
			</div>
		</apex:pageBlock>
		
		<apex:outputPanel id="errorPanel">
			<apex:pageMessages escape="false" />
		</apex:outputPanel>
		<div class="titleHeader" style="height: 25px;">
			<apex:outputText value="Start Dates" />
			<apex:outputPanel id="buttons" styleClass="buttons" layout="block">			
				<apex:commandButton action="{!editStartDates}" status="ipStatus" value="Edit" rendered="{!NOT(editMode)}" reRender="listFees, buttons, errorPanel"  />
				
				<apex:commandButton onclick="if(!confirm('You are about to delete the selected start dates, proceed?')) return false; else deleteDates();" value="Delete Selected" rendered="{!NOT(editMode)}" reRender="listFees, buttons, errorPanel"   />
				
				<apex:actionFunction action="{!deleteStartDates}" status="ipStatus" name="deleteDates" reRender="listFees, buttons, errorPanel" oncomplete="window.opener.refreshExtraFees();" />
				
				<apex:commandButton action="{!saveStartDates}" status="ipStatus" value="Save" rendered="{!editMode}" reRender="listFees, buttons, errorPanel" oncomplete="window.opener.refreshExtraFees();" />
				<apex:commandButton action="{!cancelEdit}" status="ipStatus" value="Cancel" rendered="{!editMode}" reRender="listFees, buttons, errorPanel" />
			</apex:outputPanel>
		</div>
		
		<apex:pageBlock id="listFees">
			
			<apex:outputPanel layout="none" rendered="{!type != 'relatedFee'}">
								
				<table width="100%" cellspacing="0" class="relatedFees">
					<tr style="line-height: 20px;">
						<th> <apex:inputCheckbox title="Select All" onclick="selectAll(this.checked, 'startDateCheck');"/> </th>
						<apex:outputPanel rendered="{!deal.Promotion_Type__c == PROMOTION_EXTRA_FEE_DISCOUNT}" layout="none" >
							<th>Promotion Name</th>
						</apex:outputPanel>
						<th>Start Date From</th>
						<th>Start Date To</th>
						<th>
							<apex:outputText value="Start Date Value" rendered="{!type != 'promotion' || (type == 'promotion' && deal.Promotion_Type__c == PROMOTION_EXTRA_FEE_DISCOUNT) }" />
							<apex:outputText value="Get Free" rendered="{!type == 'promotion' && deal.Promotion_Type__c == PROMOTION_FREE_UNITS}" />
						</th>
						<th>Comments</th>
					</tr>
					<apex:repeat value="{!listStartDates}" var="range">
						<tr class="{!range.id}">
							<td width="3%">
								<apex:inputField value="{!range.Selected__c}" styleClass="startDateCheck" />
							</td>
							<apex:outputPanel rendered="{!deal.Promotion_Type__c == PROMOTION_EXTRA_FEE_DISCOUNT}" layout="none" >
								<td width="20%">
									<apex:outputField value="{!range.Promotion_Name__c}" rendered="{!NOT(editMode)}" />
									<apex:inputField value="{!range.Promotion_Name__c}" rendered="{!editMode}" />
								</td>
							</apex:outputPanel>
							<td width="20%">
								<apex:outputField value="{!range.From_Date__c}" rendered="{!NOT(editMode)}" />
								<apex:inputField value="{!range.From_Date__c}" rendered="{!editMode}" />												
							</td>
							<td width="20%">
								<apex:outputField value="{!range.To_Date__c}" rendered="{!NOT(editMode)}" />
								<apex:inputField value="{!range.To_Date__c}" rendered="{!editMode}" />
							</td>
							<td width="20%">
								
								<apex:outputPanel rendered="{!type != 'promotion' || (type == 'promotion' && deal.Promotion_Type__c == PROMOTION_EXTRA_FEE_DISCOUNT) }">
									<apex:outputText value="$" rendered="{!NOT(editMode) && deal.Extra_Fee_Type__c != 'Percentage'}" />
									<apex:outputField value="{!range.Value__c}" rendered="{!NOT(editMode)}" />
									<apex:inputField value="{!range.Value__c}" rendered="{!editMode}" />
									<apex:outputText value="%" rendered="{!NOT(editMode) && deal.Extra_Fee_Type__c == 'Percentage'}" />
								</apex:outputPanel>
								
								<apex:outputPanel rendered="{!type == 'promotion' && deal.Promotion_Type__c == PROMOTION_FREE_UNITS}" >
									<apex:outputField value="{!range.Number_of_Free_Weeks__c}" rendered="{!NOT(editMode)}" />
									<apex:inputField value="{!range.Number_of_Free_Weeks__c}" rendered="{!editMode}" />
								</apex:outputPanel>
								
							</td>
							<td width="37%">
								<apex:outputField value="{!range.Comments__c}" rendered="{!NOT(editMode)}" />
								<apex:inputField value="{!range.Comments__c}" rendered="{!editMode}" style="height:15px;width:80%" />
							</td>
						</tr>
						<tr style="display:none;"><td colspan="15"></td></tr>
						<tr class="fileRow fileRow-{!range.id} {!range.id} noborder" style="{!IF(NOT(ISBLANK(range.Account_Document_File__c)) || editMode, '', 'display:none')}" >
							<td></td>
							<td colspan="14" >
								<apex:outputText value="Related File: " style="color:orangeRed;"/>
								<apex:commandLink value="{!range.Account_Document_File__r.File_Name__c} ({!range.Account_Document_File__r.Description__c})" action="{!viewFile}" target="_blank"  rendered="{!NOT(ISNULL(range.Account_Document_File__c)) && NOT(editMode)}" style="color:navy;">
										<apex:param value="{!range.Account_Document_File__c}" name="fileId" />
									</apex:commandLink>
	                        	<apex:selectList value="{!range.Account_Document_File__c}" size="1" style="width:600px; {!IF(ISNULL(range.Account_Document_File__c),'color:#999;','')}" styleClass="files files-{!range.id}" rendered="{!editMode}" onchange="jQuery(this).css('background-color','#B0E0E6')" >
									<apex:SelectOptions value="{!files}" /> 
								</apex:selectList>
								<script>
									jQuery('.files option[value!="Folder"]').attr('style','color:orangered;');
									jQuery('.files option[value="Folder"]').attr('style','color:black;background-image: url("/resource/icons/icons/home_page/gradientBlue.png") !important');
									jQuery('.files option[value="Folder"]').attr('disabled','disabled');
									jQuery('.files option[value="disabled"]').attr('disabled','disabled');
									
									jQuery('.files option[value!="Folder"]').each(function(){
										var str = jQuery(this).html();
										str = str.replace('[*', '<span style="color:#999999;font-size:0.85em;margin-left:5px;"> ( ');
										str = str.replace('*]', ') </span>');
										jQuery(this).html(str);
									});
								</script>
							</td>
						</tr>
					</apex:repeat>
				</table>
				
			</apex:outputPanel>
			
			<apex:outputPanel layout="none" rendered="{!type == 'relatedFee' && NOT(ISNULL(relatedFees)) && relatedFees.size > 0 }">
				
				<table class="results" cellspacing="0" width="100%">
					<tr style="background-color:lightgrey">
						<th>Related Fee</th>
						<th>From (Unit)</th>
						<th>Value</th>
						<th>Pay Date From</th>
						<th>Pay Date To</th>
					</tr>		
					<apex:repeat value="{!relatedFees}" var="dependent">
						<tr style="line-height: 20px;">
							<td >
								<span style="font-weight: bold; padding-left: 5px;">
									<apex:outputField value="{!dependent.Product__r.Name__c}"  />
								</span>
							</td>
							<td width="15%">
								<apex:outputField value="{!dependent.From__c}" />
							</td>
							<td width="15%">
								$<apex:outputField value="{!dependent.Value__c}" />
							</td>
							<td width="15%">
								<apex:outputField value="{!dependent.Date_Paid_From__c}" />
							</td>
							<td width="15%">
								<apex:outputField value="{!dependent.Date_Paid_To__c}" />
							</td>
							
						</tr>
						<tr style="display:none;"><td></td></tr>
						<tr style="{!IF(NOT(ISNULL(dependent.Start_Date_Ranges__r)) && dependent.Start_Date_Ranges__r.size > 0, '', 'display:none;')}">
							<td colspan="5" style="padding: 5px 20px 10px 15px;" >
								<table width="100%" cellspacing="0" class="relatedFees">
									<tr style="line-height: 20px;">
										<th> <apex:inputCheckbox title="Select All" onclick="selectAll(this.checked, '{!dependent.id}');"/> </th>
										<th>Start Date From</th>
										<th>Start Date To</th>
										<th>Start Date Value</th>
										<th>Comments</th>
									</tr>
									<apex:repeat value="{!dependent.Start_Date_Ranges__r}" var="range">
										<tr class="{!range.id} ranges">
											<td width="3%">
												<apex:inputField value="{!range.Selected__c}" styleClass="{!dependent.id}" />
											</td>
											<td width="20%">
												<apex:outputField value="{!range.From_Date__c}" rendered="{!NOT(editMode)}" />
												<apex:inputField value="{!range.From_Date__c}" rendered="{!editMode}" onchange="jQuery(this).css('background-color','#B0E0E6')" onkeypress="isValidInput(event, this)"/>
											</td>
											<td width="20%">
												<apex:outputField value="{!range.To_Date__c}" rendered="{!NOT(editMode)}" />
												<apex:inputField value="{!range.To_Date__c}" rendered="{!editMode}" onchange="jQuery(this).css('background-color','#B0E0E6')" onkeypress="isValidInput(event, this)"/>
											</td>
											<td width="20%">
												<apex:outputText value="$" rendered="{!NOT(editMode)}" />
												<apex:outputField value="{!range.Value__c}" rendered="{!NOT(editMode)}" />
												<apex:inputField value="{!range.Value__c}" rendered="{!editMode}" onchange="jQuery(this).css('background-color','#B0E0E6')" onkeypress="isValidInput(event, this)" />
											</td>
											<td width="37%">
												<apex:outputField value="{!range.Comments__c}" rendered="{!NOT(editMode)}" />
												<apex:inputField value="{!range.Comments__c}" rendered="{!editMode}" style="height:15px;width:80%" onchange="jQuery(this).css('background-color','#B0E0E6')" onkeypress="isValidInput(event, this)" />
											</td>
										</tr>
										<tr style="display:none;"><td colspan="15"></td></tr>
										<tr class="fileRow fileRow-{!range.id} {!range.id} noborder" style="{!IF(NOT(ISBLANK(range.Account_Document_File__c)) || editMode, '', 'display:none')}" >
											<td></td>
											<td colspan="14">
												<apex:outputText value="Related File: " style="color:orangeRed;"/>
												<apex:commandLink value="{!range.Account_Document_File__r.File_Name__c} ({!range.Account_Document_File__r.Description__c})" action="{!viewFile}" target="_blank"  rendered="{!NOT(ISNULL(range.Account_Document_File__c)) && NOT(editMode)}" style="color:navy;">
													<apex:param value="{!range.Account_Document_File__c}" name="fileId" />
												</apex:commandLink>
					                        	<apex:selectList value="{!range.Account_Document_File__c}" size="1" style="width:600px; {!IF(ISNULL(range.Account_Document_File__c),'color:#999;','')}" styleClass="files files-{!range.id}" rendered="{!editMode}" onchange="jQuery(this).css('background-color','#B0E0E6')" >
													<apex:SelectOptions value="{!files}" /> 
												</apex:selectList>
												<script>
													jQuery('.files option[value!="Folder"]').attr('style','color:orangered;');
													jQuery('.files option[value="Folder"]').attr('style','color:black;background-image: url("/resource/icons/icons/home_page/gradientBlue.png") !important');
													jQuery('.files option[value="Folder"]').attr('disabled','disabled');
													jQuery('.files option[value="disabled"]').attr('disabled','disabled');
													
													jQuery('.files option[value!="Folder"]').each(function(){
														var str = jQuery(this).html();
														str = str.replace('[*', '<span style="color:#999999;font-size:0.85em;margin-left:5px;"> ( ');
														str = str.replace('*]', ') </span>');
														jQuery(this).html(str);
													});
												</script>
											</td>
										</tr>
									</apex:repeat>										
								</table>
							</td>
						</tr>
						
					</apex:repeat>
				</table>
				
			</apex:outputPanel>
			
			<apex:repeat value="{!errorUpdateRange}" var="er3">
				<script>
					jQuery('.'+'{!er3}').css('background-color','#FFB1B1');
					jQuery('.'+'{!er3}').find('td').css('border-top','solid 2px #FFF');
				</script>
			</apex:repeat>
			
			
		</apex:pageBlock>
			
		
		
	</apex:form>
</apex:page>