<apex:page controller="xCourseSearch" standardStylesheets="false" showHeader="false" sidebar="false" cache="false" >
	<head>
        <meta charset="utf-8" />
        <title>Compare Schools, Courses &amp; Prices</title>
        <meta name="viewport" content="width=device-width; initial-scale=1.0" />
        <!--js library-->
        <link rel="stylesheet" type="text/css" href="assets/css/reset.css"  />
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
        <link rel="stylesheet" type="text/css" href="assets/css/global.css"  />
        <link rel="stylesheet" type="text/css" href="assets/css/style.css"  />
        
        <!-- calendar js. upload to static resources -->
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
        <script src="https://code.jquery.com/ui/1.8.24/jquery-ui.js"></script>
        
        <apex:stylesheet value="{!URLFOR($Resource.assets,'css/reset.css')}"/>
        <apex:stylesheet value="{!URLFOR($Resource.assets,'fonts')}"/>
        <apex:stylesheet value="{!URLFOR($Resource.assets,'css/global.css')}"/>
        <apex:stylesheet value="{!URLFOR($Resource.assets,'css/style.css')}"/>
		
		<apex:includeScript value="{!$Resource.jquery}"/> 
        <apex:includeScript value="{!$Resource.jqueryBlockUI}" />
		
     </head>   
        
	<style>
		
		body {
			font-family: 'pt_sansbold';
		}
		
		.header{
			
			background: -webkit-gradient(linear, left top, left bottom, from(#E34A4C), to(#DC2328));
			background: -webkit-linear-gradient(top, #E34A4C, #DC2328);
  			background: linear-gradient(to bottom, #E34A4C 0%, #DC2328 99%) repeat scroll 0 0 rgba(0, 0, 0, 0);
		
			font-family: 'pt_sans';
			color: #FFF;
			font-size: 3em;
			font-weight: bold;
			text-align: center;
			vertical-align: middle;
			padding: 3px;
			height: 35px;
		}
		
		.desc{
			color: #999;
			
			height: 50px;
			font-size: 1.5em;
			text-align: center;
			vertical-align: middle;
		}
		
		.fieldSize{
			width: 97%;
		}
		
		.errorMsg{
			clear: left;
		    color: #FF0000;
		    display: block;
		}
		
		.pageHeader {
			height: 100px;
			background-color: #F7F7F7;
			text-align: center;
			display:block;
			width: 100%;
		}
		
		.pageHeaderTitle {
			color: #A9A9A9;
			font-size: 4.5em;
			padding-top: 10px;
			display: block;
			font-family: 'pt_sansbold';
		}
		
		.pageHeaderTitleFooter {
			font-family: 'pt_sansbold';
			color: #A9A9A9;
			font-size: 1.5em;
			padding-top: 15px;
			display: block;			
		}
		
		.bottomHeader {
			margin-top: 2px;
			background-color: #CDCDCD;
			clear:both;
			color: #606060			
			height: 20px;
			display: block;
			font-family: 'pt_sansbold';
		}
		
		.mainDiv {
			padding: 2%;
			margin-top: 15px;
		}
		
		.thanksDiv{
			height: 50px;
			text-align: center;
			display:block;
			width: 100%;
		}
		
		.thanksLabel {			
			color: #797979;
			font-size: 3.5em;
			padding-top: 10px;
			display: block;
			font-family: 'pt_sansbold';		
		}
		
		.objectiveLabel {
			font-size: 1.8em;
			color: #797979;
			font-family: 'pt_sansbold';
			display: block;
			
		}
		
		.line {
			border-bottom: 1px solid #E6E6E6; 
			clear: both;
		}
		
		.items {
			width: 60%;
			display: block;
			float: left;
		}
		
		.login {
			width: 30%;
			display: block;
			float: right;			
		}
		
		.loginBorder {
			border: 1px solid #E6E6E6; 
		}
		
		.registerDiv {
			width: 100%;
			clear: both;
			margin-top: 5px;
		}
		
		.arrowDiv {
			width: 10%;
			display: block;
			float: left;
		}
		
		.itemlist {
			list-style: disc !important;			
		}
		
		.itemlist li {
			color:#DC2328;
			font-family: 'pt_sans';
			font-size: 2em;
			margin: 15px;
		}
		
		.itemlist li span {
			color:#606060;
		}
		
		input.watermark { color: #999; font-family: 'pt_sans'; }
		
		.login a {
			background: url("../images/go.png") no-repeat scroll 73px center #DC2328;
		    color: #FFFFFF;
		    float: right;
		    font-size: 1.4em;
		    margin: 10px 10px 0;
		    padding: 3px 0;
			border-radius: 3px;
			width: 100px;
			text-align: center;
		}
		
		.registerDiv {
			font-size: 1.4em;
		}
		
		.registerDiv a {
			background: none;
			color: #4D4D4D;
			float: none;			
			margin: 0px;
			font-size: 1.0em;
			padding: 0px;
			text-decoration: underline;
		}
		
		.importantDiv {
			height: 25px;
			background-color: #F7F7F7;
			text-align: center;
			display:block;
			width: 100%;
			padding-top: 9px;
		}
		
		.importantLabel {			
		    font-size: 1.4em;
		    padding-top: 5px;
		}
		
		
	</style>
	
	<script>
		jQuery(document).ready(function() {
			setWatermark('name', 'Name');
			setWatermark('email', 'Work Email');
			setWatermark('company', 'Company');
		
			sizeFrame();
		});
		
		function setWatermark(inputClass, watermarkText){
			jQuery('.'+inputClass).blur(function(){
				if (jQuery(this).val().length == 0)
					jQuery(this).val(watermarkText).addClass('watermark');
			}).focus(function(){
				if (jQuery(this).val() == watermarkText)
					jQuery(this).val('').removeClass('watermark');
			});
				
			jQuery('.'+inputClass).each(function(){
				if(jQuery(this).val().length == 0)
					jQuery(this).val(watermarkText).addClass('watermark');
			});
		}
		
		function removeWatermark(inputClass, watermarkText){
			if(jQuery('.'+inputClass).val() == watermarkText){
				jQuery('.'+inputClass).val('');
			}		
		}
		
		function removeAllWM(){
			removeWatermark('name', 'Name');
			removeWatermark('email', 'Work Email');
			removeWatermark('company', 'Company');
		}
		
		function sizeFrame() {
		    var pageSize = jQuery(document).height();    
		    // Going to 'pipe' the data to the parent through the helpframe..
		    var pipe = document.getElementById('helpframe');
		    // Cachebuster a precaution here to stop browser caching interfering
		    pipe.src = 'http://weusetheforce.com/helper.html?height='+pageSize+'&cacheb='+Math.random();
	   }
		
	</script>
	
	<apex:form >
		<div class="page clearfix"> 
			<div class="pageHeader">
				<apex:outputLabel styleClass="pageHeaderTitle" value="COMPARE SCHOOLS, COURSES & PRICES"></apex:outputLabel>
				<apex:outputLabel styleClass="pageHeaderTitleFooter" value="Only for International Education Agents & Schools"></apex:outputLabel>
			</div>
			
			<div class="bottomHeader">
				<table width="100%" style="padding-top: 10px; height: 20px; font-size: 1.5em; font-family: 'pt_sansbold';">
					<tr>
						<td width="37.5%" style="text-align:right; vertical-align: middle;">253 Schools</td>
						<td width="25%" style="text-align:center; vertical-align: middle;">424 Campuses</td>
						<td width="37.5%" style="vertical-align: middle;">5203 Courses</td>
					</tr>
				</table>
			</div>
			
			<div class="mainDiv">
				<div class="thanksContainer">
					<div class="thanksDiv">
						<apex:outputLabel styleClass="thanksLabel" value="Sales & Communication platform for Schools and Agents."></apex:outputLabel>
					</div>
					<div class="thanksDiv" style="padding-top: 15px;">
						<apex:outputLabel styleClass="objectiveLabel" value="Thanks for participating in this Beta TEST"></apex:outputLabel>
						
					</div>
				</div>
				
				<div class="line"></div>
				
				<div style="display:block; width: 100%; margin-top: 20px;" >
					<div class="items" >
						<apex:outputLabel value="In this Test you will be able to" style="color:#DC2328; font-size: 2em; margin-bottom: 15px; display:block; " />
						<ul class="itemlist">
							<li><span>Compare prices and promotions for language Schools around the globe by student's nationalities</span></li>
							<li><span>Compare School prices from different destinations (Australia, Canada, NZ, etc)</span></li>
							<li><span>All prices include promotions for each nationality eg. (13+2 free weeks) (free material fee) (10% discount on full price), etc</span></li>
							<li><span>Click on "Save and Compare" to narrow your search and compare courses</span></li>
						</ul>
					</div>
					
					
					<div class="login">
						<div class="loginBorder">
							<table width="100%">
								<tr>
									<td class="header">Test our system</td>
								</tr>
								<tr>
									<td class="desc">Integrated Sales &amp; Communication platform</td>
								</tr>
								<apex:outputPanel layout="none" rendered="{!newUser}">
									<tr>
										<td class="desc" style="padding: 5px 10px;">
											<apex:outputLabel value="I am:" style="float:left; margin-top: 2px; "/>
											<apex:selectRadio value="{!objCart.User_Type__c}" style="float:left; margin-left: 5px;" id="userType" required="true">
								            	<apex:selectOptions value="{!UserType}"/>
								            </apex:selectRadio>
											<apex:message for="userType" styleclass="errorMsg" style="float:left;font-size:0.8em;"/>
											
										</td>
									</tr>
									<tr>
										<td style="padding: 7px 10px;">
											<apex:inputField value="{!objCart.User_Name__c}" styleClass="fieldSize name" required="true"  /> 
											
										</td>
									</tr>
								</apex:outputPanel>
								<tr>
									<td style="padding: 7px 10px;">
										<apex:inputField value="{!objCart.Email__c}" styleClass="fieldSize email" required="true" /> 
										
									</td>
								</tr>
								<apex:outputPanel layout="none" rendered="{!newUser}">
									<tr>
										<td style="padding: 7px 10px;">
											<apex:inputField value="{!objCart.Company_Name__c}" styleClass="fieldSize company" required="true" /> 
											
										</td>
									</tr>
									<tr>
										<td style="padding: 7px 10px;">
											<apex:selectList value="{!objCart.Country__c}" size="1" required="true" id="country" styleClass="fieldSize">
								            	<apex:selectOptions value="{!countries}"/>
								            </apex:selectList>
											<apex:message for="country" styleclass="errorMsg"/>
											
										</td>
									</tr>
								</apex:outputPanel>
								<tr>
									<td style="padding-bottom:10px;">
										<!--<apex:commandLink value="Register" action="{!changeView}" style="float:left; font-size: 1em;" rendered="{!NOT(newUser)}" immediate="true" onclick="removeAllWM();" />
										<apex:commandLink value="Login" action="{!changeView}" style="float:left; font-size: 1em;" rendered="{!newUser}" immediate="true" onclick="removeAllWM();" />-->
										<apex:commandLink value="Register" action="{!addUser}" styleClass="loginButton" rendered="{!newUser}" onclick="removeAllWM();"  /> 
										<apex:commandLink value="Login" action="{!loginUser}" styleClass="loginButton" rendered="{!NOT(newUser)}" onclick="removeAllWM();" /> 
									</td>
								</tr>
							</table>
						</div>
						<div class="registerDiv" style="{!IF(NOT(newUser), '', 'display:none;')}">
							<apex:outputText value="Not yet registered? " styleClass="notRegistered" />
							<apex:commandLink value="Click here " action="{!changeView}"  styleClass="notRegistered" immediate="true" onclick="removeAllWM();" />
							<apex:outputText value="to register." styleClass="notRegistered"  />
						</div>
						<div class="registerDiv" style="{!IF(newUser, '', 'display:none;')}">
							<apex:outputText value="Already registered? " styleClass="notRegistered"  />
							<apex:commandLink value="Click here " action="{!changeView}"  styleClass="notRegistered" immediate="true" onclick="removeAllWM();" />
							<apex:outputText value="to login." styleClass="notRegistered"  />
						</div>
					</div>
				</div>
			</div>
			<div style="clear:both;height:20px;"></div>
			
			<div class="importantDiv">
				
				<apex:outputText styleClass="importantLabel" value="Test Site" style="font-size:1.75em;" />
				<apex:outputText styleClass="importantLabel" value=" - prices/promotions are updated for" />
				<apex:outputText styleClass="importantLabel" value=" Latin America " style="font-size:1.75em;" />
				<apex:outputText styleClass="importantLabel" value="and " />
				<apex:outputText styleClass="importantLabel" value="Europe " style="font-size:1.75em;" />
				<apex:outputText styleClass="importantLabel" value="countries only." />
			</div>
			
			<div style="clear:both;height:20px;"></div>
			
			<div style="width:100%; padding:2%">
				<div class="message">
					<apex:outputLabel style="color:#797979; font-size:2em;" value="Using this platform, we believe that Schools and Agents could improve their communication, lower their operational costs, increase productivity and improve staff training." />
				</div>
				<div style="clear:both;height:45px;"></div>
				<div class="feedback">
					<apex:outputLabel style="color:#DC2328; font-size:4em;" value="We appreciate your feedback!" />
				</div>
			</div>
			
		</div>
	</apex:form>
	<site:googleAnalyticsTracking />
	<iframe id="helpframe" src="" height="0" width="0" frameborder="0"></iframe>
</apex:page>