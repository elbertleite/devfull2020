<apex:page controller="SetupAgencyController" sidebar="false" showHeader="false">

<apex:includeScript value="{!$Resource.jquery}"/> 
<apex:includeScript value="{!$Resource.jqueryBlockUI}" />
<script src="https://code.jquery.com/ui/1.8.24/jquery-ui.js"></script>

<apex:styleSheet value="{!URLFOR($Resource.chosen_1_2_0,'chosen.min.css')}" />    
<apex:includeScript value="{!URLFOR($Resource.chosen_1_2_0,'chosen.jquery.min.js')}" />

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

<style>	
	.divContainer{
		//margin-top: 10px;
		width:100%;
	}
	
	.divBlock{
		display: block;
   		margin-bottom: 2px;
   		width: 100%;
	}
	
	.divHeader {
	    background-color: #999999;
	    clear: both;
	    color: #ffffff;
	    font-size: 1.2em;
	    font-weight: bold;
	    line-height: 30px;
	    padding-left: 0.6em;
	}
	
	.divContent {
	    border: 1px solid #aaaaaa;
	    padding: 0.5em;
	}
	
    .bPageBlock {
      border: none !important;
      background: none !important;
    }
    
    .bPageBlock .detailList .labelCol {
	    width: 18%;
	}
	
	.inputField{	
		width:300px;
	}
	
	.apexMessage{
		color:red;
		display: block;
	}
	
	.drag-handle {
	    background-color: #236fbd !important;
	}

</style>

<script>	
	var geocoder = new google.maps.Geocoder();

	jQuery(document).ready(function(){
		jQuery('.chosen').chosen();
		jQuery('.chosen').chosen({no_results_text: "Sorry, nothing found!" , placeholder_text_multiple: "Type to find the options", placeholder_text_single: "Type to find the option"});
		
	});
	
	function codeAddress() {
		var address = jQuery('.street').val() + ' ' + jQuery('.city').val() + ' ' + jQuery('.country').val();
		
	  	geocoder.geocode( { 'address': address}, function(results, status) {
	    if (status == google.maps.GeocoderStatus.OK) {
	    	//alert(results[0].geometry.location.lat());
			jQuery('.latitude').val(results[0].geometry.location.lat());
			jQuery('.longitude').val(results[0].geometry.location.lng());
	    } else {
	      //alert('Geocode was not successful for the following reason: ' + status);
	    }
	  });
	}
	
	 function showLoading(msg) { 
        jQuery.blockUI({ 
			message: '<H1>'+msg+'</H1>',
			css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } });
     }
		
	function hideLoading(){
		jQuery.unblockUI();
	}		
</script>


<apex:form >
<apex:actionStatus id="statusUpdate" onstart="showLoading('Please wait...');" onstop="hideLoading();" />
<div class="divContainer">

	<!-- <div style="margin-bottom:10px;width:41%;float:right">
		<apex:commandbutton value="Save" onClick="codeAddress()" action="{!saveAgency}" status="statusUpdate" />		
		<apex:commandbutton value="Cancel" />
	</div> -->
	
	<!-- AGENCY GROUP -->
	<apex:outputpanel id="groupPanel" rendered="{!action=='setup'}" >
	<div class="divBlock">
		<div class="divHeader">
			<apex:outputtext value="Agency Group Details" />
		</div>
		
		<div class="divContent">
			<apex:pageBlock >
				<apex:pageBlockSection columns="1"  >
					<apex:pageBlockSectionItem >
						<apex:outputtext value="Name" />
						<apex:inputfield value="{!agencyGroup.Name}"  style="width:300px;"/>
					</apex:pageBlockSectionItem>
					
					<apex:pageBlockSectionItem >
						<apex:outputtext value="Group Type" />
						<apex:inputfield value="{!agencyGroup.Group_Type__c}" required="true"  style="width:300px;"/>
					</apex:pageBlockSectionItem>
					
				</apex:pageBlockSection>
			</apex:pageBlock>
		</div>
		
	</div>
	</apex:outputpanel>
	<!-- END AGENCY GROUP -->
	
	<!-- AGENCY DETAILS -->
	<div class="divBlock">
		<div class="divHeader">
			<apex:outputtext value="Agency Details" />
		</div>
		
		<div class="divContent">
			<apex:pageBlock >
				<apex:pageBlockSection columns="1"  >
				
					<apex:pageBlockSectionItem >
						<apex:outputtext value="Name" />
						<apex:inputfield value="{!agency.Name}"  styleclass="inputField"/>
					</apex:pageBlockSectionItem>
					
					<apex:pageBlockSectionItem >
						<apex:outputtext value="Registration Name" />
						<apex:inputfield value="{!agency.Registration_name__c}" required="true"  styleclass="inputField"/>
					</apex:pageBlockSectionItem>
					
					<apex:pageBlockSectionItem >
						<apex:outputtext value="Agency Type" />
						<apex:inputfield value="{!agency.Agency_Type__c}" required="true"  styleclass="inputField"/>
					</apex:pageBlockSectionItem>
					
					<apex:pageBlockSectionItem >
						<apex:outputtext value="Currency Iso Code" />
						<apex:outputpanel >
							<div class="requiredInput"><div class="requiredBlock"></div>
							<apex:selectList value="{!agency.Account_Currency_Iso_Code__c}" styleclass="inputField chosen" required="true" size="1" id="currentIso" >
								<apex:selectOptions value="{!currencies}" />
							</apex:selectList>
							<apex:message styleClass="apexMessage" for="currentIso" />
							</div>
						</apex:outputpanel>
					</apex:pageBlockSectionItem>
					
					<apex:pageBlockSectionItem >
						<apex:outputtext value="Optional Currency" />
						<apex:outputpanel >
							<div class="requiredInput"><div class="requiredBlock"></div>
							<apex:selectList value="{!agency.Optional_Currency__c}" styleclass="inputField chosen" required="true" size="1" id="optionalIso" >
								<apex:selectOptions value="{!currencies}" />
							</apex:selectList>
							<apex:message styleClass="apexMessage" for="optionalIso" />
							</div>
						</apex:outputpanel>
						
					</apex:pageBlockSectionItem>
										
				</apex:pageBlockSection>
			</apex:pageBlock>
		</div>
		
	</div>
	<!-- END AGENCY DETAILS -->
	
	<!-- AGENCY ADDRESS -->
	<div class="divBlock">
		<div class="divHeader">
			<apex:outputtext value="Agency Address" />
		</div>
		
		<div class="divContent">
			<apex:pageBlock >
				<apex:pageBlockSection columns="1"  >
				
					<apex:pageBlockSectionItem >
						<apex:outputText value="{!$ObjectType.Account.fields.BillingStreet.Label}"  />
						<apex:inputField value="{!agency.BillingStreet}"  styleclass="inputField"  />
					</apex:pageBlockSectionItem>
					
					<apex:pageBlockSectionItem >
						<apex:outputText value="{!$ObjectType.Account.fields.BillingCity.Label}" />
						<apex:inputField value="{!agency.BillingCity}" required="true" styleclass="inputField" />	
					</apex:pageBlockSectionItem>
					
					<apex:pageBlockSectionItem >
						<apex:outputText value="{!$ObjectType.Account.fields.BillingState.Label}" />
						<apex:inputField value="{!agency.BillingState}" required="true" styleclass="inputField" />		
					</apex:pageBlockSectionItem>
					
					<apex:pageBlockSectionItem >
						<apex:outputText value="{!$ObjectType.Account.fields.BillingPostalCode.Label}" />
						<apex:inputField value="{!agency.BillingPostalCode}" styleclass="inputField" />			
					</apex:pageBlockSectionItem>
					
					<apex:pageBlockSectionItem >
						<apex:outputText value="{!$ObjectType.Account.fields.BillingCountry.Label}" />
						<apex:outputpanel >
							<div class="requiredInput"><div class="requiredBlock"></div>
							<apex:selectList value="{!agency.BillingCountry}" size="1" styleClass="inputField chosen" id="billingCountry" >
			                	<apex:selectOptions value="{!destinations}" />
		              		</apex:selectList>
							<apex:message styleClass="apexMessage" for="billingCountry" />
							</div>
						</apex:outputpanel>
						
					</apex:pageBlockSectionItem>
					
					<apex:outputPanel style="display:none">
						<apex:inputText value="{!agency.BillingLatitude}" styleclass="latitude"/>
						<apex:inputText value="{!agency.BillingLongitude}" styleclass="longitude"/>												
					</apex:outputPanel>
					
				</apex:pageBlockSection>
			</apex:pageBlock>
		</div>
		
	</div>
	<!-- END AGENCY ADDRESS -->
	
	<!-- AGENCY CONTACT DETAILS -->
	<div class="divBlock">
		<div class="divHeader">
			<apex:outputtext value="Agency Contact Details" />
		</div>
		
		<div class="divContent">
			<apex:pageBlock >
				<apex:pageBlockSection columns="1"  >
				
					<apex:pageBlockSectionItem >
						<apex:outputText value="Website" />
						<apex:inputField value="{!agency.Website}" styleclass="inputField" />
					</apex:pageBlockSectionItem>
				
					<apex:pageBlockSectionItem >
						<apex:outputText value="{!$ObjectType.Account.fields.BusinessEmail__c.Label}" />
						<apex:inputField value="{!agency.BusinessEmail__c}" styleClass="inputField"  />												
					</apex:pageBlockSectionItem>
					
					<apex:pageBlockSectionItem >
						<apex:outputText value="Phone Number" />
						<apex:inputField value="{!agency.Phone}" styleClass="inputField" />												
					</apex:pageBlockSectionItem>
					
					<apex:pageBlockSectionItem >
						<apex:outputText value="{!$ObjectType.Account.fields.Skype__c.Label}" />
						<apex:inputField value="{!agency.Skype__c}" styleClass="inputField"  />												
					</apex:pageBlockSectionItem>
					
				</apex:pageBlockSection>
			</apex:pageBlock>
		</div>
		
	</div>
	<!-- END AGENCY CONTACT DETAILS -->
	
	<div style="width:100%;text-align: center;">
		<apex:commandbutton value="Save" onClick="codeAddress()" action="{!saveAgency}" status="statusUpdate" rendered="{!action!='edit'}" />
		<apex:commandbutton value="Save" onClick="codeAddress()" action="{!updateAgency}" status="statusUpdate" rendered="{!action=='edit'}" />
		<apex:commandbutton value="Cancel" onClick="javascript: parent.closemodal();" immediate="true" />
	</div>
	
	<apex:outputpanel id="verifyErrors" rendered="{!showError=true}">
		<script>		
			if(jQuery('.errorMsg').length == 0){
				parent.closeAndRefresh();
			}
		</script>
	</apex:outputpanel>
	
</div>
</apex:form>
</apex:page>