<apex:page controller="commissions_find_invoice" showHeader="true" sidebar="true" tabstyle="Finance__tab">
        <apex:include pageName="xStylePageV2" />
        <apex:include pageName="xCourseSearchStyle" />

    <style>
        .tableInstal{
            border: 0px;
            margin-top: 5px;
        }

        .tableInstal > tbody > tr:hover > td  {background-color:#f6fafe}

        .tableInstal tbody td {
            font-size: 1em !important;
            vertical-align:top !important;
            padding: 2px 0.3em !important;
        }

        .tableInstal thead tr td {
            border-bottom: medium none;
            padding: 3px;
            font-size: 0.9em;
        }

        .tableInstal tbody th  {
            background-color:  #eeeeee;
            border-top: solid 1px #ccc;
        }

        .tableInstal tbody tr td:first-child {
            border-left: 1px solid #ccc;
        }

        .tableInstal tbody tr td:last-child {
            border-right: 1px solid #ccc;
        }

        .tableInstal tbody tr th{
            border: 1px solid #ccc;
            border-right: 0px;
            text-indent: 3px;
        }

        .tableInstal tbody tr th:last-child {
            border-right: 1px solid #ccc !important;
        }

        .filterbox {
            display: inline-table;
        }

        .filterbox select { width: 150px; }

        .filters {
            border: 1px solid #999999 !important;
            border-radius: 5px;
            box-shadow: 0 0 0 #cccccc;
            margin: 1em 0.5em 0.5em 0.5em;
            padding: 10px;
            display: inline-table;
            background-color: #f3f3f3;
            width:98%;
        }

        .details {font-size: 0.9em; display:block;}
    </style>
    
    
    <table class="tbl_Details">
        <tr>
            <td class="sidesColumn">
                <c:menuFinance selectedItem="findInvoices" />
            </td>

            <td class="middleColumn bodyColumn">
                <div class="title" style="font-size:1.5em;">
                    <span class="arrow-e" style="font-size: 1.5em;"></span>&nbsp;Find Invoice

                    <div class="title" style="float:right;">
                        <apex:outputLink value="/help_link?fl={!$CurrentPage.Name}" target="_blank" style="float:right;font-weight: normal;text-decoration: none; color: #777777; font-size: 0.7em;display: block;
                    width: 100%;text-align: right;">
                            <apex:image value="{!urlfor($Resource.icons,'icons/commons/help.png')}" style="height: 16px; width: 16px; position: relative; top: 3px;" />
                            Help
                        </apex:outputLink>
                    </div>
                </div>

                <apex:form >
                    <div class="filters">
                        <div class="filterbox">
                            <apex:inputtext value="{!invoiceNumber}"/>
                            <apex:outputText value="Invoice Number" style="display: block;" styleclass="filterLabel"/>
                        </div>
                        <div class="filterbox">
                            <apex:commandLink value="Search" status="statusLoad" rerender="searchResult" styleClass="agencyButton" action="{!searchInvoice}" style="padding: 5px 20px; " />
                        </div> 
                    </div>
                </apex:form>

                <apex:outputPanel id="searchResult">
                    <apex:outputpanel id="noResult" layout="none" rendered="{!ISNULL(result) && ISNULL(resultInstallmentInvoice)}">
                        <div style="text-align:center; color:red; border:1px solid grey; padding:5px; margin-top: 6px;width: 100%;">No result found</div>
                    </apex:outputpanel>

                    <apex:outputpanel layout="none" rendered="{!AND(NOT(ISNULL(result)), NOT(ISNULL(result.id)))}">
                        <table class="tableInstal">
                            <tr>
                                <th>Invoice Number</th>
                                <th>Due Date</th>
                                <th>Issued by</th>
                                <th>Issued to </th>
                                <th style="text-align:center"># </th>
                                <apex:outputpanel layout="none" rendered="{!AND(OR(result.isShare_Commission_Invoice__c, result.isCommission_Invoice__c, result.isProvider_Commission__c), NOT(result.isCancelled__c))}">
                                    <th style="text-align:right;padding-right:3px;">Total Amount </th>
                                    <apex:outputpanel layout="none" rendered="{!result.isShare_Commission_Invoice__c}">
                                        <th>Paid On</th>
                                     </apex:outputpanel>
                                    <th>Received On</th>
                                </apex:outputpanel>

                                <apex:outputpanel layout="none" rendered="{!result.isCancelled__c}">
                                    <th style="background-color:#fff2f2 !important;">Invoice Cancelled</th>
                                </apex:outputpanel>

                                <apex:outputpanel layout="none" rendered="{!result.isPDS_Confirmation__c}">
                                    <th>Online Form </th>
                                    <th>Completed by School</th>
                                    <th>Closed On</th>
                                </apex:outputpanel>


                            </tr>
                            <tr>
                                <!-- LINK -->
                                <td>
                                    <!-- <apex:outputLink value="{!link}" target="_blank">{!invoiceNumber}</apex:outputLink> -->
                                    <apex:repeat var="link" value="{!invoiceLinks}">
                                        <div>
                                            <apex:outputLink value="{!invoiceLinks[link]}" target="_blank">{!link}</apex:outputLink>
                                        </div>
                                    </apex:repeat>
                                </td>

                                <!-- DUE DATE -->
                                <td style="text-align:center">
                                    <apex:outputText value="{0, date, d/MM/yyyy}">
                                        <apex:param value="{!result.Due_Date__c}" />
                                    </apex:outputText>
                                </td>

                                <!-- REQUESTED BY -->
                                <td>
                                    {!result.Requested_by_Agency__r.Name}
									<span class="details">{!result.createdBy.Name}</span>
                                </td>
                                
                                <!-- REQUESTED TO -->
                                <td>
                                    {!result.Requested_Commission_To_Agency__r.Name}
									<span class="details">Sent to: {!result.Sent_Email_To__c}</span>
                                </td>

                                <!-- INSTALMENTS REQUESTED -->
                                <td style="text-align:center">
                                    {!result.Total_Instalments_Requested__c}
                                </td>

                                <!-- TOTAL VALUE / PAID ON / RECEIVED ON  -->
                                <apex:outputpanel layout="none" rendered="{!AND(OR(result.isShare_Commission_Invoice__c, result.isCommission_Invoice__c, result.isProvider_Commission__c), NOT(result.isCancelled__c))}">
                                    <td style="text-align: right; font-weight: bold; font-size: 1.2em !important; ">
                                        <apex:outputText value="{0, number, currency}">
                                            <apex:param value="{!result.Total_Value__c}" />
                                        </apex:outputText>
                                    </td>
                                    <apex:outputpanel layout="none" rendered="{!result.isShare_Commission_Invoice__c}">
                                        <td style="vertical-align:middle !important">
                                            <apex:outputpanel rendered="{!NOT(ISNULL(result.Paid_On__c))}">
                                                <apex:outputfield value="{!result.Paid_On__c}" />
                                                <span class="details">by: {!result.Paid_by__r.Name}</span>
                                            </apex:outputpanel>

                                            <apex:outputpanel rendered="{!ISNULL(result.Paid_On__c)}" layout="block" styleclass="pendingStatus2">
                                                Pending
                                            </apex:outputpanel>
                                        </td>
                                    </apex:outputpanel>

                                    <td style="vertical-align:middle !important">
                                        <apex:outputpanel rendered="{!NOT(ISNULL(result.Confirmed_Date__c))}">
                                            <apex:outputfield value="{!result.Confirmed_Date__c}" />
                                            <span class="details">by: {!result.Confirmed_By__r.Name}</span>
                                        </apex:outputpanel>
                                      
                                        <apex:outputpanel rendered="{!ISNULL(result.Confirmed_Date__c)}" layout="block" styleclass="pendingStatus2">
                                            Pending
                                        </apex:outputpanel>
                                    </td>

                                </apex:outputpanel>

                                <!-- PDS CONFIRMATION -->
                                <apex:outputpanel layout="none" rendered="{!result.isPDS_Confirmation__c}">
                                    <td style="text-align:center">
                                        <apex:outputfield value="{!result.isSchool_to_Answer__c}" />
                                    </td>
                                    
                                    <td style="text-align:center;vertical-align:middle !important;">
                                        <apex:outputpanel rendered="{!NOT(result.isSchool_to_Answer__c)}">
                                            <span style="font-style:italic"> not applicable </span>
                                        </apex:outputpanel>
                                        <apex:outputfield value="{!result.Request_Completed_by_School_On__c}" />
                                    </td>
                                    
                                    <td style="vertical-align:middle !important;">
                                        <apex:outputpanel rendered="{!NOT(ISNULL(result.Request_Confirmation_Completed_On__c))}">
                                           <apex:outputfield value="{!result.Request_Confirmation_Completed_On__c}" />
                                            <span class="details">by: {!result.Request_Confirmation_Completed_by__r.Name}</span>
                                        </apex:outputpanel>

                                        <apex:outputpanel rendered="{!ISNULL(result.Request_Confirmation_Completed_On__c)}" layout="block" styleclass="pendingStatus2">
                                            Pending
                                        </apex:outputpanel>
                                    </td>
                                </apex:outputpanel>

                                <!-- INVOICE CANCELLED -->
                                 <apex:outputpanel layout="none" rendered="{!result.isCancelled__c}">
                                    <td style="background-color:#fff2f2;">
                                        <div style="display:v=block;font-weight:bold;"><apex:outputfield value="{!result.Cancel_Reason__c}" /></div>
                                        <apex:outputfield value="{!result.Cancel_Description__c}" />
                                    </td>
                                </apex:outputpanel>
                            </tr>
                        </table>
                    </apex:outputpanel> 
                    <!-- INSTALMENT INVOICE -->
                    <apex:outputpanel layout="none" rendered="{!AND(NOT(ISNULL(resultInstallmentInvoice)), NOT(ISNULL(resultInstallmentInvoice.id)))}">
                        <table class="tableInstal">
                            <tr>
                                <th>Invoice Number</th>
                                <th>Due Date</th>
                                <th>Issued by</th>
                                <th>Total Amount</th>
                                <th>Received On</th>
                            </tr>
                            <tr>
                                <!-- LINK -->
                                <td><apex:outputLink value="{!link}" target="_blank">{!invoiceNumber}</apex:outputLink></td>

                                <!-- DUE DATE -->
                                <td style="text-align:center">
                                    <apex:outputText value="{0, date, d/MM/yyyy}">
                                        <apex:param value="{!resultInstallmentInvoice.Due_Date__c}" />
                                    </apex:outputText>
                                </td>

                                <!-- REQUESTED BY -->
                                <td>
									<span class="details">{!resultInstallmentInvoice.Sent_Email_Receipt_By__r.Name}</span>
                                </td>
                                <!-- Total Amount -->
                                <td>
									<span class="details">{!resultInstallmentInvoice.Amendment_Receive_From_School__c}</span>
                                </td>
                                <!-- Received On -->
                                <td>
                                    {!resultInstallmentInvoice.Booking_Closed_By__r.name}
									<span class="details">{!resultInstallmentInvoice.Booking_Closed_On__c}</span>
                                     <apex:outputpanel rendered="{!ISNULL(resultInstallmentInvoice.Booking_Closed_On__c)}" layout="block" styleclass="pendingStatus2">
                                        Pending
                                    </apex:outputpanel>
                                </td>
                                
                              
                            </tr>
                        </table>
                    </apex:outputpanel> 
                </apex:outputpanel>
            </td>
        </tr>
    </table>
</apex:page>