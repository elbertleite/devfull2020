<apex:page controller="finance_dashboard" tabstyle="Finance__tab">
    <apex:include pageName="xStylePageV2" />
    <apex:include pageName="xCourseSearchStyle" />
    
    <apex:styleSheet value="{!URLFOR($Resource.chosen,'chosen.css')}" />    
    <apex:includeScript value="{!URLFOR($Resource.chosen,'chosen.jquery.js')}" />
    
    <style>
        .chartHeader{
            display: block;
            padding: 5px;
            font-weight: bold;
            border-bottom: solid 1px #c1c1c1;
            font-size: 1.5em;
            color: #c1c1c1;
            margin: 10px 0;
        }
        
        .filterbox {
            display: block;
            float: left;
            //width: 10%;
            min-width: 160px;
            margin-right: 10px;
        }
        
        .filterbox select { width: 150px; }
        
        
        .filters {          
            border: 1px solid #999999 !important;
            border-radius: 5px;
            box-shadow: 0 0 0 #cccccc;
            height: 35px;
            margin: 1em 0.5em 0.5em 0.5em;
            padding: 15px;
            display: block;         
            background-color: #f3f3f3;
        }
        
        .even{
            background-color: #fff;
        }
        .odd{
            background-color: #f1f1f1;
        }
        
        .even td{
            color: #666 !important;
        }
        
        .odd td{
            color: #666 !important;
        }
        
    </style>
    
    
    
    <table class="tbl_Details">
        <tr>                    
            <td class="sidesColumn">                    
                <c:menuFinance selectedItem="dashFinance" />
            </td>
        
            <td class="middleColumn bodyColumn">    
                <div class="divWrapper" style="margin-top:10px;">
                
                    <div class="title" style="font-size:1.5em;">
                        <span class="arrow-e" style="font-size: 1.5em;"></span>&nbsp;Finance Dashboard

                        <div class="title" style="float:right;">
                            <apex:outputLink value="/help_link?fl={!$CurrentPage.Name}" target="_blank" style="float:right;font-weight: normal;text-decoration: none; color: #777777; font-size: 0.7em;display: block;
                        width: 100%;text-align: right;">
                                <apex:image value="{!urlfor($Resource.icons,'icons/commons/help.png')}" style="height: 16px; width: 16px; position: relative; top: 3px;" />
                                Help
                            </apex:outputLink>
                        </div>

                    </div>
                </div>
                
                <apex:form >
                    <div class="filters">
                        <apex:outputPanel id="formFilters">
                            <div class="filterbox">
                                <apex:selectList value="{!selectedAgencyGroup}" size="1" styleclass="chosen" style="width: 200px; ">
                                     <apex:SelectOptions value="{!agencyGroupOptions}" />
                                     <apex:actionSupport action="{!updateGroup}" event="onchange" rerender="formFilters,charts" status="statusLoad" oncomplete="jQuery('.chosen').chosen();"/>
                                 </apex:selectList>
                                 <apex:outputText value="Agency Group" style="display: block;" /> 
                            </div>
                            <div class="filterbox">
                                <apex:selectList value="{!selectedAgency}" size="1" id="agencyList" styleclass="chosen" style="width: 200px;">
                                     <apex:SelectOptions value="{!agencyOptions}" />
                                     <apex:actionSupport action="{!updateChart}" event="onchange" rerender="charts" status="statusLoad" oncomplete="jQuery('.chosen').chosen();"/>
                                 </apex:selectList>
                                 <apex:outputText value="Agency" style="display: block;" /> 
                            </div>
                        </apex:outputPanel>
                    </div>
                </apex:form>
                
                <apex:outputPanel id="charts">
                    <!-- <apex:outputText value="{!agencyName[selectedAgency]}" rendered="{!NOT(ISNULL(agencyName))}" /> -->
                    <table width="100%">
                        <tr>
                            <td>
                                <div class="chartHeader">Instalments/Commissions received Last 5 months ({!selectedAgencyName})</div>
                                <apex:chart data="{!PaymentPerAgency}" height="400" width="100%" resizable="true">
                                    <apex:legend position="right"/>
                                    <apex:axis type="Numeric" position="left" grid="true"  dashSize="2" fields="sales,commissions" title="Client Payments/Commissions"/>
                                    <apex:axis type="Numeric" position="right" fields="totItems" title="Number Instalments Received" steps="5"/>
                                    <apex:axis type="Category" position="bottom" fields="period" title="">
                                         <apex:chartLabel rotate="315"/>
                                    </apex:axis>
                                    <apex:barSeries orientation="vertical"  axis="left"  xField="period" yField="sales,commissions" groupGutter="0" stacked="false" title="Total Instalments, Total Commissions">
                                        <apex:chartTips labelField="period" height="40" width="220"  />
                                    </apex:barSeries>
                                    <apex:lineSeries axis="right" xField="period" yField="totItems" markerType="cross" markerSize="4" markerFill="#8E35EF" title="Number of Instalments" >
                                        <apex:chartTips labelField="period" height="40" width="220"  />
                                    </apex:lineSeries>
                                </apex:chart>
                            </td>
                            <td style="width:500px">
                                <div class="chartHeader">The 15 Most Enrolled Campuses </div>
                                <apex:pageBlock >
                                    <apex:pageBlockTable value="{!mostEnroledCampuses}" var="r">
                                        <apex:column value="{!r['campus']}">
                                            <apex:facet name="header">Campus</apex:facet>
                                        </apex:column>
                                        <apex:column value="{!r['total']}" style="text-align:right;">
                                            <apex:facet name="header">Total Enrolments</apex:facet>
                                        </apex:column>
                                    </apex:pageBlockTable>
                                </apex:pageBlock>
                            </td>
                        </tr>
                    </table>
                    
                    <!-- <div class="chartHeader">Account Balance last 3 months grouped per week ({!selectedAgencyName})</div>
                    <apex:chart data="{!agencyBalance}" height="400" width="100%" resizable="true">
                        <apex:legend position="right"/>
                        <apex:axis type="Numeric" position="left" grid="true"  dashSize="2" fields="instalment,fromClient,toSchool,balance,discount" title="Total Value"/>
                        <apex:axis type="Category" position="bottom" fields="period" title="">
                             <apex:chartLabel rotate="315"/>
                        </apex:axis>
                        
                        <!-- <apex:lineSeries axis="left" xField="period" yField="fromClient" markerType="circle" markerSize="4" markerFill="#8E35EF" title="Total Received From Client" >
                            <apex:chartTips labelField="period" height="40" width="220"  />
                        </apex:lineSeries>
                        <apex:lineSeries axis="left" fill="true" xField="period" yField="toSchool" markerType="circle" markerSize="4" markerFill="#FF0000" title="Total Paid to School" >
                            <apex:chartTips labelField="period" height="40" width="220"  />
                        </apex:lineSeries>
                        <apex:lineSeries axis="left" fill="true" xField="period" yField="instalment" markerType="circle" markerSize="4" title="Total Instalments">
                            <apex:chartTips labelField="period" height="40" width="220"  />
                        </apex:lineSeries>
                        <apex:lineSeries axis="left" fill="true" xField="period" yField="discount" markerType="circle" markerSize="4" title="Total Discounts">
                            <apex:chartTips labelField="period" height="40" width="220"  />
                        </apex:lineSeries>
                        <apex:lineSeries axis="left" fill="true" xField="period" yField="balance" markerType="circle" markerSize="4" title="Balance">
                            <apex:chartTips labelField="period" height="40" width="220"  />
                        </apex:lineSeries> ->
                        
                        <apex:barSeries orientation="vertical"  axis="left"  xField="period" yField="instalment,fromClient,toSchool,balance,discount" groupGutter="0" stacked="false" title="Total Instalments, Total Received From Client, Total Paid to School, Balance, Total Discounts">
                            <apex:chartTips labelField="period" height="40" width="220"  />
                        </apex:barSeries>
                    </apex:chart> -->
                    
                    <table width="100%">
                        <tr>
                            <td>
                                <div class="chartHeader">Instalments due Next 6 months ({!selectedAgencyName})</div>
                                <apex:chart data="{!PaymentForecast}" height="400" width="100%" resizable="true">
                                    <apex:legend position="right"/>
                                    <apex:axis type="Numeric" position="left" grid="true"  dashSize="2" fields="value,commissions,totItems" title="Total Instalments Value"/>
                                     <apex:axis type="Numeric" position="right" fields="totItems" title="Number Instalments Due" steps="5"/>
                                    <apex:axis type="Category" position="bottom" fields="period" title="">
                                         <apex:chartLabel rotate="315"/>
                                    </apex:axis>
                                    <apex:lineSeries axis="right" xField="period" yField="totItems" markerType="cross" markerSize="4" markerFill="#8E35EF" title="Number of Instalments" >
                                        <apex:chartTips labelField="period" height="40" width="220"  />
                                    </apex:lineSeries>
                                    <apex:lineSeries axis="left" fill="true" xField="period" yField="value" markerType="circle" markerSize="4" markerFill="#FF0000" title="Total Instalment Value" >
                                        <apex:chartTips labelField="period" height="40" width="220"  />
                                    </apex:lineSeries>
                                    <apex:lineSeries axis="left" fill="true" xField="period" yField="commissions" markerType="circle" markerSize="4" title="Total Commission Value">
                                        <apex:chartTips labelField="period" height="40" width="220"  />
                                    </apex:lineSeries>
                                </apex:chart>
                            </td>
                            <td style="width:500px">
                                <div class="chartHeader">The 15 Most Enrolled Courses </div>
                                <apex:pageBlock >
                                    <apex:pageBlockTable value="{!mostEnroledCourses}" var="r" rowClasses="even,odd">
                                        <apex:column value="{!r['campus']}">
                                            <apex:facet name="header">Campus</apex:facet>
                                        </apex:column>
                                        <apex:column value="{!r['course']}">
                                            <apex:facet name="header">Course</apex:facet>
                                        </apex:column>
                                        <apex:column value="{!r['total']}"  style="text-align:right;">
                                            <apex:facet name="header">Total Enrolments</apex:facet>
                                        </apex:column>
                                    </apex:pageBlockTable>
                                </apex:pageBlock>
                            </td>
                        </tr>
                    </table>
                
                </apex:outputPanel>
            </td>
        </tr>
    </table>

    <script>

        jQuery( document ).ready(function() {
            jQuery('.chosen').chosen();
        });
        
    </script>
    
</apex:page>