<apex:page controller="report_leads_per_quotation" showHeader="true" docType="html-5.0" lightningStyleSheets="false" readOnly="true" tabStyle="Reports__tab">
    <apex:slds />
    <!-- <apex:include pageName="xCourseSearchStyle" /> --> 
	<apex:includeScript value="{!$Resource.up_jQuery}"/>
	<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" />

	<apex:styleSheet value="{!URLFOR($Resource.up_chosen,'chosen.css')}" />
    <apex:includeScript value="{!URLFOR($Resource.up_chosen,'chosen.jquery.js')}" />
    
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <apex:includeScript value="{!URLFOR($Resource.amcharts,'amcharts/amcharts.js')}" />

	<apex:includeScript value="{!URLFOR($Resource.amcharts,'amcharts/serial.js')}" />
	<apex:includeScript value="{!URLFOR($Resource.amcharts,'amcharts/themes/light.js')}" />

    <style>
        .chosen, .dateSearchInput, .chosen-container{
            width: 100% !important;
        }
        .dateSearchInput{
            border: 1px solid #aaa;
            border-radius: 5px;
            height: 25px;
            padding-left: 10px;
        }

        .container-input{
            margin-top: 10px;
        }
        
        .amcharts-chart-div{
            min-height: 400px;
        }

        .amcharts-chart-div svg{
            /*position: relative;*/
        }

        .amcharts-chart-div > a{
           /* display: none !important;*/
        }

        h1.header{
            font-weight: 700;
            color: rgb(62, 62, 60);
            text-transform: uppercase;
            font-size: .925rem;
            letter-spacing: .0625em;
            line-height: 1.25;
            margin: 10px;
        }

        .list-contacts-closed{
            display: none;
        }
        .show-children, .hide-children{
            cursor: pointer;
        }
        .show-children svg{
            transform: rotate(-90deg);
        }
        .hide-children svg{
            transform: rotate(180deg);
        }
    </style>
    <script type="text/Javascript">
        $(document).ready(function(){
			beginAjaxRequest();
            $('.chosen').chosen();
			$(".dateSearchInput").datepicker({dateFormat:'dd/mm/yy'});
            //generateReport();
            showModalLoading(false);
			$(document).on("change", ".chosenGroup", function() {
				beginAjaxRequest();
				updateAgencies($(this).val());
			});
            $(document).on("change", ".chosenAgency", function() {
				beginAjaxRequest();
				updateEmployees($(this).val());
			});
            $(document).on("change", ".chosenSchool", function() {
				beginAjaxRequest();
				updateCampuses($(this).val());
			});
            $(document).on("change", ".chosenCountry", function() {
				beginAjaxRequest();
				updateSchools($(this).val());
			});
            $(document).on("click", ".show-children", function() {
                var id = $(this).attr("id");
                $(this).closest("table").find("."+id).show();
                $(this).removeClass("show-children");
                $(this).addClass("hide-children");
            });
            $(document).on("click", ".hide-children", function() {
                var id = $(this).attr("id");
                $(this).closest("table").find(".inner-"+id).hide();
                $(this).closest("table").find(".inner-"+id).removeClass("hide-children");
                $(this).closest("table").find(".inner-"+id).addClass("show-children");
                $(this).addClass("show-children");
                $(this).removeClass("hide-children");
            });
		});
        function beginAjaxRequest(){
			showModalLoading(true);
		}

		function completeAjaxRequest(){
			$(".dateSearchInput").datepicker({dateFormat:'dd/mm/yy'});
			$('.chosen').chosen();
			showModalLoading(false);
		}
        function rebuildGraphs(){
            beginAjaxRequest();
            generateReport();
            completeAjaxRequest();
        }
        
    </script>
    <c:modalLoadingLightning />
    <apex:outputPanel layout="block" id="container-report" styleClass="slds-scope">
        <script type="text/Javascript">
            function generateReport(){
                if('{!graphResult}' != ''){
                    var graph = JSON.parse('{!graphResult}');
                    AmCharts.makeChart("report", {
                        "type": "serial",
                        "theme": "light",
                        "dataProvider": graph,
                        "creditsPosition" : "bottom-right",
                        "valueAxes": [{
                            "stackType": "regular",
                            "axisAlpha": 0.5,
                            "gridAlpha": 0
                        }],
                        "graphs": [
                        {
                            "balloonText": "<b>[[title]]: </b><span style='font-size:14px'><b>[[value]]</b></span>",
                            "fillAlphas": 0.8,
                            "labelText": "[[value]]",
                            "lineAlpha": 0.3,
                            "title": "Total Quotations For [[category]]",
                            "type": "column",
                            "color": "#000000",
                            "valueField": "totalQuotations"
                        }
                        ],
                        "rotate": true,
                        "categoryField": "schoolName",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "axisAlpha": 0,
                            "gridAlpha": 0,
                            "position": "left"
                        },
                        "export": {
                            "enabled": true
                        }
                    });
                }
            }
        </script>
        <div class="slds-grid slds-gutters slds-wrap" style="background-color: #fbfbfb;">
            <div class="slds-col slds-size_1-of-1" style="text-align: right; margin-bottom: 5px;">
                <apex:outputLink value="/help_link?fl=report_leads_per_quotation" target="_blank" style="font-weight: normal;text-decoration: none;  color: #777777;">
                    <apex:image value="{!urlfor($Resource.icons,'icons/commons/help.png')}" style="height: 16px; width: 16px; top: -2px; position: relative" />
                    Help
                </apex:outputLink>
            </div>
            <div class="slds-col slds-size_1-of-1">
                <h1 class="header">Contacts Per School Quotation</h1>
            </div>
            <div class="slds-col slds-size_1-of-1">
                <apex:form id="form-filters">
                    <apex:actionFunction name="updateAgencies" action="{!updateAgencies}" reRender="form-filters" oncomplete="completeAjaxRequest();">
                        <apex:param name="idGroup" value=""/>
                    </apex:actionFunction>
                    <apex:actionFunction name="updateEmployees" action="{!updateEmployees}" reRender="form-filters" oncomplete="completeAjaxRequest();">
                        <apex:param name="idAgency" value=""/>
                    </apex:actionFunction>
                    <apex:actionFunction name="updateCampuses" action="{!updateCampuses}" reRender="form-filters" oncomplete="completeAjaxRequest();">
                        <apex:param name="idSchool" value=""/>
                    </apex:actionFunction>
                    <apex:actionFunction name="updateSchools" action="{!updateSchools}" reRender="form-filters" oncomplete="completeAjaxRequest();">
                        <apex:param name="country" value=""/>
                    </apex:actionFunction>
                    <div class="slds-clearfix" style="padding: 5px 10px;">					
                        <h1 style="font-weight: bold">FILTERS</h1>
                        <div class="slds-grid slds-gutters slds-wrap">
                            
                            <div class="slds-col slds-size_1-of-5">
                                <div class="container-input slds-form-element">
                                    <legend class="slds-form-element__label sub-folders">
                                        Group
                                    </legend>
                                    <apex:selectList value="{!groupSelected}" styleClass="chosenGroup chosen" disabled="{!NOT(user.Contact.Group_View_Permission__c)}">
                                        <apex:SelectOptions value="{!groups}" />					
                                    </apex:selectList>
                                </div>
                                <div class="container-input slds-form-element">
                                    <legend class="slds-form-element__label sub-folders">
                                        Agency
                                    </legend>
                                    <apex:selectList value="{!agencySelected}" styleClass="chosenAgency chosen" disabled="{!AND(NOT(user.Contact.Group_View_Permission__c),ISNULL(user.Aditional_Agency_Managment__c))}">
                                        <apex:SelectOptions value="{!agencies}" />
                                    </apex:selectList>
                                </div>
                            </div>
                            
                            <div class="slds-col slds-size_1-of-5">
                                <div class="container-input slds-form-element">
                                    <legend class="slds-form-element__label sub-folders">
                                        Employee
                                    </legend>
                                    <apex:selectList value="{!employeeSelected}" styleClass="chosen" disabled="{!NOT(user.Contact.Group_View_Permission__c) && NOT(user.Contact.Agency_View_Permission__c) && ISNULL(user.Aditional_Agency_Managment__c)}">
                                        <apex:SelectOptions value="{!employees}" />
                                    </apex:selectList>
                                </div>
                                <div class="container-input slds-form-element">
                                    <legend class="slds-form-element__label sub-folders">
                                        Destination
                                    </legend>
                                    <apex:selectList value="{!countrySelected}" styleClass="chosenCountry chosen">
                                        <apex:SelectOptions value="{!countries}" />
                                    </apex:selectList>
                                </div>
                            </div>

                            <div class="slds-col slds-size_1-of-5">
                                <div class="container-input slds-form-element">
                                    <legend class="slds-form-element__label sub-folders">
                                        School
                                    </legend>
                                    <apex:selectList value="{!schoolSelected}" styleClass="chosenSchool chosen">
                                        <apex:SelectOptions value="{!schools}" />
                                    </apex:selectList>
                                </div>
                                <div class="container-input slds-form-element">
                                    <legend class="slds-form-element__label sub-folders">
                                        Campus
                                    </legend>
                                    <apex:selectList value="{!campusSelected}" styleClass="chosen">
                                        <apex:SelectOptions value="{!campuses}" />
                                    </apex:selectList>
                                </div>
                            </div>

                            <div class="slds-col slds-size_1-of-5">
                                <div class="container-input slds-form-element">
                                    <legend class="slds-form-element__label sub-folders">
                                        From Date
                                    </legend>
                                    <apex:inputText styleClass="inputText dateSearchInput" value="{!beginDate}"/>
                                </div>
                                <div class="container-input slds-form-element">
                                    <legend class="slds-form-element__label sub-folders">
                                        To Date
                                    </legend>
                                    <apex:inputText styleClass="inputText dateSearchInput" value="{!endDate}"/>
                                </div>
                            </div>

                            <div class="slds-col slds-size_1-of-5">
                                <div class="container-input slds-form-element" style="text-align: center">
                                    <legend class="slds-form-element__label sub-folders">
                                        &nbsp;
                                    </legend> 
                                    <legend class="slds-form-element__label sub-folders">
                                        &nbsp;
                                    </legend> 
                                </div>
                                <div class="container-input slds-form-element" style="text-align: center">
                                    <!-- <legend class="slds-form-element__label sub-folders">
                                        &nbsp;
                                    </legend>  -->
                                    <apex:commandLink styleClass="slds-button slds-button_brand agencyButton" action="{!generateReport}" value="Generate Report" reRender="container-report" onclick="beginAjaxRequest();" oncomplete="rebuildGraphs();" style="padding: 0px 10px !important; margin-top: 20px !important;"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </apex:form>
            </div>
            <apex:outputPanel rendered="{!NOT(startingPage) && resultReport == null || resultReport.empty}" layout="block" styleClass="slds-col slds-size_1-of-1" style="background-color: white;">
                <h1 class="header" style="text-align: center; margin: 20px 0px 15px 0px;">
                    No results were found with these filters.
                </h1>
            </apex:outputPanel>
            <apex:outputPanel rendered="{!resultReport != null && NOT(resultReport.empty)}" layout="block" styleClass="slds-col slds-size_1-of-1" style="background-color: white;">
                <h1 class="header" style="text-align: center; margin: 30px 0px 15px 0px;">
                    Result
                </h1>
                <!-- <div id="report"></div> -->
            </apex:outputPanel>
            <apex:outputPanel rendered="{!resultReport != null && NOT(resultReport.empty)}" layout="block" styleClass="slds-col slds-size_1-of-1" style="background-color: white; margin-top: 10px;">
                <div style="margin-bottom: 25px;">
                    <apex:outputPanel layout="block" styleClass="slds-grid slds-gutters slds-wrap">
                        <apex:outputPanel layout="block" styleClass="slds-col slds-size_1-of-2" rendered="{!resultTooBig}" style="margin-top: 10px; color: #bf1d1d;">
                            Your data is too large. Showing the first 500 rows. To see all the results please export to excel.
                        </apex:outputPanel>
                        <apex:outputPanel layout="block" styleClass="slds-col {!IF(resultTooBig, 'slds-size_1-of-2', 'slds-size_1-of-1')}" style="text-align: right">
                            <apex:outputLink styleClass="slds-button slds-button_brand agencyButton" rendered="{!canExportToExcel}" value="report_leads_per_quotation_excel?export=true&group={!groupSelected}&agency={!agencySelected}&begin={!beginDate}&end={!endDate}&school={!schoolSelected}&campus={!campusSelected}&employee={!employeeSelected}" target="_blank" style="margin-bottom: 15px;">
                                Export Leads to Excel
                            </apex:outputLink>
                        </apex:outputPanel>
                    </apex:outputPanel>
                    <table class="slds-table slds-table_bordered slds-max-medium-table_stacked-horizontal slds-table_cell-buffer slds-table_col-bordered">
                        <tbody>
                            <apex:repeat var="result" value="{!resultReport}">
                                <tr id="{!result.country}" class="slds-line-height_reset {!IF(countrySelected == '', 'show-children', 'hide-children')}">
                                    <th class="slds-text-title_caps" scope="col" style="font-weight: bold; background-color: #ecebeb; color: #005fb2;" colspan="10">    
                                        <a href="javascript:void(0);" class="slds-button slds-button_icon slds-button_icon slds-context-bar__button">
                                            <svg class="slds-button__icon" aria-hidden="true">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#chevrondown')}" />
                                            </svg>
                                            <span class="slds-assistive-text">{!result.country} - {!result.totalPerCountry} Quotations</span>
                                        </a>&nbsp;{!result.country} - {!result.totalPerCountry} Quotations
                                    </th>
                                </tr>
                                <apex:repeat var="school" value="{!result.schools}">
                                    <tr id="{!school.schoolID}" class="slds-line-height_reset {!IF(schoolSelected == '', 'show-children', 'hide-children')} {!result.country} inner-{!result.country}" style="display: {!IF(countrySelected == '', 'none', 'table-row')};">
                                        <th class="slds-text-title_caps" scope="col" style="font-weight: bold; background-color: #f7f7f7;" colspan="10">    
                                            <a href="javascript:void(0);" class="slds-button slds-button_icon slds-button_icon slds-context-bar__button" style="margin-left: 15px;">
                                                <svg class="slds-button__icon" aria-hidden="true">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#chevrondown')}" />
                                                </svg>
                                                <span class="slds-assistive-text">{!school.schoolName} - {!school.totalPerSchool} Quotations</span>
                                            </a>&nbsp;{!school.schoolName} - {!school.totalPerSchool} Quotations
                                        </th>
                                    </tr>
                                    <apex:repeat var="campus" value="{!school.campuses}">
                                        <tr id="{!campus.campusID}" class="slds-line-height_reset {!IF(campusSelected == '', 'show-children', 'hide-children')} inner-{!result.country} inner-{!school.schoolID} {!school.schoolID}" style="display: {!IF(schoolSelected == '', 'none', 'table-row')};">
                                            <th class="slds-text-title_caps" scope="col" style="font-weight: bold; background-color: white;" colspan="10">    
                                                <a href="javascript:void(0);" class="slds-button slds-button_icon slds-button_icon slds-context-bar__button" style="margin-left: 40px;">
                                                    <svg class="slds-button__icon" aria-hidden="true">
                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#chevrondown')}" />
                                                    </svg>
                                                    <span class="slds-assistive-text">{!campus.campusName} - {!campus.totalPerCampus} Quotations</span>
                                                </a>&nbsp;{!campus.campusName} - {!campus.totalPerCampus} Quotations
                                            </th>
                                        </tr>
                                        <tr class="slds-line-height_reset inner-{!result.country} inner-{!school.schoolID} inner-{!campus.campusID} {!campus.campusID}" style="display: {!IF(campusSelected == '', 'none', 'table-row')}">      
                                            <th class="slds-text-title_caps" scope="col">
                                                <div class="slds-truncate" title="Opportunity Name" style="font-weight: bold; text-transform: uppercase;">Quotation</div>
                                            </th>
                                            <th class="slds-text-title_caps" scope="col" style="text-align: center;">
                                                <div class="slds-truncate" title="Opportunity Name" style="font-weight: bold; text-transform: uppercase;">Visualized (Opened)</div>
                                            </th>
                                            <th class="slds-text-title_caps" scope="col">
                                                <div class="slds-truncate" title="Opportunity Name" style="font-weight: bold; text-transform: uppercase;">Course</div>
                                            </th>
                                            <th class="slds-text-title_caps" scope="col">
                                                <div class="slds-truncate" title="Opportunity Name" style="font-weight: bold; text-transform: uppercase; text-align: center;">Duration</div>
                                            </th>
                                            <th class="slds-text-title_caps" scope="col">
                                                <div class="slds-truncate" title="Opportunity Name" style="font-weight: bold; text-transform: uppercase;">Contact Name</div>
                                            </th>
                                            <th class="slds-text-title_caps" scope="col">
                                                <div class="slds-truncate" title="Opportunity Name" style="font-weight: bold; text-transform: uppercase;">Contact Email</div>
                                            </th>
                                            <th class="slds-text-title_caps" scope="col">
                                                <div class="slds-truncate" title="Opportunity Name" style="font-weight: bold; text-transform: uppercase;">Owner</div>
                                            </th>
                                            <th class="slds-text-title_caps" scope="col">
                                                <div class="slds-truncate" title="Opportunity Name" style="font-weight: bold; text-transform: uppercase;">Campus</div>
                                            </th>
                                            <th class="slds-text-title_caps" scope="col">
                                                <div class="slds-truncate" title="Opportunity Name" style="font-weight: bold; text-transform: uppercase;">Quotation Created By</div>
                                            </th>
                                            <th class="slds-text-title_caps" scope="col">
                                                <div class="slds-truncate" title="Opportunity Name" style="font-weight: bold; text-transform: uppercase;">Quotation Created Date</div>
                                            </th>
                                        </tr>
                                        <apex:repeat var="contactRelated" value="{!campus.contacts}">
                                            <tr class="slds-hint-parent inner-{!result.country} inner-{!school.schoolID} inner-{!campus.campusID} {!campus.campusID}" style="display: {!IF(campusSelected == '', 'none', 'table-row')}">
                                                <th data-label="Opportunity Name" scope="row">
                                                    <div class="slds-truncate">
                                                        <apex:outputLink value="quotation?id={!contactRelated.quotationID}" target="_blank">
                                                            {!contactRelated.quotation}
                                                        </apex:outputLink>
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="slds-truncate" style="text-align: center;">
                                                        {!contactRelated.numberOfTimesOpened}
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="slds-truncate">
                                                        {!contactRelated.course}
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="slds-truncate" style="text-align: center">
                                                        {!contactRelated.courseDurationStr}
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="slds-truncate">
                                                        <apex:outputLink value="contact_new_activity_log?id={!contactRelated.contactID}" target="_blank">
                                                            {!contactRelated.contactName}
                                                        </apex:outputLink>
                                                    </div>
                                                </th>
                                                <th data-label="Opportunity Name" scope="row">
                                                    <div class="slds-truncate">
                                                        {!contactRelated.contactEmail}
                                                    </div>
                                                </th>
                                                <th data-label="Opportunity Name" scope="row">
                                                    <div class="slds-truncate">
                                                        <div>
                                                            {!contactRelated.contactOwner}
                                                        </div>
                                                        <div style="font-size: .7rem;">
                                                            {!contactRelated.contactAgency}
                                                        </div>
                                                    </div>
                                                </th>
                                                <th data-label="Opportunity Name" scope="row">
                                                    <div class="slds-truncate">
                                                        {!contactRelated.campus}
                                                    </div>
                                                </th>
                                                <th data-label="Opportunity Name" scope="row">
                                                    <div class="slds-truncate">
                                                        <div>
                                                            {!contactRelated.quotationCreatedBy}
                                                        </div>
                                                        <div style="font-size: .7rem;">
                                                            {!contactRelated.quotationCreatedAgency}
                                                        </div>
                                                    </div>
                                                </th>
                                                <th data-label="Opportunity Name" scope="row">
                                                    <div class="slds-truncate">
                                                        <apex:outputText value=" {!contactRelated.quotationCreatedDate}" />
                                                    </div>
                                                </th>
                                            </tr>
                                        </apex:repeat>
                                    </apex:repeat>
                                </apex:repeat>
                            </apex:repeat>
                        </tbody>
                    </table>
                </div>
            </apex:outputPanel>
        </div>
    </apex:outputPanel>
</apex:page>