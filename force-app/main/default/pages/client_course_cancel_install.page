<apex:page standardController="client_course__c" extensions="client_course_cancel_install" showHeader="false" sidebar="false" docType="html-5.0">
	<apex:include pageName="xStylePage" />
	<apex:include pageName="xCourseSearchStyle" />
	
	<style>
		.content {			
			background-color: white;
			margin: 3px;
			border: solid 1px #c1c1c1;
		}
		
	
		.tableInstall td{
			padding: 3px 5px;
			//text-align: right;
		}
		
		
		
		.tableInstall tr:nth-child(even)		{ background-color:#f1f1f1; }
		.tableInstall tr:nth-child(odd)		{ background-color:#FFF; }
		
		
		.extraInstallment td{
			font-weight: bold;
			color: #0000AB;
		}
		
		.closeBar{
		    background-color: #E1E1E1;
		    border-top: 1px solid #808080;
		    bottom: 0;
		    display: block;
		    height: 15px;
		    padding: 10px 0;
		    position: fixed;
		    text-align: center;
		    width: 100%;
		}
		
		.noCancelInstall td{
			//font-weight: bold;
			//color: #6EB300;
		}
		
		.errorMsg{
			font-weight: bold;
    		text-align: left;
		}
		
		
		.requiredInput {
		    height: 100%;
		    position: static !important;
		}
		.requiredInput {
		    position: relative;
		}
		.requiredInput {
		    border-left: 3px solid #cc0000;
		    left: -2px;
		    width: 100%;
		}
		
		.tableInstall{
			font-size: 1.2em;
			width: 100%;
			border: solid 1px #ccc;
			border-spacing: 0;
		}
		
		.tableInstall th{
			border-bottom: 1px solid #ccc;
			border-right: 1px solid #ccc;
		}
		
		.tableInstall th:last-child, .tableInstall th:first-child{
			border-right: 0px;
		}
		
		.message {
			margin: 2px;
		}
		
		.wholePage{
			overflow-y:auto;
			height:560px;
		}
		
		.installDetails{
			width: 100%;
		}
		
		.installDetails tr.name td{
			//background-color: #808080;
		    //color: #dc2628;
		    font-size: 1.4em;
		    font-weight: bold;
		}
		
		.tbTitle{
			width:15%;
			font-weight:bold;
		}
		
		.tbContent{
			width:15%;
		}
		
		.tbContent2{
			width:50%;
		}
	</style>
	
	<script>
		function toggle(popUpDiv) {
			var el = document.getElementById(popUpDiv);
			if ( el.style.display == 'none' ) { el.style.display = '';}
			else {el.style.display = 'none';}
			return false;
		}	
	</script>
	
	<apex:form >
		<div class="wholePage" >
		<div class="content">
		
			<table class="installDetails">
				<tr>
					<td class="tbTitle"> Client Name </td>
					<td class="tbContent2"> {!course.Client__r.name}</td>
					
					<td class="tbTitle"> Course Duration</td>
					<td class="tbContent">
						<apex:outputField value="{!course.Course_Length__c}" />
						<apex:outputText value="{!course.Unit_Type__c}" style="margin-left: 5px;" />
						<apex:outputText value="s" rendered="{!course.Course_Length__c > 1}" />
					</td>
				</tr>
				
				<tr>
					<td class="tbTitle"> School / Campus </td>
					<td class="tbContent2"> {!course.School_Name__c} - {!course.Campus_Name__c}</td>
					
					<td class="tbTitle"> Tuition Value</td>
					<td class="tbContent">
						<apex:outputText value="{0, number, currency}" >
							<apex:param value="{!course.Total_Tuition__c}" />
						</apex:outputText>
						&nbsp;
						<apex:outputfield value="{!course.CurrencyIsoCode__c}" />
					</td>
				</tr>
				
				<tr>
					<td class="tbTitle"> Course </td>
					<td class="tbContent2"> {!course.Course_Name__c}</td>
					
					<td class="tbTitle"> Extra Fees Value</td>
					<td class="tbContent">
						<apex:outputText value="{0, number, currency}" >
							<apex:param value="{!course.Total_Extra_Fees__c}" />
						</apex:outputText>
						&nbsp;
						<apex:outputfield value="{!course.CurrencyIsoCode__c}" />
					</td>
				</tr>
				
				<tr>
					<td class="tbTitle"> Course Period </td>
					<td class="tbContent2"> 
						<apex:outputField value="{!course.Start_Date__c}"  />
							&nbsp;-&nbsp;
						<apex:outputField value="{!course.End_Date__c}"  />
					</td>
					
					<td class="tbTitle"> Total Course Value</td>
					<td class="tbContent2">
						<apex:outputText value="{0, number, currency}" >
							<apex:param value="{!course.Total_Course__c}" />
						</apex:outputText>
						&nbsp;
						<apex:outputfield value="{!course.CurrencyIsoCode__c}" />
					</td>
				</tr>			
			</table>
		
					
		</div>
		
		
		<apex:pageMessage severity="info" strength="2" id="infoMsg" escape="false" summary="You can only cancel installments that are <b>not inside a grouped invoice</b>. <br/> 
																							You must cancel the group invoice before you cancel the installment."  title="Info" rendered="{!hasGroupedInvoice}"/>
		<apex:outputPanel layout="block"  id="pnlInstall" >
			<div class="content">
				
				<div style="display: block; margin-bottom: 10px;">
					<div class="divHeader">Cancellation Reason</div>
					<div style="padding: 5px;">
						<apex:inputField value="{!reason.Cancelled_Reason__c}" required="true" style="padding: 3px; width: 99%;"/>
					</div>
				</div>			
			
				<div class="divHeader">Installments</div>
				<table class="tableInstall" cellpadding="2px">
					<tr>
						<th style="width:15px;"></th>
						<th style="width:65px;">Cancel</th>
						<th>Number</th>
						<th>Status</th>
						<th style="text-align: left">Due Date</th>
						<th>Tuition</th>
						<th>Extra Fee</th>
						<th>Total</th>
					</tr>
					<apex:repeat value="{!course.client_course_instalments__r}" var="fi">
						<tr > <!-- class="{!IF(AND(ISNULL(fi.Received_Date__c), ISNULL(fi.Cancelled_On__c)), '', 'noCancelInstall')}" -->
							<td style="text-align: center;">
								<apex:commandLink status="statusLoad" onClick="toggle('pnlReason-{!fi.id}'); toggle('{!$Component.buttonMaxDp}'); toggle('{!$Component.buttonMinDp}'); return false;" rendered="{!NOT(ISNULL(fi.Cancelled_Reason__c))}" >
									<apex:image value="{!URLFOR($Resource.assets,'images/button-maximize.png')}" id="buttonMaxDp" styleclass="arrow" />
									<apex:image value="{!URLFOR($Resource.assets,'images/button-minimize.png')}" id="buttonMinDp" styleclass="arrow" style="display:none"/>
								</apex:commandLink>
							</td>
							<td >
								<apex:inputField value="{!fi.isCancelled__c}" rendered="{!AND(ISNULL(fi.Received_Date__c), ISNULL(fi.Cancelled_On__c), ISNULL(fi.Invoice__c))}" />
								
								
							</td>
							
							<td >
								{!fi.Number__c}
								<apex:outputText value="." rendered="{!NOT(ISNULL(fi.Split_Number__c))}" style="{!IF(fi.Due_Date__c < today(),'color:red; font-weight: bold;','')}" />
								<apex:outputText value="{!fi.Split_Number__c}" rendered="{!NOT(ISNULL(fi.Split_Number__c))}" style="{!IF(fi.Due_Date__c < today(),'color:red; font-weight: bold;','')}"/>
														
							
							</td>
							
							<td >
								
								<apex:outputText value="Cancelled" styleClass="paymentCancceled" rendered="{!fi.isCancelled__c}" />
								<apex:outputText value="Paid" styleClass="paymentPaid" rendered="{!NOT(ISNULL(fi.Received_Date__c)) && NOT(fi.isPDS__c)}" />
								<apex:outputText value="Open" styleClass="paymentOpen" rendered="{!AND(ISNULL(fi.Received_Date__c),NOT(fi.isCancelled__c), fi.Due_Date__c >= today())}" />
								
								<apex:outputText value="PDS" styleClass="paymentPDS" rendered="{!fi.isPDS__c}" />
								<apex:outputText value="Overdue" styleClass="paymentOverdue" rendered="{!ISNULL(fi.Received_Date__c) && fi.Due_Date__c < today() && NOT(fi.isCancelled__c)}" />
								
								<apex:outputpanel layout="block" style="display:block;" rendered="{!NOT(ISNULL(fi.Invoice__c))}">
									<span style="font-weight:bold;font-size: 0.7em !important;">Grouped Invoice</span>																
								</apex:outputpanel>
								
							</td>
							
							
							<td ><apex:outputField value="{!fi.Due_Date__c}" /></td>
							<td style="text-align: right;">
								<apex:outputPanel style="{!IF(fi.Tuition_Value__c == 0 , 'color: red;', '')}">
									<apex:outputField value="{!fi.Tuition_Value__c}" />
								</apex:outputPanel>
							</td>
							<td style="text-align: right;"><apex:outputField value="{!fi.Extra_Fee_Value__c}" /></td>
							<td style="text-align: right;">
								<apex:outputText value="{0, number, currency}" >
									<apex:param value="{!fi.Instalment_Value__c}" />
								</apex:outputText>
							</td>
						</tr>
						<tr style="display:none;" id="pnlReason-{!fi.id}">
							<td></td>
							<td colspan="6" style="padding: 5px; text-align: left;" >
								<div class="content" style="display: block;">
									<div class="divHeader">Cancellation Reason</div>
									<div style="padding: 5px;">
										<apex:outputField value="{!fi.Cancelled_Reason__c}" />
									</div>
								</div>			
								
							</td>
						</tr>
						<tr style="display: none" />
					</apex:repeat>
						
				</table>
			</div>
			
		</apex:outputPanel>
		
		<!-- <div style="text-align: center; padding:5px; display:block; margin-top: 10px; margin-bottom: 50px;">
			<apex:commandLink action="{!cancelInstalments}"  value="Cancel Selected Installments" status="statusUpdate" styleClass="agencyButton" rerender="pnlInstall" onclick="if(!confirm('Are you sure you want to Delete the selected installment(s)')) return false;"/>
		</div> -->
			
		</div>	
		
		<div class="closeBar">
			<apex:commandLink value="Close" styleClass="agencyButton" onclick="window.parent.UpdateOpener(); return false;" />
			<apex:commandLink action="{!cancelInstalments}"  value="Cancel Selected Installments" status="statusUpdate" styleClass="agencyButton" rerender="pnlInstall" onclick="if(!confirm('Are you sure you want to Delete the selected installment(s)')) return false;"/>
		</div>
	</apex:form>
</apex:page>