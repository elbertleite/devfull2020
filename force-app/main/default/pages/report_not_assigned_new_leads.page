<apex:page controller="report_not_assigned_new_leads" showHeader="true" docType="html-5.0" lightningStyleSheets="true" tabStyle="reports__tab">
	
	<apex:slds />
	<apex:include pageName="xCourseSearchStyle" />
	<apex:includeScript value="{!$Resource.up_jQuery}"/>
	<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" />

	<apex:styleSheet value="{!URLFOR($Resource.up_chosen,'chosen.css')}" />
    <apex:includeScript value="{!URLFOR($Resource.up_chosen,'chosen.jquery.js')}" />

	<apex:includeScript value="{!$Resource.jqueryBlockUI}" />
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

	<style>
		.table-result tr th, .table-result tr td{
			border-left: 1px solid rgb(217, 219, 221);
			border-right: 1px solid rgb(217, 219, 221);
		}
		.container-input{
			margin: 0px 10px;
		}
		.inputText {
			width: 200px;
			height: 27px;
			border: 1px solid #aaa;
			border-radius: 5px;
			padding-left: 10px;
		}
		.chosen-container{
			width: 200px !important;
		}

		.slds-scope [hidden], .slds-scope template {
			display: block !important;
		}

		h2.subheader-content {
			font-weight: 700;
			color: rgb(62, 62, 60);
			text-transform: uppercase;
			font-size: .925rem;
			letter-spacing: .0625em;
			line-height: 1.25;
			padding-bottom: 15px;
			margin: 5px 10px 5px 10px;
		}
	</style>
	<script type="text/Javascript">
		$(document).ready(function(){
			$('.chosen').chosen();
			$(".dateSearchInput").datepicker({dateFormat:'dd/mm/yy'});
			$(document).on("change", ".chosenGroup", function() {
				 beginAjaxRequest()
				updateAgencies($(this).val());
			});
		});
		function beginAjaxRequest(){
			showModalLoading(true);
		}

		function completeAjaxRequest(){
			$('.chosen').chosen();
			$(".dateSearchInput").datepicker({dateFormat:'dd/mm/yy'});
			showModalLoading(false);
		}
	</script>
    <c:modalConfirmLightning />
    <c:modalLoadingLightning />
	<apex:outputPanel layout="block" id="container-report" styleClass="slds-scope">
		<apex:form id="form-filters">
			<apex:actionFunction name="updateAgencies" action="{!updateAgencies}" reRender="form-filters" oncomplete="completeAjaxRequest();">
				<apex:param name="idGroup" value=""/>
			</apex:actionFunction>
			<div class="slds-grid slds-gutters slds-wrap">
				<div class="slds-col slds-size_1-of-1" style="text-align: right; margin-bottom: 5px;">
                    <apex:outputLink value="/help_link?fl=report_not_assigned_new_leads" target="_blank" style="font-weight: normal;text-decoration: none;  color: #777777;">
                        <apex:image value="{!urlfor($Resource.icons,'icons/commons/help.png')}" style="height: 16px; width: 16px; top: -2px; position: relative" />
                        Help
                    </apex:outputLink>
                </div>
				<div class="slds-col slds-size_1-of-1">
					<div class="slds-clearfix" style="padding: 10px; border: 1px solid #d8dde6; border-radius: .5rem;">					
						<div class="container-input slds-form-element slds-float_left">
							<legend class="slds-form-element__label sub-folders">
								Group
							</legend>
							<apex:selectList value="{!groupSelected}" styleClass="chosenGroup chosen" disabled="{!NOT(user.Contact.Group_View_Permission__c)}">
								<apex:SelectOptions value="{!groups}" />					
							</apex:selectList>
						</div>
						<div class="container-input slds-form-element slds-float_left">
							<legend class="slds-form-element__label sub-folders">
								Agency
							</legend>
							<apex:selectList value="{!agencySelected}" styleClass="chosen" disabled="{!AND(NOT(user.Contact.Group_View_Permission__c),ISNULL(user.Aditional_Agency_Managment__c))}">
								<apex:SelectOptions value="{!agencies}" />
							</apex:selectList>
						</div>
						<div class="container-input slds-form-element slds-float_left" ng-show="dateFilter != 'None'">
							<legend class="slds-form-element__label sub-folders">
								From Date (Lead Creation)
							</legend>
							<apex:inputText styleClass="inputText dateSearchInput" value="{!beginDate}"/>
						</div>
						<div class="container-input slds-form-element slds-float_left" ng-show="dateFilter != 'None'">
							<legend class="slds-form-element__label sub-folders">
								To Date (Lead Creation)
							</legend>
							<apex:inputText styleClass="inputText dateSearchInput" value="{!endDate}"/>
						</div>
						<div class="container-input slds-form-element slds-float_left">
							<legend class="slds-form-element__label sub-folders">
								&nbsp;
							</legend> 
							<apex:commandLink styleClass="slds-button slds-button_brand agencyButton" action="{!generateNewReport}" value="Generate Report" reRender="container-report" onclick="beginAjaxRequest();" oncomplete="completeAjaxRequest();"/>
						</div>
					</div>
				</div>
			</div>
		</apex:form>
		<div class="slds-grid slds-gutters slds-wrap">
			<apex:outputPanel layout="block" styleClass="slds-col" rendered="{!contacts != null && contacts.size > 0}" style="margin: 40px 0px;">
				<apex:chart height="400" width="100%" data="{!dataGraph}" resizable="true">
					<apex:axis type="Numeric" position="left" fields="value" title="Total Leads not assigned" dashSize="5" minimum="0" steps="1"/>
					<apex:axis type="Category" position="bottom" fields="title">
						<apex:chartLabel rotate="{!IF(dataGraph.size > 5 , 315, 0)}"/>
					</apex:axis>
					<apex:barSeries orientation="vertical" axis="left" xField="title" yField="value">
						<apex:chartTips height="40" width="220"/>
					</apex:barSeries>
				</apex:chart>
			</apex:outputPanel>
		</div>
		<div class="slds-grid slds-gutters slds-wrap" style="margin-top: 20px;">
			<apex:form id="form-table" style="width: 100%;">
				<apex:outputPanel layout="block" styleClass="slds-col" rendered="{!contacts != null && contacts.size > 0}">
					<div class="slds-clearfix">
						<h2 class="subheader-content" style="float: left;">Contacts Found ({!totalResult})</h2>
						<apex:outputLink rendered="{!canExportToExcel}" value="report_not_assigned_new_leads_excel?export=true&group={!groupSelected}&agency={!agencySelected}&begin={!beginDate}&end={!endDate}&order={!orderBy}" target="_blank" style="float: right;" styleClass="slds-button slds-button_brand agencyButton">Export to Excel</apex:outputLink>
					</div>
					<apex:outputPanel layout="block" styleClass="pagination" style="margin-top: 20px;" rendered="{!pages != null && pages.size > 1}">
                        <span style="margin-right: 10px;">Pages:</span>
                        <apex:commandLink action="{!changePage}" reRender="form-table" onclick="beginAjaxRequest();" oncomplete="completeAjaxRequest();" style="{!IF(currentPage == 1,'color: red','')}">
                                First <apex:outputText value=" ... " rendered="{!currentPage > 5}"/>
                                <apex:param name="page" value="1"/> 
                            </apex:commandLink>
                        <apex:repeat var="page" value="{!pages}">
                            <apex:commandLink action="{!changePage}" reRender="form-table" onclick="beginAjaxRequest();" oncomplete="completeAjaxRequest();" style="{!IF(page == currentPage,'color: red','')}">
                                {!page}
                                <apex:param name="page" value="{!page}"/> 
                            </apex:commandLink>
                        </apex:repeat>
                        <apex:commandLink action="{!changePage}" reRender="form-table" onclick="beginAjaxRequest();" oncomplete="completeAjaxRequest();" style="{!IF(currentPage == allPages.size,'color: red','')}">
                                <apex:outputText value=" ..." rendered="{!currentPage < allPages.size - 5}"/> Last
                                <apex:param name="page" value="{!allPages.size}"/> 
                            </apex:commandLink>
                    </apex:outputPanel>
					<table class="table-result slds-table slds-table_bordered slds-table_cell-buffer">
						<thead>
							<tr class="slds-text-title_caps">
								<th scope="col">
									<div class="slds-truncate" title="Opportunity Name">
										Contact Name	<apex:outputPanel layout="none" rendered="{!orderBy == 'name'}">
															<a href="javascript:void(0);" style="color: #18bb52 !important">
																<svg class="slds-button__icon" aria-hidden="true">
																	<use xmlns:xlink="http://www.w3.org/1999/xlink" 
																		xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#down')}" />
																</svg>
															</a>
														</apex:outputPanel>
														<apex:commandLink action="{!changeOrderBy}" rendered="{!orderBy != 'name'}" reRender="form-table" style="color: #D81921 !important;" onclick="beginAjaxRequest();" oncomplete="completeAjaxRequest();" title="Order by Name">
															<svg class="slds-button__icon" aria-hidden="true">
																<use xmlns:xlink="http://www.w3.org/1999/xlink" 
																	xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#up')}" />
															</svg>
															<apex:param name="orderBy" value="name"/>
														</apex:commandLink>	
									</div>
								</th>
								<th scope="col" style="text-align: center">
									<div class="slds-truncate" title="Account Name">
										Type	<apex:outputPanel layout="none" rendered="{!orderBy == 'type'}">
													<a href="javascript:void(0);" style="color: #18bb52 !important">
														<svg class="slds-button__icon" aria-hidden="true">
															<use xmlns:xlink="http://www.w3.org/1999/xlink" 
																xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#down')}" />
														</svg>
													</a>
												</apex:outputPanel>
												<apex:commandLink action="{!changeOrderBy}" rendered="{!orderBy != 'type'}" reRender="form-table" style="color: #D81921 !important;" onclick="beginAjaxRequest();" oncomplete="completeAjaxRequest();" title="Order by Type">
													<svg class="slds-button__icon" aria-hidden="true">
														<use xmlns:xlink="http://www.w3.org/1999/xlink" 
															xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#up')}" />
													</svg>
													<apex:param name="orderBy" value="type"/>
												</apex:commandLink>
									
									</div>
								</th>
								<th scope="col" style="text-align: center">
									<div class="slds-truncate">
										Created Date   <apex:outputPanel layout="none" rendered="{!orderBy == 'createdDate'}">
															<a href="javascript:void(0);" style="color: #18bb52 !important">
																<svg class="slds-button__icon" aria-hidden="true">
																	<use xmlns:xlink="http://www.w3.org/1999/xlink" 
																		xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#down')}" />
																</svg>
															</a>
														</apex:outputPanel>
														<apex:commandLink action="{!changeOrderBy}" rendered="{!orderBy != 'createdDate'}" reRender="form-table" style="color: #D81921 !important;" onclick="beginAjaxRequest();" oncomplete="completeAjaxRequest();" title="Order by Creation Date">
															<svg class="slds-button__icon" aria-hidden="true">
																<use xmlns:xlink="http://www.w3.org/1999/xlink" 
																	xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#up')}" />
															</svg>
															<apex:param name="orderBy" value="createdDate"/>
														</apex:commandLink>
									</div>
								</th>
								<th scope="col" style="text-align: center">
									<div class="slds-truncate" title="Stage">
										Agency		<apex:outputPanel layout="none" rendered="{!orderBy == 'agency'}">
														<a href="javascript:void(0);" style="color: #18bb52 !important">
															<svg class="slds-button__icon" aria-hidden="true">
																<use xmlns:xlink="http://www.w3.org/1999/xlink" 
																	xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#down')}" />
															</svg>
														</a>
													</apex:outputPanel>
													<apex:commandLink action="{!changeOrderBy}" rendered="{!orderBy != 'agency'}" reRender="form-table" style="color: #D81921 !important;" onclick="beginAjaxRequest();" oncomplete="completeAjaxRequest();" title="Order by agency">
														<svg class="slds-button__icon" aria-hidden="true">
															<use xmlns:xlink="http://www.w3.org/1999/xlink" 
																xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#up')}" />
														</svg>
														<apex:param name="orderBy" value="agency"/>
													</apex:commandLink>
									</div>
								</th>
								<th scope="col" style="text-align: center">
									<div class="slds-truncate" title="Confidence">
										Agency Group	<apex:outputPanel layout="none" rendered="{!orderBy == 'agencyGroup'}">
															<a href="javascript:void(0);" style="color: #18bb52 !important">
																<svg class="slds-button__icon" aria-hidden="true">
																	<use xmlns:xlink="http://www.w3.org/1999/xlink" 
																		xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#down')}" />
																</svg>
															</a>
														</apex:outputPanel>
														<apex:commandLink action="{!changeOrderBy}" rendered="{!orderBy != 'agencyGroup'}" reRender="form-table" style="color: #D81921 !important;" onclick="beginAjaxRequest();" oncomplete="completeAjaxRequest();" title="Order by Agency Group">
															<svg class="slds-button__icon" aria-hidden="true">
																<use xmlns:xlink="http://www.w3.org/1999/xlink" 
																	xlink:href="{!URLFOR($Asset.SLDS, 'assets/icons/utility-sprite/svg/symbols.svg#up')}" />
															</svg>
															<apex:param name="orderBy" value="agencyGroup"/>
														</apex:commandLink>
									</div>
								</th>				
							</tr>
						</thead>
						<tbody>
							<apex:repeat var="contact" value="{!contacts}">
								<tr>
									<th scope="row" data-label="Opportunity Name">
										<div class="slds-truncate" title="Cloudhub"><a href="javascript:void(0);">
											<apex:outputLink value="{!IF(redirectNewContactPage,'contact_new_activity_log','activity_log')}?id={!contact.id}" target="_blank">{!contact.Name}</apex:outputLink>										
										</a></div>
									</th>
									<td data-label="Account Name">
										<div class="slds-truncate" title="Cloudhub" style="text-align: center">{!contact.RecordType.Name}</div>
									</td>
									<td data-label="Close Date">
										<div class="slds-truncate" title="4/14/2015" style="text-align: center"><apex:outputText value=" {!contact.CreatedDate}"/></div>
									</td>
									<td data-label="Prospecting">
										<div class="slds-truncate" title="Prospecting" style="text-align: center">
											<apex:outputText value=" {!contact.Account.Name}" rendered="{!contact.Account.Name != null && contact.Account.Name != ''}"/>
											<apex:outputText value="No Agency defined." rendered="{!contact.Account.Name == null || contact.Account.Name == ''}"/>
										</div>
									</td>
									<td data-label="Confidence">
										<div class="slds-truncate" title="20%" style="text-align: center">
											<apex:outputText value=" {!contact.Account.Parent.Name}" rendered="{!contact.Account.Parent.Name != null && contact.Account.Parent.Name != ''}"/>
											<apex:outputText value="No Group defined." rendered="{!contact.Account.Parent.Name == null || contact.Account.Parent.Name == ''}"/>
										</div>
									</td>
								</tr>
							</apex:repeat>
						</tbody>
					</table>
				</apex:outputPanel>
			</apex:form>
			<apex:outputPanel layout="block" styleClass="slds-col" rendered="{!contacts == null || contacts.size == 0}" style="text-align: center; margin: 50px 0px;">
				<h2 class="subheader-content">NO RESULTS FOUND FOR THIS QUERY.</h2>
			</apex:outputPanel>
		</div>
	</apex:outputPanel>
</apex:page>