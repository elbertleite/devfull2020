<apex:component controller="contact_controller" allowDML="true">

  <apex:attribute name="ContactID" description="The id of the contact" type="String" assignTo="{!ctid}" required="true" />

  <apex:attribute name="size" description="The number os quotations to display" type="Integer" assignTo="{!limitQuotations}" required="false" default="0" />

  <style>
    .stickyClass #sticky{
      background: #ccc none repeat scroll 0 0;
      z-index: 100;
      border: 2px solid black;
      padding-top: 8px;
    }

    .btnStickClass{
      background-color: #999;
      border: solid 1px #555;
      padding: 10px;
      z-index: 100;
    }

    .invoiceStickClass{
      margin-top:10px;
    }

    table tr .quoteHeader td{
      background-color: #f1f1f1;
      font-weight: bold;
    }

    table tr .quoteCourses td{
      border-bottom: solid 1px #f1f1f1;

    }

    .listCourse{
      width: 100%;
      border: solid 1px #c1c1c1;
      border-spacing: 0;
    }

    .listCourse tr th{
      color: #c1c1c1;
        font-size: 0.8em;
    }

    .enrollLink{
      color: navy !important;
    }

    .pendingLink{
      color: orangered !important;
    }

    //table tr .quoteHeader:nth-child(odd) {background-color: white !important; }
      //table tr .quoteHeader:nth-child(even) {background-color:#f1f1f1 !important;}


    .enrollBtnPnl {
        background-color: #c1c1c1;
        border-radius: 3px;
        display: block;
        margin: 5px 0;
        padding: 12px;
        text-align: center;
    }

    .agProducts{
      background-color: #c1c1c1;
        display: block;
        padding: 5px;
    }

    .productsTable{
      border: 1px solid #c1c1c1;
          width: 100%;
       }

       .quotesTable tr:nth-child(odd) td {background-color: #fff; }
      .quotesTable tr:nth-child(even) td {background-color:#f1f1f1;}

      .listCourse tr:nth-child(even) td {background-color: #fff; }
      .listCourse tr:nth-child(odd) td {background-color:#f1f1f1;}

       .productsTable tr:nth-child(odd) td {background-color: #fff; }
      .productsTable tr:nth-child(even) td {background-color:#f1f1f1;}

      .message{
        margin: 4px 0;
      }

  </style>


  <apex:outputpanel layout="block" styleclass="content quotationComp">

    <div class="divWrapper">

      <div class="divHeader">
        <apex:commandLink status="statusLoad" onClick="toggle('{!$Component.bquotations}'); toggle('{!$Component.qbuttonMax}'); toggle('{!$Component.qbuttonMin}'); return false;" immediate="true">
          <apex:image value="{!URLFOR($Resource.assets,'images/button-maximize.png')}" id="qbuttonMax" styleclass="arrow" style="display:none"/>
          <apex:image value="{!URLFOR($Resource.assets,'images/button-minimize.png')}" id="qbuttonMin" styleclass="arrow" />
        </apex:commandLink>
        Quotations
      </div>

      <apex:pageMessage title="Info" summary="Simply select items from the quotations below and click on button ‘Add to Booking’." severity="info" strength="3" escape="false" rendered="{!$ObjectType.User.fields.CliMenu_CourseProd__c.Accessible}"/>

      <apex:outputPanel layout="block" styleclass="enrollBtnPnl sticky btnStickClass" rendered="{!$ObjectType.User.fields.CliMenu_CourseProd__c.Accessible}">
        <apex:commandLink value="Add to Booking" status="statusLoad" onclick="if(!confirmEnrol()) return false;" action="{!createEnrollment}" styleClass="agencyButton" rendered="{!ISNULL($CurrentPage.parameters.bk)}">
          <apex:param name="isMigrated" value="false" assignTo="{!isMigrated}" />
        </apex:commandLink>
        <apex:commandLink value="Add to Booking as Migrated" status="statusLoad" onclick="if(!confirmEnrol()) return false;" action="{!createEnrollment}" styleClass="agencyButton" rendered="{!ISNULL($CurrentPage.parameters.bk) && $ObjectType.User.fields.CliMenu_ClientMigration__c.Accessible}">
          <apex:param name="isMigrated" value="true" assignTo="{!isMigrated}" />
        </apex:commandLink>
      </apex:outputPanel>

      <apex:outputpanel layout="block" styleclass="divContent" id="bquotations" style="display:yes">
        <apex:outputPanel id="listQuotations">
          <table width="100%" cellspacing="0" cellpadding="0" border="0" class="quotesTable grey-zebra noArrow quotationsTable">
            <thead>
              <tr>
                <th>Number</th>
                <apex:outputPanel rendered="{!contact.RecordType.Name == 'Employee'}" layout="none">
                  <th>
                    Client
                  </th>
                </apex:outputPanel>
                <th>Number of Views</th>
                <th>Created By</th>
                <th>Created on</th>
                <th>Expiry Date</th>
                <th>Status</th>
                <th>Options</th>
              </tr>
            </thead>

            <apex:repeat value="{!quotations}" var="q">
              <tr class="quoteHeader">
                <td>
                  <apex:commandLink id="link" style="float:left; margin-right:15px" onClick="jQuery('.btMax{!q.quote.Name}').toggle(); jQuery('.btMin{!q.quote.Name}').toggle(); jQuery('.{!q.quote.Name}').toggle(); return false;" >
                    <apex:image value="{!URLFOR($Resource.ShowCaseIcons,'button-max.png')}" styleclass="btMax{!q.quote.Name}" />
                    <apex:image value="{!URLFOR($Resource.ShowCaseIcons,'button-min.png')}" styleclass="btMin{!q.quote.Name}"  style="display:none"/>
                  </apex:commandLink>
                  <apex:outputLink value="quotation?id={!q.quote.id}" target="_blank" rendered="{!$ObjectType.Contact.fields.allowQuoteByEmail__c.accessible}">
                    {!q.quote.Name}
                  </apex:outputLink>
                  <apex:outputText value="{!q.quote.Name}" rendered="{!NOT($ObjectType.Contact.fields.allowQuoteByEmail__c.accessible)}" />
                </td>
                <apex:outputPanel rendered="{!contact.RecordType.Name == 'Employee'}" layout="none">
                  <td>
                    <apex:outputLink value="activity_log?id={!q.quote.Client__c}" >
                      {!q.quote.Client__r.Name}
                    </apex:outputLink>
                  </td>
                </apex:outputPanel>
                <td>
                  <apex:outputText value="{!q.quote.Quotation_Total_Counter__c}" rendered="{!q.quote.Quotation_Total_Counter__c != null}" />
                  <apex:outputText value="-" rendered="{!q.quote.Quotation_Total_Counter__c == null}" />
                </td>
                <td>
                  <apex:outputField value="{!q.quote.CreatedBy.Name}" />
                </td>
                <td>
                  <apex:outputField value="{!q.quote.CreatedDate}" />
                </td>
                <td>
                  <apex:outputField value="{!q.quote.Expiry_Date__c}" />
                </td>
                <td>
                  <apex:outputField value="{!q.quote.Status__c}" />
                </td>
                <td>
                  <apex:outputPanel rendered="{!$ObjectType.Contact.fields.allowQuoteByEmail__c.accessible}">
                    <apex:outputLink value="quotation?id={!q.quote.id}" target="_blank" styleClass="contactLink" style="margin-right: 10px;">
                      <apex:outputText value="Online" />
                    </apex:outputLink>
                    <apex:outputLink value="clientquote?id={!q.quote.id}" target="_blank" styleClass="contactLink" style="margin-right: 10px;">
                      <apex:outputText value="Print" />
                    </apex:outputLink>

                      
                  </apex:outputPanel>

                  <apex:outputLink style="margin-right: 10px;" value="contact_quotationEmail?id={!contact.id}&quoteId={!q.quote.id}" styleClass="contactLink" rendered="{!NOT($CurrentPage.parameters.page == 'contactNewPage')}">
                    <apex:outputText value="Email" />
                  </apex:outputLink>
                  <apex:outputLink style="margin-right: 10px;" value="contact_new_quotation_email?id={!contact.id}&quoteId={!q.quote.id}&page=contactNewPage" styleClass="contactLink" rendered="{!$CurrentPage.parameters.page == 'contactNewPage'}">
                    <apex:outputText value="Email" />
                  </apex:outputLink>
                                                                                                                                                                    
                       
               

                    

                  <apex:commandLink action="{!recreateQuote}" onclick="if(!confirm('This {!contact.RecordType.Name} has courses in the cart.\nRecreating the quote will overwrite these courses.\nDo you wish to proceed?')) return false;"
                            styleClass="contactLink" rendered="{!q.quote.AllowRecreate__c && pendingQuote}">
                    <apex:param name="recreateQuote" value="{!q.quote.id}" />
                    <apex:param name="nationality" value="{!q.quote.Client__r.Nationality__c}" />
                    <apex:outputText value="Recreate" />
                  </apex:commandLink>

                  <apex:commandLink action="{!recreateQuote}" styleClass="contactLink" rendered="{!q.quote.AllowRecreate__c && !pendingQuote}">
                    <apex:param name="recreateQuote" value="{!q.quote.id}" />
                    <apex:param name="nationality" value="{!q.quote.Client__r.Nationality__c}" />
                    <apex:outputText value="Recreate" />
                  </apex:commandLink>

                </td>
              </tr>
              <tr class="{!q.quote.Name}" style="display: none">
                <td colspan="6">
                  <table class="listCourse" >
                    <tr>
                      <th style="width: 1%;"></th>
                      <th style="width: 10%;">Country</th>
                      <th style="width: 10%;">City</th>
                      <th style="width: 15%;">Campus</th>
                      <th style="width: 20%;">Course</th>
                      <th style="width: 5%;">Unit Type</th>
                      <th style="width: 5%;">Duration</th>
                      <th style="width: 10%;">Value Unit</th>
                      <th style="width: 10%;">Total</th>
                      <th style="width: 10%;">Start Date</th>
                      <th style="width: 5%;"> </th>
                    </tr>
                    <apex:repeat value="{!q.listCourses}" var="course">
                      <tr class="quoteCourses">
                        <td>
                          <apex:inputCheckBox value="{!course.quoteDetail.isSelected__c}" disabled="{!ISNULL(course.quoteDetail.Campus_Course__c)}" styleclass="{!course.quoteDetail.Course_Name__c} - {!course.quoteDetail.Campus_Name__c}"
                                    rendered="{!ISNULL(course.clientCourse) &&
                                        OR( NOT(course.quoteDetail.Campus_Course__r.Course__r.Package__c) , AND(course.quoteDetail.Campus_Course__r.Course__r.Package__c, NOT(ISBLANK(course.quoteDetail.Campus_Course__r.Package_Courses__c) ) ) )
                                         && $ObjectType.User.fields.CliMenu_CourseProd__c.Accessible}" />
                        </td>
                        <td>
                          <apex:outputField value="{!course.quoteDetail.Campus_Country__c}" />
                        </td>
                        <td>
                          <apex:outputField value="{!course.quoteDetail.Campus_City__c}" />
                        </td>
                        <td>
                          <apex:outputField value="{!course.quoteDetail.Campus_Name__c}" />
                        </td>
                        <td>
                          <apex:outputField value="{!course.quoteDetail.Course_Name__c}" />
                        </td>
                        <td>
                          <apex:outputField value="{!course.quoteDetail.Course_Unit_Type__c}" />
                        </td>

                        <td>
                          <apex:outputField value="{!course.quoteDetail.Course_Duration__c}" />
                        </td>
                        <td>
                          <apex:outputField value="{!course.quoteDetail.Course_Tuition_per_Unit__c}" />
                        </td>
                        <td>
                          <apex:outputField value="{!course.quoteDetail.Course_Total_Value__c}" />
                        </td>
                        <td>
                          <apex:outputField value="{!course.quoteDetail.Course_Start_Date__c}" />
                        </td>

                        <td style="text-align: right">
                          <!-- <apex:commandLink value="Enroll"  action="{!clientEnroll}" styleClass="enrollLink" rendered="{!ISNULL(course.clientCourse)}">
                            <apex:param value="{!course.quoteDetail.id}" name="courseId" />
                          </apex:commandLink> -->

                          <apex:commandLink value="Pending" onclick="" action="{!openEnrollment}" styleClass="pendingLink" rendered="{!$ObjectType.User.fields.CliMenu_CourseProd__c.Accessible && NOT(ISNULL(course.clientCourse)) && ISNULL(course.clientCourse.Enrolment_Date__c)}">
                            <apex:param value="{!course.clientCourse.id}" name="clientCourseId" />
                          </apex:commandLink>

                          <apex:outputLabel value="Enrolled" rendered="{!NOT(ISNULL(course.clientCourse)) && NOT(ISNULL(course.clientCourse.Enrolment_Date__c))}" />
                        </td>
                      </tr>
                      <apex:outputPanel rendered="{!NOT(ISNULL(course.listProducts)) && course.listProducts.size > 0}" layout="none">
                        <tr class="{!q.quote.Name}" style="display: none">
                          <td colspan="11">
                            <div class="agProducts">Agency Products</div>
                            <table class="productsTable">
                              <tr>
                                <th width="10px;"></th>
                                <th>Category</th>
                                <th>Name</th>
                                <th>Quantity</th>
                   
                 
                                <th>Price Unit</th>
                                <th>Unit Type</th>
                                <th>Value</th>
                              </tr>
                              <apex:repeat value="{!course.listProducts}" var="lp">
                                <tr>
                                  <td><apex:inputField value="{!lp.isSelected__c}" styleclass="{!lp.Quantity__c}x - {!lp.Product_Name__c} " /></td>
                                  <td>{!lp.Category__c}</td>
                                  <td>{!lp.Product_Name__c}</td>
                                  <td>{!lp.Quantity__c}</td>
                                  <td>{!lp.Price_Unit__c}</td>
                                  <td>{!lp.Unit_Description__c}</td>
                                                      
                                  <td>{!lp.Currency__c} {!lp.Price_Total__c}</td>
                                </tr>
                              </apex:repeat>
                            </table>
                          </td>
                        </tr>
                      </apex:outputPanel>
                    </apex:repeat>
                  </table>
                </td>
              </tr>

         
                          
                      

           
                            
               
       
             
                            
               
       
      
     
        
              <apex:outputPanel rendered="{!NOT(ISNULL(q.listProducts)) && q.listProducts.size > 0}" layout="none">
                <tr class="{!q.quote.Name}" style="display: none">
                  <td colspan="11">
                    <div class="agProducts">Agency Products</div>
                    <table class="productsTable">
                      <tr>
                        <th width="10px;"></th>
                        <th>Category</th>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Price Unit</th>
                        <th>Unit Type</th>
                 
               
                        <th>Value</th>
                      </tr>
                      <apex:repeat value="{!q.listProducts}" var="lp">
                        <tr>
                          <td><apex:inputField value="{!lp.isSelected__c}" styleclass="{!lp.Quantity__c}x - {!lp.Product_Name__c} " /></td>
                          <td>{!lp.Category__c}</td>
                          <td>{!lp.Product_Name__c}</td>
                          <td>{!lp.Quantity__c}</td>
                          <td>{!lp.Price_Unit__c}</td>
                          <td>{!lp.Unit_Description__c}</td>
                                                     
                                                  
                          <td>{!lp.Currency__c} {!lp.Price_Total__c}</td>
                        </tr>
                      </apex:repeat>
                    </table>
                  </td>
                </tr>
              </apex:outputPanel>
              <!-- <tr class="{!q.quote.Name}" style="display: none">
                <td colspan="6">
                  <apex:repeat value="{!q.listProducts}" var="lp">
                    {!lp.Product_Name__c}  <br />
                  </apex:repeat>
                </td>
              </tr> -->
            </apex:repeat>

          </table>



          <div style="height: 50px; padding: 10px;">
            <div style="float: left;">
                        <apex:outputLink styleClass="contactLink" value="contact_quotations?id={!contact.id}" rendered="{!limitQuotations != 0}" >
                          + Check the list of all quotations
                        </apex:outputLink>
                      </div>
                      <div style="float: right;">
                        <!-- <apex:outputLink value="xcoursesearch2?clientid={!contact.id}" styleClass="agencyButton" rendered="{!contact.RecordType.Name != 'Employee'}" >
                          Create New Quotation
                        </apex:outputLink>

                        <apex:outputLink value="xcoursesearch2" styleClass="agencyButton" rendered="{!contact.RecordType.Name == 'Employee'}" >
                          Create New Quotation
                        </apex:outputLink> -->
                      </div><br/><br/>
                        <apex:outputPanel id="myButtons" style="display:block; text-align: center; padding: 7px;" rendered="{!limitQuotations!=null && limitQuotations <1}" >

                <apex:outputPanel style="float:right;color:gray;padding-top:5px;">Page {!pageNumber} of {!totalPages}</apex:outputPanel>
                <apex:commandButton action="{!Beginning}" title="Beginning" disabled="{!disablePrevious}" value="<<" reRender="listQuotations "/>
                    <apex:commandButton action="{!Previous}" title="Previous" value="<" disabled="{!disablePrevious}" reRender="listQuotations " status="ipStatus" />
                    <apex:commandButton action="{!Next}" title="Next" value=">" disabled="{!disableNext}" reRender="listQuotations " status="ipStatus"  />
                <apex:commandButton action="{!End}" title="End" value=">>" disabled="{!disableNext}" reRender="listQuotations " status="ipStatus"  />
              </apex:outputPanel>
          </div>

        </apex:outputPanel>

      </apex:outputpanel>
    </div>



    <script>
       jQuery(document).ready(function(){
        jQuery('.sticky').sticky({topSpacing:0, className: "stickyClass"});
      });

      jQuery('.btnStickClass').width(jQuery('.middleColumn').width()-24);


      function confirmEnrol(){
        var courses = '';

        jQuery('.quotationComp input:checked').each(function(){
          courses += '- ' + jQuery(this).attr('class') + '\n';
        });

        if(courses ==''){
          alert('Please select at least one course.');
          return false;
        }
        else{
          if(confirm('You are adding the following items to a booking: \n \n' + courses + '\n Do you want to proceed?'))
            return true;
          else
            return false;
        }
      }

    </script>







  </apex:outputpanel> <!-- content -->

</apex:component>