@isTest
private class sharecommissionrequest_test {

   static testMethod void myUnitTest() {
        TestFactory tf = new TestFactory();
       
       	Account school = tf.createSchool();
       	
       	Account agencyGroup = tf.createAgencyGroup();
       	
       	Account agency = tf.createSimpleAgency(agencyGroup);
       	Account agency2 = tf.createSimpleAgency(agencyGroup);
       	
      	Contact emp = tf.createEmployee(agency);
      	
       	User portalUser = tf.createPortalUser(emp);
       	
       	Account campus = tf.createCampus(school, agency);
       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
        Department__c department = tf.createDepartment(agency);
        
		Contact client = tf.createLead(agency, emp);
		
		client_course__c booking = tf.createBooking(client);
       	client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);

		List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
		
       	cc.Enrolment_Date__c = System.today();
       	cc.Enroled_by_Agency__c = agency2.id;
       	cc.Commission_Type__c = 0;
		update cc;
       
		instalments[0].Received_By_Agency__c = agency.id;
		instalments[0].Received_Date__c = System.today();
		instalments[0].Commission_Confirmed_On__c = System.today();
		instalments[0].isRequestedShareCommission__c = true;
		update instalments;
		
       	Test.startTest();
       	system.runAs(portalUser){
			
			sharecommissionrequest testClass = new sharecommissionrequest(new Apexpages.Standardcontroller(cc));

	    	
					
			testClass.selectedShareDecisionStatus = 'Accepted';
			List<SelectOption> agencyOptions = testClass.agencyOptions;
			List<SelectOption> shareDecisionStatus = testClass.shareDecisionStatus;
			testClass.decDetails = 'hello';
			testClass.course.share_commission_decision_status__c = 'Declined';
			testClass.saveRequest();
			
			
			testClass.retrieveCourse();
			for(client_course_instalment__c ci:testClass.course.client_course_instalments__r)
				ci.isRequestedShareCommission__c = false;
			testClass.saveRequest();
			
			
       	}
        
    }
}