public with sharing class commissions_school_commission_invoice {

	public boolean hasExtraFee {get;set;}
	public boolean hasKeepFee {get;set;}
	public boolean showResults {get;set;}
	public Account agencyDetails {get;set;}
	public Account schoolDetails {get;set;}
	public Account mainCampusDetails {get;set;}
	public list<Result> instResult {get;set;}
	public Invoice__c invoice {get;set;}
	public decimal totalInvoice {get;set;}
	public decimal totalTuition {get;set;}
	public decimal totalExtraFee {get;set;}
	public decimal totalKeepFee {get;set;}
	public decimal totalInstalment {get;set;}
	public decimal totalCommission {get;set;}
	public decimal totalTax {get;set;}

	private String invoiceID {get;set;}
	private String schoolId {get;set;}
	private String schoolGroup {get;set;}

	public String cancelDetails {get;set;}
	public String cancelledBy {get;set;}
	public String cancelledOn {get;set;}
	public String cancelReason {get;set;}
	public String cancelDesc {get;set;}
	public String invoiceCurrency {get;set;}

	public commissions_school_commission_invoice() {
		invoiceID = ApexPages.currentPage().getParameters().get('i');
		cancelDetails = ApexPages.currentPage().getParameters().get('cancelDt');

		if(cancelDetails!=null && cancelDetails != ''){
			list<String> lCancelDt = cancelDetails.split(';#;');
			cancelledBy = lCancelDt[0];
			cancelledOn = lCancelDt[1];
			cancelReason = lCancelDt[2];
			cancelDesc = lCancelDt[3];
		}
		 
		User currentUser;
		String campusId;
		// I N V O I C E
		try{
			list<client_course_instalment__c> result;
			if(invoiceID!=null && invoiceID!=''){

				invoice = [SELECT School_Invoice_Number__c, Due_Date__c, createdDate, Sent_By__r.Contact.AccountId, Sent_Email_To__c, Request_Confirmation_Completed_On__c, School_Id__c, Campus_Ids__c, lastModifiedBy.Name, lastModifiedDate, isCancelled__c, Cancel_Reason__c, Cancel_Description__c, CurrencyIsoCode__c FROM Invoice__c WHERE id = :invoiceID];

				schoolId = invoice.School_Id__c;

				if(invoice.Campus_Ids__c != null && invoice.Campus_Ids__c.split(';').size()==1){
					campusId = invoice.Campus_Ids__c;
				}

				result = findInvInstalments(true, null);
			}
			else{
				currentUser = [SELECT Contact.AccountId, Contact.PDS_to_Request__c, Contact.PDS_School_to_Request__c, Contact.PDS_Campus_Requested__c, Contact.PDS_School_Group__c FROM user WHERE id = :userInfo.getUserId()];

				schoolId = currentUser.Contact.PDS_School_to_Request__c;
				schoolGroup = currentUser.Contact.PDS_School_Group__c;

				if(currentUser.Contact.PDS_Campus_Requested__c != null && currentUser.Contact.PDS_Campus_Requested__c.split(';').size()==1){
					campusId = currentUser.Contact.PDS_Campus_Requested__c;
				}

				list<String> ids = currentUser.Contact.PDS_to_Request__c.split(';');

				result = findInvInstalments(false, ids);
			} 


			buildResult(result);
		}
		catch(Exception e ){
			system.debug('Error==>' + e.getMessage());
			system.debug('Line==>' + e.getLineNumber());
			instResult = new list<Result>();
		}

		// A G E N C Y 		D E T A I L S
		try{
			if(invoiceID!=null && invoiceID!=''){
				agencyDetails = [SELECT Id, Name, Logo__c, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode, Registration_name__c, Trading_Name__c,
																Business_Number__c, Type_of_Business_Number__c,
																Bank_PDS_PFS_Invoice__r.Bank__c,
																Bank_PDS_PFS_Invoice__r.Branch_Address__c,
																Bank_PDS_PFS_Invoice__r.Account_Name__c,
																Bank_PDS_PFS_Invoice__r.BSB__c,
																Bank_PDS_PFS_Invoice__r.Account_Number__c,
																Bank_PDS_PFS_Invoice__r.Swift_Code__c,
																Bank_PDS_PFS_Invoice__r.IBAN__c,
																//Details for sub agency account
																Bank_Sub_Agency__r.Bank__c,
																Bank_Sub_Agency__r.Branch_Address__c,
																Bank_Sub_Agency__r.Account_Name__c,
																Bank_Sub_Agency__r.BSB__c,
																Bank_Sub_Agency__r.Account_Number__c,
																Bank_Sub_Agency__r.Swift_Code__c,
																Bank_Sub_Agency__r.IBAN__c
																FROM Account WHERE Id = :invoice.Sent_By__r.Contact.AccountId limit 1];
			}
			else{
				agencyDetails = [SELECT Id, Name, Logo__c, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode, Registration_name__c, Trading_Name__c,
																Business_Number__c, Type_of_Business_Number__c,
																Bank_PDS_PFS_Invoice__r.Bank__c,
																Bank_PDS_PFS_Invoice__r.Branch_Address__c,
																Bank_PDS_PFS_Invoice__r.Account_Name__c,
																Bank_PDS_PFS_Invoice__r.BSB__c,
																Bank_PDS_PFS_Invoice__r.Account_Number__c,
																Bank_PDS_PFS_Invoice__r.Swift_Code__c,
																Bank_PDS_PFS_Invoice__r.IBAN__c,
																//Details for sub agency account
																Bank_Sub_Agency__r.Bank__c,
																Bank_Sub_Agency__r.Branch_Address__c,
																Bank_Sub_Agency__r.Account_Name__c,
																Bank_Sub_Agency__r.BSB__c,
																Bank_Sub_Agency__r.Account_Number__c,
																Bank_Sub_Agency__r.Swift_Code__c,
																Bank_Sub_Agency__r.IBAN__c

																FROM Account WHERE Id = :currentUser.Contact.AccountId limit 1];
			}

		}
		catch(Exception e){
			agencyDetails = new Account();
			system.debug('Error==>' + e.getMessage());
			system.debug('Line==>' + e.getLineNumber()); 
		}

		try{
			IPFunctions.SearchNoSharing NS = new IPFunctions.SearchNoSharing();

			if(schoolGroup == null){

				if(campusId != null){
					schoolDetails = ns.NSAccount('SELECT Registration_Name__c, Type_of_Business_Number__c, Business_Number__c, ShippingStreet, ShippingCountry, ShippingCity, ShippingState, ShippingPostalCode FROM Account WHERE  Id= \''+ campusId + '\' limit 1 ');

					mainCampusDetails = schoolDetails;

					if(schoolDetails.Business_Number__c == NULL){
						schoolDetails = ns.NSAccount('SELECT Registration_Name__c, Type_of_Business_Number__c, Business_Number__c FROM Account WHERE  Id= \''+ schoolId + '\' limit 1 ');
						try{
							mainCampusDetails = ns.NSAccount('SELECT ShippingStreet, ShippingCountry, ShippingCity, ShippingState, ShippingPostalCode FROM Account WHERE  ParentId= \''+ schoolId + '\' and Main_Campus__c = true limit 1 ');
						}catch(Exception e){
							mainCampusDetails = new Account();
						}
					}
				}
				else{
					schoolDetails = ns.NSAccount('SELECT Registration_Name__c, Type_of_Business_Number__c, Business_Number__c FROM Account WHERE  Id= \''+ schoolId + '\' limit 1 ');
					try{
						mainCampusDetails = ns.NSAccount('SELECT ShippingStreet, ShippingCountry, ShippingCity, ShippingState, ShippingPostalCode FROM Account WHERE  ParentId= \''+ schoolId + '\' and Main_Campus__c = true limit 1 ');
					}catch(Exception e){
						mainCampusDetails = new Account();
					}
				}
			}
			else{
				schoolDetails = ns.NSAccount('SELECT Registration_Name__c, Type_of_Business_Number__c, Business_Number__c, ShippingStreet, ShippingCountry, ShippingCity, ShippingState, ShippingPostalCode FROM Account WHERE  Id= \''+ schoolGroup + '\' limit 1 ');

				mainCampusDetails = schoolDetails;

			}


		}catch(Exception e){
			schoolDetails = new Account();
			system.debug('Error==>' + e.getMessage());
			system.debug('Line==>' + e.getLineNumber());
		}



	}

	//Get Invoice Instalments
	public list<client_course_instalment__c> findInvInstalments(boolean hasInvoice, list<String> ids){

		String sql = 'SELECT id, Name, Instalment_Value__c, Due_Date__c, Related_Fees__c, Scholarship_Taken__c, Commission__c,  Commission_Value__c, Tuition_Value__c, '+
									'Received_By_Agency__r.Name, Received_By__r.Name, Total_School_Payment__c, Number__c, Commission_Tax_Value__c, Commission_Tax_Rate__c, Received_Date__c, '+
									'Split_Number__c, Agent_Deal_Name__c, Agent_Deal_Value__c,Related_Promotions__c, School_Invoice_Number__c, Extra_Fee_Value__c, Kepp_Fee__c, '+
									'School_PDS_Notes__c, Balance__c,'+

									'isInstalment_Amendment__c, Amendment_Related_Fees__c, Amendment_PFS_Adjustment__c, Commission_Adjustment__c, Commission_Tax_Adjustment__c, Original_Instalment__r.Commission_Value__c, Original_Instalment__r.Commission_Tax_Value__c, Original_Instalment__r.Balance__c, Original_Instalment__r.isCommission_charged_on_amendment__c, Original_Instalment__r.Tuition_Value__c, Original_Instalment__r.Extra_Fee_Value__c, Original_Instalment__r.Kepp_Fee__c, Original_Instalment__r.Instalment_Value__c,'+

									'client_course__r.Commission_Tax_Name__c,'+
									'client_course__r.CurrencyIsoCode__c,'+
									'client_course__r.Commission_Type__c,'+
									'client_course__r.Start_Date__c,'+
									'client_course__r.client__c,'+
									'client_course__r.School_Student_Number__c,'+
									'client_course__r.client__r.Name,'+
									'client_course__r.client__r.Birthdate,'+
									'client_course__r.Course_Name__c,'+
									'client_course__r.Campus_Id__c,'+
									'client_course__r.Campus_Name__c '+

									'FROM client_course_instalment__c ';

		if(hasInvoice)
			sql += 'WHERE Request_Commission_Invoice__c = :invoiceID ';
		else{
			sql += 'WHERE id in :ids ';
		}

		sql += 'order by client_course__r.Campus_Name__c, client_course__r.Course_Name__c, client_course__r.Client__r.Name, Due_Date__c';

		system.debug('sql===>' + sql);
		return Database.query(sql);
	}

	//Build Result List
	private void buildResult(list<client_course_instalment__c> result){
		totalInvoice = 0;
		totalTuition = 0;
		totalExtraFee = 0;
		totalKeepFee = 0;
		totalInstalment = 0;
		totalCommission = 0;
		totalTax = 0;

		instResult = new list<Result>();

		Instalment i;
		courseDetails course;
		map<string, result> resultMap = new map<string,result>();
		for(client_course_instalment__c cci : result){

			if(cci.Commission_Tax_Adjustment__c==null) cci.Commission_Tax_Adjustment__c = 0;
			if(cci.Commission_Adjustment__c==null) cci.Commission_Adjustment__c = 0;

			totalTuition += cci.Tuition_Value__c;
			totalExtraFee += cci.Extra_Fee_Value__c;
			totalInstalment += cci.Instalment_Value__c;
			totalCommission += cci.Commission_Value__c + cci.Commission_Adjustment__c;
			totalTax += cci.Commission_Tax_Value__c + cci.Commission_Tax_Adjustment__c ;
			totalKeepFee += cci.Kepp_Fee__c;

			if((cci.Original_Instalment__c !=null && cci.Original_Instalment__r.isCommission_charged_on_amendment__c) || cci.Amendment_PFS_Adjustment__c>0){
				totalTuition += cci.Original_Instalment__r.Tuition_Value__c;
				totalExtraFee += cci.Original_Instalment__r.Extra_Fee_Value__c;
				totalInstalment += cci.Original_Instalment__r.Instalment_Value__c;
				totalCommission += cci.Original_Instalment__r.Commission_Value__c;
				totalTax += cci.Original_Instalment__r.Commission_Tax_Value__c;
				totalKeepFee += cci.Original_Instalment__r.Kepp_Fee__c;
			}

			i = new instalment();
			i.inst = cci;

			/** F E E S **/
			if(cci.Related_Fees__c!=null && cci.Related_Fees__c!=''){
				for(string lf:cci.Related_Fees__c.split('!#')){

					/** Original Fees  **/
					if(lf.split(';;').size() >= 7){ //It's amended

						i.fees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[6]), false,  decimal.valueOf(lf.split(';;')[7])));

						if(decimal.valueOf(lf.split(';;')[7])>0){ // Has keep fee
							i.KeepFees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[6]), false,  decimal.valueOf(lf.split(';;')[7])));
							i.totalKeepFee += decimal.valueOf(lf.split(';;')[7]);
						}

						else if(decimal.valueOf(lf.split(';;')[7])<0){ // Decreased keep fee
							i.KeepFees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[6]), false,  decimal.valueOf(lf.split(';;')[7]) + decimal.valueOf(lf.split(';;')[5])));
							i.totalKeepFee += decimal.valueOf(lf.split(';;')[7]);
						}
							i.totalFees +=	decimal.valueOf(lf.split(';;')[6]);
					}
					else {
						i.fees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2]), false,  decimal.valueOf(lf.split(';;')[5])));
						i.totalFees +=	decimal.valueOf(lf.split(';;')[2]);

						if(decimal.valueOf(lf.split(';;')[5])>0){ // Has keep fee
							i.KeepFees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2]), false,  decimal.valueOf(lf.split(';;')[5])));
							i.totalKeepFee += decimal.valueOf(lf.split(';;')[5]);
						}
					}
				}//end for
			}

			/** Amended Fees **/
			if(cci.Amendment_Related_Fees__c!=null && cci.Amendment_Related_Fees__c!=''){
				for(string lf:cci.Amendment_Related_Fees__c.split('!#')){

					i.fees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2]), false,  decimal.valueOf(lf.split(';;')[5])));
					i.totalFees +=	decimal.valueOf(lf.split(';;')[2]);
					if(decimal.valueOf(lf.split(';;')[5])>0){ // Has keep fee
						i.KeepFees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2]), false,  decimal.valueOf(lf.split(';;')[5])));
						i.totalKeepFee += decimal.valueOf(lf.split(';;')[5]);
					}
				}//end for
			}

			//Flag to show Fees Columns
			if(i.fees.size()>0)
				hasExtraFee = true;
			if(i.KeepFees.size()>0)
				hasKeepFee = true;

			//Group by Campus and Course
			if(!resultMap.containsKey(cci.client_course__r.Campus_Id__c)){ //New Campus
				course = new courseDetails(cci.client_course__r.Course_Name__c, i);
				resultMap.put(cci.Client_Course__r.Campus_Id__c, new result(cci.Client_Course__r.Campus_Id__c, cci.client_course__r.Campus_Name__c,
																																		cci.client_course__r.Course_Name__c,course));
			}
			else{
				result res = resultMap.get(cci.Client_Course__r.Campus_Id__c);

				if(!res.mapCourses.containsKey(cci.client_course__r.Course_Name__c)){ //New Course
					res.mapCourses.put(cci.client_course__r.Course_Name__c, new courseDetails(cci.client_course__r.Course_Name__c, i));
				}
				else{ //Add Instalment to Course
					res.mapCourses.get(cci.client_course__r.Course_Name__c).addInstalment(i);
				}
				resultMap.put(res.campusId, res);

			}

		}//end for
		totalInvoice += totalCommission + totalKeepFee + totalTax;
		result rm;
		for(String cp : resultMap.keySet()){
			rm = resultMap.get(cp);
			rm.setListCourses();

			instResult.add(rm);
		}//end for

	}


	/********************** Inner Classes **********************/

	//Result by Campus
	public class result{
		public String campusId {get;set;}
		public String campusName {get;set;}
		public list<courseDetails> courses {get;set;}
		private map<string, courseDetails> mapCourses {get{if(mapCourses==null) mapCourses = new map<string,courseDetails>(); return mapCourses;}set;}

		private result(String campusId, String campusName,String courseName, courseDetails course){
			this.campusId = campusId;
			this.campusName = campusName;
			this.mapCourses.put(courseName, course);
		}

		private void setListCourses(){
			courses = new list<courseDetails>();

			for(String c: mapCourses.keySet())
				courses.add(mapCourses.get(c));
		}

	}

	//Course Details
	public class courseDetails{
		public String courseName {get;set;}
		public list<Instalment> instalments {get{if(instalments == null) instalments = new list<Instalment>(); return instalments;}set;}

		private courseDetails(String courseName, Instalment inst){
			this.courseName = courseName;
			this.instalments.add(inst);
		}

		private void addInstalment(Instalment inst){
			this.instalments.add(inst);
		}
	}


	//Instalment Details
	public class Instalment{
		public client_course_instalment__c inst {get;set{
				inst = value;
				tuitionValue = value.Tuition_Value__c;
				extraFeeValue = value.Extra_Fee_Value__c;
				instalmentValue = value.Instalment_Value__c;
				commissionValue = value.Commission_Value__c + value.Commission_Adjustment__c;
				taxValue = value.Commission_Tax_Value__c + value.Commission_Tax_Adjustment__c ;
				totalEarned = value.Balance__c;

				if((value.Original_Instalment__c !=null && value.Original_Instalment__r.isCommission_charged_on_amendment__c) || value.Amendment_PFS_Adjustment__c>0){
					tuitionValue += value.Original_Instalment__r.Tuition_Value__c;
					extraFeeValue += value.Original_Instalment__r.Extra_Fee_Value__c;
					instalmentValue += value.Original_Instalment__r.Instalment_Value__c;
					commissionValue += value.Original_Instalment__r.Commission_Value__c;
					taxValue += value.Original_Instalment__r.Commission_Tax_Value__c;
					totalEarned += value.Original_Instalment__r.Balance__c;
				}

			}
		}
		public list<feeInstallmentDetail> fees {get{if(fees == null) fees = new list<feeInstallmentDetail>(); return fees;}set;}
		public list<feeInstallmentDetail> keepFees {get{if(keepFees == null) keepFees = new list<feeInstallmentDetail>(); return keepFees;}set;}
		public decimal totalFees {get{if(totalFees==null) totalFees = 0; return totalFees;}set;}
		public decimal totalKeepFee {get{if(totalKeepFee==null) totalKeepFee = 0; return totalKeepFee;}set;}

		public decimal tuitionValue {get;set;}
		public decimal extraFeeValue {get;set;}
		public decimal instalmentValue {get;set;}
		public decimal commissionValue {get;set;}
		public decimal taxValue {get;set;}
		public decimal totalEarned {get;set;}

	}

	//Fees Details
	public class feeInstallmentDetail{
		public string feeName{get; set;}
		public decimal feeValue{get; set;}
		public boolean isPromotion{get; set;}
		public decimal keepFeeValue{get; set;}

		public feeInstallmentDetail(string feeName, decimal feeValue, boolean isPromotion, decimal keepFeeValue){
			this.feeName = feeName;
			this.feeValue = feeValue;
			this.isPromotion = isPromotion;
			this.keepFeeValue = keepFeeValue;
		}
	}
}