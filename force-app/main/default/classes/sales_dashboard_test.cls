@isTest
private class sales_dashboard_test{
	static testMethod void myUnitTest() {
		TestFactory tf = new TestFactory();
		
		Account agency = tf.createAgency();

		Account agencyGroup = tf.createAgencyGroup();
		agencyGroup.RDStation_Campaigns__c = '["leading-page3","leading-australia","insercao-manual","leading-teste2"]';
		update agencyGroup;

		tf.createChecklists(agency.Parentid);

		Contact emp = tf.createEmployee(agency);

		User portalUser = tf.createPortalUser(emp);

		Test.startTest();
     	system.runAs(portalUser){
             sales_dashboard.retrieveGroupSalesInfo('01/01/2000', '30/12/2019', 'current');
		}
	}
}