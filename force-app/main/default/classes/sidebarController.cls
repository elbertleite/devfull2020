public with sharing class sidebarController {
	
	public String ctid {
		get;
		set {
			ctid = value;
			getContact();
		}
	}
	
	public sidebarController(){
		
		
		
	}
	
	public void refreshContact(){
		contact = null;
	}
	
	public Contact contact {get;set;}
	public boolean isLead {get;set;}
	public boolean pendingQuotation {get;set;}
	
	public Contact getContact(){
		if(contact == null && ctid != null){			
		
			contact = [Select id, Name, FirstName, LastName, Current_Agency__r.Hify_Agency__c, Destination_Country__c, Destination_City__c, Expected_Travel_Date__c, Nationality__c, Secondary_Passport__c, Country_of_Birth__c, Nickname__c, Birthdate, Gender__c, Age__c, Email, RecordType.Name, 
						Marital_Status__c, Occupation__c, Preferable_Language__c, Highest_Education_Completed__c, Current_Agency__r.AgencyGroup__c, Preferable_School_Country__c, Preferable_School_City__c, Preferable_Nationality__c, Lead_State__c,
						(SELECT Id FROM Web_Search__r Where active__c = true and Courses__c > 0 ORDER BY CreatedDate DESC LIMIT 1)
					from Contact where id = :ctid];
					
			if(contact.RecordType.name == 'Lead')
				isLead = true;
			else
				isLead = false;		
			
			if(contact.Web_Search__r != null && contact.Web_Search__r.size()>0)
				pendingQuotation = true;
			else
				pendingQuotation = false;
		}
		
		return contact;
			
	}
	
	private Map<String, List<Forms_of_Contact__c>> formsOfContact;
	public Map<String, List<Forms_of_Contact__c>> getFormsOfContact() {
		if(formsOfContact == null){
			formsOfContact = new Map<String, List<Forms_of_Contact__c>>();
			for(Forms_of_Contact__c foc : [Select Comments__c, Country__c, Detail__c, Type__c from Forms_of_Contact__c where Contact__c = :contact.id and (Type__c = 'Mobile' or Type__c = 'Home Phone') order by Country__c]){
				if(formsOfContact.containsKey(foc.Country__c))
					formsOfContact.get(foc.Country__c).add(foc);
				else
					formsOfContact.put(foc.Country__c, new List<Forms_of_Contact__c>{foc});
			}			
		}
		return formsOfContact;
	}

	public PageReference convertToClient(){
		
		User user = [Select ID, ContactID, Contact.Account.ParentId, Contact.Account.Parent.RDStation_Salesforce_mapping__c, Contact.Account.Parent.RDStation_Client_ID__c from User where id = :Userinfo.getUserId() LIMIT 1];

		String recordTypeClient = [Select id from RecordType where Name = 'Client' and isActive = true LIMIT 1].id;
		
		contact.RecordTypeId = recordTypeClient;
		contact.Lead_Converted_By__c = user.ContactID;
		contact.Lead_Converted_On__c = System.now();
		
		update contact;
		
		String url = '/Contact_Overview?id=' + contact.id;	

		if(!String.isEmpty(user.Contact.Account.Parent.RDStation_Client_ID__c) && !String.isEmpty(user.Contact.Account.Parent.RDStation_Salesforce_mapping__c)){
			url = url + '&callRD=convertToClient';
		}

		PageReference pageRef = new PageReference(url);
		pageRef.setRedirect(true);
		return pageRef;

		//getContact();
	}

}