public class commissions {

	/*public class ruleDetails implements Comparable {
		public integer key{get{if(key == null) key = 0; return key;} set;}
		public integer fromUnit{get{if(fromUnit == null) fromUnit = 0; return fromUnit;} set;}
		public integer control{get{if(control == null) control = 0; return control;} set;}
		public integer action{get{if(action == null) action = 0; return action;} set;}
		public decimal value{get{if(value == null) value = 0; return value;} set;}
		public string details{get{if(details == null) details = ''; return details;} set;}
		public ruleDetails(integer key, integer fromUnit, integer control, integer action, decimal value, string details){
			this.key = key;
			this.fromUnit = fromUnit;
			this.control = control;
			this.action = action;
			this.value = value;
			this.details = details;
		}

		public Integer compareTo(Object compareTo){
			ruleDetails rd = (ruleDetails) compareTo;

			if(this.fromUnit == rd.fromUnit) return 0;
			if(this.fromUnit > rd.fromUnit) return 1;
			return -1;

		}
	}*/

	public Commission__c commission{get; set;}

	private map<string, Commission__c> mpc;
	private map<string, Commission__c> mapCommission(){
		if(mpc == null && selectedSchool != null && selectedSchool != 'none'){
			mpc = new map<string, commission__c>();
			for(Commission__c cm:[Select 	Rules__c, Campus_Course__c, school_Id__c, campus_Id__c, CampusCourseTypeId__c, courseType__c, Comments__c, Campus_Course__r.campus__r.name, Campus_Course__r.course__r.name, Campus_Commission__c, Course_Commission__c, Course_Type_Commission__c, School_Commission__c, School_Commission_Type__c, Campus_Commission_Type__c, Course_Commission_Type__c, Course_Type_Commission_Type__c, Id from Commission__c where School_Control__c= :selectedSchool AND Agreement_Account__c = null]){
				if(cm.Campus_Course__c != null)
					mpc.put(cm.Campus_Course__c, cm);
				if(cm.CampusCourseTypeId__c != null)
					mpc.put(cm.CampusCourseTypeId__c, cm);
				if(cm.campus_Id__c != null)
					mpc.put(cm.campus_Id__c, cm);
				if(cm.school_Id__c != null)
					mpc.put(cm.school_Id__c, cm);
			}
		}
		return mpc;
	}

	public class commissionSchoolDetails{
		public string schoolName{get; set;}
		public decimal schoolCommission{get; set;}
		public Commission__c SchoolCommissionObj{get;set;}
		//public list<ruleDetails> listRules{get{if(listRules == null) listRules = new list<ruleDetails>(); return listRules;} set;}
		public list<commissionCampusDetails> listCampus{get{if(listCampus == null) listCampus = new list<commissionCampusDetails>(); return listCampus;} set;}
	}


	public class commissionCampusDetails{

		public string CampusName{get; set;}
		public decimal campusCommission{get; set;}
		public Commission__c campusCommissionObj{get;set;}
		//public list<ruleDetails> listRules{get{if(listRules == null) listRules = new list<ruleDetails>(); return listRules;} set;}
		public list<commissioncourseTypeDetails> listCourseType{get{if(listCourseType == null) listCourseType = new list<commissioncourseTypeDetails>(); return listCourseType;} set;}
	}

	public class commissioncourseTypeDetails{
		public string courseType{get; set;}
		public decimal courseTypeCommission{get; set;}
		public Commission__c courseTypeCommissionObj{get;set;}
		//public list<ruleDetails> listRules{get{if(listRules == null) listRules = new list<ruleDetails>(); return listRules;} set;}
		public list<Commission__c> listCourseCommission{get{if(listCourseCommission == null) listCourseCommission = new list<Commission__c>(); return listCourseCommission;} set;}
		/*public commissionDetails(string schoolName, decimal schoolCommission, string CampusName, decimal campusCommission, string courseType, decimal courseTypeCommission, list<Commission__c> listCourseCommission){
			this.schoolName = schoolName;
			this.schoolCommission = schoolCommission;
			this.CampusName = CampusName;
			this.campusCommission = campusCommission;
			this.courseType = courseType;
			this.courseTypeCommission = courseTypeCommission;
			this.listCourseCommission = listCourseCommission;
		}*/
	}

	public String rulesKey {get;set;}
	public PageReference setRulesKey(){

		system.debug('++ruleskey++ ' + ApexPages.currentPage().getParameters().get('ruleskey'));
		rulesKey = ApexPages.currentPage().getParameters().get('ruleskey');

		return null;

	}

	public void refreshCommissions(){
		coursesCommission = null;
	}

	public map<string, string> schoolName{get{if(schoolName == null) schoolName = new map<string, string>(); return schoolName;} set;}
	public map<string, string> campusName{get{if(campusName == null) campusName = new map<string, string>(); return campusName;} set;}

	//public map<string,list<ruleDetails>> mapRules{get{if(mapRules == null) mapRules = new map<string,list<ruleDetails>>(); return mapRules;} set;}

	private list<commissionSchoolDetails> coursesCommission;
	public list<commissionSchoolDetails> getCoursesCommission(){
		if(coursesCommission == null && selectedSchool != null && selectedSchool != 'none'){
			mpc = null;
			mapCommission();
			coursesCommission = new list<commissionSchoolDetails>();
			map<string,map<string,map<string,list<Commission__c>>>> mapCommission = new map<string,map<string,map<string,list<Commission__c>>>>();
			Commission__c cd;
	 		//map<string,map<string,map<string,list<Commission__c>>>> mapCom = new map<string,map<string,map<string,list<Commission__c>>>>();
	 		for(Campus_Course__c cc:[Select Campus__r.parentId, Campus__c, Campus__r.parent.name, Campus__r.name, course__r.name, Course__r.Course_Type__c, Id from Campus_Course__c WHERE Campus__r.ParentId = :selectedSchool order by Campus__r.parent.name, Campus__r.name, Course__r.Course_Type__c, course__r.name]){

	 			system.debug('cc.id ===>' + cc.id);
	 			system.debug('cc.Campus__r.parentId===>' + cc.Campus__r.parentId);
	 			system.debug('cc.Campus__c===>' + cc.Campus__c);
				system.debug('cc.Course__r.Course_Type__c===>' + cc.Course__r.Course_Type__c);
	 			if(!mapCommission.containsKey(cc.Campus__r.parentId)){//school
	 				//mapRules.put(cc.Campus__r.parentId, new list<ruleDetails>());
	 				//mapRules.put(cc.Campus__c, new list<ruleDetails>());
	 				//mapRules.put(cc.Campus__c+cc.Course__r.Course_Type__c, new list<ruleDetails>());
	 				//mapRules.put(cc.id, new list<ruleDetails>());
	 				schoolName.put(cc.Campus__r.parentId, cc.Campus__r.parent.name);
	 				campusName.put(cc.Campus__c, cc.Campus__r.name);
	 				cd = returnCommission(cc.id, cc.course__r.name);
	 				cd.Campus_Name__c = cc.Campus__r.name;
	 				cd.School_Name__c = cc.Campus__r.parent.name;
	 				cd.Campus_Course__c = cc.id;
	 				cd.Campus_Control__c = cc.campus__c;
	 				cd.Course_Type_Control__c = cc.Course__r.Course_Type__c;
	 				mapCommission.put(cc.Campus__r.parentId, new map<string,map<string,list<Commission__c>>>{cc.Campus__c => new map<string,list<Commission__c>>{cc.Course__r.Course_Type__c => new list<Commission__c>{cd}}});
	 				//mapCommission.add(new commissionDetails(cc.Campus__r.parent.name, cd.School_Commission__c, cc.Campus__r.name, cd.Campus_Commission__c, cc.Course__r.Course_Type__c, cd.Course_Type_Commission__c, new list<Commission__c>{cd}));
	 			}

	 			else if(!mapCommission.get(cc.Campus__r.parentId).containsKey(cc.Campus__c)){//campus
	 				//mapRules.put(cc.Campus__c, new list<ruleDetails>());
	 				//mapRules.put(cc.Campus__c+cc.Course__r.Course_Type__c, new list<ruleDetails>());
	 				//mapRules.put(cc.id, new list<ruleDetails>());
	 				campusName.put(cc.Campus__c, cc.Campus__r.name);
	 				cd = returnCommission(cc.id, cc.course__r.name);
	 				cd.Campus_Name__c = cc.Campus__r.name;
	 				cd.School_Name__c = cc.Campus__r.parent.name;
	 				cd.Campus_Course__c = cc.id;
	 				cd.Campus_Control__c = cc.campus__c;
	 				cd.Course_Type_Control__c = cc.Course__r.Course_Type__c;
	 				mapCommission.get(cc.Campus__r.parentId).put(cc.Campus__c, new map<string,list<Commission__c>>{cc.Course__r.Course_Type__c => new list<Commission__c>{cd}});
	 				//mapCommission.add(new commissionDetails(cc.Campus__r.parent.name, cd.School_Commission__c, cc.Campus__r.name, cd.Campus_Commission__c, cc.Course__r.Course_Type__c, cd.Course_Type_Commission__c, new list<Commission__c>{cd}));
	 			}
	 			else if(!mapCommission.get(cc.Campus__r.parentId).get(cc.Campus__c).containsKey(cc.Course__r.Course_Type__c)){//course type
	 				//mapRules.put(cc.Campus__c+cc.Course__r.Course_Type__c, new list<ruleDetails>());
	 				//mapRules.put(cc.id, new list<ruleDetails>());
	 				cd = returnCommission(cc.id, cc.course__r.name);
	 				cd.Campus_Name__c = cc.Campus__r.name;
	 				cd.School_Name__c = cc.Campus__r.parent.name;
	 				cd.Campus_Course__c = cc.id;
	 				cd.Campus_Control__c = cc.campus__c;
	 				cd.Course_Type_Control__c = cc.Course__r.Course_Type__c;
	 				System.debug('==>Course_Type__c: '+cc.Campus__c+cc.Course__r.Course_Type__c);
	 				mapCommission.get(cc.Campus__r.parentId).get(cc.Campus__c).put(cc.Course__r.Course_Type__c, new list<Commission__c>{cd}); //course
	 				//mapCommission.add(new commissionDetails(cc.Campus__r.parent.name, cd.School_Commission__c, cc.Campus__r.name, cd.Campus_Commission__c, cc.Course__r.Course_Type__c, cd.Course_Type_Commission__c, new list<Commission__c>{cd}));
	 			}
	 			else {//course
	 				//mapRules.put(cc.id, new list<ruleDetails>());
	 				cd = returnCommission(cc.id, cc.course__r.name);
	 				cd.Campus_Name__c = cc.Campus__r.name;
	 				cd.School_Name__c = cc.Campus__r.parent.name;
	 				cd.Campus_Course__c = cc.id;
	 				cd.Campus_Control__c = cc.campus__c;
	 				cd.Course_Type_Control__c = cc.Course__r.Course_Type__c;
	 				mapCommission.get(cc.Campus__r.parentId).get(cc.Campus__c).get(cc.Course__r.Course_Type__c).add(cd);
	 				//mapCommission.add(new commissionDetails(cc.Campus__r.parent.name, cd.School_Commission__c, cc.Campus__r.name, cd.Campus_Commission__c, cc.Course__r.Course_Type__c, cd.Course_Type_Commission__c, new list<Commission__c>{cd}));
	 			}

	 		}

	 		//System.debug('==>mapRules: '+mapRules);

	 		list<commissionSchoolDetails> listSchool;
	 		list<commissionCampusDetails> listCampuses;
	 		list<commissioncourseTypeDetails> listCourseType;

	 		listSchool = new list<commissionSchoolDetails>();
	 		for(string school:mapCommission.keySet()){
	 			commissionSchoolDetails csd = new commissionSchoolDetails();
	 			csd.schoolName = schoolName.get(school);
	 			csd.SchoolCommissionObj = returnCommission(school, null);
	 			csd.SchoolCommissionObj.school_Id__c = school;
	 			csd.SchoolCommissionObj.school_name__c = csd.schoolName;
	 			listCampuses = new list<commissionCampusDetails>();
	 			for(string campus:mapCommission.get(school).keySet()){
	 				commissionCampusDetails ccd = new commissionCampusDetails();
	 				ccd.CampusName = campusName.get(campus);
	 				ccd.campusCommissionObj = returnCommission(campus, null);
	 				ccd.campusCommissionObj.school_name__c = csd.schoolName;
	 				ccd.campusCommissionObj.campus_name__c = ccd.CampusName;
	 				ccd.campusCommissionObj.campus_Id__c = campus;
	 				listCourseType = new list<commissioncourseTypeDetails>();
	 				for(string courseType:mapCommission.get(school).get(campus).keySet()){
	 					commissioncourseTypeDetails cct = new commissioncourseTypeDetails();
	 					cct.courseType = courseType;
	 					cct.courseTypeCommissionObj = returnCommission(campus+courseType, null);
	 					cct.courseTypeCommissionObj.CampusCourseTypeId__c = campus+courseType;
	 					cct.courseTypeCommissionObj.Campus_Control__c = campus;
	 					cct.courseTypeCommissionObj.school_name__c = csd.schoolName;
	 					cct.courseTypeCommissionObj.campus_name__c = ccd.CampusName;
	 					cct.courseTypeCommissionObj.Course_Type_Control__c = courseType;
	 					cct.listCourseCommission.addAll(mapCommission.get(school).get(campus).get(courseType));
	 					listCourseType.add(cct);
	 				}
	 				ccd.listCourseType.addAll(listCourseType);
	 				listCampuses.add(ccd);
	 			}
	 			csd.listCampus.addAll(listCampuses);
	 			coursesCommission.add(csd);
	 		}



		}
		return coursesCommission;
	}


	private Commission__c returnCommission(string cc, string courseName){
		System.debug('==>cc: '+cc);
		System.debug('==>mpc: '+mpc);
		Commission__c commission;
 		if(mpc.containsKey(cc)){
 			commission = mpc.get(cc);

 			/*if(commission.rules__c != null && commission.rules__c != '')
				for(string lr:commission.rules__c.split('!#!')){
					if(mapRules.containsKey(cc)){
						System.debug('-->lr: '+lr);
						list<string> rule = lr.split('!@!');
						System.debug('-->rule: '+rule);
						string details = '';
						if(rule.size() > 4)
							details = rule[4];
						else details = '';
    					mapRules.get(cc).add(new ruleDetails(mapRules.get(cc).size(), integer.valueOf(rule[0]), integer.valueOf(rule[1]), integer.valueOf(rule[2]), decimal.valueOf(rule[3]), details));
					}
				}

			mapRules.get(cc).sort();

 			System.debug('==>mapRules: '+mapRules);*/
 			commission.Course_Name__c = courseName;
 		}else commission = new Commission__c(Course_Name__c = courseName, Campus_Commission__c = 0, Course_Commission__c = 0, School_Commission__c = 0, Course_Type_Commission__c = 0,
 											School_Commission_Type__c = 0, Campus_Commission_Type__c = 0, Course_Commission_Type__c = 0, Course_Type_Commission_Type__c = 0);
 		return commission;
	}


	public string selectedCountry{get{if(selectedCountry == null) selectedCountry = 'none'; return selectedCountry;} set;}
	public string selectedSchool{get{if(selectedSchool == null) selectedSchool = 'none'; return selectedSchool;} set;}
	private List<SelectOption> listCountries;
	public List<SelectOption> listSchools{get; set;}

	public List<SelectOption> getlistCountries(){
		if(listCountries == null){
			listCountries = new List<SelectOption>();
        	listCountries.add(new SelectOption('none','--Select Country--'));
	        for(AggregateResult ar:[Select BillingCountry from Account WHERE RecordType.name = 'campus' GROUP BY BillingCountry ORDER BY BillingCountry]){
							if((string)ar.get('BillingCountry')!=null && (string)ar.get('BillingCountry')!='')
		        		listCountries.add(new SelectOption((string)ar.get('BillingCountry'),(string)ar.get('BillingCountry')));
	        }
        }
        return listCountries;
	}

	public void retrievelistSchools() {
        if(selectedCountry != null && selectedCountry != 'none'){
			listSchools = new List<SelectOption>();
	        listSchools.add(new SelectOption('none','--Select School--'));
	        for(AggregateResult ar:[Select BillingCountry, parent.Name school, ParentId from Account WHERE RecordType.name = 'campus' and BillingCountry = :selectedCountry GROUP BY BillingCountry, parent.Name, ParentId ORDER BY BillingCountry, parent.Name]){
		        listSchools.add(new SelectOption((string)ar.get('parentId'),(string)ar.get('school')));
	        }
	        selectedSchool = 'none';
	        refreshCommissions();
        }
    }

	public integer fromUnit{get; set;}
	public decimal value{get; set;}
	public string details{get; set;}

	public string itemControl{get; set;}
	private List<SelectOption> optionsControl;
	public List<SelectOption> getItemsControl() {
        if(optionsControl == null){
			optionsControl = new List<SelectOption>();
	        optionsControl.add(new SelectOption('0','Weeks'));
	        optionsControl.add(new SelectOption('1','Enrollment'));
        }
        return optionsControl;
    }

    public string itemAction{get; set;}
    private List<SelectOption> optionsAction;
    public List<SelectOption> getItemsAction() {
    	if(optionsAction == null){
	        optionsAction = new List<SelectOption>();
	        optionsAction.add(new SelectOption('0','Increase Commission'));
	        optionsAction.add(new SelectOption('1','Agent Bonus'));
    	}
        return optionsAction;
    }

    public string commissionType{get; set;}
    private List<SelectOption> optionscommissionType;
    public List<SelectOption> getItemCommissionType() {
    	if(optionscommissionType == null){
	        optionscommissionType = new List<SelectOption>();
	        optionscommissionType.add(new SelectOption('0','%'));
	        optionscommissionType.add(new SelectOption('1','Cash'));
    	}
        return optionscommissionType;
    }

    private map<string,string> mapAction;
    public map<string,string> getmapAction(){
    	if(mapAction == null){
    		mapAction = new  map<string,string>();
    		mapAction.put('0','Increase Commission');
	    	mapAction.put('1','Agent Bonus');
    	}
	    return mapAction;
    }

    private map<string,string> mapControl;
    public map<string,string> getmapControl(){
    	if(mapControl == null){
    		mapControl = new  map<string,string>();
    		mapControl.put('0','Weeks');
	    	mapControl.put('1','Enrollment');
    	}
	    return mapControl;
    }


    /*public pageReference addRule(){

    	string paramId = ApexPages.currentPage().getParameters().get('paramId');
    	string fromUnit = ApexPages.currentPage().getParameters().get('fromUnit');
    	string control = ApexPages.currentPage().getParameters().get('control');
    	string action = ApexPages.currentPage().getParameters().get('action');
    	string value = ApexPages.currentPage().getParameters().get('value');
    	string details = ApexPages.currentPage().getParameters().get('details');
    	integer posAdd = 0;

    	System.debug('===> '+paramId + ' - ' + fromUnit + ' - ' + control + ' - ' + action + ' - ' + value + ' - ' + details);

    	if(!mapRules.containsKey(paramId))
    		mapRules.put(paramId, new list<ruleDetails>{new ruleDetails(mapRules.get(paramId).size(), integer.valueOf(fromUnit), integer.valueOf(control), integer.valueOf(action), decimal.valueOf(value), details)});
    	else {
    		if(mapRules.get(paramId).size() > 0)
    			posAdd = mapRules.get(paramId)[mapRules.get(paramId).size()-1].key+1;
    		else posAdd = 0;
    		mapRules.get(paramId).add(new ruleDetails(posAdd, integer.valueOf(fromUnit), integer.valueOf(control), integer.valueOf(action), decimal.valueOf(value), details));
    	}
    	System.debug('==>mapRules_new: '+mapRules);
    	return null;
    }



   public pageReference deleteRule(){
   		string mapId = ApexPages.currentPage().getParameters().get('mapId');
   		integer ruleId = integer.valueOf(ApexPages.currentPage().getParameters().get('ruleId'));
   		integer delPosition = 0;
   		if(mapRules.containsKey(mapId)){
   			for(ruleDetails rd:mapRules.get(mapId)){
   				if(rd.key == ruleId)
   					break;
   				delPosition++;
   			}
   			mapRules.get(mapId).remove(delPosition);
   		}
   		return null;

   }*/

    public pageReference saveCommission(){
    	list<commission__c> commissionsUpdate = new list<commission__c>();
    	list<commission__c> commissionsDelete = new list<commission__c>();
    	for(commissionSchoolDetails school:coursesCommission){

    		if(school.SchoolCommissionObj.School_Commission__c != null && school.SchoolCommissionObj.School_Commission__c != 0){
    			/*if(mapRules.size() > 0 && mapRules.containsKey(school.SchoolCommissionObj.school_Id__c)){
    				school.SchoolCommissionObj.rules__c = '';
    				list<string> schoolRules = new list<string>();
    				for(ruleDetails rd:mapRules.get(school.SchoolCommissionObj.school_Id__c))
    					schoolRules.add(rd.fromUnit+'!@!'+rd.control+'!@!'+rd.action+'!@!'+rd.value +'!@!'+rd.details);
    				school.SchoolCommissionObj.rules__c = string.join(schoolRules,'!#!');
    			}*/
    			school.SchoolCommissionObj.School_Control__c = selectedSchool;
    			school.SchoolCommissionObj.country__c = selectedCountry;
    			//upsert school.SchoolCommissionObj;
    			commissionsUpdate.add(school.SchoolCommissionObj);
    		} else if (school.SchoolCommissionObj.School_Commission__c == 0 && mpc.containsKey(school.SchoolCommissionObj.school_Id__c)){
    			commissionsDelete.add(school.SchoolCommissionObj);
    		}
    		for(commissionCampusDetails campus:school.listCampus){
    			if(campus.campusCommissionObj.Campus_Commission__c != null && campus.campusCommissionObj.Campus_Commission__c != 0){
    				/*if(mapRules.size() > 0 && mapRules.containsKey(campus.campusCommissionObj.campus_Id__c)){
	    				campus.campusCommissionObj.rules__c = '';
	    				list<string> campusRules = new list<string>();
	    				for(ruleDetails rd:mapRules.get(campus.campusCommissionObj.campus_Id__c))
	    					campusRules.add(rd.fromUnit+'!@!'+rd.control+'!@!'+rd.action+'!@!'+rd.value +'!@!'+rd.details);
	    				campus.campusCommissionObj.rules__c = string.join(campusRules,'!#!');
	    			}*/
	    			campus.campusCommissionObj.School_Control__c = selectedSchool;
	    			campus.campusCommissionObj.country__c = selectedCountry;
	    			campus.campusCommissionObj.Campus_Control__c = campus.campusCommissionObj.campus_Id__c;
    				//upsert campus.campusCommissionObj;
    				commissionsUpdate.add(campus.campusCommissionObj);
    			}else if(campus.campusCommissionObj.Campus_Commission__c == 0 && mpc.containsKey(campus.campusCommissionObj.campus_Id__c)){
    				commissionsDelete.add(campus.campusCommissionObj);
    			}
    			for(commissioncourseTypeDetails courseType:campus.listCourseType){
    				if(courseType.courseTypeCommissionObj.Course_Type_Commission__c != null && courseType.courseTypeCommissionObj.Course_Type_Commission__c != 0){
    					/*if(mapRules.size() > 0 && mapRules.containsKey(courseType.courseTypeCommissionObj.CampusCourseTypeId__c)){
		    				courseType.courseTypeCommissionObj.rules__c = '';
		    				list<string> courseTypeRules = new list<string>();
		    				for(ruleDetails rd:mapRules.get(courseType.courseTypeCommissionObj.CampusCourseTypeId__c))
		    					courseTypeRules.add(rd.fromUnit+'!@!'+rd.control+'!@!'+rd.action+'!@!'+rd.value +'!@!'+rd.details);
		    				courseType.courseTypeCommissionObj.rules__c = string.join(courseTypeRules,'!#!');
		    			}*/
		    			courseType.courseTypeCommissionObj.School_Control__c = selectedSchool;
		    			courseType.courseTypeCommissionObj.country__c = selectedCountry;
    					//upsert courseType.courseTypeCommissionObj;
    					commissionsUpdate.add(courseType.courseTypeCommissionObj);
    				}else if(courseType.courseTypeCommissionObj.Course_Type_Commission__c == 0 && mpc.containsKey(courseType.courseTypeCommissionObj.CampusCourseTypeId__c)){
    					commissionsDelete.add(courseType.courseTypeCommissionObj);
    				}
    				System.debug('==>courseType.listCourseCommission: '+courseType.listCourseCommission);
    				for(Commission__c course:courseType.listCourseCommission){
    					if(course.Course_Commission__c != null && course.Course_Commission__c != 0){
    						/*if(mapRules.size() > 0 && mapRules.containsKey(course.Campus_Course__c)){
			    				course.rules__c = '';
			    				list<string> courseRules = new list<string>();
			    				for(ruleDetails rd:mapRules.get(course.Campus_Course__c))
			    					courseRules.add(rd.fromUnit+'!@!'+rd.control+'!@!'+rd.action+'!@!'+rd.value +'!@!'+rd.details);
			    				course.rules__c = string.join(courseRules,'!#!');
			    			}*/
			    			System.debug('==>course: '+course);
			    			course.School_Control__c = selectedSchool;
			    			course.country__c = selectedCountry;
    						//upsert course;
    						commissionsUpdate.add(course);
    					}
    					else if(course.Course_Commission__c == 0 && mpc.containsKey(course.Campus_Course__c)){
    						commissionsDelete.add(course);
    					}
    				}
    			}
    		}
    	}

    	if(commissionsDelete.size() > 0)
    		delete commissionsDelete;

    	if(commissionsUpdate.size() > 0)
    		upsert commissionsUpdate;

    	return null;
    }

}