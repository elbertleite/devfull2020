public with sharing class school_comm_amend_request {
    
    public list<client_course_instalment__c> instalments {get;set;}

	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}
    
    public school_comm_amend_request(){
    	instalments = new list<client_course_instalment__c>();
    	
    	if(ApexPages.currentPage().getParameters().get('gr')!=null)
			selectedAgencyGroup = ApexPages.currentPage().getParameters().get('gr');
			
		if(ApexPages.currentPage().getParameters().get('ag')!=null){
			selectedAgency = ApexPages.currentPage().getParameters().get('ag');
			if(!String.isEmpty(selectedAgency)){
				selectedAgencies = new List<String>{selectedAgency};
				selectedAgenciesReplica = new Set<String>(selectedAgencies); 
			}
		}
		
		if(ApexPages.currentPage().getParameters().get('sc')!=null)
			selectedSchool = ApexPages.currentPage().getParameters().get('sc');
		
		if(ApexPages.currentPage().getParameters().get('ca')!=null)
			selectedCampus = ApexPages.currentPage().getParameters().get('ca');

		mainCurrencies = ff.retrieveMainCurrencies();
        agencyCurrencies = ff.retrieveAgencyCurrencies();
		currencyLastModifiedDate = ff.currencyLastModifiedDate;
    }

	//Currencies
	public DateTime currencyLastModifiedDate {get;set;}
	public list<SelectOption> mainCurrencies {get;set;}
	private Map<String, double> agencyCurrencies;
	//

	private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}
    
    
    /** Find Amended Commissions to Request **/
    public void findRequestComm(){
    	
    	instalments.clear();
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && currentUser.Aditional_Agency_Managment__c == null){ //set the user to see only his own agency
			selectedAgency = currentUser.Contact.AccountId;
			selectedAgencies = new List<String>{selectedAgency};
		}

		List<String> agenciesToSearch;
		if(selectedAgencies.size() == 1 && selectedAgencies.get(0) == 'all'){
			agenciesToSearch = new List<String>();
			for(SelectOption opt : getAgencies()){
				if(opt.getValue() != 'all'){
					agenciesToSearch.add(opt.getValue());		
				}
			}
		}else{
			agenciesToSearch = selectedAgencies;
		}
		
		String sql = ' SELECT Id, Name, Due_Date__c, Number__c, Discount__c,isPFS__c, Amendment_Commission_Balance__c, Commission_Value__c, Commission_Tax_Value__c, Amendment_Tax_Balance__c, Kepp_Fee__c,  Commission_Adjustment__c, Commission_Tax_Adjustment__c, Extra_Fee_Value__c, Tuition_Value__c, Instalment_Value__c, Amendment_PFS_Adjustment__c, Amendment_Receive_From_School__c, Received_By_Agency__r.Name, '+
					 
					 ' Original_Instalment__r.Tuition_Value__c, ' +
					 ' Original_Instalment__r.Extra_Fee_Value__c, ' +
					 ' Original_Instalment__r.Kepp_Fee__c, ' +
					 ' Original_Instalment__r.Instalment_Value__c, ' +
					 ' Original_Instalment__r.Paid_To_School_By__r.Name, ' +
					 ' Original_Instalment__r.Paid_To_School_On__c, ' +
					 ' Original_Instalment__r.Commission_Value__c, ' +
					 ' Original_Instalment__r.Commission_Tax_Value__c, ' +
					 ' Original_Instalment__r.School_Invoice_Number__c, ' +
					 
					 ' School_Invoice_Number__c, ' +
					 ' Client_Course__c, ' +
					 ' client_course__r.CurrencyIsoCode__c, ' +
					 ' client_course__r.Client__r.Owner__c, ' +
					 ' Client_Course__r.School_Name__c, ' +
					 ' Client_Course__r.Campus_Name__c, ' +
					 ' Client_Course__r.Course_Name__c, ' +
					 ' Client_Course__r.Course_Length__c, ' +
					 ' Client_Course__r.Unit_Type__c,  ' +
					 ' Client_Course__r.Start_Date__c, ' +
					 ' Client_Course__r.End_Date__c, ' +
					 ' Client_Course__r.Client__c,  ' +
					 ' Client_Course__r.Client__r.Status__c, ' +
					 ' Client_Course__r.Client__r.Owner__r.Name, ' +
					 ' client_course__r.Client__r.Email, ' +
					 ' Client_Course__r.Client__r.Name, ' +
					 ' Client_Course__r.Client__r.Visa_Expiry_Date__c  ' +
					
					//  ' FROM Client_Course_Instalment__c WHERE Received_By_Agency__c =:selectedAgency '; //Agency Received_By_Agency__c = :selectedAgency
					 ' FROM Client_Course_Instalment__c WHERE Received_By_Agency__c  IN :agenciesToSearch '; //Agency Received_By_Agency__c = :selectedAgency
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
		 	sql+= ' AND Received_By_Agency__r.ParentId = :selectedAgencyGroup ';
		 	
		 	
	 	if(selectedCampus!='all')
			sql+= ' AND client_course__r.Campus_Id__c = :selectedCampus '; //Campus
		else if(selectedSchool!='all')
			sql+= ' AND client_course__r.School_Id__c = :selectedSchool '; //School
		
		sql+= ' AND isCommission_Amendment__c = TRUE AND isPFS__c = false AND isInstalment_Amendment__c = FALSE AND Sent_Email_Receipt__c = NULL'; 
		
		system.debug('sql===' + sql);

		instalments = Database.query(sql);
    }
    
    
     /** Find Amended Commissions to Request **/
    public void findConfirmComm(){
    	
    	instalments.clear();
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && currentUser.Aditional_Agency_Managment__c == null){ //set the user to see only his own agency
			selectedAgency = currentUser.Contact.AccountId;
			selectedAgencies = new List<String>{selectedAgency};
		}

		List<String> agenciesToSearch;
		if(selectedAgencies.size() == 1 && selectedAgencies.get(0) == 'all'){
			agenciesToSearch = new List<String>();
			for(SelectOption opt : getAgencies()){
				if(opt.getValue() != 'all'){
					agenciesToSearch.add(opt.getValue());		
				}
			}
		}else{
			agenciesToSearch = selectedAgencies;
		}
		
		String sql = ' SELECT Id, Name, Due_Date__c, Number__c, Discount__c,isPFS__c, Amendment_Commission_Balance__c, Commission_Value__c, Commission_Tax_Value__c, Amendment_Tax_Balance__c, Kepp_Fee__c, Extra_Fee_Value__c, Tuition_Value__c, Instalment_Value__c, Commission_Paid_Date__c, Sent_Email_Receipt_On__c, Sent_Email_Receipt_By__r.Name, Amendment_Receive_From_School__c, Commission_Adjustment__c, Commission_Tax_Adjustment__c, Agency_Currency__c, Agency_Currency_Value__c, Transfer_Receive_Fee_Currency__c, Transfer_Receive_Fee__c, Agency_Currency_Rate__c, Received_By_Agency__r.Name, ' +
					 
					 ' Original_Instalment__r.Tuition_Value__c, ' +
					 ' Original_Instalment__r.Extra_Fee_Value__c, ' +
					 ' Original_Instalment__r.Kepp_Fee__c, ' +
					 ' Original_Instalment__r.Instalment_Value__c, ' +
					 ' Original_Instalment__r.Paid_To_School_By__r.Name, ' +
					 ' Original_Instalment__r.Paid_To_School_On__c, ' +
					 ' Original_Instalment__r.Commission_Value__c, ' +
					 ' Original_Instalment__r.Commission_Tax_Value__c, ' +
					 ' Original_Instalment__r.School_Invoice_Number__c, ' +
					 
					 ' School_Invoice_Number__c, ' +
					 ' Client_Course__c, ' +
					 ' client_course__r.CurrencyIsoCode__c, ' +
					 ' client_course__r.Client__r.Owner__c, ' +
					 ' Client_Course__r.School_Name__c, ' +
					 ' Client_Course__r.Campus_Name__c, ' +
					 ' Client_Course__r.Course_Name__c, ' +
					 ' Client_Course__r.Course_Length__c, ' +
					 ' Client_Course__r.Unit_Type__c,  ' +
					 ' Client_Course__r.Start_Date__c, ' +
					 ' Client_Course__r.End_Date__c, ' +
					 ' Client_Course__r.Client__c,  ' +
					 ' Client_Course__r.Client__r.Status__c, ' +
					 ' Client_Course__r.Client__r.Owner__r.Name, ' +
					 ' client_course__r.Client__r.Email, ' +
					 ' Client_Course__r.Client__r.Name, ' +
					 ' Client_Course__r.Client__r.Visa_Expiry_Date__c  ' +
					 
					 //' FROM Client_Course_Instalment__c WHERE Received_By_Agency__c =:selectedAgency '; //Agency Received_By_Agency__c = :selectedAgency
					 ' FROM Client_Course_Instalment__c WHERE Received_By_Agency__c =:agenciesToSearch '; //Agency Received_By_Agency__c = :selectedAgency
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
		 	sql+= ' AND Received_By_Agency__r.ParentId = :selectedAgencyGroup ';
		 	
		 	
	 	if(selectedCampus!='all')
			sql+= ' AND client_course__r.Campus_Id__c = :selectedCampus '; //Campus
		else if(selectedSchool!='all')
			sql+= ' AND client_course__r.School_Id__c = :selectedSchool '; //School
		
		
		sql+= ' AND  isCommission_Amendment__c = TRUE AND isPFS__c = false AND Sent_Email_Receipt_On__c != NULL AND Amendment_Adjustment_Confirmed_On__c = NULL '; 
		
		system.debug('sql===' + sql);

		instalments.clear();
		for(client_course_instalment__c cci : Database.query(sql)){
			//C U R R E N C Y
			cci.Agency_Currency__c = currentUser.Contact.Account.account_currency_iso_code__c;
			cci.Transfer_Receive_Fee_Currency__c = cci.Agency_Currency__c;
			cci.Agency_Currency_Rate__c = agencyCurrencies.get(cci.client_course__r.CurrencyIsoCode__c);
			ff.convertCurrency(cci, cci.client_course__r.CurrencyIsoCode__c, cci.Amendment_Receive_From_School__c, null, 'Agency_Currency__c', 'Agency_Currency_Rate__c', 'Agency_Currency_Value__c');

			instalments.add(cci);
		}//end for
    }
    
    
    public PageReference confirmCommission(){
		
		String instId = ApexPages.currentPage().getParameters().get('id');
		system.debug('installment==>' + instId);
		
		DateTime currentTime = DateTime.now();
		
		client_course_instalment__c cci;
		
		//Find related Installment
		boolean hasError = false;
		for(client_course_instalment__c inst : instalments)
			if(inst.id == instId){

				if(inst.Agency_Currency_Rate__c == null){
					inst.Agency_Currency_Rate__c.addError('Please fill the rate.');
					hasError = true;
				}

				if(inst.Agency_Currency_Value__c == null){
					inst.Agency_Currency_Value__c.addError('Please fill the value.');
					hasError = true;
				}

				if(inst.Transfer_Receive_Fee__c == null){
					inst.Transfer_Receive_Fee__c.addError('Please fill the amount.');
					hasError = true;
				}
				
				if(inst.Commission_Paid_Date__c == null){
					inst.Commission_Paid_Date__c.addError('Please select the Received Date.');
					hasError = true;
				}else if(inst.Commission_Paid_Date__c > system.today()){
					inst.Commission_Paid_Date__c.addError('Payment Date cannot be a future date.');
					hasError = true;
				}
				
				if(hasError){
					return null;
				}
				else{
					inst.Amendment_Adjustment_Confirmed_On__c = currentTime;
					inst.Amendment_Adjustment_Confirmed_By__c = UserInfo.getUserId();
					inst.Booking_Closed_By__c = UserInfo.getUserId();
					inst.Booking_Closed_On__c = currentTime;
					inst.Amendment_Adjustment_Confirmed_By_Agency__c = currentUser.Contact.AccountId;
					inst.Status__c = 'Adjustment Commission Confirmed';
					
					update inst;
							
					/** Update Client Course Details **/
					client_course__c cc = [SELECT Id, Total_Paid__c, Total_Keep_Fee_Received__c, Total_Commission_Received__c, Total_Discount_Given__c, Total_GST_Paid__c, Total_Scholarship_Taken__c, Total_PFS_Commission_Received__c FROM client_course__c WHERE id = :inst.client_course__c limit 1];
									
					cc.Total_Commission_Received__c += inst.Amendment_Receive_From_School__c;
					
					update cc;
					break;
				}
			}
		findConfirmComm();	
		return null;
	}
			
	/** END -- Confirm Commission **/
    
    
    public integer getTotalRequest(){
    	integer result;
    	
    	String sql = 'SELECT count(id) total From Client_Course_Instalment__c WHERE Received_By_Agency__c =:selectedAgency '; 
    	
    	
    	if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
		 	sql+= ' AND Received_By_Agency__r.ParentId = :selectedAgencyGroup ';
		 	
		 	
	 	if(selectedCampus!='all')
			sql+= ' AND client_course__r.Campus_Id__c = :selectedCampus '; //Campus
		else if(selectedSchool!='all')
			sql+= ' AND client_course__r.School_Id__c = :selectedSchool '; //School	
    	
    	
 		sql+= 'AND isCommission_Amendment__c = TRUE AND isPFS__c = false AND isInstalment_Amendment__c = FALSE  AND Sent_Email_Receipt__c = NULL';
    	
    	
    	
		for(AggregateResult ar : Database.query(sql))
			result = integer.valueOf(ar.get('total'));	
			
		return result;
    }
    
    
    public integer getTotalConfirm(){
    	integer result;
    	
    	
    	String sql = 'Select count(id) total From Client_Course_Instalment__c WHERE Received_By_Agency__c =:selectedAgency '; 
    	
    	
    	if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
		 	sql+= ' AND Received_By_Agency__r.ParentId = :selectedAgencyGroup ';
		 	
	 	if(selectedCampus!='all')
			sql+= ' AND client_course__r.Campus_Id__c = :selectedCampus '; //Campus
		else if(selectedSchool!='all')
			sql+= ' AND client_course__r.School_Id__c = :selectedSchool '; //School	
			
		sql+= 'AND  isCommission_Amendment__c = TRUE AND isPFS__c = false AND Sent_Email_Receipt_On__c != NULL AND Amendment_Adjustment_Confirmed_On__c = NULL  ';
		
		
		for(AggregateResult ar : Database.query(sql))
			result = integer.valueOf(ar.get('total'));	
			
		return result;
    }
    
	
	public String selectedAction {get;set;}
	public List<SelectOption> actionOptions {
		get{
			if(actionOptions == null){
				actionOptions = new List<SelectOption>();	
				actionOptions.add(new SelectOption('none', 'Actions'));
				actionOptions.add(new SelectOption('receivePayment', 'Payment Received'));
				actionOptions.add(new SelectOption('sendEmail', 'Send Email'));
			}
			
			return actionOptions;
		}
		set;
	}    
	
	
	/********************** Filters **********************/	
	private User currentUser {get{if(currentUser==null) currentUser = ff.currentUser; return currentUser;}set;}
	
	public string selectedAgencyGroup{
    	get{
    		if(selectedAgencyGroup == null)
    			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
    		return selectedAgencyGroup;
    	}  
    	set;
    }
    
	 public List<SelectOption> agencyGroupOptions {
  		get{
   			if(agencyGroupOptions == null){
   				
   				agencyGroupOptions = new List<SelectOption>();  
   				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group  
	    			for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:currentUser.Contact.AccountId order by Parent.Name]){
		     			agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
		    		}
   				}else{
   					for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
		     			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
		    		}
   				}
	   		}
	   		return agencyGroupOptions;
	  }
	  set;
 	}
	
	public string selectedAgency {get{if(selectedAgency==null) selectedAgency = currentUser.Contact.AccountId; return selectedAgency;}set;}
	public List<string> selectedAgencies {get{if(selectedAgencies==null) selectedAgencies = new List<String>{currentUser.Contact.AccountId}; return selectedAgencies;}set;}
	public Set<string> selectedAgenciesReplica {get{if(selectedAgenciesReplica==null) selectedAgenciesReplica = new Set<String>{currentUser.Contact.AccountId}; return selectedAgenciesReplica;}set;}
		
	public list<SelectOption> getAgencies(){
		List<SelectOption> result = new List<SelectOption>();
		if(selectedAgencyGroup != null){
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
				for(Account a: [Select Id, Name from Account where id =:currentUser.Contact.AccountId order by name]){
					if(selectedAgency=='')
						selectedAgency = a.Id;					
					result.add(new SelectOption(a.Id, a.Name));
					if(currentUser.Aditional_Agency_Managment__c != null){
						for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
							result.add(new SelectOption(ac.id, ac.name));
						}
					}
				}
			}else{
				for(Account a: [Select Id, Name from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
					result.add(new SelectOption(a.Id, a.Name));
					if(selectedAgency=='')
						selectedAgency = a.Id;	
				}
			}
		}
		if(result.size() > 2){
			result.add(0, new SelectOption('all', '-- All Agencies --'));
		}
		
		return result;
	}
	
	public void changeGroup(){
		selectedAgency = '';
		getAgencies();
		
		changeAgency();
	}
		
	public void changeAgency(){
		if(selectedAgencies.size() > selectedAgenciesReplica.size()){
			String itemAdded;
			for(String agency : selectedAgencies){
				if(!selectedAgenciesReplica.contains(agency)){
					itemAdded = agency;
				}
			}
			if(itemAdded == 'all' || selectedAgencies.indexOf('all') != -1){
				selectedAgencies = new List<String>{itemAdded};
			}
		}
		selectedAgenciesReplica = new Set<String>(selectedAgencies);
		
		selectedSchool ='all';
		schoolOptions = null;
		
		campusOptions = null;
		selectedCampus = 'all';
	}
	
	public IPFunctions.CampusAvailability ca {get{if(ca==null) ca = new IPFunctions.CampusAvailability(); return ca;}set;}
	
	public String selectedSchool {get{if(selectedSchool==null) selectedSchool = 'all'; return selectedSchool;}set;}
	public  List<SelectOption> schoolOptions {
		get{
			if(schoolOptions == null){
				schoolOptions = new List<SelectOption>();
		
				//schoolOptions = ca.getAvlSchools(selectedAgency);
				if(selectedAgencies.size() == 1 && selectedAgencies.get(0) != 'all'){
					schoolOptions = ca.getAvlSchools(selectedAgencies.get(0));
				}else{
					schoolOptions = new List<SelectOption>();
					schoolOptions.add(new SelectOption('all','-- All --'));
				}
				
			}
			return schoolOptions;
		}set;}
		
	public void changeSchool(){
		campusOptions = null;
		selectedCampus = 'all';
	}		
			
	public String selectedCampus {get{if(selectedCampus==null) selectedCampus = 'all'; return selectedCampus;}set;}
	public List<SelectOption> campusOptions {
		get{
			if(campusOptions == null && selectedSchool != 'all'){
				
				campusOptions = new List<SelectOption>();				
				campusOptions = ca.getSchoolCampus(selectedSchool);				
			}
			
			if(campusOptions == null && selectedSchool == 'all'){
				campusOptions = new List<SelectOption>();	
				campusOptions.add(new SelectOption('all', '-- All --'));
			}
			
			return campusOptions;
		}
		set;
	}
}