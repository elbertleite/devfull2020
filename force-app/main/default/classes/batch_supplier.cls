global class batch_supplier implements Database.Batchable<sObject> {
	
	global String field;
	global Set<String> agencies;
	global Map<String, Decimal> rankingMap;
	
	global batch_supplier(Set<String> agencies, String field, Map<String, Decimal> rankingMap){
		this.agencies = agencies;
		this.field = field;
		this.rankingMap = rankingMap;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC){   		
   		String query = 'select Supplier__c, Rank__c, Student_Rank__c from Supplier__c where Agency__c in :agencies and record_type__c = \'Campus\' order by Agency__c';   		
    	return Database.getQueryLocator(query);
   	}

   	global void execute(Database.BatchableContext BC, List<Supplier__c> scope){
     	
    	for(Supplier__c sup : scope)    		
    		sup.put(field, rankingMap.get(sup.Supplier__c));
    	
		update scope;
     	
   	}

   	global void finish(Database.BatchableContext BC){
   		
   	}



}