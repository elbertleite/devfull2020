@isTest
private class LimitsController_test {
	
	@isTest static void test_method_one() {
		LimitsController test = new LimitsController();
		test.getQueries();
		test.getLimitQueries();
		test.getQueryPerc();
		test.getQueryRows();
		test.getLimitQueryRows();
		test.getQueryRowsPerc();
		test.getDmlStatements();
		test.getLimitDmlStatements();
		test.getDmlStatementsPerc();
		test.getDmlRows();
		test.getLimitDmlRows();
		test.getDmlRowsPerc();
		test.getCpuTime();
		test.getLimitCpuTime();
		test.getCpuTimePer();
		test.getHeapSize();
		test.getLimitHeapSize();
		test.getLimitHeapSizePer();
	}
	

}