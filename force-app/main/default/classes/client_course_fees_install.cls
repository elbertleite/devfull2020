public with sharing class client_course_fees_install {
	
	public class installmentFeesDetails{
		public string feeId{get;set;}
		public string feeName{get;set;}
		public decimal feeValue{get;set;}
		public string feeType{get;set;} //(original or Added) or prmotion related FeeId
		public decimal originalValue{get;set;}
		public decimal keepFee {get;set;}
		public decimal feeAmendment {get;set;}
		public decimal keepFeeAmendment {get;set;}
		public installmentFeesDetails(string feeId, string feeName, decimal feeValue, string feeType, decimal originalValue, decimal keepFee, decimal feeAmendment, decimal keepFeeAmendment){
			this.feeId = feeId;
			this.feeName = feeName;
			this.feeValue = feeValue;
			this.feeType = feeType;
			this.originalValue = originalValue;
			this.keepFee = keepFee;
			this.feeAmendment = feeAmendment;
			this.keepFeeAmendment = keepFeeAmendment;
		}
	}
	
	public client_course_instalment__c selectedInstallment{get; set;}
	public list<client_course_fees__c> courseFees{get; set;}
	public map<string,installmentFeesDetails> intallmentFees{get; set;}
	public map<string,installmentFeesDetails> intallmentPromotions{get; set;}
	
	private map<string, client_course_fees__c> mapFees = new map<string, client_course_fees__c>();
	private map<string, client_course_fees__c> relatedPromotions = new map<string, client_course_fees__c>();
	private map<string, client_course_fees__c> mapAllCourseFees = new map<string, client_course_fees__c>();
	
	private string instId;
	public client_course_fees_install(ApexPages.StandardController ctr){
		instId = ctr.getId();
		loadFees();
	}
	
	public void loadFees(){	
		selectedInstallment = [SELECT id, client_course__c, client_course__r.Total_School_Payment__c, client_course__r.Client__r.name, client_course__r.Course_Name__c, client_course__r.Campus_Course__r.campus__r.ParentId, client_course__r.Total_Tuition__c, Due_Date__c, Extra_Fee_Value__c, 
								Extra_Fee_Discount_Value__c, Instalment_Value__c, Number__c, Related_Fees__c, Related_Promotions__c, Tuition_Value__c,  Commission_Value__c, Commission_Tax_Value__c, Kepp_Fee__c, Scholarship_Taken__c, isPFS__c,
								client_course__r.Course_Length__c, client_course__r.Start_Date__c, client_course__r.End_Date__c, client_course__r.Unit_Type__c, Total_School_Payment__c, Discount__c, client_course__r.Total_Instalments__c FROM client_course_instalment__c 
							WHERE id = :instId];
		 
		intallmentFees = new map<string,installmentFeesDetails>();
		intallmentPromotions = new map<string,installmentFeesDetails>();
		
		courseFees = new list<client_course_fees__c>();			
		if(selectedInstallment.Related_Fees__c != null && selectedInstallment.Related_Fees__c != ''){
			for(string lf:selectedInstallment.Related_Fees__c.split('!#')){
				System.debug('==>lf: '+lf);
				decimal feeAmendment = 0;
				decimal keepFeeAmendment = 0;
				if(lf.split(';;').size() >= 7){
			    	feeAmendment = decimal.valueOf(lf.split(';;')[6]);
				  	keepFeeAmendment = decimal.valueOf(lf.split(';;')[7]);
			 	}
				 
				intallmentFees.put(lf.split(';;')[0],new installmentFeesDetails(lf.split(';;')[0],lf.split(';;')[1],decimal.valueOf(lf.split(';;')[2]),lf.split(';;')[3],decimal.valueOf(lf.split(';;')[4]), decimal.valueOf(lf.split(';;')[5]), feeAmendment, keepFeeAmendment));
			}
		}	
		
		
		if(selectedInstallment.Related_Promotions__c != null && selectedInstallment.Related_Promotions__c != ''){
			for(string lp:selectedInstallment.Related_Promotions__c.split('!#')){
				System.debug('==>lf: '+lp);
				decimal feeAmendment = 0;
				decimal keepFeeAmendment = 0;
				if(lp.split(';;').size() >= 7){
			    	feeAmendment = decimal.valueOf(lp.split(';;')[6]);
				  	keepFeeAmendment = decimal.valueOf(lp.split(';;')[7]);
			 	}
				
				intallmentPromotions.put(lp.split(';;')[3],new installmentFeesDetails(lp.split(';;')[0],lp.split(';;')[1],decimal.valueOf(lp.split(';;')[2]),lp.split(';;')[3],decimal.valueOf(lp.split(';;')[4]), decimal.valueOf(lp.split(';;')[5]), feeAmendment, keepFeeAmendment));
			}
		}		
		
		for(client_course_fees__c ccf:[Select Allow_Change_Unit__c, client_course__c, Commission_Value__c, Course_Extra_Fee__c, Course_Extra_Fee_Dependent__c, Extra_Fee_Discount_Value__c, Fee_Name__c, Fee_Type__c, isOptional__c, 
										isPromotion__c, isSelected__c,Number_Units__c, Promotion_Discount_Type__c, Promotion_Discount_Value__c, Promotion_Name__c, Id, relatedPromotionFee__c, Total_Value__c, Original_Total_Value__c, 
										Original_Value__c, Original_Fee_Type__c, Original_Number_Units__c, Original_Fee_Name__c, Unit_Type__c, Value__c 
											from client_course_fees__c where client_course__c = :selectedInstallment.client_course__c]){
			if(intallmentFees.containsKey(ccf.id)){
				//if(ccf.isPromotion__c && ccf.relatedPromotionFee__c != null)
				//	relatedPromotions.put(ccf.relatedPromotionFee__c, ccf);
				if(!ccf.isPromotion__c)
					mapFees.put(ccf.Fee_Name__c, ccf);
			}			
			mapAllCourseFees.put(ccf.Fee_Name__c, ccf);		
			if(ccf.isPromotion__c && ccf.relatedPromotionFee__c != null)
				relatedPromotions.put(ccf.relatedPromotionFee__c, ccf);
		}
		
	}
	
	
	public list<client_course_fees__c> listFeesToAdd{get{if(listFeesToAdd == null) listFeesToAdd = new list<client_course_fees__c>(); return listFeesToAdd;} set;}
	    
	private List<SelectOption> numberOfUnits;
	public List<SelectOption> getNumberOfUnits() {
		if(numberOfUnits == null){
	        numberOfUnits = new List<SelectOption>();
			for(integer i = 1; i <= 52; i++)
	            numberOfUnits.add(new SelectOption(string.valueOf(i),string.valueOf(i)));
		}
        return numberOfUnits;
    }
    
    private List<SelectOption> unitTypes;
	public List<SelectOption> getUnitTypes(){
		if(unitTypes == null){
			unitTypes = new List<SelectOption>();
			unitTypes.add(new SelectOption('', '--None--'));
			for(String uType : IPFunctions.getUnitTypes())
				unitTypes.add(new SelectOption(uType, uType));
		}
		unitTypes.sort();
		return unitTypes;
		
	}
	    
	public client_course_fees__c newFee { 
		get {
			if(newFee == null)
				newFee = new client_course_fees__c(Client_Course__c = selectedInstallment.client_course__c, Extra_Fee_Discount_Value__c = 0);
			return newFee; 
		}
		set;
	}
	
	/*public client_course_fees__c newDiscount { 
		get {
			if(newDiscount == null)
				newDiscount = new client_course_fees__c(Client_Course__c = selectedInstallment.client_course__c);
			return newDiscount; 
		}
		set;
	}*/
	
	public pageReference addNewFee(){
    	newFee.Client_Course__c = selectedInstallment.client_course__c;
    	newFee.isAmendment__c = true;
    	newFee.client_course_instalment__c = selectedInstallment.id;
    	
    	System.debug('==>mapAllCourseFees: '+mapAllCourseFees);
    	System.debug('==>newFee.Fee_Name__c: '+newFee.Fee_Name__c);
    	if(mapAllCourseFees.containsKey(newFee.Fee_Name__c)) // to avoid to add duplicated course fee
    		newFee.id = mapAllCourseFees.get(newFee.Fee_Name__c).Id;
    	
    	System.debug('==>newFee: '+newFee);
    	
    	if(mapFees.containsKey(newFee.Fee_Name__c)){
    		string feeId = mapFees.get(newFee.Fee_Name__c).id;
    		newFee.errorMsg__c = 'This fee will replace the current value of ' + intallmentFees.get(feeId).feeValue;
    			
    		//System.debug('==>relatedPromotions: '+relatedPromotions);
    		//newFee.Total_Value__c -= relatedPromotions.get(feeId).Total_Value__c;
    		
    		newFee.Original_Fee_Type__c = mapFees.get(newFee.Fee_Name__c).Original_Fee_Type__c;
			newFee.Original_Number_Units__c = mapFees.get(newFee.Fee_Name__c).Original_Number_Units__c;
			newFee.Original_Fee_Name__c = mapFees.get(newFee.Fee_Name__c).Original_Fee_Name__c;
    		
    		newFee.Original_Total_Value__c = mapFees.get(newFee.Fee_Name__c).Original_Total_Value__c;
    		newFee.Original_Value__c = mapFees.get(newFee.Fee_Name__c).Original_Value__c;
    	}else if(newFee.id == null){
    		newFee.Original_Total_Value__c = newFee.Total_Value__c;
    		newFee.Original_Value__c = newFee.Value__c;
    		
    		newFee.Original_Fee_Type__c = newFee.Fee_Type__c;
			newFee.Original_Number_Units__c = newFee.Number_Units__c;
			newFee.Original_Fee_Name__c = newFee.Fee_Name__c;
    	}else if(newFee.id != null){
    		newFee.Original_Total_Value__c = mapAllCourseFees.get(newFee.Fee_Name__c).Original_Total_Value__c;
    		newFee.Original_Value__c =  mapAllCourseFees.get(newFee.Fee_Name__c).Original_Value__c;
    		
    		newFee.Original_Fee_Type__c = mapAllCourseFees.get(newFee.Fee_Name__c).Original_Fee_Type__c;
			newFee.Original_Number_Units__c = mapAllCourseFees.get(newFee.Fee_Name__c).Original_Number_Units__c;
			newFee.Original_Fee_Name__c = mapAllCourseFees.get(newFee.Fee_Name__c).Original_Fee_Name__c;
    	}
    	
    	for(integer i = 0; i<listFeesToAdd.size(); i++){
    		if(listFeesToAdd[i].Fee_Name__c == newFee.Fee_Name__c){
    			listFeesToAdd.remove(i);
    			break;
    		}
    	}		
    	
    	listFeesToAdd.add(newFee);	
    	newFee = null;
    	
    	return null;
    }
    
    private List<String> extraFeeNames;
	public List<String> getExtraFeeNames(){
		
		if(extraFeeNames == null){
			extraFeeNames = new List<String>();
			System.debug('==>schoolId: '+selectedInstallment.client_course__r.Campus_Course__r.campus__r.ParentId);
			
			IPFunctions.schoolProductsNoSharing schoolFees = new IPFunctions.schoolProductsNoSharing();
			for(Product__c prd :schoolFees.extraFeeNames(selectedInstallment.client_course__c) )
				extraFeeNames.add(prd.Name__c);
			
		}
		return extraFeeNames;	
		
	}
	
	/*private List<SelectOption> quoteFees;
	public List<SelectOption> getQuoteFees(){
		if(quoteFees == null){
			quoteFees = new List<SelectOption>();
			for(installmentFeesDetails cf : intallmentFees.values()){
				//if(!cf.isPromotion__c && !relatedPromotions.containsKey(cf.id))
					quoteFees.add(new SelectOption(cf.feeName, cf.feeName));
					
					//mapFees.put(cf.feeName, cf);
				//else if(cf.isPromotion__c && cf.relatedPromotionFee__c != null)
					//relatedPromotions.put(cf.relatedPromotionFee__c, cf.id);
			}
		}
		return quoteFees;
		
	}*/
	
	 public pageReference saveFees(){
	 	list<string> relatedFees = new list<string>();
	 	string addRelatedFees = '';
	 	decimal totalFees = 0;
		decimal totalPromotions = 0;
		set<string> sameFee = new set<string>();
		decimal addTotal_Value = 0;
		
    	if(listFeesToAdd.size() > 0){
    		
    		for(client_course_fees__c fu:listFeesToAdd){
    			if(mapFees.containsKey(fu.Fee_Name__c))
    				sameFee.add(mapFees.get(fu.Fee_Name__c).Id);
    		}
    		
    		
    		
			for(installmentFeesDetails cf : intallmentFees.values()){
				if(!sameFee.contains(cf.feeId)){
	    			relatedFees.add(cf.feeId + ';;' + cf.feeName + ';;' + cf.feeValue + ';;' + cf.feeType + ';;' + cf.originalValue + ';;'+ cf.keepFee+ ';;' + cf.feeAmendment + ';;' + cf.keepFeeAmendment); //original fee
	    			totalFees+= cf.feeValue;
				}
			}
			
			decimal valueTotal = 0;
    		for(client_course_fees__c f:listFeesToAdd){
    			valueTotal = f.Total_Value__c;
    			if(!mapFees.containsKey(f.Fee_Name__c)){
    				if(mapAllCourseFees.containsKey(f.Fee_Name__c))
    					f.Total_Value__c = mapAllCourseFees.get(f.Fee_Name__c).Total_Value__c + valueTotal;	
    			}else{
    				/*decimal valueTotalAux = f.Total_Value__c;
    				//string feeId = mapFees.get(f.Fee_Name__c).Id;
    				f.Total_Value__c += intallmentFees.get(f.Id).feeValue;
    				valueTotal = f.Total_Value__c;
    				
    				f.Total_Value__c = mapAllCourseFees.get(f.Fee_Name__c).Total_Value__c + valueTotalAux;	*/
    				
    				f.Total_Value__c = mapAllCourseFees.get(f.Fee_Name__c).Total_Value__c - intallmentFees.get(f.Id).feeValue + valueTotal;	
    				
    			}
    			
    			//if(relatedPromotions.containsKey(f.Id))
    			//	f.Total_Value__c -= relatedPromotions.get(f.Id).Total_Value__c; //subtract fee discount
    			
    			//System.debug('==>intallmentFees.get(f.id).originalValue: '+intallmentFees.get(f.id).originalValue);
    			System.debug('==>f.Original_Total_Value__c: '+f.Original_Total_Value__c);
    			if(intallmentFees.containsKey(f.id))
    				relatedFees.add(f.id + ';;' + f.Fee_Name__c + ';;' + valueTotal + ';;1' + ';;' + intallmentFees.get(f.id).originalValue + ';;'+ intallmentFees.get(f.id).keepFee+ ';;' + intallmentFees.get(f.id).feeAmendment + ';;' + intallmentFees.get(f.id).keepFeeAmendment); //new fee added/replaced
    			else {
    				relatedFees.add(f.id + ';;' + f.Fee_Name__c + ';;' + valueTotal + ';;1' + ';;' + f.Original_Total_Value__c + ';;0'+ ';;0'+ ';;0'); //new fee added/replaced
    			}
    			totalFees+= valueTotal;
    		}
    		
    		upsert listFeesToAdd;
    		
    		list<string> relatedFeesToAdd = new list<string>();
    		map<string, string> mapFeeId = new map<string, string>();
    		for(client_course_fees__c ccf:[Select id, Fee_Name__c from client_course_fees__c where client_course_instalment__c = :selectedInstallment.id])
    			mapFeeId.put(ccf.Fee_Name__c, ccf.id);
    		
    		
    		for(string st:relatedFees){
				if(st.split(';;')[0] == 'null' && mapFeeId.containsKey(st.split(';;')[1]))
    				relatedFeesToAdd.add(mapFeeId.get(st.split(';;')[1]) + ';;' + st.split(';;')[1] + ';;' + st.split(';;')[2] + ';;1' + ';;' + st.split(';;')[4]  + ';;' + st.split(';;')[5]);
				else relatedFeesToAdd.add(st.split(';;')[0] + ';;' + st.split(';;')[1] + ';;' + st.split(';;')[2] + ';;1' + ';;' + st.split(';;')[4]  + ';;' + st.split(';;')[5]);
			}
    		
    		
    		System.debug('==>relatedFeesToAdd: '+relatedFeesToAdd);
    		
    		if(relatedFeesToAdd.size() > 0)
				addRelatedFees = string.join(relatedFeesToAdd,'!#');
			
			selectedInstallment.Related_Fees__c = addRelatedFees;
			selectedInstallment.Extra_Fee_Value__c = totalFees;
			selectedInstallment.Instalment_Value__c = selectedInstallment.Tuition_Value__c + totalFees - selectedInstallment.Discount__c;
			selectedInstallment.isAmendment__c = true;
			
			if(selectedInstallment.isPFS__c)
				selectedInstallment.Total_School_Payment__c = selectedInstallment.Tuition_Value__c + selectedInstallment.Extra_Fee_Value__c;
			else selectedInstallment.Total_School_Payment__c = selectedInstallment.Tuition_Value__c + selectedInstallment.Extra_Fee_Value__c - selectedInstallment.Commission_Value__c - selectedInstallment.Commission_Tax_Value__c - selectedInstallment.Kepp_Fee__c - selectedInstallment.Scholarship_Taken__c;
			
			selectedInstallment.client_course__r.Total_School_Payment__c += selectedInstallment.Extra_Fee_Value__c;
			selectedInstallment.client_course__r.Total_Instalments__c += selectedInstallment.Extra_Fee_Value__c;
			
			update selectedInstallment;
			update selectedInstallment.client_course__r;
			
    		listFeesToAdd = new list<client_course_fees__c>();
    		//quoteFees = null;	
    		recalculateCoursesTotal();
    		loadFees();	
    	}
    		
    	return null;
    }
    
    public void recalculateCoursesTotal(){
    	decimal totalFees = 0;
		decimal totalPromotions = 0;
		for(client_course_fees__c f:[Select id, Total_Value__c, isPromotion__c, Fee_Name__c, Extra_Fee_Discount_Value__c from client_course_fees__c where client_course__c = :selectedInstallment.client_course__c]){
			if(!f.isPromotion__c){
				totalFees += f.Total_Value__c;
			}else {
				totalPromotions += f.Total_Value__c;
				//totalFees += f.Total_Value__c;
			}
		}
		update new client_course__c(id = selectedInstallment.client_course__c, Total_Extra_Fees__c = totalFees, Total_Extra_Fees_Promotion__c = totalPromotions, Total_Course__c = selectedInstallment.client_course__r.Total_Tuition__c + totalFees - totalPromotions, total_Extra_Fees_Unallocated__c = 0);
		  	
    }
    
    /*  public pageReference saveDiscount(){
    	
    	list<string> relatedPromotionsList = new list<string>();
    	string addRelatedPromotions = '';
    	System.debug('==>mapFees '+mapFees);
    	if(mapFees.containsKey(newDiscount.Fee_Name__c) && mapFees.get(newDiscount.Fee_Name__c).Total_Value__c < newDiscount.Value__c){
    		newDiscount.Value__c.addError('Value bigger than Fee');
    		return null;
    	}
    	string feename = newDiscount.Fee_Name__c;
    	System.debug('==>feename '+feename);
    	
    	newDiscount.Client_Course__c = selectedInstallment.client_course__c;
    	newDiscount.isAmendment__c = true;
    	newDiscount.client_course_instalment__c = selectedInstallment.id;
    	newDiscount.isPromotion__c = true;
    	newDiscount.Total_Value__c = newDiscount.Value__c;
    	newDiscount.relatedPromotionFee__c = mapFees.get(feename).id;
    	newDiscount.Fee_Name__c += ' ' + newDiscount.Promotion_Name__c;
    	
    	System.debug('==>newDiscount.Fee_Name__c '+newDiscount.Fee_Name__c);
    	System.debug('==>feename '+feename);
    	decimal samefeeValue = 0;
    	if(mapAllCourseFees.containsKey(feename)){ // to avoid to add duplicated course fee discount
    		string feeId = mapAllCourseFees.get(feename).Id;
    		
    		decimal feeDiscount = 0;
    		if(intallmentPromotions.containsKey(newDiscount.relatedPromotionFee__c))
    			feeDiscount = intallmentPromotions.get(newDiscount.relatedPromotionFee__c).feeValue;
    			
    		System.debug('==>relatedPromotions '+relatedPromotions);
    		System.debug('==>newDiscount.Total_Value__c '+newDiscount.Total_Value__c);
    		System.debug('==>newDiscount.Value__c '+newDiscount.Value__c);
    		System.debug('==>feeDiscount '+feeDiscount);
    		System.debug('==>feeId '+feeId);
    		
    		if(!relatedPromotions.containsKey(feeId)){
    			//newDiscount.Total_Value__c += newDiscount.Value__c;
    			newDiscount.Original_Value__c = newDiscount.Value__c;
    			newDiscount.Original_Total_Value__c = newDiscount.Value__c;
    		}else{
    			newDiscount.id = relatedPromotions.get(feeId).Id;
    			newDiscount.Total_Value__c = relatedPromotions.get(feeId).Total_Value__c + newDiscount.Value__c  - feeDiscount;
    			if(relatedPromotions.get(feeId).Value__c != relatedPromotions.get(feeId).Total_Value__c)
    				samefeeValue = relatedPromotions.get(feeId).Value__c;
    		}
    	}
    	 
    	upsert newDiscount;
    	
    	if(selectedInstallment.Related_Promotions__c != null){
	    	for(string rp:selectedInstallment.Related_Promotions__c.split('!#')){
	    		System.debug('==> '+rp.split(';;')[0] +' != ' +newDiscount.id);
	    		if(rp.split(';;')[0] != newDiscount.id)	
					relatedPromotionsList.add(rp.split(';;')[0] + ';;' + rp.split(';;')[1] + ';;' + rp.split(';;')[2] + ';;' + rp.split(';;')[3] + ';;' + rp.split(';;')[4]);
			}
    	}
    	
    	
    	if(intallmentPromotions.containsKey(newDiscount.relatedPromotionFee__c))
			relatedPromotionsList.add(newDiscount.id + ';;' + newDiscount.Fee_Name__c + ';;' + newDiscount.Value__c + ';;' + newDiscount.relatedPromotionFee__c + ';;' + intallmentPromotions.get(newDiscount.relatedPromotionFee__c).originalValue); //new fee added/replaced
		else {
			relatedPromotionsList.add(newDiscount.id + ';;' + newDiscount.Fee_Name__c + ';;' + newDiscount.Value__c + ';;' + newDiscount.relatedPromotionFee__c + ';;0'); //new fee added/replaced
		}
    	
    	mapFees.get(feename).Extra_Fee_Discount_Value__c = newDiscount.Total_Value__c;
    	update mapFees.get(feename);
    	
   	//	update new client_course__c(id = selectedInstallment.client_course__c, Total_Extra_Fees_Promotion__c = selectedInstallment.Total_Extra_Fees_Promotion__c + newDiscount.Total_Value__c, Total_Course__c = selectedInstallment.Total_Course__c - newDiscount.Total_Value__c); 	
    	
    	
    	if(relatedPromotions.size() > 0)
			addRelatedPromotions = string.join(relatedPromotionsList,'!#');
		
		selectedInstallment.Related_Promotions__c = addRelatedPromotions;
		selectedInstallment.Extra_Fee_Discount_Value__c += newDiscount.Value__c - samefeeValue;
		//selectedInstallment.Extra_Fee_Value__c = selectedInstallment.Extra_Fee_Value__c - newDiscount.Value__c;
		selectedInstallment.Instalment_Value__c = selectedInstallment.Tuition_Value__c + selectedInstallment.Extra_Fee_Value__c - selectedInstallment.Extra_Fee_Discount_Value__c;
		
		update selectedInstallment;
    	
    	newDiscount = null;
    	quoteFees = null;
    	recalculateCoursesTotal();
    	loadFees();   	
    		
    	return null;
    }*/

}