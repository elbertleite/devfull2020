public with sharing class ReportQuoteDetailsAgencyGroupController{

	
	public ReportQuoteDetailsAgencyGroupController(){
		selectedAgency = userDetails.Contact.AccountId;
		selectedQuoteCity = 'all';
		ca = new IPFunctions.CampusAvailability();
	}
	
	//return the user's agency 
	public User userDetails{
		get{
			if (userDetails == null)
				userDetails = [SELECT Contact.AccountId, Contact.Account.Name, Contact.Account.ParentId, Name, Email, Contact.Export_Report_Permission__c FROM User WHERE id = :UserInfo.getUserId()];
			return userDetails;
		}
		set;
	}
	
	public Contact fromDate {
		get{
			if(fromDate == null){
				fromDate = new Contact();
				fromDate.Expected_Travel_Date__c = system.today();
				fromDate.Expected_Travel_Date__c = fromDate.Expected_Travel_Date__c.addMonths(-1);
			}
			return fromDate;}
		set;}
			
	public Contact toDate {
		get{
			if(toDate == null){
				toDate = new Contact();
				toDate.Expected_Travel_Date__c = system.today();
			}
			return toDate;}
			set;}

	
	public static string selectedQuoteCountry{get{if(selectedQuoteCountry == null) selectedQuoteCountry = 'all'; return selectedQuoteCountry;} set;}
	
	public List<SelectOption> CountryOptions{
		get{			
			if(CountryOptions == null){
				CountryOptions = IPFunctions.getAllCountries();
				CountryOptions.add(0, new SelectOption('all','--All--'));
			}
			return CountryOptions;
		}
		set;
	}
	
	private list<string> Countries(){
		list<string> ls = new list<string>();
		if(selectedQuoteType == 'std')
			for(AggregateResult ar: [Select Campus_Country__c from Quotation_Detail__c where Campus_Country__c != null and  Campus_Course__c != null group by Campus_Country__c])
				ls.add((string)ar.get('Campus_Country__c'));
		else
			for(AggregateResult ar: [Select Campus_Country__c from Quotation_Detail__c where Campus_Country__c != null and  Campus_Course__c != null group by Campus_Country__c])
				ls.add((string)ar.get('Campus_Country__c'));	
		return ls;
	}
	
	public void searchCountry(){
		CountryOptions = null;
		searchCity();		
	}
	
	public PageReference searchCity(){
		selectedQuoteCity = null;
		CityOptions = null;
		searchSchools();
		searchCourseNames();
		return null;
	}
	
	public void searchSchools(){
		selectedSchool = '';	
		schools = null;
//		searchCourseTypes();		
	}
	
	/*public void searchCourseTypes(){
		selectedCourseType = null;
		courseTypes = null;
		searchCourseFields();
	}
	
	public void searchCourseFields(){
		selectedCourseField = null;
		courseFields = null;
	}*/
	
	public String sCourseFilter {get;set;}
	public void searchCourseNames(){
		sCourseFilter = 'all';
		coursesFilter = null;
	}
	
	public List<SelectOption> coursesFilter {
		get {
			List<SelectOption> courses = new List<SelectOption>();
			courses.add(new SelectOption('all', '--All--'));
			system.debug('Selected School===>' + selectedSchool);
			 
			if(selectedSchool!='all' && selectedSchool !=''){
				if(selectedQuoteType != 'custom'){
					
					String sql = ' SELECT Course__c, Course__r.Name  FROM Campus_Course__c WHERE Campus__c = \'' + selectedSchool + '\' ORDER BY Course__r.Name ';
					system.debug('sqlCoursesFilter===>' + sql);
					for(Campus_Course__c course : Database.query(sql)){
						courses.add(new SelectOption(course.Course__c, course.Course__r.Name ));
					}
				}
				else{
					String sql = ' SELECT Course_Name__c FROM Quotation_Detail__c WHERE Campus_Name__c = \'' + selectedSchool + '\' GROUP BY Course_Name__c ORDER BY Course_Name__c  ';
					system.debug('sqlCoursesFilter===>' + sql);
					for(AggregateResult course : Database.query(sql)){
						courses.add(new SelectOption((string)course.get('Course_Name__c'), (string)course.get('Course_Name__c')));
					}
				}
			}/*else{
				
					String sql = 'SELECT Course__c, Course__r.Name  FROM Campus_Course__c WHERE Campus__r.BillingCountry = \'' + selectedQuoteCountry + '\'';
					
					if(selectedQuoteCity!= null && selectedQuoteCity != 'all')
						sql+='Campus__r.BillingCity = \'' + selectedQuoteCity + '\'';
					
					sql+=' ORDER BY Course__r.Name ';
										
					system.debug('sqlCoursesFilter===>' + sql);
					for(Campus_Course__c course : Database.query(sql)){
						courses.add(new SelectOption(course.Course__c, course.Course__r.Name ));
					}
				
			}	*/
			return courses;
		}		
		set;
		
	}
	
	public static string selectedQuoteCity{get{if(selectedQuoteCity == null) selectedQuoteCity = 'all'; return selectedQuoteCity;} set;}
	
	public List<SelectOption> CityOptions {
		
		get {
			if(CityOptions == null){
				CityOptions = new List<SelectOption>();
				CityOptions.add(new SelectOption('all','--All--'));
				
				Set<String> ls = new Set<String>();
				if(selectedQuoteCountry != 'all' && selectedQuoteCountry != '')	{
					if(selectedQuoteType == 'std')				
						for(AggregateResult ar : [Select Campus_City__c from Quotation_Detail__c where Campus_Country__c = :selectedQuoteCountry and  Campus_Course__c != null  group by Campus_City__c] )
							ls.add((string)ar.get('Campus_City__c'));
					else
						for(AggregateResult ar : [Select Campus_City__c from Quotation_Detail__c where   Campus_Country__c = :selectedQuoteCountry and  Campus_Course__c = null  group by Campus_City__c])
							ls.add((string)ar.get('Campus_City__c'));
					
					List<String> ordered = new List<String>(ls);
					ordered.sort();
					for(String ar: ordered)
						CityOptions.add(new SelectOption(ar,ar));
				}
			}
			return CityOptions;
		} 
		set;
	}
	
	map<string,string> selectedGroupName; 
	public String selectedAgencyGroup {get{if(selectedAgencyGroup == null) selectedAgencyGroup = userDetails.Contact.Account.ParentId; return selectedAgencyGroup;}set;}
	public List<SelectOption> agencyGroupOptions {
		get{
		if(agencyGroupOptions == null){
			selectedGroupName = new map<string,string>();
			agencyGroupOptions = new List<SelectOption>();
			//agencyGroupOptions.add(new SelectOption('', 'Select Agency Group'));	
			for(Account ag : [select id, name from Account where RecordType.Name = 'Agency Group' order by Name]){
				agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
			selectedGroupName.put(ag.id, ag.Name);
			}		
		}
		return agencyGroupOptions;
	}
		set;
	}
	
	public string getGroupName(){
		return 	selectedGroupName.get(selectedAgencyGroup);
	}	
	
	public void searchAgencies(){
		agencyOptions = null;
		selectedAgency = null;
		userOptions = null;
		selectedUser = null;
	}
	
	public String selectedAgency {get{if(selectedAgency == null) selectedAgency = ''; return selectedAgency;}set;}
	public List<SelectOption> agencyOptions {
		get{
		if(agencyOptions == null){
			agencyOptions = new List<SelectOption>();
			agencyOptions.add(new SelectOption('', '--All--'));
			agenciesId = new Set<ID>();
			for(Account ag : [Select Id, Name from Account A where ParentId = :selectedAgencyGroup order by Name]){
			agencyOptions.add(new SelectOption(ag.Id, ag.Name));	
			agenciesId.add(ag.Id);
			}	
		}
		return agencyOptions;
	}
		set;
	}
	
	public void searchUsers(){
		userOptions = null;
	}
	
	private set<id> agenciesId;
	
	List<id> lEmployee = new List<id>();
	
	public list<String> selectedUser {get{if(selectedUser == null) selectedUser = new list<String>(); return selectedUser;}set;}
	public List<SelectOption> userOptions {
		get{
		if(userOptions == null){
			userOptions = new List<SelectOption>();
			List<SelectOption> ao = agencyOptions;
			
			system.debug('selectedAgency===' + selectedAgency );
			system.debug('agenciesId===' + agenciesId );
			
			List<User> users;		
			if(selectedAgency!= '')
				users = [SELECT Contact.AccountId, Name, Id FROM User WHERE Contact.AccountId = :selectedAgency and Contact.Chatter_Only__c = false order by name];
			else 
				users = [SELECT Contact.AccountId, Name, Id FROM User WHERE Contact.AccountId in :agenciesId and Contact.Chatter_Only__c = false order by name];
			
			for(User ac : users){
				userOptions.add(new SelectOption(ac.Id, ac.Name));	
				lEmployee.add(ac.Id);
			}
		}
		return userOptions;
	}
		set;
	}
	
	private IPFunctions.CampusAvailability ca;
	
	public List<SelectOption> schools{
		get{
			if(schools == null && selectedQuoteCountry =='all'){
				schools = new List<SelectOption>();
				schools.add(new SelectOption('', '--All--'));
			}
			else if(schools == null && selectedQuoteCountry != 'all' && selectedQuoteCountry != ''){
				schools = new List<SelectOption>();
				schools.add(new SelectOption('', '--All--'));
				if(selectedQuoteType == 'std'){	
				
					schools.addAll(ca.getSchools(userDetails.Contact.AccountId, false,selectedQuoteCountry, selectedQuoteCity));   
				} else{
					String sql = '';
					sql = ' Select Campus_Name__c from Quotation_Detail__c where Campus_Course__c = null  and Campus_Name__c != null ';
				
					if(selectedQuoteCountry != 'all')
						sql += ' and Campus_Country__c = \'' + selectedQuoteCountry + '\' ';
					
					if(selectedQuoteCity != 'all')
						sql += ' and Campus_City__c = \'' + selectedQuoteCity + '\'';
					
					
					sql += ' group by Campus_Name__c order by Campus_Name__c ';
					
					for(AggregateResult ar : Database.query(sql))
						schools.add(new SelectOption((string)ar.get('Campus_Name__c'),(string)ar.get('Campus_Name__c')));
					}
			}
			return schools;
		}
		set;
	}
	
	public String selectedSchool {get{if(selectedSchool == null) selectedSchool = ''; return selectedSchool;}set;}
	/*public List<SelectOption> schools {
		get{
		if(schools == null){
			schools = new List<SelectOption>();
			schools.add(new SelectOption('', '--All--'));
			String sql = '';
			
			if(selectedQuoteType == 'std'){
				sql = ' Select Campus_Course__r.Campus__r.Parentid, Campus_Course__r.Campus__r.Parent.Name from Quotation_Detail__c Q ' +
							 ' where Campus_Course__c != null ';
			
				if(selectedQuoteCountry != 'all')
					sql += ' and Campus_Country__c = \'' + selectedQuoteCountry + '\' ';
				
				if(selectedQuoteCity != 'all')
					sql += ' and Campus_City__c = \'' + selectedQuoteCity + '\'';
								
				sql += ' group by Campus_Course__r.Campus__r.Parentid, Campus_Course__r.Campus__r.Parent.Name order by Campus_Course__r.Campus__r.Parent.Name ';
				
				
				for(AggregateResult ar : Database.query(sql))
					schools.add(new SelectOption((string)ar.get('Parentid'),(string)ar.get('Name')));
				
			} else {
				
				sql = ' Select Campus_Name__c from Quotation_Detail__c where Campus_Course__c = null  and Campus_Name__c != null ';
			
				if(selectedQuoteCountry != 'all')
					sql += ' and Campus_Country__c = \'' + selectedQuoteCountry + '\' ';
				
				if(selectedQuoteCity != 'all')
					sql += ' and Campus_City__c = \'' + selectedQuoteCity + '\'';
				
				
				sql += ' group by Campus_Name__c order by Campus_Name__c ';
				
				for(AggregateResult ar : Database.query(sql))
					schools.add(new SelectOption((string)ar.get('Campus_Name__c'),(string)ar.get('Campus_Name__c')));
				
			}		
		}	
		return schools;
	}
		set;
	}*/
			
	/*public String selectedCourseType {get{if(selectedCourseType == null) selectedCourseType = 'all'; return selectedCourseType;}set;}
	public List<SelectOption> courseTypes {
		get{
		if(courseTypes == null){
			courseTypes = new List<SelectOption>();
			courseTypes.add(new SelectOption('all', '--All--'));
			
			String sql = ' Select Campus_Course__r.Course__r.Course_Type__c from Quote_detail__c where Campus_Course__r.Course__r.Course_Type__c != null ';
			
			if(selectedQuoteCountry != 'all')
				sql += ' and Country__c = \'' + selectedQuoteCountry + '\' ';
				
			if(selectedQuoteCity != 'all')
				sql += ' and City__c = \'' + selectedQuoteCity + '\'';
			
			if(selectedSchool != null && selectedSchool != '' && selectedQuoteType == 'std')				
				sql += ' and Campus_Course__r.Campus__r.Parentid = \'' + selectedSchool + '\'';
				
			sql += ' group by Campus_Course__r.Course__r.Course_Type__c order by Campus_Course__r.Course__r.Course_Type__c ';	
			
				
			for(AggregateResult ar : Database.query(sql))
				courseTypes.add(new SelectOption((string)ar.get('Course_Type__c'),(string)ar.get('Course_Type__c')));
		}	
			
		return courseTypes;
	}
		set;
	}
	
	
	
	public String selectedCourseField {get{if(selectedCourseField == null) selectedCourseField = 'all'; return selectedCourseField;}set;}
	public List<SelectOption> courseFields {
		get{
		if(courseFields == null){
			courseFields = new List<SelectOption>();
			courseFields.add(new SelectOption('all', '--All--'));
			
			if(	selectedCourseType != 'all' && selectedCourseType != '') {
			
				String sql = ' Select Campus_Course__r.Course__r.Type__c from Quote_detail__c where Campus_Course__r.Course__r.Type__c != null ';
				
				sql += ' and Campus_Course__r.Course__r.Course_Type__c = \'' + selectedCourseType + '\' ';
				
				if(selectedQuoteCountry != 'all')
					sql += ' and Country__c = \'' + selectedQuoteCountry + '\' ';
					
				if(selectedQuoteCity != 'all')
					sql += ' and City__c = \'' + selectedQuoteCity + '\'';
				
				if(selectedSchool != null && selectedSchool != '' && selectedQuoteType == 'std')				
					sql += ' and Campus_Course__r.Campus__r.Parentid = \'' + selectedSchool + '\'';
					
				sql += ' group by Campus_Course__r.Course__r.Type__c order by Campus_Course__r.Course__r.Type__c ';	
				
								
				for(AggregateResult ar : Database.query(sql))
					courseFields.add(new SelectOption((string)ar.get('Type__c'),(string)ar.get('Type__c')));
			}
		}	
			
		return courseFields;
	}
		set;
	}*/
	
	
	public String selectedQuoteType {get{if(selectedQuoteType == null) selectedQuoteType = 'std'; return selectedQuoteType;}set;}	
	public List<SelectOption> quoteType {
		get{
			if(quoteType == null){
		        quoteType = new List<SelectOption>(); 
		        quoteType.add(new SelectOption('std','Standard Quotes')); 
		        quoteType.add(new SelectOption('custom','Custom Quotes'));		         
    		}
			return quoteType;
		}
		set;
	}                   
  	
	public List<AgencyWrapper> agencyList {
		get{
		if(agencyList == null)
			agencyList = new List<AgencyWrapper>();
		return agencyList;
	}
		set;
	}
	
	public integer totalCourses{get{if(totalCourses == null) totalCourses = 0; return totalCourses;}set;}
	
	public class AgencyWrapper {
		public String agencyID {get;set;}
		public String agencyName {get;set;}
		public Integer quoteCount {get{if(quoteCount == null) quoteCount = 0; return quoteCount;}set;}
		
		public List<UserWrapper> userList {
			get{
			if(userList == null)
				userList = new List<UserWrapper>();
			return userList;
		}
			set;}
	}
	
	public class coursesPerCampus{
		public string country{get; set;}	
		public string city{get; set;}	
		public string campus{get; set;}	
		public string course{get; set;}	
		public integer totCourses{get{if(totCourses == null) totCourses = 0; return totCourses;}set;}
	}
	
	public class UserWrapper {
		public String userID {get;set;}
		public String userName {get;set;}
		public Integer quoteCount {get{if(quoteCount == null) quoteCount = 0; return quoteCount;}set;}
		public List<coursesPerCampus> quoteList {
			get{
			if(quoteList == null)
				quoteList = new List<coursesPerCampus>();
			return quoteList;
		}
			set;
		}
	}	
	
	public set<id> agencyIds {get;set;}
	public Map<String, String> agenciesMap { get;set; }
	public Map<String, String> selectSFUser { get{if(selectSFUser == null) selectSFUser = new Map<String, String>(); return selectSFUser;}set; }
	Set<String> userIDs = new Set<String>();
	public void search(){	
		agencyIds = new set<id>();
		system.debug('####### selectedQuoteCity: ' + selectedQuoteCity);
		totalCourses = 0;
		agencyList = null;
		List<AggregateResult> agencies;
		
		if(selectedAgencyGroup != null && selectedAgencyGroup != ''){
			
			agenciesMap = new Map<string, String>();
			if(selectedAgency == null || selectedAgency == ''){			
				system.debug('All agencies');
				for(Account ag : [SELECT Id, Name FROM Account WHERE ParentId = :selectedAgencyGroup and RecordType.Name = 'Agency' order by Name]){
					agenciesMap.put(ag.Id, ag.Name);				
					AgencyWrapper aw = new AgencyWrapper();
					aw.agencyID = ag.Id;
					aw.agencyName = ag.Name;			
					agencyList.add(aw);
					agencyIds.add(ag.Id);
					
				}
							
			} else {		
				system.debug('selected one agency');
				for(Account a : [Select id, Name  from Account A where id = :selectedAgency]){
					agenciesMap.put(a.Id, a.Name);
					AgencyWrapper aw = new AgencyWrapper();
					aw.agencyID = a.id;
					aw.agencyName = a.Name;
					agencyList.add(aw);
				}
			}
			
			system.debug('agenciesMap===>' + agenciesMap);
			
			List<User> users;		
			if(selectedUser.size() > 0)
				users = [SELECT Contact.AccountId, Name, Id FROM User WHERE Id in :selectedUser order by name desc];
			else 
				users = [SELECT Contact.AccountId, Name, Id FROM User WHERE Contact.AccountId in :agenciesMap.keySet() order by name desc];
			
			system.debug('@@@@@@@@@@ users: ' + users);
			userIDs = new Set<String>();
			for(User a : users){
				//if(!selectSFUser.containsKey(a.id))
				//	selectSFUser.put(a.id,a.User__c);
				userIDs.add(a.Id);
				UserWrapper uw = new UserWrapper();
				uw.userID = a.Id;
				uw.userName = a.Name;
				for(AgencyWrapper aw: agencyList)
					if(aw.agencyID == a.Contact.AccountId)
						aw.userList.add(uw);
			}
				
			if(selectedUser.size() == 1){
				ApexPages.currentPage().getParameters().put('userID',selectedUser[0]);
				searchQuotesByUser();
			}
			getNumberOfQuotesPerUser(userIDs);
		}
	}
	
	//Get the course quotes
	public void searchQuotesByCourse(){
		String campusID = ApexPages.currentPage().getParameters().get('cm');
		String courseID = ApexPages.currentPage().getParameters().get('cr');
		String mapFirstKey = ApexPages.currentPage().getParameters().get('fk');
		String mapSecKey = ApexPages.currentPage().getParameters().get('sk');
		String courseName = ApexPages.currentPage().getParameters().get('name');
		if(mapSecKey=='')
			mapSecKey=null;
		if(mapFirstKey=='')
			mapFirstKey=null;
		
		system.debug('campusID==>' + campusID + 'courseID==>' + courseID);
		system.debug('mapFirstKey==>' + mapFirstKey + 'mapSecKey==>' + mapSecKey);
		system.debug('AgencyGroup==>' + selectedAgencyGroup);
		system.debug('AgencyIds==>' + agencyIds);
		
		
		String sql = 'SELECT Quotation__r.Client__c, Quotation__r.Client__r.Name, Quotation__r.CreatedBy.Name, Quotation__r.Client__r.Nationality__c, Quotation__c, Quotation__r.Name, Quotation__r.Status__c, Quotation__r.CreatedDate, Quotation__r.Expiry_date__c ';
		sql += 		 'FROM Quotation_Detail__c WHERE Quotation__r.CreatedDate >= ' + FormatSqlDateTimeIni(fromDate.Expected_Travel_Date__c) + 'AND Quotation__r.CreatedDate <= '+ FormatSqlDateTimeFin(toDate.Expected_Travel_Date__c) + ' ' ; 

		if(selectedQuoteType == 'custom')
			sql += ' AND Campus_Course__c = null AND  Course_Name__c= \'' + courseName + '\'AND  Campus_Name__c= \'' + mapFirstKey + '\'';
		else 
			sql += ' AND Campus_Course__r.Campus__c = :campusID AND Campus_Course__r.Course__C = :courseID  ';
		
		sql += ' AND createdByid in :userIDs ';
		
		if(selectedQuoteCountry != 'all' && selectedQuoteCountry != '')
			sql += ' AND Campus_Country__c = \'' + selectedQuoteCountry + '\'';
	
		if(selectedQuoteCity != 'all' && selectedQuoteCity != '')
			sql += ' AND Campus_City__c = \'' + selectedQuoteCity + '\'';
		
		if(selectedSchool != null && selectedSchool != '')
			if(selectedQuoteType == 'std'){
				sql += ' AND Campus_Course__r.Campus__c = \'' + selectedSchool + '\'';
				
				if(sCourseFilter != 'all' && sCourseFilter != ''){
					sql += ' AND Campus_Course__r.Course__c = \'' + sCourseFilter + '\'';
				}
			}
			else {
				sql += ' AND Campus_Name__c = \'' + selectedSchool + '\'';
				
				if(sCourseFilter != 'all' && sCourseFilter != ''){
					sql += ' AND Course_Name__c = \'' + sCourseFilter + '\'';
				}					
			
		}
		if(selectedAgency!=null && selectedAgency!='')
			sql += ' AND Quotation__r.Agency__c = :selectedAgency order by Quotation__r.Expiry_date__c desc ';
		else
			sql += ' AND Quotation__r.Agency__c in :agencyIds order by Quotation__r.Expiry_date__c desc ';
		

			
		list<Quotation_Detail__c> quotes = Database.query(sql);
		
		if(mapCC.containsKey(mapFirstKey) && mapCC.get(mapFirstKey).containsKey(mapSecKey)){
			list<courseCampus> cc = mapCC.get(mapFirstKey).get(mapSecKey);
			
			if(selectedQuoteType != 'custom'){
				for(courseCampus course: cc){
					if(course.courseId == courseID){
						course.quotes = new list<Quotation_Detail__c>(quotes);
						break;
					}
				}
			}else{
				for(courseCampus course: cc){
					if(course.courseName == courseName){
						course.quotes = new list<Quotation_Detail__c>(quotes);
						break;
					}
				}
			}
			
			mapCC.get(mapFirstKey).put(mapSecKey,cc);
		}
	}
		
	public void searchQuotesByUser(){
		
		String theUserID = ApexPages.currentPage().getParameters().get('theUserID');
		String showALL = ApexPages.currentPage().getParameters().get('showALL');
		
		System.debug('==>theUserID: '+theUserID);
		if(theUserID != null && theUserID != ''){
			List<AggregateResult> quotes = new List<AggregateResult>();
			
			/*
				String sql = 'Select country__c, city__c, campus__c, Product_name__c, count(id) tot ' +
				' from Quote_detail__c Q where parent__c = null ';
			*/
			
			String sql = 'SELECT Campus_Country__c, Campus_City__c, Campus_Name__c, Course_Name__c, count(id) tot ' +
						 ' FROM  Quotation_Detail__c Q WHERE';
				
			/*if(selectedQuoteType == 'custom')
				sqlperCampus += ' productId__c like \'%00N20000000thp%\' ';	
			else sqlperCampus += ' Product_Type__c = \'Course\' ';
			*/
			if(selectedQuoteType == 'custom')
				sql += ' Campus_Course__c = null ';	
			else sql += ' Campus_Course__c != null ';
			
			sql += ' AND CreatedById = \'' + theUserID + '\'';
			/*if(selectQuoteStatus != '')
				sql += ' and Quote__r.Status__c = \'' + selectQuoteStatus + '\'';*/
		
			if(fromDate.Expected_Travel_Date__c != null)
				sql += ' AND CreatedDate >= ' + FormatSqlDateTimeIni(fromDate.Expected_Travel_Date__c) + ' ';
				
			if(toDate.Expected_Travel_Date__c != null)
				sql += ' AND CreatedDate <= ' + FormatSqlDateTimeFin(toDate.Expected_Travel_Date__c) + ' ';
		
			if(selectedQuoteCountry != 'all' && selectedQuoteCountry != '')
				sql += ' AND Campus_Country__c = \'' + selectedQuoteCountry + '\'';
		
			if(selectedQuoteCity != 'all' && selectedQuoteCity != '')
				sql += ' AND Campus_City__c = \'' + selectedQuoteCity + '\'';
			
			/*if(selectedCourseType != 'all' && selectedCourseType != '')
				sql += ' and Campus_Course__r.Course__r.Course_Type__c = \'' + selectedCourseType + '\'';
			
			if(selectedCourseField != 'all' && selectedCourseField != '')
				sql += ' and Campus_Course__r.Course__r.Type__c = \'' + selectedCourseField + '\'';
			*/
			
			if(selectedSchool != null && selectedSchool != '')
				if(selectedQuoteType == 'std'){
					sql += ' AND Campus_Course__r.Campus__c = \'' + selectedSchool + '\'';
					
					if(sCourseFilter != 'all' && sCourseFilter != ''){
						sql += ' AND Campus_Course__r.Course__c = \'' + sCourseFilter + '\'';
					}
				}
				else {
					sql += ' AND Campus_Name__c = \'' + selectedSchool + '\'';
					
					if(sCourseFilter != 'all' && sCourseFilter != ''){
						sql += ' AND Course_Name__c = \'' + sCourseFilter + '\'';
					}
				}
			if(selectedAgency!=null && selectedAgency!='')
				sql += ' AND Quotation__r.Agency__c = :selectedAgency   ';
			else
				sql += ' AND Quotation__r.Agency__c in :agencyIds   ';
			
				
			sql += ' GROUP BY Campus_Country__c, Campus_City__c, Campus_Name__c, Course_Name__c ORDER BY Campus_Country__c, Campus_City__c, count(id) desc ';	
			
			System.debug('==>sql: '+sql);
			quotes = Database.query(sql);
				
			
			coursesPerCampus cpc;
			for(AggregateResult q : quotes)
				for(AgencyWrapper aw: agencyList)
					for(UserWrapper uw : aw.userList)
						if(uw.userID == theUserID){
							cpc = new coursesPerCampus();
							cpc.country = (string)q.get('Campus_Country__c');
							cpc.city = (string)q.get('Campus_City__c');
							cpc.campus = (string)q.get('Campus_Name__c');
							cpc.course = (string)q.get('Course_Name__c');
							cpc.totCourses = (integer)q.get('tot');
							uw.quoteList.add(cpc);
						}
		}
		
	}
	
	class countQuotes{
		integer allQuotes;
		integer enrolled;
	}
	
	public class courseCampus{
		public string courseName {get; set;}
		public integer total {get; set;}
		public string courseId {get; set;}
		public string courseType {get; set;}
		public string campusId {get; set;}
		public List<Quotation_Detail__c> quotes {get;set;}
	}
	
	public map<string,integer> courseTypeTotal{get{if(courseTypeTotal == null) courseTypeTotal = new map<string,integer>(); return courseTypeTotal;} set;}
	
	public map<string,map<string,list<courseCampus>>> mapCC{get; set;}
	
	private void getNumberOfQuotesPerUser(Set<String> userIDs){
		system.debug('userIDs-----' + userIDs);
		
		//All Quotes
		string innerSql = '';
		//String sql = 'Select createdByid, Count(Name) tot from Quote_detail__c Q where parent__c = null  ';
		
		String sql = 'SELECT createdByid, Count(Name) tot FROM Quotation_Detail__c Q WHERE ';
		
		if(selectedQuoteType == 'custom')
			sql += ' Campus_Course__c = null ';	
		else 
			sql += ' Campus_Course__c != null ';
		
		sql += ' AND createdByid in :userIDs ';
		/*if(selectQuoteStatus != '')
			sql += ' and Quote__r.Status__c = \'' + selectQuoteStatus + '\'';*/
		
		if(fromDate.Expected_Travel_Date__c != null)
			innerSql += ' AND CreatedDate >= ' + FormatSqlDateTimeIni(fromDate.Expected_Travel_Date__c) + ' ';
				
		if(toDate.Expected_Travel_Date__c != null)
			innerSql += ' AND CreatedDate <= ' + FormatSqlDateTimeFin(toDate.Expected_Travel_Date__c) + ' ';
		
		if(selectedQuoteCountry != 'all' && selectedQuoteCountry != '')
			innerSql += ' AND Campus_Country__c = \'' + selectedQuoteCountry + '\'';
		
		if(selectedQuoteCity != 'all' && selectedQuoteCity != '')
			innerSql += ' AND Campus_City__c = \'' + selectedQuoteCity + '\'';
		
		/*if(selectedCourseType != 'all' && selectedCourseType != '')
			innerSql += ' AND Campus_Course__r.Course__r.Course_Type__c = \'' + selectedCourseType + '\'';
			
		if(selectedCourseField != 'all' && selectedCourseField != '')
			innerSql += ' AND Campus_Course__r.Course__r.Type__c = \'' + selectedCourseField + '\'';
		*/
		
		if(selectedSchool != null && selectedSchool != '')
			if(selectedQuoteType == 'std'){
				innerSql += ' AND Campus_Course__r.Campus__c = \'' + selectedSchool + '\'';
				
				if(sCourseFilter != 'all' && sCourseFilter != ''){
					innerSql += ' AND Campus_Course__r.Course__c = \'' + sCourseFilter + '\'';
				}
			}
			else {
				innerSql += ' AND Campus_Name__c = \'' + selectedSchool + '\'';
				
				if(sCourseFilter != 'all' && sCourseFilter != ''){
					innerSql += ' AND Course_Name__c = \'' + sCourseFilter + '\'';
				}
			}
		
			if(selectedAgency!=null && selectedAgency!='')
				innerSql += ' AND Quotation__r.Agency__c = :selectedAgency   ';
			else
				innerSql += ' AND Quotation__r.Agency__c in :agencyIds   ';
			
		//innerSql += ' AND (NOT productid__c like \'%00N20000000thc%\') '; //This is the id created manually when a course is cloned in the quotation, so the same course in one quotation will hae a different id and count in the report as one per quotation
		sql += innerSql;
		sql += ' GROUP BY createdByid ';
		
		system.debug('@@@@@@@@@@@@@@@@@@@@@@ sql: ' + sql);
		
		List<AggregateResult> lar = Database.query(sql);
		
		//Quotes enrolled
	/*	string sqlEnroll = 'Select createdByid, Count(Name) tot from Quote__c Q where status__c = \'enrolled\' and createdByid in ' + SetToString(userIDs);
		
		sqlEnroll += innerSql;
		sqlEnroll += ' group by createdByid ';
		system.debug('@@@@@@@@@@@@@@@@@@@@@@ sqlEnroll: ' + sqlEnroll);
		
		List<AggregateResult> larEnroll = Database.query(sqlEnroll);*/
		
		
		//Quotes per campus
		
		string sqlperCampus = 'SELECT Campus_Course__r.Campus__c, Campus_Course__r.Course__c , Campus_Name__c, Campus_Course__r.Course__r.Course_Type__c, Course_Name__c, count(id) tot FROM Quotation_Detail__c WHERE ';
		
		if(selectedQuoteType == 'custom')
			sqlperCampus += ' Campus_Course__c = null ';	
		else 
			sqlperCampus += ' Campus_Course__c != null ';
				
		sqlperCampus += ' AND createdByid in :userIDs ';
			
		sqlperCampus += innerSql;
		
		sqlperCampus += ' GROUP BY Campus_Course__r.Campus__c,Campus_Course__r.Course__c , Campus_Name__c, Campus_Course__r.Course__r.Course_Type__c, Course_Name__c ORDER BY Campus_Name__c ';
		system.debug('@@@@@@@@@@@@@@@@@@@@@@ sqlperCampus: ' + sqlperCampus);
		
		mapCC = new map<string,map<string,list<courseCampus>>>();
		courseCampus cc;
		list<courseCampus> lcc;
		map<string,list<courseCampus>> mp;
		for(AggregateResult pc : Database.query(sqlperCampus)){
			if(!mapCC.containsKey((string)pc.get('Campus_Name__c'))){
				mp = new map<string,list<courseCampus>>();
				lcc = new list<courseCampus>();
				cc = new courseCampus();
				cc.courseName = (string)pc.get('Course_Name__c');
				cc.total = (integer)pc.get('tot');
				
				cc.campusId = (string)pc.get('Campus__c');
				cc.courseId = (string)pc.get('Course__c');
				cc.courseType = (string)pc.get('Course_Type__c');
								
				lcc.add(cc);
				mp.put((string)pc.get('Course_Type__c'),lcc);
				mapCC.put((string)pc.get('Campus_Name__c'),mp);
				
				courseTypeTotal.put((string)pc.get('Campus_Name__c'),(integer)pc.get('tot'));
			}else{
				if(!mapCC.get((string)pc.get('Campus_Name__c')).containsKey((string)pc.get('Course_Type__c'))){
					mp = new map<string,list<courseCampus>>();
					lcc = new list<courseCampus>();
					cc = new courseCampus();
					cc.courseName = (string)pc.get('Course_Name__c');
					cc.total = (integer)pc.get('tot');
					
					cc.campusId = (string)pc.get('Campus__c');
					cc.courseId = (string)pc.get('Course__c');
					cc.courseType = (string)pc.get('Course_Type__c');
					
					lcc.add(cc);
					mapCC.get((string)pc.get('Campus_Name__c')).put((string)pc.get('Course_Type__c'),lcc);
					courseTypeTotal.put((string)pc.get('Campus_Name__c'),courseTypeTotal.get((string)pc.get('Campus_Name__c')) + (integer)pc.get('tot'));
				}else{
					cc = new courseCampus();
					cc.courseName = (string)pc.get('Course_Name__c');
					cc.total = (integer)pc.get('tot');
					
					cc.campusId = (string)pc.get('Campus__c');
					cc.courseId = (string)pc.get('Course__c');
					cc.courseType = (string)pc.get('Course_Type__c');
					
					mapCC.get((string)pc.get('Campus_Name__c')).get((string)pc.get('Course_Type__c')).add(cc);
					courseTypeTotal.put((string)pc.get('Campus_Name__c'),courseTypeTotal.get((string)pc.get('Campus_Name__c')) + (integer)pc.get('tot'));
				}
			}
		}
		
		System.debug('==>mapCC: '+mapCC);
				
		Map<String, countQuotes> quotesPerUser = new Map<String, countQuotes>();
		
		for(AggregateResult ar : lar){
			countQuotes cq = new countQuotes();
			cq.allQuotes = Integer.valueOf(ar.get('tot'));
			cq.enrolled = 0;
			/*for(AggregateResult are : larEnroll){
				if(String.valueOf(ar.get('createdByid')) == String.valueOf(are.get('createdByid'))){
					cq.enrolled = Integer.valueOf(are.get('tot'));
				}
			}*/
			system.debug('cq====' + cq);
			quotesPerUser.put(String.valueOf(ar.get('createdByid')),cq );
		}
		
		for(String id : quotesPerUser.keySet())			
			for(AgencyWrapper aw: agencyList)
				for(UserWrapper uw : aw.userList)
					if(uw.userID == id){						
						uw.quoteCount = quotesPerUser.get(id).allQuotes;
					}
		
		
		//COUNT PER AGENCY
		for(AgencyWrapper aw: agencyList){
			for(UserWrapper uw : aw.userList){					
				aw.quoteCount += uw.quoteCount;
			}
			totalCourses += aw.quoteCount;
		}
	}
	
	private static string FormatSqlDateTimeIni(date d){
		string day = string.valueOf(d.day());
		string month = string.valueOf(d.month());
		if (day.length() == 1)  
			day = '0'+day;
		if (month.length() == 1)    
			month = '0'+month;
		return d.year()+'-'+month+'-'+day+'T'+System.now().formatGmt('00:00:00')+'Z';
	}
	
	private static string FormatSqlDateTimeFin(date d){
		string day = string.valueOf(d.day());
		string month = string.valueOf(d.month());
		if (day.length() == 1)  
			day = '0'+day;
		if (month.length() == 1)    
			month = '0'+month;
		return d.year()+'-'+month+'-'+day+'T'+System.now().formatGmt('23:59:59')+'Z';
	}
	
/*	public string selectQuoteStatus{get{if(selectQuoteStatus == null) selectQuoteStatus = ''; return selectQuoteStatus;} set;}
	public List<SelectOption> getQuoteStatus() {
		List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult F = Quote__c.fields.Status__c.getDescribe();
		List<Schema.PicklistEntry> P = F.getPicklistValues();
		options.add(new SelectOption('','--All Status--'));
		for (Schema.PicklistEntry lst:P){
			options.add(new SelectOption(lst.getLabel(),lst.getValue()));
		}
		return options;
	}*/
	
	//Graph
	public class quotesPerAgency{
		public string agency { get; set; }
		public Integer total { get; set; }
		public quotesPerAgency( string agency , Integer total) {
			this.agency = agency;
			this.total = total;
		}
	}
	public list<quotesPerAgency> getquotesPerAgency(){
		list<quotesPerAgency> lqpc = new list<quotesPerAgency>();
		quotesPerAgency qpc;

		if(SelectedUser.size() > 0 ){
			string sql = 'SELECT count(id) tot, Campus_Name__c cpn, Course_Name__c c FROM Quotation_Detail__c Q WHERE';
			
			if(selectedQuoteType == 'custom')
				sql += ' Campus_Course__c = null ';	
			else sql += ' Campus_Course__c != null ';
			
			sql += ' and CreatedById in :selectedUser ';
			/*if(selectQuoteStatus != '')
				sql += ' and Quote__r.Status__c = \'' + selectQuoteStatus + '\'';*/
		
			if(fromDate.Expected_Travel_Date__c != null)
				sql += ' AND CreatedDate >= ' + FormatSqlDateTimeIni(fromDate.Expected_Travel_Date__c) + ' ';
				
			if(toDate.Expected_Travel_Date__c != null)
				sql += ' AND CreatedDate <= ' + FormatSqlDateTimeFin(toDate.Expected_Travel_Date__c) + ' ';
			
			if(selectedQuoteCountry != 'all' && selectedQuoteCountry != '')
				sql += ' AND Campus_Country__c = \'' + selectedQuoteCountry + '\'';
		
			if(selectedQuoteCity != 'all' && selectedQuoteCity != '')
				sql += ' AND Campus_City__c = \'' + selectedQuoteCity + '\'';
			
			/*if(selectedCourseType != null && selectedCourseType != 'all')
				sql += ' AND Campus_Course__r.Course__r.Course_Type__c = \'' + selectedCourseType + '\'';
			
			if(selectedCourseField != null && selectedCourseField != 'all')
				sql += ' AND Campus_Course__r.Course__r.Type__c = \'' + selectedCourseField + '\'';
			*/
			if(selectedSchool != null && selectedSchool != '')
				if(selectedQuoteType == 'std'){
					sql += ' AND Campus_Course__r.Campus__c = \'' + selectedSchool + '\'';
					
					if(sCourseFilter != 'all' && sCourseFilter != ''){
						sql += ' AND Campus_Course__r.Course__c = \'' + sCourseFilter + '\'';
					}
				}
				else {
					sql += ' AND Campus_Name__c = \'' + selectedSchool + '\'';
					
					if(sCourseFilter != 'all' && sCourseFilter != ''){
						sql += ' AND Course_Name__c = \'' + sCourseFilter + '\'';
					}
				}
			if(selectedAgency!=null && selectedAgency!='')
				sql += ' AND Quotation__r.Agency__c = :selectedAgency   ';
			else
				sql += ' AND Quotation__r.Agency__c in :agencyIds   ';
				
			
			sql += ' GROUP BY Campus_Name__c, Course_Name__c ORDER BY count(id) desc limit 25';

			System.debug('==> sql: '+sql);
			for(AggregateResult qd:Database.query(sql)){
				qpc = new quotesPerAgency('('+(string)qd.get('c') + ') - '+ (string)qd.get('cpn'),(integer)qd.get('tot'));
				lqpc.add(qpc);
			}
		
		}else if(agencyList.size() > 0 && (selectedAgency != null && selectedAgency != '')){
			for(AgencyWrapper aw:agencyList)
				for(UserWrapper uw:aw.userList){
					//if(selectQuoteStatus == '')
					qpc = new quotesPerAgency(uw.userName,uw.quoteCount);
					//	else qpc = new quotesPerAgency(uw.userName,uw.quoteCount, 0);
					lqpc.add(qpc);
				}
		}else if(selectedAgencyGroup != null && selectedAgencyGroup != ''){
			for(AgencyWrapper aw:agencyList){
				//	if(selectQuoteStatus == '')
				qpc = new quotesPerAgency(aw.agencyName,aw.quoteCount);
				//	else qpc = new quotesPerAgency(aw.agencyName,aw.quoteCount, 0);
				lqpc.add(qpc);
			}
		}
		return lqpc;
	}
	
	/* Export quotes per course to Excel */
	public PageReference quotesPerCourseExcel(){
					
		return Page.report_quotesPerCourseExcel;
	}
	public List<Quotation_Detail__c> quotesExcel {get;set;}
	public void quotesCourseExcelSearch(){
		
		string sql = 'SELECT Campus_Country__c, Campus_City__c, School_Name__c, Campus_Name__c, Quotation__r.Agency__r.Name, Quotation__r.Client__r.Email, Quotation__r.Client__r.Name, ' +
					 ' Quotation__r.Client__r.Nationality__c, Campus_Course__r.Course__r.Course_Type__c,  Course_Name__c, Quotation__r.Name, Quotation__r.Status__c, ' +
					 ' Quotation__r.CreatedDate, Quotation__r.Expiry_date__c, Quotation__r.Client__r.Owner__r.Name ' +
					 ' FROM Quotation_Detail__c WHERE ';
		
		if(selectedQuoteType == 'custom')
			sql+= ' Campus_Course__c = null ';	
		else 
			sql+= ' Campus_Course__c != null ';
			
				
		sql+= ' AND createdByid in :userIds ';
		
		
		if(fromDate.Expected_Travel_Date__c != null) 
			sql += ' AND CreatedDate >= ' + FormatSqlDateTimeIni(fromDate.Expected_Travel_Date__c) + ' ';
				
		if(toDate.Expected_Travel_Date__c != null)
			sql += ' AND CreatedDate <= ' + FormatSqlDateTimeFin(toDate.Expected_Travel_Date__c) + ' ';
		
		if(selectedQuoteCountry != 'all' && selectedQuoteCountry != '')
			sql += ' AND Campus_Country__c = \'' + selectedQuoteCountry + '\'';
		
		if(selectedQuoteCity != 'all' && selectedQuoteCity != '')
			sql += ' AND Campus_City__c = \'' + selectedQuoteCity + '\'';
		
		if(selectedSchool != null && selectedSchool != '' && selectedSchool != 'all')
			if(selectedQuoteType == 'std'){
				sql += ' AND Campus_Course__r.Campus__c = \'' + selectedSchool + '\'';
				
				if(sCourseFilter != 'all' && sCourseFilter != ''){
					sql += ' AND Campus_Course__r.Course__c = \'' + sCourseFilter + '\'';
				}
			}
			else {
				sql += ' AND Campus_Name__c = \'' + selectedSchool + '\'';
				
				if(sCourseFilter != 'all' && sCourseFilter != ''){
					sql += ' AND Course_Name__c = \'' + sCourseFilter + '\'';
				}
			}
		if(selectedAgency!=null && selectedAgency!='')
			sql += ' AND Quotation__r.Agency__c = :selectedAgency   ';
		else
			sql += ' AND Quotation__r.Agency__c in :agencyIds   ';
		sql+= ' ORDER BY Campus_Country__c, Campus_City__c, School_Name__c, Campus_Name__c, Quotation__r.Agency__r.Name ';
		
		system.debug('SQL==>' + sql);
		quotesExcel = Database.query(sql);		
	}
}