/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ReportQuotesPerAgencyGroupCtr_test {

    static testMethod void myUnitTest() {
    	
    	TestFactory tf = new TestFactory();
        
        
		Map<String,String> recordTypes = new Map<String,String>();
       		
		Account agency = tf.createAgency();
		
		Contact employee = tf.createEmployee(agency);		
					
		Contact client = tf.createClient(agency);
		
		User portalUser = tf.createPortalUser(employee);
        
		Account school = tf.createSchool();
		
		Account campus = tf.createCampus(school, agency);        
		
		Course__c course = tf.createCourse();
       
		Campus_Course__c cc = tf.createCampusCourse(campus,course);
		
		
		system.runAs(portalUser){
			Quotation__c quote = tf.createQuotation(client, cc);
		}
		
		
		
		
		ReportQuotesPerAgencyGroupController report = new ReportQuotesPerAgencyGroupController(null);
		Contact fromDate = report.fromDate;
		Contact toDate = report.toDate;
		
        String selectedAgencyGroup = report.selectedAgencyGroup;
        List<SelectOption> agencyGroupOptions = report.agencyGroupOptions;
        report.selectedAgencyGroup = agency.parentid;
        
        report.getGroupName();
        
        report.searchAgencies();
        
        
        String selectedAgency = report.selectedAgency;
        List<SelectOption> agencyOptions = report.agencyOptions;
        report.selectedAgency = agency.Id;
        
        report.searchUsers();
        
        list<String> selectedUser = report.selectedUser;
        List<SelectOption> userOptions = report.userOptions;
        report.selectedUser.add(portalUser.id);
        
        
       	
        List<ReportQuotesPerAgencyGroupController.AgencyWrapper> agencyList = report.agencyList;
        
        
        
        report.search();
        
        report.getQuotesperStatus();
        
        Apexpages.currentPage().getParameters().put('userID', portalUser.id);
        report.searchQuotesByUser();
        
        report.getQuoteStatus();
        
        list<ReportQuotesPerAgencyGroupController.quotesPerAgency> quotesPerAgency = report.getquotesPerAgency();
        
    }
}