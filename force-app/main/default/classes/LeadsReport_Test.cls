/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class LeadsReport_Test {

     static testMethod void myUnitTest() {
    	TestFactory factory = new TestFactory();
    	    	
    		
		Account agencyGroup = factory.createAgencyGroup();
		Account agency = factory.createPortalAgency(UserInfo.getUserid());
		Contact employee = factory.createEmployee(agency);
		Contact lead1;
		User userEmp = factory.createPortalUser(employee);
    	
    			
	    Date dueDate = Date.today();
	    TasksCalendarController cal = new TasksCalendarController();
	    
		system.runAs(userEmp){
			
			lead1 = factory.createLead(agency,employee); //Client
			
			Test.startTest();
			
			
			webFormInquiry__c wfi1 = new webFormInquiry__c();
			wfi1.Account__c = lead1.id; 
			wfi1.City__c = lead1.Lead_Residence_City__c;
			wfi1.Country__c = lead1.Lead_Residence_Country__c;
			wfi1.Destination__c = lead1.Lead_Destinations__c;
			wfi1.How_did_you_hear_about_us__c = lead1.LeadSource;
			wfi1.How_long_are_you_going_for__c = lead1.Travel_Duration__c;
			wfi1.Message__c = lead1.Lead_Message__c;
			wfi1.Other_Destination__c = lead1.Lead_Other_Destination__c;
			wfi1.Other_Product_Type__c = lead1.Lead_Other_Product__c;
			wfi1.State__c = lead1.Lead_Residence_State__c;
			wfi1.Study_Area__c = lead1.Lead_Study_Type__c;
			wfi1.What_are_you_looking_for__c = lead1.Lead_Product_Type__c;
			wfi1.When_are_you_planning_to_travel__c = lead1.When_are_you_planning_to_travel__c;
			wfi1.Selected__c = true;
			insert wfi1;
		
	        
	        LeadsReportController ec = new LeadsReportController();
	        String userAgencyName = ec.userAgencyName;		
			List<SelectOption> agencyGroupOptions = ec.agencyGroupOptions;
			ec.selectedAgencyGroup = agency.id;
			ec.searchAgencies();
			
			List<SelectOption> agencyOptions = ec.agencyOptions;
			ec.selectedAgency = agency.id;
			Map<String, String> otherAgencies = ec.otherAgencies;
			Map<String, String> educationalPlanners = ec.educationalPlanners;
			ec.searchUsers();
			List<Contact> listUsers = ec.listUsers;
	     	
			Contact dates = ec.dates;
			String leadName = ec.leadName;
			
			ec.refreshLists();
			
			List<Contact> leadsInProgress = ec.getLip();
			List<Contact> newLeads = ec.getNL();
			
			ApexPages.currentPage().getParameters().put('leadID', lead1.id);
			ec.acceptLead();
			
			ec.refreshLists();
			ApexPages.currentPage().getParameters().put('leadID', lead1.id);
			//ec.markAsRead();
			
			ec.selectedLevelOfInterest = '3 - Cold Lead';
			ec.selectedLeadSource = 'Web';		
			ec.selectedLeadClassification = 'P1 - (0 to 3 months)';
			ec.selectedLeadTravelDuration = 'In 3 to 6 months';
			ec.selectedLeadStudyType = new List<String>{'English'};
			ec.selectedLeadStatus = 'New Lead';
			ec.selectedDatesBasedOn = 'enquiryDate';
			ec.dates.Expected_Travel_date__c = System.today().addDays(-5);
			ec.dates.Visa_Expiry_Date__c = System.today().addDays(5);
			ec.selectedLeadDestination = 'Australia';
			ec.selectedLeadProduct = new List<String>();
			ec.selectedLeadProduct.add('Study');
			ec.leadName = 'Silva';
			
			ec.refreshLists();
			leadsInProgress = ec.getlip();
			newLeads = ec.getnl();
			
			ec.selectedLevelOfInterest = null;
			ec.selectedLeadSource = null;		
			ec.selectedLeadClassification = null;
			ec.selectedLeadTravelDuration = null;
			ec.selectedLeadStudyType = null;
			ec.selectedLeadStatus = null;
			ec.selectedDatesBasedOn = null;
			ec.dates.Expected_Travel_date__c = null;
			ec.dates.Visa_Expiry_Date__c = null;
			ec.selectedLeadDestination = null;
			ec.selectedLeadProduct = null;
			ec.leadName = null;
			
			//ec.refreshLists();
			leadsInProgress = ec.getLip();
			newLeads = ec.getNL();
			

			ec.selectedLeadDestination = 'Australia';
			
			ec.getManagerOptions();		
			List<SelectOption> inProgressActions =  ec.inProgressActions;
			List<SelectOption> receivedActions = ec.receivedActions;
			List<SelectOption> enquiryActions = ec.enquiryActions;
			List<SelectOption> levelOfInterestOptions = ec.levelOfInterestOptions;
			List<SelectOption> leadStatusOptions = ec.leadStatusOptions;
			List<SelectOption> leadSourceOptions= ec.leadSourceOptions;
			List<SelectOption> leadClassificationOptions = ec.leadClassificationOptions;
			List<SelectOption> leadDestinationOptions = ec.leadDestinationOptions;
			List<SelectOption> leadProductOptions = ec.leadProductOptions;
			List<SelectOption> leadStudyTypeOptions = ec.leadStudyTypeOptions;
			List<SelectOption> leadTravelDurationOptions = ec.leadTravelDurationOptions;
			List<SelectOption> leadDatesBasedOnOptions = ec.leadDatesBasedOnOptions;
			
			Boolean isMainAgency = ec.isMainAgency;
			
			
			ec.refreshLeadStatus();
			
			ApexPages.currentPage().getParameters().put('convertID', lead1.id);
			ec.convert();
			ec.markAsRead();
			
			Date testDate = Date.today();
			
			//String fomatedDate = ec.FormatSqlDate(testDate);
			
			
			Integer Queries = ec.getQueries();
			Integer LimitQueries = ec.getLimitQueries();
			Decimal QueryPerc = ec.getQueryPerc();
			Integer QueryRows = ec.getQueryRows();
			Integer LimitQueryRows = ec.getLimitQueryRows();
			Decimal QueryRowsPerc = ec.getQueryRowsPerc();
			Integer DmlStatements = ec.getDmlStatements();
			Integer LimitDmlStatements = ec.getLimitDmlStatements();
			Integer ScriptStatements= ec.getScriptStatements();
			Integer LimitScriptStatements = ec.getLimitScriptStatements();
			Decimal ScriptStatementsPerc = ec.getScriptStatementsPerc();
			Integer CpuTime = ec.getCpuTime();
			Integer LimitCpuTime = ec.getLimitCpuTime();
			Decimal CpuTimePer = ec.getCpuTimePer();
			
			
			ApexPages.currentPage().getParameters().put('type', 'Agency');
			ApexPages.currentPage().getParameters().put('typeID', agency.id);
			ec.getVariableLeads();
			
			ApexPages.currentPage().getParameters().put('type', 'Agency Group');
			ApexPages.currentPage().getParameters().put('typeID', agency.id);
			ec.getVariableLeads();
			
			ec.leadsPerAgencyGroup = null;
			Integer leadsPerAgencyGroup = ec.leadsPerAgencyGroup;
			
			ec.leadsPerUser = null;
			Map<String, Integer> leadsPerUser = ec.leadsPerUser;
			
			ec.leadsPerAgency = null;
			Map<String, Integer> leadsPerAgency = ec.leadsPerAgency;
			
			Integer all = ec.all;
			
			
			ec.getAllLeads();
			
			Test.stopTest();
	        
		}
     }
    
}