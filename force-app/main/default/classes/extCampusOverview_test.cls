/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class extCampusOverview_test {

   static testMethod void verifyCampusOverview() {
    	
    	Map<String,String> recordTypes = new Map<String,String>();
    	
    	for( RecordType r :[select id, name from recordtype where isActive = true] )
    		recordTypes.put(r.Name,r.Id);
    	
    	Account school = new Account();
    	school.recordtypeid = recordTypes.get('School');
    	school.name = 'Test School';
    	insert school;
    	
    	Account campus = new Account();
    	campus.RecordTypeId = recordTypes.get('Campus');
    	campus.Name = 'Test Campus CBD';
    	campus.ParentId = school.id;
    	insert campus;
    	
    	Course__c course = new Course__c();
    	course.Name = 'Certificate III in Business ';    	
    	course.Type__c = 'Business';
    	course.Sub_Type__c = 'Business';
    	course.Course_Qualification__c = 'Certificate III';
    	course.Course_Type__c = 'VET';
    	course.School__c = school.id;
    	insert course;
    	
    	Campus_Course__c cc = new Campus_Course__c();
    	cc.Course__c = course.Id;
    	cc.Campus__c = campus.Id;
    	insert cc;
    	
    	Course_Price__c cp = new Course_Price__c();
    	
    	cp.Nationality__c = 'South America';
    	cp.From__c = 150;
    	cp.Price_per_week__c = 150;
    	cp.Campus_Course__c = cc.Id;
    	insert cp;
    	
    	Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(campus);
    	extCampusOverview eco = new extCampusOverview(controller);
    	String courseName = eco.courseName;
    	List<Campus_Course__c> CampusCourses = eco.CampusCourses;
    	eco.courseName = course.Name;
    	
    	
    	
        
    }
}