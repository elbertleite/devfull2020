global with sharing class menuEnrolment {

	public String accoID{get; Set;}
	public String currTab{get;set;}
	public String agencyID {get;set;}
	public String rtName {get;set;}
	public String rtNamePlural {get;set;}
	private User userDetails;

	public String TYPE_DRAFT { get{ return EnrolmentManager.TYPE_DRAFT; } private set; }
	public String TYPE_LOO_REQUESTED { get{ return EnrolmentManager.TYPE_LOO_REQUESTED; } private set; }
	public String TYPE_LOO_RECEIVED { get{ return EnrolmentManager.TYPE_LOO_RECEIVED; } private set; }
	public String TYPE_LOO_CONFIRM_SIGNED { get{ return EnrolmentManager.TYPE_LOO_CONFIRM_SIGNED; } private set; }
	public String TYPE_AWAITING_PAYMENT { get{ return EnrolmentManager.TYPE_AWAITING_PAYMENT; } private set; }
	public String TYPE_WAITING_CLI_PAYMENT { get{ return EnrolmentManager.TYPE_WAITING_CLI_PAYMENT; } private set; }
	public String TYPE_PAID_BY_CLIENT { get{ return EnrolmentManager.TYPE_PAID_BY_CLIENT; } private set; }
	public String TYPE_COE_REQUESTED { get{ return EnrolmentManager.TYPE_COE_REQUESTED; } private set; }
	public String TYPE_COE_RECEIVED { get{ return EnrolmentManager.TYPE_COE_RECEIVED; } private set; }
	public String TYPE_COE_SENT { get{ return EnrolmentManager.TYPE_COE_SENT; } private set; }
	public String TYPE_VISA { get{ return EnrolmentManager.TYPE_VISA; } private set; }
    public String TYPE_ENROLLED { get{ return EnrolmentManager.TYPE_ENROLLED; } private set; }

	public String TYPE_CANCELLED { get{ return EnrolmentManager.TYPE_CANCELLED; } private set; }
	public String TYPE_URGENT { get{ return EnrolmentManager.TYPE_URGENT; } private set; }
	public String TYPE_MISSING_INFORMATION { get{ return EnrolmentManager.TYPE_MISSING_INFORMATION; } private set; }
	public String TYPE_VISA_EXPIRING { get{ return EnrolmentManager.TYPE_VISA_EXPIRING; } private set; }
	public String TYPE_TRAVEL_DATE { get{ return EnrolmentManager.TYPE_TRAVEL_DATE; } private set; }

	public menuEnrolment(){
		getRecordTypeName();
	}

	public void getRecordTypeName(){
		userDetails = [SELECT Contact.AccountId, General_User_Agencies__c, Contact.Account.RecordType.Name, Contact.Department__c FROM User WHERE id = :userInfo.getUserId() limit 1];
		String sName = [Select Account.RecordType.Name From Contact where ID = :[Select u.ContactId From User u where User.id = :UserInfo.getUserId()].ContactId].Account.RecordType.Name;
		system.debug('User ID==' + UserInfo.getUserId());
		String ContactID = [Select u.ContactId From User u where User.id = :UserInfo.getUserId()].ContactId;
		system.debug('Contact ID==' + ContactID);
		system.debug('Nameee' + sName);
		if(sName!=null){
			if(sName.equals('Agency')){
				rtName = 'Agency';
				rtNamePlural = 'Agencies';
			}

			else if(sName.equals('Campus')){
				rtName = 'School';
				rtNamePlural = 'Schools';
			}
		}
	}

	private Account acco;
	public Account getAcco(){
		if(accoID==null)
			accoID = userDetails.Contact.AccountId;

		acco =  [Select id, CreatedById, CreatedDate, ParentId, RecordTypeId, Logo__c, recordType.name, Campus_Photo_URL__c from Account A where id= :accoID];
		return acco;
	}

	private String fileURL;
	public String getfileURL(){

		string accountPicture;
		try{
			getAcco();
			if(acco.recordType.name == 'agency')
				accountPicture = acco.Logo__c;
			else accountPicture = acco.Campus_Photo_URL__c;
		}catch(Exception e){}

		// URL for pictures
		if(accountPicture != null && accountPicture != ''){
			fileURL = accountPicture;
		} else {
			fileURL = 'NoPhoto';
		}

		return fileURL;

	}




	@RemoteAction
	global static String getMenuTotals(){
		User ud = [SELECT Contact.AccountId, General_User_Agencies__c, Contact.Account.RecordType.Name, Contact.Department__c FROM User WHERE id = :userInfo.getUserId() limit 1];
		TotalJson tj = new TotalJson();
		String sql = 'select count(id) ct from Client_Course__c where ' + EnrolmentManager.ENROLMENT_SQLWHERE + ' and course_package__c = null and Client__r.Owner__r.Contact.Department__c = \'' + ud.Contact.Department__c + '\' ';

		//in progress
		AggregateResult ar = Database.query(sql + ' and Enrolment_Completed_On__c = null ');
		tj.inProgress = Double.valueOf( ar.get('ct') );

		//cancelled
		ar = Database.query(sql + ' and isCancelled__c = true AND isEnrolment_on_hold__c = FALSE');
		tj.cancelled = Double.valueOf( ar.get('ct') );

		//urgent
		ar = Database.query(sql + ' and isUrgentEnrolment__c = true AND isEnrolment_on_hold__c = FALSE');
		tj.urgent = Double.valueOf( ar.get('ct') );

		//missingInformation
		ar = Database.query(sql + ' and isCancelled__c = false and Enrolment_Status__c = \'' + EnrolmentManager.STATUS_MISSING_DOCUMENTS + '\' AND isEnrolment_on_hold__c = FALSE');
		tj.missingInformation = Double.valueOf( ar.get('ct') );

		//waitingPayment
		ar = Database.query(sql + ' and Enrolment_Status__c = \'' + EnrolmentManager.STATUS_WAITING_CLI_PAYMENT + '\' AND isEnrolment_on_hold__c = FALSE' );
		tj.waitingPayment = Double.valueOf( ar.get('ct') );

		//paid
		ar = Database.query(sql + ' and Enrolment_Status__c = \'' + EnrolmentManager.STATUS_PAID_BY_CLIENT + '\' AND isEnrolment_on_hold__c = FALSE' );
		tj.paid = Double.valueOf( ar.get('ct') );

		//Received LOO
		ar = Database.query(sql + ' and Enrolment_Status__c = \'' + EnrolmentManager.STATUS_LOO_RECEIVED + '\' AND isEnrolment_on_hold__c = FALSE' );
		tj.receivedLOO = Double.valueOf( ar.get('ct') );

		//On Hold
		ar = Database.query(sql + ' and isCancelled__c = false AND isEnrolment_on_hold__c = TRUE ');
		tj.onHold = Double.valueOf( ar.get('ct') );

		//Received COE
		ar = Database.query(sql + ' and Enrolment_Status__c = \'' + EnrolmentManager.STATUS_COE_RECEIVED + '\' AND isEnrolment_on_hold__c = FALSE' );
		tj.receivedCOE = Double.valueOf( ar.get('ct') );

		//visa expiring
		ar = Database.query(sql + ' and Client__r.Visa_Expiry_Date__c = NEXT_N_DAYS:7  AND isEnrolment_on_hold__c = FALSE');
		tj.visaExpiring = Double.valueOf( ar.get('ct') );

		//travel date
		ar = Database.query(sql + ' and Client__r.Expected_Travel_Date__c = NEXT_N_DAYS:30  AND isEnrolment_on_hold__c = FALSE');
		tj.travelDate = Double.valueOf( ar.get('ct') );


		//pendingInstalments
		String financeSQL = 'select count(id) ct FROM Client_Course_Instalment__c WHERE isOverpayment_Amendment__c = false and Invoice__c = null and Client_Course__r.enrolment_Date__c != null and Received_Date__c = null ' +
						' and isCancelled__c = false and ((Due_Date__c >= '+IPFunctions.FormatSqlDate(system.today())+' and Due_Date__c <=  '+IPFunctions.FormatSqlDate(system.today().addDays(7))+' ) or Due_Date__c < '+IPFunctions.FormatSqlDate(system.today())+' )' +
						' and Client_Course__r.Client__r.Owner__r.Contact.Department__c = \'' + ud.Contact.Department__c + '\'';
		ar = Database.query(financeSQL);
		tj.pendingInstalments = Double.valueOf( ar.get('ct') );

		financeSQL = 'select count(id) ct FROM Invoice__c WHERE isClientInvoice__c = true and isCancelled__c = false and Received_Date__c = null ' +
						' and ((Due_Date__c >= '+IPFunctions.FormatSqlDate(system.today())+' and Due_Date__c <=  '+IPFunctions.FormatSqlDate(system.today().addDays(7))+' ) or Due_Date__c < '+IPFunctions.FormatSqlDate(system.today())+' )' +
						' and Client__r.Owner__r.Contact.Department__c = \'' + ud.Contact.Department__c + '\'';
		ar = Database.query(financeSQL);
		tj.pendingInstalments += Double.valueOf( ar.get('ct') );


		// refunds
		financeSQL = 'SELECT count(id) ct FROM client_course__c WHERE isRefund__c = true  AND Client__r.Owner__r.Contact.Department__c = \'' + ud.Contact.Department__c + '\' AND Status__c NOT IN (\'Refund Paid to Client\', \'Refund Paid to School\')';
		ar = Database.query(financeSQL);
		tj.refund = Double.valueOf( ar.get('ct') );

		financeSQL = 'SELECT count(id) ct FROM client_course_instalment_payment__c WHERE isClient_Refund__c = true  AND Client__r.Owner__r.Contact.Department__c = \'' + ud.Contact.Department__c + '\' AND (Paid_to_Client_On__c != null ) AND  Sent_Email_Receipt__c = false';
		ar = Database.query(financeSQL);
		tj.refund += Double.valueOf( ar.get('ct') );

		//course transfer
		financeSQL = 'SELECT count(id) ct FROM client_course__c WHERE isCancelled__c = true AND Course_Cancellation_Service__c= \'1\' and Client__r.Owner__r.Contact.Department__c = \'' + ud.Contact.Department__c + '\' AND Status__c != \'Course Credit Confirmed\' ';
		ar = Database.query(financeSQL);
		tj.courseTransfer = Double.valueOf( ar.get('ct') );

		//products
		financeSQL = 'SELECT count(id) ct FROM client_product_service__c where Received_Date__c = null and Invoice__c  = null and Client__r.Owner__r.Contact.Department__c = \'' + ud.Contact.Department__c + '\' ';
		ar = Database.query(financeSQL);
		tj.products = Double.valueOf( ar.get('ct') );

		//Cancel Course Requests
		financeSQL = 'SELECT count(id) ct FROM client_course__c WHERE Enroled_by_Agency__c = \'' + ud.Contact.AccountId + '\' AND isCancelRequest__c = true and Client__r.Owner__r.Contact.Department__c = \'' + ud.Contact.Department__c + '\' AND isCancelled__c = false AND Cancel_Request_Confirmed_to_Client__c = false';
		ar = Database.query(financeSQL);
		tj.cancelRequests = Double.valueOf( ar.get('ct') );

		//Cancel Course Requests
		financeSQL = 'SELECT count(id) ct FROM Payment_Plan__c where  (client__r.current_Agency__c = \'' + ud.Contact.AccountId + '\') and ((Instalments__c != null and Received_On__c = null) or (Received_On__c != null and Payment_To_Amend__c = true))';
		ar = Database.query(financeSQL);
		tj.paymentPlan = Double.valueOf( ar.get('ct') );

	

		return JSON.serialize(tj);
	}

	public class TotalJson{
		public double inProgress;
		public double cancelled;
		public double urgent;
		public double missingInformation;
		public double notes;
		public double visaExpiring;
		public double travelDate;
		public double receivedLOO;
		public double receivedCOE;
		public double waitingPayment;
		public double paid;
		public double pendingInstalments;
		public double refund;
		public double courseTransfer;
		public double products;
		public double cancelRequests;
		public double paymentPlan;
		public double onHold;
	}
}