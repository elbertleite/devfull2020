@isTest
private class AgencyMaterialsController_Test{
	
	static testMethod void myUnitTest() {
		Date dueDate = Date.today();
		TestFactory tf = new TestFactory();
		
		Account agency = tf.createAgency();

		Contact emp = tf.createEmployee(agency);

		User portalUser = tf.createPortalUser(emp);

		Test.startTest();
     	system.runAs(portalUser){					
			AgencyMaterialsController controller = new AgencyMaterialsController();

			video_upload__c v = new video_upload__c();
			v.Name = 'aaa';
			v.Description__c = 'aaa';
			v.materialLanguage__c = 'aaa';
			v.permissionLevelRequired__c = 1;
			v.objectType__c = 'Material';
			v.materialLinks__c = 'youtubeVideo;;test(password: pass);;www.goolg.eom.com!#!vimeoVideo;;test2(password: password);;www.goolg.eom.com!#!website;;uihuihu;;www.goolg.eom.com!#!';
			v.parentFolderID__c = portalUser.Contact.Accountid;
			
			insert v;

			video_upload__c v2 = new video_upload__c();
			v2.Name = 'aaa';
			v2.Description__c = 'aaa';
			v2.materialLanguage__c = 'aaa';
			v2.permissionLevelRequired__c = 1;
			v2.objectType__c = 'Material';
			v2.materialLinks__c = 'youtubeVideo;;test(password: pass);;www.goolg.eom.com!#!vimeoVideo;;test2(password: password);;www.goolg.eom.com!#!website;;uihuihu;;www.goolg.eom.com!#!';
			v2.parentFolderID__c = portalUser.Contact.Accountid;
			
			insert v2;

			video_upload__c v3 = new video_upload__c();
			v3.Name = 'aaa';
			v3.Description__c = 'aaa';
			v3.materialLanguage__c = 'aaa';
			v3.permissionLevelRequired__c = 1;
			v3.objectType__c = 'Material';
			v3.materialLinks__c = 'youtubeVideo;;test(password: pass);;www.goolg.eom.com!#!vimeoVideo;;test2(password: password);;www.goolg.eom.com!#!website;;uihuihu;;www.goolg.eom.com!#!';
			v3.parentFolderID__c = portalUser.Contact.Accountid;
			
			insert v3;

			controller.init(v.id);
			Apexpages.currentPage().getParameters().put('idMaterial', v.id);

			AgencyMaterialsController.createNewComment(v.id, 'aaaaa');
			AgencyMaterialsController.searchMaterialsForTraining('aaa');
			AgencyMaterialsController.searchMaterialsForTrainingByAgency(portalUser.Contact.Accountid);

			controller.openMaterial();

			AgencyMaterialsController.saveTrainingPackage(portalUser.Contact.Accountid, 'aaa', 'String packageDescription', 'String packageName', '10', '10', v.id + ';;', null,'1', null, null, 'Portuguese (Brazil);;Albanian;;');

			controller.init(portalUser.Contact.Accountid);

			controller.closeMaterial();
			controller.searchMaterials();
			controller.changeViewType();
			controller.cancelSavingMaterial();
			controller.cancelFolderToSaveMaterial();
			controller.openChangeFolderToSaveMaterial();
			controller.getPermissionToCreate();
			controller.openNewFolderToSaveMaterial();
			
			controller.openNewFolder();
			controller.newFolder.name = 'aaaa';
			controller.newFolder.permission = 1;
			controller.saveNewFolder();

			controller.init(ApexPages.currentPage().getParameters().get('idFolder'));

			controller.openNewFolder();
			controller.newFolder.name = 'aaaa';
			controller.newFolder.permission = 1;

			controller.openEditFolder();
			controller.editFolder();

			controller.openNewFolder();

			controller.getAllFolders();
			

			controller.openNewMaterial();
			controller.changeFolderToSaveMaterial();
			controller.openMaterialForEditionFromBoxPage();

			Apexpages.currentPage().getParameters().put('idFolder', v3.id);
			
			controller.deleteFolder();

			controller.newMaterial = v;

			controller.saveMaterial();
		

			AgencyMaterialsController.saveTrainingPackage(portalUser.Contact.Accountid, 'aaa2', 'String packageDescription', 'String packageName', '10', '10', v.id + ';;', null,'1', 'aaaa', '1', 'Portuguese (Brazil);;Albanian;;');

			video_upload__c newPackage = new video_upload__c();		
			newPackage.parentFolderID__c = portalUser.Contact.Accountid;
			newPackage.parentFolderName__c = 'aaaa';

			newPackage.objectType__c = 'Training';
			newPackage.Name = 'aaaa';
			newPackage.permissionLevelRequired__c = 1;
			newPackage.Description__c = 'aaaa';
			newPackage.materialAgencyGroup__c = portalUser.Contact.Accountid;
			newPackage.trainingMaterialsRelated__c = v.id + ';;' + v2.id + ';;' + v3.id + ';;';
			newPackage.trainingLanguages__c = 'Portuguese (Brazil);;Albanian;;';
			newPackage.trainingEstimatedHours__c = Decimal.valueOf(10);
			newPackage.trainingEstimatedMinutes__c = Decimal.valueOf(10);

			insert newPackage;

			TrainingMaterialsHelper.TrainingMaterial mt = (TrainingMaterialsHelper.TrainingMaterial) AgencyMaterialsController.loadFullMaterial(newPackage.id);

			mt.init();
			mt.openNext();
			mt.openPrevious();

			controller.fromVideoObjToMaterial(newPackage);
			controller.fromVideoObjToMaterial(v);

			AgencyMaterialsController.openPackageForEdition(newPackage.id);

			controller.getLinkTypes();
			controller.getVisibilityLevels();
			controller.getAllAgencyGroups();
			controller.getLanguages();
			controller.searchMaterials('aaa', 'aaa');

            Apexpages.currentPage().getParameters().put('idMaterial', newPackage.id);

			controller.openMaterial();

			Apexpages.currentPage().getParameters().put('idMaterial', v3.id);

			controller.openModuleTraining();

			Apexpages.currentPage().getParameters().put('idMaterial', v.id);

			controller.deleteMaterial();

			Apexpages.currentPage().getParameters().put('sortType', 'updatedBy');
			controller.changeSortBy();
			Apexpages.currentPage().getParameters().put('sortType', 'materialName');
			controller.changeSortBy();
			Apexpages.currentPage().getParameters().put('sortType', 'language');
			controller.changeSortBy();

			controller.generateCompleteListFiles();

		}

	}

}