public without sharing class schools_payment_rules {
    private string schoolId;
    public schools_payment_rules() {
       schoolId = ApexPages.CurrentPage().getParameters().get('id');
       retrieveCourses();
       
    }

    private map<string, Course__c> originalMapCourses;
    public map<string, Course__c> mapCourses{get; set;}
    public void retrieveCourses(){
        retriveFirstValues();
        retrivePaymentRules();
    }

    private void retriveFirstValues(){
        mapCourses = new map<string, Course__c>([Select id, name, Payment_Rules__c, Payment_Rules_Updated_By__c, Payment_Rules_Updated_By__r.SmallPhotoURL, Payment_Rules_Updated_By__r.name, Account_Document_File__r.File_Name__c, Account_Document_File__r.Description__c,
                                            Payment_Rules_Updated_On__c, Important_information__c, Important_information_Updated_By__c, Important_information_Updated_By__r.SmallPhotoURL, Instalment_Term_Option__c, Instalment_Term_Interval__c,
                                            Important_information_Updated_By__r.name, Important_information_Updated_On__c, Account_Document_File__r.Preview_Link__c, Instalment_Option__c, Instalment_Interval__c, Account_Document_File__c , Course_Unit_Type__c, Course_Unit_Length__c
                                            from course__c where School__c = :schoolId]);
        originalMapCourses = mapCourses.deepclone();

    }

    public map<string,map<string,map<string,list<Course_Price__c>>>> listCourses{get; set;}

    public void retrivePaymentRules(){
        list<string> courseType = new list<string>();
        list<string> courses = new list<string>();

        if(ApexPages.currentPage().getParameters().get('type') != null && ApexPages.currentPage().getParameters().get('type').trim() != '')
            courseType = new list<string>(ApexPages.currentPage().getParameters().get('type').split(','));
        if(ApexPages.currentPage().getParameters().get('course') != null && ApexPages.currentPage().getParameters().get('course').trim() != '')
            courses = new list<string>(ApexPages.currentPage().getParameters().get('course').split(','));
        string sql = 'SELECT Availability__c,from__c, Nationality__c, Campus_course__r.course__c, Campus_Course__r.campus__r.name, Campus_course__r.course__r.name, Price_per_week__c,Price_valid_from__c,Price_valid_until__c ';
                sql += ' FROM Course_Price__c where Campus_Course__r.campus__r.parentId = :schoolId ';
                if(courseType != null && courseType.size() > 0)
                    sql += ' and campus_course__r.course__r.Type__c in :courseType ';
                if(courses != null && courses.size() > 0)
                    sql += ' and campus_course__r.course__c in : courses ';
                sql += ' order by Campus_course__r.campus__r.name,Campus_Course__r.course__r.name,Nationality__c, Availability__c, from__c, Price_valid_from__c';
        System.debug('==> SQl: '+sql);

        listCourses = new map<string,map<string,map<string,list<Course_Price__c>>>>();
        for(Course_Price__c cp:database.query(sql)){
            if(mapCourses.containsKey(cp.Campus_course__r.course__c)){
                if(!listCourses.containsKey(cp.Campus_course__r.course__c)){
                    listCourses.put(cp.Campus_course__r.course__c, new map<string,map<string,list<Course_Price__c>>>{cp.Campus_Course__r.campus__r.name => new map<string,list<Course_Price__c>>{cp.Nationality__c => new list<Course_Price__c>{cp}}});
                }else if(!listCourses.get(cp.Campus_course__r.course__c).containsKey(cp.Campus_Course__r.campus__r.name)){
                    listCourses.get(cp.Campus_course__r.course__c).put(cp.Campus_Course__r.campus__r.name, new map<string,list<Course_Price__c>>{cp.Nationality__c => new list<Course_Price__c>{cp}});
                }else if(!listCourses.get(cp.Campus_course__r.course__c).get(cp.Campus_Course__r.campus__r.name).containsKey(cp.Nationality__c)){
                    listCourses.get(cp.Campus_course__r.course__c).get(cp.Campus_Course__r.campus__r.name).put(cp.Nationality__c, new list<Course_Price__c>{cp});
                } else listCourses.get(cp.Campus_course__r.course__c).get(cp.Campus_Course__r.campus__r.name).get(cp.Nationality__c).add(cp);

            }
        }
    }

    public void cancelUpdates(){
        retrieveCourses();
    }

    public Pagereference  saveUpdates(){
        list<course__c> listUpdate = new list<course__c>();
        for(string mc:mapCourses.keySet()){
            if(originalMapCourses.get(mc).Payment_Rules__c == null)
                originalMapCourses.get(mc).Payment_Rules__c = '';
            if(mapCourses.get(mc).Payment_Rules__c == null)
                mapCourses.get(mc).Payment_Rules__c = '';
            if(originalMapCourses.get(mc).Important_information__c == null)
                originalMapCourses.get(mc).Important_information__c = '';
            if(mapCourses.get(mc).Important_information__c == null)
                mapCourses.get(mc).Important_information__c = '';
            System.System.debug('==>originalMapCourses.get(mc).Payment_Rules__c: '+originalMapCourses.get(mc).Payment_Rules__c.trim());
            System.System.debug('==>mapCourses.get(mc).Payment_Rules__c.trim(): '+mapCourses.get(mc).Payment_Rules__c.trim().trim());
            
            if(originalMapCourses.get(mc).Payment_Rules__c.trim() != mapCourses.get(mc).Payment_Rules__c.trim() || originalMapCourses.get(mc).Important_information__c.trim() != mapCourses.get(mc).Important_information__c.trim() ||
                originalMapCourses.get(mc).Instalment_Option__c != mapCourses.get(mc).Instalment_Option__c || originalMapCourses.get(mc).Instalment_Interval__c != mapCourses.get(mc).Instalment_Interval__c || originalMapCourses.get(mc).Account_Document_File__c != mapCourses.get(mc).Account_Document_File__c){
                System.System.debug('==>originalMapCourses.get(mc).Payment_Rules__c: '+originalMapCourses.get(mc).Payment_Rules__c.trim() != mapCourses.get(mc).Payment_Rules__c.trim());
                if(originalMapCourses.get(mc).Payment_Rules__c != mapCourses.get(mc).Payment_Rules__c){
                    mapCourses.get(mc).Payment_Rules_Updated_On__c = System.today();
                    mapCourses.get(mc).Payment_Rules_Updated_By__c = UserInfo.getUserId();
                }
                if(originalMapCourses.get(mc).Important_information__c != mapCourses.get(mc).Important_information__c){
                    mapCourses.get(mc).Important_information_Updated_On__c = System.today();
                    mapCourses.get(mc).Important_information_Updated_By__c = UserInfo.getUserId();
                }
                listUpdate.add(mapCourses.get(mc));
            }
        }
        if(listUpdate.size() > 0)
            update listUpdate;
        
        retriveFirstValues();
        retrivePaymentRules();

        return null;
        //update mapCourses.values();
    }


    @RemoteAction
    public static list<AggregateResult> changeCourseType(string schoolId) { 
        return [SELECT  campus_course__r.course__r.Type__c cType FROM Course_Price__c where campus_course__r.campus__r.parentId = :schoolId group by campus_course__r.course__r.Type__c order by campus_course__r.course__r.Type__c];
    }

    @RemoteAction
    public static list<AggregateResult> changeCourse(string schoolId, list<string> courseType) { 
        string sql = 'SELECT campus_course__r.course__c courseId, campus_course__r.course__r.Name courseName FROM Course_Price__c where campus_course__r.campus__r.parentId = :schoolId ';
        if(courseType != null && courseType.size() > 0)
            sql += ' and campus_course__r.course__r.Type__c in :courseType ';
        sql += ' group by campus_course__r.course__c,campus_course__r.course__r.Name order by campus_course__r.course__r.Name';
        return database.query(sql);
    }

    public string payment_rules_string{get{if(payment_rules_string == null) payment_rules_string = ''; return payment_rules_string;} set;}
    public string important_information_string{get{if(important_information_string == null) important_information_string = ''; return important_information_string;} set;}

    //==================== File Details
    
    public class sDriveFiles{
        public String URL {get;set;}
        public Account_Document_File__c File {get;set;}
    } 
    
    private Map<String, sDriveFiles> mapFiles = new Map<String, sDriveFiles>();
    public List<SelectOption> Files {
        get{
            if(Files== null){
                Files = new List<SelectOption>();
                List<ID> parentIDs = new List<ID>();
                List<ID> fileObjects = new List<ID>();
                files.add(new SelectOption('','-- Select a File --'));  
                
                Map<String, List<Account_Document_File__c>> campusFiles = new Map<String, List<Account_Document_File__c>>();
                Map<String, String> folderNames = new Map<String, String>();
                folderNames.put('','Not Classified');
                campusFiles.put( '' , new List<Account_Document_File__c>() );
                for(Account_Document_File__c folder : [SELECT id, File_Name__c FROM Account_Document_File__c WHERE (Parent__r.ParentId = :schoolId or Parent__c = :schoolId) and Content_Type__c = 'Folder' order by Content_Type__c]){
                    campusFiles.put( folder.File_Name__c, new List<Account_Document_File__c>() );
                    folderNames.put( Folder.id, Folder.File_Name__c );
                }
                
                
                for(Account_Document_File__c folder : [SELECT id, Parent_Folder_Id__c, Description__c, CreatedDate, File_Name__c,File_Size__c FROM Account_Document_File__c WHERE (Parent__r.ParentId = :schoolId or Parent__c = :schoolId) and Content_Type__c != 'Folder' and WIP__c = false order by Content_Type__c]){
                    List<Account_Document_File__c> ladf = campusFiles.get(Folder.Parent_Folder_Id__c == null ? '' : folderNames.get(Folder.Parent_Folder_Id__c));
                    System.debug('==>Folder.Parent_Folder_Id__c: '+folderNames.get(Folder.Parent_Folder_Id__c));
                    ladf.add(folder);
                    campusFiles.put(Folder.Parent_Folder_Id__c == null ? '' : folderNames.get(Folder.Parent_Folder_Id__c), ladf);
                    
                }
                
                for(String folderid : campusFiles.keyset()){                    
                    if(!campusFiles.get(folderid).isEmpty()){
                        
                        files.add(new SelectOption('Folder',folderid));
                        
                        for(Account_Document_File__c file : campusFiles.get(folderid)){                         
                            parentIDs.add(schoolId);
                            fileObjects.add(file.id);
                            sDriveFiles sdf = new sDriveFiles();
                            sdf.File = file;
                            mapFiles.put(file.id,sdf);
                            files.add(new SelectOption(file.id, file.Description__c + ' [*' + file.File_Name__c + ' *]')  );
                            
                        }
                        
                        files.add(new SelectOption('disabled', ''));    
                    }
                }
                
                try {
                    if(files.size() > 1)
                        files.remove(files.size()-1);
                } catch (Exception e){}
                
                
                if(parentIDs.size() > 0 && fileObjects.size() > 0){
                    try {   
                        List<String> urls = cg.SDriveTools.getAttachmentURLs( parentIDs , fileObjects, (100 * 60));
                            
                        for(string sd : mapFiles.keySet())
                            for(String url : urls )
                                if(url.contains(mapFiles.get(sd).file.id)){
                                    mapFiles.get(sd).url = url;
                                    break;
                                }
                    } catch (Exception e){}
                }
            }
            return Files;       
        }
        set;
    }

}