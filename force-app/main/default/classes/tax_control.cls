public class tax_control {
 
 	public tax_control(){
 		retrieveCountries();
 	}
 
 	public map<string, list<account>> mapCountry{get; set;}
 	
 	public void retrieveCountries(){
 		mapCountry = new map<string, list<account>>();
 		for(account ac:[Select Id, Parent.name, Name, Tax_Name__c, Tax_Rate__c from Account WHERE RecordType.name = 'city' order by Parent.name, name])
 			if(!mapCountry.containsKey(ac.Parent.name))
 				mapCountry.put(ac.Parent.name, new list<account>{ac});
 			else mapCountry.get(ac.Parent.name).add(ac);
 	}   
 	
 	public string selectedTax{get; set;}
 	
 	private List<SelectOption> listTax;
 	public list<SelectOption> getlistTax {
    get{
      if(listTax == null){
        listTax = new List<SelectOption>();        
        Schema.DescribeFieldResult F = Account.fields.Tax_Name__c.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        listTax.add(new SelectOption('','--Select--')); 
        for (Schema.PicklistEntry lst:P)
          listTax.add(new SelectOption(lst.getValue(),lst.getLabel())); 
      }
        
      return listTax;
    }
    set;
  }
  
  public pageReference saveTax(){
  	string pr = ApexPAges.currentPage().getParameters().get('paramCountry');
  	boolean hasError = false;
  	if(mapCountry.containsKey(pr)){
  		for(account acc:mapCountry.get(pr)){
  			if(acc.Tax_Name__c != null && acc.Tax_Name__c != '' && acc.Tax_Rate__c == null){
  				acc.Tax_Rate__c.addError('Value Required!');
  				hasError = true;
  			}
  			if(acc.Tax_Rate__c != null && (acc.Tax_Name__c == null || acc.Tax_Name__c == '')){
  				acc.Tax_Name__c.addError('Value Required!');
  				hasError = true;
  			}
  		}
  		if(hasError)
  			return null;
  			
  		update mapCountry.get(pr);
  	}
  	return null;
  }
 	
}