public with sharing class sharecommissionrequest {

	public boolean showError {get;set;}
	public client_course__c course{get; set;}
	private string clientCourseId;
	public sharecommissionrequest(ApexPages.StandardController ctr){
		showError = true;
		clientCourseId = ctr.getId();
		currentUser = [Select Id, Name, Contact.Account.ParentId, Contact.Account.Name, Contact.AccountId, Contact.Department__c from User where id = :UserInfo.getUserId() limit 1];
		retrieveCourse();

	}

	public string reqDetails{get{if(reqDetails == null) reqDetails = ''; return reqDetails;} set;}
	public string decDetails{get{if(decDetails == null) decDetails = ''; return decDetails;} set;}

	//public list<string> reqDetailsList{get{if(reqDetailsList == null) reqDetailsList = new list<string>(); return reqDetailsList;} set;}
	public list<string> decDetailsList{get{if(decDetailsList == null) decDetailsList = new list<string>(); return decDetailsList;} set;}

	private map<string, client_course_instalment__c> beforeUpdate;
	private string currentDecisionStatus = '';
	public string currentRequestAgency{get;set;}
	public boolean hasSharedCommConfirmed{get{if(hasSharedCommConfirmed == null) hasSharedCommConfirmed = false; return hasSharedCommConfirmed;} set;}
	public map<String, Boolean> showCheckBox {get;set;}
	public void retrieveCourse(){
		course  = [Select client__r.name, Course_Name__c,
						share_commission_decision_by__c ,share_commission_decision_on__c, share_commission_decision_status__c, share_commission_request_agency__c,
					 share_commission_request_by__c, share_commission_request_on__c, share_commission_request_details__c, share_commission_decision_details__c, share_commission_request_reason__c,
					 (Select id,  isRequestedShareCommission__c, isShare_Commission__c, isMigrated__c, isCancelled__c, number__c, PFS_Pending__c, Split_Number__c, Instalment_Value__c, Tuition_Value__c, Shared_Commission_Bank_Dep_Enrolled__c, isShareCommissionSplited__c, Shared_Commission_Bank_Dep_Received__c, Shared_Commission_Bank_Dep_Request__c, Shared_Commission_Bank_Dep_Pay_Agency__c, Shared_Comm_Agency_Request_Value__c, Shared_Comm_Agency_Request__c, Share_Commission_Paid_On__c from client_course_instalments__r order by number__c, Split_Number__c)
					  from client_course__c where id = :clientCourseId limit 1];
		currentRequestAgency = course.share_commission_request_agency__c;
		if(course.share_commission_request_agency__c == null)
			course.share_commission_request_agency__c = currentUser.Contact.AccountId;

		currentDecisionStatus = course.share_commission_decision_status__c;
		if(course.share_commission_request_agency__c != null)
			isDecisionUser = true;
		if(course.share_commission_decision_status__c != null && course.share_commission_decision_status__c != 'Requested')
			selectedShareDecisionStatus = course.share_commission_decision_status__c;

		/*if(course.share_commission_request_details__c != null && course.share_commission_request_details__c != '')
			reqDetailsList.addAll(course.share_commission_request_details__c.split('#-#'));*/

		if(course.share_commission_decision_details__c != null && course.share_commission_decision_details__c != '')
			decDetailsList.addAll(course.share_commission_decision_details__c.split('#-#'));


		showCheckBox = new map<String,Boolean>();

		for(client_course_instalment__c ci: course.client_course_instalments__r){
			if(ci.Share_Commission_Paid_On__c != null){
				hasSharedCommConfirmed = true;
			}

			//Check if User can Request Share Comm. for each instalment
			if((!ci.isMigrated__c || ci.PFS_Pending__c) && (ci.Shared_Commission_Bank_Dep_Enrolled__c == NULL && ci.Shared_Commission_Bank_Dep_Received__c == NULL && ci.Shared_Commission_Bank_Dep_Request__c == NULL && ci.Shared_Commission_Bank_Dep_Pay_Agency__c == NULL && ci.Share_Commission_Paid_On__c == NULL) && !ci.isCancelled__c)
				showCheckBox.put(ci.id, true);
			else	
				showCheckBox.put(ci.id, false);

		}//end for

		beforeUpdate = new map<string, client_course_instalment__c>(course.client_course_instalments__r.deepClone(true));
	}

	boolean isDecisionUser;
	public void saveRequest(){
		showError = false;
		//boolean isDecisionUser = ApexPages.currentPage().getParameters().get('isde') == '0';
		if(course.share_commission_request_reason__c == null || course.share_commission_request_reason__c == '')
			showError = true;
		if(decDetails == null || decDetails.trim() == '')
			showError = true;

		DateTime dT = System.now().dategmt();
		string myDate = dT.day()+'/'+dT.month()+'/'+dT.year();

		if(isDecisionUser){
			course.share_commission_decision_on__c = System.now();
			course.share_commission_decision_by__c = UserInfo.getUserId();
		}else{
			course.share_commission_request_by__c = UserInfo.getUserId();
			course.share_commission_request_on__c = System.now();
		}
		if(course.share_commission_decision_status__c == null || !isDecisionUser || currentRequestAgency == null)
			course.share_commission_decision_status__c = 'Requested';
		/*if(reqDetails.trim() != ''){
			if(course.share_commission_request_details__c == null || course.share_commission_request_details__c == '')
				course.share_commission_request_details__c = '<span class=\'hdInfo\'>'+myDate +' - ' +UserInfo.getName() +' - ' + currentUser.Contact.Account.Name +'</span> ' + reqDetails;
			else course.share_commission_request_details__c += '#-# <span class=\'hdInfo\'>'+ myDate +' - ' +UserInfo.getName() +' - ' + currentUser.Contact.Account.Name +'</span> ' + reqDetails;
			reqDetails = '';
		}*/
		if(course.share_commission_decision_details__c == null || course.share_commission_decision_details__c == ''){
			course.share_commission_decision_details__c = 'Commission Requested By: ' +mapAgencies.get(course.share_commission_request_agency__c)+ '<br>Reason: '+ course.share_commission_request_reason__c + ' <span class=\'hdInfo\'>'+myDate +' - ' +UserInfo.getName() +' - ' + currentUser.Contact.Account.Name +'</span> ';
		}

		if(currentDecisionStatus != null && currentDecisionStatus != '' && currentDecisionStatus != course.share_commission_decision_status__c){
			if(course.share_commission_decision_status__c == 'Requested')
				course.share_commission_decision_details__c = 'Commission Requested By: ' +mapAgencies.get(course.share_commission_request_agency__c)+ '<br>Reason: '+ course.share_commission_request_reason__c +' <span class=\'hdInfo\'>'+ myDate +' - ' +UserInfo.getName() +' - ' + currentUser.Contact.Account.Name +'</span> #-#' + course.share_commission_decision_details__c;
			else course.share_commission_decision_details__c = 'Commission: '+course.share_commission_decision_status__c+ ' <span class=\'hdInfo\'>'+ myDate +' - ' +UserInfo.getName() +' - ' + currentUser.Contact.Account.Name +'</span> #-#' + course.share_commission_decision_details__c;
		}
		if(decDetails.trim() != ''){
			if(course.share_commission_decision_details__c == null || course.share_commission_decision_details__c == '')
				course.share_commission_decision_details__c = decDetails.replaceAll('\n','<br>') + '<span class=\'hdInfo\'>'+myDate +' - ' +UserInfo.getName() +' - ' + currentUser.Contact.Account.Name +'</span> ';
			else course.share_commission_decision_details__c = decDetails.replaceAll('\n','<br>')+' <span class=\'hdInfo\'>'+ myDate +' - ' +UserInfo.getName() +' - ' + currentUser.Contact.Account.Name +'</span> #-#' + course.share_commission_decision_details__c;
			decDetails = '';
		}

		if(course.share_commission_decision_status__c == 'Declined'){
			course.share_commission_request_agency__c = null;
			course.share_commission_request_by__c = null;
			course.share_commission_request_on__c = null;
			course.share_commission_request_reason__c = null;
		}

		for(client_course_instalment__c cci:course.client_course_instalments__r){
			//if(course.share_commission_decision_status__c == 'Requested' && cci.Shared_Commission_Bank_Dep_Enrolled__c == null && cci.Shared_Commission_Bank_Dep_Received__c == null && cci.Shared_Commission_Bank_Dep_Request__c == null)
			//	cci.isRequestedShareCommission__c = true;
			//else
			if(course.share_commission_decision_status__c == 'Declined' && cci.Shared_Commission_Bank_Dep_Enrolled__c == null && cci.Shared_Commission_Bank_Dep_Received__c == null && cci.Shared_Commission_Bank_Dep_Request__c == null){
				cci.isRequestedShareCommission__c = false;
				cci.Shared_Comm_Agency_Request_Value__c = 0;
				cci.Shared_Comm_Agency_Request__c = 0;
				cci.isShareCommissionSplited__c = false;

			}
			System.debug('==>cci.isRequestedShareCommission__c: '+cci.isRequestedShareCommission__c);
			System.debug('==>beforeUpdate.get(cci.id).isRequestedShareCommission__c: '+beforeUpdate.get(cci.id).isRequestedShareCommission__c);
			if(!cci.isRequestedShareCommission__c && beforeUpdate.get(cci.id).isRequestedShareCommission__c){
				cci.isRequestedShareCommission__c = false;
				cci.Shared_Comm_Agency_Request_Value__c = 0;
				cci.Shared_Comm_Agency_Request__c = 0;
				cci.isShareCommissionSplited__c = false;
				cci.Shared_Comm_Agency_Enroll_Value__c = 0;
				cci.Shared_Comm_Agency_Enroll__c = 0;
				cci.Shared_Comm_Agency_Receive_Value__c = 0;
				cci.Shared_Comm_Agency_Receive__c = 0;
				cci.Shared_Comm_Pay_Agency_Value__c = 0;
				cci.Shared_Comm_Pay_Agency__c = 0;
			}
		}

		update course;
		update course.client_course_instalments__r;
	}


	private User currentUser {get;set;}

	private map<string, string> mapAgencies;
	public List<SelectOption> agencyOptions {
		get{
	   		if(agencyOptions == null){
	   			mapAgencies = new map<string, string>();
	   			agencyOptions = new List<SelectOption>();
				for(Account a: [Select Id, Name from Account where RecordType.Name = 'agency' order by name]){
					agencyOptions.add(new SelectOption(a.Id, a.Name));
					mapAgencies.put(a.Id, a.Name);
				}
			}
		   	return agencyOptions;
	  	}
	  	set;
	}

	public string selectedShareDecisionStatus{get; set;}
	public List<SelectOption> shareDecisionStatus {
		get{
	   		if(shareDecisionStatus == null){
	   			shareDecisionStatus = new List<SelectOption>();
	   			shareDecisionStatus.add(new SelectOption('Accepted', 'Accepted'));
				shareDecisionStatus.add(new SelectOption('Declined', 'Declined'));
			}
		   	return shareDecisionStatus;
	  	}
	  	set;
	}



}