/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class WS_restCallouts_test {

    static testMethod void myUnitTest() {
        
        WS_restCallouts.getIPAgencyContacts();
        
        WS_restCallouts.getCoursesPerNationality('0012000000eA8w6', 'ag','01/01/2014', '2/12/2014');
        
        WS_restCallouts.getIPAgencies('Australia', 'Sydney', 'office');
        
        List<WS_restCallouts.agencyContacts> lac = new List<WS_restCallouts.agencyContacts>();
        WS_restCallouts.agencyContacts ac = new WS_restCallouts.agencyContacts('Bruce Wayne', 'batman@wayne.com', 'Unenployed');
        lac.add(ac);
        
        WS_restCallouts.agencyDetails ad = new WS_restCallouts.agencyDetails('Gothan Agency', lac);
        
    }
}