/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class S3Controller_test {
	
	static testMethod void myUnitTest() {
		test1();
		test2();
		test3();
		test4();
		test5();
		test6();
		test7();
		test8();
		test9();
		test10();
		test11();
		test12();
		test13();
		test14();
		test15();
	}

    public static testmethod void test1() {
	 	    
 	    S3Controller c = new S3Controller();
 		String credName = c.createTestCredentials();
 		c.AWSCredentialName = credName;
 		c.constructor();
 		c.getPolicy(); 
 		system.assert( c.allBuckets == null );
 		
 		c.AWSCredentialName = 'does not exist';
 		c.constructor();
	 		
	 }
	
	 public static testmethod void test2() {
	 		S3Controller c = new S3Controller();
			String credName = c.createTestCredentials();
	 		c.AWSCredentialName = credName;
			try{
				system.debug ( c.getFolders() );
				//system.assert( c.docs != null );
				system.debug ( c.getAccessTypes() );
			}catch(Exception ex){
			}
			//c.allbuckets = new string[]{'f','b'};
			//system.debug( c.allBuckets );
			//c.getBucketNames();
			
			
	 }
	 public static testmethod void test3() {
	 		S3Controller c = new S3Controller();
	 		String credName = c.createTestCredentials();
	 		c.AWSCredentialName = credName;
	 		c.constructor();
	 		c.createBucket('test');
	 }
	 public static testmethod void test4() {
	 		S3Controller c = new S3Controller();
	 		String credName = c.createTestCredentials();
	 		c.AWSCredentialName = credName;
	 		c.constructor();
	 		c.bucketNameToDelete = 'test';
	 		c.deleteBucket();
	 }
	 public static testmethod void test5() {
	 		S3Controller c = new S3Controller();
	 		String credName = c.createTestCredentials();
	 		c.AWSCredentialName = credName;
	 		c.constructor();
	 		c.deleteObject();
	 }
	 public static testmethod void test6() {
	 		S3Controller c = new S3Controller();
	 		String credName = c.createTestCredentials();
	 		c.AWSCredentialName = credName;
	 		c.constructor();
	 		c.listBucket('00D110000008Zs0EAE', null, null, null, null);
	 }
	 public static testmethod void test7() {
	 		S3Controller c = new S3Controller();
	 		String credName = c.createTestCredentials();
	 		c.AWSCredentialName = credName;
	 		c.constructor();
	 		
	 		try{
	 			c.make_sig( 'foo');
	 		}catch(Exception ex){
			}
	 		//c.redirectToS3Key();
	 }
	 
	 public static testmethod void test8() {
	 		S3Controller c = new S3Controller();
	 		String credName = c.createTestCredentials();
	 		c.AWSCredentialName = credName;
	 		c.constructor();
	 		try{
	 			c.getHexPolicy( );
	 			c.getSignedPolicy();
	 		}catch(Exception ex){
			}
	 }
	 
	  public static testmethod void test9() {
	 		S3Controller c = new S3Controller();
	 		String credName = c.createTestCredentials();
	 		c.AWSCredentialName = credName;
	 		c.constructor();
	 		try 	{c.getBucketNames( );
	 		} catch(exception ee ) { 
	 		}
	 }
	 
	 public static testmethod void test10() {
	 		S3Controller c = new S3Controller();
	 		String credName = c.createTestCredentials();
	 		c.AWSCredentialName = credName;
	 		c.constructor();
	 		try{
	 			c.syncForceDoc();
	 		}catch(Exception ex){
			}
	 }
	 public static testmethod void test11() {
	 		S3Controller c = new S3Controller();
	 		String credName = c.createTestCredentials();
	 		c.AWSCredentialName = credName;
	 		c.constructor();
	 		c.updateBucketToUpload();
	 }
	 public static testmethod void test12() {
	 		S3Controller c = new S3Controller();
	 		String credName = c.createTestCredentials();
	 		c.AWSCredentialName = credName;
	 		c.constructor();
	 		c.updateFolderId();
	 }
	 public static testmethod void test13() {
	 	S3Controller c = new S3Controller();
	 	String credName = c.createTestCredentials();
	 	c.AWSCredentialName = credName;
	 	c.constructor();
	 	PageReference pageRef = Page.AWS_S3_Examples;
	 	
        pageRef.getParameters().put('filename','foo');
        Test.setCurrentPage(pageRef);
        c.bucketToList='testBucket';
	 	try{
	 		c.redirectToS3Key();
	 	}catch(Exception ex){
			}
	 	
	 }
	 
	public static testmethod void test14() {
 		S3Controller c = new S3Controller();
 		String credName = c.createTestCredentials();
 		c.AWSCredentialName = credName;
 		c.constructor();
 		c.updateAccessTypeSelected();
 		try{
 			c.syncFilesystemDoc();
 		}catch(Exception ex){
		}
	 }	 
	 
  	public static testmethod void test15() {
 		S3Controller c = new S3Controller();
 		String credName = c.createTestCredentials();
 		c.AWSCredentialName = credName;
 		c.constructor();
 		c.bucketNameToDelete = null;
 		c.deleteBucket();
	 }
	 
	 
	 
}