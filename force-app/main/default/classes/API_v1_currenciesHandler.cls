global class API_v1_currenciesHandler extends API_Manager {
	
	public AgencyCurrency getAgencyCurrencies(String agencyID, String agencyCurrencyCode){
			
		AgencyCurrency agencycurrency = new AgencyCurrency();
		agencycurrency.agencyID = agencyID;
		agencyCurrency.agencyCurrencyCode = agencyCurrencyCode;
		agencyCurrency.rates = new List<CurrencyRate>();
		
		for(Currency_rate__c cr : [Select CurrencyCode__c, Value__c from Currency_rate__c where Agency__c = :agencyID and CurrencyCode__c != null order by CurrencyCode__c])
			agencyCurrency.rates.add(createCurrencyRate(cr));
			
		return agencyCurrency;
		
	}
	
	
	private CurrencyRate createCurrencyRate(Currency_rate__c cr){
		CurrencyRate innercr = new CurrencyRate();
		innercr.code = cr.CurrencyCode__c;
		innercr.value = cr.value__c;
		return innercr;
	}

}