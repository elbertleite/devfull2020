public with sharing class school_campus_pictures {
	
	public Account accSelected{get;set;}
	public String maxUploadSize{get{if(maxUploadSize == null) maxUploadSize = '256000'; return maxUploadSize;} set;}
	public String maxUploadSizeInKB{get{if(maxUploadSizeInKB == null) maxUploadSizeInKB = '250KB'; return maxUploadSizeInKB;} set;}
	public List<IPClasses.S3File> images{get;set;}
	public String bucketName{get;set;}

	public Integer separatorImages{get;set;}

	public boolean eraseSchoolImages{get{if(eraseSchoolImages == null) eraseSchoolImages = false; return eraseSchoolImages;} set;}

	public school_campus_pictures() {
		try{
			bucketName = IPFunctions.getBucket();
			
			String id = ApexPages.currentPage().getParameters().get('id');
			accSelected = Database.query('SELECT ID, RecordType.Name, Preview_Link_Images__c, School_Images__c, Parent.ID FROM Account WHERE ID = :id');
			
		}catch(Exception ex){
			system.debug('Error on school_campus_pictures() <=== '+ex.getMessage()+' '+ex.getLineNumber());
		}
	}

	public void loadSchoolImages(){
		try{
			AWSKeys credentials = null;
			if(!Test.isRunningTest()){
				credentials = new AWSKeys(IPFunctions.awsCredentialName);
			}
			S3Controller s3 = new S3Controller();
			s3.constructor();
		
			s3.listBucket(bucketName, 'schools/'+accSelected.Parent.ID+'/images/'+accSelected.ID, null, null, null);
		
			if(Test.isRunningTest()){
				s3.bucketList = new List<S3.ListEntry>();
				S3.ListEntry entry = new S3.ListEntry();
				entry.key = 'schools/'+accSelected+'/images/'+accSelected+'/[u:Vitor Machado;]Classroom3_.jpeg';
				s3.bucketList.add(entry);
			}
			if(s3.bucketList != null && !s3.bucketList.isEmpty()){
				
				system.debug('IMAGES FOUND '+JSON.serialize(s3.bucketList));

				if(String.isEmpty(accSelected.School_Images__c)){
					images = new List<IPClasses.S3File>();
				}else{
					images = (List<IPClasses.S3File>) JSON.deserialize(accSelected.School_Images__c, List<IPClasses.S3File>.class);
				}

				String entryFileName;
				String shortfilename;
				String url;
				String downloadUrl;
				String previewLink;
				IPClasses.S3File picture;
				List<String> imagesName = new List<String>();

				for(S3.ListEntry entry : s3.bucketList){
					entryFileName = entry.key.remove(accSelected.ID+'/images/');
					String[] decon = FileUpload.breakFilename(entryFileName);
					shortfilename = decon[1];
					if(!Test.isRunningTest()){
						url = AWSHelper.generateAWSLink(bucketName, entry.key, credentials.key, credentials.secret);
						downloadUrl = AWSHelper.generateAWSLinkFileDownload(bucketName, entry.key, credentials.key, credentials.secret, shortfilename);
					}
					imagesName.add(shortfilename);

					picture = new IPClasses.S3File(url, shortfilename, downloadUrl);
					
					if(images.indexOf(picture) == -1){
						images.add(picture);
					}
				}

				List<String> imagesToRemove = new List<String>();
				for(Integer i = 0; i < images.size(); i++){
					if(!imagesName.contains(images.get(i).fileName)){
						imagesToRemove.add(images.get(i).fileName);
					}
				}
				
				if(imagesToRemove.size() > 0){
					for(String imageToRemove : imagesToRemove){
						images.remove(images.indexOf(new IPClasses.S3File(null, imageToRemove, null)));
					}
				}

				//separatorImages = images.size() > 6 ? 6 : images.size();
				separatorImages = 5;
				accSelected.School_Images__c = JSON.serialize(images);
			}else{
				images = new List<IPClasses.S3File>();
				accSelected.School_Images__c = JSON.serialize(new List<IPClasses.S3File>()); 
			}

			update accSelected;
		}catch(Exception ex){
			system.debug('Error on loadSchoolImages() <=== '+ex.getMessage()+' '+ex.getLineNumber());
		}
	}

	public void saveNewOrderImages(){
		try{
			String jsonImages = ApexPages.currentPage().getParameters().get('images');
			List<IPClasses.S3File> newOrderImages = (List<IPClasses.S3File>) JSON.deserialize(jsonImages, List<IPClasses.S3File>.class);
			accSelected.School_Images__c = JSON.serialize(newOrderImages);
			update accSelected;
		}catch(Exception ex){
			system.debug('Error on saveNewOrderImages() <=== '+ex.getMessage()+' '+ex.getLineNumber());
		}
	}
}