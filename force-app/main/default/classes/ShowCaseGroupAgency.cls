public without sharing class ShowCaseGroupAgency {
	
  public Account agencyGroup {get;set;}
  private String agencyGroupID;
  public Account agency {get;set;}
  public string officesIn { get{ if(officesIn == null) officesIn = ''; return officesIn;} set;}
  
  /* LIST */
  public list<string> destinationsOffered{get{ if(destinationsOffered == null) destinationsOffered = new list<string>(); return destinationsOffered; } set; }
  public list<string> mainCustormesFrom{get{ if(mainCustormesFrom == null) mainCustormesFrom = new list<string>(); return mainCustormesFrom; } set; }
  public list<string> mainCustomersTypesList { get{ if(mainCustomersTypesList == null) mainCustomersTypesList = new list<string>(); return mainCustomersTypesList; } set; }
  public list<string> recruitingStudentsList { get{ if(recruitingStudentsList == null) recruitingStudentsList = new list<string>(); return recruitingStudentsList; } set; }
  public list<string> industryCertificatesList { get{ if(industryCertificatesList == null) industryCertificatesList = new list<string>(); return industryCertificatesList; } set; }
  public list<enrolled> estudentsEnroller { get{ if(estudentsEnroller == null) estudentsEnroller = new list<enrolled>(); return estudentsEnroller; } set; }
  
  /* MAP */ 
  public map<string, string> socialNetwork{ get{ if(socialNetwork == null) socialNetwork = new map<string, string>(); return socialNetwork; } set; }
  public map<string, string> videos{ get{ if(videos == null) videos = new map<string, string>(); return videos; } set; }
 
  /* Wrapper chart CLASS */
  public class enrolled {
    public string typeStudies { get; set; }
    public Integer numStudents { get; set; }
    public enrolled(){}
    public enrolled( Integer numStudents, string typeStudies) {
      this.numStudents = numStudents;
      this.typeStudies = typeStudies;
    }
      
  }
  
  /* IMG CLASS */
  public class sDrivePictures{
    public string url {get;set;}
    public string description {get;set;}
  } 
  
  public List<sDrivePictures> imgs {
    get{        
      if(imgs == null){
        imgs = new List<sDrivePictures>();
        //:agencyGroupID '001O000000XrpxkIAB'
        for(Account_Picture_File__c apf:[Select id, url__c, Description__c FROM Account_Picture_File__c WHERE WIP__c = false and Content_Type__c LIKE 'image/%' and Parent__c = :agencyGroupID ORDER BY LastModifiedDate DESC]){
            sDrivePictures sdp = new sDrivePictures();                  
            sdp.url = apf.url__c;
            sdp.description = apf.Description__c;
            imgs.add(sdp);
        }
      }
      return imgs;
    }
    set;
  }
  
  public Integer numOffices{
  	get{
	  	if(numOffices==null){ 
	  		try{
	  			if(infoPlanetId == agencyGroupID){
	  				numOffices = agencyContactsList.size();
	  			}else {
		  			AggregateResult ar = [Select count(id) numOffices from Account where ParentID = :agencyGroupID group by ParentID];
				  	numOffices = (integer)ar.get('numOffices'); 
	  			}
	  		}catch(Exception e){
	  			String numberOffices = apexPages.currentPage().getParameters().get('of');
	  			if(numberOffices != null && numberOffices != '') 
	  			  numOffices = integer.valueof(numberOffices);
	  			else
	  			  numOffices = 0;
	  		}
	  	}	
	  	return numOffices;
  	}
  	set;
  }
  
  /* CONTACTS */
  
  private string infoPlanetId {
  	get{
  	 try {
  	 	 return [Select id from account where name = 'Information Planet Group' and recordType.name = 'Agency Group' limit 1].id;
  	 } catch (Exception e) {
  	   return '';
  	 }  		
  	}
  	private set;
  }
  	 
  
  public list<WS_restCallouts.agencyDetails> agencyContactsList{
    get{
      if(agencyContactsList == null){
        map<string, list<WS_restCallouts.agencyContacts>> aux = new map<string, list<WS_restCallouts.agencyContacts>>();
        list<string> orderList = new list<string>();
        agencyContactsList = new list<WS_restCallouts.agencyDetails>();
        if(infoPlanetId == agencyGroupID){
        	 for(WS_restCallouts.agencyDetails agency : WS_restCallouts.getIPAgencyContacts())
		          agencyContactsList.add(agency);
        }else{
	        for(Contact agencyContact: [Select Account.name, AccountId, Name, email, title from Contact WHERE recordtype.name = 'employee' AND account.parentId = :agencyGroupID order by Account.name, name]){
	          if(!aux.containsKey(agencyContact.Account.name)) {
	          	aux.put(agencyContact.Account.name, new list<WS_restCallouts.agencyContacts>{new WS_restCallouts.agencyContacts(agencyContact.name, agencyContact.email, agencyContact.title)});
	          	orderList.add(agencyContact.Account.name);
	          }
	          else aux.get(agencyContact.Account.name).add(new WS_restCallouts.agencyContacts(agencyContact.name, agencyContact.email, agencyContact.title));
	        }  
	          //agencyContactsList.add(new agencyDetails(agencyContact.Account.name, new agencyContacts(agencyContact.name, agencyContact.email, agencyContact.Occupation__c)));        
	        for(string ac: orderList)
	          agencyContactsList.add(new WS_restCallouts.agencyDetails(ac, aux.get(ac)));
	      }
	     
      }
      return agencyContactsList;
    }
    set;
  }
  
  /* CLASS WS_restCallouts 
  public class agencyDetails{
    public String name{get;set;}
    public list<agencyContacts> listContacts{get{if(listContacts == null) listContacts = new list<agencyContacts>(); return listContacts;} set;}
    
    public agencyDetails(String name, list<agencyContacts> listContacts){
     this.name = name;
     this.listContacts = listContacts;
    }
  }
  
  public class agencyContacts{
    public String name{get;set;}
    public String email{get;set;}
    public String occupation{get;set;}
    
    public agencyContacts(String name, String email, String occupation){
     this.name = (name!=null)?name:'';
     this.email = (email!=null)?email:'';
     this.occupation = (occupation!=null)?occupation:'';
    }
    //Select Account.name, AccountId, Name, email, Occupation__c from Contact WHERE recordtype.name = 'employee' AND account.parentId = '001O000000YXpjOIAT' order by Account.name, name
  }
  */
   
  public ShowCaseGroupAgency(ApexPages.StandardController controller){
    
    //agency = [Select id, ParentID from account where id = :controller.getRecord().id];
    System.debug('--------controller.getRecord().id: '+apexPages.currentPage().getParameters().get('id'));
    
    agencyGroupID = apexPages.currentPage().getParameters().get('id');
    
    System.debug('--------agencyGroupID: '+agencyGroupID);
    
    agencyGroup = getDetails(this.agencyGroupID);
    
    if(agencyGroup.Destinations_offered__c != null) destinationsOffered = agencyGroup.Destinations_offered__c.split(';');
    if(agencyGroup.Main_customers_from__c != null) mainCustormesFrom = agencyGroup.Main_customers_from__c.split(';');
    if(agencyGroup.Main_customer_types__c != null) mainCustomersTypesList = agencyGroup.Main_customer_types__c.split(';');
    if(agencyGroup.Recruiting_students_for__c != null) recruitingStudentsList = agencyGroup.Recruiting_students_for__c.split(';');
    if(agencyGroup.Industry_Certificates__c != null) industryCertificatesList = agencyGroup.Industry_Certificates__c.split(';');
    
    genEstudentsEnroller((String.isEmpty(agencyGroup.num_students_enrolled__c))?new list<string>():agencyGroup.num_students_enrolled__c.split(';'));
    genOfficesIn();
    genSocialNetwork((String.isEmpty(agencyGroup.Social_network_url__c))?new list<string>():agencyGroup.Social_network_url__c.split(';'));
    genVideos((String.isEmpty(agencyGroup.Videos__c))?new list<string>():agencyGroup.Videos__c.split(';'));
    
  }
  
  public void genSocialNetwork(list<string> network){
  	list<string> aux = new list<string>();
    for(string n: network){
      aux = n.split('>');
      socialNetwork.put(aux[0], aux[1]);
    }
  }
  
  /*private void genVideos(list<string> vhs){
    list<string> aux = new list<string>();
    for(string v: vhs){
      aux = v.split('>');
      videos.put(aux[0], aux[1]);
    }
    
  }*/
  
  private void genVideos(list<string> vhs){ //<url, img>
    list<string> aux = new list<string>();
    String url, img;
    for(string v: vhs){
      aux = v.split('>');
      if(aux[1] == 'youtube')  videos.put('https://www.youtube.com/embed/'+aux[0], 'https://i.ytimg.com/vi/'+ aux[0] +'/hqdefault.jpg');
      else{
      	//if(aux[1] == 'vimeo:xxxxxxxx') 
      	list<String> vimK = aux[1].split(':');
      	system.debug('aux==' + aux);
      	system.debug('Vimeo==' + vimK);
      	system.debug('Get0==' + aux[0]);
      	system.debug('Get1==' + vimK[1]);
      	if(!vimK.isEmpty()) videos.put('https://player.vimeo.com/video/'+aux[0], 'https://i.vimeocdn.com/video/'+ vimK.get(1) +'_100x75.jpg');
      	else videos.put('https://player.vimeo.com/video/'+aux[0], '');
      }
    }
    
  }
  
  public void genOfficesIn(){  	
  	map<string, list<string>> officesInAux = new map<string, list<string>>();
  	for(AggregateResult ar: [Select BillingCountry country, BillingCity city from Account where ParentID = :agencyGroupID group by BillingCountry, BillingCity order by BillingCountry]){
  		if((string) ar.get('country') != null){
  		if(!officesInAux.containsKey((string) ar.get('country')))
  			officesInAux.put((string) ar.get('country'), new list<string>{(string) ar.get('city')});
  		else  			
  			officesInAux.get((string) ar.get('country')).add((string) ar.get('city'));
  		}
  	}
  	
  	integer i = 0;	
  	for(string cities: officesInAux.keySet()){
  		officesIn += String.join(officesInAux.get(cities), ', ') + ' (' + cities + ')';
  		if(officesInAux.size()-1 != i) officesIn += ', ';
  		i++;
  	}
  	
  }
  
  private Account getDetails(String id){
    
    return [Select Social_network_url__c, Logo__c, Students_enrolled_in_the_last_12_months__c, num_students_enrolled__c, General_Description__c,  
              NumberOfEmployees, Skype__c, Phone, BillingCountry, BillingPostalCode, BillingState, BillingStreet, BillingCity, Destination__c, 
              Industry_Certificates__c, Recruiting_students_for__c, Main_customer_types__c, Main_customers_from__c, Destinations_offered__c, Name, 
              CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name, Group_Type__c, Videos__c,
              Registration_name__c, Year_established__c, BusinessEmail__c, Website, Directors__c
              from Account where id = :id];
  }
  
  /*private void genEstudentsEnroller(list<string> enroller){
    list<String> aux = new list<String>();
    enrolled enrol;
    
    for(string e: enroller){    
      aux = e.split(':');
    	enrol = new enrolled(Integer.valueOf(aux[1]), aux[0]);  
      estudentsEnroller.add(enrol);   
    }
  }*/
  
  private void genEstudentsEnroller(list<string> enroller){
    map<Integer, String> auxMap = new  map<Integer, String>();
    list<Integer> orderedList = new list<Integer>();
    list<String> auxList = new list<String>();
    enrolled enrol;
    
    for(string e: enroller){   
    	auxList = e.split(':');
      orderedList.add(Integer.valueOf(auxList[1]));
      auxMap.put(Integer.valueOf(auxList[1]), auxList[0]);
    }
    
    orderedList.sort();
    
    for(integer ol: orderedList){     
      enrol = new enrolled(ol, auxMap.get(ol));  
      estudentsEnroller.add(enrol);   
    }
  }
}