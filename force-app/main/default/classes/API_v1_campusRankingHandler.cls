global class API_v1_campusRankingHandler{
	global API_v1_campusRankingHandler(){}
}

/*global class API_v1_campusRankingHandler extends API_Manager {

    global without sharing class NoSharing {
		
		public List<CampusSupplier> getCampusRanking(String agencyID){
			
			List<CampusSupplier> supplierList = new List<CampusSupplier>();
			
			for(Supplier__c supplier : [select id, Supplier__r.id, Supplier__r.Name, rank__c, student_rank__c from Supplier__c 
									where agency__c = :agencyID and web_available__c = true and Supplier__r.Disabled_Campus__c = false and supplier__r.showCaseOnly__c = false
									order by Supplier__r.Name])
				supplierList.add(createCampusSupplierWrapper(supplier));
				
			return supplierList;
			
		}
		
		
		private CampusSupplier createCampusSupplierWrapper(Supplier__c supplier){
			CampusSupplier campus = new CampusSupplier();
			campus.id = supplier.Supplier__r.id;
			campus.name = supplier.supplier__r.name;
			campus.agencyRanking = supplier.rank__c;
			campus.studentRanking = supplier.student_rank__c;
			return campus;
		}
		
	}
	
	
	
}*/