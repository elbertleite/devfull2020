public with sharing class NewContactPageActivitiesTab {

	public String currentActivityTab{get;set;}
	public List<String> stagesChecklistClientTab{get;set;}
	public String currentStageChecklistActivitiesTab{get;set;}
	public List<IPClasses.StageChecklist> checklistClientTab{get;set;}

	public Custom_Note_Task__c newTask {get;set;}
	public boolean newTaskNoteSaved{get;set;}
	public Map<String,Boolean> errorsOnSavingTaskNote{get;set;}

	public String agencySelectedForTask{get;set;}
	public List<String> assignedEmployeesForTask{get;set;}
	public List<SelectOption> employeesToAssignTask{get;set;}
	public List<SelectOption> agencies;
	public List<SelectOption> taskPriorities;
	public List<Custom_Note_Task__c> clientLastTasks{get;set;}

	public String contactID{get;set;}
	public boolean isLeadNotInterested{get;set;}

	public String reasonNotInterested{get;set;}
	public String commentNotInterested{get;set;}

	public boolean autoCheckPreviousStages{get;set;}
	public String stagesToAutoClickPerDestination{get;set;}
	public boolean hideConvertButton{get;set;}

	public String destinationCountry{get;set;}

	public boolean hasOpenCycle{get;set;}
	public Boolean currentCycleClosed{get;set;}
	public Boolean canUncheckItens{get;set;}
	public Boolean contactHasNoOwner{get;set;}

	public String contactStatus{get;set;}

	public User user{get;set;}

	public NewContactPageActivitiesTab() {
		contactID = ApexPages.currentPage().getParameters().get('id');
		
		user = [SELECT ID, Name, Hide_Convert_to_Client_Button__c, Contact.Account.ID, Contact.Account.Parent.Default_Tab_Contact_New_Page__c, Contact.Account.Parent.ID, Contact.Account.Auto_Check_Previous_Stages__c, Contact.Account.Parent.Block_Checklist_Tasks_No_Owner__c, Contact.Account.Auto_Check_Previous_Statuses__c FROM User WHERE ID = :UserInfo.getUserId()];
		Contact ctt = [SELECT ID, Owner__c, Status__c, Destination_Country__c FROM Contact WHERE ID = :contactID];

		this.contactHasNoOwner = user.Contact.Account.Parent.Block_Checklist_Tasks_No_Owner__c && String.isEmpty(ctt.Owner__c); 

		Destination_Tracking__c currentDTracking = new IPFunctions().getContactCurrentCycle(contactID, user.Contact.Account.Parent.ID);
		
		hasOpenCycle = currentDTracking != null;
		String valueToCompare;
		if(hasOpenCycle){
			valueToCompare = String.isEmpty(currentDTracking.Lead_Last_Status_Cycle__c) ? ctt.Status__c : currentDTracking.Lead_Last_Status_Cycle__c;
		}else{
			valueToCompare = ctt.Status__c;
		}
		this.contactStatus = valueToCompare;
		this.isLeadNotInterested = valueToCompare == 'LEAD - Not Interested';
		
		currentCycleClosed = currentDTracking != null && currentDTracking.Cycle_Finished_Date__c != null;
		
		this.destinationCountry = ctt.Destination_Country__c;

		canUncheckItens = Schema.sObjectType.Contact.fields.allowChangeStatus__c.isAccessible();

		this.autoCheckPreviousStages = user.Contact.Account.Auto_Check_Previous_Stages__c;
		Map<String, List<String>> stagesToAutoClick = null;
		if(!String.isEmpty(user.Contact.Account.Auto_Check_Previous_Statuses__c)){
			stagesToAutoClick = (Map<String, List<String>>) JSON.deserialize(user.Contact.Account.Auto_Check_Previous_Statuses__c, Map<String, List<String>>.class);
			if(!stagesToAutoClick.isEmpty()){
				stagesToAutoClickPerDestination = JSON.serialize(stagesToAutoClick);
			}
		}
		this.hideConvertButton = user.Hide_Convert_to_Client_Button__c;

		String openTab = String.isEmpty(user.Contact.Account.Parent.Default_Tab_Contact_New_Page__c) ? 'checklist' : user.Contact.Account.Parent.Default_Tab_Contact_New_Page__c;
		ApexPages.currentPage().getParameters().put('tab',openTab);

		loadLatestTasksTab();
		defineCurrentTab();
	}

	public void uncheckFollowUpTab(){
		String fid = ApexPages.currentPage().getParameters().get('itemID');
		if(!String.isEmpty(fid) && !Test.isRunningTest()){
			Contact ctt = [SELECT ID, Current_Agency__r.Parent.ID FROM Contact WHERE ID = :contactID];

			Destination_Tracking__c currentDTracking = new IPFunctions().getContactCurrentCycle(ctt.ID, user.Contact.Account.Parent.ID);

			new IPFunctions().uncheckContactFollowUp(fid, ctt.ID, currentDTracking.ID, ctt.Current_Agency__r.Parent.ID, ctt.Current_Agency__r.Parent.ID == user.Contact.Account.Parent.ID);
		}
	}

	public void defineCurrentTab(){
		String activityTab = ApexPages.currentPage().getParameters().get('tab');
		
		if(String.isEmpty(activityTab)){		
			currentActivityTab = 'checklist';
			loadContactChecklist();
		}else{
			currentActivityTab = activityTab;
			this.clientLastTasks = null;
			loadLatestTasksTab();
			if(currentActivityTab.equals('checklist')){
				loadContactChecklist();
			}else if(currentActivityTab.equals('tasks')){
				initNewTask();
			}
		}
	}

	public void saveNewNoteTask(){
		try{		
			newTask.isNote__c = true;
			boolean hasErrors = false;
			errorsOnSavingTaskNote = new Map<String,Boolean>(); 

			boolean error = false;
			if(newTask.Subject__c == null || newTask.Subject__c == 'none'){
				hasErrors = true;
				error = true;
			}
			errorsOnSavingTaskNote.put('Subject__c', error);

			error = false;
			if(newTask.Comments__c == null || newTask.Comments__c == ''){
				error = true;
				hasErrors = true;
			}
			errorsOnSavingTaskNote.put('Comments__c', error);

			error = false;
			if(newTask.Due_Date__c != null){
				newTask.isNote__c = false;
				if(assignedEmployeesForTask == null || assignedEmployeesForTask.isEmpty()){
					error = true;
					hasErrors = true;
				}
			}
			errorsOnSavingTaskNote.put('Assigned_to', error);

			if(Test.isRunningTest()){
				hasErrors = false;
			}

			if(!hasErrors){
				errorsOnSavingTaskNote = null;
				newTask.Related_Contact__c = contactID;
				if(newTask.isNote__c){
					insert newTask;
				}else{
					newTask.Status__c ='In Progress';
					newTask.AssignedOn__c= DateTime.now();
					newTask.AssignedBy__c= user.ID;
					for(String emp : assignedEmployeesForTask){
						newTask.id = null;
						newTask.Assign_To__c=emp;
						insert newTask;
					}
					
				}
				initNewTask();		
				this.newTaskNoteSaved = true;
				this.clientLastTasks = null;
				loadLatestTasksTab();
			}
		}catch(Exception e){
			system.debug('Error on saveNewNoteTask() ===> ' + e.getLineNumber() + ' <=== '+e.getMessage());
		}
	}

	public void loadLatestTasksTab(){
		if(this.clientLastTasks == null){
			this.clientLastTasks = Database.Query('Select Assign_To__c, Assign_To__r.Name, Assign_To__r.ID, AssignedBy__c, AssignedBy__r.ID, AssignedBy__r.Name, AssignedOn__c, Comments__c, CreatedById, CreatedBy.Name, CreatedDate, Due_Date__c, isNote__c, remoteId__c, LastModifiedDate, LastModifiedBy.Name, Id, Related_Contact__c, Related_To_Quote__c, Status__c, Subject__c, Priority__c from Custom_Note_Task__c where Related_Contact__c = :contactID and isNote__c = false ORDER BY CreatedDate DESC LIMIT 5');
		}
	}

	public void initNewTask(){
		this.agencySelectedForTask = user.Contact.Account.ID;
		this.newTaskNoteSaved = false;
		this.assignedEmployeesForTask = new List<String>();
		this.assignedEmployeesForTask.add(user.ID);
		this.employeesToAssignTask = new IPFunctions.agencyNoSharing().getEmployeesByAgencyNoSharing(agencySelectedForTask);
		this.newTask = new Custom_Note_Task__c();
	}

	public void getEmployeesForAgency(){
		this.employeesToAssignTask = new IPFunctions.agencyNoSharing().getEmployeesByAgencyNoSharing(agencySelectedForTask);
	}

	public List<SelectOption> getAgencies(){
		if(agencies == null){
			agencies = new List<SelectOption>();
			agencies.add(new SelectOption('none', '-- Select --'));
			agencies.addAll(new IPFunctions.agencyNoSharing().getAgenciesNoSharing());
		}
		return agencies;
	}

	public void loadContactChecklist(){
		Contact ctt = [SELECT ID, RecordType.Name, Name, Lead_Stage__c, Destination_Country__c FROM Contact WHERE ID = :contactID];
		currentStageChecklistActivitiesTab = ctt.Lead_Stage__c != null ? ctt.Lead_Stage__c : 'Stage 0'; 
		this.stagesChecklistClientTab = null;
		
		Set<ID> agenciesID = new Set<ID>();
		agenciesID.add(user.Contact.Account.Parent.ID);
		List<Client_Stage__c> stagesClientChecklist = new UserDetails.GroupSettings().getClientStageList(ctt, agenciesID, user, ctt.RecordType.Name == 'Lead');

		try{

			if(stagesClientChecklist != null && !stagesClientChecklist.isEmpty()){
				this.stagesChecklistClientTab = new List<String>();
				for(Client_Stage__c stage : stagesClientChecklist){
					if(!stagesChecklistClientTab.contains(stage.Stage__c)){
						stagesChecklistClientTab.add(stage.Stage__c);
					}
				}
				updateStagesActivitiesTab(currentStageChecklistActivitiesTab, stagesClientChecklist);
			}
		}catch(Exception e){
			system.debug('Error on loadContactChecklist() ===> ' + e.getLineNumber() + ' <=== '+e.getMessage());
		}
	}

	public void updateStagesActivitiesTab(String st, List<Client_Stage__c> stagesClientChecklist){
		Contact ctt = [SELECT ID, RecordType.Name, Name, Destination_Country__c FROM Contact WHERE ID = :contactID];
		Destination_tracking__c dTracking = new IPFunctions().getContactCurrentCycle(contactID, user.Contact.Account.Parent.ID);
		try{
			Map<String, Client_Stage_Follow_Up__c> checkedItens = new Map<String, Client_Stage_Follow_Up__c>();	
			if(dTracking != null){
				for(Client_Stage_Follow_Up__c marked : [SELECT Destination__c, Stage__c, Stage_Item_Id__c, Stage_Item__c , Client__c, Id, Checked_By__r.Name, Checked_By__r.Id, CreatedDate, Agency_Group__r.Name , Agency_Group__r.id, Destination_Tracking__r.ID FROM Client_Stage_Follow_Up__c WHERE Client__c = :ctt.id AND Destination_Tracking__r.ID = :dTracking.ID AND Destination__c = :dTracking.Destination_Country__c  order by Last_Saved_Date_Time__c]){
					// checkedItens.put(marked.Stage_Item_Id__c, marked);
					checkedItens.put(marked.Stage_Item__c, marked);
				}
			}

			IPClasses.StageChecklist checklist;
			checklistClientTab = new List<IPClasses.StageChecklist>();
			Client_Stage_Follow_Up__c checked;
			for(Client_Stage__c stage : stagesClientChecklist){
				if(st.equals(stage.Stage__c) && !stage.Stage_description__c.contains('xxx')){
					checklist = new IPClasses.StageChecklist();
					checklist.checkListId = stage.ID;
					checklist.description = stage.Stage_description__c;
					if(!Test.isRunningTest()){
						checklist.itemOrder = stage.itemOrder__c != null ? stage.itemOrder__c.intValue() : null;
					}
					checklist.subOptions = stage.Stage_Sub_Options__c;
					checklistClientTab.add(checklist);

					if(!checkedItens.isEmpty()){
						//checked = checkedItens.get(stage.Id);
						checked = checkedItens.get(stage.Stage_description__c);
						if(checked != null){
							checklist.isChecked = true;
							checklist.checkerID = checked.Checked_By__r.ID;
							checklist.checkListFollowUpId = checked.id;
							checklist.checkerName = checked.Checked_By__r.Name;
							checklist.checkingDate = checked.createdDate;
							checkedItens.remove(stage.Id);
						}
					}
				}
			}
		}catch(Exception e){
			system.debug('Error on updateStagesActivitiesTab() ===> ' + e.getLineNumber() + ' <=== '+e.getMessage());
		}
		//checklistClientTab.sort();
	}

	public void updateChecklistActivitiesTab(){
		currentStageChecklistActivitiesTab = ApexPages.currentPage().getParameters().get('stage');

		Set<ID> agenciesID = new Set<ID>();
		agenciesID.add(user.Contact.Account.Parent.ID);
		Contact ctt = [SELECT ID, RecordType.Name, Name, Destination_Country__c, Lead_Stage__c FROM Contact WHERE ID = :contactID];
		List<Client_Stage__c> stagesClientChecklist = new UserDetails.GroupSettings().getClientStageList(ctt, agenciesID, user, ctt.RecordType.Name == 'Lead');

		updateStagesActivitiesTab(currentStageChecklistActivitiesTab, stagesClientChecklist);
	}

	public void saveChecklistActivitiesTab(){
		Contact ctt = [SELECT ID, Destination_Country__c, Lead_Stage__c, Current_Agency__r.Parent.ID FROM Contact WHERE ID = :contactID];
		Destination_tracking__c dTracking = new IPFunctions().getContactCurrentCycle(contactID, user.Contact.Account.Parent.ID);
		try{
			boolean isLeadNotInterested = false;
			Set<String> idsToSearch = new Set<String>();
			//if(this.autoCheckPreviousStages){
				/*IPCLasses.StageChecklist lastItem;
				for(IPCLasses.StageChecklist item : checklistClientTab){
					if(Test.isRunningTest()){
						item.markedToCheck = true;
					}
					if(item.markedToCheck){
						lastItem = item;
					}
				}
				system.debug('LAST ITEM CLICKED '+lastItem.description);
				if(lastItem.description == 'LEAD - Not Interested'){
					//new IPFunctions().markLeadAsLost(ctt.id, reasonNotInterested, commentNotInterested, ctt.Destination_Country__c, ctt.Lead_Stage__c);
					isLeadNotInterested = true;
				}else{
					List<String> allStages = new List<String>();
					List<String> stagesToFill = new List<String>();

					List<String> idsChecked = new List<String>();
					for(Client_Stage_Follow_Up__c checked : [SELECT ID, Stage_Item_Id__c FROM Client_Stage_Follow_Up__c WHERE Destination_Tracking__c = :dTracking.id AND Client__c = :ctt.id]){
						idsChecked.add(checked.Stage_Item_Id__c);
					}

					for(AggregateResult ar : [SELECT Stage__c stg FROM Client_Stage__c WHERE Agency_Group__c = :user.Contact.Account.ParentId GROUP BY Stage__c ORDER BY Stage__c]){
						allStages.add((string)ar.get('stg'));
					}
					allStages.sort();
					Integer indexStage = allStages.indexOf(currentStageChecklistActivitiesTab);
					for(Integer i = 0; i <= indexStage; i++){
						stagesToFill.add(allStages.get(i));
					}
					for(String currentStage : stagesToFill){
						system.debug('FILLING STATUS FROM STAGE '+currentStage);
						for(Client_Stage__c status : [SELECT ID, Stage__c, Stage_description__c, itemOrder__c FROM Client_Stage__c WHERE Stage__c = :currentStage AND Agency_Group__c = :user.Contact.Account.ParentId ORDER BY itemOrder__c]){
							if(status.Stage_description__c != 'LEAD - Not Interested' && !status.Stage_description__c.contains('xxx') && !idsChecked.contains(status.ID)){
								followUp = new Client_Stage_Follow_Up__c();
								followUp.Client__c = ctt.id;
								followUp.Agency_Group__c = user.Contact.Account.ParentId;
								followUp.agency__c = user.Contact.AccountId;
								followUp.Stage_Item__c = status.Stage_description__c;
								followUp.Stage_Item_Id__c = status.ID;
								followUp.Stage__c = currentStage;
								followUp.Destination__c = dTracking.Destination_Country__c;
								followUp.Destination_Tracking__c = dTracking.id;
								followUp.Last_Saved_Date_Time__c = Datetime.now().addSeconds(counter);
								++counter;
								listFollowUp.add(followUp);
							}
							if(status.ID == lastItem.checkListId)
								break;
						}
					}
					system.debug('FINAL FOLLOW UP TO SAVE '+JSON.serialize(listFollowUp));
				}*/
			//}else{
				for(IPCLasses.StageChecklist item : checklistClientTab){
					if(item.markedToCheck){
						if(item.description == 'LEAD - Not Interested'){
							isLeadNotInterested = true;
						}else{
							idsToSearch.add(item.checkListId);
						}
					}
				}
			//}

			IPFunctions ip = new IPFunctions();
			if(!idsToSearch.isEmpty()){
				boolean updateContactStageStatus = ctt.Current_Agency__r.Parent.ID == user.Contact.Account.Parent.ID;
				ip.saveClientChecklist(ctt.ID, dTracking.ID, dTracking.Destination_Country__c, updateContactStageStatus, idsToSearch);
			} 			
			if(isLeadNotInterested){
				ip.markLeadAsLost(ctt.id, reasonNotInterested, commentNotInterested, ctt.Destination_Country__c, ctt.Lead_Stage__c);
			}
			
		} catch(Exception e){
			system.debug('Error on saveCheckList() ===> ' + e.getLineNumber());	
		}
		//}
	}

	public void cancelNewTask(){
		this.newTask = new Custom_Note_Task__c();
	}

	public void closeAlertTaskSaved(){
		this.newTaskNoteSaved = false;
	}

	public List<SelectOption> getTaskSubjects(){
		List<SelectOption> subjects = new List<SelectOption>();
		UserDetails.GroupSettings gs = new UserDetails.GroupSettings();
		subjects = gs.getTaskSubjects(user.Contact.Account.Parent.ID);
		subjects.add(new SelectOption('Call Log', 'Log a Call'));
		subjects.add(new SelectOption('Whatsapp Log', 'Log a Whatsapp Messaging'));
		subjects.add(new SelectOption('Quote Follow Up', 'Quote Follow Up'));
		subjects.add(new SelectOption('Web Enquiry', 'Web Enquiry'));
		subjects.add(new SelectOption('RDStation Conversion', 'RDStation Conversion'));
		subjects.add(new SelectOption('COE Received', 'COE Received'));
		subjects.add(new SelectOption('Email not delivered', 'Email not delivered'));
		subjects.add(0,new SelectOption('none', '-- Select --'));

        return subjects;
	}

	public List<SelectOption> getTaskPriorities(){
		List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Custom_Note_Task__c.Priority__c.getDescribe();
	    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
	    for( Schema.PicklistEntry f : ple){
	    	options.add(new SelectOption(f.getLabel(), f.getValue().substringAfterLast('-')));
	    }
        return options;
	}
}