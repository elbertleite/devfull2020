@isTest(SeeAllData=false)
private class commissions_pend_pds_confirmations_test {

	public static Account school {get;set;}
	public static Account agency {get;set;}
	public static Contact emp {get;set;}
	public static Contact client {get;set;}
	public static User portalUser {get{
		if (null == portalUser) {
			portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
		}
		return portalUser;
	}set;}
	public static Account campus {get;set;}
	public static Course__c course {get;set;}
	public static Campus_Course__c campusCourse {get;set;}
	public static client_course__c clientCourseBooking {get;set;}
	public static client_course__c clientCourse {get;set;}
	public static client_course__c clientCourse2 {get;set;}


	@testSetup static void setup() {
		TestFactory tf = new TestFactory();

		school = tf.createSchool();
		agency = tf.createAgency();
		emp = tf.createEmployee(agency);
		portalUser = tf.createPortalUser(emp);
		campus = tf.createCampus(school, agency);
		course = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);

		Back_Office_Control__c bo = new Back_Office_Control__c(Agency__c = agency.id, Country__c = 'Australia', Backoffice__c = agency.id, Services__c = 'PDS_PCS_PFS_Chase');
		insert bo;

		system.runAs(portalUser){
			client = tf.createClient(agency);
			clientCourseBooking = tf.createBooking(client);
			clientCourse = tf.createClientCourse(client, school, campus, course, campusCourse, clientCourseBooking);
			tf.createClientCourseFees(clientCourse, false);
			List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(clientCourse);

			//TO REQUEST PDS
			instalments[0].Received_By_Agency__c = agency.id;
			instalments[0].Received_Date__c =  system.today();
			instalments[0].Paid_To_School_On__c =  system.today();
			instalments[0].isPDS__c = true;
			instalments[0].PDS_Confirmation_Invoice__c = null;

			update instalments;

			commissions_request_pds req = new commissions_request_pds();
			req.selectedCountry = 'Australia';
			req.selectedSchoolGP = 'all';
			req.selectedSchool = 'all';
			req.searchName = '';
			req.dates.Commission_Paid_Date__c = system.today().addYears(1);
			req.searchToRequest();

			req.result[0].campuses[0].instalments[0].installment.isSelected__c = true;
			ApexPages.currentPage().getParameters().put('schId', string.valueOf(req.result[0].schoolId));
			req.setToRequest();

			ApexPages.CurrentPage().getParameters().put('cn', 'Australia');
			ApexPages.currentPage().getParameters().put('type', 'requestConfirm');
			PaymentSchoolEmail invoice = new PaymentSchoolEmail();
			invoice.setupEmail();
			invoice.toAddress = 'test@educationhify.com';
			invoice.createInvoice();

			list<client_course_instalment__c> requestInstalments = [Select Id from client_course_instalment__c where PDS_Confirmation_Invoice__c != null];
			System.assertEquals(requestInstalments.size()>0, true);

		}
	}

	static testMethod void confirmPds(){
		Test.startTest();
		system.runAs(portalUser){
			commissions_pending_pds_confirmations toTest = new commissions_pending_pds_confirmations();
			toTest.selectedCountry = 'Australia';
			toTest.selectedSchoolGP = 'all';
			toTest.selectedSchool = 'all';
			toTest.searchName = '';
			toTest.searchPendingRequests();

			toTest.invResult[0].invoices[0].instalments[0].isSelected__c = true;
			String invoiceId = toTest.invResult[0].invoices[0].inv.id;
			String instalmentId = toTest.invResult[0].invoices[0].instalments[0].id;
			ApexPages.currentPage().getParameters().put('inv', invoiceId);
			toTest.confirmPds();

			client_course_instalment__c inst = [SELECT Id, School_Request_Confirmation_Status__c FROM client_course_instalment__c WHERE id = :instalmentId limit 1];
			system.assertEquals(inst.School_Request_Confirmation_Status__c, 'Confirmed');
		}
		Test.stopTest();
	}

	static testMethod void sendBackToRequest(){
		Test.startTest();
		system.runAs(portalUser){
			commissions_pending_pds_confirmations toTest = new commissions_pending_pds_confirmations();
			toTest.selectedCountry = 'Australia';
			toTest.selectedSchoolGP = 'all';
			toTest.selectedSchool = 'all';
			toTest.searchName = '';
			toTest.searchPendingRequests();

			toTest.invResult[0].invoices[0].instalments[0].isSelected__c = true;
			String invoiceId = toTest.invResult[0].invoices[0].inv.id;
			String instalmentId = toTest.invResult[0].invoices[0].instalments[0].id;
			ApexPages.currentPage().getParameters().put('invId', invoiceId);
			ApexPages.currentPage().getParameters().put('inst', instalmentId);
			toTest.sendBackToRequest();

			client_course_instalment__c inst = [SELECT Id, status__c  FROM client_course_instalment__c WHERE id = :instalmentId limit 1];
			system.assertEquals(inst.status__c , 'Sent back to request');
		}
		Test.stopTest();
	}

	static testMethod void noCommissionClaim(){
		Test.startTest();
		system.runAs(portalUser){
			commissions_pending_pds_confirmations toTest = new commissions_pending_pds_confirmations();
			toTest.selectedCountry = 'Australia';
			toTest.selectedSchoolGP = 'all';
			toTest.selectedSchool = 'all';
			toTest.searchName = '';
			toTest.searchPendingRequests();

			String instalmentId = toTest.invResult[0].invoices[0].instalments[0].id;
			String invoiceId = toTest.invResult[0].invoices[0].inv.id;
			ApexPages.currentPage().getParameters().put('cancelId', instalmentId);
			ApexPages.currentPage().getParameters().put('invId', invoiceId);
			toTest.setIdCancel();
			toTest.noCommissionClaim();

			client_course_instalment__c inst = [SELECT Id, Commission_Not_Claimable__c FROM client_course_instalment__c WHERE id = :instalmentId limit 1];
			system.assertEquals(inst.Commission_Not_Claimable__c, true);
		}
		Test.stopTest();
	}

	static testMethod void cancelPayment(){
		Test.startTest();
		system.runAs(portalUser){
			commissions_pending_pds_confirmations toTest = new commissions_pending_pds_confirmations();
			toTest.selectedCountry = 'Australia';
			toTest.selectedSchoolGP = 'all';
			toTest.selectedSchool = 'all';
			toTest.searchName = '';
			toTest.searchPendingRequests();

			String instalmentId = toTest.invResult[0].invoices[0].instalments[0].id;
			String invoiceId = toTest.invResult[0].invoices[0].inv.id;
			ApexPages.currentPage().getParameters().put('cancelId', instalmentId);
			ApexPages.currentPage().getParameters().put('invId', invoiceId);
			toTest.setIdCancel();
			toTest.cancelPayment();

			client_course_instalment__c inst = [SELECT Id, Received_Date__c FROM client_course_instalment__c WHERE id = :instalmentId limit 1];
			system.assertEquals(inst.Received_Date__c, null);
		}
		Test.stopTest();
	}

	static testMethod void cancelRequest(){
		Test.startTest();
		system.runAs(portalUser){
			commissions_pending_pds_confirmations toTest = new commissions_pending_pds_confirmations();
			toTest.selectedCountry = 'Australia';
			toTest.selectedSchoolGP = 'all';
			toTest.selectedSchool = 'all';
			toTest.searchName = '';
			toTest.searchPendingRequests();

			String invoiceId = toTest.invResult[0].invoices[0].inv.id;
			ApexPages.currentPage().getParameters().put('inv', invoiceId);
			toTest.cancelRequest();

			list<Invoice__c> invoice = [SELECT Id FROM Invoice__c WHERE id = :invoiceId];
			system.assertEquals(invoice.size()==0, true);
		}
		Test.stopTest();
	}
}