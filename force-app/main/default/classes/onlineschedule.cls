public without sharing class onlineschedule {
    
    public onlineschedule() {
        retrieveSchedule();
    }

    public Account selectedAgency{
        get{
            if(selectedAgency == null){

                string agencyId = ApexPages.currentPage().getParameters().get('ag');
                if(agencyId == null){
                    agencyId = '0019000001MjqIIAAZ'; //Sydney
                }
                selectedAgency = [Select id, showcase_timezone__c, BillingCountry, parent.Website from Account where id = :agencyId limit 1];
                if(selectedAgency.showcase_timezone__c != null ){
                    selectedAgency.showcase_timezone__c = selectedAgency.showcase_timezone__c.split(' \\(')[1].replaceAll('\\)','');
                    System.debug('==>selectedAgency.showcase_timezone__c: '+selectedAgency.showcase_timezone__c);
                }
                if(selectedAgency.parent.Website == null)
                    selectedAgency.parent.Website = 'https://www.informationplanet.com/';
                else if(selectedAgency.parent.Website.startsWithIgnoreCase('www'))
                    selectedAgency.parent.Website = 'http://'+selectedAgency.parent.Website;
                if(selectedAgency.showcase_timezone__c == null )
                    selectedAgency.showcase_timezone__c = 'Australia/Sydney';
            }
            return selectedAgency;
        } 
        set;
    }
    public string selectedDepartment{get; set;}
    
    public Client_Booking__c newBooking{
        get{
            if(newBooking == null){
                date bookingStartDate = date.valueOf(date.newInstance(GMTDate.year(),GMTDate.month(),GMTDate.day()) );
                String todayDay = GMTDate.format('EEEE');  
                if(todayDay == 'Saturday')
                    bookingStartDate = bookingStartDate.addDays(2);
                else if(todayDay =='Sunday')  
                    bookingStartDate = bookingStartDate.addDays(1);
                newBooking = new Client_Booking__c(Date_Booking__c = bookingStartDate);
            }
            return newBooking;
        }
        set;
    }
    public string bookToSave{get; set;}

    public class scheduleDetails{
        public string agencyName{get; set;}
        public map<string,departments> agencyDepartments{get; set;}
        public list<string> agencyDepartmentsOrdered{get; set;}
        public scheduleDetails(string agencyName, map<string,departments> agencyDepartments, list<string> agencyDepartmentsOrdered){
            this.agencyName = agencyName;
            this.agencyDepartments = agencyDepartments;
            this.agencyDepartmentsOrdered = agencyDepartmentsOrdered;
        }
    }

    public class departments{
        public string name{get; set;}
        public string departmentId{get; set;}
        public string Services_Details{get; set;}
        public map<string,string> services{get; set;}
        public map<string,list<userDetails>> users{get; set;}
    }

    public class userDetails{
        public user educationPlanner{get; set;}
        public list<bookTimeDetails> bookedTime{get; set;}
        public userDetails(user educationPlanner, list<bookTimeDetails> bookedTime){
            this.educationPlanner = educationPlanner;
            this.bookedTime = bookedTime;
        }
    }
    
    public class bookTimeDetails{
        public string vl_time{get; set;}
        public boolean booked{get; set;}
        public boolean outHour{get; set;}
        public bookTimeDetails(string vl_time, boolean booked, boolean outHour){
            this.vl_time = vl_time;
            this.booked = booked;
            this.outHour = outHour;
        }
    }

    // private final Date monday = Date.newInstance(1900, 1, 1);
	// private Integer getDayOfWeek(Date value) {
	    // return Math.mod(monday.daysBetween(value), 7);
    Datetime GMTDate = Datetime.valueOf(datetime.now().format('yyyy-MM-dd HH:mm:ss', selectedAgency.showcase_timezone__c));
    private list<bookTimeDetails> populateTime(set<string> selectedTime, string userId){
        System.debug('==>selectedTime: '+selectedTime);
        list<bookTimeDetails> lt = new list<bookTimeDetails>();
        bookTimeDetails bd;
        time Break_From;
        time Break_To;
        
        boolean resetTime = false;
        if(mapUsersDetails.get(userId).Break_From__c != null && mapUsersDetails.get(userId).Break_To__c != null){
            Break_From = Time.newInstance(integer.ValueOf(mapUsersDetails.get(userId).Break_From__c.split(':')[0]),integer.ValueOf(mapUsersDetails.get(userId).Break_From__c.split(':')[1]),0,0);
            Break_To = Time.newInstance(integer.ValueOf(mapUsersDetails.get(userId).Break_To__c.split(':')[0]),integer.ValueOf(mapUsersDetails.get(userId).Break_To__c.split(':')[1]),0,0);
        }

        // GMTDate = Datetime.valueOf(datetime.now().format('MM/dd/yyyy HH:mm:ss', 'America/New_York'));
        
        date GMTDateComp = date.valueOf(date.newInstance(GMTDate.year(),GMTDate.month(),GMTDate.day()) );
        
        integer interval = 30;
        time Work_FromTime = time.newInstance(9,0,0,0);
        time Work_ToTime = time.newInstance(18,0,0,0);
        if(mapUsersDetails.get(userId).Work_From_Time__c != null)
            Work_FromTime = time.newInstance(integer.ValueOf(mapUsersDetails.get(userId).Work_From_Time__c.split(':')[0]),integer.ValueOf(mapUsersDetails.get(userId).Work_From_Time__c.split(':')[1]),0,0);
        if(mapUsersDetails.get(userId).Work_To_Time__c != null)
            Work_ToTime = time.newInstance(integer.ValueOf(mapUsersDetails.get(userId).Work_To_Time__c.split(':')[0]),integer.ValueOf(mapUsersDetails.get(userId).Work_To_Time__c.split(':')[1]),0,0);
        if(mapUsersDetails.get(userId).Booking_Gap__c != null)
            interval = integer.valueOf(mapUsersDetails.get(userId).Booking_Gap__c);

        for(time i = Work_FromTime; i < Work_ToTime; i = i.addMinutes(interval) ){
            System.debug('==>i: '+i);
            System.debug('Work_FromTime: '+Work_FromTime);
            System.debug('Work_ToTime: '+Work_ToTime);
            if((Break_From == null || Break_To == null) || (Break_From != null && Break_To != null &&  ((i < Break_From && i < Break_To) || (i >= Break_From && i >= Break_To)))){
                lt.add(new bookTimeDetails(i.hour()+':'+string.valueOf(i.minute()).leftPad(2,'0'),selectedTime.contains(i.hour()+':'+string.valueOf(i.minute()).leftPad(2,'0')),newBooking.Date_Booking__c == GMTDateComp && i <= Time.newInstance(GMTDate.hour(), GMTDate.minute(),0,0)));
            }else if(Break_To != null && i <= Break_To && !resetTime){
                i = Break_To;
                i = i.addMinutes(-interval);
                resetTime = true;
            }
          
         
        }


        // for(integer i = Work_From; i < Work_To; i++){
        //     System.debug('==>Time.newInstance(i, 0, 0, 0): '+ i + ': ' +Time.newInstance(i, 0, 0, 0));

        //     if(!Delay_Start){
        //         if((Break_From == null || Break_To == null) || (Break_From != null && Break_To != null &&  ((Time.newInstance(i, 0, 0, 0) < Break_From && Time.newInstance(i, 0, 0, 0) < Break_To) || (Time.newInstance(i, 0, 0, 0) >= Break_From && Time.newInstance(i, 0, 0, 0) >= Break_To)))){
        //             lt.add(new bookTimeDetails(i+':00',selectedTime.contains(i+':00'),newBooking.Date_Booking__c == GMTDateComp && Time.newInstance(i, 0, 0, 0) <= Time.newInstance(GMTDate.hour(), GMTDate.minute(),0,0)));
        //         }
        //     // else{
        //     //     lt.add(new bookTimeDetails('Break', false, true));
        //     // }
        //     } else Delay_Start = false;
        //     if(!selectedAgency.Full_Hour_Booking__c && ( (Break_From == null || Break_To == null) || (Break_From != null && Break_To != null &&  ((Time.newInstance(i, integer.ValueOf(interval), 0, 0) < Break_From && Time.newInstance(i, integer.ValueOf(interval), 0, 0) < Break_To) || (Time.newInstance(i, integer.ValueOf(interval), 0, 0) >= Break_From && Time.newInstance(i, integer.ValueOf(interval), 0, 0) >= Break_To))) )){
        //         lt.add(new bookTimeDetails(i+':'+interval,selectedTime.contains(i+':'+interval), newBooking.Date_Booking__c == GMTDateComp && Time.newInstance(i, integer.valueOf(interval), 0, 0) <= Time.newInstance(GMTDate.hour(), GMTDate.minute(),0,0)));
        //     }
        //     // else{
        //     //     lt.add(new bookTimeDetails('Break', false, true));
        //     // }
        // }
        return lt;
    }


    public boolean showDepartments{get; set;}
    public map<string, scheduleDetails> scheduleDt{get; set;}
    public void retrieveSchedule(){
        // selectedAgency = '0019000001MjqIIAAZ';
        populateUsers(selectedAgency.id);
        map<String, List<User>> mpUser = departmentStaff(selectedAgency.id);
        scheduleDt = new map<string, scheduleDetails>();
        System.debug('==>mpUser: '+mpUser);
        for(Department__c dp:[SELECT Agency__c, Agency__r.name, Id, Name,Services__c, Services_Details__c FROM Department__c where agency__c  = :selectedAgency.id and Inactive__c = false and Allow_Booking__c = true]){
            if(selectedDepartment == null)
                selectedDepartment = dp.id;
            if(!scheduleDt.containsKey(dp.Agency__c)){
                scheduleDt.put(dp.Agency__c, new scheduleDetails(dp.Agency__r.name, new map<string, departments>{dp.id => createDepartment(dp, mpUser.get(dp.id))}, new list<string>{dp.id}) );
            }else{
                
               scheduleDt.get(dp.Agency__c).agencyDepartments.put(dp.id, createDepartment(dp, mpUser.get(dp.id)));
               scheduleDt.get(dp.Agency__c).agencyDepartmentsOrdered.add(dp.id);
            }
        }
        showDepartments = scheduleDt.size() > 0;
    }

    public void changeDepartment(){
        retrieveSchedule();
        selectedDepartment = ApexPages.currentPage().getParameters().get('selAgency');
    }

    public departments createDepartment(Department__c dp, List<User> usersDep){
        departments newDep = new departments();
        newDep.name = dp.Name;
        newDep.departmentId = dp.id;
        newDep.Services_Details = dp.Services_Details__c;

        if(dp.Services__c != null && dp.Services__c != ''){
            newDep.services = new map<string,string>();
            for(string serv:dp.Services__c.split(';'))
            newDep.services.put(serv.replaceAll('[^a-zA-Z0-9\\s+]', '_'), serv);

        }
        newDep.users = new map<string,list<userDetails>>(); // to make sure the map contains all departments
        newDep.users.put(dp.id,new list<userDetails>());
        for(user u:usersDep){

            newDep.users.get(dp.id).add(new userDetails(u, populateTime(mapUsers.get(u.id), u.id)));
        }
        return newDep;
    }

    private map<string, set<string>> mapUsers;
    private map<string, User> mapUsersDetails;
    private void populateUsers(string agencyId){
        mapUsers = new map<string, set<string>>();
        mapUsersDetails = new map<string, User>();
        String selectedDay = datetime.newInstance(newBooking.Date_Booking__c.year(),newBooking.Date_Booking__c.month(),newBooking.Date_Booking__c.day(),0,0,0).format('EEEE');  
        System.debug('==>selectedDay; '+selectedDay);
        for(user u:[Select Id, Contact.Account.ParentId, Break_From__c, Break_To__c, Booking_Gap__c,  Work_From_Time__c, Work_To_Time__c, Days_Off__c, User_Description__c, (SELECT Time_Booking__c FROM Clients_Booking__r where Date_Booking__c = :newBooking.Date_Booking__c) from User where Contact.AccountId = :agencyId and Contact.RecordType.name = 'Employee' and Contact.Department__c != null and isActive = true and Contact.Chatter_Only__c = false and Days_Off__c excludes (:selectedDay) ]){
            mapUsers.put(u.id, new set<string>());
            mapUsersDetails.put(u.id, u);
            for(Client_Booking__c bk:u.Clients_Booking__r)
                mapUsers.get(u.id).add(bk.Time_Booking__c.hour()+':'+string.valueOf(bk.Time_Booking__c.minute()).leftPad(2,'0'));

        }
    }


    public map<String, List<User>> departmentStaff(string agencyId){
        String selectedDay = datetime.newInstance(newBooking.Date_Booking__c.year(),newBooking.Date_Booking__c.month(),newBooking.Date_Booking__c.day(),0,0,0).format('EEEE');  
        System.debug('==>selectedDay; '+selectedDay);
		map<String, List<User>> mapDeptUsers = new map<String, List<User>>();
		List<Department__c> departments = [Select Id, Name, Services__c, Show_Visits__c from Department__c where Agency__c = :agencyId and Inactive__c = false and Allow_Booking__c = true];
		for(Department__c d : departments)
			mapDeptUsers.put(string.valueOf(d.Id), new list<User>());
		
		List<User> deptStaff= [Select Id, AccountId, ContactId, Contact.Name, Contact.Department__c, Contact.Department__r.name, SmallPhotoURL, Break_From__c, Break_To__c, Booking_Gap__c,  Work_From_Time__c, Work_To_Time__c, User_Description__c from User where Contact.AccountId = :agencyId and Contact.RecordType.name = 'Employee' and Contact.Department__c != null and isActive = true and Contact.Chatter_Only__c = false and Days_Off__c excludes (:selectedDay)];
		
		for(User u : deptStaff){
			if(u.Contact.Department__c!=null){
				if(mapDeptUsers.containsKey(string.valueOf(u.Contact.Department__c)))
					mapDeptUsers.get(string.valueOf(u.Contact.Department__c)).add(u);	
				// else
				// 	staffList.add(u);
			}
        }
        return mapDeptUsers;
    }

    public string selectedUserName{get; set;}
    public void retrieveBookDetails(){
        string vlTime = ApexPages.currentPage().getParameters().get('bookId');
        if(vlTime != null){
            list<string> bkTime = new list<string>(vlTime.split('#')[1].split(':'));
    
            newBooking.Time_Booking__c = Time.newInstance( Integer.valueOf(bkTime[0]) //hour
                                         ,Integer.valueOf(bkTime[1]) //min
                                         ,0                                //sec
                                         ,0);

            selectedUserName = [Select Name from User where id = :vlTime.split('#')[0] limit 1].name;
        }
    }

    public void openContact(){
        string contactId = ApexPages.currentPage().getParameters().get('ctId');
        contact opCont = [Select id, FirstName, LastName, Visa_Expiry_Date__c, Email, MobilePhone, Name, (Select Country__c, Detail__c from Forms_of_contact__r where Country__c = :selectedAgency.BillingCountry and Type__c = 'Mobile')  from contact where id = :contactId limit 1];
        newBooking = null;
        newBooking.Book_Email__c = opCont.Email;
        if(opCont.Forms_of_contact__r.size() > 0 && opCont.Forms_of_contact__r[0].Detail__c != null)
            newBooking.Mobile_Number__c = opCont.Forms_of_contact__r[0].Detail__c;
        newBooking.Visa_Expiring_Date__c = opCont.Visa_Expiry_Date__c;
        newBooking.Client_First_Name__c = opCont.FirstName;
        newBooking.Client_Last_Name__c = opCont.LastName;
        newBooking.Date_Booking__c = System.today();
       
    }

    public PageReference saveBooking(){
        bookToSave = ApexPages.currentPage().getParameters().get('bookId');
        string lstServices = ApexPages.currentPage().getParameters().get('lstServices');
        string sendEmail = ApexPages.currentPage().getParameters().get('sendEmail');
        successSaved = true;
        System.debug('===>lstServices: '+lstServices);
        list<string> bkDetail = bookToSave.split('#');
        String[] bkTime = bkDetail.get(1).split(':');
        Time timeBooking = Time.newInstance( Integer.valueOf(bkTime[0]) //hour
                                     ,Integer.valueOf(bkTime[1]) //min
                                     ,0                                //sec
                                     ,0);     

        string listServicesBook;
        if(lstServices != null){
            listServicesBook = string.join((List<String>) JSON.deserialize(lstServices, List<String>.class),', ');
        }
        
        if(sendEmail != null && boolean.valueOf(sendEmail))
            newBooking.Email_Confirmation__c = true;

        String QueryString = 'Select count() from Client_Booking__c where User__c = \''+bkDetail.get(0)+'\' and Date_Booking__c = '+IPFunctions.formatSqlDate(newBooking.Date_Booking__c)+' and Time_Booking__c = :timeBooking';
        if(Database.countQuery(QueryString) > 0){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, bkDetail.get(1) + ' is already booked. Please try another Time.');
            Apexpages.addMessage( myMsg );
            successSaved = false;
            //return null;
        }else{
            Client_Booking__c cb = new Client_Booking__c(Date_Booking__c = newBooking.Date_Booking__c, User__c = bkDetail.get(0), Time_Booking__c = timeBooking, Services_Requested__c = listServicesBook,
                                        Mobile_Number__c = newBooking.Mobile_Number__c, Book_Email__c = newBooking.Book_Email__c, Description__c = newBooking.Description__c, Email_Confirmation__c = newBooking.Email_Confirmation__c,
                                        Client_First_Name__c = newBooking.Client_First_Name__c, Client_Last_Name__c = newBooking.Client_Last_Name__c, Visa_Expiring_Date__c = newBooking.Visa_Expiring_Date__c,
                                        Contact_Type__c = newBooking.Contact_Type__c, Contact_Type_Detail__c = newBooking.Contact_Type_Detail__c, Facebook__c = newBooking.Facebook__c);
            insert cb;
            string groupId = mapUsersDetails.get(bkDetail.get(0)).Contact.Account.ParentId;
            try{
                System.debug('===>newBooking.Book_Email__c: '+newBooking.Book_Email__c);
                System.debug('===>groupId: '+groupId);
                list<Contact> listAcco = [Select id, Email from Contact WHERE Email = :newBooking.Book_Email__c and current_agency__r.ParentId = :groupId limit 1]; //If return should only have one contact
                System.debug('===>listAcco: '+listAcco);
                System.debug('===>cb: '+cb);
                if(listAcco != null || listAcco.size() > 0){
                    for(Contact ct:listAcco)
                        update new Client_Booking__c(id = cb.id, client__c = ct.id);
                }    		
                    
            }catch(Exception e){
                system.debug('Exception==>' + e);
            }
            system.debug('==>newBooking.Email_Confirmation__c: '+newBooking.Email_Confirmation__c);
            // if(newBooking.Email_Confirmation__c){
            //     string bodyEmail = 'This is a test';
            //     IPFunctions.sendEmail('Information Planet', 'develop@educationhify.com', newBooking.Client_First_Name__c +' ' +newBooking.Client_Last_Name__c, null, 'Booking Confirmation', bodyEmail);
            // }
            newBooking = null;
        }
        retrieveSchedule();
        return null;
    }
    
    public list<string> selectedServices{get; set;}



    // SERARCH CLIENT
    static List<String> rTypes = new List<String>{'Lead','Client'};

    static string urlEmptyPhoto;
    private static void geturlEmptyPhoto(){
        String commName = IPFunctions.getCommunityName();
        string resourceName = 'assets';
        string resources = '/' + commName + '/resource/';
        List<StaticResource> resourceList = [SELECT Name, NamespacePrefix, SystemModStamp FROM StaticResource WHERE Name = :resourceName];  
                      
        //Checking if the result is returned or not  
        if(resourceList.size() == 1)  
        {  
           //Getting namespace  
           String namespace = resourceList[0].NamespacePrefix;  
           //Resource URL  
           urlEmptyPhoto = resources + resourceList[0].SystemModStamp.getTime() + '/' + (namespace != null && namespace != '' ? namespace + '__' : '') + resourceName + '/images/unknown_profile_pic.png';  
        } 
        else urlEmptyPhoto = '';
    
    }

    @RemoteAction
    public static List <Contact> retrieveContact(String ContactName, string country) {

        String param = '%'+ContactName+'%';
        geturlEmptyPhoto();
      
            
        system.debug('@ rtypes: ' + rtypes);    
        
        list<string> listIds = new list<string>();
  
        if(ContactName.length() > 2){
        	            
            String sosl;
            sosl = 'find \''+ContactName+'\'  RETURNING Contact (Id where RecordType.name in :rTypes ) , Forms_of_Contact__c(Contact__c where Type__c in (\'Mobile\',\'WhatsApp\',\'Secondary e-mail\')) ';
            
            system.debug('contact sosl: ' + sosl);
            
            List<List<SObject>> searchList = search.query(sosl);
            map<string, Contact> Contacts = new map<string, Contact> ((List<Contact>)searchList[0]);

            for(Forms_of_Contact__c fc :(List<Forms_of_Contact__c>)searchList[1])
                listIds.add(fc.Contact__c);
            
            System.debug('==>Contacts: '+Contacts);
            
            if(Contacts != null && Contacts.size() >0)
                listIds.addAll(Contacts.keySet());


            return [SELECT Id, Name, MobilePhone, Email, Nationality__c, Current_Agency__r.ParentId, Current_Agency__r.Name, Owner__r.name, PhotoClient__c, RecordType.name, Expected_Travel_Date__c, Visa_Expiry_Date__c, 
                        (Select Country__c, Detail__c, Type__c from Forms_of_contact__r where Country__c = :country and Type__c = 'Mobile' and isDefault__c = true)
                        
                        FROM Contact where Id in :listIds order by name limit 10];
            
        } 
        
        return null;
    }

    public boolean successSaved{get; set;}
    public PageReference redirectThanks(){
        PageReference pageRef = new PageReference(selectedAgency.parent.Website);
        pageRef.setRedirect(true);
        return pageRef;
    }
}