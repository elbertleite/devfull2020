public class Quotation_Disclaimer{
	
	string agencyId;
	public Account agency {get;set;}
	
	public Quotation_Disclaimer(ApexPages.StandardController std){
		agencyId = std.getRecord().id;
		if(this.agencyId != null)
			agency = getDetails(this.agencyId);
	}
	
	private Account getDetails(String id){
		Account acc = [Select Name, CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name, Nickname__c, Agency_Type__c, Group_Type__c, Phone, Skype__c,
						BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,  ParentId, Parent.Name, 
						Registration_name__c, Year_established__c, BusinessEmail__c, Website,
						( Select Account_Nickname__c, Account_Name__c, Account_Number__c, Bank__c, Branch_Address__c, Branch_Name__c, Branch_Phone__c, BSB__c, IBAN__c, Notes__c, Swift_Code__c from Bank_Details1__r)
				from Account where id = :id];
		
		return acc;		
	}

	public Quotation_Disclaimer__c quoteDisclaimer{
		get{
			if(quoteDisclaimer == null)
				try{
					quoteDisclaimer = [Select Agency__c, Default_English_Disclaimer__c, Id, Destination__c, Language__c,
					(Select Disclaimer__c, Nationalities_Applicable__c, Quotation_Disclaimer__c, Language__c  from Quotation_Disclaimer_Other_Nationalities__r)
					from Quotation_Disclaimer__c where agency__c = :agencyId and destination__c = :destination order by LastModifiedDate limit 1];
					
					
				}catch(Exception e){
					quoteDisclaimer = new Quotation_Disclaimer__c();
					system.debug(e.getMessage());
					system.debug('agencyId: ' +  agencyId);
					system.debug('destination: ' + destination);
				}
			return quoteDisclaimer;
		}
		set;
	}
	
	public map<string,string> languages {
		get {
			languages = IPFunctions.getDisclaimerLanguages();
			languages.put(null,'');
			languages.put('en_US','English');
			return languages;
		}
		Set;
			
	}
	
	public List<SelectOption> notSelectedLanguagesOptions { 
		get {
			map<string,string> languages = IPFunctions.getDisclaimerLanguages();
			List<SelectOption> options = new List<SelectOption>();
			
			for (String s: languages.keySet()) {
				options.add(new SelectOption(s, languages.get(s)));
			}
			
			options = IPFunctions.SortOptionList(options);
			options.add(0, new SelectOption('','--Select a Language--'));
			options.add(1, new SelectOption('en_US','English'));
			
			
			return options;
		}
	}
	
	public string destination{get; set;}
	
	public pageReference retrieveDestination(){
		quoteDisclaimer = null;
		return null;
	}
	
	public pageReference cancelEdit(){
		if(quoteDisclaimer.id == null){
			destination = null;
		} else quoteDisclaimer = null;
		editmod = false;
		return null;
	}
	
	
	public pageReference saveDisclaimer(){
				
		if(quoteDisclaimer.Agency__c == null){
			quoteDisclaimer.Agency__c = agencyId;
			quoteDisclaimer.destination__c = destination;
		}
		upsert quoteDisclaimer;
		
		return null;
	}
	
	public List<SelectOption> getDestinationOptions() { 
		List<SelectOption> selOptions = new List<SelectOption>();
		
		selOptions.addAll(IPFunctions.CountryDestinations());
		if(selOptions.size() > 1)
			selOptions.add(1,new SelectOption('all', 'All Destinations'));
		return selOptions;
	}
	
	public Quotation_Disclaimer_Other_Nationality__c languageDisclaimer{get; set;}
	
	public boolean editmod{get{if(editmod == null) editmod = false; return editmod;} set;}
	public string langId{get; set;}
	public pageReference addNewLanguage(){
		langId = null;
		editmod = true;
		return null;
	}
	public pageReference editLanguage(){
		langId = ApexPages.currentPage().getParameters().get('langId');
		editmod = true;
		return null;
	}

	public pageReference deleteLanguage(){
		langId = ApexPages.currentPage().getParameters().get('langId');
		delete [Select id from Quotation_Disclaimer_Other_Nationality__c where id = :langId];
		quoteDisclaimer = null;
		return null;
	}

	public pageReference deleteDestination(){
		delete [Select id from Quotation_Disclaimer__c where Agency__c = :agencyId and destination__c = :destination];
		quoteDisclaimer = null;
		return null;
	}

	public map<string,string> getLanguage(){
		return IPFunctions.language;	
	}
	
}