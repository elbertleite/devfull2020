global without sharing class mass_message_sender {

    global static final Integer MAX_ITENS_PER_QUERY = 200;
    public mass_message_sender() {}

    @ReadOnly
	@RemoteAction
    global static Map<String, Object> initPage(){
        Map<String, Object> response = new Map<String, Object>();
        Map<String, Object> filterOptions = new Map<String, Object>();
        Map<String, Object> filterSelected = new Map<String, Object>();       
       
        User user = [Select Id, Name, Report_Convertion_Rate_Filters__c, Contact.Department__c, Contact.Department_View_Permission__c,  Contact.Export_Report_Permission__c, Contact.AccountId, Contact.Name, Contact.Account.Id, Contact.Account.Name, Contact.Account.Parent.RDStation_Campaigns__c, Contact.Account.Parent.Name, Contact.Account.Parent.Id, Aditional_Agency_Managment__c, Contact.Group_View_Permission__c, Contact.Agency_View_Permission__c, Leads_Distribution_Options__c, Leads_Distribution_Date_Filter_Access__c from User where Id = :UserInfo.getUserId()];

        filterSelected.put('groupSelected', user.Contact.Account.ParentId);
        filterOptions.put('groups', loadGroups(user.Contact.AccountId));

        filterSelected.put('agencySelected', user.Contact.Account.Id);
       //filterOptions.put('agencySelected', '0019000001w5TjqAAE');
        filterOptions.put('agencies', loadAgencies(user.Contact.Account.Id, user.Contact.Account.ParentId, Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible(), user.Contact.Group_View_Permission__c, user.Aditional_Agency_Managment__c));

        filterOptions.put('departments', loadDepartments(user.Contact.Account.Id));
        filterSelected.put('departmentSelected', String.isEmpty(user.Contact.Department__c) ? 'All' : user.Contact.Department__c);
        
        filterSelected.put('employeeSelected', new List<String>{user.Contact.ID});
        filterSelected.put('contactTypeSelected', 'All');

        List<IPClasses.SelectOptions> options = new List<IPClasses.SelectOptions>();
        for(SelectOption item : IPFunctions.getEmployeesByAgencyAndDepartment(user.Contact.Account.Id, user.Contact.Department__c)){
            options.add(new IPClasses.SelectOptions(item.getValue(), item.getLabel()));
        }
        if(options.size() > 1){
            options.add(0, new IPClasses.SelectOptions('All', 'All Owners'));
        }
        filterOptions.put('employees', options);
        
        filterOptions.put('hasPermissionGroup', user.Contact.Group_View_Permission__c);  
      	filterOptions.put('hasAditionalAgency', !String.isEmpty(user.Aditional_Agency_Managment__c));  
      	filterOptions.put('hasPermissionAgency', user.Contact.Agency_View_Permission__c);
      	filterOptions.put('hasPermissionExcel', user.Contact.Export_Report_Permission__c);
      	filterOptions.put('hasPermissionDepartment', user.Contact.Department_View_Permission__c);

        List<SelectOption> destinationsOptions = IPFunctions.getAllCountries();
        destinationsOptions.add(0, new SelectOption('All','All Destinations'));

        List<String> destinations = new List<String>();

        for(SelectOption option : destinationsOptions){
            destinations.add(option.getValue());
        }

        filterSelected.put('destinationSelected', 'All');
        filterSelected.put('nationalitySelected', 'All');
        filterOptions.put('countries', destinations);
        
        filterSelected.put('stageSelected','All');
        filterOptions.put('stages', loadStages(user.Contact.Account.ParentId, null));
        
        filterSelected.put('statusSelected','All');
        filterOptions.put('statuses', new List<String>{'All'});

		filterSelected.put('fromDate', Date.today().addMonths(-1).format());
        filterSelected.put('toDate', Date.today().format());
        
        //List<String> dateFilters = loadDateFilters(user.Leads_Distribution_Date_Filter_Access__c);
        List<String> dateFilters = loadDateFilters(NULL);
        filterOptions.put('dateFilter', dateFilters);
        filterSelected.put('dateFilterSelected', dateFilters.isEmpty() ? null : dateFilters.get(0));
        filterSelected.put('agencyBasedOn', 'currentAgency');
        filterSelected.put('maximumItensPerQuery', MAX_ITENS_PER_QUERY);
        filterSelected.put('queryIndex', 1);
        
        response.put('filterSelected', filterSelected);
        response.put('filterOptions', filterOptions);

        return response;
    }
    @ReadOnly
    @RemoteAction
    global static Map<String, Object> retrieveContacts(Map<String, Object> searchDetails, Map<String, User> owners){
        String lastItemId = (String) searchDetails.get('lastItemId');
        Map<String, Object> response = new Map<String, Object>();
        Map<String, ContactResponse> contacts = new Map<String, ContactResponse>(); 
        for(Destination_Tracking__c cycle : (List<Destination_Tracking__c>) query('cycles', searchDetails, lastItemId, owners)){
            contacts.put(cycle.Client__c, new ContactResponse(cycle));
        }
        for(Forms_Of_Contact__c fc : [SELECT Contact__c, Detail__c, isDefault__c, Is_WhatsApp_Number__c FROM Forms_Of_Contact__c WHERE Contact__c IN :contacts.keySet() AND (isDefault__c = true OR Is_WhatsApp_Number__c = true)]){
            if(fc.isDefault__c){
                contacts.get(fc.Contact__c).mobile = fc.Detail__c;    
            }
            if(fc.Is_WhatsApp_Number__c){
                contacts.get(fc.Contact__c).whatsApp = fc.Detail__c;    
            }
        }
        response.put('contacts', contacts.values());
        response.put('lastItemId', contacts.values().isEmpty() ? '' : contacts.values().get(contacts.values().size() - 1).cycleId);
        return response;
    }

    @RemoteAction
    @ReadOnly
    global static List<IPClasses.SelectOptions> retrieveEmailTemplates(Map<String, Object> searchDetails){
        String groupSelected = (String) searchDetails.get('groupSelected');
        String agencySelected = (String) searchDetails.get('agencySelected');
        String query;
        if(agencySelected != 'All'){
            query = 'SELECT id, Template_Description__c, agency__r.Name FROM Email_Template__c where agency__c = :agencySelected and Category__c = \'Lead/Client Mass Emails\' order by Template_Description__c ';
        }else{
            query = 'SELECT id, Template_Description__c, agency__r.Name FROM Email_Template__c where agency__r.parent.ID = :groupSelected and Category__c = \'Lead/Client Mass Emails\' order by agency__r.Name, Template_Description__c ';
        }
        List<IPClasses.SelectOptions> response = new List<IPClasses.SelectOptions>();
        String templateName; 
        response.add(new IPClasses.SelectOptions('', 'Choose a template', true));
        for(Email_Template__c et : Database.query(query)){
            //templateName = agencySelected == 'All' ? et.Template_Description__c : et.Template_Description__c + '('+et.agency__r.name+')';
            templateName = et.agency__r.name+' - '+et.Template_Description__c;
            response.add(new IPClasses.SelectOptions(et.ID, templateName, false));
        }

        return response;
    }
    @RemoteAction
    @ReadOnly
    global static Map<String, Map<String, String>> retrieveSMSTemplates(Map<String, Object> searchDetails){
        String groupSelected = (String) searchDetails.get('groupSelected');
        String agencySelected = (String) searchDetails.get('agencySelected');
        String query;
        if(agencySelected != 'All'){
            query = 'SELECT ID, Email_Subject__c, Template__c, Template_Description__c, Agency__r.Name FROM Email_Template__c where agency__c = :agencySelected and Category__c = \'Client Payments SMS\' order by Template_Description__c ';
        }else{
            query = 'SELECT ID, Email_Subject__c, Template__c, Template_Description__c, Agency__r.Name FROM Email_Template__c where agency__r.parent.ID = :groupSelected and Category__c = \'Client Payments SMS\' order by agency__r.Name, Template_Description__c ';
        }
        Map<String, Map<String, String>> response = new Map<String, Map<String, String>>();
        String templateName; 
        
        response.put('', new Map<String, String>());
        response.get('').put('name', 'Choose a template');
        response.get('').put('template', '');
        
        for(Email_Template__c et : Database.query(query)){
            templateName = et.agency__r.name+' - '+et.Template_Description__c;
            
            response.put(et.ID, new Map<String, String>());
            response.get(et.ID).put('name', templateName);
            response.get(et.ID).put('template', et.Template__c);
        }

        return response;
    }

    @RemoteAction
    @ReadOnly
    global static Map<String, String> retrieveEmailTemplatePreview(String id){
        Email_Template__c tp = [Select Template__c, Email_Subject__c from Email_Template__c where id = :id limit 1];
        Map<String, String> response = new Map<String, String>();
        response.put('subject', tp.Email_Subject__c);
        response.put('preview', tp.Template__c);
        return response;
    }

    @RemoteAction
    global static Object sendMassEmail(String templateId, List<String> contactsIds){
        Email_Template__c tp = [Select Template__c, Email_Subject__c from Email_Template__c where id = :templateId limit 1];
		String htmlAsString = tp.Template__c;
        String emailSubject = tp.Email_Subject__c;
        map<string, Object> mapFields = IPFunctions.findTokens(IPFunctions.tokensContact);
        List<String> fieldsTemplate = emailTemplateDynamic.extractFieldsFromFile(htmlAsString, mapFields);
        
        String ids = '';
        for(String ctt : contactsIds){
            ids += '\''+ctt+'\',';
        }
        ids = ids.removeEnd(',');

        String sqlWhere = ' FROM Contact WHERE ID in ('+ids+')';
        List<Contact> listResult = emailTemplateDynamic.createListResult(sqlWhere, mapFields, htmlAsString, fieldsTemplate);  
        map<string,string> templates = emailTemplateDynamic.createTemplate(mapFields, htmlAsString, fieldsTemplate, listResult);
        map<string, string> mSubject = new map<string, string>();
        for(contact ct : listResult){
            mSubject.put(ct.id, emailTemplateDynamic.createSingleTemplate(mapFields, emailSubject, fieldsTemplate, ct));
        }
        List<mandrillSendEmail.messageJson> listEmails = new list<mandrillSendEmail.messageJson>();

        string contactId = [Select ContactId from user where id = :UserInfo.getUserId() limit 1].ContactId;
	    Contact userDetails = [select id, Name, Email, Signature__c, AccountId, Account.Name, Account.ParentId, Account.Global_Link__c, Account.Clients_Payments_Email__c from Contact where id = :contactId];
        for(contact a : listResult){

            mandrillSendEmail mse = new mandrillSendEmail();
            mandrillSendEmail.messageJson email_md = new mandrillSendEmail.messageJson();
            
            email_md.setTo(a.Email, a.Name);
            email_md.setFromEmail(userDetails.Email);
            email_md.setFromName(userDetails.Name);
            email_md.setSubject(mSubject.get(a.id));
            email_md.setHtml(templates.get(a.id));

            listEmails.add(email_md);
            
            String emailService = IpFunctions.getS3EmailService();
            EmailToS3Controller s3 = new EmailToS3Controller();
            Datetime myDT = Datetime.now();
            String myDate = myDT.format('dd-MM-yyyy HH:mm:ss');
            Blob bodyblob = Blob.valueof(templates.get(a.id));//Blob body, String subject, String instalment, Contact client, Contact employee, String dateCreated
            s3.generateEmailToS3(a, userDetails, true, mSubject.get(a.id), bodyblob, false, myDate);
        }
        if(!Test.isRunningTest()){
            batch_sendMassEmail batchSendMass = new batch_SendMassEmail(listEmails);
            Database.executeBatch(batchSendMass,50);
        }

        return mSubject;
    }

    global class ContactResponse{
        global String cycleId{get;set;}
        global String id{get;set;}
        global String name{get;set;}
        global String agency{get;set;}
        global String owner{get;set;}
        global String recordType{get;set;}
        global String email{get;set;}
        global String whatsApp{get;set;}
        global String mobile{get;set;}
        global String nationality{get;set;}
        global String destination{get;set;}
        global Date birthDate{get;set;}
        global Datetime createdDate{get;set;}
        global Date expectedCloseDate{get;set;}
        global Date expectedTravelDate{get;set;}
        global Date arrivalDate{get;set;}
        global Date visaExpiryDate{get;set;}

        global ContactResponse(Destination_Tracking__c cycle){
            this.cycleId = cycle.ID;
            this.id = cycle.Client__c;
            this.name = cycle.Client__r.Name;
            this.recordType = cycle.Client__r.RecordType.Name;
            this.createdDate = cycle.Client__r.CreatedDate;
            this.email = cycle.Client__r.Email;
            this.birthDate = cycle.Client__R.Birthdate;
            if(cycle.Client__r.Owner__c != null){
                this.agency = cycle.Client__r.Owner__r.Contact.Account.Name;
                this.owner = cycle.Client__r.Owner__r.Contact.Name;
            //}else if(cycle.Client__r.Lead_Assignment_To__c != null){
            }else{
                this.agency = cycle.Client__r.Lead_Assignment_To__r.Account.Name;
                this.owner = cycle.Client__r.Lead_Assignment_To__r.Name;
            }/*else{
                this.agency = 'No Agency';
                this.owner = 'No Owner';
            }*/
            this.nationality = cycle.Client__r.Nationality__c; 
            this.destination = cycle.Destination_Country__c;
            this.expectedCloseDate = cycle.Estimated_Date_to_Close__c; 
            this.expectedTravelDate = cycle.Expected_Travel_date__c; 
            this.arrivalDate = cycle.Client__r.Arrival_date__c; 
            this.visaExpiryDate = cycle.Client__r.Visa_Expiry_Date__c; 
        }
    }

    @ReadOnly
    @RemoteAction
    global static Map<String, Object> countTotalContacts(Map<String, Object> searchDetails, List<String> employees){
        String groupSelected = (String) searchDetails.get('groupSelected');
        String agencySelected = (String) searchDetails.get('agencySelected');
        String departmentSelected = (String) searchDetails.get('departmentSelected');
        Map<String, User> owners = retrieveUsersForQuery(groupSelected, agencySelected, departmentSelected, employees);
        Map<String, Object> response = new Map<String, Object>();
        response.put('total', query('count', searchDetails, null, owners)); 
        response.put('owners', owners); 
        return response;
    }

    global static Object query(String returnType, Map<String, Object> searchDetails, String lastItemId, Map<String, User> owners){
        List<String> contactTypes = retrieveContactTypes((String) searchDetails.get('contactTypeSelected'));
        String groupSelected = (String) searchDetails.get('groupSelected');
        String agencySelected = (String) searchDetails.get('agencySelected');
        String agencyBasedOn = (String) searchDetails.get('agencyBasedOn');
        String departmentSelected = (String) searchDetails.get('departmentSelected');
        String nationalitySelected = (String) searchDetails.get('nationalitySelected');
        String destinationSelected = (String) searchDetails.get('destinationSelected');
        String statusSelected = (String) searchDetails.get('statusSelected');
        String stageSelected = (String) searchDetails.get('stageSelected');
        String dateFilterSelected = (String) searchDetails.get('dateFilterSelected');
        Date dateSearchFrom = IPFunctions.getDateFromString((String) searchDetails.get('fromDate'));
        Date dateSearchTo = IPFunctions.getDateFromString((String) searchDetails.get('toDate'));

        Set<String> usersIds = owners.keySet();
        Set<String> userContactIds = new Set<String>(); 
        for(String id : usersIds){
            userContactIds.add(owners.get(id).Contact.ID);
        }

        String query;
        if(returnType == 'count'){
            query = 'SELECT COUNT(Client__c) cnt FROM Destination_Tracking__c ';
        }else{
            //query = 'SELECT ID, Client__r.Current_Agency__r.Name, RDStation_Campaigns__c, Client__r.Contact_Origin__c, Client__r.Original_Agency__r.Name, Client__r.MobilePhone, Client__r.Phone, Client__r.Owner__r.Contact.Name, Client__c, Client__r.Name, Estimated_Date_to_Close__c, Expected_Travel_date__c, Client__r.Visa_Expiry_Date__c, CreatedDate, CreatedBy.Name, Client__r.Email, Client__r.CreatedDate, Client__r.Added_on_Newsletter__c, Client__r.CreatedBy.UserType, Client__r.CreatedBy.Name, Client__r.Lead_Converted_By__r.Name, Client__r.Lead_Converted_By__r.Account.Name, Client__r.Lead_Converted_On__c, Client__r.Owner__c, Client__r.RecordType.Name, Client__r.Client_Classification__c, Lead_Last_Stage_Cycle__c, Lead_Last_Status_Cycle__c, Destination_Country__c, Client__r.Nationality__c, Lead_Last_Change_Status_Cycle__c, Client__r.Arrival_date__c, Client__r.Lead_Status_Reason__c, Lead_Lost_Reason__c FROM Destination_Tracking__c ';
            query = 'SELECT ID, Client__c, Client__r.Name, Client__r.CreatedDate, Client__r.Arrival_date__c, Client__r.Visa_Expiry_Date__c, Estimated_Date_to_Close__c, Expected_Travel_date__c, Client__r.Email, Client__r.RecordType.Name, Client__r.Birthdate, Client__r.Owner__r.Contact.Name, Client__r.Owner__r.Contact.Account.Name, Client__r.Lead_Assignment_To__r.Name, Client__r.Lead_Assignment_To__c, Client__r.Lead_Assignment_To__r.Account.Name, Client__r.Nationality__c, Destination_Country__c FROM Destination_Tracking__c ';
        }
        query += ' WHERE Agency_Group__c = :groupSelected AND Current_Cycle__c = true AND Client__r.RecordType.Name in :contactTypes ';
        if(agencySelected != 'All'){
            if(agencyBasedOn == 'currentAgency'){
                query += 'AND Client__r.Current_Agency__c = :agencySelected ';
            }else{
                query += 'AND Client__r.Original_Agency__c = :agencySelected ';
            }
        }
        query += ' AND (Client__r.Owner__c IN :usersIDs OR Client__r.Lead_Assignment_To__c = :userContactIds) ';
        if(nationalitySelected != 'All'){
            query += 'AND Client__r.Nationality__c = :nationalitySelected ';
        }
        if(destinationSelected != 'All'){
            query += 'AND Destination_Country__c = :destinationSelected ';
        }
        if(stageSelected != 'All'){
            query += ' AND Lead_Last_Stage_Cycle__c = :stageSelected ';
        }
        if(statusSelected != 'All'){
            query += ' AND Lead_Last_Status_Cycle__c = :statusSelected ';
        }
        if(dateFilterSelected != 'None'){
            String dateField;
            if(dateFilterSelected == 'Lead / Client Created Date'){
                dateField = 'DAY_ONLY(convertTimezone(Client__r.CreatedDate))';
            }else if(dateFilterSelected == 'Lead / Client Birthday'){
                dateField = 'Client__r.Birthdate';
            }else if(dateFilterSelected == 'Client Arrival Date'){
                dateField = 'Client__r.Arrival_Date__c';
            }else if(dateFilterSelected == 'Expected Close Date'){
                dateField = 'Estimated_Date_to_Close__c';
            }else if(dateFilterSelected == 'Expected Travel Date'){
                dateField = 'Expected_Travel_date__c'; 
            }else if(dateFilterSelected == 'Visa Expiry Date'){
                dateField = 'Client__r.Visa_Expiry_Date__c';
            }
            query += 'AND '+dateField+' >= :dateSearchFrom AND '+dateField+' <= :dateSearchTo ';
        }
        if(returnType == 'count'){
            //query += ' GROUP BY COUNT(Client__c) ';
            AggregateResult ar = Database.query(query);
            return (Integer) ar.get('cnt');
        }else{
            if(!String.isEmpty(lastItemId)){
                query += ' AND ID > :lastItemId ';
            }
            query += ' LIMIT :MAX_ITENS_PER_QUERY ';
            return Database.query(query);
        }
    }

    global static Map<String, User> retrieveUsersForQuery(String groupSelected, String agencySelected, String departmentSelected, List<String> employees){
        String query = 'SELECT ID, Contact.name, Contact.ID, Contact.Account.Name, Contact.Account.ID FROM User WHERE Contact.Account.Parent.ID = :groupSelected AND Contact.RecordType.Name=\'Employee\' AND IsActive = true ';
        if(!String.isEmpty(agencySelected) && agencySelected != 'All'){
            query += 'AND Contact.Account.ID = :agencySelected ';
        }
        if(!String.isEmpty(departmentSelected) && departmentSelected != 'All'){
            query += 'AND Contact.Department__c = :departmentSelected ';
        }
        if(employees != null && !employees.isEmpty() && employees.get(0) != 'All'){
            query += 'AND Contact.ID IN :employees ';
        }
        Map<String, User> response = new Map<String, User>();
        for(User usr : Database.query(query)){
            response.put(usr.ID, usr);
        }
        return response;
    }

    public static List<String> retrieveContactTypes(String contactTypeSelected){
        if(contactTypeSelected == 'All'){
            return new List<String>{'Lead','Client'};    
        }else{
            return new List<String>{contactTypeSelected};    
        }
    }

    @ReadOnly
	@RemoteAction
    global static Map<String, Object> updateFieldsByGroup(Map<String, Object> searchDetails){
        User user = [Select Id, Contact.Account.Parent.Id, Aditional_Agency_Managment__c from User where Id = :UserInfo.getUserId()];
        Map<String, Object> response = new Map<String, Object>();
        String groupSelected = (String) searchDetails.get('groupSelected');
        List<IPClasses.SelectOptions> agencies =  loadAgencies(null, groupSelected, true, true, groupSelected == user.Contact.Account.Parent.Id ? user.Aditional_Agency_Managment__c : null);
        response.put('agencies', agencies);
        response.put('agencySelected', agencies.get(0).value);
        
        return response;
    }
    @ReadOnly
	@RemoteAction
    global static Map<String, Object> updateFieldsByAgency(Map<String, Object> searchDetails){
        String agency = (String) searchDetails.get('agencySelected');
        Map<String, Object> response = new Map<String, Object>();
        response.put('departments', loadDepartments(agency));
        response.put('departmentSelected', 'All');
        response.put('employees', loadEmployees(agency));

        return response;
    }
    @ReadOnly
	@RemoteAction
    global static Map<String, Object> updateFieldsByDepartment(Map<String, Object> searchDetails){
        String agency = (String) searchDetails.get('agencySelected');
        String department = (String) searchDetails.get('departmentSelected');
        Map<String, Object> response = new Map<String, Object>();
        if(agency == 'All'){
            response.put('employees', loadEmployees(agency));    
        }else{
            if(department != 'All'){
                List<IPClasses.SelectOptions> options = new List<IPClasses.SelectOptions>();
                for(SelectOption item : IPFunctions.getEmployeesByAgencyAndDepartment(agency, department)){
                    options.add(new IPClasses.SelectOptions(item.getValue(), item.getLabel()));
                }
                if(options.size() > 1){
                    options.add(0, new IPClasses.SelectOptions('All', 'All Owners'));
                }
                response.put('employees', options);
            }else{
                response.put('employees', loadEmployees(agency));
            }
        }
        return response;

    }
    @ReadOnly
	@RemoteAction
    global static Map<String, Object> updateFieldsByDestination(Map<String, Object> searchDetails){
        String groupSelected = (String) searchDetails.get('groupSelected'); 
        String destination = (String) searchDetails.get('destinationSelected'); 
        Map<String, Object> response = new Map<String, Object>();
        response.put('stageSelected','All');
        response.put('statusSelected','All');
        response.put('stages', loadStages(groupSelected, destination));
        response.put('statuses', new List<String>{'All'});
        return response;
    }
    global static List<String> loadDateFilters(String filtersAccess){
        List<String> response = new List<String>();
        //response.add('None');
        if(String.isEmpty(filtersAccess)){
            response.add('None');
            response.add('Lead / Client Created Date');
            response.add('Lead / Client Birthday');
            response.add('Client Arrival Date');
            response.add('Expected Close Date');
            response.add('Expected Travel Date');
            response.add('Visa Expiry Date');
        }else{
            Map<String, Boolean> access = (Map<String, Boolean>) JSON.deserialize(filtersAccess, Map<String, Boolean>.class);
            for(String key : access.keySet()){
                if(access.get(key)){
                    response.add(key);
                }
            }
        }     
        return response;
    }
    @ReadOnly
	@RemoteAction
    global static Map<String, Object> updateFieldsByStage(Map<String, Object> searchDetails){
        String groupSelected = (String) searchDetails.get('groupSelected');   
        String stage = (String) searchDetails.get('stageSelected');  
        String destination = (String) searchDetails.get('destinationSelected'); 
        Map<String, Object> response = new Map<String, Object>();
        
        response.put('statusSelected','All');
        response.put('statuses', loadStatuses(groupSelected, stage, destination));
        
        return response;
    }

    global static List<String> loadStages(String groupSelected, String destination){
        List<String> response;
        
        String query = 'SELECT Stage__c FROM Client_Stage__c WHERE Agency_Group__c = :groupSelected ';
        if(!String.isEmpty(destination) && destination != 'All'){
            query += 'AND Destination__c = :destination';
        }
        query += ' ORDER BY Stage__c';
        
        Set<String> stages = new Set<String>();
        for(Client_Stage__c stage : Database.query(query)){
            stages.add(stage.Stage__c);
        }

        if(stages.isEmpty()){
            response = new List<String>();
            response.add('All');
            response.add('Stage 0');
        }else{
            response = new List<String>(stages);
            if(!stages.contains('Stage 0')){
                response.add('Stage 0');
            }
            response.sort();
            response.add(0,'All');
        }
        return response;
    }
    global static List<String> loadStatuses(String groupSelected, String stage, String destination){
        if(stage == 'All'){
            return new List<String>{'All'};
        }else{
            String query = 'Select Stage_description__c FROM Client_Stage__c WHERE Agency_Group__c = :groupSelected AND Stage__c = :stage ';
            if(stage != 'Stage 0' && destination != 'All'){
                query += 'AND Destination__c = :destination';
            }
            Set<String> statuses = new Set<String>();
            for(Client_Stage__c status : Database.query(query)){
                if(!status.Stage_description__c.contains('xxx')){
                    statuses.add(status.Stage_description__c);
                }
            }
            List<String> response = new List<String>(statuses);
            response.sort();
            response.add(0,'All');
            return response;
        }
    }
    global static List<IPClasses.SelectOptions> loadGroups(String agencyID){
		Set<String> idsAllGroups = new Set<String>();
    	for(SelectOption option : IPFunctions.getAgencyGroupsByPermission(agencyID, Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())){
      		idsAllGroups.add(option.getValue());
		}
    	List<IPClasses.SelectOptions> options = new List<IPClasses.SelectOptions>();
    	for(Account option : [Select Id, Name from Account where id in :idsAllGroups AND Inactive__c = false order by name]){
      		options.add(new IPClasses.SelectOptions(option.ID, option.Name));
    	}
    	return options;
    }
    global static List<IPClasses.SelectOptions> loadAgencies(String agencyID, String groupID, Boolean allAgenciesAcessible, Boolean hasPermissionGroup, String aditionalAgencies){
		List<IPClasses.SelectOptions> options = new List<IPClasses.SelectOptions>();
		Set<String> agencies = new Set<String>();
        if(!allAgenciesAcessible && !hasPermissionGroup){ //set the user to see only his own agency
            for(Account a: [Select Id, Name from Account where id =:agencyID AND Inactive__c = false order by name]){
                 agencies.add(a.ID);
            }
            if(!String.isEmpty(aditionalAgencies)){
                for(Account ac:ipFunctions.listAgencyManagment(aditionalAgencies)){
                    if(!ac.Inactive__c){
                        agencies.add(ac.ID);
                    }
                }
            }
        }else{
            for(Account ag : [Select Id, Name From Account Where Recordtype.Name = 'Agency' AND ParentId = :groupID AND Inactive__c = false]){
                agencies.add(ag.ID);
            }
        }
        if(agencies.size() > 1){
           options.add(new IPClasses.SelectOptions('All', 'All Available Agencies')); 
        }

        for(Account option : [Select Id, Name from Account where id in :agencies AND Inactive__c = false order by name]){
            options.add(new IPClasses.SelectOptions(option.ID, option.Name));
        }
        
		return options;
  	}
    global static List<IPClasses.SelectOptions> loadEmployees(String agencyID){
		List<IPClasses.SelectOptions> options = new List<IPClasses.SelectOptions>();
		if(!String.isEmpty(agencyID) && agencyID != 'All'){
			for(SelectOption option : IPFunctions.getEmployeesByAgency(agencyID)){
				options.add(new IPClasses.SelectOptions(option.getValue(), option.getLabel()));
			}
            if(options.size() > 1){
                options.add(0, new IPClasses.SelectOptions('All', 'All Owners'));
            }
		}
		return options;
	}
    global static List<IPClasses.SelectOptions> loadDepartments(String agencyID){
        List<IPClasses.SelectOptions> options = new List<IPClasses.SelectOptions>();
        options.add(new IPClasses.SelectOptions('All','All'));
        Set<String> ids = new Set<String>();
        if(!String.isEmpty(agencyID) && agencyID != 'All'){
            for(User department : [SELECT Contact.Department__r.Name, Contact.Department__c FROM User WHERE Contact.Account.Id = :agencyID and Contact.RecordType.Name='Employee' and IsActive = true ORDER BY Contact.Department__r.Name]){
                if(!ids.contains(department.Contact.Department__c)){
                    options.add(new IPClasses.SelectOptions(department.Contact.Department__c, department.Contact.Department__r.Name));
                    ids.add(department.Contact.Department__c);
                }
            }
        }   
        return options;
    }
}