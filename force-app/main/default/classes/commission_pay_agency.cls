public with sharing class commission_pay_agency {


	public String bucketName {get;set;}
	public String gbLink {get;set;}
	public list<countryCourses> result {get;set;}
	public DateTime currencyLastModifiedDate {get;set;}
	public list<SelectOption> mainCurrencies {get;set;}

	private Map<String, double> agencyCurrencies;
	private String lastAgency {get;set;} //variable that controls if counter must be restarted or not;
	private IPFunctions.SearchNoSharing sns {get{if(sns == null) sns = new IPFunctions.SearchNoSharing(); return sns;}set;}
	private map<string,string> mapgbLink {get{if(mapgbLink==null) mapgbLink = new map<String,String>(); return mapgbLink;}set;}
	private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}

	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}

	//CONSTRUCTOR
	public commission_pay_agency() {
		gbLink = currentUser.Contact.Account.Global_Link__c;
		bucketName = IPFunctions.s3bucket;

		mainCurrencies = ff.retrieveMainCurrencies();
		agencyCurrencies = ff.retrieveAgencyCurrencies();
		currencyLastModifiedDate = ff.currencyLastModifiedDate;

		getAgencyGroupOptions();
		changeGroup();
	}


	//F I N D 		I N V O I C E S
	public void findInvoices(){

		if(mapgbLink.size()>0)
			gbLink = mapgbLink.get(selectedAgency);
		else
			gbLink = currentUser.Contact.Account.Global_Link__c;

		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && currentUser.Aditional_Agency_Managment__c == null) //set the user to see only his own agency
			selectedAgency = currentUser.Contact.AccountId;

		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()) //set the user to see only his own agency group
			selectedAgencyGroup = currentUser.Contact.Account.ParentId;

		String sql = 'SELECT ID, Invoice__c, Due_Date__c, summary__c, Invoice_Activities__c, Sent_Email_To__c, Share_Commission_Number__c, Total_Value__c, Paid_On__c, Requested_Commission_for_Agency__c, Requested_Commission_for_Agency__r.Name, Requested_Commission_for_Agency__r.BillingCountry, Total_Instalments_Requested__c, Payment_Receipt__c, createdBy.Name, Requested_Commission_To_Agency__c, Country__c, CurrencyIsoCode__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, Transfer_Send_Fee_Currency__c, Transfer_Send_Fee__c, Bank_to_Deposit__r.Bank__c,	Bank_to_Deposit__r.Account_Nickname__c,	Bank_to_Deposit__r.Branch_Address__c,	Bank_to_Deposit__r.Account_Name__c,	Bank_to_Deposit__r.BSB__c,	Bank_to_Deposit__r.Account_Number__c,	Bank_to_Deposit__r.Swift_Code__c,	Bank_to_Deposit__r.IBAN__c, createdBy.Contact.Email, Requested_Commission_To_Agency__r.Name, Requested_by_Agency__r.Name, ';

		 sql+= ' (SELECT Id, client_course__r.Enroled_by_Agency__r.Name, Number__c, Split_Number__c, Tuition_Value__c, Commission_Tax_Value__c, client_course__r.School_Name__c, isPFS__c, isPCS__c, isPDS__c, Shared_Comm_Agency_Enroll_Value__c, Shared_Comm_Tax_Agency_Enroll_Value__c, Shared_Comm_Agency_Enroll__c, client_course__r.Client__r.Name, Client_Course__r.Client__r.Owner__r.Name, Client_Course__r.Course_Name__c, isInstalment_Amendment__c, instalment_activities__c FROM cci_shared_commission_bank_dept_enrolled__r order by client_course__r.Client__r.Name ),';

		 sql+= ' (SELECT Id, Pay_to_Agency__r.Name, Number__c, Split_Number__c, Tuition_Value__c, Discount__c, Commission_Tax_Rate__c, Commission__c, Commission_Value__c, Commission_Tax_Value__c, isPFS__c, isPCS__c, isPDS__c, Shared_Comm_Pay_Agency_Value__c, Shared_Comm_Pay_Agency__c, Shared_Comm_Tax_Pay_Agency_Value__c, client_course__r.Client__r.Name, client_course__r.School_Name__c, Client_Course__r.Course_Name__c, Client_Course__r.Client__r.Owner__r.Name, isInstalment_Amendment__c, client_course__r.Commission_Type__c, instalment_activities__c FROM client_course_inst_bank_pay_Agency__r order by client_course__r.Client__r.Name,  client_course__r.School_Name__c, Client_Course__r.Course_Name__c ),';

		 sql+= ' (SELECT Id, Received_By_Agency__r.Name, Number__c, Split_Number__c, Tuition_Value__c, Commission_Tax_Value__c, client_course__r.School_Name__c, isPFS__c, isPCS__c, isPDS__c, Shared_Comm_Agency_Receive_Value__c, Shared_Comm_Agency_Receive__c, Shared_Comm_Tax_Agency_Receive_Value__c, Commission_Value__c, Discount__c, client_course__r.Client__r.Name, Client_Course__r.Client__r.Owner__r.Name, Client_Course__r.Course_Name__c, isInstalment_Amendment__c, isReceived_Enrolled_Different_Agency__c, Received_By_Agency__c, client_course__r.Enroled_by_Agency__c, isRequestedShareCommission__c, instalment_activities__c FROM cci_shared_commission_bank_dept_received__r order by client_course__r.Client__r.Name ),';

		 sql+= ' (SELECT Id, client_course__r.share_commission_request_agency__r.Name, Number__c, Split_Number__c, Tuition_Value__c, Commission_Tax_Value__c, client_course__r.School_Name__c, isPFS__c, isPCS__c, isPDS__c, Shared_Comm_Agency_Request_Value__c, Shared_Comm_Tax_Agency_Request_Value__c, Shared_Comm_Agency_Request__c, client_course__r.Client__r.Name, Client_Course__r.Client__r.Owner__r.Name, Client_Course__r.Course_Name__c, isInstalment_Amendment__c, instalment_activities__c FROM cci_shared_commission_bank_dept_request__r order by client_course__r.Client__r.Name )';

		 sql+= ' FROM Invoice__c WHERE isShare_Commission_Invoice__c = TRUE AND Requested_Commission_To_Agency__c =  \'' + selectedAgency + '\' AND Paid_On__c = NULL AND Invoice__c != NULL AND isCancelled__c = FALSE ';

		 sql+= ' order by createdDate';

		system.debug('@SQL==>' + sql);

		// map<string, agencyInvoices> mapResult = new map<string,agencyInvoices>();
		map<String,countryCourses> countryMap = new map<String, countryCourses>();
		result = new list<countryCourses>();
		Invoice newInv;

		for(Invoice__c inv : sns.NSLInvoice(sql)){

			inv.Agency_Currency__c = currentUser.Contact.Account.account_currency_iso_code__c;
			inv.Transfer_Send_Fee_Currency__c = inv.Agency_Currency__c;
			inv.Agency_Currency_Rate__c = agencyCurrencies.get(inv.CurrencyIsoCode__c);

			ff.convertCurrency(inv, inv.CurrencyIsoCode__c, inv.Total_Value__c, null, 'Agency_Currency__c', 'Agency_Currency_Rate__c', 'Agency_Currency_Value__c');

			//Group by Agency
			newInv = new Invoice();
			newInv.invoice = inv;

			if(!countryMap.containsKey(inv.Requested_Commission_for_Agency__r.BillingCountry))
				countryMap.put(inv.Requested_Commission_for_Agency__r.BillingCountry, new countryCourses(inv.Requested_Commission_for_Agency__r.Name, new agencyInvoices(inv.Requested_Commission_for_Agency__c, inv.Requested_Commission_for_Agency__r.Name, newInv), inv.Requested_Commission_for_Agency__r.BillingCountry, inv.CurrencyIsoCode__c));

			else if(countryMap.get(inv.Requested_Commission_for_Agency__r.BillingCountry).hasAgency(inv.Requested_Commission_for_Agency__r.Name))
				countryMap.get(inv.Requested_Commission_for_Agency__r.BillingCountry).addInvoice(inv.Requested_Commission_for_Agency__r.Name, newInv);

			else
				countryMap.get(inv.Requested_Commission_for_Agency__r.BillingCountry).addAgency(inv.Requested_Commission_for_Agency__r.Name, new agencyInvoices(inv.Requested_Commission_for_Agency__c, inv.Requested_Commission_for_Agency__r.Name, newInv));
		}//end for

		for(String ct : countryMap.keySet()){
			countryMap.get(ct).orderAgencies();
			result.add(countryMap.get(ct));
		}//end for
	}


	//C O N F I R M 		P A Y M E N T
	public void confirmPayment(){
		Savepoint sp;

		try{
			sp = Database.setSavepoint();

			String country = ApexPages.currentPage().getParameters().get('country');
			String agencyId = ApexPages.currentPage().getParameters().get('agId');
			String invoiceId = ApexPages.currentPage().getParameters().get('invId');
			
			for(countryCourses rs : result)
				if(rs.countryName == country){
					for(agencyInvoices agInv : rs.allAgencies){
						if(agInv.agencyId == agencyId){
							for(Invoice inv : agInv.invoices){
								if(inv.invoice.id == invoiceId){ //Confirm Invoice

									String payDocs = [SELECT Id, Payment_Receipt__r.preview_link__c FROM Invoice__c WHERE id = :inv.invoice.id limit 1].Payment_Receipt__r.preview_link__c;

									if(payDocs!=null && payDocs.length()>0){
										inv.showError = false;		
										ff.payShareCommInv(new list<Invoice__c>{inv.invoice}, true);
										findInvoices();
										break;
									}
									else{
										inv.showError = true;
										break;
									}
									break;
								}
								else continue;
							}//end invoice loop
						}//agency
						else continue;
					}//end for
				}
		}
		catch(Exception e){
			Database.rollback(sp);
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage() + ' => ' + e.getLineNumber());
			ApexPages.addMessage(myMsg);
		}
		finally{
			ApexPages.currentPage().getParameters().remove('agId');
			ApexPages.currentPage().getParameters().remove('invId');
		}
	}


	//R E C A L C U L A T E 		R A T E
	public void recalculateRate(){

		try{
			String country = ApexPages.currentPage().getParameters().remove('country');
			String agencyId = ApexPages.currentPage().getParameters().remove('agencyId');
			String invId = ApexPages.currentPage().getParameters().remove('invId');

			for(countryCourses rs : result)
				if(rs.countryName == country){
					for(agencyInvoices agInv : rs.allAgencies){
						if(agInv.agencyId == agencyId){
							for(Invoice inv : agInv.invoices){
								if(inv.invoice.id == invId){

									ff.convertCurrency(inv.invoice, inv.invoice.CurrencyIsoCode__c, inv.invoice.Total_Value__c, null, 'Agency_Currency__c', 'Agency_Currency_Rate__c', 'Agency_Currency_Value__c');
									break;
								}
								else continue;
							}//end invoice loop
						}//agency
						else continue;
					}//end for
				}
		}
		catch(Exception e){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage() + ' => ' + e.getLineNumber());
			ApexPages.addMessage(myMsg);
		}
		finally{
			ApexPages.currentPage().getParameters().remove('country');
			ApexPages.currentPage().getParameters().remove('agencyId');
			ApexPages.currentPage().getParameters().remove('invId');
		}

	}

	//*************************  FILTERS *************************

	private User currentUser {get{if(currentUser==null) currentUser = ff.currentUser; return currentUser;}set;}

	public String selectedAgency {get;set;}
	public String selectedAgencyGroup {get;set;}
	public List<SelectOption> agencyGroupOptions {get;set;}

	public void getAgencyGroupOptions(){
		if(agencyGroupOptions == null){
			selectedAgency = 'none';
			selectedAgencyGroup = null;

			agencyGroupOptions = new List<SelectOption>();
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group
			agencyGroupOptions.add(new SelectOption(currentUser.Contact.Account.ParentId, currentUser.Contact.Account.Parent.Name));
			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
			}else{
				for(AggregateResult ag : sns.NSAggregate('SELECT Requested_Commission_to_Agency_Group__c, Requested_Commission_to_Agency_Group__r.Name agName FROM Invoice__c WHERE isShare_Commission_Invoice__c = TRUE AND Paid_On__c = NULL AND Invoice__c != Null AND isCancelled__c = FALSE group by Requested_Commission_to_Agency_Group__c, Requested_Commission_to_Agency_Group__r.Name order by Requested_Commission_to_Agency_Group__r.Name ')){
   			agencyGroupOptions.add(new SelectOption(String.valueOf(ag.get('Requested_Commission_to_Agency_Group__c')), String.valueOf(ag.get('agName'))));

				if(selectedAgencyGroup==null)
					selectedAgencyGroup = String.valueOf(ag.get('Requested_Commission_to_Agency_Group__c'));

				if(String.valueOf(ag.get('Requested_Commission_to_Agency_Group__c')) == currentUser.Contact.Account.ParentId)
					selectedAgencyGroup = String.valueOf(ag.get('Requested_Commission_to_Agency_Group__c'));
  		}//end for
			}
		}
	}

	public list<SelectOption> agencies{get; set;}
    public void retrieveAgencies(){
        agencies = new List<SelectOption>();
        if(selectedAgencyGroup != null){
            if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency

                selectedAgency = currentUser.Contact.AccountId;
                gbLink = currentUser.Contact.Account.Global_Link__c;
                agencies.add(new SelectOption(currentUser.Contact.AccountId, currentUser.Contact.Account.Name));
                mapgbLink.put(currentUser.Contact.AccountId, currentUser.Contact.Account.Global_Link__c);

				if(currentUser.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
						agencies.add(new SelectOption(ac.id, ac.name));
						mapgbLink.put(ac.Id, ac.Account_Currency_Iso_Code__c);
					}
				}

            }else{
                for(AggregateResult ag: sns.NSAggregate('SELECT Requested_Commission_To_Agency__c, Requested_Commission_To_Agency__r.Name agName, Requested_Commission_To_Agency__r.Global_Link__c agLink FROM Invoice__c where Requested_Commission_to_Agency_Group__c = \'' + selectedAgencyGroup + '\' AND isShare_Commission_Invoice__c = TRUE AND Paid_On__c = NULL AND Invoice__c != Null AND isCancelled__c = FALSE group by Requested_Commission_To_Agency__c, Requested_Commission_To_Agency__r.Name, Requested_Commission_To_Agency__r.Global_Link__c order by Requested_Commission_To_Agency__r.Name')){
                    agencies.add(new SelectOption(String.valueOf(ag.get('Requested_Commission_To_Agency__c')), String.valueOf(ag.get('agName'))));
                    mapgbLink.put(String.valueOf(ag.get('Requested_Commission_To_Agency__c')), String.valueOf(ag.get('agLink')));

                    if(selectedAgency=='none'){
                        selectedAgency = String.valueOf(ag.get('Requested_Commission_To_Agency__c'));
                        gbLink = String.valueOf(ag.get('agLink'));
                    }

                    if(String.valueOf(ag.get('Requested_Commission_To_Agency__c')) == currentUser.Contact.AccountId){
                        selectedAgency = String.valueOf(ag.get('Requested_Commission_To_Agency__c'));
                        gbLink = String.valueOf(ag.get('agLink'));
                    }
                }
            }//end for
            if(selectedAgency=='none'){
                selectedAgency = currentUser.Contact.AccountId;
                gbLink = currentUser.Contact.Account.Global_Link__c;
            }
            findInvoices();
        }
 
    }

	public void changeGroup(){
		selectedAgency = 'none';
		retrieveAgencies();
	}

	//************************* INNER CLASSES *************************
	public class countryCourses{
		public String countryName {get;set;}
		public String countryCurrency {get;set;}
		public list<agencyInvoices> allAgencies {get;set;}
		private map<String, agencyInvoices> mapAgencies {get;set;}

		public countryCourses (String agName, agencyInvoices ag, String countryName, String countryCurrency){
			mapAgencies = new map<String, agencyInvoices>();
			this.mapAgencies.put(agName, ag);
			this.countryName = countryName;
			this.countryCurrency = countryCurrency;
		}

		public boolean hasAgency(String agName){
			if(!mapAgencies.containsKey(agName))
				return false;
			else
				return true;
		}

		public void addAgency (String agName, agencyInvoices ag){
				mapAgencies.put(agName, ag);
		}

		public void addInvoice (String agName, Invoice i){
				mapAgencies.get(agName).addInvoice(i);
		}

		public void orderAgencies(){
			list<String> names = new list<String>();
			names.addAll(mapAgencies.keySet());
			names.sort();

			this.allAgencies = new list<agencyInvoices>();

			for(String nm : names)
				this.allAgencies.add(mapAgencies.get(nm));
		}
	}

	public class agencyInvoices{
		public String agencyId {get;set;}
		public String agencyName {get;set;}
		public list<Invoice> invoices {get{if(invoices==null) invoices= new list<Invoice>(); return invoices;}set;}
		public decimal totalAmount {get{if(totalAmount==null) totalAmount= 0; return totalAmount;}set;}

		public agencyInvoices (String agencyId, String agencyName, Invoice i){
				this.agencyId = agencyId;
				this.agencyName = agencyName;
				this.invoices.add(i);
				this.totalAmount = i.invoice.Total_Value__c;
		}

		public void addInvoice(Invoice i){
				this.invoices.add(i);
				this.totalAmount += i.invoice.Total_Value__c;
		}
	}

	public class Invoice{
		public Invoice__c invoice {get;set;}
		public list<SelectOption> options {get;set;}
		public boolean showError {get{if(showError == null) showError = false; return showError;}set;}
	}

}