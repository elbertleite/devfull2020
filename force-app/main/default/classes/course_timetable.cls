public with sharing class course_timetable{

	String accoId;
	public course_timetable(ApexPages.StandardController controller){
		accoId = controller.getId();
	}
	
	public List<Campus_Course__c> ListNewTimeTable;
    public List<Campus_Course__c> getListNewTimeTable(){
        if(ListNewTimeTable == null){
            ListNewTimeTable = [Select CreatedBy.Name, CreatedDate, LastModifiedBy.Name, LastModifiedDate, id, course__r.Name, Time_Table__c,
                        Course__r.Only_Sold_in_Blocks__c, Course__c, Course__r.Important_information__c, Course__r.Instalment_Interval__c, Course__r.Instalment_Option__c, Course__r.Payment_Rules__c
                        from Campus_Course__c where campus__c = :accoId order by course__r.Name];
        }
        return  ListNewTimeTable;
    }
	
	    
    public boolean editMode {
        get{
        if (editMode == null)
            editMode = false;
        return editMode;
    }
        set;
	}
	
	  public void setEditMode(){
        editMode = !editMode;
    }
	  
	 public pageReference saveCourseInfo(){
        Savepoint sp = Database.setSavepoint();
        try{
            List<Campus_Course__c> campuscourseToUpdate = new List<Campus_Course__c>();
            for(Campus_Course__c cc:[Select id, Time_Table__c from Campus_Course__c where campus__c = :accoId order by course__r.Name])
                for(Campus_Course__c modTimeTable:ListNewTimeTable)
                    if(cc.Id == modTimeTable.id && cc.Time_Table__c != modTimeTable.Time_Table__c)
                        campuscourseToUpdate.add(modTimeTable);
            if(campuscourseToUpdate.size() > 0)
                update campuscourseToUpdate;
            editMode = false;
        } catch (Exception e) {
            Database.rollback(sp);
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(msg);
        } 
        return null;
    }
	 
	public pageReference cancel(){
        editMode = false;
        return null;
    }

}