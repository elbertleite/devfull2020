public class emailTemplateDynamic {
	
	// public class Object{
	// 	public string fddName{get; set;}
	// 	public string fdType{get; set;}
	// 	public Object(string fddName, string fdType){
	// 		this.fddName = fddName;
	// 		this.fdType = fdType;
	// 	}
	// }

	
	
	public static list<sObject> createListResult(string sqlWhere, map<string, Object> fieldsOpt, string htmlAsString, list<string> fieldsTemplate){
		try{
			set<String> fieldsSql = new set<String>();
			map<String, Object> dt;
			for(string lt:fieldsTemplate){
				dt = (map<String, Object>)fieldsOpt.get(lt);	
				fieldsSql.add((String)dt.get('fddName'));
			}//end for
			string sql = 'Select '+ joinList(fieldsSql,', ') + sqlWhere;
			System.debug('==> sql: '+sql);
			return Database.query(sql);
		}catch(Exception e){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No results found.' + e.getLineNumber());
            ApexPages.addMessage(myMsg);
			ApexPages.addMessages(e);
			return null;
		}
	}


	public static map<string,string> createTemplate(map<string, Object> fieldsOpt, string htmlAsString,  list<string> fieldsTemplate, list<sObject> listResult){
		if(htmlAsString == null){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a Template.');
            ApexPages.addMessage(myMsg);
			return null;
		}else{
			try{

				map<string,string> listFieldsTemplate = new map<string,string>();
				//string itemTemplate;


				for(sObject c:listResult){
					listFieldsTemplate.put(c.id, openTemplate(c, fieldsOpt, htmlAsString, fieldsTemplate)); 
				}
				return listFieldsTemplate;
			}catch(Exception e){
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Invalid File.' + e.getLineNumber());
				ApexPages.addMessage(myMsg);
				ApexPages.addMessages(e);
			}
			return null;
		}
	}


	public static String createSingleTemplate(map<string, Object> fieldsOpt, string htmlAsString,  list<string> fieldsTemplate, sObject listResult){
		try{
			return openTemplate(listResult, fieldsOpt, htmlAsString, fieldsTemplate);
		}catch(Exception e){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error ==> ' + e.getLineNumber());
				ApexPages.addMessage(myMsg);
			return null;
		}
	}


	private static string openTemplate(sObject c, map<string, Object> fieldsOpt, string htmlAsString, list<string> fieldsTemplate){
		string itemTemplate = htmlAsString;
		map<String, Object> dt;
		for(string fd:fieldsTemplate){
			Object theField;
			dt = (map<String, Object>)fieldsOpt.get(fd);
			String fieldName = (String) dt.get('fddName');
			System.debug('==> fieldName: '+fieldName);
			
			try{
				if(fieldName.contains('.')){
					list<string> fieldDetails = fieldName.replace('.',';').split(';');
					if(fieldDetails.size() == 2)
						theField = c.getSObject(fieldDetails[0]).get(fieldDetails[1]);
					else if(fieldDetails.size() == 3)
						theField = c.getSObject(fieldDetails[0]).getSObject(fieldDetails[1]).get(fieldDetails[2]);
					else if(fieldDetails.size() == 4)
						theField = c.getSObject(fieldDetails[0]).getSObject(fieldDetails[1]).getSObject(fieldDetails[2]).get(fieldDetails[3]);
					else if(fieldDetails.size() == 5)
						theField = c.getSObject(fieldDetails[0]).getSObject(fieldDetails[1]).getSObject(fieldDetails[2]).getSObject(fieldDetails[3]).get(fieldDetails[4]);
					else if(fieldDetails.size() == 6)
						theField = c.getSObject(fieldDetails[0]).getSObject(fieldDetails[1]).getSObject(fieldDetails[2]).getSObject(fieldDetails[3]).getSObject(fieldDetails[4]).get(fieldDetails[5]);
				}
				else 
				theField = c.get(fieldName);
			}catch(Exception e){
				theField = null;
			}
			
			
			System.debug('==> theField: '+theField);
			if(theField != null){
				object obj;
				obj = convertFieldType((String)dt.get('fdType'), theField);
				itemTemplate = itemTemplate.replace('{#'+fd+'#}', (String)obj);
			}else{
				itemTemplate = itemTemplate.replace('{#'+fd+'#}', '');
			}
		}
		return itemTemplate;
	}

	// Join an Apex list of fields into a SELECT fields list string
    private static String joinList(set<String> theList, String separator) {

        if (theList == null) {
            return null;
        }
        if (separator == null) {
            separator = '';
        }

        String joined = '';
        Boolean firstItem = true;
        for (String item : theList) {
            if(null != item) {
                if(firstItem){
                    firstItem = false;
                }
                else {
                    joined += separator;               
                }
                joined += item;
            }
        }
        return joined;
    }

	public static list<string> extractFieldsFromFile(string fl, map<string, Object> mp){ 
		list<string> listFields = new list<string>();
		for(string st:mp.keySet()){
			//if(fl.contains('{#'+st+'#}'))
				listFields.add(st);
		}
		return listFields;
	}

	private static Object convertFieldType(string fielddataType, Object theField){
	 	if(fielddataType == 'string') {
			return string.valueOf(theField);
		} else if(fielddataType == 'datetime') {
			return DateTime.valueOf(theField).format();
		}  else if(fielddataType == 'date') {
			return date.valueOf(theField).format();
		} else {
			return theField;
		}

	}



	
}