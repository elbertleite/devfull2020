public without sharing class amendments {

	IPFunctions.CampusAvailability result = new IPFunctions.CampusAvailability();
    IPFunctions.agencyNoSharing agencyAndGroup = new IPFunctions.agencyNoSharing();

	private User currentUser {get{if(currentUser==null) currentUser = [Select Id, Aditional_Agency_Managment__c, Contact.Account.ParentId, Contact.AccountId from User where id = :UserInfo.getUserId() limit 1]; return currentUser;}set;}

	public class feesDetails{
		public string status{get; set;}
		public string fee{get; set;}
		public decimal fromValue{get; set;}
		public decimal toValue{get; set;}
		public boolean isKeepfee{get; set;}
		public feesDetails(string status,string fee,decimal fromValue,decimal toValue,boolean isKeepfee){
			this.status = status;
			this.fee = fee;
			this.fromValue = fromValue;
			this.toValue = toValue;
			this.isKeepfee = isKeepfee;
		}
	}

	public class tuitionDetails{
		public string status{get; set;}
		public decimal tuitionValue{get; set;}
		public decimal commissionValue{get; set;}
		public decimal taxValue{get; set;}
		// public tuitionDetails(string status,decimal tuitionValue,decimal commissionValue,decimal taxValue){
		// 	this.status = status;
		// 	this.tuitionValue = tuitionValue;
		// 	this.commissionValue = commissionValue;
		// 	this.taxValue = taxValue;
		// }
	}

	public string selectedCountry{get {if(selectedCountry == null) selectedCountry = ''; return selectedCountry; } set;}
    private List<SelectOption> countries;
    public List<SelectOption> getCountries() {
        if(countries == null){
		   countries = IPFunctions.CountryDestinations();
		   if(selectedCountry == null)
		   		selectedCountry = countries[0].getValue();
        }
        return countries;
    }

	private with sharing class SharingList {

		private SharingList(user paramUser){
			currentUser = paramUser;
		}
		user currentUser;
		private List<SelectOption> lisAgencyGroupOptions(string userAgency) {
			List<SelectOption> agencyGroupOptions = new List<SelectOption>();
			agencyGroupOptions.add(new SelectOption('none', '--All Groups--'));
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group
				for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:userAgency order by Parent.Name]){
					agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
				}
			}else{
				for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
					agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
				}
			}
			return agencyGroupOptions;
		}

		private list<SelectOption> listAgencies(string selectedAgencyGroup, string userAgency){
			List<SelectOption> result = new List<SelectOption>();
			if(selectedAgencyGroup != null && selectedAgencyGroup != 'none'){
				result.add(new SelectOption('none', '--All Agencies--'));
				if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
					for(Account a: [Select Id, Name from Account where id =:userAgency order by name]){
						result.add(new SelectOption(a.Id, a.Name));
					}
					if(currentUser.Aditional_Agency_Managment__c != null){
						for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
							result.add(new SelectOption(ac.id, ac.name));
						}
					}
				}else{
					for(Account a: [Select Id, Name from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
						result.add(new SelectOption(a.Id, a.Name));
					}
				}
			}

			return result;
		}
	}

	public string selectedAgencyGroup{
        get{
            if(selectedAgencyGroup == null)
                selectedAgencyGroup = currentUser.Contact.Account.ParentId;
            return selectedAgencyGroup;
        }
        set;
    }
	SharingList la = new SharingList(currentUser);

	private List<SelectOption> agencyGroupOptions;
	public List<SelectOption> getAgencyGroupOptions() {
		if(agencyGroupOptions == null){
			agencyGroupOptions = new List<SelectOption>();
			agencyGroupOptions.addAll(la.lisAgencyGroupOptions(currentUser.Contact.AccountId));
			
		}
		return agencyGroupOptions;
    }

	 public string selectedAgency {get{
        if(selectedAgency==null){
            selectedAgency = currentUser.Contact.AccountId;
        }
        return selectedAgency;
    }set;}

	private List<SelectOption> agencies;
    public list<SelectOption> getAgencies(){
        agencies = new List<SelectOption>();
        if(selectedAgencyGroup != null && selectedAgencyGroup != 'none'){
			agencies.addAll(la.listAgencies(selectedAgencyGroup, currentUser.Contact.AccountId));
        }
        return agencies;
    }

    public void changeGroup(){
        selectedAgency = 'none';
        getAgencies();
    }

	public string selectedSchoolGroup{get {if(selectedSchoolGroup == null) selectedSchoolGroup = 'all'; return selectedSchoolGroup; } set;}
    private List<SelectOption> schoolGroup;
    public List<SelectOption> getschoolGroup() {
        if(schoolGroup == null && selectedCountry != null){
		   schoolGroup = result.getSchoolGroupByCountry(selectedCountry);
        }
        return schoolGroup;
    }
    
    public string selectedSchool{get {if(selectedSchool == null) selectedSchool = 'all'; return selectedSchool; } set;}
    private List<SelectOption> schools;
    public List<SelectOption> getSchools() {
		if(schools == null && selectedCountry != null){
			if(selectedSchoolGroup != null && selectedSchoolGroup != 'all')
			schools = result.getSchoolByCountryGroup(selectedCountry, selectedSchoolGroup);
			else schools = result.getSchoolByCountry(selectedCountry);
        }
        return schools;
    }

	public string selectedCampus{get {if(selectedCampus == null) selectedCampus = 'all'; return selectedCampus; } set;}
    private List<SelectOption> campuses;
    public List<SelectOption> getCampuses() {
        if(campuses == null && selectedSchool != null && selectedSchool != 'all'){
		   campuses = result.getCampusBySchoolNA(selectedSchool);
        }
        return campuses;
    }

	public string SelectedPeriod{get {if(SelectedPeriod == null) SelectedPeriod = 'THIS_WEEK'; return SelectedPeriod; } set;}
    private List<SelectOption> periods;
    public List<SelectOption> getPeriods() {
        if(periods == null){
            periods = new List<SelectOption>();
            periods.add(new SelectOption('THIS_WEEK','This Week'));
            periods.add(new SelectOption('LAST_WEEK','Last Week'));
            periods.add(new SelectOption('THIS_MONTH','This Month'));
            periods.add(new SelectOption('LAST_MONTH','Last Month'));
            periods.add(new SelectOption('range','Range'));
        }
        return periods;
    }

	public string selectedDateType{get {if(selectedDateType == null) selectedDateType = 'created'; return selectedDateType; } set;}
    private List<SelectOption> dateType;
    public List<SelectOption> getDateType() {
        if(dateType == null){
			dateType = new List<SelectOption>();
		   dateType.add(new selectOption('created','Created Date'));
		   dateType.add(new selectOption('confirmed','Confirmed Date'));
        }
        return dateType;
    }

	 public client_course_instalment__c dateFilter{
		get{
			if(dateFilter == null){
				dateFilter = new client_course_instalment__c();
				Date myDate = Date.today();
				Date weekStart = myDate.toStartofWeek();
				dateFilter.Original_Due_Date__c = weekStart;
				dateFilter.Discounted_On__c = dateFilter.Original_Due_Date__c.addDays(6);
			}
			return dateFilter;
		}
		set;
	}

	public void refreshSchoolGroup(){
		schoolGroup = null;
    	refreshSchool(); 
    }

	public void refreshSchool(){
    	schools = null;
    	refreshCampus();
    	//refreshCourseType();
    }

	public void refreshCampus(){
    	campuses = null;
    	//refreshCourseCategory();
    }


	public class isntalmentAmend{
		public string amendedFee{get{if(amendedFee == null) amendedFee = ''; return amendedFee;} set;}
		public string addFee{get{if(addFee == null) addFee = ''; return addFee;} set;}
		public list<feesDetails> updateFee{get{if(updateFee == null) updateFee = new list<feesDetails>(); return updateFee;} set;}
		public list<tuitionDetails> updateTuition{get{if(updateTuition == null) updateTuition = new list<tuitionDetails>(); return updateTuition;} set;}
		// public string updateCommission{get{if(updateCommission == null) updateCommission = ''; return updateCommission;} set;}
		public string updateCommissionValue{get{if(updateCommissionValue == null) updateCommissionValue = ''; return updateCommissionValue;} set;}
		public string updateCommissionTaxValue{get{if(updateCommissionTaxValue == null) updateCommissionTaxValue = ''; return updateCommissionTaxValue;} set;}

		public decimal Charge_Client{get{if(Charge_Client == null) Charge_Client = 0; return Charge_Client;} set;}
		public decimal Pay_to_Client{get{if(Pay_to_Client == null) Pay_to_Client = 0; return Pay_to_Client;} set;}
		public decimal Charge_School{get{if(Charge_School == null) Charge_School = 0; return Charge_School;} set;}
		public decimal Pay_to_School{get{if(Pay_to_School == null) Pay_to_School = 0; return Pay_to_School;} set;}

		public client_course_instalment__c install{get; set;}
	}

	public list<isntalmentAmend> lstCommissionAmendments{get; set;}
	public list<isntalmentAmend> lstInstalmentAmendments{get; set;}
	public list<isntalmentAmend> lstOverPaymentAmendments{get; set;}

	public void searchAmendments() {
		string selectedInstall = ApexPages.currentPage().getParameters().get('id');
		String  fromDate = IPFunctions.FormatSqlDateIni(dateFilter.Original_Due_Date__c);
		String  toDate = IPFunctions.FormatSqlDateFin(dateFilter.Discounted_On__c);
		isntalmentAmend inst;
		lstCommissionAmendments = new list<isntalmentAmend>();
		lstInstalmentAmendments = new list<isntalmentAmend>();
		lstOverPaymentAmendments = new list<isntalmentAmend>();
		string sql = ' SELECT id, Received_By_Agency__r.name, isCommission_Amendment__c, isInstalment_Amendment__c, isOverpayment_Amendment__c, createddate, createdby.name, ';
				sql += ' Amended_Agency_Deal_On__c,Amended_Commission_On__c,Amended_Fees_On__c,Amended_Keep_Fee_On__c,Amended_Scholarship_On__c,Amended_Total_Fees__c, ';
				sql += ' Amended_Total_Keep_Fees__c,Amended_Tuition_On__c,Amendment_Adjustment_Confirmed_By_Agency__c,Amendment_Adjustment_Confirmed_By__c,Amendment_Adjustment_Confirmed_On__c, ';
				sql += ' Amendment_Agency_Deal__c,Amendment_Commission_Balance__c,Amendment_Commission__c,Amendment_Fees__c,Amendment_Fee_Balance__c,Amendment_Instalment_Balance__c,Amendment_Keep_Fee_Balance__c, ';
				sql += ' Amendment_Keep_Fee__c,Amendment_Pay_To_School_Balance__c,Amendment_PFS_Adjustment__c,Amendment_Receive_From_School__c,Amendment_Related_Fees__c,Amendment_Scholarship__c,Amendment_Tax_Balance__c, ';
				sql += ' Amendment_Total_Charge_Client__c,Amendment_Tuition_Balance__c,Amendment_Tuition__c,client_course__c, Net_Paid_To_School__c, Related_Fees__c, ';
				sql += ' Amendment_Adjustment_Confirmed_By_Agency__r.name, Commission_Confirmed_By_Agency__r.name, ';
				sql += ' client_course__r.client__r.name, client_course__r.campus_name__c, client_course__r.Course_Name__c, client_course__r.Commission_Type__c, '; 
				sql += ' Commission_Paid_Date__c,Commission_Tax_Adjustment__c, Commission_Adjustment__c,Commission_Tax_Value__c,Commission_Value__c,Commission__c, Total_School_Payment__c, ';
				sql += ' original_instalment__r.Commission_Tax_Value__c, original_instalment__r.Commission_Value__c, original_instalment__r.Tuition_Value__c, original_instalment__r.Commission__c, '; 
				sql += ' original_instalment__r.Extra_Fee_Value__c, original_instalment__r.Instalment_Value__c, original_instalment__r.Net_Paid_To_School__c, original_instalment__r.isCommission_charged_on_amendment__c, '; 
				sql += ' Confirmed_By__c,Confirmed_Date__c,Due_Date__c,Extra_Fee_Value__c,Instalment_Value__c,isPCS__c,isPDS__c,isPFS__c,Number__c,Paid_to_School_By_Agency__c, ';
				sql += ' Paid_To_School_By__c,Paid_to_School_Date__c,Paid_To_School_On__c,Received_By_Agency__c,Received_by__c,Received_Date__c,School_Invoice_Number__c,Split_Number__c,Tuition_Value__c ';
				sql += ' FROM client_course_instalment__c where original_instalment__c != null ';
				
				if(selectedInstall != null && selectedInstall != ''){
					sql += ' and id =:selectedInstall ';
				}else{
					if(selectedSchool != 'all')
						sql += ' and client_course__r.campus_course__r.campus__r.parentId = :selectedSchool and ';
					else if(selectedSchoolGroup != 'all')
						sql += ' and client_course__r.campus_course__r.campus__r.parent.parentId = :selectedSchoolGroup and ';
					else sql += ' and client_course__r.campus_course__r.campus__r.BillingCountry = :selectedCountry and ';

					if(selectedDateType == 'created'){
						if(SelectedPeriod == 'range')
							sql += ' DAY_ONLY(convertTimezone(CreatedDate)) >= '+fromDate+' and DAY_ONLY(convertTimezone(CreatedDate)) <= '+toDate;
						else if(SelectedPeriod == 'THIS_WEEK')
							sql += ' CreatedDate = THIS_WEEK ';
						else if(SelectedPeriod == 'LAST_WEEK')
							sql += ' CreatedDate = LAST_WEEK ';
						else if(SelectedPeriod == 'THIS_MONTH')
							sql += ' CreatedDate = THIS_MONTH ';
						else if(SelectedPeriod == 'LAST_MONTH')
							sql += ' CreatedDate = LAST_MONTH ';
					}else{
						if(SelectedPeriod == 'range')
							sql += ' DAY_ONLY(convertTimezone(Amendment_Adjustment_Confirmed_On__c)) >= '+fromDate+' and DAY_ONLY(convertTimezone(Amendment_Adjustment_Confirmed_On__c)) <= '+toDate;
						else if(SelectedPeriod == 'THIS_WEEK')
							sql += ' Amendment_Adjustment_Confirmed_On__c = THIS_WEEK ';
						else if(SelectedPeriod == 'LAST_WEEK')
							sql += ' Amendment_Adjustment_Confirmed_On__c = LAST_WEEK ';
						else if(SelectedPeriod == 'THIS_MONTH')
							sql += ' Amendment_Adjustment_Confirmed_On__c = THIS_MONTH ';
						else if(SelectedPeriod == 'LAST_MONTH')
							sql += ' Amendment_Adjustment_Confirmed_On__c = LAST_MONTH ';

					}
					sql += ' order by client_course__r.campus_name__c, Received_Date__c';
				}



		 for(client_course_instalment__c ci:database.query(sql)){
			inst = new isntalmentAmend();
			if(ci.isCommission_Amendment__c){
				if(ci.Related_Fees__c != null)
					for(string st:ci.Related_Fees__c.split('!#')){
						if(st.split(';;').size() > 6 &&  st.split(';;')[7] != null && st.split(';;')[7] != '0'){
							if(decimal.valueOf(st.split(';;')[7]) < 0)
								inst.updateFee.add(new feesDetails('Subtract Keep fee',st.split(';;')[1],null,decimal.valueOf(st.split(';;')[7]),true)); //+= '<tr><td>'+st.split(';;')[1] + '</td><td>Subtract Keep fee</td><td>-</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[7])) +'</td></tr>';
							else inst.updateFee.add(new feesDetails('Add Keep fee',st.split(';;')[1],null,decimal.valueOf(st.split(';;')[7]),true)); // += '<tr><td>'+st.split(';;')[1] + '</td><td>Add Keep fee</td><td>-</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[7])) +'</td></tr>';
							// if(decimal.valueOf(st.split(';;')[7]) < 0)
							// 	inst.amendedFee += '<span style="color:red;font-weight: bold;">Subtract Keep fee:</span> '+st.split(';;')[1] + ' - '+st.split(';;')[7]+'<br>';
							// else inst.amendedFee += '<span style="color:red;font-weight: bold;">Add Keep fee:</span> '+st.split(';;')[1] + ' - '+st.split(';;')[7]+'<br>';
						}
						if(st.split(';;').size() > 6 && st.split(';;')[4] !=  st.split(';;')[6] && st.split(';;')[6] != '0'){
							inst.updateFee.add(new feesDetails('Updated',st.split(';;')[1],decimal.valueOf(st.split(';;')[4]),decimal.valueOf(st.split(';;')[4]) + decimal.valueOf(st.split(';;')[6]),false));// += '<tr><td>'+st.split(';;')[1] + '</td><td>Updated</td><td>'+st.split(';;')[4] + '</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[4]) + decimal.valueOf(st.split(';;')[6])) +'</td></tr>';
							// if(decimal.valueOf(st.split(';;')[6]) > 0)
							// 	inst.updateFee += '<span style="color:blue;font-weight: bold;">Add Fee Value:</span><br> '+st.split(';;')[1] + ' <span style="color:blue;font-weight: bold;">from: </span> '+st.split(';;')[4] + ' <span style="color:blue;font-weight: bold;">to: </span> '+ string.valueOf(decimal.valueOf(st.split(';;')[4]) + decimal.valueOf(st.split(';;')[6])) +'<br>';
							// else inst.updateFee += '<span style="color:blue;font-weight: bold;">Subtract Fee Value</span><br> '+st.split(';;')[1] + ' <span style="color:blue;font-weight: bold;">from: </span> '+st.split(';;')[4] + ' <span style="color:blue;font-weight: bold;">to: </span> '+ string.valueOf(decimal.valueOf(st.split(';;')[4]) + decimal.valueOf(st.split(';;')[6])) +'<br>';
						}
					}
				if(ci.Amendment_Related_Fees__c != null)
					for(string st:ci.Amendment_Related_Fees__c.split('!#')){
						if(st.split(';;').size() > 4)
							inst.updateFee.add(new feesDetails('Added',st.split(';;')[1],null,decimal.valueOf(st.split(';;')[4]),false)); //inst.updateFee += '<tr><td>'+st.split(';;')[1] + '</td><td>Added</td><td>-</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[4])) +'</td></tr>';
						if(st.split(';;').size() > 4 &&  st.split(';;')[5] != null && st.split(';;')[5] != '0'){
							if(decimal.valueOf(st.split(';;')[5]) < 0)
								inst.updateFee.add(new feesDetails('Subtract Keep fee',st.split(';;')[1],null,decimal.valueOf(st.split(';;')[7]),true)); //+= '<tr><td>'+st.split(';;')[1] + '</td><td>Subtract Keep fee</td><td>-</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[7])) +'</td></tr>';
							else inst.updateFee.add(new feesDetails('Add Keep fee',st.split(';;')[1],null,decimal.valueOf(st.split(';;')[7]),true)); // += '<tr><td>'+st.split(';;')[1] + '</td><td>Add Keep fee</td><td>-</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[7])) +'</td></tr>';
								
							
						}
					}
				
				tuitionDetails td;
				//Check tuition updates
				if(ci.Tuition_Value__c != 0 && ci.Tuition_Value__c != ci.original_instalment__r.Tuition_Value__c){
					td = new tuitionDetails();
					if(ci.Tuition_Value__c > 0)
						td.status = 'Tuition added';
					else
						td.status = 'Tuition subtracted';

					td.tuitionValue = ci.Tuition_Value__c;
					
					
				}


				//Check Commission Value updates
				if(ci.Commission_Value__c != 0 && ci.Commission_Value__c != ci.original_instalment__r.Commission_Value__c){
					if(td == null){
						td = new tuitionDetails();
						if(ci.Commission_Value__c > 0)
							td.status = 'Commission added';
						else
							td.status = 'Commission subtracted';
					}
					td.commissionValue = ci.Commission_Value__c;
					
				}

				//Check Commission Tax Value updates
				if(ci.Commission_Tax_Value__c != 0 && ci.Commission_Tax_Value__c != ci.original_instalment__r.Commission_Tax_Value__c){
					if(td == null){
						td = new tuitionDetails();
						if(ci.Commission_Tax_Value__c > 0)
							td.status = 'Tax added';
						else
							td.status = 'Tax subtracted';
					}
					td.taxValue = ci.Commission_Tax_Value__c;
					
				}

				if(td != null)
					inst.updateTuition.add(td);

				//Check Commission updates
				if(ci.Commission__c != 0 && ci.Commission__c != ci.original_instalment__r.Commission__c){
					//if(ci.Commission__c > 0)
						// inst.updateCommission += '<span style="color:purple;font-weight: bold;">Commission changed from: </span> '+ci.original_instalment__r.Commission__c+ ' <span style="color:blue;font-weight: bold;">to: </span> '+ci.Commission__c;
					//else
						//inst.updateCommission += '<span style="color:purple;font-weight: bold;">Commission subtracted</span> '+ci.Commission__c;
					
					td = new tuitionDetails();
					string tp = ci.client_course__r.Commission_Type__c == 0 ? '%':'$';
					if(ci.Commission__c > ci.original_instalment__r.Commission__c)
						td.status = 'Commission incremented ('+string.valueOf(ci.Commission__c - ci.original_instalment__r.Commission__c)+')' + tp;
					else
						td.status = 'Commission decremented ('+string.valueOf(ci.Commission__c - ci.original_instalment__r.Commission__c)+')' + tp;
					td.tuitionValue = ci.original_instalment__r.Tuition_Value__c;
					td.commissionValue = ci.Commission_Adjustment__c;
					td.taxValue = ci.Commission_Tax_Adjustment__c; 
					inst.updateTuition.add(td);
				}
				
					
				if(ci.Amendment_Total_Charge_Client__c != null && ci.Amendment_Total_Charge_Client__c > 0){
					inst.Charge_Client = ci.Amendment_Total_Charge_Client__c;
				}
				if(ci.Amendment_Total_Charge_Client__c != null && ci.Amendment_Total_Charge_Client__c < 0){
					inst.Pay_to_Client = ci.Amendment_Total_Charge_Client__c;
				}
				if(ci.Amendment_Receive_From_School__c != null && ci.Amendment_Receive_From_School__c > 0 && ci.Amendment_PFS_Adjustment__c == 0){ 
					inst.Charge_School = ci.Amendment_Receive_From_School__c;
				}

				inst.Pay_to_School = ci.Total_School_Payment__c;
				
				
				inst.install = ci;
				lstCommissionAmendments.add(inst);
			}
			else if(ci.isInstalment_Amendment__c){
				if(ci.Related_Fees__c != null)
					for(string st:ci.Related_Fees__c.split('!#')){
						if(st.split(';;').size() > 6 &&  st.split(';;')[7] != null && st.split(';;')[7] != '0'){
							if(decimal.valueOf(st.split(';;')[7]) < 0)
								inst.updateFee.add(new feesDetails('Subtract Keep fee',st.split(';;')[1],null,decimal.valueOf(st.split(';;')[7]),true)); //+= '<tr><td>'+st.split(';;')[1] + '</td><td>Subtract Keep fee</td><td>-</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[7])) +'</td></tr>';
							else inst.updateFee.add(new feesDetails('Add Keep fee',st.split(';;')[1],null,decimal.valueOf(st.split(';;')[7]),true)); // += '<tr><td>'+st.split(';;')[1] + '</td><td>Add Keep fee</td><td>-</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[7])) +'</td></tr>';
							// if(decimal.valueOf(st.split(';;')[7]) < 0)
							// 	inst.amendedFee += '<span style="color:red;font-weight: bold;">Subtract Keep fee:</span> '+st.split(';;')[1] + ' - '+st.split(';;')[7]+'<br>';
							// else inst.amendedFee += '<span style="color:red;font-weight: bold;">Add Keep fee:</span> '+st.split(';;')[1] + ' - '+st.split(';;')[7]+'<br>';
						}
						if(st.split(';;').size() > 6 && st.split(';;')[4] !=  st.split(';;')[6] && st.split(';;')[6] != '0'){
							inst.updateFee.add(new feesDetails('Updated',st.split(';;')[1],decimal.valueOf(st.split(';;')[4]),decimal.valueOf(st.split(';;')[4]) + decimal.valueOf(st.split(';;')[6]),false));// += '<tr><td>'+st.split(';;')[1] + '</td><td>Updated</td><td>'+st.split(';;')[4] + '</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[4]) + decimal.valueOf(st.split(';;')[6])) +'</td></tr>';
							// if(decimal.valueOf(st.split(';;')[6]) > 0)
							// 	inst.updateFee += '<span style="color:blue;font-weight: bold;">Add Fee Value:</span><br> '+st.split(';;')[1] + ' <span style="color:blue;font-weight: bold;">from: </span> '+st.split(';;')[4] + ' <span style="color:blue;font-weight: bold;">to: </span> '+ string.valueOf(decimal.valueOf(st.split(';;')[4]) + decimal.valueOf(st.split(';;')[6])) +'<br>';
							// else inst.updateFee += '<span style="color:blue;font-weight: bold;">Subtract Fee Value</span><br> '+st.split(';;')[1] + ' <span style="color:blue;font-weight: bold;">from: </span> '+st.split(';;')[4] + ' <span style="color:blue;font-weight: bold;">to: </span> '+ string.valueOf(decimal.valueOf(st.split(';;')[4]) + decimal.valueOf(st.split(';;')[6])) +'<br>';
						}
					}
				if(ci.Amendment_Related_Fees__c != null)
					for(string st:ci.Amendment_Related_Fees__c.split('!#')){
						if(st.split(';;').size() > 4)
							inst.updateFee.add(new feesDetails('Added',st.split(';;')[1],null,decimal.valueOf(st.split(';;')[4]),false)); //inst.updateFee += '<tr><td>'+st.split(';;')[1] + '</td><td>Added</td><td>-</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[4])) +'</td></tr>';
							// inst.addFee += '<span style="color:green;font-weight: bold;">Fee Added: </span> '+st.split(';;')[1]  + ' - '+st.split(';;')[4] + '<br>';
						if(st.split(';;').size() > 4 &&  st.split(';;')[5] != null && st.split(';;')[5] != '0'){
							if(decimal.valueOf(st.split(';;')[5]) < 0)
								inst.updateFee.add(new feesDetails('Subtract Keep fee',st.split(';;')[1],null,decimal.valueOf(st.split(';;')[5]),true)); //+= '<tr><td>'+st.split(';;')[1] + '</td><td>Subtract Keep fee</td><td>-</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[7])) +'</td></tr>';
							else inst.updateFee.add(new feesDetails('Add Keep fee',st.split(';;')[1],null,decimal.valueOf(st.split(';;')[5]),true)); // += '<tr><td>'+st.split(';;')[1] + '</td><td>Add Keep fee</td><td>-</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[7])) +'</td></tr>';
								
								//inst.updateFee += '<tr><td>'+st.split(';;')[1] + '</td><td>Subtract Keep fee</td><td>-</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[5])) +'</td></tr>';
							//else inst.updateFee += '<tr><td>'+st.split(';;')[1] + '</td><td>Add Keep fee</td><td>-</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[5])) +'</td></tr>';

							// if(decimal.valueOf(st.split(';;')[5]) < 0)
							// 	inst.amendedFee += '<span style="color:red;font-weight: bold;">Subtract Keep fee: </span> '+st.split(';;')[1] + ' - '+st.split(';;')[5]+'<br>';
							// else inst.amendedFee += '<span style="color:red;font-weight: bold;">Add Keep fee :</span> '+st.split(';;')[1] + ' - '+st.split(';;')[5]+'<br>';
						}
					}

				tuitionDetails td;
				//Check tuition updates
				if(ci.Tuition_Value__c != 0 && ci.Tuition_Value__c != ci.original_instalment__r.Tuition_Value__c){
					td = new tuitionDetails();
					if(ci.Tuition_Value__c > 0)
						td.status = 'Tuition added';
					else
						td.status = 'Tuition subtracted';

					td.tuitionValue = ci.Tuition_Value__c;
					
					
				}


				//Check Commission Value updates
				if(ci.Commission_Value__c != 0 && ci.Commission_Value__c != ci.original_instalment__r.Commission_Value__c){
					if(td == null){
						td = new tuitionDetails();
						if(ci.Commission_Value__c > 0)
							td.status = 'Commission added';
						else
							td.status = 'Commission subtracted';
					}
					td.commissionValue = ci.Commission_Value__c;
				}

				//Check Commission Tax Value updates
				if(ci.Commission_Tax_Value__c != 0 && ci.Commission_Tax_Value__c != ci.original_instalment__r.Commission_Tax_Value__c){
					if(td == null){
						td = new tuitionDetails();
						if(ci.Commission_Tax_Value__c > 0)
							td.status = 'Tax added';
						else
							td.status = 'Tax subtracted';
					}
					td.taxValue = ci.Commission_Tax_Value__c;
				}

				if(td != null)
					inst.updateTuition.add(td);

				//Check Commission updates
				if(ci.Commission__c != 0 && ci.Commission__c != ci.original_instalment__r.Commission__c){
					//if(ci.Commission__c > 0)
						// inst.updateCommission += '<span style="color:purple;font-weight: bold;">Commission changed from: </span> '+ci.original_instalment__r.Commission__c+ ' <span style="color:blue;font-weight: bold;">to: </span> '+ci.Commission__c;
					//else
						//inst.updateCommission += '<span style="color:purple;font-weight: bold;">Commission subtracted</span> '+ci.Commission__c;
					
					td = new tuitionDetails();
					string tp = ci.client_course__r.Commission_Type__c == 0 ? '%':'$';
					if(ci.Commission__c > ci.original_instalment__r.Commission__c)
						td.status = 'Commission incremented ('+string.valueOf(ci.Commission__c - ci.original_instalment__r.Commission__c)+')' + tp;
					else
						td.status = 'Commission decremented ('+string.valueOf(ci.Commission__c - ci.original_instalment__r.Commission__c)+')' + tp;
					td.tuitionValue = ci.original_instalment__r.Tuition_Value__c;
					td.commissionValue = ci.Commission_Adjustment__c;
					td.taxValue = ci.Commission_Tax_Adjustment__c; 
					inst.updateTuition.add(td);
				}
				
				if(ci.Amendment_Total_Charge_Client__c != null && ci.Amendment_Total_Charge_Client__c > 0){
					inst.Charge_Client = ci.Amendment_Total_Charge_Client__c;
				}
				if(ci.Amendment_Total_Charge_Client__c != null && ci.Amendment_Total_Charge_Client__c < 0){
					inst.Pay_to_Client = ci.Amendment_Total_Charge_Client__c;
				}
				if(ci.Amendment_Receive_From_School__c != null && ci.Amendment_Receive_From_School__c > 0 && ci.Amendment_PFS_Adjustment__c == 0){ 
					inst.Charge_School = ci.Amendment_Receive_From_School__c;
				}
				
				inst.Pay_to_School = ci.Total_School_Payment__c;
				
				// if(ci.Amendment_Pay_To_School_Balance__c != null && ci.Amendment_Pay_To_School_Balance__c > 0){
				// 	inst.Pay_to_School = ci.Amendment_Pay_To_School_Balance__c;
				// }

				// if(ci.Amendment_Keep_Fee_Balance__c != null && ci.Amendment_Keep_Fee_Balance__c >  0)
				// 	inst.Pay_to_School += ci.Amendment_Keep_Fee_Balance__c;

				// if(ci.Amendment_PFS_Adjustment__c != null && ci.Amendment_PFS_Adjustment__c > 0){ 
				// 	inst.Pay_to_School += ci.Amendment_PFS_Adjustment__c;
				// }
				
				inst.install = ci;
				lstInstalmentAmendments.add(inst);
			}
			else if(ci.isOverpayment_Amendment__c){
				if(ci.Related_Fees__c != null)
					for(string st:ci.Related_Fees__c.split('!#')){
						if(st.split(';;').size() > 6 &&  st.split(';;')[7] != null && st.split(';;')[7] != '0'){
							if(decimal.valueOf(st.split(';;')[7]) < 0)
								inst.updateFee.add(new feesDetails('Subtract Keep fee',st.split(';;')[1],null,decimal.valueOf(st.split(';;')[7]),true)); //+= '<tr><td>'+st.split(';;')[1] + '</td><td>Subtract Keep fee</td><td>-</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[7])) +'</td></tr>';
							else inst.updateFee.add(new feesDetails('Add Keep fee',st.split(';;')[1],null,decimal.valueOf(st.split(';;')[7]),true)); // += '<tr><td>'+st.split(';;')[1] + '</td><td>Add Keep fee</td><td>-</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[7])) +'</td></tr>';
							// if(decimal.valueOf(st.split(';;')[7]) < 0)
							// 	inst.amendedFee += '<span style="color:red;font-weight: bold;">Subtract Keep fee:</span> '+st.split(';;')[1] + ' - '+st.split(';;')[7]+'<br>';
							// else inst.amendedFee += '<span style="color:red;font-weight: bold;">Add Keep fee:</span> '+st.split(';;')[1] + ' - '+st.split(';;')[7]+'<br>';
						}
						if(st.split(';;').size() > 6 && st.split(';;')[4] !=  st.split(';;')[6] && st.split(';;')[6] != '0'){
							inst.updateFee.add(new feesDetails('Updated',st.split(';;')[1],decimal.valueOf(st.split(';;')[4]),decimal.valueOf(st.split(';;')[4]) + decimal.valueOf(st.split(';;')[6]),false));// += '<tr><td>'+st.split(';;')[1] + '</td><td>Updated</td><td>'+st.split(';;')[4] + '</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[4]) + decimal.valueOf(st.split(';;')[6])) +'</td></tr>';
							// if(decimal.valueOf(st.split(';;')[6]) > 0)
							// 	inst.updateFee += '<span style="color:blue;font-weight: bold;">Add Fee Value:</span><br> '+st.split(';;')[1] + ' <span style="color:blue;font-weight: bold;">from: </span> '+st.split(';;')[4] + ' <span style="color:blue;font-weight: bold;">to: </span> '+ string.valueOf(decimal.valueOf(st.split(';;')[4]) + decimal.valueOf(st.split(';;')[6])) +'<br>';
							// else inst.updateFee += '<span style="color:blue;font-weight: bold;">Subtract Fee Value</span><br> '+st.split(';;')[1] + ' <span style="color:blue;font-weight: bold;">from: </span> '+st.split(';;')[4] + ' <span style="color:blue;font-weight: bold;">to: </span> '+ string.valueOf(decimal.valueOf(st.split(';;')[4]) + decimal.valueOf(st.split(';;')[6])) +'<br>';
						}
					}
				if(ci.Amendment_Related_Fees__c != null)
					for(string st:ci.Amendment_Related_Fees__c.split('!#')){
						if(st.split(';;').size() > 4)
							inst.updateFee.add(new feesDetails('Added',st.split(';;')[1],null,decimal.valueOf(st.split(';;')[4]),false)); //inst.updateFee += '<tr><td>'+st.split(';;')[1] + '</td><td>Added</td><td>-</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[4])) +'</td></tr>';
							// inst.addFee += '<span style="color:green;font-weight: bold;">Fee Added: </span> '+st.split(';;')[1]  + ' - '+st.split(';;')[4] + '<br>';
						if(st.split(';;').size() > 4 &&  st.split(';;')[5] != null && st.split(';;')[5] != '0'){
							if(decimal.valueOf(st.split(';;')[5]) < 0)
								inst.updateFee.add(new feesDetails('Subtract Keep fee',st.split(';;')[1],null,decimal.valueOf(st.split(';;')[5]),true)); //+= '<tr><td>'+st.split(';;')[1] + '</td><td>Subtract Keep fee</td><td>-</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[7])) +'</td></tr>';
							else inst.updateFee.add(new feesDetails('Add Keep fee',st.split(';;')[1],null,decimal.valueOf(st.split(';;')[5]),true)); // += '<tr><td>'+st.split(';;')[1] + '</td><td>Add Keep fee</td><td>-</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[7])) +'</td></tr>';
								
								//inst.updateFee += '<tr><td>'+st.split(';;')[1] + '</td><td>Subtract Keep fee</td><td>-</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[5])) +'</td></tr>';
							//else inst.updateFee += '<tr><td>'+st.split(';;')[1] + '</td><td>Add Keep fee</td><td>-</td><td>'+ string.valueOf(decimal.valueOf(st.split(';;')[5])) +'</td></tr>';

							// if(decimal.valueOf(st.split(';;')[5]) < 0)
							// 	inst.amendedFee += '<span style="color:red;font-weight: bold;">Subtract Keep fee: </span> '+st.split(';;')[1] + ' - '+st.split(';;')[5]+'<br>';
							// else inst.amendedFee += '<span style="color:red;font-weight: bold;">Add Keep fee :</span> '+st.split(';;')[1] + ' - '+st.split(';;')[5]+'<br>';
						}
					}
				
				tuitionDetails td;
				//Check tuition updates
				if(ci.Tuition_Value__c != 0 && ci.Tuition_Value__c != ci.original_instalment__r.Tuition_Value__c){
					td = new tuitionDetails();
					if(ci.Tuition_Value__c > 0)
						td.status = 'Tuition added';
					else
						td.status = 'Tuition subtracted';

					td.tuitionValue = ci.Tuition_Value__c;
				}


				//Check Commission Value updates
				if(ci.Commission_Value__c != 0 && ci.Commission_Value__c != ci.original_instalment__r.Commission_Value__c){
					if(td == null){
						td = new tuitionDetails();
						if(ci.Commission_Value__c > 0)
							td.status = 'Commission added';
						else
							td.status = 'Commission subtracted';
					}
					td.commissionValue = ci.Commission_Value__c;
				}

				if(td != null)
					inst.updateTuition.add(td);
				//Check Commission Tax Value updates
				if(ci.Commission_Tax_Value__c != 0 && ci.Commission_Tax_Value__c != ci.original_instalment__r.Commission_Tax_Value__c){
					if(td == null){
						td = new tuitionDetails();
						if(ci.Commission_Tax_Value__c > 0)
							td.status = 'Tax added';
						else
							td.status = 'Tax subtracted';
					}
					td.taxValue = ci.Commission_Tax_Value__c;
				}

				//Check Commission updates
				if(ci.Commission__c != 0 && ci.Commission__c != ci.original_instalment__r.Commission__c){
					//if(ci.Commission__c > 0)
						// inst.updateCommission += '<span style="color:purple;font-weight: bold;">Commission changed from: </span> '+ci.original_instalment__r.Commission__c+ ' <span style="color:blue;font-weight: bold;">to: </span> '+ci.Commission__c;
					//else
						//inst.updateCommission += '<span style="color:purple;font-weight: bold;">Commission subtracted</span> '+ci.Commission__c;
					td = new tuitionDetails();
					string tp = ci.client_course__r.Commission_Type__c == 0 ? '%':'$';
					if(ci.Commission__c > ci.original_instalment__r.Commission__c)
						td.status = 'Commission incremented ('+string.valueOf(ci.Commission__c - ci.original_instalment__r.Commission__c)+')' + tp;
					else
						td.status = 'Commission decremented ('+string.valueOf(ci.Commission__c - ci.original_instalment__r.Commission__c)+')' + tp;
					td.tuitionValue = ci.original_instalment__r.Tuition_Value__c;
					td.commissionValue = ci.Commission_Adjustment__c;
					td.taxValue = ci.Commission_Tax_Adjustment__c; 
					inst.updateTuition.add(td);
				}

				if(ci.Amendment_Total_Charge_Client__c != null && ci.Amendment_Total_Charge_Client__c > 0){
					inst.Charge_Client = ci.Amendment_Total_Charge_Client__c;
				}
				if(ci.Amendment_Total_Charge_Client__c != null && ci.Amendment_Total_Charge_Client__c < 0){
					inst.Pay_to_Client = ci.Amendment_Total_Charge_Client__c;
				}
				if(ci.Amendment_Receive_From_School__c != null && ci.Amendment_Receive_From_School__c > 0 && ci.Amendment_PFS_Adjustment__c == 0){ 
					inst.Charge_School = ci.Amendment_Receive_From_School__c;
				}
				
				inst.Pay_to_School = ci.Total_School_Payment__c;

				// if(ci.Amendment_Pay_To_School_Balance__c != null && ci.Amendment_Pay_To_School_Balance__c > 0){
				// 	inst.Pay_to_School = ci.Amendment_Pay_To_School_Balance__c;
				// }

				// if(ci.Amendment_Keep_Fee_Balance__c != null && ci.Amendment_Keep_Fee_Balance__c >  0)
				// 	inst.Pay_to_School += ci.Amendment_Keep_Fee_Balance__c;

				// if(ci.Amendment_PFS_Adjustment__c != null && ci.Amendment_PFS_Adjustment__c > 0){ 
				// 	inst.Pay_to_School += ci.Amendment_PFS_Adjustment__c;
				// }

				
				inst.install = ci;
				lstOverPaymentAmendments.add(inst);
			}
		}
	}
}