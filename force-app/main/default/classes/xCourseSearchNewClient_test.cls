/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class xCourseSearchNewClient_test {

    static testMethod void myUnitTest() {
    	
    	TestFactory TestFactory = new TestFactory();
        
        Account agency = TestFactory.createAgency();
        Contact employee = TestFactory.createEmployee(agency);
        Contact c = TestFactory.createClient(agency);
		
		Quotation__c q = new Quotation__c();
		q.client__c = c.id;
		q.Status__c = 'In Progress';
		q.Expiry_Date__c = system.today().addDays(7);
		insert q;
		
        
        xCourseSearchNewClient newClient = new xCourseSearchNewClient();
        xCourseSearchNewClient.getlstContact(c.FirstName, c.nationality__c, 'Client', true);
        
        newClient.getContactType();
        //newClient.getclientType();
        newClient.newClient.FirstName = 'Jane';
        newClient.newClient.LastName = 'Doe';
        newClient.newClient.Email = 'margarete@googlerose.com.test';
        newClient.addNewClient();
        
        newClient.newClient = null;
        newClient.newClient.FirstName = 'Jane';
        newClient.newClient.LastName = 'Doe';
        newClient.newClient.Email = 'jd@googlerose.com.test';
        newclient.addAnyway();
        
        
       	xCourseSearchNewClient.getlstContact('','','Lead', true);
        string all = newClient.allClients;
        
        String str = xCourseSearchNewClient.lstContact;
        
        ApexPages.currentPage().getParameters().put('clientID', newClient.newClient.id);
        newClient.addClient();
        
        newClient.clientids = c.id;
        newClient.addClientCart();
        
        Forms_of_Contact__c newClientFormContact = newclient.newClientFormContact;
        boolean b = newClient.isOnShore;
        newClient.getGender();

   	    List<SelectOption> listleadSource = newClient.listleadSource;
        List<SelectOption> d = newClient.leadSourceSpecific;
        List<SelectOption> d1 = newClient.getCountries();
        List<SelectOption> d2 = newClient.getOtherCountries();
        List<SelectOption> d3 = newClient.getDestinationsCountry();
        List<SelectOption> d4 = newClient.getplanningTravel();
        List<string> d5 = newClient.getContactDestinations();
        newClient.refreshlistLeadsSource();
        //newClient.deleteNewSocialNetwork();
        List<String> df = newclient.getLeadProductType();
        List<String> df1 = newclient.getLeadStudyType();
        xCourseSearchNewClient.addContactToQueue(c.Id);
    }
}