public class batch_AccountLogo_IP_Urls{
	public batch_AccountLogo_IP_Urls(){}
}

/*global class batch_AccountLogo_IP_Urls implements Database.Batchable<sObject>, Database.AllowsCallouts {
	
	S3Controller S3 {get;set;}
  String query;
  
  global batch_AccountLogo_IP_Urls(String qr) {
    
  	query = qr;
  }
  
  global Database.QueryLocator start(Database.BatchableContext BC) {

    return Database.getQueryLocator(query);
  }

     global void execute(Database.BatchableContext BC, List<account> scope) {
    // try{
      S3 = new S3Controller();
      S3.constructor();
      String prefix = '';
      for(account apf : scope){
        prefix = apf.SDrive_Account__c + '/' + apf.SDrive_File__c;

        s3.listBucket('ehfsdrive', prefix , null, null, null);

        if(s3.bucketList != null && !s3.bucketList.isEmpty()){  
          for(S3.ListEntry file : s3.bucketList){
            system.debug('file===>' + file);

            String url = IPFunctions.GenerateAWSLink('ehfsdrive', file.key, S3.S3Key, s3.S3Secret);
            system.debug('url===>' + url); 
            apf.Campus_Photo_URL__c = url;
            //apf.isUrlUpdated__c = true;
          }
        }
      }//end for

      update scope;
    // }
    // catch(Exception e){
    //   system.debug('error==' + e.getMessage());
    //   system.debug('error line==' + e.getLineNumber());
    // }

  }
  
  global void finish(Database.BatchableContext BC) {
    system.debug('Url update finished...');
  }
  
}*/

/*
 batch_AccountLogo_IP_Urls batch = new batch_AccountLogo_IP_Urls('Select id, SDrive_Account__c, SDrive_File__c, Campus_Photo_URL__c from from account where recordtype.name in (\'campus\',\'school\') and sDrive_File__c != null');
Id batchId = Database.executeBatch(batch, 80); //Define batch size
*/