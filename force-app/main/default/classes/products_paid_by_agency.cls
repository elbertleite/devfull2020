public with sharing class products_paid_by_agency {
    
    private User currentUser {get;set;}
	public String agencyCurrency {get;set;}
	private map<String,String> mapAgCurrency {get;set;}

	public Boolean isEdit {get{if(isEdit==null) isEdit = false; return isEdit;}set;}

    
    public products_paid_by_agency(){
    	
    	currentUser = [Select Id, Name, Aditional_Agency_Managment__c, Contact.Account.ParentId, Contact.AccountId, Contact.Department__c from User where id = :UserInfo.getUserId() limit 1];
		findAgencyOptions();
		selectedAgency = currentUser.Contact.AccountId;
		changeAgency();
		searchProducts();
	}

	public void editMode(){
		isEdit = true;
	}

	public void saveEdit(){
		for(client_product_service__c p : listProducts)
			p.isSelected__c = false;
			
		update listProducts;
		isEdit = false;
	}
	

	public void confirmPay(){
		list<client_product_service__c> upProds = new list<client_product_service__c>();

		for(client_product_service__c p : listProducts){
			if(p.isSelected__c){
				p.Creditcard_reconciled_by__c = userInfo.getUserId();
				p.Creditcard_reconciled_on__c = system.now();
				p.isSelected__c = false;

				upProds.add(p);

				for(client_product_service__c f : p.paid_products__r){
					f.Creditcard_reconciled_by__c = userInfo.getUserId();
					f.Creditcard_reconciled_on__c = system.now();
					upProds.add(f);
				}
			}
		}

		update upProds;

		searchProducts();
	}
	
	public AggregateResult summaryValues{get; set;}
	public list<client_product_service__c> listProducts{get; set;}
	
	public void searchProducts(){
		isEdit = false;
		String  fromDate = IPFunctions.FormatSqlDateIni(dateFilter.Original_Due_Date__c); 
		String  toDate = IPFunctions.FormatSqlDateFin(dateFilter.Discounted_On__c);
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && currentUser.Aditional_Agency_Managment__c == null) //set the user to see only his own agency
			selectedAgency = currentUser.Contact.AccountId;

		agencyCurrency = mapAgCurrency.get(selectedAgency);
			
		string sqlFilter = '';
		string sql = 'SELECT isSelected__c, Products_Services__r.Provider__r.Provider_Name__c, Creditcard_reconciled_on__c, Creditcard_reconciled_by__c, Client__r.name, Received_Date__c, Creditcard_debit_date__c, Received_By_Department__c, Received_By_Department__r.name, Price_Total__c, Price_Unit__c, Quantity__c, Received_By_Agency__r.name, Received_by__r.name, Confirmed_Date__c, Confirmed_By__r.name, Received_by__r.Contact.Department__r.name, Product_Name__c, Paid_by_Agency__c, Currency__c, Agency_Currency_Value__c, Agency_Currency__c,	Agency_Currency_Rate__c, (SELECT Product_Name__c, Price_Total__c, Agency_Currency_Value__c FROM paid_products__r) FROM client_product_service__c WHERE Received_By_Agency__c = :selectedAgency and Paid_by_Agency__c = true  and Creditcard_debit_date__c != null and ';
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
 			sql += ' Received_By_Agency__r.ParentId = \'' + selectedAgencyGroup +'\' AND ';


		if(dateCriteria=='rc'){
			if(selectedPeriod == 'range')
				sqlFilter += ' Creditcard_debit_date__c >= '+ fromDate + ' and  Creditcard_debit_date__c <= '+ toDate;
			
			else if(selectedPeriod == 'TODAY')
				sqlFilter += ' Creditcard_debit_date__c = TODAY';
			
			else if(selectedPeriod == 'THIS_WEEK')
				sqlFilter += ' Creditcard_debit_date__c = THIS_WEEK';
			
			else if(selectedPeriod == 'THIS_MONTH')
				sqlFilter += ' Creditcard_debit_date__c = THIS_MONTH';
			
			else if(selectedPeriod == 'unc')
				sqlFilter += ' Creditcard_reconciled_on__c = NULL';
		}
		else if(dateCriteria=='pr'){
			if(SelectedPeriod == 'range')
				sqlFilter += ' DAY_ONLY(convertTimezone(Creditcard_reconciled_on__c)) >= '+ fromDate + ' and  DAY_ONLY(convertTimezone(Creditcard_reconciled_on__c)) <= '+ toDate;
			
			else if(SelectedPeriod == 'TODAY')
				sqlFilter += ' Creditcard_reconciled_on__c = TODAY ';
			
			else if(SelectedPeriod == 'THIS_WEEK')
				sqlFilter += ' Creditcard_reconciled_on__c = THIS_WEEK ';
			
			else if(SelectedPeriod == 'THIS_MONTH')
				sqlFilter += ' Creditcard_reconciled_on__c = THIS_MONTH ';
			else if(SelectedPeriod == 'unc')
				sqlFilter += ' Creditcard_reconciled_on__c = NULL';
		}
		
		if(selectedDepartment != 'all')
			sqlFilter+= ' AND Received_by_Department__c = :selectedDepartment '; 
		
		if(selectedProduct != 'all')
			sqlFilter += ' and Products_Services__c = \''+selectedProduct +'\'';
		
		sql += sqlFilter;
			
		sql += ' order by Currency__c, Creditcard_debit_date__c nulls first, Product_Name__c';

		system.debug('sql===>' + sql);

		listProducts = Database.query(sql);
		retrieveSummary(fromDate, toDate, sqlFilter);
	}
    
    
    public void retrieveSummary(string fromDate, string toDate, string sqlFilter){
		string sql = 'Select count(id) totItens, SUM(Agency_Currency_Value__c) totProd from client_product_service__c WHERE Received_By_Agency__c = :selectedAgency and Paid_by_Agency__c = true  and ';
		
		sql += sqlFilter;
			
		summaryValues = Database.query(sql);
	}
	
    
    public client_course_instalment__c dateFilter{
		get{
			if(dateFilter == null){
				dateFilter = new client_course_instalment__c(); 
				Date myDate = Date.today();
				Date weekStart = myDate.toStartofWeek();
				dateFilter.Original_Due_Date__c = weekStart;
				dateFilter.Discounted_On__c = dateFilter.Original_Due_Date__c.addDays(6);
			}
			return dateFilter;
		} 
		set;
	}
    
   
    private string ContactId = [Select ContactId from user where id = :UserInfo.getUserId() limit 1].ContactId;
    private contact userDetails = [Select AccountId, Account.ParentId from Contact where id = :ContactId limit 1];
    public string selectedAgencyGroup{
    	get{
    		if(selectedAgencyGroup == null)
    			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
    		return selectedAgencyGroup;
    	}  
    	set;
    }
    
       
    public List<SelectOption> agencyGroupOptions {
  		get{
   			if(agencyGroupOptions == null){
   				
   				agencyGroupOptions = new List<SelectOption>();  
   				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group  
	    			for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:currentUser.Contact.AccountId order by Parent.Name]){
		     			agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
		    		}
   				}else{
   					for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
		     			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
		    		}
   				}
	   		}
	   		return agencyGroupOptions;
	  }
	  set;
 	}

	public void changeGroup(){
		selectedAgency = null;
		findAgencyOptions();
		changeAgency();
	}

	public void changeAgency(){
		findDepartments();
		findAgencyProducts();
	}

	public void changeDepartment(){
		findAgencyProducts();
	}

	public list<SelectOption> agencyOptions {get;set;}
	public String selectedAgency {get;set;}
	public void findAgencyOptions(){
		mapAgCurrency = new map<String,String>();
		agencyOptions = new List<SelectOption>();
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
			for(Account a: [Select Id, Name, Account_Currency_Iso_Code__c from Account where id =:currentUser.Contact.AccountId order by name]){
				if(selectedAgency == null)
					selectedAgency = a.Id;					
				agencyOptions.add(new SelectOption(a.Id, a.Name));
				mapAgCurrency.put(a.Id, a.Account_Currency_Iso_Code__c);
			}
			if(currentUser.Aditional_Agency_Managment__c != null){
				for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
					agencyOptions.add(new SelectOption(ac.id, ac.name));
					mapAgCurrency.put(ac.Id, ac.Account_Currency_Iso_Code__c);
				}
			}
		}else{
			for(Account a: [Select Id, Name, Account_Currency_Iso_Code__c from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
				agencyOptions.add(new SelectOption(a.Id, a.Name));
				mapAgCurrency.put(a.Id, a.Account_Currency_Iso_Code__c);
				if(selectedAgency == null)
					selectedAgency = a.Id;	
			}
		}
	}
	
	
	
	public string selectedProduct{
    	get{
    		if(selectedProduct == null)
    			selectedProduct = 'all';
    		return selectedProduct;
    	}  
    	set;
    }
    

	public List<SelectOption> agencyProducts {get;set;}
	public void findAgencyProducts(){
		agencyProducts = new List<SelectOption>();
		agencyProducts.add(new SelectOption('all','-- All --'));

		for(AggregateResult ag : [Select Products_Services__c pId, Product_Name__c nm from client_product_service__c WHERE Received_By_Agency__c = :selectedAgency and Paid_by_Agency__c = true and Creditcard_reconciled_on__c = NULL AND Products_Services__c != NULL group by Products_Services__c, Product_Name__c]){
			if((String) ag.get('pId') != null)
				agencyProducts.add(new SelectOption((String) ag.get('pId'), (String) ag.get('nm')));
		}//end for
	}
	
	public string selectedDepartment {get{if(selectedDepartment==null) selectedDepartment = 'all'; return selectedDepartment;}set;}
	private set<Id> allDepartments {get;set;}
	public list<SelectOption> departments {get;set;}
	public void findDepartments(){
		allDepartments = new set<Id>();
		departments = new List<SelectOption>();
		departments.add(new SelectOption('all', '-- All --'));

		for(Department__c d: [Select Id, Name from Department__c where Agency__c = :selectedAgency and Inactive__c = false]){
			departments.add(new SelectOption(d.Id, d.Name));
			allDepartments.add(d.Id);
		}//end for
	}

	public string SelectedPeriod{get {if(SelectedPeriod == null) SelectedPeriod = 'unc'; return SelectedPeriod; } set;}
	
	// Periods Options
	private List<SelectOption> periods;
	public List<SelectOption> getPeriods() {
		if(periods == null){
			periods = new List<SelectOption>();
			periods.add(new SelectOption('TODAY','Today'));
			periods.add(new SelectOption('THIS_WEEK','This Week'));
			periods.add(new SelectOption('THIS_MONTH','This Month'));
			periods.add(new SelectOption('unc','Unconfirmed'));
			periods.add(new SelectOption('range','Range'));
		}
		return periods;
	}

	//Selected Date Criteria
	public string dateCriteria{get {if(dateCriteria == null) dateCriteria = 'rc'; return dateCriteria; } set;}

	//Date Criteria Options
	private List<SelectOption> dateCriteriaOptions;
	public List<SelectOption> getdateCriteriaOptions() {
		if(dateCriteriaOptions == null){
			dateCriteriaOptions = new List<SelectOption>();
			dateCriteriaOptions.add(new SelectOption('rc','Product Received On'));
			dateCriteriaOptions.add(new SelectOption('pr','Payment Reconciled'));
		}
		return dateCriteriaOptions;
	}
	
	public PageReference generateExcel(){
		PageReference pr = Page.products_paid_by_agency_excel;
		pr.setRedirect(false);
		return pr;		
	}
	
	 public void searchProductsExcel(){
    	searchProducts();
    }
}