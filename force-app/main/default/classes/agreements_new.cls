public without sharing class agreements_new {

	public String agencyOptions {get;set;} // Signed by agency options
	public String allUsers {get;set;} //Signed by users
	public String allAgencies {get;set;} // Select agencies connected to agreement
	public String agreementType {get;set;} 
	public String providerOptions {get;set;} // Agreement providers
	public list<String> globeRegions {get;set;} // Agencies Region
	public list<String> agencyCountries {get;set;} 

	public list<String> selectedRegions {get;set;}
	public list<String> selectedCountries {get;set;}
	public list<String> selectedOnly {get;set;}

	public String selectedAgencies {get;set;}

	public String agreement {get;set;}

	public String agreeId {get;set;}
	public String agreeDuration {get;set;}
	public String nationalityExc {get;set;}
	public String enrollConditions {get;set;}
	public String coursePayment {get;set;}
	public String refundCancel {get;set;}
	public String extraInfo {get;set;}

	public String bucketName {get;set;}
	
	// CONSTRUCTOR
	public agreements_new() {

		bucketName = IPFunctions.s3bucket;

		arrangeFilters();
		selectedAgencies = 'newAgree';

		String agId = ApexPages.currentPage().getParameters().get('id');
		if(agId != null && agId != ''){
			Agreement__c ag = [SELECT Id, Renew_in_progress__c, Renew_upon_request__c, Expired_Notes__c, Status_formula__c, Signed_by_Agency__c, Signed_by_User__c, Signed_Date__c, Provider_Type__c, Agency_Regions__c, Agency_Exc_Countries__c, Include_Countries__c, Start_Date__c, End_Date__c, Until_Terminated__c, Contact_Details__c, Nationality_Conditions__c, Agreement_Duration__c, Course_Payments__c, Enrolment_Conditions__c, Extra_Information__c, Refund_and_Cancellation__c, isAvailable__c, Agreement_Name__c, Allow_Sub_Representatives__c, Agreement_Type__c, School_not_Sign__c, (SELECT Account__c, Account__r.Name, Account__r.ParentId, Account__r.Parent.Name, Account__r.BillingCountry, Account__r.RecordType.Name FROM Agreement_Accounts__r order by Account__r.Parent.Name) FROM Agreement__c WHERE id = :agId];

			if(ag.Agency_Regions__c != null)
				selectedRegions = ag.Agency_Regions__c.split(';');
			else
				selectedRegions = new list<String>();

			if(ag.Agency_Exc_Countries__c != null)
				selectedCountries = ag.Agency_Exc_Countries__c.split(';');
			else
				selectedCountries = new list<String>();


			if(ag.Include_Countries__c != null)
				selectedOnly = ag.Include_Countries__c.split(';');
			else
				selectedOnly = new list<String>();

			selectedAgencies = agreements_new.findAgencies(String.join(selectedRegions, ','), String.join(selectedCountries, ','), String.join(selectedOnly, ','));
			if(selectedAgencies == null) selectedAgencies= 'newAgree';

			agreeDuration = ag.Agreement_Duration__c;
			nationalityExc = ag.Nationality_Conditions__c;
			enrollConditions = ag.Enrolment_Conditions__c;
			coursePayment = ag.Course_Payments__c;
			refundCancel = ag.Refund_and_Cancellation__c;
			extraInfo = ag.Extra_Information__c;
			agreeId = ag.Id;

			agreement = JSON.serialize(ag);

		}
		else{
			agreement = JSON.serialize('newAgree');
		} 
	}

	//SAVE AGREEMENT (Step 1)
	@RemoteAction
	public static callResult saveAgreement(String agreementId, String aName, String signedByAgency, String signedBy, String signedDate, String agreeType, String provider, String accounts, String regions, String excCountries, String incOnly, String allowSub, String noAgreement){
		set<String> lAccounts = new set<String>(accounts.split(';'));
		list<IPFunctions.LogHistory> logs = new list<IPFunctions.LogHistory>();

		system.debug('agreementId==>' + agreementId);

		Agreement__c newA = new Agreement__c();
		newA.Id = agreementId;
		newA.Agreement_Name__c = aName;
		if(!Boolean.valueOf(noAgreement)){
			newA.Signed_by_Agency__c = signedByAgency;
			newA.Signed_by_User__c = signedBy;
			newA.Signed_Date__c = Date.valueOf(signedDate);
		}
		else{
			newA.Signed_by_Agency__c = null;
			newA.Signed_by_User__c = null;
			newA.Signed_Date__c = null;
			newA.Start_Date__c = null;
			newA.End_Date__c = null;
			newA.Until_Terminated__c = false;
		}
		newA.Agreement_Type__c = agreeType;	
		newA.Provider_Type__c = provider;
		newA.Allow_Sub_Representatives__c = Boolean.valueOf(allowSub);
		newA.School_not_Sign__c = Boolean.valueOf(noAgreement);
		system.debug('allowSub==>' + newA.Allow_Sub_Representatives__c);

		if(regions != null && regions != '')
			newA.Agency_Regions__c = regions.replace(',', ';');
		else
			newA.Agency_Regions__c = null;
		
		if(excCountries != null && excCountries != '')
			newA.Agency_Exc_Countries__c = excCountries.replace(',', ';');
		else
			newA.Agency_Exc_Countries__c = null;
		
		if(incOnly != null && incOnly != '')
			newA.Include_Countries__c = incOnly.replace(',', ';');
		else
			newA.Include_Countries__c = null;

		

		list<Agreement_Accounts__c> lAA = new list<Agreement_Accounts__c>();
		//EDIT
		if(agreementId!= null && agreementId != ''){
			User currentUser = [SELECT Name, Contact.Account.Name FROM User WHERE id = :userInfo.getUserId()];
			Datetime GMT = system.now();
			String editDate = GMT.format('yyyy-MM-dd HH:mm:ss', 'UTC');

			/** COMPARE FIELDS - AGREEMENT LOG **/
				Agreement__c oldAgreement = [SELECT Agreement_Name__c, Signed_by_Agency__c, Signed_by_Agency__r.Name, Signed_by_User__c, Signed_Date__c, Agreement_Type__c, Provider_Type__c, Allow_Sub_Representatives__c, Agency_Regions__c, Agency_Exc_Countries__c, Include_Countries__c FROM Agreement__c WHERE Id = :agreementId];
				system.debug(oldAgreement.Agreement_Name__c);
				system.debug(newA.Agreement_Name__c);
				//Agreement Name
				if(oldAgreement.Agreement_Name__c != newA.Agreement_Name__c)
					logs.add(new IPFunctions.LogHistory('Agreement_Name__c', Schema.Agreement__c.fields.Agreement_Name__c.getDescribe().getLabel(), oldAgreement.Agreement_Name__c, newA.Agreement_Name__c, currentUser.Name, currentUser.Contact.Account.Name, editDate));

				//Signed Agency
				if(oldAgreement.Signed_by_Agency__c != newA.Signed_by_Agency__c)
					logs.add(new IPFunctions.LogHistory('Signed_by_Agency__c', Schema.Agreement__c.fields.Signed_by_Agency__c.getDescribe().getLabel(), oldAgreement.Signed_by_Agency__c, newA.Signed_by_Agency__c, currentUser.Name, currentUser.Contact.Account.Name, editDate));

				//Signed User
				if(oldAgreement.Signed_by_User__c != newA.Signed_by_User__c)
					logs.add(new IPFunctions.LogHistory('Signed_by_User__c', Schema.Agreement__c.fields.Signed_by_User__c.getDescribe().getLabel(), oldAgreement.Signed_by_User__c, newA.Signed_by_User__c, currentUser.Name, currentUser.Contact.Account.Name, editDate));

				//Signed Date
				if(oldAgreement.Signed_Date__c != newA.Signed_Date__c)
					logs.add(new IPFunctions.LogHistory('Signed_Date__c', Schema.Agreement__c.fields.Signed_Date__c.getDescribe().getLabel(), IPFunctions.getStringDate(oldAgreement.Signed_Date__c), IPFunctions.getStringDate(newA.Signed_Date__c), currentUser.Name, currentUser.Contact.Account.Name, editDate));

				//Agreement Type
				if(oldAgreement.Agreement_Type__c != newA.Agreement_Type__c)
					logs.add(new IPFunctions.LogHistory('Agreement_Type__c', Schema.Agreement__c.fields.Agreement_Type__c.getDescribe().getLabel(), oldAgreement.Agreement_Type__c, newA.Agreement_Type__c, currentUser.Name, currentUser.Contact.Account.Name, editDate));

				//Provider Type
				if(oldAgreement.Provider_Type__c != newA.Provider_Type__c)
					logs.add(new IPFunctions.LogHistory('Provider_Type__c', Schema.Agreement__c.fields.Provider_Type__c.getDescribe().getLabel(), oldAgreement.Provider_Type__c, newA.Provider_Type__c, currentUser.Name, currentUser.Contact.Account.Name, editDate));

				//Allow Sub
				if(oldAgreement.Allow_Sub_Representatives__c != newA.Allow_Sub_Representatives__c)
					logs.add(new IPFunctions.LogHistory('Allow_Sub_Representatives__c', Schema.Agreement__c.fields.Allow_Sub_Representatives__c.getDescribe().getLabel(), string.valueOf(oldAgreement.Allow_Sub_Representatives__c), string.valueOf(newA.Allow_Sub_Representatives__c), currentUser.Name, currentUser.Contact.Account.Name, editDate));

				//Regions
				if(oldAgreement.Agency_Regions__c != newA.Agency_Regions__c)
					logs.add(new IPFunctions.LogHistory('Agency_Regions__c', Schema.Agreement__c.fields.Agency_Regions__c.getDescribe().getLabel(), oldAgreement.Agency_Regions__c, newA.Agency_Regions__c, currentUser.Name, currentUser.Contact.Account.Name, editDate));

				//Exc. Countries
				if(oldAgreement.Agency_Exc_Countries__c != newA.Agency_Exc_Countries__c)
					logs.add(new IPFunctions.LogHistory('Agency_Exc_Countries__c', Schema.Agreement__c.fields.Agency_Exc_Countries__c.getDescribe().getLabel(), oldAgreement.Agency_Exc_Countries__c, newA.Agency_Exc_Countries__c, currentUser.Name, currentUser.Contact.Account.Name, editDate));
					
				//Inc. Countries Only
				if(oldAgreement.Include_Countries__c != newA.Include_Countries__c)
					logs.add(new IPFunctions.LogHistory('Include_Countries__c', Schema.Agreement__c.fields.Include_Countries__c.getDescribe().getLabel(), oldAgreement.Include_Countries__c, newA.Include_Countries__c, currentUser.Name, currentUser.Contact.Account.Name, editDate));

			/** COMPARE FIELDS - AGREEMENT LOG **/

			list<Id> delAccounts = new list<Id>();

			//For the Agreement Accounts we must delete all the removed ones and add the new ones (cannot delete all and insert new again because of the School Setup)
			map<String, Agreement_Accounts__c> oldAccounts = new map<String, Agreement_Accounts__c>(); // All accounts already saved in Agreement
			for(Agreement_Accounts__c aa : [SELECT Id, Account__c, Account__r.Name, Account__r.RecordType.Name, (Select Id FROM Commissions__r) FROM Agreement_Accounts__c WHERE Agreement__c = :newA.id])
				oldAccounts.put(aa.Account__c, aa);

			//Check the Accounts to be deleted
			for(String aa : oldAccounts.keySet()){
				if(!lAccounts.contains(aa)){
					//Log Removed Accounts
					logs.add(new IPFunctions.LogHistory('Agreement_Accounts__c', 'Related ' + oldAccounts.get(aa).Account__r.RecordType.Name, oldAccounts.get(aa).Account__r.Name, 'Removed', currentUser.Name, currentUser.Contact.Account.Name, editDate));

					delAccounts.add(oldAccounts.get(aa).Id); //Delete Agreement Account
					for(Commission__c c : oldAccounts.get(aa).Commissions__r)
						delAccounts.add(c.id); //Delete Commissions
				}
			}//end for

			Database.delete(delAccounts);
			set<Id> newAccounts = new set<Id>();
			for(String ag : lAccounts)
				if(!oldAccounts.containsKey(ag)){
					newAccounts.add(ag);
					lAA.add(new Agreement_Accounts__c(Account__c = id.valueOf(ag), Agreement__c = newA.id));
				}
				else continue;
				
			//Log New Accounts
			if(newAccounts.size()>0){
				for(Account ac : [Select Id, Name, RecordType.Name from Account where Id in :newAccounts])
					logs.add(new IPFunctions.LogHistory('Agreement_Accounts__c', 'Related ' + ac.RecordType.Name, ac.Name, 'Added', currentUser.Name, currentUser.Contact.Account.Name, editDate));
			}

			update newA;
		}
		//NEW
		else{
			insert newA;

			for(String ag : lAccounts)
				lAA.add(new Agreement_Accounts__c(Account__c = id.valueOf(ag), Agreement__c = newA.id));

			// IPFunctions.wsAgreement ws = new IPFunctions.wsAgreement();
			// ws.saveAgreementAcc(lAA);

		}

		if(lAA != null && lAA.size()>0)
			insert lAA;	

		//Save Logs on Amazon S3
		callResult cr;
		if(logs.size()>0)
			cr = new callResult(newA.Id, logs);
		else
			cr = new callResult(newA.Id, null);

		system.debug('cr ==>' + cr);
		
		return cr;
		
	}

	@RemoteAction
	public static String saveLogs(String logs, String agreementId){ 

		system.debug('agreementId===>' + agreementId);
		system.debug('logs===>' + logs);

		// list<IPFunctions.LogHistory> logsList = (list<IPFunctions.LogHistory>) JSON.deserialize(logs, list<IPFunctions.LogHistory>.class);
		// system.debug('logsList===>' + logsList);

		String previusLog = findAgreementLog(agreementId);

		system.debug('previusLog===>' + previusLog);

		if(previusLog != null){
			list<IPFunctions.LogHistory> logsList = (list<IPFunctions.LogHistory>) JSON.deserialize(previusLog, list<IPFunctions.LogHistory>.class);
			logsList.addAll((list<IPFunctions.LogHistory>) JSON.deserialize(logs, list<IPFunctions.LogHistory>.class));
			saveLogAmazonS3(JSON.serialize(logsList), agreementId);
			
		}
		else{
			saveLogAmazonS3(logs, agreementId);
		}
		

		return 'calledSaveLogs';
	}

	//Find agreement log on Amazon S3
	public static String findAgreementLog(String agreementId){
		String result;

		S3Controller cS3= new S3Controller();
		cS3.constructor();
		cs3.getObject('AgreementLog/' + agreementId, IPFunctions.s3bucket);		

		if(cs3.resultSingleObject != null && cs3.resultSingleObject.Data != null)
			result = EncodingUtil.base64Decode(cs3.resultSingleObject.Data).toString();

		return result;
	}

	//Save Log Amazon S3
	public static void saveLogAmazonS3(String agreeLog, String agreementId){
		S3Controller cS3 = new S3Controller();
		cS3.constructor();
		List<String> allbuck= cS3.allBuckets;
		cS3.insertFileS3(Blob.valueOf(agreeLog), IPFunctions.s3bucket, 'AgreementLog/' + agreementId, null);
	}

	public class callResult{
		public String agreementId {get;set;}
		public list<IPFunctions.LogHistory> logs {get;set;}

		public callResult(String agreementId, list<IPFunctions.LogHistory> logs){
			this.agreementId = agreementId;
			this.logs = logs;
		}
	}


	public void updateAgreeId(){
		agreeId = ApexPages.currentPage().getParameters().get('agreeId');
	}

	//Save General Info (Step 2)
	@RemoteAction
	public static callResult saveGeneralInfo(String agreementId, String startDate, String endDate, Boolean noExpire, String contactDetails, String nationalityExc, String agreeDuration, String enrollConditions, String coursePayment, String refundCancel, String extraInfo, Boolean isAvailable, Boolean noAgreement, Boolean renewProgress, Boolean renewUpon, String expiredNotes){

		Date endAgree;
		if(endDate == 'none' || endDate == '' || noExpire) 
			endAgree = null;
		else endAgree = date.valueOf(endDate); 

		if(nationalityExc == '<br>') nationalityExc = null;
		if(agreeDuration == '<br>') agreeDuration = null;
		if(enrollConditions == '<br>') enrollConditions = null;
		if(coursePayment == '<br>') coursePayment = null;
		if(refundCancel == '<br>') refundCancel = null;
		if(extraInfo == '<br>') extraInfo = null;

		if(!noAgreement){
			if(date.valueOf(startDate) <= system.today() && (noExpire || (endAgree != null && endAgree >= system.today()))){
				renewProgress = false;
				renewUpon = false;
				expiredNotes = null;
			}else if(date.valueOf(startDate) > system.today()){
				renewProgress = false;
				renewUpon = false;
				expiredNotes = null;
			}
		}

		Agreement__c agreement = new Agreement__c(
			Id = id.valueOf(agreementId),
			isAvailable__c = boolean.valueOf(isAvailable),
			Contact_Details__c = contactDetails,
			Nationality_Conditions__c = nationalityExc,
			Agreement_Duration__c = agreeDuration,
			Course_Payments__c = coursePayment,
			Enrolment_Conditions__c = enrollConditions,
			Extra_Information__c = extraInfo,
			Refund_and_Cancellation__c = refundCancel,
			Renew_in_progress__c = renewProgress,
			Renew_upon_request__c = renewUpon,
			Expired_Notes__c = expiredNotes
		);

		if(!noAgreement){
			agreement.Start_Date__c = date.valueOf(startDate);
			agreement.End_Date__c = endAgree;
			agreement.Until_Terminated__c = boolean.valueOf(noExpire);
		}
		else{
			agreement.Start_Date__c = null;
			agreement.End_Date__c = null;
			agreement.Until_Terminated__c = false;
		}


		Agreement__c oldAgreement = [SELECT Start_Date__c, End_Date__c, Until_Terminated__c, isAvailable__c, Contact_Details__c, Nationality_Conditions__c, Agreement_Duration__c, Course_Payments__c, Enrolment_Conditions__c, Extra_Information__c, Refund_and_Cancellation__c  FROM Agreement__c WHERE Id = :agreementId];

		User currentUser = [SELECT Name, Contact.Account.Name FROM User WHERE id = :userInfo.getUserId()];
		Datetime GMT = system.now();
		String editDate = GMT.format('yyyy-MM-dd HH:mm:ss', 'UTC');

		list<IPFunctions.LogHistory> logs = new list<IPFunctions.LogHistory>();

		if(oldAgreement.Start_Date__c != agreement.Start_Date__c)
			logs.add(new IPFunctions.LogHistory('Start_Date__c', Schema.Agreement__c.fields.Start_Date__c.getDescribe().getLabel(), IPFunctions.getStringDate(oldAgreement.Start_Date__c), IPFunctions.getStringDate(agreement.Start_Date__c), currentUser.Name, currentUser.Contact.Account.Name, editDate));

		if(oldAgreement.End_Date__c != agreement.End_Date__c)
			logs.add(new IPFunctions.LogHistory('End_Date__c', Schema.Agreement__c.fields.End_Date__c.getDescribe().getLabel(), IPFunctions.getStringDate(oldAgreement.End_Date__c), IPFunctions.getStringDate(agreement.End_Date__c), currentUser.Name, currentUser.Contact.Account.Name, editDate));

		if(oldAgreement.Until_Terminated__c != agreement.Until_Terminated__c)
			logs.add(new IPFunctions.LogHistory('Until_Terminated__c', Schema.Agreement__c.fields.Until_Terminated__c.getDescribe().getLabel(), String.valueOf(oldAgreement.Until_Terminated__c), String.valueOf(agreement.Until_Terminated__c), currentUser.Name, currentUser.Contact.Account.Name, editDate));

		if(oldAgreement.isAvailable__c != agreement.isAvailable__c)
			logs.add(new IPFunctions.LogHistory('isAvailable__c', Schema.Agreement__c.fields.isAvailable__c.getDescribe().getLabel(), String.valueOf(oldAgreement.isAvailable__c), String.valueOf(agreement.isAvailable__c), currentUser.Name, currentUser.Contact.Account.Name, editDate));

		if(oldAgreement.Contact_Details__c != agreement.Contact_Details__c)
			logs.add(new IPFunctions.LogHistory('Contact_Details__c', Schema.Agreement__c.fields.Contact_Details__c.getDescribe().getLabel(), oldAgreement.Contact_Details__c, agreement.Contact_Details__c, currentUser.Name, currentUser.Contact.Account.Name, editDate));

		update agreement;

		callResult cr;
		if(logs.size()>0)
			cr = new callResult(agreement.Id, logs);
		else
			cr = new callResult(agreement.Id, null);

		return cr;
	}

	public PageReference redirectToCommission(){

		PageReference agreeCommissions = new PageReference('/apex/agreement_commissions?id='+ApexPages.currentPage().getParameters().get('agreeId'));
		agreeCommissions.setRedirect(true);

		return agreeCommissions;
	}

	//FIND SCHOOLS
	@RemoteAction
	public static String findSchools(String schName){
		String result = '';

		if(schName.length() > 1){
			// IPFunctions.SearchNoSharing sns = new IPFunctions.SearchNoSharing();
			// List<Account> allSchools = sns.NSLAccount('SELECT Id, Name, BillingCountry FROM Account WHERE Name LIKE \'%' + schName + '%\' and RecordType.Name = \'School\'');
			
			List<Account> allSchools = Database.query('SELECT Id, Name, BillingCountry FROM Account WHERE Name LIKE \'%' + schName + '%\' and RecordType.Name = \'School\'');

			result = JSON.serialize(allSchools);
		}else{
			result = JSON.serialize(result);
		}

		return result;
	}

	//FIND AGENCIES
	@RemoteAction
	public static String findAgencies(String regions, String excCountries, String incOnly){
		String result = '';

		IPFunctions.SearchNoSharing sns = new IPFunctions.SearchNoSharing();

		system.debug('regions==>' + regions);
		system.debug('excCountries==>' + excCountries);
		system.debug('incOnly==>' + incOnly);

		list<String> pRegions = new list<String>();
		list<String> pCountries = new list<String>();
		list<String> pIncOnly = new list<String>();
		
		if( regions != null && regions != '')
			pRegions = regions.split(',');

		if( excCountries != null && excCountries != '')
			pCountries = excCountries.split(',');

		if( incOnly != null && incOnly != '')
			pIncOnly = incOnly.split(',');

		List<opWrapper> agencies = new list<opWrapper>();

		

		String sql = ' SELECT Id, Name, BillingCountry, Globe_Region__c, ParentId, Parent.Name FROM Account WHERE RecordType.Name = \'Agency\' and Inactive__c = false ';

		if(pRegions.size()>0 && regions != 'All Regions')
			sql += ' AND Globe_Region__c IN ( \''+ String.join(pRegions, '\',\'') + '\' )';

		if(pCountries.size()>0)
			sql += ' AND BillingCountry NOT IN ( \''+ String.join(pCountries, '\',\'') + '\' )';

		if(pIncOnly.size()>0)
			sql += ' AND BillingCountry IN ( \''+ String.join(pIncOnly, '\',\'') + '\' )';

		sql += ' order by Parent.Name, BillingCountry, Name';

		system.debug('sql===>' + sql);



		map<String, opWrapper> groups = new map<String, opWrapper>();
		
		for(Account ac: Database.query(sql)){
			if(groups.containsKey(ac.ParentId))
				groups.get(ac.ParentId).addInnerOp(new opWrapper(ac.Globe_Region__c, ac.Id, ac.Name, ac.BillingCountry));
			else{
				groups.put(ac.ParentId, new opWrapper(ac.Globe_Region__c, ac.ParentId, ac.Parent.Name, ac.BillingCountry));
				groups.get(ac.ParentId).addInnerOp(new opWrapper(ac.Globe_Region__c, ac.Id, ac.Name, ac.BillingCountry));
			}
			
		}//end for

		for(String g : groups.keyset())
			agencies.add(groups.get(g));


		result = JSON.serialize(agencies);

		return result;
	}
	
	//ARRANGE FILTERS
	private void arrangeFilters(){

		//***************************************************************************
		//								 AGENCY OPTIONS							   //
		//***************************************************************************
		set<String> groups = new set<String>();

		list<opWrapper> allAgOptions = new list<opWrapper>();

		for(Account ac : [SELECT Id, Name, ParentId, Parent.Name FROM Account WHERE RecordType.Name = 'Agency' and Hify_Agency__c = false and Inactive__c = false order by Parent.Name, Name]){
			if(!groups.contains(ac.Parent.Name)){
				groups.add(ac.Parent.Name);

			}	

			allAgOptions.add(new opWrapper(ac.Parent.Name, ac.Id, ac.Name));
		}//end for

		agencyOptions = JSON.serialize(allAgOptions);

		// list<Agency> allAg = new list<Agency>();


		//***************************************************************************
		//								 USER OPTIONS							   //
		//***************************************************************************
		IPFunctions.SearchNoSharing sns = new IPFunctions.SearchNoSharing();
		allUsers = JSON.serialize(sns.NSallUsers());


		//***************************************************************************
		//								 AGREEMENT TYPE 						   //
		//***************************************************************************

		Schema.DescribeFieldResult fieldResult = Agreement__c.Agreement_Type__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

		list<opWrapper> allAgreeTypes = new list<opWrapper>();

		for( Schema.PicklistEntry f : ple)
		allAgreeTypes.add(new opWrapper('', f.getValue(), f.getLabel()));

		agreementType = JSON.serialize(allAgreeTypes);

		//***************************************************************************
		//								 PROVIDER OPTIONS						   //
		//***************************************************************************

		fieldResult = Agreement__c.Provider_Type__c.getDescribe();
		ple = fieldResult.getPicklistValues();

		list<opWrapper> allProviders = new list<opWrapper>();

		for( Schema.PicklistEntry f : ple)
		allProviders.add(new opWrapper('', f.getValue(), f.getLabel()));

		providerOptions = JSON.serialize(allProviders);


		//***************************************************************************
		//								 GLOBE REGIONS OPTIONS					   //
		//***************************************************************************

		globeRegions = new list<String>{'All Regions'};
		fieldResult = Account.Globe_Region__c.getDescribe();
		ple = fieldResult.getPicklistValues();

		for(Schema.PicklistEntry f : ple)
			globeRegions.add(f.getLabel());


		//***************************************************************************
		//								 AGENCY COUNTRIES						   //
		//***************************************************************************

		agencyCountries = new list<String>(IPFunctions.getAllCountriesString());
	}

	private class opWrapper{
		public String region {get;set;}
		public String country {get;set;}
		public String groupName {get;set;}
		public String opId {get;set;}
		public String opName {get;set;}
		public list<opWrapper> innerOp {get;set;}
		public opWrapper(String region, String opId, String opName, String country){
			this.region = region;
			this.opId = opId;
			this.opName = opName;
			this.country = country;
			this.innerOp = new list<opWrapper>();
		}
		public opWrapper(String groupName, String opId, String opName){
			this.groupName = groupName;
			this.opId = opId;
			this.opName = opName;
		}

		public void addInnerOp(opWrapper op){
			innerOp.add(op);
		}
	}

	
}