/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class client_course_add_product_test {

    static testMethod void myUnitTest() {
       
       TestFactory tf = new TestFactory();
       
       Account school = tf.createSchool();
       Account agency = tf.createAgency();
       Contact emp = tf.createEmployee(agency);
       User portalUser = tf.createPortalUser(emp);
       Account campus = tf.createCampus(school, agency);
       Course__c course = tf.createCourse();
       Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
       
       
       Test.startTest();
       system.runAs(portalUser){
       	   Contact client = tf.createLead(agency, emp);
	       client_course__c booking = tf.createBooking(client);
	       client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       client_product_service__c bkproduct =  tf.createCourseProduct(booking, agency);
	       client_course_add_product testClass = new client_course_add_product(new ApexPages.StandardController(cc));
	       
	       List<SelectOption> productCategories = testClass.productCategories;
	       
	       Map<String, List<Quotation_Products_Services__c>> products = testClass.products;
	       Map<String, List<SelectOption>> prodQtdList = testClass.prodQtdList;
	       List<client_course_add_product.CustomProductWrapper> customProductList = testClass.customProductList;
	       client_course_add_product.CustomProductWrapper customProduct = testClass.customProduct;
	       
	       testClass.filterProducts();
	       
	       testClass.getUnitsRange();
	       testClass.getCurrencies();
	       
	       client_course_add_product.CustomProductWrapper cp  = new client_course_add_product.CustomProductWrapper();
	       cp.product = bkproduct;
	       
	       testClass.customProduct = cp;
	       
	       
	       testClass.addCustomProduct();
	       testClass.addProducts();
	       
	       ApexPages.currentPage().getParameters().put('cpid', '0');
	       testClass.deleteCustomProduct();
	       
	       ApexPages.currentPage().getParameters().put('productID', bkproduct.id);
	       ApexPages.currentPage().getParameters().put('category', 'Government Fee');
	       testClass.getValue();
      
       }
       
       Test.stopTest();
    }
}