/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class xCourseSearchCustomCourse_test {

    static testMethod void myUnitTest() {
        
        Web_Search__c ws = new Web_Search__c();
        ws.Combine_Quotation__c = true;
        insert ws;
        
        Search_Courses__c sc = new Search_Courses__c();
        sc.UseAsCustomTemplate__c = true;
        sc.isCustomCourse__c = true;
        sc.Web_Search__c = ws.id;
        sc.Custom_Currency__c = 'AUD';
        insert sc;
        
        Search_Course_Extra_Fee__c ef = new Search_Course_Extra_Fee__c();
        ef.Custom_Fee_Name__c = 'enrolment fee';
        ef.Fee_Value__c = 150;
        ef.Number_Units__c = 1;
        ef.Search_Course__c = sc.id;
        ef.Extra_Fee_Interval__c = 'Week';
        
        insert ef;
        
        
        Apexpages.Standardcontroller controller = new ApexPages.Standardcontroller(ws);
        xCourseSearchCustomCourse custom = new xCourseSearchCustomCourse(controller);
        
       
        custom.newCustomCourse.Custom_Campus__c = 'Custom Campus';
        custom.newCustomCourse.Custom_City__c = 'Sydney';
        custom.newCustomCourse.Custom_Country__c = 'Australia';
        custom.newCustomCourse.Custom_Course__c = 'General english';
        custom.newCustomCourse.Custom_Currency__c = 'AUD';
        custom.newCustomCourse.Custom_NumberOfUnits__c = 20;
        custom.newCustomCourse.Custom_StartDate__c = system.today();
        custom.newCustomCourse.Custom_PriceOption__c = 'Per Unit';
        custom.newCustomCourse.Custom_Value__c = 300;
        
        custom.newFee.Custom_Fee_Name__c = 'Enrolment fee';
        custom.newfee.Fee_Value__c = 200;
        custom.newFee.Extra_Fee_Interval__c = 'Unit';
        custom.newFee.Number_Units__c = 1;
        
        custom.addNewFee();
        
        custom.newFee.Custom_Fee_Name__c = 'Enrolment fee';
        custom.newfee.Fee_Value__c = 200;
        custom.newFee.Extra_Fee_Interval__c = 'Unit';
        custom.newFee.Number_Units__c = 1;
        custom.addNewFee();
        
        ApexPages.currentPage().getParameters().put('feeToDelete', '0');
        custom.deleteFee();
        
        
        custom.newCustomCourse.UseAsCustomTemplate__c = true;
        
        custom.newCustomCourse.Selected__c = true;
        
        custom.saveNewCourse();
        
        custom.getCourseTemplate();
        
        custom.courseTemplateId = sc.id;
        custom.openTemplate();
        
        custom.removeCoursesTemplate();
        
        custom.getCurrencies();
        
        ApexPages.currentPage().getParameters().put('cid', sc.id);
        xCourseSearchCustomCourse edit = new xCourseSearchCustomCourse(controller);
        
        String str = edit.combinedQuoteCountry;
        str = edit.combinedQuoteCurrency;
        
        edit.getCountries();
        edit.getFeeInterval();
        edit.getPriceOptions();
        edit.getListUnitType();
      	edit.getUnitsRange();
        
    }
}