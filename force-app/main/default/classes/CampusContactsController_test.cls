/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CampusContactsController_test {

    static testMethod void myUnitTest() {
        
		TestFactory tf = new TestFactory();
		Account agency = tf.createAgency();				
		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);
		Contact employee = tf.createEmployee(campus);
		User portalUser = tf.createPortalUser(employee);
		
		Test.startTest();
		
		
		system.runAs(portalUser){
			
			Contact ct = tf.createLead(campus, employee);
			ApexPages.currentPage().getParameters().put('ct', ct.id);
			ApexPages.currentPage().getParameters().put('id', campus.id);
			CampusContactsController ccc = new CampusContactsController();			
			ccc.setupCampusContact();	
			ccc.redirectNewContact();
			ccc.redirectView();
			ccc.schoolCampus(school.id);
			ccc.viewContacts();
			for(Account c : ccc.campuses)
				c.isSelected__c = true;
				
			
			ccc.updateContact();
			ApexPages.currentPage().getParameters().put('delId', ct.id);
			//ccc.deleteContact();
				
				
				
			ApexPages.currentPage().getParameters().remove('ct');	
			ccc = new CampusContactsController();			
			ccc.setupCampusContact();	
			ccc.schoolCampus(school.id);
			ccc.viewContacts();
			for(Account c : ccc.campuses)
				c.isSelected__c = true;
			ccc.saveContact();
				
		}
		
	
		Test.stopTest(); 
        
    }
}