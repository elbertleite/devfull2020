global with sharing class contact_school_emails {
	
	public integer qttEmails{get; set;}
	private string contactId;
	
	global contact_school_emails(ApexPages.standardController controller){
		contactId = controller.getId();
		renderSearchField = true;
	}

	global contact_school_emails(String clientId){
		contactId = clientId;
		renderSearchField = true;
	}
	
	public static List<EmailDetails> emailDetails {get;set;}
	public static String emailBody {get;set;}	
	public EmailDetails downloadEmail {get;set;}	
	public boolean renderSearchField {get;set;}
	
	public without sharing class WithouSharingContactsDetails{
		
		public List<Contact> getContactDetails(set<String> accID ){
			
			List<Contact> result = new List<Contact>();
			result = [SELECT id, name, Email  FROM Contact WHERE id in:accID];
			return result;
		}
		
	}
	public String emailsJSON {
		get { 			
			if(emailsJSON == null){
				
				Contact ct = [Select id, Current_Agency__r.Global_Link__c from Contact where id = :contactId LIMIT 1];
			
				String instalmentsPrefix = IPFunctions.EmailInstalmentlFileNameS3(ct.Current_Agency__r.Global_Link__c, ct.Id);
				String courseEmails =  ct.Current_Agency__r.Global_Link__c + '/' + 'Clients/'+ct.Id+'/Courses/';	 	
								
				/* Get list of emails of the client on the organization bucket */
				S3Controller cS3 = new S3Controller();
				cS3.constructor(); //Generate Credentials	
				cS3.listBucket(Userinfo.getOrganizationId(), instalmentsPrefix, null, null, null);
											
				if(Test.isRunningTest()){	    	
			    	S3.ListEntry listEn = new S3.ListEntry();
			    	listEn.Key='Clients/test/S:-:0039000001bIxaeAAC:-:IP Australia Sydney - Head Office - Course Refund - Bruno N Test:-:22-02-2016 13:31:29:-:testest@gmail.com:-:instalmentID';		    	
			    	cs3.bucketList = new List<S3.ListEntry>{listEn};
		    	}
				
				map<DateTime, list<EmailDetails>> emailDetailsMap = new map<DateTime, list<EmailDetails>>();		
				set<String> accID = new set<String>();
				set<String> instIds = new set<String>();
				Map<String, Contact> mapID;
	
				emailDetails = new List<EmailDetails>();
				accID.add(contactId);
				integer count = 0;

				//Get all Client Invoices
				set<Id> invoices = new set<Id>();
				set<String> showPfs = new set<String>();
				set<String> showNet = new set<String>();


				for(client_course_instalment__c cci : [SELECT isPFS__c, School_Payment_Invoice__c, School_Payment_Invoice__r.School_Invoice_Number__c, School_Payment_Invoice__r.Preview_Link__c FROM client_course_instalment__c WHERE client_course__r.client__c = :ct.id AND Paid_To_School_On__c != NULL AND School_Payment_Invoice__r.Sent_Email_Receipt__c = true]){
					invoices.add(cci.School_Payment_Invoice__c);
					if(cci.isPFS__c)
						showPfs.add(cci.School_Payment_Invoice__c);
					else
						showNet.add(cci.School_Payment_Invoice__c);
				}

				/** Get emails School Payment Invoice **/
				for(Invoice__c inv : [SELECT Id, School_Invoice_Number__c, Preview_Link__c FROM Invoice__c WHERE Id in :invoices]){
					
					if(inv.Preview_Link__c != null){
						EmailDetails email = courseFileSubstring(EncodingUtil.urlDecode(inv.Preview_Link__c, 'UTF-8'), count);
						
						if(email!=null){
							accID.add(email.idEmployee);
							// instIds.add(email.instalmentId);

							email.invoice = inv.Id;
							email.invoiceNumber = inv.School_Invoice_Number__c;
							if(showPFS.contains(inv.Id))
								email.showPFS = true;
							if(showNet.contains(inv.Id))
								email.showNet = true;

							if(!emailDetailsMap.containsKey(email.dateTimeOrder))
								emailDetailsMap.put(email.dateTimeOrder, new list<EmailDetails>{email});
							else emailDetailsMap.get(email.dateTimeOrder).add(email);
						}
						count++;
					}

				}//end for

				/** END --  Get emails School Payment Invoice **/		
				
				/** Get emails from Instalments **/				
				cS3.listBucket(Userinfo.getOrganizationId(), instalmentsPrefix, null, null, null);
				if(cS3.bucketList != null && !cS3.bucketList.isEmpty()){		
					for(S3.ListEntry file : cS3.bucketList){
						
						EmailDetails email = filenameSubstring(file.key, count);
						
						if(email!=null){
							accID.add(email.idEmployee);
							instIds.add(email.instalmentId);
							
							if(!emailDetailsMap.containsKey(email.dateTimeOrder))
								emailDetailsMap.put(email.dateTimeOrder, new list<EmailDetails>{email});
							else emailDetailsMap.get(email.dateTimeOrder).add(email);
						}
						count++;
					}
				}
				/** END -- Get emails from Instalments **/
				
				
				/** Get all emails related to the course (Refund, Transfer, COE) **/				
				cS3.listBucket(Userinfo.getOrganizationId(), courseEmails, null, null, null);
				if(cS3.bucketList != null && !cS3.bucketList.isEmpty()){		
					for(S3.ListEntry file : cS3.bucketList){
						
						EmailDetails email = courseFileSubstring(file.key, count);
						
						if(email!=null){
							accID.add(email.idEmployee);
							
							if(!emailDetailsMap.containsKey(email.dateTimeOrder))
								emailDetailsMap.put(email.dateTimeOrder, new list<EmailDetails>{email});
							else emailDetailsMap.get(email.dateTimeOrder).add(email);
						}
						count++;
					}
				}
				/** END -- Get all emails related to the course (Refund, Transfer, COE) **/
				
				
				list<DateTime> sortList = new list<DateTime>(emailDetailsMap.keySet());
				sortList.sort();
				
				//Put it in DESC Order			
				if(qttEmails != null && qttEmails != 0){
					Integer qtt=0;
					for (Integer i = (sortList.size() - 1);(i >= 0 && qtt<qttEmails); i--){
						for(EmailDetails ed:emailDetailsMap.get(sortList[i])){
		            		emailDetails.add(ed);
						}				     		
						qtt++;
					}
				}else{
					for (Integer i = (sortList.size() - 1); i >= 0; i--)
						for(EmailDetails ed:emailDetailsMap.get(sortList[i]))
		            		emailDetails.add(ed);
				}
			
				mapID = new map<String, Contact>();
				
				WithouSharingContactsDetails ctDetails = new WithouSharingContactsDetails();	
				List<Contact> contactsResult = ctDetails.getContactDetails(accID);
				map<string, client_course_instalment__c> mapinstalments;
				if(!Test.isRunningTest()){	    
					mapinstalments = new map<string, client_course_instalment__c>([Select Id, Number__c, Client_Course__r.Course_Name__c, Client_Course__r.Campus_Name__c, Split_Number__c FROM client_course_instalment__c where id in: instIds]);
					mapinstalments.put(null, new client_course_instalment__c());
				}
				
				
				system.debug('instIds===>' + instIds);
				system.debug('mapinstalments===>' + mapinstalments);
				
				
				for(Contact c : contactsResult){
					mapID.put(c.Id, c);
				}
				
				Contact employee;
				for(EmailDetails email : emailDetails){
					
					if(mapiD.containsKey(email.idEmployee)){
						employee=mapID.get(email.idEmployee);	
						email.employeeName=employee.Name;
						email.employeeEmail=employee.Email;
					}
					if(!Test.isRunningTest() && mapinstalments.get(email.instalmentId) != null && email.instalmentId!=null){
						email.instNumber = string.valueOf(mapinstalments.get(email.instalmentId).Number__c);
						email.splitNumber = string.valueOf(mapinstalments.get(email.instalmentId).Split_Number__c);
						email.courseName = mapinstalments.get(email.instalmentId).Client_Course__r.Course_Name__c;
						email.campusName = mapinstalments.get(email.instalmentId).Client_Course__r.Campus_Name__c;
					}
				}
							
				emailsJSON=JSON.serialize(emailDetails);
			}		
			
			return emailsJSON;
		}		
		set;
	}
	
	public String emailContent {get;set;}
	public void viewEmail(){
		String fileName = String.valueOf(ApexPages.currentPage().getParameters().get('fileName'));
		
		system.debug('fileName===' + fileName);
		
		S3Controller cS3;
		cS3= new S3Controller();
		cS3.constructor(); //Generate Credentials			
		cs3.getObject(fileName, Userinfo.getOrganizationId());			
		
		if(!Test.isRunningTest())
			emailContent =EncodingUtil.base64Decode(cs3.resultSingleObject.Data).toString();
		else
			emailContent = 'my test';
	}
	
	@RemoteAction
	global static String openEmail(String fileName){
		//String key = Apexpages.currentpage().getparameters().get('paramKey');
		
		S3Controller cS3;
		cS3= new S3Controller();
		cS3.constructor(); //Generate Credentials			
		cs3.getObject(fileName, Userinfo.getOrganizationId());			
		
		if(!Test.isRunningTest())
			emailBody =EncodingUtil.base64Decode(cs3.resultSingleObject.Data).toString();
		else
			emailBody = 'my test';
			
		return emailBody;
	}
	
	public String emailsFromClient(String idClient){
		
		Apexpages.currentPage().getParameters().put('id',idClient);
		
		return emailsJSON;
	}
	
	
	/*
     * Get all the email details and body using the fileName + client Id
     */
	public void downloadEmailData(String fileName){
		//Get email Data: From, To, Subject, Sent Date
		downloadEmail = filenameSubstring(fileName, null);
		
		//Get email body		
		downloadEmail.bodyStr= openEmail(fileName);
	}
	
	public EmailDetails courseFileSubstring(String fileName, Integer count){
		EmailDetails email;
		system.debug('fileName: ' + fileName);
		
		list<string> splitKey;
		list<string> contactId;
		
		Boolean isIpS3;
		if(fileName.indexOf('Clients/') > -1)
			splitKey = new list<string>(fileName.split('Clients/'));
		else if(fileName.indexOf('Payment/Receipt/') > -1)
			splitKey = new list<string>(fileName.split('Payment/Receipt/'));

		system.debug('splitKey===>' + splitKey);
		
		contactId=splitKey[1].split('/');						
		
		String key= fileName.substring(fileName.lastIndexOf('/')+1, fileName.length());
		list<string> s = new list<string>(key.split(':-:'));
		system.debug('s====> ' + s);
		if(s.size()>1){
			email = new EmailDetails();
			email.virtualId='email-'+count;
			email.key=fileName;
			email.ipMail = false;
			
			if(s[0].equals('R')){
				email.isFromEmployee=false;
			}
			if(s[0].equals('S')){
				email.isFromEmployee=true;
			}
			
			email.idClient=contactId[0];
			email.idEmployee=s[1];	
			email.subject=s[2];
			email.dateTimeOrder = parse(s[3]);
			email.dateSent= email.dateTimeOrder.format();
			email.dateOnlySent=  email.dateTimeOrder.date().format();

			if(fileName.indexOf('Clients/') > -1)
				email.toAddress = s[4];
			else if(fileName.indexOf('Payment/Receipt/') > -1){
				email.toAddress = s[5].substringBefore('?');
			}
			
		}
			return email;
	}
	
	public EmailDetails filenameSubstring(String fileName, Integer count){
		EmailDetails email;
		system.debug('fileName: ' + fileName);
		
		list<string> splitKey;
		list<string> contactId;
		
		Boolean isIpS3;
		splitKey = new list<string>(fileName.split('Clients/'));

		contactId=splitKey[1].split('/');						
		
		String key= fileName.substring(fileName.lastIndexOf('/')+1, fileName.length());
		list<string> s = new list<string>(key.split(':-:'));
		if(s.size()>1){
			email = new EmailDetails();
			email.virtualId='email-'+count;
			email.key=fileName;
			email.ipMail = false;
			
			if(s[0].equals('R')){
				email.isFromEmployee=false;
			}
			if(s[0].equals('S')){
				email.isFromEmployee=true;
			}
			
			email.idClient=contactId[0];
			email.idEmployee=s[1];	
			email.subject=s[2];
			email.dateTimeOrder = parse(s[3]);
			//list<string> sDate = new list<string>(s[4].split(' '));
			email.dateSent= email.dateTimeOrder.format();
			email.dateOnlySent=  email.dateTimeOrder.date().format();
			email.toAddress = s[5];
			email.instalmentId = s[4];
			
		}
			return email;
	}
	
	public static Datetime parse(String strDate){
		
		system.debug(Logginglevel.INFO, '$ strDate: ' + strDate);
		
		string year = strDate.substring(6, 10);
		string month = strDate.substring(3, 5);
		string day = strDate.substring(0,2);
		string hour = strDate.substring(11,13);
		string minute = strDate.substring(14,16);
		string second = strDate.substring(17,19);
		
		Datetime dt;
		
		try {
			dt = Datetime.valueOf(year + '-' + month + '-' + day + ' ' + hour + ':' + minute +  ':' + second);
		} catch(Exception e){
			system.debug('ERROR PARSING ' + strDate + ': ' + e.getMessage());
			dt = Datetime.now();
		}
		
		return dt;
	}
	
	
	public class emailDetails{
		public String virtualId {get;set;}
		public String key {get; set;}
		public String subject {get; set;}
		public Blob body {get; set;}
		public String bodyStr {get; set;}
		public String dateSent{get;set;}
		public String dateOnlySent{get;set;}
		public boolean isFromEmployee {get;set;}
		public String idClient {get;set;}
		public String idEmployee {get;set;}
		public String toAddress {get;set;}
		public String employeeName {get;set;}
		public String employeeEmail {get;set;}
		public boolean ipMail {get;set;}
		public String instalmentId{get;set;}
		public String courseName {get;set;}
		public String campusName {get;set;}
		public String instNumber {get;set;}
		public String invoice {get;set;}
		public String invoiceNumber {get;set;}
		public DateTime dateTimeOrder {get;set;}
		public String splitNumber {get;set;}
		public boolean showPFS {get;set;}
		public boolean showNet {get;set;}

		public emailDetails(){
			showPFS = false;
			showNet = false;
		}
		
	}

}