@isTest
private class pmc_new_classes_test4 {
    static testMethod void myUnitTest() {
        TestFactory tf = new TestFactory();
        
        Account agency = tf.createAgency();
        Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
        
        Map<String,String> recordTypes = new Map<String,String>();
        for( RecordType r :[select id, name from recordtype where isActive = true] ){
            recordTypes.put(r.Name,r.Id);
        }

        Contact client = tf.createClient(agency);

        Account schoolGroup = tf.createSchoolGroup();
        Account school = tf.createSchool();

        Account campus = tf.createCampus(school, agency);

        Apexpages.currentPage().getParameters().put('page', 'page');
        Apexpages.currentPage().getParameters().put('idRedirect', school.id);
        Apexpages.currentPage().getParameters().put('idGroup', schoolGroup.id);
        createNewSchool cns = new createNewSchool();
        cns.openCloseModalNewSchoolGroup();
        cns.getCurrencies();
        cns.getSchoolCampusCategories();
        cns.saveSchoolGroup();
        cns.saveSchool();
        cns.updateFieldDisabledReason();
        
        //Apexpages.currentPage().getParameters().put('campus', campus.id);
        createNewCampus cnc = new createNewCampus();
        cnc.loadCampusForUpdate(campus.id);
        cnc.refreshAddress();
        cnc.getCurrencies();
        cnc.getSocialNetworks();
        cnc.getSchoolCampusCategories();
        cnc.saveCampus();
        cnc.updateFieldDisabledReason();
	}
}