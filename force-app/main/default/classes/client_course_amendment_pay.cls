public with sharing class client_course_amendment_pay {


	public client_course_instalment__c instalment {get;set;}
	public decimal totalPay {get;set;}
	public boolean showError {get;set;}
	public String errorMsg {get;set;}

	private decimal currentDepositUsed = 0;
	private decimal currentCourseCredit = 0;
	private decimal totalpaidAgencyCurrency = 0;

	public client_course__c courseCredit{get; set;}

	/** Constructor **/
	public client_course_amendment_pay(ApexPages.StandardController controller){

		retrieveInstalment(controller.getId());

		totalPay = instalment.Amendment_Total_Charge_Client__c;

		retrieveDeposit(instalment.client_course__r.Client__c);

		system.debug('instalment.client_course__r.School_Id__c==' + instalment.client_course__r.School_Id__c);

		try{
			courseCredit = [Select id, Credit_Related_To__c, Credit_Courses__c, School_Name__c, Credit_Used_By__c, Credit_Value__c, Credit_Available__c, Deposit_Description__c FROM client_course__c WHERE Client__c = :instalment.client_course__r.Client__c and isCourseCredit__c = true and Credit_Valid_For_School__c = :instalment.client_course__r.School_Id__c and Credit_Available__c > 0 limit 1];
			currentCourseCredit = courseCredit.Credit_Available__c;
		}catch(Exception e){

		}
	}

	public Boolean isPDS{get{if(isPDS == null) isPDS = false; return isPDS;} Set;}
	public Boolean isPCS{get{if(isPCS == null) isPCS = false; return isPCS;} Set;}

    /** Retrieve Instalment **/
    public void retrieveInstalment(String id){
    	instalment = [SELECT
						Split_Number__c, isCancelled__c, Received_Date__c, Tuition_Value__c, Extra_Fee_Value__c, Extra_Fee_Discount_Value__c, Amendment_Tuition_Balance__c, Amendment_Fee_Balance__c, isPDS__c, Amendment_Total_Charge_Client__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, Instalment_Value__c, Commission_Tax_Rate__c, client_course__r.Course_Country__c, client_course__r.Campus_Course__r.Campus__r.Parent.isTaxDeductible__c, Total_Paid_School_Credit__c,
						client_course__c,
						Client_course__r.Client__r.Owner__r.AccountId,
						Client_course__r.Client__r.Owner__r.Contact.Department__c,
						client_course__r.Client__c,
						client_course__r.Client__r.name,
						client_course__r.Course_Name__c,
						client_course__r.CurrencyIsoCode__c,
						client_course__r.School_Name__c,
						client_course__r.Campus_Name__c,
						client_course__r.Total_Paid__c,
						client_course__r.School_Id__c,
					    client_course__r.Enroled_by_Agency__c,
					    client_course__r.Total_Paid_Deposit__c,
						client_course__r.Total_Paid_PDS__c,
						client_course__r.Total_Paid_Credit__c,

						Original_Instalment__r.Discount__c,
						Original_Instalment__r.Number__c,
						Original_Instalment__r.Split_Number__c,
						Original_Instalment__r.Tuition_Value__c,
						Original_Instalment__r.Extra_Fee_Value__c,
						Original_Instalment__r.Extra_Fee_Discount_Value__c

		 				FROM client_course_instalment__c WHERE id = :id];

			/** Instalment Currency **/
			retrieveMainCurrencies();
			instalment.Agency_Currency__c = fin.currentUser.Contact.Account.Account_Currency_Iso_Code__c;
			recalculateRate();
    }


    /** Add Payment **/
    public pageReference addPaymentValue(){

		if(totalPay >0  && newPayDetails.Value__c <= 0)
			newPayDetails.Value__c.addError('Value need to be bigger than 0');

		if(newPayDetails.Payment_Type__c != 'pds' && newPayDetails.Payment_Type__c != 'offShore' && (newPayDetails.Value__c > totalPay || newPayDetails.Value__c > (totalPay - totalpaid))){ //&& newPayDetails.Payment_Type__c != 'Covered by Agency'
			newPayDetails.Value__c.addError('Value cannot be bigger than ' + (totalPay - totalpaid));
			return null;
		}

		if(newPayDetails.Payment_Type__c == 'Deposit'){
			if((clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c - clientDeposit.Deposit_Total_Refund__c) >= 0 && newPayDetails.Value__c <= (clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c - clientDeposit.Deposit_Total_Refund__c)){
				clientDeposit.Deposit_Total_Used__c += newPayDetails.Value__c;
				instalment.client_course__r.Total_Paid_Deposit__c += newPayDetails.Value__c;
			}else {
				newPayDetails.Value__c.addError('Deposit Value Invalid');
				return null;
			}
		}

		if(newPayDetails.Date_Paid__c > system.today()){
			newPayDetails.Date_Paid__c.addError('Date paid cannot be future date. ');
			return null;
		}

		if(newPayDetails.Date_Paid__c < Date.today().addMonths(-2)){
			newPayDetails.Date_Paid__c.addError('Date paid cannot be lower than 2 months. ');
			return null;
		}

		if(newPayDetails.Payment_Type__c == 'School Credit'){
			if(courseCredit.Credit_Available__c > 0 && newPayDetails.Value__c <= courseCredit.Credit_Available__c){
				courseCredit.Credit_Available__c -= newPayDetails.Value__c;
				instalment.client_course__r.Total_Paid_Credit__c += newPayDetails.Value__c;
			}else {
				newPayDetails.Value__c.addError('Credit Value Invalid');
				return null;
			}
		}


		if(newPayDetails.Payment_Type__c == 'Pds'|| newPayDetails.Payment_Type__c == 'notContacted'|| newPayDetails.Payment_Type__c == 'Pcs'|| newPayDetails.Payment_Type__c == 'offShore'){
			listPayDetails.clear();
			listPayDetails.add(new paymentValue(countNumberPaymentValues++, newPayDetails));
			totalpaid = newPayDetails.Value__c;
			newPayDetails.Agency_Currency_Value__c = newPayDetails.Value__c;
			instalment.client_course__r.Total_Paid__c += newPayDetails.Value__c;
			clientDeposit.Deposit_Total_Used__c = currentDepositUsed;

			if(newPayDetails.Payment_Type__c == 'notContacted'){
				instalment.Client_not_Contacted__c = true;
				newPayDetails.Payment_Type__c = 'Pds'; // Change type to follow the PDS rules
			}

			if(newPayDetails.Payment_Type__c == 'Pds'){
				instalment.isPDS__c = true;
				instalment.isPFS__c = false;
				instalment.isPCS__c = false;
				isPDS = true;
			}
			else if(newPayDetails.Payment_Type__c == 'Pcs'){
				instalment.isPDS__c = false;
				instalment.isPFS__c = false;
				instalment.isPCS__c = true;
				isPCS = true;
			}
			/** 
			else if(newPayDetails.Payment_Type__c == 'offShore'){
				hasOffShorePayment = true;
				instalment.isPaidOffShore__c = true;
			}
			 **/

		}else{

			//Relate deposit ID to the payment
			if(newPayDetails.Payment_Type__c=='deposit'){
				newPayDetails.Paid_From_Deposit__c = clientDeposit.id;
			}

			//Relate school credit ID to the payment
			if(newPayDetails.Payment_Type__c=='School Credit'){
				newPayDetails.Related_Credit__c = courseCredit.id;
			}

			boolean isSameTypeDate = false;
			for(integer i = 0; i < listPayDetails.size(); i++)
				if(listPayDetails[i].instalmentPayment.Payment_Type__c == newPayDetails.Payment_Type__c && listPayDetails[i].instalmentPayment.Date_Paid__c == newPayDetails.Date_Paid__c){

					listPayDetails[i].instalmentPayment.Value__c += newPayDetails.Value__c;
					instalment.client_course__r.Total_Paid__c += newPayDetails.Value__c;
					totalpaid += newPayDetails.Value__c;
					isSameTypeDate = true;
					break;

					//============	CONVERT PAYMENT TO AGENCY CURRENCY ===================================
					if(instalment.Agency_Currency_Rate__c < 0)
						newPayDetails.Agency_Currency_Value__c = newPayDetails.Value__c / instalment.Agency_Currency_Rate__c.setScale(6);
					else newPayDetails.Agency_Currency_Value__c = newPayDetails.Value__c * instalment.Agency_Currency_Rate__c.setScale(6);

					if(instalment.Instalment_Value__c - totalpaid == 0){
						newPayDetails.Agency_Currency_Value__c = instalment.Agency_Currency_Value__c.setScale(2) - totalpaidAgencyCurrency;
					}
					listPayDetails[i].instalmentPayment.Agency_Currency_Value__c += newPayDetails.Agency_Currency_Value__c.setScale(2);
					totalpaidAgencyCurrency += newPayDetails.Agency_Currency_Value__c.setScale(2);
				}

			if(!isSameTypeDate){
				listPayDetails.add(new paymentValue(countNumberPaymentValues++, newPayDetails));
				totalpaid += newPayDetails.Value__c;
				instalment.client_course__r.Total_Paid__c += newPayDetails.Value__c;

				//============	CONVERT PAYMENT TO AGENCY CURRENCY ===================================
				if(instalment.Agency_Currency_Rate__c < 0)
					newPayDetails.Agency_Currency_Value__c = newPayDetails.Value__c / instalment.Agency_Currency_Rate__c.setScale(6);
				else newPayDetails.Agency_Currency_Value__c = newPayDetails.Value__c * instalment.Agency_Currency_Rate__c.setScale(6);

				if(instalment.Instalment_Value__c - totalpaid == 0){
					newPayDetails.Agency_Currency_Value__c = instalment.Agency_Currency_Value__c.setScale(2) - totalpaidAgencyCurrency;
				}
				totalpaidAgencyCurrency += newPayDetails.Agency_Currency_Value__c.setScale(2);
			}
		}

		newPayDetails = new client_course_instalment_payment__c();
		newPayDetails.Date_Paid__c = System.today();
		newPayDetails.Payment_Type__c = '';
		newPayDetails.client_course_instalment__c = instalment.id;
		newPayDetails.client__c = instalment.client_course__r.Client__c;
		newPayDetails.isAmendment__c = true;

		instalmentSaved = false;

		return null;
	}


	/** Remove Payment **/
	public pageReference removePaymentValue(){
		integer index = integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
		system.debug('index====>' + index);
		system.debug('listPayDetails.size()====>' + listPayDetails.size());


		for(integer i = 0; i < listPayDetails.size(); i++){
			if(index == listPayDetails[i].payIndex){
				if(listPayDetails[i].instalmentPayment.Payment_Type__c == 'Pds'){
					isPDS = false;
					instalment.Client_not_Contacted__c = false;
					instalment.client_course__r.Total_Paid_PDS__c -= listPayDetails[i].instalmentPayment.Value__c;
				}
				if(listPayDetails[i].instalmentPayment.Payment_Type__c == 'Pcs'){
					isPCS = false;
					instalment.client_course__r.Total_Paid_PDS__c -= listPayDetails[i].instalmentPayment.Value__c;
				}
				totalpaid -= listPayDetails[i].instalmentPayment.Value__c;
				if(listPayDetails[i].instalmentPayment.Payment_Type__c == 'Deposit'){
					clientDeposit.Deposit_Total_Used__c -= listPayDetails[i].instalmentPayment.Value__c;
					instalment.client_course__r.Total_Paid_Deposit__c -= listPayDetails[i].instalmentPayment.Value__c;
				}
				if(listPayDetails[i].instalmentPayment.Payment_Type__c == 'School Credit'){
					courseCredit.Credit_Available__c += listPayDetails[i].instalmentPayment.Value__c;
					instalment.client_course__r.Total_Paid_Credit__c -= listPayDetails[i].instalmentPayment.Value__c;
				}

				/** 
				if(listPayDetails[i].instalmentPayment.Payment_Type__c == 'offShore'){
					hasOffShorePayment = false;
					instalment.isPaidOffShore__c = false;
				}
				 **/

				instalment.client_course__r.Total_Paid__c -= listPayDetails[i].instalmentPayment.Value__c;
				totalpaidAgencyCurrency -= listPayDetails[i].instalmentPayment.Agency_Currency_Value__c;
				listPayDetails.remove(i);
				break;
			}
		}//end for
		instalmentSaved = false;

		return null;
	}


	/** Save Payment **/
	private Boolean isGeneralUser {get;set;}
	public pageReference savePayment(){
		Savepoint sp = Database.setSavepoint();
		//try{

		errorMsg = '';

		 	/** Double Check if instalment is paid **/

		 	Client_Course_Instalment__c updatedinstalment = [Select Received_Date__c, Received_by__r.Name from Client_Course_Instalment__c where id = :instalment.id limit 1];

			if(updatedinstalment.Received_Date__c!=null){
				errorMsg = 'This amendment has been already paid by ' +updatedinstalment.Received_by__r.Name + ' on ' + updatedinstalment.Received_Date__c.format() + '. Please refresh your page.';
				return null;
			}


			/** Offshore
			if(hasOffShorePayment){

		        if(selectedUser == null || selectedUser =='noUser'){
		            errorMsg += '<span style="display:block">Please fill the field <b>Payment Received By</b>.</span>';
		        }

		        if(receivedOn.Expected_Travel_Date__c == null){
		            errorMsg += '<span style="display:block">Please fill the field <b>Payment Received On</b>.</span>';
		        }

		        if(receivedOn.Expected_Travel_Date__c > system.today()){
		        	errorMsg += '<span style="display:block"><b>Payment Received On</b> cannot be future date.</span>';
				}
			}**/

			if(errorMsg.length()>0){

				return null;
			}

			else{

				if(clientDeposit != null && clientDeposit.id != null && currentDepositUsed != null && currentDepositUsed != clientDeposit.Deposit_Total_Value__c){
					clientDeposit.Deposit_Total_Available__c = clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c;
					update clientDeposit;
				}

				if(courseCredit != null && currentCourseCredit != courseCredit.Credit_Available__c)
					update courseCredit;



				list<client_course_instalment_payment__c> paymentList = new list<client_course_instalment_payment__c>();

				String receivedByAgency;
				String receivedByDepartment;

				isGeneralUser = FinanceFunctions.isGeneralUser(userAgency.General_User_Agencies__c, instalment.Client_course__r.Client__r.Owner__r.AccountId);
				
				if(!isGeneralUser){
					receivedByAgency = userAgency.Contact.AccountId;
					receivedByDepartment = userAgency.Contact.Department__c;
				}
				else{
					receivedByAgency = instalment.Client_course__r.Client__r.Owner__r.AccountId;
					receivedByDepartment = instalment.Client_course__r.Client__r.Owner__r.Contact.Department__c;
				}

				boolean isReconciled = true; //Used to automatically reconcile Third Party Payments 

				for(paymentValue lpd:listPayDetails){

					lpd.instalmentPayment.Received_By__c = UserInfo.getUserId();
					lpd.instalmentPayment.Received_On__c = DateTime.now();
					lpd.instalmentPayment.Received_By_Agency__c = receivedByAgency;

					if(lpd.instalmentPayment.Payment_Type__c.toLowerCase() != 'payment 3rd party')
						isReconciled = false;
					else{
						lpd.instalmentPayment.Confirmed_By__c = UserInfo.getUserId();
						lpd.instalmentPayment.Confirmed_Date__c = DateTime.now();
						lpd.instalmentPayment.Confirmed_By_Agency__c = receivedByAgency;
					}

					paymentList.add(lpd.instalmentPayment);

					if(lpd.instalmentPayment.Payment_Type__c=='Covered by Agency'){

						lpd.instalmentPayment.Confirmed_By__c = userAgency.id;
						lpd.instalmentPayment.Confirmed_Date__c = system.now();
						lpd.instalmentPayment.Confirmed_By_Agency__c = receivedByAgency;

						instalment.Covered_By_Agency__c = lpd.instalmentPayment.Value__c;
						instalment.Confirmed_By__c = userAgency.id;
						instalment.Confirmed_Date__c = system.now();
					}
					else if(lpd.instalmentPayment.Payment_Type__c.toLowerCase() == 'pds'){
						// instalment.client_course__r.Total_Paid__c -= instalment.Client_Scholarship__c;
						isPDS = true;
					}

					else if(lpd.instalmentPayment.Payment_Type__c.toLowerCase() == 'pcs'){
						// instalment.client_course__r.Total_Paid__c -= instalment.Client_Scholarship__c;
						isPCS = true;
					}

					else if(lpd.instalmentPayment.Payment_Type__c.toLowerCase() == 'school credit')
						instalment.Total_Paid_School_Credit__c += lpd.instalmentPayment.Value__c;
				}//end for

				insert paymentList;

				if(isReconciled){
					instalment.Confirmed_By__c = Userinfo.getuserId();
					instalment.Confirmed_Date__c = System.now();
				}

				//if(!hasOffShorePayment){
					instalment.Received_By_Agency__c = receivedByAgency;
					instalment.Received_by__c = Userinfo.getUserId();
					instalment.Received_by_Department__c = receivedByDepartment;
					instalment.Received_Date__c = System.now();



				/** }else{
					instalment.Received_By_Agency__c = selectedAgency;
					instalment.Received_by__c = selectedUser;
					instalment.Received_by_Department__c = userDept.get(selectedUser);
					instalment.Received_Date__c = DateTime.newInstance(receivedOn.Expected_Travel_Date__c.year(), receivedOn.Expected_Travel_Date__c.month(), receivedOn.Expected_Travel_Date__c.day(), 0, 0, 0);
				}**/

				instalment.Status__c = 'Paid';

				String typePay;
				if(instalment.isPDS__c){
					typePay = 'Marked as PDS';
				}
				
				else if(instalment.isPCS__c)
					typePay = 'Marked as PCS';
				
				else typePay = 'Paid';

				if(instalment.instalment_activities__c != null)
					instalment.instalment_activities__c += typePay + ';;-;;' + '-' +';;' + System.now().format('yyyy-MM-dd HH:mm:ss') + ';;-;;-;;' + UserInfo.getName() + '!#!';
				else instalment.instalment_activities__c = typePay +';;-;;' + '-' +';;' + System.now().format('yyyy-MM-dd HH:mm:ss') + ';;-;;-;;' + UserInfo.getName() + '!#!';


				/** Check if it will be a shared commission /
				String userAccount = [Select Contact.AccountId from User where id =:instalment.Received_by__c limit 1].Contact.AccountId;

				if(instalment.client_course__r.Enroled_by_Agency__c!= null && (instalment.client_course__r.Enroled_by_Agency__c != userAccount))
					instalment.isSharedCommission__c = true;
				/**/

				if((instalment.Commission_Tax_Rate__c != null && instalment.Commission_Tax_Rate__c > 0 && instalment.client_course__r.Course_Country__c != fin.currentUser.Contact.Account.billingCountry) 
					|| instalment.client_course__r.Campus_Course__r.Campus__r.Parent.isTaxDeductible__c){ //used to set currency rate for courses enrolled destination with with payment offshore to tax rate = 0
					instalment.Commission_Tax_Rate__c = 0;
				} else if((instalment.Commission_Tax_Rate__c == null || instalment.Commission_Tax_Rate__c == 0) && instalment.client_course__r.Course_Country__c == fin.currentUser.Contact.Account.billingCountry){ //used to set currency rate for courses enrolled offshore with no tax rate
					retrieveTaxRate();
					instalment.Commission_Tax_Rate__c = taxDetails.Tax_Rate__c;
				}

				update instalment;

				if(instalment.Discount__c!=null && instalment.Discount__c>0)
					instalment.client_course__r.Total_Paid__c += instalment.Discount__c;


				update instalment.client_course__r;

				//Update course and instalments
				client_course_new clientCourseNew = new client_course_new(instalment.client_course__r.Client__c, instalment.client_course__c);
				clientCourseNew.updateCourseInstalmentsValue(clientCourseNew.enrollCourse);

			//}catch(Exception e){
			//	Database.rollback(sp);
			//}
			showError = true;
			return null;
			}
	}




	private User userAgency = [Select General_User_Agencies__c, Contact.AccountId, Contact.Account.ParentId, Contact.Department__c from user where id = :UserInfo.getUserId() limit 1];
	public Boolean instalmentSaved{get{if(instalmentSaved == null) instalmentSaved = true; return instalmentSaved;} Set;}
	private integer countNumberPaymentValues = 0;

    public decimal totalpaid{
		get{
			if(totalpaid == null)
				totalpaid = 0;
			return totalpaid;
		}
		set;
	}


    public list<paymentValue> listPayDetails{
		get{
			if(listPayDetails == null){
				listPayDetails = new list<paymentValue>();
			}
			return listPayDetails;}set;}


    public class paymentValue{
		public integer payIndex{get; set;}
		public client_course_instalment_payment__c instalmentPayment{get; set;}
		public paymentValue(){}
		public paymentValue(integer depositIndex, client_course_instalment_payment__c instalmentPayment){
			this.payIndex = depositIndex;
			this.instalmentPayment = instalmentPayment;
		}
	}


	public client_course_instalment_payment__c newPayDetails{
		get{
			if(newPayDetails == null && instalment != null){
				newPayDetails = new client_course_instalment_payment__c();
				newPayDetails.client_course_instalment__c = instalment.id;
				newPayDetails.client__c = instalment.client_course__r.Client__c;
				newPayDetails.isAmendment__c = true;
			}
			return newPayDetails;}set;}


	public client_course__c clientDeposit{get; set;}

	private void retrieveDeposit(string contactId){
		try{
		 	clientDeposit = [Select Deposit_Description__c, Deposit_Total_Used__c, Deposit_Total_Value__c, Deposit_Total_Available__c, Deposit_Type__c, Deposit_Value_Track__c, Id, client__c, Client__r.name, Deposit_Total_Refund__c, (SELECT Id, Value__c, Received_By__r.Name, Received_On__c, Received_By_Agency__r.Name, Payment_Type__c, Date_Paid__c FROM client_course_payments_deposit__r)
 							FROM client_course__c where client__c = :contactId and isDeposit__c = true and Deposit_Total_Available__c > 0];
		}catch(Exception e){
			clientDeposit = new client_course__c();
		}
	}

	public list<selectOption> depositListType{
		get{
			if(depositListType==null){
				depositListType = new list<selectOption>();
				depositListType.add(new SelectOption('','Select Type'));

				if(clientDeposit.Deposit_Total_Used__c == null)
					clientDeposit.Deposit_Total_Used__c = 0;

				// && instalment.client_course__r.Enrolment_Date__c != null
				if(clientDeposit.Deposit_Total_Value__c != null && (clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c - clientDeposit.Deposit_Total_Refund__c) > 0 ){
					depositListType.add(new SelectOption('Deposit','Deposit'));
					newPayDetails.Payment_Type__c = 'Deposit';
					newPayDetails.Date_Paid__c = System.today();
					if(clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c - clientDeposit.Deposit_Total_Refund__c > instalment.Instalment_Value__c)
						newPayDetails.Value__c = instalment.Instalment_Value__c;
					else newPayDetails.Value__c = clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c - clientDeposit.Deposit_Total_Refund__c;
				}

				if(courseCredit != null && courseCredit.Credit_Available__c > 0){
					depositListType.add(new SelectOption('School Credit','School Credit'));

					if(newPayDetails.Payment_Type__c== null){
						newPayDetails.Payment_Type__c = 'School Credit';
						newPayDetails.Date_Paid__c = System.today();
						if(courseCredit.Credit_Available__c > instalment.Instalment_Value__c)
							newPayDetails.Value__c = instalment.Instalment_Value__c;
						else newPayDetails.Value__c = courseCredit.Credit_Available__c;
					}
				}

				depositListType.add(new SelectOption('Pds','PDS'));

				if(Schema.sObjectType.User.fields.Finance_Allow_Pay_PCS__c.isAccessible())
					depositListType.add(new SelectOption('Pcs','PCS'));


				Schema.DescribeFieldResult fieldResult = client_course__c.Deposit_Type__c.getDescribe();
				List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
				for( Schema.PicklistEntry f : ple)
					if(f.getLabel()!='offShore')
						depositListType.add(new SelectOption(f.getLabel(), f.getValue()));
				//if(Schema.sObjectType.User.fields.Finance_Create_Amendment__c.isAccessible())
				//	depositListType.add(new SelectOption('Covered by Agency', 'Covered by Agency'));
				depositListType.sort();
			}
			return depositListType;}set;}



	//================CURRENCY=============================
	public List<SelectOption> MainCurrencies{get; set;}
	public Datetime currencyLastModifiedDate {get;set;}
	public void retrieveMainCurrencies(){

	    MainCurrencies = fin.retrieveMainCurrencies();
        currencyLastModifiedDate = fin.currencyLastModifiedDate;

	}
	private financeFunctions fin = new financeFunctions();
	public void recalculateRate(){
		fin.convertCurrency(instalment, instalment.client_course__r.CurrencyIsoCode__c, instalment.Instalment_Value__c, null, 'Agency_Currency__c', 'Agency_Currency_Rate__c', 'Agency_Currency_Value__c');
	}

	private Account taxDetails;
	IPFunctions.SearchNoSharing searchNoSharing = new IPFunctions.SearchNoSharing();

	private void retrieveTaxRate(){
		String sql = 'Select Id, Parent.name, Name, Tax_Name__c, Tax_Rate__c from Account WHERE RecordType.name = \'city\' and name = \'' + instalment.client_course__r.Course_City__c + '\'  and Parent.name = \'' + instalment.client_course__r.Course_Country__c + '\' limit 1';

		if(instalment.client_course__r.Course_Country__c == fin.currentUser.Contact.Account.billingCountry && !instalment.client_course__r.Campus_Course__r.Campus__r.Parent.isTaxDeductible__c){ //used to avoid offshore enrolments to claim tax and schools that do not pay tax to agency
			try{
				taxDetails = searchNoSharing.NSAccount(sql);

				if(taxDetails.Tax_Rate__c== null)
					taxDetails.Tax_Rate__c= 0;

			}catch(Exception e){
				//No tax found
				taxDetails = new Account(Tax_Name__c = '', Tax_Rate__c= 0);
			}
		}else{
			taxDetails = searchNoSharing.NSAccount(sql);
			taxDetails = new Account(Tax_Name__c = taxDetails.Tax_Name__c, Tax_Rate__c= 0);
		}
	}
}