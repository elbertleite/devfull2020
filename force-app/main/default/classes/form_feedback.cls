public class form_feedback{

	public Web_Search_Feedback__c newFeedback{get; Set;}
	
	public form_feedback(){
		newFeedback = new Web_Search_Feedback__c();
		newFeedback.Web_Search__c = ApexPages.currentPage().getParameters().get('sid');
		
	}
	
	public pageReference save(){
		insert newFeedback;
		return null;
	}
	
	
	public List<SelectOption> getDesign_appearance() {
 	 	List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult F = Web_Search_Feedback__c.Design_appearance__c.getDescribe();
		List<Schema.PicklistEntry> P = F.getPicklistValues();
 	 	for (Schema.PicklistEntry lst:P){
			options.add(new SelectOption(lst.getLabel(),lst.getValue()));
		}
 	 	return options;
  	}
	
	public List<SelectOption> getInformation_Quality() {
 	 	List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult F = Web_Search_Feedback__c.Information_Quality__c.getDescribe();
		List<Schema.PicklistEntry> P = F.getPicklistValues();
 	 	for (Schema.PicklistEntry lst:P){
			options.add(new SelectOption(lst.getLabel(),lst.getValue()));
		}
 	 	return options;
  	}
	
	public List<SelectOption> getOverall_Experience() {
 	 	List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult F = Web_Search_Feedback__c.Overall_Experience__c.getDescribe();
		List<Schema.PicklistEntry> P = F.getPicklistValues();
 	 	for (Schema.PicklistEntry lst:P){
			options.add(new SelectOption(lst.getLabel(),lst.getValue()));
		}
 	 	return options;
  	}

	public List<SelectOption> getPotential_to_improve_your_operations() {
 	 	List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult F = Web_Search_Feedback__c.Potential_to_improve_your_operations__c.getDescribe();
		List<Schema.PicklistEntry> P = F.getPicklistValues();
 	 	for (Schema.PicklistEntry lst:P){
			options.add(new SelectOption(lst.getLabel(),lst.getValue()));
		}
 	 	return options;
  	}
	
	public List<SelectOption> getUser_friendly() {
 	 	List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult F = Web_Search_Feedback__c.User_friendly__c.getDescribe();
		List<Schema.PicklistEntry> P = F.getPicklistValues();
 	 	for (Schema.PicklistEntry lst:P){
			options.add(new SelectOption(lst.getLabel(),lst.getValue()));
		}
 	 	return options;
  	}
	
	
	
}