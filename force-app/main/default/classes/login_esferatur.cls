global with sharing class login_esferatur {
    global String username {get; set;}
    global String password {get; set;}
    global login_esferatur () {}
     global PageReference forwardTologin_esferatur() {
        return new PageReference( '/login_esferatur');
    }
    global PageReference login() {
        return Site.login(username, password, null); 
    } 
}