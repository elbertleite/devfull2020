public without sharing class lwcDashboardSetting {
    
    public class wrpGeneric {
        @AuraEnabled public String label {get; set;}
        @AuraEnabled public string value {get; set;}

        public wrpGeneric(String label, String value) {
            this.label = label;           
            this.value = value;            
        }       
    } 

    private class wrpDashReports {
        @AuraEnabled private String id {get; set;}
        @AuraEnabled private String name {get; set;}
        @AuraEnabled private string period {get; set;}
        @AuraEnabled private string category {get; set;}

        private wrpDashReports(string id, string name, string period, string category) {
            this.id = id;   
            this.name = name;           
            this.period = period;  
            this.category = category;          
        }       
    }

    public class wrpDashSettings {
        @AuraEnabled public String id {get; set;}
        @AuraEnabled public string name {get; set;}
        @AuraEnabled public string depId {get; set;}
        @AuraEnabled public string depName {get; set;}
        @AuraEnabled public List<wrpDashReports> reports {get; set;}

        public wrpDashSettings(String id, String name, String depId, String depName, List<wrpDashReports> reports) {
            this.id = id;           
            this.name = name; 
            this.depId = depId;           
            this.depName = depName; 
            this.reports = new List<wrpDashReports>();
            this.reports.addAll(reports);           
        }       
    }

    @AuraEnabled(cacheable=true)
    public static list<Department__c> getDepartmentTargets(string selectedAgency){

        list<Department__c> dp = new list<Department__c>();            
        return [SELECT Id, Name, Department_Sales_Target__c FROM Department__c WHERE Agency__c = :selectedAgency AND Inactive__c = false ORDER BY Name]; 
    }

    @AuraEnabled
    public static string updateSettingDashboard(string objJson, String agencyId) {
        System.debug('<---updateeSettingDashboard---> '); 
        string res = 'success';

        try{          
            
            //String agencyId = lwcFrontDesk.getUserContacttAccountID();    
            List<Object> j = (List<Object>) JSON.deserializeUntyped(objJson);            
                        
            Map<String, Map<String, Object>> jsonPerUser = new Map<String, Map<String, Object>>();
            for (Object item : j) {                        
                Map<String, Object> f = (Map<String, Object>)item;                                          
                String idUser = String.valueOf(f.get('id'));
                jsonPerUser.put(idUser, f);                                 
            }  

            List<User> users = new List<User>();
            for(String key : jsonPerUser.keySet()){
                users.add(new User(ID = key, DashboardSettings__c = JSON.serialize(jsonPerUser.get(key))));
            }

            for(User us :[SELECT id, DashboardSettings__c FROM User WHERE isActive = true AND Contact.AccountID = :agencyId AND ID NOT IN :jsonPerUser.keySet()]){                             
                users.add(new User(ID = us.id, DashboardSettings__c = null));
            } 
            update users;
            
        }catch(Exception ex){
            res = 'error';   
            System.debug('updateeSettingDashboard Error---> ' + ex.getMessage());                
        }
        return res;        
    }

    @AuraEnabled
    public static string updateSettingTarget(string objJson, String agencyId) {
        System.debug('<---updateSettingsTarget---> '); 
        string res = 'success';

        try{          
            Map<String, decimal> jsonDepart = new Map<String, decimal>();
            //String agencyId = lwcFrontDesk.getUserContacttAccountID();    
            List<Object> j = (List<Object>) JSON.deserializeUntyped(objJson);                 
            for (Object item : j) {                        
                Map<String, Object> f = (Map<String, Object>)item;                                          
                String idDpt = String.valueOf(f.get('id'));
                String vlDpt = String.valueOf(f.get('value'));
                decimal vlr = 0;
                if ((vlDpt != '') && (vlDpt != '0')){
                    vlr = decimal.valueOf(vlDpt);
                }                
                System.debug('vlr---> ' + vlr);                
                jsonDepart.put(idDpt, vlr);                                 
            }  
            
            List<Department__c> dep = new List<Department__c>();                     
            for(Department__c us :[SELECT id, Department_Sales_Target__c FROM Department__c WHERE Agency__c = :agencyId AND ID IN :jsonDepart.keySet()]){                                         
                dep.add(new Department__c(ID = us.id, Department_Sales_Target__c = jsonDepart.get(us.id)));
            } 
            update dep;
            
        }catch(Exception ex){
            res = 'error';   
            System.debug('updateSettingsTarget Error---> ' + ex.getMessage());                
        }
        return res;        
    }

    @AuraEnabled(cacheable=true)
    public static List<wrpGeneric> getDashReports() {     
        //Filling head of grid dashboard Settings
        List<wrpGeneric> pickListValuesList= new List<wrpGeneric>();

        Schema.DescribeFieldResult fieldResult = Account.DashboardReports__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        integer id = 0;    
        for(Schema.PicklistEntry pickListVal : ple){
            id +=1;  
            if ((pickListVal.getValue() != 'Target') && (pickListVal.getValue() != 'Contact') &&
                (pickListVal.getValue() != 'StageStatusNZ') && (pickListVal.getValue() != 'StageStatusCA') &&
                (pickListVal.getValue() != 'StageStatusCZ') && (pickListVal.getValue() != 'StageStatusIRL') &&
                (pickListVal.getValue() != 'StageStatusUK') && (pickListVal.getValue() != 'StageStatusBR') &&
                (pickListVal.getValue() != 'StageStatus')) {
                pickListValuesList.add(new wrpGeneric(string.valueOf(id), pickListVal.getLabel()));    
            }                        
            // (pickListVal.getValue() != 'Enrollments') && (pickListVal.getValue() != 'EnrollmentsFC') 
        }    		        

		System.debug('pickListValuesList ' + pickListValuesList);
        return pickListValuesList;		
	}

    @AuraEnabled(cacheable=true)
    public static List<wrpDashSettings> getDashboardSettingsUsers(string accoId){
        System.debug('getDashboarddSettingsUsers---> ' + accoId);
        List<wrpDashSettings> dash = new List<wrpDashSettings>();
        list<String> dNames = new list<String>();     
        try{
            //Filling up VALUES grid dashboard Settings
            for(User us :[SELECT id, name, DashboardSettings__c, Contact.Department__r.Name FROM User WHERE isActive = true AND Contact.RecordType.Name = 'Employee' AND api_user__c = false AND Contact.AccountID = : accoId ORDER BY Contact.Department__c, name]){ 
                string depName= us.Contact.Department__r.Name;
                if (dNames.Contains(depName))
                {
                     depName = '';
                }
                if ((!String.isBlank(us.DashboardSettings__c)) &&
                    (!String.isEmpty(us.DashboardSettings__c)) &&
                    (!us.DashboardSettings__c.isWhitespace()) &&
                    (us.DashboardSettings__c != null) ) {  
                        System.debug(' us.DashboardSettings__c---> ' + us.DashboardSettings__c);
                        List<wrpDashReports> dashRep = new List<wrpDashReports>();                                        
                        string objJson = us.DashboardSettings__c;                                                                
                        Object j = (Object) JSON.deserializeUntyped(objJson);                     
                        Map<String, Object> jItem = (Map<String, Object>)j;                    
                        List<Object> reports = (List<Object>)jItem.get('reports');                    
                        for (Object re : reports) {                             
                            Map<String, Object> r = (Map<String, Object>)re;                             
                            string id = (string)r.get('id');
                            string name = (string)r.get('name'); 
                            string period = (string)r.get('period'); 
                            string category = (string)r.get('category');        
                            
                            dashRep.add(new wrpDashReports(id, name, period, category));                            
                        }                                
                        dNames.add(us.Contact.Department__r.Name);
                        dash.add(new wrpDashSettings(us.Id, us.Name, us.Contact.Department__c, depName, dashRep));                  
                                                                        
                }else{
                    dNames.add(us.Contact.Department__r.Name);
                    dash.add(new wrpDashSettings(us.Id, us.Name, us.Contact.Department__c, depName, new List<wrpDashReports>()));
                }            
            }
        }catch(Exception ex){            
            System.debug('getDashboarddSettingsUsers Error---> ' + ex.getMessage());                
        }
        System.debug(' dash---> ' + dash);    
        return dash;
    }

    @AuraEnabled(cacheable=true)
    public static List<wrpGeneric> getFilterCriteria() {
		List<wrpGeneric> options = new List<wrpGeneric>();
		options.add(new wrpGeneric('TODAY','Today'));
		options.add(new wrpGeneric('TOMORROW','Tomorrow'));
		
        options.add(new wrpGeneric('THIS_WEEK','This week'));
		options.add(new wrpGeneric('NEXT_WEEK','Next week'));
		
        options.add(new wrpGeneric('THIS_MONTH','This month'));
		options.add(new wrpGeneric('NEXT_MONTH','Next month'));		
		
        options.add(new wrpGeneric('LAST_WEEK','Last week'));
		options.add(new wrpGeneric('LAST_MONTH','Last month'));
        options.add(new wrpGeneric('LAST_N_MONTHS:3','Last 3 months')); 
        options.add(new wrpGeneric('LAST_N_MONTHS:6','Last 6 months'));

        options.add(new wrpGeneric('NEXT_N_DAYS:7','Next 7 days'));
		options.add(new wrpGeneric('NEXT_N_DAYS:15','Next 15 days'));
        options.add(new wrpGeneric('NEXT_N_DAYS:30','Next 30 days'));
        options.add(new wrpGeneric('NEXT_N_DAYS:60','Next 60 days'));
        options.add(new wrpGeneric('NEXT_N_DAYS:90','Next 90 days'));
        options.add(new wrpGeneric('NEXT_N_DAYS:120','Next 120 days'));
        options.add(new wrpGeneric('NEXT_N_DAYS:180','Next 180 days'));

		options.add(new wrpGeneric('LAST_N_DAYS:7','Last 7 days'));
		options.add(new wrpGeneric('LAST_N_DAYS:15','Last 15 days'));
        options.add(new wrpGeneric('LAST_N_DAYS:30','Last 30 days'));
        options.add(new wrpGeneric('LAST_N_DAYS:60','Last 60 days'));
        options.add(new wrpGeneric('LAST_N_DAYS:90','Last 90 days'));
        options.add(new wrpGeneric('LAST_N_DAYS:120','Last 120 days'));
        options.add(new wrpGeneric('LAST_N_DAYS:180','Last 180 days'));
		return options;
	}    
}