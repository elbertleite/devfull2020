public with sharing class commission_share_invoices_received {

    public sharePayments pageResult {get;set;}

    public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}
    
    public commission_share_invoices_received() {
        
    }

    private set<Id> searchedAgencies;
    public void findPayments(){
        searchedAgencies = new set<Id>();

        if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()) //set the user to see only his own agency group
            selectedAgencyGroup = currentUser.Contact.Account.ParentId;

        if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && currentUser.Aditional_Agency_Managment__c == null) //set the user to see only his own agency
            selectedAgency = currentUser.Contact.AccountId;
        
        if(selectedAgency != 'all')
            searchedAgencies.add(selectedAgency);
        else
            searchedAgencies.addAll(allAgencies);

        String fromDate = IPFunctions.FormatSqlDateIni(dateFilter.Original_Due_Date__c); 
        String toDate = IPFunctions.FormatSqlDateFin(dateFilter.Discounted_On__c);

        boolean executeQuery = true;

        String sql = 'SELECT Commission__c, Commission_Tax_Rate__c, Commission_Tax_Value__c, Commission_Value__c, Discount__c, isFirstPayment__c, isPDS__c, isPFS__c, isPCS__c, Discounted_By__c, Discounted_On__c, Instalment_Value__c, Kepp_Fee__c, Number__c, Split_Number__c, Tuition_Value__c, Received_By_Agency__c, Received_By_Agency__r.name, Shared_Comm_Agency_Enroll__c, Shared_Comm_Agency_Enroll_Value__c, Shared_Comm_Agency_Receive__c, Shared_Comm_Agency_Receive_Value__c, Shared_Commission_Bank_Dep_Enrolled__c, Shared_Commission_Bank_Dep_Enrolled__r.Invoice__c, Shared_Commission_Bank_Dep_Received__c, Shared_Commission_Bank_Dep_Received__r.Invoice__c, isRequestedShareCommission__c, Shared_Commission_Bank_Dep_Request__c, Shared_Commission_Bank_Dep_Request__r.Invoice__c, isReceived_Enrolled_Different_Agency__c, Shared_Comm_Agency_Request_Value__c, Shared_Comm_Agency_Request__c, Shared_Comm_Tax_Agency_Enroll_Value__c, Shared_Comm_Tax_Agency_Receive_Value__c, Shared_Comm_Tax_Agency_Request_Value__c, isReceived_Requested_Same_Agency__c, Request_Commission_Invoice__r.Requested_by_Agency__c, Request_Commission_Invoice__r.Requested_by_Agency__r.Name, Received_By_Agency__r.ParentId, isPaidOffShore__c, isShare_Commission__c, isNotNetCommission__c, isEnrolled_Requested_Same_Agency__c, pay_to_agency__c, pay_to_agency__r.ParentID, pay_to_agency__r.name, isPayToAgency_Diff_Enroll_Receive_Agency__c, Shared_Comm_Pay_Agency__c, Shared_Comm_Pay_Agency_Value__c, Shared_Comm_Tax_Pay_Agency_Value__c, Shared_On_Enrolled_Agency__c, Shared_On_Pay_Agency__c, Shared_On_Received_Agency__c, Shared_On_Requested_Agency__c, Shared_Commission_Bank_Dep_Pay_Agency__r.Invoice__c, ' +

        ' Shared_Commission_Bank_Dep_Enrolled__r.Invoice__r.Share_Commission_Number__c, '+ 
        ' Shared_Commission_Bank_Dep_Enrolled__r.Invoice__r.Requested_Commission_for_Agency__r.Name, '+
        ' Shared_Commission_Bank_Dep_Enrolled__r.Invoice__r.Received_Currency_Rate__c, ' + 
        ' Shared_Commission_Bank_Dep_Enrolled__r.Invoice__r.Received_Currency__c, ' +
        ' Shared_Commission_Bank_Dep_Pay_Agency__r.Invoice__r.Share_Commission_Number__c,  ' + 
        ' Shared_Commission_Bank_Dep_Pay_Agency__r.Invoice__r.Requested_Commission_for_Agency__r.Name,  ' + 
        ' Shared_Commission_Bank_Dep_Pay_Agency__r.Invoice__r.Received_Currency_Rate__c,  ' + 
        ' Shared_Commission_Bank_Dep_Pay_Agency__r.Invoice__r.Received_Currency__c, ' +
        ' Shared_Commission_Bank_Dep_Received__r.Invoice__r.Share_Commission_Number__c,  ' + 
        ' Shared_Commission_Bank_Dep_Received__r.Invoice__r.Requested_Commission_for_Agency__r.Name,  ' + 
        ' Shared_Commission_Bank_Dep_Received__r.Invoice__r.Received_Currency_Rate__c,  ' + 
        ' Shared_Commission_Bank_Dep_Received__r.Invoice__r.Received_Currency__c, ' +
        ' Shared_Commission_Bank_Dep_Request__r.Invoice__r.Share_Commission_Number__c,  ' + 
        ' Shared_Commission_Bank_Dep_Request__r.Invoice__r.Requested_Commission_for_Agency__r.Name, ' + 
        ' Shared_Commission_Bank_Dep_Request__r.Invoice__r.Received_Currency_Rate__c, ' + 
        ' Shared_Commission_Bank_Dep_Request__r.Invoice__r.Received_Currency__c, ' +

        ' client_course__r.share_commission_request_agency__c, ' +
        ' client_course__r.share_commission_request_agency__r.Name, ' +
        ' client_course__r.Client_Type__c, ' +
        ' client_course__r.Campus_Name__c, ' +
        ' client_course__r.Client__r.name, ' +
        ' client_course__r.Commission_Tax_Rate__c, ' +
        ' client_course__r.Commission_Tax_Value__c, ' +
        ' client_course__r.Commission_Type__c, ' +
        ' client_course__r.Course_Name__c, ' +
        ' client_course__r.Enroled_by_Agency__c,  ' +
        ' client_course__r.Enroled_by_Agency__r.name,  ' +
        ' client_course__r.Course_Length__c,  ' +
        ' client_course__r.Free_Units__c, ' +
        ' client_course__r.Free_Weeks__c,  ' +
        ' client_course__r.Unit_Type__c, ' +
        ' client_Course__r.Campus_Country__c, ' +
        ' client_Course__r.CurrencyIsoCode__c, ' +
        ' client_course__r.Campus_Course__r.Course__r.Course_Type__c,  ' +
        ' client_course__r.Client__r.owner__r.contact.account.name,  ' +
        ' client_course__r.Client__r.owner__r.name FROM client_course_instalment__c  WHERE ';

        if(selectedAgency != 'all'){
            sql += ' (client_course__r.Enroled_by_Agency__c = \'' + selectedAgency + '\' OR pay_to_agency__c = \'' + selectedAgency + '\' OR (isRequestedShareCommission__c = TRUE AND client_course__r.share_commission_decision_status__c = \'Accepted\'  AND client_course__r.share_commission_request_agency__c = \'' + selectedAgency + '\') OR (Received_By_Agency__c = \'' + selectedAgency + '\' ))';
        }
        else{
          sql += ' (client_course__r.Enroled_by_Agency__c IN ( \''+ String.join(allAgencies, '\',\'') + '\' )  OR pay_to_agency__c IN ( \''+ String.join(allAgencies, '\',\'') + '\' )  OR (isRequestedShareCommission__c = TRUE AND client_course__r.share_commission_decision_status__c = \'Accepted\'  AND client_course__r.share_commission_request_agency__c IN ( \''+ String.join(allAgencies, '\',\'') + '\' ) ) OR (Received_By_Agency__c IN ( \''+ String.join(allAgencies, '\',\'') + '\' )))';
        }

        sql += ' AND client_course__r.Campus_Country__c = \'' + selectedCountry + '\' AND ((isNotNetCommission__c = TRUE AND isShare_Commission__c = FALSE ) OR (isShare_Commission__c = TRUE AND isShareCommissionSplited__c = TRUE)) AND (isMigrated__c = FALSE OR PFS_Pending__c = TRUE) AND Commission_Not_Claimable__c = FALSE ';


        if(selectedTypeFilter=='range'){ //Search by Date Range

            sql += ' AND (';

            if(selectedAgency != 'all'){

                //Enrolled
                sql += ' (isReceived_Enrolled_Different_Agency__c = TRUE AND client_course__r.Enroled_by_Agency__c = \'' + selectedAgency + '\' AND Shared_On_Enrolled_Agency__c >= '+ fromDate +' and Shared_On_Enrolled_Agency__c <= '+toDate + ') '; 
                    
                //Requested
                sql += ' OR (isRequestedShareCommission__c = TRUE AND client_course__r.share_commission_request_agency__c = \'' + selectedAgency + '\' AND isReceived_Requested_Same_Agency__c = FALSE AND isEnrolled_Requested_Same_Agency__c = FALSE AND Shared_On_Requested_Agency__c >= '+ fromDate +' and Shared_On_Requested_Agency__c <= '+toDate + ') '; 
                
                //Offshore
                sql += ' OR (pay_to_agency__c != NULL AND pay_to_agency__c = \'' + selectedAgency + '\' AND isPayToAgency_Diff_Enroll_Receive_Agency__c = TRUE AND Shared_On_Pay_Agency__c >= '+ fromDate +' and Shared_On_Pay_Agency__c <= '+toDate + ') '; 

                //PDS/PCS/PFS
                sql += ' OR (isNotNetCommission__c = TRUE  AND Received_By_Agency__c = \'' + selectedAgency + '\' AND   Shared_On_Received_Agency__c >= '+ fromDate +' and  Shared_On_Received_Agency__c <= '+toDate + ') '; 
                
                sql += ') ';
            }
            else{

                //Enrolled
                sql += ' (isReceived_Enrolled_Different_Agency__c = TRUE AND client_course__r.Enroled_by_Agency__r.ParentId = \'' + selectedAgencyGroup + '\' AND Shared_On_Enrolled_Agency__c >= '+ fromDate +' and Shared_On_Enrolled_Agency__c <= '+toDate + ') '; 
                    
                //Requested
                sql += ' OR (isRequestedShareCommission__c = TRUE AND client_course__r.share_commission_request_agency__r.ParentId = \'' + selectedAgencyGroup + '\' AND isReceived_Requested_Same_Agency__c = FALSE AND isEnrolled_Requested_Same_Agency__c = FALSE AND Shared_On_Requested_Agency__c >= '+ fromDate +' and Shared_On_Requested_Agency__c <= '+toDate + ') '; 
                
                //Offshore
                sql += ' OR (pay_to_agency__c != NULL AND pay_to_agency__r.ParentId = \'' + selectedAgencyGroup + '\' AND isPayToAgency_Diff_Enroll_Receive_Agency__c = TRUE AND Shared_On_Pay_Agency__c >= '+ fromDate +' and Shared_On_Pay_Agency__c <= '+toDate + ') '; 

                //PDS/PCS/PFS
                sql += ' OR (isNotNetCommission__c = TRUE  AND Received_By_Agency__r.ParentId = \'' + selectedAgencyGroup + '\' AND Shared_On_Received_Agency__c >= '+ fromDate +' and Shared_On_Received_Agency__c <= '+toDate + ') '; 

                sql += ') ';
            }
        }
        else if(invoiceNumber != null && invoiceNumber != '') { //Search by Invoice Number 
            list<String> allInvoices = new list<String>(invoiceNumber.split(','));

            sql += ' AND (';

            if(selectedAgency != 'all'){

                //Enrolled
                sql += ' (Shared_Commission_Bank_Dep_Enrolled__r.Invoice__r.Share_Commission_Number__c IN ( \''+ String.join(allInvoices, '\',\'') + '\' ) AND isReceived_Enrolled_Different_Agency__c = TRUE AND client_course__r.Enroled_by_Agency__c = \'' + selectedAgency + '\' AND Shared_On_Enrolled_Agency__c != NULL ) '; 
                    
                //Requested
                sql += ' OR (Shared_Commission_Bank_Dep_Request__r.Invoice__r.Share_Commission_Number__c IN ( \''+ String.join(allInvoices, '\',\'') + '\' ) AND isRequestedShareCommission__c = TRUE AND client_course__r.share_commission_request_agency__c = \'' + selectedAgency + '\' AND isReceived_Requested_Same_Agency__c = FALSE AND isEnrolled_Requested_Same_Agency__c = FALSE AND Shared_On_Requested_Agency__c != NULL ) '; 
                
                //Offshore
                sql += ' OR (Shared_Commission_Bank_Dep_Pay_Agency__r.Invoice__r.Share_Commission_Number__c IN ( \''+ String.join(allInvoices, '\',\'') + '\' ) AND pay_to_agency__c != NULL AND pay_to_agency__c = \'' + selectedAgency + '\' AND isPayToAgency_Diff_Enroll_Receive_Agency__c = TRUE AND Shared_On_Pay_Agency__c != NULL) '; 

                //PDS/PCS/PFS
                sql += ' OR (Shared_Commission_Bank_Dep_Received__r.Invoice__r.Share_Commission_Number__c IN ( \''+ String.join(allInvoices, '\',\'') + '\' ) AND isNotNetCommission__c = TRUE  AND Received_By_Agency__c = \'' + selectedAgency + '\' AND Shared_On_Received_Agency__c != NULL )'; 
                
                sql += ') ';
            }
            else{

                //Enrolled
                sql += ' (Shared_Commission_Bank_Dep_Enrolled__r.Invoice__r.Share_Commission_Number__c IN ( \''+ String.join(allInvoices, '\',\'') + '\' ) AND isReceived_Enrolled_Different_Agency__c = TRUE AND client_course__r.Enroled_by_Agency__r.ParentId = \'' + selectedAgencyGroup + '\' AND Shared_On_Enrolled_Agency__c != NULL ) '; 
                    
                //Requested
                sql += ' OR (Shared_Commission_Bank_Dep_Request__r.Invoice__r.Share_Commission_Number__c IN ( \''+ String.join(allInvoices, '\',\'') + '\' ) AND isRequestedShareCommission__c = TRUE AND client_course__r.share_commission_request_agency__r.ParentId = \'' + selectedAgencyGroup + '\' AND isReceived_Requested_Same_Agency__c = FALSE AND isEnrolled_Requested_Same_Agency__c = FALSE AND Shared_On_Requested_Agency__c != NULL) '; 
                
                //Offshore
                sql += ' OR (Shared_Commission_Bank_Dep_Pay_Agency__r.Invoice__r.Share_Commission_Number__c IN ( \''+ String.join(allInvoices, '\',\'') + '\' ) AND pay_to_agency__c != NULL AND pay_to_agency__r.ParentId = \'' + selectedAgencyGroup + '\' AND isPayToAgency_Diff_Enroll_Receive_Agency__c = TRUE AND Shared_On_Pay_Agency__c != NULL) '; 

                //PDS/PCS/PFS
                sql += ' OR (Shared_Commission_Bank_Dep_Received__r.Invoice__r.Share_Commission_Number__c IN ( \''+ String.join(allInvoices, '\',\'') + '\' ) AND isNotNetCommission__c = TRUE  AND Received_By_Agency__r.ParentId = \'' + selectedAgencyGroup + '\' AND Shared_On_Received_Agency__c != NULL) '; 

                sql += ') ';
            }
        }
        else{
            executeQuery = false;
        }
        
        sql += ' ORDER BY Client_Course__r.Client__r.Name, Client_Course__r.Campus_Country__c';

        system.debug('@SQL==>' + sql);

        sharePayments payments = new sharePayments();

        if(executeQuery){
            for(client_course_instalment__c cci : searchNoSharing.NSInstalments(sql)){

                //Enrolled
                if(cci.Shared_On_Enrolled_Agency__c != NULL && cci.isReceived_Enrolled_Different_Agency__c && cci.Shared_Commission_Bank_Dep_Enrolled__c != NULL && searchedAgencies.contains(cci.client_course__r.Enroled_by_Agency__c)){
                    if(payments.hasAgency(cci.client_course__r.Enroled_by_Agency__c))
                        payments.addInstalment(cci.client_course__r.Enroled_by_Agency__c, new shareInstalment(cci, 1), cci.Shared_Commission_Bank_Dep_Enrolled__r.Invoice__r.Received_Currency__c);
                    else
                        payments.addAgency(cci.client_course__r.Enroled_by_Agency__c, new agencyInstalments(cci.client_course__r.Enroled_by_Agency__c, cci.client_course__r.Enroled_by_Agency__r.Name, new shareInstalment(cci, 1), cci.Shared_Commission_Bank_Dep_Enrolled__r.Invoice__r.Received_Currency__c));
                }

                //Received w/ Share Comm.   
                if(cci.Shared_On_Received_Agency__c != NULL && cci.isShare_Commission__c && cci.isNotNetCommission__c && cci.Shared_Commission_Bank_Dep_Received__c != NULL && searchedAgencies.contains(cci.Received_By_Agency__c)){
                    if(payments.hasAgency(cci.Received_By_Agency__c))
                        payments.addInstalment(cci.Received_By_Agency__c, new shareInstalment(cci, 2), cci.Shared_Commission_Bank_Dep_Received__r.Invoice__r.Received_Currency__c);
                    else
                        payments.addAgency(cci.Received_By_Agency__c, new agencyInstalments(cci.Received_By_Agency__c, cci.Received_By_Agency__r.Name, new shareInstalment(cci, 2),cci.Shared_Commission_Bank_Dep_Received__r.Invoice__r.Received_Currency__c));
                }

                //Requested Share Comm.
                if(cci.Shared_On_Requested_Agency__c != NULL && cci.isRequestedShareCommission__c && !cci.isEnrolled_Requested_Same_Agency__c && cci.Shared_Commission_Bank_Dep_Request__c != NULL && !cci.isReceived_Requested_Same_Agency__c && searchedAgencies.contains(cci.client_course__r.share_commission_request_agency__c)){
                    if(payments.hasAgency(cci.client_course__r.share_commission_request_agency__c))
                        payments.addInstalment(cci.client_course__r.share_commission_request_agency__c, new shareInstalment(cci, 3), cci.Shared_Commission_Bank_Dep_Request__r.Invoice__r.Received_Currency__c);
                    else
                        payments.addAgency(cci.client_course__r.share_commission_request_agency__c, new agencyInstalments(cci.client_course__r.share_commission_request_agency__c, cci.client_course__r.share_commission_request_agency__r.Name, new shareInstalment(cci, 3), cci.Shared_Commission_Bank_Dep_Request__r.Invoice__r.Received_Currency__c));
                }

                //PDS/PCS/PFS 100%
                if(cci.Shared_On_Received_Agency__c != NULL && !cci.isShare_Commission__c && cci.isNotNetCommission__c && cci.Shared_Commission_Bank_Dep_Received__c != NULL && searchedAgencies.contains(cci.Received_By_Agency__c)){
                    if(payments.hasAgency(cci.Received_By_Agency__c))
                        payments.addInstalment(cci.Received_By_Agency__c, new shareInstalment(cci, 4), cci.Shared_Commission_Bank_Dep_Received__r.Invoice__r.Received_Currency__c);
                    else
                        payments.addAgency(cci.Received_By_Agency__c, new agencyInstalments(cci.Received_By_Agency__c, cci.Received_By_Agency__r.Name, new shareInstalment(cci, 4), cci.Shared_Commission_Bank_Dep_Received__r.Invoice__r.Received_Currency__c));
                }

                //Offshore
                if(cci.Shared_On_Pay_Agency__c != NULL && cci.Pay_to_Agency__c != NULL && cci.isPayToAgency_Diff_Enroll_Receive_Agency__c && cci.Shared_Commission_Bank_Dep_Pay_Agency__c != NULL && searchedAgencies.contains(cci.Pay_to_Agency__c)){
                    if(payments.hasAgency(cci.Pay_to_Agency__c))
                        payments.addInstalment(cci.Pay_to_Agency__c, new shareInstalment(cci, 5), cci.Shared_Commission_Bank_Dep_Pay_Agency__r.Invoice__r.Received_Currency__c);
                    else
                        payments.addAgency(cci.Pay_to_Agency__c, new agencyInstalments(cci.Pay_to_Agency__c, cci.Pay_to_Agency__r.Name, new shareInstalment(cci, 5), cci.Shared_Commission_Bank_Dep_Pay_Agency__r.Invoice__r.Received_Currency__c));
                }

            }//end Database Result for
        }

        payments.orderAgencies();
        pageResult = payments;
    }


    public PageReference generateExcel(){
        PageReference pr = Page.commission_share_invoices_received_excel;
        pr.setRedirect(false);
        return pr;
    }

    public void searchExcel(){
        findPayments();
    }

    public String xlsHeader {
        get {
            String strHeader = '';
            strHeader += '<?xml version="1.0"?>';
            strHeader += '<?mso-application progid="Excel.Sheet"?>';
            return strHeader;
        }
    }


    public class sharePayments{
        public list<agencyInstalments> allAgencies {get;set;}
        private map<String, agencyInstalments> mapAgencies {get{if(mapAgencies==null) mapAgencies = new map<String, agencyInstalments>(); return mapAgencies;}set;}

        public sharePayments (){}

        public boolean hasAgency(String agName){
            if(!mapAgencies.containsKey(agName))
                return false;
            else
                return true;
        }

        public void addAgency (String agName, agencyInstalments ag){
            mapAgencies.put(agName, ag);
        }

        public void addInstalment (String agName, shareInstalment cci, String recCurrency){
            mapAgencies.get(agName).addInstalment(cci, recCurrency);
        }

        public void orderAgencies(){
            list<String> names = new list<String>();
            names.addAll(mapAgencies.keySet());
            names.sort();

            this.allAgencies = new list<agencyInstalments>();

            for(String nm : names)
                this.allAgencies.add(mapAgencies.get(nm));
        }
    }

    public class agencyInstalments{
    public String agencyId {get;set;}
    public String agencyName {get;set;}
    public set<String> agencyCurrency {get{if(agencyCurrency==null) agencyCurrency = new set<String>(); return agencyCurrency;}set;}
    public list<shareInstalment> shares {get{if(shares==null) shares= new list<shareInstalment>(); return shares;}set;}
    public decimal totalAmount {get{if(totalAmount==null) totalAmount= 0; return totalAmount;}set;}

    public agencyInstalments (String agencyId, String agencyName, shareInstalment cci, String recCurrency){
        this.agencyId = agencyId;
        this.agencyName = agencyName;
        this.shares.add(cci);
        this.agencyCurrency.add(recCurrency);
        // this.totalAmount = cci.invoice.Total_Value__c;
    }

    public void addInstalment(shareInstalment cci, String recCurrency){
        this.shares.add(cci);
        this.agencyCurrency.add(recCurrency);
        // this.totalAmount += cci.invoice.Total_Value__c;
    }
  }

  public class shareInstalment{
    public transient client_course_instalment__c inst {get;set;}
    public String agencyName {get;set;}
    public integer typeRequest {get;set;} 
    public String description {get;set;}
    public decimal commReceive {get;set;}
    public decimal commShare {get;set;}
    public decimal commTaxReceive {get;set;}
    public decimal commTaxShare {get;set;}
    
    public String invNumber {get;set;}
    public String invId {get;set;}
    public client_course_instalment__c commReceivedOn {get;set;}
    public String commReceivedBy {get;set;}
    public Decimal commValueInCurr {get;set;}
    public Decimal commReceivedRate {get;set;}

    public shareInstalment(client_course_instalment__c instalment, integer typeRequest){
      this.inst = instalment;
      this.typeRequest = typeRequest;
      if(typeRequest == 1){ //Enrolled
        this.commShare = inst.Shared_Comm_Agency_Enroll__c;
        this.commReceive = inst.Shared_Comm_Agency_Enroll_Value__c;
        this.commTaxReceive = inst.Shared_Comm_Tax_Agency_Enroll_Value__c;
        if(inst.Commission_Tax_Value__c != null && inst.Commission_Tax_Value__c >0)
          this.commTaxShare = (inst.Shared_Comm_Tax_Agency_Enroll_Value__c * 100) / inst.Commission_Tax_Value__c ;
        else
          this.commTaxShare = 0;
        this.agencyName = inst.client_course__r.Enroled_by_Agency__r.name;
        this.description = 'Enroled agency';

        this.invNumber = inst.Shared_Commission_Bank_Dep_Enrolled__r.Invoice__r.Share_Commission_Number__c;
        this.invId = inst.Shared_Commission_Bank_Dep_Enrolled__r.Invoice__c;
        this.commReceivedOn = new client_course_instalment__c(Discounted_On__c = inst.Shared_On_Enrolled_Agency__c);
        this.commReceivedBy = inst.Shared_Commission_Bank_Dep_Enrolled__r.Invoice__r.Requested_Commission_for_Agency__r.Name;
        this.commValueInCurr = (this.commReceive + this.commTaxReceive ) * inst.Shared_Commission_Bank_Dep_Enrolled__r.Invoice__r.Received_Currency_Rate__c;
        this.commReceivedRate = inst.Shared_Commission_Bank_Dep_Enrolled__r.Invoice__r.Received_Currency_Rate__c;
      }
      else if(typeRequest == 2){ //Received w/ Share
        this.commShare = inst.Shared_Comm_Agency_Receive__c;
        this.commReceive = inst.Shared_Comm_Agency_Receive_Value__c;
        this.commTaxReceive = inst.Shared_Comm_Tax_Agency_Receive_Value__c;
        if(inst.Commission_Tax_Value__c != null && inst.Commission_Tax_Value__c >0)
          this.commTaxShare = (inst.Shared_Comm_Tax_Agency_Receive_Value__c * 100) / inst.Commission_Tax_Value__c ;
        else
          this.commTaxShare = 0;
        this.agencyName = inst.Received_by_Agency__r.Name;
        this.description = 'Received agency';
        
        this.invNumber = inst.Shared_Commission_Bank_Dep_Received__r.Invoice__r.Share_Commission_Number__c;
        this.invId = inst.Shared_Commission_Bank_Dep_Received__r.Invoice__c;
        this.commReceivedOn = new client_course_instalment__c(Discounted_On__c = inst.Shared_On_Received_Agency__c);
        this.commReceivedBy = inst.Shared_Commission_Bank_Dep_Received__r.Invoice__r.Requested_Commission_for_Agency__r.Name;
        this.commValueInCurr = (this.commReceive + this.commTaxReceive ) * inst.Shared_Commission_Bank_Dep_Received__r.Invoice__r.Received_Currency_Rate__c;
        this.commReceivedRate = inst.Shared_Commission_Bank_Dep_Received__r.Invoice__r.Received_Currency_Rate__c;
      }
      else if(typeRequest == 3){ //Requested
        this.commShare = inst.Shared_Comm_Agency_Request__c;
        this.commReceive = inst.Shared_Comm_Agency_Request_Value__c;
        this.commTaxReceive = inst.Shared_Comm_Tax_Agency_Request_Value__c;
        if(inst.Commission_Tax_Value__c != null && inst.Commission_Tax_Value__c >0)
          this.commTaxShare = (inst.Shared_Comm_Tax_Agency_Request_Value__c * 100) / inst.Commission_Tax_Value__c ;
        else
          this.commTaxShare = 0;
        this.agencyName = inst.client_course__r.share_commission_request_agency__r.Name;
        this.description = 'Requested agency';
        
        this.invNumber = inst.Shared_Commission_Bank_Dep_Request__r.Invoice__r.Share_Commission_Number__c;
        this.invId = inst.Shared_Commission_Bank_Dep_Request__r.Invoice__c;
        this.commReceivedOn = new client_course_instalment__c(Discounted_On__c = inst.Shared_On_Requested_Agency__c);
        this.commReceivedBy = inst.Shared_Commission_Bank_Dep_Request__r.Invoice__r.Requested_Commission_for_Agency__r.Name;
        this.commValueInCurr = (this.commReceive + this.commTaxReceive ) * inst.Shared_Commission_Bank_Dep_Request__r.Invoice__r.Received_Currency_Rate__c;
        this.commReceivedRate = inst.Shared_Commission_Bank_Dep_Request__r.Invoice__r.Received_Currency_Rate__c;
      }
      else if(typeRequest == 4){ //PDS/PCS/PFS
        this.commShare = 100;
        this.commReceive = inst.Commission_Value__c - inst.Discount__c;
        this.commTaxReceive = inst.Commission_Tax_Value__c;
        this.commTaxShare = 100;
        this.agencyName = inst.Received_by_Agency__r.Name;
        this.description = 'Received agency';
        
        this.invNumber = inst.Shared_Commission_Bank_Dep_Received__r.Invoice__r.Share_Commission_Number__c;
        this.invId = inst.Shared_Commission_Bank_Dep_Received__r.Invoice__c;
        this.commReceivedOn = new client_course_instalment__c(Discounted_On__c = inst.Shared_On_Received_Agency__c);
        this.commReceivedBy = inst.Shared_Commission_Bank_Dep_Received__r.Invoice__r.Requested_Commission_for_Agency__r.Name;
        this.commValueInCurr = (this.commReceive + this.commTaxReceive ) * inst.Shared_Commission_Bank_Dep_Received__r.Invoice__r.Received_Currency_Rate__c;
        this.commReceivedRate = inst.Shared_Commission_Bank_Dep_Received__r.Invoice__r.Received_Currency_Rate__c;
      }
      else if(typeRequest == 5){ //Offshore
        this.commShare = inst.Shared_Comm_Pay_Agency__c;
        this.commReceive = inst.Shared_Comm_Pay_Agency_Value__c;
        this.commTaxReceive = inst.Shared_Comm_Tax_Pay_Agency_Value__c;
        if(inst.Commission_Tax_Value__c != null && inst.Commission_Tax_Value__c >0)
          this.commTaxShare = (inst.Shared_Comm_Tax_Pay_Agency_Value__c * 100) / inst.Commission_Tax_Value__c ;
        else
          this.commTaxShare = 0;
        this.agencyName = inst.Pay_To_Agency__r.Name;
        this.description = 'Offshore payment';
        
        this.invNumber = inst.Shared_Commission_Bank_Dep_Pay_Agency__r.Invoice__r.Share_Commission_Number__c;
        this.invId = inst.Shared_Commission_Bank_Dep_Pay_Agency__r.Invoice__c;
        this.commReceivedOn = new client_course_instalment__c(Discounted_On__c = inst.Shared_On_Pay_Agency__c);
        this.commReceivedBy = inst.Shared_Commission_Bank_Dep_Pay_Agency__r.Invoice__r.Requested_Commission_for_Agency__r.Name;
        this.commValueInCurr = (this.commReceive + this.commTaxReceive ) * inst.Shared_Commission_Bank_Dep_Pay_Agency__r.Invoice__r.Received_Currency_Rate__c;
        this.commReceivedRate = inst.Shared_Commission_Bank_Dep_Pay_Agency__r.Invoice__r.Received_Currency_Rate__c;
      }
    }
  }

    //  ********** F I L T E R S *************

    private IPFunctions.SearchNoSharing searchNoSharing {get{if(searchNoSharing==null) searchNoSharing = new IPFunctions.SearchNoSharing(); return searchNoSharing;}set;}
    private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}
    private User currentUser {get{if(currentUser==null) currentUser = ff.currentUser; return currentUser;}set;}

    public string selectedCountry{get;set;}
    public List<SelectOption> destinationCountries{ 
        get{
            if(destinationCountries == null){
                destinationCountries = new List<SelectOption>();
                // destinationCountries.addAll(FinanceFunctions.CountryDestinations());
                // destinationCountries.remove(0);
                // System.debug('==>:destinationCountries: '+destinationCountries);
                // // if(selectedCountry == null)
                // //  selectedCountry = destinationCountries[0].getValue();
                // selectedCountry = 'Australia';

                system.debug('allGroups==>' + allGroups);
				system.debug('agencyGroupOptions==>' + agencyGroupOptions);
				system.debug('allGroups==>' + allGroups);

				for(AggregateResult ar : [SELECT client_course__r.Course_Country__c ct FROM client_course_instalment__c WHERE (client_course__r.Enroled_by_Agency__r.ParentId IN :allGroups OR pay_to_agency__r.ParentId IN :allGroups OR (isRequestedShareCommission__c = TRUE AND client_course__r.share_commission_decision_status__c = 'Accepted' AND client_course__r.share_commission_request_agency__r.ParentId IN :allGroups) OR Received_By_Agency__r.ParentId IN :allGroups) AND Share_Commission_Paid_On__c = NULL  AND ((isNotNetCommission__c = TRUE AND isShare_Commission__c = FALSE ) OR (isShare_Commission__c = TRUE AND isShareCommissionSplited__c = TRUE)) AND (isMigrated__c = FALSE OR PFS_Pending__c = TRUE) AND Commission_Not_Claimable__c = FALSE AND( (isReceived_Enrolled_Different_Agency__c = TRUE AND client_course__r.Enroled_by_Agency__r.ParentId IN :allGroups AND Shared_Commission_Bank_Dep_Enrolled__r.Commission_Paid_Date__c != NULL ) OR (isRequestedShareCommission__c = TRUE AND client_course__r.share_commission_request_agency__r.ParentId IN :allGroups AND isReceived_Requested_Same_Agency__c = FALSE AND isEnrolled_Requested_Same_Agency__c = FALSE AND Shared_Commission_Bank_Dep_Request__r.Commission_Paid_Date__c != NULL) OR (pay_to_agency__c != NULL AND pay_to_agency__r.ParentId IN :allGroups AND isPayToAgency_Diff_Enroll_Receive_Agency__c = TRUE AND Shared_Commission_Bank_Dep_Pay_Agency__r.Commission_Paid_Date__c != NULL) OR (isNotNetCommission__c = TRUE  AND Received_By_Agency__r.ParentId IN :allGroups AND Shared_Commission_Bank_Dep_Received__r.Commission_Paid_Date__c != NULL)) group by client_course__r.Course_Country__c ORDER BY client_course__r.Course_Country__c])
					destinationCountries.add(new SelectOption((String) ar.get('ct'), (String) ar.get('ct')));

				if(destinationCountries.size()>0)
					selectedCountry = destinationCountries[0].getLabel();
            }
            return destinationCountries;
        }
        set;
    }

    public string selectedAgencyGroup{
        get{
            if(selectedAgencyGroup == null)
                selectedAgencyGroup = currentUser.Contact.Account.ParentId;
            return selectedAgencyGroup;
        }set;}

    private set<Id> allGroups {get{if(allGroups == null){agencyGroupOptions = null;}return allGroups;}set;}
    public List<SelectOption> agencyGroupOptions {
        get{
            if(agencyGroupOptions == null){
				allGroups = new set<id>();
                agencyGroupOptions = new List<SelectOption>();
                if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group
                    agencyGroupOptions.add(new SelectOption(currentUser.Contact.Account.ParentId, currentUser.Contact.Account.Parent.Name));
					allGroups.add(currentUser.Contact.Account.ParentId);
                }
                   else{
                    for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
                        agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
						allGroups.add(ag.Id);
                    }
                }
            }
            return agencyGroupOptions;
      }set;}

    public void changeGroup(){
        selectedAgency = '';
        getAgencies();
    }

    public String selectedAgency {get;set;}
    public list<Id> allAgencies {get;set;}
    public list<SelectOption> getAgencies(){
        List<SelectOption> result = new List<SelectOption>();
        allAgencies = new list<Id>();
        if(selectedAgencyGroup != null){
            if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
                selectedAgency = currentUser.Contact.AccountId;
                result.add(new SelectOption(currentUser.Contact.AccountId, currentUser.Contact.Account.Name));
                if(currentUser.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
						result.add(new SelectOption(ac.id, ac.name));
					}
				}
            }else{
                result.add(new SelectOption('all', ' -- All -- '));
                selectedAgency = 'all';
                for(Account a: [Select Id, Name from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
                    result.add(new SelectOption(a.Id, a.Name));
                    allAgencies.add(a.Id);
                }
            }
        }return result;}

    public String selectedTypeFilter {get;set;}
    public list<SelectOption> typeFilter {get{
        if(typeFilter==null){
            typeFilter = new list<SelectOption>();
            typeFilter.add(new SelectOption('range', 'Range'));
            typeFilter.add(new SelectOption('invoice', 'Invoice Number'));
        }
        return typeFilter;
    }set;}

    public client_course_instalment__c dateFilter{
        get{
            if(dateFilter == null){
                dateFilter = new client_course_instalment__c();
                Date myDate = Date.today();
                dateFilter.Discounted_On__c = system.today();
                dateFilter.Original_Due_Date__c = dateFilter.Discounted_On__c.addDays(-30);
            }
            return dateFilter;
        }
        set;
    }

    public String invoiceNumber {get;set;}

}