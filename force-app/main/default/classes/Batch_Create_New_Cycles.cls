public class Batch_Create_New_Cycles{
	public Batch_Create_New_Cycles(){}
}

/*global class Batch_Create_New_Cycles implements Database.Batchable<sObject> {
	
	String query;
	
	global Batch_Create_New_Cycles() {
		String query = 'SELECT ID FROM Contact ORDER BY CreatedDate DESC';
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		String query = 'SELECT ID, Agency_Group__c, Client__c, Destination_Country__c, Destination_City__c, Expected_Travel_date__c, Arrival_date__c FROM Destination_Tracking__c WHERE Current_Cycle__c = true';
				
		return Database.getQueryLocator(query);
	}
	

   	global void execute(Database.BatchableContext BC, List<Destination_Tracking__c> cycles) {
		List<Client_Stage_Follow_Up__c> checkedItensToUpdate = new List<Client_Stage_Follow_Up__c>();
		Map<String, Destination_Tracking__c> idsCycles = new Map<String, Destination_Tracking__c>();
		List<Client_Stage_Follow_Up__c> contactChecklist;
		Destination_Tracking__c trackingToCopy;
		Destination_Tracking__c newTracking;
		Map<String, Map<String, List<Client_Stage_Follow_Up__c>>> checkedItensPerCycle = new Map<String, Map<String, List<Client_Stage_Follow_Up__c>>>();
		for(Destination_Tracking__c cycle : cycles){
			idsCycles.put(cycle.ID, cycle);
		}

		contactChecklist = [SELECT Id, Destination_Tracking__c, Destination_Tracking__r.Agency_Group__c , Last_Saved_Date_Time__c, Agency_Group__c FROM Client_Stage_Follow_Up__c WHERE Destination_Tracking__c IN :idsCycles.keySet() ORDER BY Destination_Tracking__c, Last_Saved_Date_Time__c];

		if(contactChecklist != null && !contactChecklist.isEmpty()){
			for(Client_Stage_Follow_Up__c checked : contactChecklist){
				if(!checkedItensPerCycle.containsKey(checked.Destination_Tracking__c)){
					checkedItensPerCycle.put(checked.Destination_Tracking__c, new Map<String, List<Client_Stage_Follow_Up__c>>());
				}
				if(checked.Destination_Tracking__r.Agency_Group__c != checked.Agency_Group__c){
					if(!checkedItensPerCycle.get(checked.Destination_Tracking__c).containsKey(checked.Agency_Group__c)){
						checkedItensPerCycle.get(checked.Destination_Tracking__c).put(checked.Agency_Group__c, new List<Client_Stage_Follow_Up__c>());
					}
					checkedItensPerCycle.get(checked.Destination_Tracking__c).get(checked.Agency_Group__c).add(checked);
				}
			}
			for(String cycle : checkedItensPerCycle.keySet()){
				if(!checkedItensPerCycle.get(cycle).isEmpty()){
					trackingToCopy = idsCycles.get(cycle);
					for(String agencyGroup : checkedItensPerCycle.get(cycle).keySet()){
						newTracking = new Destination_Tracking__c();
						newTracking.Client__c = trackingToCopy.Client__c;
						newTracking.Agency_Group__c = agencyGroup;
						newTracking.Destination_Country__c = trackingToCopy.Destination_Country__c;
						newTracking.Destination_City__c = trackingToCopy.Destination_City__c;
						newTracking.Expected_Travel_date__c = trackingToCopy.Expected_Travel_date__c;
						newTracking.Arrival_date__c = trackingToCopy.Arrival_date__c;
						newTracking.Current_Cycle__c = true;

						insert newTracking;

						for(Client_Stage_Follow_Up__c checked : checkedItensPerCycle.get(cycle).get(agencyGroup)){
							checked.Destination_Tracking__c = newTracking.ID;
							checkedItensToUpdate.add(checked);
						}
					}
				}
			}
			if(!checkedItensToUpdate.isEmpty()){
				update checkedItensToUpdate;
			}
		}		
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}*/