public with sharing class school_commission_reminder {

	@InvocableMethod(label='School Commission Remidner' description='Send Email Reminder')
	public static void sendEmail (List<Id> invoiceId) {

		try{
			system.debug('invoiceId==>' + invoiceId);

			mandrillSendEmail mse = new mandrillSendEmail();
			List<Invoice__c> invoices = new list<Invoice__c>();
			

			for(Invoice__c invoice : [SELECT Id, Invoice_Activities__c, Total_Emails_Sent__c, School_Invoice_Number__c, Sent_Email_To__c, School_Id__c, Confirmed_Date__c, createdBy.Name, createdBy.Email FROM Invoice__c WHERE id IN :invoiceId]){

				//Double check if invoice is not confirmed
				if(invoice.Confirmed_Date__c == null){
					
					invoices.add(invoice);
				}
			}//end for

			//Known SF issue: Cannot use function getContentAsPDF(), created a batch for work around;
			batch_createPDFandSendEmail batch_createPDFandSendEmail = new batch_createPDFandSendEmail(invoices);
			Database.executeBatch(batch_createPDFandSendEmail,99);
		}
		catch(Exception e ){
			system.debug('Error===' + e);
			system.debug('Error Line ===' + e.getLineNumber());
		}
	}
}