/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ProductService_test {

    static testMethod void myUnitTest() {
        
        TestFactory tf = new TestFactory();       
		Account agency = tf.createAgency();		
		Contact employee = tf.createEmployee(agency);
		
		Account school = tf.createSchool();
				
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		p2.Website_Fee_Type__c = 'Homestay Single';
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;		
		insert p3;
		
		
		Account campus = tf.createCampus(school, agency);
		    
		Course__c course = tf.createCourse();
		
		Course__c course2 = tf.createLanguageCourse();
				
		Campus_Course__c cc = tf.createCampusCourse(campus, course);
		
		Campus_Course__c cc2 = tf.createCampusCourse(campus, course2);
       
		Course_Extra_Fee__c cef = tf.createCourseExtraFee(cc, 'Latin America', p2);
		
		Course_Extra_Fee_Dependent__c cefd = tf.createCourseRelatedExtraFee(cef, p3);
		
		Course_Extra_Fee__c cef2 = new Course_Extra_Fee__c();
		cef2.Campus__c = campus.Id;
		cef2.campus_parent__c = campus.id;
		cef2.Nationality__c = 'Published Price';
		cef2.Availability__c = 3;
		cef2.From__c = 2;
		cef2.Value__c = 100;		
		cef2.Optional__c = true;
		cef2.Product__c = p2.id;
		cef2.date_paid_from__c = system.today();
		cef2.date_paid_to__c = system.today().addDays(+31);
		insert cef2;
		
		tf.createCourseRelatedExtraFee(cef2, p3);
        
        Translation__C tr = tf.populateLabelTranslation();
        tr.extra_fees__c = p2.Website_Fee_Type__c + '> Casa de Familia';
        insert tr;
        
        Test.startTest();
        
        Set<String> ccs = new Set<String>{cc.id, cc2.id};
        
        ProductService ps = new ProductService();
        ps.getWebsiteAccommodationFees(ccs, 'Brazil', system.today(), 3, 'pt_BR');
        
        Test.stopTest();
        
    }
}