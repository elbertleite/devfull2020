@isTest
private class agency_products_sales_test {

   public static User portalUser {get{
	if (null == portalUser) {
	portalUser = [Select Id, Name, Contact.AccountId from User where email = 'test12345@test.com' limit 1];
	} return portalUser;} set;}

	@testSetup static void setup() {
		TestFactory tf = new TestFactory();
		tf.setupProdcutsTest();

		Contact client = [Select Id, Accountid From Contact limit 1];
	
		portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
		system.runAs(portalUser){
			
			//Add product to booking
			ApexPages.currentPage().getParameters().put('id', client.Id);
			products_search ps = new products_search();

			for(String av : ps.result.keySet())
				for(String cat : ps.result.get(av).keyset())
					for(products_search.products p : ps.result.get(av).get(cat)){
						p.product.Selected__c = true;

						for(Quotation_List_Products__c f : p.prodFees)
							f.isSelected__c = true;
					}

			ps.saveProduct();

			//Pay Product
			client_product_service__c cliProduct = [Select Id from client_product_service__c WHERE Related_to_Product__c = null limit 1];

			client_course_product_pay payProd = new client_course_product_pay(new ApexPages.StandardController(cliProduct));

			payProd.newPayDetails.Payment_Type__c = 'Creditcard';
			payProd.newPayDetails.Value__c = payProd.totalPay;
			payProd.addPaymentValue();
			payProd.savePayment();
		}
	}

	static testMethod void agencyProductsSales(){

		system.runAs(portalUser){
			agency_products_sales p = new agency_products_sales();

			p.getCategories();
			p.getcommType();
			p.getAgencies();

			list<SelectOption> agencyGroupOptions = p.agencyGroupOptions;
			p.changeGroup();
			list<SelectOption> agencyOptions = p.agencyOptions;
			p.changeAgency();
			p.selectedProvider = p.providersOpt[1].getValue();
			p.changeProvider();
			p.selectedCategory = p.categoriesOpt[1].getValue();
			p.changeCategory();
			p.getPeriods();
			p.getdateCriteriaOptions();

			p.dateCriteria = 'pc';
			p.saveUpdates();
			p.cancelUpdates();
			p.dateCriteria = 'cnp';
			p.searchExcel();
			p.generateExcel();
		}
	}
}