public class synchronizationPanel{
	
	/*public pageReference deleteRecords(){
		deleteBatch_Account batch = new deleteBatch_Account();
		Id batchId = Database.executeBatch(batch);
		return null;	
	}*/


	public Integer getSchool(){
		return [Select count() from Account A where recordtype.name = 'School'];
	}
	
	public Integer getContacts(){
		return [Select count() from Contact  where recordtype.name = 'School Campus Contact'];
	}
	
	public Integer getProducts(){
		return [Select count() from Product__c];
	}
	
	public Integer getCampus(){
		return [Select count() from Account A where recordtype.name = 'Campus'];
	}
	
	public Integer getCampusInstalPayDate(){
		return [Select count() from Campus_Instalment_Payment_Date__c];
	}
	public Integer getStartDateRange(){
		return [Select count() from Start_Date_Range__c];
	}
	public Integer getExtraFeeDependent(){
		return [Select count() from Course_Extra_Fee_Dependent__c];
	}
	
	public Integer getCampusCourse(){
		return [select count() from campus_course__c];
	}
	public Integer getCourses(){
		return [select count() from course__c];
	}
	public Integer getNationalityGroups(){
		return [select count() from Nationality_Group__c];
	}
	public Integer getNationalityMixs(){
		return [select count() from Nationality_Mix__c];
	}
	public Integer getVideos(){
		return [select count() from Video__c];
	}
	public Integer getCoursePrices(){
		return [select count() from course_price__c];
	}
	public Integer getCourseIntakeDates(){
		return [select count() from course_intake_date__c];
	}
	public Integer getAccountDocFile(){
		return [select count() from Account_Document_File__c];
	}
	public Integer getAccountPicFile(){
		return [select count() from Account_Picture_File__c];
	}
	public Integer getPromotions(){
		return [select count() from Deal__c];
	}
	
	public Integer getCourseExtraFee(){
		return [select count() from Course_Extra_Fee__c];
	}
	public Integer getTestimonial(){
		return [Select count() from Testimonial__c];
	}
	public Integer getTestimonialPicture(){
		return [Select count() from Testimonial_Picture__c];
	}
	/*public Integer getPartnerNetworkRecordConnection(){
		return [Select COUNT() from PartnerNetworkRecordConnection];
	}*/
}