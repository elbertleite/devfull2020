/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class ClientClassificationReport_test2 {
	
	
	static testMethod void myUnitTest() {
		User portalUser = [Select id, name from User where UserType = 'CspLitePortal' and isActive = true and ContactID != null and Contact.Account.Name like '%IP%Sydney%' limit 1];
    	
    	system.runAs(portalUser){
    		
    		
    		
	    	ClientClassificationReport ccr = new ClientClassificationReport(null);
	    	List<SelectOption> dummy = ccr.agencyGroupOptions;
	    	dummy = ccr.userOptions;
	    	dummy = ccr.agencyOptions;	    	
	    	dummy = ccr.destinations;
	    	dummy = ccr.clientClassifications;
	    	dummy = ccr.searchTypeOptions;
			List<SelectOption> stageOptions = ccr.stageOptions;
	    		    	
	    	ccr.selectedSearchType = 'dateOfItem';
	    	ccr.selectedStageSearch = 'ticked';
	    	ccr.fromDate.Expected_Travel_Date__c = System.today().addDays(-30);
	    	ccr.toDate.Expected_Travel_Date__c = System.today().addDays(10);
	    	ccr.getStages();
	    	ccr.selectedStage = 'Stage 1';
	    	ccr.search();
	    	
	    	ApexPages.currentPage().getParameters().put('userID', portalUser.id);
	    	ccr.searchClientsByUser();
	    	
	    	ccr.generateExcel();
	    	
	    	ccr.getquotesPerAgency();
	    	
	    	
	    	
	    	ccr.getClassficationTotalsPerAgency();
	    	ccr.getClassficationTotals();
	    	
	    	ccr.getExportList();
    	}
    	
	}
}