@IsTest
private class leads_distribution_test {
    static testMethod void myUnitTest() {
		TestFactory tf = new TestFactory();
		
		Account agency = tf.createAgency();

		tf.createChecklists(agency.Parentid);

		Contact emp = tf.createEmployee(agency);
		
		Contact lead2 = tf.createClient(agency);
		Contact lead3 = tf.createClient(agency);

		Contact client = tf.createClient(agency);

		User portalUser = tf.createPortalUser(emp);

		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);

       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);

		Test.startTest();
     	system.runAs(portalUser){
		 
			emp.Account = agency;
			portalUser.contact = emp;
			update portalUser;

            Destination_Tracking__c dt = new Destination_tracking__c();
            dt.Client__c = lead2.id;
            dt.Agency_Group__c = agency.ParentID;
            dt.Arrival_Date__c = system.today().addMonths(3);
            dt.Destination_Country__c = 'Australia';
            dt.Destination_City__c = 'Sydney';
            dt.Expected_Travel_Date__c = system.today().addMonths(3);
            dt.Current_Cycle__c = true;
            dt.lead_Cycle__c = false;
            insert dt;

            leads_distribution controller = new leads_distribution();
            leads_distribution.initPage();
            leads_distribution.loadGroups(agency.ID);
            leads_distribution.openClientTasks(lead2.id);
            leads_distribution.loadEmployees(agency.ID);
            leads_distribution.loadDepartments(agency.ID);
            leads_distribution.saveReportOptions(new Map<String, String>());
            leads_distribution.updateFieldsByAgency(agency.ID);
            leads_distribution.updateFieldsByDepartment(agency.ID, emp.Department__c);
            leads_distribution.loadAgencies(agency.ID, agency.ParentID, true, true, '');
            leads_distribution.loadStatuses(agency.ParentID, 'Stage 1', 'Australia');
            leads_distribution.updateFieldsByGroup(agency.ParentID);
            leads_distribution.updateFieldsByStage(agency.ParentID, 'Stage 1', 'Australia');
            leads_distribution.updateFieldsByDestination(agency.ParentID, 'Australia');
            leads_distribution.retrieveContactTypes('All');
            leads_distribution.retrieveContactTypes('Lead');
            leads_distribution.search(agency.ParentID, agency.ID, new List<String>{emp.id}, 'All', 'Australia', 'All', 'All', new List<String>{'Stage 1'}, 'All', 'Contact Created Date', '01/01/2001', '01/01/2020', emp.Department__c, true);
            leads_distribution.search(agency.ParentID, agency.ID, new List<String>{emp.id}, 'All', 'Australia', 'All', 'All', new List<String>{'Stage 1'}, 'All', 'Cycle Created Date', '01/01/2001', '01/01/2020', emp.Department__c, false);
            leads_distribution.search(agency.ParentID, agency.ID, new List<String>{emp.id}, 'All', 'Australia', 'All', 'All', new List<String>{'Stage 1'}, 'All', 'Expected Close Date', '01/01/2001', '01/01/2020', emp.Department__c, true);
            leads_distribution.search(agency.ParentID, agency.ID, new List<String>{emp.id}, 'All', 'Australia', 'All', 'All', new List<String>{'Stage 1'}, 'All', 'Visa Expiry Date', '01/01/2001', '01/01/2020', emp.Department__c, true);
            leads_distribution.search(agency.ParentID, agency.ID, new List<String>{emp.id}, 'All', 'Australia', 'All', 'All', new List<String>{'Stage 1'}, 'All', 'Added to Newsletter', '01/01/2001', '01/01/2020', emp.Department__c, true);
            leads_distribution.search(agency.ParentID, agency.ID, new List<String>{emp.id}, 'All', 'Australia', 'All', 'All', new List<String>{'Stage 1'}, 'All', 'Status Update', '01/01/2001', '01/01/2020', emp.Department__c, true);

            leads_distribution.saveFieldsForExcel(agency.ParentID, new List<String>{agency.ID},  new List<String>{emp.id}, 'All', 'Australia', 'All', 'All', new List<String>{'Stage 1'}, 'All', 'Contact Created Date', '01/01/2001', '01/01/2020', emp.Department__c);

            controller.generateExcelFile();

            agencies_sales_target ast = new agencies_sales_target();
            ast.saveTargets();

            Batch_Force_Database_Caching b = new Batch_Force_Database_Caching();

            new Contact_Visa_Expiry_Date();
            new batch_clientAddress();
            new ForceDatabaseCaching();
            new eport_school_year_to_year_test();

            Apexpages.currentPage().getParameters().put('id', agency.ID);

            new school_nationality_groups();
            new school_campus_payment_dates();
            new Batch_Fix_Empty_Status_Cycle();
            new API_v1_campusRanking();
            new API_v1_campusRankingHandler();
            new Update_Remote_Ids();

		}
	}
}