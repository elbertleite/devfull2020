/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Update_Document_Folder_test {
	
	static testMethod void myUnitTest() {
		
		TestFactory tf = new TestFactory();
		Account agency = tf.createAgency();
		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);
		
		Update_Document_Folder udf = new Update_Document_Folder(new ApexPages.StandardController(campus));
		
		udf.getFiles();
		udf.save();
		
		List<String> orderedFolders = udf.orderedFolders;
		udf.getFolderMap();
		udf.getFolderOptions();
		udf.edit();
		udf.cancelEdit();
		
		Account_Document_File__c adf = new Account_Document_File__c();
		Apexpages.currentPage().getParameters().put('fileId', tf.getFakeId(adf.getsObjectType()));
		udf.viewFile();
		udf.createFolders();
		
		String selectedFolder = udf.selectedFolder;
		
	}


}