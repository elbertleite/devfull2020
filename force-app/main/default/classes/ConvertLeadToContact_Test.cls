/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ConvertLeadToContact_Test {

    static testMethod void myUnitTest() {
        
        TestFactory factory = new TestFactory();
        
       	Map<String,String> recordTypes = new Map<String,String>();       
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
        
        
    	//Create Agency Group
		Account agencyGroup = factory.createAgencyGroup();
		
		//Create Agency
		Account agency = factory.createAgency();
		
		//Create Employee 
		Contact employee = factory.createEmployee(agency); 	
		
		//Create user	
		User userEmp = factory.createPortalUser(employee);
		  
        Test.startTest();
        
		Contact lead1 = new Contact();
		lead1.FirstName = 'Lead1 Test';
		lead1.LastName = 'da Silva';
		lead1.Full_Name__c = lead1.FirstName + ' ' + lead1.LastName;
		lead1.Email = 'johnthelead@test.com';
		lead1.RecordTypeId = recordTypes.get('Lead');
		lead1.Owner__c = UserInfo.getUserId();
		insert lead1;
		
		Lead l = new Lead();
		l.FirstName = 'John';
		l.LastName = 'The Lead';
		l.email = 'johnthelead@test.com';		
		l.Company = 'Test';
		l.Agency_Group__c = agency.ParentId;
		insert l;
		
		Lead l2 = new Lead();
		l2.FirstName = 'Jane';
		l2.LastName = 'The Lead';
		l2.Company = 'Test';
		l2.email = 'janethelead@test.com';		
		l2.Agency_Group__c =  agency.ParentId;
		l2.isLead__c = true;
		insert l2;
		
		l.isLead__c = true;
		update l;
		
		system.debug('lead 1 ===='  + l);
		system.debug('lead 12 ===='  + l2);
		
		List<Task> tl = new List<Task>();
	
		Task t = new Task();
		t.whoID = l.id;
		t.subject = 'A Task';
		t.activityDate = System.today().addDays(10);
		t.description = 'task description';
		t.Status = 'Completed';
		insert t;
		
		Task t2 = new Task();
		t2.whoID = l2.id;
		t2.subject = 'A Task';
		t2.activityDate = System.today().addDays(10);
		t2.description = 'task description';
		t2.Status = 'Completed';
		insert t2;
		
		
		Test.stopTest();
		
		


    }
}