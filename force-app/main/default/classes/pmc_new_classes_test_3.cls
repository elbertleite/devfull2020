@isTest
private class pmc_new_classes_test_3 {
    static testMethod void myUnitTest() {
		TestFactory tf = new TestFactory();
        
        Account agency = tf.createAgency();
        Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
        
        Map<String,String> recordTypes = new Map<String,String>();
        for( RecordType r :[select id, name from recordtype where isActive = true] ){
            recordTypes.put(r.Name,r.Id);
        }

        Contact client = tf.createClient(agency);

        Account schoolGroup = tf.createSchoolGroup();
        
        Account school = tf.createSchool();
        school.parentId = schoolGroup.id;
        update school;

        Account campus = tf.createCampus(school, agency);

        Course__c course = tf.createCourse();
        Campus_Course__c cc = tf.createCampusCourse(campus, course);

        Course_Price__c price = tf.createCoursePrice(cc, 'Published Price');

        Start_Date_Range__c sd = tf.createProdDateRange(agency, new Quotation_List_Products__c());

        Quotation__c quote = tf.createQuotation(client, cc);

        Apexpages.currentPage().getParameters().put('idCampus', campus.id);
        new_campus_course ncc = new new_campus_course();

        Apexpages.currentPage().getParameters().put('idCourse', cc.id);
        ncc = new new_campus_course();
        

        ncc.getCourseCategories();
        ncc.getCourses();
        ncc.getPeriods();

        ncc.changeCourseCategory();
        ncc.changeCourseAvailable();
        ncc.changeCourseIsPackage();
        ncc.changeSoldInBlocks();

        ncc.saveCourse();

        Apexpages.currentPage().getParameters().put('id', campus.id);
        school_finance_info sfi = new school_finance_info();
        Apexpages.currentPage().getParameters().put('id', school.id);
        sfi = new school_finance_info();
        sfi.saveFinanceInfo();
	}
    
}