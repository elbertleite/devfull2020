public with sharing class extProductsServices{
	
	private string AccoId{get;set;}
	private boolean editMode = false;
	public Account agency {get;set;}

	public User currentUser {get{if(currentUser==null) currentUser = IpFunctions.getUserInformation(UserInfo.getUserId()); return currentUser;}set;}
	
	public extProductsServices(ApexPages.StandardController controller){
		AccoId = controller.getId();


		if(ApexPages.currentPage().getParameters().get('gpid') != null)
			AccoId = ApexPages.currentPage().getParameters().get('gpid');
		
		if(AccoId != null)
			agency = searchDetails(AccoId); 
			searchAgencyProducts();
			String prodId = ApexPages.currentPage().getParameters().get('prodId'); 

			if(prodId != null){
				editMode = true;
				editProduct(prodId);	
			}else{ 
				searchAgencyProducts();
			}
	}
	
	private Account searchDetails(String id){
		return [Select Name, CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name, Nickname__c, Agency_Type__c, Group_Type__c, Phone, Skype__c,
						BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,  ParentId, Parent.Name, 
						Registration_name__c, Year_established__c, BusinessEmail__c, Website,
						( Select Account_Nickname__c, Account_Name__c, Account_Number__c, Bank__c, Branch_Address__c, Branch_Name__c, Branch_Phone__c, BSB__c, IBAN__c, Notes__c, Swift_Code__c from Bank_Details1__r)
				from Account where id = :id];
				
	}


	public void searchProduct(){ 
		//AccoId = ApexPages.currentPage().getParameters().get('AgencyId');
		agencyProducts = null;
		searchAgencyProducts();
	}


	
	public class productPrices{
		public Integer quantity{get; Set;}
		public decimal value{get; Set;}
	}
	
	public class productDetails{
		public Quotation_Products_Services__c product{get{if(product == null) product = new Quotation_Products_Services__c(); return product;} Set;}
		public list<productPrices> listPrices{get{if(listPrices == null) listPrices = new list<productPrices>(); return listPrices;} Set;}
	}
	
	public productDetails newProduct{
		get{
			if(newProduct == null){
				newProduct = new productDetails();
			}
			return newProduct;
		}
		Set;
	}
	
	public Quotation_list_products__c productItem{
		get{
			if(productItem == null) 
				productItem = new Quotation_list_products__c(); 
			return productItem;
		} 
		Set;
	}
	
	public list<Quotation_list_products__c> listProductItem{
		get{
			if(listProductItem == null) 
				listProductItem = new list<Quotation_list_products__c>(); 
			return listProductItem;
		} 
		Set;
	}
	
	
	public list<SelectOption> commissionType{
		get{
			if(commissionType == null){
				commissionType = new list<SelectOption>(); 
				commissionType.add(new SelectOption('0', '%'));
				commissionType.add(new SelectOption('1', 'Cash'));
			}
			return commissionType;
		} 
		Set;
	}
	
	public map<string,list<Quotation_Products_Services__c>> agencyProducts{get; set;}
	public list<String> listProducts{get;Set;}
	private  map<string,list<Quotation_Products_Services__c>> searchAgencyProducts(){
		if(agencyProducts == null){
			listProducts = new list<String>();
			agencyProducts = new map<string,list<Quotation_Products_Services__c>>();
			for(Quotation_Products_Services__c ps:[select id,Use_list_of_products__c,Agency__c,Product_Name__c,Quantity__c,Price__c,Description__c, Currency__c,Unit_description__c,Category__c,Country__c, Group_Other__c,selected__c, 
														Commission__c, Commission_Type__c,
													( Select Quantity__c, Quotation_Products_Services__c, Value__c, isSelected__c from Quotation_list_products__r order by Quantity__c) from Quotation_Products_Services__c where agency__c = :AccoId AND Provider__c = NULL order by Category__c, Product_Name__c]){
					
				if(!agencyProducts.containsKey(ps.Category__c)){
					listProducts.add(ps.Category__c);
					agencyProducts.put(ps.Category__c, new list<Quotation_Products_Services__c>{ps});
				}else{
					agencyProducts.get(ps.Category__c).add(ps);
				}								
			}					
		}
		return agencyProducts;
		
	}
	
	public PageReference refreshProducts(){
		agencyProducts = null;
		searchAgencyProducts();
		return null;
	}
	
	public PageReference deleteProdItems(){
		String prodId = ApexPages.currentPage().getParameters().get('prodId');
		String category = ApexPages.currentPage().getParameters().get('category');
		list<string> listItemsDelete = new list<string>();
		for(Quotation_Products_Services__c ap:agencyProducts.get(category)){
			if(ap.Id == prodId){
				System.debug('==>ap.Quotation_list_products__r:'+ap.Quotation_list_products__r);
				for(Quotation_List_Products__c lp:ap.Quotation_list_products__r){
					System.debug('==>lp.isSelected__c:'+lp.isSelected__c);
					if(lp.isSelected__c){
						listItemsDelete.add(lp.id);
					}
				}
			}
			
		}
		if(listItemsDelete.size() > 0)
			delete [Select Id from Quotation_List_Products__c where id in :listItemsDelete];
		agencyProducts = null;
		searchAgencyProducts();
		return null;
	}
	
		public PageReference deleteProduct(){
			String category = ApexPages.currentPage().getParameters().get('category');
			list<string> productDelete = new list<string>();
			for(Quotation_Products_Services__c ap:agencyProducts.get(category)){
				if(ap.Selected__c)
					productDelete.add(ap.id);
			}
			if(productDelete.size() > 0)
				delete [Select Id from Quotation_Products_Services__c where id in :productDelete];
			agencyProducts = null;
			searchAgencyProducts();
			return null;
		}
	
	private void editProduct(String pId){
		newProduct.product = [select id,Use_list_of_products__c,Agency__c,Product_Name__c,Quantity__c,Price__c,Description__c, Currency__c,Unit_description__c,Category__c,Country__c, Group_Other__c, Commission_type__c, Commission__c,
											( Select Quantity__c, Quotation_Products_Services__c, Value__c from Quotation_list_products__r order by Quantity__c) from Quotation_Products_Services__c where id = :pId ];
		listProductItem.addAll(newProduct.product.Quotation_list_products__r);
		
	}
	
	public List<SelectOption> getUnitsRange(){    	
        return xCourseSearchFunctions.getUnitsRange();
    }
	
	public List<SelectOption> getCurrencies() {
		//string agencyId = ApexPAges.CurrentPage().GetParameters().get('agId');
		if(ApexPages.currentPage().getParameters().get('gpid') != null)
			return xCourseSearchFunctions.getAgencyCurrencies(currentUser.Contact.Account.Id, null);
		else
			return xCourseSearchFunctions.getAgencyCurrencies(agency.Id, null);
	}
	
	public PageReference addPrice(){
		
		if(productItem.Quantity__c == null || productItem.Value__c==null){
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please fill the field Quantity and Value'));
			return null;
		}else{
			addfromFile = false;
			listProductItem.add(productItem);
			productItem = new Quotation_list_products__c(); 
			return null;			
		}
		
	}
	
	public list<integer> tablePrices {get{if(tablePrices == null) tablePrices = new LIST<integer>{1,2,3,4,5,6,7}; return tablePrices;} Set;}
	
	private boolean hasEmptyData {get{if(hasEmptyData==null) hasEmptyData = false; return hasEmptyData;}set;}
	public boolean showError {get{if(showError==null) showError = false; return showError;}set;}
	public PageReference saveproduct(){
		boolean hasEmptyData = false;
		showError = true;
		for(Quotation_list_products__c qp:listProductItem)
			if(qp.Quantity__c == null || qp.Value__c == null){
				hasEmptyData = true;
				break;
			}
			
		if(hasEmptyData){
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please fill all the blank fields'));
			return null;
		}
		
		else{
			if(!editMode)
				newProduct.product.Agency__c = AccoId;
			if(!listProductItem.isEmpty())
				newProduct.product.Use_list_of_products__c = true;
				
			if(addfromFile)
				delete [Select id from Quotation_List_Products__c  where Quotation_Products_Services__c = :newProduct.product.Id];
			
			upsert newProduct.product;
			for(Quotation_list_products__c qp:listProductItem){
				if(qp.Quotation_Products_Services__c == null)
					qp.Quotation_Products_Services__c = newProduct.product.Id;
			}
			
			upsert listProductItem;
			return null;
		}
		
		
		
	}
	
	
	//=======IMPORT FILE======================
	
	public Blob csvFileBody{get;set;}
	public string csvAsString{get;set;}
	public boolean addfromFile{get{if(addfromFile == null) addfromFile = false; return addfromFile;} set;}
	private class productFileDetails{
	 	private integer quantity;
	 	private decimal value;
		private productFileDetails(integer quantity, decimal value){
			this.quantity = quantity;
			this.value = value;
		}
	 }
	 
	  
	  public void importCSVFile(){
	  	addfromFile = true;
	  	listProductItem = new list<Quotation_list_products__c>(); 
	  	csvAsString = csvFileBody.toString();
	    String[] csvFileLines = csvAsString.split('\n'); 
	    System.debug('==>csvFileLines: '+csvFileLines);
	    for(Integer i=1;i<csvFileLines.size();i++){
	        string[] csvRecordData = csvFileLines[i].split(',');
	  		listProductItem.add(new Quotation_list_products__c(Quantity__c = integer.valueOf(csvRecordData[0].trim()),Value__c = decimal.valueOf(csvRecordData[1].trim()) ));
	    }
	 }
}