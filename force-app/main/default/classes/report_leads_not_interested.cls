public with sharing class report_leads_not_interested {

	public User user{get;set;}
	public List<SelectOption> agencies{get;set;}
	public List<SelectOption> employees{get;set;}

	public Integer totalResult{get;set;}
	public Integer totalLost{get;set;}
	public Integer totalCancelled{get;set;}

	public List<ContactReport> contacts{get;set;}
	public Map<String, List<String>> phones{get;set;}

	public String funnelReport{get;set;}
	public String leadsLostReport{get;set;}
	public String leadsCancelledReport{get;set;}

	public List<StageFilter> stagesFilter{get;set;}
	public List<StatusFilter> statusFilters{get;set;}
	public String agencySelected{get;set;}
	public String groupSelected{get;set;}
	public String destinationSelected{get;set;}
	public String reasonSelected{get;set;}

	public String filterByDate{get;set;}

	public String endDate{get;set;}
	public String beginDate{get;set;}
	public String visaExpiryEndDate{get;set;}
	public String visaExpiryBeginDate{get;set;}


	public String employeeSelected{get;set;}

	public boolean showStatusFilter{get;set;}
	public boolean showExtraFilters{get;set;}
	public boolean showContactsStillOnStatus{get;set;}

	public boolean startingPage{get;set;}
	public boolean canExportToExcel{get;set;}

	private Map<String, String> filtersForExcel{get;set;}

	// private String queryContact = 'SELECT ID, Name, Email, Phone, MobilePhone, CreatedBy.Name, CreatedDate, Lead_Status_Date_Time__c, Lead_Lost_Comment__c, Lead_Stage__c, Last_Stage_Before_Not_Interested__c, Lead_Status_Reason__c, Destination_Country__c, Owner__r.Name, Lead_Assignment_To__r.Name, Lead_Assignment_To__c, Current_Agency__c, Current_Agency__r.Name, Current_Agency__r.Parent.Name, Original_Agency__r.Name, Original_Agency__r.Parent.Name, Visa_Expiry_Date__c FROM Contact WHERE RecordType.Name IN (\'Lead\',\'Client\') AND Current_Agency__r.parent.ID = :groupSelected AND Status__c = \'LEAD - Not Interested\' ';
	private String queryContact = 'SELECT ID, Name, Email, Phone, Nationality__c, MobilePhone, CreatedBy.Name, CreatedDate, Lead_Status_Date_Time__c, Status__c, Lead_Lost_Comment__c, Lead_Stage__c, Last_Stage_Before_Not_Interested__c, Lead_Status_Reason__c, Destination_Country__c, Owner__r.Name, Lead_Assignment_To__r.Name, Lead_Assignment_To__c, Current_Agency__c, Current_Agency__r.Name, Current_Agency__r.Parent.Name, Original_Agency__r.Name, Original_Agency__r.Parent.Name, Visa_Expiry_Date__c FROM Contact WHERE RecordType.Name IN (\'Lead\',\'Client\') AND Current_Agency__r.parent.ID = :groupSelected ';
	
	public report_leads_not_interested() {
		try{
			showExtraFilters = false;
			user = [Select Id, Name, Contact.Export_Report_Permission__c, Contact.AccountId, Contact.Account.ParentId, Contact.Group_View_Permission__c, Contact.Agency_View_Permission__c, Aditional_Agency_Managment__c, Report_Leads_Lost_Filters__c FROM  User where Id = :UserInfo.getUserId()];

			canExportToExcel = user.Contact.Export_Report_Permission__c;

			if(!String.isEmpty(ApexPages.currentPage().getParameters().get('export')) && Boolean.valueOf(ApexPages.currentPage().getParameters().get('export'))){
				filtersForExcel = (Map<String, String>) JSON.deserialize(user.Report_Leads_Lost_Filters__c, Map<String, String>.class);	
				generateListContacts();
			}else{
				showContactsStillOnStatus = false;
				startingPage = true;
				totalResult = 0;
				endDate = Date.today().format();
				beginDate = Date.today().addYears(-1).format();
				agencySelected = user.Contact.AccountId;
				groupSelected = user.Contact.Account.ParentId;
				destinationSelected = '';
				reasonSelected = '';
				this.agencies = getAgenciesUser(user.Contact.Account.ParentId);
				this.employees = loadEmployees(user.Contact.AccountId);
				this.stagesFilter = getStages();
				employeeSelected = user.Contact.ID;
				filterByDate = 'lostDate';
				checkStatus();
				//test();
				//generateReports();
			}

		}catch(Exception e){
			system.debug('Error on report_leads_not_interested() ===> ' + e.getLineNumber() + ' '+e.getMessage());	
		}
	}

	/*public void test(){

		Map<String, Client_Stage__c> stageZeroItens = new Map<String, Client_Stage__c>();
        for(Client_Stage__c stage : [SELECT ID, Stage_description__c, Stage__c, itemOrder__c FROM Client_Stage__c WHERE Stage__c = 'Stage 0' AND Agency_Group__c = '0019000001MjqDX' AND Stage_description__c != 'LEAD - Not Interested'  ORDER BY itemOrder__c DESC]){
			if(!stage.Stage_description__c.contains('xxx')){
				stageZeroItens.put(stage.Stage_description__c, stage);
			}
		}


		String [] ids = new String [] {'0039000002Nipm2AAB'};
        String query = 'SELECT ID, Client__c FROM Destination_Tracking__c WHERE Agency_Group__c = \'0019000001MjqDX\' AND Current_Cycle__c = true AND Client__c IN :ids'; 

		List<String> idCycles = new List<String>();
        Map<String, List<Client_Stage_Follow_up__c>> contactsChecklist = new Map<String, List<Client_Stage_Follow_up__c>>();
        
        for(Destination_Tracking__c cycle : Database.query(query)){
            idCycles.add(cycle.ID);
        }

		List<Client_Stage_Follow_up__c> itensToUpdate = new List<Client_Stage_Follow_up__c>();
        for(Client_Stage_Follow_up__c check : [SELECT ID, Client__c, Client__r.Name, Agency_Group__c, Agency__c, Destination__c, Destination_Tracking__c, Stage_Item__c, Stage_Item_Id__c, Stage__c, Last_Saved_Date_Time__c, Checked_By__c FROM Client_Stage_Follow_up__c WHERE Stage__c = 'Stage 0' AND Agency_Group__c = '0019000001MjqDX' AND Destination_Tracking__c in :idCycles ORDER BY Client__c, Last_Saved_Date_Time__c DESC]){
			if(!contactsChecklist.containsKey(check.Client__c)){
				contactsChecklist.put(check.Client__c, new List<Client_Stage_Follow_up__c>()); 
			}
			contactsChecklist.get(check.Client__c).add(check);
		}

		system.debug('contactsChecklist '+JSON.serialize(contactsChecklist));

		List<ReferenceDateByCheck> referenceDates;
		Integer totalDays;
		for(String contact : contactsChecklist.keySet()){
			referenceDates = generateReferenceDates(stageZeroItens, contactsChecklist.get(contact));
			system.debug('referenceDates '+JSON.serialize(referenceDates));
			for(Integer i = 0; i < referenceDates.size(); i++){
				if(i + 1 < referenceDates.size()){
					system.debug('CHEKING ITEM '+referenceDates.get(i).item.Stage_Item__c);
					totalDays = referenceDates.get(i).item.Last_Saved_Date_Time__c.date().daysBetween(referenceDates.get(i+1).item.Last_Saved_Date_Time__c.date());
					system.debug('CHEKING TOTAL DAYS '+totalDays);
					if(totalDays < 0){
						referenceDates.get(i).item.Last_Saved_Date_Time__c = referenceDates.get(i+1).item.Last_Saved_Date_Time__c.addSeconds(-1);
						itensToUpdate.add(referenceDates.get(i+1).item);
					}
				}
			}
		} 
	}*/

	private List<ReferenceDateByCheck> generateReferenceDates(Map<String, Client_Stage__c> stageZeroItens, List<Client_Stage_Follow_up__c> checklist){
        List<ReferenceDateByCheck> itens = new List<ReferenceDateByCheck>();
        for(Client_Stage_Follow_up__c checked : checklist){
            if(stageZeroItens.get(checked.Stage_Item__c) != null){
                itens.add(new ReferenceDateByCheck(stageZeroItens.get(checked.Stage_Item__c).itemOrder__c, checked.Last_Saved_Date_Time__c, checked));
            }   
        }
        itens.sort();
        return itens;
    }

	public class ReferenceDateByCheck implements Comparable {
        public Decimal itemOrder{get;set;}
        public Datetime dateChecked{get;set;}
		public Client_Stage_Follow_up__c item{get;set;}

        public ReferenceDateByCheck(Decimal itemOrder, Datetime dateChecked, Client_Stage_Follow_up__c item){
            this.itemOrder = itemOrder;
            this.dateChecked = dateChecked;
            this.item = item;
        }

        public Integer compareTo(Object compareTo) {
			ReferenceDateByCheck oh1 = (ReferenceDateByCheck)compareTo;
			if(this.itemOrder > oh1.itemOrder){
				return 1;
			}else if(this.itemOrder < oh1.itemOrder){
				return -1;
			}
			return 0;
		}
        public Boolean equals(Object obj) {
            if (obj instanceof ReferenceDateByCheck) {
                ReferenceDateByCheck s = (ReferenceDateByCheck)obj;
				return itemOrder == s.itemOrder;
            }
            return false;
        }

        public Integer hashCode() {
            return itemOrder.intValue();
        }
    }

	public void generateListContacts(){
		try{
			Set<String> idsAgencies = null;
			Date fromDate = null;
			Date toDate = null;
			Date fromDateVisaExpiryDate = null;
			Date toDateVisaExpiryDate = null;
			List<String> stages = null;
			String currentStage = null;
			List<String> statusStageSelected = null;
			showContactsStillOnStatus = false;
			
			groupSelected = filtersForExcel.get('groupSelected');
			
			filterByDate = filtersForExcel.get('filterByDate');

			if(filterByDate == 'lostDate' || filterByDate == 'createdDate'){
				beginDate = filtersForExcel.get('beginDate');
				endDate = filtersForExcel.get('endDate');
				
				fromDate = Date.parse(beginDate);
				toDate = Date.parse(endDate);
			}else{
				visaExpiryBeginDate = filtersForExcel.get('visaExpiryBeginDate');
				visaExpiryEndDate = filtersForExcel.get('visaExpiryEndDate');
				
				fromDateVisaExpiryDate = Date.parse(visaExpiryBeginDate);
				toDateVisaExpiryDate = Date.parse(visaExpiryEndDate);
				showContactsStillOnStatus = filtersForExcel.get('showContactsStillOnStatus') == 'true';
			}		

			employeeSelected = filtersForExcel.get('employeeSelected');
			destinationSelected = filtersForExcel.get('destinationSelected');
			reasonSelected = filtersForExcel.get('reasonSelected');
			
			if(filtersForExcel.get('multipleStages') == 'true'){
				if(filtersForExcel.containsKey('stages')){
					stages = (List<String>) JSON.deserialize(filtersForExcel.get('stages'), List<String>.class);
				}
			}else{
				currentStage = (String) filtersForExcel.get('currentStage');	
				if(filtersForExcel.containsKey('statusStageSelected')){
					statusStageSelected = (List<String>) JSON.deserialize(filtersForExcel.get('statusStageSelected'), List<String>.class);
				}

			}
			if(filtersForExcel.containsKey('multipleAgencies')){
				idsAgencies = (Set<String>) JSON.deserialize(filtersForExcel.get('agencySelected'), Set<String>.class);
			}else{
				agencySelected = filtersForExcel.get('agencySelected');
			}		

			String queryAnd = generateQueryAnd();
			String query;
			if(currentStage != null){
				query = queryContact + queryAnd + ' AND Lead_Stage__c = :currentStage ';
				if(!showContactsStillOnStatus && statusStageSelected != null){
					query = query + ' AND Last_Stage_Before_Not_Interested__c IN :statusStageSelected ';
				}
			}else{
				query = queryContact + queryAnd + ' AND Lead_Stage__c IN :stages ';
			}

			query = query + ' ORDER BY Lead_Status_Date_Time__c, Name';
			system.debug('THE QUERY '+query);
			contacts = new List<ContactReport>();
			ContactReport contact;
			Set<String> ids = new Set<String>();
			Map<String, String> defaultPhone = new Map<String, String>(); 
			List<Contact> contactsQuery = new MethodsWithoutSharing().runQuery(query, groupSelected, fromDate, toDate, employeeSelected, destinationSelected, reasonSelected, stages, currentStage, statusStageSelected, idsAgencies, agencySelected, fromDateVisaExpiryDate, toDateVisaExpiryDate);
			if(Test.isRunningTest()){
				Contact ctt = new Contact();
				ctt.FirstName = 'Teste';
				ctt.LastName = 'Teste';
				ctt.Email= 'test@test.com';
				ctt.Status__c = 'LEAD - Not Interested';
				ctt.Lead_Status_Date_Time__c = Datetime.now();
				insert ctt;
				contactsQuery.add(ctt);
			}
			phones = new Map<String, List<String>>();
			for(Contact ctt : contactsQuery){
				if(showContactsStillOnStatus || ctt.Status__c == 'LEAD - Not Interested'){
					ids.add(ctt.ID);
					phones.put(ctt.ID, new List<String>());
					if(!String.isEmpty(ctt.MobilePhone) && ctt.MobilePhone != '000-null'){
						phones.get(ctt.ID).add(ctt.MobilePhone);
					}
					if(!String.isEmpty(ctt.Phone) && ctt.Phone != '000-null'){
						phones.get(ctt.ID).add(ctt.Phone);
					}
					contact = new ContactReport();
					contact.Id = ctt.ID ;
					contact.Name = ctt.Name ;
					contact.Email = ctt.Email ;
					contact.nationality = ctt.Nationality__c ;
					if(!Test.isRunningTest()){
						contact.CreatedDate = ctt.CreatedDate.date();
					}
					contact.CreatedBy = ctt.CreatedBy.Name ;
					contact.visaExpiryDate = ctt.Visa_Expiry_Date__c;
					if(ctt.Status__c == 'LEAD - Not Interested'){
						contact.StatusBeforeCancelledLost = ctt.Last_Stage_Before_Not_Interested__c ;
						if(!Test.isRunningTest()){
							contact.TimebeforeLost = ctt.CreatedDate.date().daysBetween(ctt.Lead_Status_Date_Time__c.date()) + ' Days' ;
							contact.LostCancelledDate = ctt.Lead_Status_Date_Time__c.date() ;
						}
						contact.active = 'NO';
						contact.Reason = ctt.Lead_Status_Reason__c;
						contact.Comment = ctt.Lead_Lost_Comment__c;
					}else{
						contact.StatusBeforeCancelledLost = ' - ';
						contact.LostCancelledDate = null;
						contact.TimebeforeLost = ' - ';
						contact.active = 'YES';
						contact.Reason = ' - ';
						contact.Comment = ' - ';
					}
					contact.Stage = ctt.Lead_Stage__c ;
					contact.Destination = String.isEmpty(ctt.Destination_Country__c) ? 'No destination defined.' : ctt.Destination_Country__c;			
					contact.AssignedTo = String.isEmpty(ctt.Owner__r.Name) ? 'No Owner.' : ctt.Owner__r.Name;		
					contact.CurrentAgency = String.isEmpty(ctt.Current_Agency__c) ? 'No Current Agency Defined.' : ctt.Current_Agency__r.Name;
					contact.CreatedAgency = String.isEmpty(ctt.Original_Agency__r.Name) ? 'No Original Agency Defined.' : ctt.Original_Agency__r.Name;	
					contacts.add(contact);

					/*if(ctt.MobilePhone != null){
						defaultPhone.put(ctt.ID, ctt.MobilePhone);
					}else{
						defaultPhone.put(ctt.ID, ctt.Phone);
					}*/
				}	
			}
			
			String [] types = new String [] {'Home Phone','Mobile'};
			for(Forms_of_contact__C ctt : [SELECT id, Detail__c, Contact__c, Country__c, From_Website__c from Forms_of_contact__C WHERE Contact__c IN :ids AND Type__c IN :types AND (CreatedBy.Contact.Account.Parent.ID = :groupSelected OR CreatedBy.Contact.Account.Parent.ID = null) ORDER BY Type__c DESC, CreatedDate]){
				phones.get(ctt.Contact__c).add(ctt.Detail__c);
			}
			Map<String, String> statusesPerClient = new Map<String, String>(); 
			for(Client_Stage_Follow_up__c check : [SELECT ID, Client__c, Stage_Item__c, Stage__c FROM Client_Stage_Follow_up__c WHERE Destination_Tracking__r.Agency_Group__c = :groupSelected AND Agency_Group__c = :groupSelected AND Destination_Tracking__r.Client__c in :ids AND Destination_Tracking__r.Current_Cycle__c = true ORDER BY Client__c, Last_Saved_Date_Time__c]){
				if(!statusesPerClient.containsKey(check.Client__c)){
					statusesPerClient.put(check.Client__c, check.Stage__c + ' - ' + check.Stage_Item__c);
				}
			}
			for(ContactReport ctt : contacts){
				ctt.firstStatusCycle = statusesPerClient.get(ctt.id);
			}

		}catch(Exception e){
			system.debug('Error on generateListContacts() ===> ' + e.getLineNumber() + ' '+e.getMessage());	
		}
	}

	public void generateReports(){
		try{
			user.Report_Leads_Lost_Filters__c = null;

			filtersForExcel = new Map<String, String>();

			totalResult = 0;
			totalLost = 0;
			totalCancelled = 0;
			startingPage = false;
			String queryResult = null;
			List<Contact> ctts = null;

			ReportResult reportDataEntry;
			IPClasses.DataReport dataEntry = null;
			List<ReportResult> dataResult = new List<ReportResult>();
			List<IPClasses.DataReport> dataResultLost = new List<IPClasses.DataReport>();
			List<IPClasses.DataReport> dataResultCancelled = new List<IPClasses.DataReport>();
			
			String queryAnd = generateQueryAnd();
			Set<String> idsAgencies = getAgenciesForQuery();

			Date fromDate = null;
			Date toDate = null;
			Date fromDateVisaExpiryDate = null;
			Date toDateVisaExpiryDate = null;
			if(filterByDate == 'lostDate' || filterByDate == 'createdDate'){
				fromDate = Date.parse(beginDate);
				toDate = Date.parse(endDate);
			}else{
				fromDateVisaExpiryDate = Date.parse(visaExpiryBeginDate);
				toDateVisaExpiryDate = Date.parse(visaExpiryEndDate);
			}
			
			List<String> stages = new List<String>();

			String fieldToLabelMainGraph;
			String fieldToLabelSecondaryGraph;
		
			for(StageFilter stage : stagesFilter){
				if(stage.selected){
					stages.add(stage.stage);
				}
			}

			if(stages.size() == 1){
				String currentStage = stages.get(0);
				
				List<String> statusStageSelected = new List<String>();
				for(StatusFilter status : statusFilters){
					if(status.selected){
						statusStageSelected.add(status.status);
					}
				}

				/*This query just gets the statuses saved on the settings to show them in the graph*/
				String queryStatuses = 'SELECT ID, Stage_description__c FROM Client_Stage__c WHERE Agency_Group__c = :groupSelected AND Stage__c = :currentStage ';
				if(!String.isEmpty(destinationSelected) && currentStage != 'Stage 0'){ 
					queryStatuses = queryStatuses + ' AND Destination__c = :destinationSelected ';
				}
				List<Client_Stage__c> statusStages = Database.query(queryStatuses + 'ORDER BY ItemOrder__c');
				
				for(Client_Stage__c stage : statusStages){
					if(stage.Stage_description__c != 'LEAD - Not Interested' && statusStageSelected.contains(stage.Stage_description__c)){
						/*dataEntry = new IPClasses.DataReport(stage.Stage_description__c.toUpperCase() , 0);
						dataEntry.color = '#D81921';*/
						//dataResult.add(dataEntry);
						reportDataEntry = new ReportResult();
						reportDataEntry.status = stage.Stage_description__c.toUpperCase();
						reportDataEntry.lost = 0;
						reportDataEntry.won = 0;
						reportDataEntry.colorWon = '#92d465';
						reportDataEntry.colorLost = '#da3c3d'; 
						dataResult.add(reportDataEntry);
					}
				}

				filtersForExcel.put('multipleStages', 'false');
				filtersForExcel.put('currentStage', currentStage);
				filtersForExcel.put('statusStageSelected', JSON.serialize(statusStageSelected));

				/*This query only gets the statuses saved on the settings to show them in the graph*/

				queryResult = queryContact + queryAnd + ' AND Lead_Stage__c = :currentStage ';
				if(!showContactsStillOnStatus){
					queryResult = queryResult + ' AND Last_Stage_Before_Not_Interested__c IN :statusStageSelected '; //THIS QUERY RESTRICTS THE RESULT FOR THE SELECTED STATUSES ONLY
				}

				fieldToLabelMainGraph = 'Last_Stage_Before_Not_Interested__c';
				fieldToLabelSecondaryGraph = 'Lead_Status_Reason__c';
			}else{
				queryResult = queryContact + queryAnd + ' AND Lead_Stage__c IN :stages ';

				fieldToLabelMainGraph = 'Lead_Stage__c';
				fieldToLabelSecondaryGraph = 'Lead_Status_Reason__c';

				for(String stage : stages){
					/*dataEntry = new IPClasses.DataReport(stage.toUpperCase(), 0);
					dataEntry.color = '#D81921';*/
					//dataResult.add(dataEntry);
					reportDataEntry = new ReportResult();
					reportDataEntry.status = stage.toUpperCase();
					reportDataEntry.lost = 0;
					reportDataEntry.won = 0; 
					reportDataEntry.colorWon = '#92d465';
					reportDataEntry.colorLost = '#da3c3d'; 
					dataResult.add(reportDataEntry);
				}

				filtersForExcel.put('multipleStages', 'true');
				filtersForExcel.put('stages', JSON.serialize(stages));
			}	
			
			system.debug('THE QUERY '+queryResult);
			ctts = Database.query(queryResult);

			if(ctts != null && !ctts.isEmpty()){
				String graphStage;
				String graphReason;
				for(Contact ctt : ctts){
					if(fieldToLabelMainGraph == 'Last_Stage_Before_Not_Interested__c'){
						if(ctt.Status__c == 'LEAD - Not Interested'){
							graphStage = String.isEmpty(ctt.Last_Stage_Before_Not_Interested__c) ? 'NO STATUS DEFINED' : ctt.Last_Stage_Before_Not_Interested__c.toUpperCase();
						}else{
							if(showContactsStillOnStatus){
								graphStage = String.isEmpty(ctt.Status__c) ? 'NO STATUS DEFINED' : ctt.Status__c.toUpperCase();
							}
						}
					}else{
						graphStage = String.isEmpty(ctt.Lead_Stage__c) ? 'NO STAGE DEFINED' : ctt.Lead_Stage__c.toUpperCase();
					}

					reportDataEntry = new ReportResult();
					reportDataEntry.status = graphStage;
					if(dataResult.indexOf(reportDataEntry) < 0){
						reportDataEntry.lost = 0;
						reportDataEntry.won = 0; 
						reportDataEntry.colorWon = '#92d465';
						reportDataEntry.colorLost = '#676767';
						dataResult.add(reportDataEntry);
					}else{
						reportDataEntry = dataResult.get(dataResult.indexOf(reportDataEntry));
					}
					if(ctt.Status__c == 'LEAD - Not Interested'){
						totalResult += 1;
						reportDataEntry.lost += 1;
					}else{
						if(showContactsStillOnStatus){
							totalResult += 1;
							reportDataEntry.won += 1;
						}
					}
					/*dataEntry = new IPClasses.DataReport(graphStage);
					if(dataResult.indexOf(dataEntry) < 0){
						dataEntry.value = 1;
						dataEntry.color = '#676767';
						//dataResult.add(dataEntry);
					}else{
						dataEntry = dataResult.get(dataResult.indexOf(dataEntry));
						dataEntry.value += 1;
					}*/

					if(fieldToLabelSecondaryGraph == 'Lead_Status_Reason__c'){
						graphReason = String.isEmpty(ctt.Lead_Status_Reason__c) ? 'NO REASON DEFINED' : ctt.Lead_Status_Reason__c.toUpperCase();
					}else{
						graphReason = String.isEmpty(ctt.Last_Stage_Before_Not_Interested__c) ? 'NO REASON DEFINED' : ctt.Last_Stage_Before_Not_Interested__c.toUpperCase();
					}
					
					dataEntry = new IPClasses.DataReport(graphReason);

					if(ctt.Status__c == 'LEAD - Not Interested'){
						if(ctt.Lead_Stage__c == 'Stage 0'){
							totalLost += 1;
							if(dataResultLost.indexOf(dataEntry) < 0){
								dataEntry.value = 1;
								//dataEntry.color = '#676767';
								dataResultLost.add(dataEntry);
							}else{
								dataEntry = dataResultLost.get(dataResultLost.indexOf(dataEntry));
								dataEntry.value += 1;
							}		
						}else{
							totalCancelled += 1;
							if(dataResultCancelled.indexOf(dataEntry) < 0){
								dataEntry.value = 1;
								dataEntry.color = '#D81921';
								dataResultCancelled.add(dataEntry);
							}else{
								dataEntry = dataResultCancelled.get(dataResultCancelled.indexOf(dataEntry));
								dataEntry.value += 1;
							}		
						}
					}
				}
				//totalResult = ctts.size();

				user.Report_Leads_Lost_Filters__c = JSON.serialize(filtersForExcel);
			}

			update user;

			funnelReport = JSON.serialize(dataResult);
			leadsLostReport = JSON.serialize(dataResultLost);
			leadsCancelledReport = JSON.serialize(dataResultCancelled);
			
			resetFilters();
		}catch(Exception e){
			system.debug('Error on generateReport() ===> ' + e.getLineNumber() + ' '+e.getMessage());	
		}
	}

	private Set<String> getAgenciesForQuery(){
		Set<String> agencies = null;
		if(!String.isEmpty(agencySelected) && agencySelected.equals('All')){
			agencies = new Set<String>();
			for(SelectOption option : this.agencies){
				if(option.getValue() != 'All'){
					agencies.add(option.getValue());
				}
			}
			filtersForExcel.put('agencySelected', JSON.serialize(agencies));
			filtersForExcel.put('multipleAgencies', 'true');
		}else{
			filtersForExcel.put('agencySelected', agencySelected);
		}
		return agencies;
	}

	private String generateQueryAnd(){
		String query = '';

		filtersForExcel.put('groupSelected', groupSelected);
		filtersForExcel.put('filterByDate', filterByDate);

		if(filterByDate == 'lostDate' || filterByDate == 'createdDate'){
			if(filterByDate == 'lostDate'){
				query = query + ' AND DAY_ONLY(Lead_Status_Date_Time__c) >= :fromDate '; 
				query = query + ' AND DAY_ONLY(Lead_Status_Date_Time__c) <= :toDate ';
			}else{
				query = query + ' AND DAY_ONLY(CreatedDate) >= :fromDate '; 
				query = query + ' AND DAY_ONLY(CreatedDate) <= :toDate ';
			}
			
			filtersForExcel.put('beginDate', beginDate);
			filtersForExcel.put('endDate', endDate);
		}else{
			query = query + ' AND Visa_Expiry_Date__c >= :fromDateVisaExpiryDate '; 
			query = query + ' AND Visa_Expiry_Date__c <= :toDateVisaExpiryDate ';
			
			filtersForExcel.put('visaExpiryBeginDate', visaExpiryBeginDate);
			filtersForExcel.put('visaExpiryEndDate', visaExpiryEndDate);
			filtersForExcel.put('showContactsStillOnStatus', String.valueOf(showContactsStillOnStatus));
		}
		if(!String.isEmpty(agencySelected)){
			if(agencySelected == 'All'){
				query = query + ' AND Current_Agency__r.ID IN :idsAgencies ';
			}else{
				query = query + ' AND Current_Agency__r.ID = :agencySelected ';
			}
		}
		if(!String.isEmpty(destinationSelected)){ 
			filtersForExcel.put('destinationSelected', destinationSelected);
			query = query + ' AND Destination_Country__c = :destinationSelected ';
		}
		if(!String.isEmpty(reasonSelected)){
			filtersForExcel.put('reasonSelected', reasonSelected);
			query = query + ' AND Lead_Status_Reason__c = :reasonSelected ';
		}
		if(!String.isEmpty(employeeSelected)){
			//userSelected = [SELECT ID FROM User WHERE Contact.ID = :employeeSelected].ID;
			filtersForExcel.put('employeeSelected', employeeSelected);
			query = query + ' AND Owner__r.Contact.ID = :employeeSelected ';
		}
		return query;
	}

	public void checkStages(){
		Integer countActiveStages = 0;
		String stageSelected = null;
		showStatusFilter = false;
		for(StageFilter stage : stagesFilter){
			if(stage.selected){
				++countActiveStages;
				stageSelected = stage.stage;
			}
		}
		if(countActiveStages == 1){
			showStatusFilter = true;
			this.statusFilters = getStatus(stageSelected);
		}
		resetFilters();
	}

	public void updateAgencies(){
		this.groupSelected = ApexPages.currentPage().getParameters().get('idGroup');
		//this.agencies = loadAgencies(user.Contact.AccountId, idGroup);
		this.agencies = getAgenciesUser(this.groupSelected);
		this.stagesFilter = getStages();
		checkStatus();
		agencySelected = 'All';		
		employeeSelected = '';
		resetFilters();
		this.employees = loadEmployees(null);		
	}

	public void updateEmployees(){
		employeeSelected = '';
		this.agencySelected = ApexPages.currentPage().getParameters().get('idAgency');
		this.employees = loadEmployees(this.agencySelected);		
		resetFilters();
	}

	public void updateFilterDates(){
		this.filterByDate = ApexPages.currentPage().getParameters().get('filterDate');
		system.debug('THE NEW VALUE '+this.filterByDate);
		/*this.endDate = null;
		this.beginDate = null;
		this.visaExpiryEndDate = null;
		this.visaExpiryBeginDate = null;
		this.showContactsStillOnStatus = false;*/
		resetFilters();
	}

	public void resetFilters(){
		if(employeeSelected == null){
			employeeSelected = '';
		}
		if(destinationSelected == null){
			destinationSelected = '';
		}
		if(reasonSelected == null){
			reasonSelected = '';
		}
	}

	public void showHideExtraFilters(){
		this.showExtraFilters = !showExtraFilters;
		this.visaExpiryEndDate = '';
		this.visaExpiryBeginDate = '';
	}

	public void checkStatus(){
		showStatusFilter = false;
		if(this.stagesFilter.size() == 1){
			showStatusFilter = true;
			this.statusFilters = getStatus(this.stagesFilter.get(0).stage);
		}
	}

	public void updateStages(){
		this.destinationSelected = ApexPages.currentPage().getParameters().get('destination');
		this.stagesFilter = getStages();
		resetFilters();
		checkStatus();
	}

	/*public List<SelectOption> loadAgencies(String agencyID, String groupID, Boolean permissionViewAllAgencies){
		List<SelectOption> options = new List<SelectOption>();
		if(permissionViewAllAgencies){
			options.add(new SelectOption('', ' - All Agencies Available - '));
		}
		for(SelectOption option : IPFunctions.getAgencyOptions(agencyID, groupID, permissionViewAllAgencies)){
			options.add(new SelectOption(option.getValue(), option.getLabel()));
		}
		return options;
	}*/

	public List<SelectOption> getAgenciesUser(String parentID){
		List<SelectOption> agencyOptions = new List<SelectOption>();
		Set<String> ids = new Set<String>();
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && !user.Contact.Group_View_Permission__c){ //set the user to see only his own agency
			for(Account a: [Select Id, Name from Account where id =:user.Contact.AccountId order by name]){
				//agencyOptions.add(new SelectOption(a.Id, a.Name));
				ids.add(a.ID);
			}
			if(user.Aditional_Agency_Managment__c != null){
				for(Account ac:ipFunctions.listAgencyManagment(user.Aditional_Agency_Managment__c)){
					//agencyOptions.add(new SelectOption(ac.id, ac.name));
					ids.add(ac.ID);
				}
			}
		}else{
			for(Account ag : [Select Id, Name From Account Where Recordtype.Name = 'Agency' and ParentId = :parentID]){
				//agencyOptions.add(new SelectOption(ag.id, ag.Name));
				ids.add(ag.ID);
			}
		}
		//String nome;
		for(Account ag : [Select Id, Name From Account Where Recordtype.Name = 'Agency' AND Inactive__c = false AND ID IN :ids ORDER BY NAME]){
			//nome = ag.Name.contains('(HO)') ? 'Sydney' : ag.Name;
			agencyOptions.add(new SelectOption(ag.id, ag.Name));
		}

		if(agencyOptions.size() > 1){			
			agencyOptions.add(0, new SelectOption('All', 'All Agencies'));
		}
		return agencyOptions;
	}

	public List<SelectOption> loadAgencies(String agencyID, String groupID){
		Set<String> idsAllAgencies = new Set<String>();
		for(SelectOption option : IPFunctions.getAgencyOptions(agencyID, groupID, Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible())){
			idsAllAgencies.add(option.getValue());
		}
		
		List<SelectOption> options = new List<SelectOption>();
		for(Account option : [Select Id, Name from Account where id in :idsAllAgencies AND Inactive__c = false order by name]){
			options.add(new SelectOption(option.ID, option.Name));
		}

		if(options.size() > 1){
			options.add(0, new SelectOption('All', 'All Agencies'));
		}
		return options;
	}

	public List<SelectOption> loadEmployees(String agencyID){
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('','-All Employees-'));
		if(!String.isEmpty(agencyID)){
			for(SelectOption option : IPFunctions.getEmployeesByAgency(agencyID)){
				options.add(new SelectOption(option.getValue(), option.getLabel()));
			}
		}
		return options;
	}

	public List<SelectOption> getDestinations(){
		List<String> destinations = new List<String>();
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('','-- All Destinations --'));
		List<Client_Stage__c> clientStages = [SELECT Destination__c, Stage__c FROM Client_Stage__c where Agency_Group__c = :groupSelected];
		for(Client_Stage__c ct : clientStages){
			if(!String.isEmpty(ct.Destination__c) && !destinations.contains(ct.Destination__c)){
				destinations.add(ct.Destination__c);
				//options.add(new SelectOption(ct.Destination__c, ct.Destination__c));
			}
		}
		destinations.sort();
		for(String destination : destinations){
			options.add(new SelectOption(destination, destination));
		}
		//List<SelectOption> options = IPFunctions.getAllCountries();
		return options;
	}
	public List<SelectOption> getReasons(){
		List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Schema.Contact.Lead_Status_Reason__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple){
			options.add(new SelectOption( f.getValue(), f.getLabel() ));
		}
		try{
			List<Client_Stage__c> leadNotInterested = [SELECT ID, Stage_Sub_Options__c FROM Client_Stage__c WHERE Stage_description__c = 'LEAD - Not Interested' AND Agency_Group__r.ID = :groupSelected];
			for(Client_Stage__c cs : leadNotInterested){
				if(!String.isEmpty(cs.Stage_Sub_Options__c)){
					List<String> subOptions = (List<String>) JSON.deserialize(cs.Stage_Sub_Options__c, List<String>.class);
					for(String option : subOptions){
						options.add(new SelectOption( option, option ));
					}
				}
			}
		}catch(Exception ex){
			system.debug('LEAD NOT INTERESTED NOT FOUND FOR THE GROUP '+groupSelected);
		}
		List<SelectOptionWrapper> wrapper = new List<SelectOptionWrapper>();
		for(SelectOption option : options){
			wrapper.add(new SelectOptionWrapper(option));
		}
		wrapper.sort();
		options = new List<SelectOption>();
		options.add(new SelectOption('', ' - All Reasons - '));
		for(SelectOptionWrapper wrapped : wrapper){
			options.add(wrapped.option);
		}
		return options;
	}

	public class SelectOptionWrapper implements Comparable {
		public SelectOption option;

		public SelectOptionWrapper(SelectOption option){
			this.option = option;
		}

		public Integer compareTo(Object compareTo) {
			SelectOptionWrapper compareToOppy = (SelectOptionWrapper)compareTo;

			return this.option.getValue().compareTo(compareToOppy.option.getValue());
		}

	}

	/*public String getStatusString(){
		String statusString = '';
		if(showStatusFilter){
			for(StatusFilter status : statusFilters){
				if(status.selected){
					statusString = statusString + ',' + status.status;
				}
			}
			statusString = statusString.removeStart(';');
		}
		return statusString;
	}

	public String getStagesString(){
		String stageString = '';
		for(StageFilter stage : stagesFilter){
			if(stage.selected){
				stageString = stageString + ',' + stage.stage;
			}
		}
		stageString = stageString.removeStart(';');
		return stageString;
	}*/

	public List<StageFilter> getStages(){
		List<StageFilter> allStages = new List<StageFilter>();
		String query = null;
		if(String.isEmpty(destinationSelected)){
			query = 'SELECT Stage__c stg FROM Client_Stage__c WHERE Agency_Group__c = :groupSelected AND Destination__c = null GROUP BY Stage__c ORDER BY Stage__c';
		}else{
			query = 'SELECT Stage__c stg FROM Client_Stage__c WHERE Agency_Group__c = :groupSelected AND (Destination__c = null OR Destination__c = :destinationSelected) GROUP BY Stage__c ORDER BY Stage__c';
		}
		StageFilter filter = null;
		for(AggregateResult ar: Database.query(query)){
			filter = new StageFilter();
			filter.stage = (string)ar.get('stg');
			filter.selected = true;
			allStages.add(filter);
		}
		allStages.sort();
		return allStages;
	}

	public List<StatusFilter> getStatus(String stage){
		List<StatusFilter> status = new List<StatusFilter>();
		String query;
		if(stage == 'Stage 0'){
			query = 'SELECT ID, Stage_description__c FROM Client_Stage__c WHERE Stage__c = :stage AND Agency_Group__c = :groupSelected ORDER BY itemOrder__c ';
		}else{
			query = 'SELECT ID, Stage_description__c FROM Client_Stage__c WHERE Stage__c = :stage AND Agency_Group__c = :groupSelected AND Destination__c = :destinationSelected ORDER BY itemOrder__c ';
		}
		List<Client_Stage__c> allStatusStage = Database.query(query);
		system.debug('FOUND '+JSON.serialize(allStatusStage));
		for(Client_Stage__c statusStage : allStatusStage){
			if(statusStage.Stage_description__c != 'LEAD - Not Interested' && !statusStage.Stage_description__c.contains('xxx')){
				status.add(new StatusFilter(statusStage.id, statusStage.Stage_description__c));
			}
		}
		return status;
	}

	public List<SelectOption> getGroups(){
		Set<String> idsAllGroups = new Set<String>();
		for(SelectOption option : IPFunctions.getAgencyGroupsByPermission(user.Contact.AccountId, Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())){
			idsAllGroups.add(option.getValue());
			//
		}
		List<SelectOption> options = new List<SelectOption>();
		for(Account option : [Select Id, Name from Account where id in :idsAllGroups AND Inactive__c = false order by name]){
			options.add(new SelectOption(option.ID, option.Name));
		}
		
		return options;
	}

	public class StageFilter implements Comparable{
		public String stage{get;set;}
		public boolean selected{get;set;}

		public Integer compareTo(Object compareTo) {
			StageFilter lst = (StageFilter)compareTo;
			if(this.stage < lst.stage){
				return -1;
			}else if(this.stage > lst.stage){
				return 1;
			}
			return 0;
		}
	}
	
	public class StatusFilter implements Comparable{
		public String id{get;set;}
		public String status{get;set;}
		public boolean selected{get;set;}

		public StatusFilter(){}
		public StatusFilter(String id, String status){
			this.id = id;
			this.status = status;
			this.selected = true;
		}

		public Integer compareTo(Object compareTo) {
			StatusFilter lst = (StatusFilter)compareTo;
			if(this.id < lst.id){
				return -1;
			}else if(this.id > lst.id){
				return 1;
			}
			return 0;
		}
	}

	public class ContactReport{
		public String Id{get;set;}
		public String Name{get;set;}
		public String Email{get;set;}
		public String Phone{get;set;}
		public Date CreatedDate{get;set;}
		public String CreatedBy{get;set;}
		public Date LostCancelledDate{get;set;}
		public Date visaExpiryDate{get;set;}
		public String TimebeforeLost{get;set;}
		public String Stage{get;set;}				
		public String StatusBeforeCancelledLost{get;set;}							
		public String Destination{get;set;}				
		public String AssignedTo{get;set;}				
		public String CurrentAgency{get;set;}
		public String CreatedAgency{get;set;}
		public String agencyGroup{get;set;}
		public String Reason{get;set;}
		public String Comment{get;set;}
		public String active{get;set;}
		public String firstStatusCycle{get;set;}
		public String nationality{get;set;}

		public Boolean equals(Object obj) {
        if (obj instanceof ContactReport) {
            ContactReport ctt = (ContactReport)obj;
				if(ctt != null){
                	return this.id.equals(ctt.id);
				}
				return false;
            }
            return false;
        }
        public Integer hashCode() {
            return id.hashCode();
        }
	}

	public without sharing class MethodsWithoutSharing{
		public List<Contact> runQuery(String query, String groupSelected, Date fromDate, Date toDate, String employeeSelected, String destinationSelected, String reasonSelected, List<String> stages, String currentStage, List<String> statusStageSelected, Set<String> idsAgencies, String agencySelected,  Date fromDateVisaExpiryDate, Date toDateVisaExpiryDate){
			return Database.query(query);
		}
	}

	public class ReportResult{
		public String status{get;set;}
		public Integer lost {get{if(lost == null) lost = 0; return lost;} set;}
		public Integer won {get{if(won == null) won = 0; return won;} set;} 
		public String colorWon{get;set;}
		public String colorLost{get;set;}
		//public String colorWon {get{if(colorWon == null) colorWon = '#92d465'; return colorWon;} set;}
		//public String colorLost {get{if(colorLost == null) colorLost = '#da3c3d'; return colorLost;} set;}
		
		public Boolean equals(Object obj) {
        	if (obj instanceof ReportResult) {
            ReportResult s = (ReportResult)obj;
				if(status != null){
                	return status.equals(s.status);
				}
				return false;
            }
            return false;
        }

        public Integer hashCode() {
            return status.hashCode();
        }
	}

}