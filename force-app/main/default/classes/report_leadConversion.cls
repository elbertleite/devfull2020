public with sharing class report_leadConversion {
	
	private Map<String, String> subjectIds {get{if(subjectIds == null) subjectIds = new Map<String, String>(); return subjectIds;}set;}
	private Map<String, String> subjectNames = new Map<String, String>();
	
	public User user{get;set;}

	public report_leadConversion(){
		
		user = [Select Id, Name, Contact.Export_Report_Permission__c, Contact.AccountId, Contact.Name, Contact.Account.Id, Contact.Account.Name, Contact.Account.Parent.RDStation_Campaigns__c, Contact.Account.Parent.Name, Contact.Account.Parent.Id, Aditional_Agency_Managment__c, Contact.Group_View_Permission__c, Contact.Agency_View_Permission__c, Contact.Account.Global_Link__c from User where Id = :UserInfo.getUserId()];
		
		selectedAgencyGroup = myDetails.Account.Parentid;		
		selectedAgency = mydetails.Accountid;
		
        selectedSearchOption = 'conversion_rate';
        getAgencyGroupOptions();
        getAgencyOptions();
        
		search();

		
	}
	
	 public PageReference checkUserAccess(){

     	if(!myDetails.Management_Report_Permission__c){
      		PageReference retURL = new PageReference('/apex/agency_reports');
      		retURL.setRedirect(true);
      		return retURL;
      	} else return null;      	
      	
 	}
    
    public boolean searchByCreatedDate {
        get{
            if(searchByCreatedDate == null)
                searchByCreatedDate = true;
           	return searchByCreatedDate;
        }
        set;
    }
	
	public Contact myDetails {
		get{
			if(myDetails == null)
				myDetails = UserDetails.getMyContactDetails();
			return myDetails;
		}
		set;
	}
	
	public String selectedAgencyGroup {get;set;}
	private List<SelectOption> agencyGroupOptions;
	public List<SelectOption> getAgencyGroupOptions() {
		/*if(agencyGroupOptions == null){
			agencyGroupOptions = new List<SelectOption>();
			if(myDetails.Account.Parent.Destination_Group__c || myDetails.Global_Manager__c){
				agencyGroupOptions.add(new SelectOption('all', '-- ALL --'));
				for(Account ag : IPFunctionsGLobal.getAgencyGroups(myDetails.Account.Parent.Global_Link__c)){
					agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
					subjectIds.put(ag.Name, ag.id);
					subjectNames.put(ag.id, ag.Name);
				}
			} else {
				for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
					agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
					subjectIds.put(ag.Name, ag.id);
					subjectNames.put(ag.id, ag.Name);
				}
			}
		}
		
		return agencyGroupOptions;*/
		if(agencyGroupOptions == null){
			agencyGroupOptions = IPFunctions.getAgencyGroupsByPermission(user.Contact.AccountId, Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible());
		}
		//options.add(new SelectOption('0019000001MjqDX','IP Brazil'));
		//options.remove(0);
		return agencyGroupOptions;
	}
	
	public void refreshAgencies(){
		selectedAgency = null;		
		agencyOptions = null;		
		getAgencyOptions();		
		//leadSourceOptions = null;
		//getLeadSourceOptions();
	}
	
	public String selectedAgency {get;set;}
	private List<SelectOption> agencyOptions;
	public List<SelectOption> getAgencyOptions(){
		if(agencyOptions == null){
			agencyOptions = new List<SelectOption>();
			agencyOptions.add(new SelectOption('all', '-- ALL --'));
			/*if(selectedAgencyGroup != null && selectedAgencyGroup != 'all'){
				for(Account agency : [select id, name from Account where RecordType.Name = 'Agency' and parentid = :selectedAgencyGroup order by Name]){				
					agencyOptions.add(new SelectOption(agency.id, agency.Name));
					subjectNames.put(agency.id, agency.Name);
				}
			}*/
			for(SelectOption option : IPFunctions.getAgencyOptions(selectedAgency, selectedAgencyGroup, Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible())){
				agencyOptions.add(new SelectOption(option.getValue(), option.getLabel()));
			}
			
		}
		return agencyOptions;
	}
	
	public Contact range {
		get {
			if(range == null){
				range = new Contact();
				range.Arrival_Date__c = system.today().addMonths(-5); 
				range.Expected_Travel_Date__c = system.today();
			}
			return range;
		}
		set;
	}
    
    public String selectedSearchOption {get;set;}
	private List<SelectOption> searchOptions;
	public List<SelectOption> getSearchOptions(){
		if(searchOptions == null){
			searchOptions = new List<SelectOption>();
			searchOptions.add(new SelectOption('conversion_rate', 'Leads created between'));
			searchOptions.add(new SelectOption('conversion_date', 'Leads converted between'));
		}
		return searchOptions;
	}
	
	public class Wrapper{
		public String name {get;set;}
		public Integer totalLeads {get { if(totalLeads == null) totalLeads = 0; return totalLeads;} set;}
		public Integer convertedLeads {get { if(convertedLeads == null) convertedLeads = 0; return convertedLeads; } set;}
		public double avgConversionInDays {get { if(avgConversionInDays == null) avgConversionInDays = 0; return avgConversionInDays; } set;}
		public String conversionTime {get;set;}
		public Wrapper(String name, Integer totalLeads, Integer convertedLeads, double avgConversionInDays){
			this.name = name;
			this.totalLeads = totalLeads;
			this.convertedLeads = convertedLeads;
			this.avgConversionInDays = avgConversionInDays;			
		}
	}
	
	
	private void populateSubjectIDs(){
		
		String sql = 'select id, name from Contact where RecordType.Name = \'Employee\' ';
		
		if(selectedAgencyGroup == 'all')
			for(Account agency : [select id, name from Account where RecordType.Name = 'Agency' order by Name])
				subjectIds.put(agency.name, agency.id);
		else {
			for(Account agency : [select id, name from Account where RecordType.Name = 'Agency' and parentid = :selectedAgencyGroup order by Name])
				subjectIds.put(agency.name, agency.id);
			
			sql += ' and Account.ParentID = \'' + selectedAgencyGroup + '\'';
		}
		system.debug('@selectedAgency: ' + selectedAgency);
		if(selectedAgency != 'all')
			sql += ' and AccountID = \'' + selectedAgency + '\'';
		
		
		system.debug('@sql: ' + sql);		
		for(Contact emp : Database.query(sql))
			subjectIds.put(emp.Name, emp.id);	
		
		system.debug('@# subjectIds: ' + subjectIds);
	}
	
	//        <Group,  Map<Agency, Map<User  , Wrapper>>>
	public Map<String, Map<String, Map<String, Wrapper>>> conversionMap {get; set;}		
	public Map<String, double> agencyGroupConversion {get;set;} 
	public void search(){
		
		//initialize all the things!
		agencyGroupConversion = new Map<String, double>();
		conversionMap = new Map<String, Map<String, Map<String, Wrapper>>>();
		
		//populate Map of Ids to be used in the chart
		populateSubjectIDs();
		
		
		//set type of search
        searchByCreatedDate = selectedSearchOption == 'conversion_rate';
        
		
		
		/*Start them queries!*/
		String queryWhere = getQueryFilters(true);		
		
		//POPULATE TOTAL LEADS
		String query = 'select Lead_Assignment_To__r.Account.Parent.Name agencyGroup, Lead_Assignment_To__r.Account.Name agency, Lead_Assignment_To__r.Name user, COUNT(Id) total FROM CONTACT' ;		
		String queryGroup = 'group by Lead_Assignment_To__r.Account.Parent.Name, Lead_Assignment_To__r.Account.Name, Lead_Assignment_To__r.Name ';	
		String queryOrder = 'order by Lead_Assignment_To__r.Account.Parent.Name, Lead_Assignment_To__r.Account.Name, Lead_Assignment_To__r.Name ';
		
		queryWhere += ' and Lead_Assignment_To__c != null ';
		
		String theQuery = query + queryWhere + queryGroup + queryOrder;
		system.debug('theQuery 0: ' + theQuery);
		
		for(AggregateResult ar : Database.query(theQuery)){
			String agencyGroup = (String) ar.get('agencyGroup');
			String agency = (String) ar.get('agency');
			String user = (String) ar.get('user');
			Integer total = Integer.valueOf(ar.get('total'));
			
			if(conversionMap.containsKey(agencyGroup)){
				
				if(conversionMap.get(agencyGroup).containsKey(agency)){					
					conversionMap.get(agencyGroup).get(agency).put(user, new Wrapper(user, total, 0, 0));
										
				} else{
					conversionMap.get(agencyGroup).put(agency, new Map<String, Wrapper>{ user => new Wrapper(user, total, 0, 0) });					
				}
			} else {				
				conversionMap.put(agencyGroup, new Map<String, Map<String, Wrapper>>{ agency => new Map<String, Wrapper>{ user => new Wrapper(user, total, 0, 0) } });
			}
			
			
		}
		//POPULATE TOTAL LEADS
		
		
		//POPULATE CONVERTED LEADS
		if(searchByCreatedDate){
			queryWhere += ' and Lead_Converted_On__c != null ';
			theQuery = query + queryWhere + queryGroup + queryOrder;
			system.debug('theQuery 1: ' + theQuery);
			
			for(AggregateResult ar : Database.query(theQuery)){
				String agencyGroup = (String) ar.get('agencyGroup');
				String agency = (String) ar.get('agency');
				String user = (String) ar.get('user');
				Integer converted = Integer.valueOf(ar.get('total'));
				
				if(conversionMap.containsKey(agencyGroup) && conversionMap.get(agencyGroup).containsKey(agency) && conversionMap.get(agencyGroup).get(agency).containsKey(user))
					conversionMap.get(agencyGroup).get(agency).get(user).convertedLeads = converted;
				else 
					system.debug('CONVERTED NOT FOUND: agencyGroup: ' + agencyGroup + ' agency: ' + agency + ' user: ' + user + ' converted: ' + converted);	
				
			}
		}
		//POPULATE CONVERTED LEADS		
		
		
		
		//POPULATE USER AVG CONVERSION TIME
		queryWhere = getQueryFilters(true);
		query = 'select Lead_Assignment_To__r.Account.Parent.Name agencyGroup, Lead_Assignment_To__r.Account.Name agency, Lead_Assignment_To__r.Name user, AVG(Conversion_Time_in_Days__c) conversion from Contact ';
		queryGroup = ' group by Lead_Assignment_To__r.Account.Parent.Name, Lead_Assignment_To__r.Account.Name, Lead_Assignment_To__r.Name ';
		queryOrder = ' order by Lead_Assignment_To__r.Account.Parent.Name, Lead_Assignment_To__r.Account.Name, Lead_Assignment_To__r.Name ';
		theQuery = query + queryWhere + queryGroup + queryOrder;		
		system.debug('theQuery 2: ' + theQuery);
		for(AggregateResult ar : Database.query(theQuery)){
			String agencyGroup = (String) ar.get('agencyGroup');
			String agency = (String) ar.get('agency');
			String user = (String) ar.get('user');
			double conversion = double.valueOf(ar.get('conversion'));
			double conversionDays = conversion != null ? conversion : 0;
			
			conversion = conversion != null ? conversion / 30.436875 : 0; //to months
						
			if(conversionMap.containsKey(agencyGroup) && conversionMap.get(agencyGroup).containsKey(agency) && conversionMap.get(agencyGroup).get(agency).containsKey(user)){
				conversionMap.get(agencyGroup).get(agency).get(user).avgConversionInDays = conversionDays;
				if(conversion > 0)
					conversionMap.get(agencyGroup).get(agency).get(user).conversionTime = getLengthInMonths(conversion);
			} else 
				system.debug('AVERAGE NOT FOUND: agencyGroup: ' + agencyGroup + ' agency: ' + agency + ' user: ' + user + ' conversion: ' + conversion);	
			
		
		}
		//POPULATE USER AVG CONVERSION TIME
		
		
		//POPULATE AGENCY AVG CONVERSION TIME
		queryWhere = getQueryFilters(true);
		query = 'select Lead_Assignment_To__r.Account.Parent.Name agencyGroup, Lead_Assignment_To__r.Account.Name agency, AVG(Conversion_Time_in_Days__c) conversion from Contact ';
		queryGroup = ' group by Lead_Assignment_To__r.Account.Parent.Name, Lead_Assignment_To__r.Account.Name';
		queryOrder = ' order by Lead_Assignment_To__r.Account.Parent.Name, Lead_Assignment_To__r.Account.Name';
		theQuery = query + queryWhere + queryGroup + queryOrder;		
		system.debug('theQuery 3: ' + theQuery);
		for(AggregateResult ar : Database.query(theQuery)){
			String agencyGroup = (String) ar.get('agencyGroup');
			String agency = (String) ar.get('agency');
			double conversion = double.valueOf(ar.get('conversion'));
			double conversionDays = conversion != null ? conversion : 0;
			agencyGroupConversion.put(agency, conversionDays);
			
			if(selectedAgency != 'all'){
				agencyGroupConversion.put(agencyGroup, conversionDays);
				agencyGroupConversion.put('overall', conversionDays);
			}
			
		}
		//POPULATE AGENCY AVG CONVERSION TIME
		
		
		//POPULATE AGENCY GROUP AVG CONVERSION TIME
		if(selectedAgency == 'all'){
			queryWhere = getQueryFilters(true);
			query = 'select Lead_Assignment_To__r.Account.Parent.Name agencyGroup, AVG(Conversion_Time_in_Days__c) conversion from Contact ';
			queryGroup = ' group by Lead_Assignment_To__r.Account.Parent.Name';
			queryOrder = ' order by Lead_Assignment_To__r.Account.Parent.Name';
			theQuery = query + queryWhere + queryGroup + queryOrder;		
			system.debug('theQuery 4: ' + theQuery);
			for(AggregateResult ar : Database.query(theQuery)){
				String agencyGroup = (String) ar.get('agencyGroup');
				double conversion = double.valueOf(ar.get('conversion'));
				double conversionDays = conversion != null ? conversion : 0;			
				agencyGroupConversion.put(agencyGroup, conversionDays);
				
				if(selectedAgencyGroup != 'all')
					agencyGroupConversion.put('overall', conversionDays);
			}
		}
		//POPULATE AGENCY GROUP AVG CONVERSION TIME
		
		//POPULATE OVERALL AVG CONVERSION TIME
		if(selectedAgencyGroup == 'all'){
			queryWhere = getQueryFilters(false);
			query = 'select AVG(Conversion_Time_in_Days__c) conversion from Contact ';
			theQuery = query + queryWhere;		
			system.debug('theQuery 5: ' + theQuery);
			for(AggregateResult ar : Database.query(theQuery)){				
				double conversion = double.valueOf(ar.get('conversion'));
				double conversionDays = conversion != null ? conversion : 0;			
				agencyGroupConversion.put('overall', conversionDays);
				
			}
		}
		//POPULATE AGENCY GROUP AVG CONVERSION TIME
		
		/* queries end*/
		
		
		
		
		//get that chart data
		if(selectedAgency != 'all'){
			loadChartData('agency', selectedAgency);
			chartTitle = subjectNames.get(selectedAgency);
		} else if(selectedAgencyGroup != 'all'){
		 	loadChartData('agencyGroup', selectedAgencyGroup);
		 	chartTitle = subjectNames.get(selectedAgencyGroup);
		} else {
			loadChartData('all', null);
			chartTitle = 'All Groups';
		}
		
		
		
	}
	
	private String getLengthInMonths(double conversionInMonths){
		String theLength = '';		
		integer month = conversionInMonths.intValue();
		integer week = (((conversionInMonths - conversionInMonths.intValue())/25)*100).intValue();
		string s = '';
		if(month > 0){
			s = month > 1 ? 's' : '';		
			theLength =  month + ' month' + s;
		}
		if(week > 0){
			if(month > 0)
				theLength += ' and ';
			s = week > 1 ? 's' : '';
			theLength +=  + week + ' week' + s;
		}
		
		return theLength;
	}
	
	private String getQueryFilters(boolean applyAssignmentFilter){
		
		String queryWhere = ' where RecordType.Name in (\'Lead\', \'Client\') ';
		
		if(searchByCreatedDate){
            if(range.Arrival_Date__c != null)
                queryWhere += ' and DAY_ONLY(CreatedDate) >= ' + IPFunctions.FormatSqlDateIni(range.Arrival_Date__c);
            if(range.Expected_Travel_Date__c != null)		
                queryWhere += ' and DAY_ONLY(CreatedDate) <= ' + IPFunctions.FormatSqlDateIni(range.Expected_Travel_Date__c);
        } else {
            if(range.Arrival_Date__c != null)
                queryWhere += ' and DAY_ONLY(Lead_Converted_on__c) >= ' + IPFunctions.FormatSqlDateIni(range.Arrival_Date__c);                
            if(range.Expected_Travel_Date__c != null)		
                queryWhere += ' and DAY_ONLY(Lead_Converted_on__c) <= ' + IPFunctions.FormatSqlDateIni(range.Expected_Travel_Date__c);      		      
        }
			
		if(applyAssignmentFilter){
			if(selectedAgency != 'all')
				queryWhere += ' and Lead_Assignment_To__r.Accountid = :selectedAgency ';
			else if(selectedAgencyGroup != 'all')
				queryWhere += ' and Lead_Assignment_To__r.Account.Parentid = :selectedAgencyGroup ';
		}
		
		return queryWhere;
	}
	public PageReference generateExcel(){
		PageReference pr = Page.report_leadConversion_excel;
		pr.setRedirect(false);
		return pr;		
	}
	
	
	
	public transient List<Contact> exportList {get;set;}
	public void excelSearch(){
		
		String sql = 'Select RDStation_Last_Conversion__c, Current_Agency__r.Name, Lead_Assignment_To__r.Account.Name, Original_Agency__r.Name, Name, RecordType.Name, CreatedDate, CreatedBy.UserType, CreatedBy.Name, Lead_Stage__c, Status__c, Nationality__c, Email, Client_Classification__c, ';
		sql += ' LeadSource, Lead_Source_Specific__c, Lead_Reference_Type__c, Lead_Reference_Name__c, Lead_First_Contact__c, Lead_First_Contact_Date__c, Lead_Product_Type__c, Lead_Study_Type__c, Lead_Other_Product__c, Lead_Assignment_To__r.Name, ';
		sql += ' Lead_Destinations__c, Lead_Other_Destination__c, Lead_Converted_by__r.Name, Lead_Converted_on__c, Conversion_Time_in_Days__c, Destination_Country__c, Destination_City__c, Expected_Travel_Date__c, Arrival_Date__c, Visa_Expiry_Date__c, ';
		sql += ' ( select detail__c from forms_of_contact__r where type__c = \'Mobile\' ) ' ;
		sql += ' from Contact ';
		
		String queryWhere = getQueryFilters(true) + ' and Lead_Assignment_To__c != null ';
		
		String sqlOrder = ' order by Original_Agency__r.Name, Current_Agency__r.Name, Lead_Assignment_To__r.Account.Name, Name';
		
		//exportList = Database.query(sql + queryWhere + sqlOrder);

		exportList = new MethodsWithoutSharing().queryExcel(sql + queryWhere + sqlOrder, selectedAgency, selectedAgencyGroup);
		
	}
	
	public without sharing class MethodsWithoutSharing{
		public List<Contact> queryExcel(String query, String selectedAgency, String selectedAgencyGroup){
			return Database.query(query);
		}
	}
	
	
	public void refreshChart(){
		
		String sType = ApexPages.currentPage().getParameters().get('subjectType');
		String sID = ApexPages.currentPage().getParameters().get('subjectId');
		chartTitle = sID;
		loadChartData(sType, subjectIds.get(sID));	
	}
	
	
			 		 // <period, Wrapper>
	public transient Map<Date, Wrapper> chartData {get;set;}
	public String chartTitle {Get;set;}
	
	private void loadChartData(String stype, string sid){
		
		String groupedField = '';
		if(searchByCreatedDate)
			groupedField = 'CreatedDate';
		else
			groupedField = 'Lead_Converted_On__c';
		
		String subjectFilter = '';
		if(sType == 'user'){			
			subjectFilter = ' and Lead_Assignment_To__c = \'' + sid + '\' ';
		} else if(sType == 'agency'){			
			subjectFilter = ' and Lead_Assignment_To__r.AccountId = \'' + sid + '\' ';
		} else if(stype == 'agencyGroup'){
			subjectFilter = ' and Lead_Assignment_To__r.Account.ParentId = \'' + sid + '\' ';
		}
		
		String queryWhere = getQueryFilters(false);
		
		
		
		String query = 'select CALENDAR_MONTH('+groupedField+') month, CALENDAR_YEAR('+groupedField+') year, COUNT(Id) total FROM CONTACT ';
		String queryGroup = ' group by CALENDAR_MONTH('+groupedField+'), CALENDAR_YEAR('+groupedField+') ';	
		String queryOrder = ' order by CALENDAR_MONTH('+groupedField+'), CALENDAR_YEAR('+groupedField+') ';
		
		queryWhere += subjectFilter;
		
		String theQuery = query + queryWhere + queryGroup + queryOrder;
		system.debug('theQueryChart 1: ' + theQuery);
		
		chartData = new Map<Date, Wrapper>();
		
		for(AggregateResult ar : Database.query(theQuery)){
			
			Date myDate = Date.newInstance( Integer.valueOf(ar.get('year')), Integer.valueOf(ar.get('month')), 1);
			String monKey = getMonthName(Integer.valueOf(ar.get('month'))) + '/' + Integer.valueOf(ar.get('year'));			
			Integer total = Integer.valueOf(ar.get('total'));
			
			chartData.put( myDate, new Wrapper(monKey, total, 0, 0) );
			
		}
		
		
		if(searchByCreatedDate){
			//CONVERTED LEADS
			queryWhere += ' and Lead_Converted_On__c != null ';
			theQuery = query + queryWhere + queryGroup + queryOrder;
			system.debug('theQueryChart 2: ' + theQuery);
			
			for(AggregateResult ar : Database.query(theQuery)){
				Date myDate = Date.newInstance(Integer.valueOf(ar.get('year')), Integer.valueOf(ar.get('month')), 1);
				Integer converted = Integer.valueOf(ar.get('total'));
				
				chartData.get( myDate ).convertedLeads = converted;			
			}
			//CONVERTED LEADS
		}
		
		
		//POPULATE AVG CONVERSION TIME
		queryWhere = getQueryFilters(false) + subjectFilter;
		query = 'select CALENDAR_MONTH('+groupedField+') month, CALENDAR_YEAR('+groupedField+') year, AVG(Conversion_Time_in_Days__c) conversion from Contact ';
		queryGroup = ' group by CALENDAR_MONTH('+groupedField+'), CALENDAR_YEAR('+groupedField+') ';
		queryOrder = ' order by CALENDAR_MONTH('+groupedField+'), CALENDAR_YEAR('+groupedField+') ';
		theQuery = query + queryWhere + queryGroup + queryOrder;		
		system.debug('theQueryChart 3: ' + theQuery);
		for(AggregateResult ar : Database.query(theQuery)){
			Date myDate = Date.newInstance(Integer.valueOf(ar.get('year')), Integer.valueOf(ar.get('month')), 1);			
			double conversion = double.valueOf(ar.get('conversion'));
			Decimal conversionDays = Decimal.valueOf( conversion != null ? conversion : 0 );
			
			if(chartData.get( myDate ).convertedLeads > 0 && conversionDays == 0) conversionDays = 1;
			
			chartData.get( myDate ).avgConversionInDays = conversionDays.setScale(2);			
		}
		//POPULATE AVG CONVERSION TIME
		
	}
	
    
    
    public String getMonthName(Integer month){
		
		if(month != null){
			if(month == 1) return 'JAN';			
			else if(month == 2) return 'FEB';
			else if(month == 3) return 'MAR';
			else if(month == 4) return 'APR';
			else if(month == 5) return 'MAY';
			else if(month == 6) return 'JUN';
			else if(month == 7) return 'JUL';
			else if(month == 8) return 'AUG';
			else if(month == 9) return 'SEP';
			else if(month == 10) return 'OCT';
			else if(month == 11) return 'NOV';
			else if(month == 12) return 'DEC';
			else return '';
		} else return '';			
			
	}
}