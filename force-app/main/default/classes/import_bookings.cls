public without sharing class import_bookings {

    public String CLIENT_NAME { get{ return 'Estudante'; } private set; }
    public String CLIENT_EMAIL { get{ return 'E-mail'; } private set; }
    public String CURSO_NAME { get{ return 'Tipo Curso'; } private set; }
    public String CAMPUS_NAME { get{ return 'Escola'; } private set; }
    public String CURSO_TUITION { get{ return 'Tuition Moeda'; } private set; }
    public String TYPE_LOO_CONFIRM_SIGNED { get{ return 'confirmLOO'; } private set; }


    public Blob csvFileBody{get;set;}
    public string csvAsString{get;set;}
    public String[] csvFileLines{get;set;}

    public list<string> listHeader{get; set;}
    public list<fileDetails> listBody{get; set;}
    private list<string> listEmail{get; set;}
    public map<string, map<string, list<Quotation_Detail__c>>> mapCourses{get; set;}
    public map<string, list<client_course__c>> mapBookings{get; set;}
    

    public class fileDetails{
        public string email{get; set;}
        public string campus{get; set;}
        public string course{get; set;}
        public decimal tuition{get; set;}
        public string clientId{get; set;}
        public map<string,string> listItems{get; set;}
        public fileDetails(string email, string campus, string course, decimal tuition, string clientId, list<string> listHeader, list<string> listItems){
            this.email = email;
            this.campus = campus;
            this.course = course;
            this.tuition = tuition;
            this.clientId = clientId;
            this.listItems = new map<string,string>();
            for(integer i =0; i < listHeader.size(); i++)
                this.listItems.put(listHeader[i], listItems[i]);
        }
    }

    public void importCSVFile(){
        mapCourses = new map<string, map<string, list<Quotation_Detail__c>>>();
        mapBookings = new map<string, list<client_course__c>>(); 
        listEmail = new list<string>();
        listHeader = new list<string>();
        listBody =  new list<fileDetails>();
        csvAsString = csvFileBody.toString();
        csvFileLines = csvAsString.split('\n');
        listHeader = new list<string>(csvFileLines[0].split(','));
        string email;
        string campus;
        string course;
        decimal tuition;
        string clientId;
        string columnName;

        list<string> listClientEmails =  new list<string>();
        for(Integer i=1;i<csvFileLines.size();i++)
            listClientEmails.add(csvFileLines[i].split(',')[listHeader.indexOf(CLIENT_EMAIL)]);
        map<string, string> mapClientIds = new map<string, string>();
        for(contact ct:[Select id, email from contact where email in:listClientEmails])
            mapClientIds.put(ct.email, ct.id);

        for(Integer i=1;i<csvFileLines.size();i++){
            email = csvFileLines[i].split(',')[listHeader.indexOf(CLIENT_EMAIL)]; //email
            campus = csvFileLines[i].split(',')[listHeader.indexOf(CAMPUS_NAME)]; //email
            course = csvFileLines[i].split(',')[listHeader.indexOf(CURSO_NAME)]; //course
            tuition = decimal.valueOf(csvFileLines[i].split(',')[listHeader.indexOf(CURSO_TUITION)]); //course
            if(mapClientIds.containsKey(email.toLowerCase()))
                clientId = mapClientIds.get(email.toLowerCase());
            else clientId = null;
            listBody.add(new fileDetails(email.toLowerCase(), campus, course, tuition, clientId, listHeader, new list<string>(csvFileLines[i].split(','))));
            listEmail.add(email);
            if(!mapCourses.containsKey(email.toLowerCase())){
                mapCourses.put(email.toLowerCase(), new map<string, list<Quotation_Detail__c>>{campus => new list<Quotation_Detail__c>()});
                mapBookings.put(email.toLowerCase(), new list<client_course__c>());
            }else mapCourses.get(email.toLowerCase()).put(campus, new list<Quotation_Detail__c>());
        }

        for(Quotation_Detail__c qd:[SELECT Quotation__r.client__c, Quotation__c, Quotation__r.Agency__r.name, Quotation__r.client__r.name, Quotation__r.client__r.email, Course_Tuition_Fee__c, createdDate, Course_Total_Extra_Fees__c, Course_Name__c, School_Name__c, Campus_Name__c, Campus_City__c, Campus_Country__c, Course_Start_Date__c, Course_Finish_Date__c, Course_Duration__c, Course_Number_Free_Weeks__c, Course_Number_Extra_Weeks__c, Course_Booking__c FROM Quotation_Detail__c where Quotation__r.client__r.email in :listEmail order by Quotation__r.client__r.email, Campus_Name__c, Course_Name__c, createdDate desc, Course_Tuition_Fee__c]){
            if(mapCourses.containsKey(qd.Quotation__r.client__r.email.toLowerCase())){
                if(mapCourses.get(qd.Quotation__r.client__r.email.toLowerCase()).containsKey(qd.Campus_Name__c))
                    mapCourses.get(qd.Quotation__r.client__r.email.toLowerCase()).get(qd.Campus_Name__c).add(qd);
                else mapCourses.get(qd.Quotation__r.client__r.email.toLowerCase()).put(qd.Campus_Name__c, new list<Quotation_Detail__c>{qd});
            }
            else mapCourses.put(qd.Quotation__r.client__r.email.toLowerCase(), new map<string, list<Quotation_Detail__c>>{qd.Campus_Name__c => new list<Quotation_Detail__c>{qd}});
        }

        for(client_course__c cc:[Select id, Client__c, Course_Name__c, createdBy.name, Campus_Name__c, Client__r.email, 
                            Course_Length__c, Unit_Type__c, Total_Tuition__c, Total_Paid__c, isCancelled__c, Total_Extra_Fees__c, Free_Weeks__c, Number_Instalments__c
                            from client_course__c where Client__r.email in :listEmail and isCourse__c = true] )
            if(!mapBookings.containsKey(cc.Client__r.email.toLowerCase()))
                mapBookings.put(cc.Client__r.email.toLowerCase(), new list<client_course__c>{cc});
            else mapBookings.get(cc.Client__r.email.toLowerCase()).add(cc);
    }


    

    private list<client_course_fees__c> listFees;
	private list<client_course_fees__c> listPromotions;
	private list<client_course_instalment__c> listInstalments;

    public class quoteCourse{
 		public Quotation_Detail__c quoteDetail{get; set;}
 		public client_course__c clientCourse{get; set;} //FINANCE
 		public list<client_product_service__c> listProducts{get; set;}
 	}

 	public class listQuotes{
 		public Quotation__c quote{get; set;}
 		public list<quoteCourse> listCourses{get; set;}
 		public list<client_product_service__c> listProducts{get; set;}
 	}
    
    
    public pageReference openQuoteEnrol(){
        string quoteId = ApexPages.currentPage().getParameters().get('quoteId');
        string courseId = ApexPages.currentPage().getParameters().get('courseId');

        string clientId = ApexPages.currentPage().getParameters().get('clientId');
        String sql = 'Select Client__c, Client__r.Name, CreatedDate, Expiry_date__c, Status__c, Name, CreatedBy.Name, LastModifiedBy.Name, LastModifiedDate, Status_Changed_On__c, Client__r.Nationality__c, AllowRecreate__c, Agency_Products__c, Agency_Products_JSON__c, ' +
                        ' (Select Campus_Course__c, Campus_Course__r.Course__r.Package__c, Campus_Course__r.Package_Courses__c, Quotation__c, id, Agency_Products__c, Agency_Products_JSON__c, Campus_Currency__c, Campus_Name__c, Course_Duration__c, Course_Instalments__c, Course_Name__c, Course_Number_Free_Weeks__c, Course_Sold_in_Blocks__c, Course_Total_Extra_Fees__c, ' +
                        ' Course_Total_Extra_Fees_Promotion__c, Course_Total_Value__c, Course_Total_Value_Without_Discount__c, Course_Tuition_Fee__c, Course_Tuition_per_Unit__c, Course_Unit_Type__c, Extra_Fees_Details__c, Course_Finish_Date__c, Course_Start_Date__c, ' +
                        ' Promotion_Details__c,  Quotation__r.Client__c, Related_Fees_Details__c, School_Name__c,Campus_City__c, Campus_Country__c, Campus__c, LastModifiedBy.Name, LastModifiedDate, isSelected__c FROM Quotation_Courses__r where id = \''+courseId+'\') ' +
                        ' FROM Quotation__c where id = \''+quoteId+ '\'' ;
        
        system.debug('SQL===>' + sql);

        list<listQuotes> quotations = new list<listQuotes>();
        listQuotes quote;
        quoteCourse course;

        //FINANCE
        map<string,client_course__c> mapCC = new map<string,client_course__c>();
        for(client_course__c cc:[Select id, Client__c, Quotation__c, Quotation_Detail__c, Enrolment_Date__c from client_course__c WHERE Client__c = :clientId])
            mapCC.put(cc.Quotation_Detail__c, cc);
        //FINANCE

        for(Quotation__c qt:Database.query(sql)){
            quote = new listQuotes();
            quote.quote = qt;

            quote.listCourses = new list<quoteCourse>();
            for(Quotation_Detail__c qd:qt.Quotation_Courses__r){
                course = new quoteCourse();
                course.quoteDetail = qd;

                //FINANCE
                if(mapCC.containsKey(qd.id)){
                    course.clientCourse = mapCC.get(qd.id);
                }

                //FINANCE

                quote.listCourses.add(course);

            }
            quotations.add(quote);
        }


        return createEnrollment(clientId, quotations);
       
    }

    public pageReference createEnrollment(string clientId, list<listQuotes> quotations){
   		Savepoint sp = Database.setSavepoint();
		try{
	 		list<client_product_service__c> productsEnroll = new list<client_product_service__c>();

	 		//Create or Load Booking
	 		Client_Course__c booking;
            booking = new Client_Course__c(client__c = clientId, isBooking__c = true);
            insert booking;

	 		listFees = new list<client_course_fees__c>();
	 		listPromotions = new list<client_course_fees__c>();
	 		listInstalments = new list<client_course_instalment__c>();

			map<String, client_product_service__c> mProds = new map<String, client_product_service__c>();
			list<client_product_service__c> pFees = new list<client_product_service__c>();

		
            contact_controller ct = new contact_controller();
            ct.isMigrated = true;
		    for(listQuotes lq:quotations){
		    	System.debug('==>lq: '+lq);
		    	for(quoteCourse qd:lq.listCourses){
		    		System.debug('==>qd: '+qd);
                    ct.createClientCourse(qd.quoteDetail, booking.id);
		    	}
		    	
		    }

		    //============Add Fees=================
		    //if(listFees.size() > 0)
			//	insert listFees;

			//============Add Promotions=================
			if(listPromotions.size() > 0)
				insert listPromotions;

			//============Add Instalments=================
			if(listInstalments.size() > 0)
				insert listInstalments;

			

			
            PageReference acctPage = new PageReference('/apex/contact_new_bookings');
            acctPage.getParameters().put('id', clientId);
            acctPage.getParameters().put('ccid', ct.courseAdded);
            acctPage.getParameters().put('page', 'contactNewPage');
        	acctPage.setRedirect(true);
        	return acctPage;

	    }catch(Exception e){
			ApexPages.addMessages(e);
			Database.rollback(sp);
		}
   		return null;
 	}
  
}

// http://www.forcetree.com/2010/08/read-and-insert-records-from-csv-file.html