/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class agency_staff_test {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        TestFactory tf = new TestFactory();       	
		
		Account agency = tf.createAgency();
		Contact ct = tf.createEmployee(agency);
		
		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);
        
        Apexpages.currentPage().getParameters().put('contactID', ct.id);
        
        agency_staff agencyStaff = new agency_staff(new Apexpages.Standardcontroller(agency));
        
        agencyStaff.setEdit();
        agencyStaff.cancel();
        agencyStaff.save();
        
        List<SelectOption> nationalities = agencyStaff.nationalities;
        List<SelectOption> countries = agencyStaff.countries;
        
        agencyStaff.retrieveCities();
        
        Apexpages.currentPage().getParameters().remove('contactID');
        agencyStaff = new agency_staff(new Apexpages.Standardcontroller(agency));
        
        
        
    }
}