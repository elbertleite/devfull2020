public with sharing class inc_ClientQuotations{
	
	public String selectedQuote {Get;Set;}
	
	public inc_ClientQuotations(ApexPages.StandardController controller){
		selectedQuote = ApexPages.currentPage().getParameters().get('qid');
	}
	
	public List<Quotation__c> getQuotations(){
		
		return [Select Name,Client__c, CreatedBy.Name, Expiry_Date__c, Status__c from Quotation__c where Client__c = :ApexPages.currentPage().getParameters().get('Id') order by createdDate desc];
		
	}

}