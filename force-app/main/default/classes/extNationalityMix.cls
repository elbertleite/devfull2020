public with sharing class extNationalityMix{

	public Account acco {get{return acco;} set{acco = value;}}
    /*
    Controller for Visualforce inPlaceAccounts.page 
    */
    
	public extNationalityMix(ApexPages.StandardController controller) {
		acco = (Account)controller.getRecord();
		
		//newNationalityMixCt = new Nationality_Mix__c(); //Create a new object for Nationality_Mix__c.
	}
	public double totalPercentage {get;set;}
	public boolean edit {get;set;}
	public void editMode(){
		edit = true;
	}
	public void cancelEditMode(){
		edit = false;
	}
	
	public void updateNationalities(){
		
		double total = 0;
		for(Nationality_Mix__c n : NationalityMix)
			total += n.Percentage__c;
		
		if(total > 100){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'The total can not exceed 100%.');
			ApexPages.addMessage(myMsg);			
		} else {
			update NationalityMix;
			edit=false;
		}
		
	}
	

	public Nationality_Mix__c newNationalityMixCt {get{if (newNationalityMixCt == null) newNationalityMixCt = new Nationality_Mix__c(); return newNationalityMixCt;} set; }    //A Nationality_Mix__c variable to add a new Nationality_Mix__c in the place.
	public Nationality_Mix__c editNationalityMixCt { get; set; }    //A temporary variable to store the existing Nationality_Mix__c in place.
    
    
	/*
	@Function: getNationalityMix
	@params: None
	@Return Type: List of Nationality_Mix__c to display
	@Usage: The getter method to fetch Nationality_Mix__c. 
	*/
	private List<Nationality_Mix__c> NationalityMix;
	public List<Nationality_Mix__c> getNationalityMix() {
        totalPercentage = 0;
		NationalityMix =  [Select 
			N.Account__c, 
		N.IsDeleted, 
		N.Nationality__c, 
		N.Percentage__c 
			from Nationality_Mix__c N
		WHERE N.Account__c = :acco.Id And IsDeleted=false order by Nationality__c];
		
		for(Nationality_Mix__c n : NationalityMix)
			totalPercentage += n.Percentage__c;
		
		return NationalityMix; 
	}
    
    
    /*
    @Function: getParam
    @Param: String, the Parameter name to fetch value from.
    @Return Type: String, The value of the URL paramter.
    @Usage: The getter method to fetch Nationality_Mix__c. 
    */
	public String getParam(String name) {
		return ApexPages.currentPage().getParameters().get(name);   
	}
    
    /*
     Action method to add new Nationality_Mix__c. 
    */
	public PageReference addNationalityMixCt() {
			
		if(newNationalityMixCt.Nationality__c == null || newNationalityMixCt.Nationality__c.equals('')){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Nationality: You must enter a value');
			ApexPages.addMessage(msg);
			return null;
		}
				
		if(newNationalityMixCt.Percentage__c == null || newNationalityMixCt.Percentage__c == 0){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Percentage: You must enter a value');
			ApexPages.addMessage(msg);
			return null;
		}
			
			
		if (validate('add'))
			return SaveData();
		else 
			return null;
	}
	
		
     

	public boolean validate(string tp){
		
		try{
			if ([Select count() from Nationality_Mix__c where Nationality__c = :newNationalityMixCt.Nationality__c and Account__c = :acco.Id and isDeleted = false] > 0)
			throw new applicationException(newNationalityMixCt.Nationality__c + ' is already in the list.');
			else if (checkTotalNationalityMix(tp)){
				throw new applicationException('The total can not exceed 100%');
			} 
		}catch(applicationException e){
			ApexPages.addMessages(e);
			return false;
		}
		return true;
	}
	
	public boolean checkTotalNationalityMix(string tp){
		double tot = 0; 
		if (tp=='add'){				
			tot=newNationalityMixCt.Percentage__c;
			for(Nationality_Mix__c nm :NationalityMix)
				tot+= nm.Percentage__c;
		}
		else{
			tot=editNationalityMixCt.Percentage__c;
			for(Nationality_Mix__c nm :NationalityMix){
				if (nm.id != editNationalityMixCt.id)
					tot+= nm.Percentage__c;
			}
		}
		if (tot > 100)
			return true;
		else return false;
				
	}
	
	public PageReference SaveData(){
		//	try {
		newNationalityMixCt.Account__c = acco.Id;
		INSERT newNationalityMixCt;
		// if successful, reset the new newNationalityMixCt for the next entry
		newNationalityMixCt = new Nationality_Mix__c();
		//	} catch (Exception e) {
		//		ApexPages.addMessages(e);
		//	}
		return null;
	
	}


    /*
     Action method to del a Address. 
    */
     
	public PageReference del() {
		try {
			String delid = getParam('delMix');
			Nationality_Mix__c delNationalityMixCt = [SELECT Id FROM Nationality_Mix__c WHERE ID=:delid];
			DELETE delNationalityMixCt;
			
			
		} catch (Exception e) {
			ApexPages.addMessages(e);
		}
		return null;
	}
    
    /*
     Action method to edit a account. 
    */      
	public PageReference editNationalityMixCt() {
		String editid = getParam('editMix');
		editNationalityMixCt = [Select 
			N.Account__c, 
		N.IsDeleted, 
		N.Nationality__c, 
		N.Percentage__c 
			from Nationality_Mix__c N
		WHERE N.Id=:editid];
		return null;
	}
    
    /*
     Action method to cancel new account. 
    */
	public PageReference cancelEdit() {
		editNationalityMixCt= null;
		return null;
	}
    
    /*
     Action method to save the edited account. 
    */
	public PageReference saveeditNationalityMixCt() {
		if (validate('edit')){
	
			try {
				UPDATE editNationalityMixCt;
				editNationalityMixCt= null;
			} catch (Exception e) {
				ApexPages.addMessages(e);
			}
		}
		return null;
	}
    

	public String getGValues()
	{
		String s = '';
		integer i = 0;
		for(Nationality_Mix__c n : NationalityMix)	{
			if(i!=0)
				s+= ',';	
			s += n.Percentage__c;
			i++;}
		return s;
	}
		
	public String getGLabels()
	{
		String s = '';
		integer i = 0;
		for(Nationality_Mix__c n : NationalityMix)	{
			if(i!=0)
				s+= ',';	
			s +='\'' +  n.Nationality__c.replace('\'', '') + '\'';
			i++;}
		
		return s;
	}
	
	public String getGTooltips()
	{
		String s = '';
		integer i = 0;
		for(Nationality_Mix__c n : NationalityMix)	{
			if(i!=0)
				s+= ',';	
			s +='\'' +  n.Nationality__c.replace('\'', '') + ' (' + n.Percentage__c +  '%)\'';
			i++;}
		
		return s;
	}
	
	class applicationException extends Exception {}    

}