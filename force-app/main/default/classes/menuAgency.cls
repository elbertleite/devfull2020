public with sharing class menuAgency{

	public String accoID{get; Set;}
	public String currTab{get;set;}
	public String agencyID {get;set;}
	public String rtName {get;set;}
	public String rtNamePlural {get;set;}
	public Boolean generalUser {get;set;}
	public User userDetails {get;set;}
	public menuAgency (){
		getRecordTypeName();
	}

	public void getRecordTypeName(){
		userDetails = [SELECT Contact.AccountId, General_User_Agencies__c, Finance_Backoffice_User__c, Contact.Account.RecordType.Name, Aditional_Agency_Managment__c FROM User WHERE id = :userInfo.getUserId() limit 1];
		String sName = userDetails.Contact.Account.RecordType.Name;
		generalUser = userDetails.General_User_Agencies__c != null ? true : false;
		// system.debug('User ID==' + UserInfo.getUserId());
		// String ContactID = [Select u.ContactId From User u where User.id = :UserInfo.getUserId()].ContactId;
		// system.debug('Contact ID==' + ContactID);
		// system.debug('Nameee' + sName);
		if(sName!=null){
			if(sName.equals('Agency')){
				rtName = 'Agency';
				rtNamePlural = 'Agencies';
			}

			else if(sName.equals('Campus')){
				rtName = 'School';
				rtNamePlural = 'Schools';
			}
		}
	}

	private Account acco;
	public Account getAcco(){

		if(accoID==null)
			accoID = userDetails.Contact.AccountId;

		acco =  [Select id, CreatedById, CreatedDate, ParentId, RecordTypeId, Logo__c, recordType.name, Campus_Photo_URL__c, Hify_Agency__c,
						(Select Id from Account_Document_Files__r where WIP__c = false and Content_Type__c != 'Folder')
				 from Account A where id= :accoID];
		return acco;
	}

	private String fileURL;
	public String getfileURL(){

		string accountPicture;
		try{
			getAcco();
			if(acco.recordType.name == 'agency')
				accountPicture = acco.Logo__c;
			else accountPicture = acco.Campus_Photo_URL__c;
		}catch(Exception e){}

		// URL for pictures
		if(accountPicture != null && accountPicture != ''){
			fileURL = accountPicture;
		} else {
			fileURL = 'NoPhoto';
		}

		return fileURL;

	}
}