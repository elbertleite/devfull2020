public class ExternalSharingHelper { 
    
	@future 
	public static void linkCampusCourse(Map<Id,Id> campusCourseIdMap, Map<Id,Id> campusIdMap) { 
		System.debug('==> campusCourseIdMap: '+campusCourseIdMap);
		System.debug('==> campusIdMap: '+campusIdMap);
		Set<string> partnerCampusCourseIdSet = new Set<string>(); 
		for (String ccId: campusCourseIdMap.values()) { 
			partnerCampusCourseIdSet.add(ccId.left(15)); 
		} 
		Set<string> CampusIdSet = new Set<string>(); 
		for (String ccId: campusIdMap.values()) { 
			if(ccId != null)
				CampusIdSet.add(ccId.left(15)); 
		} 
		System.debug('==> partnerCampusCourseIdSet: '+partnerCampusCourseIdSet);
		Map<Id,Id> campusCourseMap = new Map<Id,Id>(); 
        Map<Id,Id> campusMap = new Map<Id,Id>(); 
		for (PartnerNetworkRecordConnection campusCourseConnection: 
			[SELECT Id, Status, ConnectionId, PartnerRecordId, LocalRecordId 
			FROM PartnerNetworkRecordConnection 
			WHERE PartnerRecordId in:CampusIdSet]) { 
			if ( campusCourseConnection.Status == 'Sent' || campusCourseConnection.Status == 'Received') {                                                                
				campusMap.put(campusCourseConnection.PartnerRecordId, campusCourseConnection.LocalRecordId); 
			} 
		} 
		
		System.debug('==> partnerCampusCourseIdSet: '+partnerCampusCourseIdSet);
			for (Course__c courses:[SELECT Id, remoteCourseId__c FROM Course__c where remoteCourseId__c in:partnerCampusCourseIdSet]){ 
			System.debug('==> cc: '+courses.remoteCourseId__c + ' == '+ courses.id);
			campusCourseMap.put(courses.remoteCourseId__c, courses.id); 
		}
		
	  
		List<Campus_Course__c> localCampusCourseList = new List<Campus_Course__c>(); 
	        
		for (Id courseId: campusCourseIdMap.keySet()) { 
			Campus_Course__c campusCourseForUpdate = new Campus_Course__c(id=courseId); 
			campusCourseForUpdate.Course__c  = campusCourseMap.get(campusCourseIdMap.get(courseId)); 
			campusCourseForUpdate.Campus__c  = campusMap.get(campusIdMap.get(courseId)); 
			System.debug('==> campusCourseForUpdate: '+campusCourseForUpdate);
			localCampusCourseList.add(campusCourseForUpdate);                                                          
		}                                                            

		database.update(localCampusCourseList); 
	}     
	
	//Create relationship between Campus to School(ParentId)
	@future 
	public static void linkCampusSchool(Map<Id,Id> campusSchoolIdMap) { 
		System.debug('==> campusSchoolIdMap: '+campusSchoolIdMap);
		Set<string> partnerCampusSchoolIdSet = new Set<string>(); 
		for (String ccId: campusSchoolIdMap.values()) { 
			partnerCampusSchoolIdSet.add(ccId.left(15)); 
		} 

		System.debug('==> partnerCampusSchoolIdSet: '+partnerCampusSchoolIdSet);
		Map<Id,Id> campusSchoolMap = new Map<Id,Id>(); 


		
		System.debug('==> partnerCampusSchoolIdSet: '+partnerCampusSchoolIdSet);
		for (Account acco:[SELECT Id, remoteId__c FROM Account where remoteId__c in:partnerCampusSchoolIdSet]){ 
		System.debug('==> cc: '+acco.RemoteId__c + ' == '+ acco.id);
			campusSchoolMap.put(acco.RemoteId__c, acco.id); 
		}
		
	  
		List<Account> localCampusSchoolList = new List<Account>(); 
	        
		for (Id schoolId: campusSchoolIdMap.keySet()) { 
			Account campusSchoolForUpdate = new Account(id=schoolId); 
			campusSchoolForUpdate.ParentId  = campusSchoolMap.get(campusSchoolIdMap.get(schoolId)); 
			//campusSchoolForUpdate.Campus__c  = campusMap.get(campusesIdMap.get(courseId)); 
			System.debug('==> campusSchoolForUpdate: '+campusSchoolForUpdate);
			localCampusSchoolList.add(campusSchoolForUpdate);                                                          
		}                                                            

		database.update(localCampusSchoolList); 
	}     
	
	//Create relationship between CourseProduct, DocFile and Campus
	@future 
	public static void linkCourseFeeLookups(Map<Id,Id> extraFeeProductMap, Map<Id,Id> extraFeeFileMap, Map<Id,Id> extraFeeCampusMap) { 
		
		Course_Extra_Fee__c ExtraFeeForUpdate; 
		
		if(extraFeeProductMap.size() > 0){
			Map<Id,Id> ProductMap = new Map<Id,Id>();
			List<Course_Extra_Fee__c> localExtraFeesList = new List<Course_Extra_Fee__c>(); 
			Set<string> partnerProductIdSet = new Set<string>(); 
			for (String efId: extraFeeProductMap.values()) { 
				partnerProductIdSet.add(efId.left(15)); 
			} 
			for (Product__c prod:[SELECT Id, remoteProductId__c FROM Product__c where remoteProductId__c in:partnerProductIdSet])
				ProductMap.put(prod.remoteProductId__c, prod.id); 
			
			for (Id feeId: extraFeeProductMap.keySet()) { 
				ExtraFeeForUpdate = new Course_Extra_Fee__c(id=feeId); 
				ExtraFeeForUpdate.Product__c = ProductMap.get(extraFeeProductMap.get(feeId)); 
				System.debug('==> ExtraFeeForUpdate: '+ExtraFeeForUpdate);
				localExtraFeesList.add(ExtraFeeForUpdate);                                                          
			}   
			
			if (localExtraFeesList.size() > 0)
				database.update(localExtraFeesList);
		}
		
		if(extraFeeFileMap.size() > 0){
			Map<Id,Id> docFileMap = new Map<Id,Id>();
			List<Course_Extra_Fee__c> localExtraFeesFileList = new List<Course_Extra_Fee__c>(); 
			Set<string> partnerFileIdSet = new Set<string>(); 
			for (String fileId: extraFeeFileMap.values()) { 
				partnerFileIdSet.add(fileId.left(15)); 
			} 
			for (Account_Document_File__c file:[SELECT Id, remoteDocFileId__c FROM Account_Document_File__c where remoteDocFileId__c in:partnerFileIdSet])
				docFileMap.put(file.remoteDocFileId__c, file.id); 
			
			for (Id feeId: extraFeeFileMap.keySet()) { 
				ExtraFeeForUpdate = new Course_Extra_Fee__c(id=feeId); 
				ExtraFeeForUpdate.Account_Document_File__c = docFileMap.get(extraFeeFileMap.get(feeId)); 
				localExtraFeesFileList.add(ExtraFeeForUpdate);                                                          
			}      
		
			if (localExtraFeesFileList.size() > 0)
				database.update(localExtraFeesFileList); 
			
		}
		
		if(extraFeeCampusMap.size() > 0){
			Map<Id,Id> campusMap = new Map<Id,Id>();
			List<Course_Extra_Fee__c> localExtraFeesCampusList = new List<Course_Extra_Fee__c>(); 
			Set<string> partnerCampusIdSet = new Set<string>(); 
			for (String fileId: extraFeeCampusMap.values()) { 
				partnerCampusIdSet.add(fileId.left(15)); 
			} 
			for (Account campus:[SELECT Id, remoteId__c FROM Account where remoteId__c in:partnerCampusIdSet])
				campusMap.put(campus.remoteId__c, campus.id); 
			
			for (Id feeId: extraFeeCampusMap.keySet()) { 
				ExtraFeeForUpdate = new Course_Extra_Fee__c(id=feeId); 
				ExtraFeeForUpdate.Campus__c = campusMap.get(extraFeeCampusMap.get(feeId)); 
				localExtraFeesCampusList.add(ExtraFeeForUpdate);                                                          
			}      
		
			if (localExtraFeesCampusList.size() > 0)
				database.update(localExtraFeesCampusList); 
			
		}
		 
	
	}     
	
	
	//Create relationship between CourseProduct, DocFile
	@future 
	public static void linkCourseFeeDependentLookups(Map<Id,Id> dependentFeeProductMap, Map<Id,Id> dependentFeeFileMap) { 
		
		Course_Extra_Fee_Dependent__c ExtraFeeDependentForUpdate; 
		
		if(dependentFeeProductMap.size() > 0){
			Map<Id,Id> ProductMap = new Map<Id,Id>();
			List<Course_Extra_Fee_Dependent__c> localExtraFeesList = new List<Course_Extra_Fee_Dependent__c>(); 
			Set<string> partnerProductIdSet = new Set<string>(); 
			for (String efId: dependentFeeProductMap.values()) { 
				partnerProductIdSet.add(efId.left(15)); 
			} 
			for (Product__c prod:[SELECT Id, remoteProductId__c FROM Product__c where remoteProductId__c in:partnerProductIdSet])
				ProductMap.put(prod.remoteProductId__c, prod.id); 
			
			for (Id feeId: dependentFeeProductMap.keySet()) { 
				ExtraFeeDependentForUpdate = new Course_Extra_Fee_Dependent__c(id=feeId); 
				ExtraFeeDependentForUpdate.Product__c = ProductMap.get(dependentFeeProductMap.get(feeId)); 
				System.debug('==> ExtraFeeDependentForUpdate: '+ExtraFeeDependentForUpdate);
				localExtraFeesList.add(ExtraFeeDependentForUpdate);                                                          
			}   
			
			if (localExtraFeesList.size() > 0)
				database.update(localExtraFeesList);
		}
		
		if(dependentFeeFileMap.size() > 0){
			Map<Id,Id> docFileMap = new Map<Id,Id>();
			List<Course_Extra_Fee_Dependent__c> localExtraFeesFileList = new List<Course_Extra_Fee_Dependent__c>(); 
			Set<string> partnerFileIdSet = new Set<string>(); 
			for (String fileId: dependentFeeFileMap.values()) { 
				partnerFileIdSet.add(fileId.left(15)); 
			} 
			for (Account_Document_File__c file:[SELECT Id, remoteDocFileId__c FROM Account_Document_File__c where remoteDocFileId__c in:partnerFileIdSet])
				docFileMap.put(file.remoteDocFileId__c, file.id); 
			
			for (Id feeId: dependentFeeFileMap.keySet()) { 
				ExtraFeeDependentForUpdate = new Course_Extra_Fee_Dependent__c(id=feeId); 
				ExtraFeeDependentForUpdate.Account_Document_File__c = docFileMap.get(dependentFeeFileMap.get(feeId)); 
				localExtraFeesFileList.add(ExtraFeeDependentForUpdate);                                                          
			}      
		
			if (localExtraFeesFileList.size() > 0)
				database.update(localExtraFeesFileList); 
			
		}
		
	
	}   
	
	//Create relationship between CourseProduct, DocFile and Campus
	@future 
	public static void linkPromotionLookups(Map<Id,Id> promotionProductMap, Map<Id,Id> promotionFileMap, Map<Id,Id> promotionCampusMap) { 
		
		Deal__c PromotionForUpdate; 
		
		if(PromotionProductMap.size() > 0){
			Map<Id,Id> ProductMap = new Map<Id,Id>();
			List<Deal__c> localPromotionsList = new List<Deal__c>(); 
			Set<string> partnerProductIdSet = new Set<string>(); 
			for (String efId: PromotionProductMap.values()) { 
				partnerProductIdSet.add(efId.left(15)); 
			} 
			for (Product__c prod:[SELECT Id, remoteProductId__c FROM Product__c where remoteProductId__c in:partnerProductIdSet])
				ProductMap.put(prod.remoteProductId__c, prod.id); 
			
			for (Id feeId: PromotionProductMap.keySet()) { 
				PromotionForUpdate = new Deal__c(id=feeId); 
				PromotionForUpdate.Product__c = ProductMap.get(PromotionProductMap.get(feeId)); 
				localPromotionsList.add(PromotionForUpdate);                                                          
			}   
			
			if (localPromotionsList.size() > 0)
				database.update(localPromotionsList);
		}
		
		if(PromotionFileMap.size() > 0){
			Map<Id,Id> docFileMap = new Map<Id,Id>();
			List<Deal__c> localPromotionsFileList = new List<Deal__c>(); 
			Set<string> partnerFileIdSet = new Set<string>(); 
			for (String fileId: PromotionFileMap.values()) { 
				partnerFileIdSet.add(fileId.left(15)); 
			} 
			for (Account_Document_File__c file:[SELECT Id, remoteDocFileId__c FROM Account_Document_File__c where remoteDocFileId__c in:partnerFileIdSet])
				docFileMap.put(file.remoteDocFileId__c, file.id); 
			
			for (Id feeId: PromotionFileMap.keySet()) { 
				PromotionForUpdate = new Deal__c(id=feeId); 
				PromotionForUpdate.Account_Document_File__c = docFileMap.get(PromotionFileMap.get(feeId)); 
				localPromotionsFileList.add(PromotionForUpdate);                                                          
			}      
		
			if (localPromotionsFileList.size() > 0)
				database.update(localPromotionsFileList); 
			
		}
		
		if(PromotionCampusMap.size() > 0){
			Map<Id,Id> campusMap = new Map<Id,Id>();
			List<Deal__c> localPromotionsCampusList = new List<Deal__c>(); 
			Set<string> partnerCampusIdSet = new Set<string>(); 
			for (String fileId: PromotionCampusMap.values()) { 
				partnerCampusIdSet.add(fileId.left(15)); 
			} 
			for (Account campus:[SELECT Id, remoteId__c FROM Account where remoteId__c in:partnerCampusIdSet])
				campusMap.put(campus.remoteId__c, campus.id); 
			
			for (Id feeId: PromotionCampusMap.keySet()) { 
				PromotionForUpdate = new Deal__c(id=feeId); 
				PromotionForUpdate.Campus_Account__c = campusMap.get(PromotionCampusMap.get(feeId)); 
				localPromotionsCampusList.add(PromotionForUpdate);                                                          
			}      
		
			if (localPromotionsCampusList.size() > 0)
				database.update(localPromotionsCampusList); 
			
		}
		 
	
	} 
	
	//Create relationship between CourseProduct, DocFile and Campus
	@future 
	public static void linkStartDateRangeLookups(Map<Id,Id> startDateExtraFeeMap, Map<Id,Id> startDateExtraFeeDependentMap, Map<Id,Id> startDateFileMap, Map<Id,Id> startDateCoursePriceMap, Map<Id,Id> startDatePromotionMap){ 
		
		Start_Date_Range__c startDateRangeForUpdate; 
		
		if(startDateExtraFeeMap.size() > 0){
			Map<Id,Id> extraFeeMap = new Map<Id,Id>();
			List<Start_Date_Range__c> localStartDateFeeList = new List<Start_Date_Range__c>(); 
			Set<string> partnerExtraFeeIdSet = new Set<string>(); 
			for (String efId: startDateExtraFeeMap.values()) { 
				partnerExtraFeeIdSet.add(efId.left(15)); 
			} 
			for (Course_Extra_Fee__c fee:[SELECT Id, remoteExtraFeeId__c FROM Course_Extra_Fee__c where remoteExtraFeeId__c in:partnerExtraFeeIdSet])
				extraFeeMap.put(fee.remoteExtraFeeId__c, fee.id); 
			
			for (Id sdId: startDateExtraFeeMap.keySet()) { 
				startDateRangeForUpdate = new Start_Date_Range__c(id=sdId); 
				startDateRangeForUpdate.Course_Extra_Fee__c = extraFeeMap.get(startDateExtraFeeMap.get(sdId)); 
				localStartDateFeeList.add(startDateRangeForUpdate);                                                          
			}   
			
			if (localStartDateFeeList.size() > 0)
				database.update(localStartDateFeeList);
		}
		
		if(startDateExtraFeeDependentMap.size() > 0){
			Map<Id,Id> ExtraFeeDependentMap = new Map<Id,Id>();
			List<Start_Date_Range__c> localStartDateDependentFeeList = new List<Start_Date_Range__c>(); 
			Set<string> partnerExtraFeeDependentIdSet = new Set<string>(); 
			for (String efId: startDateExtraFeeDependentMap.values()) { 
				partnerExtraFeeDependentIdSet.add(efId.left(15)); 
			} 
			for (Course_Extra_Fee_Dependent__c fee:[SELECT Id, remoteExtraFeeDependentId__c FROM Course_Extra_Fee_Dependent__c where remoteExtraFeeDependentId__c in:partnerExtraFeeDependentIdSet])
				ExtraFeeDependentMap.put(fee.remoteExtraFeeDependentId__c, fee.id); 
			
			for (Id sdId: startDateExtraFeeDependentMap.keySet()) { 
				startDateRangeForUpdate = new Start_Date_Range__c(id=sdId); 
				startDateRangeForUpdate.Course_Extra_Fee_Dependent__c = ExtraFeeDependentMap.get(startDateExtraFeeDependentMap.get(sdId)); 
				localStartDateDependentFeeList.add(startDateRangeForUpdate);                                                          
			}   
			
			if (localStartDateDependentFeeList.size() > 0)
				database.update(localStartDateDependentFeeList);
		}
		
		if(startDateFileMap.size() > 0){
			Map<Id,Id> docFileMap = new Map<Id,Id>();
			List<Start_Date_Range__c> localStartDateFileList = new List<Start_Date_Range__c>(); 
			Set<string> partnerFileIdSet = new Set<string>(); 
			for (String fileId: startDateFileMap.values()) { 
				partnerFileIdSet.add(fileId.left(15)); 
			} 
			for (Account_Document_File__c file:[SELECT Id, remoteDocFileId__c FROM Account_Document_File__c where remoteDocFileId__c in:partnerFileIdSet])
				docFileMap.put(file.remoteDocFileId__c, file.id); 
			
			for (Id sdId: startDateFileMap.keySet()) { 
				startDateRangeForUpdate = new Start_Date_Range__c(id=sdId); 
				startDateRangeForUpdate.Account_Document_File__c = docFileMap.get(startDateFileMap.get(sdId)); 
				localStartDateFileList.add(startDateRangeForUpdate);                                                          
			}      
		
			if (localStartDateFileList.size() > 0)
				database.update(localStartDateFileList); 
			
		}
		
		if(startDateCoursePriceMap.size() > 0){
			Map<Id,Id> coursePriceMap = new Map<Id,Id>();
			List<Start_Date_Range__c> localStartDatePriceList = new List<Start_Date_Range__c>(); 
			Set<string> partnercoursePriceIdSet = new Set<string>(); 
			for (String efId: startDateCoursePriceMap.values()) { 
				partnercoursePriceIdSet.add(efId.left(15)); 
			} 
			for (Course_Price__c fee:[SELECT Id, remotecoursePriceId__c FROM Course_Price__c where remotecoursePriceId__c in:partnercoursePriceIdSet])
				coursePriceMap.put(fee.remotecoursePriceId__c, fee.id); 
			
			for (Id sdId: startDateCoursePriceMap.keySet()) { 
				startDateRangeForUpdate = new Start_Date_Range__c(id=sdId); 
				startDateRangeForUpdate.Course_Price__c = coursePriceMap.get(startDateCoursePriceMap.get(sdId)); 
				localStartDatePriceList.add(startDateRangeForUpdate);                                                          
			}   
			
			if (localStartDatePriceList.size() > 0)
				database.update(localStartDatePriceList);
		}
		
		
		if(startDatePromotionMap.size() > 0){
			Map<Id,Id> promotionMap = new Map<Id,Id>();
			List<Start_Date_Range__c> localStartDatepromotionList = new List<Start_Date_Range__c>(); 
			Set<string> partnerpromotionIdSet = new Set<string>(); 
			for (String efId: startDatePromotionMap.values()) { 
				partnerpromotionIdSet.add(efId.left(15)); 
			} 
			for (Deal__c fee:[SELECT Id, remotepromotionId__c FROM Deal__c where remotepromotionId__c in:partnerpromotionIdSet])
				promotionMap.put(fee.remotepromotionId__c, fee.id); 
			
			for (Id sdId: startDatePromotionMap.keySet()) { 
				startDateRangeForUpdate = new Start_Date_Range__c(id=sdId); 
				startDateRangeForUpdate.Promotion__c = promotionMap.get(startDatePromotionMap.get(sdId)); 
				localStartDatepromotionList.add(startDateRangeForUpdate);                                                          
			}   
			
			if (localStartDatepromotionList.size() > 0)
				database.update(localStartDatepromotionList);
		}
		
		/*if(PromotionCampusMap.size() > 0){
			Map<Id,Id> campusMap = new Map<Id,Id>();
			List<Deal__c> localPromotionsCampusList = new List<Deal__c>(); 
			Set<string> partnerCampusIdSet = new Set<string>(); 
			for (String fileId: PromotionCampusMap.values()) { 
				partnerCampusIdSet.add(fileId.left(15)); 
			} 
			for (Account campus:[SELECT Id, remoteId__c FROM Account where remoteId__c in:partnerCampusIdSet])
				campusMap.put(campus.remoteId__c, campus.id); 
			
			for (Id feeId: PromotionCampusMap.keySet()) { 
				PromotionForUpdate = new Deal__c(id=feeId); 
				PromotionForUpdate.Campus_Account__c = campusMap.get(PromotionCampusMap.get(feeId)); 
				localPromotionsCampusList.add(PromotionForUpdate);                                                          
			}      
		
			if (localPromotionsCampusList.size() > 0)
				database.update(localPromotionsCampusList); 
			
		}*/
		 
	
	} 
	
	//Create relationship between DocFile
	@future 
	public static void linkCoursePriceLookups(Map<Id,Id> coursePriceFileMap) { 
		
		Course_Price__c CoursePriceForUpdate; 
		
		System.debug('==>coursePriceFileMap: '+coursePriceFileMap);
		
		if(CoursePriceFileMap.size() > 0){
			Map<Id,Id> docFileMap = new Map<Id,Id>();
			List<Course_Price__c> localCoursePricesFileList = new List<Course_Price__c>(); 
			Set<string> partnerFileIdSet = new Set<string>(); 
			for (String fileId: CoursePriceFileMap.values()) { 
				partnerFileIdSet.add(fileId.left(15)); 
			} 
			for (Account_Document_File__c file:[SELECT Id, remoteDocFileId__c FROM Account_Document_File__c where remoteDocFileId__c in:partnerFileIdSet])
				docFileMap.put(file.remoteDocFileId__c, file.id); 
			
			for (Id feeId: CoursePriceFileMap.keySet()) { 
				CoursePriceForUpdate = new Course_Price__c(id=feeId); 
				CoursePriceForUpdate.Account_Document_File__c = docFileMap.get(CoursePriceFileMap.get(feeId)); 
				localCoursePricesFileList.add(CoursePriceForUpdate);                                                          
			}      
		
			if (localCoursePricesFileList.size() > 0)
				database.update(localCoursePricesFileList); 
			
		}
		
		 
	
	}   
	
	
	
	//Create relationship between Course and DocFile 
	@future 
	public static void linkCourseLookups(Map<Id,Id> courseFileMap) { 
		
		Course__c courseForUpdate; 
		
				
		if(courseFileMap.size() > 0){
			Map<Id,Id> docFileMap = new Map<Id,Id>();
			List<Course__c> localcoursesFileList = new List<Course__c>(); 
			Set<string> partnerFileIdSet = new Set<string>(); 
			for (String fileId: courseFileMap.values()) { 
				partnerFileIdSet.add(fileId.left(15)); 
			} 
			for (Account_Document_File__c file:[SELECT Id, remoteDocFileId__c FROM Account_Document_File__c where remoteDocFileId__c in:partnerFileIdSet])
				docFileMap.put(file.remoteDocFileId__c, file.id); 
			
			for (Id feeId: courseFileMap.keySet()) { 
				courseForUpdate = new Course__c(id=feeId); 
				courseForUpdate.Account_Document_File__c = docFileMap.get(courseFileMap.get(feeId)); 
				localcoursesFileList.add(courseForUpdate);                                                          
			}      
		
			if (localcoursesFileList.size() > 0)
				database.update(localcoursesFileList); 
			
		}		
				 
	
	} 
	
	
	
	
	@future 
	public static void linkAccountDocFileParentFolderId(Map<Id,Id> AccountDocFileMap){
		list<Account_Document_File__c> localAccountDocList = new List<Account_Document_File__c>(); 
		Account_Document_File__c accountFileForUpdate; 
		Map<Id,Id> docFolderMap = new Map<Id,Id>();
		Set<string> partnerFileIdSet = new Set<string>(); 
		for (String fileId: AccountDocFileMap.values()) { 
			partnerFileIdSet.add(fileId.left(15)); 
		} 
		for (Account_Document_File__c folder:[SELECT Id, remoteDocFileId__c FROM Account_Document_File__c where remoteDocFileId__c in:partnerFileIdSet and Content_Type__c = 'Folder' and WIP__c = false])
			docFolderMap.put(folder.remoteDocFileId__c, folder.id); 
		
		for (Id docId: AccountDocFileMap.keySet()) { 
			accountFileForUpdate = new Account_Document_File__c(id=docId); 
			accountFileForUpdate.Parent_Folder_Id__c = docFolderMap.get(AccountDocFileMap.get(docId)); 
			localAccountDocList.add(accountFileForUpdate);                                                          
		}  
		if (localAccountDocList.size() > 0)
			database.update(localAccountDocList); 
	}
	
	//Create relationship between 
	/*@future 
	public static void linkBOSI(Map<Id,Id> SchoolIdMap, boolean isInsert) { 
		System.debug('==> SchoolIdMap: '+SchoolIdMap);
		Set<string> partnerSchoolIdSet = new Set<string>(); 
		for (String ccId: SchoolIdMap.values()) { 
			partnerSchoolIdSet.add(ccId.left(15)); 
		} 

		System.debug('==> partneSchoolIdSet: '+partnerSchoolIdSet);
		Map<Id,Id> campusSchoolMap = new Map<Id,Id>(); 


		
		System.debug('==> partneSchoolIdSet: '+partnerSchoolIdSet);
		for (Account acco:[SELECT Id, remoteId__c FROM Account where remoteId__c in:partnerSchoolIdSet]){ 
			System.debug('==> cc: '+acco.RemoteId__c + ' == '+ acco.id);
			campusSchoolMap.put(acco.RemoteId__c, acco.id); 
		}
	
		List<Backoffice_System_Information__c> localSchoolList = new List<Backoffice_System_Information__c>(); 
	    List<Backoffice_Agency_Group__c> listBackofficeAgencyGroup = new List<Backoffice_Agency_Group__c>();
		Backoffice_Agency_Group__c newBackofficeAgencyGroup;
		
		for (Id bosiId: SchoolIdMap.keySet()) { 
			Backoffice_System_Information__c bosiForUpdate = new Backoffice_System_Information__c(id=bosiId); 
			bosiForUpdate.School__c  = campusSchoolMap.get(SchoolIdMap.get(bosiId)); 
			//SchoolForUpdate.__c  = Map.get(esIdMap.get(courseId)); 
			System.debug('==> SchoolForUpdate: '+bosiForUpdate);
			localSchoolList.add(bosiForUpdate); 
			if(isInsert){
				newBackofficeAgencyGroup = new Backoffice_Agency_Group__c();
				newBackofficeAgencyGroup.Backoffice_System_Information__c = bosiId;
				listBackofficeAgencyGroup.add(newBackofficeAgencyGroup);
			}
		}                                                            

		database.update(localSchoolList); 
		
		if (listBackofficeAgencyGroup.size() > 0){
			for(Agency_Group__c agId:[Select Id from Agency_Group__c])
				for(Backoffice_Agency_Group__c boag:listBackofficeAgencyGroup )					
					boag.Agency_Group__c = agId.id;
			
			upsert 	listBackofficeAgencyGroup;
		}
		

	}     
	
	//Create relationship between 
	@future 
	public static void linkBOC(Map<Id,Id> SchoolIdMap) { 
		System.debug('==> SchoolIdMap: '+SchoolIdMap);
		Set<string> partnerCampusIdSet = new Set<string>(); 
		for (String ccId: SchoolIdMap.values()) { 
			partnerCampusIdSet.add(ccId.left(15)); 
		} 

		System.debug('==> partneSchoolIdSet: '+partnerCampusIdSet);
		Map<Id,Id> campusSchoolMap = new Map<Id,Id>(); 


		
		System.debug('==> partneSchoolIdSet: '+partnerCampusIdSet);
		for (Account acco:[SELECT Id, remoteId__c FROM Account where remoteId__c in:partnerCampusIdSet]){ 
		System.debug('==> cc: '+acco.RemoteId__c + ' == '+ acco.id);
		campusSchoolMap.put(acco.RemoteId__c, acco.id); 
	}
	
		List<Backoffice_Campus__c> localSchoolList = new List<Backoffice_Campus__c>(); 
	        
		for (Id BOCId: SchoolIdMap.keySet()) { 
			Backoffice_Campus__c BOCForUpdate = new Backoffice_Campus__c(id=BOCId); 
			BOCForUpdate.Campus__c  = campusSchoolMap.get(SchoolIdMap.get(BOCId)); 
			//SchoolForUpdate.__c  = Map.get(esIdMap.get(courseId)); 
			System.debug('==> SchoolForUpdate: '+BOCForUpdate);
			localSchoolList.add(BOCForUpdate);                                                          
		}                                                            

		database.update(localSchoolList); 
	}     
	*/
	
	/*
	//Create relationship between 
	@future 
	public static void linkBONG(Map<Id,Id> NationGroupIdMap) { 
		System.debug('==> NationGroupIdMap: '+NationGroupIdMap);
		Set<string> partnerCampusIdSet = new Set<string>(); 
		for (String ccId: NationGroupIdMap.values()) { 
			partnerCampusIdSet.add(ccId.left(15)); 
		} 

		System.debug('==> partneNationGroupIdSet: '+partnerCampusIdSet);
		Map<Id,Id> campusNationGroupMap = new Map<Id,Id>(); 


		List<Backoffice_Nationality_Group__c> localNationGroupList = new List<Backoffice_Nationality_Group__c>(); 
		
		System.debug('==> partneNationGroupIdSet: '+partnerCampusIdSet);
		for (PartnerNetworkRecordConnection ptn:[Select LocalRecordId, PartnerRecordId, Id from PartnerNetworkRecordConnection P where PartnerRecordId in:NationGroupIdMap.values()]){ 
			campusNationGroupMap.put(ptn.PartnerRecordId, ptn.LocalRecordId); 
		}
	
		System.debug('==> NationGroupIdMap: '+NationGroupIdMap);
		/*for (Backoffice_Nationality_Group__c bong:[Select id, Nationality_Group__c from Backoffice_Nationality_Group__c where id in: NationGroupIdMap.keySet()]){
			Backoffice_Nationality_Group__c BOCForUpdate = new Backoffice_Nationality_Group__c(id=bong.id); 
			BOCForUpdate.Nationality_Group__c  = null; 
			localNationGroupList.add(BOCForUpdate);                                                          
		}                                                            
	
		database.update(localNationGroupList); 
	localNationGroupList.clear();/
		
	//	delete [Select id from Backoffice_Nationality_Group__c where ConnectionReceivedId = null];
		
		
	        
		for (Id BOCId: NationGroupIdMap.keySet()) { 
			Backoffice_Nationality_Group__c BOCForUpdate = new Backoffice_Nationality_Group__c(id=BOCId); 
			BOCForUpdate.Nationality_Group__c  = campusNationGroupMap.get(NationGroupIdMap.get(BOCId)); 
			//NationGroupForUpdate.__c  = Map.get(esIdMap.get(courseId)); 
			System.debug('==> NationGroupForUpdate: '+BOCForUpdate);
			localNationGroupList.add(BOCForUpdate);                                                          
		}                                                            

		database.update(localNationGroupList); 
		}     
	
	
	*/
	
	
	
	
	
	
	
	
	
	
	
	//Delete local records that has been deleted in the Sourse Org
	@future
	public static void deleteRelatedRecords(){

		/*Schema.SObjectType result;
		map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
		String prefix;
		map<String, String> prefixMap = new map<String, String>();
		for (Schema.SObjectType ot : gd.values())
		{
			prefix = ot.getDescribe().getKeyPrefix();
			if (prefix != null)
			{
				prefixMap.put(prefix, ot.getDescribe().getName());
			}
		}

		String sub;
		String lbl;
		map<string, list<string>> objectTypeListIds = new map<string, list<string>>();
		for (PartnerNetworkRecordConnection pt : [Select LocalRecordId from PartnerNetworkRecordConnection where EndDate != null and EndDate = LAST_N_DAYS:2]){
			sub = String.valueOf(pt.LocalRecordId).substring(0, 3);
			if (!objectTypeListIds.containsKey(prefixMap.get(sub))){ 
				List<string> lId = new list<string>();
				lId.add(pt.LocalRecordId);
				objectTypeListIds.put(prefixMap.get(sub),lId);
			}else{
				objectTypeListIds.get(prefixMap.get(sub)).add(pt.LocalRecordId);	
			}
		}
		System.debug('==>objectTypeListIds: '+objectTypeListIds);
		for(string lo:objectTypeListIds.keySet())
			if(lo.equalsIgnoreCase('Account')){
				delete [Select id from Account where ConnectionReceivedId = null AND recordType.name IN ('school', 'campus')];
			}else if(lo.equalsIgnoreCase('Course_Price__c')){
				delete [Select id from Course_Price__c where ConnectionReceivedId = null];	
			}else if(lo.equalsIgnoreCase('Deal__c')){
				delete [Select id from Deal__c where ConnectionReceivedId = null];	
			}else if(lo.equalsIgnoreCase('Course_Intake_Date__c')){
				delete [Select id from Course_Intake_Date__c where ConnectionReceivedId = null];	
			}else if(lo.equalsIgnoreCase('Nationality_Group__c')){
				delete [Select id from Nationality_Group__c where ConnectionReceivedId = null];	
			}else if(lo.equalsIgnoreCase('Account_Document_File__c')){
				delete [Select id from Account_Document_File__c where ConnectionReceivedId = null];
			}

		}*/
		//DELETE ACCOUNT(SHCOOL, CAMPUS)
		try {
    		delete [Select id from Account where ConnectionReceivedId = null AND recordType.name IN ('school', 'campus')];
		} catch(DmlException e) {
		    System.debug(e.getMessage());
		} catch(Exception e) {
		     System.debug(e.getMessage());
		}
		
		
		//DELETE Campus_Course__c
		try {
    		delete [Select id from Campus_Course__c where ConnectionReceivedId = null];
		} catch(DmlException e) {
		    System.debug(e.getMessage());
		} catch(Exception e) {
		     System.debug(e.getMessage());
		}
		
		//DELETE COURSEPRICE
		try {
    		delete [Select id from Course_Price__c where ConnectionReceivedId = null];	
		} catch(DmlException e) {
		    System.debug(e.getMessage());
		} catch(Exception e) {
		     System.debug(e.getMessage());
		}
				
		//DELETE DEAL
		try {
    		delete [Select id from Deal__c where ConnectionReceivedId = null];	
		} catch(DmlException e) {
		    System.debug(e.getMessage());
		} catch(Exception e) {
		     System.debug(e.getMessage());
		}
		
		//DELETE Course_Extra_Fee__c
		try {
    		delete [Select id from Course_Extra_Fee__c where ConnectionReceivedId = null];	
		} catch(DmlException e) {
		    System.debug(e.getMessage());
		} catch(Exception e) {
		     System.debug(e.getMessage());
		}
		
		//DELETE Course__c
		try {
    		delete [Select id from Course__c where ConnectionReceivedId = null];
		} catch(DmlException e) {
		    System.debug(e.getMessage());
		} catch(Exception e) {
		     System.debug(e.getMessage());
		}
		
		
		
		
		
		//DELETE INTAKEDATE
		try {
    		delete [Select id from Course_Intake_Date__c where ConnectionReceivedId = null];
		} catch(DmlException e) {
		    System.debug(e.getMessage());
		} catch(Exception e) {
		     System.debug(e.getMessage());
		}
		
		//DELETE NATIONALITY GROUP
		try {
    		delete [Select id from Nationality_Group__c where ConnectionReceivedId = null];
		} catch(DmlException e) {
		    System.debug(e.getMessage());
		} catch(Exception e) {
		     System.debug(e.getMessage());
		}
		
			
		//DELETE Campus_Instalment_Payment_Date__c
		try {
    		delete [Select id from Campus_Instalment_Payment_Date__c where ConnectionReceivedId = null];
		} catch(DmlException e) {
		    System.debug(e.getMessage());
		} catch(Exception e) {
		     System.debug(e.getMessage());
		}
		
		//DELETE Account_Picture_File__c
		try {    		
    		delete [Select id from Account_Picture_File__c where ConnectionReceivedId = null AND (Parent__r.Recordtype.Name = 'Campus' OR Parent__r.Recordtype.Name = 'School')];
		} catch(DmlException e) {
		    System.debug(e.getMessage());
		} catch(Exception e) {
		     System.debug(e.getMessage());
		}
		
		
		//DELETE Testimonial__c
		try {
    		delete [Select id from Testimonial__c where ConnectionReceivedId = null];
		} catch(DmlException e) {
		    System.debug(e.getMessage());
		} catch(Exception e) {
		     System.debug(e.getMessage());
		}
		
		//DELETE Testimonial_Picture__c
		try {
    		delete [Select id from Testimonial_Picture__c where ConnectionReceivedId = null];
		} catch(DmlException e) {
		    System.debug(e.getMessage());
		} catch(Exception e) {
		     System.debug(e.getMessage());
		}
			
		//DELETE Nationality_Mix__c
		try {
    		delete [Select id from Nationality_Mix__c where ConnectionReceivedId = null];
		} catch(DmlException e) {
		    System.debug(e.getMessage());
		} catch(Exception e) {
		     System.debug(e.getMessage());
		}
		
		
		//DELETE Video__c
		try {
    		delete [Select id from Video__c where ConnectionReceivedId = null];
		} catch(DmlException e) {
		    System.debug(e.getMessage());
		} catch(Exception e) {
		     System.debug(e.getMessage());
		}
		
		//DELETE Product__c
		try {
    		delete [Select id from Product__c where ConnectionReceivedId = null];
		} catch(DmlException e) {
		    System.debug(e.getMessage());
		} catch(Exception e) {
		     System.debug(e.getMessage());
		}
		
		//DELETE Course_Extra_Fee_Dependent__c
		try {
    		delete [Select id from Course_Extra_Fee_Dependent__c where ConnectionReceivedId = null];
		} catch(DmlException e) {
		    System.debug(e.getMessage());
		} catch(Exception e) {
		     System.debug(e.getMessage());
		}
		
		//DELETE Start_Date_Range__c
		try {
    		delete [Select id from Start_Date_Range__c where ConnectionReceivedId = null];
		} catch(DmlException e) {
		    System.debug(e.getMessage());
		} catch(Exception e) {
		     System.debug(e.getMessage());
		}
		
		
		//DELETE ACCOUNT DOCUMENT FILE
		try {
    		delete [Select id from Account_Document_File__c where ConnectionReceivedId = null];
		} catch(DmlException e) {
		    System.debug(e.getMessage());
		} catch(Exception e) {
		     System.debug(e.getMessage());
		}
		
		//DELETE Public Holidays
		try {
    		delete [Select id from Public_Holiday__c where ConnectionReceivedId = null];
		} catch(DmlException e) {
		    System.debug(e.getMessage());
		} catch(Exception e) {
		     System.debug(e.getMessage());
		}
		
	}
}