@isTest
private class client_course_cancel_test {

	public static Account school {get;set;}
	public static Account agency {get;set;}
	public static Contact emp {get;set;}
	public static Contact client {get;set;}
	public static User portalUser {get{
    if (null == portalUser) {
      portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
    } return portalUser;} set;}
	public static Account campus {get;set;}
	public static Course__c course {get;set;}
	public static Campus_Course__c campusCourse {get;set;}
	public static client_course__c clientCourseBooking {get;set;}
	public static client_course__c clientCourse {get;set;}
    
	@testSetup static void setup() {
		TestFactory tf = new TestFactory();
		Account schGroup = tf.createSchoolGroup();

		school = tf.createSchool();
		school.ParentId = schGroup.Id;
		update school;

		agency = tf.createAgency();
		emp = tf.createEmployee(agency);
		portalUser = tf.createPortalUser(emp);
		campus = tf.createCampus(school, agency);
		course = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);

		Schema.DescribeFieldResult fieldResult = client_course__c.Credit_Reason__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		
		Test.startTest();
		system.runAs(portalUser){
			client = tf.createClient(agency);
			clientCourseBooking = tf.createBooking(client);
			clientCourse = tf.createClientCourse(client, school, campus, course, campusCourse, clientCourseBooking);
			List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(clientCourse);
			instalments[0].isPFS__c = true;
			update instalments;

			client_course__c courseCredit =  tf.createClientCourse(client, school, campus, course, campusCourse, clientCourseBooking);
			courseCredit.isCourseCredit__c = true;
			courseCredit.Credit_Available__c = 100000;
			courseCredit.Credit_Valid_For_School__c = school.ParentId;
			update courseCredit;

			client_course__c deposit =  tf.createClientCourse(client, school, campus, course, campusCourse, clientCourseBooking);
			deposit.isDeposit__c = true;
			deposit.Deposit_Total_Available__c = instalments[2].Instalment_Value__c;
			deposit.Deposit_Total_Value__c  = instalments[2].Instalment_Value__c;
			deposit.Deposit_Total_Used__c = 0;
			update deposit;

			client_course_instalment_pay payClass = new client_course_instalment_pay(new ApexPages.StandardController(instalments[0]));
			payClass.newPayDetails.Value__c = instalments[0].Instalment_Value__c;
			payClass.selectedUser = client.id;
			payClass.selectedAgency = agency.id;
			payClass.newPayDetails.Payment_Type__c = 'Cash';
			payClass.addPaymentValue();
			payClass.savePayment();

			payClass = new client_course_instalment_pay(new ApexPages.StandardController(instalments[1]));
			payClass.newPayDetails.Value__c = instalments[1].Instalment_Value__c;
			payClass.selectedUser = client.id;
			payClass.selectedAgency = agency.id;
			payClass.newPayDetails.Payment_Type__c = 'School Credit';
			payClass.addPaymentValue();
			payClass.savePayment();

			payClass = new client_course_instalment_pay(new ApexPages.StandardController(instalments[2]));
			payClass.newPayDetails.Value__c = instalments[2].Instalment_Value__c;
			payClass.selectedUser = client.id;
			payClass.selectedAgency = agency.id;
			payClass.newPayDetails.Payment_Type__c = 'Deposit';
			payClass.addPaymentValue();
			payClass.savePayment();

			payClass = new client_course_instalment_pay(new ApexPages.StandardController(instalments[3]));
			payClass.newPayDetails.Value__c = instalments[3].Instalment_Value__c;
			payClass.selectedUser = client.id;
			payClass.selectedAgency = agency.id;
			payClass.newPayDetails.Payment_Type__c = 'covered by agency';
			payClass.addPaymentValue();
			payClass.savePayment();

			
			//------------- Create Cancel Request ------------------//
			client_course__c course = [SELECT Id, (Select Id from client_course_instalments__r Where ((Received_Date__c != null AND Paid_To_School_On__c = null AND isMigrated__c = false AND isPDS__c = false AND isPCS__c = false AND Original_Instalment__c = NULL) OR (isInstalment_Amendment__c = true AND Total_School_Payment__c> 0 AND Paid_To_School_On__c = null AND isPDS__c = false AND isPCS__c = false))) FROM client_course__c WHERE Booking_Number__c != null AND isCourseCredit__c = FALSE and isDeposit__c = FALSE];

			ApexPages.currentPage().getParameters().put('id', course.Id);
			client_course_cancel test = new client_course_cancel();

			//Cancel Open Payments
			list<String> lInst = new list<String>();
			for(client_course_instalment__c i : course.client_course_instalments__r)
				lInst.add(i.Id);

			client_course_cancel.cancelPayment(lInst);
		 
			client_course_cancel.saveCancel('undefined', client_course_cancel.TYPE_DRAFT, new list<String>{course.Id}, ple[0].getValue(), '0', '');

		}
		Test.stopTest();
  	}

	public static String requestId {get{
    if (null == requestId) {
      requestId = [SELECT Id FROM client_course__c WHERE isCancelRequest__c = true limit 1].Id;
    } return requestId;} set;}

	static testMethod void viewRequest(){
		system.runAs(portalUser){
			String courseId = [ SELECT Id FROM client_course__c WHERE Booking_Number__c != null AND isCourseCredit__c = FALSE and isDeposit__c = FALSE AND isCancelRequest__c = FALSE limit 1].Id;

			ApexPages.currentPage().getParameters().put('id', courseId);
			ApexPages.currentPage().getParameters().put('ac', 'view');

			client_course_cancel test = new client_course_cancel();

			Schema.DescribeFieldResult fieldResult = client_course__c.Credit_Reason__c.getDescribe();
			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

			client_course_cancel.saveCancel(requestId, client_course_cancel.TYPE_DRAFT, new list<String>{courseId}, ple[0].getValue(), '0', '');
			
		}
	}

	static testMethod void checkInstalments(){
		system.runAs(portalUser){
			String courseId = [ SELECT Id FROM client_course__c WHERE Booking_Number__c != null AND isCourseCredit__c = FALSE and isDeposit__c = FALSE AND isCancelRequest__c = FALSE limit 1].Id;
			client_course_cancel.checkInstalments(new list<String>{courseId});
		}
	}

	static testMethod void addNote(){
		system.runAs(portalUser){
			client_course_cancel.addNote(requestId, 'Test');
		}
	}

	static testMethod void requestToAdm(){
		system.runAs(portalUser){
			client_course_cancel.requestToADM(requestId, 'Test');
		}
	}

	static testMethod void undoRequest(){
		system.runAs(portalUser){
			client_course_cancel.undoRequest(requestId, 'Test');
		}
	}

	static testMethod void missingDocs(){
		system.runAs(portalUser){
			client_course_cancel.missingDocs(requestId, 'Test');
		}
	}

	static testMethod void requestRefund(){
		system.runAs(portalUser){
			client_course_cancel.requestRefund(requestId, '2500', 'Test', 'Cancel Req. SCH');
		}
	}

	static testMethod void requestCredit(){
		system.runAs(portalUser){
			client_course_cancel.requestCredit(requestId, '2500', 'Test');
		}
	}
	
	static testMethod void cancelCourse(){
		system.runAs(portalUser){
			Account agency = [Select Id from Account where RecordType.Name = 'Agency' limit 1];
			client_course__c course = [SELECT Id FROM client_course__c WHERE Booking_Number__c != null AND isCourseCredit__c = FALSE and isDeposit__c = FALSE];
			client_course_cancel.cancelCourse(requestId, 'cancel notes', 'cancel', null, null); //Cancel Course
		}
	}

	static testMethod void cancelCourse2(){
		system.runAs(portalUser){
			Account agency = [Select Id from Account where RecordType.Name = 'Agency' limit 1];
			client_course__c course = [SELECT Id FROM client_course__c WHERE Booking_Number__c != null AND isCourseCredit__c = FALSE and isDeposit__c = FALSE];
			client_course_cancel.cancelCourse(requestId, 'cancel notes', 'cancel', null, new client_course_cancel.refundDetails(requestId, 'agency', string.valueOf(agency.Id), 1000, system.today(), new map<String, Decimal>{course.Id => 100}, 0, 1100, system.today(), 'notes', '2', 'Cancel Req. SCH', system.today())); //Cancel Course
		}
	}

	static testMethod void transferCourse(){
		system.runAs(portalUser){
			Account agency = [Select Id from Account where RecordType.Name = 'Agency' limit 1];
			client_course__c course = [SELECT Id FROM client_course__c WHERE Booking_Number__c != null AND isCourseCredit__c = FALSE and isDeposit__c = FALSE];
			client_course_cancel.cancelCourse(requestId, 'cancel notes', 'transfer', 1000, null); //Course Transfer
		}
	}
	static testMethod void refundSchool(){
		system.runAs(portalUser){
			Account agency = [Select Id from Account where RecordType.Name = 'Agency' limit 1];
			client_course__c course = [SELECT Id FROM client_course__c WHERE Booking_Number__c != null AND isCourseCredit__c = FALSE and isDeposit__c = FALSE];
			client_course_cancel.cancelCourse(requestId, 'cancel notes', 'refund', 1000,  new client_course_cancel.refundDetails(requestId, 'school', 1000, string.valueOf(agency.Id), system.today(), 'notes')); //Refund by School
		}
	}
	static testMethod void reefundAgency(){
		system.runAs(portalUser){
			Account agency = [Select Id from Account where RecordType.Name = 'Agency' limit 1];
			client_course__c course = [SELECT Id FROM client_course__c WHERE Booking_Number__c != null AND isCourseCredit__c = FALSE and isDeposit__c = FALSE];
			client_course_cancel.cancelCourse(requestId, 'cancel notes', 'refund', 1000,  new client_course_cancel.refundDetails(requestId, 'agency', string.valueOf(agency.Id), 1000, system.today(), new map<String, Decimal>{course.Id => 100}, 0, 1100, system.today(), 'notes', '2', 'Cancel Req. SCH', system.today())); //Refund by Agency
		}
	}

	static testMethod void innerClasses(){
		system.runAs(portalUser){
			client_course_cancel.courseInfo ci = new client_course_cancel.courseInfo(new client_course__c(), new list<IPFunctions.noteWrapper>(), new list<IPFunctions.activityWrapper>(),  new list<IPFunctions.s3Docs>());
			
			ci = new client_course_cancel.courseInfo(new client_course__c(), new list<IPFunctions.noteWrapper>(), new list<IPFunctions.activityWrapper>(),  'global link', false);

			ci = new client_course_cancel.courseInfo(new client_course__c(), new list<IPFunctions.noteWrapper>(), new list<IPFunctions.activityWrapper>(),  'global link', new list<IPFunctions.s3Docs>());

			client_course_cancel.requestsWrapper rw = new client_course_cancel.requestsWrapper(new list<client_course_cancel.courseInfo>{ci}, 1, 1, 1, 1, 1);
			
			client_course_cancel.refundDetails rd = new client_course_cancel.refundDetails('cId', 'whoRef', 10000, 'agencyId', system.today(), 'notes');

			rd = new client_course_cancel.refundDetails('cId', 'whoRef', 'agencyId', 1000, system.today(), new map<String, Decimal>(), 0, 1000, system.today(), 'notes', '2', 'Cancel Req. SCH', system.today());

		}
	}
}