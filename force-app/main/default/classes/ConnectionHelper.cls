public class ConnectionHelper {

	public static Id getConnectionId(String connectionName) {
        return [Select id from PartnerNetworkConnection where connectionStatus = 'Accepted' and connectionName =:connectionName limit 1].id;
    }



	public static Id getConnectionOwnerId(String connectionName) {
    
		List<PartnerNetworkConnection> partnerNetConList = 
				[Select createdById from PartnerNetworkConnection where connectionStatus = 'Accepted' and connectionName =:connectionName];
        
		if ( partnerNetConList.size()!= 0 ) {
		return partnerNetConList.get(0).createdById;
	}
        
		return null;
		}
		
	public static list<Id> getConnectionId() {
    
		list<id> listConnectionIds = new list<id>();
		for(PartnerNetworkConnection partnerNetConList:[Select id from PartnerNetworkConnection where connectionStatus = 'Accepted'])
			listConnectionIds.add(partnerNetConList.id);
		
		if (listConnectionIds.size()!= 0 ) 
			return listConnectionIds;
		else return null;
	}
}