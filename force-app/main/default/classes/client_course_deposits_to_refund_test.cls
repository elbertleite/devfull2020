/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class client_course_deposits_to_refund_test {

    static testMethod void myUnitTest() {
    	
       TestFactory tf = new TestFactory();
       
       Account school = tf.createSchool();
       Account agency = tf.createAgency();
       Contact emp = tf.createEmployee(agency);
       User portalUser = tf.createPortalUser(emp);
       Account campus = tf.createCampus(school, agency);
       Course__c course = tf.createCourse();
       Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);

       
       Test.startTest();
       system.runAs(portalUser){
    	
    	   Contact client = tf.createLead(agency, emp);
	       client_course__c booking = tf.createBooking(client);
	       client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
	       
		   client_course_deposit toDeposit = new client_course_deposit(new ApexPages.StandardController(client));
	       
	       String clientName = toDeposit.clientName;
	       toDeposit.getdepositListType();
	       Account agencyDetails = toDeposit.agencyDetails;
	       
	       toDeposit.newDepositValue.Value__c=2230;
	       toDeposit.newDepositValue.Date_Paid__c=system.today();
	       toDeposit.newDepositValue.Payment_Type__c = 'Cash';
	       toDeposit.addDepositValue();
	       toDeposit.saveDeposit();        
	       
	       
	       client_course_deposit_refund toRefund = new client_course_deposit_refund(new ApexPages.StandardController(client));
	      
	       Account agencyDetails2 = toRefund.agencyDetails;
	       system.debug('client======>' + client);
	       toRefund.refund.Description__c = 'Client paid too much.';
	       toRefund.refund.Value__c = 100;
	       toRefund.requestRefund();
	      
	       toRefund.refund.Value__c = 200;
	       toRefund.refund.Description__c = 'Client paid too much again.';
	       toRefund.requestRefund();
	       
	      
	       client_course_deposits_to_refund testClass = new client_course_deposits_to_refund();
	       
	       /* C O N F I R M 	R E F U N D **/
	       ApexPages.CurrentPage().getParameters().put('id',testClass.refDeposits[0].id);
	       testClass.confirmRefund();
	       testClass.refDeposits[0].Payment_Type__c = 'Cash';
	       testClass.confirmRefund();
	       testClass.refDeposits[0].Date_Paid__c = Date.today().addDays(1);
	       testClass.confirmRefund();
	       testClass.refDeposits[0].Date_Paid__c = Date.today();
	       testClass.confirmRefund();
	       
	       
	       /* C A N C E L 	 	R E F U N D **/
	       ApexPages.CurrentPage().getParameters().put('id',testClass.refDeposits[0].id);
	       testClass.setIdCancel();
	       testClass.cancelRefund();
	       

	      
	       testClass.getAgencies();
	       testClass.getrefundActions();
	       testClass.getdepositListType();
	       
	       List<SelectOption> agencyGroupOptions = testClass.agencyGroupOptions;
	       List<SelectOption> schoolOptions = testClass.schoolOptions;
	       List<SelectOption> campusOptions = testClass.campusOptions;
	       
	       testClass.changeGroup();
	       testClass.changeSchool();
       }   
    }
}