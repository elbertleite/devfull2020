global class FileUpload extends AWSHelper {

	public static final String SEPARATOR_URL = '!#!';
	public static final String SEPARATOR_FILE = ';;';


	private static string createFilename(String filename){
		String options = '[u:' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + ';]';
		return options + filename;

	}

	public static String[] breakFilename(String filename){ //possible filenames: "[u:Natalia Moura]Passaporte.jpeg" or "005O0000003TyjW - Renata Ilonciakova - IP%20Header%20Page%20Test.html"
		try {
			if(filename.contains('[') && filename.contains(']')){
				String options = filename.substringBefore(']');
				String username = options.substringAfter('u:').substringBefore(';');
				String thefilename = filename.substringAfter(']');
				return new String[]{username, thefilename};
			} else if(filename.contains('-')){
				String splitToken = ' - ';
				String[] thesplit = filename.split(splitToken);
				String userid = thesplit[0];
				String username = thesplit[1];
				String thefilename = filename.remove(userid+splitToken).remove(username+splitToken).trim();
				return new String[]{username, thefilename};
			} else
				return new String[]{'', filename};
		} catch (exception e){
			return new String[]{'', filename};
		}

	}


	@RemoteAction
	global static Authorization getAuthorizationObject(String bucket, String folder, String filename, String contentType, String description) {

		bucket = IPFunctions.isOrgBucket() ? bucket.toLowerCase() : bucket;

		filename = createFilename(filename);

		string path = AWSHelper.uriEncode(bucket + '/' + folder + '/' + filename).replaceAll('%2F', '/');
		path = path.replaceAll('//','/');

		Authorization auth = new Authorization();
		auth.endpoint = 'https://s3.amazonaws.com';
		auth.path = path;

		Datetime requestTime = DateTime.now();
		URL endpoint = new Url('https://s3.amazonaws.com/' + path);

		auth.authorization = AWSHelper.getAuthorizationHeader(requestTime, endpoint, description);
		auth.host = endpoint.getHost();

		// String formattedDateString = requestTime.formatGMT('E, dd MMM yyyy HH:mm:ss z');

        // auth.timestamp = formattedDateString;

        auth.timestamp = requestTime.formatGmt('yyyyMMdd\'T\'HHmmss\'Z\'');

		return auth;
	}

	@RemoteAction
  	global static String listFiles(String bucket, String folder, boolean saveData, String field, String oid){
		return FileUpload.listFiles2(bucket, folder, saveData, field, oid, false, false);
	}

	@RemoteAction
	global static String listFiles2(String bucket, String folder, boolean saveData, String field, String oid, Boolean limitFilesToFolder, Boolean callUpdatePMCFiles){

		bucket = IPFunctions.isOrgBucket() ? bucket.toLowerCase() : bucket;

		AWSKeys credentials = new AWSKeys(IPFunctions.awsCredentialName);
		List<File> files = new List<File>();
		S3Controller s3 = new S3Controller();
		s3.constructor();

		List<String> previewList = new List<String>();

		if(Test.isRunningTest()){
			S3.ListEntry le = new S3.ListEntry();
			le.key = '/test/[u:Natalia Moura]Passaporte.jpeg';
			le.lastmodified = system.now();
			le.etag = '9N8V72834V5N985734';
			le.size = 123434.34;
			s3.bucketList = new S3.ListEntry[]{ le };
		} else {
			system.debug('SEARCHING THE FOLDER '+folder);
			folder = folder+'/';
			folder = folder.replaceAll('//','/');
  			s3.listBucket(bucket, folder, null, null, limitFilesToFolder ? '/' : null);
		}

		system.debug('THE LIST FOUND '+JSON.serialize(s3.bucketList));

		String removeStart;
		if(folder.trim().endsWith('/')){
			removeStart = folder.trim();
		}else{
			removeStart = folder.trim()+'/';
		}
		String fname;
		String previewLink;
		List<Account_Document_File__c> filesToSave = new List<Account_Document_File__c>();
		Account_Document_File__c fileToSave;

		Account_Document_File__c folderToVerify = null;
        Set<String> existingFiles = new Set<String>(); 
        if(callUpdatePMCFiles){
            folderToVerify = [SELECT ID, Parent__c FROM Account_Document_File__c WHERE ID = :oid];
            for(Account_Document_File__c existingFile : [SELECT ID, File_Name__c FROM Account_Document_File__c WHERE Parent__c = :folderToVerify.Parent__c AND Parent_Folder_Id__c = :oid AND Content_Type__c != 'Folder']){
                existingFiles.add(existingFile.File_Name__c);
            }
        }

		if(s3.bucketList != null && !s3.bucketList.isEmpty()){
  			for(s3.ListEntry file : s3.bucketList){

				//system.debug('THE REMOVE START '+removeStart);
				//system.debug('THE FILEKEY '+file.key);
				fname = file.key.remove(removeStart);
				//system.debug('THE FNAME '+fname);

				if(!fname.endsWith('/') && fname != null && fname.trim() != ''){

  					String url = AWSHelper.generateAWSLink(bucket, file.key, credentials.key, credentials.secret);
					String[] decon = breakFilename(fname);
					String user = decon[0];
					String shortfilename = decon[1];
  					String urlFileDownload = AWSHelper.generateAWSLinkFileDownload(bucket, file.key, credentials.key, credentials.secret, shortfilename);
					previewLink = url + SEPARATOR_URL + shortfilename + SEPARATOR_URL + urlFileDownload + SEPARATOR_URL + file.LastModified.format();
					previewList.add(previewLink);
					files.add(new file( shortfilename, fname, file.size, url, urlFileDownload, user, file.LastModified.format() ) ); 

					if(callUpdatePMCFiles){
						fileToSave = new Account_Document_File__c();
						fileToSave.S3_File_Name__c = fname; 
						fileToSave.Parent__c = folderToVerify.Parent__c;
						fileToSave.Is_Folder_New_School_Page__c = true;
						fileToSave.Content_Type__c = IPFunctions.getContentType(shortfilename);
						fileToSave.Parent_Folder_Id__c = oid;
						fileToSave.File_Name__c = shortfilename; 
						fileToSave.Description__c = 'No description provided.';
						fileToSave.Preview_Link__c = previewLink;
						fileToSave.File_Size_in_Bytes__c = file.size;
						if(existingFiles.isEmpty() || !existingFiles.contains(shortfilename)){
                            filesToSave.add(fileToSave);
                        }
					}
				}
			}
		}
		System.debug('VERIFICANDO SAVEDATA ==*** '+saveData+ ' '+field+' '+ oid);

		if(saveData && AWSHelper.hasValue(field) && AWSHelper.hasValue(oid)){			
			System.debug('ATUALIZOU O OBJETO ');
			System.debug(previewList);
			sobject sObj = id.valueOf(oid).getSobjectType().newSObject(oid) ;
			System.debug(sObj);
			sObj.put(field, String.join(previewList, SEPARATOR_FILE));
			update sObj;
			System.debug(sObj);
			System.debug('ATUALIZOU O PREVIEWLINK OBJETO ');
		
			if(callUpdatePMCFiles && !filesToSave.isEmpty()){
				insert filesToSave;
			}
		}


		return JSON.serialize(files);
	}


	@RemoteAction
	global static boolean deleteObject(String bucket, String folder, String filename) {

		bucket = IPFunctions.isOrgBucket() ? bucket.toLowerCase() : bucket;

		AWSKeys credentials = new AWSKeys(IPFunctions.awsCredentialName);
		
		system.debug('THE FOLDER TO DELETE '+folder);
		system.debug('THE FILE NAME '+filename);
		
		return AWSHelper.deleteObjectFromS3(credentials.key, credentials.secret, bucket, folder, filename);

	}














 }