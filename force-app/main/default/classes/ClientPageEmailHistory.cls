public with sharing class ClientPageEmailHistory {
	public String idContact{get;set;}

	public List<IPClasses.ContactEmail> emailsHistory{get;set;}
	//public List<IPClasses.ContactEmail> emailsToShow{get;set;}

	public User user{get;set;}
	public String filterSelected{get;set;}
	public boolean allowViewSchoolEmails{get;set;}

	/*public Integer maxEmailsPerPage{get{if(maxEmailsPerPage == null) maxEmailsPerPage = 5; return maxEmailsPerPage;}set;}
	public Integer currentPage{get{if(currentPage == null) currentPage = 1; return currentPage;}set;}
	public Integer totalEmails{get;set;}
	public List<IPClasses.Paginator> pages{get;set;}*/

	/*public void verifyMandrillStatus(IPClasses.ContactEmail email, Map<String, Map<String, String>> clientEmailsStatus){
		String status = 'Sent';
		if(clientEmailsStatus != null){
			List<String> emailKeyData = email.key.split(':-:');
			if(emailKeyData.size() > 5){
				String mandrillEmailKey = emailKeyData.get(emailKeyData.size() - 1);
				if(clientEmailsStatus.containsKey(mandrillEmailKey)){
					status = clientEmailsStatus.get(mandrillEmailKey).get('status');
					if(clientEmailsStatus.get(mandrillEmailKey).containsKey('mandrillMsg')){
						email.mandrillMsg = clientEmailsStatus.get(mandrillEmailKey).get('mandrillMsg');
					}
				} 	
			}
		}
		email.mandrillStatus = status;
	}*/

	public ClientPageEmailHistory() {
		user = [SELECT ID, Name FROM User WHERE ID = :UserInfo.getUserId()];
		emailsHistory = new List<IPClasses.ContactEmail>();
		idContact = ApexPages.currentPage().getParameters().get('id');
		filterSelected = 'showAll';
		allowViewSchoolEmails = Schema.sObjectType.User.fields.Allow_View_Email_School_Invoice__c.isAccessible();
		String emailsJson = null;
		List<IPClasses.ContactEmail> listEmails = null;
		
		emailsJson = new EmailFromS3Controller().getEmailsJson(idContact);
		listEmails = (List<IPClasses.ContactEmail>) JSON.deserialize(emailsJson, List<IPClasses.ContactEmail>.class);
		
		Contact ctt = [SELECT ID, Email, Mandrill_Emails_Status__c FROM Contact WHERE ID = :idContact];
		Map<String, Map<String, String>> clientEmailsStatus = null;
		if(!String.isEmpty(ctt.Mandrill_Emails_Status__c)){
			clientEmailsStatus = (Map<String, Map<String, String>>) JSON.deserialize(ctt.Mandrill_Emails_Status__c, Map<String, Map<String, String>>.class);
		}
		Map<String, String> mandrillResponse;
		for(IPClasses.ContactEmail email : listEmails){
			email.emailType = 'emailFromClient';
			
			mandrillResponse = IPFunctions.retrieveMandrillEmailStatusAndMessage(email.key, clientEmailsStatus); 
			email.mandrillStatus = mandrillResponse.get('status');
			if(mandrillResponse.containsKey('mandrillMsg')){
				email.mandrillMsg = mandrillResponse.get('mandrillMsg');
			}
			//verifyMandrillStatus(email, clientEmailsStatus);
			emailsHistory.add(email);
		}

		if(Test.isRunningTest()){
			allowViewSchoolEmails = true;
		}
		if(allowViewSchoolEmails){
			emailsJson = new contact_school_emails(idContact).emailsFromClient(idContact);
			listEmails = (List<IPClasses.ContactEmail>) JSON.deserialize(emailsJson, List<IPClasses.ContactEmail>.class);
			
			for(IPClasses.ContactEmail email : listEmails){
				//email.emailType = 'emailFromClient';
				email.emailType = 'emailFromSchool';
				mandrillResponse = IPFunctions.retrieveMandrillEmailStatusAndMessage(email.key, clientEmailsStatus);
				email.mandrillStatus = mandrillResponse.get('status');
				if(mandrillResponse.containsKey('mandrillMsg')){
					email.mandrillMsg = mandrillResponse.get('mandrillMsg');
				}
				//verifyMandrillStatus(email, clientEmailsStatus);
				emailsHistory.add(email);
			}
		}
		emailsHistory.sort();
		if(emailsHistory != null && !emailsHistory.isEmpty()){
			//generatePagination();
			//loadPage();
		}
	}

	/*public void loadPage(){
		String pageNumber = ApexPages.currentPage().getParameters().get('page');
		if(String.isEmpty(pageNumber)){
			pageNumber = '1';
		}
		currentPage = Integer.valueOf(pageNumber);

		emailsToShow = new List<IPClasses.ContactEmail>();
		List<IPClasses.ContactEmail> emailsToRun = new List<IPClasses.ContactEmail>();
		
		IPClasses.Paginator pageToOpen = pages.get(pages.indexOf(new IPClasses.Paginator(currentPage)));
		if(filterSelected == 'showAll'){
			emailsToRun = emailsHistory;
		}else if(filterSelected == 'showClient'){
			for(IPClasses.ContactEmail email : emailsHistory){
				if(email.emailType == 'emailFromClient'){
					emailsToRun.add(email);
				}
			}
		}else{
			for(IPClasses.ContactEmail email : emailsHistory){
				if(email.emailType == 'emailFromSchool'){
					emailsToRun.add(email);
				}
			}
		}
		for(Integer i = pageToOpen.firstResult; i <= pageToOpen.lastResult ; i++){
			if(i < emailsToRun.size()){
				emailsToShow.add(emailsToRun.get(i));
			}
		}
	}*/

	/*public void generatePagination(){
		totalEmails = 0;
		if(filterSelected == 'showAll'){
			totalEmails = emailsHistory.size();
		}else if(filterSelected == 'showClient'){
			for(IPClasses.ContactEmail email : emailsHistory){
				if(email.emailType == 'emailFromClient'){
					totalEmails += 1;
				}
			}
		}else{
			for(IPClasses.ContactEmail email : emailsHistory){
				if(email.emailType == 'emailFromSchool'){
					totalEmails += 1;
				}
			}
		}
		system.debug('TOTAL EMAILS '+totalEmails);
		Integer totalPages = totalEmails / maxEmailsPerPage;
		if(Math.mod(totalEmails, maxEmailsPerPage) > 1){
			totalPages += 1;
		}
		pages = new List<IPClasses.Paginator>();
		for(Integer i = 1; i <= totalPages; i++){
			pages.add(new IPClasses.Paginator(i, maxEmailsPerPage*(i-1), maxEmailsPerPage*i -1));
		}
		system.debug('PAGES '+pages);
	}*/

	/*public void refreshTableEmails(){
		if(filterSelected == 'showAll'){
			for(IPClasses.ContactEmail email : emailsHistory){
				email.showEmail = true;
			}
		}else if(filterSelected == 'showClient'){
			for(IPClasses.ContactEmail email : emailsHistory){
				if(email.emailType == 'emailFromClient'){
					email.showEmail = true;
				}else{
					email.showEmail = false;
				}
			}
		}else{
			for(IPClasses.ContactEmail email : emailsHistory){
				if(email.emailType == 'emailFromSchool'){
					email.showEmail = true;
				}else{
					email.showEmail = false;
				}
			}
		}
	}*/

	@RemoteAction
	public static String openDetailsEmail(String emailKey, String emailType){
		String body = '';
		try{
			if(emailType == 'emailFromClient'){
				body = EmailFromS3Controller.openEmail(emailKey);
				system.debug('EmailFromS3Controller GOT EMAIL '+body);
			}else{
				body = contact_school_emails.openEmail(emailKey);
				system.debug('contact_school_emails GOT EMAIL '+body);
			}
		}catch(Exception e){
			system.debug('THERE WAS AN ERROR RETRIEVING THE EMAIL '+ e.getLineNumber() + ' '+e.getMessage());
			body = 'THERE WAS AN ERROR RETRIEVING THIS EMAIL. PLEASE CONTACT THE ADMINISTRATOR. ';
		}
		return body;
	}
}