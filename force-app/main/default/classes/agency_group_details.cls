public with sharing class agency_group_details {
 
    public Account agencyGroup {get;set;}
    public String agencyGroupID {get;set;}
    public String groupLink {get;set;}
    public Account agency {get;set;}
    public list<String> countries;
    public string numStudents{get{if(numStudents==null) numStudents = ''; return numStudents;} set;}
    public string program{get{ if(program == null) program = ''; return program; } set;}
    public string programKey {get;set;}
    public string networkUrl{get{if(networkUrl==null) networkUrl = ''; return networkUrl;} set;}
    public string networkName{get{ if(networkName == null) networkName = ''; return networkName; } set;}
    public string networkKey {get;set;}
    public string videoKey{get{if(videoKey==null) videoKey = ''; return videoKey;} set;}
    public string videoHostName{get{ if(videoHostName == null) videoHostName = ''; return videoHostName; } set;}
    public string videoListKey {get;set;}
    public string directorsName{get{ if(directorsName == null) directorsName = ''; return directorsName; } set;}
    public string directorsKey {get;set;}
    public set<string> listDirectors { get{ if(listDirectors == null) listDirectors = new set<string>(); return listDirectors; } set; }
 
  public String defaultPreferableAgency{get;set;}
    
    //public string industryCertificatesList {get{if(industryCertificatesList==null) industryCertificatesList = ''; return industryCertificatesList;} set;}
    public string industry{ get{if(industry==null) industry=''; return industry; } set;}
    
    /* LIST */
  public list<string> destinationsOffered{get{ if(destinationsOffered == null) destinationsOffered = new list<string>(); return destinationsOffered; } set; }
  public list<string> mainCustormesFrom{get{ if(mainCustormesFrom == null) mainCustormesFrom = new list<string>(); return mainCustormesFrom; } set; }
  public list<string> mainCustomersTypesList { get{ if(mainCustomersTypesList == null) mainCustomersTypesList = new list<string>(); return mainCustomersTypesList; } set; }
  public list<string> recruitingStudentsList { get{ if(recruitingStudentsList == null) recruitingStudentsList = new list<string>(); return recruitingStudentsList; } set; }
  public set<string> industryCertificatesList { get{ if(industryCertificatesList == null) industryCertificatesList = new set<string>(); return industryCertificatesList; } set; }
  
  /* MAP */ 
  public map<string, string> socialNetwork { get{ if(socialNetwork == null) socialNetwork = new map<string, string>(); return socialNetwork; } set; }
  public map<string, string> estudentsEnroller { get{ if(estudentsEnroller == null) estudentsEnroller = new map<string, string>(); return estudentsEnroller; } set; }
  public map<string, string> videos { get{ if(videos == null) videos = new map<string, string>(); return videos; } set; }
  public map<string, string> agencyNames { get{ if(agencyNames == null) agencyNames = new map<string, string>(); return agencyNames; } set; }
 
  public List<IPClasses.NewContactPageHeaderFields> fieldsHeaderNewPage{get;set;}
 
  public Map<String, String> languages{get;set;}
  public Map<String, Map<String, String>> quotationFieldsTranslated{get;set;}
  public List<SelectOption> optionsQuotationFieldsToTranslate{get;set;}
  public String quotationFieldToTranslate{get;set;}
  public String quotationFieldTranslation{get;set;}
  public List<SelectOption> optionsLanguageToTranslate{get;set;}
  public String fieldLanguageTranslation{get;set;}
  
  public Map<String, String> quoteExpiredMessageTranslation{get;set;}
  public String quoteExpiredLanguageTranslation{get;set;}
  public String quoteExpiredTranslation{get;set;}
  
  public Map<String, String> combinedQuotationMessageTranslation{get;set;}
  public String combinedQuotationLanguageTranslation{get;set;}
  public String combinedQuotationTranslation{get;set;}
  
  public Map<String, List<String>> quoteLinkThumbnailInfo{get;set;}
  public List<SelectOption> optionsQuoteLinkThumbnail{get;set;}
  public List<SelectOption> optionsValuesQuoteLinkThumbnail{get;set;}
  public String optionSelectedQuoteLinkThumbnail{get;set;}
  public String valueSelectedQuoteLinkThumbnail{get;set;}

    public agency_group_details(ApexPages.StandardController controller){
        
        agency = [Select id, ParentID, Global_Link__c from account where id = :controller.getRecord().id];
        agencyGroupID =  agency.ParentID;
        groupLink =  agency.Global_Link__c;
        List<SelectOption> ag = ga.AgencyUsers;
        loadData();
 
    optionsLanguageToTranslate = IPFunctions.getContactPreferableLanguages();
    optionsLanguageToTranslate.remove(0);
    languages = IPFunctions.getMapLanguages();
    loadQuotationsFieldToTranslate();
    }
 
  public void saveQuoteExpiredMessageTranslation(){
    if(quoteExpiredMessageTranslation == null){
      quoteExpiredMessageTranslation = new Map<String, String>(); 
    }
    quoteExpiredMessageTranslation.put(quoteExpiredLanguageTranslation, quoteExpiredTranslation);
    quoteExpiredTranslation = null;
  }
 
  public void deleteQuoteExpiredMessageTranslation(){
    String language = ApexPages.currentPage().getParameters().get('language');
    quoteExpiredMessageTranslation.remove(language);
  }
 
  public void saveCombinedQuoteMessageTranslation(){
    if(combinedQuotationMessageTranslation == null){
      combinedQuotationMessageTranslation = new Map<String, String>(); 
    }
    combinedQuotationMessageTranslation.put(combinedQuotationLanguageTranslation, combinedQuotationTranslation);
    combinedQuotationTranslation = null;
  }
 
  public void deleteCombinedQuoteMessageTranslation(){
    String language = ApexPages.currentPage().getParameters().get('language');
    combinedQuotationMessageTranslation.remove(language);
  }
 
  public void deleteQuoteFieldTranslation(){
    String language = ApexPages.currentPage().getParameters().get('language');
    String translation = ApexPages.currentPage().getParameters().get('translation');
    quotationFieldsTranslated.get(language).remove(translation);
    if(quotationFieldsTranslated.get(language).isEmpty()){
      quotationFieldsTranslated.remove(language);
    }
    loadQuotationsFieldToTranslate();
  }

  public void deleteValueThumbnailInfo(){
    String index = ApexPages.currentPage().getParameters().get('index');
    String key = ApexPages.currentPage().getParameters().get('key');
    quoteLinkThumbnailInfo.get(key).remove(Integer.valueOf(index));
  }

  public void saveQuoteLinkThumbnailValue(){
    quoteLinkThumbnailInfo.get(optionSelectedQuoteLinkThumbnail).add(valueSelectedQuoteLinkThumbnail);
  }
 
  public void saveQuoteFieldTranslation(){
    if(quotationFieldsTranslated == null){
      quotationFieldsTranslated = new Map<String, Map<String, String>>();
    }
    if(!String.isEmpty(quotationFieldToTranslate) && !String.isEmpty(fieldLanguageTranslation) && !String.isEmpty(quotationFieldTranslation)){
      if(!quotationFieldsTranslated.containsKey(fieldLanguageTranslation)){
        quotationFieldsTranslated.put(fieldLanguageTranslation, new Map<String, String>());
      }
      quotationFieldsTranslated.get(fieldLanguageTranslation).put(quotationFieldToTranslate, quotationFieldTranslation);
      quotationFieldTranslation = null;
    }
    loadQuotationsFieldToTranslate();
  }
 
  public void loadQuotationsFieldToTranslate(){
    optionsQuotationFieldsToTranslate = new List<SelectOption>();
    Set<String> values = new Set<String>();
    List<Schema.PicklistEntry> picklistValues = Quotation_Products_Services__c.category__c.getDescribe().getPicklistValues();
    for (Schema.PicklistEntry pe: picklistValues) {
      if(!values.contains(pe.getValue())){
        //if(quotationFieldsTranslated == null || !quotationFieldsTranslated.containsKey(pe.getValue())){
          optionsQuotationFieldsToTranslate.add(new SelectOption(pe.getValue(), pe.getLabel()));
        //}
      }
      values.add(pe.getValue());
    }
    picklistValues = Course_Extra_Fee__c.Extra_fee_type__c.getDescribe().getPicklistValues();
    for (Schema.PicklistEntry pe: picklistValues) {
      if(!values.contains(pe.getValue())){
        //if(quotationFieldsTranslated == null || !quotationFieldsTranslated.containsKey(pe.getValue())){
          optionsQuotationFieldsToTranslate.add(new SelectOption(pe.getValue(), pe.getLabel()));
        //}
      }
      values.add(pe.getValue());
    }
    optionsQuotationFieldsToTranslate.add(new SelectOption('Custom', 'Custom'));
    optionsQuotationFieldsToTranslate.add(new SelectOption('Promotions', 'Promotions'));
  }
    
    public void loadData(){
        agencyGroup = getDetails(this.agencyGroupID);
        System.debug('==>agencyGroup: '+agencyGroup);
        if(agencyGroup.Destinations_offered__c != null) destinationsOffered = agencyGroup.Destinations_offered__c.split(';');
        if(agencyGroup.Main_customers_from__c != null) mainCustormesFrom = agencyGroup.Main_customers_from__c.split(';');
        if(agencyGroup.Main_customer_types__c != null) mainCustomersTypesList = agencyGroup.Main_customer_types__c.split(';');
        if(agencyGroup.Recruiting_students_for__c != null) recruitingStudentsList = agencyGroup.Recruiting_students_for__c.split(';');
        if(agencyGroup.Industry_Certificates__c != null) {
            industryCertificatesList = new Set<string>(agencyGroup.Industry_Certificates__c.split(';'));
            industry = agencyGroup.Industry_Certificates__c.replaceAll(';', ',');
        }
      
      if(!String.isEmpty(agencyGroup.Contact_Preferable_Language_Default__c)){
        defaultPreferableAgency = IPFunctions.getLanguage(agencyGroup.Contact_Preferable_Language_Default__c);
      }
        
        if(agencyGroup.Directors__c != null) {      
            list<string> l = agencyGroup.Directors__c.split(';');
            listDirectors.addAll(l);
        }
 
      if(agencyGroup.Restrict_Document_By_User__c != null)
        selectedUsers = new list<string>(agencyGroup.Restrict_Document_By_User__c.split(';'));
      else selectedUsers = new list<string>();
 
      if(agencyGroup.Restrict_Document_By_Category__c != null)
        selectedTypes = new list<string>(agencyGroup.Restrict_Document_By_Category__c.split(';'));
      else selectedTypes = new list<string>();
        
        agencyNames.put(selectedAgency, '');
        for(Account agency : agencyGroup.ChildAccounts)         
            agencyNames.put(agency.id, agency.Name);
        
        
        genSocialNetwork((String.isEmpty(agencyGroup.Social_network_url__c))?new list<string>():agencyGroup.Social_network_url__c.split(';'));
        
        genEstudentsEnroller((String.isEmpty(agencyGroup.num_students_enrolled__c))?new list<string>():agencyGroup.num_students_enrolled__c.split(';'));
        
        genVideos((String.isEmpty(agencyGroup.Videos__c))?new list<string>():agencyGroup.Videos__c.split(';'));
 
      if(!String.isEmpty(agencyGroup.Quote_Fields_Translation__c)){
        this.quotationFieldsTranslated = (Map<String, Map<String, String>>) JSON.deserialize(agencyGroup.Quote_Fields_Translation__c, Map<String, Map<String, String>>.class);
      }
      if(!String.isEmpty(agencyGroup.Quote_Expired_Alert__c)){
        this.quoteExpiredMessageTranslation = (Map<String, String>) JSON.deserialize(agencyGroup.Quote_Expired_Alert__c, Map<String, String>.class);
      }
      if(!String.isEmpty(agencyGroup.Combined_Quote_Alert__c)){
        this.combinedQuotationMessageTranslation = (Map<String, String>) JSON.deserialize(agencyGroup.Combined_Quote_Alert__c, Map<String, String>.class);
      }
      if(!String.isEmpty(agencyGroup.Quote_Link_Thumbnail_Info__c)){
        this.quoteLinkThumbnailInfo = (Map<String, List<String>>) JSON.deserialize(agencyGroup.Quote_Link_Thumbnail_Info__c, Map<String, List<String>>.class);
      }else{
        quoteLinkThumbnailInfo = new Map<String, List<String>>();
        quoteLinkThumbnailInfo.put('Title', new List<String>());
        quoteLinkThumbnailInfo.put('Description', new List<String>());
      }
      optionsQuoteLinkThumbnail = new List<SelectOption>();
      for(String key : quoteLinkThumbnailInfo.keySet()){
        optionsQuoteLinkThumbnail.add(new SelectOption(key, key));
      }
      optionsValuesQuoteLinkThumbnail = new List<SelectOption>();
      for(String item : IPFunctions.getQuotationLinkOptionsToShow()){
        optionsValuesQuoteLinkThumbnail.add(new SelectOption(item, item));
      }

      if(String.isEmpty(agencyGroup.New_Contact_Page_Header_Config__c)){
                this.fieldsHeaderNewPage = IPClasses.getDefaultNewContactPageHeaderFields();
            }else{
                this.fieldsHeaderNewPage = (List<IPClasses.NewContactPageHeaderFields>) JSON.deserialize(agencyGroup.New_Contact_Page_Header_Config__c, List<IPClasses.NewContactPageHeaderFields>.class);
 
        List<IPClasses.NewContactPageHeaderFields> allFieldsAvailable = IPClasses.getDefaultNewContactPageHeaderFields();
        for(IPClasses.NewContactPageHeaderFields field : allFieldsAvailable){
          if(this.fieldsHeaderNewPage.indexOf(field) < 0){
            this.fieldsHeaderNewPage.add(field);   
          }
        }
            }
    }
        
  private void genEstudentsEnroller(list<string> enroller){
    list<string> aux = new list<string>();
    for(string e: enroller){
        aux = e.split(':');
        estudentsEnroller.put(aux[0], aux[1]);
    }
    
  }
  
  private void genSocialNetwork(list<string> network){
    list<string> aux = new list<string>();
    for(string n: network){
      aux = n.split('>');
      socialNetwork.put(aux[0], aux[1]);
    }
    
  }
  
  private void genVideos(list<string> vhs){
    list<string> aux = new list<string>();
    for(string v: vhs){
      aux = v.split('>');
      videos.put(aux[0], aux[1]);      
    }    
  }
    private Account getDetails(String id){
        System.debug('==>id: '+id);
        return [Select Social_network__c, Social_network_url__c, Students_enrolled_in_the_last_12_months__c, num_students_enrolled__c, General_Description__c,  Directors__c,
                  NumberOfEmployees, Skype__c, Phone, BillingCountry, BillingPostalCode, BillingState, BillingStreet, BillingCity, Destination__c, 
                  Industry_Certificates__c, Recruiting_students_for__c, Main_customer_types__c, Main_customers_from__c, Destinations_offered__c, Name,
                Restrict_Document_By_Category__c, Restrict_Document_By_User__c, New_Contact_Page_Header_Config__c, Contact_Preferable_Language_Default__c,
                  CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name, Group_Type__c, Videos__c, Is_Lead_Source_Mandatory__c, Check_New_Lead_On_New_Cycle__c, Create_New_Cycle_Incoming_Leads__c, Email_Quotation_Option__c, Quote_Expiration_In_Days__c,
                  Registration_name__c, Year_established__c, BusinessEmail__c, Website, bgcolor__c, labelColor__c, Lead_Lost_Comment_Mandatory__c, Block_Checklist_Tasks_No_Owner__c, show_instalment_average__c,
                RDStation_Access_Token__c, RDStation_Refresh_Token__c, Default_Tab_Contact_New_Page__c, RDStation_Client_ID__c, RDStation_Client_Secret__c, RDStation_credential__c, RDStation_authorization__c, RDStation_Token_Expiration__c, Quote_Fields_Translation__c, Quote_Expired_Alert__c, Combined_Quote_Alert__c, Quote_Link_Thumbnail_Info__c,
                  (Select id, Name, View_Web_Leads__c from ChildAccounts)
                  from Account where id = :id];
  }
 
  public List<SelectOption> getOptionsDefaultPreferableLanguage(){
    return IPFunctions.getContactPreferableLanguages();
  }
  
  /* SELECT OPTIONS PICK LIST */
  public list<SelectOption> studentsEnrolledPL {
    get{
      if(studentsEnrolledPL == null){
        studentsEnrolledPL = new List<SelectOption>();        
        Schema.DescribeFieldResult F = Account.fields.Students_enrolled_in_the_last_12_months__c.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        for (Schema.PicklistEntry lst:P)
          studentsEnrolledPL.add(new SelectOption(lst.getValue(),lst.getLabel())); 
      }
        
      return studentsEnrolledPL;
    }
    set;
  }
  
  public list<SelectOption> socialNetworkPL {
    get{
      if(socialNetworkPL == null){
        socialNetworkPL = new List<SelectOption>();        
        Schema.DescribeFieldResult F = Account.fields.Social_network__c.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        for (Schema.PicklistEntry lst:P)
          socialNetworkPL.add(new SelectOption(lst.getValue(),lst.getLabel())); 
      }
        
      return socialNetworkPL;
    }
    set;
  }
  
    public list<SelectOption> videoPL {
    get{
      if(videoPL == null){
        videoPL = new List<SelectOption>();        
        videoPL.add(new SelectOption('YouTube','YouTube')); 
        videoPL.add(new SelectOption('Vimeo','Vimeo')); 
      }
        
      return videoPL;
    }
    set;
  }
 
   public list<SelectOption> numberDays {
    get{
      if(numberDays == null){
        numberDays = new List<SelectOption>();        
        for(integer i = 1; i <= 60; i++)
          numberDays.add(new SelectOption(i+'',i+'')); 
      }
        
      return numberDays;
    }
    set;
  }
  
  /* SELECT OPTIONS MULTIPICK LIST */
  public list<SelectOption> destinations {
    get{
      if(destinations == null){
        destinations = new List<SelectOption>();        
        Schema.DescribeFieldResult F = Account.fields.Destination__c.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        for (Schema.PicklistEntry lst:P)
          destinations.add(new SelectOption(lst.getValue(),lst.getLabel())); 
      }
        
      return destinations;
    }
    set;
  }
  
  public list<SelectOption> mainCustomersTypes {
    get{
      if(mainCustomersTypes == null){
        mainCustomersTypes = new List<SelectOption>();
        Schema.DescribeFieldResult F = Account.fields.Main_customer_types__c.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        for (Schema.PicklistEntry lst:P)
          mainCustomersTypes.add(new SelectOption(lst.getValue(),lst.getLabel()));
      }
        
      return mainCustomersTypes;
    }
    set;
   
  }
  
  public list<SelectOption> recruitingStudents {
    get{
      if(recruitingStudents == null){
        recruitingStudents = new List<SelectOption>();
        Schema.DescribeFieldResult F = Account.fields.Recruiting_students_for__c.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        for (Schema.PicklistEntry lst:P)
          recruitingStudents.add(new SelectOption(lst.getValue(),lst.getLabel()));
      }
        
      return recruitingStudents;
    }
    set;
    
  }
  
  public list<string> industryCertificates {
    get{
      if(industryCertificates == null){
        //industryCertificates = new List<SelectOption>();
        industryCertificates = new List<string>();
        Schema.DescribeFieldResult F = Account.fields.Industry_Certificates__c.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        for (Schema.PicklistEntry lst: P)
          //industryCertificates.add(new SelectOption(lst.getValue(),lst.getLabel()));
          if(!industryCertificatesList.contains(lst.getLabel())) industryCertificates.add(lst.getLabel());
        
      }
        
      return industryCertificates;
    }
    set;
    
  }
  
    /* ADD AND DELETE PROGRAMMES */
    public void addNumEnrolled(){
        if(program != null)
          estudentsEnroller.put(program, numStudents);
    }
    
    public void delNumEnrolled(){
    if(programKey != null)
      estudentsEnroller.remove(programKey);
  }
  
  public void addSocialNetwork(){
    if(networkName != null)
      socialNetwork.put(networkName, networkUrl);
  }
  
  
  public void delSocialNetwork(){
    if(networkKey != null)
      socialNetwork.remove(networkKey);
  }
  
  public void addDirectors(){
     if(directorsName != null)
      listDirectors.add(directorsName);
  }
  
  public void delDirectors(){
    if(directorsKey != null)
      listDirectors.remove(directorsKey);
  }
  
  /*public void addVideo(){
    if(videoHostName != null)
      videos.put(videoKey, videoHostName);
      if(!videos.containsKey(videoKey))
        videos.put(videoKey, new list<string>{videoHostName});
      else        
        videos.get(videoKey).add(videoHostName);
  }*/
  
  public void addVideo(){
    if(videoHostName != null)
      videos.put(videoKey, videoHostName);
  }
  
  public void delVideo(){
    if(videoListKey != null)
      videos.remove(videoListKey);
  }
    
    /*--- EDIT AND CANCEL ---*/
    public boolean edit {get{if(edit== null) edit = false; return edit;}set;}
    
    public void setEdit(){
        edit = true;
    }
    
    private string generateEnrolleds(){
        list<string> aux = new list<string>();
        for (String key : estudentsEnroller.keyset()) {
            aux.add(key+':'+estudentsEnroller.get(key));
    }
    return String.join(aux,';');
    }
    
    private string generateSocialNetworks(){
    list<string> aux = new list<string>();
    for (String key : socialNetwork.keyset()) {
      aux.add(key+'>'+socialNetwork.get(key));
    }
    return String.join(aux,';');
  }
  
    private string generateDirectors(){
    list<string> l = new list<string>();
    l.addAll(listDirectors);
    return String.join(l,';');
  }
  
  private string generateVideos(){
    list<string> aux = new list<string>();
      
    for (String key : videos.keyset()) {
        if(videos.get(key) == 'Vimeo'){
            String vkey = vimeoThumbnail(key);
            aux.add(key+'>'+videos.get(key)+':'+vkey);
        }   else if(videos.get(key) == 'Youtube') aux.add(key+'>'+videos.get(key));
    }
    return String.join(aux,';');
  }
    
    public void saveGroup(){
        System.debug('>>>>>>>>>industry: '+industry);
        
        //string param = ApexPages.currentPage().getParameters().get('industry');
        
        agencyGroup.Destinations_offered__c = String.join(destinationsOffered, ';');
        agencyGroup.Main_customers_from__c = String.join(mainCustormesFrom, ';');
        agencyGroup.Main_customer_types__c = String.join(mainCustomersTypesList, ';');
        agencyGroup.Recruiting_students_for__c = String.join(recruitingStudentsList, ';');
        //agencyGroup.Industry_Certificates__c = String.join(industryCertificatesList, ';');
 
    agencyGroup.Restrict_Document_By_User__c = String.join(selectedUsers, ';');
    agencyGroup.Restrict_Document_By_Category__c = String.join(selectedTypes, ';');
        
        agencyGroup.Industry_Certificates__c = industry.replaceAll(',', ';');
        
        //agencyGroup.Industry_Certificates__c = industryCertificatesList;
        agencyGroup.num_students_enrolled__c = generateEnrolleds();
        agencyGroup.Social_network_url__c = generateSocialNetworks();
        agencyGroup.Videos__c = generateVideos();
        agencyGroup.Directors__c = generateDirectors();
 
    if(quotationFieldsTranslated != null){
      agencyGroup.Quote_Fields_Translation__c = JSON.serialize(this.quotationFieldsTranslated);
    }
    if(quoteExpiredMessageTranslation != null){
      agencyGroup.Quote_Expired_Alert__c = JSON.serialize(this.quoteExpiredMessageTranslation);
    }
    if(combinedQuotationMessageTranslation != null){
      agencyGroup.Combined_Quote_Alert__c = JSON.serialize(this.combinedQuotationMessageTranslation);
    }
    agencyGroup.Quote_Link_Thumbnail_Info__c = JSON.serialize(this.quoteLinkThumbnailInfo);
        agencyGroup.New_Contact_Page_Header_Config__c = JSON.serialize(this.fieldsHeaderNewPage);
        
        update agencyGroup;
        
        List<Account> childagencies = new List<Account>();
        for(Account agency : agencyGroup.ChildAccounts){
            if(selectedAgency == agency.id && agency.View_Web_Leads__c)
                break;
            else if(selectedAgency == agency.id && !agency.View_Web_Leads__c){
                agency.View_Web_Leads__c = true;
                childagencies.add(agency);
            } else if(selectedAgency != agency.id && agency.View_Web_Leads__c){
                agency.View_Web_Leads__c = false;
                childagencies.add(agency);
            }
        }
        
        update childAgencies;
        
        loadData();
        edit = false;
        
        //PageReference reload = new PageReference('apex/Agency_Group_Details?id='+agencyGroup.Id);
        //reload.setRedirect(false);
        //return reload;        
        
    }
    
    public void cancel(){
        loadData();
        edit = false;           
    }
    
    public string vimeoThumbnail(string vimeoCode){
    HttpRequest req = new HttpRequest();
    HttpResponse res = new HttpResponse();
    Http http = new Http();
    
    req.setEndpoint('https://vimeo.com/api/v2/video/'+ vimeoCode +'.json');
    req.setMethod('GET');
    
    List<jsonVimeo> listVimeo; 
    String vk = '';
    
    try {
        res = http.send(req);
        system.debug('@#$ res.getBody(): ' + res.getBody());
        
        listVimeo = ( List<jsonVimeo> ) JSON.deserialize( res.getBody(), List<jsonVimeo>.class );
        system.debug('list object: ' + listVimeo.get(0).thumbnail_small);
        
        vk = vimeoKey(listVimeo.get(0).thumbnail_small);
  
    } catch(System.CalloutException e) {
      System.debug('Callout error: '+ e);
      System.debug(res.toString());
    }
    
    return vk;
  }
  
  private string vimeoKey(string thumbnail){
    string key;
    list<string> aux = new list<string>();
    if(thumbnail != null){
        aux = thumbnail.split('video');
        aux = aux.get(1).split('/');
        aux = aux.get(1).split('_');
        key = aux.get(0);
        System.debug('--aux: '+aux);
        System.debug('--key: '+key);
    }
    return key; 
  }
  
  private class jsonVimeo{
    public string id, title, description, url, upload_date, mobile_url, thumbnail_small, thumbnail_medium, thumbnail_large, user_id, user_name, user_url, user_portrait_small,
      user_portrait_medium, user_portrait_large, user_portrait_huge, duration, width, height, tags, embed_privacy;
  }
  
  public String selectedAgency {
    get{
        if(selectedAgency == null)
            for(Account agency : agencyGroup.childAccounts)
                if(agency.View_Web_Leads__c)
                    selectedAgency = agency.id;
        return selectedAgency;
    }
    set;
  }
 
  public List<SelectOption> getOptionsShowFieldHeaderNewContactPage(){
        return IPClasses.getOptionsShowFieldHeaderNewContactPage();
    }
  private List<SelectOption> agencies;
  public List<SelectOption> getAgencies(){
    
    if(agencies == null){
        agencies = new List<SelectOption>();
        agencies.add(new SelectOption('', '-- Select --'));
        for(Account agency : agencyGroup.childAccounts){
            agencies.add(new SelectOption(agency.id, agency.Name));
        }   
    }
    return agencies;
    
  }
 
  public groupAgencies ga{
        get{
            if(ga == null)
                ga = new groupAgencies(groupLink);
            return ga;
        }
        set;
    }
 
  public without sharing class groupAgencies{
    public groupAgencies(string gpId){
            groupId = gpId;
        }
        private string groupId;
    
    public map<string, string> mapUsers{get; set;}
    public List<SelectOption> AgencyUsers {
      get {
        if(AgencyUsers == null){
          AgencyUsers = new List<SelectOption>();
          mapUsers = new map<string, string>();
          for(User ac:[SELECT id, name, contact.account.Name FROM user where isActive = true and contact.account.Parent.Global_Link__c = :groupId order by contact.account.name, name]){
            AgencyUsers.add(new SelectOption(ac.id,ac.contact.account.Name + ' - '+ ac.name));
            mapUsers.put(ac.id, ac.contact.account.Name + ' - '+ ac.name);
          }
        }
        return AgencyUsers;
      }set;
    }
  }
 
  public list<string> selectedUsers{
        get{
            if(selectedUsers == null)
                selectedUsers = new list<string>();
            return selectedUsers;
        }
        set;
    }
 
  public list<string> selectedTypes{
        get{
            if(selectedTypes == null)
                selectedTypes = new list<string>();
            return selectedTypes;
        }
        set;
    }
 
 
    public List<SelectOption> categoriesList{
    get{
      if(categoriesList == null){
        categoriesList = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Client_Document__c.Document_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
          categoriesList.add(new SelectOption(f.getValue(), f.getLabel()));
      }
      return categoriesList;
    }
    set;
  }
 
}