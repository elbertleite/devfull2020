@isTest
private class instalment_payment_history_test {
	public static Account school {get;set;}
	public static Account agency {get;set;}
	public static Contact emp {get;set;}
	public static Contact client {get;set;}
	public static User portalUser {get{
    if (null == portalUser) {
      portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
    }
    return portalUser;
  }set;}
	public static Account campus {get;set;}
	public static Course__c course {get;set;}
	public static Campus_Course__c campusCourse {get;set;}
	public static client_course__c clientCourseBooking {get;set;}
	public static client_course__c clientCourse {get;set;}
	public static client_course__c clientCourse2 {get;set;}
	@testSetup static void setup() {
		TestFactory tf = new TestFactory();

		school = tf.createSchool();
		agency = tf.createAgency();
		emp = tf.createEmployee(agency);
		portalUser = tf.createPortalUser(emp);
		campus = tf.createCampus(school, agency);
		course = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);

		Back_Office_Control__c bo = new Back_Office_Control__c(Agency__c = agency.id, Country__c = 'Australia', Backoffice__c = agency.id, Services__c = 'PDS_PCS_PFS_Chase');
		insert bo;

		system.runAs(portalUser){
			client = tf.createClient(agency);
			clientCourseBooking = tf.createBooking(client);
			clientCourse = tf.createClientCourse(client, school, campus, course, campusCourse, clientCourseBooking);
			tf.createClientCourseFees(clientCourse, false);
			List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(clientCourse);

			//TO REQUEST PDS
			instalments[0].Received_By_Agency__c = agency.id;
			instalments[0].Received_Date__c =  system.today();
			instalments[0].Paid_To_School_On__c =  system.today();
			instalments[0].isPDS__c = true;

			instalments[1].isPDS__c = true;
			instalments[1].Received_By_Agency__c = agency.id;
			instalments[1].Paid_To_School_On__c =  system.today();
			instalments[1].Received_Date__c =  system.today();

			update instalments;
		}
  	}
	@isTest static void test_method_one() {
		client_course_instalment__c instalment = [Select Id from client_course_instalment__c where Received_Date__c != null limit 1];
		instalment_payment_history test = new instalment_payment_history(new ApexPages.StandardController(instalment));
		test.getHistory();
	}
	
	
}