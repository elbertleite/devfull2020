public with sharing class client_course_cancel_requests {
	
	public String requests {get;set;}
	public String SEPARATOR_FILE {get{return Fileupload.SEPARATOR_FILE;}set;} 
	public String SEPARATOR_URL {get{return Fileupload.SEPARATOR_URL;}set;} 
	public String notesSplit {get{return client_course_cancel.notesSplit;}set;} 
	public String noteFld {get{return client_course_cancel.noteFld;}set;} 
	public String typeMissDocs {get{return client_course_cancel.TYPE_MISS_DOCS;}set;}
	public String typeCourseTransferred {get{return client_course_cancel.TYPE_COURSE_TRANSFERRED;}set;}
	public String typeCourseRefunded {get{return client_course_cancel.TYPE_COURSE_REFUNDED;}set;}
	public String typeReleaseLetterReq {get{return client_course_cancel.TYPE_RELEASE_LETTER_REQUESTED;}set;}

	public String agencyOptions {get;set;}

	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}
	 
	public client_course_cancel_requests() {
		
		findGroups();
		findAgencies();
		findSchools();

		if(selGroup!=null)
			requests = findRequests('pending', selGroup, selAgency, seldept, 'all', '');
		else{
			requests = JSON.serialize(new client_course_cancel.requestsWrapper());
		}

		List<IPFunctions.opWrapper> result = new List<IPFunctions.opWrapper>();
		for(Account a : [Select Name, Id from Account A where RecordType.Name = 'Agency' order by Name])
			result.add(new IPFunctions.opWrapper(a.Id, a.Name));
		// result.add(0,new IPFunctions.opWrapper('none', '-- Select Agency -- '));
		agencyOptions = JSON.serialize(result);



	}

	@RemoteAction
	public static String checkInstalments(list<String> courses){
		return client_course_cancel.checkInstalments(courses);
	}
	
	@RemoteAction
	public static String cancelPayment(list<String> ids){
		return client_course_cancel.cancelPayment(ids);
	}

	private static User user {get;set;}
	public static String fAgencies {get;set;}
	public static String fGroup {get;set;}
	public static String fdept {get;set;}
	public static String fSchools {get;set;}
	public static String selGroup {get;set;}
	public static list<String> selAgency {get;set;}
	public static String seldept {get;set;}
	private static void findGroups(){
		user = [Select Id, Aditional_Agency_Managment__c, Contact.Department__c, Contact.Account.ParentId, Contact.Account.Global_Link__c, Contact.AccountId FROM User where Id = :UserInfo.getUserId()];

		list<IPFunctions.opWrapper> lgroup = new list<IPFunctions.opWrapper>();
		String sql = 'SELECT Client__r.Owner__r.Contact.Account.ParentId gpId, Client__r.Owner__r.Contact.Account.Parent.Name gpName FROM client_course__c WHERE isCancelRequest__c = true and Cancel_Request_Confirmed_to_Client__c = false';

		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
		 	sql+= ' AND Client__r.Owner__r.Contact.Account.ParentId = \'' + user.Contact.Account.ParentId + '\'';
		else
			 sql+= ' AND Client__r.Owner__r.Contact.Account.Global_Link__c = \'' + user.Contact.Account.Global_Link__c + '\'';

		sql+= ' AND Cancel_Course_Status__c != \'' + client_course_cancel.TYPE_DRAFT+  '\' AND isCancelled__c = false ';

		sql += 'group by Client__r.Owner__r.Contact.Account.ParentId, Client__r.Owner__r.Contact.Account.Parent.Name order by Client__r.Owner__r.Contact.Account.Parent.Name';

		for(AggregateResult ar : Database.query(sql)){
			lgroup.add(new IPFunctions.opWrapper((String) ar.get('gpId'), (String) ar.get('gpName')));
			if((String) ar.get('gpId') == user.Contact.Account.ParentId)
				selGroup = (String) ar.get('gpId');
		}//end for

			system.debug('sql group==> ' + sql);	
		

		if(selGroup == null && lgroup.size()>0)
			selGroup = lgroup[0].opId;

		fGroup = JSON.serialize(lgroup);
	}
	private static void findAgencies(){
		if(user == null) 
			user = [Select Id, Aditional_Agency_Managment__c, Contact.Department__c, Contact.Account.ParentId, Contact.Account.Global_Link__c, Contact.AccountId FROM User where Id = :UserInfo.getUserId()];

			list<IPFunctions.opWrapper> lagency = new list<IPFunctions.opWrapper>();
		if(selGroup != null){
			String sql = 'SELECT Client__r.Owner__r.Contact.AccountId gpId, Client__r.Owner__r.Contact.Account.Name gpName FROM client_course__c WHERE isCancelRequest__c = true and Cancel_Request_Confirmed_to_Client__c = false';

			system.debug('user.Aditional_Agency_Managment__c==> ' + user.Aditional_Agency_Managment__c);	
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){
				list<string> agManagment = new list<string>{user.Contact.AccountId};
				 if(user.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(user.Aditional_Agency_Managment__c)){
						agManagment.add(ac.id);
					}
				}
				System.debug('==>agManagment: '+agManagment);
				sql+= ' AND Client__r.Owner__r.Contact.AccountId in :agManagment ';
				//sql+= ' AND Client__r.Owner__r.Contact.AccountId = \'' + user.Contact.AccountId + '\'';
			}
			else{

				sql+= ' AND Client__r.Owner__r.Contact.Account.ParentId = \'' + selGroup + '\'';
				lagency.add(new IPFunctions.opWrapper('all', '-- All Agencies --'));
			}
		
		
			sql+= ' AND Cancel_Course_Status__c != \'' + client_course_cancel.TYPE_DRAFT+  '\' AND isCancelled__c = false ';

		
			sql += 'group by Client__r.Owner__r.Contact.AccountId, Client__r.Owner__r.Contact.Account.Name order by Client__r.Owner__r.Contact.Account.Name';

			system.debug('sql agency==> ' + sql);	


			for(AggregateResult ar : Database.query(sql)){
				lagency.add(new IPFunctions.opWrapper((String) ar.get('gpId'), (String) ar.get('gpName')));
				// if((String) ar.get('gpId') == user.Contact.AccountId)
				// 	selAgency = (String) ar.get('gpId');
			}//end for

		}
		fAgencies = JSON.serialize(lagency);
	}

	public String opPayment {get{
		if(opPayment == null){
			list<IPFunctions.opWrapper> result = new list<IPFunctions.opWrapper>();
		
			Schema.DescribeFieldResult fieldResult = client_course__c.Deposit_Type__c.getDescribe();
			for( Schema.PicklistEntry f : fieldResult.getPicklistValues())
				result.add(new IPFunctions.opWrapper(f.getValue(), f.getLabel()));

			opPayment = JSON.serialize(result);
		} return opPayment;
	}set;}

	
	private static void findSchools(){

		list<IPFunctions.opWrapper> lSchool = new list<IPFunctions.opWrapper>();

		if(selGroup != null){
			
			String sql = 'SELECT School_Id__c sch FROM client_course__c WHERE ';
		
			sql+= ' Client__r.Owner__r.Contact.Account.ParentId = \'' + selGroup + '\'';

			sql+= ' AND isCancelRequest__c = true and Cancel_Request_Confirmed_to_Client__c = false AND Cancel_Course_Status__c != \'' + client_course_cancel.TYPE_DRAFT+  '\' AND isCancelled__c = false ';

			sql += 'group by School_Id__c';
				
			list<String> ids = new list<String>();
			for(AggregateResult ar : Database.query(sql)){
				ids.add((String) ar.get('sch'));
			}//end for

			nSharing ns = new nSharing();
			for(Account ac : ns.findSchools(ids))
				lSchool.add(new IPFunctions.opWrapper(ac.Id, ac.Name));
			
			lSchool.add(0,new IPFunctions.opWrapper('all', '-- All --'));
		}

		fSchools = JSON.serialize(lSchool);
		
	}

	@RemoteAction
	public static String changeGroup(String groupId){
		selGroup = groupId;
		findSchools();

		return fAgencies;
	}
	@RemoteAction
	public static String changeAgency(String agencyId){

		return '';
	}
	@RemoteAction
	public static String changeDepartment(String agencyId, String deptId){
		return '';
	}

	@RemoteAction
	public static String inputOutcome(String cId, String outcomeType, String rlGrant, String note, String tab, String groupId, list<String> agencyId, String deptId, String schId, String clientNm){
		String granted;
		if(rlGrant == '1') granted = 'Granted';
		else if(rlGrant == '0'){
			granted = 'Not Granted';
			outcomeType = 'cancelRequest';
		} 
		
		client_course__c upCourse = new client_course__c(Id = cId, Release_Letter_Grant__c = granted);
		Update upCourse;
		
		if(outcomeType == 'cancelRequest')
			return undoRequest(cId, note, tab, groupId, agencyId, deptId, schId, clientNm);

		
		else if(outcomeType == 'cancelCourse')
			return cancelCourse(cId, note, tab, groupId, agencyId, deptId, schId, clientNm);

		
		else if(outcomeType == 'refundCourse')
			return requestRefund(cId, null, note, tab, groupId, agencyId, deptId, schId, clientNm, 'releaseLetter');

		else return null;
	}	

	@RemoteAction
	public static String addNote(String cId, String note, String tab){
		return client_course_cancel.addNote(cId, note);
	}

	@RemoteAction
	public static String cancelCourse(String cId, String note, String tab, String groupId, list<String> agencyId, String deptId, String schId, String clientNm){
		String result = client_course_cancel.cancelCourse(cId,  note, 'cancel', null, null); 
		system.debug('===>>' + result);
		if(result == 'success'){
			return findRequests(tab, groupId, agencyId, deptId, schId, clientNm);
		}
		else
			return result;

		return null;
	}

	@RemoteAction
	public static String requestCredit(String cId, String creditAmount, String note, String tab, String groupId, list<String> agencyId, String deptId, String schId, String clientNm){
		String result = client_course_cancel.requestCredit(cId, creditAmount, note); 
		system.debug('===>>' + result);
		if(result == 'success'){
			return findRequests(tab, groupId, agencyId, deptId, schId, clientNm);
		}
		else
			return result;

		return null;
	}

	@RemoteAction
	public static String requestRefund(String cId, String creditAmount, String note, String tab, String groupId, list<String> agencyId, String deptId, String schId, String clientNm, String statusType){
		String courseStatus;
		if(statusType == 'request')
			courseStatus = client_course_cancel.TYPE_COURSE_REFUND_REQUESTED;
		else if(statusType == 'releaseLetter')
			courseStatus = client_course_cancel.TYPE_COURSE_REFUND_CONFIRMED;

		String result = client_course_cancel.requestRefund(cId, creditAmount, note, courseStatus); 
		system.debug('===>>' + result);
		if(result == 'success'){
			return findRequests(tab, groupId, agencyId, deptId, schId, clientNm);
		}
		else
			return result;

		return null;
	}

	@RemoteAction
	public static String undoRequest(String cId, String note, String tab, String groupId, list<String> agencyId, String deptId, String schId, String clientNm){
		String result = client_course_cancel.undoRequest(cId, note); 
		system.debug('===>>' + result);
		if(result == 'success'){
			return findRequests(tab, groupId, agencyId, deptId, schId, clientNm);
		}
		else
			return result;
	}

	@RemoteAction
	public static String addMissingDocs(String cId, String note, String tab, String groupId, list<String> agencyId, String deptId, String schId, String clientNm){
		String result = client_course_cancel.missingDocs(cId, note);
		system.debug('===>>' + result);
		if(result == 'success'){
			return findRequests(tab, groupId, agencyId, deptId, schId, clientNm);
		}
		else
			return result;
	}

	@RemoteAction
	public static String findRequests(String tab, String groupId, list<String> agencyId, String deptId, String schId, String clientNm){
		if(user == null) 
			user = [Select Id, Aditional_Agency_Managment__c, Contact.Account.Global_Link__c, Contact.AccountId, Contact.Account.ParentId FROM User where Id = :UserInfo.getUserId()];

		list<client_course_cancel.courseInfo> result = new list<client_course_cancel.courseInfo>();

		String sql ='SELECT School_Id__c, Cancel_Course_Status__c, Course_Cancellation_Service__c, Cancel_Notes__c, Credit_Reason__c, previewLink__c, Enrolment_Activities__c, Credit_Description__c, Client__c, Client__r.Name, Client__r.Age__c, Enroled_by_Agency__r.Name, Credit_Value__c,	Enroled_by_User__r.Name, Client__r.Owner__r.Name, Client__r.Owner__r.Contact.Account.Name, createdDate, (Select Id, isPackage__c, course_package__c, School_Name__c, Campus_Name__c, Course_Name__c, Start_Date__c, End_Date__c From client_courses__r order by Start_Date__c) FROM client_course__c WHERE ';

		String sWhere;

		// if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
		// 	sWhere = 'Client__r.Owner__r.Contact.Account.ParentId = \'' + user.Contact.Account.ParentId + '\'';
		// else
		// 	sWhere = 'Client__r.Owner__r.Contact.Account.ParentId = \'' + groupId + '\'';

		// if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){
		// 	list<string> agManagment = new list<string>{user.Contact.AccountId};
		// 		if(user.Aditional_Agency_Managment__c != null){
		// 		for(Account ac:ipFunctions.listAgencyManagment(user.Aditional_Agency_Managment__c)){
		// 			agManagment.add(ac.id);
		// 		}
		// 	}
		// 	sWhere += ' AND Client__r.Owner__r.Contact.AccountId in :agManagment ';
		// }
		// else
		// 	sWhere += ' AND Client__r.Owner__r.Contact.AccountId = \'' + agencyId + '\'';

		system.debug('sel agencies==>' + agencyId);
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){
			if(agencyId == null || agencyId.size()==0)
				agencyId = new list<string>{user.Contact.AccountId};
		}
		if(agencyId == null || agencyId.size()==0 || agencyId[0] == 'all')
			sWhere = ' Client__r.Owner__r.Contact.Account.ParentId = \'' + user.Contact.Account.ParentId + '\'';
		else
			sWhere = ' Client__r.Owner__r.Contact.AccountId  IN :agencyId ';


		if(schId != 'all' && schId != '')
			sWhere += ' AND School_Id__c = \'' + schId + '\'';
		
		if(clientNm.trim().length()>0)
			sWhere += ' AND Client__r.Name LIKE \'%' + clientNm.trim() + '%\'';

		sWhere += ' AND isCancelRequest__c = true';

		sql += sWhere;

		String pend = ' AND (Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_REQUESTED_ADM + '\' )'; //OR Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_MISS_DOCS + '\'
		
		String sch = ' AND Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_REQUESTED_SCH + '\'';
		
		String transfer = ' AND (Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_COURSE_TRANSFER_REQUESTED + '\' OR (Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_COURSE_TRANSFERRED + '\' AND Cancel_Request_Confirmed_to_Client__c = false))';
		
		String refund = ' AND (Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_COURSE_REFUND_REQUESTED + '\' OR Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_COURSE_REFUND_CONFIRMED + '\' OR (Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_COURSE_REFUNDED + '\' AND Cancel_Request_Confirmed_to_Client__c = false))';

		String releaseLetter = ' AND (Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_RELEASE_LETTER_REQUESTED + '\' )'; //OR Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_MISS_DOCS + '\'

		
		if(tab == 'pending')
			sql += pend;
		else if(tab == 'reqSchool')
			sql += sch;
		else if(tab == 'transfer')
			sql += transfer;
		else if(tab == 'refund')
			sql += refund;
		else if(tab == 'releaseLetter')
			sql += releaseLetter;

		sql +=' ORDER BY createdDate ';

		system.debug('sql==>' + sql); 
		for(client_course__c c : Database.query(sql)){
			result.add(new client_course_cancel.courseInfo(c, IPFunctions.loadNotes(c.Cancel_Notes__c), IPFunctions.loadActivities(c.Enrolment_Activities__c), user.Contact.Account.Global_Link__c, true));
		}//end for

		Integer totPending;
		Integer totSchool;
		Integer totTransfer;
		Integer totRefund;
		Integer totReleaseLetter;
		
		//Tot Pending
		system.debug('tot==>' + 'SELECT count(Id) total FROM client_course__c WHERE ' + sWhere + ' ' + pend);
		for(AggregateResult ar : Database.query('SELECT count(Id) total FROM client_course__c WHERE ' + sWhere + ' ' + pend)){
			totPending = Integer.valueOf(ar.get('total'));
		}
		
		//Tot School
		system.debug('tot==>' + 'SELECT count(Id) total FROM client_course__c WHERE ' + sWhere + ' ' + sch);
		for(AggregateResult ar : Database.query('SELECT count(Id) total FROM client_course__c WHERE ' + sWhere + ' ' + sch)){
			totSchool = Integer.valueOf(ar.get('total'));
		}

		//Tot Transfer
		system.debug('tot==>' + 'SELECT count(Id) total FROM client_course__c WHERE ' + sWhere + ' ' + transfer);
		for(AggregateResult ar : Database.query('SELECT count(Id) total FROM client_course__c WHERE ' + sWhere + ' ' + transfer)){
			totTransfer = Integer.valueOf(ar.get('total'));
		}
		
		//Tot Refund
		system.debug('tot==>' + 'SELECT count(Id) total FROM client_course__c WHERE ' + sWhere + ' ' + refund);
		for(AggregateResult ar : Database.query('SELECT count(Id) total FROM client_course__c WHERE ' + sWhere + ' ' + refund)){
			totRefund = Integer.valueOf(ar.get('total'));
		}

		//Tot Release Letter
		system.debug('tot==>' + 'SELECT count(Id) total FROM client_course__c WHERE ' + sWhere + ' ' + releaseLetter);
		for(AggregateResult ar : Database.query('SELECT count(Id) total FROM client_course__c WHERE ' + sWhere + ' ' + releaseLetter)){
			totReleaseLetter = Integer.valueOf(ar.get('total'));
		}

		return JSON.serialize(new client_course_cancel.requestsWrapper(result, totPending, totSchool, totTransfer, totRefund, totReleaseLetter));
	}
	

	@RemoteAction
	public static String schoolRefund(String cId, String amount, String paidDate, String agencyId, String note, String tab, String groupId, list<String> fagencyId, String deptId, String schId, String clientNm){
		system.debug('cId====>' + cId);
		system.debug('amount====>' + amount);
		system.debug('paidDate====>' + paidDate);

		Savepoint sp;
		
		try{
			client_course_cancel.refundDetails dt = new client_course_cancel.refundDetails(cId, 'school', Decimal.valueOf(amount.replaceAll('[^.\\d]','')), agencyId.split('string:')[1], date.valueOf(paidDate), note);

			//Update Cancel Request
			client_course_cancel.cancelCourse(cId, note, 'refund', null, dt);

			return findRequests(tab, groupId, fagencyId, deptId, schId, clientNm);

		}catch(exception e){
			Database.rollback(sp);
			system.debug('error line==' + e.getLineNumber());
			return 'failed';
		}
	}

	@RemoteAction
	public static String agencyRefund(String cId, String agencyId, String schoolAmount, String schoolDate, list<String> commissions, String adminFee, String totalAmount, String note, String tab, String groupId, list<String> fagencyId, String deptId, String schId, String clientNm, String refundType, String typePayment, String paidOn, String clientId){
		
		system.debug('cId====>' + cId);
		system.debug('agencyId====>' + agencyId);
		system.debug('schoolAmount====>' + schoolAmount);
		system.debug('schoolDate====>' + schoolDate);
		system.debug('commissions====>' + commissions);
		system.debug('adminFee====>' + adminFee);
		system.debug('totalAmount====>' + totalAmount);
		system.debug('note====>' + note);
		system.debug('refundType====>' + refundType);
		system.debug('typePayment====>' + typePayment);
		system.debug('paidOn====>' + paidOn);

		Savepoint sp;
		
		try{
			map<String, Decimal> coursesComm = new map<String, Decimal>();
			for(String comm : commissions)
				coursesComm.put(comm.split(';;')[0], Decimal.valueOf(comm.split(';;')[1].replaceAll('[^.\\d]','')) );

			client_course_cancel.refundDetails dt = new client_course_cancel.refundDetails(cId, 'agency', agencyId.split('string:')[1], Decimal.valueOf(schoolAmount.replaceAll('[^.\\d]','')), date.valueOf(schoolDate), coursesComm, Decimal.valueOf(adminFee.replaceAll('[^.\\d]','')), Decimal.valueOf(totalAmount.replaceAll('[^.\\d]','')), date.valueOf(system.now()), note, refundType, typePayment.split('string:')[1], date.valueOf(paidOn));
			
			//Update Cancel Request
			String result = client_course_cancel.cancelCourse(cId, note, 'refund', null, dt);

			if(result == 'success'){
				return findRequests(tab, groupId, fagencyId, deptId, schId, clientNm);
			}
			else
				return result;
		}catch(exception e){
			Database.rollback(sp);
			system.debug('error line==' + e.getLineNumber());
			return 'failed';
		}
	}


	@RemoteAction
	public static String confirmCourseTransfer(String cId, String creditAmount, String note, String tab, String groupId, list<String> agencyId, String deptId, String schId, String clientNm){

		Savepoint sp;
		
		try{
			Decimal credtAm;
			system.debug('creditAmount===>' + creditAmount);
			credtAm = Decimal.valueOf(creditAmount.replaceAll('[^.\\d]',''));
			system.debug('credtAm===>' + credtAm);

			nSharing ns = new nSharing();
			//Create School Credit
			client_course__c course = ns.findCourse(cId);
			system.debug('course===>' + course.Campus_Course__r.Campus__r.Parent.ParentId);
			
			client_course__c credit;
			try{
				credit = ns.findCredit(course.Campus_Course__r.Campus__r.Parent.ParentId, course.Client__c);

			}catch(exception e){
				credit = new client_course__c(Client__c = course.Client__c, isCourseCredit__c = true, Credit_Valid_For_School__c = course.Campus_Course__r.Campus__r.Parent.ParentId, School_Name__c = course.Campus_Course__r.Campus__r.Parent.Parent.Name, Credit_Value__c = 0, Credit_Available__c = 0);
			}

			credit.Credit_Value__c += credtAm;
			credit.Credit_Available__c += credtAm;
			upsert credit;


			//Update Cancel Request
			client_course_cancel.cancelCourse(cId, note, 'transfer', credtAm, null);

			return findRequests(tab, groupId, agencyId, deptId, schId, clientNm);

		}catch(exception e){
			Database.rollback(sp);
			system.debug('error line==' + e.getLineNumber());
			return 'failed';
		}

		
	}

	public without sharing class nSharing{

		public client_course__c findCourse(String req){
			return [SELECT Client__c, Campus_Course__r.Campus__r.Parent.ParentId, Campus_Course__r.Campus__r.Parent.Parent.Name FROM client_course__c WHERE course_cancel_request__c = :req limit 1];
		}
		public client_course__c findCredit(String schId, String clientId){
			return [SELECT Id, Credit_Available__c, Credit_Value__c, Credit_Courses__c FROM client_course__c WHERE Client__c =:clientId and Credit_Valid_For_School__c = :schId and isCourseCredit__c = true limit 1];
		}
		public list<Account> findSchools(list<String> ids){
			return [SELECT Id, Name FROM Account WHERE Id in :ids order by Name];
		}
	}

}