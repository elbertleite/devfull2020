public with sharing class payment_plans_client {

	string ct;
	public payment_plans_client(ApexPages.StandardController ctrl) {
		ct = ctrl.getId();
		retrievePaymentPlans();
	}

	public map<string,list<planItem>> listPaymentPlans{get; set;}

	private void retrievePaymentPlans(){
		listPaymentPlans = new map<string,list<planItem>>();
		planItem item;
		for (Payment_Plan__c pp:[Select id, Client__r.name, Value__c, Instalments__c, Instalments__r.Notes__c, Value_Remaining__c, Total_Paid__c, Client__c, Value_Paid__c, Currency_Code__c, 
											Value_in_Local_Currency__c, Description__c, Total_Available__c, Plan_Type__c, Loan_Approved_On__c, loan_status__c,
											Reminder_Date__c from Payment_Plan__c where Instalments__c = null and client__c = :ct  order by createdDate]){
			item = new planItem();
			item.itemPlan = pp;
			if(!listPaymentPlans.containsKey(pp.Plan_Type__c))
				listPaymentPlans.put(pp.Plan_Type__c,new list<planItem>{item});
			else listPaymentPlans.get(pp.Plan_Type__c).add(item);
		}
	}


	

	public class planItem{
		public Payment_Plan__c itemPlan{get{if(itemPlan == null) itemPlan = new Payment_Plan__c(); return itemPlan;} set;} 
	}

	
}