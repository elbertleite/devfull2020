global with sharing class EmailFromS3Controller {
	
	public integer qttEmails{get; set;}
	
	public EmailFromS3Controller(){
	}
	
	public static List<EmailDetails> emailDetails {get;set;}
	public static String emailBody {get;set;}	
	public EmailDetails downloadEmail {get;set;}	
	public string clientId{get; set;}
	public boolean renderSearchField {get;set;}
	
	global PageReference listEmails(){
		return null;		
	}
	
	
	public without sharing class WithouSharingContactsDetails{
		
		public List<Contact> getContactRemote(String cRemoteId, set<String> accID ){
			
			List<Contact> result = new List<Contact>();
				if(cRemoteId !=null){
					result = [SELECT id, name, Email, remoteAccountId__c FROM Contact WHERE id in:accID OR remoteAccountId__c in:accId];
				}else{
					result = [SELECT id, name, Email, remoteAccountId__c FROM Contact WHERE id in:accID];
				}	
			return result;
		}
		
		public Contact getContactDetails(String clientid){
			return [Select id, Current_Agency__r.Global_Link__c, remoteAccountId__c from Contact where id = :clientid LIMIT 1];
		}
		
	}

	public String getEmailsJson(String clientId){
		WithouSharingContactsDetails ctDetails = new WithouSharingContactsDetails();	
				
		Contact ct = ctDetails.getContactDetails(clientId);
	
		String prefix = IPFunctions.emailFileNameS3(ct.Current_Agency__r.Global_Link__c, ct.Id);
		String instalmentsPrefix = IPFunctions.EmailInstalmentlFileNameS3(ct.Current_Agency__r.Global_Link__c, ct.Id);
		String IPprefix = IPFunctions.IPemailFileNameS3(string.valueOf(ct.remoteAccountId__c));
		
		/* Get list of emails of the client on the organization bucket */
		S3Controller cS3 = new S3Controller();
		cS3.constructor(); //Generate Credentials	
		cS3.listBucket(Userinfo.getOrganizationId(), prefix, null, null, null);
		
		S3Controller ipS3;
		if(ct.remoteAccountId__c !=null){
			/* List of Emails from IP */
			ipS3 = new S3Controller();
			ipS3.IpCredentials(); //Generate Credentials	
			ipS3.listBucket('00D20000000K9EgEAK', IPprefix, null, null, null);				
		}			
					
		if(Test.isRunningTest()){	    	
			S3.ListEntry listEn = new S3.ListEntry();
			listEn.Key='00D110000008Zs0EAE/Clients/0012000000w05fdAAA/Emails/S:-:0012000000eA8KvAAK:-:132456:-:AttY:-:22-10-2014 16:37:51';		    	
			cs3.bucketList = new List<S3.ListEntry>{listEn};
		}
		
		/* If list is not empty fill the email Details */				
		map<DateTime, list<EmailDetails>> emailDetailsMap = new map<DateTime, list<EmailDetails>>();		
		set<String> accID = new set<String>();
		Map<String, Contact> mapID;

		emailDetails = new List<EmailDetails>();
		
		accID.add(clientId);
		
		integer count = 0;
		
		if(cS3.bucketList != null && !cS3.bucketList.isEmpty()){		
			for(S3.ListEntry file : cS3.bucketList){
				
				EmailDetails email = filenameSubstring(file.key, count);
				
				if(email!=null){
					accID.add(email.idEmployee);
				
					if(!emailDetailsMap.containsKey(email.dateTimeOrder))
						emailDetailsMap.put(email.dateTimeOrder, new list<EmailDetails>{email});
					else emailDetailsMap.get(email.dateTimeOrder).add(email);
				}
				count++;
			}
		}
		
		
		/** Get emails from Instalments /				
		cS3.listBucket(Userinfo.getOrganizationId(), instalmentsPrefix, null, null, null);
		if(cS3.bucketList != null && !cS3.bucketList.isEmpty()){		
			for(S3.ListEntry file : cS3.bucketList){
				
				EmailDetails email = filenameSubstring(file.key, count);
				
				if(email!=null){
					accID.add(email.idEmployee);
				
					if(!emailDetailsMap.containsKey(email.dateTimeOrder))
						emailDetailsMap.put(email.dateTimeOrder, new list<EmailDetails>{email});
					else emailDetailsMap.get(email.dateTimeOrder).add(email);
				}
				count++;
			}
		}
		
		/** END -- Get emails from Instalments **/
		
		
		//IP Emails			
		if(ct.remoteAccountId__c !=null && ipS3.bucketList != null && !ipS3.bucketList.isEmpty()){
			system.debug('ipS3.bucketList==' + ipS3.bucketList.size());
			for(S3.ListEntry file : ipS3.bucketList){
				system.debug('file.key===' + file.key);
				EmailDetails email = filenameSubstring(file.key, count);
				
				if(email!=null){
					accID.add(email.idEmployee);
				
					if(!emailDetailsMap.containsKey(email.dateTimeOrder))
						emailDetailsMap.put(email.dateTimeOrder, new list<EmailDetails>{email});
					else emailDetailsMap.get(email.dateTimeOrder).add(email);
				}
				count++;
			}
		}
		list<DateTime> sortList = new list<DateTime>(emailDetailsMap.keySet());
		system.debug('before===' + sortList);
		sortList.sort();
		system.debug('sortList after===' + sortList);
		
	
		
		//Put it in DESC Order			
		if(qttEmails != null && qttEmails != 0){
			Integer qtt=0;
			for (Integer i = (sortList.size() - 1);(i >= 0 && qtt<qttEmails); i--){
				for(EmailDetails ed:emailDetailsMap.get(sortList[i])){
					emailDetails.add(ed);
				}				     		
				qtt++;
			}
		}else{
			for (Integer i = (sortList.size() - 1); i >= 0; i--)
				for(EmailDetails ed:emailDetailsMap.get(sortList[i]))
					emailDetails.add(ed);
		}
	
		map<String,String> clientRemoteId = new map<String,String>();
		mapID = new map<String, Contact>();
		
		
		List<Contact> contactsResult = ctDetails.getContactRemote(ct.remoteAccountId__c, accID);
		
		for(Contact c : contactsResult){
			mapID.put(c.Id, c);
			clientRemoteId.put(c.remoteAccountId__c, c.Id);
		}
		
		Contact client;
		Contact employee;
		for(EmailDetails email : emailDetails){
			if(mapiD.containsKey(email.idClient)){
				client=mapID.get(email.idClient);	
				email.clientName=client.Name;
				email.clientEmail=client.Email;					
			}
			
			if(ct.remoteAccountId__c !=null && clientRemoteId.containsKey(email.idClient)){
				client=mapID.get(clientRemoteId.get(email.idClient));	
				email.clientName=client.Name;
				email.clientEmail=client.Email;			
			}				
			
			if(mapiD.containsKey(email.idEmployee)){
				employee=mapID.get(email.idEmployee);	
				email.employeeName=employee.Name;
				email.employeeEmail=employee.Email;
			}

			if(ct.remoteAccountId__c !=null && clientRemoteId.containsKey(email.idEmployee)){
				employee=mapID.get(clientRemoteId.get(email.idEmployee));	
				email.employeeName=employee.Name;
				email.employeeEmail=employee.Email;			
			}									
		}

		return JSON.serialize(emailDetails);
	}
	
	public String emailsJSON {
		get { 			
			if(emailsJSON == null){
				String clientId= Apexpages.currentpage().getparameters().get('id');										
							
				emailsJSON = getEmailsJson(clientId);
				
			}		
			
			return emailsJSON;
		}		
		set;
	}
	
	public String emailContent {get;set;}
	public void viewEmail(){
		
		
		if(ApexPages.currentPage().getParameters().get('tp') == 'email'){
			
			String fileName = EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get('fileName'),'UTF-8');
			
			system.debug('fileName===' + fileName);
			
			S3Controller cS3;
			if(fileName.contains('Clients/')){			
				cS3= new S3Controller();
				cS3.constructor(); //Generate Credentials			
				cs3.getObject(fileName, Userinfo.getOrganizationId());			
			}else{
				/* List of Emails from IP */
				cS3 = new S3Controller();
				cS3.IpCredentials(); //Generate Credentials	
				cS3.getObject(fileName, '00D20000000K9EgEAK');
			}
			
			if(!Test.isRunningTest())
				emailContent =EncodingUtil.base64Decode(cs3.resultSingleObject.Data).toString();
			else
				emailContent = 'my test';
		}else{
			viewSMS();
		}
		
		
	}
	
	public void viewSMS(){
		String fileName = string.valueOf(ApexPages.currentPage().getParameters().get('fileName'));
		
		system.debug('fileName===' + fileName);
		
		S3Controller cS3;
		cS3= new S3Controller();
		cS3.constructor(); //Generate Credentials			
		cs3.getObject(fileName, Userinfo.getOrganizationId());			
		
		if(!Test.isRunningTest())
			emailContent =EncodingUtil.base64Decode(cs3.resultSingleObject.Data).toString();
		else
			emailContent = 'my test';
	}
	
	@RemoteAction
	global static String openEmail(String fileName){
		//String key = Apexpages.currentpage().getparameters().get('paramKey');
		
		S3Controller cS3;
		if(fileName.contains('Clients/')){			
			cS3= new S3Controller();
			cS3.constructor(); //Generate Credentials			
			cs3.getObject(fileName, Userinfo.getOrganizationId());			
		}else{
			/* List of Emails from IP */
			cS3 = new S3Controller();
			cS3.IpCredentials(); //Generate Credentials	
			cS3.getObject(fileName, '00D20000000K9EgEAK');
		}
		
		
		if(!Test.isRunningTest())
			emailBody =EncodingUtil.base64Decode(cs3.resultSingleObject.Data).toString();
		else
			emailBody = 'my test';
			
		return emailBody;
	}
	
	public String emailsFromClient(String idClient){
		
		Apexpages.currentPage().getParameters().put('id',idClient);
		
		return emailsJSON;
	}
	
	
	/*
     * Get all the email details and body using the fileName + client Id
     */
	public void downloadEmailData(String fileName){
		//Get email Data: From, To, Subject, Sent Date
		downloadEmail = filenameSubstring(fileName, null);
		
		//Get email body		
		downloadEmail.bodyStr= openEmail(fileName);
	}
	
	public EmailDetails filenameSubstring(String fileName, Integer count){
		EmailDetails email;
		system.debug('fileName: ' + fileName);
		
		list<string> splitKey;
		list<string> clientId;
		
		Boolean isIpS3;
		
		if(fileName.contains('Clients/')){
			splitKey = new list<string>(fileName.split('Clients/'));
			clientId=splitKey[1].split('/');						
		}else{
			clientId= fileName.split('/');//Is from IP
		}
		system.debug('Client ID===>' + clientId);
		
		String key= fileName.substring(fileName.lastIndexOf('/')+1, fileName.length());
		list<string> s = new list<string>(key.split(':-:'));
		if(s.size()>1){
			email = new EmailDetails();
			email.virtualId='email-'+count;
			email.key=fileName;
			email.ipMail = false;
			
			if(s[0].equals('R')){
				email.isFromEmployee=false;
			}
			if(s[0].equals('S')){
				email.isFromEmployee=true;
			}
			
			if(s[3].equals('AttY')){
				email.hasAtt=true;
			}
			if(s[3].equals('AttN')){
				email.hasAtt=false;
			}
			email.idClient=clientId[0];
			email.idEmployee=s[1];	
			email.subject=s[2];
			email.dateTimeOrder = parse(s[4]);
			//list<string> sDate = new list<string>(s[4].split(' '));
			email.dateSent= email.dateTimeOrder.format();
			email.dateOnlySent=  email.dateTimeOrder.date().format();
			
			
		}
			return email;
	}
	
	
	public static Datetime parse(String strDate){
		
		system.debug(Logginglevel.INFO, '$ strDate: ' + strDate);
		
		string year = strDate.substring(6, 10);
		string month = strDate.substring(3, 5);
		string day = strDate.substring(0,2);
		string hour = strDate.substring(11,13);
		string minute = strDate.substring(14,16);
		string second = strDate.substring(17,19);
		
		Datetime dt;
		
		try {
			dt = Datetime.valueOf(year + '-' + month + '-' + day + ' ' + hour + ':' + minute +  ':' + second);
		} catch(Exception e){
			system.debug('ERROR PARSING ' + strDate + ': ' + e.getMessage());
			dt = Datetime.now();
		}
		
		return dt;
	}
	
	
	public class emailDetails{
		public String virtualId {get;set;}
		public String key {get; set;}
		public String subject {get; set;}
		public Blob body {get; set;}
		public String bodyStr {get; set;}
		public String dateSent{get;set;}
		public String dateOnlySent{get;set;}
		public boolean isFromEmployee {get;set;}
		public String idClient {get;set;}
		public String idEmployee {get;set;}
		public Boolean hasAtt {get;set;}
		public String clientName {get;set;}
		public String clientEmail {get;set;}
		public String employeeName {get;set;}
		public String employeeEmail {get;set;}
		public boolean ipMail {get;set;}
		
		public DateTime dateTimeOrder {get;set;}
		
		
	}

}