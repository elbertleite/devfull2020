public with sharing class products_creditcard_payment {
	private User currentUser = [Select Id, Name, Aditional_Agency_Managment__c, Contact.Account.ParentId, Contact.AccountId, Contact.Department__c, Contact.Account.Global_Link__c from User where id = :UserInfo.getUserId() limit 1];
	public String s3Bucket {get;set;}
	public String s3Folder {get;set;}

	public products_creditcard_payment() {
		searchProducts();
		s3Bucket = IPFunctions.s3bucket;
	}

	public Date payDate {get{if(payDate == null) payDate = system.today(); return payDate;}set;}

	// P A Y 		C R E D I T C A R D
	public Invoice__c payInvoice {get;set;}
	public void confirmPay(){

		list<client_product_service__c> upProds = new list<client_product_service__c>();

		if(payDate == null)
			payDate = system.today();

		String strProds = '';

		for(client_product_service__c p : listProducts){
			if(p.isSelected__c){
				p.Creditcard_paid_on__c = system.now();
				p.Creditcard_paid_by__c = userInfo.getUserId();
				p.Creditcard_paid_date__c = payDate;
				p.isSelected__c = false;

				strProds += p.Id + ';';
				
				upProds.add(p);

				for(client_product_service__c f : p.paid_products__r){

					f.Creditcard_paid_on__c = system.now();
					f.Creditcard_paid_by__c = userInfo.getUserId();
					f.Creditcard_paid_date__c = payDate;
					strProds += f.Id + ';';

					upProds.add(f);
				}
			}
			else continue;
		}

		payInvoice = new Invoice__c (
			isCreditCard_payment__c = true, 
			Instalments__c = strProds, 	
			Total_Value__c = totalInvoice, 
			CurrencyIsoCode__c = mapAgCurrency.get(currentUser.Contact.AccountId),
			Paid_Date__c = payDate, 
			Paid_by__c = UserInfo.getUserId(), 	
			Paid_by_Agency__c = currentUser.Contact.AccountId);

		insert payInvoice;

		for(client_product_service__c p : upProds){
			p.Creditcard_Invoice__c = payInvoice.Id;
		}

		update upProds;

		s3Folder = currentUser.Contact.Account.Global_Link__c + '/' + payInvoice.Paid_by_Agency__c + '/creditCardPayments/' + payInvoice.Id;

		// searchProducts();
	}

	// C A L C U L A T E 		I N V O I C E
	public Decimal totalInvoice {get;set;}
	public Integer countProds {get{if(countProds == null) countProds = 0; return countProds;}set;}
	public void calcInvoice(){
		s3Folder = '';

		totalInvoice = 0;
		countProds = 0;

		for(client_product_service__c p : listProducts)

			if(p.isSelected__c){
				totalInvoice += p.Agency_Currency_Value__c != NULL ? p.Agency_Currency_Value__c : p.Price_Total__c;

				for(client_product_service__c f : p.paid_products__r){
					totalInvoice += f.Agency_Currency_Value__c != NULL ? f.Agency_Currency_Value__c : f.Price_Total__c;

				}
				countProds++;
			}
	}


	// S E A R C H 		P R O D U C T S
	public list<client_product_service__c> listProducts{get; set;}
	public void searchProducts(){
		String  fromDate = IPFunctions.FormatSqlDate(dateFilter.Original_Due_Date__c); 
		String  toDate = IPFunctions.FormatSqlDate(dateFilter.Discounted_On__c);
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && currentUser.Aditional_Agency_Managment__c == null) //set the user to see only his own agency
			selectedAgency = currentUser.Contact.AccountId;

			
		system.debug('selectedAgency===>' + selectedAgency);
		system.debug('selectedProduct===>' + selectedProduct);

		string sqlFilter = '';
		string sql = 'SELECT isSelected__c, Products_Services__r.Provider__r.Provider_Name__c, Creditcard_paid_on__c, Client__r.name, Creditcard_debit_date__c, Received_By_Department__r.name, Price_Total__c, Quantity__c, Received_By_Agency__r.name, Received_by__r.name, Product_Name__c, Currency__c, Agency_Currency_Value__c, Agency_Currency__c, Agency_Currency_Rate__c, (SELECT Product_Name__c, Price_Total__c, Agency_Currency_Value__c FROM paid_products__r) FROM client_product_service__c WHERE Received_By_Agency__c = :selectedAgency AND Paid_by_Agency__c = true AND Creditcard_reconciled_on__c != NULL AND ';
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
 			sql += ' Received_By_Agency__r.ParentId = \'' + selectedAgencyGroup +'\' AND ';


		if(selectedPeriod == 'range')
			sqlFilter += ' Creditcard_debit_date__c >= '+ fromDate + ' and  Creditcard_debit_date__c <= '+ toDate;
		
		else if(selectedPeriod == 'TODAY')
			sqlFilter += ' Creditcard_debit_date__c = TODAY';
		
		else if(selectedPeriod == 'THIS_WEEK')
			sqlFilter += ' Creditcard_debit_date__c = THIS_WEEK';
		
		else if(selectedPeriod == 'THIS_MONTH')
			sqlFilter += ' Creditcard_debit_date__c = THIS_MONTH';
		
		else if(selectedPeriod == 'unc')
			sqlFilter += ' Creditcard_Invoice__c = NULL AND Creditcard_paid_on__c = NULL';
		
		
		if(selectedProduct != 'all')
			sqlFilter += ' and Products_Services__c = \''+selectedProduct +'\'';
		
		sql += sqlFilter;
			
		sql += ' order by Creditcard_debit_date__c, Product_Name__c';

		system.debug('sql===>' + sql);

		listProducts = Database.query(sql);
	}


	// ---------------------------------------------------	FILTERS


	// Agency Group Options
	public string selectedAgencyGroup{
    	get{
    		if(selectedAgencyGroup == null)
    			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
    		return selectedAgencyGroup;
    	}set;}
	public List<SelectOption> agencyGroupOptions {
  		get{
   			if(agencyGroupOptions == null){
   				
   				agencyGroupOptions = new List<SelectOption>();  
   				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group  
	    			for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:currentUser.Contact.AccountId order by Parent.Name]){
		     			agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
		    		}
   				}else{
   					for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
		     			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
		    		}
   				}
	   		}
	   		return agencyGroupOptions;
	  }set;}

	public void changeGroup(){
		agencyOptions = null;
	}

	// Agency Options
	public String selectedAgency {
    	get{
    		if(selectedAgency == null)
    			selectedAgency = currentUser.Contact.AccountId;
    		return selectedAgency;
    	}set;}
	public map<String,String> mapAgCurrency {get;set;}
	public list<SelectOption> agencyOptions {get{
		if(agencyOptions == null){
			mapAgCurrency = new map<String,String>();
			agencyOptions = new List<SelectOption>();
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
				for(Account a: [Select Id, Name, Account_Currency_Iso_Code__c from Account where id =:currentUser.Contact.AccountId order by name]){
					if(selectedAgency == null)
						selectedAgency = a.Id;					
					agencyOptions.add(new SelectOption(a.Id, a.Name));
					mapAgCurrency.put(a.Id, a.Account_Currency_Iso_Code__c);
				}
				if(currentUser.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
						agencyOptions.add(new SelectOption(ac.id, ac.name));
						mapAgCurrency.put(ac.Id, ac.Account_Currency_Iso_Code__c);
					}
				}
			}else{
				for(Account a: [Select Id, Name, Account_Currency_Iso_Code__c from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
					agencyOptions.add(new SelectOption(a.Id, a.Name));
					mapAgCurrency.put(a.Id, a.Account_Currency_Iso_Code__c);
					if(selectedAgency == null)
						selectedAgency = a.Id;	
				}
			}

			agencyProducts = null;
		}

		return agencyOptions;
	}set;}

	public void changeAgency(){
		agencyProducts = null;
	}

	// Products
	public string selectedProduct{
    	get{
    		if(selectedProduct == null)
    			selectedProduct = 'all';
    		return selectedProduct;
    	}set;}

	public List<SelectOption> agencyProducts {get{
		if(agencyProducts == null){
			agencyProducts = new List<SelectOption>();
			agencyProducts.add(new SelectOption('all','-- All --'));

			for(AggregateResult ag : [Select Products_Services__c pId, Product_Name__c nm from client_product_service__c WHERE Received_By_Agency__c = :selectedAgency and Creditcard_reconciled_on__c != NULL AND 	Creditcard_paid_on__c = NULL group by Products_Services__c, Product_Name__c]){
				agencyProducts.add(new SelectOption((String) ag.get('pId'), (String) ag.get('nm')));
			}//end for
		}

		return agencyProducts;
	}set;}
	
	// Periods Options
	public string SelectedPeriod{get {if(SelectedPeriod == null) SelectedPeriod = 'unc'; return SelectedPeriod; } set;}
	public List<SelectOption> periods {get{
		if(periods == null){
			periods = new List<SelectOption>();
			periods.add(new SelectOption('TODAY','Today'));
			periods.add(new SelectOption('THIS_WEEK','This Week'));
			periods.add(new SelectOption('THIS_MONTH','This Month'));
			periods.add(new SelectOption('unc','Payment not Confirmed'));
			periods.add(new SelectOption('range','Range'));
		}
		return periods;
	}set;}

	// Date Filter
	public client_course_instalment__c dateFilter{
		get{
			if(dateFilter == null){
				dateFilter = new client_course_instalment__c(); 
				Date myDate = Date.today();
				Date weekStart = myDate.toStartofWeek();
				dateFilter.Original_Due_Date__c = weekStart;
				dateFilter.Discounted_On__c = dateFilter.Original_Due_Date__c.addDays(6);
			}
			return dateFilter;
		} 
		set;
	}
	
}