public class Batch_Social_Network{
	public Batch_Social_Network(){}
}
/*global class Batch_Social_Network implements Database.Batchable<sObject> {
	
	String query;
	
	global Batch_Social_Network() {}
	
	

	global Database.QueryLocator start(Database.BatchableContext BC) {
		String [] recordTypes = new String [] {'Lead','Client'};
		String query = 'SELECT ID FROM Contact WHERE RecordType.Name IN :recordTypes ORDER BY CreatedDate DESC ';
				
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Contact> ctts) {
		Set<String> idsContacts = new Set<String>();
		for(Contact ctt : ctts){
			idsContacts.add(ctt.ID);
		}
		Map<String, Destination_Tracking__c> lastTrackings = new Map<String, Destination_Tracking__c>(); 

		for(Destination_Tracking__c result : [SELECT Client__r.ID, ID, Name, CreatedBy.Contact.Account.Parent.ID, Client__r.Current_Agency__r.Parent.ID, Client__r.Original_Agency__r.Parent.ID, Client__r.Account.Parent.ID FROM Destination_Tracking__c WHERE Client__r.ID IN :idsContacts ORDER BY Client__r.ID, CreatedDate]){
			lastTrackings.put(result.Client__r.ID, result);
		}

		List<Destination_Tracking__c> trackingsToSave = new List<Destination_Tracking__c>();
		Destination_Tracking__c dTracking;
		Destination_Tracking__c cycle;
		for(String clientID : lastTrackings.keySet()){
			cycle = lastTrackings.get(clientID);
			dTracking = new Destination_Tracking__c(ID = cycle.ID);
			dTracking.Current_Cycle__c = true;
			if(!String.isEmpty(cycle.Client__r.Current_Agency__r.Parent.ID)){
				dTracking.Agency_Group__c = cycle.Client__r.Current_Agency__r.Parent.ID;
			}else if(!String.isEmpty(cycle.CreatedBy.Contact.Account.Parent.ID)){
				dTracking.Agency_Group__c = cycle.CreatedBy.Contact.Account.Parent.ID;
			}else if(!String.isEmpty(cycle.Client__r.Original_Agency__r.Parent.ID)){
				dTracking.Agency_Group__c = cycle.Client__r.Original_Agency__r.Parent.ID;
			}else{
				dTracking.Agency_Group__c = cycle.Client__r.Account.Parent.ID;
			}
			trackingsToSave.add(dTracking);
		}

		update trackingsToSave;
	}
	
	global void finish(Database.BatchableContext BC) {
		System.debug('Batch Process Complete');
	}
	
}

/*global Database.QueryLocator start(Database.BatchableContext BC) {
		String [] recordTypes = new String [] {'Lead','Client'};
		String query = 'SELECT Client__r.ID, ID, Name, Client__r.Current_Agency__r.Parent.ID, CreatedBy.Contact.Account.Parent.ID, Client__r.Original_Agency__r.Parent.ID FROM Destination_Tracking__c WHERE Current_Cycle__c = true AND Agency_Group__c = null';
				
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Destination_Tracking__c> cycles) {
		for(Destination_Tracking__c cycle : cycles){
			cycle.Agency_Group__c = cycle.Client__r.Current_Agency__r.Parent.ID;
		}
		update cycles;
	}*/