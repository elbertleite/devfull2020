public with sharing class client_course_receipt_by_email {
	public boolean checkSelectedFromParent {get;set;}
	public Contact clients {get;set;}
	public boolean sentEmailOwner {get;set;}
	public boolean emailImportant {get;set;}
	public String cc {get;set;}
	public String bcc {get;set;}
	public String subject {get;set;}
	public String body {get;set;}
	public String theMsg {get;set;}
	public boolean showMsg {get;set;}
	public boolean useSignature {get{if(useSignature == null) useSignature = true; return useSignature;}set;}
	private String fileNameS3;
	private String emailAction;
    public map<string,boolean> renderDocs {get;set;}
	public client_course_instalment__c instalment{get;set;}
	public Invoice__c invoice {get;set;}
	public client_product_service__c product{get;set;}
	
	public client_course_receipt_by_email(ApexPages.StandardController std){
		virtualId= 0;		
		clients = [Select Id, Name, Email, remoteContactId__c from Contact where id = :(Id)std.getRecord().id];
		
		/**if(ApexPages.currentPage().getParameters().get('instId') !=null){
			instalment = [SELECT id, client_course__r.Client__r.FirstName, client_course__r.Client__r.Current_Agency__c, client_course__r.Client__r.Preferable_Language__c, client_course__r.Client__r.Name, name,
         				client_course__r.Client__r.Owner__r.Name, client_course__r.Client__c, client_course__r.CreatedBy.Email, 
         				client_course__r.Client__r.Owner__r.Email, client_course__r.Client__r.Email 
	         				FROM client_course_instalment__c
	       						 WHERE id = :ApexPages.currentPage().getParameters().get('instId')];    
		}
		
		else if(ApexPages.currentPage().getParameters().get('invId') !=null){
			
			invoice = [SELECT id, Client__r.FirstName, Client__r.Current_Agency__c, Client__r.Preferable_Language__c, Client__r.Name, name,
         				Client__r.Owner__r.Name, Client__c, CreatedBy.Email, Client__r.Owner__r.Email, Client__r.Email 
	         				FROM Invoice__c
	       						 WHERE id = :ApexPages.currentPage().getParameters().get('invId')];    
	       						 
		}
		
		else if(ApexPages.currentPage().getParameters().get('prodId') !=null){
			
			product = [SELECT id, Client__r.FirstName, Client__r.Current_Agency__c, Client__r.Preferable_Language__c, Client__r.Name, name,
         				Client__r.Owner__r.Name, Client__c, CreatedBy.Email, Client__r.Owner__r.Email, Client__r.Email 
	         				FROM client_product_service__c
	       						 WHERE id = :ApexPages.currentPage().getParameters().get('invId')];    
   			
		}**/
	}
	
	
	
	public String textSignature {
	  	get{
	  		if(textSignature == null){
	  			textSignature = '<div style="margin-top: 20px;" class="sig">' + userdetails.Name;
	            textSignature += '<br />' + userDetails.Account.Name;
	            textSignature += '<br />' + userDetails.Email + ' </div>';  
	  		}
	  		return textSignature;
	  	}
	  	set;
	  }
	
	
	public Contact userDetails{
	    get{
	      if(userDetails == null){
	      	if(!Test.isRunningTest()){
	      		string contactId = [Select ContactId from user where id = :UserInfo.getUserId() limit 1].ContactId;
	       		userDetails = [select id, Name, Email, Signature__c, Account.Name, AccountId, Account.ParentId, Account.Global_Link__c from Contact where id = :contactId];
	      	} else {
	      		userDetails = new Contact();
	      	}
	        
	       
	      }
	      return userDetails;
	    }
	    set;
	}
	
	
	
	public String docCategory {get;set;}
	public List<SelectOption> categories {
		get {
		if(categories == null){
			categories = new List<SelectOption>();
			categories.add(new SelectOption('all', 'Select Category'));
			List<AggregateResult> lar = [Select Doc_Document_Category__c from Client_Document_File__c where Client__c = :clients.id and Doc_Document_Category__c != 'EmailAttachments' group by Doc_Document_Category__c];
			for(AggregateResult ar : lar)
				categories.add(new SelectOption( (String) ar.get('Doc_Document_Category__c'), (String) ar.get('Doc_Document_Category__c') ) );
		}
		return categories;
	}
		set;  		
	}
	
	public void reloadTypes(){
		types = null;
		docType = null;
		reloadFiles();
	}
	
	public void reloadFiles(){		
		files = null;
	}
	
	public void selectFile(){
		String fileID = ApexPages.currentPage().getParameters().get('fileID');
		
		if(!selectedFiles.containsKey(fileID)){			
			for(Client_Document_File__c cdf : files)
				if(fileID == cdf.Id){
					selectedFiles.put(fileID, cdf);
					break;
				}
		} else {
			selectedFiles.remove(fileID);
		}
		
	}
	
	private Map<String,Client_Document_File__c> selectedFiles = new Map<String,Client_Document_File__c>();
	
	public String docType {get;set;}
	public List<SelectOption> types {
		get {
		if(types == null){
			types = new List<SelectOption>();
			types.add(new SelectOption('all', 'Select Type'));
			List<AggregateResult> lar = [Select Doc_Document_Type__c from Client_Document_File__c where Client__c = :clients.id and Doc_Document_Category__c = :docCategory group by Doc_Document_Type__c];
			for(AggregateResult ar : lar)
				types.add(new SelectOption( (String) ar.get('Doc_Document_Type__c'), (String) ar.get('Doc_Document_Type__c') ) );
				
		}
		return types;
	}
		set;
	}
	
	public List<Client_Document_File__c> files {
		get{
			if( files == null && clients != null ){
					
				String sqlCat = '';
				String sqlType = '';
				if(docCategory != null && docCategory != '' && docCategory != 'all')
					sqlCat = ' and Doc_Document_Category__c = \''+docCategory+'\'';
					
				if(docType != null && docType != '' && docType != 'all')
					sqlType = ' and Doc_Document_Type__c = \''+docType+'\'';
					
				String sql = ' Select C.File_Name__c,Doc_Document_Type__c, Doc_Document_Category__c, C.File_Size__c, C.File_Size_in_Bytes__c, C.Preview_Link__c, CreatedDate, CreatedBy.Name, Selected__C, Client__c ' + 
					' from Client_Document_File__c C where Client__c =\''+clients.id+'\' and Doc_Document_Category__c != \'EmailAttachments\' ' + sqlCat + sqlType + ' order by Doc_Document_Category__c, Doc_Document_Type__c, CreatedDate desc';
					
					
				files = Database.query(sql);
					
				for(Client_Document_File__c cdf : files)					
					if(selectedFiles.containsKey(cdf.id))
						cdf.Selected__c = true;
					
			}
			return files;
		}
		set;
	}
	
	
	
	
	
	
	
	public string getSignature(){
	    if(useSignature && userDetails.Signature__c != null && userDetails.Signature__c != '' )
	      return userDetails.Signature__c;
	    else return textSignature;	        
	}
	
	
	
	
	public void getToEmails(){
		String clientEmails = ApexPages.currentPage().getParameters().get('toemails');
		if(clientEmails != null && clientEmails != ''){
			clientEmails = clientEmails.replace('[','').replace(']','').replace(' ','');
			List<String> emailsList = clientEmails.split(',');
			
			for(Contact ac : [Select id, name, Email from Contact where Email in :emailsList]){
				listClientEmail.put(ac.id, new Client(ac.id, ac.Name, ac.Email));
			}
			
		}
		
		showButton = false;
			
	}
	
	/** EPs PC Files /
	public string getOrgId(){
		return 	UserInfo.getOrganizationId();
	}
	
	public list<userAttachments> lUserAttachments {get;set;}
	
	public class userAttachments{
		public Boolean isChecked {get;set;}
		public String fileName {get;set;}
		public String fileKey {get;set;}
	}
	
	public void addUserAttachment(){
		if(lUserAttachments == null)
			lUserAttachments = new List<userAttachments>();
			
		userAttachments file = new userAttachments();
		file.fileName = ApexPages.CurrentPage().getParameters().get('fileName');
		file.fileKey = ApexPages.CurrentPage().getParameters().get('fileKey');		
		file.isChecked = true;
		
		lUserAttachments.add(file);
		
		system.debug('lUserAttachment====' + lUserAttachments);	
	
	}
	
	/** END EPs PC Files **/
	
	
	public PageReference sendEmail(){
		
		try { 
			mandrillSendEmail mse = new mandrillSendEmail();
		    mandrillSendEmail.messageJson email_md = new mandrillSendEmail.messageJson();
		    
		    //create json to mail
		    System.debug('Clients: '+clients);
   		    email_md.setTo(clients.Email, clients.Name);
		    
		    email_md.setFromEmail(userDetails.Email);
		    email_md.setFromName(userDetails.Name);
		    String emailService = IpFunctions.getS3EmailService();
	     	//email_md.setHeaders('Reply-To', userDetails.email+';'+emailService);
		    email_md.setSubject(subject); 
		    
		   
		     
		    //customer documents     
		    
		    //email.Body__c += getAttachments();
		    
		    boolean first = true;
		   	if (!files.isEmpty()) {
		   		for(Client_Document_File__c cdf : files){
			    	if(cdf.Selected__c){  
			      		if(first){
			        		body += '<br/><br/><b>Attachments:</b><br/>';
			        		first = false;          
			      		}
			        	body += '<a href="'+cdf.Preview_Link__c+'" >' + cdf.File_Name__c + '</a> <i>('+cdf.File_Size__c+')</i> <br/>';
			      	}      
			    }
			} 
		    
		    email_md.setHtml(body);
		        
		    String[] toBcc;
		    String[] toCc;
		    if (Bcc != null && Bcc != ''){
		      toBcc = Bcc.split(';', 0);
		      for(String b_cc : toBcc) email_md.setBcc(b_cc, '');
		    }       
		    if (Cc != null && Cc != ''){
		      toCc = Cc.split(';', 0);
		      for(String c_c : toCc) email_md.setCc(c_c, '');
		    }  
		    if (sentEmailOwner)
		      email_md.setCc(userDetails.Email, '');
		      
		    email_md.setImportant(emailImportant);
		    email_md.preserve_recipients(false);
		    
		    //save mail into S3  
		    if(!Test.isRunningTest())
		    	saveToS3(); 
		    	
		    // attach receipt
		   	Pagereference pr = Page.client_course_instalment_receipt;
		   	if(ApexPages.currentPage().getParameters().get('inst') !=null)
		   		pr.getParameters().put('inst', ApexPages.currentPage().getParameters().get('inst'));
		   		
	   		else if(ApexPages.currentPage().getParameters().get('inv') !=null)
		   		pr.getParameters().put('inv', ApexPages.currentPage().getParameters().get('inv'));
		   	
		   	else if(ApexPages.currentPage().getParameters().get('prod') !=null)	
		   		pr.getParameters().put('prod', ApexPages.currentPage().getParameters().get('prod'));
		   		
		    if(!Test.isRunningTest()){
			    blob fileBody = pr.getContentAsPDF();			    
			    email_md.setAttachment('application/pdf', 'receipt.pdf', fileBody);
		    }
		    
		    
		    //send email through mandrill
		    mse.sendMail(email_md);
		    
		    theMsg = 'Email sent.';
		    showMsg = true;
		     
		} catch (Exception e){
			system.debug(e.getMessage());
			theMsg = e.getMessage();
		}
	   	
	   	
		
			
	   
	   
	    return null;
	  }
	  
	  //S3 emails
	  public void saveToS3(){
	    
	    EmailToS3Controller s3 = new EmailToS3Controller();
	    Datetime myDT = Datetime.now();
	    String myDate = myDT.format('dd-MM-yyyy HH:mm:ss');	    
	    Blob bodyblob = Blob.valueof(body);
	    
	      s3.generateEmailToS3(clients, userDetails, true, subject, bodyblob, false, myDate);
	  }
	
	//addClientEmailListTo
	
	public void removeEmail(){
		
		String emailID = ApexPages.currentPage().getParameters().get('emailID');
		
		if(emailId != null && emailId != ''){
			listClientEmail.remove(emailId);
			showButton = true;
		}
		
		
	}
	
	
	public string accountID{get; set;}
	public string accountName{get; set;}		
	public string accountEmail{get; set;}
	
	
	
	public Map<String, Client> listClientEmail{get{if(listClientEmail == null) listClientEmail = new Map<String, Client>(); return listClientEmail;} set;}
	
	public Integer mapSize { 
		get { 
        	return listClientEmail.size(); 
    	}
	}
	
	public String clientEmailIDs {
		get {
			clientEmailIDs = '';
			for(String id : listClientEmail.keySet())
				clientEmailIDs += id + ', ';
			return clientEmailIDs;
		}
		set;
	}
	
	public pageReference addAddress(){
		if(accountId != null && accountEmail != null && accountId != '' && accountEmail != '' && accountName != null && accountName != ''){
			listClientEmail.put(accountId, new Client(accountId, accountName, accountEmail));
			accountName = null;
			accountId = null;
			accountEmail = null;
			showButton = true;
		}
		return null;
	}
	
	public Boolean showButton {get;set;}
	
	
	public class Client {
		
		public string clientID{get; set;}
		public string clientName{get; set;}		
		public string clientEmail{get; set;}
		
		public Client(String id, String name, String email){
			clientID = id;
			clientName = name;
			clientEmail = email;
		}
	}
	
	
	
	/**public boolean hasDocuments {
		get{
			if(hasDocuments == null)
				findContactDocuments();
			return hasDocuments;
		}
		set;
	}  
	  
	public Map<String, List<IPFunctions.ContactDocument>> contactDocuments {get;set;}
	public Map<String, List<IPFunctions.ContactDocument>> findContactDocuments(){
		hasDocuments = false;
  		if(contactDocuments == null && clients != null && clients.size() == 1){
  			AWSKeys credentials = new AWSKeys(IPFunctions.awsCredentialName);
  			contactDocuments = new Map<String, List<IPFunctions.ContactDocument>>();
  			
  			List<IPFunctions.ContactDocument> cds = IPFunctions.getContactDocuments(clients.get(0).id, credentials.key, credentials.secret); 
  			integer newfilesCount = cds != null && !cds.isEmpty() ? cds.size()+1 : 0;
  			List<IPFunctions.ContactDocument> migratedFiles = IPFunctions.getDocumentFiles(clients.get(0).id, newfilesCount);
  			
  			
  			renderDocs.put(null, true);
  			
  			if(cds != null && !cds.isEmpty()){
  				hasDocuments = true;
	  			for( IPFunctions.ContactDocument cd : cds ){
	  				if(contactDocuments.containsKey(cd.docCategory)){
	  					contactDocuments.get(cd.docCategory).add(cd);
	  				} else{
	  					renderDocs.put(cd.docCategory, false);
	  					contactDocuments.put(cd.docCategory,  new List<IPFunctions.ContactDocument>{cd} );
	  				}
	  			}
  			}
  			
  			
  			if(migratedFiles != null && !migratedFiles.isEmpty()){
  				hasDocuments = true;
	  			for( IPFunctions.ContactDocument cd : migratedFiles ){
	  				if(contactDocuments.containsKey(cd.docCategory)){
	  					contactDocuments.get(cd.docCategory).add(cd);
	  				} else{
	  					renderDocs.put(cd.docCategory, false);
	  					contactDocuments.put(cd.docCategory,  new List<IPFunctions.ContactDocument>{cd} );
	  				}
	  			}
  			}
  			
  			
  		}
  			
  		return contactDocuments;
  		
	}**/
	
  	public class Doc {
        public String category {get;Set;}       
        public list<DocumentDetails> listDocumentDetails{get{if(listDocumentDetails == null) listDocumentDetails = new list<DocumentDetails>(); return listDocumentDetails;} set;}
     	public list<IPFunctions.ContactDocument> listFiles{get{if(listFiles == null) listFiles = new list<IPFunctions.ContactDocument>(); return listFiles;} set;}
    }
    
    
    public class DocumentDetails{
        public Client_Document__c clientDocument {get{if(clientDocument == null) clientDocument = new Client_Document__c(); return clientDocument;} set;}
        
        public list<IPFunctions.ContactDocument> listFiles{get{if(listFiles == null) listFiles = new list<IPFunctions.ContactDocument>(); return listFiles;} set;}
    }
	
    private Map<String, Doc> docsMap;
    private List<Client_Document__c> listdocs {get;set;}
    private List<IPFunctions.ContactDocument> oldIPFiles {get;set;}
    public Map<String,Doc> getDocs(){
    	
        if(docsMap == null){
        	
        	docsMap = new Map<String,Doc>();
        	renderDocs = new map<string,boolean>();
        	
	       	listdocs = [Select Document_Category__c, C.Document_Type__c, lastModifiedDate, isSelected__c, Name, Preview_Link__c from Client_Document__c C where Client__c = :clients.id and (Agency_Access_Restriction__c = null or Agency_Access_Restriction__c includes ( :userDetails.AccountId)) order by  Document_Category__c, Document_Type__c, CreatedDate desc];

			for(Client_Document__c cd : listDocs){

				if(!docsMap.containsKey(cd.Document_Category__c)){
					Doc doc = new Doc();
					doc.Category = cd.Document_Category__c;
					DocumentDetails dd = new DocumentDetails();
					dd.clientDocument = cd;
					if(cd.preview_link__c!= null)
						doc.listFiles = IPFunctions.getFilesFromPreviewLink(cd.preview_link__c, virtualId);

					doc.listDocumentDetails.add(dd);
					docsMap.put(doc.Category, doc);

					renderDocs.put(doc.Category, false);
				}else{
					Doc d = docsMap.get(cd.Document_Category__c);

					DocumentDetails dd = new DocumentDetails();
					dd.clientDocument = cd;
					if(cd.preview_link__c!= null)
						d.listFiles.addAll(IPFunctions.getFilesFromPreviewLink(cd.preview_link__c, virtualId));

					d.listDocumentDetails.add(dd);

					docsMap.put(d.Category, d);
				}

				virtualId += docsMap.get(cd.Document_Category__c).listFiles.size();

			}//end for
	        
	        //   for(Client_Document__c cd : listDocs){
	          	
	        //   	if(!docsMap.containsKey(cd.Document_Category__c)){
		    //       	Doc doc = new Doc();
	        //         doc.Category = cd.Document_Category__c;
	        //         DocumentDetails dd = new DocumentDetails();
	        //         dd.clientDocument = cd;   
	                	              
	        //         doc.listDocumentDetails.add(dd);
	        //         docsMap.put(doc.Category, doc);
	               
	        //         renderDocs.put(doc.Category, false);
	          		
	        //   	}else{
	        //   		Doc d = docsMap.get(cd.Document_Category__c);
	          		
	        //   		DocumentDetails dd = new DocumentDetails();
	        //         dd.clientDocument = cd;   
	                	              
	        //         d.listDocumentDetails.add(dd);
	        //   	}
	                
               
            			
	        //   }//end for
	          
	          
            // oldIPFiles = new list<IPFunctions.ContactDocument>();
	        // if(clients.remoteContactId__c!=null){
	        // 	oldIPFiles = IPFunctions.getDocumentFiles(clients.id, 0);
			// 	if(oldIPFiles!=null)
			// 		virtualId = oldIPFiles.size()+1;	    	  	        
	        // }
	       
        }
        
		return docsMap;
        
    
    }
    
    
	public integer virtualId {get;set;}
	// public void renderDoc(){
	// 	String docCat = ApexPages.currentPage().getParameters().get('cat');
	// 	renderDocs.put(docCat, true);
		        
    //     S3Controller s3 = new S3Controller();
    //     s3.constructor();
    //     renderDocs.put(null, true);
                    
    //     Doc d = docsMap.get(docCat);
    //     for(DocumentDetails dd : d.listDocumentDetails){
	//         List<IPFunctions.ContactDocument> docFiles = IPFunctions.getDocument(String.valueOf(dd.clientDocument.id), dd.clientDocument.name, s3, virtualId);
	        
	//         d.listFiles.addAll(docFiles);
	        
	//         virtualId += docFiles.size();
	        
	//          if(oldIPFiles != null)    
    //             for(IPFunctions.ContactDocument cdfile : oldIPFiles)
	//             	if(cdfile.clientDocumentID == dd.clientDocument.id)
	//             		d.listFiles.add(cdfile);
        	
    //     }//end for
        
	// }  

 	private List<String> order = new List<String>{'Visa', 'Passport', 'Personal', 'Enrolment', 'Education'};
	public List<String> categoriesOrder {
        get{
            if(categoriesOrder == null){
                categoriesOrder = new List<String>();
                Schema.DescribeFieldResult fieldResult = Client_Document__c.Document_Category__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues(); 
                
                
                for(String orderCat : order)
                    for(String category : getDocs().keySet())
                        if(orderCat == category)
                            categoriesOrder.add(orderCat);
                
                        
                for(Schema.PicklistEntry f : ple){
                    
                    Boolean found = false;
                    for(String orderCat : order)
                        if(orderCat == f.getValue())
                            found = true;
                    
                    if(!found)
                        for(String category : getDocs().keySet())
                            if(category == f.getValue())
                                categoriesOrder.add(f.getValue());                      
                }
                        
                
                
            }
            return categoriesOrder;
        }
        Set;
        
    }
	
	
}