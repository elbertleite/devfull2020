@isTest
private class enrolmentRedirect_test {

	@isTest static void test_method_one() {

		Contact c = new Contact(LastName = 'Test');
		insert c;

		Client_Course__c cc = new Client_Course__c();
		cc.Client__c = c.id;
		insert cc;

		enrolmentRedirect er = new enrolmentRedirect();

		//Test Different statuses
		ApexPages.currentPage().getParameters().put('courseid', cc.id);
		er.redir();

		cc.LOO_Requested_TO_BO__C = true;
		update cc;
		er.redir();

		cc.LOO_Requested_TO_SC__C = true;
		update cc;
		er.redir();

		cc.LOO_Received__c = true;
		update cc;
		er.redir();

		cc.LOO_Requested_TO_CLI__c = true;
		update cc;
		er.redir();

		cc.Enrolment_Date__C = system.today();
		update cc;
		er.redir();

		cc.COE_Requested__c = true;
		update cc;
		er.redir();

		cc.COE_Received__c = true;
		update cc;
		er.redir();

		cc.COE_Sent_to_CLI__c = true;
		update cc;
		er.redir();



	}

}