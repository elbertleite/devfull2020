/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class fileUploadS3_test {

    static testMethod void myUnitTest() {
        TestFactory tf = new TestFactory();
		Account agency = tf.createAgency();
		Contact employee = tf.createEmployee(agency);
		User portalUser = tf.createPortalUser(employee);		
		
		Account school = tf.createSchool();
		School_Agreement__c sa = tf.createSchoolAgreement(school, agency);
		
		tf.createAWSKey();
					
		Test.startTest();
		//system.runAs(portalUser){
			
			fileUploadS3 fu = new fileUploadS3();
			fu.getPageHost();
			fu.getOrgId();
			
			Apexpages.currentPage().getParameters().put('schId', school.id);
            Apexpages.currentPage().getParameters().put('agrID', sa.id);
            Apexpages.currentPage().getParameters().put('longD', 'whatDoIPutHere');
            Apexpages.currentPage().getParameters().put('fromAgreement', 'AndHere');
        	Apexpages.currentPage().getParameters().put('filename', 'myAgreement');
			fu.addDocToAgreement();
			
			Apexpages.currentPage().getParameters().put('bucket', 'balde');
			fu.constructor();
			
			fu.setParameters();
			//String str = fu.commUrlPathPrefix;
			String str= fu.commName;
			str = fu.secret;
			str = fu.key;
			str = fu.getPolicyDesc();
			fu.getSignedPolicy();
			fu.getHexPolicy();
		//}
		Test.stopTest(); 
		
    }
}