global class batch_sendMassEmail implements Database.Batchable<mandrillSendEmail.messageJson>, Database.AllowsCallouts{
	
	list<mandrillSendEmail.messageJson> lEmails;
	global batch_sendMassEmail(List<mandrillSendEmail.messageJson> emails){
		
		lEmails = emails;
	}

	global Iterable<mandrillSendEmail.messageJson> start(Database.BatchableContext bc) {
		return lEmails;
	}

	global void execute(Database.BatchableContext BC, List<mandrillSendEmail.messageJson> lEmail){
		
		System.debug('lEmail: '+lEmail);
    	
		HttpRequest req = new HttpRequest();
		req.setEndpoint('https://mandrillapp.com/api/1.0/messages/send.json');
		req.setMethod('POST');
		
		mandrillSendEmail.jsonStringDetails js = new mandrillSendEmail.jsonStringDetails();
		
		for(mandrillSendEmail.messageJson em: lEmail){
			js.message = em;
			req.setBody(JSON.Serialize(js));
			Http h = new Http();
			HttpResponse res = h.send(req);
			//check response
			String status = res.getStatus();
			System.debug('responseBody: '+res.getBody());
			
		}//end for
		
		
	}//end execute
	
	
	 global void finish(Database.BatchableContext BC)
     {

     }


}