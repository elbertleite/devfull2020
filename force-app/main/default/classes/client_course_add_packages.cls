public without sharing class client_course_add_packages {
	
	public String visaType {get;set;}
	public String clientType {get;set;}
	public String clientNumber{get;set;}
	
	public list<Client_Course__c> packageCourses {get;set;}

	private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}
	private User currentUser {get{if(currentUser==null) currentUser = ff.currentUser; return currentUser;}set;}

	set<String> cities;
	set<String> countries;
	String clientId;
	contact client;
	private boolean isMigration {get;set;}
	public client_course_add_packages() {
		String pkg = ApexPages.currentPage().getParameters().get('cs');
		clientId = ApexPages.currentPage().getParameters().get('cid');
		system.debug('courses @=>' + pkg); 

		client = [Select Id, owner__r.AccountId from contact where id = :clientId];

		system.debug('migration==>' + ApexPages.currentPage().getParameters().get('mi'));

		isMigration = ( ApexPages.currentPage().getParameters().get('mi') != null && ApexPages.currentPage().getParameters().get('mi') != '' ) ? boolean.valueOf(ApexPages.currentPage().getParameters().get('mi')) : false;

		packageCourses = new list<Client_Course__c>();

		cities = new set<String>();
		countries = new set<String>();
		visaType = 'Student';
		
		for(Campus_Course__c cc : [select course__r.Name, Campus__c, Campus__r.BillingCountry, Campus__r.BillingCity, Campus__r.Name, Campus__r.Parent.Name, Campus__r.Parent.isTaxDeductible__c, Campus__r.Account_Currency_Iso_Code__c, Course__r.Course_Type__c, Id, Course__r.Only_Sold_in_Blocks__c, Course__r.Course_Unit_Length__c, Campus__r.Parent.Pfs_First_Instalment_Only__c, Campus__r.Parent.PFS_School__c, Campus__r.Parent.PFS_in_Bulk__c, Campus__r.Parent.PFS_Details__c, Campus__r.Parent.isCOE_Required__c, Course__r.Package__c, Package_Courses__c, Course__r.Course_Unit_Type__c FROM Campus_Course__c where id in :pkg.split(',')]){

			packageCourses.add(new Client_Course__c(
				Client__c = clientId, 
				Campus_Course__c = cc.id, 
				Campus_Name__c = cc.Campus__r.Name, 
				School_Name__c = cc.Campus__r.Parent.Name, 
				School_Id__c = cc.Campus__r.ParentId, 
				Campus_Id__c = cc.Campus__c, 
				Course_Name__c = cc.Course__r.Name, 
				Commission__c = 0, 
				Commission_Type__c = 0, 
				Deposit_Total_Used__c = 0, 
				Deposit_Total_Value__c = 0,
				Total_Extra_Fees_Unallocated__c = 0, 
				Course_Length__c = cc.Course__r.Course_Unit_Length__c,
				Unit_Type__c = cc.Course__r.Course_Unit_Type__c, 
				Course_Country__c = cc.Campus__r.BillingCountry, 
				Course_City__c = cc.Campus__r.BillingCity, 
				Course_Type__c = cc.Course__r.Course_Type__c, 
				Total_Course__c = 0, 
				Total_Extra_Fees__c = 0,
				Total_Extra_Fees_Promotion__c = 0, 
				isSoldBlocks__c = cc.Course__r.Only_Sold_in_Blocks__c, 
				CurrencyIsoCode__c = cc.Campus__r.Account_Currency_Iso_Code__c, 
				isMigrated__c = isMigration,  
				// Pfs_First_Instalment_Only__c = cc.Campus__r.Parent.Pfs_First_Instalment_Only__c, 
				// PFS_School__c = cc.Campus__r.Parent.PFS_School__c, 
				PFS_in_Bulk__c = cc.Campus__r.Parent.PFS_in_Bulk__c, 
				PFS_Details__c = cc.Campus__r.Parent.PFS_Details__c
				// isCOE_Required__c = cc.Campus__r.Parent.isCOE_Required__c
			));

			cities.add(cc.Campus__r.BillingCity );
			countries.add(cc.Campus__r.BillingCountry);

		}//end for

	}

	private map<String,Account> mTaxRate {get;set;}

	// R E T R I E V E 		T A X 		R A T E 
	private void retrieveTaxRate(){

		mTaxRate = new map<String,Account>();

		list<String> allCities = new list<String>(cities);
		list<String> allCountries = new list<String>(countries);
        
        String sql = 'Select Id, Parent.name, Name, Tax_Name__c, Tax_Rate__c from Account WHERE RecordType.name = \'city\' and name IN ( \''+ String.join(allCities, '\',\'') + '\' )  and Parent.name IN ( \''+ String.join(allCountries, '\',\'') + '\' ) limit 1';

		system.debug('@sql courses==>' + sql);

		for(Account acc : Database.query(sql))
			mTaxRate.put(acc.Name + ';;' + acc.Parent.Name, acc);

		system.debug('mTaxRate==>' + mTaxRate);

    }



	// S A V E 		C O U R S E S 
	public PageReference saveCourses(){

		if(visaType == null || visaType == 'none'){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please selected the visa type.'));
			return null;
		}

		retrieveTaxRate();
		
		boolean hasFirstCourse = false; // used to control and double check if has flag on package course

		String bk = ApexPages.currentPage().getParameters().get('bk');
		String pk = ApexPages.currentPage().getParameters().get('pk'); //course has already a package
		String nc = ApexPages.currentPage().getParameters().get('nc'); //needs to create a package

		String addCoursePkg = null;

		if(pk != null && pk != '')
			addCoursePkg = pk;
		else if(nc != null && nc != '')
			addCoursePkg = nc;

		if(bk==null || bk == ''){ // Check Booking Number
			Client_Course__c booking = new Client_Course__c(client__c = clientId, isBooking__c = true);
			insert booking;
			bk = booking.id;
		}
		

		Client_Course__c mainCourse;
		if(addCoursePkg != null){
			mainCourse = [SELECT Id, School_Student_Number__c, 	Client_Type__c, Visa_Type__c, Enrolment_Activities__c FROM client_course__c WHERE id = :addCoursePkg];
			clientNumber = mainCourse.School_Student_Number__c;
			clientType = mainCourse.Client_Type__c;
			visaType = mainCourse.Visa_Type__c;
		}

		list<Client_Course__c> packages = new list<Client_Course__c>();

		for(Client_Course__c cc : packageCourses){
			cc.School_Student_Number__c = clientNumber;
			cc.Client_Type__c = clientType;
			cc.Visa_Type__c = visaType;

			cc.Booking_Number__c = bk;

			cc.Commission_Tax_Name__c = mTaxRate.get(cc.Course_City__c + ';;' + cc.Course_Country__c).Tax_Name__c;
			cc.Commission_Tax_Rate__c = mTaxRate.get(cc.Course_City__c + ';;' + cc.Course_Country__c).Tax_Rate__c;

			cc.Enrolment_Start_Date__c = system.now();

			if(!currentUser.Finance_Enrolment_User__c) 
				cc.Enrolment_Agency__c = currentUser.Contact.AccountId;
			else 
				cc.Enrolment_Agency__c = client.owner__r.Accountid;

			if(hasFirstCourse || addCoursePkg != null) // to be sure only one course is the first course.
				cc.isPackage__c = false;

			if(addCoursePkg == null && cc.isPackage__c){
				hasFirstCourse = true;
				mainCourse = cc;
			}
			else{
				packages.add(cc);
			}
		}//end for

		if(addCoursePkg == null && !hasFirstCourse){// If by any reason the main course was not selected, make the first one as main course
			packages[0].isPackage__c = true;
			mainCourse = packages.remove(0);
		}
		else if(addCoursePkg != null){
			mainCourse.isPackage__c = true;
			mainCourse.Enrolment_Activities__c += EnrolmentManager.createActivity(EnrolmentManager.ACTIVITY_TYPE_SYSTEM, EnrolmentManager.ACTIVITY_COURSE_ADDED, system.now(), currentUser.Name + ' ('+currentUser.Contact.Account.Name+')' );
		}
		upsert mainCourse;

		// Associate packages 
		for(Client_Course__c cc : packages){
			cc.course_package__c = mainCourse.id;
		}//end for
		insert packages;

		Client_Document__c loO;
		Client_Document__c ef;

		//Create LOO and Enrolment Form
		if(!isMigration){
			if(addCoursePkg == null ){
				loO = new Client_Document__c (Client__c = clientId, Document_Category__c = 'Enrolment', Document_Type__c = 'LOO', Client_course__c = mainCourse.id);
				insert loO;
				ef = new Client_Document__c (Client__c = clientId, Document_Category__c = 'Enrolment', Document_Type__c = 'Enrolment Form', Client_course__c =  mainCourse.id);
				insert ef;
			}
			else{
				loO = [Select Id from Client_Document__c Where Document_Type__c = 'LOO' and Client_Course__c = :mainCourse.id Limit 1];
				ef = [Select Id from Client_Document__c Where Document_Type__c = 'Enrolment Form' and Client_Course__c = :mainCourse.id Limit 1];
			}
			
			//Create instalments and Associate Docs
			list<client_course_instalment__c> instalments = new list<client_course_instalment__c>();
			for(Client_Course__c cc : packageCourses){
				cc.Loo__c = loO.id;
				cc.Enrolment_Form__c = ef.id;	

				//Create instalments
				instalments.add(new client_course_instalment__c(client_course__c = cc.id, isPFS__c = cc.PFS_School__c, Number__c = 1, Due_Date__c = System.today(),Tuition_Value__c = 0, Extra_Fee_Value__c = 0, Instalment_Value__c = 0, Commission__c = 0, Commission_Value__c = 0));
			}//end for


			update packageCourses;
			insert instalments;
		}
		else{
			//Create instalments
			list<client_course_instalment__c> instalments = new list<client_course_instalment__c>();
			for(Client_Course__c cc : packageCourses){
				instalments.add(new client_course_instalment__c(client_course__c = cc.id, isPFS__c = cc.PFS_School__c, Number__c = 1, Due_Date__c = System.today(),Tuition_Value__c = 0, Extra_Fee_Value__c = 0, Instalment_Value__c = 0, Commission__c = 0, Commission_Value__c = 0));
			}//end for

			insert instalments;
		}
		
		

		PageReference pr;
		String newPageContact = ApexPages.currentPage().getParameters().get('page');
		if(!String.isEmpty(newPageContact) && newPageContact == 'contactNewPage'){
			pr  = Page.contact_new_bookings;
			pr.getParameters().put('id', clientId);
			pr.getParameters().put('cc', mainCourse.id);
			pr.getParameters().put('page', newPageContact);
		}
		/*if(isMigration){
			pr  = Page.client_course_migration;
			pr.getParameters().put('id', clientId);
			pr.getParameters().put('cc', mainCourse.id);
		}*/
		else{
			pr  = Page.client_course_new;
			pr.getParameters().put('id', clientId);
			pr.getParameters().put('ccid', mainCourse.id);
		}

		pr.setRedirect(true);
		return pr;
	}


	// private without sharing class courseInfo{

	// 	private Campus_Course__c campusCourse {get;set;}

	// 	private void findCampusDetails(camspusId){
			
	// 	}

	// }


	private List<SelectOption> numberOfUnits;
	public List<SelectOption> getNumberOfUnits() {
        if(numberOfUnits == null){
            numberOfUnits = new List<SelectOption>();
            for(integer i = 1; i <= 104; i++)
                numberOfUnits.add(new SelectOption(string.valueOf(i),string.valueOf(i)));
        }
        return numberOfUnits;
    }

	public List<SelectOption> unitType {
		get{
			if(unitType == null){
				unitType = new List<SelectOption>();
				Schema.DescribeFieldResult F = Course__c.fields.Course_Unit_Type__c.getDescribe();
				List<Schema.PicklistEntry> P = F.getPicklistValues();
				for (Schema.PicklistEntry lst:P)
					unitType.add(new SelectOption(lst.getValue(),lst.getLabel()));
			}return unitType;
		}set;}

	public List<SelectOption> clientTypeOpt {
		get{
			if(clientTypeOpt == null){
				clientTypeOpt = new List<SelectOption>();
				Schema.DescribeFieldResult F = Client_Course__c.fields.Client_Type__c.getDescribe();
				List<Schema.PicklistEntry> P = F.getPicklistValues();
				for (Schema.PicklistEntry lst:P)
					clientTypeOpt.add(new SelectOption(lst.getValue(),lst.getLabel()));
			}return clientTypeOpt;
		}set;}

	public List<SelectOption> visaTypeOpt {
		get{
			if(visaTypeOpt == null){
				visaTypeOpt = new List<SelectOption>{new SelectOption('none','-- Select Visa Type --')};
				Schema.DescribeFieldResult F = Client_Course__c.fields.Visa_Type__c.getDescribe();
				List<Schema.PicklistEntry> P = F.getPicklistValues();
				for (Schema.PicklistEntry lst:P)
					visaTypeOpt.add(new SelectOption(lst.getValue(),lst.getLabel()));
			}return visaTypeOpt;
		}set;}
}