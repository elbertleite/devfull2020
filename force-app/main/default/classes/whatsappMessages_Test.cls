@IsTest
private class whatsappMessages_Test {
    static testMethod void myUnitTest() {
		TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();

        Contact lead = tf.createClient(agency);
        Contact lead2 = tf.createClient(agency);
        Contact lead3 = tf.createClient(agency);
        Contact lead4 = tf.createClient(agency);

        Contact emp = tf.createEmployee(agency);
        User portalUser = tf.createPortalUser(emp);

        
		Test.startTest();
        system.runAs(portalUser){

            Forms_Of_Contact__c fc = new Forms_Of_Contact__c();
            fc.Detail__c = '+61222333444';
            fc.Type__c = 'WhatsApp';
            fc.Contact__c = lead.ID;
            fc.Is_WhatsApp_Number__c = true;
            
            insert fc;
            Batch_Mark_Last_Number_WhatsApp_Default batch = new Batch_Mark_Last_Number_WhatsApp_Default();
            batch.start(null);
            batch.execute(null, new List<Contact>{lead});
            batch.finish(null);
            
            Whatsapp_Message__c newMessage = new Whatsapp_Message__c(Contact__c = lead.ID, Message_Direction__c = 'Received', Message_Status__c = 'Received', SID__c = 'aaaaaa', Is_New_Message__c = true, Agency__c = agency.ID);
            insert newMessage;
            
            whatsappMessages.openChat(lead.ID, true);
            whatsappMessages.searchContactsMerge('a');
            whatsappMessages.markMessageAsRead(lead.id, newmessage.id);
            whatsappMessages.retrieveLeadsFromWhatsapp();
            whatsappMessages.retrieveOlderMessages(lead.id, 1, newMessage.id);

            Whatsapp_Message__c newMessage2 = new Whatsapp_Message__c(Contact__c = lead.ID, Message_Direction__c = 'Received', Message_Status__c = 'Received', SID__c = 'aaaaaa2', Is_New_Message__c = true, Agency__c = agency.ID);
            insert newMessage2;

            whatsappMessages.retrieveTotalNewMessagesForContact(lead.id);
            whatsappMessages.retrieveNewMessages(lead.id, newmessage.id, true);

            whatsappMessages.retrieveAgenciesAndUsers();


            Apexpages.currentPage().getParameters().put('MessageSid', 'aaaaaa');
            Apexpages.currentPage().getParameters().put('MessageStatus', 'failed');
            whatsappMessages.updateMessagestatus();
            Apexpages.currentPage().getParameters().put('MessageStatus', 'delivered');
            whatsappMessages.updateMessagestatus();
            Apexpages.currentPage().getParameters().put('MessageStatus', 'received');
            whatsappMessages.updateMessagestatus();
            Apexpages.currentPage().getParameters().put('MessageStatus', 'redad');
            whatsappMessages.updateMessagestatus();

            new whatsappMessages.clientDocumentsCategory();
            whatsappMessages.clientDocumentsCategory cdc = new whatsappMessages.clientDocumentsCategory('aaa');
            cdc.docs = null;
            new whatsappMessages.Message();
            new whatsappMessages.clientDocumentsResponse();
            whatsappMessages.clientDocumentsResponse cdr = new whatsappMessages.clientDocumentsResponse('aaa', 'aaa');
            cdr.quotationCourses = null;
            new whatsappMessages.QuotationCourseResponse();
            new whatsappMessages.QuotationCourseResponse('aaa','aaa','aaa','aaa','aaa');
            new whatsappMessages.WhatsappTemplate();
            new whatsappMessages.WhatsappTemplate(new Email_Template__c());

            Apexpages.currentPage().getParameters().put('agency', agency.id);
            Map<String, String> values = new Map<String, String>();
            values.put('numberFrom','+614399999');
            values.put('twillioAccountId','ACcb354be01c5c1a27dadcoooe926c89fa4e');
            values.put('twillioAccountToken','128acb9d3befa8cb6d5booo6fbebb6c10f');
            values.put('businessHourMessage','hahaha');
            values.put('defaultAgencyToCreateContact',agency.id);
            agency.Whatsapp_Messages_Config__c = JSON.serialize(values);
            agency.Whatsapp_Numbers__c = '+61999999999';
            agency.showcase_timezone__c = '(GMT+11:00) Australian Eastern Daylight Time (Australia/Sydney)';
            update agency;
    
            portalUser.Is_Default_Whatsapp_User__c = true;
            portalUser.Default_WhatsApp_Page_Filter__c = 'Group';
            update portalUser;

            whatsappMessages.doPost();

            whatsappMessages.transferConversationToContactAgency(lead.ID, true);
            whatsappMessages.WhatsappTemplate tmp = new whatsappMessages.WhatsappTemplate();
            tmp.template = 'aaaa';
            whatsappMessages.retrieveMessageFromTemplate(lead.ID, tmp);

            Map<String, String> actions = new Map<String, String>();
            actions.put('whatsappContactId', lead.id);
            actions.put('action', 'nada');
            actions.put('firstName', 'nada');
            actions.put('lastName', 'nada');
            actions.put('email', 'nada@nada.com');
            whatsappMessages.takeActionWhatsappContact(actions);
            actions.put('whatsappContactId', lead3.id);
            actions.put('action', 'merge');
            actions.put('idContactToMerge', lead2.id);
            whatsappMessages.takeActionWhatsappContact(actions);
            actions.put('whatsappContactId', lead4.id);
            actions.put('action', 'delete');
            whatsappMessages.takeActionWhatsappContact(actions);

            whatsappMessages wp = new whatsappMessages();
            actions = new Map<String, String>();
            actions.put('twillioAccountId', 'lead.id');
            actions.put('twillioAccountToken', 'aaaaaa');

            whatsappMessages.getTwillioRequest(actions);

		}
	}
}