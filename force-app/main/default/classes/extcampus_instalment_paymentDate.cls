public with sharing class extcampus_instalment_paymentDate {
	private Account acco;
	public extcampus_instalment_paymentDate(ApexPages.StandardController controller) {
		acco = (Account)controller.getRecord();
		loadInstalmentsPaymentDates();
	}

	public string SelectedDatesList 
	{
		get
	{
		if(loadSelectedDatesList)
		{
			SelectedDatesList = SavedDatesStringList;
		}
		return SelectedDatesList;
	}
		set;
	}
	
	private boolean loadSelectedDatesList{get;set;}
	public integer year
	{
		get
	{
		if(year == null)
			year = System.today().year();
		return year; 
	}
		set;
	}
	
	public ID campusID {
		get{
		try{
			if (campusID == null){ 
				campusID = acco.ID;
    
			}
			return campusID;
		}catch(Exception e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'There are currently no time table registered to this course.');
			ApexPages.addMessage(msg);
			return null;
		}
	} 
		set;
	}
    
	public List<date> datesSaved 
	{
		get{
		return datesSaved;
	}
		set;
	}
	
	public list<Campus_Instalment_Payment_Date__c> lcc 
	{
		get
	{
		lcc = retrieveInstalmentPaymentDates();
		return lcc;
	}
		set;
	}

	public static Map<Integer, String> months{
		get{
			if(months == null){
				months = new Map<Integer, String>();
				months.put(1,'January'); 
				months.put(2,'February'); 
				months.put(3,'March'); 
				months.put(4,'April'); 
				months.put(5,'May'); 
				months.put(6,'June'); 
				months.put(7,'July'); 
				months.put(8,'August'); 
				months.put(9,'September'); 
				months.put(10,'October'); 
				months.put(11,'November'); 
				months.put(12,'December'); 
			}
			return months;
		}
		set;
	}

	public List<InstalmentPaymentDates> getInstalmentPaymentDates(){
		List<InstalmentPaymentDates> dates = new List<InstalmentPaymentDates>();
		InstalmentPaymentDates payment;
		list<Campus_Instalment_Payment_Date__c> instalments = lcc;
		if(instalments != null && !instalments.isEmpty()){
			for(Campus_Instalment_Payment_Date__c instalment : instalments){
				if(instalment.Instalment_Payment_Date__c != null){
					payment = new InstalmentPaymentDates(extractMonthYearFromInstalment(instalment.Instalment_Payment_Date__c));
					if(dates.indexOf(payment) > -1){
						payment = dates.get(dates.indexOf(payment));
					}else{
						dates.add(payment);
					}
					payment.paymentDates.add(instalment.Instalment_Payment_Date__c);
				}
			}
		}
		system.debug('PAYMENTS FOUND '+json.serialize(dates));
		return dates;
	}

	public String extractMonthYearFromInstalment(Date paymentDate){
		//return months.get(paymentDate.month())+'/'+paymentDate.year();
		return months.get(paymentDate.month());
	}

	public class InstalmentPaymentDates{
		public String monthYear{get;set;}
		public List<Date> paymentDates{get;set;}

		public InstalmentPaymentDates(String monthYear){
			this.monthYear = monthYear;
			this.paymentDates = new List<Date>();
		}

		public Boolean equals(Object obj) {
        if (obj instanceof InstalmentPaymentDates) {
            InstalmentPaymentDates st = (InstalmentPaymentDates)obj;
				return st.monthYear.equals(this.monthYear);
            }
            return false;
        }

        public Integer hashCode() {
            return monthYear.hashCode();
        }
	}
	
	
	public void loadInstalmentsPaymentDates()
	{
			
		SelectedDatesList = '';
		SavedDatesStringList = '';
		loadSelectedDatesList = true;
		
		datesSaved = new List<date>();
		
		if(lcc != null && lcc.size() > 0){
			for(Campus_Instalment_Payment_Date__c cc : lcc)
			{
					system.debug('===================================================================> cc.Instalment_Payment_Date__c:' + cc.Instalment_Payment_Date__c);
					Date d = date.valueOf(cc.Instalment_Payment_Date__c + '00:00:00');
					system.debug('===================================================================> d:' + d);
					datesSaved.add(d);
					system.debug('===================================================================> datesSaved.size:' + datesSaved.size());
				//SelectedDatesList = SavedDatesStringList;
	
				/*SelectedDatesList = '2011/01/' + Math.ceil(Math.random()*30);
				SavedDatesStringList = '2011/01/' + Math.ceil(Math.random()*30);*/
			}			
		}
	}
            
		

	

	public string SavedDatesStringList {
		get{
		SavedDatesStringList = '';
		list<date> dl = new list<date>();
			
		if(lcc!=null && lcc.size()>0){
			datesSaved = new List<date>();
			for (Campus_Instalment_Payment_Date__c cc:lcc){
				Date d = date.valueOf(cc.Instalment_Payment_Date__c + '00:00:00');
				datesSaved.add(d);					
				if(SavedDatesStringList.length()>0)
					SavedDatesStringList+=',';
				SavedDatesStringList += d.year() + '/' + d.month() + '/' + d.day();
			}
		}
        
		return SavedDatesStringList;
	}
		set;
	}
    
	private List<Date> filterByYear(List<Date> dl)
	{
		List<Date> filteredDateList = new List<Date>();
		for(Date d:dl)
		{
			if(d.year() == year)
				filteredDateList.add(d);
		}
		return filteredDateList;
	}
	
	public PageReference save(){
		system.debug('===============> entrou ');
		loadSelectedDatesList = false;
		List<date> dl = filterByYear( StringListToDateList( SelectedDatesList ) );
		loadSelectedDatesList = true;    
		List<Campus_Instalment_Payment_Date__c> lOriginal =  new List<Campus_Instalment_Payment_Date__c>();
		List<Campus_Instalment_Payment_Date__c> lEdited = new List<Campus_Instalment_Payment_Date__c>();
		List<Campus_Instalment_Payment_Date__c> lDifference;
            
		//Fill lOriginal list
		if(lcc != null && lcc.size()>0)
			lOriginal = lcc;
            
		//Fill  lEdited list
		
		for(Date d:dl){
			Campus_Instalment_Payment_Date__c cid = new Campus_Instalment_Payment_Date__c();
			cid.Instalment_Payment_Date__c = d;
			cid.Campus__c = campusID;
			lEdited.add(cid);       
		}

        
		system.debug('===================================================================> lOriginal.size:' + lOriginal.size());
		system.debug('===================================================================> lEdited.size:' + lEdited.size());

        
		//diference between lOriginal - lEdited, to find the deleted dates
		lDifference = new List<Campus_Instalment_Payment_Date__c>();
		for(Campus_Instalment_Payment_Date__c lo : lOriginal){
			boolean found = false;
			for(Campus_Instalment_Payment_Date__c le : lEdited){
				if(lo.Instalment_Payment_Date__c == le.Instalment_Payment_Date__c)
				{found=true; break;}        
			}
			if(!found)
				lDifference.add(lo);
		}

		system.debug('===================================================================> to deleted lDifference.size:' + lDifference.size());

		DELETE lDifference;
        
        
		//diference between lEdited - lOriginal, to find the added dates
		lDifference = new List<Campus_Instalment_Payment_Date__c>();
		for(Campus_Instalment_Payment_Date__c le : lEdited){
			boolean found = false;
			for(Campus_Instalment_Payment_Date__c lo : lOriginal){
				if(le.Instalment_Payment_Date__c == lo.Instalment_Payment_Date__c)
					{found=true; break;}
			}
			if(!found){
				le.Campus__c = campusID;   
				lDifference.add(le);
			}
		}

		system.debug('===================================================================> to add lDifference.size:' + lDifference.size());
        
		INSERT lDifference; 
        
		SelectedDatesList = '';
		SavedDatesStringList = '';
        
		loadInstalmentsPaymentDates();
        
		return null;
	}
    
    
	public PageReference saveExtras()
	{
		if(lcc != null && lcc.size()>0)
			UPDATE lcc;
        
		return null;
	}
     
	private List<date> StringListToDateList(string sl){
		List<date> dl = new List<date>();
        
		if(sl.Length()>0){
			List<String> strDates = sl.split(',');
			for(String s : strDates)
			{
				s = s.replace('/','-');
				Date d = date.valueOf(s + ' 00:00:00');
				dl.add(d);
			}}
        
		return dl;
	}
    
    
	private List<Campus_Instalment_Payment_Date__c> retrieveInstalmentPaymentDates(){
		
		Date beginOfYear = date.newInstance(year, 1, 1);
		Date endOfYear = date.newInstance(year, 12, 31);
		
		list<Campus_Instalment_Payment_Date__c>  tlcc = [Select C.Instalment_Payment_Date__c from Campus_Instalment_Payment_Date__c C where Campus__c = :campusID and (Instalment_Payment_Date__c>=:beginOfYear and Instalment_Payment_Date__c<=:endOfYear) ORDER BY Instalment_Payment_Date__c];
		
		return tlcc;
	}
	
    
	public String getParam(String name) {
		return ApexPages.currentPage().getParameters().get(name);   
	}
	
}