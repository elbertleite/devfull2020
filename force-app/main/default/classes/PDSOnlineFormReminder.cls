global class PDSOnlineFormReminder implements Schedulable {

	global void execute(SchedulableContext sc) {

		List<mandrillSendEmail.messageJson> listEmails = new list<mandrillSendEmail.messageJson>();
		
		list<Invoice__c> toUpdate = new list<Invoice__c>();

		map<String, String> mapInv = new map<String, String>();

		date dt = System.today() + 14;

		for(Invoice__c inv : [SELECT Id, Request_Confirmation_Number__c, Due_Date__c, Sent_Email_To__c, createdBy.Name, createdBy.Email, Invoice_Activities__c, online_form_address__c, Total_Emails_Sent__c FROM Invoice__c WHERE Due_Date__c = :dt AND isPDS_Confirmation__c = TRUE AND isSchool_to_Answer__c = TRUE AND Request_Completed_by_School_On__c = NULL]){

			
			mandrillSendEmail mse = new mandrillSendEmail();
			mandrillSendEmail.messageJson email_md = new mandrillSendEmail.messageJson();

			String subject = 'Email - Request School Confirmation Reminder - ' + inv.Request_Confirmation_Number__c;

			String[] toAd;

			if(inv.Sent_Email_To__c.contains(';')){
		      	toAd = inv.Sent_Email_To__c.split(';', 0);
		      	for(String t : toAd)
		      		if(!String.isBlank(t))
		      			email_md.setTo(t, '');
		    }
		    else if(inv.Sent_Email_To__c.contains(',')){
		      	toAd = inv.Sent_Email_To__c.split(',', 0);
		      	for(String t : toAd)
		      		if(!String.isBlank(t))
		      			email_md.setTo(t, '');
		    }else{
		    	email_md.setTo(inv.Sent_Email_To__c, '');
		    }

			email_md.setCc(inv.createdBy.Email, '');
			email_md.setFromEmail(inv.createdBy.Email);
			email_md.setFromName(inv.createdBy.Name);

			email_md.setSubject(subject);
			listEmails.add(email_md);

			String body = Label.PDS_Confirmation_Reminder;

			body = body.replace('#DueDate#', inv.Due_Date__c.format()).replace('#InvoiceNumber#', inv.Request_Confirmation_Number__c);


			PageReference pg =  Page.commissions_pds_confirmation;
			Blob cryptoId = generateCrypto(string.valueOf(inv.id));
			String urlId = EncodingUtil.base64Encode(cryptoId);
			urlId = encodingUtil.URLEncode(urlId,'UTF-8');

			body = body.replaceAll('#OnlineForm#', '<a href=\''+inv.online_form_address__c+'\'>Click here</a>');

			email_md.setHtml(body);

			String activity = 'Email - Request School Confirmation Reminder;;' + inv.Sent_Email_To__c+ ';;' + subject +';;' + system.now().format('yyyy-MM-dd HH:mm:ss') + ';;sent;;-;;auto generated!#!';
			inv.Invoice_Activities__c += activity;
			inv.Total_Emails_Sent__c ++;

			mapInv.put(inv.id, activity);


			toUpdate.add(inv);
		}//end for

		update toUpdate;


		list<client_course_instalment__c> updatePDSInst = [SELECT Id, instalment_activities__c, PDS_Confirmation_Invoice__c FROM client_course_instalment__c WHERE PDS_Confirmation_Invoice__c IN :mapInv.keySet()];

		for(client_course_instalment__c i : updatePDSInst)
			i.instalment_activities__c += mapInv.get(i.PDS_Confirmation_Invoice__c);

		update updatePDSInst;

		if(listEmails.size()>0){
			batch_sendMassEmail batchSendMass = new batch_SendMassEmail(listEmails);
			Database.executeBatch(batchSendMass,99);
		}
	}


	private Blob generateCrypto(String idToCrypt){
		Blob key = Blob.valueOf('380db410e8b11fa9');
		Blob data = Blob.valueOf(idToCrypt);
		Blob encrypted = Crypto.encryptWithManagedIV('AES128', key, data);

		//Blob decrypted = Crypto.decryptWithManagedIV('AES256', key, encrypted);
		//String decryptedString = decrypted.toString();
		//System.assertEquals('Data to be encrypted', decryptedString);
		return encrypted;
	}
}