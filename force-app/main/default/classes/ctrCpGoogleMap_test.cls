@isTest
public class ctrCpGoogleMap_test {

	static testMethod void verifyGoogleMap(){
        
        
        Map<String,String> recordTypes = new Map<String,String>();
       
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
        
        Account acc = new Account();
        acc.RecordTypeId = recordTypes.get('Agency');
        acc.Name = 'IP Sydney'; 
        insert acc;
        
        Contact ct = new Contact();
        ct.accountid = acc.id;
        ct.FirstName = 'John';
        ct.LastName = 'Doe';
        ct.Email = 'jd@test.com';
        insert ct;
        
        
        Address__c address = new Address__c();
        address.Current_Address__c = true;
        address.Contact__c = ct.Id;        
        address.Suburb__c = 'Surry Hills';
        address.City__c = 'Sydney';
        address.State__c = 'NSW';
        address.Country__c = 'Australia';
        insert address;
        
        ctrCpGoogleMap gm = new ctrCpGoogleMap();
        
        String str = gm.inAddress;
        gm.inAddress = null;
        gm.ObjAddress = address;
        str = gm.inAddress;
        gm.StrAddress = 'Crown';
        gm.inAddress = null;
        str = gm.inAddress;
        gm.IncList = '';
        
        
        
        
        
    }

}