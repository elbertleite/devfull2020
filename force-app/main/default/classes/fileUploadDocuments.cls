public class fileUploadDocuments{
    
    
    public String fileDate{get;set;}
    
    public User employee {
    	get {
    		if(employee == null)
    			employee = [SELECT id, Contact.Name, Contact.Account.AgencyGroup__c, Contact.Account.Name, Contact.AccountID, Contact.Account.account_currency_iso_code__c , Contact.Account.Optional_Currency__c, Contact.Account.BillingCountry FROM User where id = :UserInfo.getUserId() limit 1];
    		return employee;
    	}
    	set;
    }
    
    public String commUrlPathPrefix {
    	get{
    		
    		if(ConnectApi.Communities.getCommunities().communities.size() > 0)
    			return ConnectApi.Communities.getCommunities().communities[0].urlPathPrefix;
    		else return null;
    	}
    	private set;    	
    }
    
    public String commName {
    	get{
    		return IPFunctions.getCommunityName();
    	}
    	set;
    }

    
    public Client_Document_File__c clientDocFile {
        get{
        if(clientDocFile == null){
            clientDocFile = new Client_Document_File__c();
            if (Apexpages.currentPage().getParameters().get('doctype') != null)
                clientDocFile.Doc_Document_Type__c = Apexpages.currentPage().getParameters().get('doctype');
            if (Apexpages.currentPage().getParameters().get('docCategory') != null)
            clientDocFile.Doc_Document_Category__c = Apexpages.currentPage().getParameters().get('docCategory');
        }
        return clientDocFile;
    }
        set;
    }
    
    public string getPageHost(){
        return  ApexPages.currentPage().getHeaders().get('Host');
    }
    
    public string getOrgId(){
        return  UserInfo.getOrganizationId();
    }
    
    public list<userAttachments> listAttachments {get{if(listAttachments ==null) listAttachments= new LIst<userAttachments>(); return listAttachments;}set;}
    
    public class userAttachments{
    	
    	public String fileName {get;set;}
    	public String s3FileKey {get;set;}
    }
    
    public PageReference createClientFile(){
        
        
        if(fromEmail != null && fromAgreement==null){
        	fileName = Apexpages.currentPage().getParameters().get('filename');
        	system.debug('fileName=======' + fileName); 
            /*Email_Attachments_Tracker__c eat = [Select id,Link__c,Files__c from Email_Attachments_Tracker__c where id = :Apexpages.currentPage().getParameters().get('docId')];
            if(eat.Link__c == null)
                eat.Link__c = 'https://s3.amazonaws.com/'+bucketName+'/'+Apexpages.currentPage().getParameters().get('doctype')+'/'+Apexpages.currentPage().getParameters().get('docId')+'/';
            
            if(eat.Files__c != null)
                eat.Files__c = eat.Files__c + ',' + Apexpages.currentPage().getParameters().get('filename') + ':' + Apexpages.currentPage().getParameters().get('fileSize');
            else
                eat.Files__c = Apexpages.currentPage().getParameters().get('filename') + ':' + Apexpages.currentPage().getParameters().get('fileSize');

            update eat;*/
            
        }
        
        else if (fromAgreement !=null){
        	system.debug('Entrou atualizacao');
        	schoolId = Apexpages.currentPage().getParameters().get('schId');
            agreementId = Apexpages.currentPage().getParameters().get('agrID');
            longD = Apexpages.currentPage().getParameters().get('longD');
            fromAgreement = Apexpages.currentPage().getParameters().get('fromAgreement');
        	fileName = Apexpages.currentPage().getParameters().get('filename');
               
        	School_Agreement__c a = [SELECT Id, document_link__c From School_Agreement__c where id = :Apexpages.currentPage().getParameters().get('agrID')];
        	
        	if(a.document_link__c==null || a.document_link__c =='')
        		a.document_link__c = fileName;
    		else
    			a.document_link__c = string.valueOf(a.document_link__c)+';'+string.valueOf(fileName);
        	update a;
        	system.debug('Saiu Atualizacao'); 
        }
        
        else {
        
            clientDocFile.Client__c = clientId;
            clientDocFile.File_Name__c = Apexpages.currentPage().getParameters().get('filename');
            clientDocFile.Country_of_Issue__c = Apexpages.currentPage().getParameters().get('docissued');
            clientDocFile.Doc_Document_Type__c = Apexpages.currentPage().getParameters().get('doctype');
            clientDocFile.Doc_Document_Category__c = Apexpages.currentPage().getParameters().get('docCategory');
            clientDocFile.Document_Number__c = Apexpages.currentPage().getParameters().get('docnum');
            clientDocFile.File_Size_in_Bytes__c = Decimal.valueOf( Apexpages.currentPage().getParameters().get('fileSize') );
            clientDocFile.ParentId__c = Apexpages.currentPage().getParameters().get('docId');
            clientDocFile.Preview_Link__c = 'https://s3.amazonaws.com/'+bucketName+ '/' + employee.contact.account.AgencyGroup__c + '/Clients/'+clientId+'/Documents/'+clientDocFile.Doc_Document_Type__c+'/'+clientDocFile.ParentId__c+'/'+clientDocFile.File_Name__c;            
            insert clientDocFile;
           
        }
        fileSize = '';
        return null;
    
    }
    

    
    
    /** AMAZON S3 **/
    public string secret { get {return credentials.secret;} }
    public AWSKeys credentials {get;set;}
    public string key { get {return credentials.key;} set;}
    private String AWSCredentialName = IPFunctions.awsCredentialName;
    public S3.AmazonS3 as3 { get; private set; }
    
    /* Variables for Agreement */ 
    public String schoolId {get;set;}
    public String agreementId {get;set;}
    public String longD {get;set;}
    public String fromAgreement {get;set;}
    
    public PageReference constructor(){
        try{
         	DateTime dt = DateTime.now(); 
    		fileDate = dt.format('dd-MM-yyyy') + '-';
    	
    	    credentials = new AWSKeys(AWSCredentialName);
            as3 = new S3.AmazonS3(credentials.key,credentials.secret);
    
            bucketName = Apexpages.currentPage().getParameters().get('bucket');

            clientId = Apexpages.currentPage().getParameters().get('clientId');
            
            docId = Apexpages.currentPage().getParameters().get('docId');
        
            fileSize =  Apexpages.currentPage().getParameters().get('fileSize');
            
            fromEmail = Apexpages.currentPage().getParameters().get('fromEmail');
        
            /* FOR AGREEMENT */
            schoolId = Apexpages.currentPage().getParameters().get('schId');
            agreementId = Apexpages.currentPage().getParameters().get('agrID');
            longD = Apexpages.currentPage().getParameters().get('longD');
            fromAgreement = Apexpages.currentPage().getParameters().get('fromAgreement');
            
    
            createBucket();
            
            if (Apexpages.currentPage().getParameters().get('filename') != null && Apexpages.currentPage().getParameters().get('filename') != 'fileName')
                createClientFile();
    
        }catch(AWSKeys.AWSKeysException AWSEx){
            System.debug('Caught exception in extQuote: ' + AWSEx);
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, AWSEx.getMessage());
            ApexPages.addMessage(errorMsg);
        }   
    
        return null;    
    }
    
    

    
    public void setParameters(){
    
        System.debug('**> fileName: '+fileName);
        contentType = ApexPages.currentPage().getParameters().get('contentType');
        docId = ApexPages.currentPage().getParameters().get('docId');
        clientId = ApexPages.currentPage().getParameters().get('clientId');
    
    }
    
    //public String docType;
    public String fromEmail;
    public String clientId;
    public String docId;
    
    public String bucketName { get { if(bucketName == null) bucketName = UserInfo.getOrganizationId(); return bucketName; } set; }
 	public String globalLinkId { get 
 		{ if(globalLinkId == null)
 			globalLinkId = [Select Contact.Account.Global_Link__c from User where id = :UserInfo.getUserId()].Contact.Account.Global_Link__c;
 			return globalLinkId ; 
 		} set; }
 		
    public String access { get { if(access == null) access = 'public-read'; return access; } set; }
    public String contentType { get { if(contentType == null) contentType = ''; return contentType; } set; }
	public String contentDisposition { get { if(contentDisposition == null) contentDisposition = 'attachment'; return contentDisposition; } set; }
    public String fileName { get { if(fileName == null) fileName = 'fileName'; return fileName; } set; }
    public String fileSize { get { if(fileSize == null) fileSize = '0'; return fileSize; } set; }
    
    datetime expire = system.now().addyears(20);
    String formattedexpire = expire.formatGmt('yyyy-MM-dd')+'T'+
        expire.formatGmt('HH:mm:ss')+'.'+expire.formatGMT('SSS')+'Z';
    
    private String success_action_redirect() {
        String str;
        if(fromEmail == null && fromAgreement ==null)
            str = 'https://'+ApexPages.currentPage().getHeaders().get('Host') + '/' + commName + '/fileUploadDocuments?filename=' + fileName + '&docCategory='+clientDocFile.Doc_Document_Category__c + '&docType='+clientDocFile.Doc_Document_Type__c+'&docId='+docId+'&clientId='+clientId+'&fileSize='+fileSize+'"},';
        else if(fromAgreement != null && fromEmail== null){
        	 str = 'https://'+ApexPages.currentPage().getHeaders().get('Host') + '/' + commName + '/fileUploadDocuments?filename=' + fileName + '&schId='+schoolId +'&agrID='+agreementId+'&longD='+longD+'&fromAgreement=true"},';
        	 system.debug('Passou Aqui' + str);
        }else 
            str = 'https://'+ApexPages.currentPage().getHeaders().get('Host') + '/' + commName + '/fileUploadDocuments?filename=' + fileName + '&clientId='+clientId+'&fileSize='+fileSize+'&fromEmail=true"},';
        
        system.debug('str=====' + str);
        return str;
        
    }
    
    string policy { get {return 
            '{ "expiration": "'+formattedexpire+'","conditions": [ {"bucket": "'+
            bucketName +'" } ,{ "acl": "'+
            access +'" },'+
            //  '{"success_action_status": "201" },'+
            '{"content-type":"'+contentType+'"},'+
			'{"content-disposition":"'+contentDisposition+'"},'+
            '{"success_action_redirect": "'+success_action_redirect()+                    
            '["starts-with", "$key", ""] ]}';   } } 
    
    public String getPolicy() {
    	return EncodingUtil.base64Encode(Blob.valueOf(policy));
    }
    
     public String getPolicyDesc() {
    	system.debug('Descricao Policy:'+policy );
    	return policy;
    }
    
    
    public String getSignedPolicy() {    
        return make_sig(EncodingUtil.base64Encode(Blob.valueOf(policy)));        
    }
    
    // tester
    public String getHexPolicy() {
        String p = getPolicy();
        return EncodingUtil.convertToHex(Blob.valueOf(p));
    }
    
    //method that will sign
    private String make_sig(string canonicalBuffer) {        
        String macUrl ;
        String signingKey = EncodingUtil.base64Encode(Blob.valueOf(secret));
        Blob mac = Crypto.generateMac('HMacSHA1', blob.valueof(canonicalBuffer),blob.valueof(Secret)); 
        macUrl = EncodingUtil.base64Encode(mac);                
        return macUrl;
    }
    
 
    /*
       Method to create a bucket on AWS S3 
    
    */
    public String createBucketErrorMsg {get;set;}
    public PageReference createBucket(){
        try{     
            createBucketErrorMsg= null;
            Datetime now = Datetime.now();        
            System.debug('about to create S3 bucket called: ' + UserInfo.getOrganizationId());
           
            //This performs the Web Service call to Amazon S3 and create a new bucket.
            S3.CreateBucketResult createBucketReslt = as3.CreateBucket(UserInfo.getOrganizationId(),null,as3.key,now,as3.signature('CreateBucket',now));
            System.debug('Successfully created a Bucket with Name: ' + createBucketReslt.BucketName);
            createBucketErrorMsg='Success';
            return null;
        }
        catch(System.CalloutException callout){
            System.debug('CALLOUT EXCEPTION: ' + callout);
            ApexPages.addMessages(callout);
            createBucketErrorMsg = callout.getMessage();
            return null;    
        }
        catch(Exception ex){
            System.debug(ex);
            ApexPages.addMessages(ex);
            createBucketErrorMsg = ex.getMessage();
            return null;    
        }
       
    }

}