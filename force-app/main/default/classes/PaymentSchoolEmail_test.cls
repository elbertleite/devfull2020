/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PaymentSchoolEmail_test {

    static testMethod void myUnitTest() {

       TestFactory tf = new TestFactory();

       Account school = tf.createSchool();
       Account agency = tf.createAgency();
       Contact emp = tf.createEmployee(agency);
       User portalUser = tf.createPortalUser(emp);
       Account campus = tf.createCampus(school, agency);
       Course__c course = tf.createCourse();
       Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);

		Back_Office_Control__c bo = new Back_Office_Control__c(Agency__c = agency.id, Country__c = 'Australia', Backoffice__c = agency.id, Services__c = 'Admissions');
		insert bo;

	  

       Test.startTest();
       system.runAs(portalUser){
       	   Contact client = tf.createLead(agency, emp);
	       client_course__c booking = tf.createBooking(client);
	       client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);

			Invoice__c inv = new Invoice__c();
			inv.Client__c = client.id;
			inv.Total_Products__c = 250;
			inv.Total_Tuition__c = 500;
			inv.Total_Value__c = 750;
			inv.Invoice_Activities__c += 'Marked as PDS;;-;;' + '-' +';;' + System.now().format('yyyy-MM-dd HH:mm:ss') + ';;-;;-;;Pryscilla!#!';
			inv.Paid_Date__c = System.today();
			insert Inv;

	       List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
		   
			instalments[0].isFirstPayment__c = true;
			instalments[0].isAmended_BeforeSchoolPayment__c = false;
			instalments[0].School_Payment_Invoice__c = inv.id;
			instalments[0].Received_By_Agency__c = agency.id;
			update instalments;

	       Client_Document__c receiptDoc = new Client_Document__c (Client__c = client.id, Document_Category__c = 'Finance', Document_Type__c = 'School Installment Receipt', Client_course__c = cc.id, client_course_instalment__c = instalments[0].id);
		   insert receiptDoc;

		   Client_Document__c receiptDoc2 = new Client_Document__c (Client__c = client.id, Document_Category__c = 'Enrolment', Document_Type__c = 'LOO', Client_course__c = cc.id, client_course_instalment__c = instalments[0].id);
		   insert receiptDoc2;


		   client_product_service__c product =  tf.createCourseProduct(booking, agency);

		   ApexPages.currentPage().getParameters().put('cs', instalments[3].id);
	       ApexPages.currentPage().getParameters().put('pd', product.id);
	       ApexPages.currentPage().getParameters().put('ct', client.id);

	       client_course_create_invoice invoicePay = new client_course_create_invoice();
	       invoicePay.createInvoice();
	       invoicePay.newPayDetails.Value__c = 1600;
		   invoicePay.newPayDetails.Payment_Type__c = 'Creditcard';
		   invoicePay.addPaymentValue();
		   invoicePay.savePayment();

		   String invoiceId = invoicePay.invoice.id;

		   Invoice__c updateInvoice = new Invoice__c(id = invoiceId, Instalments__c = instalments[3].id +';');
		   update updateInvoice;


	       PaymentSchoolEmail testClass = new PaymentSchoolEmail();

	       ApexPages.currentPage().getParameters().put('clientsIds', instalments[3].id);
	       testClass.loadClients();

	       testClass.getOrgId();

	       ApexPages.currentPage().getParameters().put('type', 'payment');
	       ApexPages.currentPage().getParameters().put('id', instalments[0].id);
	       ApexPages.currentPage().getParameters().put('doc', receiptDoc.id);

	       testClass.setupEmail();

	       testClass.toAddress ='testhify@hify.com;testhify2@hify.com';
	       testClass.Bcc ='testhify@hify.com;testhify2@hify.com';
	       testClass.sentEmailOwner =true;
	       testClass.SendEmail();

	       ApexPages.currentPage().getParameters().remove('type');
	       ApexPages.currentPage().getParameters().remove('id');
	       ApexPages.currentPage().getParameters().put('id', cc.id);
	       ApexPages.currentPage().getParameters().put('type', 'requestCOE');
	       testClass.setupEmail();
	       testClass.SendEmail();

	      //  ApexPages.currentPage().getParameters().remove('type');
	      //  ApexPages.currentPage().getParameters().put('type', 'requestConfirm');
	      //  testClass.toAddress ='testhify@hify.com,testhify2@hify.com';
	      //  testClass.Bcc ='testhify@hify.com,testhify2@hify.com';
	      //  testClass.setupEmail();
	      //  testClass.SendEmail();

	       ApexPages.currentPage().getParameters().remove('type');
	       ApexPages.currentPage().getParameters().put('type', 'coursecredit');
	       testClass.setupEmail();
	       testClass.SendEmail();

	      //  ApexPages.currentPage().getParameters().remove('type');
	      //  ApexPages.currentPage().getParameters().put('invoice', invoiceId);
	      //  ApexPages.currentPage().getParameters().put('type', 'sendInvoice');
	      //  testClass.setupEmail();
	      //  testClass.groupedInvoice.Due_Date__c = system.today();
	      //  testClass.dueDate.Expected_Travel_Date__c = system.today();
	      //  testClass.SendEmail();
	      //


	      //  List<String> categoriesOrder = testClass.categoriesOrder;
	      //  ApexPages.currentPage().getParameters().put('cat', 'Finance');
	      //  testClass.renderDoc();

	       testClass.cancelInvoice();

	       ApexPages.currentPage().getParameters().put('fileName', 'fileName');
	       ApexPages.currentPage().getParameters().put('fileKey', 'fileKey');
	       testClass.addUserAttachment();

           ApexPages.currentPage().getParameters().remove('type');
           ApexPages.currentPage().getParameters().put('type', 'requestLOO');
           testClass.setupEmail();
           testClass.SendEmail();
           testClass.getDocs();

       }
       Test.stopTest();

    }
}