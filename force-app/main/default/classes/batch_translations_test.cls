/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class batch_translations_test {
	
	static testMethod void myUnitTest() {
		
		TestFactory tf = new TestFactory();
		Account agency = tf.createAgency();
		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);		    
		Course__c course = tf.createCourse();		
		Course__c course2 = tf.createLanguageCourse();				
		Campus_Course__c cc = tf.createCampusCourse(campus, course);		
		Campus_Course__c cc2 = tf.createCampusCourse(campus, course2);
		
		tf.populateCourseTranslation(cc);
		tf.populateCourseTranslation(cc2);
		
		batch_translations b = new batch_translations('');		
		Database.executeBatch(b);
		
	}
    
}