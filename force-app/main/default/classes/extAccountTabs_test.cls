/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class extAccountTabs_test {

    static testMethod void verifyAccountTabs(){
        
        
        
       Map<String,String> recordTypes = new Map<String,String>();
       
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
			
		Account acc = new Account();
        acc.RecordTypeId = recordTypes.get('Agency');
        acc.Name = 'IP Sydney'; 
        insert acc;
        
        Contact ct = new Contact();
        ct.accountid = acc.id;
        ct.FirstName = 'John';
        ct.LastName = 'Doe';
        insert ct;
        
        
        ApexPages.StandardController controller = new ApexPages.StandardController(acc);
        extAccountTabs eat = new extAccountTabs(controller);
        
        List<String> AboutAsList = eat.AboutAsList;
        List<String> SellingPointsAsList = eat.SellingPointsAsList;
        eat.getAddresses();
       
        //eat.getLocation();
       
        //eat.getSchoolNews();
        //eat.getNotes();
        List<Campus_Course__c> getCourses = eat.getCourses();
        List<Video__c> getVideos = eat.getVideos();
        
        List<SchoolTestimonial__c> getSchoolTestimonials = eat.getSchoolTestimonials();
        List<Account> SiblingCampuses = eat.SiblingCampuses;
        eat.getAcco();
        eat.getLocation();
        
        
        String str = eat.s;
        
        
        
        
        
    }
}