public with sharing class commission_pay_bo {
	
	//C ON S T R U C T O R
	public commission_pay_bo(){

		gbLink = currentUser.Contact.Account.Global_Link__c;
		bucketName = IPFunctions.s3bucket;

		mainCurrencies = ff.retrieveMainCurrencies();
		agencyCurrencies = ff.retrieveAgencyCurrencies();
		currencyLastModifiedDate = ff.currencyLastModifiedDate;

		getAgencyGroupOptions();
		changeGroup();

		findInvoices();
	}


	//F I N D 		I N V O I C E S
	public void findInvoices(){
		showError = false;
		if(mapgbLink.size()>0)
			gbLink = mapgbLink.get(selectedAgency);
		else
			gbLink = currentUser.Contact.Account.Global_Link__c;

		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && currentUser.Aditional_Agency_Managment__c == null) //set the user to see only his own agency
			selectedAgency = currentUser.Contact.AccountId;

		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()) //set the user to see only his own agency group
			selectedAgencyGroup = currentUser.Contact.Account.ParentId;

		String sql= 'SELECT Summary__c, Invoice__c, Due_Date__c, Invoice_Activities__c, Sent_Email_To__c, Share_Commission_Number__c, Requested_by_Agency__r.Name, Requested_Commission_To_Agency__r.Name, Total_Value__c, Paid_On__c, Requested_Commission_for_Agency__c, Requested_Commission_for_Agency__r.Name, Requested_Commission_for_Agency__r.BillingCountry, Total_Instalments_Requested__c, Payment_Receipt__c, createdBy.Name, Requested_Commission_To_Agency__c, Country__c, CurrencyIsoCode__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, Transfer_Send_Fee_Currency__c, Transfer_Send_Fee__c, Bank_to_Deposit__r.Bank__c,	Bank_to_Deposit__r.Account_Nickname__c,	Bank_to_Deposit__r.Branch_Address__c, Bank_to_Deposit__r.Account_Name__c, Bank_to_Deposit__r.BSB__c, Bank_to_Deposit__r.Account_Number__c, Bank_to_Deposit__r.Swift_Code__c, Bank_to_Deposit__r.IBAN__c, createdBy.Contact.Email, Confirmed_Date__c, Comments__c, Preview_Link__c, Commission_Notes__c, ';

		sql += ' (SELECT Due_Date__c, Requested_Commission_To_Agency__r.Name, Share_Commission_Number__c, Total_Value__c, Paid_On__c, Paid_by__r.Name, Confirmed_Date__c, Confirmed_By__r.Name, Total_Instalments_Requested__c, Total_Emails_Sent__c, Sent_Email_To__c, Sent_On__c, CurrencyIsoCode__c FROM Invoices__r order by Requested_Commission_To_Agency__r.Name) ';

		sql += 'FROM Invoice__c WHERE isShare_Commission_Invoice__c = TRUE AND Requested_Commission_To_Agency__c =  \'' + selectedAgency + '\' ';

		if(selectedPaymentStatus=='unpaid')
			sql +=' AND Paid_On__c = NULL ';
		else
			sql +=' AND Paid_On__c != NULL ';


		if(invoiceNumber.length()>0){
			invoiceNumber = invoiceNumber.replace(' ', '');
			list<String> allRequest = new list<String>(invoiceNumber.split(','));
			sql += ' AND Share_Commission_Number__c in ( \''+ String.join(allRequest, '\',\'') + '\' ) ';
		}
		else{


			if(selectedTypeDate == 'dueDate'){
				sql+= ' AND ((Due_Date__c >= '+ IPFunctions.FormatSqlDateIni(dates.Commission_Due_Date__c); //Commission Due Date

				sql+= ' AND Due_Date__c <= '+ IPFunctions.FormatSqlDateFin(dates.Commission_Paid_Date__c); //Commission Due Date

				if(selectedPaymentStatus=='unpaid')
					sql+= ' ) OR Due_Date__c = NULL OR Due_Date__c < ' + System.now().format('yyyy-MM-dd') + ' )';//Overdue
				else
					sql += '))';
			}
			else{
				sql+= ' AND ((Paid_Date__c >= '+ IPFunctions.FormatSqlDateIni(dates.Commission_Due_Date__c); //Invoice Paid Date

				sql+= ' AND Paid_Date__c <= '+ IPFunctions.FormatSqlDateFin(dates.Commission_Paid_Date__c); //Invoice Paid Date

				sql += '))';
			}
		}
		

		 sql +=' AND Invoice__c = NULL AND isCancelled__c = FALSE ';

		 sql+= ' order by createdDate';

		system.debug('@SQL==>' + sql);

		map<String,countryCourses> countryMap = new map<String, countryCourses>();
		result = new list<countryCourses>();
		Invoice newInv;

		for(Invoice__c inv : sns.NSLInvoice(sql)){

			if(inv.Paid_On__c == NULL){
				inv.Agency_Currency__c = currentUser.Contact.Account.account_currency_iso_code__c;
				inv.Transfer_Send_Fee_Currency__c = inv.Agency_Currency__c;
				inv.Agency_Currency_Rate__c = agencyCurrencies.get(inv.CurrencyIsoCode__c);

				ff.convertCurrency(inv, inv.CurrencyIsoCode__c, inv.Total_Value__c, null, 'Agency_Currency__c', 'Agency_Currency_Rate__c', 'Agency_Currency_Value__c');
			}

			//Group by Agency
			newInv = new Invoice();
			newInv.invoice = inv;
			newInv.payStatus = 'complete';

			boolean isCompleted = true; // all payments are confirmed
			boolean isPending = false; // there are mini paid but not confirmed yet

			for(Invoice__c mini : inv.Invoices__r)
				if(mini.Confirmed_Date__c == null){
					isCompleted = false;
					if(mini.Paid_On__c != null)	
						isPending = true;
					else continue;
					// newInv.paymentOk = false;
					// break;
				}else continue;
				
			if(isPending) newInv.payStatus = 'pending';
			else if(!isCompleted) newInv.payStatus = 'waiting';
			
			/** Invoice Activities **/
   			if(inv.Invoice_Activities__c!=null && inv.Invoice_Activities__c != ''){

   				List<String> allAc = inv.Invoice_Activities__c.split('!#!');

   				for(Integer ac = (allAc.size() - 1); ac >= 0; ac--){//activity order desc

   					IPFunctions.instalmentActivity instA = new IPFunctions.instalmentActivity();
   					List<String> a = allAc[ac].split(';;');

   					instA.acType = a[0];
   					instA.acTo = a[1];
   					instA.acSubject = a[2];
   					if(instA.acType=='SMS') // saved date in system mode
   						instA.acDate = new Contact(Lead_Accepted_On__c = Datetime.valueOfGmt(a[3]));
   					else
   						instA.acDate = new Contact(Lead_Accepted_On__c = Datetime.valueOf(a[3]));
   					instA.acStatus = a[4];
   					instA.acError = a[5];
   					instA.acFrom = a[6];

   					newInv.activities.add(instA);
   				}
        	}

			if(!countryMap.containsKey(inv.Requested_Commission_for_Agency__r.BillingCountry))
				countryMap.put(inv.Requested_Commission_for_Agency__r.BillingCountry, new countryCourses(inv.Requested_Commission_for_Agency__r.Name, new agencyInvoices(inv.Requested_Commission_for_Agency__c, inv.Requested_Commission_for_Agency__r.Name, newInv), inv.Requested_Commission_for_Agency__r.BillingCountry, inv.CurrencyIsoCode__c));

			else if(countryMap.get(inv.Requested_Commission_for_Agency__r.BillingCountry).hasAgency(inv.Requested_Commission_for_Agency__r.Name))
				countryMap.get(inv.Requested_Commission_for_Agency__r.BillingCountry).addInvoice(inv.Requested_Commission_for_Agency__r.Name, newInv);

			else
				countryMap.get(inv.Requested_Commission_for_Agency__r.BillingCountry).addAgency(inv.Requested_Commission_for_Agency__r.Name, new agencyInvoices(inv.Requested_Commission_for_Agency__c, inv.Requested_Commission_for_Agency__r.Name, newInv));
		}//end for

		for(String ct : countryMap.keySet()){
			countryMap.get(ct).orderAgencies();
			result.add(countryMap.get(ct));
		}//end for

	}

	//C O N F I R M		A L L 
	public void confirmAll(){
		list<Invoice__c> upInvoices = new list<Invoice__c>();

		for(countryCourses pr : result)
			for(agencyInvoices agInv : pr.allAgencies)
				for(Invoice inv : agInv.invoices)
					if(inv.payStatus == 'complete' && inv.invoice.Paid_On__c == null)
						upInvoices.add(inv.invoice);
		
		system.debug('upInvoices==>' + upInvoices);
		ff.payShareCommInv(upInvoices, false);

		findInvoices();
	}

	//C O N F I R M     P A Y M E N T 
    public void confirmDeposit(){
      Savepoint sp;

      try{
        sp = Database.setSavepoint();

        String actionType = ApexPages.currentPage().getParameters().get('actionType');
        String country = ApexPages.currentPage().getParameters().get('country');
        String agencyId = ApexPages.currentPage().getParameters().get('agId');
        String invoiceId = ApexPages.currentPage().getParameters().get('invId');

        for(countryCourses pr : result)
          if(pr.countryName == country){
            for(agencyInvoices agInv : pr.allAgencies){
              if(agInv.agencyId == agencyId){
                for(Invoice inv : agInv.invoices){
                  if(inv.invoice.id == invoiceId){ 
					String payDocs = [SELECT Id, Payment_Receipt__r.preview_link__c FROM Invoice__c WHERE id = :inv.invoice.id limit 1].Payment_Receipt__r.preview_link__c;
					
					if(payDocs!=null && payDocs.length()>0){
						inv.showError = false;		
						ff.payShareCommInv(new list<Invoice__c>{inv.invoice}, true);
						findInvoices();
						break;
					}
					else{
						inv.showError = true;
						break;
					}
				}
				else continue;
                }//end invoice loop
              }//agency
              else continue;
            }//end for
          }
      }
      catch(Exception e){
        Database.rollback(sp);
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage() + ' => ' + e.getLineNumber());
        ApexPages.addMessage(myMsg);
      }
      finally{
        ApexPages.currentPage().getParameters().remove('country');
        ApexPages.currentPage().getParameters().remove('agId');
		ApexPages.currentPage().getParameters().remove('invId');
      }
    }

	public boolean isConfirmed {get;set;}
	//Set id to Unconfirm
	public void setIdToUnconfirm(){ 
		unconfirmCt = ApexPages.currentPage().getParameters().get('country');
		unconfirmAg = ApexPages.currentPage().getParameters().get('agId');
		unconfirmId = ApexPages.currentPage().getParameters().get('invId');
		invReason = new Invoice__c();
		invReason.Instalments__c = ApexPages.currentPage().getParameters().get('invNumber');

		isConfirmed = false;
		Invoice__c dbCheck = [SELECT Confirmed_Date__c FROM Invoice__c WHERE Id = :unconfirmId limit 1];
		
		if(dbCheck.Confirmed_Date__c != Null)
			isConfirmed = true;

		//Remove params
		ApexPages.currentPage().getParameters().remove('country');
		ApexPages.currentPage().getParameters().remove('agId');
		ApexPages.currentPage().getParameters().remove('invId');
		ApexPages.currentPage().getParameters().remove('invNumber');
	}


	public void unconfirmPayment(){
		Savepoint sp;
		try{
        sp = Database.setSavepoint();

		Invoice__c toUpdateInvoice;
        for(countryCourses pr : result)
			if(pr.countryName == unconfirmCt){
				for(agencyInvoices agInv : pr.allAgencies){
					if(agInv.agencyId == unconfirmAg){
						for(Invoice inv : agInv.invoices){
							if(inv.invoice.id == unconfirmId){ 
								ff.unpayShareCommInv(inv.invoice, invReason);	
								showError = true;
								break;
							}
							else continue;
						}//end invoice loop
					}//agency
					else continue;
				}//end for
			}
		}
		catch(Exception e){
			Database.rollback(sp);
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage() + ' => ' + e.getLineNumber());
			ApexPages.addMessage(myMsg);
		}
		
	}



	//************************* INNER CLASSES *************************
	public class countryCourses{
		public String countryName {get;set;}
		public String countryCurrency {get;set;}
		public list<agencyInvoices> allAgencies {get;set;}
		private map<String, agencyInvoices> mapAgencies {get;set;}

		public countryCourses (String agName, agencyInvoices ag, String countryName, String countryCurrency){
			mapAgencies = new map<String, agencyInvoices>();
			this.mapAgencies.put(agName, ag);
			this.countryName = countryName;
			this.countryCurrency = countryCurrency;
		}

		public boolean hasAgency(String agName){
			if(!mapAgencies.containsKey(agName))
				return false;
			else
				return true;
		}

		public void addAgency (String agName, agencyInvoices ag){
				mapAgencies.put(agName, ag);
		}

		public void addInvoice (String agName, Invoice i){
				mapAgencies.get(agName).addInvoice(i);
		}

		public void orderAgencies(){
			list<String> names = new list<String>();
			names.addAll(mapAgencies.keySet());
			names.sort();

			this.allAgencies = new list<agencyInvoices>();

			for(String nm : names)
				this.allAgencies.add(mapAgencies.get(nm));
		}
	}

	public class agencyInvoices{
		public String agencyId {get;set;}
		public String agencyName {get;set;}
		public list<Invoice> invoices {get{if(invoices==null) invoices= new list<Invoice>(); return invoices;}set;}
		public decimal totalAmount {get{if(totalAmount==null) totalAmount= 0; return totalAmount;}set;}

		public agencyInvoices (String agencyId, String agencyName, Invoice i){
			this.agencyId = agencyId;
			this.agencyName = agencyName;
			this.invoices.add(i);
			this.totalAmount = i.invoice.Total_Value__c;
		}

		public void addInvoice(Invoice i){
			this.invoices.add(i);
			this.totalAmount += i.invoice.Total_Value__c;
		}
	}

	public class Invoice{
		public Invoice__c invoice {get;set;}
		public Boolean showError {get{if(showError == null) showError = false; return showError; }set;}
		public String payStatus {get;set;}
		public list<IPFunctions.instalmentActivity> activities {get{if(activities == null) activities = new list<IPFunctions.instalmentActivity>(); return activities;} set;}
	}

	//*************************  FILTERS *************************
	private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}
	private User currentUser {get{if(currentUser==null) currentUser = ff.currentUser; return currentUser;}set;}
	private Map<String, double> agencyCurrencies;
	private IPFunctions.SearchNoSharing sns {get{if(sns == null) sns = new IPFunctions.SearchNoSharing(); return sns;}set;}
	private map<string,string> mapgbLink {get{if(mapgbLink==null) mapgbLink = new map<String,String>(); return mapgbLink;}set;}

	private String unconfirmCt {get;set;}
	private String unconfirmAg {get;set;}	
	private String unconfirmId {get;set;}

	public Boolean showError {get{if(showError == null) showError = false; return showError;}set;}
	public String invoiceNumber {get{if(invoiceNumber == null) invoiceNumber = ''; return invoiceNumber;}set;}
	public String selectedAgency {get;set;}
	public String selectedAgencyGroup {get;set;}
	public String bucketName {get;set;}
	public String gbLink {get;set;}
	public String selectedPaymentStatus {get{if(selectedPaymentStatus==null) selectedPaymentStatus = 'unpaid'; return selectedPaymentStatus;}set;}
	public DateTime currencyLastModifiedDate {get;set;}
	public Invoice__c invReason {get{if(invReason==null) invReason = new Invoice__c(); return invReason;}set;}
	public list<SelectOption> mainCurrencies {get;set;}
	public list<SelectOption> agencyGroupOptions {get;set;}
	public list<SelectOption> agencies{get; set;}
	public list<countryCourses> result {get;set;}


	public list<SelectOption> paymentStatus {get{
      if(paymentStatus == null){
        paymentStatus = new list<SelectOption>();
        paymentStatus.add(new SelectOption('unpaid','Unpaid'));
        paymentStatus.add(new SelectOption('paid','Paid'));
      }
      return paymentStatus;
    }set;}

	public string selectedTypeDate {get{if(selectedTypeDate == null) selectedTypeDate = 'dueDate'; return selectedTypeDate;}set;}

	public void getAgencyGroupOptions(){
		if(agencyGroupOptions == null){
			selectedAgency = 'none';
			selectedAgencyGroup = null;

			agencyGroupOptions = new List<SelectOption>();
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group
				agencyGroupOptions.add(new SelectOption(currentUser.Contact.Account.ParentId, currentUser.Contact.Account.Parent.Name));
				selectedAgencyGroup = currentUser.Contact.Account.ParentId;
			}else{
				for(AggregateResult ag : sns.NSAggregate('SELECT Requested_Commission_to_Agency_Group__c, Requested_Commission_to_Agency_Group__r.Name agName FROM Invoice__c WHERE isShare_Commission_Invoice__c = TRUE AND Paid_On__c = NULL AND Invoice__c = Null AND isCancelled__c = FALSE group by Requested_Commission_to_Agency_Group__c, Requested_Commission_to_Agency_Group__r.Name order by Requested_Commission_to_Agency_Group__r.Name ')){
   			agencyGroupOptions.add(new SelectOption(String.valueOf(ag.get('Requested_Commission_to_Agency_Group__c')), String.valueOf(ag.get('agName'))));

				if(selectedAgencyGroup==null)
					selectedAgencyGroup = String.valueOf(ag.get('Requested_Commission_to_Agency_Group__c'));

				if(String.valueOf(ag.get('Requested_Commission_to_Agency_Group__c')) == currentUser.Contact.Account.ParentId)
					selectedAgencyGroup = String.valueOf(ag.get('Requested_Commission_to_Agency_Group__c'));
  		}//end for
			}
		}
	}

    public void retrieveAgencies(){
        agencies = new List<SelectOption>();
        if(selectedAgencyGroup != null){
            if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency

                selectedAgency = currentUser.Contact.AccountId;
                gbLink = currentUser.Contact.Account.Global_Link__c;
                agencies.add(new SelectOption(currentUser.Contact.AccountId, currentUser.Contact.Account.Name));
                mapgbLink.put(currentUser.Contact.AccountId, currentUser.Contact.Account.Global_Link__c);
				if(currentUser.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
						agencies.add(new SelectOption(ac.id, ac.name));
					}
				}

            }else{
                for(AggregateResult ag: sns.NSAggregate('SELECT Requested_Commission_To_Agency__c, Requested_Commission_To_Agency__r.Name agName, Requested_Commission_To_Agency__r.Global_Link__c agLink FROM Invoice__c where Requested_Commission_to_Agency_Group__c = \'' + selectedAgencyGroup + '\' AND isShare_Commission_Invoice__c = TRUE AND Paid_On__c = NULL AND Invoice__c = Null AND isCancelled__c = FALSE group by Requested_Commission_To_Agency__c, Requested_Commission_To_Agency__r.Name, Requested_Commission_To_Agency__r.Global_Link__c order by Requested_Commission_To_Agency__r.Name')){
                    agencies.add(new SelectOption(String.valueOf(ag.get('Requested_Commission_To_Agency__c')), String.valueOf(ag.get('agName'))));
                    mapgbLink.put(String.valueOf(ag.get('Requested_Commission_To_Agency__c')), String.valueOf(ag.get('agLink')));

                    if(selectedAgency=='none'){
                        selectedAgency = String.valueOf(ag.get('Requested_Commission_To_Agency__c'));
                        gbLink = String.valueOf(ag.get('agLink'));
                    }

                    if(String.valueOf(ag.get('Requested_Commission_To_Agency__c')) == currentUser.Contact.AccountId){
                        selectedAgency = String.valueOf(ag.get('Requested_Commission_To_Agency__c'));
                        gbLink = String.valueOf(ag.get('agLink'));
                    }
                }
            }//end for
            if(selectedAgency=='none'){
                selectedAgency = currentUser.Contact.AccountId;
                gbLink = currentUser.Contact.Account.Global_Link__c;
            }
        }
    }

	public void changeGroup(){
		selectedAgency = 'none';
		retrieveAgencies();
	}


	public client_course_instalment__c dates{get{
		if(dates==null){
			dates = new client_course_instalment__c();
			dates.Commission_Due_Date__c = system.today();
			dates.Commission_Paid_Date__c = dates.Commission_Due_Date__c.addMonths(1);
		}
		return dates; 
	}set;}
}