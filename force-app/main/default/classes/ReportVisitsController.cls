public with sharing class ReportVisitsController {

	// ---->              Agency  | Department   | User |     list Visits
	public transient map<string, map<string, map<string, list<ClientVisit>>>> resultMap {get;set;}
	public transient map<string, map<string, visitAvg>> countEmpMap {get;set;}
	public transient map<string, visitAvg> countDepMap {get;set;}

	public transient Integer totalVisits {get;set;}
	public transient Integer totalVAg {get;set;}
	public transient Integer totalVDept {get;set;}
	public transient Integer totalVUser {get;set;}
	public transient Integer totalServices {get;set;}
	public transient Integer totalAllVisits {get;set;}

	public Integer workingDays {get;set;}

	public boolean showResult {get;set;}
	public boolean showMsg {get;set;}
	public string selectedStatus{get{if(selectedStatus == null) selectedStatus = 'Completed'; return selectedStatus;} set;}
	public string selectedDepartment{get{if(selectedDepartment == null) selectedDepartment = 'all'; return selectedDepartment;} set;}
	public string selectedService{get{if(selectedService == null) selectedService = 'all'; return selectedService;} set;}
	//public string selectedCountry{get{if(selectedCountry == null) selectedCountry = userDetails.Contact.MailingCountry; return selectedCountry;} set;}
	public String selectedAgencyGroup {get{if(selectedAgencyGroup == null) selectedAgencyGroup = userDetails.Contact.Account.ParentId; return selectedAgencyGroup;}set;}
	public String selectedAgency {get{if(selectedAgency == null) selectedAgency = userDetails.Contact.AccountId; return selectedAgency;}set;}
	public String selectedPeriod {get{if(selectedPeriod == null) selectedPeriod = 'PERIOD'; return selectedPeriod;}set;}

	public list<String> selectedUser {get{if(selectedUser == null) selectedUser = new list<String>(); return selectedUser;}set;}

	private set<id> agenciesId;


	/* Inner class for Average Calculation */
	public class visitAvg{
		public integer visitCount {get;set;}
		public decimal visitDuration {get;set;}
		public decimal totalAverage {get;set;}
		public decimal waitingDuration {get;set;}
		public decimal totalAverageWaiting {get;set;}
	}

	/* Inner Class for the search result */
	public class ClientVisit{
		public String idVisit {get;set;}
		public String acceptedDate {get;set;}
		public String closeDate {get;set;}
		public String comments {get;set;}
		public String cancelReason {get;set;}
		public String agencyName {get;set;}
		public String deptName {get;set;}
		public String serviceName {get;set;}
		public String status {get;set;}
		public String clientId {get;set;}
		public String clientName {get;set;}
		public String clientEmail {get;set;}
		public String clientTotVisits {get;set;}
		public Decimal compHourDuration {get;set;}
		public Decimal compHourWaiting {get;set;}
		public boolean isAttCancelled {get;set;}
		public boolean isAttCompleted {get;set;}
	}

	public ReportVisitsController(){
		showResult = false;
		showMsg = false;

		fromDate = new Contact();
		toDate = new Contact();

	}



	/** Search **/
	public void searchClientVisits(){

		showResult = true;
		system.debug('selectedPeriod==' + selectedPeriod);
	 	resultMap = new map<string, map<string, map<string, list<ClientVisit>>>>();
		if(selectedPeriod == 'PERIOD' && (fromDate.Expected_Travel_Date__c==null || toDate.Expected_Travel_Date__c==null)){
			system.debug('its a period filter');
			fromDate.Arrival_Date__c.addError('Fill a value for the period.');
			showResult = false;
		}
		else{
			//counters
			countEmpMap = new map<string, map<string,visitAvg>>();
			countDepMap = new map<string, visitAvg>();
			totalVisits = 0;
			totalAllVisits = 0;
			String DatefromDate;
			String DatetoDate;
			if(selectedPeriod=='PERIOD'){
				DatefromDate =  IPFunctions.FormatSqlDateIni(fromDate.Expected_Travel_Date__c);
				DatetoDate = IPFunctions.FormatSqlDateFin(toDate.Expected_Travel_Date__c);
			}

			//Maps and Lists
		 	list<ClientVisit> rClientVisit = new list<ClientVisit>();

		 	String sql= ' Select Parent_Visit__c, Department__r.Agency__r.Name, Department__r.Name, Contact__r.Total_Visits__c, Contact__r.Email, Accepted_Date__c, Close_Date__c, Completed_with_parent__c, '+
		 				' CreatedDate, Status__c,Service__c, Contact__c, Contact__r.Name, Closed_By__c, Closed_By__r.Name , Duration_Seconds__c, Waiting_Time_Seconds__c, Cancel_Reason__c, Comments__c ' +
						' FROM Visit__c ';

		 	String sqlWhere= ' WHERE Department__r.Show_Visits__c = true ';

		 	if(selectedUser.size() > 0){
		 		sqlWhere+= ' AND Closed_By__c in :selectedUser ';
		 	}else if(selectedDepartment!='all'){
		 		sqlWhere+= ' AND Department__c = \''+ selectedDepartment + '\' ';
		 	}else if(selectedAgency!='all'){
		 		sqlWhere+= ' AND Department__r.Agency__c= \''+ selectedAgency + '\' ';
		 	}

		 	if(selectedStatus=='completed'){
		 		sqlWhere+= ' AND Status__c= \'Completed\' ';
		 	}else if(selectedStatus=='cancelled'){
		 		sqlWhere+= 'AND Status__c= \'Cancelled\' ';
		 	}

		 	if(selectedPeriod!='PERIOD'){
		 		sqlWhere+= ' AND CreatedDate= '+ selectedPeriod ;
		 	}else{
		 		Date ini = fromDate.Expected_Travel_Date__c;
		 		Date fin = toDate.Expected_Travel_Date__c;
		 		system.debug('Data Ini==>' + ini);
		 		system.debug('Data fin==>' + fin);
		 		sqlWhere+= ' AND DAY_ONLY(convertTimezone(CreatedDate)) >= ' + DatefromDate + ' AND DAY_ONLY(convertTimezone(CreatedDate)) <= ' + DatetoDate;
		 	}



		 	if(selectedService!='all'){
		 		sqlWhere+= ' AND Service__c= \''+ selectedService + '\' ';
		 	}

		 	sql+=sqlWhere;

		 	system.debug('Main SQL: ' + sql);

		 	set<Id> visitIds = new set<id>();
		 	map <string, list<ClientVisit>> visitAttMap = new map <string, list<ClientVisit>>();

		 	/* Put values on the ClientVisit List  */

			mobileNumbers = new Map<String, String>();
			timeinfo = new Map<String, String[]>();

			if(isExport){

				sql+= ' ORDER BY Department__r.Agency__c, Department__r.Name, Closed_By__r.name, Service__c, Contact__r.Name, CreatedDate ';

				exportVisits = Database.query(sql);

				for(Visit__c att : Database.query(sql)){
					mobileNumbers.put(att.Contact__c, '');
					timeinfo.put(att.id, new String[]{ getReadableTime(att.Duration_Seconds__c), getReadableTime(att.Waiting_Time_Seconds__c) } );
				}

				Set<String> contactids = mobileNumbers.keySet();
				String sqlfoc = 'select detail__c, Contact__c from forms_of_contact__c where Contact__c in :contactids and Type__c = \'Mobile\' ';
				if( userDetails.Contact.Account.BillingCountry != null )
						sqlfoc += ' and Country__c = \'' +userDetails.Contact.Account.BillingCountry + '\'';
				sqlfoc += ' order by Contact__c';

				for(forms_of_contact__c foc : Database.query(sqlfoc))
					mobileNumbers.put(foc.Contact__c, mobileNumbers.get(foc.Contact__c) != '' ? mobileNumbers.get(foc.Contact__c) + ', '  + foc.Detail__c : foc.Detail__c);


			} else {

				sql+= ' ORDER BY CreatedDate ';
			 	for(Visit__c att : Database.query(sql)){
					mobileNumbers.put(att.Contact__c, '');
			 		ClientVisit	c = new ClientVisit();
					if(att.Accepted_Date__c != null)
		 				c.acceptedDate = att.Accepted_Date__c.format();
					if(att.Close_Date__c != null)
						c.closeDate = att.Close_Date__c.format();
					c.comments = att.Comments__c;
			 		c.agencyName = att.Department__r.Agency__r.Name;
			 		c.deptName = att.Department__r.Name;
					c.serviceName = att.Service__c;
					c.idVisit = att.Id;
					c.clientId = att.Contact__c;
					c.clientName = att.Contact__r.Name;
					c.clientEmail = att.Contact__r.Email;
					c.clientTotVisits = string.valueOf(att.Contact__r.Total_Visits__c);
					c.status = att.Status__c;
					c.cancelReason = att.Cancel_Reason__c;
					c.compHourDuration = att.Duration_Seconds__c;
					c.compHourWaiting = att.Waiting_Time_Seconds__c;

					if(att.Parent_Visit__c==null || att.Parent_Visit__c == '')
						totalAllVisits ++;

					/* Put on result map */
					if(!resultMap.containsKey(c.agencyName)){
						map<string, list<ClientVisit>> m1 = new map<string, list<ClientVisit>>{att.Closed_By__r.Name => new list<ClientVisit>{c}};
						map<String, map<string, list<ClientVisit>>> m2 = new map<String, map<string, list<ClientVisit>>>{c.deptName => m1};
						resultMap.put(c.agencyName, m2);
					}else if(!resultMap.get(c.agencyName).containsKey(c.deptName)){
						map<string, list<ClientVisit>> m1 = new map<string, list<ClientVisit>>{att.Closed_By__r.Name => new list<ClientVisit>{c}};
						map<String, map<string, list<ClientVisit>>> m2 = new map<String, map<string, list<ClientVisit>>>{c.deptName => m1};
						resultMap.get(c.agencyName).put(c.deptName, m1);
					}else if(!resultMap.get(c.agencyName).get(c.deptName).containsKey(att.Closed_By__r.Name)){
						resultMap.get(c.agencyName).get(c.deptName).put(att.Closed_By__r.Name, new list<ClientVisit>{c});
					}else{
						resultMap.get(c.agencyName).get(c.deptName).get(att.Closed_By__r.Name).add(c);
					}

					/* Calculates staff average */
					visitAvg va = new visitAvg();
					if(!countEmpMap.containsKey(c.deptName)){
						va.visitDuration = att.Duration_Seconds__c;
						va.visitCount=1;
						va.totalAverage = (va.visitDuration / va.visitCount);
						va.waitingDuration = att.Waiting_Time_Seconds__c;
						va.totalAverageWaiting = (va.waitingDuration / va.visitCount);

						countEmpMap.put(c.deptName, new map<string,visitAvg>{att.Closed_By__r.Name => va});
					}else if(!countEmpMap.get(c.deptName).containsKey(att.Closed_By__r.Name)){
						va.visitDuration = att.Duration_Seconds__c;
						va.visitCount=1;
						va.totalAverage = (va.visitDuration / va.visitCount);
						va.waitingDuration = att.Waiting_Time_Seconds__c;
						va.totalAverageWaiting = (va.waitingDuration / va.visitCount);

						countEmpMap.get(c.deptName).put(att.Closed_By__r.Name,va);
					}else{
						va = countEmpMap.get(c.deptName).get(att.Closed_By__r.Name);
						va.visitCount= va.visitCount+1;
						if(va.visitDuration!=null && att.Duration_Seconds__c !=null){
							va.visitDuration = va.visitDuration + att.Duration_Seconds__c;
							va.totalAverage = (va.visitDuration / va.visitCount);
							va.waitingDuration = va.waitingDuration + att.Waiting_Time_Seconds__c;
							va.totalAverageWaiting = (va.waitingDuration / va.visitCount);
						}
						else if(va.visitDuration==null && att.Duration_Seconds__c !=null){
							va.visitDuration = att.Duration_Seconds__c;
							va.totalAverage = (va.visitDuration / va.visitCount);
							va.waitingDuration = att.Waiting_Time_Seconds__c;
							va.totalAverageWaiting = (va.waitingDuration / va.visitCount);
						}
						countEmpMap.get(c.deptName).put(att.Closed_By__r.Name, va);
					}
			 	} //end for

			 	totalServices = 0;
				for(string dept : countEmpMap.keyset()){
					for(string emp : countEmpMap.get(dept).keyset()){

						system.debug('emp====' + emp);
						//Get the emp avg
						visitAvg empData = countEmpMap.get(dept).get(emp);
						//Put avg on dept
						if(!countDepMap.containsKey(dept)){
							visitAvg depData = new visitAvg();
							depData.visitCount = empData.visitCount;
							depData.visitDuration = empData.visitDuration;
							depData.waitingDuration = empData.waitingDuration;
							if(depData.visitDuration !=null){
								depData.totalAverage = (depData.visitDuration / depData.visitCount);
							}
							countDepMap.put(dept, depData);

						}else{
							//get actual avg and sum with the new one
							visitAvg totAvg = countDepMap.get(dept);
							totAvg.visitCount = totAvg.visitCount + empData.visitCount;
							if(empData.visitDuration!=null && totAvg.visitDuration !=null){
								totAvg.visitDuration = totAvg.visitDuration + empData.visitDuration;
								totAvg.waitingDuration = totAvg.waitingDuration + empData.waitingDuration;
								totAvg.totalAverage = (totAvg.visitDuration / totAvg.visitCount);
							}
							else if(empData.visitDuration!=null && totAvg.visitDuration ==null){
								totAvg.visitDuration = empData.visitDuration;
								totAvg.waitingDuration = totAvg.waitingDuration + empData.waitingDuration;
								totAvg.totalAverage = (totAvg.visitDuration / totAvg.visitCount);
							}
							countDepMap.put(dept, totAvg);
						}
					}//end for
					//Total Of All Dept Together
					system.debug('countDepMap====' + countDepMap);
					visitAvg v = countDepMap.get(dept);
					totalServices = totalServices + v.visitCount;
				}// end for


				//Calculate Average per worked days ALL DEPTS TOGETHER
				if(selectedPeriod.equals('PERIOD')){
					Date startDate = date.newinstance(fromDate.Expected_Travel_Date__c.year(), fromDate.Expected_Travel_Date__c.month(), fromDate.Expected_Travel_Date__c.day());
					Date endDate = date.newinstance(toDate.Expected_Travel_Date__c.year(), toDate.Expected_Travel_Date__c.month(), toDate.Expected_Travel_Date__c.day());
					workingDays = getWorkingDays(startDate, endDate);
				}else if(selectedPeriod.equals('TODAY')){
					Date startDate = Date.today();
					Date endDate = Date.today();
					workingDays = getWorkingDays(startDate, endDate);
				}
				else if(selectedPeriod.equals('THIS_WEEK')){
					Date startDate = Date.today().toStartOfWeek();
					Date endDate = Date.today();
					system.debug('Start Day:' + startDate);
					system.debug('End Day:' + endDate);
					workingDays = getWorkingDays(startDate, endDate);
				}
				else if(selectedPeriod.equals('THIS_MONTH')){
					Date startDate = Date.today().toStartOfMonth();
					Date endDate = Date.today();
					system.debug('Start Day:' + startDate);
					system.debug('End Day:' + endDate);
					workingDays = getWorkingDays(startDate, endDate);
				}
				else if(selectedPeriod.equals('LAST_MONTH')){
					Date startDate = Date.today().toStartOfMonth().addMonths(-1);
					Date endDate = date.newinstance(startDate.year(), startDate.month(), date.daysInMonth(startDate.year(), startDate.month()));
					system.debug('Start Day:' + startDate);
					system.debug('End Day:' + endDate);
					workingDays = getWorkingDays(startDate, endDate);
				}

				Set<String> contactids = mobileNumbers.keySet();
				String sqlfoc = 'select detail__c, Contact__c from forms_of_contact__c where Contact__c in :contactids and Type__c = \'Mobile\' ';
				if( userDetails.Contact.Account.BillingCountry != null )
						sqlfoc += ' and Country__c = \'' +userDetails.Contact.Account.BillingCountry + '\'';
				sqlfoc += ' order by Contact__c';

				for(forms_of_contact__c foc : Database.query(sqlfoc))
					mobileNumbers.put(foc.Contact__c, mobileNumbers.get(foc.Contact__c) != '' ? mobileNumbers.get(foc.Contact__c) + ', '  + foc.Detail__c : foc.Detail__c);


			}
		}


		if(resultMap.isEmpty() || resultMap.size()<1){
			showMsg = true;
		}else{
			showMsg = false;
		}
	 }


	/********** Get Working Days **********/

	// array of seven boolean indicating working days, Monday is index 0 */
	private static final List<Boolean> isWorkingDay;

	// count of the number of working days in the array */
	private static final Integer workingDaysInWeek;
	static {
	    isWorkingDay = new List<Boolean> { true, true, true, true, true, false, false };
	    workingDaysInWeek = 5; //Has to be the same number as the 'true' above
	}

	private static final Date monday = Date.newInstance(1900, 1, 1);
	private static Integer getDayOfWeek(Date value) {
	    return Math.mod(monday.daysBetween(value), 7);
	}

	public static Integer getWorkingDays(Date startDate, Date endDate) {
	    //save some calculations when the number of working days is 0
	    if(workingDaysInWeek == 0 || startDate == null || endDate == null) {
	        return 0;
	    } else {
	        Integer difference = startDate.daysBetween(endDate);
	        if(difference == 0) {
	            //If the 2 dates are the same day check if the day is a working day or not
	            return isWorkingDay[getDayOfWeek(startDate)] ? 1 : 0;
	        } else if(workingDaysInWeek == 7) {
	            //when every day is a working day return the difference
	            return difference;
	        } else {
	            //The guts of the solution
	            Integer wholeWeeks = Math.floor(difference / 7).intValue();
	            Integer workingDays = wholeWeeks * workingDaysInWeek;
	            Integer dayOfWeek = getDayOfWeek(endDate);
	            for(Integer remainder = Math.mod(difference, 7); remainder >= 0; remainder--) {
	                if(isWorkingDay[remainder]) {
	                    workingDays++;
	                }
	                dayOfWeek--;
	                if(dayOfWeek < 0) {
	                    dayOfWeek = 6;
	                }
	            }
	            return workingDays;
	        }
	    }
	}

	/********** Filters Behaviour **********/

	public void changeAgencyGroup(){
		agencyOptions = null;
		selectedAgency='all';

		departments = null;
		selectedDepartment = 'all';

		services = null;
		selectedService = 'all';

		userOptions = null;
		selectedUser = null;
	}

	public void changeAgency(){
		departments = null;
		selectedDepartment = 'all';

		services = null;
		selectedService = 'all';

		userOptions = null;
		selectedUser = null;
	}

	public void changeDepartment(){
		services = null;
		selectedService = 'all';

		userOptions = null;
		selectedUser = null;
	}

	/********** Filters **********/
	public List<SelectOption> status {
		get{
		if(status == null){
			status = new List<SelectOption>();
			status.add(new SelectOption('Completed', 'Completed'));
			status.add(new SelectOption('Cancelled', 'Cancelled'));
		}
		return status;
	} set; }

	public List<SelectOption> periods {
		get{
		if(periods == null){
			periods = new List<SelectOption>();
			periods.add(new SelectOption('PERIOD', 'Period'));
			periods.add(new SelectOption('TODAY', 'Today'));
			periods.add(new SelectOption('THIS_WEEK', 'This Week'));
			periods.add(new SelectOption('THIS_MONTH', 'This Month'));
			periods.add(new SelectOption('LAST_MONTH', 'Last Month'));
		}
		return periods;
	} set; }

	public List<SelectOption> services {
		get{
		if(services == null){

			services = new List<SelectOption>();
			services.add(new SelectOption('all', '--All--'));

			List<String> serv;

			if(selectedDepartment!='all')
				serv = new List<String>{[Select Services__c from Department__c where id = :selectedDepartment].Services__c};
			else if (selectedAgency!='all')
				serv = new List<String>{[Select Services__c from Account where id = :selectedAgency].Services__c};
			else{
				serv = new List<String>();
				List<Account> result = [Select Id, name, Services__c from Account where id in :agenciesId];
				for(Account a : result){
					if(a.Services__c != null && a.Services__c != '')
						serv.add(a.Services__c);
				}
			}

			if(serv!= null && serv.size()>0){
				set<String> sv = new set<String>();
				for(String s : serv){
					if(s!=null)
						sv.addAll(s.split(';'));
				}

				for(String s : sv){
					services.add(new SelectOption(s, s));
				}
			}
		}
		return services;
	} set; }

	public List<SelectOption> userOptions {
		get{
		if(userOptions == null){
			userOptions = new List<SelectOption>();

			if(selectedDepartment != null && selectedDepartment != 'all'){
				for(User ac : [SELECT Contact.AccountId, Name, Id FROM User WHERE Contact.Department__c = :selectedDepartment and isActive = true and Contact.Chatter_Only__c = false order by name]){
					userOptions.add(new SelectOption(ac.Id, ac.Name));
				}
			}
			else if(selectedAgency != null && selectedAgency != 'all'){
				for(User ac : [SELECT Contact.AccountId, Name, Id FROM User WHERE Contact.AccountId = :selectedAgency and isActive = true and Contact.Chatter_Only__c = false order by name]){
					userOptions.add(new SelectOption(ac.Id, ac.Name));
				}
			}
			else if(selectedAgency != null && selectedAgency == 'All'){
				for(User ac : [SELECT Contact.AccountId, Name, Id FROM User WHERE Contact.AccountId in :agenciesId and isActive = true and Contact.Chatter_Only__c = false order by name]){
					userOptions.add(new SelectOption(ac.Id, ac.Name));
				}
			}
		}
		return userOptions;
	} set; }

	public List<SelectOption> departments {
		get{
		if(departments == null){
			departments = new List<SelectOption>();
			departments.add(new SelectOption('all', '--All--'));
			for(Department__c ag : [Select Id, Name from Department__c A where Agency__c = :selectedAgency order by Name]){
				departments.add(new SelectOption(ag.Id, ag.Name));
			}
		}
		return departments;
	} set; }

	public List<SelectOption> agencyOptions {
		get{
		if(agencyOptions == null){
			agencyOptions = new List<SelectOption>();
			agencyOptions.add(new SelectOption('all', '--All--'));
			agenciesId = new Set<ID>();
			for(Account ag : [Select Id, Name from Account A where ParentId = :selectedAgencyGroup order by Name]){
			agencyOptions.add(new SelectOption(ag.Id, ag.Name));
			agenciesId.add(ag.Id);
			}
		}
		return agencyOptions;
	} set; }

	public List<SelectOption> agencyGroupOptions {
		get{
		if(agencyGroupOptions == null){
			agencyGroupOptions = new List<SelectOption>();
			//agencyGroupOptions.add(new SelectOption('', 'Select Agency Group'));
			for(Account ag : [select id, name from Account where RecordType.Name = 'Agency Group'  order by Name]){
				agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
			}
		}
		return agencyGroupOptions;
	} set; }


	/*public List<SelectOption> CountryOptions{
		get{
			if(CountryOptions == null){
				CountryOptions = IPFunctions.getAllCountries();
				CountryOptions.add(0, new SelectOption('all','--All--'));
			}
			return CountryOptions;
		} set; }*/

	public User userDetails{
		get{
			if (userDetails == null)
				userDetails = [SELECT Contact.AccountId, Contact.Account.Name, Contact.Account.ParentId, Contact.Account.BillingCountry, Name,Contact.MailingCountry, Contact.Export_Report_Permission__c FROM User WHERE id = :UserInfo.getUserId()];
			return userDetails;
		} set; }

	public Contact fromDate {
		get{
			if(fromDate == null){
				fromDate = new Contact();
				fromDate.Expected_Travel_Date__c = system.today();
			} return fromDate; } set; }

	public Contact toDate {
		get{
			if(toDate == null){
				toDate = new Contact();
				toDate.Expected_Travel_Date__c = system.today();
			} return toDate; } set; }


	//EXPORT METHODS
	private boolean isExport = false;
	public List<Visit__c> exportVisits {get;set;}
	public Map<String, String> mobileNumbers {get;set;}
	public Map<String, String[]> timeinfo {get;set;}
	public void search(){
		isExport = true;
		searchClientVisits();
	}
	public PageReference generateExcel(){
		PageReference pr = Page.report_visits_excel;
		pr.setRedirect(false);
		return pr;
	}

	public String xlsHeader {
        get {
            String strHeader = '';
            strHeader += '<?xml version="1.0"?>';
            strHeader += '<?mso-application progid="Excel.Sheet"?>';
            return strHeader;
        }
    }

	public String getReadableTime(Decimal timeInSeconds){
		if(timeInSeconds != null && timeInSeconds > 0){
			Integer hours = Integer.valueOf(timeInSeconds / 3600);
			Integer minutes = Integer.valueof(Math.mod((Integer) timeInSeconds, 3600) / 60);
			Integer seconds = Math.mod((Integer) timeInSeconds, 60);
			return String.format('{0}:{1}:{2}', new List<String>{ twoDigitString(hours), twoDigitString(minutes), twoDigitString(seconds) });
		} else {
			return '';
		}
	}

	private String twoDigitString(Integer theint) {
	    if (theint == 0)
	        return '00';

	    if (theint / 10 == 0)
	        return '0' + theint;

	    return String.valueOf(theint);
	}

}