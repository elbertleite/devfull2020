/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class extNationalityMix_test {

	static testMethod void verifyNationalityMix() {
    	
    	Map<String,String> recordTypes = new Map<String,String>();
       
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
			
		Account acc = new Account();
        acc.RecordTypeId = recordTypes.get('Agency');
        acc.Name = 'IP Sydney'; 
        insert acc;
        
    	
    	Nationality_Mix__c nm = new Nationality_Mix__c();
    	nm.Account__c = acc.id;
    	nm.Percentage__c = 10;
    	nm.Nationality__c = 'South America';
    	insert nm;
    	
    	nm = new Nationality_Mix__c();
    	nm.Account__c = acc.id;
    	nm.Percentage__c = 25;    	
    	nm.Nationality__c = 'Australia';
    	insert nm;
    	
    	Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(acc);
    	extNationalityMix enm = new extNationalityMix(controller);
    	Nationality_Mix__c newNationalityMixCt = enm.newNationalityMixCt;
    	Nationality_Mix__c editNationalityMixCt = enm.editNationalityMixCt;
    	List<Nationality_Mix__c> getNationalityMix = enm.getNationalityMix();
    	   
    	Apexpages.currentPage().getParameters().put('editMix', nm.id);
    	enm.editNationalityMixCt();
    	enm.newNationalityMixCt.Nationality__c = 'Europe';
    	enm.newNationalityMixCt.Percentage__c = 10;
    	enm.editNationalityMixCt.Percentage__c = 25;
    	enm.addNationalityMixCt();
    	enm.saveeditNationalityMixCt(); 	
    	enm.cancelEdit();
    	
    	String gLabels = enm.getGLabels();
    	String gValues = enm.getGValues();
    	String gTooltips = enm.getGTooltips();
    	
    	
    	enm.del();
    	
    	
    }
	
}