public without sharing class pmc_schools {
   public String instTypes {get;set;}
	public String courseTypes {get;set;}
	public String profileTypes {get;set;}
	public pmc_schools(){	

		profileTypes();
		courseTypes();
		institutionType();
	}

	private void profileTypes(){
		Schema.DescribeFieldResult fieldResult = Account.School_campus_category__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

		list<IPFunctions.opWrapper> result = new list<IPFunctions.opWrapper>();

		for( Schema.PicklistEntry f : ple)
			result.add(new IPFunctions.opWrapper(f.getValue(), f.getLabel()));

		result.add(0, new IPFunctions.opWrapper('all', 'All'));

		profileTypes = JSON.serialize(result);

		system.debug('PROFILES TYPES '+profileTypes);
	}

	private void courseTypes(){

		Schema.DescribeFieldResult fieldResult = Account.Course_Types_Offered__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

		list<IPFunctions.opWrapper> result = new list<IPFunctions.opWrapper>();

		for( Schema.PicklistEntry f : ple)
			result.add(new IPFunctions.opWrapper(f.getValue(), f.getLabel()));

		result.add(0, new IPFunctions.opWrapper('all', 'All'));

		courseTypes = JSON.serialize(result);

	}

	private void institutionType(){

		Schema.DescribeFieldResult fieldResult = Account.Type_of_Institution__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

		list<IPFunctions.opWrapper> result = new list<IPFunctions.opWrapper>();

		for( Schema.PicklistEntry f : ple)
			result.add(new IPFunctions.opWrapper(f.getValue(), f.getLabel()));

		result.add(0, new IPFunctions.opWrapper('all', 'All'));

		instTypes = JSON.serialize(result);

	}
	
	// SEARCH FIELDS
	public string sShow {get{if(sShow == null) sShow = 'all'; return sShow;} set;}
	
	public class countriesNames{
		public string name {get; set;}
		public integer position {get;set;}
		public integer schoolsPerCountry {get; set;}
		public string nameFormated {get; set;}
	}
	
	public map<string, integer> mp;
	
	public String name {get{if(name==null)name='';return name;}set;}
	
	public static string country{get; set;}

	public list<countriesNames> lcountry {get{if (lcountry == null) getCountries(); return lcountry;} set;}
	public List<SelectOption> getCountries() {
		
		if(mp == null){
			mp = new map<string, integer>();
		}
		
		countriesNames ct = new countriesNames();
		list<countriesNames> lct = new list<countriesNames>();
		Map<integer, countriesNames> theMap = new Map<integer, countriesNames>();
		lcountry = new list<countriesNames>();
		set<string> scountry = new set<string>();
		integer count = 14;
		boolean toAddPosition = true;
		for (AggregateResult a : [Select Supplier__r.BillingCountry ct from Supplier__c WHERE Record_Type__c = 'campus' and available__c = true group by Supplier__r.BillingCountry order by Supplier__r.BillingCountry])
			scountry.add((string)a.get('ct'));
		for(string s:scountry){ 
			ct = new countriesNames();
			ct.name = s;
			ct.schoolsPerCountry = mp.get(s);
			s = s.replaceAll(' ','_');
			ct.nameFormated = s;
			
			if(ct.nameFormated == 'Australia')
				ct.position = 0;
			else if(ct.nameFormated == 'New_Zealand')
				ct.position = 1;
			else if(ct.nameFormated == 'Canada')
				ct.position = 2;
			else if(ct.nameFormated == 'United_States')
				ct.position = 3;
			else if(ct.nameFormated == 'United_Kingdom')
				ct.position = 4;
			else if(ct.nameFormated == 'Ireland')
				ct.position = 5;
			else if(ct.nameFormated == 'South_Africa')
				ct.position = 6;
			else if(ct.nameFormated == 'Denmark')
				ct.position = 7;
			else if(ct.nameFormated == 'Sweden')
				ct.position = 8;
			else if(ct.nameFormated == 'Malta')
				ct.position = 9;
			else if(ct.nameFormated == 'Germany')
				ct.position = 10;
			else if(ct.nameFormated == 'Spain')
				ct.position = 11;
			else if(ct.nameFormated == 'Italy')
				ct.position = 12;
			else if(ct.nameFormated == 'Netherlands')
				ct.position = 13;
			else{
				theMap.put(count, ct);
				count++;
				toAddPosition = false;
			}
			
			if(toAddPosition)
				theMap.put(ct.position, ct);
			toAddPosition = true;
		}
		
		List<Integer> theList = new List<Integer>();
		for(Integer i : theMap.keySet())
			theList.add(i);
		
		theList.sort();
		
		for(Integer i : theList)
			lcountry.add(theMap.get(i));
		
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('','--None--'));
		for (String ld:scountry)
			options.add(new SelectOption(ld,ld));
		return options;
	}
	
	public static String state{get;set;}
	private static String findStates() {

		List<IPFunctions.opWrapper> options = new List<IPFunctions.opWrapper>{new IPFunctions.opWrapper('','--All--')};

		for(AggregateResult a : [Select Supplier__r.billingState bs  from Supplier__c A where Record_Type__c = 'campus' and Available__c = true and Supplier__r.BillingCountry = :country Group by Supplier__r.billingState order by Supplier__r.billingState])
			options.add(new IPFunctions.opWrapper((string)a.get('bs'),(string)a.get('bs')));

		return JSON.serialize(options);
	}

	@RemoteAction
	public static String changeState(String selCountry, String selState){
		country = selCountry;
		state = selState;
		return findCities();
	} 
	
	public string city{get;set;}
	
	private static String findCities() {
		
		List<IPFunctions.opWrapper> options = new List<IPFunctions.opWrapper>{new IPFunctions.opWrapper('','--All--')};
		
		if(country != null && country != 'all'){
			String Sql = 'Select Supplier__r.billingCity ct from Supplier__c where Available__c = true and Supplier__r.billingCountry = \'' + country + '\' ';
			
			if(state != null && state != '')
				Sql += ' and Supplier__r.billingState = \'' + state + '\'';
			
			Sql += ' and Record_Type__c = \'Campus\' Group by Supplier__r.billingCity order by Supplier__r.billingCity '; //and Supplier__r.Disabled_Campus__c = false 

			Map<string,string> lCity = new Map<string,string>();
			for (AggregateResult a :Database.query(sql)){
				lCity.put((string)a.get('ct'), (string)a.get('ct'));
				options.add(new IPFunctions.opWrapper((string)a.get('ct'),(string)a.get('ct')));
			}		
		}
		
		return JSON.serialize(options);
	}
	
	
	public class schoolCampi{
		public Account school {get; set;}
		public integer schoolDocuments {get; set;}
		public integer campusDocuments {get; set;}
		public Agreement_Accounts__c schoolAgree {get;set;}
		public String parentName {get;set;}
		public list<Agreement_Accounts__c> otherAgree {get;set;}
		public map<string, list<string>> nationalityGroup {get;set;}
		public String [] courseTypesOffered {get;set;}
		public boolean isNew {get;set;}
		public double rank {get; set;}
		public string Relationship {get; set;}
		public boolean isAgencyRanked {get{if(isAgencyRanked == null)isAgencyRanked = false;return isAgencyRanked;}set;}
		public List<Account> campuses {
			get{
			if (campuses == null)
				campuses = new List<Account>();
			return campuses;
		}set;}
	}
	

	
	public static List<schoolCampi> selectedSchools {get{if(selectedSchools == null)selectedSchools = new List<schoolCampi>();return selectedSchools;}set;}
	//public static User userDetails {get{if(userDetails == null) userDetails =  [Select Contact.AccountId, Contact.Account.Global_Link__c, Contact.View_Basic_Agreement__c, Contact.View_Full_Agreement__c  from user where id = :UserInfo.getUserId() limit 1]; return userDetails;}set;}

	@RemoteAction
	public static String findSchools(Integer sqlOffset, Integer pageLimit, String selCountry, String selState, String selCity, String name, String selectedType, String selectedAvailability, String selInstType, String selCourseType) {
		system.debug('sqlOffset===>' + sqlOffset);
		system.debug('pageLimit===>' + pageLimit);
		
		country = selCountry;
		state = selState;
		
		set<string> listSuppliers = new set<string>();
		set<Id> allSchools = new set<Id>();
		
		String sqlSupplier;

		
        sqlSupplier = 'SELECT Id, ParentId FROM Account WHERE RecordType.Name = \'campus\' '; 
        
        if(selCountry != null && selCountry != 'all')
            sqlSupplier += ' and BillingCountry = \'' + selCountry + '\' ';
        if(selState != null && selState != '')
            sqlSupplier += ' and billingState = \'' + selState + '\'';
        if(selCity != null && selCity != '')
            sqlSupplier += ' and BillingCity = \'' + selCity + '\'';
        if(name != null && name != ''){
            String param = '%'+name+'%';
            sqlSupplier += ' and (Parent.Name like \'' + param + '\' or name like \'' + param + '\' or Parent.Parent.Name like \'' + param + '\') ';
        }

        //ShowCaseOnly__c
		if(selectedType != 'all'){
            sqlSupplier += ' and School_campus_category__c  = :selectedType';

		}
        // if(selectedType == 'full' || selectedType == 'Full Profile'){
        //     sqlSupplier += ' and (ShowCaseOnly__c  = false OR School_campus_category__c  = :selectedType)';
        // }
        // else if(selectedType == 'showcase' || selectedType == 'Showcase Only'){
        //     sqlSupplier += ' and (ShowCaseOnly__c  = true OR School_campus_category__c  = :selectedType)';
        // }

        // Course Type
        if(selCourseType != 'all')
            sqlSupplier += ' and Parent.Course_Types_Offered__c Includes ( \''+ selCourseType + '\' ) ';

        // Institution type
        if(selInstType != 'all')
            sqlSupplier += ' and Parent.Type_of_Institution__c = :selInstType ';
            
        sqlSupplier += ' AND ParentId != null ORDER BY Name ';
        
        system.debug('SQL===>' + sqlSupplier);

        for(Account sp: Database.Query(sqlSupplier)){
            allSchools.add(sp.ParentId);
            // mSchoolAgree.put(sp.Supplier__r.ParentId, null);
            listSuppliers.add(sp.Id);
        }//end for

			
		// map<String, Agreement_Accounts__c> mSchoolAgree = new map<String, Agreement_Accounts__c>();
		set<Id> pagedSch = new set<Id>();
		map<String, Agreement_Accounts__c> agencyAgreement = new map<String, Agreement_Accounts__c>();
		map<String, list<Agreement_Accounts__c>> otherAgencies = new map<String, list<Agreement_Accounts__c>>();

		for(Account ac : [SELECT Id FROM Account WHERE Id in:allSchools order by Name limit :pageLimit offset :sqlOffset]){
			pagedSch.add(ac.Id);

			otherAgencies.put(ac.Id, new list<Agreement_Accounts__c>());
			agencyAgreement.put(ac.Id, null);
		}//end for

		system.debug('allSchools===>' + allSchools.size());
		system.debug('pagedSch===>' + pagedSch.size());

		//---------------- FIND SCHOOL AGREEMENTS --------------------- //
		
		
		//Find all agreements from the paged schools
		map<Id, set<Id>> schoolAgrees = new map<Id, set<Id>>();

		for(Agreement_Accounts__c aa : [SELECT Agreement__c, Account__c FROM Agreement_Accounts__c WHERE Account__c IN :pagedSch]){ // AND Agreement__r.isAvailable__c = true AND Agreement__r.isCancelled__c = false
			if(!schoolAgrees.containsKey(aa.Agreement__c))
				schoolAgrees.put(aa.Agreement__c, new set<Id>{aa.Account__c});
			else
				schoolAgrees.get(aa.Agreement__c).add(aa.Account__c);
		}
		

		//Find all agreements within the agency's network for the selected schools
		// for(Agreement_Accounts__c aa : [SELECT Agreement__c, Account__c, Account__r.Name, Account__r.ParentId, Account__r.Parent.Name, Agreement__r.Status_formula__c, Agreement__r.Renew_in_progress__c, Agreement__r.Renew_upon_request__c FROM Agreement_Accounts__c WHERE Agreement__c IN :schoolAgrees.keySet() AND Account__r.Global_Link__c = :userDetails.Contact.Account.Global_Link__c order by createdDate]){ // AND Agreement__r.isAvailable__c = true AND Agreement__r.isCancelled__c = false
		// 	if(aa.Account__c != userDetails.Contact.AccountId){

		// 		for(Id sch : schoolAgrees.get(aa.Agreement__c)){
					
		// 			if(otherAgencies.containsKey(sch))
		// 				otherAgencies.get(sch).add(aa);
		// 			else
		// 				otherAgencies.put(sch, new list<Agreement_Accounts__c>{aa});

		// 		}//end for
				
		// 	} 
		// 	else{
		// 		for(Id sch : schoolAgrees.get(aa.Agreement__c)){
		// 			agencyAgreement.put(sch, aa);
		// 		}//end for
		// 	}
		// }//end for
		
		
		//----------- END - SCHOOL AGREEMENTS -----------------------//

		//FIND RESULT
		string currSchool = '';
		schoolCampi sc;
		List<schoolCampi> ls = new List<schoolCampi>();
		Set<String> schools = new Set<String>();
		for(Account acco : [Select Id, Name, ParentId, Parent.Name, Parent.Parent.Name, Website, ShowCaseOnly__c, Parent.schoolDocuments__c, schoolDocuments__c, Parent.Website, CreatedDate,  BillingCountry, RecordType.Name, School_campus_category__c, Disabled_Campus__c, Parent.Course_Types_Offered__c from Account  where ParentId in :pagedSch order by Parent.Name, Name]){
			if(String.isEmpty(acco.School_campus_category__c)){
				acco.School_campus_category__c = '';
			}
			if(listSuppliers.contains(acco.Id)){
				if(acco.ParentId != null){
					schoolsInList.put(acco.Parent.id,acco.Parent.id);
					system.debug('currSchool===>' + currSchool);
					system.debug('acco.Parent.id===>' + acco.Parent.id);
					system.debug('COURSES OFFERED '+acco.Parent.Course_Types_Offered__c);
					if (currSchool != string.valueOf(acco.Parent.id)){
						sc = new schoolCampi();
						sc.school = acco.Parent;
						sc.schoolDocuments = integer.valueOf(acco.Parent.schoolDocuments__c);

						sc.campusDocuments = integer.valueOf(acco.schoolDocuments__c);
						sc.parentName = acco.Parent.Parent.Name;
						sc.courseTypesOffered = String.isEmpty(acco.Parent.Course_Types_Offered__c) ? new String[]{} : acco.Parent.Course_Types_Offered__c.split(';');
						sc.schoolAgree = agencyAgreement.get(acco.ParentId);
						sc.otherAgree  = otherAgencies.get(acco.ParentId);

						sc.campuses.add(acco);
						if(acco.createdDate > System.today().addDays(-3))
							sc.isNew = true;

						currSchool = acco.Parent.id;
						
						ls.add(sc);
					}else{
						if(acco.createdDate > System.today().addDays(-3))
							sc.isNew = true;
						sc.campuses.add(acco);
					}
				}				
			}
		}//end for

		result r = new result();
		// for(schoolCampi s : ls){
		// 		r.totSchools++;			
		// 	for(Account campus : s.campuses){
		// 			r.totCampus++;
		// 		if(campus.createdDate > System.today().addDays(-3))
		// 			s.isNew = true;
		// 	}//end for
		// }//end for

		map<string, map<string, list<string>>> natioGroup = findNationalityGroup();

		for(schoolCampi sch : ls){
			sch.nationalityGroup = natioGroup.get(sch.school.Id);
			r.schools.add(sch);
		}//end for

		system.debug('r.schools===>' + r.schools.size());

		r.states = findStates();
		r.cities = findCities();
		r.totSchools = allSchools.size();
		r.totCampus = listSuppliers.size();

		return JSON.serialize(r);
	}

	// @RemoteAction
	// public static String checkAgreement(String schoolId){

	// 	//Find the agreements within the selected school
	// 	String sql = 'SELECT Agreement__c FROM Agreement_Accounts__c WHERE Account__c = \''+ schoolId+ '\' and Agreement__r.isAvailable__c = true order by Agreement__r.Start_Date__c';
		
	// 	system.debug('sql ==>' + sql);

	// 	list<Id> agree = new list<Id>();
	// 	for(Agreement_Accounts__c aa : Database.query(sql))
	// 		agree.add(aa.Agreement__c);

	// 	//Find only agreement in the user's global link
	// 	String gbLink = [SELECT Contact.Account.Global_Link__c FROM User WHERE Id = :userInfo.getUserId()].Contact.Account.Global_Link__c;

	// 	List<Agreement_Accounts__c> result = [SELECT Account__r.Name, Account__r.Parent.Name, Agreement__r.Status_formula__c FROM Agreement_Accounts__c WHERE Agreement__c in :agree AND Account__r.Global_Link__c = :gbLink];

	// 	return JSON.serialize(result);
	// }

	private class result{
		private Integer totSchools {get;set;}
		private Integer totCampus {get;set;}
		private list<schoolCampi> schools {get;set;}
		private String states {get;set;}
		private String cities {get;set;}

		public result(){
			totSchools = 0;
			totCampus = 0;
			schools = new list<schoolCampi>();
		}
	}
	
	private map<string, map<string, list<string>>> schoolNationalityGroup;

	@RemoteAction
	public static List<IPClasses.PMCSchoolDocuments> retrieveSchoolDocuments(String schoolId){
		return PMCHelper.retrieveSchoolDocumentsTreeFormat(new List<String>{schoolId}, true);
	}

	@RemoteAction
	public static map<string, map<string, list<string>>> findNationalityGroup(){
		map<string, map<string, list<string>>>schoolNationalityGroup = new map<string, map<string, list<string>>>();
		
		for(schoolCampi sc : selectedSchools)
			schoolNationalityGroup.put(sc.school.id, new map<string, list<string>>());
		
		for(Nationality_Group__c ng:[Select Account__c, Country__c, Name from Nationality_Group__c where Account__c in :schoolsInList.keySet() order by Country__c]){
			if(!schoolNationalityGroup.containsKey(ng.Account__c))
				schoolNationalityGroup.put(ng.Account__c, new map<string,list<string>>{ng.name => new list<string>{ng.Country__c}});
			else if(!schoolNationalityGroup.get(ng.Account__c).containsKey(ng.name) )
				schoolNationalityGroup.get(ng.Account__c).put(ng.name, new list<string>{ng.Country__c});
			else schoolNationalityGroup.get(ng.Account__c).get(ng.name).add(ng.Country__c);
		}
		return schoolNationalityGroup;
	}
	
	public Integer totalSchools {get; set;}
	public Integer totalCampus {get; set;}
	
	
	public String linkTo {get; set;}
	public PageReference redirectTo() {
		String accountID = getParam('accountID');
		linkTo = getParam('LinkTo');
		PageReference providerPage;
        
		if(linkTo == null)
			providerPage = new PageReference('/' + accountID);
	            
		providerPage.setRedirect(true);
		return providerPage;
	}
	
	public String getParam(String name) {
		return ApexPages.currentPage().getParameters().get(name);   
	}
	
	//========================Search School Page==========================
	
	// public String IncListShowCases {
	// 	get{
	// 	if(IncListShowCases==null)
	// 		IncListShowCases = 'planet';
	// 	return IncListShowCases;
	// }set;}
	
	
	
	private static Map<String,String> schoolsInList {
		get{
		if(schoolsInList==null)
			schoolsInList = new Map<String,String>();
		return schoolsInList;
	}set;}
	
	private static String ListToString(list<String> sli)
	{
		integer i =0;
		String li = '';
		for (String s:sli){
			if(i>0)
				li += ',';
			li+= '\'' + s + '\'';
			i++;
		}

		li = '(' + li + ')';
		return li;
	}
	
	public String selectedType {get{if(selectedType == null) selectedType = 'all'; return selectedType;}set;}
	public list<SelectOption> typeOptions{get{
		if(typeOptions == null){
			typeOptions = new list<SelectOption>();
			typeOptions.add(new SelectOption('all', '-- All --'));
			typeOptions.add(new SelectOption('full', 'Full Profile'));
			typeOptions.add(new SelectOption('showcase', 'Showcase Only'));
		} 
		return typeOptions;
	}set;}


	@RemoteAction
	public static map<string, string> colorValue(){
		return IPFunctions.campusStatus(); 
	}
}