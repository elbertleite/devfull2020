public with sharing class contact_quotationEmail {
	
	  public Quotation__c quote{get;set;}
	  
	  public String attachmentLabel {
	  	get{
	  		return Label.Attachments;
	  	}
	  	set;
	  }
	  
	  public contact_quotationEmail(ApexPages.StandardController std){
	  	virtualId= 0;
	  	string quoteId = ApexPages.currentPage().getParameters().get('quoteId');

        quote = [Select id, Client__r.FirstName, Client__r.AccountId, Client__r.Preferable_Language__c, Client__r.Name, Client__c, CreatedBy.Name, 
        CreatedBy.Email, Client__r.Email , Expiry_date__c, LastModifiedById, LastModifiedDate, Name, Status__c, Client__r.remoteContactId__c from Quotation__c
        where id = :quoteId];    
	        
	    	mapFields = IPFunctions.findTokens(IPFunctions.tokensQuotation);
			list<sObject> listResult = emailTemplateDynamic.createListResult(' FROM QUOTATION__C WHERE id = \''+quote.ID +'\'', mapFields, null, new list<String>(mapFields.keySet()));	
			objTemplate = listResult[0];
			findEmailTemplate(userDetails.Accountid, 'Quotation');
	        
	  }
	  
	  public String Language {
	  	get{
	  		if(language == null)
	  			language = quote.Client__r.Preferable_Language__c != null ? quote.Client__r.Preferable_Language__c : 'en_US';
	  		return language;
	  	}
	  	set;
	  }
	  
	  public  Contact userDetails{
	    get{
	      if (userDetails == null){
	      	if(!Test.isRunningTest()){
	      		string contactId = [Select ContactId from user where id = :UserInfo.getUserId() limit 1].ContactId;
	       		userDetails = [select id, Name, Email, Signature__c, Account.Name, Account.ParentId, Account.Global_Link__c, Account.Email_Quotation_Option__c  from Contact where id = :contactId];
	      	} else {
	      		userDetails = new Contact();
	      	}
	        
	       
	      }
	      return userDetails;
	    }
	    set;
	  }
	    
	  //send email+
	  
	  public String textSignature {
	  	get{
	  		if(textSignature == null){
	  			textSignature = '<div style="margin-top: 20px;" class="sig">' + Quote.CreatedBy.Name;
	            textSignature += '<br />' + userDetails.Account.Name;
	            textSignature += '<br />' + Quote.CreatedBy.Email + ' </div>';  
	  		}
	  		return textSignature;
	  	}
	  	set;
	  }
	
	 public String communityName {
        get {
            return IPFunctions.getCommunityName();
        }
        private set;
      }

	  public string email {get;set;}
	    //get {
	    //   if(email == null){
	    //     email = '';
	    //     email +=Label.Dear+' '+quote.Client__r.FirstName + ',<br /><br />';
	    //     email += Label.Sending_quotation.replaceAll('\n', '<br />');
	        
		// 	//ADD Quotation Link



	    //     if(!Schema.sObjectType.Contact.fields.allowQuoteByEmail__c.isAccessible()){
	       
		//          email += '<br /><br />';
		         
		//           Pagereference pr = Page.xCourseSearchQuotation_html; //tudo
		// 	      pr.getParameters().put('id', quote.id);
		// 	      email += pr.getContent().toString();
			      
		         
		         
	    //     }
	                
	    //     if(useSignature && userDetails.Signature__c != null && userDetails.Signature__c != '' )
	    //       email += '<br /><div style="margin-top: 20px;" class="sig">'+getSignature()+'</div>';
	    //       else {
	    //         email += textSignature;	                    
	    //     }

		// 	if(userDetails.Account.Email_Quotation_Option__c == 'link' || userDetails.Account.Email_Quotation_Option__c == null)
		// 		email += '<br /><br />'+Label.Your_Quote+': <a style="color: black;text-decoration: none;font-size: 1.5em;border: solid 2px #c1c1c1;padding: 5px;border-radius: 4px;background: #eee;cursor: pointer; white-space: nowrap;" target="_blank" href="'+Url.getSalesforceBaseUrl().toExternalForm()+'/'+communityName+'/quotation?id='+quote.id+'" class="showCaseLink" >Quotation - '+quote.name+'</a><br /><br />';

	    //   }
	    //   return email;
	//     }
	//     set;
	//   }
	    
	  public boolean useSignature {get{if(useSignature == null) useSignature = true; return useSignature;}set;}
	    
	  public List<Document> documents {get{if(documents == null) documents = new List<Document>(); return documents;}set;}
	  private List<String> docIDs {get{if(docIDs == null) docIDs = new List<String>(); return docIDs;}set;}
	  public Document document {get{if(document == null) document = new Document(); return document;}set;}
	  public boolean sentEmailOwner {get;set;}
	  public boolean emailImportant {get;set;}
	  public string Bcc {get;set;}
	  public string Cc {get;set;}
	  public boolean showMsg {get{if(showMsg==null)showMsg = false;return showMsg;}set;} 
	  public string subject {get{if(subject == null) subject = 'QUOTE - '+ quote.name + ' - ' + quote.Client__r.Name; return subject;}set;}
	  public String EHFShortnerHost{ get { return IPFunctions.EHFShortnerHost; } private set; }
	 // account ac = [Select Email_Signature__c from account where User__r.id = :UserInfo.getUserId() limit 1]; 
	     
	 public string getSignature(){
	    if(useSignature && userDetails.Signature__c != null && userDetails.Signature__c != '' )
	      return userDetails.Signature__c;
	    else return textSignature;
	        
	  }
	      
	  public string quoteMsg{get;set;}
	  public boolean sent{ get{if(sent == null) sent = false; return sent;} set; }
	  
	  public PageReference sendEmailQuote(){
	  		
		    boolean first = true;
		    mandrillSendEmail mse = new mandrillSendEmail();
		    mandrillSendEmail.messageJson email_md = new mandrillSendEmail.messageJson();
		    system.debug('::::::::::::: sendEmailQuote:::::::::::::::');
		    
		    //create json to mail
		   email_md.setTo(quote.Client__r.Email, quote.Client__r.Name);
		   email_md.setTag(quote.Client__c);
		   email_md.setFromEmail(userDetails.email);
		   email_md.setFromName(userDetails.name);
		   
		   String emailService = IpFunctions.getS3EmailService();
		   //email_md.setHeaders('Reply-To',emailService +';'+ userDetails.email);
		   //email_md.setHeaders('Reply-To', emailService);
	       email_md.setSubject(subject); 
		        
		    //customer documents  
		    if (!files.isEmpty()) {
			    for(Client_Document_File__c cdf : files){
			       
			      if(cdf.Selected__c){  
			      	if(first){
		            email += '<br/><br/><b>Attachments:</b><br/>';
		            first = false;          
		          }
			        email += '<a href="'+cdf.Preview_Link__c+'" >' + cdf.File_Name__c + '</a> <i>('+cdf.File_Size__c+')</i> <br/>';
			      }      
			    }
		    }
		    
		    email_md.setHtml(email);
		        
		    system.debug('@ cc: ' + cc);
		    system.debug('@ bcc: ' + bcc);
		        
		    String[] toBcc;
		    String[] toCc;
		    if (Bcc != null && Bcc != ''){
		      Bcc = Bcc.replaceAll(',',';').replaceAll('\n',';');		
		      toBcc = Bcc.split(';', 0);
		      for(String b_cc : toBcc) email_md.setBcc(b_cc, '');
		    }       
		    if (Cc != null && Cc != ''){
		      Cc = Cc.replaceAll(',',';').replaceAll('\n',';');	 	
		      toCc = Cc.split(';', 0);
		      for(String c_c : toCc) email_md.setCc(c_c, '');
		    }  


		    if (sentEmailOwner)
		      email_md.setCc(userDetails.Email, '');
		      
		    email_md.setImportant(emailImportant);
		    
		    // attach quotations send attach
		    if(Schema.sObjectType.Contact.fields.allowQuoteByEmail__c.isAccessible() && (userDetails.Account.Email_Quotation_Option__c == 'pdf' || userDetails.Account.Email_Quotation_Option__c == null)){
			   	Pagereference pr = Page.clientquote;
			    pr.getParameters().put('id', quote.id);
			    if(!Test.isRunningTest()){
				    blob fileBody = pr.getContentAsPDF();			    
				    email_md.setAttachment('application/pdf', 'quote.pdf', fileBody);
			    }
		    }
			Contact ctt = [SELECT ID, Mandrill_Emails_Status__c FROM Contact WHERE ID = :quote.Client__c];
		    String emailID = saveToS3();
			email_md.setMetadata('hify_id', emailID);

			IPFunctions.updateContactsMandrillStatus(new List<Contact>{ctt}, emailID);
		    
		    system.debug('==========>>>> ' + email_md);
		    
		   mse.sendMail(email_md);
		    
			update ctt;

			sent = true;
				    
		    return null;
	}
	
	
	public PageReference redirectToTasks(){
		String newPageContact = ApexPages.currentPage().getParameters().get('page');
		PageReference pr = null;
		if(!String.isEmpty(newPageContact) && newPageContact == 'contactNewPage'){
			pr = Page.contact_new_quotations;
			pr.setRedirect(true);
			pr.getParameters().put('id', quote.Client__c);
			pr.getParameters().put('page', 'contactNewPage');
		}else{
			//pr = new Pagereference('/apex/contact_tasks');
			pr = Page.contact_new_quotations;
			pr.setRedirect(true);
			pr.getParameters().put('id', quote.Client__c);
			pr.getParameters().put('page', 'contactNewPage');
		}
	  return pr;
		
	} 		
	
	
		  
	  public String saveToS3(){
	  	
	  	EmailToS3Controller s3 = new EmailToS3Controller();
	    Datetime myDT = Datetime.now();
	    String myDate = myDT.format('dd-MM-yyyy HH:mm:ss');
	    //(Account client, Account employee, Boolean sendByEmployee, String subject, Blob body, Boolean hasAtt, String dateCreated
	    Blob body =  Blob.valueof(email);
	    String emailID = s3.generateEmailToS3(quote.Client__r, userDetails, true, subject, body, true, myDate);

		return emailID;
	  }
	    
	  public void selectFile(){
	    String fileID = ApexPages.currentPage().getParameters().get('fileID');
	    
	    if(!selectedFiles.containsKey(fileID)){     
	      for(Client_Document_File__c cdf : files)
	        if(fileID == cdf.Id){
	          selectedFiles.put(fileID, cdf);
	          break;
	        }
	    } else {
	      selectedFiles.remove(fileID);
	    }
	    
	  }
	  
	  private Folder folder {
	    get {
	      if(folder == null){	      	
        	folder = [select id from Folder where Name = 'Attachments' limit 1];	      
	      }
	      return folder;
	    }
	    set;
	  }
	    
	 	  
  	    
	  public void attach(){
	    document.FolderId = folder.id;
	    document.IsPublic = true;
	    insert document;
	        
	    docIDs.add(document.id);
	    document = new Document();
	        
	    getDocName();
	  }   
	    
	  public pageReference redirectQuotations(){
	    PageReference quotePage = new PageReference('/apex/Inc_clientQuotations?id=' + quote.Client__c);
	    quotePage.setRedirect(true);
	        
	    return quotePage;
	  }
	    
	  private void getDocName(){
	    documents = [select id,name,IsPublic,BodyLength from Document where id in :docIDs];
	  }
	        
	  public string getLanguageCode(){
	    return quote.Client__r.Preferable_Language__c; //this values are stored in the formula Preferable_Language__c
	  }
	    
	  public void reloadAttachments(){
	    getAttachments();
	  }
	    
	  public string getAttachments(){
	    boolean first = true;
	    string att = '';
	    System.debug('==> files: '+files);
	    
	    if(selectedFiles != null && selectedFiles.size() > 0)
	      for(String key : selectedFiles.keySet()){
	        
	        Client_Document_File__c cdf = selectedFiles.get(key);
	          
	        if(first){
	          att += '<br/><br/><b>Attachments:</b><br/>';
	          first = false;          
	        }
	        att += '<a href="'+cdf.Preview_Link__c+'" >' + cdf.File_Name__c + '</a> <i>('+cdf.File_Size__c+')</i> <br/>';
	        
	      }
	       
	   
	    return att;
	        
	  }
	    
	  public String docCategory {get;set;}
	  public List<SelectOption> categories {
	    get {
	      if(categories == null){
	        categories = new List<SelectOption>();
	        categories.add(new SelectOption('all', 'Select Category'));
	        List<AggregateResult> lar = [Select Doc_Document_Category__c from Client_Document_File__c where Client__c = :quote.Client__c and Doc_Document_Category__c != 'EmailAttachments' group by Doc_Document_Category__c];
	        for(AggregateResult ar : lar)
	          if(ar.get('Doc_Document_Category__c') != null)
	            categories.add(new SelectOption( (String) ar.get('Doc_Document_Category__c'), (String) ar.get('Doc_Document_Category__c') ) );
	      }
	      return categories;
	    }
	    set;        
	  }
	    
	  public void reloadTypes(){
	    types = null;
	    docType = null;
	    reloadFiles();
	  }
	    
	  public void reloadFiles(){
	    files = null;
	  }
	    
	  public String docType {get;set;}
	  
	  public List<SelectOption> types {
	    get {
	      if(types == null){
	        types = new List<SelectOption>();
	        types.add(new SelectOption('all', 'Select Type'));
	        List<AggregateResult> lar = [Select Doc_Document_Type__c from Client_Document_File__c where Client__c = :quote.Client__c and Doc_Document_Category__c = :docCategory group by Doc_Document_Type__c];
	        for(AggregateResult ar : lar)
	          types.add(new SelectOption( (String) ar.get('Doc_Document_Type__c'), (String) ar.get('Doc_Document_Type__c') ) );
	                
	        }
	        return types;
	    }
	    set;
	  }
	    
	  private Map<String,Client_Document_File__c> selectedFiles = new Map<String,Client_Document_File__c>();
	    
	  public List<Client_Document_File__c> files {
	    get{
	      if( files == null && quote.Client__c != null){
	        String sqlCat = '';
	        String sqlType = '';
	        if(docCategory != null && docCategory != '' && docCategory != 'all')
	          sqlCat = ' and Doc_Document_Category__c = \''+docCategory+'\'';
	                
	        if(docType != null && docType != '' && docType != 'all')
	          sqlType = ' and Doc_Document_Type__c = \''+docType+'\'';
	                
	        String sql = ' Select C.File_Name__c,Doc_Document_Type__c, Doc_Document_Category__c, C.File_Size__c, C.File_Size_in_Bytes__c, C.Preview_Link__c, CreatedDate, CreatedBy.Name, Selected__C, Client__c ' + 
	                     ' from Client_Document_File__c C where Client__c =\''+quote.Client__c+'\' and Doc_Document_Category__c != \'EmailAttachments\' ' + sqlCat + sqlType + ' order by Doc_Document_Category__c, Doc_Document_Type__c, CreatedDate desc';
	                
	        System.debug('@@@@@> ' + sql);
	        files = Database.query(sql);
	        
	        for(Client_Document_File__c cdf : files)          
	          if(selectedFiles.containsKey(cdf.id))
	            cdf.Selected__c = true;
	        
	        
	                
	      }
	      return files;
	    }
	    set;
	  }
	  
	  public string getOrgId(){
	    return  UserInfo.getOrganizationId();
	  }
	    
	  public void reloadEmailAttachments(){
	   
	    getAttachments();
	  }
	  
	/** EPs PC Files **/
	
	public list<userAttachments> lUserAttachments {get;set;}
	
	public class userAttachments{
		public Boolean isChecked {get;set;}
		public String fileName {get;set;}
		public String fileKey {get;set;}
	}
	
	public void addUserAttachment(){
		if(lUserAttachments == null)
			lUserAttachments = new List<userAttachments>();
			
		userAttachments file = new userAttachments();
		file.fileName = ApexPages.CurrentPage().getParameters().get('fileName');
		file.fileKey = ApexPages.CurrentPage().getParameters().get('fileKey');		
		file.isChecked = true;
		
		lUserAttachments.add(file);
		
		system.debug('lUserAttachment====' + lUserAttachments);	
	
	}
	
	/** END EPs PC Files **/  
	  
	public boolean hasDocuments {
		get{
			if(hasDocuments == null)
				getContactDocuments();
			return hasDocuments;
		}
		set;
	}  
	  
	private Map<String, List<IPFunctions.ContactDocument>> contactDocuments;
	public Map<String, List<IPFunctions.ContactDocument>> getContactDocuments(){ 
		hasDocuments = false;
  		if(contactDocuments == null){
  			AWSKeys credentials = new AWSKeys(awsCredentialName);
  			contactDocuments = new Map<String, List<IPFunctions.ContactDocument>>();
  			
  			List<IPFunctions.ContactDocument> cds = IPFunctions.getContactDocuments(quote.Client__c, credentials.key, credentials.secret); 
  			if(cds != null && !cds.isEmpty()){
  				hasDocuments = true;
	  			for( IPFunctions.ContactDocument cd : cds ){
	  				if(contactDocuments.containsKey(cd.docCategory)){
	  					contactDocuments.get(cd.docCategory).add(cd);
	  				} else
	  					contactDocuments.put(cd.docCategory,  new List<IPFunctions.ContactDocument>{cd} );
	  				
	  			}
  			}
  			
  			
  		}
  			
  		return contactDocuments;
  		
	}
	
	
	public class Doc {
        public String category {get;Set;}       
        public list<DocumentDetails> listDocumentDetails{get{if(listDocumentDetails == null) listDocumentDetails = new list<DocumentDetails>(); return listDocumentDetails;} set;}
     	public list<IPFunctions.ContactDocument> listFiles{get{if(listFiles == null) listFiles = new list<IPFunctions.ContactDocument>(); return listFiles;} set;}
    }
    
    
    public class DocumentDetails{
        public Client_Document__c clientDocument {get{if(clientDocument == null) clientDocument = new Client_Document__c(); return clientDocument;} set;}
        
        public list<IPFunctions.ContactDocument> listFiles{get{if(listFiles == null) listFiles = new list<IPFunctions.ContactDocument>(); return listFiles;} set;}
    }
	
    private Map<String, Doc> docsMap;
    private List<Client_Document__c> listdocs {get;set;}
    private List<IPFunctions.ContactDocument> oldIPFiles {get;set;}
    public Map<String,Doc> getDocs(){
    	
        if(docsMap == null){
        	
        	docsMap = new Map<String,Doc>();
        	renderDocs = new map<string,boolean>();
        	
	       	listdocs = [Select Document_Category__c, C.Document_Type__c, lastModifiedDate, isSelected__c, Name, preview_link__c from Client_Document__c C where Client__c = :quote.Client__c order by  Document_Category__c, Document_Type__c, CreatedDate desc];


					for(Client_Document__c cd : listDocs){

						if(!docsMap.containsKey(cd.Document_Category__c)){
							Doc doc = new Doc();
							doc.Category = cd.Document_Category__c;
							DocumentDetails dd = new DocumentDetails();
							dd.clientDocument = cd;
							if(cd.preview_link__c!= null)
								doc.listFiles = IPFunctions.getFilesFromPreviewLink(cd.preview_link__c, virtualId);

							doc.listDocumentDetails.add(dd);
							docsMap.put(doc.Category, doc);

							renderDocs.put(doc.Category, false);
						}else{
							Doc d = docsMap.get(cd.Document_Category__c);

							DocumentDetails dd = new DocumentDetails();
							dd.clientDocument = cd;
							if(cd.preview_link__c!= null)
								d.listFiles.addAll(IPFunctions.getFilesFromPreviewLink(cd.preview_link__c, virtualId));

							d.listDocumentDetails.add(dd);

							docsMap.put(d.Category, d);
						}

						virtualId += docsMap.get(cd.Document_Category__c).listFiles.size();

					}//end for
	        
	      //     for(Client_Document__c cd : listDocs){
	          	
	      //     	if(!docsMap.containsKey(cd.Document_Category__c)){
		    //       	Doc doc = new Doc();
	      //           doc.Category = cd.Document_Category__c;
	      //           DocumentDetails dd = new DocumentDetails();
	      //           dd.clientDocument = cd;   
	                	              
	      //           doc.listDocumentDetails.add(dd);
	      //           docsMap.put(doc.Category, doc);
	               
	      //           renderDocs.put(doc.Category, false);
	          		
	      //     	}else{
	      //     		Doc d = docsMap.get(cd.Document_Category__c);
	          		
	      //     		DocumentDetails dd = new DocumentDetails();
	      //           dd.clientDocument = cd;   
	                	              
	      //           d.listDocumentDetails.add(dd);
	      //     	}
	                
               
            			
	      //     }//end for
	          
	          
        //     oldIPFiles = new list<IPFunctions.ContactDocument>();
	      //   if(quote.Client__r.remoteContactId__c!=null){
	      //   	oldIPFiles = IPFunctions.getDocumentFiles(quote.Client__c, 0);
				// if(oldIPFiles!=null)
				// 	virtualId = oldIPFiles.size()+1;	            		        
	        
	      //   }
	       
        }
        
		return docsMap;
        
    
    }
    
    
	public integer virtualId {get;set;}
	// public void renderDoc(){
	// 	String docCat = ApexPages.currentPage().getParameters().get('cat');
	// 	renderDocs.put(docCat, true);
		        
  //       S3Controller s3 = new S3Controller();
  //       s3.constructor();
  //       renderDocs.put(null, true);
                    
  //       Doc d = docsMap.get(docCat);
  //       for(DocumentDetails dd : d.listDocumentDetails){
	//         List<IPFunctions.ContactDocument> docFiles = IPFunctions.getDocument(String.valueOf(dd.clientDocument.id), dd.clientDocument.name, s3, virtualId);
	        
	//         d.listFiles.addAll(docFiles);
	        
	//         virtualId += docFiles.size();
	        
	//          if(oldIPFiles != null)    
  //               for(IPFunctions.ContactDocument cdfile : oldIPFiles)
	//             	if(cdfile.clientDocumentID == dd.clientDocument.id)
	//             		d.listFiles.add(cdfile);
        	
  //       }//end for
        
	// }  
	
	private List<String> order = new List<String>{'Visa', 'Passport', 'Personal', 'Enrolment', 'Education'};
	public List<String> categoriesOrder {
        get{
            if(categoriesOrder == null){
                categoriesOrder = new List<String>();
                Schema.DescribeFieldResult fieldResult = Client_Document__c.Document_Category__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues(); 
                
                
                for(String orderCat : order)
                    for(String category : getDocs().keySet())
                        if(orderCat == category)
                            categoriesOrder.add(orderCat);
                
                        
                for(Schema.PicklistEntry f : ple){
                    
                    Boolean found = false;
                    for(String orderCat : order)
                        if(orderCat == f.getValue())
                            found = true;
                    
                    if(!found)
                        for(String category : getDocs().keySet())
                            if(category == f.getValue())
                                categoriesOrder.add(f.getValue());                      
                }
                        
                
                
            }
            return categoriesOrder;
        }
        Set;
        
    }
    
    public map<string,boolean> renderDocs {get;set;}
	
			 	      
	
	public static string secret { get {return credentials.secret;} }
	public static AWSKeys credentials {get;set;}
	public static string key { get {return credentials.key;} set;}
	private static String AWSCredentialName = IPFunctions.awsCredentialName;
	public static S3.AmazonS3 as3 { get; private set; }


	/** Email Template */
	public String selectedTemplate {get;set;}
	public list<SelectOption> emailOptions {get;set;}
	public map<Id, Email_Template__c> mEmailTemplate {get;set;}

	public String tempBody {get;set;}
	private sObject objTemplate;
	private list<string> fieldsTemplate;
	private map<string, Object> mapFields;
	private map<Id,String> mapSubject;

	private void findEmailTemplate(String agencyId, String category){
		emailOptions = new list<SelectOption>();
		mEmailTemplate = new map<Id, Email_Template__c>();
		mapSubject = new map<Id, String>();
		String mSubject;

		for(Email_Template__c et : [SELECT Email_Subject__c, Template__c, Template_Description__c FROM Email_Template__c WHERE Agency__c = :agencyId and Category__c = :category order by Template_Description__c, Email_Subject__c]){
			try{
				fieldsTemplate = emailTemplateDynamic.extractFieldsFromFile(et.Email_Subject__c, mapFields); 
				mSubject = emailTemplateDynamic.createSingleTemplate(mapFields, et.Email_Subject__c, fieldsTemplate, objTemplate);
				mapSubject.put(et.id, mSubject);
				emailOptions.add(new SelectOption(et.Id, et.Template_Description__c));
				mEmailTemplate.put(et.id, et);
			}catch(Exception e){
				system.debug('ERROR FOUND ON findEmailTemplate() ===> ' + e.getLineNumber() + ' <=== ===> '+e.getMessage());
				system.debug('fieldsTemplate==>' + fieldsTemplate);
				system.debug('et.Email_Subject__c==>' + et.Email_Subject__c);
			}
		}//end for

		String typeEmail = ApexPages.currentPage().getParameters().get('type');

		if(emailOptions.size()>0){
			emailOptions.add(0, new SelectOption('', '– Select Email Template –'));
			selectedTemplate = emailOptions[0].getValue();
			changeTemplate();
		}
	}
	public void changeTemplate(){
		String typeEmail = ApexPages.currentPage().getParameters().get('type');
		selectedTemplate = ApexPages.currentPage().getParameters().get('template');
		system.debug('CALLED CHANGE TEMPLATE '+selectedTemplate);
		email = '';
		subject = '';
		if(selectedTemplate != null && selectedTemplate != ''){
			subject =  mapSubject.get(selectedTemplate);
			
			fieldsTemplate = emailTemplateDynamic.extractFieldsFromFile(mEmailTemplate.get(selectedTemplate).Template__c, mapFields); 
			system.debug('fieldsTemplate '+fieldsTemplate);
			system.debug('mapFields '+mapFields);
			system.debug('mEmailTemplate.get(selectedTemplate).Template__c '+mEmailTemplate.get(selectedTemplate).Template__c);
			tempBody = emailTemplateDynamic.createSingleTemplate(mapFields, mEmailTemplate.get(selectedTemplate).Template__c, fieldsTemplate, objTemplate);
			if(tempBody != null && (userDetails.Account.Email_Quotation_Option__c == 'link' || userDetails.Account.Email_Quotation_Option__c == null)){
				string linkDesc = tempBody.substringBetween('{#qt#}');
				string quoteDetailButton = '<a style="cursor: pointer; white-space: nowrap;" target="_blank" href="'+Url.getSalesforceBaseUrl().toExternalForm()+'/'+communityName+'/quotation?id='+quote.id+'" class="showCaseLink" >'+linkDesc+'</a>';
				tempBody =  tempBody.replace('{#qt#}'+linkDesc+'{#qt#}',quoteDetailButton);
			}

			// if(useSignature)
			// 	email += getSignature();
		}
		else{
			//email = '';
			if(typeEmail == 'emailToStudent'){
				email += getSignature();
				tempBody = '';
			}
		}
	}
}