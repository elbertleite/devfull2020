@isTest
private class contact_template_report_test
{

	static testMethod void contactTemplateTest() {
        TestFactory tf = new TestFactory();
       
       	Account school = tf.createSchool();
       	
       	Account agencyGroup = tf.createAgencyGroup();
       	
       	Account agency = tf.createAgency();
       	
      	Contact emp = tf.createEmployee(agency);
      	 
       	User portalUser = tf.createPortalUser(emp);
       	
        Department__c department = tf.createDepartment(agency);
        
		Contact client = tf.createLead(agency, emp);
		client.Arrival_Date__c = System.today();
		client.isSelected__c = true;
		client.Birthdate = System.today().addYears(-20);
		client.Current_Agency__c = agency.id;
		update client;
		
       	Test.startTest();
       	system.runAs(portalUser){
			
			contact_template_report testClass = new contact_template_report();

			testClass.selectedAgencyGroup = agencyGroup.id;
			testClass.selectedAgency = agency.id;
			
			testClass.retrieveAgencyGroupOptions();
			testClass.agencyOptions();
			testClass.retrieveDepartments();

			list<SelectOption> listClientOwnerType = testClass.listClientOwnerType;
			list<SelectOption> listMonths  = testClass.listMonths;
			list<SelectOption> destinations = testClass.destinations;
			list<SelectOption> contactTypeOptions = testClass.contactTypeOptions;
			

			testClass.generateTemplate();

    		testClass.csvFileBody = Blob.valueOf('document');
			testClass.csvAsString = 'document body';
			testClass.selectedType = 'birthdate';
			testClass.generateTemplate();

			testClass.sendEmail();

			testClass.selectedClientAgency = 2;
			testClass.selectedType = 'arrival';
			testClass.generateTemplate();

			
			testClass.emailSubject = 'test';
			testClass.sendEmail();
       	}
        
    }
}