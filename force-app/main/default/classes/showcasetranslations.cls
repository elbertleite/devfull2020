public class showcasetranslations {
    
    private List<Account> campusFilter;
    private boolean isInternalUser;
    private Contact myContact;
    
    public void showcasetranslations(){
    		
    }
	
	public void loadUserInfo(){
		isInternalUser = IPFunctions.isInternalUser(UserInfo.getUserID());
		system.debug('isInternalUser: ' + isInternalUser);		
		if(!isInternalUser){
			myContact = UserDetails.getMyContactDetails();
    		getSuppliers();    		
		}
	}
	
	private Set<String> suppliers;
	private void getSuppliers(){		
		suppliers = new Set<String>();
		for(Supplier__c s : [select Supplier__c from Supplier__c where agency__c = :myContact.AccountID])
			suppliers.add(s.Supplier__c);
	}
	
	    
    public String selectedCountry { get;set; }
    private List<SelectOption> countries;
    public List<SelectOption> getCountries(){
    	try {
	    	if(countries == null){
	    		countries = new List<SelectOption>();
	    		
	    		if(isInternalUser)
	    			for(AggregateResult ar : [select BillingCountry from account where recordtype.name = 'Campus' and BillingCountry != null group by BillingCountry order by BillingCountry]){
		    			countries.add(new SelectOption( (String) ar.get('BillingCountry') , (String) ar.get('BillingCountry')));
		    			
		    		}
	    		else
		    		for(AggregateResult ar : [select BillingCountry from account where recordtype.name = 'Campus' and BillingCountry != null and id in :suppliers group by BillingCountry order by BillingCountry]){
		    			countries.add(new SelectOption( (String) ar.get('BillingCountry') , (String) ar.get('BillingCountry')));
		    			
		    		}
		    		
		    	selectedCountry = countries.get(0).getValue();
	    	}
    	} catch (Exception e){
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.Error, e.getMessage() + '<br/>' + e.getStackTraceString()));    		
    	}
    	
    	return countries;
    }
    
    public void refreshCities(){
    	cities = null;
    	selectedCity = null;
    	refreshSchools();
    }
    
    public String selectedCity {get;set;}
    private List<SelectOption> cities;
    public List<SelectOption> getCities(){
    	try {
	    	if(cities == null && selectedCountry != null){
	    		cities = new List<SelectOption>();
	    		cities.add(new SelectOption('all', '-- ALL --'));
	    		if(isInternalUser)
	    			for(AggregateResult ar : [select BillingCity from account where recordtype.name = 'Campus' and BillingCountry = :selectedCountry group by BillingCity order by BillingCity])
	    				cities.add(new SelectOption((String) ar.get('BillingCity'), (String) ar.get('BillingCity')));
	    		else
	    			for(AggregateResult ar : [select BillingCity from account where recordtype.name = 'Campus' and BillingCountry = :selectedCountry and id in :suppliers group by BillingCity order by BillingCity])
	    				cities.add(new SelectOption((String) ar.get('BillingCity'), (String) ar.get('BillingCity')));
	    	}
    	} catch (Exception e){
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.Error, e.getMessage() + '<br/>' + e.getStackTraceString()));    		
    	}
    	return cities;
    }
    
    public void refreshSchools(){
    	schools = null;
    	selectedSchool = null;
    }
    
    public String selectedSchool {get;set;}
    private List<SelectOption> schools;
    public List<SelectOption> getSchools(){
    	try {
	    	if(schools == null && selectedCountry != null){
	    		schools = new List<SelectOption>();
	    		schools.add(new SelectOption('all', '-- ALL --'));
	    		String sql = 'select parentid, Parent.name from account where recordtype.name = \'Campus\' and parentid != null and BillingCountry = :selectedCountry ';
	    		if(selectedCity != null)
	    			sql += ' and BillingCity = :selectedCity ';
	    			
	    			
	    			
	    		sql += ' group by parentid, Parent.name order by Parent.name ';
	    		
	    		for(AggregateResult ar : Database.query(sql))
	    			schools.add(new SelectOption((String) ar.get('parentid'), (String) ar.get('name')));
	    	}
	    } catch (Exception e){
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.Error, e.getMessage() + '<br/>' + e.getStackTraceString()));    		
    	}
    	
    	return schools;
    }
    
    
    public Map<String, String> languageMap {get;set;}
    public String selectedLanguage {get;set;}
	private List<SelectOption> languages;
	public List<SelectOption> getLanguages(){
		
		if(languages == null){
			languages = new List<SelectOption>();
			languageMap = new Map<String, String>();
			//languages.add(new SelectOption('', '-- Select Language --'));
			Schema.DescribeFieldResult fieldResult = Contact.Preferable_Language__c.getDescribe();
   			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
   			for(Schema.PicklistEntry f : ple)
   				if(f.getLabel() != 'en_US'){
      				languages.add(new SelectOption(f.getLabel(), IPFunctions.getLanguage(f.getLabel())));
      				languageMap.put(f.getLabel(), IPFunctions.getLanguage(f.getLabel()));
   				}
		}
		languages.sort();
		return languages;		
	}
	
	private List<SelectOption> languageResults;
	public List<SelectOption> getLanguageResults(){
		
		if(languageResults == null){			
			languageResults = new List<SelectOption>();
			
			if(selectedLanguage != null && selectedLanguage != ''){
				languageResults.add(new SelectOption(selectedLanguage, IPFunctions.getLanguage(selectedLanguage)));
			} else {		
				Schema.DescribeFieldResult fieldResult = Contact.Preferable_Language__c.getDescribe();
	   			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
	   			for(Schema.PicklistEntry f : ple)
	   				if(f.getLabel() != 'en_US')
	      				languageResults.add(new SelectOption(f.getLabel(), IPFunctions.getLanguage(f.getLabel())));
			}
		}
		languageResults.sort();
		return languageResults;		
	}
	
	
	
	public String selectedStatus {get;set;}
	private List<SelectOption> statuses;
	public List<SelectOption> getStatuses(){
		if(statuses == null){
			statuses = new List<SelectOption>();
			Schema.DescribeFieldResult fieldResult = Translation__c.Status__c.getDescribe();
   			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
   			for(Schema.PicklistEntry f : ple)
   				if(f.getLabel() != 'Not Started')
      				statuses.add(new SelectOption(f.getLabel(), f.getLabel()));
		}
		return statuses;
	}
	
	
    
    public void search(){
    	languageResults = null;
    	showcases = new Map<String, CountryWrapper>();
    	
    	
    	if(selectedStatus == null || selectedStatus == ''){
	    	String sql = 'select id, Name, BillingCountry, BillingCity, (Select id, Language__c, Status__c, Progress__c from Translations__r where Type__c = \'content\' order by Language__c) from Account where recordType.Name = \'Campus\' ';
	    	if(selectedCountry != null && selectedCountry != 'all')
	    		sql += ' and BillingCountry = :selectedCountry ';
	    	if(selectedCity != null && selectedCity != 'all')
	    		sql += ' and BillingCity = :selectedCity ';
	    	if(selectedSchool != null && selectedSchool != 'all')
	    		sql += ' and ParentId = :selectedSchool ';
	    	if(!isInternalUser)
	    		sql += ' and id in :suppliers ';	
	    		
	    	sql += ' order by Name ';
	    	
	    	for(Account ac : Database.query(sql)){
	    		if(showcases.containsKey(ac.BillingCountry)){
	    			
	    			if(showcases.get(ac.BillingCountry).cities.containsKey(ac.BillingCity)){
	    				
	    				showcases.get(ac.BillingCountry).cities.get(ac.BillingCity).campuses.put( ac.Name, createCampusWrapper(ac, null) );
	    				
	    				
	    			} else {
	    				
	    				showcases.get(ac.BillingCountry).cities.put(ac.BillingCity, createCityWrapper(ac.BillingCity, new Map<String, CampusWrapper>{ ac.Name => createCampusWrapper(ac, null) }, null) );
	    				
	    			}
	    			
	    		} else {
	    			
	    			CountryWrapper countryWrapper = new CountryWrapper();
	    			countryWrapper.Name = ac.BillingCountry;
	    			countryWrapper.translations = createTranslationsMap(new List<Translation__c>()); 
	    			countryWrapper.cities = new Map<String, CityWrapper>{ ac.BillingCity => createCityWrapper(ac.BillingCity, new Map<String, CampusWrapper>{ ac.Name => createCampusWrapper(ac, null) }, null) };    			
	    			showcases.put(countryWrapper.Name, countryWrapper);    			
	    		}
	    		
	    	}
	    	    	
	    	for(Account destination : [select id, name, RecordType.Name, Parent.Name, (Select id, Language__c, Progress__c, Status__c from Translations__r where Type__c = 'content' order by Language__c) from Account where recordtype.name in ('Country', 'City')]){
	    		
	    		if(destination.recordType.name == 'Country'){
	    			if(showcases.containsKey(destination.Name)){
	    				showcases.get(destination.name).id = destination.id;
	    				showcases.get(destination.name).translations = createTranslationsMap(destination.translations__r);
	    			}
	    			
	    		} else if(destination.recordtype.name == 'City'){
	    			if(showcases.containsKey(destination.Parent.Name) && showcases.get(destination.parent.name).cities.containsKey(destination.name)){
	    				showcases.get(destination.parent.name).cities.get(destination.name).id = destination.id;
	    				showcases.get(destination.parent.name).cities.get(destination.name).translations = createTranslationsMap(destination.translations__r);
	    			}
	    		}
	    			
	    	}
	    	
    	} else {
    		
    		Map<String, String> destinationAccounts = new Map<String, String>();
    		for(Account da : [select id, name, RecordType.Name, Parent.Name from Account where recordtype.name in ('Country', 'City')])
    			destinationAccounts.put(da.Name, da.id);
    		
    		
    		for(Translation__c tr : [select id, Account__c, Account__r.BillingCountry, Account__r.BillingCity, Account__r.Name, Account__r.RecordType.Name, Account__r.Parent.Name, Language__c, Status__c, Progress__c
    									from Translation__c where Status__c = :selectedStatus and Language__c = :selectedLanguage order by Account__r.BillingCountry, Account__r.BillingCity, Account__r.Name]){
    			
    			if(tr.Account__r.RecordType.Name == 'Campus'){
    			    			
	    			if(showcases.containsKey(tr.account__r.BillingCountry)){
		    			
		    			if(showcases.get(tr.Account__r.BillingCountry).cities.containsKey(tr.Account__r.BillingCity)){
		    				
		    				if(showcases.get(tr.Account__r.BillingCountry).cities.get(tr.Account__r.BillingCity).campuses.containsKey(tr.Account__r.Name))
		    					showcases.get(tr.Account__r.BillingCountry).cities.get(tr.Account__r.BillingCity).campuses.get(tr.Account__r.name).translations.put(tr.Language__c, tr);
		    				else
		    					showcases.get(tr.Account__r.BillingCountry).cities.get(tr.Account__r.BillingCity).campuses.put(tr.account__r.Name, createCampusWrapper(tr.account__r, tr) );
		    				
		    				
		    			} else {
		    				
		    				showcases.get(tr.Account__r.BillingCountry).cities.put(tr.Account__r.BillingCity, createCityWrapper(tr.Account__r.BillingCity, new Map<String, CampusWrapper>{ tr.Account__c => createCampusWrapper(tr.Account__r, tr) }, null) );
		    				if(destinationAccounts.containsKey(tr.account__r.billingCity))
		    					showcases.get(tr.Account__r.BillingCountry).cities.get(tr.Account__r.BillingCity).id = destinationAccounts.get(tr.account__r.billingcity); 
		    			}
		    			
		    		} else {
		    			
		    			CountryWrapper countryWrapper = new CountryWrapper();
		    			countryWrapper.Name = tr.Account__r.BillingCountry;
		    			countryWrapper.translations = createTranslationsMap(new List<Translation__c>()); 
		    			//countryWrapper.translations.put(tr.Language__c, tr);
		    			countryWrapper.cities = new Map<String, CityWrapper>{ tr.Account__r.BillingCity => createCityWrapper(tr.Account__r.BillingCity, new Map<String, CampusWrapper>{ tr.Account__r.id => createCampusWrapper(tr.Account__r, tr) }, null) };
		    			if(destinationAccounts.containsKey(tr.Account__r.BillingCountry))
	    					countryWrapper.id = destinationAccounts.get(tr.account__r.billingCountry);     			
		    			showcases.put(countryWrapper.Name, countryWrapper);    			
		    		}
		    		
    			} else if(tr.Account__r.RecordType.Name == 'Country') {
    				
    				if(showcases.containsKey(tr.account__r.Name)){
    					
    					if(showcases.get(tr.account__r.Name).id == null)
    						showcases.get(tr.account__r.Name).id = tr.Account__c;
    						
    					showcases.get(tr.account__r.Name).translations.put(tr.Language__c, tr);
    					
    				} else {
    					CountryWrapper countryWrapper = new CountryWrapper();
    					countryWrapper.id = tr.Account__c;
		    			countryWrapper.Name = tr.account__r.Name;
		    			countryWrapper.cities = new Map<String, CityWrapper>();
		    			countryWrapper.translations = createTranslationsMap(new List<Translation__c>()); 
		    			countryWrapper.translations.put(tr.Language__c, tr);		    			    			
		    			showcases.put(countryWrapper.Name, countryWrapper);   
    				}
    				
    			} else if(tr.Account__r.RecordType.Name == 'City'){
    				
    				if(showcases.containsKey(tr.account__r.Parent.Name)){
    					 
    					 if(showcases.get(tr.account__r.parent.name).cities.containsKey(tr.account__r.name)){
    					 	
    					 	if(showcases.get(tr.account__r.parent.name).cities.get(tr.account__r.name).id == null)
    					 		showcases.get(tr.account__r.parent.name).cities.get(tr.account__r.name).id = tr.account__c;
    					 		
    					 	showcases.get(tr.account__r.parent.name).cities.get(tr.account__r.name).translations.put(tr.language__c, tr);    					 	
    					 } else {    					 	
    					 	showcases.get(tr.account__r.parent.name).cities.put(tr.account__r.name, createCityWrapper(tr.account__r.name, new Map<String, CampusWrapper>(), tr) );    					 	
    					 }
    					 
    				} else {
    					CountryWrapper countryWrapper = new CountryWrapper();    					
						countryWrapper.id = tr.Account__r.Parentid;
		    			countryWrapper.Name = tr.account__r.Parent.Name;
		    			countryWrapper.translations = createTranslationsMap(new List<Translation__c>());
		    			countryWrapper.cities = new Map<String, CityWrapper>{ tr.account__r.name =>  createCityWrapper(tr.account__r.name, new Map<String, CampusWrapper>(), tr) };
		    			showcases.put(countryWrapper.Name, countryWrapper);		    			
    				}
    				    				
    			}
	    		
    		}
    		
    		
    	}
    }
    
    
    private CityWrapper createCityWrapper(String cityName, Map<String, CampusWrapper> campuses, Translation__c tr){
    	CityWrapper cityWrapper = new CityWrapper();
		cityWrapper.name = cityName;
		cityWrapper.campuses = campuses;
		cityWrapper.translations = createTranslationsMap(new List<Translation__c>());
		
		if(tr != null){
			cityWrapper.translations.put(tr.Language__c, tr);
			cityWrapper.id = tr.Account__c;
		}
		system.debug('@#$ cityWrapper: ' + cityWrapper);
		return cityWrapper;
    }
    
    private CampusWrapper createCampusWrapper(Account campus, Translation__c tr){
    	
    	CampusWrapper cw = new CampusWrapper();
		cw.campus = campus;
		cw.translations = createTranslationsMap(campus.translations__r);
		if(tr != null)
			cw.translations.put(tr.Language__c, tr);
		return cw;
    	
    }
    
    private Map<String, Translation__c> createTranslationsMap(List<Translation__c> translations){
    	Map<String, Translation__c> trs = new Map<String, Translation__c>();
    	
    	for(SelectOption so : getLanguageResults())
    		trs.put(so.getValue(), new Translation__c());
    	
    	
		for(Translation__c tr : translations)
			trs.put(tr.Language__c, tr);
			
		return trs;	
    }
    
    
    public Map<String, CountryWrapper> showcases {get {if(showcases == null) showcases = new Map<String, CountryWrapper>(); return showcases; } set;}
    public boolean foundShowcases { get { return !showcases.isEmpty(); } set; }
    
    public class CountryWrapper {
    	public String id {get;set;}
    	public String name {get;set;}
    	public Map<String, CityWrapper> cities {get;set;}
    	public Map<String, Translation__c> translations {get;set;}
    }
    
    public class CityWrapper {
    	public String id {get;set;}
    	public String name {get;set;}
    	public Map<String, CampusWrapper> campuses {get;set;}
    	public Map<String, Translation__c> translations {get;set;}
    	
    }
    
    public class CampusWrapper{
    	public Account campus {get;set;}
    	public Map<String, Translation__c> translations {get;set;}
    }
    
}