public class batch_AccountPictureFiles_IP_Urls{
	public batch_AccountPictureFiles_IP_Urls(){}
}


/*global class batch_AccountPictureFiles_IP_Urls implements Database.Batchable<sObject>, Database.AllowsCallouts {
  
  
  S3Controller S3 {get;set;}
  String query;
  
  global batch_AccountPictureFiles_IP_Urls(String qr) {
    
  	query = qr;
  }
  
  global Database.QueryLocator start(Database.BatchableContext BC) {

    //Be sure Account Picture Files Generate Link Trigger is Inactive
    // batch_AccountPictureFiles_IP_Urls execBatch = new batch_AccountPictureFiles_IP_Urls('');
    // Database.executeBatch(execBatch,99);
    

    //String query = 'SELECT Id, Url__c, sDrive_Account__c, SDrive_File__c FROM Account_Picture_file__c WHERE SDrive_Account__c != null and isUrlUpdated__c = false and id =\'a029000000NnY6WAAV\'';

    return Database.getQueryLocator(query);
  }

     global void execute(Database.BatchableContext BC, List<Account_Picture_file__c> scope) {
    // try{
      S3 = new S3Controller();
      S3.constructor();
      String prefix = '';
      for(Account_Picture_file__c apf : scope){
        prefix = apf.SDrive_Account__c + '/' + apf.SDrive_File__c;

        s3.listBucket('ehfsdrive', prefix , null, null, null);

        if(s3.bucketList != null && !s3.bucketList.isEmpty()){  
          for(S3.ListEntry file : s3.bucketList){
            system.debug('file===>' + file);

            String url = IPFunctions.GenerateAWSLink('ehfsdrive', file.key, S3.S3Key, s3.S3Secret);
            system.debug('url===>' + url); 
            apf.Url__c = url;
            apf.isUrlUpdated__c = true;
          }
        }
      }//end for

      update scope;
    // }
    // catch(Exception e){
    //   system.debug('error==' + e.getMessage());
    //   system.debug('error line==' + e.getLineNumber());
    // }

  }
  
  global void finish(Database.BatchableContext BC) {
    system.debug('Url update finished...');
  }
  
}*/

/*
 batch_AccountPictureFiles_IP_Urls batch = new batch_AccountPictureFiles_IP_Urls('Select id, File_Name__c, Parent__c, url__c, createdby.name, SDrive_Account__c, SDrive_File__c from Account_Document_File__c where createdBy.name = null and Content_Type__c != \'Folder\' and SDrive_File__c != null');
    Id batchId = Database.executeBatch(batch, 80); //Define batch size
*/