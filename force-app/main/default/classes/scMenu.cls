public class scMenu{

	public String accoID{get; Set;}
	public String currTab{get;set;} 

	private Account acco;
	public Account getAcco(){
		System.debug('-----------accoID: '+accoID);
        
		acco =  [Select 
		A.Id,
		A.CreatedById,
		A.CreatedDate,
		A.Name,
		A.RecordType.Name,
		//A.FirstName,
		A.LastModifiedById,
		A.LastModifiedDate,
		//A.LastName,
		//A.PersonTitle,
		A.ParentId,
		A.Parent.ParentId,
		A.Original_Agency__c, 
		A.RecordTypeId, 
		A.isLead__c, 
		//A.Agency__pc, 
		A.Lead_Converted_by__c, 
		A.Lead_Converted_on__c,
		//A.Full_Name__pc,
		A.Destination__c,
		A.City_Destination__c,
		A.Expected_Travel_date__c,
		A.Campus_Photo_URL__c,
		A.Lead_Status__c, 
		(Select Id from Social_Networks__r),
		//(Select Id from Forms_of_Contact__r),
		(Select Id from Addresses__r),
		//(Select Id from Client_Family_Friends__r),
		//(Select Id from AccounttoAccounts__r),
		//(Select Id from AccounttoAccounts1__r),
		(Select Id from Campus_Courses__r),
		//(Select Id from Flight_Details__r),
		//(Select ID from Client_visas__r),
		(Select Id from Bank_Details1__r),
		//(Select Id from Educational_History__r),
		//(Select Id from Employments_History__r),
		//(Select Id from Client_Document1__r),
		//(Select Id from Account_Extra_Information_File__r),
		(Select Id from Videos__r),
		(Select Id from cg__Account_Files__r) ,
		//(Select Id from Emails__r where Read__c = false and Received__c = true),
		//(Select Id from Client_Documents__r),
		//(Select Id from Account_Document_Files__r where WIP__c = false and Content_Type__c != 'Folder'),
		(Select Id from Account_Picture_Files__r where WIP__c = false and Content_Type__c LIKE 'image/%'),
		(Select Id from Account_Logos_File__r where WIP__c = false and Content_Type__c LIKE 'image/%'),
		(Select Id from Partners_Relationship__r)
            
		from Account A
			where id= :accoID];
		return acco;
	}
	public Account testimonials {
		get{
			if(testimonials == null){
				testimonials =  [Select A.Id, (Select Id from Testimonials__r) from Account A where id= :accoID];
			}
			return testimonials;
		}
		set;
	}
	/*public Integer agreements {
		get {
			List<Account_Agreement_File__c> aafs = [Select id from Account_Agreement_File__c where parent__c = :acco.id];
			return aafs.size();
		}
		Set;
	}*/
	
	private String fileURL;
	public String getfileURL(){
   		if(fileURL == null){
			string accountPicture;
			try{
				accountPicture = [Select id from Account_Picture_File__c where WIP__c = false and Content_Type__c LIKE 'image/%' and Parent__c = :accoID ORDER BY LastModifiedDate DESC LIMIT 1].id;
			}catch(Exception e){}
			
			// URL for pictures
			if(accountPicture != null && accountPicture != '')
			{
				fileURL = cg.SDriveTools.getAttachmentURL(accoID, accountPicture, (100 * 60));
			}
			else
			{
				fileURL = 'NoPhoto';
			
			}
		}
		return fileURL;
		
	}
	
	//private string idAgency = acco == null ?'':acco.Id;
	private List<User> userAgency = [Select Contact.AccountId, UserType from User where id = :UserInfo.getUserId() limit 1];
	
	public List<Account> SiblingCampuses {
		get{if(SiblingCampuses==null)
		SiblingCampuses = new List<Account>();
		
		if(userAgency[0].UserType.equals('CspLitePortal'))  
			SiblingCampuses = [Select A.Id, A.Name, A.Website from Account A WHERE A.ParentId = :acco.ParentID and A.RecordType.Name = 'Campus' 
										and id in (Select Supplier__c from Supplier__c S where Agency__c = :userAgency[0].Contact.AccountId and record_type__c = 'campus')];
		else {
			if(acco.RecordType.Name == 'Campus')
				SiblingCampuses = [Select A.Id, A.Name, A.Website from Account A WHERE A.ParentId = :acco.ParentID and A.RecordType.Name = 'Campus'];
			else if(acco.RecordType.Name == 'School')
				SiblingCampuses = [Select A.Id, A.Name, A.Website from Account A WHERE A.ParentId = :acco.id and A.RecordType.Name = 'Campus'];
			else if(acco.RecordType.Name == 'School Group')
				SiblingCampuses = [Select A.Id, A.Name, A.Website from Account A WHERE A.parent.ParentId = :acco.Id and A.RecordType.Name = 'Campus'];
		}   
		return SiblingCampuses;
		
	}
		set;
		
	}

	public List<Account>  schoolGroup {
		get{if(schoolGroup==null)
			schoolGroup = new List<Account>();
		
		if(userAgency[0].UserType.equals('CspLitePortal'))  
			schoolGroup = [Select Id, Name from Account WHERE ParentId = :acco.Parent.ParentID and RecordType.Name = 'School' 
										and id in (Select Supplier__c from Supplier__c S where Agency__c = :userAgency[0].Contact.AccountId and record_type__c = 'School')];
		else {
			if(acco.RecordType.Name == 'Campus')
				schoolGroup = [Select Id, Name from Account WHERE ParentId = :acco.Parent.ParentID and RecordType.Name = 'School'];
			else if(acco.RecordType.Name == 'School')
				schoolGroup = [Select Id, Name from Account WHERE ParentId = :acco.ParentID and RecordType.Name = 'School'];
			else if(acco.RecordType.Name == 'School Group')
				schoolGroup = [Select Id, Name from Account WHERE ParentId = :acco.ID and RecordType.Name = 'School Group'];
		}   
		return schoolGroup;
		
	}
		set;
		
	}


}