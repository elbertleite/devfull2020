public with sharing class transfer_install_payment {

	public transfer_install_payment() {
		searchInstalments();
	}

	public string clientName{get{	if(clientName == null) clientName = ''; return clientName; } set;	}
	public map<string, list<client_course_instalment__c>> mapCountry{get; set;} 
	private list<string> searchAgencies;
	public integer numberItemsReceived{get{if(numberItemsReceived == null) numberItemsReceived = 0; return numberItemsReceived;} set;}
	public string agenciesUnderBackoffice{get{if(agenciesUnderBackoffice == null) agenciesUnderBackoffice = ''; return agenciesUnderBackoffice;} set;}

	private IPFunctions.SearchNoSharing sns {get{if(sns == null) sns = new IPFunctions.SearchNoSharing(); return sns;}set;}
	public void searchInstalments(){
		searchAgencies = new list<string>();
		for(Back_Office_Control__c ag:[Select Agency__c, Agency__r.Name from Back_Office_Control__c where Backoffice__c = :selectedAgency /*and country__c = :selectedCountry*/ and Services__c includes('Pay_Instalments_Agency')]){
			searchAgencies.add(ag.Agency__c);
			if(agenciesUnderBackoffice == '')
				agenciesUnderBackoffice = '<span>'+ag.Agency__r.Name+'</span>';
			else agenciesUnderBackoffice += ',<span>'+ ag.Agency__r.Name+'</span>';
			
		}
		system.debug('==>searchAgencies: ' + searchAgencies);
		// if(searchAgencies.size() == 0)
		// 	searchAgencies.add(selectedAgency);

		string sqlWhere = '';
    	String sql  = ' SELECT ID, client_course__c, Hify_Agency_Commission__c, Hify_Agency_Commission_Tax__c, Hify_Agency_Commission_Value__c, Hify_Agency_Instalment_Collected__c, client_course__r.Client__r.name, isSelected__c, isPFS__c, Discount__c, Received_By_Agency__r.name, Received_by__r.name, ';
			   sql += ' Received_Date__c, Client_Course__r.Course_Country__c, Client_Course__r.CurrencyIsoCode__c, Client_Course__r.Campus_Name__c, client_course__r.Course_Name__c, number__c, Tuition_Value__c, Extra_Fee_Value__c ';
			   sql += ' FROM client_course_instalment__c  WHERE ';  

		sqlWhere += ' Received_By_Agency__c in ( \''+ String.join(searchAgencies, '\',\'') + '\' )  and Hify_Commission_Invoice__c = null and Hify_Agency_Instalment_Collected__c != null and received_date__c != null and isPds__c = false and isPcs__c = false ';
		// ******************==== ATTENTION === NEEDS TO REMOVE and Hify_Agency_Instalment_Collected__c != null FROM LINE ABOVE ***************************************
       	if(clientName.length()>0)
			sqlWhere += ' AND client_course__r.Client__r.Name like \'%'+ clientName.trim() +'%\' ';

		sql += sqlWhere;
        sql += ' ORDER BY Client_Course__r.Course_Country__c, Received_By_Agency__r.Name, Received_Date__c';

      	system.debug('@SQL==>' + sql);

		mapCountry = new map<string, list<client_course_instalment__c>>();
		for(client_course_instalment__c cci : Database.Query(sql)){
			//totResult ++;
			if(!mapCountry.containsKey(cci.Client_Course__r.Course_Country__c))
				mapCountry.put(cci.Client_Course__r.Course_Country__c, new list<client_course_instalment__c>{cci});
			else
				mapCountry.get(cci.Client_Course__r.Course_Country__c).add(cci);

    	}
		searchInstalmentsTotal(sqlWhere);
		searchInvoicesToPay();
		searchnumberInvoicesPaid();
  	}

	public map<string, list<AggregateResult>> mapCountryTotal{get; set;} 
	public void searchInstalmentsTotal(string sqlWhere){
			String sql  = ' SELECT count(id) totItems, Sum(Hify_Agency_Commission_Tax__c) tax, Sum(Hify_Agency_Commission_Value__c) comm, Sum(Hify_Agency_Instalment_Collected__c) collected, sum(Extra_Fee_Value__c) fee, ';
				sql += ' Client_Course__r.Course_Country__c country, Sum(tuition_Value__c) tuition, sum(Discount__c) disc ';
				sql += ' FROM client_course_instalment__c  WHERE ';
				sql += sqlWhere;
				sql += ' group by Client_Course__r.Course_Country__c ';

		
			system.debug('@SQL==>' + sql);

			mapCountryTotal = new map<string, list<AggregateResult>>();
			numberItemsReceived = 0;
			for(AggregateResult ar : database.query(sql)){
				//totResult ++;
				if(!mapCountryTotal.containsKey((String)ar.get('country'))){
					mapCountryTotal.put((String)ar.get('country'), new list<AggregateResult>{ar});
					numberItemsReceived += (integer)ar.get('totItems');
				}else{
					mapCountryTotal.get((String)ar.get('country')).add(ar);
					numberItemsReceived += (integer)ar.get('totItems');
				}

		}
	}

// ========================== CREATE INVOICE ================
	public void generateInvoice(){

        Savepoint sp;
        try{
            sp = Database.setSavepoint();
            String countryName = ApexPages.CurrentPage().getParameters().get('country');
            String campusName;
            String schoolId;
            String schoolName;
            String campusCurrency;
            String campusCountry;

            boolean hasPFS = false;
            boolean hasCommissionKept = false;

            list<id> allIds = new list<id>();
            decimal totalAmount = 0;
            decimal totalInstalments = 0;
            decimal totalTuition = 0;
            decimal totalCommission = 0;
            decimal totalTax = 0;
            decimal totalExtraFee = 0;
            decimal totalDiscount = 0;
            
            list<String> instIds = new list<String>();
            list<client_course_instalment__c> instalments = new list<client_course_instalment__c>();

            for(client_course_instalment__c inst : mapCountry.get(countryName)){
                if(inst.isSelected__c)
                	instIds.add(inst.id);
			}

            //Double check if instalments has already a School Payment Invoice   
            for(client_Course_instalment__c cci : [SELECT  Instalment_Value__c, Tuition_Value__c,  Discount__c, Extra_Fee_Value__c, isSelected__c, Hify_Agency_Commission__c, Hify_Agency_Commission_Tax__c, Hify_Agency_Commission_Value__c,
													Hify_Agency_Instalment_Collected__c, Hify_Commission_Invoice__c FROM client_Course_instalment__c WHERE id IN :instIds AND Hify_Commission_Invoice__c = NULL]){

                allIds.add(cci.id);
                instalments.add(cci);
                cci.isSelected__c = false;

                //Totals
                totalAmount += cci.Hify_Agency_Instalment_Collected__c; //Balance to be paid by the agency hify
                totalInstalments += cci.Instalment_Value__c; //Instalment value
                totalTuition += cci.Tuition_Value__c; //Tuition value
                totalCommission += cci.Hify_Agency_Commission_Value__c; //Commission Value
                totalTax += cci.Hify_Agency_Commission_Tax__c; //Tax Value
                totalExtraFee += cci.Extra_Fee_Value__c; //Extra Fee
                totalDiscount += cci.Discount__c; //Discount
            }//end for


            if(allIds.size()>0){
                Invoice__c invoice = new Invoice__c();
                invoice.Total_Value__c = totalAmount;
                invoice.Instalments__c = string.join(allIds, ';');
                invoice.isHifyAgency__c = true;
                
                invoice.Requested_by_Agency__c = selectedAgency;
                invoice.Requested_by_Group__c = selectedAgencyGroup;
               
                invoice.CurrencyIsoCode__c = campusCurrency;

				invoice.Country__c = countryName;
                
                //TOTALS
                invoice.Total_Commission_to_Receive__c = totalCommission;
                invoice.Total_Tax_to_Receive__c = totalTax;
                invoice.Total_Extra_Fee__c = totalExtraFee;
                invoice.Total_Instalments__c = totalInstalments;
                invoice.Total_Tuition__c =  totalTuition;
                invoice.Total_Value__c = totalAmount;
                invoice.Total_Discount__c = totalDiscount;

                NoSharingClass ns = new NoSharingClass();
                ns.insertInvoice(invoice);

                //CREATE CLIENT DOCUMENT
                Client_Document__c receiptDoc = new Client_Document__c (Document_Category__c = 'Finance', Document_Type__c = 'Agency Payment Invoice', Invoice__c = invoice.id);
                insert receiptDoc;

                //UPDATE INVOICE W/ CLIENT DOC
                invoice.Payment_Receipt__c = receiptDoc.id;
                update invoice;

                //UPDATE INSTALMENTS
                for(client_course_instalment__c up : instalments){
                    up.Hify_Commission_Invoice__c = invoice.id;
                    up.Client_Document__c = receiptDoc.id;

                }//end for
                update instalments;
            }

            searchInstalments();

        }catch(exception e){
            Database.rollBack(sp);
            ApexPages.addMessages(e);
            system.debug('Error===' + e);
            system.debug('Error Line ===' + e.getLineNumber());

        }
        finally{
            ApexPages.CurrentPage().getParameters().remove('cpID');
        }
    }

	public without sharing class NoSharingClass{

        public void insertInvoice(Invoice__c invoice){
            insert invoice;
        }

        public void deleteInvoice(Invoice__c invoice){
            Database.delete(invoice.Payment_Receipt__c);
            delete invoice;
        }

    }

// ==========================  FILTERS ======================
	public String selectedAgencyGroup {
		get{
			if(selectedAgencyGroup == null){
				if(ApexPages.currentPage().getParameters().get('sg') != null)
					selectedAgencyGroup = ApexPages.currentPage().getParameters().get('sg');
				else selectedAgencyGroup = currentUser.Contact.Account.ParentId; 
			} 
			return selectedAgencyGroup;
		} 
		set;
	}
	public String selectedAgency {
	get{
			if(selectedAgency == null){
				if(ApexPages.currentPage().getParameters().get('sa') != null)
					selectedAgency = ApexPages.currentPage().getParameters().get('sa');
				else selectedAgency = currentUser.Contact.AccountId; 
			} 
			return selectedAgency;
		} 
		set;
	}
	private User currentUser = [Select Id, Name, Aditional_Agency_Managment__c, Contact.Account.ParentId, Contact.Account.Parent.Name, Contact.AccountId, Contact.Account.Name, Contact.Account.BillingCountry, Contact.Department__c, Contact.Account.Global_Link__c from User where id = :UserInfo.getUserId() limit 1];

	// public string selectedCountry{get{if(selectedCountry == null) selectedCountry = 'Australia'; return selectedCountry;} set;}
	// public List<SelectOption> destinationCountries{ 
	// 	get{
	// 		if(destinationCountries == null){
	// 			destinationCountries = new List<SelectOption>();
	// 			destinationCountries.addAll(FinanceFunctions.CountryDestinations());
	// 			destinationCountries.remove(0);
	// 			System.debug('==>:destinationCountries: '+destinationCountries);
	// 			if(selectedCountry == null)
	// 				selectedCountry = 'Australia'; 
	// 		}
	// 		return destinationCountries;
	// 	}
	// 	set;
	// }

	public List<SelectOption> agencyGroupOptions { 
  		get{
   			if(agencyGroupOptions == null){

   				agencyGroupOptions = new List<SelectOption>();
				
   				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group
	    			for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:currentUser.Contact.AccountId order by Parent.Name]){
		     			agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
		    		}
   				}else{
   					for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
		     			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
		    		}
   				}
				retrieveAgencies();
	   		}
	   		return agencyGroupOptions;
	  }
	  set;
 	}

   public list<SelectOption> agencies{get; set;}

	public list<SelectOption> retrieveAgencies(){
		if(agencies == null){
			agencies = new List<SelectOption>();
			if(selectedAgencyGroup != null){
				if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
					for(Account a: [Select Id, Name, Global_Link__c from Account where id = :currentUser.Contact.AccountId order by name]){
						if(selectedAgency==''){
							selectedAgency = a.Id; 
						}
						agencies.add(new SelectOption(a.Id, a.Name));
					}
					if(currentUser.Aditional_Agency_Managment__c != null){
						for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
							agencies.add(new SelectOption(ac.id, ac.name));
						}
					}
				}else{
					for(Account a: [Select Id, Name, Global_Link__c from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
						agencies.add(new SelectOption(a.Id, a.Name));
						if(selectedAgency==''){
							selectedAgency = a.Id;
						}
					}
				}
			}
		}
		return agencies;
	}

	
	public void changeCommGroup(){
		agencies = null;
		retrieveAgencies();
	}

	public integer InvoicesToPay{get{if(InvoicesToPay == null) InvoicesToPay = 0; return InvoicesToPay;} set;}
	// public integer InvoicesPaid{get{if(InvoicesPaid == null) InvoicesPaid = 0; return InvoicesPaid;} set;}
	public void searchInvoicesToPay(){
		InvoicesToPay = [SELECT count() from invoice__c where Requested_by_Agency__c = :currentUser.Contact.AccountId and isHifyAgency__c = true and paid_On__c = null and isCancelled__c = false];
	}

	public integer numberInvoicesPaid{get{if(numberInvoicesPaid == null) numberInvoicesPaid = 0; return numberInvoicesPaid;} set;}
	public void searchnumberInvoicesPaid(){
		numberInvoicesPaid = [SELECT count() from invoice__c where isHifyAgency__c = true and Paid_On__c != null 
								AND DAY_ONLY(convertTimezone(Paid_On__c)) >= :system.today().addDays(-7)
								AND DAY_ONLY(convertTimezone(Paid_On__c)) <= :system.today() ];
	}

	// public void searchInvoicesPaid(){
	// 	InvoicesPaid = [SELECT count() from invoice__c where isHifyAgency__c = true and Paid_On__c != null and Hify_Invoice_Confirmed_On__c != null];
	// }
}