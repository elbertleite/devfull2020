global class EmailToS3Controller implements Messaging.InboundEmailHandler {
	global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email,
	Messaging.InboundEnvelope env){

		Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
		try{

				if (email.fromAddress.contains('forwarding-noreply@google.com')){
					System.debug('Is a forward message');
					forwardEmailCofirmationCode(email.subject);
				}
				else{
					System.debug('Is not forward message');
					String confirmationCode = email.subject.substringBetween('(#',')');
					System.debug('Code: ' + confirmationCode );
					/*
						* Get Email Details
					*/
					EmailDetails emailDetails = new EmailDetails();
					emailDetails.subject= email.subject;
					emailDetails.fromAddress= email.fromAddress;
					emailDetails.toAddress = email.toAddresses;
					emailDetails.messageID= email.messageId;
					emailDetails.hasAtt='AttN';

					DateTime dateCreated = DateTime.now();
					String myDatetimeStr = dateCreated.format('dd-MM-yyyy HH:mm:ss');
					//String myDatetimeStr = dateCreated.formatGMT('YYYY-MM-DDTHH:mm:ssZ');
					emailDetails.dateSent= myDatetimeStr;

					/*//Get sent date from the headers[]
					for (Messaging.InboundEmail.Header header : email.headers) {
				      if (header.name == 'Date') {
				    	emailDetails.dateSent=header.value;
				       }
				    }*/

				  String htmlBody;
				  if(email.htmlBody!=null){
					htmlBody= email.htmlBody.unescapehtml4().normalizeSpace();
				  }else{
				  	htmlBody= email.plainTextBody;
  				  }

					//In case of the email has attachments describe it at the email body.
					integer i = 0;
				    if((email.textAttachments != null && email.textAttachments.size() > 0) || (email.binaryAttachments != null && email.binaryAttachments.size() > 0)){
				   	emailDetails.hasAtt='AttY';

					htmlBody += '<p><span style="color:black; font-weight:bold; font-size: 0.8em; text-decoration:underline;">ATTACHMENT(S):</span></p>';
					htmlBody += '<p/><span style="color:black; font-weight:bold; font-size: 0.8em;">You have received the attachment(s) below.  To view, please check your email account.</span>';

					if(email.textAttachments != null && email.textAttachments.size() > 0)
						for (Messaging.Inboundemail.TextAttachment tAttachment : email.textAttachments){
							i++;
							htmlBody+= '<br/><span style="color:maroon; font-size: 0.8em;">'+i + ' - '+tAttachment.fileName+'</span></p>';
						}
					if(email.binaryAttachments != null && email.binaryAttachments.size() > 0)
						for (Messaging.Inboundemail.BinaryAttachment bAttachment : email.binaryAttachments){
							i++;
							htmlBody += '<br/><span style="color:maroon; font-size: 0.8em;">'+i + ' - '+bAttachment.fileName+'</span></p>';
						}
				    }
					emailDetails.body=Blob.valueOf(htmlBody != null ? htmlBody : '');

					//TODO: change code
					if (email.toAddresses != null && !email.toAddresses.isEmpty())
						for(string ta: email.toAddresses)
							emailDetails.setEmails.add(ta);
					if (email.ccAddresses != null && !email.ccAddresses.isEmpty())
						for(string cc: email.ccAddresses)
							if(email.fromAddress != cc)
								emailDetails.setEmails.add(cc);

					System.debug('From: ' + emailDetails.fromAddress);
					System.debug('To: ' + emailDetails.toAddress);

					/*
					  * Verify sender and receiver
					  */
					boolean saveFile = verifySender(emailDetails);

					/*
					  * Check is is necessary save email
					  * In case of a Client or a Employee is NOT FOUND the email WON'T be saved.
					  */

  					if(saveFile){
						//Generate FileName
						List<String> fileName = generateFileName(emailDetails, Datetime.now().getTime()+'');

						//Save email on S3
						String bucketName = Userinfo.getOrganizationId();
						for(String file : fileName )
							saveEmailToS3(emailDetails.body, bucketName, file,null);

					}else{
						System.debug('Client or Employee is not registered.');
					}

				}
				result.success = true;
		}
		catch (Exception e) {
			result.success = false;
		}
   		return result;
	}

	@future (callout=true)
	public static void saveEmailtoS3(Blob body, String bucketName, String fileName, String contentType){
	/*
	 * Initialize controller to save file in Amazon S3
	 */
		S3Controller cS3 = new S3Controller();
		cS3.constructor(); //Generate Credentials

	 /*
	  * List All buckets to Check if it Has one For the Organization AND
	  * Get the Canonical User ID
	  */
		List<String> allbuck= cS3.allBuckets;

		system.debug('ORGANIZATION ID '+Userinfo.getOrganizationId());

		//Insert File
		cS3.insertFileS3(body, bucketName, fileName, contentType);
	}


	/*
		* Method created to receive data from other classes and send the email
	*/
	public String generateEmailToS3(Contact client, Contact employee, Boolean sendByEmployee, String subject, Blob body, Boolean hasAtt, String dateCreated){
		String hifyEmailID = '' + Datetime.now().getTime();
		return generateEmailToS3(client, employee, sendByEmployee, subject, body, hasAtt, dateCreated, hifyEmailID);
	}

	public String generateEmailToS3(Contact client, Contact employee, Boolean sendByEmployee, String subject, Blob body, Boolean hasAtt, String dateCreated, String emailID){
		System.debug('Called the method generateEmailToS3');

		//list<Contact> employeeClient = [Select Id, Email from Contact where Email = :client.Email and RecordType.name = 'Client' and current_agency__r.Global_Link__c = :employee.Account.Global_Link__c  order by createdDate];

		//Get email Details
		EmailDetails email= new EmailDetails();
		email.subject=subject;
		email.isFromEmployee=sendByEmployee;
		email.body=body;
		email.dateSent=dateCreated;
		//if(employeeClient!=null && employeeClient.size()>0)
		//	email.accClient.add(employeeClient[0]);
		//else
			email.accClient.add(client);
		email.accEmployee.add(employee);

		if(hasAtt)
			email.hasAtt='AttY';
		else
			email.hasAtt='AttN';

		//Generate FileName
		List<String> fileName= generateFileName(email, emailID);

		//Save the email
		String bucketName = Userinfo.getOrganizationId();

		system.debug('save filename==='+fileName[0]);
		saveEmailToS3(email.body, bucketName, fileName[0], null);

		return emailID;
	}

	/** Save emails from Cancel Course Flow **/
	//TODO
	/** END -- Save emails from Cancel Course Flow **/


	/** Save emails from Instalments on S3 **/
	public String bulkReceiptS3(String invoiceId, String toAddress, Blob body, String subject, String campusId, Id schoolId, String dateCreated,  Contact employee, String emailHifyId){

		// String fileName = employee.Account.Global_Link__c + '/' + schoolId +'/' + campusId + '/Payment/Receipt/';
		String fileName = employee.Account.Global_Link__c + '/' + schoolId + '/Payment/Receipt/';

		subject = checkSubject(subject);

		fileName+= 'S:-:'+employee.id+':-:'+subject+':-:'+dateCreated+':-:'+invoiceId+':-:'+toAddress+':-:'+emailHifyId;

		System.debug('FileName' + fileName);

		String bucketName = Userinfo.getOrganizationId();

		system.debug('save filename==='+fileName);
		saveEmailToS3(body, bucketName, fileName, null);

		S3Controller S3 = new S3Controller();
     	S3.constructor();

		return fileName;
	}
	/** END -- Save emails from Instalments on S3 **/


	/** Save emails from Instalments on S3 **/
	public void instalmentEmailS3(String toAddress, Blob body, String subject, String instalment, Contact client, Contact employee, String dateCreated, String emailHifyId){

		String fileName = IPFunctions.EmailInstalmentlFileNameS3(employee.Account.Global_Link__c, client.Id);

		subject = checkSubject(subject);

		fileName+='S:-:'+employee.id+':-:'+subject+':-:'+dateCreated+':-:'+instalment+':-:'+toAddress+':-:'+emailHifyId;

		System.debug('FileName' + fileName);

		String bucketName = Userinfo.getOrganizationId();

		system.debug('save filename==='+fileName);
		saveEmailToS3(body, bucketName, fileName, null);
	}
	/** END -- Save emails from Instalments on S3 **/


	/** Save emails from Cancel Course **/
	public void saveRefundEmailS3(Blob body, client_course__c course, String subject, String dateCreated, String createdBy, String toAddress, String cancelType, String emailHifyId){
		User userDetails = [select Id, ContactId,  Contact.Account.Global_Link__c from User where id = :createdBy limit 1];
		String fileName;

		if(cancelType == 'refund')
			fileName = userDetails.Contact.Account.Global_Link__c + '/Clients/' + course.Client__c + '/Courses/'+ course.Id + '/RefundCourse/';
		else if (cancelType =='coursecredit')
			fileName = userDetails.Contact.Account.Global_Link__c + '/Clients/' + course.Client__c + '/Courses/'+ course.Id + '/TransferCourse/';
		else if (cancelType =='cancelRequest')
			fileName = userDetails.Contact.Account.Global_Link__c + '/Clients/' + course.Client__c + '/Courses/'+ course.Id + '/CancelRequest/';

		String emSubject = checkSubject(subject);

		fileName+='S:-:'+userDetails.ContactId+':-:'+emSubject+':-:'+dateCreated+':-:'+toAddress+':-:'+emailHifyId;

		System.debug('FileName' + fileName);

		String bucketName = Userinfo.getOrganizationId();

		saveEmailToS3(body, bucketName, fileName, null);
	}
	/** END -- Save emails from Cancel Course **/

	/** Save emails from Cancel Course **/
	public void saveProviderPaymentEmailS3(Blob body, string providerId, string invoiceId, String subject, String dateCreated, String createdBy, String toAddress){
		User userDetails = [select Id, ContactId,  Contact.Account.Global_Link__c from User where id = :createdBy limit 1];
		String fileName;

		fileName = userDetails.Contact.Account.Global_Link__c + '/Providers/' + providerId + '/Invoices/'+ invoiceId + '/';

		String emSubject = checkSubject(subject);

		fileName+='S:-:'+userDetails.ContactId+':-:'+emSubject+':-:'+dateCreated+':-:'+toAddress;

		System.debug('FileName' + fileName);

		String bucketName = Userinfo.getOrganizationId();
		System.debug('bucketName' + bucketName);

		saveEmailToS3(body, bucketName, fileName, null);
	}
	/** END -- Save emails from Cancel Course **/


	/** Save emails for Request COE **/
	public void saveRequestCOEEmailS3(Blob body, client_course__c course, String subject, String dateCreated, String createdBy, String toAddress, String emailHifyId){
		User userDetails = [select Id, ContactId, Contact.Account.Global_Link__c from User where id = :createdBy limit 1];
		String fileName;

		fileName = userDetails.Contact.Account.Global_Link__c + '/Clients/' + course.Client__c + '/Courses/'+ course.Id + '/COE/';

		String emSubject = checkSubject(subject);

		fileName+='S:-:'+userDetails.ContactId+':-:'+emSubject+':-:'+dateCreated+':-:'+toAddress+':-:'+emailHifyId;

		System.debug('FileName' + fileName);

		String bucketName = Userinfo.getOrganizationId();

		saveEmailToS3(body, bucketName, fileName, null);
	}
	/** END -- Save emails for Request COE **/



	/** Save emails from Request Confirmation (School Payment) on S3 **/
	public void reqConfirmEmailS3(Blob body, Blob attachment, String subject,  String schoolId , String dateCreated, String createdBy, String requestNumber,String toAddress, String emailHifyId){
		User userDetails = [select Id, Contact.Account.Global_Link__c from User where id = :createdBy limit 1];
		String fileName = userDetails.Contact.Account.Global_Link__c + '/' + schoolId +'/Commissions/PDSConfirmation/'+requestNumber+'/Email/';

		String emailName = fileName+'email:-:'+userDetails.id+':-:'+dateCreated+':-:'+toAddress+':-:'+checkSubject(subject)+':-:'+string.valueOf(attachment!=null)+':-:'+emailHifyId;

		String bucketName = Userinfo.getOrganizationId();
		saveEmailToS3(body, bucketName, emailName, null);

		if(attachment!=null){//save email attachment
			String attachName = fileName +'attachment:-:'+dateCreated;
			saveEmailToS3(attachment, bucketName, attachName, 'application/pdf');
		}
	}
	/** END -- Save emails from Request Confirmation (School Payment) on S3 **/

	public void reqConfirmSchoolResponse(Blob body, String schoolId, String invoiceID, String createdBy,String requestNumber){
		User userDetails = [select Id, Contact.Account.Global_Link__c from User where id = :createdBy limit 1];
		String fileName = userDetails.Contact.Account.Global_Link__c + '/' + schoolId +'/Commissions/PDSConfirmation/'+requestNumber+'/SchoolResult/';

		fileName+=invoiceID;

		System.debug('FileName' + fileName);

		String bucketName = Userinfo.getOrganizationId();

		system.debug('save filename==='+fileName);
		saveEmailToS3(body, bucketName, fileName, 'application/pdf');
	}

	/** Save emails from Request School Commission on S3 **/
	public void reqSchoolCommissionEmailS3(Blob body, Blob attachment, String subject,  String schoolId , String dateCreated, String createdBy, String invoiceNumber,String toAddress, String emailHifyId){
		User userDetails = [select Id, Contact.Account.Global_Link__c from User where id = :createdBy limit 1];
		String fileName = userDetails.Contact.Account.Global_Link__c + '/' + schoolId +'/Commissions/CommissionInvoice/'+invoiceNumber+'/Email/';

		String emailName = fileName+'email:-:'+userDetails.id+':-:'+dateCreated+':-:'+toAddress+':-:'+checkSubject(subject)+':-:'+string.valueOf(attachment!=null)+':-:'+emailHifyId;

		String bucketName = Userinfo.getOrganizationId();

		saveEmailToS3(body, bucketName, emailName, null);

		if(attachment!=null){//save email attachment
			String attachName = fileName +'attachment:-:'+dateCreated;
			saveEmailToS3(attachment, bucketName, attachName, 'application/pdf');
		}
	}
	/** END -- Save emails from Request Confirmation (School Payment) on S3 **/


	/** Save emails from Mass Invoices (School Payment) on S3 **/
	public String massInvoiceEmailS3(Blob body, String schoolId ,String campusId, String dateCreated, String createdBy){
		User userDetails = [select Id, Contact.Account.Global_Link__c from User where id = :createdBy limit 1];
		String fileName = userDetails.Contact.Account.Global_Link__c + '/' + schoolId + '/'+ campusId + '/Invoices/PDS/';

		fileName+=userDetails.id+':-:'+dateCreated;

		System.debug('FileName' + fileName);

		String bucketName = Userinfo.getOrganizationId();

		system.debug('save filename==='+fileName);
		saveEmailToS3(body, bucketName, fileName, 'application/pdf');

		return fileName;
	}
	/** END -- Save emails from Mass Invoices (School Payment) on S3 **/




	/*
		* Method GenerateFileName
		*
		* R = Receive [receive by the employee]
		* S = Sent [sent by the employee]
		* Formats:
		* 		 R + email address for the EMPLOYEE that RECEIVED + subject
		*		 S + email address from the EMPLOYEE that SENT + subject
	*/
	private list<String> generateFileName(EmailDetails emailDetails, String timestamp){

		list<String> lfileName = new list<String>();

		set<id> agencyGroupId = new set<id>();
		for( Contact c : emailDetails.accEmployee){
			if(!agencyGroupId.contains(c.Account.ParentId)){
				agencyGroupId.add(c.Account.ParentId); //Just garantee the email will be saved once in the agencyGroup

				String fileName = IPFunctions.emailFileNameS3(c.Account.Global_Link__c, emailDetails.accClient[0].Id);

				String subject= emailDetails.subject;

				subject = checkSubject(subject);


				if(emailDetails.isFromEmployee){
						fileName+='S:-:'+c.id+':-:'+subject+':-:'+emailDetails.hasAtt+':-:'+emailDetails.dateSent+':-:'+timestamp;
					}else{
						fileName+='R:-:'+c.id+':-:'+subject+':-:'+emailDetails.hasAtt+':-:'+emailDetails.dateSent+':-:'+timestamp;
					}

				System.debug('FileName' + fileName);

				lfileName.add(fileName);

			}
		}

		return lfileName;

	}


	private String checkSubject(String subject){
		/*
		 * Treat the subject to save the file with no one of the characters
		 * '/' create a new folder
		 * ':-:' is our delimiter
		 * '<' and '>' is not allowed in amazon file name
		 */
		if(subject!=null && subject!=''){
			subject=subject.replace('\\', '-');
			subject=subject.replace('/', '-');
			subject=subject.replace(':-:', '-');
			subject=subject.replace('|', '-');
			subject=subject.replace('<', '');
			subject=subject.replace('>', '');
			System.debug('Subject:::' +  subject);
		}else{
			subject='(no subject)';
		}

		return subject;
	}

	/*
		* Method VerifySender
	*/
	public boolean verifySender(EmailDetails emailDetails){

		boolean saveFile;

		//Find the employee using toAddress
		emailDetails.accEmployee= [Select Id, Account.Global_Link__c, Account.ParentId from Contact A where Email in :emailDetails.toAddress and recordType.name = 'Employee' order by LastModifiedDate desc limit 1];

		/* Found EP on toAddresss*/
		if(emailDetails.accEmployee!=null && emailDetails.accEmployee.size() > 0){
			emailDetails.isFromEmployee =  false; // This email was received by the employee

			List<Contact> lct;
			//Step 1 - Look for client with the same global link
			lct = [Select Id, Current_Agency__r.ParentID, Original_Agency__r.ParentID from Contact A where Email = :emailDetails.fromAddress and recordType.name in ('Client', 'Lead') and current_agency__r.Global_Link__c = :emailDetails.accEmployee[0].Account.Global_Link__c order by LastModifiedDate desc];

			if(lct!= null && lct.size()>1){
				//Step 2 - More than 1 client found: Check if there s one with same group as EP
				boolean foundClient = false;

				for(Contact c : lct){
					if(c.Current_Agency__r.ParentId == emailDetails.accEmployee[0].Account.ParentId){
						foundClient = true;
						emailDetails.accClient = new List<Contact>();
						emailDetails.accClient.add(c);
					}
				}

				if(!foundClient){
					//Step 3 - No Client with same Group as EP: Check if there s one with Original Group = EP Group
					for(Contact c : lct){
						if(c.Original_Agency__r.ParentId == emailDetails.accEmployee[0].Account.ParentId){
							foundClient = true;
							emailDetails.accClient = new List<Contact>();
							emailDetails.accClient.add(c);
						}
					}//end for

					if(!foundClient){
						//Stp 4 - No client match - Get the first one of the list
						emailDetails.accClient = new List<Contact>();
						emailDetails.accClient.add(lct[0]);
					}
				}
			}else if(lct!= null && lct.size()>0){
				emailDetails.accClient.add(lct[0]);
			}

		/* Find EP on fromAddresss*/
		}else{
			emailDetails.isFromEmployee =  true;

			emailDetails.accEmployee= [Select Id, Account.Global_Link__c, Account.ParentId from Contact A where Email = :emailDetails.fromAddress and recordType.name = 'Employee' order by LastModifiedDate desc limit 1];

			if(emailDetails.accEmployee!=null && emailDetails.accEmployee.size() > 0){
				List<Contact> lct;
				//Step 1 - Look for client with the same global link
				lct = [Select Id, Current_Agency__r.ParentID, Original_Agency__r.ParentID from Contact A where Email = :emailDetails.toAddress and recordType.name in ('Client', 'Lead') and current_agency__r.Global_Link__c = :emailDetails.accEmployee[0].Account.Global_Link__c order by LastModifiedDate desc];

				if(lct!= null && lct.size()>1){
					//Step 2 - More than 1 client found: Check if there s one with same group as EP
					boolean foundClient = false;

					for(Contact c : lct){
						if(c.Current_Agency__r.ParentId == emailDetails.accEmployee[0].Account.ParentId){
							foundClient = true;
							emailDetails.accClient = new List<Contact>();
							emailDetails.accClient.add(c);
						}
					}

					if(!foundClient){
						//Step 3 - No Client with same Group as EP: Check if there s one with Original Group = EP Group
						for(Contact c : lct){
							if(c.Original_Agency__r.ParentId == emailDetails.accEmployee[0].Account.ParentId){
								foundClient = true;
								emailDetails.accClient = new List<Contact>();
								emailDetails.accClient.add(c);
							}
						}//end for


						if(!foundClient){
							//Stp 4 - No client match - Get the first one of the list
							emailDetails.accClient = new List<Contact>();
							emailDetails.accClient.add(lct[0]);
						}
					}
				}else if(lct!= null && lct.size()>0){
					emailDetails.accClient.add(lct[0]);
				}

			}

		}

		//Verify if one of them wasn't found in Account
		if(emailDetails.accClient!=null && emailDetails.accClient.size() > 0 && emailDetails.accEmployee!=null && emailDetails.accEmployee.size() > 0){
			saveFile = true;
		} else{
			saveFIle = false;
		}
		return saveFIle;
	}

	public void forwardEmailCofirmationCode(String subject){
		String emailService = IPFunctions.getS3EmailService();
		String confirmationCode = subject.substringBetween('(#',')');
		String emailAddress = subject.substringAfterLast(' ');

		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

		String[] toAddresses = new String[] {emailAddress};
		mail.setToAddresses(toAddresses);
		mail.setSenderDisplayName(UserInfo.getOrganizationName() + ' Support');
		mail.setSubject('Gmail Forwarding Confirmation');

		// Set to True if you want to BCC yourself on the email.
		mail.setBccSender(false);

		// Specify the text content of the email.
		mail.setPlainTextBody('Your confirmation code is: ' + confirmationCode);

		mail.setHtmlBody('This email is to confirm forwarding incoming emails to following address: '+emailService+'<p/>'+
			'Your confirmation code is:<b> ' + confirmationCode +' </b><p/>'+
			'Please go to settings and add the confirmation code above and click on the button "verify".<p/>'+

			'<b>Please do not reply this email.</b><p/>'+
			'Regards,<br>'+UserInfo.getOrganizationName() + ' Support Team');

		// Send the email you have created.
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

		System.debug('Code: ' + confirmationCode );
	}


	/*
		* Class EmailDetails
	*/
	public class emailDetails{
		public String subject {get; set;}
		public String fromAddress {get; set;}
		public list<string> toAddress {get; set;}
		public Blob body {get; set;}
		public String messageID {get;set;}
		public String dateSent {get;set;}
		public boolean isFromEmployee {get; set;}
		public String hasAtt {get; set;}
		set<string> setEmails = new set<String>();
		public list<Contact> accClient = new list<Contact>();
		public list<Contact> accEmployee = new list<Contact>();
	}



}