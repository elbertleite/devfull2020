public class FileDeleteClass{
    
    sObject docFile;
	String visaID;
    public FileDeleteClass(ApexPages.StandardController stc){        
		docFile = (sObject)stc.getRecord();
		if(docFile.id == null)
			visaID = ApexPages.currentpage().getParameters().get('visaID');
    }
    
    /** AMAZON S3 **/
    public string secret { get {return credentials.secret;} }
    public AWSKeys credentials {get;set;}
    public string key { get {return credentials.key;} set;}
    private String AWSCredentialName = IPFunctions.awsCredentialName;
    public S3.AmazonS3 as3 { get; private set; }
    public boolean deleteListObjects = false;
    
    
    
    public PageReference deleteAgreeDocs(){
        
        try{
        	String FileName = Apexpages.currentPage().getParameters().get('FileName');
        	String Org = Apexpages.currentPage().getParameters().get('Org');
        	String School = Apexpages.currentPage().getParameters().get('School');
        	String Agreement = Apexpages.currentPage().getParameters().get('Agreement');
        	String LongD = Apexpages.currentPage().getParameters().get('Long');
											
            credentials = new AWSKeys(AWSCredentialName);
            as3 = new S3.AmazonS3(credentials.key,credentials.secret);
            
            Datetime now = Datetime.now();        
           
            S3.Status deleteObjectReslt;
            String objectToDelete; 
            objectToDelete = Org+'/'+School+'/Agreements/'+Agreement+'/'+LongD+'/'+FileName;
            System.debug('about to delete S3 object with key: ' + objectToDelete);
            //This performs the Web Service call to Amazon S3 and create a new bucket.
            deleteObjectReslt= as3.DeleteObject(UserInfo.getOrganizationId(),objectToDelete,as3.key,now,as3.signature('DeleteObject',now), as3.secret);
            System.debug('Successfully deleted a Bucket: ' + deleteObjectReslt.Description);
                            
        	return null;
        } catch(System.CalloutException callout){
            System.debug('CALLOUT EXCEPTION: ' + callout);
            ApexPages.addMessages(callout);
            
            return null;    
        }
        catch(Exception ex){
            System.debug(ex);
            ApexPages.addMessages(ex);
            
            return null;    
        }
        
    }
    
    public PageReference deleteClientDocs(){
        
        try{

            string DocFileId = Apexpages.currentPage().getParameters().get('clientId');
            String FileType = Apexpages.currentPage().getParameters().get('docType');
            String FileName = Apexpages.currentPage().getParameters().get('fileName');
            String parentId = Apexpages.currentPage().getParameters().get('parentId');
            credentials = new AWSKeys(AWSCredentialName);
            as3 = new S3.AmazonS3(credentials.key,credentials.secret);
            
            Datetime now = Datetime.now();        
           
            S3.Status deleteObjectReslt;
            String objectToDelete;
            if(!deleteListObjects){ 
                objectToDelete = DocFileId+'/'+FileType+'/'+parentId+'/'+FileName;
                System.debug('about to delete S3 object with key: ' + objectToDelete);
                //This performs the Web Service call to Amazon S3 and create a new bucket.
                deleteObjectReslt= as3.DeleteObject(UserInfo.getOrganizationId(),objectToDelete,as3.key,now,as3.signature('DeleteObject',now), as3.secret);
                System.debug('Successfully deleted a Bucket: ' + deleteObjectReslt.Description);
            }else{
                for(Client_Document_File__c cdf:docDelete){
                    objectToDelete = cdf.Client__c+'/'+cdf.Document_Type__c+'/'+cdf.ParentId__c+'/'+cdf.File_Name__c;
                    //now = Datetime.now();  
                    deleteObjectReslt= as3.DeleteObject(UserInfo.getOrganizationId(),objectToDelete,as3.key,now,as3.signature('DeleteObject',now), as3.secret);
                    System.debug('Successfully deleted a Bucket: ' + deleteObjectReslt.Description);
                }
            
            }
        
            
            if(!deleteListObjects)
                delete [Select id from Client_Document_File__c where id = :Apexpages.currentPage().getParameters().get('fileId')];  
            
            //getEmergencyContactDocuments();
            
            //PageReference pageRef = new PageReference('/apex/test?bucket=LoO&quoteId='+quoteid+'&campusCourseId='+campusCourseId+'&clientID='+clientId+'&campusId='+campusId);            
            return null;
        } catch(System.CalloutException callout){
            System.debug('CALLOUT EXCEPTION: ' + callout);
            ApexPages.addMessages(callout);
            
            return null;    
        }
        catch(Exception ex){
            System.debug(ex);
            ApexPages.addMessages(ex);
            
            return null;    
        }
        
    }
	
    
    list<Client_Document_File__c> docDelete;
    public pageReference deleteDocument(){
        docDelete = new list<Client_Document_File__c>([Select id, Client__c, File_Name__c, ParentId__c from Client_Document_File__c C where ParentId__c = :docFile.Id]);
            
        //Delete related docs from Client_Document_File__c
        deleteListObjects = true;
        deleteClientDocs();
        
        
        
        delete docDelete;
        
		System.debug('@@@> ' + docFile.id);
		
		if(docFile.id != null)
			delete docFile;
			
        
        PageReference pageRef = new PageReference(ApexPages.currentPage().getParameters().get('page'));         
        pageRef.setRedirect(true);
        return pageRef;
            
    }
	
	/*@Future(Callout=true)
	public static void deleteObjects(List<String> keys){
		
		credentials = new AWSKeys(AWSCredentialName);
        as3 = new S3.AmazonS3(credentials.key,credentials.secret);
		
		XmlStreamWriter xml = new XmlStreamWriter();
		xml.writeStartDocument(null, '1.0');
		xml.writeStartElement(null, 'Delete', null);
		for(String key : keys){
			xml.writeStartElement(null, 'Object', null);
			xml.writeStartElement(null, 'Key', null);
			xml.writeCharacters(key);
			xml.writeEndElement();	
			xml.writeEndElement();				
		}
		xml.writeEndElement();			
		String xmlStr = xml.getXmlString();
		xml.close();
		
		System.debug('@@@ xmlStr: ' + xmlStr);
		
		
		HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
       
		String auth = 'AWS' + ' ' + as3.key + ':' + as3.signature('DeleteObjects',System.now());
        
		Blob keyblob = Blob.valueof(xmlStr);
		Blob key = Crypto.generateDigest('MD5',keyblob);
		String md5 = encodingUtil.convertToHex(key);
		
		System.debug('@@> auth ' + auth);
		System.debug('@@> md5 ' + md5);
		
		//req.setHeader('Delete', null);
		req.setHeader('Authorization', auth );
		req.setHeader('Content-Length', '1024' );
		req.setHeader('Content-MD5', md5 );
		req.setHeader('Host',UserInfo.getOrganizationId()+'.s3.amazonaws.com');
		
		req.setEndPoint('https://s3.amazonaws.com/'+UserInfo.getOrganizationId());
		
		//deleteObjectReslt= as3.DeleteObject(UserInfo.getOrganizationId(),objectToDelete,as3.key,now,as3.signature('DeleteObject',now), as3.secret);
		
        req.setMethod('POST');
        req.setBody(xmlStr);
		
		System.debug('@@> req ' + req);
 
        try {
            res = http.send(req);
        } catch(System.Exception e) {
            System.debug('Callout error: '+ e);
            System.debug(res);
        }
		System.debug(res);
			
		//return 	res.getBody();
		
		
		
	}
	
	*/
	
	
	
	
	
	
}