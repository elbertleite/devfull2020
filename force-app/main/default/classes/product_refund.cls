public with sharing class product_refund {

	public list<client_product_service__c> product {get;set;}
	public String prodCurrency {get;set;}
	public Decimal totProds {get;set;}

	//TODO: CHECK FLOW WHEN THERE ARE EXTRA FEES

	public product_refund() {

		product = [SELECT Related_to_Product__c, Start_Date__c, End_Date__c, Products_Services__r.Provider__r.Provider_Name__c, Category__c, Client__c, Client__r.Name, Client__r.FirstName, Currency__c, Quantity__c, Price_Total__c, Product_Name__c, Unit_Description__c, Client__r.Owner__r.AccountId, Client__r.Owner__r.Contact.Department__c, Agency_Refund_Client__c, Agency_to_Refund__c, Refund_Notes__c,Refund_amount__c, Refund_Date__c, isSelected__c, Refund_Start_Date__c FROM client_product_service__c WHERE Id = :ApexPages.currentPage().getParameters().get('id') order by Related_to_Product__c nulls first, Product_Name__c]; //OR Related_to_Product__c = :ctr.getId() 

		prodCurrency = product[0].Currency__c;
		product[0].Agency_to_Refund__c = currentUser.Contact.AccountId;
		product[0].Refund_Start_Date__c = system.today();


		// totProds = 0;
		// for(client_product_service__c p  : product)
		// 	totProds +=  p.Price_Total__c;
		
	}

	public void saveRefund(){

		product[0].isRefund_Requested__c = true;

		update product;
		
	}



	private User currentUser = [Select Id, Contact.Account.ParentId, Contact.AccountId, Contact.Account.Global_Link__c, Aditional_Agency_Managment__c from User where id = :UserInfo.getUserId() limit 1];

	// Agency Options
	public list<SelectOption> agencyOptions {get{
		if(agencyOptions == null){
			agencyOptions = new List<SelectOption>();
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
				for(Account a: [Select Id, Name, Account_Currency_Iso_Code__c from Account where id =:currentUser.Contact.AccountId order by name]){
					agencyOptions.add(new SelectOption(a.Id, a.Name));
				}
				if(currentUser.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
						agencyOptions.add(new SelectOption(ac.id, ac.name));
					}
				}
			}else{
				for(Account a: [Select Id, Name, Account_Currency_Iso_Code__c from Account WHERE RecordType.Name = 'agency' order by name]){
					agencyOptions.add(new SelectOption(a.Id, a.Name));
				}
			}
		}

		return agencyOptions;
		
	}set;}
}