/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class IPFunctions_test {

    static testMethod void verifyIPFunctions(){
        
        TestFactory testFactory = new TestFactory();
       
        Account account = new Account();
        Map<String,String> recordTypes = new Map<String,String>();
       
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
        
        account.RecordTypeId = recordTypes.get('Agency');
        account.Name = 'IP Sydney';
        account.BillingCity = 'Sydney';
        account.BillingCountry = 'AUS';
        account.BillingState = 'NSW';
        account.BillingStreet = 'Alexander ST';
        account.BillingPostalCode = '2010';
        insert account;
        
        Contact employee = testFactory.createEmployee(account);
        User portalUser = testFactory.createPortalUser(employee);

		Contact lead = testFactory.createLead(account, employee);
        
        account school = testFactory.createSchool();
        account schoolGroup = testFactory.createSchoolGroup();
        
		account campus = testFactory.createCampus(school,account);
        //Account campus = new Account();
		// campus.RecordTypeId = recordTypes.get('Campus');
		// campus.Name = 'Test Campus CBD';
		// campus.BillingCountry = 'Australia';
		// campus.BillingCity = 'Sydney';		
		// insert campus;
		
		// Supplier__c s = new Supplier__c();
		// s.supplier__c = campus.id;
		// s.agency__c = account.id;
		// s.record_type__c = 'campus';
		// insert s;
        
		Course__c course = testFactory.createCourse();
        
        Campus_Course__c campusCourse = new Campus_Course__c();
        campusCourse.Average_Students_Class__c = 10;
        campusCourse.Campus__c = campus.id;
        insert campusCourse;
        
        
        Course_Price__c coursePrice = new Course_Price__c();
        coursePrice.Campus_Course__c = campusCourse.Id;
        coursePrice.Nationality__c = 'Brazil';
        coursePrice.Price_per_week__c = 200;
        coursePrice.From__c = 150;
        insert coursePrice;
        
        coursePrice.From__c = 200;
        update coursePrice;
        
        Course_Extra_Fee__c cef = new Course_Extra_Fee__c();
        cef.Campus_Course__c = campusCourse.Id;
        cef.Nationality__c = 'Brazil';
        insert cef;
        
        Nationality_Group__c ng = new Nationality_Group__c();
        ng.Account__c = account.Id;
        ng.Name = 'Brazil';
        ng.Country__c = 'Brazil';
        insert ng;
       
        Deal__c deal = new Deal__c();
        deal.Campus_Course__c = campusCourse.Id;
        deal.Nationality_Group__c = 'South America';
        insert deal;
                
        
        Contact c = testFactory.createClient(account);
        
        IPFunctions.getAgencyCountries();
        IPFunctions.getCourseCountries(campusCourse.Id);
        IPFunctions.getCourseExtraFeeCountries(campusCourse.Id, campus.id);
        
        //IPFunctions.getNationalityGroups();
        IPFunctions.getNationalityGroups(account.Id);
        
        IPFunctions.getPromotionsCountries(campusCourse.Id);
        delete coursePrice;
        
        List<Account> l = new List<Account>();
        l.add(null);
      	
      	IPFunctions.getNationalityGroups();

		

      	IPFunctions.getCommunityName();
      	IPFunctions.emailFileNameS3(c.Account.ParentId, c.id);      	
      	map<string,string> language = IPFunctions.language;
      	language = IPFunctions.getDisclaimerLanguages();
      	List<SelectOption> so = IPFunctions.CountryDestinations();


      	IPFunctions.SortOptionList(so);
      	IPFunctions.getAllCountries();
      	IPFunctions.getCourseNameWithHours('General English', 25, 5, 'week');
		  
      	IPFunctions.getSchoolsGroup();
      	IPFunctions.getS3EmailService();
		IPFunctions.getUnitTypes();

      	IPFunctions.getLocaleID('Australia');
      	
      	IPFunctions.CampusAvailability ca = new IPFunctions.CampusAvailability();
      	
		List<SelectOption> getSchoolByCountry =  ca.getSchoolByCountry('Australia');
		List<SelectOption> getSchoolGroupByCountry =  ca.getSchoolGroupByCountry('Australia');
		List<SelectOption> getSchoolByCountryGroup =  ca.getSchoolByCountryGroup('Australia','Group');
		List<SelectOption> getAvlSchoolsGroup =  ca.getAvlSchoolsGroup(account.id);
		List<SelectOption> getAvlSchools =  ca.getAvlSchools(account.id);
		List<SelectOption> getAvlSchoolsByGroup =  ca.getAvlSchoolsByGroup(school.id, schoolGroup.id);
		List<SelectOption> getSchoolCampus =  ca.getSchoolCampus(school.id);
		List<SelectOption> getCampusBySchoolNA =  ca.getCampusBySchoolNA(school.id);

      	ca.getSchoolCountries(account.id, true);
      	ca.getSchoolCities(account.id, 'Australia', true);
      	
      	IPFunctions.sendEmail('Pota Toe', 'develop@educationhify.com', 'Bata Ta', 'batata@educationhify.com', 'batatinha quando nasce', 'se esparrama pelo chao.');
      	
      	IPFunctions.getRecordsType();
      	
      	IPFunctions.getContactExceptionMessages('DUPLICATE_VALUE', c);
      	
      	IPFunctions.formatSqlDate(system.today());
      	IPFunctions.formatSQLDate('11/12/13');
      	IPFunctions.FormatSqlDateTimeIni(system.today());
      	IPFunctions.FormatSqlDateTimeFin(system.today());
      	IPFunctions.FormatSqlDateIni(system.today());
      	IPFunctions.FormatSqlDateFin(system.today());
      	IPFunctions.getSearchConfigLabel(null);
      	IPFunctions.getSearchConfigLabel('selectedschools');
      	IPFunctions.getSearchConfigLabel('all');
      	IPFunctions.getSearchConfigLabel('tuition');
      	IPFunctions.getSearchConfigLabel('fullprice');
      	IPFunctions.getSearchConfigLabel('full');
      	IPFunctions.getSearchConfigLabel('instal');
      	IPFunctions.getSearchConfigLabel('totpriceDown');
      	IPFunctions.getSearchConfigLabel('totpriceUp');
      	IPFunctions.getSearchConfigLabel('campusName');
      	IPFunctions.getSearchConfigLabel('onlywithpromo');
      	
      	IPFunctions.getDocumentFiles(c.id, 0);
		IPFunctions.getContactDocuments(c.id, 'key', 'secret');	
			
			
		IPFunctions.updateUserContact(new Set<ID>{c.id});
		IPFunctions.isInternalUser(Userinfo.getuserid());	
		IPFunctions.getUserInformation(Userinfo.getuserid());
		//IPFunctions ipf = new IPFunctions();
		//ipf.createMassClients(1);
		//IPFunctions.deleteMassClients();
		IPFunctions.getFreeWeeksHelpText();
		
		IPFunctions.ContactNoSharing cns = new IPFunctions.ContactNoSharing();
		cns.getContactCurrentAgency(c.id);	
		cns.getCurrentUser(employee.id);

		cns.getEPS(new List<String>{employee.id});
		cns.deleteLeads(new List<String>{lead.id});
		
		IPFunctions.instalmentlFileNameS3('Test', 'Test');
		IPFunctions.SMSInstalmentlFileNameS3('Test', 'Test');
		IPFunctions.EmailInstalmentlFileNameS3('Test', 'Test');
		IPFunctions.IPemailFileNameS3('Test');
			
		List<SelectOption> agencies = IPFunctions.getAgencies();
		List<SelectOption> employees = IPFunctions.getEmployeesByAgency(account.id);	
		List<String> allCountries = IPFunctions.getAllCountriesString();	
      	
      	
      	
      	IPFunctions.Sharing sh = new IPFunctions.Sharing();
      	List<SelectOption> shAgencies = sh.getAgenciesWithSharing();	
      	
      	IPFunctions.CampusAvailability cpa = new IPFunctions.CampusAvailability();
      	List<SelectOption> cpaSchools = cpa.getSchools((String)account.id, true, 'Australia', 'Sydney');	
      	//List<SelectOption> cpaAvlSchools = cpa.getAvlSchools((String)account.id);	
      	List<SelectOption> cpaGroupSchools = cpa.getGroupSchools();	

		sh.verifyRepeatedEmail(c); 
		sh.getContactsByEmail(c.email); 	
      	
      	
      	IPFunctions.agencyNoSharing ans = new IPFunctions.agencyNoSharing();
      	List<SelectOption> ansAgencies = ans.getAgenciesNoSharing();	
      	List<SelectOption> ansEmployees = ans.getEmployeesByAgencyNoSharing((String)account.id);	
      	List<SelectOption> getAllGroups = ans.getAllGroups();	
      	List<SelectOption> getAgenciesByParentNoSharing = ans.getAgenciesByParentNoSharing(account.id);	
      	Account agencyParentDetails = ans.getAgencyParentDetails((String)account.id);
      	map<String,Account> mapAgencyParent =  ans.getListAgencyParentDetails(new set<Id>{account.id});


		IPFunctions.getStringDate(System.today());
		IPFunctions.findTokens('test');

		IPFunctions.LogHistory LogHistory = new IPFunctions.LogHistory();
		LogHistory = new IPFunctions.LogHistory('String fieldName','String fieldLabel', 'String oldValue', 'String newValue', 'String userName', 'String userAgency', 'String editDate');
	
		for(Integer i; i <= 12; i++){
			IPFunctions.getMonthName(i);
		}

		IPFunctions.campusStatus();
		IPFunctions.getFilterDueDate();
		IPFunctions.getBucket();
		IPFunctions.isOrgBucket();
		IPFunctions.getCurrentEnvironment();
		IPFunctions.redirectNewContactPage();
		IPFunctions.getQuotationLinkOptionsToShow();
		IPFunctions.getTravelPlans();
		IPFunctions.getTravelDurations();
		IPFunctions.getProductTypes();
		IPFunctions.getStudyTypes();
		IPFunctions.getMapLanguages();
		IPFunctions.getContactPreferableLanguages();
		IPFunctions.getStringFromDate(Date.today());
		for(String color : new String[]{'Delivered','Refused','Opened','Nothing'}){
			IPFunctions.getMandrillStatusColor(color);
		}
		for(String color : new String[]{'Delivered.pdf','Refused.txt','Opened.png','Nothing.jpg'}){
			IPFunctions.getContentType(color);
		}
		IPFunctions.getTimeInSeconds(Datetime.now());
		IPFunctions.getDateFromString('22/05/1988');
	}
}