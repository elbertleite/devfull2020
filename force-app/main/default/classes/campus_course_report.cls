public class campus_course_report{

    private Account acco;
    public campus_course_report(ApexPages.StandardController controller){
        acco = [Select Id, Parent.ID from Account where id = :controller.getRecord().id];
    }
    
    public class feesDetails{
        public  Course_Extra_Fee__c fee{get; Set;}
        public Boolean isCampusFee{get; Set;}
        public List<Start_Date_Range__c> startDates{get{if(startDates == null) startDates = new List<Start_Date_Range__c>(); return startDates;} Set;}
        public list<relatedFeeDetails> reletedFee{get{if(reletedFee == null) reletedFee = new list<relatedFeeDetails>(); return reletedFee;} Set;}
        // public list<Course_Extra_Fee_Start_Date_Range__c> startDates{get{if(startDates == null) startDates = new list<Course_Extra_Fee_Start_Date_Range__c>(); return startDates;} Set;}
    }
    
    public class priceDetails{
        public  Course_Price__c price{get; Set;}
        public List<Start_Date_Range__c> startDates{get{if(startDates == null) startDates = new List<Start_Date_Range__c>(); return startDates;} Set;}
    }

    public class relatedFeeDetails{
        public  Course_Extra_Fee_Dependent__c fee{get; Set;}
        public List<Start_Date_Range__c> startDates{get{if(startDates == null) startDates = new List<Start_Date_Range__c>(); return startDates;} Set;}
    }

    public class promoDetails{
        public  Deal__c promo{get; Set;}
        public List<Start_Date_Range__c> startDates{get{if(startDates == null) startDates = new List<Start_Date_Range__c>(); return startDates;} Set;}
    }

    public class detailsPerNationality{
        public List<priceDetails> prices{get{if(prices == null) prices = new List<priceDetails>(); return prices;} Set;}
        public List<feesDetails> extraFees{get{if(extraFees == null) extraFees = new List<feesDetails>(); return extraFees;} Set;}
        public List<feesDetails> courseExtraFeesOptional{get{if(courseExtraFeesOptional == null) courseExtraFeesOptional = new List<feesDetails>(); return courseExtraFeesOptional;} Set;}
        public List<feesDetails> optionalExtraFees{get{if(optionalExtraFees == null) optionalExtraFees = new List<feesDetails>(); return optionalExtraFees;} Set;}
        //public List<Course_Extra_Fee_Dependent__c> relatedFees{get{if(relatedFees == null) relatedFees = new List<Course_Extra_Fee_Dependent__c>(); return relatedFees;} Set;}
        public List<promoDetails> extraFeePromotions{get{if(extraFeePromotions == null) extraFeePromotions = new List<promoDetails>(); return extraFeePromotions;} Set;}
        public List<promoDetails> freeUnitsPromotions{get{if(freeUnitsPromotions == null) freeUnitsPromotions = new List<promoDetails>(); return freeUnitsPromotions;} Set;}
        
        
    }
    
    public class courseDetails{
        public String courseName{get; Set;}
        public String unitType{get; Set;}
        public String timetable{get; Set;}
        public String importantInformation{get; Set;}
        public boolean isSoldinBlocks{get; Set;}
        public Map<String, map<string,detailsPerNationality>> nationalityDetail{get{if(nationalityDetail == null) nationalityDetail = new Map<String, map<string,detailsPerNationality>>(); return nationalityDetail;} Set;}
    }
    
    public Map<String, map<string,List<feesDetails>>> campusOptionalExtraFees{get{if(campusOptionalExtraFees == null) campusOptionalExtraFees = new Map<String, map<string,List<feesDetails>>>(); return campusOptionalExtraFees;} Set;}
    
    public List<courseDetails> getCourseDetails(){
        List<courseDetails> ld = new List<courseDetails>();
        courseDetails cd;
        detailsPerNationality dpn; 
        feesDetails fd;
        
        Map<String, Course_Extra_Fee__c> relatedFees = new map<String, Course_Extra_Fee__c>([Select Id, ( Select From_Date__c, To_Date__c, Value__c from Extra_Fee_Start_Date_Prices__r), ( Select Allow_Change_Units__c, Course_Extra_Fee__c, Date_Paid_From__c, Date_Paid_To__c, Date_Start_From__c, Date_Start_To__c, Details__c, From__c, Optional__c, Product__c, Value__c, Product__r.name__c, Account_Document_File__r.Preview_Link__c, Account_Document_File__r.File_Name__c, Account_Document_File__r.Description__c from Course_Extra_Fees_Dependent__r) from Course_Extra_Fee__c where Campus_Course__r.campus__c = :acco.id OR campus__c = :acco.id]);
        
        map<string, list<Start_Date_Range__c>> mapStartDates = new map<string, list<Start_Date_Range__c>>();

        for(Start_Date_Range__c sd:[Select Course_Extra_Fee__c, Course_Extra_Fee_Dependent__c, Course_Price__c, Promotion__c, From_Date__c, To_Date__c, Value__c, Comments__c, Account_Document_File__r.Preview_Link__c, Account_Document_File__r.File_Name__c, Account_Document_File__r.Description__c from Start_Date_Range__c where Campus__c = :acco.id order by From_Date__c, Value__c]){
            if(sd.Course_Extra_Fee__c != null){
                if(!mapStartDates.containsKey(sd.Course_Extra_Fee__c))
                    mapStartDates.put(sd.Course_Extra_Fee__c, new list<Start_Date_Range__c>{sd});
                else mapStartDates.get(sd.Course_Extra_Fee__c).add(sd);
            }else if(sd.Course_Extra_Fee_Dependent__c != null){
                if(!mapStartDates.containsKey(sd.Course_Extra_Fee_Dependent__c))
                    mapStartDates.put(sd.Course_Extra_Fee_Dependent__c, new list<Start_Date_Range__c>{sd});
                else mapStartDates.get(sd.Course_Extra_Fee_Dependent__c).add(sd);
            }else if(sd.Course_Price__c != null){
                if(!mapStartDates.containsKey(sd.Course_Price__c))
                    mapStartDates.put(sd.Course_Price__c, new list<Start_Date_Range__c>{sd});
                else mapStartDates.get(sd.Course_Price__c).add(sd);
            }else if(sd.Promotion__c != null){
                if(!mapStartDates.containsKey(sd.Promotion__c))
                    mapStartDates.put(sd.Promotion__c, new list<Start_Date_Range__c>{sd});
                else mapStartDates.get(sd.Promotion__c).add(sd);
            }
        }
        
        //Map<String, list<Course_Extra_Fee__c>> campusFees = new Map<String, list<Course_Extra_Fee__c>>();
        //Map<String, list<Course_Extra_Fee__c>> campusOptionalFees = new Map<String, list<Course_Extra_Fee__c>>();
        
        String sqlCampusFees = 'Select Allow_change_units__c, Availability__c, Extra_Fee__c, Extra_fee_type__c, From__c, Nationality__c, Value__c, Product__c, Product__r.name__c, Product__r.Product_Type__c , Combine_fees__c, Optional__c, date_paid_from__c, date_paid_to__c, ';
        //sqlCampusFees += ' Account_Document_File__c, Extra_info__c, ( Select Allow_Change_Units__c, Course_Extra_Fee__c, Date_Paid_From__c, Date_Paid_To__c, Date_Start_From__c, Date_Start_To__c, Details__c, From__c, Optional__c, Product__c, Value__c, Product__r.name__c from Course_Extra_Fees_Dependent__r) from Course_Extra_Fee__c where Campus__c = \''+acco.Id+'\' and Nationality__c in :selectedNationalities order by Nationality__c, Extra_Fee__c, From__c';
        sqlCampusFees += ' Account_Document_File__r.Preview_Link__c, Account_Document_File__r.File_Name__c, Account_Document_File__r.Description__c, Extra_info__c, ( Select Allow_Change_Units__c, Course_Extra_Fee__c, Date_Paid_From__c, Date_Paid_To__c, Date_Start_From__c, Date_Start_To__c, Details__c, From__c, Optional__c, Product__c, Value__c, Product__r.name__c, Account_Document_File__r.Preview_Link__c, Account_Document_File__r.File_Name__c, Account_Document_File__r.Description__c from Course_Extra_Fees_Dependent__r) from Course_Extra_Fee__c where Campus__c = \''+acco.Id+'\'  order by Nationality__c, Extra_Fee__c, From__c';
        /*for(Course_Extra_Fee__c cf:Database.query(sqlCampusFees))
            if(cf.Optional__c){
                if(!campusOptionalFees.containsKey(cf.Nationality__c))
                    campusOptionalFees.put(cf.Nationality__c, new list<Course_Extra_Fee__c>{cf});
                else campusOptionalFees.get(cf.Nationality__c).add(cf);
            }else{
                if(!campusFees.containsKey(cf.Nationality__c))
                    campusFees.put(cf.Nationality__c, new list<Course_Extra_Fee__c>{cf});
                else campusFees.get(cf.Nationality__c).add(cf);
            }*/
        
        String sql = 'Select C.Campus__r.BillingStreet, C.Time_Table__c,  C.Campus__r.Name, C.Campus__r.Parent.Name, C.Course__r.Name, Campus__r.ParentId, ';
        sql += ' Course__r.Only_Sold_in_Blocks__c, C.Campus__r.Campus_Photo_URL__c, Course__r.Maximum_length__c, Course__r.Minimum_length__c, Course__r.Course_Unit_Type__c, Course__r.Important_information__c, ';
        sql += ' ( Select From__c, Availability__c, LastModifiedDate, Price_valid_from__c, Price_valid_until__c, Nationality__c, To__c, Unavailable__c, unit_description__c, Price_per_week__c, Account_Document_File__r.Preview_Link__c, Account_Document_File__r.File_Name__c, Account_Document_File__r.Description__c ';
        sql += ' from Course_Prices__r ';
        sql += ' where Nationality__c in :selectedNationalities ';
        sql += ' order by Nationality__c, Availability__c, Price_valid_until__c, From__c), ';
        sql += ' ( Select Availability__c, Extra_Fee_Name__c, Extra_Fee_Value__c, Extra_Fee_Type__c, Extra_Fee__c, Extra_Fee_Dependent__c, From__c, Promotion_Type__c, Promotion_Weeks__c, Number_of_Free_Weeks__c, From_Date__c, Nationality_Group__c, Promotion_Name__c, ';
        sql += ' To_Date__c, Product__c, Product__r.name__c, Account_Document_File__r.Preview_Link__c, Account_Document_File__r.File_Name__c, Account_Document_File__r.Description__c, Comments__c from Promotions__r ';
        sql += ' where Nationality_Group__c in :selectedNationalities ';
        sql += ' order by Nationality_Group__c, Promotion_Type__c, Number_of_Free_Weeks__c), ';
        sql += ' ( Select Allow_change_units__c, Availability__c, Extra_Fee__c, Extra_fee_type__c, From__c, Nationality__c, Value__c, Product__c, Product__r.name__c, Product__r.Product_Type__c , Combine_fees__c, Optional__c, date_paid_from__c, date_paid_to__c, ';
        sql += ' Account_Document_File__r.Preview_Link__c, Account_Document_File__r.File_Name__c, Account_Document_File__r.Description__c, Extra_info__c from Course_Extra_Fees__r ';
        sql += ' where Nationality__c in :selectedNationalities ';
        sql += ' order by Nationality__c, Extra_Fee__c, Availability__c, From__c) from Campus_Course__c C where Is_Available__c = true and campus__c = \''+acco.id+'\' ';
        sql += ' and Id in :courseClone ';
        
        system.debug('@@@ courseClone ' + courseClone);
        system.debug('@@@ sql ' +sql );
        
        
        
        
        for(Campus_Course__c cc: Database.query(sql)){
            cd = new courseDetails();
            cd.courseName = cc.Course__r.Name;
            cd.isSoldinBlocks = cc.Course__r.Only_Sold_in_Blocks__c;
            cd.unitType = cc.Course__r.Course_Unit_Type__c;
            if(cc.Time_Table__c != null)
                cd.timetable = cc.Time_Table__c.replaceAll('\n', '<br>');
            if(cc.Course__r.Important_information__c != null)
                cd.importantInformation = cc.Course__r.Important_information__c.replaceAll('\n', '<br>');
            
            cd.nationalityDetail = new  Map<String, map<string,detailsPerNationality>>();
            for(Course_Price__c cp:cc.Course_Prices__r){
                
                if(!cd.nationalityDetail.containsKey(cp.Nationality__c)){
                    dpn = new detailsPerNationality();
                    priceDetails pd = new priceDetails();
                    pd.price = cp;
                    pd.startDates = new List<Start_Date_Range__c>();
                    if(mapStartDates.containsKey(cp.id))
                        pd.startDates.addAll(mapStartDates.get(cp.id));
                    dpn.prices.add(pd);
                    cd.nationalityDetail.put(cp.Nationality__c, new map<string,detailsPerNationality>{''+cp.Price_valid_from__c+cp.Price_valid_until__c => dpn});
                }else{
                    if(!cd.nationalityDetail.get(cp.Nationality__c).containsKey(''+cp.Price_valid_from__c+cp.Price_valid_until__c)){
                        dpn = new detailsPerNationality();
                        priceDetails pd = new priceDetails();
                        pd.price = cp;
                        pd.startDates = new List<Start_Date_Range__c>();
                        if(mapStartDates.containsKey(cp.id))
                            pd.startDates.addAll(mapStartDates.get(cp.id));
                        dpn.prices.add(pd);
                        cd.nationalityDetail.get(cp.Nationality__c).put(''+cp.Price_valid_from__c+cp.Price_valid_until__c, dpn);
                    } else{
                        priceDetails pd = new priceDetails();
                        pd.price = cp;
                        pd.startDates = new List<Start_Date_Range__c>();
                        if(mapStartDates.containsKey(cp.id))
                            pd.startDates.addAll(mapStartDates.get(cp.id));
                        cd.nationalityDetail.get(cp.Nationality__c).get(''+cp.Price_valid_from__c+cp.Price_valid_until__c).prices.add(pd);
                    } 
                }
            }
            //if(cd.nationalityDetail.get(cp.Nationality__c).coursePrice.size() > 0 && !pd.coursePrice[0].Campus_Course__r.Course__r.Only_Sold_in_Blocks__c)
            for(String nt:cd.nationalityDetail.keySet()){
                System.debug('======>nt: '+nt);
                for(String vl:cd.nationalityDetail.get(nt).keySet()){
                    LIST<priceDetails> lcp = cd.nationalityDetail.get(nt).get(vl).prices;
                    for(Integer i = 0; i < lcp.size(); i++)
                        if(i < lcp.size()-1 && lcp[i+1].price != null && lcp[i+1].price.Nationality__c == lcp[i].price.Nationality__c && lcp[i+1].price.Availability__c == lcp[i].price.Availability__c && lcp[i+1].price.Price_valid_from__c == lcp[i].price.Price_valid_from__c && lcp[i+1].price.Price_valid_until__c == lcp[i].price.Price_valid_until__c){
                            lcp[i].price.to__c = lcp[i+1].price.From__c - 1;
                        }
                }
            }
            for(Course_Extra_Fee__c cef:cc.Course_Extra_Fees__r){
                if(!cd.nationalityDetail.containsKey(cef.Nationality__c)){
                    dpn = new detailsPerNationality();
                    fd = new feesDetails();
                    fd.fee = cef;
                    fd.startDates = new List<Start_Date_Range__c>();
                    if(mapStartDates.containsKey(cef.id))
                        fd.startDates.addAll(mapStartDates.get(cef.id));
                    if(relatedFees.containsKey(cef.id)){
                        relatedFeeDetails feeDependent;
                        for(Course_Extra_Fee_Dependent__c fr:relatedFees.get(cef.id).Course_Extra_Fees_Dependent__r){
                            feeDependent = new relatedFeeDetails();
                            feeDependent.fee = fr;
                            feeDependent.startDates = new List<Start_Date_Range__c>();
                            if(mapStartDates.containsKey(fr.id))
                                feeDependent.startDates.addAll(mapStartDates.get(fr.id));
                            fd.reletedFee.add(feeDependent);
                        }
                        //fd.startDates.addAll(relatedFees.get(cef.id).Extra_Fee_Start_Date_Prices__r);
                    }
                    if(!cef.Optional__c)
                        dpn.extraFees.add(fd);
                    else dpn.courseExtraFeesOptional.add(fd);
                    cd.nationalityDetail.put(cef.Nationality__c, new map<string,detailsPerNationality>{''+cef.date_paid_from__c+cef.date_paid_to__c => dpn});
                }else{
                    if(!cd.nationalityDetail.get(cef.Nationality__c).containsKey(''+cef.date_paid_from__c+cef.date_paid_to__c)){
                        dpn = new detailsPerNationality();
                        fd = new feesDetails();
                        fd.fee = cef;
                        fd.startDates = new List<Start_Date_Range__c>();
                        if(mapStartDates.containsKey(cef.id))
                            fd.startDates.addAll(mapStartDates.get(cef.id));
                        if(relatedFees.containsKey(cef.id)){
                            relatedFeeDetails feeDependent;
                            for(Course_Extra_Fee_Dependent__c fr:relatedFees.get(cef.id).Course_Extra_Fees_Dependent__r){
                                feeDependent = new relatedFeeDetails();
                                feeDependent.fee = fr;
                                feeDependent.startDates = new List<Start_Date_Range__c>();
                                if(mapStartDates.containsKey(fr.id))
                                    feeDependent.startDates.addAll(mapStartDates.get(fr.id));
                                fd.reletedFee.add(feeDependent);
                            }
                            //fd.startDates.addAll(relatedFees.get(cef.id).Extra_Fee_Start_Date_Prices__r);
                        }
                        if(!cef.Optional__c)
                            dpn.extraFees.add(fd);
                        else dpn.courseExtraFeesOptional.add(fd);
                        cd.nationalityDetail.get(cef.Nationality__c).put(''+cef.date_paid_from__c+cef.date_paid_to__c, dpn);
                    }else{ 
                        fd = new feesDetails();
                        fd.fee = cef;
                        fd.startDates = new List<Start_Date_Range__c>();
                        if(mapStartDates.containsKey(cef.id))
                            fd.startDates.addAll(mapStartDates.get(cef.id));
                        if(relatedFees.containsKey(cef.id)){
                            relatedFeeDetails feeDependent;
                            for(Course_Extra_Fee_Dependent__c fr:relatedFees.get(cef.id).Course_Extra_Fees_Dependent__r){
                                feeDependent = new relatedFeeDetails();
                                feeDependent.fee = fr;
                                feeDependent.startDates = new List<Start_Date_Range__c>();
                                if(mapStartDates.containsKey(fr.id))
                                    feeDependent.startDates.addAll(mapStartDates.get(fr.id));
                                fd.reletedFee.add(feeDependent);
                            }
                            //fd.startDates.addAll(relatedFees.get(cef.id).Extra_Fee_Start_Date_Prices__r);
                        }
                        if(!cef.Optional__c)
                            cd.nationalityDetail.get(cef.Nationality__c).get(''+cef.date_paid_from__c+cef.date_paid_to__c).extraFees.add(fd);
                        else cd.nationalityDetail.get(cef.Nationality__c).get(''+cef.date_paid_from__c+cef.date_paid_to__c).courseExtraFeesOptional.add(fd);
                    }
                }
            }
            //Add campus extraFees
            for(Course_Extra_Fee__c cef:Database.query(sqlCampusFees)){
                if(!cef.Optional__c){
                    if(!cd.nationalityDetail.containsKey(cef.Nationality__c)){
                        fd = new feesDetails();
                        fd.fee = cef;
                        fd.isCampusFee = true;
                        fd.startDates = new List<Start_Date_Range__c>();
                        if(mapStartDates.containsKey(cef.id))
                            fd.startDates.addAll(mapStartDates.get(cef.id));
                        if(relatedFees.containsKey(cef.id)){
                            relatedFeeDetails feeDependent;
                            for(Course_Extra_Fee_Dependent__c fr:relatedFees.get(cef.id).Course_Extra_Fees_Dependent__r){
                                feeDependent = new relatedFeeDetails();
                                feeDependent.fee = fr;
                                feeDependent.startDates = new List<Start_Date_Range__c>();
                                if(mapStartDates.containsKey(fr.id))
                                    feeDependent.startDates.addAll(mapStartDates.get(fr.id));
                                fd.reletedFee.add(feeDependent);
                            }
                            //fd.startDates.addAll(relatedFees.get(cef.id).Extra_Fee_Start_Date_Prices__r);
                        }
                        dpn = new detailsPerNationality();
                        dpn.extraFees.add(fd);
                        cd.nationalityDetail.put(cef.Nationality__c, new map<string,detailsPerNationality>{''+cef.date_paid_from__c+cef.date_paid_to__c => dpn});
                    }else{
                        if(!cd.nationalityDetail.get(cef.Nationality__c).containsKey(''+cef.date_paid_from__c+cef.date_paid_to__c)){
                            fd = new feesDetails();
                            fd.fee = cef;
                            fd.isCampusFee = true;
                            fd.startDates = new List<Start_Date_Range__c>();
                            if(mapStartDates.containsKey(cef.id))
                                fd.startDates.addAll(mapStartDates.get(cef.id));
                            if(relatedFees.containsKey(cef.id)){
                                relatedFeeDetails feeDependent;
                                for(Course_Extra_Fee_Dependent__c fr:relatedFees.get(cef.id).Course_Extra_Fees_Dependent__r){
                                    feeDependent = new relatedFeeDetails();
                                    feeDependent.fee = fr;
                                    feeDependent.startDates = new List<Start_Date_Range__c>();
                                    if(mapStartDates.containsKey(fr.id))
                                        feeDependent.startDates.addAll(mapStartDates.get(fr.id));
                                    fd.reletedFee.add(feeDependent);
                                }
                                //fd.startDates.addAll(relatedFees.get(cef.id).Extra_Fee_Start_Date_Prices__r);
                            }
                            dpn = new detailsPerNationality();
                            dpn.extraFees.add(fd);
                            cd.nationalityDetail.get(cef.Nationality__c).put(''+cef.date_paid_from__c+cef.date_paid_to__c, dpn);
                        }else{ 
                            fd = new feesDetails();
                            fd.fee = cef;
                            fd.isCampusFee = true;
                            fd.startDates = new List<Start_Date_Range__c>();
                            if(mapStartDates.containsKey(cef.id))
                                fd.startDates.addAll(mapStartDates.get(cef.id));
                            if(relatedFees.containsKey(cef.id)){
                                relatedFeeDetails feeDependent;
                                for(Course_Extra_Fee_Dependent__c fr:relatedFees.get(cef.id).Course_Extra_Fees_Dependent__r){
                                    feeDependent = new relatedFeeDetails();
                                    feeDependent.fee = fr;
                                    feeDependent.startDates = new List<Start_Date_Range__c>();
                                    if(mapStartDates.containsKey(fr.id))
                                        feeDependent.startDates.addAll(mapStartDates.get(fr.id));
                                    fd.reletedFee.add(feeDependent);
                                }
                                //fd.startDates.addAll(relatedFees.get(cef.id).Extra_Fee_Start_Date_Prices__r);
                            }
                            cd.nationalityDetail.get(cef.Nationality__c).get(''+cef.date_paid_from__c+cef.date_paid_to__c).extraFees.add(fd);
                        }
                    }
                }
            }
            //Add campus extraFees
            /*for(Course_Extra_Fee__c cef:Database.query(sqlCampusFees)){
                if(!cd.nationalityDetail.containsKey(cef.Nationality__c)){
                    dpn = new detailsPerNationality();
                    if(!cef.Optional__c)
                        dpn.extraFees.add(cef);
                    else dpn.optionalExtraFees.add(cef);
                    cd.nationalityDetail.put(cef.Nationality__c, new map<string,detailsPerNationality>{''+cef.date_paid_from__c+cef.date_paid_to__c => dpn});
                }else{
                    if(!cd.nationalityDetail.get(cef.Nationality__c).containsKey(''+cef.date_paid_from__c+cef.date_paid_to__c)){
                        dpn = new detailsPerNationality();
                        if(!cef.Optional__c)
                            dpn.extraFees.add(cef);
                        else dpn.optionalExtraFees.add(cef);
                        cd.nationalityDetail.get(cef.Nationality__c).put(''+cef.date_paid_from__c+cef.date_paid_to__c, dpn);
                    }else{ 
                        if(!cef.Optional__c)
                            cd.nationalityDetail.get(cef.Nationality__c).get(''+cef.date_paid_from__c+cef.date_paid_to__c).extraFees.add(cef);
                        else cd.nationalityDetail.get(cef.Nationality__c).get(''+cef.date_paid_from__c+cef.date_paid_to__c).optionalExtraFees.add(cef);
                    }
                }
            }*/
                
            promoDetails pd;
            for(Deal__c d: cc.Promotions__r){
                if(!cd.nationalityDetail.containsKey(d.Nationality_Group__c)){
                    dpn = new detailsPerNationality();
                    
                    pd = new promoDetails();
                    pd.startDates = new List<Start_Date_Range__c>();
                    pd.promo = d;
                    if(mapStartDates.containsKey(d.id))
                        pd.startDates.addAll(mapStartDates.get(d.id));
                    if(d.Promotion_Type__c == 3)
                        dpn.extraFeePromotions.add(pd);
                    else dpn.freeUnitsPromotions.add(pd);
                    cd.nationalityDetail.put(d.Nationality_Group__c, new map<string,detailsPerNationality>{''+d.From_Date__c+d.To_Date__c => dpn});
                }else{
                    if(!cd.nationalityDetail.get(d.Nationality_Group__c).containsKey(''+d.From_Date__c+d.To_Date__c)){
                        dpn = new detailsPerNationality();
                        pd = new promoDetails();
                        pd.startDates = new List<Start_Date_Range__c>();
                        pd.promo = d;
                        if(mapStartDates.containsKey(d.id))
                            pd.startDates.addAll(mapStartDates.get(d.id));
                        if(d.Promotion_Type__c == 3)
                            dpn.extraFeePromotions.add(pd);
                        else dpn.freeUnitsPromotions.add(pd);
                        cd.nationalityDetail.get(d.Nationality_Group__c).put(''+d.From_Date__c+d.To_Date__c, dpn);
                    }else{
                        pd = new promoDetails();
                        pd.startDates = new List<Start_Date_Range__c>();
                        pd.promo = d;
                        if(mapStartDates.containsKey(d.id))
                            pd.startDates.addAll(mapStartDates.get(d.id));
                        if(d.Promotion_Type__c == 3)
                            cd.nationalityDetail.get(d.Nationality_Group__c).get(''+d.From_Date__c+d.To_Date__c).extraFeePromotions.add(pd);
                        else cd.nationalityDetail.get(d.Nationality_Group__c).get(''+d.From_Date__c+d.To_Date__c).freeUnitsPromotions.add(pd);
                    }
                }
            }
            ld.add(cd);
        }

        feesDetails feed;
        for(Course_Extra_Fee__c cef:Database.query(sqlCampusFees)){
            if(cef.Optional__c){
                feed = new feesDetails();
                feed.fee = cef;
                feed.startDates = new List<Start_Date_Range__c>();
                if(mapStartDates.containsKey(cef.id))
                    feed.startDates.addAll(mapStartDates.get(cef.id));
                relatedFeeDetails feeDependent;
                for(Course_Extra_Fee_Dependent__c fr:cef.Course_Extra_Fees_Dependent__r){
                    feeDependent = new relatedFeeDetails();
                    feeDependent.fee = fr;
                    feeDependent.startDates = new List<Start_Date_Range__c>();
                    if(mapStartDates.containsKey(fr.id))
                        feeDependent.startDates.addAll(mapStartDates.get(fr.id));
                    feed.reletedFee.add(feeDependent);
                }
                if(!campusOptionalExtraFees.containsKey(cef.Nationality__c))
                    campusOptionalExtraFees.put(cef.Nationality__c, new map<string,list<feesDetails>>{''+cef.date_paid_from__c+cef.date_paid_to__c => new list<feesDetails>{feed}});
                else{
                    if(!campusOptionalExtraFees.get(cef.Nationality__c).containsKey(''+cef.date_paid_from__c+cef.date_paid_to__c))
                        campusOptionalExtraFees.get(cef.Nationality__c).put(''+cef.date_paid_from__c+cef.date_paid_to__c, new list<feesDetails>{feed});
                    else campusOptionalExtraFees.get(cef.Nationality__c).get(''+cef.date_paid_from__c+cef.date_paid_to__c).add(feed);
                }
            }
        }
        return ld;





        
        
    }

    //============================FILTERS=====================================
    
    public List<string> selectedNationalities{get{if(selectedNationalities == null) selectedNationalities = new List<string>(); return selectedNationalities;} Set;}
    private List<SelectOption> NationalitiesEdit;
    public List<SelectOption> getNationalitiesEdit(){
        if(NationalitiesEdit == null){
            getnationalityGroupHint();
            NationalitiesEdit = new List<SelectOption>();
            NationalitiesEdit.add(new SelectOption('Published Price','[*Every nationality that it is not included in a group of nationalities*]Published Price**]'));
            for (AggregateResult lnt:[Select Name from Nationality_Group__c WHERE Account__c = :acco.Parent.ID GROUP BY name order by name])
                NationalitiesEdit.add(new SelectOption((string)lnt.get('name'),'[*'+nationalityGroupHint.get((string)lnt.get('name')) + '*]' + (string)lnt.get('name') + '**]'));
        }
        return NationalitiesEdit;
    }
    
    public List<string> selectedCourseType{get{if(selectedCourseType == null) selectedCourseType = new List<string>(); return selectedCourseType;} Set;}
    private List<SelectOption> selectedCourseTypeOptions;
    public List<SelectOption> getselectedCourseTypeOptions(){
        if(selectedCourseTypeOptions == null){
            selectedCourseTypeOptions = new List<SelectOption>();
            for (AggregateResult lnt:[Select Course__r.Type__c type from Campus_Course__c WHERE campus__c = :acco.Id and Course__r.Type__c!=null GROUP BY Course__r.Type__c order by Course__r.Type__c]){
                selectedCourseTypeOptions.add(new SelectOption((string)lnt.get('type'),(string)lnt.get('type')));
                selectedCourseType.add((string)lnt.get('type'));
            }
        }
        return selectedCourseTypeOptions;
    }
    
    private Map<String, string> nationalityGroupHint;
    public Map<String, string> getnationalityGroupHint(){
        if(nationalityGroupHint == null){
            nationalityGroupHint = new Map<String, string>();
            for(Nationality_Group__c ng:[Select Country__c, Name from Nationality_Group__c WHERE Account__c = :acco.Parent.Id order by Country__c])
                if(!nationalityGroupHint.containsKey(ng.name))
                    nationalityGroupHint.put(ng.Name, ng.country__c);
                else nationalityGroupHint.put(ng.Name,nationalityGroupHint.get(ng.Name) + ', '+ ng.country__c);
        }
        return nationalityGroupHint;
    }
    
        
    public boolean CourseAvailable{get{if(CourseAvailable == null) CourseAvailable = true; return CourseAvailable;} Set;}
    private List<SelectOption> CourseAvailableList;
    public List<SelectOption> getCourseAvailableList() {
        if(CourseAvailableList == null){
            CourseAvailableList = new List<SelectOption>();
            CourseAvailableList.add(new SelectOption('true','Available'));
            CourseAvailableList.add(new SelectOption('false','Not Available'));
        }
        return CourseAvailableList;
    }
    
    public string period {get{if(period == null) period = 'any'; return period;} set;}
    private List<SelectOption> Periods;
    public List<SelectOption> getPeriods(){
        if(Periods == null){
            Periods = new List<SelectOption>();
            Periods.add(new selectOption('any', '--Any Period--'));
            for (AggregateResult lnt:[Select course__r.Period__c period from Campus_Course__c where campus__c = :acco.id Group by course__r.Period__c]){
                if((string)lnt.get('period') != null)
                    Periods.add(new SelectOption((string)lnt.get('period'),(string)lnt.get('period')));
            }
        }
        return Periods;
    }
    
    public List<Campus_Course__c> lst{get{if (lst == null) lst = new List<Campus_Course__c>(); return lst;}set;}
     private map<string,string> CourseListCloneSelected;
    public list<String> courseClone {get{if(courseClone == null) courseClone = new list<String>(); return courseClone;}set;}     
    public List<SelectOption> getCourseListClone() {
        List<SelectOption> options = new List<SelectOption>();
        CourseListCloneSelected = new map<string,string>();
        String sql; 
         sql = 'Select C.Course__r.Name, C.Is_Available__c from Campus_Course__c C WHERE Is_Available__c = :CourseAvailable and C.Campus__r.ID = \'' +acco.ID+'\' and course__r.Type__c in :selectedCourseType ';
        if(period != 'any')
            sql += ' and Course__r.Period__c = \''+period+'\'';
        sql += ' order by Course__r.Name';
        
        system.debug('@@@@ : ' + sql);
        
        for (Campus_Course__c lst:Database.query(sql)){
        options.add(new SelectOption(lst.ID,lst.Course__r.Name));
        CourseListCloneSelected.put(lst.ID,lst.Course__r.Name);
    }
        return options;
    }
}