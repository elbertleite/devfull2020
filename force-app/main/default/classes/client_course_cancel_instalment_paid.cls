public with sharing class client_course_cancel_instalment_paid {

    private string instId;
    private string depositId;
    private client_course__c clientDeposit;
    public boolean showConfirm {get{if(showConfirm==null) showConfirm = false; return showConfirm;}set;}

	public client_course_cancel_instalment_paid(ApexPages.standardController controller){
		instId = controller.getId();
		retrieveInstalment();
	}

	public client_course_instalment__c installment{get; set;}

	public void retrieveInstalment(){
		try{
	    	installment = [SELECT Id, Cancel_Payment_Reason__c, Tuition_Value__c, Extra_Fee_Value__c, Instalment_Value__c, Discount__c, Received_by__r.name, Received_Date__c, Received_by_Department__r.name, Received_By_Agency__r.name, client_course__r.Campus_Name__c, client_course__r.School_Name__c, client_course__r.Client__c, client_course__r.Client__r.name, client_course__r.Course_Name__c, Covered_by_Agency__c, Total_Paid_School_Credit__c, client_course__r.Client__r.Current_agency__c, client_course__r.Total_Extra_Fees__c, client_course__r.CurrencyIsoCode__c, isPFS__c, isPDS__c, isPCS__c, instalment_activities__c, Request_Commission_Invoice__c, client_course__r.Total_Paid__c, client_course__r.Total_Paid_Credit__c, client_course__r.Total_Paid_Deposit__c, client_course__r.Total_Paid_PDS__c, client_course__r.Total_Paid_PFS__c, client_course__r.isCancelled__c, isMigrated__c, ( Select Bank_Deposit__c, Booking_Closed_On__c, client_product_service__c, client_course_instalment__c, Date_Paid__c, Deposit__c, Invoice__c, Transfered_from_Deposit__c, Paid_From_Deposit__c, Payment_Type__c, Payment_Plan__c, Received_By__c, Received_By_Agency__c, Received_On__c, Related_Credit__c, Value__c, Confirmed_By__c, Confirmed_By_Agency__c, Confirmed_Date__c from client_course_instalment_payments__r) FROM client_course_instalment__c WHERE Id = :instId and Paid_To_School_On__c = null and School_Payment_Invoice__c = null];
			/*for(client_course_instalment_payment__c ccip:installment.client_course_instalment_payments__r)
				if(ccip.Payment_Type__c == 'Deposit'){
					depositId = ccip.Paid_From_Deposit__c;
					break;
				}*/
			if(installment.Covered_by_Agency__c==null)
				installment.Covered_by_Agency__c = 0;

			retrieveDeposit(installment.client_course__r.Client__c);
      showConfirm = true;
		}catch(Exception e){
			// ApexPages.addMessages(e);
      showConfirm = false;
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'This instalment either has already been paid to the school or belongs to a pending invoice. You cannot cancel the payment in those situations, so cancel the invoice if possible before cancelling the payment.'));
		}
	}


	private void retrieveDeposit(string contactId){
		try{
			string sql = 'Select Deposit_Description__c, Deposit_Total_Used__c, Deposit_Total_Value__c, Deposit_Total_Available__c, Deposit_Type__c, Deposit_Value_Track__c, Id, client__c, Client__r.name, CurrencyIsoCode__c, ';
		 		sql += ' (SELECT Id, Value__c, Received_By__r.Name, Received_On__c, Received_By_Agency__r.Name, CurrencyIsoCode__c, Payment_Type__c, Date_Paid__c, deposit__c, client_course_instalment__c, invoice__c FROM client_course_payments_deposit__r) ';
		 		sql += ' FROM client_course__c where client__c = :contactId and ';

				/*if(depositId != null)
					sql += ' id = :depositId ';
				else */
					sql += ' isDeposit__c = true and Deposit_Total_Available__c > 0 ';

		 	clientDeposit = Database.query(sql);
		}catch(Exception e){
			if(!installment.isPDS__c && !installment.isPCS__c)
				clientDeposit = new client_course__c(Deposit_Reconciliated__c = true, isDeposit__c = true, client__c = contactId, Deposit_Total_Value__c =0, Deposit_Total_Used__c=0,Deposit_Total_Available__c=0);
		}
	}

	public boolean showError {get{if(showError==null) showError = true; return showError;}set;}
	public String errorMsg {get{if(errorMsg == null) errorMsg = ''; return errorMsg;}set;}
	private User currentUser = [Select Id, Name, Contact.AccountId, Contact.Department__c, Contact.Account.ParentId from User where id = :UserInfo.getUserId() limit 1];
	public pageReference cancelPayment(){
		Savepoint sp;
		errorMsg = '';
		showError = true;
		try{
			sp = Database.setSavepoint();
			list<client_course_instalment_payment__c> installCreditDelete = new list<client_course_instalment_payment__c>();
			list<client_course_instalment_payment__c> toDelete  = new list<client_course_instalment_payment__c>();//Used for 'Client Scholarship' and 'Covered by Agency'
			if(!installment.isPDS__c && !installment.isPCS__c && !installment.isMigrated__c){
				//Insert deposit object
				clientDeposit.isDeposit__c = true;
				//clientDeposit.  = false;


				if(clientDeposit.id==null)
					clientDeposit.CurrencyIsoCode__c = installment.client_course__r.CurrencyIsoCode__c;
				else if(installment.client_course__r.CurrencyIsoCode__c != clientDeposit.CurrencyIsoCode__c){
					 errorMsg += '<span style="display:block">Deposit Currency is Different from Course Currency.</span>';
					return null;
				}
				upsert clientDeposit;

				for(client_course_instalment_payment__c ccip:installment.client_course_instalment_payments__r){
					if(installment.isPFS__c)
						installment.client_course__r.Total_Paid_PFS__c -= ccip.Value__c;

					installment.client_course__r.Total_Paid__c -= ccip.Value__c;
					if(ccip.Payment_Type__c != 'Deposit' && ccip.Payment_Type__c != 'School Credit' && ccip.Payment_Type__c != 'Client Loan'){


						if(ccip.Payment_Type__c.toLowerCase() == 'client scholarship'){
							//installment.Client_Scholarship__c -= ccip.Value__c;
							toDelete.add(ccip);//delete payment

						}
						else if(ccip.Payment_Type__c.toLowerCase() == 'covered by agency'){
							installment.Covered_by_Agency__c -= ccip.Value__c;
							toDelete.add(ccip);//delete payment

						}
						else if(ccip.Payment_Type__c.toLowerCase() == 'offshore'){
							toDelete.add(ccip);//delete payment
						}else{

							clientDeposit.Deposit_Total_Available__c += ccip.Value__c;
							ccip.deposit__c = clientDeposit.id;
							ccip.CurrencyIsoCode__c = installment.client_course__r.CurrencyIsoCode__c;
							ccip.client_course_instalment__c = null;
							clientDeposit.Deposit_Total_Value__c += ccip.Value__c;

							//Currency
							ccip.Agency_Currency__c = ccip.CurrencyIsoCode__c;
							ccip.Agency_Currency_Rate__c = 1;
							ccip.Agency_Currency_Value__c = ccip.Value__c;

							if(ccip.Confirmed_Date__c == null)
								clientDeposit.Deposit_Reconciliated__c = false;
						}

					}
					else if(ccip.Payment_Type__c == 'School Credit' && ccip.Related_Credit__c != null){
						installment.client_course__r.Total_Paid_Credit__c -= ccip.Value__c;
						installment.Total_Paid_School_Credit__c -= ccip.Value__c;
						client_course__c courseCredit = [Select id, Credit_Available__c from client_course__c where id = :ccip.Related_Credit__c limit 1];
						courseCredit.Credit_Available__c += ccip.Value__c;
						update courseCredit;
						installCreditDelete.add(ccip);
					}
					else if(ccip.Payment_Type__c == 'Client Loan'){
						Payment_Plan__c pp = [Select id, value_remaining__c from Payment_Plan__c where id = :ccip.Payment_Plan__c];
						pp.value_remaining__c += ccip.Value__c;
						update pp;
					}
					else{
						clientDeposit.Deposit_Total_Available__c += ccip.Value__c;
						installment.client_course__r.Total_Paid_Deposit__c -= ccip.Value__c;
						if(ccip.Paid_From_Deposit__c == clientDeposit.id){
							clientDeposit.Deposit_Total_Used__c -= ccip.Value__c;
							toDelete.add(ccip);//delete deposit
						}else{
							ccip.Transfered_from_Deposit__c = ccip.Paid_From_Deposit__c;
							ccip.deposit__c = clientDeposit.id;
							ccip.CurrencyIsoCode__c = installment.client_course__r.CurrencyIsoCode__c;
							ccip.client_course_instalment__c = null;
							clientDeposit.Deposit_Total_Value__c += ccip.Value__c;
							ccip.Payment_Type__c = 'Transferred from Deposit';
							ccip.Confirmed_By__c = currentUser.id;
							ccip.Confirmed_Date__c = system.now();
							ccip.Confirmed_By_Agency__c = currentUser.Contact.AccountId;

							//Currency
							ccip.Agency_Currency__c = ccip.CurrencyIsoCode__c;
							ccip.Agency_Currency_Rate__c = 1;
							ccip.Agency_Currency_Value__c = ccip.Value__c;
						}
					}
				}//end for
				upsert clientDeposit;
			}else{ //isPDS OR isPCS
				installment.client_course__r.Total_Paid__c -= installment.Instalment_Value__c;
				if(installment.isPDS__c || installment.isPCS__c){
					installment.client_course__r.Total_Paid_PDS__c -= installment.Instalment_Value__c;
					installment.isPDS__c = false;
					installment.isPCS__c = false;
				}

				installment.isMigrated__c = false;

				for(client_course_instalment_payment__c ccip:installment.client_course_instalment_payments__r)
					toDelete.add(ccip);
			}

			installment.Received_By_Agency__c = null;
			installment.Received_by__c =  null;
			installment.Received_by_Department__c =  null;
			installment.Received_Date__c =  null;
			installment.Confirmed_By__c = null;
			installment.Confirmed_Date__c = null;
			installment.isPaidOffShore__c = false;
			installment.isSharedCommission__c = false;
			installment.isShareCommissionSplited__c = false;
			installment.Client_not_Contacted__c = false;
			installment.Agency_Currency__c = null;
			installment.Agency_Currency_Rate__c = null;
			installment.Agency_Currency_Value__c = null;


			//No Commission Claim fields
			installment.Commission_Confirmed_By__c = null;
			installment.Commission_Confirmed_By_Agency__c = null;
			installment.Commission_Confirmed_On__c = null;
			installment.Commission_Not_Claimable__c = false;
			//
			
			installment.Status__c = 'Payment Cancelled';

			if(installment.client_course__r.isCancelled__c)
				installment.isCancelled__c = TRUE;

			if(installment.instalment_activities__c != null)
				installment.instalment_activities__c += 'Payment Cancelled' + ';;-;;' + installment.Cancel_Payment_Reason__c +';;' + System.now().format('yyyy-MM-dd HH:mm:ss') + ';;-;;-;;' + UserInfo.getName() + '!#!';
			else installment.instalment_activities__c = 'Payment Cancelled' +';;-;;' +  installment.Cancel_Payment_Reason__c +';;' + System.now().format('yyyy-MM-dd HH:mm:ss') + ';;-;;-;;' + UserInfo.getName() + '!#!';

			update installment.client_course_instalment_payments__r;

			if(toDelete.size()>0) //Delete 'Client Scholarship' and 'Covered by Agency'
				delete toDelete;

			installment.Cancel_Payment_Reason__c = null; 

			update installment;
			update installment.client_course__r;

      list<Client_Document__c> receipt =  [Select id from Client_Document__c where client_course_instalment__c = :installment.id];
      if(receipt.size()>0)
        dns.deleteReceipt(receipt);

			if(installCreditDelete.size() > 0)
				delete installCreditDelete;

			//depositSaved = true;
			showError = false;

		}catch(Exception e){
			String msg = 'Error===>' + e + ' Line:' + e.getLineNumber();
		   	system.debug('Error==>' + msg);
		   	ApexPages.addMessages(e);
			errorMsg += msg;
			Database.rollback(sp);
		}
		return null;
	}

	private noSharing dns {get{if(dns==null) dns = new noSharing(); return dns;}set;}
  public without sharing class noSharing {

		public void deleteReceipt(List<Client_Document__c> receipt){
			delete receipt;
		}
	}

}