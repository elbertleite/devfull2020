global virtual class AWSHelper { 
    


    /**
    *
    *  A W S  -  G E N E R A T E  -  F I L E  -  U R L  -  M E T H O D S 
    *
    */

    private static String encodeFilename(String str){
        String fileName = EncodingUtil.urlEncode(str, 'UTF-8');        
        fileName = fileName.replace('%28', '(').replace('%29', ')');
        return fileName;
    }

    global static String generateAWSLink(String bucketName, String fileName, String awsKey, String awsSecret){
        
        String encodedFilename = encodeFilename(fileName);
        
        Datetime now = DateTime.now().addYears(15);
        //Not working due to being 1 second off of AWS or something along those lines.
        
        Long lexpire = now.getTime()/1000;
        
        String stringToSign = 'GET\n\n\n' + lexpire + '\n/' + bucketName + '/' + encodedFilename;
        System.debug(stringToSign);
        String signed = generateAWSSignature(stringToSign, awsSecret);      
        System.debug(signed);
        String codedsigned = EncodingUtil.urlEncode(signed, 'UTF-8');
        System.debug(codedsigned);
        String url = 'https://s3.amazonaws.com/' + bucketName + '/' + encodedFilename + '?AWSAccessKeyId=' + awsKey + '&Expires=' + lexpire + '&Signature=' + signed;
        System.debug(url);
        return url;
    }

    global static String generateAWSLinkFileDownload(String bucketName, String fileName, String awsKey, String awsSecret, string shortfilename){
        
        String encodedFilename = encodeFilename(fileName);
        
        Datetime now = DateTime.now().addYears(15);
        //Not working due to being 1 second off of AWS or something along those lines.
        
        Long lexpire = now.getTime()/1000;
        
        String stringToSign = 'GET\n\n\n' + lexpire + '\n/' + bucketName + '/' + encodedFilename + '?response-content-disposition=attachment; filename='+shortfilename;
        System.debug(stringToSign);
        String signed = generateAWSSignature(stringToSign, awsSecret);      
        System.debug(signed);
        String codedsigned = EncodingUtil.urlEncode(signed, 'UTF-8');
        System.debug(codedsigned);
        String url = 'https://s3.amazonaws.com/' + bucketName + '/' + encodedFilename +'?response-content-disposition='+EncodingUtil.urlEncode('attachment; filename='+shortfilename,'UTF-8') + '&AWSAccessKeyId=' + awsKey + '&Expires=' + lexpire + '&Signature=' + signed;
        System.debug(url);
        return url;
    }
        
    global static Map<String, String> getObjectMetadata(String awsKey, String awsSecret, string bucket, string path, string filename){
        
        HttpResponse res = sendAWSRequest('HEAD', awsKey, awsSecret, bucket, path, filename);

        Map<String, String> responseHeaders = new Map<String, String>();
        for(String str : res.getHeaderKeys())
            if(str != null)
                responseHeaders.put(str, res.getHeader(str));

        return responseHeaders;
    }

    
    private static String generateAWSSignature(String toSign, String awsSecret){
        String macUrl;
        String signingKey = EncodingUtil.base64Encode(Blob.valueOf(awsSecret));
        Blob mac = Crypto.generateMac('HMacSHA1', blob.valueof(toSign), blob.valueof(awsSecret)); 
        macUrl = EncodingUtil.base64Encode(mac);
                              
        return EncodingUtil.urlEncode(macUrl, 'UTF-8');
    }


    /**
    *
    *  A W S  -  D E L E T E  -  M E T H O D S 
    *
    */

    global static boolean deleteObjectFromS3(String awsKey, String awsSecret, String bucket, String path, String filename){
        
        HttpResponse response = sendAWSRequest('DELETE', awsKey, awsSecret, bucket, path, filename);

        if(response.getStatusCode() == 200 || response.getStatusCode() == 204)
            return true;
        else return false;

    }

    private static HttpResponse sendAWSRequest(String method, String awsKey, String awsSecret, String bucket, String path, String filename){

        //String fullpath = bucket + '/' + encodeFilename(path) + '/' + encodeFilename(fileName);
        String fullpath = uriEncode(bucket + '/' + path + '/' + filename).replaceAll('%2F', '/');


        system.debug('@@@ fullpath: ' + fullpath); 


        string timestamp = Datetime.now().format('E, dd MMM yyyy HH:mm:ss z', 'Australia/Sydney');
        string stringToSign= method+'\n\n'+ '' +'\n'+ timestamp+'\n/'+ fullpath;
        Blob bsig = Crypto.generateMac('hmacSHA1',Blob.valueOf(stringToSign),Blob.valueof(awsSecret));
        string Signature= EncodingUtil.base64Encode(bsig);

        System.debug('@#$ stringToSign: ' + stringToSign);

        HttpRequest req = new HttpRequest();
        req.setMethod(method);
        req.setHeader('Host','s3.amazonaws.com');
        req.setEndpoint('https://s3.amazonaws.com/'+ fullpath);
        Http http = new Http();
        req.setHeader('Authorization','AWS '+awsKey+':'+Signature);
        req.setHeader('Date',timestamp);        
        
        HttpResponse res;
        if(Test.isRunningTest()){
            res = new HttpResponse();
            res.setStatus('ok');
            res.setStatusCode(200);
        } else
            res = http.send(req);
        
        return res;
    }



    /**
    *
    *  A W S  -  S I G N I N G  -  M E T H O D S 
    *
    */

    global static String getAuthorizationHeader(Datetime requestTime, URL endpoint){

        String region = 'us-east-1';
        String service = 's3';
        AWSKeys credentials = new AWSKeys(IPFunctions.awsCredentialName);
        
        String resource = '/';
        
        Map<String, String> queryParams = new Map<String, String>();
        Map<String, String> headerParams = new Map<String, String>();

        // headerParams.put('x-amz-date', requestTime.formatGmt('E, dd MMM YYYY HH:mm:ss z'));
        headerParams.put('x-amz-date', requestTime.formatGmt('yyyyMMdd\'T\'HHmmss\'Z\''));
        headerParams.put('host',  endpoint.getHost());
        headerParams.put('x-amz-content-sha256', 'UNSIGNED-PAYLOAD');

        String[] headerKeys = new String[0];
        String stringToSign = createStringToSign(headerKeys, headerParams, queryParams, requestTime, region, service, endpoint, resource);

        String authorizationHeader = String.format(
            'AWS4-HMAC-SHA256 Credential={0},SignedHeaders={1},Signature={2}',
            
            new String[] {
                
                getAuthorizationCredential(credentials.key, requestTime, region, service), 
                
                getAuthorizationSignedHeaders(headerKeys), 
                
                getAuthorizationSignature(stringToSign, credentials.secret, requestTime, region, service)

            }
        );

        return authorizationHeader;
    }

    global static String getAuthorizationHeader(Datetime requestTime, URL endpoint, String description){

        String region = 'us-east-1';
        String service = 's3';
        AWSKeys credentials = new AWSKeys(IPFunctions.awsCredentialName);
        
        String resource = '/';
        
        Map<String, String> queryParams = new Map<String, String>();
        Map<String, String> headerParams = new Map<String, String>();

        // headerParams.put('x-amz-date', requestTime.formatGmt('E, dd MMM YYYY HH:mm:ss z'));
        headerParams.put('x-amz-date', requestTime.formatGmt('yyyyMMdd\'T\'HHmmss\'Z\''));
        headerParams.put('host',  endpoint.getHost());
        headerParams.put('x-amz-content-sha256', 'UNSIGNED-PAYLOAD');
        if(!String.isEmpty(description)){
            headerParams.put('x-amz-tagging', 'description='+description);
        }

        String[] headerKeys = new String[0];
        String stringToSign = createStringToSign(headerKeys, headerParams, queryParams, requestTime, region, service, endpoint, resource);

        String authorizationHeader = String.format(
            'AWS4-HMAC-SHA256 Credential={0},SignedHeaders={1},Signature={2}',
            
            new String[] {
                
                getAuthorizationCredential(credentials.key, requestTime, region, service), 
                
                getAuthorizationSignedHeaders(headerKeys), 
                
                getAuthorizationSignature(stringToSign, credentials.secret, requestTime, region, service)

            }
        );

        return authorizationHeader;
    }

    private static String getAuthorizationCredential(String awskey, Datetime requestTime, String region, String service){
        
        return String.join(new String[] { awskey, requestTime.formatGMT('yyyyMMdd'), region, service, 'aws4_request' },'/');

    }


    private static String getAuthorizationSignature(String stringToSign, String awssecret, Datetime requestTime, String region, String service){
        
        return EncodingUtil.convertToHex(Crypto.generateMac('hmacSHA256', Blob.valueOf(stringToSign), createSigningKey(awssecret, service, region, requestTime)));

    }

    private static String getAuthorizationSignedHeaders(String[] headerKeys){
        return String.join(headerKeys,';');
    }

    

    private static String createStringToSign(String[] signedHeaders, Map<String, String> headerParams, Map<String, String> queryParams, Datetime requestTime, String region, String service, URL endpoint, String resource) {
        String result = createCanonicalRequest(signedHeaders, endpoint, resource, 'UNSIGNED-PAYLOAD', queryParams, headerParams);
        String str = String.join(
            new String[] {
                'AWS4-HMAC-SHA256',
                headerParams.get('x-amz-date'),
                String.join(new String[] { requestTime.formatGMT('yyyyMMdd'), region, service, 'aws4_request' },'/'),
                EncodingUtil.convertToHex(Crypto.generateDigest('sha256', Blob.valueof(result)))
            },
            '\n'
        );

        System.debug('createStringToSign: ' + str);
        return str;
    }

    private static Blob createSigningKey(String secretKey, String service, String region, Datetime requestTime) {
        return Crypto.generateMac('hmacSHA256', Blob.valueOf('aws4_request'),
            Crypto.generateMac('hmacSHA256', Blob.valueOf(service),
                Crypto.generateMac('hmacSHA256', Blob.valueOf(region),
                    Crypto.generateMac('hmacSHA256', Blob.valueOf(requestTime.formatGMT('yyyyMMdd')), Blob.valueOf('AWS4'+secretKey))
                )
            )
        );
    }

    private static String createCanonicalRequest(String[] headerKeys, URL endpoint, String resource, String payloadSha256, Map<String, String> queryParams, Map<String, String> headerParams) {     
        String str = String.join(
            new String[] {
                'PUT',                                  //  METHOD
                endpoint.getPath(),                     //  RESOURCE
                createCanonicalQueryString(queryParams),            //  CANONICAL QUERY STRING
                createCanonicalHeaders(headerKeys, headerParams),       //  CANONICAL HEADERS
                String.join(headerKeys, ';'),           //  SIGNED HEADERS
                payloadSha256                           //  SHA256 PAYLOAD
            },
            '\n'
        );

        System.debug('createCanonicalRequest: ' + str);
        return str;
    }
    

    private static String createCanonicalQueryString(Map<String, String> queryParams) {
        String[] results = new String[0], keys = new List<String>(queryParams.keySet());
        keys.sort();
        for(String key: keys) {
            results.add(key+'='+queryParams.get(key));
        }
        String str = String.join(results, '&');
        System.debug('createCanonicalQueryString: ' + str);
        return str;
    }
    private static String createCanonicalHeaders(String[] keys, Map<String, String> headerParams) {
        keys.addAll(headerParams.keySet());
        keys.sort();
        String[] results = new String[0];
        for(String key: keys) {
            results.add(key+':'+headerParams.get(key));
        }
        String str = String.join(results, '\n')+'\n';
        System.debug('createCanonicalHeaders: ' + str); 
        return str;
    }






    /**
    *
    *  I N N E R  -  C L A S S E S 
    *
    */
        
    global class File {
        public String name;
        public double size;
        public String url;
        public String urlFileDownload;
        public String uploadedBy;
        public String uploadedDate;
        public String fullname;
        public File(String name, String fullname, double size, String url, string urlFileDownload, String uploadedBy, String uploadedDate){
            this.name = name;
            this.fullname = fullname; 
            this.size = size;
            this.url = url; 
            this.urlFileDownload = urlFileDownload;
            this.uploadedBy = uploadedBy;
            this.uploadedDate = uploadedDate;
        }
    }


    global class Authorization { 
        public String endpoint;
        public String path;
        public String authorization;
        public String timestamp;
        public String host;
    }


    /**
    *
    *  U T I L I T Y  -  M E T H O D S 
    *
    */


    global static string uriEncode(String value) {
        return value==null? null: EncodingUtil.urlEncode(value, 'utf-8').replaceAll('%7E','~').replaceAll('\\+','%20');
    }

    
    
    global static boolean hasValue(String var){
        return var != null && !var.trim().equals('');
    }

}