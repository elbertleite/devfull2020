public class Website_Sharing_Configuration {
	
	public String selectedRole {get;set;}
	public List<SelectOption> roles {get;set;}	
	public Map<String, String> groupByRole;
	public boolean isRoleSelected { get { if(isRoleSelected == null) isRoleSelected = false; return isRoleSelected; } set; }
	
	public List<Account> groups {get;set;}
	public List<Account> destinations {get;set;}
	public List<Account> schools {get;set;}
	
	public Website_Sharing_Configuration(){
		getRoles();
	}
	
	private void getRoles(){
		
		roles = new List<SelectOption>();
		Set<ID> roleids = new Set<ID>();
		groupByRole = new Map<String, String>();
		
		roles.add(new SelectOption('', '-- Select a role --'));
		for(UserRole role : [Select Id, Name from UserRole WHERE id in (Select UserRoleId from User where API_User__c = true)]){
			roleids.add(role.id);
			roles.add(new SelectOption(role.id, role.Name));
		}
		
		for(Group g : [Select Id, RelatedId from Group where RelatedId in :roleids AND Type = 'Role'])	
			groupByRole.put(g.relatedId, g.id);
				
	}
	
	public void refreshSharing(){
		isRoleSelected = false;
		groups = new List<Account>();
		destinations = new List<Account>();
		schools = new List<Account>();
		
		User theUser = [select Contact.AccountID, Contact.Account.Parent.Global_Link__c FROM USER WHERE UserRoleId = :selectedRole LIMIT 1];
		
		String globalLink = theUser.Contact.Account.Parent.Global_Link__c;
		
		Set<String> sharedAccounts = new Set<String>();
		for(AccountShare ashare : [select AccountId from AccountShare where UserOrGroupId = :groupByRole.get(selectedRole) and RowCause = 'Manual'])
			sharedAccounts.add(ashare.accountid);
		
		
		
		for(Account acc : [select id, Name, isSelected__c, recordtype.name, (Select id, Name, isSelected__c, View_Web_Leads__c from ChildAccounts where id != :theUser.Contact.AccountID order by Name) from Account 
							where ( recordtype.name = 'Agency Group' AND Global_Link__c = :globalLink) OR ( recordtype.name in ('Country', 'School') )								  
							order by Name]){
								
			acc.isSelected__c = false; //make sure we dont have any accounts selected = true in the database					
								
			if(acc.recordtype.name == 'Agency Group'){
				
				for(Account child : acc.childAccounts)
					if(sharedAccounts.contains(child.id))
						child.isSelected__c = true;
				
				groups.add(acc);
						
			} else {
				
				if(sharedAccounts.contains(acc.id))
					acc.isSelected__c = true;
				for(Account child : acc.childAccounts)
					if(sharedAccounts.contains(child.id))
						child.isSelected__c = true;
						
						
				if(acc.recordtype.name == 'Country' )
					destinations.add(acc);
				else if(acc.recordtype.name == 'School')
					schools.add(acc);
			}
		}
		
		isRoleSelected = true;
		
	}
	
	public void save(){
		
		List<AccountShare> shareList = new List<AccountShare>();
		Set<ID> removedAccounts = new Set<ID>();
		
		for(Account acc : groups)
			for(Account agency : acc.childAccounts)
				if(agency.isSelected__c)
					shareList.add(createShareRecord(agency));
				else
					removedAccounts.add(agency.id);
		
		for(Account country : destinations){
			if(country.isSelected__c)
				shareList.add(createShareRecord(country));
			else
				removedAccounts.add(country.id);
				
			for(Account city : country.childAccounts)
				if(city.isSelected__c)
					shareList.add(createShareRecord(city));
				else
					removedAccounts.add(city.id);
			
		}
		
		for(Account school : schools){
			if(school.isSelected__c)
				shareList.add(createShareRecord(school));
			else
				removedAccounts.add(school.id);
				
			for(Account campus : school.childAccounts)
				if(campus.isSelected__c)
					shareList.add(createShareRecord(campus));
				else
					removedAccounts.add(campus.id);
		}
		
		//Database.SaveResult[] inserted = new Database.SaveResult[]{};
		//Database.DeleteResult[] deleted = new Database.DeleteResult[]{};
		
		if(!shareList.isEmpty())
			Database.insert(shareList, true);
		
		if(!removedAccounts.isEmpty())
			Database.delete( [select id from AccountShare where AccountID in :removedAccounts and UserOrGroupId = :groupByRole.get(selectedRole) and RowCause = 'Manual'], true);
			
			
	}
	
	private AccountShare createShareRecord(Account acc){
		
		AccountShare share = new AccountShare();
		share.AccountID = acc.id;
		share.UserOrGroupId = groupByRole.get(selectedRole);
		share.AccountAccessLevel = 'Edit';
		share.OpportunityAccessLevel = 'None';
		return share;
		
	}
	
	
    
}