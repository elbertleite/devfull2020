/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PaymentSchoolConsolidation_test {

    static testMethod void myUnitTest() {

      //  TestFactory tf = new TestFactory();
      //
      //  Account school = tf.createSchool();
      //  Account agency = tf.createAgency();
      //  Contact emp = tf.createEmployee(agency);
      //  User portalUser = tf.createPortalUser(emp);
      //  Account campus = tf.createCampus(school, agency);
      //  Course__c course = tf.createCourse();
      //  Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
      //
      //  Test.startTest();
      //  system.runAs(portalUser){
      //  	   Contact client = tf.createLead(agency, emp);
	    //    client_course__c booking = tf.createBooking(client);
	    //    client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	    //    List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
	    //
	    //
	    //    //TO REQUEST PDS
	    //    instalments[0].Received_By_Agency__c = agency.id;
	    //    instalments[0].Received_Date__c =  system.today();
	    //    instalments[0].Paid_To_School_On__c =  system.today();
	    //    instalments[0].isPDS__c = true;
	    //
	    //    instalments[1].isPDS__c = true;
	    //    instalments[1].Received_By_Agency__c = agency.id;
	    //    instalments[1].Paid_To_School_On__c =  system.today();
	    //    instalments[1].Received_Date__c =  system.today();
	    //
	    //    instalments[2].isPFS__c = true;
	    //    instalments[2].Received_By_Agency__c = agency.id;
	    //    instalments[2].Paid_To_School_On__c =  system.today();
	    //    instalments[2].Received_Date__c =  system.today();
	    //    update instalments;
	    //
	    //    PaymentSchoolConsolidation testClass = new PaymentSchoolConsolidation();
	    //
	    //    ApexPages.currentPage().getParameters().put('tab', '#main-1');
	    //    testClass.redirectTab();
	    //
	    //    ApexPages.currentPage().getParameters().remove('tab');
	    //    ApexPages.currentPage().getParameters().put('tab', '#main-2');
	    //    testClass.redirectTab();
	    //
	    //    ApexPages.currentPage().getParameters().remove('tab');
	    //    ApexPages.currentPage().getParameters().put('tab', '#main-3');
	    //    testClass.redirectTab();
	    //
	    //    Account agencyDetails = testClass.agencyDetails;
	    //    Account campusDetails = testClass.campusDetails;
	    //    List<SelectOption> typePDSDateCriteria = testClass.typePDSDateCriteria;
	    //    List<SelectOption> typeDateCriteria = testClass.typeDateCriteria;
	    //    List<SelectOption> typePaymentStatus = testClass.typePaymentStatus;
	    //    List<SelectOption> typeOptions = testClass.typeOptions;
	    //    boolean showError = testClass.showError;
	    //
	    //
	    //    List<SelectOption> agencyGroupOptions = testClass.agencyGroupOptions;
	    //    testClass.changeAgencyGroup();
	    //
	    //    List<SelectOption> agencyOptions = testClass.agencyOptions;
	    //    testClass.changeAgencyGroup();
	    //
	    //
	    //    List<SelectOption> schoolOptions = testClass.schoolOptions;
	    //    testClass.changeSchool();
	    //
	    //    List<SelectOption> campusOptions = testClass.campusOptions;
	    //    testClass.campusOptions = null;
	    //    testClass.selectedSchool = school.id;
	    //    campusOptions = testClass.campusOptions;
	    //
	    //
	    //    testClass.selectedCampus =  campus.id;
	    //    testClass.fromDate.BirthDate = system.today().addDays(10);
	    //    testClass.toDate.BirthDate = system.today().addDays(50);
	    //    testClass.findRequestPDS();
	    //
	    //
	    //
	    //    //TO INVOICE PDS
	    //    instalments[0].PDS_Requested_On__c =  system.today();
	    //    instalments[0].isSelected__c = true;
	    //
	    //    instalments[1].PDS_Requested_On__c =  system.today();
	    //    instalments[1].isSelected__c = true;
	    //    update instalments;
	    //
	    //    testClass.findInvoicePDS();
	    //
	    //    ApexPages.currentPage().getParameters().put('id', instalments[0].id);
	    //    testClass.saveChanges();
	    //
	    //
	    //    ApexPages.currentPage().getParameters().put('campusId', campus.id);
	    //    testClass.generateInvoice();
	    //
	    //
	    //    Invoice__c paidInvoice = new Invoice__c(Id = testClass.generatedInvoice, Received_Date__c = Datetime.now(), Commission_Paid_Date__c = system.today());
	    //    update paidInvoice;
	    //
	    //    //TO CONFIRM COMMISSION PDS/PFS
	    //
	    //    instalments[0].PDS_Invoice_Sent__c = true;
	    //
	    //    instalments[1].PDS_Invoice_Sent__c = true;
	    //
	    //    instalments[2].Paid_To_School_On__c =  Datetime.now();
	    //    instalments[2].Commission_Paid_Date__c = system.today();
	    //
	    //    update instalments;
	    //
	    //    testClass.findPdsPfs();
	    //
	    //    ApexPages.currentPage().getParameters().remove('id');
	    //    ApexPages.currentPage().getParameters().put('type', 'instalment');
	    //    ApexPages.currentPage().getParameters().put('school', school.id);
	    //    ApexPages.currentPage().getParameters().put('campus', campus.id);
	    //    ApexPages.currentPage().getParameters().put('id', instalments[2].id);
	    //    testClass.confirmCommission();
	    //    testClass.unconfirmPFS();
	    //
	    //    ApexPages.currentPage().getParameters().remove('id');
	    //    ApexPages.currentPage().getParameters().remove('type');
	    //    ApexPages.currentPage().getParameters().put('id', testClass.generatedInvoice);
	    //    ApexPages.currentPage().getParameters().put('type', 'Invoice');
	    //    testClass.confirmCommission();
	    //    testClass.unconfirmPDS();
	    //
	    //
	    //    ApexPages.currentPage().getParameters().remove('id');
	    //    ApexPages.currentPage().getParameters().put('id', instalments[0].id);
	    //    testClass.notClaimCommission(); // Not Claim Commission
	    //    ApexPages.currentPage().getParameters().put('inst', instalments[0].id);
	    //    ApexPages.currentPage().getParameters().put('emailBody', 'test');
	    //    testClass.loadClients();
	    //
	    //
	    //    ApexPages.currentPage().getParameters().remove('id');
	    //    ApexPages.currentPage().getParameters().put('id', testClass.generatedInvoice);
	    //    testClass.cancelInvoice();
	    //    ApexPages.currentPage().getParameters().put('cancelId', instalments[0].id);
	    //    testClass.setIdCancel();
	    //    testClass.cancelPayment();
	    //
	    //    //TODO: LOAD CLIENTS
	    //
      //  }
      //  Test.stopTest();
    }
}