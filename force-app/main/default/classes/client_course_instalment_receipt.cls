public with sharing class client_course_instalment_receipt {

	public Account agencyDetails {get;set;}
	public list<feeInstallmentDetail> lFees {get;set;}
	public list<feeInstallmentDetail> originalLFees {get{if (originalLFees == null) originalLFees= new list<feeInstallmentDetail>(); return originalLFees;}set;}
	public client_course_instalment__c instInvoice {get;set;}
	public list<client_course_instalment__c> lInstInvoice {get;set;}
	public client_course_instalment__c originalInst {get{if (originalInst == null) originalInst= new client_course_instalment__c(); return originalInst;}set;}
	public Boolean isAmendment {get{if (isAmendment == null) isAmendment=false; return isAmendment;}set;}
	public list<client_product_service__c> prodInvoice {get;set;}
	public list<client_course_instalment_payment__c> payList {get;set;}
	public Invoice__c groupInvoice {get;set;}
	
	public String invoiceNumber {get;set;}
	public String instNumber {get;set;}
	public String clientName {get;set;}
	public Date invoiceDueDate {get;set;}
	public decimal invoiceTotal {get;set;}
	public decimal totalPaid {get;set;}
	public String invoiceCurrency {get;set;}
	private String accountId {get;set;}
	public decimal discount {get;set;}

	public string language{get; set;}
	public boolean isSplitPaid {get{if(isSplitPaid == null) isSplitPaid = false; return isSplitPaid;}set;}

	public client_course_instalment_receipt(ApexPages.StandardController ctr){
		generateReceipt();

		for(client_course_instalment_payment__c ccip : payList)
			ccip.Type_of_Payment__c = ccip.Payment_Type__c;
	}
	
	
	public String agCurrency {get;set;}
	public Decimal agCurrencyRate {get;set;}

	public void generateReceipt(){
		
		/****** INSTALMENT 	******/
		if(ApexPages.CurrentPage().getParameters().get('inst')!=null){// 
            lInstInvoice = [SELECT id, Name, Instalment_Value__c, Due_Date__c, Related_Fees__c, Related_Promotions__c, isPDS__c, Received_Date__c, Discount__c, Total_School_Payment__c, Tuition_Value__c, 	Received_By_Agency__c, isInstalment_Amendment__c, Original_Instalment__c, Amendment_Total_Charge_Client__c, Amendment_Related_Fees__c, Number__c, Split_Number__c, isOverpayment_Amendment__c, isCommission_Amendment__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, client_scholarship__c, Original_Instalment__r.isAmended_BeforeSchoolPayment__c, Paid_split__c,
							Amendment_Tuition_Balance__c, 
							Amendment_Fee_Balance__c,
							Amendment_Instalment_Balance__c, 
							
							client_course__r.CurrencyIsoCode__c,
							client_course__r.client__c, 
							client_course__r.client__r.Name,
							client_course__r.client__r.Birthdate,
							client_course__r.Course_Name__c, 
							client_course__r.Campus_Name__c,
							client_course__r.Client__r.Preferable_Language__c,
							(SELECT Date_Paid__c, Payment_Type__c, Value__c, Agency_Currency_Value__c, Createdby.Name, Received_By__r.Name, isAmendment__c FROM client_course_instalment_payments__r)
							
							FROM client_course_instalment__c where Id = :ApexPages.CurrentPage().getParameters().get('inst')];
							
			clientName = lInstInvoice[0].client_course__r.client__r.Name;
			invoiceCurrency  = lInstInvoice[0].client_course__r.CurrencyIsoCode__c;				
			agCurrency  = lInstInvoice[0].Agency_Currency__c;				
			agCurrencyRate  = lInstInvoice[0].Agency_Currency_Rate__c;		

			language = lInstInvoice[0].client_course__r.Client__r.Preferable_Language__c != null ? lInstInvoice[0].client_course__r.Client__r.Preferable_Language__c : 'en_US';		
							
			instNumber = string.valueOf(lInstInvoice[0].Number__c);
			
			if(lInstInvoice[0].Split_Number__c!=null)
				instNumber += '.' +string.valueOf(lInstInvoice[0].Split_Number__c);
			
				
			/** N O T 	A M E N D E D **/				
			if(!lInstInvoice[0].isInstalment_Amendment__c && !lInstInvoice[0].isOverpayment_Amendment__c && !lInstInvoice[0].isCommission_Amendment__c){
			
				/** Fees **/
				lFees = new list<feeInstallmentDetail>();
				for(client_course_instalment__c cci : lInstInvoice){
					if(cci.Related_Fees__c!=null && cci.Related_Fees__c!=''){
						for(string lf:cci.Related_Fees__c.split('!#'))
								lFees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2]), false));
					}
					
					if(cci.Related_Promotions__c != null && cci.Related_Promotions__c != ''){
						for(string lf:cci.Related_Promotions__c.split('!#'))
							lFees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2]), true));
					}
				}
				/** END -- Fees **/
				
				
				invoiceTotal = lInstInvoice[0].Instalment_Value__c  + lInstInvoice[0].Discount__c;
				accountId = lInstInvoice[0].Received_By_Agency__c;
				invoiceNumber = lInstInvoice[0].Name;
				invoiceDueDate = lInstInvoice[0].Due_Date__c;
				discount = lInstInvoice[0].Discount__c;
				
				payList = lInstInvoice[0].client_course_instalment_payments__r;
				
				//totalPaid = lInstInvoice[0].Instalment_Value__c  + lInstInvoice[0].Discount__c;
				
				totalPaid = lInstInvoice[0].Discount__c;	
			
				for(client_course_instalment_payment__c ccip : lInstInvoice[0].client_course_instalment_payments__r)
					totalPaid += ccip.Value__c;


				if(lInstInvoice[0].Paid_split__c){
					isSplitPaid = true;
					totalPaid = invoiceTotal;
				}
					
					
			}
			
			/** A M E N D E D  **/	
			else{ 
				
				isAmendment = true;
				originalInst = [SELECT Id, Tuition_Value__c, Instalment_Value__c, Related_Fees__c, Related_Promotions__c, Discount__c, Name, Received_By_Agency__c, Due_Date__c,
									isAmended_BeforeSchoolPayment__c, Number__c, split_number__c,
									
									client_course__r.Course_Name__c, 
									client_course__r.Campus_Name__c,
									(SELECT Date_Paid__c, Payment_Type__c, Value__c, Agency_Currency_Value__c, Createdby.Name, Received_By__r.Name, isAmendment__c FROM client_course_instalment_payments__r)
									
									FROM client_course_instalment__c WHERE id = : lInstInvoice[0].Original_Instalment__c];
				
				if(originalInst.isAmended_BeforeSchoolPayment__c)
					invoiceTotal = lInstInvoice[0].Instalment_Value__c;
				else
					invoiceTotal = lInstInvoice[0].Instalment_Value__c + originalInst.Instalment_Value__c;
				
				
				
				/** Original Fees **/
				originalLFees = new list<feeInstallmentDetail>();
				if(originalInst.Related_Fees__c!=null && originalInst.Related_Fees__c!=''){
					for(string lf:originalInst.Related_Fees__c.split('!#'))
							originalLFees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2]), false));
				}
				
				if(originalInst.Related_Promotions__c != null && originalInst.Related_Promotions__c != ''){
					for(string lf:originalInst.Related_Promotions__c.split('!#'))
						originalLFees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2]), true));
				}
				/** END -- Original Fees **/
				
				/** Amendment Fees **/
				lFees = new list<feeInstallmentDetail>();
				for(client_course_instalment__c cci : lInstInvoice){
					if(cci.Amendment_Related_Fees__c!=null && cci.Amendment_Related_Fees__c!='')
						for(string lf:cci.Amendment_Related_Fees__c.split('!#')){
							lFees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2]), false));
						}//end for
						
					if(cci.Related_Fees__c!=null && cci.Related_Fees__c!='')
						for(string lf:cci.Related_Fees__c.split('!#')){
							
							if(lf.split(';;').size() >= 7 && decimal.valueOf(lf.split(';;')[6]) != decimal.valueOf(lf.split(';;')[2]))//The value is amended
								lFees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[6]) - decimal.valueOf(lf.split(';;')[2]), false));
						}//end for	
					
				}//end for
				
				/**  N O T 		R E C E I V E D **/
				if(lInstInvoice[0].Received_Date__c == null){
				
					accountId = originalInst.Received_By_Agency__c;
					invoiceNumber = originalInst.Name;
					invoiceDueDate = originalInst.Due_Date__c;
					discount = originalInst.Discount__c;
					
					
					payList = originalInst.client_course_instalment_payments__r;
					
					//totalPaid = originalInst.Instalment_Value__c  + originalInst.Discount__c;
					
					totalPaid = originalInst.Discount__c;	
			
					for(client_course_instalment_payment__c ccip : originalInst.client_course_instalment_payments__r)
						totalPaid += ccip.Value__c;
				}
				
				
				/**  R E C E I V E D  **/
				else{
					
					payList = originalInst.client_course_instalment_payments__r;
					payList.addAll(lInstInvoice[0].client_course_instalment_payments__r);
					
					accountId = lInstInvoice[0].Received_By_Agency__c;
					invoiceNumber = lInstInvoice[0].Name;
					invoiceDueDate = lInstInvoice[0].Due_Date__c;
					discount = originalInst.Discount__c;
					
					totalPaid = discount;	
					
					for(client_course_instalment_payment__c ccip : payList)
						totalPaid += ccip.Value__c;
					
					
					/**  O R I G I N A L 	N O T 	 P A I D 	T O 	S C H O O L  
					if(lInstInvoice[0].Original_Instalment__r.isAmended_BeforeSchoolPayment__c){
						totalPaid = lInstInvoice[0].Amendment_Total_Charge_Client__c + originalInst.Instalment_Value__c  + originalInst.Discount__c;
						
					}
					
					else{
						totalPaid = lInstInvoice[0].Instalment_Value__c + originalInst.Instalment_Value__c  + originalInst.Discount__c;
					}**/
					
				}
				
			}
        }
        
        
        
    	/******  PRODUCT  ******/
        else if(ApexPages.CurrentPage().getParameters().get('prod')!=null){ //PRODUCT
			String prodId = ApexPages.CurrentPage().getParameters().get('prod');


        	prodInvoice = [SELECT Id, Products_Services__r.Provider__r.Provider_Name__c, Start_Date__c, End_Date__c, Name, Category__c, Currency__c, Description__c, Price_Total__c, Price_Unit__c, Product_Name__c, Quantity__c, Unit_Description__c, CreatedBy.Contact.AccountId, Received_Date__c, Client__r.Name, Client__r.Birthdate, Client__r.Preferable_Language__c, Received_By_Agency__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c,
        					(SELECT Date_Paid__c, Payment_Type__c, Value__c, Agency_Currency_Value__c, Createdby.Name, Received_By__r.Name, isAmendment__c FROM client_course_instalment_payments__r),
							(SELECT Product_Name__c, Currency__c, Received_by__r.Name, Received_Date__c, Unit_Description__c, Quantity__c, Start_Date__c, End_Date__c,Price_Total__c FROM paid_products__r)
        					FROM Client_product_service__c WHERE (Id = :prodId OR Related_to_Product__c = :prodId)  AND Paid_with_product__c = NULL order by Related_to_Product__c nulls first, Received_Date__c nulls last, Product_Name__c];
        					
			
			
			accountId = prodInvoice[0].Received_By_Agency__c;
			
			clientName = prodInvoice[0].client__r.Name;
			invoiceNumber = prodInvoice[0].Name;
			//invoiceTotal =  prodInvoice[0].Price_Total__c;
			invoiceCurrency  = prodInvoice[0].Currency__c;
			agCurrency  = prodInvoice[0].Agency_Currency__c;				
			agCurrencyRate  = prodInvoice[0].Agency_Currency_Rate__c;	

			language = prodInvoice[0].Client__r.Preferable_Language__c != null ? prodInvoice[0].Client__r.Preferable_Language__c : 'en_US';
			
			//payList = prodInvoice[0].client_course_instalment_payments__r;
			payList = new list<client_course_instalment_payment__c>();
			totalPaid = 0;
			invoiceTotal = 0;
			for(Client_product_service__c p : prodInvoice){

				invoiceTotal += p.Price_Total__c;

				for(Client_product_service__c ccip : p.paid_products__r)
					invoiceTotal += ccip.Price_Total__c;
				
				for(client_course_instalment_payment__c ccip : p.client_course_instalment_payments__r){
					payList.add(ccip);
					totalPaid += ccip.Value__c;
				}

			}//end for
        }
        
        
        
        
    	/******* GROUP INVOICE ******/
         else if(ApexPages.CurrentPage().getParameters().get('inv')!=null){ //GROUP INVOICE
        	groupInvoice = [Select Name, Id, Total_Value__c, Total_Discount__c, Received_Date__c, Client__c, Due_Date__c, Client__r.Name, Client__r.Birthdate, CreatedBy.Contact.AccountId, Received_By_Agency__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, CurrencyIsoCode__c, Client__r.Preferable_Language__c,

							(SELECT id, Name, Instalment_Value__c, Due_Date__c, Related_Fees__c, Related_Promotions__c, isPDS__c, Received_Date__c, Discount__c, Total_School_Payment__c, Tuition_Value__c,
								client_course__r.Course_Name__c,  client_course__r.Campus_Name__c, client_course__r.CurrencyIsoCode__c, number__c, split_number__c, client_scholarship__c,
									
									isInstalment_Amendment__c, 
									
									Original_Instalment__r.isAmended_BeforeSchoolPayment__c,
									Original_Instalment__r.Tuition_Value__c,
									Original_Instalment__r.number__c,
									Original_Instalment__r.split_number__c
									
								 		FROM client_course_instalments__r), 
							
							(SELECT Id, Name, Category__c, Currency__c, Description__c, Price_Total__c, Price_Unit__c, Product_Name__c, Quantity__c, Unit_Description__c, CreatedBy.Contact.AccountId, Received_Date__c 
								FROM client_products_services__r),
								
							(SELECT Date_Paid__c, Payment_Type__c, Value__c, Agency_Currency_Value__c, Createdby.Name, Received_By__r.Name, isAmendment__c	FROM client_course_instalment_payments__r)
							 
							FROM Invoice__c WHERE Id = :ApexPages.CurrentPage().getParameters().get('inv') limit 1];
        					
			accountId = groupInvoice.Received_By_Agency__c;
			
			clientName = groupInvoice.client__r.Name;
			invoiceNumber = groupInvoice.Name;
			invoiceDueDate = groupInvoice.Due_Date__c;
			invoiceTotal =  groupInvoice.Total_Value__c;
			agCurrency  = groupInvoice.Agency_Currency__c;				
			invoiceCurrency  = groupInvoice.CurrencyIsoCode__c;				
			agCurrencyRate  = groupInvoice.Agency_Currency_Rate__c;

			language = groupInvoice.Client__r.Preferable_Language__c != null ? groupInvoice.Client__r.Preferable_Language__c : 'en_US';
			
			lInstInvoice = groupInvoice.client_course_instalments__r;
			prodInvoice = groupInvoice.client_products_services__r;
			payList = groupInvoice.client_course_instalment_payments__r;
			
			totalPaid = 0;
			for(client_course_instalment_payment__c ccip : payList)
					totalPaid += ccip.Value__c;
			
			discount = 0;
			
			/** Fees **/
			lFees = new list<feeInstallmentDetail>();
			
			for(client_course_instalment__c cci : lInstInvoice){
				
				if(cci.Discount__c!=null){ //Discount (SUM)
					discount += cci.Discount__c;
				}
				if(cci.Related_Fees__c!=null && cci.Related_Fees__c!=''){
					for(string lf:cci.Related_Fees__c.split('!#'))
							lFees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2]), false));
				}
				
				if(cci.Related_Promotions__c != null && cci.Related_Promotions__c != ''){
					for(string lf:cci.Related_Promotions__c.split('!#'))
						lFees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2]), true));
				}
			}
			/** END -- Fees **/

			totalPaid += discount;
       		invoiceTotal +=	discount;
        }
		

		if(accountId == null)
			accountId = [Select Contact.AccountId from User Where Id = :UserInfo.getUserId() limit 1].Contact.AccountId;
							
		agencyDetails = [Select Id, Name, Logo__c, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode From Account where Id = :accountId limit 1];
	}

	
	/** INNER CLASS FEE DETAILS **/
	public class feeInstallmentDetail{
		
		public string feeName{get; set;}
		public decimal feeValue{get; set;}
		public boolean isPromotion{get; set;}
		
		public feeInstallmentDetail(string feeName, decimal feeValue, boolean isPromotion){
			this.feeName = feeName;
			this.feeValue = feeValue;
			this.isPromotion = isPromotion;
		}
	}
	/** END -- INNER CLASS FEE DETAILS **/
}