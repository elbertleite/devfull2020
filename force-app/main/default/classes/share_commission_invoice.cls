public without sharing class share_commission_invoice {

	public Invoice__c invoice {get;set;}
	public Account agencyCommission {get;set;}
	public Account agencyRequested {get;set;}

	public Decimal totalEnrolled {get{if(totalEnrolled == null) totalEnrolled = 0; return totalEnrolled;}set;}
	public Decimal totalReceived {get{if(totalReceived == null) totalReceived = 0; return totalReceived;}set;}
	public Decimal totalRequested {get{if(totalRequested == null) totalRequested = 0; return totalRequested;}set;}
	public Decimal totalOffshore {get{if(totalOffshore == null) totalOffshore = 0; return totalOffshore;}set;}

	public Decimal totalTaxEnrolled {get{if(totalTaxEnrolled == null) totalTaxEnrolled = 0; return totalTaxEnrolled;}set;}
	public Decimal totalTaxReceived {get{if(totalTaxReceived == null) totalTaxReceived = 0; return totalTaxReceived;}set;}
	public Decimal totalTaxRequested {get{if(totalTaxRequested == null) totalTaxRequested = 0; return totalTaxRequested;}set;}
	public Decimal totalTaxOffshore {get{if(totalTaxOffshore == null) totalTaxOffshore = 0; return totalTaxOffshore;}set;}

	public Decimal totalCommEnrolled {get{if(totalCommEnrolled == null) totalCommEnrolled = 0; return totalCommEnrolled;}set;}
	public Decimal totalCommReceived {get{if(totalCommReceived == null) totalCommReceived = 0; return totalCommReceived;}set;}
	public Decimal totalCommRequested {get{if(totalCommRequested == null) totalCommRequested = 0; return totalCommRequested;}set;}
	public Decimal totalCommOffshore {get{if(totalCommOffshore == null) totalCommOffshore = 0; return totalCommOffshore;}set;}


	// public list<client_course_instalment__c> Shared_Commission_Bank_Dep_Enrolled{get; set;}
	// public list<client_course_instalment__c> Shared_Commission_Bank_Dep_Pay_Agency{get; set;}
	// public list<client_course_instalment__c> Shared_Commission_Bank_Dep_Received{get; set;}
	// public list<client_course_instalment__c> Shared_Commission_Bank_Dep_Request{get; set;}

	public map<String, list<shareInstalment>> result {get;set;}
	public map<String,totals> totalAgencies {get;set;}

	private String invoiceId {get;set;}
	private set<String> allInvoices;
	//CONSTRUCTOR
	public share_commission_invoice() {

		invoiceId = ApexPages.currentPage().getParameters().get('id');
		system.debug('@invoiceId==>' + invoiceId);

		allInvoices = new set<String>();

		for(Invoice__c inv : [SELECT Id, Invoice__c FROM Invoice__c WHERE Id = :invoiceId OR Invoice__c = :invoiceId]){
			allInvoices.add(inv.id);
		}//end Invoice
			
	 	findInvoice();
	}

	//F I N D 		 I N V O I C E
	
	public void findInvoice(){

		invoice = [SELECT ID, isHifyAgency__c, Share_Commission_Number__c, Total_Value__c, Paid_On__c, Requested_Commission_for_Agency__c, Requested_Commission_for_Agency__r.Name, Requested_Commission_To_Agency__c, Requested_Commission_To_Agency__r.Name, Total_Instalments_Requested__c, createdBy.Name, Payment_Receipt__r.preview_link__c, Commission_Paid_Date__c, Invoice_Activities__c, Confirmed_Date__c, createdDate,	Bank_to_Deposit__r.Bank__c,	Bank_to_Deposit__r.Branch_Address__c,	Bank_to_Deposit__r.Account_Name__c,	Bank_to_Deposit__r.BSB__c,	Bank_to_Deposit__r.Account_Number__c,	Bank_to_Deposit__r.Swift_Code__c,	Bank_to_Deposit__r.IBAN__c, summary__c, Due_Date__c, CurrencyIsoCode__c FROM Invoice__c WHERE ID = :invoiceId];


		result = new map<String,list<shareInstalment>>();
		totalAgencies = new map<String, totals>();

		list<String> ids = new list<String>(allInvoices);

		String sql = 'SELECT isPaidOffShore__c, Number__c, Split_Number__c, Tuition_Value__c, Discount__c, isShare_Commission__c, Commission_Tax_Rate__c, Commission__c, Commission_Value__c, '; 
		sql += ' Commission_Tax_Value__c, isPFS__c, isPCS__c, isPDS__c, Shared_Comm_Agency_Enroll_Value__c, Shared_Comm_Agency_Enroll__c, Shared_Comm_Tax_Agency_Enroll_Value__c, client_course__r.Client__r.Name, '; 
		sql += ' client_course__r.School_Name__c, Client_Course__r.Course_Name__c, Client_Course__r.Client__r.Owner__r.Name, isInstalment_Amendment__c, client_course__r.CurrencyIsoCode__c, Received_By_Agency__c, '; 
		sql += ' Received_By_Agency__r.Name, Request_Commission_Invoice__r.Requested_by_Agency__c, Request_Commission_Invoice__r.Requested_by_Agency__r.Name, Shared_Comm_Agency_Receive__c, '; 
		sql += ' Shared_Comm_Agency_Receive_Value__c, Shared_Comm_Tax_Agency_Receive_Value__c, Shared_Comm_Agency_Request__c, Shared_Comm_Agency_Request_Value__c, Shared_Comm_Tax_Agency_Request_Value__c, '; 
		sql += ' client_course__r.share_commission_request_agency__r.Name, Shared_Comm_Pay_Agency__c, Shared_Comm_Pay_Agency_Value__c, Shared_Comm_Tax_Pay_Agency_Value__c, Pay_To_Agency__r.Name, '; 
		sql += ' Shared_Commission_Bank_Dep_Enrolled__c, Shared_Commission_Bank_Dep_Received__c, Shared_Commission_Bank_Dep_Pay_Agency__c, Shared_Commission_Bank_Dep_Request__c, client_course__r.Commission_Type__c, '; 
		sql += ' client_course__r.Enroled_by_Agency__r.name, ';

		sql += ' Hify_Agency_Commission_Value__c, Hify_Agency_Commission_Instal_Average__c, Hify_Agency_Commission_Tax__c, ';
		
		sql += ' Original_Instalment__c, Original_Instalment__r.isCommission_charged_on_amendment__c, Original_Instalment__r.isPfs__c, original_instalment__r.Commission_Tax_Value__c , '; 
		sql += ' Commission_Tax_Adjustment__c,  original_instalment__r.Commission_Value__c, original_instalment__r.discount__c, Commission_Adjustment__c, Original_Instalment__r.Tuition_Value__c ';

		sql += ' FROM client_course_instalment__c WHERE Shared_Commission_Bank_Dep_Enrolled__c IN :ids OR Shared_Commission_Bank_Dep_Received__c IN :ids OR Shared_Commission_Bank_Dep_Pay_Agency__c IN :ids OR Shared_Commission_Bank_Dep_Request__c IN :ids order by client_course__r.client__r.name '; 
		

		String payingAgencyName;

		decimal totalTax;
		decimal totalComm;

		for(client_course_instalment__c cci : Database.query(sql)){
			totalTax = 0;
			totalComm = 0;

			payingAgencyName = cci.Request_Commission_Invoice__r.Requested_by_Agency__r.Name != null ? cci.Request_Commission_Invoice__r.Requested_by_Agency__r.Name : cci.Received_By_Agency__r.Name;

			list<shareInstalment> allinst = new list<shareInstalment>();

			if(allInvoices.contains(cci.Shared_Commission_Bank_Dep_Enrolled__c)) //Enrolled
				allinst.add(new shareInstalment(cci, 1));
			

			if(allInvoices.contains(cci.Shared_Commission_Bank_Dep_Received__c)) //Received
				if(cci.isShare_Commission__c)
					allinst.add(new shareInstalment(cci, 2));
				else
					allinst.add(new shareInstalment(cci, 4));


			if(allInvoices.contains(cci.Shared_Commission_Bank_Dep_Request__c))	//Request
				allinst.add(new shareInstalment(cci, 3));


			if(allInvoices.contains(cci.Shared_Commission_Bank_Dep_Pay_Agency__c)) //Offshore
				allinst.add(new shareInstalment(cci, 5));


			for(shareInstalment sh : allinst){ // Sum commissions
				if(!invoice.isHifyAgency__c){
					totalTax += sh.commTaxReceive;
					totalComm += sh.commReceive;
				}else{
					totalTax += cci.Hify_Agency_Commission_Tax__c;
					totalComm += cci.Hify_Agency_Commission_Value__c;
				}
			}//end for

			if(!result.containsKey(payingAgencyName)){
				result.put(payingAgencyName, allinst);
				totalAgencies.put(payingAgencyName, new totals(totalTax, totalComm, totalTax + totalComm));
			}
			else{
				result.get(payingAgencyName).addAll(allinst);
				totalAgencies.get(payingAgencyName).sumTotals(totalTax, totalComm, totalTax + totalComm);	
			}
		}//end for


		agencyCommission = [SELECT Id, Name, Logo__c, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode, Registration_name__c, Trading_Name__c,	Business_Number__c, Type_of_Business_Number__c FROM Account WHERE Id = :invoice.Requested_Commission_for_Agency__c limit 1];


		IPFunctions.SearchNoSharing NS = new IPFunctions.SearchNoSharing();
		agencyRequested = ns.NSAccount('SELECT Id, Name, Parent.Name, Registration_Name__c, Type_of_Business_Number__c, Business_Number__c, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode From Account where Id= \''+ invoice.Requested_Commission_To_Agency__c + '\' limit 1 ');


		
	}

	public class totals{
		public decimal totalComm {get;set;}
		public decimal totalTax {get;set;}
		public decimal totalAmount {get;set;}

		public totals (Decimal totalTax, Decimal totalComm, Decimal totalAmount){
			this.totalComm = totalComm;
			this.totalTax = totalTax;
			this.totalAmount = totalAmount;
		}

		public void sumTotals (Decimal totalTax, Decimal totalComm, Decimal totalAmount){
			this.totalComm += totalComm;
			this.totalTax += totalTax;
			this.totalAmount += totalAmount;
		}
	}

	public class shareInstalment{
		// TYPE REQUESTS:
		// 1 - Enroll 
		// 2 - Receive 
		// 3- Request 
		// 4 - PDS 100%
		public transient client_course_instalment__c inst {get;set;}
		public String agencyName {get;set;}
		public integer typeRequest {get;set;} 
		public String description {get;set;}
		public decimal commReceive {get{return commReceive.setScale(2);}set;}
		public decimal commShare {get{return commShare.setScale(2);}set;}
		public decimal commTaxReceive {get{return commTaxReceive.setScale(2);}set;}
		public decimal commTaxShare {get{return commTaxShare.setScale(2);}set;}
		public shareInstalment(client_course_instalment__c instalment, integer typeRequest){
			this.inst = instalment;
			this.typeRequest = typeRequest;
			if(typeRequest == 1){
				this.commShare = inst.Shared_Comm_Agency_Enroll__c;
				this.commReceive = inst.Shared_Comm_Agency_Enroll_Value__c;
				this.commTaxReceive = inst.Shared_Comm_Tax_Agency_Enroll_Value__c;
				if(inst.Commission_Tax_Value__c != null && inst.Commission_Tax_Value__c >0)
				this.commTaxShare = (inst.Shared_Comm_Tax_Agency_Enroll_Value__c * 100) / inst.Commission_Tax_Value__c ;
				else
				this.commTaxShare = 0;
				this.agencyName = inst.client_course__r.Enroled_by_Agency__r.name;
				this.description = 'Enroled agency';
			}
			else if(typeRequest == 2){
				this.commShare = inst.Shared_Comm_Agency_Receive__c;
				this.commReceive = inst.Shared_Comm_Agency_Receive_Value__c;
				this.commTaxReceive = inst.Shared_Comm_Tax_Agency_Receive_Value__c;
				if(inst.Commission_Tax_Value__c != null && inst.Commission_Tax_Value__c >0)
				this.commTaxShare = (inst.Shared_Comm_Tax_Agency_Receive_Value__c * 100) / inst.Commission_Tax_Value__c ;
				else
				this.commTaxShare = 0;
				this.agencyName = inst.Received_by_Agency__r.Name;
				this.description = 'Received agency';
			}
			else if(typeRequest == 3){
				this.commShare = inst.Shared_Comm_Agency_Request__c;
				this.commReceive = inst.Shared_Comm_Agency_Request_Value__c;
				this.commTaxReceive = inst.Shared_Comm_Tax_Agency_Request_Value__c;
				if(inst.Commission_Tax_Value__c != null && inst.Commission_Tax_Value__c >0)
				this.commTaxShare = (inst.Shared_Comm_Tax_Agency_Request_Value__c * 100) / inst.Commission_Tax_Value__c ;
				else
				this.commTaxShare = 0;
				this.agencyName = inst.client_course__r.share_commission_request_agency__r.Name;
				this.description = 'Requested agency';
			}
			else if(typeRequest == 4){
				
				this.commShare = 100;
				if(inst.Original_Instalment__c != null && (inst.Original_Instalment__r.isCommission_charged_on_amendment__c || !inst.Original_Instalment__r.isPfs__c && inst.isPfs__c))
					this.commReceive = inst.Commission_Value__c + inst.original_instalment__r.Commission_Value__c + inst.Commission_Adjustment__c - inst.original_instalment__r.discount__c;
				else if(inst.Original_Instalment__c != null && !inst.Original_Instalment__r.isCommission_charged_on_amendment__c)
						this.commReceive = inst.Commission_Adjustment__c;
				else this.commReceive = inst.Commission_Value__c - inst.Discount__c;
				
				if(!inst.isPaidOffShore__c){
					if(inst.Original_Instalment__c != null && (inst.Original_Instalment__r.isCommission_charged_on_amendment__c || !inst.Original_Instalment__r.isPfs__c && inst.isPfs__c))
						this.commTaxReceive = inst.original_instalment__r.Commission_Tax_Value__c + inst.Commission_Tax_Adjustment__c + inst.Commission_Tax_Value__c;
					else if(inst.Original_Instalment__c != null && !inst.Original_Instalment__r.isCommission_charged_on_amendment__c)
						this.commTaxReceive = inst.Commission_Tax_Adjustment__c;
					else this.commTaxReceive = inst.Commission_Tax_Value__c;
				}
				else this.commTaxReceive = 0;
				this.commTaxShare = 100;
				this.agencyName = inst.Received_by_Agency__r.Name;
				this.description = 'Received agency';
			}
			else if(typeRequest == 5){
				this.commShare = inst.Shared_Comm_Pay_Agency__c;
				this.commReceive = inst.Shared_Comm_Pay_Agency_Value__c;
				this.commTaxReceive = inst.Shared_Comm_Tax_Pay_Agency_Value__c;
				if(inst.Commission_Tax_Value__c != null && inst.Commission_Tax_Value__c >0)
				this.commTaxShare = (inst.Shared_Comm_Tax_Pay_Agency_Value__c * 100) / inst.Commission_Tax_Value__c ;
				else
				this.commTaxShare = 0;
				this.agencyName = inst.Pay_To_Agency__r.Name;
				this.description = 'Elegible Agency';
			}
		}
	}
}