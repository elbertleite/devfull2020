@isTest
private class activity_log2_test{
	
	static testMethod void myUnitTest() {
		Date dueDate = Date.today();
		TestFactory tf = new TestFactory();
		Account agency = tf.createAgency();

		Contact emp = tf.createEmployee(agency);

		Contact clt = tf.createClient(agency);

		Contact lead = tf.createClient(agency);

		Account school = tf.createSchool();

       	Account agencyGroup = tf.createAgencyGroup();

       	agency.parentID = agencyGroup.id;

       	User portalUser = tf.createPortalUser(emp);

       	Account campus = tf.createCampus(school, agency);
       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
        Department__c department = tf.createDepartment(agency);

		client_course__c booking = tf.createBooking(clt);
		client_course__c booking2 = tf.createBooking(lead);

       	client_course__c cc =  tf.createClientCourseEnrolmentDate(clt, school, campus, course, campusCourse, booking);
		client_course__c cc2 =  tf.createClientCourseCancelled(clt, school, campus, course, campusCourse, booking);

		client_course__c cc3 =  tf.createClientCourseEnrolmentDate(lead, school, campus, course, campusCourse, booking);
		client_course__c cc4 =  tf.createClientCourseCancelled(lead, school, campus, course, campusCourse, booking);

		List<client_course_instalment__c> instalments = tf.createClientCourseInstalments(cc);
		List<client_course_instalment__c> instalments2 = tf.createClientCourseInstalments(cc3);

		Quotation__c q1 = tf.createQuotation(clt, campusCourse);
		Quotation__c q2 = tf.createQuotation(lead, campusCourse);

		Visit__c v = tf.createVisit(lead);
		Visit__c v2 = tf.createVisit(clt);

		client_course_instalment_payment__c pay = tf.addClientDepositPayment('String paymentType', agency, clt, cc, 10);
		

		client_course_instalment_payment__c payment = new client_course_instalment_payment__c();
		payment.Client__c = clt.id;
		payment.Date_Paid__c = system.today();
		payment.Received_On__c = Datetime.now();
		payment.Received_By_Agency__c = agency.id;
		payment.Payment_Type__c = '123';
		payment.Deposit__c = cc.id;
		payment.Agency_Currency_Value__c = 1;
		payment.selected__c = true;
		payment.client_course_instalment__c = instalments.get(0).id;
		insert payment;

		Custom_Note_Task__c t = new Custom_Note_Task__c();
			t.Comments__c='bla bla bla';
			t.Subject__c = 'Follow Up';
			t.Due_Date__c = dueDate;
			t.Assign_To__c =  UserInfo.getUserId();
			t.AssignedOn__c = dueDate;
			t.AssignedBy__c = UserInfo.getUserId();
			t.Status__c = 'In Progress';
			t.Selected__c = true;
			t.isNote__c = true;
			t.Instalment__c = instalments.get(0).id;
			t.Related_Contact__c = lead.id;
			insert t;

		t = new Custom_Note_Task__c();
			t.Comments__c='bla bla bla';
			t.Subject__c = 'Follow Up';
			t.Due_Date__c = dueDate;
			t.Assign_To__c =  UserInfo.getUserId();
			t.AssignedOn__c = dueDate;
			t.AssignedBy__c = UserInfo.getUserId();
			t.Status__c = 'In Progress';
			t.Selected__c = true;
			t.isNote__c = false;
			t.Related_Contact__c = clt.id;
			t.Instalment__c = instalments.get(0).id;
			insert t;

		client_course_instalment_payment__c ip = new client_course_instalment_payment__c();
		ip.Date_Paid__c = system.today();
		ip.Payment_Type__c = 'aaa';
		ip.Value__c = 123456;
		insert ip;

		Ownership_History__c ownership = new Ownership_History__c();
			ownership.From_Agency__C = agency.id;
		 	ownership.From_User__c = portalUser.id;
			ownership.To_Agency__c = agency.id;
		    ownership.To_User__c = portalUser.id;
			ownership.Contact__c = clt.id;
		insert ownership;

		Client_Stage_Follow_up__c cs = new Client_Stage_Follow_up__c();
		cs.Agency__c = agency.id;
		cs.Agency_Group__c = agencyGroup.id;
		cs.Client__c = clt.id;
		cs.Destination__c = 'AUS';

		insert cs;

		Destination_Tracking__c dt = new Destination_tracking__c();
		dt.Client__c = lead.id;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
		dt.Expected_Travel_Date__c = system.today().addMonths(3);
		dt.lead_Cycle__c = false;
		insert dt;

		Client_Checklist__c checklist = new Client_Checklist__c();
		checklist.Agency__c = agency.id;
		checklist.Agency_Group__c = agencyGroup.id;
		checklist.Destination__c = lead.Destination_Country__c;
		checklist.Checklist_Item__c = 'aaa';
		checklist.Client__c = lead.id;
		checklist.Destination_Tracking__c = dt.id;
		insert checklist;

		Client_Course_Overview__c cco = new Client_Course_Overview__c();
		cco.Client__c = clt.id;
		cco.Course_End_Date__c = system.today();
		cco.Course_Start_Date__c = system.today();
		cco.Destination_Tracking__c = dt.id;
		cco.Course_Id__c = 'aaa';
		cco.Course_Name__c = 'aaa';
		cco.Course_Type__c = 'aaa';
		cco.School_Campus_Id__c = 'aaa';
		cco.School_Campus_Name__c = 'aaa';
		
		insert cco;

		webFormInquiry__c wfi1 = new webFormInquiry__c();
		wfi1.Account__c = lead.id;
		wfi1.City__c = lead.Lead_Residence_City__c;
		wfi1.Country__c = lead.Lead_Residence_Country__c;
		wfi1.Destination__c = lead.Destination_Country__c;
		wfi1.How_did_you_hear_about_us__c = 'Internet';
		wfi1.How_long_are_you_going_for__c = ' 6 - 12 weeks';
		wfi1.Message__c = lead.Lead_Message__c;
		wfi1.Other_Destination__c = lead.Lead_Other_Destination__c;
		wfi1.Other_Product_Type__c = 'Work';
		wfi1.State__c = lead.Lead_Residence_State__c;
		wfi1.Study_Area__c = lead.Lead_Study_Type__c;
		wfi1.What_are_you_looking_for__c = lead.Lead_Product_Type__c;
		wfi1.When_are_you_planning_to_travel__c = ' 3 - 6 Months';
		wfi1.Selected__c = true;
		insert wfi1;

		Test.startTest();
     	system.runAs(portalUser){
			 new activity_log2();
			 new activity_log2(null);
			 String result = activity_log2.init(clt.id, '30');
			 result = activity_log2.init(clt.id, 'all');
			 result = activity_log2.init(lead.id, '30');
			 result = activity_log2.init(lead.id, 'all');

		}

	}
}