public with sharing class Batch_Fill_Gaps_Checklist{
	public Batch_Fill_Gaps_Checklist(){
		system.debug('');
	}
}
/*global with sharing class Batch_Fill_Gaps_Checklist implements Database.Batchable<sObject>, Database.Stateful{

    global Map<String, Client_Stage__c> stageZeroItens;
    global String groupID;
    global List<String> agencies;

    global Batch_Fill_Gaps_Checklist(String agencyGroup, List<String> agencies) {
        this.groupID = agencyGroup;
        this.agencies = agencies;
        stageZeroItens = new Map<String, Client_Stage__c>();
        for(Client_Stage__c stage : [SELECT ID, Stage_description__c, Stage__c, itemOrder__c FROM Client_Stage__c WHERE Stage__c = 'Stage 0' AND Agency_Group__c = :groupID AND Stage_description__c != 'LEAD - Not Interested'  ORDER BY itemOrder__c DESC]){
			if(!stage.Stage_description__c.contains('xxx')){
				stageZeroItens.put(stage.Stage_description__c, stage);
			}
		}
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        //String [] ids = new String [] {'0039000001qOzH1AAK','0039000002WLkdgAAD','0039000002UeXr4AAF','0039000002EvjATAAZ'};
        //String query = 'SELECT ID, Client__c FROM Destination_Tracking__c WHERE Agency_Group__c = :groupID AND Current_Cycle__c = true AND Client__c IN :ids';  
       String query = 'SELECT ID, Client__c FROM Destination_Tracking__c WHERE Agency_Group__c = :groupID AND Current_Cycle__c = true AND Client__r.Current_Agency__c IN :agencies AND Client__r.Lead_Stage__c = \'Stage 0\'';  
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Destination_Tracking__c> cycles) {
        List<String> idCycles = new List<String>();
        Map<String, List<Client_Stage_Follow_up__c>> contactsChecklist = new Map<String, List<Client_Stage_Follow_up__c>>();
        
        for(Destination_Tracking__c cycle : cycles){
            idCycles.add(cycle.ID);
        }

        for(Client_Stage_Follow_up__c check : [SELECT ID, Client__c, Client__r.Name, Agency_Group__c, Agency__c, Destination__c, Destination_Tracking__c, Stage_Item__c, Stage_Item_Id__c, Stage__c, Last_Saved_Date_Time__c, Checked_By__c FROM Client_Stage_Follow_up__c WHERE Stage__c = 'Stage 0' AND Agency_Group__c = :groupID AND Destination_Tracking__c in :idCycles ORDER BY Client__c, Last_Saved_Date_Time__c DESC]){
			if(!contactsChecklist.containsKey(check.Client__c)){
				contactsChecklist.put(check.Client__c, new List<Client_Stage_Follow_up__c>()); 
			}
			contactsChecklist.get(check.Client__c).add(check);
		}

        Map<String, List<Client_Stage_Follow_up__c>> itensToDeletePerContact = new Map<String, List<Client_Stage_Follow_up__c>>();
		Map<String, List<Client_Stage_Follow_up__c>> itensToAddPerContact = new Map<String, List<Client_Stage_Follow_up__c>>();

        Client_Stage_Follow_up__c newItem;
		
        List<Integer> indexesToRemove;
        List<String> checkListItensNamesPerContact;
        List<ReferenceDateByCheck> referenceDates;
        Client_Stage_Follow_up__c lastChecked;
		Decimal lastCheckedOrder;
		Datetime referenceDate;
        Integer counter;

        if(!contactsChecklist.isEmpty()){
            for(String contact : contactsChecklist.keySet()){
                lastChecked = null;
                indexesToRemove = new List<Integer>();
                itensToDeletePerContact.put(contact, new List<Client_Stage_Follow_up__c>()); 
                itensToAddPerContact.put(contact, new List<Client_Stage_Follow_up__c>()); 
                checkListItensNamesPerContact = new List<String>();

                for(Client_Stage_Follow_up__c checked : contactsChecklist.get(contact)){
                    if(lastChecked == null && stageZeroItens.get(checked.Stage_Item__c) != null){
                        lastChecked = checked;
                        lastCheckedOrder = stageZeroItens.get(checked.Stage_Item__c).itemOrder__c;    
                    }
                }
                if(lastChecked != null){   
                    
                    for(Client_Stage_Follow_up__c checked : contactsChecklist.get(contact)){
                        
                        checkListItensNamesPerContact.add(checked.Stage_Item__c);

                        if(checked.ID != lastChecked.ID){
                            if(stageZeroItens.get(checked.Stage_Item__c) != null && stageZeroItens.get(checked.Stage_Item__c).itemOrder__c > lastCheckedOrder){
                                itensToDeletePerContact.get(contact).add(checked); 		
                                indexesToRemove.add(contactsChecklist.get(contact).indexOf(checked));
                            }
                        }
                    }

                    if(!indexesToRemove.isEmpty()){
                        indexesToRemove.sort();
                        for(Integer i = indexesToRemove.size()-1; i >= 0; i--){
                            contactsChecklist.get(contact).remove(indexesToRemove.get(i));
                        }
                    }

                    

                    referenceDates = generateReferenceDates(contactsChecklist.get(contact));

                    counter = 0;
                    for(String item : stageZeroItens.keySet()){
                        if(stageZeroItens.get(item) != null && stageZeroItens.get(item).itemOrder__c < lastCheckedOrder && !checkListItensNamesPerContact.contains(item)){
                            counter -= 1;

                            for(ReferenceDateByCheck ref : referenceDates){
                                if(ref.itemOrder > stageZeroItens.get(item).itemOrder__c){
                                    referenceDate = ref.dateChecked.addSeconds(counter);
                                }
                            }
                            
                            newItem = new Client_Stage_Follow_up__c();
                            newItem.Checked_By__c = lastChecked.Checked_By__c;
                            newItem.Client__c = lastChecked.Client__c;
                            newItem.Agency_Group__c = lastChecked.Agency_Group__c;
                            newItem.agency__c = lastChecked.agency__c;
                            newItem.Stage_Item__c = stageZeroItens.get(item).Stage_description__c;
                            newItem.Stage_Item_Id__c = stageZeroItens.get(item).ID;
                            newItem.Stage__c = stageZeroItens.get(item).Stage__c;
                            newItem.Destination__c = lastChecked.Destination__c;
                            newItem.Destination_Tracking__c = lastChecked.Destination_Tracking__c;
                            //newItem.Last_Saved_Date_Time__c = lastChecked.Last_Saved_Date_Time__c.addMinutes(counter);	
                            newItem.Last_Saved_Date_Time__c = referenceDate;	

                            itensToAddPerContact.get(contact).add(newItem);
                        }
                    }
                }
            }

            //system.debug('ITENS TO DELETE '+JSON.serialize(itensToDeletePerContact));
            //system.debug('ITENS TO ADD '+JSON.serialize(itensToAddPerContact));

            List<Client_Stage_Follow_up__c> itensToDelete = new List<Client_Stage_Follow_up__c>();
            List<Client_Stage_Follow_up__c> itensToAdd = new List<Client_Stage_Follow_up__c>();

            for(String key : itensToDeletePerContact.keySet()){
                for(Client_Stage_Follow_up__c check : itensToDeletePerContact.get(key)){
                    itensToDelete.add(check);
                }
            }
            for(String key : itensToAddPerContact.keySet()){
                for(Client_Stage_Follow_up__c check : itensToAddPerContact.get(key)){
                    itensToAdd.add(check);
                }
            }

            system.debug('ITENS TO INSERT '+JSON.serialize(itensToAdd));

            if(!itensToDelete.isEmpty()){
                delete itensToDelete;
            }
            if(!itensToAdd.isEmpty()){
                insert itensToAdd;
            }
        }

    }

    private List<ReferenceDateByCheck> generateReferenceDates(List<Client_Stage_Follow_up__c> checklist){
        List<ReferenceDateByCheck> itens = new List<ReferenceDateByCheck>();
        for(Client_Stage_Follow_up__c checked : checklist){
            if(stageZeroItens.get(checked.Stage_Item__c) != null){
                itens.add(new ReferenceDateByCheck(stageZeroItens.get(checked.Stage_Item__c).itemOrder__c, checked.Last_Saved_Date_Time__c));
            }   
        }
        itens.sort();
        return itens;
    }

    public class ReferenceDateByCheck implements Comparable {
        public Decimal itemOrder{get;set;}
        public Datetime dateChecked{get;set;}

        public ReferenceDateByCheck(Decimal itemOrder, Datetime dateChecked){
            this.itemOrder = itemOrder;
            this.dateChecked = dateChecked;
        }

        public Integer compareTo(Object compareTo) {
			ReferenceDateByCheck oh1 = (ReferenceDateByCheck)compareTo;
			if(this.itemOrder < oh1.itemOrder){
				return 1;
			}else if(this.itemOrder > oh1.itemOrder){
				return -1;
			}
			return 0;
		}
        public Boolean equals(Object obj) {
            if (obj instanceof ReferenceDateByCheck) {
                ReferenceDateByCheck s = (ReferenceDateByCheck)obj;
				return itemOrder == s.itemOrder;
            }
            return false;
        }

        public Integer hashCode() {
            return itemOrder.intValue();
        }
    }
    
    global void finish(Database.BatchableContext BC) {}
}*/