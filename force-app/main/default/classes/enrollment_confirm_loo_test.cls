@isTest
private class enrollment_confirm_loo_test {

	public static Account school {get;set;}
	public static Account agency {get;set;}
	public static Contact emp {get;set;}
	public static Contact client {get;set;}
	public static User portalUser {get{
    if (null == portalUser) {
      portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
    } return portalUser;} set;}
	public static Account campus {get;set;}
	public static Course__c course {get;set;}
	public static Campus_Course__c campusCourse {get;set;}
	public static client_course__c clientCourseBooking {get;set;}
	public static client_course__c clientCourse {get;set;}

	public static list<client_course__c> allCourses {get{
		return [SELECT Id, Client__c, LOO_Requested_to_BO_By__c, LOO_Requested_to_BO_By__r.Contact.AccountId, LOO_Requested_to_BO_By__r.Contact.Account.ParentId, (SELECT Id,isFirstPayment__c, Number__c, Split_Number__c, Received_Date__c FROM client_course_instalments__r where Number__c = 1 AND (Split_Number__c = Null OR Split_Number__c = 1)) FROM client_course__c WHERE isBooking__c = false];
	}set;}
	
	@testSetup static void setup() {
		TestFactory tf = new TestFactory();
		Account schGroup = tf.createSchoolGroup();

		school = tf.createSchool();
		school.ParentId = schGroup.Id;
		update school;

		agency = tf.createAgency();
		emp = tf.createEmployee(agency);
		portalUser = tf.createPortalUser(emp);
		campus = tf.createCampus(school, agency);
		course = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);

		Schema.DescribeFieldResult fieldResult = client_course__c.Credit_Reason__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

		Back_Office_Control__c bo = new Back_Office_Control__c(Agency__c = agency.id, Country__c = 'Australia', Backoffice__c = agency.id, Services__c = 'Admissions');
		insert bo;
       
		
		Test.startTest();
			system.runAs(portalUser){
				client = tf.createClient(agency);
				clientCourseBooking = tf.createBooking(client);
				clientCourse = tf.createClientCourse(client, school, campus, course, campusCourse, clientCourseBooking);
				List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(clientCourse);
				
				clientCourse.LOO_Requested_To_BO__c = true;
				clientCourse.LOO_Requested_to_BO_By__c = portalUser.Id;
				update clientCourse;
			}
		Test.stopTest();
  	}

	static testMethod void testConstructor(){
		system.runAs(portalUser){
			enrollment_confirm_loo test = new enrollment_confirm_loo();

			String bucket = test.bucket; 
		}
	}

	static testMethod void testSearch(){
		String result = enrollment_confirm_loo.search('Australia', allCourses[0].LOO_Requested_to_BO_By__r.Contact.Account.ParentId, new list<String>{allCourses[0].LOO_Requested_to_BO_By__r.Contact.AccountId}, 'all', 'all', null, null, null, 'request');
	}
	
	static testMethod void testOnlineApplication(){

		system.runAs(portalUser){
			String result = enrollment_confirm_loo.onlineApplication(allCourses[0].Id, new list<String>(), 'comments', String.valueof(allCourses[0].client_course_instalments__r[0].Id), true, allCourses[0].Client__c);
		}
	}

	static testMethod void testMissingDocs(){

		system.runAs(portalUser){
			String result = enrollment_confirm_loo.missingDocs(allCourses[0].Id, new list<String>(), 'comments', String.valueof(allCourses[0].client_course_instalments__r[0].Id), true, allCourses[0].Client__c);
		}
	}

	static testMethod void testConfirmLOO(){

		system.runAs(portalUser){
			String result = enrollment_confirm_loo.confirmLOO(allCourses[0].Id, new list<String>(), 'comments', String.valueof(allCourses[0].client_course_instalments__r[0].Id), true, 'true', allCourses[0].Client__c, 'false');
		}
	}

	static testMethod void testIsFileUploaded(){

		system.runAs(portalUser){
			String result = enrollment_confirm_loo.isFileUploaded(allCourses[0].Id);
		}
	}
	
	
}