public with sharing class product_commission_invoice {
	
	private User currentUser = [Select Id, Name, Aditional_Agency_Managment__c, Contact.Account.ParentId, Contact.AccountId, Contact.Department__c, Contact.Account.Global_Link__c from User where id = :UserInfo.getUserId() limit 1];
	
	public product_commission_invoice() {
		String id = ApexPages.currentPage().getParameters().get('id');
		String toInvoice = ApexPages.currentPage().getParameters().get('ids');

		if(toInvoice != null && toInvoice != '')
			newInvoice(toInvoice);
		else if(id != null && id != '')
			findInvoice(id);
	}

	public list<client_product_service__c> listProducts {get;set;}
	public Provider__c provider {get;set;}
	public Account agencyDetails {get;set;}
	public String curr {get;set;}
	public Decimal totInv {get;set;}

	private String prodFields = 'SELECT Products_Services__r.Provider__c, Products_Services__r.Provider__r.Provider_Name__c, Client__r.name, Received_By_Department__r.name, Price_Total__c, Quantity__c, Unit_Description__c, Received_Date__c, Received_By_Agency__r.name, Received_by__r.name, Product_Name__c, Currency__c, Agency_Currency_Value__c, Agency_Currency__c, Agency_Currency_Rate__c, Claim_Commission_Type__c, Total_Provider_Payment__c, Commission_Tax_Rate__c, Commission_Tax_Value__c, Commission_Type__c, Commission_Value__c, Commission__c FROM client_product_service__c';

	private String agencyFields = 'SELECT Id, Name, Logo__c, BillingStreet, BillingCity, BillingCountry,BillingState, BillingPostalCode, Registration_name__c, Trading_Name__c,Business_Number__c, Type_of_Business_Number__c, Product_Commission_Bank__r.Bank__c,Product_Commission_Bank__r.Branch_Address__c, Product_Commission_Bank__r.Account_Name__c, Product_Commission_Bank__r.BSB__c, Product_Commission_Bank__r.Account_Number__c, Product_Commission_Bank__r.Swift_Code__c, Product_Commission_Bank__r.IBAN__c, Bank_Sub_Agency__r.Bank__c, Bank_Sub_Agency__r.Branch_Address__c, Bank_Sub_Agency__r.Account_Name__c, Bank_Sub_Agency__r.BSB__c, Bank_Sub_Agency__r.Account_Number__c, Bank_Sub_Agency__r.Swift_Code__c, Bank_Sub_Agency__r.IBAN__c FROM Account ';

	private void newInvoice(String ids){
		
		try{
			list<Id> allIds = ids.split(';');

			listProducts = Database.query(prodFields + '  WHERE ID IN :allIds' );
			provider = [SELECT Registration_Name__c, Type_of_Business_Number__c, Business_Number__c, Invoice_Street__c, Invoice_City__c, Invoice_State__c, Invoice_PostalCode__c, Invoice_Country__c, Invoice_to_Email__c FROM Provider__c WHERE ID = :listProducts[0].Products_Services__r.Provider__c limit 1];

			agencyDetails = Database.query(agencyFields + '  WHERE Id = \'' + ApexPages.currentPage().getParameters().get('ag') + '\''); 

			curr = listProducts[0].Currency__c;

			totInv = 0;
			for(client_product_service__c p : listProducts)
				totInv += p.Commission_Value__c + p.Commission_Tax_Value__c;

		}catch(Exception e){
			system.debug('Error  ' + e.getMessage() + '  line ' + e.getLineNumber());
			listProducts = new list<client_product_service__c>();
			provider = new Provider__c();
			agencyDetails = new Account();
		}
	}

	public Invoice__c invoice {get;set;}
	private void findInvoice(String id){
		
		try{
			invoice = [SELECT Due_Date__c, Requested_by_Agency__c, Request_Provider_Commission__c, createdDate, CurrencyIsoCode__c, Requested_Commission_for_Agency__c, isCancelled__c, Cancel_Reason__c, Cancel_Description__c, Cancelled_by__r.Name, Instalments__c FROM Invoice__c WHERE Id = :id limit 1];

			if(!invoice.isCancelled__c)
				listProducts = Database.query(prodFields + '  WHERE Request_Commission__c = :id');
			else{
				list<String> old = invoice.Instalments__c.split(';');
				listProducts = Database.query(prodFields + '  WHERE Id = :old');
			}

			provider = [SELECT Registration_Name__c, Type_of_Business_Number__c, Business_Number__c, Invoice_Street__c, Invoice_City__c, Invoice_State__c, Invoice_PostalCode__c, Invoice_Country__c, Invoice_to_Email__c FROM Provider__c WHERE ID = :listProducts[0].Products_Services__r.Provider__c limit 1];

			
			agencyDetails = Database.query(agencyFields + '  WHERE Id = \'' + invoice.Requested_Commission_for_Agency__c + '\''); 

			

			curr = listProducts[0].Currency__c;

			totInv = 0;
			for(client_product_service__c p : listProducts)
				totInv += p.Commission_Value__c + p.Commission_Tax_Value__c;

		}catch(Exception e){
			system.debug('Error  ' + e.getMessage() + '  line ' + e.getLineNumber());
			listProducts = new list<client_product_service__c>();
			provider = new Provider__c();
			agencyDetails = new Account();
		}
	}
}