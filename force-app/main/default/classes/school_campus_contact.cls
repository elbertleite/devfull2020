public without sharing class school_campus_contact {
	public Account accSelected{get;set;}
	public List<Account> schoolCampuses{get;set;}
	public List<Contact> campusContacts{get;set;}
	public List<CampusToSaveContact> campusesToSave{get;set;}
	public Map<String, List<Account>> campusesPerContact{get;set;}
	public string campusesPerContactList{get{if(campusesPerContactList == null) campusesPerContactList = ''; return campusesPerContactList;}set;}

	public String idSchool{get;set;}

	public Contact newContact{get;set;}
	public Boolean modalNewContact{get{if(modalNewContact == null) modalNewContact = false; return modalNewContact;} set;}
	public List<Forms_of_Contact__c> otherFormsOfContact{get;set;}
	public List<Forms_of_Contact__c> otherFormsOfContactToDelete{get;set;}

	public List<SelectOption> allContactTypes{
		get{
			if(allContactTypes == null){
				allContactTypes = new List<SelectOption>();
				Schema.DescribeFieldResult fieldResult = Contact.fields.Contact_Type__c.getDescribe();
				List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
				for( Schema.PicklistEntry f : ple){
					allContactTypes.add(new SelectOption(f.getValue(), f.getValue()));
				}
			} return allContactTypes;
		} 
		set;
	}
	public List<String> contactTypesSelected{get;set;}

	public school_campus_contact() {
		try{
			String id = ApexPages.currentPage().getParameters().get('id');
			
			accSelected = Database.query('SELECT ID, Parent.ID, RecordType.Name FROM Account WHERE ID = :id');
			loadContacts();
		}catch(Exception ex){
			system.debug('Error on school_campus_contact() <=== '+ex.getMessage()+' '+ex.getLineNumber());
		}
	}

	public void openCloseModalNewContact(){
		this.modalNewContact = !this.modalNewContact;

		newContact = new Contact();
		contactTypesSelected = new List<String>();
		otherFormsOfContact = new List<Forms_of_Contact__c>();
		Forms_of_Contact__c form = new Forms_of_Contact__c();
		otherFormsOfContact.add(form);
		otherFormsOfContactToDelete = new List<Forms_of_Contact__c>();
	}

	public void openEditContact(){
		String contactID = ApexPages.currentPage().getParameters().get('contactID');
		
		otherFormsOfContactToDelete = new List<Forms_of_Contact__c>();
		otherFormsOfContact = new List<Forms_of_Contact__c>();
		contactTypesSelected = new List<String>();

		if(Test.isRunningTest()){
			String campusID = ApexPages.currentPage().getParameters().get('campusID');
			Contact ctt = new Contact();
			ctt.LastName = 'Test test';
			ctt.Contact_Type__c = '123;123;123';
			ctt.campusIds__c = campusID+';';
			insert ctt;
			contactID = ctt.id;
			campusContacts.add(ctt);
		}

		for(Contact ctt : campusContacts){
			if(ctt.ID == contactID){
				newContact = ctt;
				break;
			}
		}

		if(!String.isEmpty(newContact.Contact_Type__c)){
			for(String type : newContact.Contact_Type__c.split(';')){
				if(!String.isEmpty(type)){
					contactTypesSelected.add(type);
				}
			}
		}

		for(Forms_of_Contact__c form : newContact.Forms_of_Contact__r){
			otherFormsOfContact.add(form);
		}
		if(otherFormsOfContact.isEmpty()){
			Forms_of_Contact__c form = new Forms_of_Contact__c();
			otherFormsOfContact.add(form);
		}

		if(newContact.campusIds__c != null){
			for(String campusID : newContact.campusIds__c.split(';')){
				for(CampusToSaveContact cp : campusesToSave){
					if(cp.ID == campusID){
						cp.selected = true;
					}
				}
			}
		}

		this.modalNewContact = !this.modalNewContact;
	}

	public void addNewOtherForm(){
		Forms_of_Contact__c form = new Forms_of_Contact__c();
		otherFormsOfContact.add(form);
	}

	public void updateTypeFormContact(){
		String value = ApexPages.currentPage().getParameters().get('value');
		String index = ApexPages.currentPage().getParameters().get('index');
		if(!String.isEmpty(index)){
			Forms_of_Contact__c form = otherFormsOfContact.get(Integer.valueOf(index));
			form.Type__c = value;
		}
	}

	public void deleteOtherForm(){
		String index = ApexPages.currentPage().getParameters().get('indexOtherForm');
		if(!String.isEmpty(index)){
			Forms_of_Contact__c form = otherFormsOfContact.get(Integer.valueOf(index));
			if(form.ID != null){
				otherFormsOfContactToDelete.add(form);		
			}
			otherFormsOfContact.remove(Integer.valueOf(index));
		}
	}

	public void saveContact(){
		try{
			String contactTypes = '';
			if(contactTypesSelected != null && !contactTypesSelected.isEmpty()){
				for(String contactType : contactTypesSelected){
					contactTypes = contactTypes + contactType + ';';
				}
			}
			newContact.Contact_Type__c = contactTypes; 
			
			Set<String> campusesToSaveNewContact = new Set<String>();
			for(CampusToSaveContact cp : campusesToSave){
				if(cp.selected){
					campusesToSaveNewContact.add(cp.ID);
				}
			}
			if(!campusesToSaveNewContact.isEmpty()){
				
				String campusIds = '';
				for(String ID : campusesToSaveNewContact){
					campusIds = campusIds + ID + ';';
				}
				/*
				for(Account campus : schoolCampuses){
				}*/

				newContact.campusIds__c = campusIds;
				
				if(newContact.ID == null){
					newContact.AccountId = idSchool;
					newContact.recordTypeId = [Select Id From RecordType where Name = 'School Campus Contact' and IsActive = true Limit 1].Id;
					newContact.Full_Name__c = newContact.FirstName + ' ' + newContact.LastName;
	
					if(!Test.isRunningTest()){
						insert newContact;
					}

					List<Forms_of_Contact__c> toSave = new List<Forms_of_Contact__c>();
					for(Integer i = 0; i < otherFormsOfContact.size(); i++){
						otherFormsOfContact.get(i).Contact__c = newContact.ID;
						if(!String.isEmpty(otherFormsOfContact.get(i).Detail__c)){
							toSave.add(otherFormsOfContact.get(i));
						}
					}
					List<Account> schoolCampusesToUpdate = new list<Account>();
					for(Account campus : [SELECT ID, contactsId__c FROM Account WHERE ID IN :campusesToSaveNewContact]){
						campus.contactsId__c = String.isEmpty(campus.contactsId__c) ? newContact.ID + ';' : campus.contactsId__c + newContact.ID + ';';
						schoolCampusesToUpdate.add(campus);
					}

					if(!Test.isRunningTest()){
						if(!toSave.isEmpty()){
							insert toSave;
						}
						update schoolCampusesToUpdate;
					}
				}else{
					update newContact;
					String contactsIds;
					for(Account campus : schoolCampuses){
						contactsIds = campus.contactsId__c;
						if(!campusesToSaveNewContact.contains(campus.ID)){
							if(!String.isEmpty(contactsIds) && contactsIds.contains(newContact.ID)){
								contactsIds = contactsIds.remove(newContact.ID+';');
							}
						}else{
							if(String.isEmpty(contactsIds)){
								contactsIds = '';
							}
							if(!contactsIds.contains(newContact.ID)){
								contactsIds = contactsIds + ';' + newContact.ID + ';';
							}
						}
						campus.contactsId__c = contactsIds.replace(';;',';');
					}
					update schoolCampuses;

					List<Forms_of_Contact__c> toSave = new List<Forms_of_Contact__c>();
					for(Integer i = 0; i < otherFormsOfContact.size(); i++){
						if(!String.isEmpty(otherFormsOfContact.get(i).Detail__c)){
							if(otherFormsOfContact.get(i).ID == null){
								otherFormsOfContact.get(i).Contact__c = newContact.ID;
							}
							toSave.add(otherFormsOfContact.get(i));
						}else{
							if(otherFormsOfContact.get(i).ID != null){
								otherFormsOfContactToDelete.add(otherFormsOfContact.get(i));	
							}
						}
					}
					if(!toSave.isEmpty()){
						upsert toSave;
					}
					if(!otherFormsOfContactToDelete.isEmpty()){
						delete otherFormsOfContactToDelete;
					}
				}
			}
			this.modalNewContact = !this.modalNewContact;
			loadContacts();
		}catch(Exception ex){
			system.debug('Error on saveContact() <=== '+ex.getMessage()+' '+ex.getLineNumber());
		}
	}

	public void deleteContact(){
		try{
		String contactID = ApexPages.currentPage().getParameters().get('contactID');
			Contact contact;
			for(Contact ctt : campusContacts){
				if(ctt.ID == contactID){
					contact = ctt;
					break;
				}
			}
			delete contact;
			loadContacts();
		}catch(Exception ex){
			system.debug('Error on school_campus_contact() <=== '+ex.getMessage()+' '+ex.getLineNumber());
		}
	}

	public void loadContacts(){
		try{
			Set<String> ids = new Set<String>();
			if(accSelected.RecordType.Name == 'School'){
				system.debug('SCHOOL FOUND '+accSelected.ID);
				idSchool = accSelected.ID;
			}else if(accSelected.RecordType.Name == 'Campus'){
				system.debug('CAMPUS FOUND '+accSelected.Parent.ID);
				idSchool = accSelected.Parent.ID;
			}
			schoolCampuses = new List<Account>();
			CampusToSaveContact toSave;
			campusesToSave = new List<CampusToSaveContact>();
			for(Account campus : [SELECT ID, Name, contactsId__c FROM Account WHERE RecordType.Name = 'Campus' AND Parent.ID = :idSchool]){
				toSave = new CampusToSaveContact();
				toSave.ID = campus.ID;
				toSave.name = campus.Name;
				toSave.selected = false;
				campusesToSave.add(toSave);
				if(!String.isEmpty(campus.contactsId__c)){
					ids.addAll(campus.contactsId__c.split(';'));
				}
				schoolCampuses.add(campus);		
			}
			for(Account campus : [SELECT ID, Name, contactsId__c FROM Account WHERE RecordType.Name = 'School' AND Parent.ID = :idSchool]){
				if(!String.isEmpty(campus.contactsId__c)){
					ids.addAll(campus.contactsId__c.split(';'));
				}
			}
			if(!ids.isEmpty()){
				system.debug('CONTACTS FOUND FOR SCHOOL '+idSchool+': '+JSON.serialize(ids));
				campusContacts = [Select Id, FirstName, LastName, Name, Contact_Type__c, Position_Title__c, Email, Phone, MobilePhone, Fax, campusIds__c, (SELECT ID, Detail__c, Type__c FROM Forms_of_Contact__r) From Contact where RecordType.Name = 'School Campus Contact' and Id in :ids order by Name];

				campusesPerContact = new Map<String, List<Account>>();

				Set<String> campusesIDs;
				for(Contact ctt : campusContacts){
					if(ctt.campusIds__c != null){
						campusesIDs = new Set<String>(ctt.campusIds__c.split(';'));
						campusesPerContact.put(ctt.ID, new List<Account>());
						for(Account campus : schoolCampuses){
							if(campusesIDs.contains(campus.ID)){
								campusesPerContact.get(ctt.ID).add(campus);
							}
						}	
					}
				}
				campusesPerContactList = string.Join(new list<string>(campusesPerContact.keySet()),',');
			}
		}catch(Exception ex){
			system.debug('Error on loadContacts() <=== '+ex.getMessage()+' '+ex.getLineNumber());
		}
	}

	public List<SelectOption> getListTypesOtherContact(){
		List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Forms_of_Contact__c.fields.Type__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple){
			options.add(new SelectOption(f.getValue(), f.getLabel()));
		}
		return options;
	}

	public class CampusToSaveContact{
		public String id{get;set;}
		public String name{get;set;}
		public boolean Selected{get;set;}
	}
}