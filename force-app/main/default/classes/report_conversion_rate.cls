public without sharing class report_conversion_rate {

	public static Boolean retry = true;
	public static Integer counter = 0;

	public List<List<ContactsForExcel>> resultExcel{get;set;}

	public report_conversion_rate(){}

	public void generateExcelFile(){
		User user = [Select Id, Report_Convertion_Rate_Filters__c from User where Id = :UserInfo.getUserId()];
		//User user = [Select Id, Report_Convertion_Rate_Filters__c from User where Id = '0059000000TnfQt'];
	
		Map<String, String> filters = null;	
		if(Test.isRunningTest()){
			String txt = '{"idAgencies":"[]","showClientsEnrolled":"false","beginDate":"24/03/2019","endDate":"26/03/2019","campaignsSelected":"[]","typeOfSearchSelected":"firstClick","destinationSelected":"-- All Destinations --","employeeSelected":"all","agencySelected":"[]","groupSelected":"0019000001MjqDXAAZ"}';

			filters = (Map<String, String>) JSON.deserialize(txt, Map<String, String>.class);
		}else{
			filters = (Map<String, String>) JSON.deserialize(user.Report_Convertion_Rate_Filters__c, Map<String, String>.class);
		}

		
		String groupSelected = filters.get('groupSelected');
		List<String> agencySelected = (List<String>) JSON.deserialize(filters.get('agencySelected'), List<String>.class);
		String employeeSelected = filters.get('employeeSelected');
		String destinationSelected = filters.get('destinationSelected');
		String typeOfSearchSelected = filters.get('typeOfSearchSelected');
		List<String> campaignsSelected = (List<String>) JSON.deserialize(filters.get('campaignsSelected'), List<String>.class);
		String endDate = filters.get('endDate');
		String beginDate = filters.get('beginDate');
		Boolean showClientsEnrolled = filters.get('showClientsEnrolled') == 'true';
		List<String> idAgencies =  (List<String>) JSON.deserialize(filters.get('idAgencies'), List<String>.class);

		String queryDTrackingsExcel;
		Set<String> idClients = null;
		List<Client_Stage_Follow_Up__c> clicksToAnalize = null;
		Date fromDate = IPFunctions.getDateFromString(beginDate);
		Date toDate = IPFunctions.getDateFromString(endDate);
		/*if(campaignsSelected != null && !campaignsSelected.isEmpty()){
			clicksToAnalize = retrieveClicksByCampaign(groupSelected, agencySelected, employeeSelected, destinationSelected, campaignsSelected, fromDate, toDate, idAgencies, typeOfSearchSelected);	
		}else{*/
			String queryDestinationTrackings = generateQueryDestinationTracking(campaignsSelected, groupSelected, agencySelected, employeeSelected, destinationSelected, typeOfSearchSelected, fromDate, toDate, true);
			Map<String, String> cyclesPerContact = new Map<String, String>();
			for(Client_Stage_Follow_Up__c click : Database.query(queryDestinationTrackings)){
				cyclesPerContact.put(click.Client__c, click.Destination_Tracking__c);
			}
			if(!cyclesPerContact.isEmpty()){
				clicksToAnalize = retrieveClicksByDestinationTracking(groupSelected, cyclesPerContact.values(), fromDate, toDate);	
			}
		//}	
		if(Test.isRunningTest()){
			clicksToAnalize = new List<Client_Stage_Follow_Up__c>();
			clicksToAnalize.add(generateFollowUpTest());
		}
		if(clicksToAnalize != null && !clicksToAnalize.isEmpty()){
			Integer maxItens = 10000;
			Integer totalItens = clicksToAnalize.size();
			Integer loops = totalItens / 10000;
			if(math.mod(totalItens, maxItens) > 1){
				loops += 1;
			}
			
			Integer index = 0;
			idClients = new Set<String>();
			
			ContactsForExcel ctt;
			Client_Stage_Follow_Up__c click;

			resultExcel = new List<List<ContactsForExcel>>();
			List<ContactsForExcel> listContacts;
			Datetime conversionDate;
			String rdStationConversion;
			if(Test.isRunningTest()){
				loops = 1;
			}
			for(Integer i = 0; i < loops; i++){
				listContacts = new List<ContactsForExcel>();
				for(Integer x = 0; x < maxItens; x++){
					if(index < totalItens){
						click = clicksToAnalize.get(index);
						if(!idClients.contains(click.Client__c)){
							rdStationConversion = null;
							ctt = new ContactsForExcel();
							ctt.contactID = click.Client__r.ID;
							ctt.contactEmail = click.Client__r.Email;
							ctt.contactName = click.Client__r.Name;
							if(!Test.isRunningTest()){
								ctt.createdDate = click.Client__r.CreatedDate.format('dd/MM/yyyy');
								if(click.Destination_Tracking__r.Lead_Last_Change_Status_Cycle__c != null){
									ctt.checkedDate = click.Destination_Tracking__r.Lead_Last_Change_Status_Cycle__c.format('dd/MM/yyyy');
								}
							}
							
							if(click.Client__r.Status__c == 'LEAD - Not Interested'){
								ctt.contactStatus = click.Client__r.Lead_Stage__c == 'Stage 0' ? 'Lead Lost' : 'Client Cancelled'; 
							}else{
								ctt.contactStatus = click.Client__r.RecordType.Name == 'Client' ? 'Client' : 'Lead';
							}

							ctt.status = click.Destination_Tracking__r.Lead_Last_Status_Cycle__c == 'LEAD - Not Interested' ? click.Client__r.Last_Stage_Before_Not_Interested__c : click.Destination_Tracking__r.Lead_Last_Status_Cycle__c;
							ctt.stageStatus = click.Destination_Tracking__r.Lead_Last_Status_Cycle__c == 'LEAD - Not Interested' ? click.Client__r.Lead_Stage__c : click.Destination_Tracking__r.Lead_Last_Stage_Cycle__c;
							ctt.destination = click.Destination_Tracking__r.Destination_Country__c;
							
							conversionDate = click.Destination_Tracking__r.Lead_Last_Status_Cycle__c == 'LEAD - Not Interested' ? click.Client__r.Lead_Status_Date_Time__c : click.Client__r.Lead_Converted_On__c;
							if(conversionDate != null){
								ctt.conversionDate = conversionDate.format('dd/MM/yyyy');
							}
							ctt.checkedBy = click.Client__r.Owner__r.Name;
							ctt.currentAgency = click.Client__r.Current_Agency__r.Name;
							ctt.enrolledCourse = click.Client__r.Client_Enrolled_In_Course__c ? 'YES' : 'NO';
							ctt.contactOrigin = click.Client__r.Contact_Origin__c;
							ctt.lostReason = click.Client__r.Lead_Status_Reason__c;
							ctt.lostComment = click.Client__r.Lead_Lost_Comment__c;

							if(ctt.contactOrigin == 'RDStation'){
								if(!String.isEmpty(click.Destination_Tracking__r.RDStation_Campaigns__c)){
									rdStationConversion = click.Destination_Tracking__r.RDStation_Campaigns__c;
								}else if(!String.isEmpty(click.Client__r.RDStation_Current_Campaign__c)){
									rdStationConversion = click.Client__r.RDStation_Current_Campaign__c;
								}else if(!String.isEmpty(click.Destination_Tracking__r.RDStation_First_Conversion__c)){
									rdStationConversion = click.Destination_Tracking__r.RDStation_First_Conversion__c;
								}else if(!String.isEmpty(click.Client__r.RDStation_Last_Conversion__c)){
									rdStationConversion = click.Client__r.RDStation_Last_Conversion__c;
								}
								ctt.rdStationCampaign = rdStationConversion;
							}
							listContacts.add(ctt);

							idClients.add(click.Client__c);
						}
						index += 1;
					}else{
						break;
					}
				}
				resultExcel.add(listContacts);
			}


			/*idClients = new Set<String>();
			ContactsForExcel ctt;
			for(Client_Stage_Follow_Up__c click : clicksToAnalize){
				if(!idClients.contains(click.Client__c)){
					ctt = new ContactsForExcel();
					ctt.contactID = click.Client__r.ID;
					ctt.contactEmail = click.Client__r.Email;
					ctt.contactName = click.Client__r.Name;
					
					ctt.rdStationCampaign = click.Destination_Tracking__r.RDStation_Campaigns__c;
					
					if(click.Client__r.Status__c == 'LEAD - Not Interested'){
						ctt.contactStatus = click.Client__r.Lead_Stage__c == 'Stage 0' ? 'Lead Lost' : 'Client Cancelled'; 
					}else{
						ctt.contactStatus = click.Client__r.RecordType.Name == 'Client' ? 'Client' : 'Lead';
					}

					ctt.status = click.Destination_Tracking__r.Lead_Last_Status_Cycle__c == 'LEAD - Not Interested' ? click.Client__r.Last_Stage_Before_Not_Interested__c : click.Destination_Tracking__r.Lead_Last_Status_Cycle__c;
					ctt.stageStatus = click.Destination_Tracking__r.Lead_Last_Status_Cycle__c == 'LEAD - Not Interested' ? click.Client__r.Lead_Stage__c : click.Destination_Tracking__r.Lead_Last_Stage_Cycle__c;
					ctt.destination = click.Destination_Tracking__r.Destination_Country__c;
					ctt.checkedDate = click.Destination_Tracking__r.Lead_Last_Change_Status_Cycle__c;
					ctt.createdDate = click.Client__r.CreatedDate;
					ctt.conversionDate = click.Destination_Tracking__r.Lead_Last_Status_Cycle__c == 'LEAD - Not Interested' ? click.Client__r.Lead_Status_Date_Time__c : click.Client__r.Lead_Converted_On__c;
					ctt.checkedBy = click.Client__r.Owner__r.Name;
					ctt.currentAgency = click.Client__r.Current_Agency__r.Name;
					ctt.enrolledCourse = click.Client__r.Client_Enrolled_In_Course__c ? 'YES' : 'NO';
					ctt.contactOrigin = click.Client__r.Contact_Origin__c;
					resultExcel.add(ctt);

					idClients.add(click.Client__c);
				}
			}*/
		}	
	}

	@ReadOnly
	@RemoteAction
	public static Map<String, String> generateReport(String groupSelected, List<String> agencySelected, String employeeSelected, String destinationSelected, String typeOfSearchSelected, List<String> campaignsSelected, String endDate, String beginDate, Boolean showClientsEnrolled, List<String> idAgencies){
		Map<String, String> response = new Map<String, String>();
		List<ReportResult> graph = generateGraphBars(groupSelected);
		String firstStatusStageZero = graph.get(0).status;

		List<Client_Stage_Follow_Up__c> clicksToAnalize;

		Date fromDate = IPFunctions.getDateFromString(beginDate);
		Date toDate = IPFunctions.getDateFromString(endDate);
		/*if(campaignsSelected != null && !campaignsSelected.isEmpty()){
			clicksToAnalize = retrieveClicksByCampaign(groupSelected, agencySelected, employeeSelected, destinationSelected, campaignsSelected, fromDate, toDate, idAgencies, typeOfSearchSelected);
		}else{*/
			String queryDestinationTrackings = generateQueryDestinationTracking(campaignsSelected, groupSelected, agencySelected, employeeSelected, destinationSelected, typeOfSearchSelected, fromDate, toDate, false);
			system.debug('THE QUERY '+queryDestinationTrackings);
			Map<String, String> cyclesPerContact = new Map<String, String>();

			for(Client_Stage_Follow_Up__c click : Database.query(queryDestinationTrackings)){
				cyclesPerContact.put(click.Client__c, click.Destination_Tracking__c);
			}

			if(Test.isRunningTest()){
				Client_Stage_Follow_Up__c followUp = generateFollowUpTest();
				cyclesPerContact.put(followUp.Client__c, followUp.Destination_Tracking__c);
			}

			if(!cyclesPerContact.isEmpty()){
				clicksToAnalize = retrieveClicksByDestinationTracking(groupSelected, cyclesPerContact.values(), fromDate, toDate);	
			}
		//}
		if(Test.isRunningTest()){
			clicksToAnalize.add(generateFollowUpTest());
		}
		if(clicksToAnalize != null && !clicksToAnalize.isEmpty()){
			Integer index;
			Date dateToCompare;
			Integer daysBetweenStatuses;
			ReportResult itemGraph;
			ReportResult itemGraphAux;

          	//List<AverageTimePerContact> timeAverage;
			//Map<String, List<AverageTimePerContact>> averageTimePerClient = new Map<String, List<AverageTimePerContact>>();
			Map<String, String> clientsToRemoveFromStagnated = new Map<String, String>(); //Map necessary because contacts converted must be remove from the stagnated bar, otherwise the numbers won't match. 
        	Map<String, String> clientsWithCourses = new Map<String, String>(); //Map necessary because contacts converted must be remove from the stagnated bar, otherwise the numbers won't match.

			Map<String, Integer> itemsOrder = new Map<String, Integer>();
			Map<String, Set<String>> contactsPerStatus = new Map<String, Set<String>>(); 
			for(ReportResult itemResult : graph){
				itemsOrder.put(itemResult.status, itemResult.itemOrder);
				contactsPerStatus.put(itemResult.status, new Set<String>());
			}

			Map<String, String> lastClickPerContact = new Map<String, String>();
			for(Client_Stage_Follow_Up__c click : clicksToAnalize){
				if(click.Stage_Item__c != 'LEAD - Not Interested'){
					lastClickPerContact.put(click.Client__c, click.Stage_Item__c);
				}
			}
			Object[] previousDateClick;
			Map<String, Object[]> lastDateClickPerClient = new Map<String, Object[]>();
			//Map<String, List<Object[]>> clicksPerClient = new Map<String, List<Object[]>>();

			for(Client_Stage_Follow_Up__c click : clicksToAnalize){
				if(Test.isRunningTest() || (click.Client__r.RecordType.Name == 'Client' && click.Client__r.Status__c != 'LEAD - Not Interested')){
					if(click.Client__r.Client_Enrolled_In_Course__c){
						clientsWithCourses.put(click.Client__c, click.Stage_Item__c);
					}else{
						clientsToRemoveFromStagnated.put(click.Client__c, click.Stage_Item__c);
					}
				}			
				if(click.Stage_Item__c == 'LEAD - Not Interested'){
					if(!String.isEmpty(click.Client__r.Last_Stage_Before_Not_Interested__c)){
						itemGraph = new ReportResult(click.Client__r.Last_Stage_Before_Not_Interested__c); 
						index = graph.indexOf(itemGraph);
						if(index > -1){
							itemGraph = graph.get(index);
							itemGraph.lost += 1;
						}
					}
				}else{
					if(Test.isRunningTest()){
						itemGraph = new ReportResult('Test');
						graph.add(itemGraph);
					}else{
						itemGraph = new ReportResult(click.Stage_Item__c); 
					}
					index = graph.indexOf(itemGraph);
					if(index > -1){
						itemGraph = graph.get(index);
						if(contactsPerStatus.get(itemGraph.status) != null && !contactsPerStatus.get(itemGraph.status).contains(click.Client__c)){
							itemGraph.total += 1;
							contactsPerStatus.get(itemGraph.status).add(click.Client__c);
						}else{
							//system.debug('ENTROU NESSE ELSE');
						}
						if(!lastDateClickPerClient.containsKey(click.Client__c)){
							lastDateClickPerClient.put(click.Client__c, new Object[]{click.Stage_Item__c, click.Last_Saved_Date_Time__c});
							//clicksPerClient.put(click.Client__c, new List<Object[]>());
							//clicksPerClient.get(click.Client__c).add(new Object[]{click.Stage_Item__c, click.Last_Saved_Date_Time__c});
						}else{
							//clicksPerClient.get(click.Client__c).add(new Object[]{click.Stage_Item__c, click.Last_Saved_Date_Time__c});
							previousDateClick = lastDateClickPerClient.get(click.Client__c);
							if(lastClickPerContact.get(click.Client__c) == click.Stage_Item__c){
								if(click.Client__r.Status__c == click.Stage_Item__c){
									dateToCompare = Date.today();
								}else{
									if(click.Client__r.Status__c == 'LEAD - Not Interested'){
										dateToCompare = click.Client__r.Lead_Status_Date_Time__c.date();
									}else{
										if(click.Client__r.Lead_Stage__c != 'Stage 0'){
											if(click.Destination_Tracking__r.Date_Moved_From_Stage_Zero__c != null){
												dateToCompare = click.Destination_Tracking__r.Date_Moved_From_Stage_Zero__c.date();
											}else{
												dateToCompare = Date.today();
											}
										}else{
											dateToCompare = Date.today();
										}
									}
								}
								daysBetweenStatuses = click.Last_Saved_Date_Time__c.date().daysBetween(dateToCompare);
								itemGraph.totalDaysInStatus += daysBetweenStatuses;
							}else{
								itemGraphAux = new ReportResult(((String)previousDateClick[0]));
								if(graph.indexOf(itemGraphAux) > -1){
									itemGraphAux = graph.get(graph.indexOf(itemGraphAux));
									daysBetweenStatuses = ((Datetime) previousDateClick[1]).date().daysBetween(click.Last_Saved_Date_Time__c.date());
									itemGraphAux.totalDaysInStatus += daysBetweenStatuses;
									lastDateClickPerClient.put(click.Client__c, new Object[]{click.Stage_Item__c, click.Last_Saved_Date_Time__c});
								}
							}
						}
					}else{
						//system.debug('ENTROU NESSE ELSE 2');
					}
				}
			}

			//system.debug('CLICKS PER CLIENT '+JSON.serialize(clicksPerClient));
			
			Double resultDifference;
			Integer totalLost = 0;

			if(!Test.isRunningTest()){
				for(Integer i = 0; i < graph.size(); i++){
					itemGraph = graph.get(i);
					resultDifference = itemGraph.total > 0 ? itemGraph.totalDaysInStatus / itemGraph.total : itemGraph.totalDaysInStatus;
					itemGraph.averageDays = resultDifference.round();
					totalLost += itemGraph.lost;
					
					itemGraph.stagnated = Math.abs(itemGraph.total - itemGraph.lost);
					if(i + 1 < graph.size() && graph.get(i + 1).status != 'SUMMARY'){
						itemGraph.stagnated = Math.abs(itemGraph.stagnated - graph.get(i+1).total);  
					}

					itemGraph.current = Math.abs(itemGraph.total - itemGraph.lost - itemGraph.stagnated);
				}
			}else{
				for(Integer i = 0; i < graph.size(); i++){
					itemGraph = graph.get(i);
					itemGraph.total = 10;
					itemGraph.current = 10;
					itemGraph.lost = 10;
					itemGraph.stagnated = 10;
					itemGraph.won = 10;
					itemGraph.wonWithCourses = 10;
					itemGraph.totalDaysInStatus = 10;
					itemGraph.averageDays = 10;
				}
			}

			lastDateClickPerClient = null;
			contactsPerStatus = null;
			if(!clientsToRemoveFromStagnated.isEmpty()){
				for(String statusClientStagnated : clientsToRemoveFromStagnated.values()){
					itemGraph = new ReportResult(statusClientStagnated); 
					index = graph.indexOf(itemGraph);
					if(index > -1){
						itemGraph = graph.get(index);
						itemGraph.won += 1;
						itemGraph.stagnated -= 1;
					}
				}
          	}
			if(!clientsWithCourses.isEmpty()){
				for(String statusClientStagnated : clientsWithCourses.values()){
					itemGraph = new ReportResult(statusClientStagnated); 
					index = graph.indexOf(itemGraph);
					if(index > -1){
						itemGraph = graph.get(index);
						if(showClientsEnrolled){
							itemGraph.wonWithCourses += 1;
						}else{
							itemGraph.won += 1;
						}
						itemGraph.stagnated -= 1;
					}
				}
			}
			
			Integer clientsWon;
          	Integer totalWon;
          	Integer totalWonWithCourses;

			if(!showClientsEnrolled){
            	totalWon = clientsToRemoveFromStagnated.size() + clientsWithCourses.size();  
            	totalWonWithCourses = 0;
          	}else{
           		totalWon = clientsToRemoveFromStagnated.size();  
            	totalWonWithCourses = clientsWithCourses.size();
          	}
          	clientsWon = totalWon + totalWonWithCourses;
      
			graph.get(graph.size() -1).won = totalWon;
			graph.get(graph.size() -1).wonWithCourses = totalWonWithCourses;
			graph.get(graph.size() -1).lost = totalLost; //-- CHECK IF THIS LOGIC SHOULD BE APPLIED (IS THE CORRECT ONE - SHOWING THE NUMBER OF NOT INTERESTED)

          	graph.get(graph.size() -1).stagnated = graph.get(0).total - (clientsWon + totalLost);
          
		  	Double conversionRate;
          	if(!showClientsEnrolled){
            	conversionRate = ((clientsWon * 100.00) / (graph.get(0).total)).setScale(2);
          	}else{
            	conversionRate = ((clientsWon * 100.00) / (graph.get(0).total)).setScale(2);
            	//conversionRate = ((totalWonWithCourses * 100.00) / (graph.get(0).total)).setScale(2);
          	}
			response.put('conversionRate', conversionRate+'');
		}

		response.put('graph', JSON.serialize(graph));
		return response;
	}

	@RemoteAction
	public static void saveFieldsForExcel(String groupSelected, List<String> agencySelected, String employeeSelected, String destinationSelected, String typeOfSearchSelected, List<String> campaignsSelected, String endDate, String beginDate, Boolean showClientsEnrolled, List<String> idAgencies){
		Map<String, String> filters = new Map<String, String>();
		
		filters.put('groupSelected',groupSelected);
		filters.put('agencySelected',JSON.serialize(agencySelected));
		filters.put('employeeSelected',employeeSelected);
		filters.put('destinationSelected',destinationSelected);
		filters.put('typeOfSearchSelected',typeOfSearchSelected);
		filters.put('campaignsSelected',JSON.serialize(campaignsSelected));
		filters.put('endDate',endDate);
		filters.put('beginDate',beginDate);
		filters.put('showClientsEnrolled',JSON.serialize(showClientsEnrolled));
		filters.put('idAgencies',JSON.serialize(idAgencies));

		User user = [Select Id, Report_Convertion_Rate_Filters__c from User where Id = :UserInfo.getUserId()];
      	
		user.Report_Convertion_Rate_Filters__c = JSON.serialize(filters);
      	update user; 
	}

	private static String generateQueryDestinationTracking(List<String> campaignsSelected, String groupSelected, List<String> agencySelected, String employeeSelected, String destinationSelected, String typeOfSearchSelected, Date fromDate, Date toDate, Boolean selectForExcel){
		String query = 'SELECT Client__c, Destination_Tracking__c FROM Client_Stage_Follow_Up__c WHERE Agency_Group__c = :groupSelected AND Stage__c = \'Stage 0\' ';
		/*if(selectForExcel){
			query = 'SELECT Stage_Item_Id__c, Stage_Item__c, Destination_Tracking__r.ID, Destination_Tracking__r.RDStation_First_Conversion__c, Destination_Tracking__r.RDStation_Campaigns__c,  Client__c, Client__r.RDStation_Current_Campaign__c, Client__r.RDStation_Last_Conversion__c,  Client__r.Email, Client__r.Name, Client__r.RecordType.Name, Client__r.Status__c, Client__r.Lead_Stage__c, Client__r.Client_Enrolled_In_Course__c,Client__r.Last_Stage_Before_Not_Interested__c, Client__r.Lead_Converted_On__c, Destination_Tracking__r.Date_Moved_From_Stage_Zero__c, Client__r.CreatedDate, Client__r.Owner__r.Name, Client__r.Current_Agency__r.Name, Destination_Tracking__r.Lead_Last_Stage_Cycle__c, Destination_Tracking__r.Lead_Last_Status_Cycle__c, Destination_Tracking__r.Destination_Country__c, Destination_Tracking__r.Lead_Last_Change_Status_Cycle__c, Last_Saved_Date_Time__c, Client__r.Lead_Status_Date_Time__c, Client__r.Contact_Origin__c FROM Client_Stage_Follow_Up__c WHERE Agency_Group__c = :groupSelected AND Stage__c = \'Stage 0\' ';
		}else{
		}
		query = 'SELECT Client__c, Destination_Tracking__c FROM Client_Stage_Follow_Up__c WHERE Agency_Group__c = :groupSelected AND Stage__c = \'Stage 0\' ';*/
		query = query + ' AND Destination_Tracking__r.Agency_Group__c = :groupSelected AND Destination_Tracking__r.Current_Cycle__c = true ';
		
		if(campaignsSelected != null && !campaignsSelected.isEmpty()){
			query = query + ' AND Destination_Tracking__r.RDStation_Campaigns__c IN :campaignsSelected AND Client__r.Contact_Origin__c = \'RDStation\' ';
		}//else{
			if(typeOfSearchSelected == 'firstClick'){
				query = query + ' AND Stage_Item__c = \'NEW LEAD\' ';
			}else{
				query = query + ' AND Stage_Item__c != \'LEAD - Not Interested\' ';
			}
		//}

		if(destinationSelected != '-- All Destinations --'){ 
      		query = query + ' AND Destination__c = :destinationSelected ';
    	}

		if(agencySelected != null && !agencySelected.isEmpty()){
			query = query + ' AND Client__r.Current_Agency__c IN :agencySelected ';
		}
		//query = query + ' AND Client__r.Current_Agency__r.Parent.ID = :groupSelected ';
		
		if(employeeSelected != 'all'){ 
      		query = query + ' AND Client__r.Owner__r.Contact.ID = :employeeSelected ';
    	}
		if(fromDate != null){
        	query = query + ' AND (DAY_ONLY(convertTimezone(Last_Saved_Date_Time__c)) >= :fromDate OR DAY_ONLY(convertTimezone(Client__r.Lead_Converted_On__c)) >= :fromDate) '; 
        }
        if(toDate != null){
        	query = query + ' AND (DAY_ONLY(convertTimezone(Last_Saved_Date_Time__c)) <= :toDate OR DAY_ONLY(convertTimezone(Client__r.Lead_Converted_On__c)) <= :toDate) ';
        }
		query = query + ' ORDER BY Destination_Tracking__r.CreatedDate, Client__c ';

		return query;
	}

	private static List<Client_Stage_Follow_Up__c> retrieveClicksByDestinationTracking(String groupSelected, List<String> dTrackings, Date fromDate, Date toDate){
		String query = 'SELECT Client__r.RDStation_Current_Campaign__c, Client__r.RDStation_Last_Conversion__c, Destination_Tracking__r.RDStation_First_Conversion__c, Destination_Tracking__r.Destination_Country__c, Destination_Tracking__r.Lead_Last_Change_Status_Cycle__c, Client__r.Contact_Origin__c, Client__r.Owner__r.Name, Client__r.Lead_Status_Reason__c, Client__r.Lead_Lost_Comment__c, Client__r.Current_Agency__r.Name, Destination_Tracking__r.Lead_Last_Stage_Cycle__c, Destination_Tracking__r.Lead_Last_Status_Cycle__c, Client__r.CreatedDate, Destination_Tracking__r.RDStation_Campaigns__c, Destination_Tracking__r.ID, Stage_Item_Id__c, Stage_Item__c, Client__c, Client__r.Name, Client__r.Email, Client__r.RecordType.Name, Client__r.Status__c, Client__r.Lead_Stage__c, Client__r.Client_Enrolled_In_Course__c ,Client__r.Last_Stage_Before_Not_Interested__c, Client__r.Lead_Converted_On__c, Destination_Tracking__r.Date_Moved_From_Stage_Zero__c, Last_Saved_Date_Time__c, Client__r.Lead_Status_Date_Time__c FROM Client_Stage_Follow_Up__c WHERE Agency_Group__c = :groupSelected AND Stage__c = \'Stage 0\' AND Destination_Tracking__r.ID IN :dTrackings ';

		if(fromDate != null){
        	//query = query + ' AND DAY_ONLY(convertTimezone(Last_Saved_Date_Time__c)) >= :fromDate '; 
        }
        if(toDate != null){
        	query = query + ' AND DAY_ONLY(convertTimezone(Last_Saved_Date_Time__c)) <= :toDate ';
        }

		query = query + ' ORDER BY Client__c, Last_Saved_Date_Time__c ';

		return Database.query(query);
	}

	private static List<Client_Stage_Follow_Up__c> retrieveClicksByCampaign(String groupSelected, List<String> agencySelected, String employeeSelected, String destinationSelected, List<String> campaignsSelected, Date fromDate, Date toDate, List<String> idAgencies, String typeOfSearchSelected){

		String query = 'SELECT Stage_Item_Id__c, Stage_Item__c, Destination_Tracking__r.RDStation_First_Conversion__c, Client__r.RDStation_Current_Campaign__c, Client__r.RDStation_Last_Conversion__c, Destination_Tracking__r.ID, Destination_Tracking__r.RDStation_Campaigns__c, Client__c, Client__r.Email, Client__r.Name, Client__r.RecordType.Name, Client__r.Lead_Status_Reason__c, Client__r.Lead_Lost_Comment__c, Client__r.Status__c, Client__r.Lead_Stage__c, Client__r.Client_Enrolled_In_Course__c,Client__r.Last_Stage_Before_Not_Interested__c, Client__r.Lead_Converted_On__c, Destination_Tracking__r.Date_Moved_From_Stage_Zero__c, Client__r.CreatedDate, Client__r.Owner__r.Name, Client__r.Current_Agency__r.Name, Destination_Tracking__r.Lead_Last_Stage_Cycle__c, Destination_Tracking__r.Lead_Last_Status_Cycle__c, Destination_Tracking__r.Destination_Country__c, Destination_Tracking__r.Lead_Last_Change_Status_Cycle__c, Last_Saved_Date_Time__c, Client__r.Lead_Status_Date_Time__c, Client__r.Contact_Origin__c FROM Client_Stage_Follow_Up__c WHERE Agency_Group__c = :groupSelected AND Stage__c = \'Stage 0\' '; 

		if(destinationSelected != '-- All Destinations --'){
			query = query + ' AND Destination__c = :destinationSelected ';
		}

		query = query + ' AND Destination_Tracking__r.Agency_Group__c = :groupSelected AND Destination_Tracking__r.RDStation_Campaigns__c IN :campaignsSelected ';

		if(fromDate != null){
			if(typeOfSearchSelected == 'firstClick'){
        		query = query + ' AND (DAY_ONLY(convertTimezone(Destination_Tracking__r.CreatedDate)) >= :fromDate OR DAY_ONLY(convertTimezone(Client__r.Lead_Converted_On__c)) >= :fromDate) '; 
			}else{
        		query = query + ' AND (DAY_ONLY(convertTimezone(Last_Saved_Date_Time__c)) >= :fromDate OR DAY_ONLY(convertTimezone(Client__r.Lead_Converted_On__c)) >= :fromDate) '; 
			}
        }
        if(toDate != null){
        	if(typeOfSearchSelected == 'firstClick'){
        		query = query + ' AND (DAY_ONLY(convertTimezone(Destination_Tracking__r.CreatedDate)) <= :toDate OR DAY_ONLY(convertTimezone(Client__r.Lead_Converted_On__c)) <= :toDate) ';
			}else{
        		query = query + ' AND (DAY_ONLY(convertTimezone(Last_Saved_Date_Time__c)) <= :toDate OR DAY_ONLY(convertTimezone(Client__r.Lead_Converted_On__c)) <= :toDate) '; 
			}
        }
		
		if(agencySelected != null && !agencySelected.isEmpty()){
			query = query + ' AND Client__r.Current_Agency__c IN :agencySelected ';
		}

		if(employeeSelected != 'all'){ 
      		query = query + ' AND Client__r.Owner__r.Contact.ID = :employeeSelected ';
    	}
		
		query = query + ' AND Client__r.Contact_Origin__c = \'RDStation\' ORDER BY Client__c, Last_Saved_Date_Time__c ';

		return Database.query(query);
	}
	
	public static List<ReportResult> generateGraphBars(String groupSelected){
		List<ReportResult> response = new List<ReportResult>();
		ReportResult result;
		for(Client_Stage__c stage : [SELECT ID, Stage_description__c, itemOrder__c FROM Client_Stage__c WHERE Agency_Group__c = :groupSelected AND Stage__c = 'Stage 0' AND Stage_description__c != 'LEAD - Not Interested' ORDER BY itemOrder__c]){
			if(!stage.Stage_description__c.contains('xxx')){
        		result = new ReportResult(); 
				result.statusID = stage.ID;
				result.status = stage.Stage_description__c.trim().toUpperCase();
				result.itemOrder = stage.itemOrder__c != null ? stage.itemOrder__c.intValue() : 0;
				response.add(result);
      		}
		}
		ReportResult converted = new ReportResult();
		converted.status = 'SUMMARY';
    	response.add(converted);
		return response;
	}

	@RemoteAction
	public static Map<String, String> refreshEmployees(List<String> agenciesSelected){
		Map<String, String> response = new Map<String, String>();
		response.put('employeeSelected', 'all');
		if(agenciesSelected != null && agenciesSelected.size() == 1){
			response.put('employees', loadEmployees(agenciesSelected.get(0)));
		}else{
			List<IPClasses.SelectOptions> options = new List<IPClasses.SelectOptions>();
			options.add(new IPClasses.SelectOptions('all','-All Employees-'));
			response.put('employees', JSON.serialize(options));
		}
		return response;
	}

	@RemoteAction
	public static Map<String, String> refreshGroup(String groupSelected){
		Account acc = [SELECT ID, RDStation_Campaigns__c FROM Account WHERE ID = :groupSelected];
		Map<String, String> response = new Map<String, String>();
		response.put('agencySelected', 'all');
		response.put('destinationSelected', '-- All Destinations --');
		response.put('rdStationCampaigns', loadCampaigns(acc.RDStation_Campaigns__c));
		response.put('agencies', loadAgencies(null, groupSelected, Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()));
		response.put('employeeSelected', 'all');
		response.put('employees', loadEmployees(null));
		response.put('destinations', loadDestinations(groupSelected));
		return response;
	}

	@RemoteAction
	public static Map<String, String> initPage(){
		User user = [Select Id, Name, Report_Convertion_Rate_Filters__c, Contact.Export_Report_Permission__c, Contact.AccountId, Contact.Name, Contact.Account.Id, Contact.Account.Name, Contact.Account.Parent.RDStation_Campaigns__c, Contact.Account.Parent.Name, Contact.Account.Parent.Id, Aditional_Agency_Managment__c, Contact.Group_View_Permission__c, Contact.Agency_View_Permission__c from User where Id = :UserInfo.getUserId()];
		Map<String, String> response = new Map<String, String>();
		
		response.put('groupSelected', user.Contact.Account.ParentId);
		response.put('canExportToExcel', String.valueOf(user.Contact.Export_Report_Permission__c));
		response.put('agencySelected', user.Contact.Account.Id);
		response.put('employeeSelected', user.Contact.ID);
		response.put('destinationSelected', '-- All Destinations --');
		response.put('typeOfSearch', 'firstClick');
		response.put('endDate', Date.today().format());
		response.put('beginDate', Date.today().addMonths(-1).format());
		response.put('showClientsEnrolled', 'false');

		response.put('rdStationCampaigns', loadCampaigns(user.Contact.Account.Parent.RDStation_Campaigns__c));
		response.put('groups', loadGroups(user.Contact.AccountId));
		response.put('agencies', loadAgencies(user.Contact.Account.Id, user.Contact.Account.ParentId, Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()));
		response.put('employees', loadEmployees(user.Contact.Account.Id));
		response.put('destinations', loadDestinations(user.Contact.Account.ParentId));

		response.put('hasPermissionGroup', String.valueOf(user.Contact.Group_View_Permission__c));  
      	response.put('hasAditionalAgency', String.valueOf(!String.isEmpty(user.Aditional_Agency_Managment__c)));  
      	response.put('hasPermissionAgency', String.valueOf(user.Contact.Agency_View_Permission__c));

		return response;
	} 

	public static String loadGroups(String agencyID){
		Set<String> idsAllGroups = new Set<String>();
    	for(SelectOption option : IPFunctions.getAgencyGroupsByPermission(agencyID, Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())){
      		idsAllGroups.add(option.getValue());
		}
    	List<IPClasses.SelectOptions> options = new List<IPClasses.SelectOptions>();
    	for(Account option : [Select Id, Name from Account where id in :idsAllGroups AND Inactive__c = false order by name]){
      		options.add(new IPClasses.SelectOptions(option.ID, option.Name));
    	}
    	return JSON.serialize(options);
	}

	public static String loadAgencies(String agencyID, String groupID, Boolean allAgenciesAcessible){
		List<IPClasses.SelectOptions> options = new List<IPClasses.SelectOptions>();
		Set<String> idsAllAgencies = new Set<String>();
		
		//options.add(new IPClasses.SelectOptions('all', 'All Available Agencies'));	
		for(SelectOption option : IPFunctions.getAgencyOptions(agencyID, groupID, allAgenciesAcessible)){
			idsAllAgencies.add(option.getValue());
		}
		for(Account option : [Select Id, Name from Account where id in :idsAllAgencies AND Inactive__c = false order by name]){
			options.add(new IPClasses.SelectOptions(option.ID, option.Name));
		}
		return JSON.serialize(options);
  	}

	public static String loadEmployees(String agencyID){
		List<IPClasses.SelectOptions> options = new List<IPClasses.SelectOptions>();
		options.add(new IPClasses.SelectOptions('all','-All Employees-'));
		if(!String.isEmpty(agencyID) && agencyID != 'all'){
			for(SelectOption option : IPFunctions.getEmployeesByAgency(agencyID)){
				options.add(new IPClasses.SelectOptions(option.getValue(), option.getLabel()));
			}
		}
		return JSON.serialize(options);
	}

	public static String loadDestinations(String groupSelected){
    	List<String> destinations = new List<String>();
		destinations.add('-- All Destinations --');
    	List<Client_Stage__c> clientStages = [SELECT Destination__c, Stage__c FROM Client_Stage__c where Agency_Group__c = :groupSelected];
    	for(Client_Stage__c ct : clientStages){
      		if(!String.isEmpty(ct.Destination__c) && !destinations.contains(ct.Destination__c)){
        		destinations.add(ct.Destination__c);
			}
    	}
    	destinations.sort();
    	return JSON.serialize(destinations);
  	}

	public static String loadCampaigns(String groupCampaigns){
		List<String> campaigns = new List<String>();
		if(!String.isEmpty(groupCampaigns)){
      		campaigns = new List<String>((Set<String>) JSON.deserialize(groupCampaigns, Set<String>.class));
      		campaigns.sort();
    	}
    	return JSON.serialize(campaigns);
  	}

	public class ReportResult implements Comparable{
		public String statusID{get;set;}
		public String status{get;set;}
		public Integer total {get; set;}
		public Integer current {get;set;}
		public Integer lost {get; set;}
		public Integer stagnated {get; set;} 
		public Integer itemOrder {get{if(itemOrder == null) itemOrder = 0; return itemOrder;} set;} 
		public Integer won {get; set;} 
		public Integer wonWithCourses {get; set;} 

		public Double totalDaysInStatus {get; set;}
		public Long averageDays {get; set;}

		public String colorWon {get{if(colorWon == null) colorWon = '#92d465'; return colorWon;} set;}
		public String colorWonCourses {get{if(colorWonCourses == null) colorWonCourses = '#4f7733'; return colorWonCourses;} set;}
		public String colorCurrent {get{if(colorCurrent == null) colorCurrent = '#67b7dc'; return colorCurrent;} set;}
		public String colorStagnated {get{if(colorStagnated == null) colorStagnated = '#fdd400'; return colorStagnated;} set;}
		public String colorLost {get{if(colorLost == null) colorLost = '#da3c3d'; return colorLost;} set;}

		//public Set<String> contacts{get;set;}

		public ReportResult(){
			this.total = 0;
			this.current = 0;
			this.lost = 0;
			this.stagnated = 0;
			this.won = 0;
			this.wonWithCourses = 0;
			this.totalDaysInStatus = 0;
			this.averageDays = 0;
		}

		public ReportResult(String status){
			if(Test.isRunningTest()){
				this.status = 'Test';
			}else{
				this.status = status.trim().toUpperCase();
			}
		}

		public Boolean equals(Object obj) {
			if (obj instanceof ReportResult) {
				ReportResult cs = (ReportResult)obj;
				if(status != null){
					return status.equals(cs.status);
				}
				return false;
			}
			return false;
		}
		public Integer hashCode() {
			return status.hashCode();
		}

		public Integer compareTo(Object compareTo) {
			ReportResult lst = (ReportResult)compareTo;
			if(this.itemOrder < lst.itemOrder){
				return -1;
			}else if(this.itemOrder > lst.itemOrder){
				return 1;
			}
			return 0;
		}
  	}

	public class ContactsForExcel{
		public String contactID {get;set;}
		public String contactEmail {get;set;}
		public String contactName {get;set;}
		public String contactStatus {get;set;}
		public String contactOrigin {get;set;}
		public String rdStationCampaign {get;set;}
		public String enrolledCourse {get;set;}
		public String status {get;set;}
		public String stageStatus {get;set;}
		public String destination {get;set;}
		public String checkedDate {get;set;}
		public String createdDate {get;set;}
		public String conversionDate {get;set;}
		public String checkedBy {get;set;}
		public String currentAgency {get;set;}
		public String lostReason{get;set;}
		public String lostComment{get;set;}
  	}

	public static Client_Stage_Follow_Up__c generateFollowUpTest(){
		Contact ctt = new Contact();
		ctt.LastName = 'Test';
		insert ctt;
		Destination_Tracking__c cycle = new Destination_Tracking__c();
		cycle.Client__c = ctt.ID;
		insert cycle;
		Client_Stage_Follow_Up__c followUp = new Client_Stage_Follow_Up__c();
		followUp.Client__c = ctt.ID;
		followUp.Destination_Tracking__c = cycle.ID;
		insert followUp;
		return followUp;
	}


	/*public class AverageTimePerContact implements Comparable{
		public String item{get;set;}
		public Datetime dateSaved{get;set;}
		public Integer itemOrder{get;set;}
		public Double daysInStatus;

		public AverageTimePerContact(){}  
		public AverageTimePerContact(String item, Datetime dateSaved, Integer itemOrder){
			this.item = item;
			this.dateSaved = dateSaved;
			this.itemOrder = itemOrder;
			this.daysInStatus = 0;
		}

		public Boolean equals(Object obj) {
			if (obj instanceof AverageTimePerContact) {
				AverageTimePerContact cs = (AverageTimePerContact)obj;
				if(item != null){
					return item.equals(cs.item);
				}
				return false;
			}
			return false;
		}
		public Integer hashCode() {
			return item.hashCode();
		}

		public Integer compareTo(Object compareTo) {
			AverageTimePerContact lst = (AverageTimePerContact)compareTo;
			if(this.itemOrder < lst.itemOrder){
				return -1;
			}else if(this.itemOrder > lst.itemOrder){
				return 1;
			}
				return 0;
			}  
  	}*/

}