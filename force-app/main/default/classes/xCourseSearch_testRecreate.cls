/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class xCourseSearch_testRecreate {

    static testMethod void myUnitTest() {
        
        TestFactory TestFactory = new TestFactory();
		
		Account agency = TestFactory.createAgency();
		Quotation_Products_Services__c qps = new Quotation_Products_Services__c();
		qps.Agency__c = agency.id;
		qps.Category__c = 'Government Fee';
		qps.Product_Name__c = 'Student Visa - Additional Applicant Charge 18yrs and over';
		qps.Quantity__c = 1;
		qps.Price__c = 535;
		qps.Unit_description__c = 'Month';
		qps.Description__c = 'This fee applies to each dependant 18yrs and over in a Student Visa application.';
		qps.Currency__c = 'AUD';
		qps.Selected__c = true;
		insert qps;
		
		Contact employee = TestFactory.createEmployee(agency);
		
		Contact client = TestFactory.createClient(agency);
		
		Account school = testFactory.createSchool();
		Account campus = testFactory.createCampus(school, agency);
		Course__c course = testFactory.createCourse();
		Campus_Course__c cc = testFactory.createCampusCourse(campus, course);
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;
		insert p3;
		
		Course_Price__c cp = testFactory.createCoursePrice(cc, 'Latin America');
		Course_Intake_Date__c cid = testFactory.createCourseIntakeDate(cc);
		Course_Extra_Fee__c cef = testFactory.createCourseExtraFee(cc, 'Latin America', p);
		Course_Extra_Fee_Dependent__c cefd = testFactory.createCourseRelatedExtraFee(cef, p2);
		
		
	
		Deal__c d2 = new Deal__c();
		d2.Availability__c = 3;
		d2.Campus_Course__c = cc.id;
		d2.From__c = 1;		
		d2.Promotion_Type__c = 2;
		d2.From_Date__c = system.today();
		d2.To_Date__c = system.today().addDays(60);
		d2.Promotion_Weeks__c = 1;
		d2.Number_of_Free_Weeks__c = 1;
		d2.Nationality_Group__c = 'Latin America';
		d2.Promotion_Name__c = '1+1';
		d2.Promo_Price__c = 50;		
		insert d2;
		
		Start_Date_Range__c sdr = new Start_Date_Range__c();
		sdr.Promotion__c = d2.id;
		sdr.Campus__c = cc.campus__c;
		sdr.From_Date__c = system.today();
		sdr.To_Date__c = system.today().addDays(15);
		sdr.Number_of_Free_Weeks__c = 2;
		insert sdr;		
		
		
		
		Deal__c d3 = new Deal__c();
		d3.Availability__c = 3;
		d3.Campus_Course__c = cc.id;
		d3.From__c = 1;
		d3.Extra_Fee__c = cef.id;
		d3.Promotion_Type__c = 3;
		d3.From_Date__c = system.today();
		d3.To_Date__c = system.today().addDays(60);
		d3.Extra_Fee_Type__c = 'Cash';
		d3.Extra_Fee_Value__c = 50;
		d3.Nationality_Group__c = 'Latin America';
		insert d3;
		
		Deal__c d4 = new Deal__c();
		d4.Availability__c = 3;
		d4.Campus_Course__c = cc.id;
		d4.From__c = 1;		
		d4.Promotion_Type__c = 2;
		d4.Promotion_Name__c = '5+2';
		d4.Promo_Price__c = 50;		
		d4.From_Date__c = system.today();
		d4.To_Date__c = system.today().addDays(61);
		d4.Promotion_Weeks__c = 5;
		d4.Number_of_Free_Weeks__c = 2;
		d4.Nationality_Group__c = 'Published Price';
		insert d4;
		
		Quotation__c q = new Quotation__c();
		q.Client__c = client.id;
		q.Currency_Rates__c = 'BRL:1.0;USD:2.0;AUD:1.0';
		q.Expiry_date__c = system.today().addDays(7);
		q.isCombined__c = false;
		q.Agency_Products__c = qps.id+'::'+qps.Product_Name__c+'::'+qps.Category__c+'::'+qps.Currency__c+'::'+qps.Description__c+'::false::'+qps.Price__c+'::'+qps.Quantity__c+'::null::'+qps.Price__c*qps.Quantity__c+'::false';
		q.agency_instalments__c = 'Cartao de credito::6279.0::1883.700::4395.300::Visa?mastercard::10::1:4395.30!!2:2317.6895672979092635500!!3:1555.36599076847928685200!!4:1174.2655009289002671600!!5:945.80220185546863458600!!6:793.4357115838701884700!!7:687.2423542202885585100!!8:605.7040807856278203600!!9:541.58064498876260913900!!10:490.85083026284775151500;;Uma Parcela::6279.0::0.000::6279.000::asd::1::1:6279.00';
		q.Agency_Currency__c = 'BRL';
		q.Optional_Agency_Currency__c = 'USD';
		q.Comments__c = 'test quote';
		q.Language_ISO_Code__c = 'pt_BR';
		q.Disclaimer__c = 'some disclaimer';
		q.Agency__c = client.Accountid;
		insert q;
		
		Quotation_Detail__c qd1 = new Quotation_Detail__c();
		qd1.Quotation__c = q.id;
		qd1.Campus_Name__c = 'Campus Name';
		qd1.Campus_City__c = 'City Name';
		qd1.Campus_Country__c = 'Australia';
		qd1.Course_Name__c = 'Course Name';
		qd1.Campus_Currency__c = 'AUD';
		qd1.Course_Duration__c = 23;
		qd1.Course_Sold_in_Blocks__c  = true;
		qd1.School_Name__c = 'School 1';
		qd1.Course_Start_Date__c = system.today();
		qd1.Course_Timetable__c = 'timetable';
		qd1.Course_Unit_Type__c = 'Weeks';
		qd1.Course_Tuition_per_Unit__c = 200;
		qd1.Custom_Website__c = 'www.customcourse.com.test.au';
		qd1.Course_Finish_Date__c = system.today().addDays(15);
		qd1.Course_Total_Extra_Fees__c = 300;
		qd1.Course_Total_Value__c = 4500;
		qd1.Course_Tuition_Fee__c = 4200;
		qd1.Agency_Products__c = qps.id+'::'+qps.Product_Name__c+'::'+qps.Category__c+'::'+qps.Currency__c+'::'+qps.Description__c+'::false::'+qps.Price__c+'::'+qps.Quantity__c+'::null::'+qps.Price__c*qps.Quantity__c+'::false';
		qd1.Extra_Fees_Details__c = '0::a0Kg0000001TQKqEAO::Material Fee::1::Other::100.00::null::false::false::null;;1::a0Kg0000001TSW2EAO::Homestay - Single (Over 18yrs)::2::Accommodation::265.00::Weeks::true::true::null;;2::a0Kg0000001TQ8YEAW::Enrolment Fee - Student visa::1::Other::180.00::null::false::false::null;;0::null::sfdafdsa::1::Custom::123.0::null::false::false::null';
		insert qd1;
		
		Quotation__c q2 = new Quotation__c();
		q2.Client__c = client.id;
		q2.Currency_Rates__c = 'BRL:1.0;USD:2.0;AUD:1.0';
		q2.Expiry_date__c = system.today().addDays(7);
		q2.isCombined__c = true;
		q2.Agency_Products__c = qps.id+'::'+qps.Product_Name__c+'::'+qps.Category__c+'::'+qps.Currency__c+'::'+qps.Description__c+'::false::'+qps.Price__c+'::'+qps.Quantity__c+'::null::'+qps.Price__c*qps.Quantity__c+'::false';
		q2.agency_instalments__c = 'Cartao de credito::6279.0::1883.700::4395.300::Visa?mastercard::10::1:4395.30!!2:2317.6895672979092635500!!3:1555.36599076847928685200!!4:1174.2655009289002671600!!5:945.80220185546863458600!!6:793.4357115838701884700!!7:687.2423542202885585100!!8:605.7040807856278203600!!9:541.58064498876260913900!!10:490.85083026284775151500;;Uma Parcela::6279.0::0.000::6279.000::asd::1::1:6279.00';
		q2.Agency_Currency__c = 'BRL';
		q2.Optional_Agency_Currency__c = 'USD';
		q2.Comments__c = 'test quote';
		q2.Language_ISO_Code__c = 'pt_BR';
		q2.Disclaimer__c = 'some disclaimer';
		q2.Agency__c = client.Accountid;
		insert q2;
		
		Quotation_Detail__c qd = new Quotation_Detail__c();
		qd.Quotation__c = q2.id;
		qd.Holidays__c = 'lots of it';
		qd.Campus_Country__c = 'Australia';
		qd.Campus_City__c = 'Sydney';
		qd.Campus_Course__c = cc.id; 
		qd.Campus_Name__c = 'SEA Sydney Manly';
		qd.Course_Duration__c = 24;
		qd.Course_Finish_Date__c = system.today().addDays(24*7);
		qd.Course_Name__c = 'IELTS Preparation - Morning (20hrs/Week)';
		qd.Course_Number_Free_Weeks__c = 1;
		qd.Course_Tuition_per_Unit__c = 225;
		qd.Course_Order__c = 1;
		qd.Course_Payment_Date__c = system.today();
		qd.Course_Start_Date__c = system.today();
		qd.Course_Time_Table__c = 'Monday to Friday - 8:45am to 1:10pm<br/><br/>**Timetable subject to change without previous notice';
		qd.Course_Total_Value__c = 6483;
		qd.Course_Tuition_Fee__c = 5400;
		qd.Course_Unit_Type__c = 'Week';
		qd.Campus_Currency__c = 'AUD';
		qd.Extra_Fees_Details__c = '0::a0Kg0000001TQKqEAO::Material Fee::1::Other::100.00::null::false::false::null;;1::a0Kg0000001TSW2EAO::Homestay - Single (Over 18yrs)::2::Accommodation::265.00::Weeks::true::true::null;;2::a0Kg0000001TQ8YEAW::Enrolment Fee - Student visa::1::Other::180.00::null::false::false::null;;0::null::sfdafdsa::1::Custom::123.0::null::false::false::null';
		qd.Related_Fees_Details__c = '0::a2fg0000001lfFsAAI::Accommodation Placement Fee::1::200.00::a0Kg0000001TSW2EAO;;1::a2fg0000001lfFuAAI::Christmas week surcharge::1::40.00::a0Kg0000001TSW2EAO';
		qd.Promotion_Details__c = '0::cxnvcmhgjgkjgk::Custom::90.0::null::null::null:extraFee::null';
		qd.Course_Instalments__c = '1:31/07/2014:1080.00:1083.00;2:31/08/2014:1080.00:0;3:30/09/2014:1080.00:0;4:30/10/2014:1080.00:0;5:30/11/2014:1080.00:0.00';
		qd.Course_First_Instalment__c = 2163;
		qd.Agency_Products__c = qps.id+'::'+qps.Product_Name__c+'::'+qps.Category__c+'::'+qps.Currency__c+'::'+qps.Description__c+'::false::'+qps.Price__c+'::'+qps.Quantity__c+'::null::'+qps.Price__c*qps.Quantity__c+'::false';
		qd.agency_instalments__c = 'Cartao de credito::6279.0::1883.700::4395.300::Visa?mastercard::10::1:4395.30!!2:2317.6895672979092635500!!3:1555.36599076847928685200!!4:1174.2655009289002671600!!5:945.80220185546863458600!!6:793.4357115838701884700!!7:687.2423542202885585100!!8:605.7040807856278203600!!9:541.58064498876260913900!!10:490.85083026284775151500;;Uma Parcela::6279.0::0.000::6279.000::asd::1::1:6279.00';
		insert qd;
		
		
		Test.startTest();

		Apexpages.currentPage().getParameters().put('id', employee.id);
		
		contact_controller ccontroller = new contact_controller();
		ccontroller.ctid = client.id;
		ApexPages.currentPage().getParameters().put('recreateQuote', q.id);
		ApexPages.currentPage().getParameters().put('nationality', client.Nationality__c);
		ccontroller.recreateQuote();
		
		ApexPages.currentPage().getParameters().put('recreateQuote', q2.id);
		ApexPages.currentPage().getParameters().put('nationality', client.Nationality__c);
		ccontroller.recreateQuote();
		
		
		Test.stopTest();
        
    }
}