public class global_class{

	public List<SelectOption> getLanguages() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('sq','Albanian'));
		options.add(new SelectOption('ar','Arabic'));
		options.add(new SelectOption('hy','Armenian'));
		options.add(new SelectOption('eu','Basque'));
		options.add(new SelectOption('bs','Bosnian'));
		options.add(new SelectOption('bg','Bulgarian'));
		options.add(new SelectOption('zh_CN','Chinese (Simplified)'));
		options.add(new SelectOption('zh_TW','Chinese (Traditional)'));
		options.add(new SelectOption('hr','Croatian'));
		options.add(new SelectOption('cs','Czech'));
		options.add(new SelectOption('da','Danish'));
		options.add(new SelectOption('nl_NL','Dutch'));
		options.add(new SelectOption('en_US','English'));
		options.add(new SelectOption('en_AU','English (Australia)'));
		options.add(new SelectOption('en_CA','English (Canada)'));
		options.add(new SelectOption('en_IN','English (India)'));
		options.add(new SelectOption('en_MY','English (Malaysia)'));
		options.add(new SelectOption('en_PH','English (Philippines)'));
		options.add(new SelectOption('en_GB','English (UK)'));
		options.add(new SelectOption('et','Estonian'));
		options.add(new SelectOption('fi','Finnish'));
		options.add(new SelectOption('fr','French'));
		options.add(new SelectOption('fr_CA','French (Canada)'));
		options.add(new SelectOption('ka','Georgian'));
		options.add(new SelectOption('de','German'));
		options.add(new SelectOption('el','Greek'));
		options.add(new SelectOption('iw','Hebrew'));
		options.add(new SelectOption('hi','Hindi'));
		options.add(new SelectOption('hu','Hungarian'));
		options.add(new SelectOption('is','Icelandic'));
		options.add(new SelectOption('in','Indonesian'));
		options.add(new SelectOption('ga','Irish'));
		options.add(new SelectOption('it','Italian'));
		options.add(new SelectOption('ja','Japanese'));
		options.add(new SelectOption('ko','Korean'));
		options.add(new SelectOption('lv','Latvian'));
		options.add(new SelectOption('lt','Lithuanian'));
		options.add(new SelectOption('lb','Luxembourgish'));
		options.add(new SelectOption('mk','Macedonian'));
		options.add(new SelectOption('ms','Malay'));
		options.add(new SelectOption('mt','Maltese'));
		options.add(new SelectOption('ro_MD','Moldovan'));
		options.add(new SelectOption('sh_ME','Montenegrin'));
		options.add(new SelectOption('no','Norwegian'));
		options.add(new SelectOption('pl','Polish'));
		options.add(new SelectOption('pt_BR','Portuguese (Brazil)'));
		options.add(new SelectOption('pt_PT','Portuguese (European)'));
		options.add(new SelectOption('ro','Romanian'));
		options.add(new SelectOption('rm','Romansh'));
		options.add(new SelectOption('ru','Russian'));
		options.add(new SelectOption('sr','Serbian (Cyrillic)'));
		options.add(new SelectOption('sh','Serbian (Latin)'));
		options.add(new SelectOption('sk','Slovak'));
		options.add(new SelectOption('sl','Slovenian'));
		options.add(new SelectOption('es','Spanish'));
		options.add(new SelectOption('es_MX','Spanish (Mexico)'));
		options.add(new SelectOption('sv','Swedish'));
		options.add(new SelectOption('tl','Tagalog'));
		options.add(new SelectOption('th','Thai'));
		options.add(new SelectOption('tr','Turkish'));
		options.add(new SelectOption('uk','Ukrainian'));
		options.add(new SelectOption('ur','Urdu'));
		options.add(new SelectOption('vi','Vietnamese'));
		options.add(new SelectOption('cy','Welsh'));
		return options;
	}
	
	
	public String getClientLanguage(String country){
		string Spanish = 'Chile, Peru, Venezuela, Colombia, Spain';

        if(country == 'Brazil')
            return 'pt_BR';
        if(Spanish.contains(country)) 
            return 'es';
        else return 'en_US'; 
	}
	
	public Map<String, string> langCodeTranslation(){
		Map<String, string> lct = new Map<String, string>();
		lct.put('sq','Albanian');
		lct.put('ar','Arabic');
		lct.put('hy','Armenian');
		lct.put('eu','Basque');
		lct.put('bs','Bosnian');
		lct.put('bg','Bulgarian');
		lct.put('zh_CN','Chinese (Simplified)');
		lct.put('zh_TW','Chinese (Traditional)');
		lct.put('hr','Croatian');
		lct.put('cs','Czech');
		lct.put('da','Danish');
		lct.put('nl_NL','Dutch');
		lct.put('en_US','English');
		lct.put('en_AU','English (Australia)');
		lct.put('en_CA','English (Canada)');
		lct.put('en_IN','English (India)');
		lct.put('en_MY','English (Malaysia)');
		lct.put('en_PH','English (Philippines)');
		lct.put('en_GB','English (UK)');
		lct.put('et','Estonian');
		lct.put('fi','Finnish');
		lct.put('fr','French');
		lct.put('fr_CA','French (Canada)');
		lct.put('ka','Georgian');
		lct.put('de','German');
		lct.put('el','Greek');
		lct.put('iw','Hebrew');
		lct.put('hi','Hindi');
		lct.put('hu','Hungarian');
		lct.put('is','Icelandic');
		lct.put('in','Indonesian');
		lct.put('ga','Irish');
		lct.put('it','Italian');
		lct.put('ja','Japanese');
		lct.put('ko','Korean');
		lct.put('lv','Latvian');
		lct.put('lt','Lithuanian');
		lct.put('lb','Luxembourgish');
		lct.put('mk','Macedonian');
		lct.put('ms','Malay');
		lct.put('mt','Maltese');
		lct.put('ro_MD','Moldovan');
		lct.put('sh_ME','Montenegrin');
		lct.put('no','Norwegian');
		lct.put('pl','Polish');
		lct.put('pt_BR','Portuguese (Brazil)');
		lct.put('pt_PT','Portuguese (European)');
		lct.put('ro','Romanian');
		lct.put('rm','Romansh');
		lct.put('ru','Russian');
		lct.put('sr','Serbian (Cyrillic)');
		lct.put('sh','Serbian (Latin)');
		lct.put('sk','Slovak');
		lct.put('sl','Slovenian');
		lct.put('es','Spanish');
		lct.put('es_MX','Spanish (Mexico)');
		lct.put('sv','Swedish');
		lct.put('tl','Tagalog');
		lct.put('th','Thai');
		lct.put('tr','Turkish');
		lct.put('uk','Ukrainian');
		lct.put('ur','Urdu');
		lct.put('vi','Vietnamese');
		lct.put('cy','Welsh');
		return lct;
	}
	
	 	
	
	
}