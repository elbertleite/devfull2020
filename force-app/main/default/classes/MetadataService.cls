public class MetadataService {
	
	public class SessionHeader_element {
        public String sessionId;
        private String[] sessionId_type_info = new String[]{'sessionId','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'sessionId'};
    }
    
    public class DebuggingInfo_element {
        public String debugLog;
        private String[] debugLog_type_info = new String[]{'debugLog','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'debugLog'};
    }
    
    public class DebuggingHeader_element {
        public MetadataService.LogInfo[] categories;
        public String debugLevel;
        private String[] categories_type_info = new String[]{'categories','http://soap.sforce.com/2006/04/metadata',null,'0','-1','false'};
        private String[] debugLevel_type_info = new String[]{'debugLevel','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'categories','debugLevel'};
    }
    
    public class CallOptions_element {
        public String client;
        private String[] client_type_info = new String[]{'client','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'client'};
    }
    
    public class AllOrNoneHeader_element {
        public Boolean allOrNone;
        private String[] allOrNone_type_info = new String[]{'allOrNone','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'allOrNone'};
    }
    
        
    public class LogInfo {
        public String category;
        public String level;
        private String[] category_type_info = new String[]{'category','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] level_type_info = new String[]{'level','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'category','level'};
    }
    
    public class readMetadata_element {
        public String type_x;
        public String[] fullNames;
        private String[] type_x_type_info = new String[]{'type','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] fullNames_type_info = new String[]{'fullNames','http://soap.sforce.com/2006/04/metadata',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'type_x','fullNames'};
    }
    
    public interface IReadResult {
        MetadataService.Metadata[] getRecords();
    }
    public interface IReadResponseElement {
        IReadResult getResult();
    }
    
    public virtual class Metadata {
        public String fullName;
    }
    
    
     public class ObjectNameCaseValue {
        public String article;
        public String caseType;
        public Boolean plural;
        public String possessive;
        public String value;
        private String[] article_type_info = new String[]{'article','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] caseType_type_info = new String[]{'caseType','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] plural_type_info = new String[]{'plural','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] possessive_type_info = new String[]{'possessive','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] value_type_info = new String[]{'value','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'article','caseType','plural','possessive','value'};
    }
    public class LookupFilterTranslation {
        public String errorMessage;
        public String informationalMessage;
        private String[] errorMessage_type_info = new String[]{'errorMessage','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] informationalMessage_type_info = new String[]{'informationalMessage','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'errorMessage','informationalMessage'};
    }
    public class LayoutSectionTranslation {
        public String label;
        public String section;
        private String[] label_type_info = new String[]{'label','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] section_type_info = new String[]{'section','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'label','section'};
    }
    public class LayoutTranslation {
        public String layout;
        public String layoutType;
        public MetadataService.LayoutSectionTranslation[] sections;
        private String[] layout_type_info = new String[]{'layout','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] layoutType_type_info = new String[]{'layoutType','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] sections_type_info = new String[]{'sections','http://soap.sforce.com/2006/04/metadata',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'layout','layoutType','sections'};
    }
    public class QuickActionTranslation {
        public String label;
        public String name;
        private String[] label_type_info = new String[]{'label','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] name_type_info = new String[]{'name','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'label','name'};
    }
    public class ReportTypeColumnTranslation {
        public String label;
        public String name;
        private String[] label_type_info = new String[]{'label','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] name_type_info = new String[]{'name','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'label','name'};
    }
    public class ReportTypeSectionTranslation {
        public MetadataService.ReportTypeColumnTranslation[] columns;
        public String label;
        public String name;
        private String[] columns_type_info = new String[]{'columns','http://soap.sforce.com/2006/04/metadata',null,'0','-1','false'};
        private String[] label_type_info = new String[]{'label','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] name_type_info = new String[]{'name','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'columns','label','name'};
    }
    public class ReportTypeTranslation {
        public String description;
        public String label;
        public String name;
        public MetadataService.ReportTypeSectionTranslation[] sections;
        private String[] description_type_info = new String[]{'description','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] label_type_info = new String[]{'label','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] name_type_info = new String[]{'name','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] sections_type_info = new String[]{'sections','http://soap.sforce.com/2006/04/metadata',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'description','label','name','sections'};
    }
    public class SharingReasonTranslation {
        public String label;
        public String name;
        private String[] label_type_info = new String[]{'label','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] name_type_info = new String[]{'name','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'label','name'};
    }
    public class StandardFieldTranslation {
        public String label;
        public String name;
        private String[] label_type_info = new String[]{'label','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] name_type_info = new String[]{'name','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'label','name'};
    }
    public class ValidationRuleTranslation {
        public String errorMessage;
        public String name;
        private String[] errorMessage_type_info = new String[]{'errorMessage','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] name_type_info = new String[]{'name','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'errorMessage','name'};
    }
    public class WebLinkTranslation {
        public String label;
        public String name;
        private String[] label_type_info = new String[]{'label','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] name_type_info = new String[]{'name','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'label','name'};
    }
    public class WorkflowTaskTranslation {
        public String description;
        public String name;
        public String subject;
        private String[] description_type_info = new String[]{'description','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] name_type_info = new String[]{'name','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] subject_type_info = new String[]{'subject','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'description','name','subject'};
    }
    public class RecordTypeTranslation {
        public String label;
        public String name;
        private String[] label_type_info = new String[]{'label','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] name_type_info = new String[]{'name','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'label','name'};
    }
    
    public class ReadCustomObjectTranslationResult implements IReadResult {
        public MetadataService.CustomObjectTranslation[] records;
        public MetadataService.Metadata[] getRecords() { return records; }
        private String[] records_type_info = new String[]{'records','http://soap.sforce.com/2006/04/metadata',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'records'};
    }
    
    public class readCustomObjectTranslationResponse_element implements IReadResponseElement {
        public MetadataService.ReadCustomObjectTranslationResult result;
        public IReadResult getResult() { return result; }
        private String[] result_type_info = new String[]{'result','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'result'};
    }
    
    public class CustomObjectTranslation extends Metadata {
        public String type = 'CustomObjectTranslation';
        public String fullName;
        private String[] fullName_type_info = new String[]{'fullName','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        public MetadataService.ObjectNameCaseValue[] caseValues;
        public MetadataService.CustomFieldTranslation[] fields;
        public String gender;
        public MetadataService.LayoutTranslation[] layouts;
        public String nameFieldLabel;
        public MetadataService.QuickActionTranslation[] quickActions;
        public MetadataService.RecordTypeTranslation[] recordTypes;
        public MetadataService.SharingReasonTranslation[] sharingReasons;
        public MetadataService.StandardFieldTranslation[] standardFields;
        public String startsWith;
        public MetadataService.ValidationRuleTranslation[] validationRules;
        public MetadataService.WebLinkTranslation[] webLinks;
        public MetadataService.WorkflowTaskTranslation[] workflowTasks;
        private String[] caseValues_type_info = new String[]{'caseValues','http://soap.sforce.com/2006/04/metadata',null,'0','-1','false'};
        private String[] fields_type_info = new String[]{'fields','http://soap.sforce.com/2006/04/metadata',null,'0','-1','false'};
        private String[] gender_type_info = new String[]{'gender','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] layouts_type_info = new String[]{'layouts','http://soap.sforce.com/2006/04/metadata',null,'0','-1','false'};
        private String[] nameFieldLabel_type_info = new String[]{'nameFieldLabel','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] quickActions_type_info = new String[]{'quickActions','http://soap.sforce.com/2006/04/metadata',null,'0','-1','false'};
        private String[] recordTypes_type_info = new String[]{'recordTypes','http://soap.sforce.com/2006/04/metadata',null,'0','-1','false'};
        private String[] sharingReasons_type_info = new String[]{'sharingReasons','http://soap.sforce.com/2006/04/metadata',null,'0','-1','false'};
        private String[] standardFields_type_info = new String[]{'standardFields','http://soap.sforce.com/2006/04/metadata',null,'0','-1','false'};
        private String[] startsWith_type_info = new String[]{'startsWith','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] validationRules_type_info = new String[]{'validationRules','http://soap.sforce.com/2006/04/metadata',null,'0','-1','false'};
        private String[] webLinks_type_info = new String[]{'webLinks','http://soap.sforce.com/2006/04/metadata',null,'0','-1','false'};
        private String[] workflowTasks_type_info = new String[]{'workflowTasks','http://soap.sforce.com/2006/04/metadata',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] type_att_info = new String[]{'xsi:type'};
        private String[] field_order_type_info = new String[]{'fullName', 'caseValues','fields','gender','layouts','nameFieldLabel','quickActions','recordTypes','sharingReasons','standardFields','startsWith','validationRules','webLinks','workflowTasks'};
    }
    
    public class CustomFieldTranslation {
        public MetadataService.ObjectNameCaseValue[] caseValues;
        public String gender;
        public String help;
        public String label;
        public MetadataService.LookupFilterTranslation lookupFilter;
        public String name;
        public MetadataService.PicklistValueTranslation[] picklistValues;
        public String relationshipLabel;
        public String startsWith;
        private String[] caseValues_type_info = new String[]{'caseValues','http://soap.sforce.com/2006/04/metadata',null,'0','-1','false'};
        private String[] gender_type_info = new String[]{'gender','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] help_type_info = new String[]{'help','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] label_type_info = new String[]{'label','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] lookupFilter_type_info = new String[]{'lookupFilter','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] name_type_info = new String[]{'name','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] picklistValues_type_info = new String[]{'picklistValues','http://soap.sforce.com/2006/04/metadata',null,'0','-1','false'};
        private String[] relationshipLabel_type_info = new String[]{'relationshipLabel','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] startsWith_type_info = new String[]{'startsWith','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'caseValues','gender','help','label','lookupFilter','name','picklistValues','relationshipLabel','startsWith'};
    }
	
	public class PicklistValueTranslation {
        public String masterLabel;
        public String translation;
        private String[] masterLabel_type_info = new String[]{'masterLabel','http://soap.sforce.com/2006/04/metadata',null,'1','1','false'};
        private String[] translation_type_info = new String[]{'translation','http://soap.sforce.com/2006/04/metadata',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata','true','false'};
        private String[] field_order_type_info = new String[]{'masterLabel','translation'};
    }
    
    
    
    
    
    
    public class MetadataPort {
        public String endpoint_x = URL.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/m/35.0';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        public MetadataService.SessionHeader_element SessionHeader;
        public MetadataService.DebuggingInfo_element DebuggingInfo;
        public MetadataService.DebuggingHeader_element DebuggingHeader;
        public MetadataService.CallOptions_element CallOptions;
        public MetadataService.AllOrNoneHeader_element AllOrNoneHeader;
        private String SessionHeader_hns = 'SessionHeader=http://soap.sforce.com/2006/04/metadata';
        private String DebuggingInfo_hns = 'DebuggingInfo=http://soap.sforce.com/2006/04/metadata';
        private String DebuggingHeader_hns = 'DebuggingHeader=http://soap.sforce.com/2006/04/metadata';
        private String CallOptions_hns = 'CallOptions=http://soap.sforce.com/2006/04/metadata';
        private String AllOrNoneHeader_hns = 'AllOrNoneHeader=http://soap.sforce.com/2006/04/metadata';
        private String[] ns_map_type_info = new String[]{'http://soap.sforce.com/2006/04/metadata', 'MetadataService'};
        
        public MetadataService.IReadResult readMetadata(String type_x, String[] fullNames) {
            MetadataService.readMetadata_element request_x = new MetadataService.readMetadata_element();
            request_x.type_x = type_x;
            request_x.fullNames = fullNames;
            MetadataService.IReadResponseElement response_x;
            Map<String, MetadataService.IReadResponseElement> response_map_x = new Map<String, MetadataService.IReadResponseElement>();
            response_map_x.put('response_x', response_x);
            
            if(Test.isRunningTest()){
            	
            	PicklistValueTranslation pvt = new PicklistValueTranslation();
		    	pvt.masterLabel = 'Language';
		    	pvt.translation = 'Lingua';
		    	
		    	CustomFieldTranslation cft = new CustomFieldTranslation();
		    	cft.name = 'Course_Category__c';
		    	cft.picklistValues = new PicklistValueTranslation[]{pvt};
		    	
		    	CustomObjectTranslation cot = new CustomObjectTranslation();
		    	cot.fields = new CustomFieldTranslation[]{cft};
		    	
		    	ReadCustomObjectTranslationResult rcotr = new ReadCustomObjectTranslationResult();
		    	rcotr.records = new CustomObjectTranslation[]{cot};
		    	
		    	readCustomObjectTranslationResponse_element readElement = new readCustomObjectTranslationResponse_element();
		    	readElement.result = rcotr;
            	
            	return readElement.getResult();
            } else {
	            WebServiceCallout.invoke(this,request_x,response_map_x,new String[]{endpoint_x,'','http://soap.sforce.com/2006/04/metadata','readMetadata','http://soap.sforce.com/2006/04/metadata','readMetadataResponse','MetadataService.read' + type_x + 'Response_element'});
	            response_x = response_map_x.get('response_x');
	            system.debug('@@@ response_x: ' + response_x);
            }
            
            return response_x.getResult();
        }
    }
    
}