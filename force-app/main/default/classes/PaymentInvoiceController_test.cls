/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PaymentInvoiceController_test {

    static testMethod void myUnitTest() {
         
       TestFactory tf = new TestFactory();
       
       Account school = tf.createSchool();
       Account agency = tf.createAgency();
       Contact emp = tf.createEmployee(agency);
       User portalUser = tf.createPortalUser(emp);
       Account campus = tf.createCampus(school, agency);
       Course__c course = tf.createCourse();
       Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
       
       Test.startTest();
       system.runAs(portalUser){
       	   Contact client = tf.createLead(agency, emp);
	       client_course__c booking = tf.createBooking(client);
	       client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       
	       cc.Enroled_by_User__c = UserInfo.getUserId();
	       cc.Enroled_by_Agency__c = agency.id;
	       update cc;
	       
	       
	       List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
	       
	       client_product_service__c product =  tf.createCourseProduct(booking, agency);
		   
		   ApexPages.currentPage().getParameters().put('cs', instalments[3].id);
	       ApexPages.currentPage().getParameters().put('pd', product.id);
	       ApexPages.currentPage().getParameters().put('ct', client.id);
	       
	       
	       client_course_create_invoice invoicePay = new client_course_create_invoice();
	       invoicePay.createInvoice();
	       invoicePay.newPayDetails.Value__c = 1600;
		   invoicePay.newPayDetails.Payment_Type__c = 'Creditcard';
		   invoicePay.addPaymentValue();
		   invoicePay.savePayment();
	       
	       String invoiceId = invoicePay.invoice.id;
	       Invoice__c updateInvoice = new Invoice__c(id = invoiceId, Instalments__c = instalments[3].id +';');
		   update updateInvoice;
		   
	       ApexPages.currentPage().getParameters().put('type', 'client');
	       ApexPages.currentPage().getParameters().put('inst', instalments[0].id);
	       PaymentInvoiceController testClass = new PaymentInvoiceController();
	       ApexPages.currentPage().getParameters().remove('inst');
	       ApexPages.currentPage().getParameters().put('prod', product.id);
	       testClass = new PaymentInvoiceController();
	       ApexPages.currentPage().getParameters().remove('prod');
	       ApexPages.currentPage().getParameters().put('inv',invoiceId);
	       testClass = new PaymentInvoiceController();
	       
	       ApexPages.currentPage().getParameters().remove('type');
	       ApexPages.currentPage().getParameters().put('type', 'payment');
	       ApexPages.currentPage().getParameters().put('id', instalments[0].id);
	       testClass = new PaymentInvoiceController();
	       
	       ApexPages.currentPage().getParameters().remove('type');
	       ApexPages.currentPage().getParameters().remove('id');
	       ApexPages.currentPage().getParameters().put('id', invoiceId);
	       ApexPages.currentPage().getParameters().put('type', 'pdsInvoice');
	       ApexPages.currentPage().getParameters().put('dueDate', '16/02/2016');
	       ApexPages.currentPage().getParameters().put('emailBody', 'Test');
	       testClass = new PaymentInvoiceController();
	       
       }
       Test.stopTest();
        
    }
}