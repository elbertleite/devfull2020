public without sharing class enrolmentRedirect extends EnrolmentManager {

	public PageReference redir(){
		String courseid = ApexPages.currentPage().getParameters().get('id');
		if(courseid != null){
			String status = [select Enrolment_Status__c from Client_Course__c where id = :courseid].Enrolment_Status__c;
			PageReference pageRef = new PageReference('/enrolments?type='+getTypeByStatus(status));
            return pageRef;
		} else {
			PageReference pageRef = new PageReference('/enrolments');
            return pageRef;
		}

	}
}