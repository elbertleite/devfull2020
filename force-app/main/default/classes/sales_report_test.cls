/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class sales_report_test {

    static testMethod void myUnitTest() {
        TestFactory tf = new TestFactory();
       
       	Account school = tf.createSchool();
       	
       	Account agencyGroup = tf.createAgencyGroup();
       	
       	Account agency = tf.createAgency();
       	Account agency2 = tf.createAgency();
       	
      	Contact emp = tf.createEmployee(agency);
      	
       	User portalUser = tf.createPortalUser(emp);
       	
       	Account campus = tf.createCampus(school, agency);
       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
        Department__c department = tf.createDepartment(agency);
        
		Contact client = tf.createLead(agency, emp);
		
		client_course__c booking = tf.createBooking(client);
       	client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
		client_course__c cc2 =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
			
		List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
		List<client_course_instalment__c> instalments2 =  tf.createClientCourseInstalments(cc2);
		
       	cc.Enrolment_Date__c = System.today();
       	cc.Enroled_by_Agency__c = agency2.id;
       	cc.Commission_Type__c = 0;
		update cc;
		
		cc2.Enrolment_Date__c = System.today();
       	cc2.Enroled_by_Agency__c = agency.id;
       	cc2.Commission_Type__c = 0;
		update cc2;
       
		instalments[0].Received_By_Agency__c = agency.id;
		instalments[0].Received_Date__c = System.today();
		instalments[0].Commission_Confirmed_On__c = System.today();
		update instalments;
		
		instalments2[0].Received_By_Agency__c = agency.id;
		instalments2[0].Received_Date__c = System.today();
		instalments2[0].isPDS__c = true;
		instalments2[0].Commission_Confirmed_On__c = System.today();
		
		instalments2[1].Received_By_Agency__c = agency.id;
		instalments2[1].Received_Date__c = System.today();
		instalments2[1].isPFS__c = true;
		instalments2[1].Commission_Confirmed_On__c = System.today();
		
		update instalments2;
		
       	Test.startTest();
       	system.runAs(portalUser){
			
			sales_report testClass = new sales_report();
			
			
			list<SelectOption> Periods = testClass.getPeriods();
			list<SelectOption> commissionType  = testClass.getcommissionType();
			//list<SelectOption> dateCondition = testClass.getdateCondition();
			list<SelectOption> agencyGroupOptions = testClass.agencyGroupOptions;
			list<SelectOption> ClientType = testClass.getClientType();
			
			testClass.SelectedPeriod = 'LAST_WEEK';
			//testClass.SelectedDateCondition = 'unconfirmed';
			testClass.searchCoursesExcel();
			testClass.generateExcel();
			
			testClass.SelectedPeriod = 'THIS_MONTH';
			//testClass.SelectedDateCondition = 'confirmed';
			testClass.searchCourses();

			testClass.changeAgencyGroup();
			testClass.changeAgency();
       	}
        
    }
}