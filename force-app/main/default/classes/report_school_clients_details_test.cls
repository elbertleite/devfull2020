@isTest
private class report_school_clients_details_test {
	
	public static Account schoolGroup {get;set;}
	public static Account school {get;set;}
	public static Account agency {get;set;}
	public static Contact emp {get;set;}
	public static Contact client {get;set;}
	public static User portalUser {get{
		if (null == portalUser) {
			portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
		}
		return portalUser;
	}set;}
	public static Account campus {get;set;}
	public static Course__c course {get;set;}
	public static Campus_Course__c campusCourse {get;set;}
	public static client_course__c clientCourseBooking {get;set;}
	public static client_course__c clientCourse {get;set;}
	public static client_course__c clientCourse2 {get;set;}


	@testSetup static void setup() {
		TestFactory tf = new TestFactory();

		schoolGroup = tf.createSchoolGroup();
		school = tf.createSchool();
		school.parentId = schoolGroup.id;
		agency = tf.createAgency();
		emp = tf.createEmployee(agency);
		portalUser = tf.createPortalUser(emp);
		campus = tf.createCampus(school, agency);
		course = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);
		system.runAs(portalUser){
			client = tf.createClient(agency);
			clientCourseBooking = tf.createBooking(client);
			clientCourse = tf.createClientCourse(client, school, campus, course, campusCourse, clientCourseBooking);
			clientCourse.Enrolment_Date__c = system.today();
			update clientCourse;
			tf.createClientCourseFees(clientCourse, false);
			List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(clientCourse);

			instalments[0].Received_By_Agency__c = agency.id;
			instalments[0].Received_Date__c =  system.today();
			instalments[0].Paid_To_School_On__c =  system.today();
			instalments[0].isPFS__c = true;

			instalments[1].Received_By_Agency__c = agency.id;
			instalments[1].Received_Date__c =  system.today();
			instalments[1].isPCS__c = true;

			instalments[2].Received_By_Agency__c = agency.id;
			instalments[2].Received_Date__c =  system.today();
			instalments[2].isPDS__c = true;
			instalments[2].PDS_Confirmed__c = true;
			
			instalments[3].Received_By_Agency__c = agency.id;
			instalments[3].Received_Date__c =  system.today();

			update instalments;

		}
	}

    static testMethod void reportSchoolClientsSales() {
        Test.startTest();
		system.runAs(portalUser){
			report_school_clients_details toTest = new report_school_clients_details();
			
			List<SelectOption> countries = toTest.getCountries();
			toTest.selectedCountry = 'Australia';
			totest.refreshSchool();
			
			List<SelectOption> schoolGroup = toTest.getschoolGroup();
			totest.selectedSchoolGroup = schoolGroup[0].getValue();
			totest.refreshSchool();

			List<SelectOption> schools = toTest.getSchools();
			totest.selectedSchool = schools[1].getValue();
			totest.refreshCampus();
			
			List<SelectOption> periods = toTest.getPeriods();
			toTest.SelectedPeriod = 'LAST_WEEK';
			
			List<SelectOption> campuses = toTest.getCampuses();
			toTest.selectedCampus = campuses[1].getValue();
			
			List<SelectOption> courseCategory= toTest.getcourseCategory();
			toTest.selectedCourseCategory = courseCategory[1].getValue();
			
			List<SelectOption> courseType = toTest.getcourseType();
			
			
			List<SelectOption> agencyGroups = toTest.getAgencyGroups();
			toTest.selectedGroup = agencyGroups[1].getValue();
			toTest.refreshAgencies();
			
			
			List<SelectOption> agencies = toTest.getAgencies();
			toTest.selectedAgency = agencies[1].getValue();
			
			/*toTest.freeweeks = true;
			toTest.install = true;
			toTest.extrafee = true;
			toTest.tax = true;
			toTest.avgcomm = true;
			toTest.deal = true;
			toTest.keepfee = true;
			toTest.paidcredit = true;
			toTest.scholarship = true;
			toTest.ticket = true;
			toTest.paidschool = true;
			toTest.finalcomm = true;*/
			
			List<SelectOption> orderBy = toTest.getorderBy();
			
			List<SelectOption> searchBy = toTest.getsearchBy();
			toTest.selectSearchBy = 'startdate';
			
		
			
			toTest.sales();
			
			
			
			toTest.SelectedPeriod = 'THIS_WEEK';
			toTest.selectedOrderBy = 'agency';
			toTest.selectSearchBy = 'paydate';
			toTest.sales();
			
			
			toTest.SelectedPeriod = 'THIS_MONTH';
			toTest.selectSearchBy = 'startdate';
			toTest.sales();
			
			toTest.SelectedPeriod = 'LAST_MONTH';
			toTest.selectSearchBy = 'startdate';
			toTest.sales();
			
			string periodReport = toTest.periodReport;
			string SelectedSchoolName = toTest.getSelectedSchoolName();
			
			toTest.selectedCountry = null;
			toTest.selectedSchool = null;
			toTest.sales();
			
			//toTest.generatePDF();
			
			//toTest.generateExcel();

		}
		Test.stopTest();
        
    }
    
}