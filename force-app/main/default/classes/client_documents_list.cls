public with sharing class client_documents_list {
  
	Contact contact;
	private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}
	private User currentUser {get{if(currentUser==null) currentUser = ff.currentUser; return currentUser;}set;}
	public String bucketName {get{if(bucketName == null) bucketName = IPFunctions.s3bucket; return bucketName;}set;}
	public String gbLink {get{if(gbLink == null) gbLink = currentUser.Contact.Account.Global_Link__c; return gbLink;}set;}
  
	private set<String> hideButtonsFor {get;set;}
  
	public String typeAction {get;set;}
	public boolean errorAddDoc {get{if(errorAddDoc == null) errorAddDoc = false; return errorAddDoc;}set;}
	public list<Client_Document__c> toCreateDocument {get;set;}
	public map<String, list<ClientDocument>> listDocuments {get;set;}
	public map<String, CoursesDocuments> clientCourseDocuments {get;set;}
	public map<String, CoursesDocuments> schoolReceipts {get;set;}
	public map<String, integer> catTotals {get;set;}
	public Boolean allowDelete {get{if(allowDelete == null) allowDelete = true; return allowDelete;}set;}
  
	//C O N S T R U C T O R
	public client_documents_list(ApexPages.StandardController controller){
	  contact = [Select Id, remoteContactId__c from Contact where id = :controller.getId()];
  
	  if(currentUser.Contact.Account.Global_Link__r.hideDelete_CategoryType__c!=null){
		hideButtonsFor = new set<String>(currentUser.Contact.Account.Global_Link__r.hideDelete_CategoryType__c.split(';'));
	  }
  
	  categoriesMenu();
  
	  typeAction = 'listDocs';
  
	  if(ApexPages.currentPage().getParameters().get('cid') == null){
		listDocuments();
		findIntallmentsReceipt();
		findCancelRequests();
	  }
	  else
		editDocument();
  
	  if(currentUser.Contact.Account.ParentId != null)
		docRestriction = ga.retrieveDocRestrictions('Select Restrict_Document_By_Category__c, Restrict_Document_By_User__c from account where id = \''+currentUser.Contact.Account.ParentId+'\' ');
	  else docRestriction = new account();
  
	  if(docRestriction.Restrict_Document_By_Category__c != null){
		docCategory = new set<string>(docRestriction.Restrict_Document_By_Category__c.split(';'));
		groupRestrictedUsers.add(currentUser.Contact.AccountId);
	  }
	  else docCategory = new set<string>(); 
		
	  if(docRestriction.Restrict_Document_By_User__c != null){
		groupRestrictedUsers.addAll(docRestriction.Restrict_Document_By_User__c.split(';'));
	  }
	  system.debug('clientCourseDocuments===>' + clientCourseDocuments);
	}
  
	public client_documents_list(){
	  String id = ApexPages.currentPage().getParameters().get('id');
	  contact = [Select Id, remoteContactId__c from Contact where id = :id];
  
	  if(currentUser.Contact.Account.Global_Link__r.hideDelete_CategoryType__c!=null){
		hideButtonsFor = new set<String>(currentUser.Contact.Account.Global_Link__r.hideDelete_CategoryType__c.split(';'));
	  }
  
	  categoriesMenu();
  
	  typeAction = 'listDocs';
  
	  if(ApexPages.currentPage().getParameters().get('cid') == null){
		listDocuments();
		findIntallmentsReceipt();
		findCancelRequests();
	  }
	  else
		editDocument();
  
	  if(currentUser.Contact.Account.ParentId != null)
		docRestriction = ga.retrieveDocRestrictions('Select Restrict_Document_By_Category__c, Restrict_Document_By_User__c from account where id = \''+currentUser.Contact.Account.ParentId+'\' ');
	  else docRestriction = new account();
  
	  if(docRestriction.Restrict_Document_By_Category__c != null){
		docCategory = new set<string>(docRestriction.Restrict_Document_By_Category__c.split(';'));
		groupRestrictedUsers.add(currentUser.Contact.AccountId);
	  }
	  else docCategory = new set<string>();
		
	  if(docRestriction.Restrict_Document_By_User__c != null){
		groupRestrictedUsers.addAll(docRestriction.Restrict_Document_By_User__c.split(';'));
	  }
	  system.debug('clientCourseDocuments===>' + clientCourseDocuments);
	}
  
	private account docRestriction;
	private set<string> docCategory;
  
	public set<string> groupRestrictedUsers{get{if(groupRestrictedUsers == null) groupRestrictedUsers = new set<string>(); return groupRestrictedUsers;} set;}
  
	public map<string, listOptions> mapOptions{get{if(mapOptions == null) mapOptions = new map<string, listOptions>(); return mapOptions;} set;}
	// C R E A T E     D O C U M E N T
	public void createDocument(){
	  system.debug('selectedCategory==>' + selectedCategory);
  
	  mapOptions = new map<string, listOptions>();
	  listOptions lo;
	  if(selectedCategory == null || selectedCategory.trim() == ''){
		errorAddDoc = true;
	  }
	  else{
		errorAddDoc = false;
		toCreateDocument = new list<Client_Document__c>();
		string restrictToUsers;
		string restrictToAgency;
		string accessRestriction;
		System.debug('==>docCategory: '+docCategory);
		for(String category : selectedCategory.split(',')){
		  restrictToUsers = null;
		  restrictToAgency = null;
		  accessRestriction = null;
		  list<String> catSplit = category.split(';#;');
		  System.debug('==>catSplit[0]: '+catSplit[1]);
		  if(docCategory.contains(catSplit[1])){
			restrictToUsers = docRestriction.Restrict_Document_By_User__c;
			// if(docRestriction.Restrict_Document_By_User__c != null)
			//   selectedUsers = docRestriction.Restrict_Document_By_User__c.split(';');
			// selectedAgencies.add(currentUser.Contact.AccountId);
			restrictToAgency = currentUser.Contact.AccountId;
			accessRestriction = restrictToUsers +';' +currentUser.Contact.AccountId;
		  }
		  System.debug('==>restrictToUsers: '+restrictToUsers);
		  toCreateDocument.add(new Client_Document__c(Client__c = contact.Id, Document_Category__c = catSplit[0], Document_Type__c = catSplit[1], Access_Restricted_to__c = accessRestriction, User_Access_Restriction__c = restrictToUsers, Agency_Access_Restriction__c = restrictToAgency, Created_By_Agency__c = currentUser.Contact.AccountId));
		}//end for
  
		insert toCreateDocument;
		for(Client_Document__c cd : toCreateDocument){
		  lo = new listOptions();
		  if(cd.User_Access_Restriction__c != null)
			lo.selectedUsers = docRestriction.Restrict_Document_By_User__c.split(';');
		  if(cd.Agency_Access_Restriction__c != null)
			lo.selectedAgencies.add(cd.Agency_Access_Restriction__c);
		  mapOptions.put(cd.id, lo);
		}
		createFlightTicket();      
		typeAction = 'createDocs';
  
		selectedCategory = null;
	  }
	}
	
	//S A V E   A L L   D O C U M E N T S
	public void saveNCloseDocuments(){
	  saveDocuments();
  
	  listDocuments();
	  findIntallmentsReceipt();    
	}
	public void saveDocuments(){
	  system.debug('SAVE DOCUMENTS CALLED');
	  list<Client_Flight_Details__c> allFlights = new list<Client_Flight_Details__c>();
  
	  for(Client_Document__c cd : toCreateDocument){
		if(cd.Document_Type__c == 'Flight Ticket')
		  allFlights.addAll(mapDocFlights.get(cd.id));
		cd.isSelected__c = false; 
  
		if((cd.Agency_Access_Restriction__c == null && mapOptions.get(cd.id).selectedAgencies != null && mapOptions.get(cd.id).selectedAgencies.size() > 0) || (cd.User_Access_Restriction__c == null && mapOptions.get(cd.id).selectedUsers != null && mapOptions.get(cd.id).selectedUsers.size() > 0)){
		  mapOptions.get(cd.id).selectedAgencies.add(cd.Created_By_Agency__c);
		}
		
		cd.Agency_Access_Restriction__c = String.join(mapOptions.get(cd.id).selectedAgencies, ';');
		cd.User_Access_Restriction__c = String.join(mapOptions.get(cd.id).selectedUsers, ';');
	  }
  
  
	  list<String> docsId = new list<String>();
  
	  for(Client_Document__c cd : toCreateDocument)
		docsId.add(cd.id);
  
	  map<String, Client_Document__c> docsPreview = new map<String,Client_Document__c>([Select Id, Preview_link__c from Client_Document__c Where id IN :docsId]); //Double check preview link
	  
	  for(Client_Document__c cd : toCreateDocument)
		cd.preview_link__c = docsPreview.get(cd.id).Preview_link__c;
		
  
	  system.debug('DOCUMENT '+toCreateDocument);
	  update toCreateDocument;
	  if(allFlights.size()>0)
		upsert allFlights;
  
	}
  
	//L I S T   D O C U M E N T S
	public list<String> courseOrder {get;set;}
	public void listDocuments(){
	  typeAction = 'listDocs';
	  selectedCategory = null;
  
	  list<Id> allFlights = new list<Id>();
  
	  listDocuments = new map<String, list<ClientDocument>>();
  
	  clientCourseDocuments = new map<String, CoursesDocuments>();
	  catTotals = new map<String, integer>();
	  catTotals.put('ClientCoursesProducts', 0); 
	  
	  map<id, set<id>> mapPayments = new map<id, set<id>>();
	  courseOrder = new list<String>();
  
	  String sql = 'SELECT Agency_Access_Restriction__c, Access_Restricted_to__c, User_Access_Restriction__c, Created_By_Agency__c, Account_Name__c, Account_Number__c, Bank__c, Bank_Country__c, Branch_Address__c, Branch_Name__c, BSB__c, Client__c, Client_Course__c, Client_Course__r.Course_Name__c, Client_Course__r.School_Name__c, Client_course__r.Enrolment_status__c, Comment__c, Country_of_Issue__c, Custom_ID__c, Date_granted__c, Date_of_Issue__c, Document_Category__c, Document_Number__c, Document_Type__c, eTicket_Number__c, Expiry_Date__c, Fax__c, IBAN__c, Immigration_Office__c, Invoice__c, isSelected__c, Issuing_Authority__c, No_Further_Stay__c, Notes__c, Phone__c, preview_link__c, remoteId__c, Student_Applying_in__c, Swift_Code__c, TRN__c, Visa_conditions__c,Visa_country_applying_for__c, Visa_number__c, Visa_subclass__c, Visa_type__c, Visa_Date_Applied__c, Visa_Immigration_Officer__c, Visa_Status__c, createdBy.name, createdDate, client_course_instalment__c, client_course_instalment__r.Client_Course__c, Client_course__r.isCancelled__c, Client_course__r.Enrolment_Date__c FROM Client_Document__c ';
	  sql += ' WHERE Client__c = \''+ contact.id + '\' ';
	  sql += ' and (Agency_Access_Restriction__c = null or Agency_Access_Restriction__c includes ( \''+ currentUser.Contact.AccountId + '\' ) or User_Access_Restriction__c includes ( \''+ currentUser.Id + '\' )) ';
	  sql += ' and Document_Type__c != \'School Installment Receipt\' '; //order by Client_Course__r.School_Name__c, Client_Course__r.Course_Name__c
  
  
	  //Contact Documents
	  for(Client_Document__c cd :Database.query(sql)){
  
		list<fileDocument> allUrlDocs = new list<fileDocument>();
  
		if(cd.preview_link__c != NULL && cd.preview_link__c != '')
		  for(String dc : cd.preview_link__c.split(FileUpload.SEPARATOR_FILE)){
			list<String> d = dc.split(FileUpload.SEPARATOR_URL);
			if(d.size() > 3)
				allUrlDocs.add(new fileDocument(d[1], d[0], d[2], d[3]));
			else if(d.size() > 2)
				allUrlDocs.add(new fileDocument(d[1], d[0], d[2], null));
			else allUrlDocs.add(new fileDocument(d[1], d[0], null, null));
		  }
  
		addDocument(cd, allUrlDocs);
  
		if(cd.Client_Course__c != null || cd.client_course_instalment__c != null){
		  
		  String courseId = cd.client_course_instalment__c != null ? cd.client_course_instalment__r.Client_Course__c : cd.Client_Course__c;
		  addCourseDocument(courseId, cd, null, allUrlDocs);
		  
		}
  
		if(cd.Document_Type__c == 'Flight Ticket')
		  allFlights.add(cd.id);
  
	  }//end for
  
  
	  // Get Flight Details
	  if(allFlights.size()>0){
		sql = 'Select Id, Airline_Reference_Number__c, Arrival_Airline_Company__c, Arrival_Airport__c, Arrival_Country__c, Arrival_Date__c, Arrival_Flight_Number__c, Arrival_Hour__c, Arrival_Minute__c, Arrival_Time__c, Client_Document__c, Departure_Airline_Company__c, Departure_Airport__c, Departure_Country__c, Departure_Date__c, Departure_Flight_Number__c, Departure_Hour__c, Departure_Minute__c, Departure_Time__c, Custom_ID__c FROM Client_Flight_Details__c C WHERE Client_Document__c in :allFlights order by createdDate ';
  
		map<Id, list<Client_Flight_Details__c>> mapFlight = new map<Id, list<Client_Flight_Details__c>>();
		
		for(Client_Flight_Details__c cfc : Database.query(sql))
		  if(!mapFlight.containsKey(cfc.Client_Document__c))
			mapFlight.put(cfc.Client_Document__c, new list<Client_Flight_Details__c>{cfc});
		  else
			mapFlight.get(cfc.Client_Document__c).add(cfc);
  
		for(ClientDocument cd : listDocuments.get('Travel'))
		  if(cd.document.Document_Type__c == 'Flight Ticket' && mapFlight.get(cd.document.id) != NULL)
			cd.flightDetails = mapFlight.get(cd.document.id);
	  }
  
  
	  //Migrated Clients from IP
	  if(contact.remoteContactId__c!=null){
		list<IPFunctions.ContactDocument> oldIPFiles = IPFunctions.getDocumentFiles(contact.id, 0);
		
		if(oldIPFiles != null)
		  for(IPFunctions.ContactDocument cdfile : oldIPFiles){
  
			if(!listDocuments.containsKey(cdFile.docCategory)){
			  listDocuments.put(cdFile.docCategory, new list<ClientDocument>{new ClientDocument(cdfile)});
			  catTotals.put(cdFile.docCategory, 1); 
			}
			else{
			  listDocuments.get(cdFile.docCategory).add(new ClientDocument(cdfile));
			  catTotals.put(cdFile.docCategory, catTotals.get(cdFile.docCategory) + 1); 
			}
			
		  }//end for
	  }
	}
  
	//F I N D     C A N C E L   D O C U M E N T S (Cancel Request Flow)
	public list<cancelReq> cancelRequests {get;set;}
	public void findCancelRequests(){
	  cancelRequests = new list<cancelReq>();
  
	  for(client_course__c cc : [SELECT previewLink__c, Client__c, Cancel_Course_Status__c, Course_Cancellation_Service__c, createdDate, Cancel_Request_Ids__c, (Select Id, Client__c, Course_Name__c, School_Name__c from client_courses__r) FROM client_course__c WHERE Client__c = :contact.id and isCancelRequest__c = true order by createdDate desc]){
		cancelRequests.add(new cancelReq(cc, IPFunctions.loadPreviewLink(cc.previewLink__c)));
	  }
	}
  
	// F I N D     S C H O O L   R E C E I P T S
	 public integer totalReceipts {get;set;}
	 public void findIntallmentsReceipt(){
  
		schoolReceipts = new map<String, CoursesDocuments>();
	  catTotals.put('SchoolReceipts', 0); 
  
		  list<client_course_instalment__c> result = [Select Client_Document__r.Id, Client_Document__r.preview_link__c, Id, School_Invoice_Number__c, Number__c, Split_Number__c, client_course__c,
		 Due_Date__c, isFirstPayment__c, Required_for_COE__c, School_Payment_Invoice__c, School_Payment_Invoice__r.School_Payment_Number__c, isPFS__c,
					  client_course__r.Name,
					  client_course__r.Course_Name__c,
					  client_course__r.Campus_Name__c,
					  client_course__r.School_Name__c,
					  client_course__r.Start_Date__c,
					  Client_course__r.Enrolment_status__c,
					  Client_course__r.isCancelled__c,
					  Client_course__r.Enrolment_Date__c,
					  client_course__r.End_Date__c FROM client_course_instalment__c where client_course__r.Client__c = :contact.id and Paid_To_School_On__c != null order by client_course__r.School_Name__c, client_course__r.Course_Name__c, Client_course__r.Enrolment_Date__c, Number__c, Split_Number__c];
		 totalReceipts = result.size();
  
		 try{
		   for(client_course_instalment__c cci : result){
  
		  list<fileDocument> allUrlDocs = new list<fileDocument>();
  
		  if(cci.Client_Document__r.preview_link__c != NULL && cci.Client_Document__r.preview_link__c != '')
			for(String dc : cci.Client_Document__r.preview_link__c.split(FileUpload.SEPARATOR_FILE)){
			  list<String> d = dc.split(FileUpload.SEPARATOR_URL);
			  if(d.size() > 3)
				allUrlDocs.add(new fileDocument(d[1], d[0], d[2], d[3]));
			  else if(d.size() > 2)
				allUrlDocs.add(new fileDocument(d[1], d[0], d[2], null));
			  else allUrlDocs.add(new fileDocument(d[1], d[0], null, null));
			}
  
		   addCourseDocument(cci.client_course__c, null, cci, allUrlDocs);      
  
			 }//end for
  
		 }catch(Exception e){
		   system.debug('Error==' + e);
		   system.debug('Error==' + e.getLineNUmber());
  
		 }
	 }
  
	private void addDocument(Client_Document__c cd, list<fileDocument> allUrlDocs){
	  List<SelectOption> ag = ga.Agencies; //just to initialize the mapAgencies
	  List<SelectOption> us = ga.AgencyUsers; //same
	  if(!listDocuments.containsKey(cd.Document_Category__c)){
  
		ClientDocument ndc = new ClientDocument(cd, allUrlDocs);
		
		if(ndc.document.Agency_Access_Restriction__c != null){
		  list<string> agName = new list<string>(); 
		  for(string st:ndc.document.Agency_Access_Restriction__c.split(';'))
			if(ga.mapAgencies.containsKey(st))
			  agName.add(ga.mapAgencies.get(st));
		  ndc.agencyRestriction = string.join(agName, ', ');
  
		}
		System.debug('==> ga.mapUsers: '+ga.mapUsers);
		System.debug('==> ndc.document.User_Access_Restriction__c: '+ndc.document.User_Access_Restriction__c);
		if(ndc.document.User_Access_Restriction__c != null){
		  list<string> usName = new list<string>(); 
		  for(string st:ndc.document.User_Access_Restriction__c.split(';')){
			System.debug('==> st: '+st);
			System.debug('==> ga.mapUsers.containsKey(st): '+ga.mapUsers.containsKey(st));
			if(ga.mapUsers.containsKey(st))
			  usName.add(ga.mapUsers.get(st));
		  }
		  ndc.userRestriction = string.join(usName, ', ');
  
		}
		listDocuments.put(cd.Document_Category__c, new list<ClientDocument>{ndc});
  
		//listDocuments.put(cd.Document_Category__c, new list<ClientDocument>{new ClientDocument(cd, allUrlDocs)});
		catTotals.put(cd.Document_Category__c, 1); 
	  }
	  else{
		ClientDocument ndc = new ClientDocument(cd, allUrlDocs);
		
		if(ndc.document.Agency_Access_Restriction__c != null){
		  list<string> agName = new list<string>();
		  for(string st:ndc.document.Agency_Access_Restriction__c.split(';'))
			if(ga.mapAgencies.containsKey(st))
			  agName.add(ga.mapAgencies.get(st));
		  ndc.agencyRestriction = string.join(agName, ', ');
  
		}
		if(ndc.document.User_Access_Restriction__c != null){
		  list<string> usName = new list<string>();
		  for(string st:ndc.document.User_Access_Restriction__c.split(';'))
			if(ga.mapUsers.containsKey(st))
			  usName.add(ga.mapUsers.get(st));
		  ndc.userRestriction = string.join(usName, ', ');
  
		}
		listDocuments.get(cd.Document_Category__c).add(ndc);
  
		// listDocuments.get(cd.Document_Category__c).add(new ClientDocument(cd, allUrlDocs));
		catTotals.put(cd.Document_Category__c, catTotals.get(cd.Document_Category__c) + 1); 
	  }
	}
  
	private void addCourseDocument(String courseId, Client_Document__c cd, client_course_instalment__c inst, list<fileDocument> allUrlDocs){
  
	  if(cd != null){
		
		
		if(!clientCourseDocuments.containsKey(courseId)){
		  courseOrder.add(courseId);
		  clientCourseDocuments.put(courseId, new CoursesDocuments(cd.Client_Course__r.School_Name__c, courseId, cd.Client_Course__r.Course_Name__c, cd.Client_course__r.Enrolment_status__c, cd.client_course__r.isCancelled__c, cd.client_course__r.Enrolment_Date__c, new list<ClientDocument>{new ClientDocument(cd, allUrlDocs)}));
		  catTotals.put('cd-'+courseId, 1); 
		  catTotals.put('ClientCoursesProducts', catTotals.get('ClientCoursesProducts') + 1); 
		}
		else{
		  clientCourseDocuments.get(courseId).addDocument(new ClientDocument(cd, allUrlDocs));
		  catTotals.put('cd-'+courseId, catTotals.get('cd-'+courseId) + 1); 
		  catTotals.put('ClientCoursesProducts', catTotals.get('ClientCoursesProducts') + 1); 
		}
  
	  }
	  else{
		if(!schoolReceipts.containsKey(courseId)){
		  schoolReceipts.put(courseId, new CoursesDocuments(inst.Client_Course__r.School_Name__c, courseId, inst.Client_Course__r.Course_Name__c, inst.Client_course__r.Enrolment_status__c,inst.client_course__r.isCancelled__c, inst.client_course__r.Enrolment_Date__c, new list<ClientDocument>{new ClientDocument(inst, allUrlDocs)}));
		  catTotals.put('sch-'+courseId, 1); 
		  catTotals.put('SchoolReceipts', catTotals.get('SchoolReceipts') + 1); 
		}
		else{
		  schoolReceipts.get(courseId).addDocument(new ClientDocument(inst, allUrlDocs));
		  catTotals.put('sch-'+courseId, catTotals.get('sch-'+courseId) + 1); 
		  catTotals.put('SchoolReceipts', catTotals.get('SchoolReceipts') + 1); 
		}
	  }
	}
  
	//E D I T    D O C U M E N T 
	public void editDocument(){
	  String docId = ApexPages.currentPage().getParameters().get('cid');
	  allowDelete = true;
  
	  toCreateDocument = [SELECT Agency_Access_Restriction__c, User_Access_Restriction__c, Access_Restricted_to__c, Created_By_Agency__c, Account_Name__c, Account_Number__c, Bank__c, Bank_Country__c, Branch_Address__c, Branch_Name__c, BSB__c, Client__c, client_course_instalment__c, Client_Course__c, Comment__c, Country_of_Issue__c, Custom_ID__c, Date_granted__c, Date_of_Issue__c, Document_Category__c, Document_Number__c, Document_Type__c, eTicket_Number__c, Expiry_Date__c, Fax__c, IBAN__c, Immigration_Office__c, Invoice__c, isSelected__c, Issuing_Authority__c, No_Further_Stay__c, Notes__c, Phone__c, preview_link__c, remoteId__c, Student_Applying_in__c, Swift_Code__c, TRN__c, Visa_conditions__c,Visa_country_applying_for__c, Visa_number__c, Visa_subclass__c, Visa_type__c, Visa_Date_Applied__c, Visa_Immigration_Officer__c, Visa_Status__c, createdBy.name, createdDate, (Select Id, Airline_Reference_Number__c, Arrival_Airline_Company__c, Arrival_Airport__c, Arrival_Country__c, Arrival_Date__c, Arrival_Flight_Number__c, Arrival_Hour__c, Arrival_Minute__c, Arrival_Time__c, Client_Document__c, Departure_Airline_Company__c, Departure_Airport__c, Departure_Country__c, Departure_Date__c, Departure_Flight_Number__c, Departure_Hour__c, Departure_Minute__c, Departure_Time__c, Custom_ID__c FROM Client_Flight_Details__r) FROM Client_Document__c WHERE id = :docId AND Client__c = :contact.id];
  
	  mapOptions = new map<string, listOptions>();
	  listOptions lo = new listOptions();
	  if(toCreateDocument[0].User_Access_Restriction__c != null)
		lo.selectedUsers = toCreateDocument[0].User_Access_Restriction__c.split(';');
	  if(toCreateDocument[0].Agency_Access_Restriction__c != null)
		lo.selectedAgencies.add(toCreateDocument[0].Agency_Access_Restriction__c);
	  mapOptions.put(toCreateDocument[0].id, lo);
  
	  if(toCreateDocument[0].Agency_Access_Restriction__c != null)
		mapOptions.get(toCreateDocument[0].id).selectedAgencies = toCreateDocument[0].Agency_Access_Restriction__c.split(';');
	  if(toCreateDocument[0].User_Access_Restriction__c != null)
		mapOptions.get(toCreateDocument[0].id).selectedUsers = toCreateDocument[0].User_Access_Restriction__c.split(';');
  
	  if(toCreateDocument[0].Access_Restricted_to__c != null)
		groupRestrictedUsers.addAll(toCreateDocument[0].Access_Restricted_to__c.split(';'));
  
	  for(Client_Document__c cd : toCreateDocument){
		if(cd.Document_Type__c == 'Flight Ticket')
		  mapDocFlights.put(cd.id, cd.Client_Flight_Details__r);
  
  
		if(hideButtonsFor!= NULL && hideButtonsFor.contains(cd.Document_Type__c))
		  allowDelete = false;
  
	  }//end for
  
	  typeAction = 'editDoc';
  
	  ApexPages.currentPage().getParameters().remove('cid');
	}
  
  
	
	// A D D     F L I G H T   D E T A I L S
	public map<String,list<Client_Flight_Details__c>> mapDocFlights {get{if(mapDocFlights == null) mapDocFlights = new map<String, list<Client_Flight_Details__c>>(); return mapDocFlights;}set;}
	private String flightDocId {get;set;}
	public void createFlightTicket(){
	  // String docID = ApexPages.currentPage().getParameters().get('docID');
	  for(Client_Document__c document : toCreateDocument){
		if(document.Document_Type__c == 'Flight Ticket' && !mapDocFlights.containsKey(document.id)){
		  flightDocId = document.id;
		  addNewFlight();
		  break;
		}
		if(document.User_Access_Restriction__c != null && ga.mapUsers != null){
		  list<string> usName = new list<string>(); 
		  for(string st:document.User_Access_Restriction__c.split(';')){
			if(ga.mapUsers.containsKey(st))
			  usName.add(ga.mapUsers.get(st));
		  }
		  //userRestriction = string.join(usName, ', ');
  
		}
	  }
	}
  
	private Integer flightID = 0;
	public void addNewFlight(){
		  Client_Flight_Details__c cfd = new Client_Flight_Details__c();
		  cfd.Custom_ID__c = flightID;
	  String docId = ApexPages.currentPage().getParameters().get('docID');
	  if(docID == null || docId == '')
		docID = flightDocId;
  
	  cfd.Client_Document__c = docId;
		  flightID++;
  
	  if(!mapDocFlights.containsKey(docId))
		mapDocFlights.put(docId, new list<Client_Flight_Details__c>{cfd});
	  else
		mapDocFlights.get(docId).add(cfd);
  
	  flightDocId = null;
	  }
	// END -- Flight Details
  
	//D E L E T E     D O C U M E N T
	public String errorDeleteDoc {get;set;}
	private IPFunctions.SearchNoSharing searchNoSharing = new IPFunctions.SearchNoSharing();
	public void deleteDocument(){
	  String docId;
	  system.debug('typeAction delete==>' + typeAction);
	  if(typeAction == 'createDocs')
		docId = ApexPages.currentPage().getParameters().get('docID');
	  else 
		docId = toCreateDocument[0].id;
  
	  Client_Document__c toDelete = [SELECT preview_link__c FROM Client_Document__c WHERE id = :docId];
  
	  if(toDelete.preview_link__c != null && toDelete.preview_link__c != ''){
		errorDeleteDoc = 'Please delete all the uploaded files before you delete this document.';
		for(Integer i = 0; i <= toCreateDocument.size(); i++) {
		  if(toCreateDocument.get(i).id == docId){
			Client_Document__c upDoc = toCreateDocument.get(i);
			upDoc.isSelected__c = true;
			toCreateDocument.set(i, upDoc);
			break;
		  }
		}//end for
	  }
	  else{
		searchNoSharing.deleteNoSharing(toDelete);
		errorDeleteDoc = NULL;
  
		if(typeAction == 'createDocs')
		  for (Integer i = 0; i <= toCreateDocument.size(); i++) {
			if(toCreateDocument.get(i).id == docId){
			  toCreateDocument.remove(i);
			  break;
			}
		  }//end for
		else{
		  listDocuments();
		  findIntallmentsReceipt();
		}
	  }
	}
  
	//D E L E T E    ALL  D O C U M E N T S
	public void deleteAllDocuments(){
	  delete [Select Id from Client_Document__c where client__c = :contact.id];
	  listDocuments();
	  findIntallmentsReceipt();
	}
  
  
	// D E L E T E     F L I G H T 
	public void deleteFlight(){
		  String flightID = ApexPages.currentPage().getParameters().get('flightID');
		  String docId = ApexPages.currentPage().getParameters().get('docId');
  
		  Client_Flight_Details__c toDelete;
  
		  if(flightID != null)
		for(Client_Document__c document : toCreateDocument)
		  if(document.id == docId){
  
			list<Client_Flight_Details__c> updatedFlights = new list<Client_Flight_Details__c>();
  
			for(Client_Flight_Details__c cf : mapDocFlights.get(docId))
			  if(cf.id == flightID)
				toDelete = cf;
			  else
				updatedFlights.add(cf);
  
			mapDocFlights.put(docId, updatedFlights);
		  }
		  
		  if(toDelete != null)
			  searchNoSharing.deleteNoSharing(toDelete);
  
	  ApexPages.currentPage().getParameters().remove('flightID');
	  ApexPages.currentPage().getParameters().remove('docId');
  
	  }
	
  
	 public void deleteNewFlight(){
		  Integer customFlightID = Integer.valueOf(ApexPages.currentPage().getParameters().get('flightID'));
	  String docId = ApexPages.currentPage().getParameters().get('docId');
  
  
		  if(customFlightID != null)
		for(Client_Document__c document : toCreateDocument)
		  if(document.id == docId){
			list<Client_Flight_Details__c> updatedFlights = new list<Client_Flight_Details__c>();
  
			for(Client_Flight_Details__c cf : mapDocFlights.get(docId))
			  if(cf.Custom_ID__c != customFlightID){
				updatedFlights.add(cf);
				mapDocFlights.put(docId, updatedFlights);
			  }
  
			break;
		  }
  
	  ApexPages.currentPage().getParameters().remove('flightID');
	  ApexPages.currentPage().getParameters().remove('docId');
	  }
	//END -- Delete flight
  
  
	public class cancelReq{
	  public client_course__c request {get;set;}
	  public list<IPFunctions.s3Docs> docs {get;set;}
	  public map<String,String> relCourses {get;set;}
  
	  public cancelReq(client_course__c request, list<IPFunctions.s3Docs> docs){
		this.request = request;
		this.docs = docs;
  
		if(request.Cancel_Course_Status__c == client_course_cancel.TYPE_UNDO){ //Cancelled Request
		  relCourses = new map<String,String>();
		  for(String st : request.Cancel_Request_Ids__c.split(FileUpload.SEPARATOR_URL))
			relCourses.put(st.split(FileUpload.SEPARATOR_FILE)[0], st.split(FileUpload.SEPARATOR_FILE)[1] + '<br/><span style="color:grey;font-weight:normal;">'+ st.split(FileUpload.SEPARATOR_FILE)[2] + '</span>');
		}
	  }    
	}
	
	public class CoursesDocuments{
	  public String schoolName {get;set;}
	  public String courseId {get;set;}
	  public String courseName {get;set;}
	  public String enrolmentStatus {get;set;}
	  public boolean isCancelled {get;set;}
	  public Date enrolmentDate {get;set;}
	  public list<ClientDocument> courseDocs {get;set;}
  
	  public CoursesDocuments(String schoolName, String courseId, String courseName, String enrolmentStatus, Boolean isCancelled, Date enrolmentDate, list<ClientDocument> courseDocs){
		this.schoolName = schoolName;
		this.courseId = courseId;
		this.courseName = courseName;
		this.courseDocs = courseDocs;
		this.enrolmentStatus = enrolmentStatus;
		this.isCancelled = isCancelled;
		this.enrolmentDate = enrolmentDate;
	  }
  
	  public void addDocument(ClientDocument newDoc){
		this.courseDocs.add(newDoc);
	  }
	}
  
	public class ClientDocument{
	  public Client_Document__c document {get;set;}
	  public IPFunctions.ContactDocument oldIpDocument {get;set;} 
	  public client_course_instalment__c installment {get;set;}
	  public list<fileDocument> files {get;set;}
	  public list<Client_Flight_Details__c> flightDetails {get;set;}
	  public string agencyRestriction{get{if(agencyRestriction == null) agencyRestriction = ''; return agencyRestriction;} set;}
	  public string userRestriction{get{if(userRestriction == null) userRestriction = ''; return userRestriction;} set;}
	  public ClientDocument(Client_Document__c document, list<fileDocument> files){
		this.document = document;
		this.files = files;
	  }
	  public ClientDocument(client_course_instalment__c installment, list<fileDocument> files){
		this.installment = installment;
		this.files = files;
	  }
	  public ClientDocument(IPFunctions.ContactDocument oldIpDocument){
		this.oldIpDocument = oldIpDocument;
	  }
  
	}
  
	public class fileDocument{
	  public string name {get;set;}
	  public string url {get;set;}
	  public string urlDownload {get;set;}
	  public string dateUpload {get;set;}
  
	  public fileDocument(string name, string url, string urlDownload, string dateUploadStr){
		this.name = name;
		this.url = url;
		this.urlDownload = urlDownload;
		this.dateUpload = dateUploadStr;
		// if(dateUploadStr != null)
		// 	this.dateUpload = dateTime.parse(dateUploadStr);
		// else this.dateUpload = null;
	  }
	 }
  
  
  
  
	public String selectedCategory {get;set;}
  
	public List<SelectOption> documentsCategory {get{
	  if(documentsCategory == null){
		documentsCategory = new List<SelectOption>();
		
  
		Schema.DescribeFieldResult fieldResult = Client_Document__c.Document_Category__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
  
		for(Schema.PicklistEntry f : ple)
		  if(f.getLabel() != 'Finance')
			documentsCategory.add(new SelectOption(f.getValue(), f.getLabel()));
	  }
	  return documentsCategory;
	}set;}
  
	public List<String> selectedTypes {get;set;}
	// private map<String,String> catValues {get;set;}
	public map<String, List<SelectOption>> mapCategories {get;set;}
	public void categoriesMenu(){
  
	  selectedTypes = new list<String>();
	  mapCategories = new map<String, List<SelectOption>>();
	  // catValues = new map<String,String>();
	  Schema.DescribeFieldResult docCats = Client_Document__c.Document_Category__c.getDescribe();
	  Schema.DescribeFieldResult docType = Client_Document__c.Document_Type__c.getDescribe();
  
	  Map<String, List<String>> mapCats = PicklistUtils.getDependentOptionsImpl(docType.getSObjectField(), docCats.getSObjectField());
	  List<String> catValuesList;
	  List<String> typeValuesList;
	  for(String cat : mapCats.keySet()){
		catValuesList = cat.split(';#;');
		
		if(catValuesList[1] != 'Finance'){
		  // catValues.put(catValuesList[1], catValuesList[0]);
  
		  mapCategories.put(catValuesList[1], new list<SelectOption>());
		  for(String type : mapCats.get(cat)){
			typeValuesList = type.split(';#;');
			mapCategories.get(catValuesList[1]).add(new SelectOption(catValuesList[0]+';#;'+ typeValuesList[0], typeValuesList[1]));
		  }//end for
		}
	  }//end for
  
	}
  
  
	
  
	public List<SelectOption> countries {
		  get {
			  if(countries == null){
				  countries = IPFunctions.getAllCountries();
				  if(countries.size() > 0)
					  countries.add(0, new SelectOption('','--None--'));
			  }
			  return countries;
		  }set;}  
  
	public class listOptions{
  
	  public list<string> selectedAgencies{
		get{
		  if(selectedAgencies == null)
			selectedAgencies = new list<string>();
		  return selectedAgencies;
		}
		set;
	  }
  
	  public list<string> selectedUsers{
		get{
		  if(selectedUsers == null)
			selectedUsers = new list<string>();
		  return selectedUsers;
		}
		set;
	  }
	}
  
	public groupAgencies ga{
	  get{
		if(ga == null)
		  ga = new groupAgencies(currentUser.Contact.Account.Global_Link__c);
		return ga;
	  }
	  set;
	}
	public without sharing class groupAgencies{
	  public groupAgencies(string gpId){
		groupId = gpId;
	  }
	  private string groupId;
	  public map<string, string> mapAgencies{get; set;}
	  public List<SelectOption> Agencies {
		get {
		  if(Agencies == null){
			Agencies = new List<SelectOption>();
			mapAgencies = new map<string, string>();
			for(Account ac:[Select id, name from Account where Global_Link__c = :groupId and recordType.Name = 'Agency' order by name]){
			  Agencies.add(new SelectOption(ac.id,ac.name));
			  mapAgencies.put(ac.id, ac.name);
			}
		  }
		  return Agencies;
		}set;
	  }  
  
	  public map<string, string> mapUsers{get; set;}
	  public List<SelectOption> AgencyUsers {
		get {
		  if(AgencyUsers == null){
			AgencyUsers = new List<SelectOption>();
			mapUsers = new map<string, string>();
			for(User ac:[SELECT id, name, contact.account.Name FROM user where isActive = true and contact.account.Parent.Global_Link__c = :groupId order by contact.account.name, name]){
			  AgencyUsers.add(new SelectOption(ac.id,ac.contact.account.Name + ' - '+ ac.name));
			  mapUsers.put(ac.id, ac.contact.account.Name + ' - '+ ac.name);
			}
		  }
		  return AgencyUsers;
		}set;
	  }
  
	  private Account retrieveDocRestrictions(string sql){
		return Database.query(sql);
	  }  
	}
  
  }