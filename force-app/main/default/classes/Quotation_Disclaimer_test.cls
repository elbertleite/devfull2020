/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Quotation_Disclaimer_test {

    static testMethod void myUnitTest() {
        
        Map<String,String> recordTypes = new Map<String,String>();
        
        for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
				
		Account agency = new Account();
		agency.RecordTypeId = recordTypes.get('Agency');
		agency.name = 'IP Sydney';
		agency.Account_Currency_Iso_Code__c = 'AUD';     
		insert agency;
		

		Quotation_Disclaimer__c footer = new Quotation_Disclaimer__c();
		footer.Agency__c = agency.id;
		footer.Default_English_Disclaimer__c = 'asidaiudjaisdiaudiajidjaijsd';
		footer.Destination__c = 'Australia';
		footer.Language__c = 'en_US';
		insert footer;
		
		Quotation_Disclaimer_Other_Nationality__c translation = new Quotation_Disclaimer_Other_Nationality__c();
		translation.Disclaimer__c  = 'bla bla bla bla bla bla';
		translation.Nationalities_Applicable__c = 'Brazil,Argentina,Chile';
		translation.Quotation_Disclaimer__c = footer.id;
		translation.Language__c = 'pt_BR';
		insert translation;
		
		Quotation_Disclaimer qd = new Quotation_Disclaimer(new ApexPages.Standardcontroller(agency));
        
        Quotation_Disclaimer__c disc = qd.quoteDisclaimer;
        map<string,string> languages = qd.languages;
        List<SelectOption> notSelectedLanguagesOptions = qd.notSelectedLanguagesOptions;
        List<SelectOption> getDestinationOptions = qd.getDestinationOptions();
        qd.retrieveDestination();
        qd.cancelEdit();
        qd.saveDisclaimer();
        qd.getLanguage();
        Quotation_Disclaimer_Other_Nationality__c languageDisclaimer = qd.languageDisclaimer;
        boolean editmod = qd.editmod;
        String langId = qd.langId;
        qd.addNewLanguage();
        qd.editLanguage();
        ApexPages.currentPage().getParameters().put('langId',translation.id);
        qd.deleteLanguage();
     	qd.deleteDestination();
        
        
        
        
    }
}