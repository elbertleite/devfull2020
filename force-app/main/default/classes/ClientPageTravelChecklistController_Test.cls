@isTest
private class ClientPageTravelChecklistController_Test{
	static testMethod void myUnitTest() {

		TestFactory tf = new TestFactory();
		
		Account agency = tf.createAgency();

		Account agencyGroup = tf.createAgencyGroup();
		agencyGroup.RDStation_Campaigns__c = '["leading-page3","leading-australia","insercao-manual","leading-teste2"]';
		update agencyGroup;

		tf.createChecklists(agency.Parentid);

		Contact emp = tf.createEmployee(agency);
		
		Contact lead = tf.createLead(agency, emp);
		lead.Ownership_History_Field__c = '[{"toUserID":"005O0000004bBaSIAU","toUser":"Patricia Greco","toAgencyID":"0019000001MjqIIAAZ","toAgency":"IP Australia Sydney (HO)","fromUserID":null,"fromUser":null,"fromAgencyID":null,"fromAgency":null,"createdDate":"2018-09-18T04:17:12.024Z","createdByUserID":"005O0000004bBaSIAU","createdByUser":"Patricia Greco","action":"Taken"}]';
		lead.Lead_Stage__c = 'Stage 1';
		lead.Lead_Area_of_Study__c = 'Test 1; Test 2';
		update lead;

		Contact client = tf.createClient(agency);

		User portalUser = tf.createPortalUser(emp);

		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);

       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);

		Quotation__c q1 = tf.createQuotation(lead, campusCourse);
		Quotation__c q2 = tf.createQuotation(lead, campusCourse);

		Destination_Tracking__c dt = new Destination_tracking__c();
		dt.Client__c = lead.id;
		dt.Current_Cycle__c = true;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
		dt.Expected_Travel_Date__c = system.today().addMonths(3);
		dt.lead_Cycle__c = false;
		insert dt;

		Client_Checklist__c checklist = new Client_Checklist__c();
		checklist.Agency__c = agency.id;
		checklist.Agency_Group__c = agency.Parentid;
		checklist.Destination__c = lead.Destination_Country__c;
		checklist.Checklist_Item__c = 'aaa';
		checklist.Client__c = lead.id;
		checklist.Destination_Tracking__c = dt.id;
		insert checklist;
		
		Test.startTest();
     	system.runAs(portalUser){
		
			List<Client_Stage_Follow_up__c> followUps = new List<Client_Stage_Follow_up__c>();
			String followUpID;
			for(Client_Stage__c stage : [SELECT ID, Stage_Description__c FROM Client_Stage__c WHERE Agency_Group__c = : agency.Parentid]){
				Client_Stage_Follow_up__c cs = new Client_Stage_Follow_up__c();
				cs.Agency__c = agency.id;
				cs.Agency_Group__c = agency.Parentid;
				cs.Client__c = lead.id;
				cs.Destination__c = 'Australia';
				cs.Stage_Item__c = stage.Stage_Description__c;
				cs.Stage_Item_Id__c = stage.id;
				cs.Stage__c = 'Stage 1';
				cs.Destination_Tracking__c = dt.id;
				followUps.add(cs);
			}

			insert followUps;
		
			followUpID = followUps.get(0).ID;

			emp.Account = agency;
			portalUser.contact = emp;
			update portalUser;

			Apexpages.currentPage().getParameters().put('id', lead.id);
			Apexpages.currentPage().getParameters().put('itemid', followUpID);

			Client_Stage__c csc = new Client_Stage__c();
			csc.Stage_description__c = 'LEAD - Not Interested';
			csc.Agency_Group__c = portalUser.Contact.Account.ParentId;
			csc.Stage_Sub_Options__c = '["Option Not Interested 1","Option Not Interested 2","Option Not Interested 3"]';
			insert csc;

			ClientPageTravelChecklistController controller = new ClientPageTravelChecklistController();
			controller.getAreaOfStudyList();
			controller.getStudyTypeList();
			controller.getProductTypeList();
			controller.getOtherDestinationsList();
			controller.showHideFormNewCycle();
			controller.getCountries();
			controller.getTravelDurations();
			controller.getProductTypes();
			controller.getStudyTypes();
			controller.getCampaigns();
			controller.getTravelPlans();
			controller.saleEstimatedValue = '100,000.00';
			controller.saveDestinationTracking();
			//controller.changeTabDestinationChecklist();
			controller.otherDestinationsNewCycle = 'Ireland,United Kingdom';
			controller.createNewCycle();
			controller.saveCheckList();
			controller.uncheckFollowUp();
			controller.showHideFormEditDestinationTracking();
			controller.closeCurrentCycle();
		}
	}
}