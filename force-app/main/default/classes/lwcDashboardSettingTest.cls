@isTest
private class lwcDashboardSettingTest {
   
   @isTest static void updateSettingDashboardExceptionTest(){
        string objJson = '{"reports":[{"id":"11","name":"Commission Invoice Overview","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"ONE"}],"name":"Juliana Sousa","id":"000O0000003nhNTIAY"}';
        string r = lwcDashboardSetting.updateSettingDashboard(objJson, '');
        System.assertEquals(r, 'error');
   }

   @isTest static void updateSettingTargetExceptionTest(){
        string r =  lwcDashboardSetting.updateSettingTarget(' ','');
        System.assertEquals(r, 'error');
   }
   
     @isTest static void getDashboardSettingsUsersExceptionTest(){ 
          TestFactory tf = new TestFactory();        
          Account agency = tf.createAgency();           
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);           

          Test.startTest();
     	system.runAs(portalUser){    
            portalUser.DashboardSettings__c = '{"reports":[{"id":"11","name":"Commission Invoice Overview","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"ONE","name":"Juliana Sousa","id":"005O0000003nhNTIAY"}';
            update portalUser;        
            List<lwcDashboardSetting.wrpDashSettings> r = lwcDashboardSetting.getDashboardSettingsUsers(agency.id);
            System.assertNotEquals(r, null);
        }        
     }

     @isTest static void getDashboardSettingsUsers1ExceptionTest(){ 
          TestFactory tf = new TestFactory();        
          Account agency = tf.createAgency();           
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);           

          Test.startTest();
     	system.runAs(portalUser){    
               List<lwcDashboardSetting.wrpDashSettings> r = lwcDashboardSetting.getDashboardSettingsUsers(agency.id);
               System.assertNotEquals(r, null);
          }        
     }


   @isTest static void getDepartmentTargetsTest(){ 
        TestFactory tf = new TestFactory();        
        Account agency = tf.createAgency();           
        Contact emp = tf.createEmployee(agency);
        User portalUser = tf.createPortalUser(emp);                

        Test.startTest();
     	system.runAs(portalUser){ 
            string selectedAgency = '';
            list<Department__c> r = lwcDashboardSetting.getDepartmentTargets(agency.Id);
            System.assertNotEquals(r, null);
       }
   }
   
   @isTest static void updateSettingDashboardTest(){
        TestFactory tf = new TestFactory();        
        Account agency = tf.createAgency();           
        Contact emp = tf.createEmployee(agency);
        User portalUser = tf.createPortalUser(emp);                

        Test.startTest();
     	system.runAs(portalUser){  
            
            string objJson = '[{"id":"'+ emp.id +'","name":"Claudia Abras","reports":[{"id":"3","name":"Task","period":"Next week","criteria":"NEXT_WEEK","category":"LIST"}]}]';
            string r = lwcDashboardSetting.updateSettingDashboard(objJson, agency.id);
            //System.assertEquals(r, 'success');
        }
   }
   
   @isTest static void updateSettingTargetTest(){ 
       TestFactory tf = new TestFactory();        
        Account agency = tf.createAgency();           
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);        
        Department__c dep = tf.createDepartment(agency);
        dep.Department_Sales_Target__c = 225;
        update dep;

        Test.startTest();
     	system.runAs(portalUser){            
            string objJson = '[{"id":"' + dep.Id + '","value":1252362.52},{"id":"a0K9000000QFbRXEA1","value":"5000"},{"id":"a0K9000000QFbSuEAL","value":0},{"id":"a0K9000000QFbSfEAL","value":0},{"id":"a0K9000000QFbSaEAL","value":225},{"id":"a0K9000000QFcMkEAL","value":0},{"id":"a0K9000000TOS9UEAX","value":0},{"id":"a0K9000000QFbSzEAL","value":50000},{"id":"a0K9000000QFbOEEA1","value":0},{"id":"a0K9000000QKW0TEAX","value":0},{"id":"a0K9000000QFbSkEAL","value":0}]';
            string r =  lwcDashboardSetting.updateSettingTarget(objJson, agency.id);
            System.assertEquals(r, 'success');
        }        
   }
   
   @isTest static void getDashReportsTest(){
        List<lwcDashboardSetting.wrpGeneric> r = lwcDashboardSetting.getDashReports();
        System.assertNotEquals(r, null);
   }
   
   @isTest static void getDashboardSettingsUsersTest(){ 
      
          TestFactory tf = new TestFactory();        
          Account agency = tf.createAgency();                     
      	Contact emp = tf.createEmployee(agency);                   
       	User portalUser = tf.createPortalUser(emp);

          Test.startTest();
     	system.runAs(portalUser){  
                        
            portalUser.DashboardSettings__c = '{"reports":[{"id":"3","name":"Task","period":"This week","criteria":"THIS_WEEK","category":"LIST"}],"name":"Juliana Sousa","id":"005O0000003nhNTIAY",'
                                              +'"reports":[{"id":"3","name":"Task","period":"This week","criteria":"THIS_WEEK","category":"LIST"}],"name":"Juliana Martinez","id":"0059000000UPYLOAA5",'
                                              +'"reports":[{"id":"3","name":"Task","period":"This week","criteria":"THIS_WEEK","category":"LIST"}],"name":"Nabila Pezz","id":"0052v00000aIlgbAAC"}';
            update portalUser;

            List<lwcDashboardSetting.wrpDashSettings> r = lwcDashboardSetting.getDashboardSettingsUsers(agency.id);
            System.assertNotEquals(r, null);
        }
   }
   
   @isTest static void getFilterCriteriaTest(){
        List<lwcDashboardSetting.wrpGeneric> r = lwcDashboardSetting.getFilterCriteria();
        System.assertNotEquals(r, null);
   }
}