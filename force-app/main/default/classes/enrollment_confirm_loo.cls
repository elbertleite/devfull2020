public without sharing class enrollment_confirm_loo extends EnrolmentManager{

	private static Contact currentUser {get{if(currentUser==null) currentUser = UserDetails.getMyContactDetails(); return currentUser;}set;}

	public String bucket {get{return EnrolmentManager.bucket;} private set;}
	private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}
	private map<String, list<String>> boAgenciesMap {get;set;}
	public string boAgenciesPerCountry {get;set;}

	public String agenciesBO {get;set;}
	public String countryOptions {get;set;}
	public String groupOptions {get;set;}
	public string selectedCountry{get;set;}
	public boolean showBOerror {get;set;}

	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}

	//CONSTRUCTOR
	public enrollment_confirm_loo(){

		//Find BackOffices
		string selectedBackOffice = currentUser.AccountId;
		// if(selectedBackOffice == '0019000001mQjjhAAC')
		// 	selectedBackOffice = '0019000001MjqIIAAZ';
		boAgenciesMap = ff.findBOAgenciesPerService2(selectedBackOffice, new list<String>{'Admissions'});

		// Verify backoffice access per country
		map<String, boolean> boAgPerCountry;
		boAgPerCountry = ff.findAgencyBackOfficeCountryAccess(selectedBackOffice, new list<String>{'Admissions'});
		for(string st:boAgenciesMap.keySet())
			if(!boAgPerCountry.containsKey(st)) 
				boAgPerCountry.put(st, true);
		boAgenciesPerCountry = JSON.serialize(boAgPerCountry);

		if(boAgenciesMap.size()>0){
			showBOerror = false;
			//Country Filter
			countryOptions = countryFilter();

			//Agency Group Filter
			if(selectedCountry != null)
				groupOptions = changeCountry(boAgenciesMap.get(selectedCountry), selectedCountry);
			else groupOptions = JSON.serialize('');

			agenciesBO = JSON.serialize(boAgenciesMap);
		}
		else{
			countryOptions = JSON.serialize('no backoffice');
			groupOptions = JSON.serialize('no backoffice');
			agenciesBO = JSON.serialize('no backoffice');
			showBOerror = true;
		} 
	}

	//SEARCH
	@RemoteAction
	public static String search(String selectedCountry, String selectedAgencyGroup, List<String> searchAgencies, String selectedSchool, String selectedCampus, String visaFrom, String visaTo, String ctName, String panel){
		String sql = EnrolmentManager.looFields + ' ';
		String sqlWhere = '';

		system.debug('searchAgencies==' + searchAgencies);

		//Agencies and course contry
		sqlWhere += ' WHERE LOO_Requested_to_BO_By__r.Contact.AccountID in  ( \''+ String.join(searchAgencies, '\',\'') + '\' ) AND campus_country__c = \'' + selectedCountry +'\' ';

		// Agency Group
		sqlWhere += ' AND LOO_Requested_to_BO_By__r.Contact.Account.ParentId = \'' + selectedAgencyGroup + '\' ';

		if(selectedCampus!='all')
			sqlWhere += ' AND Campus_Id__c =  \'' + selectedCampus  + '\' '; //Campus
		else if(selectedSchool!='all')
			sqlWhere += ' AND School_Id__c =  \'' + selectedSchool  + '\' '; //School

		
		if(visaFrom != null && visaFrom != '')
			sqlWhere += ' AND Client__r.Visa_Expiry_Date__c >= '+ visaFrom; //Visa Expiry From

		if(visaTo != null && visaTo != '')
			sqlWhere += ' AND Client__r.Visa_Expiry_Date__c <= '+ visaTo; //Visa Expiry To


		if(ctName!=null && ctName != '')
			sqlWhere += ' AND Client__r.Name like \'%' +ctName+ '%\'  '; //Client Name

		sql += sqlWhere;

		/*//Request Panel
		if(panel == 'request')
			sql += ' AND LOO_Requested_To_BO__c = true AND LOO_Requested_TO_SC__c = false AND LOO_Received__c = false  ';

		//Confirm Panel
		if(panel == 'confirm')
			sql += ' AND LOO_Requested_To_BO__c = true AND LOO_Requested_TO_SC__c = true AND LOO_Received__c = false  ';

		// Order by
		sql += ' order by LOO_Requested_to_BO_On__c desc, course_package__c ';*/

		//Request Panel
		if(panel == 'request'){
			sql += ' AND LOO_Requested_To_BO__c = true AND LOO_Requested_TO_SC__c = false AND LOO_Received__c = false ORDER BY Client__r.Visa_Expiry_Date__c desc, Client__r.Expected_Travel_Date__c desc, isUrgentEnrolment__c desc, Start_Date__c asc, LOO_Requested_to_BO_On__c desc, course_package__c ';
		}

		//Confirm Panel
		if(panel == 'confirm'){
			sql += ' AND LOO_Requested_To_BO__c = true AND LOO_Requested_TO_SC__c = true AND LOO_Received__c = false ORDER BY Client__r.Visa_Expiry_Date__c desc, Client__r.Expected_Travel_Date__c desc, isUrgentEnrolment__c desc, LOO_Requested_to_SC_On__c desc, course_package__c ';
		}

		system.debug('sql==>' + sql);

		List<RequestWrapper> pendingRequests = new List<RequestWrapper>();
		Map<String, RequestWrapper> packageRequests = new Map<String, RequestWrapper>();
		List<Client_Course__c> packages = new List<Client_Course__c>();

		map<String,String> schoolMap = new map<String,String>();
		map<String, list<IPFunctions.opWrapper>> campusMap = new map<String, list<IPFunctions.opWrapper>>();
		set<Id> campusId = new set<Id>();

		for(Client_Course__c course : Database.query(sql)){

			if(course.Course_Package__c == null){
				RequestWrapper rw = new RequestWrapper();
				rw.contact = course.client__r;
				rw.statusDaysAgo = ''; //Important to avoid bug error "Illegal Arguments"
				rw.course = course;
				rw.action = 'noAction';
				rw.courseNotes = course.Custom_Notes_Tasks__r;
				rw.activities = buildEnrolmentHistoryAngular(course.Enrolment_Activities__c); 
				packageRequests.put(course.id, rw);
				pendingRequests.add(rw);
				
			}else
				packages.add(course);


			schoolMap.put(course.School_Id__c, course.School_Name__c);

			if(!campusMap.containsKey(course.School_Id__c))
				campusMap.put(course.School_Id__c, new list<IPFunctions.opWrapper>{new IPFunctions.opWrapper('all', '-- All --'), new IPFunctions.opWrapper(course.Campus_Id__c, course.Campus_Name__c)});
			else if(!campusId.contains(course.Campus_Id__c))
				campusMap.get(course.School_Id__c).add(new IPFunctions.opWrapper(course.Campus_Id__c, course.Campus_Name__c));

			campusId.add(course.Campus_Id__c);

		}//end for

		for(Client_Course__c pkg : packages){
			if(packageRequests.containsKey(pkg.Course_Package__c))
				packageRequests.get(pkg.Course_Package__c).packageCourses.add(pkg);
		}//end for

		String totRequest = panel == 'request' ? string.valueOf(pendingRequests.size()) : totals('request', sqlWhere);
		String totConfirm = panel == 'confirm' ? string.valueOf(pendingRequests.size()) : totals('confirm', sqlWhere);


		list<listOpt> refineSearch = new list<listOpt>();
		refineSearch.add(new listOpt('all', '-- All --', new list<IPFunctions.opWrapper>{new IPFunctions.opWrapper('all', '-- All --')}));

		for(String sch : schoolMap.keySet()){
			refineSearch.add(new listOpt(sch, schoolMap.get(sch), campusMap.get(sch)));
		}//end for

		Map<String, Boolean> schoolHasExternalPages = new Map<String, Boolean>();
		Map<String, List<IPClasses.SchoolExternalPage>> schoolPages = PMCHelper.retrieveSchoolExternalPages(new List<String>(schoolMap.keySet()), false, null);
		for(String schoolId : schoolPages.keySet()){
			schoolHasExternalPages.put(schoolId, !schoolPages.get(schoolId).isEmpty());
		}

		return JSON.serialize(new result(totRequest, totConfirm, packageRequests, refineSearch, schoolHasExternalPages));
	}

	private static String totals(String panel, String sqlWhere){
		Integer result;
		String sql = 'SELECT count(id) tot FROM client_course__c  ' + sqlWhere;

		if(panel == 'request')
			sql += ' AND LOO_Requested_To_BO__c = true AND LOO_Requested_TO_SC__c = false AND LOO_Received__c = false  ';

		//Confirm Panel
		if(panel == 'confirm')
			sql += ' AND LOO_Requested_To_BO__c = true AND LOO_Requested_TO_SC__c = true AND LOO_Received__c = false  ';

		sql += ' AND course_package__c = null ';
		
		for(AggregateResult ar : Database.query(sql))
			result = (Integer) ar.get('tot');

		return string.valueOf(result);
	}

	private class result{
		private String totRequest {get;set;}
		private String totConfirm {get;set;}
		private list<listOpt> refineSearch {get;set;}
		private Map<String, RequestWrapper> courses {get;set;}
		private Map<String, Boolean> schoolHasExternalPages {get;set;}
		public result(String totRequest, String totConfirm, Map<String, RequestWrapper> courses, list<listOpt> refineSearch){
			this.totRequest = totRequest;
			this.totConfirm = totConfirm;
			this.courses = courses;
			this.refineSearch = refineSearch;
		}
		public result(String totRequest, String totConfirm, Map<String, RequestWrapper> courses, list<listOpt> refineSearch, Map<String, Boolean> schoolHasExternalPages){
			this.totRequest = totRequest;
			this.totConfirm = totConfirm;
			this.courses = courses;
			this.refineSearch = refineSearch;
			this.schoolHasExternalPages = schoolHasExternalPages;
		}
	}

	// ADD NOTE
	@RemoteAction
	public static String addNote(String clientId, String courseid, String noteSubj, String noteDesc, String firstInst){
	
		try{
			IPFunctions.createCourseNote(clientId, courseid, noteSubj, noteDesc, firstInst);
			return 'Success';
		}catch(Exception e){
			return 'failed';
		}
	}

	//ONLINE APPLICATION
	@RemoteAction
	public static String onlineApplication(String mainCourse, list<String> pkg, String comments, String firstInst, Boolean isUrgent, String clientId){
		
		Savepoint sp = Database.setSavepoint();
        try{
			system.debug('mainCourse===>' + mainCourse);
			system.debug('pkg==>' + pkg);

			list<String> allIds = new list<String>{mainCourse};
			if(pkg != null && pkg.size()>0)
				allIds.addAll(pkg);

			list<client_course__c> updatecc = new list<client_course__c>();

            for(client_course__c cc : [SELECT Id, Enrolment_Activities__c FROM client_course__c WHERE Id in :allIds]){
				cc.LOO_Requested_TO_SC__c = true;
				cc.LOO_Requested_TO_SC_On__c = system.now();
				cc.LOO_Requested_TO_SC_By__c = UserInfo.getUserId();
				cc.isOnlineApplication__c = true;
				cc.isUrgentEnrolment__c = isUrgent;
				cc.Enrolment_Activities__c += EnrolmentManager.createActivity(EnrolmentManager.ACTIVITY_TYPE_SYSTEM, EnrolmentManager.STATUS_LOO_REQUESTED_TO_SCH + ' (Online Application)', system.now(), currentUser.Name + ' ('+ currentUser.Account.Name+')');
				updatecc.add(cc);
			}//end for

			if(comments != null && comments.trim().length() > 0){
				Custom_Note_Task__c note = new Custom_Note_Task__c();
				note.client_course__c = mainCourse;
				note.Related_Contact__c = clientId;
				note.isEnrolment__c = true;
				note.isNote__c = true;
				note.Comments__c = comments;
				note.subject__c = EnrolmentManager.STATUS_LOO_REQUESTED_TO_SCH;
				note.Instalment__c = firstInst;
				insert note;
			}

			update updatecc;
			return 'Success';
        }catch(Exception e){
            ApexPages.addMessages(e);
            Database.rollback(sp);
			return 'Error';
        }
	}

	//CONFIRM LOO
	@RemoteAction
	public static String confirmLOO(String mainCourse, list<String> pkg, String comments, String firstInst, Boolean isUrgent, String isConditional, String clientId, String requestConfimationStudent){
		
		Savepoint sp = Database.setSavepoint();
        try{
			system.debug('mainCourse===>' + mainCourse);
			system.debug('pkg==>' + pkg);

			list<String> allIds = new list<String>{mainCourse};
			if(pkg != null && pkg.size()>0)
				allIds.addAll(pkg);

			list<client_course__c> updatecc = new list<client_course__c>();

            for(client_course__c cc : [SELECT Id, COE_Received__c, Enrolment_Activities__c FROM client_course__c WHERE Id in :allIds]){
				cc.LOO_Received__c = true;
				cc.LOO_Received_On__c = system.now();
				cc.LOO_Received_By__c = Userinfo.getUserid();
				cc.isConditionalLOO__c = boolean.valueOf(isConditional);
				cc.isCOE_Required__c = boolean.valueOf(requestConfimationStudent);
				cc.LOO_Missing_Documents__c = false;
				cc.isUrgentEnrolment__c = isUrgent;

				// if(cc.COE_Received__c){
				// 	cc.LOO_Requested_to_CLI__c = true;
				// 	cc.LOO_Requested_to_CLI_On__c = system.now();
				// 	cc.LOO_Requested_to_CLI_By__c = UserInfo.getUserId();
				// }

				String descr = boolean.valueOf(isConditional) ? EnrolmentManager.STATUS_LOO_RECEIVED + ' (Conditional Enrolment Offer)' : EnrolmentManager.STATUS_LOO_RECEIVED;
				cc.Enrolment_Activities__c += EnrolmentManager.createActivity(EnrolmentManager.ACTIVITY_TYPE_SYSTEM, descr, system.now(), currentUser.Name + '('+ currentUser.Account.Name+')');
				updatecc.add(cc);
			}//end for

			if(comments != null && comments.trim().length() > 0){
				Custom_Note_Task__c note = new Custom_Note_Task__c();
				note.client_course__c = mainCourse;
				note.Related_Contact__c = clientId;
				note.isEnrolment__c = true;
				note.isNote__c = true;
				note.Comments__c = comments;
				note.subject__c = EnrolmentManager.STATUS_LOO_RECEIVED;
				note.Instalment__c = firstInst;
				insert note;
			}

			update updatecc;
			return 'Success';
        }catch(Exception e){
            ApexPages.addMessages(e);
            Database.rollback(sp);
			return 'Error';
        }
	}

	//MISSING DOCUMENTS
	@RemoteAction
	public static String missingDocs(String mainCourse, list<String> pkg, String comments, String firstInst, Boolean isUrgent, String clientId){

        Savepoint sp = Database.setSavepoint();
        try{
			system.debug('mainCourse===>' + mainCourse);
			system.debug('pkg==>' + pkg);

			list<String> allIds = new list<String>{mainCourse};
			if(pkg != null && pkg.size()>0)
				allIds.addAll(pkg);

			list<client_course__c> updatecc = new list<client_course__c>();

            for(client_course__c cc : [SELECT Id, Enrolment_Activities__c FROM client_course__c WHERE Id in :allIds]){
				cc.LOO_Requested_To_BO__c = false;
				cc.LOO_Requested_to_BO_By__c = null;
				cc.LOO_Requested_to_BO_On__c = null;
				cc.LOO_Requested_TO_SC__c = false;
				cc.LOO_Requested_TO_SC_On__c = null;
				cc.LOO_Requested_TO_SC_By__c = null;
				cc.LOO_Missing_Documents__c = true;
				cc.LOO_Missing_Documents_On__c = system.now();
				cc.isUrgentEnrolment__c = isUrgent;
				cc.Enrolment_Activities__c += EnrolmentManager.createActivity(EnrolmentManager.ACTIVITY_TYPE_SYSTEM, EnrolmentManager.STATUS_MISSING_DOCUMENTS, system.now(), currentUser.Name + ' ('+ currentUser.Account.Name+')');
				updatecc.add(cc);
			}//end for

			if(comments != null && comments.trim().length() > 0){
				Custom_Note_Task__c note = new Custom_Note_Task__c();
				note.client_course__c = mainCourse;
				note.Related_Contact__c = clientId;
				note.isEnrolment__c = true;
				note.isNote__c = true;
				note.Comments__c = comments;
				note.subject__c = EnrolmentManager.STATUS_MISSING_DOCUMENTS;
				note.Instalment__c = firstInst;
				insert note;
			}

			update updatecc;
			return 'Success';
        }catch(Exception e){
            ApexPages.addMessages(e);
            Database.rollback(sp);
			return 'Error';
        }
	}

	@RemoteAction
    public static String isFileUploaded(String courseID) {
        if(courseid != null){
            String previewLink = [select Loo__r.Preview_Link__c from Client_Course__c where id = :courseid].Loo__r.Preview_Link__c;
            return previewLink != null && previewLink.trim().length() > 1 ? 'true' : 'false';
        }
        return 'false';
    }

	// COUNTRY FILTER
	private String countryFilter(){

        String pCountry = ApexPages.CurrentPage().getParameters().get('ct');
       
	    if(pCountry == null || pCountry == '') 
			pCountry = currentUser.Account.BillingCountry;

		list<String> result = new list<String>();

		String sql = ' SELECT Course_Country__c ct FROM client_course__c WHERE ';

		list<String> sqlWhere = new list<String>();
		for(String ct : boAgenciesMap.keySet()){
			list<String> ag = boAgenciesMap.get(ct);
			sqlWhere.add('(LOO_Requested_to_BO_By__r.Contact.AccountID IN  ( \''+ String.join(ag, '\',\'') + '\' ) AND Course_Country__c = \'' + ct + '\')');
		}//end for

		sql += '(' + String.join(sqlWhere, ' OR ') + ') AND LOO_Requested_To_BO__c = true AND LOO_Received__c = false group by Course_Country__c order by Course_Country__c ';
		
		system.debug('sqlCountry ==>' + sql);

		for(AggregateResult ar : Database.query(sql)){
          result.add((String)ar.get('ct'));

          if((String)ar.get('ct') == pCountry)
            selectedCountry = (String)ar.get('ct');
        }//end for
		
		if(selectedCountry == null && result.size()>0)
        	selectedCountry = result[0];
			
		return JSON.serialize(result);
	}

	// AGENCY FILTER
	@RemoteAction
	public static String changeCountry(list<String> agenciesBO, String selectedCountry){

		String sql = ' SELECT LOO_Requested_to_BO_By__r.Contact.AccountID agId, LOO_Requested_to_BO_By__r.Contact.Account.Name agName, LOO_Requested_to_BO_By__r.Contact.Account.ParentId gpId, LOO_Requested_to_BO_By__r.Contact.Account.Parent.Name gpName FROM Client_Course__c WHERE ';
		
		sql += ' LOO_Requested_to_BO_By__r.Contact.AccountID in ( \''+ String.join(agenciesBO, '\',\'') + '\' ) AND LOO_Requested_To_BO__c = true AND LOO_Received__c = false AND campus_country__c = \'' + selectedCountry + '\'';
		
		sql += ' GROUP BY LOO_Requested_to_BO_By__r.Contact.Account.ParentId, LOO_Requested_to_BO_By__r.Contact.Account.Parent.Name, LOO_Requested_to_BO_By__r.Contact.AccountID, LOO_Requested_to_BO_By__r.Contact.Account.Name ORDER BY LOO_Requested_to_BO_By__r.Contact.Account.Parent.Name, LOO_Requested_to_BO_By__r.Contact.Account.Name';

		system.debug('sqlCountry ==>' + sql);

		list<listOpt> result = new list<listOpt>();
		String gpId;

		listOpt groupInfo;
		String forGroup;
		for(AggregateResult ar : Database.query(sql)){
			forGroup = (String) ar.get('gpId');

			if(forGroup != gpId){
				if(gpId!=null)
					result.add(groupInfo);
				groupInfo = new listOpt((String) ar.get('gpId'), (String) ar.get('gpName'), (String) ar.get('agId'), (String) ar.get('agName'));
				gpId = (String) ar.get('gpId');
			}
			else{
				groupInfo.addAgency((String) ar.get('agId'), (String) ar.get('agName'));
			}
		}//end for

		if(forGroup == gpId) //Add left over to the list
			result.add(groupInfo);


		system.debug('AGENCY FILTER ==>' + result);

		return JSON.serialize(result);
	}


	private class listOpt{
		private String gpId {get;set;}
		private String gpName {get;set;}
		list<IPFunctions.opWrapper> agencies {get;set;}

		private listOpt(String gpId, String gpName, String agId, String agName){
			this.gpId = gpId;
			this.gpName = gpName;
			agencies = new list<IPFunctions.opWrapper>{new IPFunctions.opWrapper('all', '-- All --'), new IPFunctions.opWrapper(agId, agName)};
		}
		private listOpt(String gpId, String gpName, list<IPFunctions.opWrapper> agencies){
			this.gpId = gpId;
			this.gpName = gpName;
			this.agencies = agencies;
		}

		private void addAgency(String agId, String agName){
			agencies.add(new IPFunctions.opWrapper(agId, agName));
		}
	}
}