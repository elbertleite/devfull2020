public without sharing class onlineappointments {
    
    public String userAgency{get;set;}
    public Integer totalAppointments{get;set;}
    public Map<String, Map<Date, List<BookingWrapper>>> appointments{get;set;}
    public Map<String, Integer> appointmentsPerMonth{get;set;}
    
    public Boolean showCompleteAppointments{get;set;}

    public String groupSelected{get;set;}
    public List<SelectOption> groups{get;set;}
    
    public String agencySelected{get;set;}
    public List<SelectOption> agencies{get;set;}

    public String departmentSelected{get;set;}
    public List<SelectOption> departments{get;set;}

    public String employeeSelected{get;set;}
    public List<SelectOption> employees{get;set;}

    public String endDate{get;set;}
	public String beginDate{get;set;}

    public User user{get;set;}

    public onlineappointments() {
        this.showCompleteAppointments = false;
        this.totalAppointments = 0;
        user = [Select Id, Name, Report_Convertion_Rate_Filters__c, Contact.Department__c, Contact.Department_View_Permission__c,  Contact.Export_Report_Permission__c, Contact.AccountId, Contact.Name, Contact.Account.Id, Contact.Account.Name, Contact.Account.Parent.RDStation_Campaigns__c, Contact.Account.Parent.Name, Contact.Account.Parent.Id, Aditional_Agency_Managment__c, Contact.Group_View_Permission__c, Contact.Agency_View_Permission__c, Leads_Distribution_Options__c, Leads_Distribution_Date_Filter_Access__c from User where Id = :UserInfo.getUserId()];

        groupSelected = user.Contact.Account.ParentId;
        groups = loadGroups(user.Contact.AccountId);

        agencySelected = user.Contact.Account.Id;
        userAgency = user.Contact.Account.Id;
        agencies = loadAgencies(user.Contact.Account.Id, user.Contact.Account.ParentId, Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible(), user.Contact.Group_View_Permission__c, user.Aditional_Agency_Managment__c);

        this.departmentSelected = String.isEmpty(user.Contact.Department__c) ? 'All' : user.Contact.Department__c;
        this.departments = loadDepartments();

        employeeSelected = user.ID;

        employees = loadEmployees();

        beginDate = Date.today().format();
        endDate = Date.today().addDays(1).format();
    }

    public List<SelectOption> loadEmployees(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('all', 'All Employees'));
        if(!String.isEmpty(agencySelected) && agencySelected != 'all' && !String.isEmpty(departmentSelected) && departmentSelected != 'all'){
            String query = 'Select Contact.Name, Id From User WHERE Contact.RecordType.Name=\'Employee\' and IsActive = true AND Contact.Account.Parent.ID = :groupSelected';
            if(!String.isEmpty(agencySelected) && agencySelected != 'all'){
                query += ' AND Contact.AccountId= :agencySelected ';
            }
            if(!String.isEmpty(departmentSelected) && departmentSelected != 'all'){
                query += ' AND Contact.Department__c = :departmentSelected ';
            }
            query += ' order by Name';
            for(User a : Database.query(query)){
                options.add(new SelectOption(a.Id, a.Contact.Name));
            }
        }
        return options;
    }
    public List<SelectOption> loadGroups(String agencyID){
		Set<String> idsAllGroups = new Set<String>();
		for(SelectOption option : IPFunctions.getAgencyGroupsByPermission(agencyID, Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())){
			idsAllGroups.add(option.getValue());
		}
		List<SelectOption> options = new List<SelectOption>();
		for(Account option : [Select Id, Name from Account where id in :idsAllGroups AND Inactive__c = false order by name]){
			options.add(new SelectOption(option.ID, option.Name));
		}
		
		return options;
    }
    
    public List<SelectOption> loadAgencies(String agencyID, String groupID, Boolean allAgenciesAcessible, Boolean hasPermissionGroup, String aditionalAgencies){
		List<SelectOption> options = new List<SelectOption>();
		Set<String> agencies = new Set<String>();
        if(!allAgenciesAcessible && !hasPermissionGroup){ //set the user to see only his own agency
            for(Account a: [Select Id, Name from Account where id =:agencyID AND Inactive__c = false order by name]){
                agencies.add(a.ID);
            }
            if(!String.isEmpty(aditionalAgencies)){
                for(Account ac:ipFunctions.listAgencyManagment(aditionalAgencies)){
                    if(!ac.Inactive__c){
                        agencies.add(ac.ID);
                    }
                }
            }
        }else{
            for(Account ag : [Select Id, Name From Account Where Recordtype.Name = 'Agency' AND ParentId = :groupID AND Inactive__c = false]){
                agencies.add(ag.ID);
            }
        }
        if(agencies.size() > 1){
           options.add(new SelectOption('all', 'All Available Agencies')); 
        }

        for(Account option : [Select Id, Name from Account where id in :agencies AND Inactive__c = false order by name]){
            options.add(new SelectOption(option.ID, option.Name));
        }
		return options;
    }
      
    public List<SelectOption> loadDepartments(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('all','All Departments'));
        Set<String> ids = new Set<String>();
        if(!String.isEmpty(agencySelected) && agencySelected != 'all'){
            for(User department : [SELECT Contact.Department__r.Name, Contact.Department__c FROM User WHERE Contact.Account.Id = :agencySelected and Contact.RecordType.Name='Employee' and IsActive = true ORDER BY Contact.Department__r.Name]){
                if(!String.isEmpty(department.Contact.Department__c) && !ids.contains(department.Contact.Department__c)){
                    options.add(new SelectOption(department.Contact.Department__c, department.Contact.Department__r.Name));
                    ids.add(department.Contact.Department__c);
                }
            }
        }   
        return options;
    }

    public class BookingWrapper{
        public Client_Booking__c booking{get;set;}
        public String bookingTime{get;set;}
        public String visaExpiryDateForm{get;set;}
        public String visaExpiryDate{get;set;}
        public BookingWrapper(){}
        public BookingWrapper(Client_Booking__c booking){
            this.booking = booking;
            String hour = booking.Time_Booking__c.hour() >= 10 ? ''+booking.Time_Booking__c.hour() : '0'+booking.Time_Booking__c.hour();
            String minute = booking.Time_Booking__c.minute() >= 10 ? ''+booking.Time_Booking__c.minute() : '0'+booking.Time_Booking__c.minute();
            this.bookingTime = hour + ':' + minute;
            if(booking.Client__c != null){
                if(booking.client__r.Visa_Expiry_Date__c != null){
                    this.visaExpiryDate = booking.client__r.Visa_Expiry_Date__c.format();
                }
            }
            if(booking.Visa_Expiring_Date__c != null){
                this.visaExpiryDateForm = booking.Visa_Expiring_Date__c.format();
            }
        }
    }

    public void showCompleteAppointments(){
        this.showCompleteAppointments = ApexPages.currentPage().getParameters().get('checked') == 'true';
        loadExistingAppointments();
    }

    public void showPastAppointments(){
        //this.showPastAppointments = ApexPages.currentPage().getParameters().get('checked') == 'true';
        //loadExistingAppointments();
    }

    public void completeBooking(){
        String bookingId = ApexPages.currentPage().getParameters().get('bookingId');
        Client_Booking__c bk = new Client_Booking__c(Id = bookingId, Status__c = 'Complete');
        update bk;
        loadExistingAppointments();
    }

    public void cancel(){
        String bookingId = ApexPages.currentPage().getParameters().get('bookingId');
        Client_Booking__c bk = new Client_Booking__c(Id = bookingId);
        delete bk;
        loadExistingAppointments();
    }

    public void updateFilters(){
        String filter = ApexPages.currentPage().getParameters().get('filter');
        String filterValue = ApexPages.currentPage().getParameters().get('filterValue');
        if(filter == 'agencyGroup'){
            this.agencies = getAgenciesUser(filterValue);
            this.agencySelected = 'all';
            this.departments = loadDepartments();
            this.departmentSelected = 'all';
            this.employees = loadEmployees();
            this.employeeSelected = 'all';
        }else if(filter == 'agency'){
            this.agencySelected = filterValue;
            this.departments = loadDepartments();
            this.departmentSelected = 'all';
            this.employees = loadEmployees();
            this.employeeSelected = 'all';
        }else if(filter == 'department'){
            this.departmentSelected = filterValue;
            this.employees = loadEmployees();
            this.employeeSelected = 'all';
        }
    }

    public void loadExistingAppointments(){
        //User user = [SELECT ID, Contact.Name FROM User WHERE ID = :];
        //Date today = Date.today();
        //String userId = UserInfo.getUserId();

        system.debug(groupSelected);
        Date beginDateSearch = IPFunctions.getDateFromString(beginDate);
        Date endDateSearch = IPFunctions.getDateFromString(endDate);
        String query = 'SELECT ID, Date_Booking__c, Time_Booking__c ,Services_Requested__c, Mobile_Number__c, Contact_Type__c, Contact_Type_Detail__c, Facebook__c, Book_Email__c, Description__c, Status__c, User__c, Client__c, Client__r.Name, Client_First_Name__c, Client_Last_Name__c, Visa_Expiring_Date__c, client__r.Visa_Expiry_Date__c, User__r.Name, Client__R.RecordType.Name, Client__r.Status__c, Client__R.Lead_Stage__c FROM Client_Booking__c WHERE User__r.Contact.Account.Parent.Id = :groupSelected ';
        if(!String.isEmpty(agencySelected) && agencySelected != 'all'){
            query += ' AND User__r.Contact.Account.Id = :agencySelected';
        }
        if(!String.isEmpty(departmentSelected) && departmentSelected != 'all'){
            query += ' AND User__r.Contact.Department__c = :departmentSelected';
        }
        if(!String.isEmpty(employeeSelected) && employeeSelected != 'all'){
            query += ' AND User__c = :employeeSelected';
        }
        if(!showCompleteAppointments){
            //query += ' AND Status__c = \'Incomplete\'';
            query += ' AND Status__c != \'Complete\'';
        }else{
        }
        if(beginDateSearch != null){
            query += ' AND Date_Booking__c >= :beginDateSearch';
        }
        if(endDateSearch != null){
            query += ' AND Date_Booking__c <= :endDateSearch';
        }
        /*if(!showPastAppointments){
            query += ' AND Date_Booking__c >= :today';
        }*/
        query += ' ORDER BY Date_Booking__c, Time_Booking__c';
        
        List<Client_Booking__c> userBookings = Database.query(query);
        
        this.totalAppointments = userBookings.size();

        appointments = new Map<String, Map<Date, List<BookingWrapper>>>();
        appointmentsPerMonth = new Map<String, Integer>();
        if(this.totalAppointments > 0){
            String monthAndYear;
            for(Client_Booking__c book : userBookings){
                monthAndYear = IPFunctions.getMonthName(book.Date_Booking__c.month())+', '+book.Date_Booking__c.year();
                if(!appointments.containsKey(monthAndYear)){
                    appointments.put(monthAndYear, new Map<Date, List<BookingWrapper>>());
                    appointmentsPerMonth.put(monthAndYear, 0);
                }
                appointmentsPerMonth.put(monthAndYear, appointmentsPerMonth.get(monthAndYear) + 1);
                if(!appointments.get(monthAndYear).containsKey(book.Date_Booking__c)){
                    appointments.get(monthAndYear).put(book.Date_Booking__c, new List<BookingWrapper>());
                }
                appointments.get(monthAndYear).get(book.Date_Booking__c).add(new BookingWrapper(book));
            }
            system.debug('THE APP '+JSON.serialize(appointments));
        }
    }

    public List<SelectOption> getAgenciesUser(String parentID){
		List<SelectOption> agencyOptions = new List<SelectOption>();
		Set<String> ids = new Set<String>();
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && !user.Contact.Group_View_Permission__c){ //set the user to see only his own agency
			for(Account a: [Select Id, Name from Account where id =:user.Contact.AccountId order by name]){
				//agencyOptions.add(new SelectOption(a.Id, a.Name));
				ids.add(a.ID);
			}
			if(user.Aditional_Agency_Managment__c != null){
				for(Account ac:ipFunctions.listAgencyManagment(user.Aditional_Agency_Managment__c)){
					//agencyOptions.add(new SelectOption(ac.id, ac.name));
					ids.add(ac.ID);
				}
			}
		}else{
			for(Account ag : [Select Id, Name From Account Where Recordtype.Name = 'Agency' and ParentId = :parentID]){
				//agencyOptions.add(new SelectOption(ag.id, ag.Name));
				ids.add(ag.ID);
			}
		}
		//String nome;
		for(Account ag : [Select Id, Name From Account Where Recordtype.Name = 'Agency' AND Inactive__c = false AND ID IN :ids ORDER BY NAME]){
			//nome = ag.Name.contains('(HO)') ? 'Sydney' : ag.Name;
			agencyOptions.add(new SelectOption(ag.id, ag.Name));
		}

		if(agencyOptions.size() > 1){			
			agencyOptions.add(0, new SelectOption('all', 'All Agencies'));
		}
		return agencyOptions;
	}
}