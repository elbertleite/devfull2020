/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class extcampus_instalment_paymentDate_test {
	
	static testMethod void myUnitTest() {
		RecordType rt = [select r.Id,  r.Name from RecordType r where r.Name = 'Campus' and isActive = true LIMIT 1];
		Account account = new Account();
		account.RecordTypeId = rt.Id;
		account.Name = 'Campus CBD';
		insert account;
		

		Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(account);
		extcampus_instalment_paymentDate ipd = new extcampus_instalment_paymentDate(controller);
		
		ipd.loadInstalmentsPaymentDates();
		
		Campus_Instalment_Payment_Date__c cipd = new Campus_Instalment_Payment_Date__c();
		cipd.Instalment_Payment_Date__c = System.today();
		ipd.SelectedDatesList = '';
		cipd.Campus__c = account.Id;
		insert cipd;
		
		ipd.loadInstalmentsPaymentDates();
		
		ipd.SelectedDatesList += ',11/01/2011,12/01/2011,13/01/2011';
		
		string SavedDatesStringList = ipd.SavedDatesStringList;
		ipd.save();
		
		// HERE.
		
		ipd.SelectedDatesList = '';
		ipd.save();
		ipd.saveExtras();
		
	}


}