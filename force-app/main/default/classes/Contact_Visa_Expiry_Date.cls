global class Contact_Visa_Expiry_Date{
    global Contact_Visa_Expiry_Date(){}
}
/*global class Contact_Visa_Expiry_Date implements Database.Batchable<sObject> {
	
	String query;
	
	global Contact_Visa_Expiry_Date() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		query = 'SELECT Visa_country_applying_for__c, Expiry_Date__c, client__r.Destination_Country__c, client__r.Visa_Expiry_Date__c FROM Client_Document__c WHERE Document_type__c=\'Visa\' and Document_Category__c = \'Visa\' ORDER BY client__c';
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Client_Document__c> scope) {
		
		map<String, Date> contactDates = new map<String,Date>();
		for(Client_Document__c doc : scope){
			if(doc.Visa_country_applying_for__c == doc.Client__r.Destination_Country__c && (doc.client__r.Visa_Expiry_Date__c == NULL || doc.Expiry_Date__c > doc.client__r.Visa_Expiry_Date__c)){
				if(!contactDates.containsKey(doc.client__c))
					contactDates.put(doc.client__c, doc.Expiry_Date__c);
				
				else if(doc.Expiry_Date__c > contactDates.get(doc.client__c))
					contactDates.put(doc.client__c, doc.Expiry_Date__c);
				
				else continue;
			}
			else continue;
		}//end for


		list<Contact> updateContacts = new list<Contact>();
	
		for(String ctId : contactDates.keySet())
			updateContacts.add(new Contact(Id = ctId, Visa_Expiry_Date__c = contactDates.get(ctId)));

		update updateContacts;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}*/