@isTest
private class client_course_new_test {
	
	
	static testMethod void myUnitTest() {
       	TestFactory tf = new TestFactory();
       	
       	Account agencyGroup = tf.createAgencyGroup();
       
		Account agency = tf.createSimpleAgency(agencyGroup);
		
		Contact employee = tf.createEmployee(agency);
		
       	User portalUser = tf.createPortalUser(employee);
		
		Contact client = tf.createLead(agency, employee);
		
		Account school = tf.createSchool();
				
		Account campus = tf.createCampus(school, agency);
		    
		Course__c course = tf.createCourse();
		
		Campus_Course__c cc = tf.createCampusCourse(campus, course);
		
		Course__c course2 = tf.createCourse();
		
		Campus_Course__c cc2 = tf.createCampusCourse(campus, course);
		
		Commission__c commission = tf.createCommission(school);
		
		client_course__c clientCourseBooking = tf.createBooking(client);
		
		client_course__c clientCourse = tf.createClientCourse(client, school, campus, course, cc, clientCourseBooking);
		clientCourse.isPackage__c = true;
		
		client_course__c clientCourse2 = tf.createClientCourse(client, school, campus, course2, cc2, clientCourseBooking);
		clientCourse.course_package__c  = clientCourse.id;
		
		client_course__c clientCourse3 = tf.createClientCourse(client, school, campus, course2, cc2, clientCourseBooking);
		clientCourse.course_package__c  = clientCourse.id;
		
		tf.createClientCourseFees(clientCourse, false);
		tf.createClientCourseFees(clientCourse, true);
		
		tf.createClientCourseInstalments(clientCourse);
		
		client_product_service__c product = tf.createCourseProduct(clientCourseBooking, agency);
		
		Test.startTest();
		
		system.runAs(portalUser){
			ApexPages.currentPage().getParameters().put('id', client.id);
			ApexPages.currentPage().getParameters().put('ccId', clientCourse.id);
			ApexPages.Standardcontroller controller = new Apexpages.Standardcontroller(client);
			client_course_new ccn = new client_course_new(controller);
			
			ApexPages.currentPage().getParameters().put('feeName', 'Material Fee');
			ApexPages.currentPage().getParameters().put('installId', ccn.enrollCourse.listInstall[0].installment.id);
			ccn.deleteInstalmentFee();
			
			ccn.saveClientCourse();
			ccn.updateCoursePosEnrolment();
			ccn.cancelInstalmentValue();
			ccn.applyInstalmentTax();
			ccn.refreshSplit();
			
			ccn.deleteFees();
			
			Contact userDetails;
			IpFunctions.ContactNoSharing cns = new IpFunctions.ContactNoSharing(); 
	        userDetails = cns.getCurrentUser(employee.id);
			
			ccn.refreshCourse();
			
			ccn.listCourses();
			
			client_course_new.optionLists opt = new client_course_new.optionLists();
			
			opt.changeCountry();
			opt.changeSchool();
			opt.changeCampus();
			
			
			ApexPages.currentPage().getParameters().put('bookId', clientCourseBooking.id);
			ApexPages.currentPage().getParameters().put('prodId', product.id);
			ccn.deleteProduts();
			
			ApexPages.currentPage().getParameters().put('iId', ccn.enrollCourse.listInstall[0].installment.id);
			ccn.PDSPayment();
			
			List<SelectOption> optCountries = opt.getschoolCountries();
			opt.selectedCountry = optCountries[1].getValue();
			List<SelectOption> optSchools = opt.getSchoolsName();
			opt.selectedSchool = optSchools[1].getValue();
			
			
			List<SelectOption> optCampuses = opt.getCampusName();
			opt.selectedCampus = optCampuses[1].getValue();
			List<SelectOption> optCouses = opt.getCourseName();
			opt.selectedCourse = optCouses[1].getValue();
			
			List<SelectOption> unitType = opt.unitType;
			
			
			System.debug('==>opt.selectedCountry: '+opt.selectedCountry);
			System.debug('==>opt.selectedSchool: '+opt.selectedSchool);
			System.debug('==>opt.selectedCampus: '+opt.selectedCampus);
			System.debug('==>opt.selectedCourse: '+opt.selectedCourse);
			
			System.debug('==>opt.campusCity: '+opt.campusCity);
			ccn.opt = opt;
			
			// ccn.addCourse();
			// ccn.setAddNewCourse();
			// ccn.enrollCourse.course.Start_Date__c = system.today();
			// ccn.enrollCourse.course.End_Date__c = system.today();
			// ccn.enrollCourse.course.Price_per_Unit__c = 150;
			// ccn.enrollCourse.course.Total_Tuition__c = 1500;
			// ccn.enrollCourse.course.Course_Length__c = 36;
			// ccn.enrollCourse.course.Commission__c = 30;
			// ccn.saveClientCourse();

			
			// ApexPages.currentPage().getParameters().put('requestCOEParam', clientCourse.id);
			// ccn.confirmCourse();
			
			// ccn.updateCourseInstalmentsValue(ccn.enrollCourse);
			// ccn.updateKeepFeeValue();
			
			// ApexPages.currentPage().getParameters().put('openCourseID', clientCourse.id);
			// ccn.OpenCourse();
			
			// List<SelectOption> NumberOfUnits = ccn.getNumberOfUnits();
			// List<SelectOption> CommissionTypes = ccn.getCommissionTypes();
			
			
			
			// ApexPages.currentPage().getParameters().put('bookingId',clientCourseBooking.id);
			// ApexPages.currentPage().getParameters().put('coursePkgId',clientCourse.id);
			// ApexPages.currentPage().getParameters().put('packageId',clientCourse.id);
			// ApexPages.currentPage().getParameters().put('bookingName','');
			
			// ccn.setAddNewCoursePackage();
			// opt.retrievesearchDetails(clientCourse.id);
			
			// ccn.addItemInvoice();
			
			// ccn.clearListInvoice();
			// ccn.clearInvoiceCart();
			
			// ccn.addCourse();
			// ccn.setAddNewCourse();
			// ccn.enrollCourse.course.Start_Date__c = system.today();
			// ccn.enrollCourse.course.End_Date__c = system.today();
			// ccn.enrollCourse.course.Price_per_Unit__c = 150;
			// ccn.enrollCourse.course.Total_Tuition__c = 1500;
			// ccn.enrollCourse.course.Course_Length__c = 36;
			// ccn.enrollCourse.course.isPackage__c = true;
			// ccn.enrollCourse.course.Commission__c = 30;
			// ccn.saveClientCourse();
			
			// ccn.enrollPackage();
			
			// ccn.deleteCoursePackage();
			
			// ccn.addCourse();
			// ccn.setAddNewCourse();
			// ccn.enrollCourse.course.Start_Date__c = system.today();
			// ccn.enrollCourse.course.End_Date__c = system.today();
			// ccn.enrollCourse.course.Price_per_Unit__c = 150;
			// ccn.enrollCourse.course.Total_Tuition__c = 1500;
			// ccn.enrollCourse.course.Course_Length__c = 36;
			// ccn.enrollCourse.course.isPackage__c = true;
			// ccn.enrollCourse.course.Commission__c = 30;
			// ccn.saveClientCourse();
			
			// ccn.deleteCourse();
			
			// ccn.addCourse();
			// ccn.setAddNewCourse();
			// ccn.enrollCourse.course.Start_Date__c = system.today();
			// ccn.enrollCourse.course.End_Date__c = system.today();
			// ccn.enrollCourse.course.Price_per_Unit__c = 150;
			// ccn.enrollCourse.course.Total_Tuition__c = 1500;
			// ccn.enrollCourse.course.Course_Length__c = 36;
			// ccn.enrollCourse.course.isPackage__c = true;
			// ccn.enrollCourse.course.Commission__c = 30;
			// ccn.saveClientCourse();
			
			// ccn.confirmCourse();
			
			
			// List<SelectOption> aggOptions = ccn.agencyGroupOptions;
			// ccn.changeAgencyGroup();
			
			// List<SelectOption> agencyOptions = ccn.agencyOptions;
			// ccn.changeAgency();
			
			// List<SelectOption> courseOptions = ccn.courseOptions;
			// List<SelectOption> userOptions = ccn.userOptions;
			
			// List<SelectOption> paidAction = ccn.getpaidActionList();
			// List<SelectOption> instAction = ccn.getinstalmentActionList();
			// List<SelectOption> depositOptions = ccn.depositOptions;
			
			// ccn.getclientDeposit();
			
			// ApexPages.currentPage().getParameters().put('bookId', clientCourseBooking.id);
			// ApexPages.currentPage().getParameters().put('cc', clientCourse3.id);
			// ccn.migration();
			// ccn.addCourse();
			// ccn.setAddNewCourse();
			// ccn.enrollCourse.course.Start_Date__c = system.today();
			// ccn.enrollCourse.course.End_Date__c = system.today();
			// ccn.enrollCourse.course.Price_per_Unit__c = 150;
			// ccn.enrollCourse.course.Total_Tuition__c = 1500;
			// ccn.enrollCourse.course.Course_Length__c = 36;
			// ccn.enrollCourse.course.isPackage__c = true;
			// ccn.enrollCourse.course.Commission__c = 30;
			// ccn.saveClientCourse();
			
			// ccn.confirmCourse();
			
			
			// ccn.backtoList();
			
			// ccn.cancelUpdates();
			
			// ccn.setFirstCourse();
			
			
			ApexPages.currentPage().getParameters().put('courrseId', clientCourse2.id);
			ccn.deletePendingCourse();
			
			
			
			
			
			//Migration
			
		}
			
		
		
		
    }
    
    
}