public without sharing class configBooking {
    public configBooking() {

    }
    //  BOOKING CONFIGURATION 
	public user userBooking{
		get{
			if(userBooking == null){
				string ctId = ApexPages.currentPage().getParameters().get('id');
				// userBooking = [Select Id, Contact.Account.ParentId, Break_From__c, Break_To__c, Booking_Gap__c,  Work_From_Time__c, Work_To_Time__c, Days_Off__c, User_Description__c, (SELECT Time_Booking__c, Date_Booking__c, isBlocked__c FROM Clients_Booking__r where Date_Booking__c = :newBooking.Date_Booking__c) from User where Contact.AccountId = :agencyId and Contact.RecordType.name = 'Employee' and Contact.Department__c != null and isActive = true and Contact.Chatter_Only__c = false and Days_Off__c excludes (:selectedDay)]
				userBooking = [Select Id, Break_From__c, Break_To__c, Booking_Gap__c, Work_From_Time__c, Work_To_Time__c, Days_Off__c, User_Description__c from User where ContactId = :ctId limit 1];

				workFromHour = userBooking.Work_From_Time__c == null ? '09' : userBooking.Work_From_Time__c.split(':')[0];
				workFromMinute = userBooking.Work_From_Time__c == null ? '00' : userBooking.Work_From_Time__c.split(':')[1];

				workToHour = userBooking.Work_To_Time__c == null ? '18' : userBooking.Work_To_Time__c.split(':')[0];
				workToMinute = userBooking.Work_To_Time__c == null ? '00' : userBooking.Work_To_Time__c.split(':')[1];

				breakFromHour = userBooking.Break_From__c == null ? '00' : userBooking.Break_From__c.split(':')[0];
				breakFromMinute = userBooking.Break_From__c == null ? '00' : userBooking.Break_From__c.split(':')[1];

				breakToHour = userBooking.Break_To__c == null ? '00' : userBooking.Break_To__c.split(':')[0];
				breakToMinute = userBooking.Break_To__c == null ? '00' : userBooking.Break_To__c.split(':')[1];
				
				controlblockedAgenda();
			}
			return userBooking;
		}
		set;
	}

	public void saveBookingConfig(){
		userBooking.Work_From_Time__c = workFromHour+':'+workFromMinute;
		userBooking.Work_To_Time__c = workToHour+':'+workToMinute;
		userBooking.Break_From__c = breakFromHour+':'+breakFromMinute;
		userBooking.Break_To__c = breakToHour+':'+breakToMinute;
		update userBooking;
		userBooking = null;
		listBlockedTime = null;
		editConfig = false;
	}

	public void saveBookingBlock(){
		if(timeSelected != null && timeSelected != ''){
			
			list<Client_Booking__c> lb = new list<Client_Booking__c>();
			for(string st:timeSelected.split(','))
				lb.add(new Client_Booking__c(User__c = userBooking.id, Date_Booking__c = selectedBlockDate.Date_Booking__c, isBlocked__c = true, Time_Booking__c = Time.newInstance(integer.ValueOf(st.split(':')[0]),integer.ValueOf(st.split(':')[1]),0,0) ));
			if(lb.size() > 0)
				insert lb;

		}

		userBooking = null;
		listBlockedTime = null;
		editBlock = false;
	}

	public string workFromHour{get;set;}
	public string workFromMinute{get;set;}
	public string workToHour{get;set;}
	public string workToMinute{get;set;}

	public string breakFromHour{get;set;}
	public string breakFromMinute{get;set;}
	public string breakToHour{get;set;}
	public string breakToMinute{get;set;}

	public Client_Booking__c selectedBlockDate{
		get{
			if(selectedBlockDate == null){
				selectedBlockDate = new Client_Booking__c();
				selectedBlockDate.Date_Booking__c = System.today();
			}
			return selectedBlockDate;
		}
		set;
	}

	public class timeStyle{
		public string vl_time{get; set;}
		public string vl_id{get; set;}
		public Client_Booking__c selected{get{if(selected == null) selected = new Client_Booking__c(); return selected;} set;}
		public timeStyle(string vl_time, string vl_id, boolean selected){
			this.vl_time = vl_time;
			this.vl_id = vl_id;
			this.selected.isSelected__c = selected;
		}
	}

	public void deleteItems(){
		System.debug('==>listBlockedTime: '+listBlockedTime);
		list<string> itensDelete = new list<string>();
		for(date lb:listBlockedTime.keySet()){
			for(timeStyle ts:listBlockedTime.get(lb)){
				System.debug('==>ts: '+ts);
				if(ts.selected.isSelected__c){
					itensDelete.add(ts.vl_id);
				}
			}
		}
		if(itensDelete.size() > 0){
			delete [Select id from Client_Booking__c where id in :itensDelete];
			listBlockedTime = null;
			controlblockedAgenda();
		}
	}

	public map<date,list<timeStyle>> listBlockedTime{
		get{
			if(listBlockedTime == null){
				listBlockedTime = new map<date,list<timeStyle>>();
				for(Client_Booking__c cb:[SELECT Time_Booking__c, Date_Booking__c FROM Client_Booking__c where User__c = :userBooking.id and Date_Booking__c >= :System.today() and isBlocked__c = true]){
					if(!listBlockedTime.containsKey(cb.Date_Booking__c))
						listBlockedTime.put(cb.Date_Booking__c, new list<timeStyle>{new timeStyle(string.valueOf(cb.Time_Booking__c.hour()).leftPad(2,'0')+':'+string.valueOf(cb.Time_Booking__c.minute()).leftPad(2,'0'), cb.id, false)});
					else listBlockedTime.get(cb.Date_Booking__c).add(new timeStyle(string.valueOf(cb.Time_Booking__c.hour()).leftPad(2,'0')+':'+string.valueOf(cb.Time_Booking__c.minute()).leftPad(2,'0'), cb.id, false));
				}
			}
			return listBlockedTime;
		}
		set;
	}
	

	public map<string, string> blockedAgenda{get; set;}
	public string timeSelected{get; set;}
	public void controlblockedAgenda(){
		map<string, boolean> blockedTime = new map<string, boolean>();
		for(Client_Booking__c cb:[SELECT Time_Booking__c, Date_Booking__c, isBlocked__c FROM Client_Booking__c where User__c = :userBooking.id and Date_Booking__c = :selectedBlockDate.Date_Booking__c]){
			blockedTime.put(string.valueOf(cb.Time_Booking__c.hour()).leftPad(2,'0')+':'+string.valueOf(cb.Time_Booking__c.minute()).leftPad(2,'0'), cb.isBlocked__c);
		}

		blockedAgenda = new map<string, string>();

		time Break_From;
        time Break_To;
        
        boolean resetTime = false;
        if(userBooking.Break_From__c != null && userBooking.Break_To__c != null){
            Break_From = Time.newInstance(integer.ValueOf(userBooking.Break_From__c.split(':')[0]),integer.ValueOf(userBooking.Break_From__c.split(':')[1]),0,0);
            Break_To = Time.newInstance(integer.ValueOf(userBooking.Break_To__c.split(':')[0]),integer.ValueOf(userBooking.Break_To__c.split(':')[1]),0,0);
        }
		integer interval = 30;
        time Work_FromTime = time.newInstance(9,0,0,0);
        time Work_ToTime = time.newInstance(18,0,0,0);
        if(userBooking.Work_From_Time__c != null)
            Work_FromTime = time.newInstance(integer.ValueOf(userBooking.Work_From_Time__c.split(':')[0]),integer.ValueOf(userBooking.Work_From_Time__c.split(':')[1]),0,0);
        if(userBooking.Work_To_Time__c != null)
            Work_ToTime = time.newInstance(integer.ValueOf(userBooking.Work_To_Time__c.split(':')[0]),integer.ValueOf(userBooking.Work_To_Time__c.split(':')[1]),0,0);
        if(userBooking.Booking_Gap__c != null)
            interval = integer.valueOf(userBooking.Booking_Gap__c);

		string agendaTime = '';
		string agendaStatus = '';
        for(time i = Work_FromTime; i < Work_ToTime; i = i.addMinutes(interval) ){
            if((Break_From == null || Break_To == null) || (Break_From != null && Break_To != null &&  ((i < Break_From && i < Break_To) || (i >= Break_From && i >= Break_To)))){
				agendaTime = string.valueOf(i.hour()).leftPad(2,'0')+':'+string.valueOf(i.minute()).leftPad(2,'0');
				if(blockedTime.containsKey(agendaTime))
					agendaStatus = blockedTime.get(agendaTime) ? 'blocked' : 'booked';
				else agendaStatus = 'free';
                blockedAgenda.put(agendaTime, agendaStatus);
            }else if(Break_To != null && i <= Break_To && !resetTime){
                i = Break_To;
                i = i.addMinutes(-interval);
                resetTime = true;
            }
          
         
        }
	}

	public List<SelectOption> listHour {
		get{
			if(listHour == null){

				listHour = new List<SelectOption>();
				for (integer i = 0; i <= 23; i++)
					listHour.add(new SelectOption(string.valueof(i).leftPad(2,'0'),string.valueof(i).leftPad(2,'0')));
			}
			return listHour;
		}
		set;
	}
	public List<SelectOption> listMinutes {
		get{
			if(listMinutes == null){

				listMinutes = new List<SelectOption>();
				for (integer i = 0; i <= 59; i++)
					listMinutes.add(new SelectOption(string.valueof(i).leftPad(2,'0'),string.valueof(i).leftPad(2,'0')));
			}
			return listMinutes;
		}
		set;
	}

	public boolean editConfig{get{if(editConfig == null) editConfig = false; return editConfig;} set;}
	public boolean editBlock{get{if(editBlock == null) editBlock = false; return editBlock;} set;}

	public void setEditConfig(){
		editConfig = true;
		editBlock = false;
	}
	public void setEditBlock(){
		editBlock = true;
		editConfig = false;
	}

	public void cancelConfig(){
		editConfig = false;
		userBooking = null;
		listBlockedTime = null;
	}

	public void cancelBlock(){
		editBlock = false;
		userBooking = null;
		listBlockedTime = null;
	}


}