public without sharing class commissions_pds_confirmation {

	private String invoiceID {get;set;}

	public list<Result> instResult {get;set;}
	public boolean hasExtraFee {get;set;}
	public boolean hasKeepFee {get;set;}
	public Account agencyDetails {get;set;}
	public Invoice__c invoice {get;set;}
	private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}
	public List<SelectOption> commissionStatus {get{
		if(commissionStatus==null){
			commissionStatus = new List<SelectOption>();

			Schema.DescribeFieldResult fieldResult = client_course_instalment__c.School_Request_Confirmation_Status__c.getDescribe();
	    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
	    for( Schema.PicklistEntry f : ple){
				// if(f.getLabel()=='Confirmed')
	    	// 	commissionStatus.add(new SelectOption(f.getValue(), 'Yes'));
				// else if(f.getLabel()=='Refused')
	    	// 	commissionStatus.add(new SelectOption(f.getValue(), 'No'));
				// else if(f.getLabel()=='Unsure')
	    	// 	commissionStatus.add(new SelectOption(f.getLabel(), 'I don\''+'t know yet'));
				// else
					commissionStatus.add(new SelectOption(f.getValue(), f.getLabel()));
	    }
		}
		return commissionStatus;
	}set;}


	//Constructor
	public commissions_pds_confirmation() {
		invoiceID = ApexPages.currentPage().getParameters().get('i');

		if(!Test.isRunningTest()){

			Blob invId = EncodingUtil.base64Decode(invoiceID);
			Blob key = Blob.valueOf('380db410e8b11fa9');
			system.debug('invoiceID===>' + invoiceID);
			system.debug('invId==>' + invId);

			Blob decrypted = Crypto.decryptWithManagedIV('AES128', key, invId);
			invoiceID = decrypted.toString();
			system.debug('decrypted.toString()===>' + decrypted.toString());
		}

		invoice = [SELECT School_Name__c, Request_Confirmation_Number__c, Due_Date__c, createdDate, Sent_By__c, Sent_By__r.Contact.AccountId, Sent_Email_To__c, Request_Completed_by_School_On__c, Invoice_Activities__c, Status__c, School_Id__c FROM Invoice__c WHERE id = :invoiceID];

		//Get Invoice Instalments
		try{
			list<client_course_instalment__c> result = findInvInstalments();
			buildResult(result);
		}
		catch(Exception e ){
			instResult = new list<Result>();
			system.debug('Error==>' + e.getMessage());
			system.debug('Line==>' + e.getLineNumber());
		}

		//Get Agency logo and information
		try{
			agencyDetails = [SELECT Id, Name, Logo__c, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode, Registration_name__c, Trading_Name__c,
			 												Business_Number__c, Type_of_Business_Number__c

															FROM Account WHERE Id = :invoice.Sent_By__r.Contact.AccountId limit 1];
		}
		catch(Exception e){
			agencyDetails = new Account();
			system.debug('Error==>' + e.getMessage());
			system.debug('Line==>' + e.getLineNumber());
		}

	}

	//Get Invoice Instalments
	public list<client_course_instalment__c> findInvInstalments(){
		return [SELECT id, Name, Instalment_Value__c, Due_Date__c, Related_Fees__c, Scholarship_Taken__c, Commission__c,  Commission_Value__c, Tuition_Value__c, Received_By_Agency__c, Received_By_Agency__r.Name,
										Received_By__r.Name, Total_School_Payment__c, Number__c, Commission_Tax_Value__c, Commission_Tax_Rate__c, Received_Date__c, Commission_Due_Date__c, 
										Split_Number__c, Agent_Deal_Name__c, Agent_Deal_Value__c, Related_Promotions__c, School_Request_Confirmation_Status__c, instalment_activities__c, Paid_PDS_Value__c,
										School_PDS_Notes__c, PDS_Confirmed_On__c,
										isInstalment_Amendment__c, Amendment_Related_Fees__c,

										client_course__r.Commission_Tax_Name__c,
										client_course__r.CurrencyIsoCode__c,
										client_course__r.Commission_Type__c,
										client_course__r.Start_Date__c,
										client_course__r.client__c,
										client_course__r.School_Student_Number__c,
										client_course__r.client__r.Name,
										client_course__r.client__r.Birthdate,
										client_course__r.Course_Name__c,
										client_course__r.Campus_Id__c,
										client_course__r.Campus_Name__c

										FROM client_course_instalment__c WHERE PDS_Confirmation_Invoice__c = :invoiceID
											order by Due_Date__c, client_course__r.Client__r.Name];
	}

	//Build Result List
	private void buildResult(list<client_course_instalment__c> result){
		instResult = new list<Result>();

		Instalment i;
		//courseDetails course;
		map<string, result> resultMap = new map<string,result>();
		for(client_course_instalment__c cci : result){
			i = new instalment();
			i.inst = cci;
			i.previousStatus = cci.School_Request_Confirmation_Status__c;
			i.previousNotes = cci.School_PDS_Notes__c;
			/** F E E S **/
			if(cci.Related_Fees__c!=null && cci.Related_Fees__c!=''){
				for(string lf:cci.Related_Fees__c.split('!#')){

					/** Original Fees  **/
					if(lf.split(';;').size() >= 7){ //It's amended

						i.fees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[6]), false,  decimal.valueOf(lf.split(';;')[7])));

						if(decimal.valueOf(lf.split(';;')[7])>0){ // Has keep fee
							i.KeepFees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[6]), false,  decimal.valueOf(lf.split(';;')[7])));
							i.totalKeepFee += decimal.valueOf(lf.split(';;')[7]);
						}

						else if(decimal.valueOf(lf.split(';;')[7])<0){ // Decreased keep fee
							i.KeepFees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[6]), false,  decimal.valueOf(lf.split(';;')[7]) + decimal.valueOf(lf.split(';;')[5])));
							i.totalKeepFee += decimal.valueOf(lf.split(';;')[7]);
						}
							i.totalFees +=	decimal.valueOf(lf.split(';;')[6]);
					}
					else {
						i.fees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2]), false,  decimal.valueOf(lf.split(';;')[5])));
						i.totalFees +=	decimal.valueOf(lf.split(';;')[2]);

						if(decimal.valueOf(lf.split(';;')[5])>0){ // Has keep fee
							i.KeepFees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2]), false,  decimal.valueOf(lf.split(';;')[5])));
							i.totalKeepFee += decimal.valueOf(lf.split(';;')[5]);
						}
					}
				}//end for
			}

			/** Amended Fees **/
			if(cci.Amendment_Related_Fees__c!=null && cci.Amendment_Related_Fees__c!=''){
				for(string lf:cci.Amendment_Related_Fees__c.split('!#')){

					i.fees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2]), false,  decimal.valueOf(lf.split(';;')[5])));
					i.totalFees +=	decimal.valueOf(lf.split(';;')[2]);
					if(decimal.valueOf(lf.split(';;')[5])>0){ // Has keep fee
						i.KeepFees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2]), false,  decimal.valueOf(lf.split(';;')[5])));
						i.totalKeepFee += decimal.valueOf(lf.split(';;')[5]);
					}
				}//end for
			}

			//Flag to show Fees Columns
			if(i.fees.size()>0)
				hasExtraFee = true;
			if(i.KeepFees.size()>0)
				hasKeepFee = true;

			//Group by Campus and Course
			if(!resultMap.containsKey(cci.client_course__r.Campus_Id__c)){ //New Campus
				// course = new courseDetails(cci.client_course__r.Course_Name__c, i);
				resultMap.put(cci.Client_Course__r.Campus_Id__c, new result(cci.Client_Course__r.Campus_Id__c, cci.client_course__r.Campus_Name__c, i));
			}
			else{

				resultMap.get(cci.Client_Course__r.Campus_Id__c).addInstalment(i);
				// result res = resultMap.get(cci.Client_Course__r.Campus_Id__c);
				//
				// if(!res.mapCourses.containsKey(cci.client_course__r.Course_Name__c)){ //New Course
				// 	res.mapCourses.put(cci.client_course__r.Course_Name__c, new courseDetails(cci.client_course__r.Course_Name__c, i));
				// }
				// else{ //Add Instalment to Course
				// 	res.mapCourses.get(cci.client_course__r.Course_Name__c).addInstalment(i);
				// }
				// resultMap.put(res.campusId, res);
			}

		}//end for

		result rm;
		for(String cp : resultMap.keySet()){
			// rm = resultMap.get(cp);
			// rm.setListCourses();

			instResult.add(resultMap.get(cp));
		}//end for

	}

	//School is not sure about the PDS ===> I Don't Know yet
	public void sendBackToRequest(list<client_course_instalment__c> unsure){
		Savepoint sp;
		try{
			sp = Database.setSavepoint();
			String activity = 'Sent back to Request Confirmation ;;-;;' + 'School is not sure about payment - Original Request Confirmation ('+ invoice.Request_Confirmation_Number__c +');;' + Datetime.now().format('yyyy-MM-dd HH:mm:ss') + ';;sent;;-;;' + UserInfo.getUserName() + '!#!';

			for(client_course_instalment__c cci : unsure){
				cci.isSelected__c = false;
				cci.instalment_activities__c += activity;
				//cci.School_Request_Confirmation_Status__c = null;
				cci.PDS_Confirmation_Invoice__c = null;
				cci.status__c = 'Sent back to request';
				cci.PDS_Requested_By__c = null;
				cci.PDS_Requested_On__c = null;
			}//end for

			update unsure;

		}
		catch(Exception e){
			system.debug('Error===>' + e.getMessage());
			system.debug('Error Line ===>' + e.getLineNumber());
			Database.rollback(sp);
		}
	}


	//Send Request Form
	public boolean hasError {get{if(hasError==null) hasError = false; return hasError;}set;}
	public void sendRequestForm(){
		// list<client_course_instalment__c> instalments = findInvInstalments(); //Double check if form is 100% filled

		hasError = false;
		boolean isFilled = true;
		integer totConfirmed = 0;
		integer totRefused = 0;
		integer totUnsure = 0;

		list<String> cancelPDS = new list<String>();
		list<client_course_instalment__c> confAndRef = new list<client_course_instalment__c>();
		list<client_course_instalment__c> paidDiff = new list<client_course_instalment__c>();
		// list<client_course_instalment__c> unsure = new list<client_course_instalment__c>();

		for(Result r : instResult)
			for(Instalment i : r.instalments){

				//Paid Different Amount
				if(i.inst.School_Request_Confirmation_Status__c =='Paid_different_amount'){

					if(i.inst.Paid_PDS_Value__c == NULL || i.inst.Paid_PDS_Value__c == 0){
						i.inst.Paid_PDS_Value__c.addError('Paid amount is required.');
						hasError = true;
						i.hasError = true;
					}
					else{
						i.hasError = false;
						totConfirmed++;
						i.inst.PDS_Confirmed__c = true;
						confAndRef.add(i.inst);
						paidDiff.add(i.inst);
					}
				}
				//Yes
				else if(i.inst.School_Request_Confirmation_Status__c =='Confirmed'){
					totConfirmed++;
					i.inst.PDS_Confirmed__c = true;
					i.inst.Paid_PDS_Value__c = null;
					i.inst.instalment_activities__c += 'Request Confirmation Completed by School ;;-;;' + 'PDS confirmed by the school' +';;' + Datetime.now().format('yyyy-MM-dd HH:mm:ss') + ';;sent;;-;;' + UserInfo.getUserName() + '!#!';
					confAndRef.add(i.inst);
				}

				//No
				else if (i.inst.School_Request_Confirmation_Status__c =='Refused'){
					totRefused ++;
					i.inst.Paid_PDS_Value__c = null;
					i.inst.PDS_Confirmed__c = false;
					i.inst.instalment_activities__c += 'Request Confirmation Completed by School ;;-;;' + 'PDS refused by the school' +';;' + Datetime.now().format('yyyy-MM-dd HH:mm:ss') + ';;sent;;-;;' + UserInfo.getUserName() + '!#!';
					confAndRef.add(i.inst);
				}

				//Course Cancelled & Commission Paid
				else if (i.inst.School_Request_Confirmation_Status__c =='course_cancelled' || i.inst.School_Request_Confirmation_Status__c == 'Commission_paid'){
					// totRefused ++;
					cancelPDS.add(i.inst.id);
				}

				else{
					isFilled = false;
					break;
				}
			}//end for

		if(!isFilled)
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You must select an option for all instalments before you send back the form.'));

		else if(hasError){}

		else{
			Savepoint sp;
			try{
				sp = Database.setSavepoint();
				String activity = 'Request Confirmation Completed by School ;;-;;' + 'Request Confirmation Completed by School' +';;' + Datetime.now().format('yyyy-MM-dd HH:mm:ss') + ';;sent;;-;;' + UserInfo.getUserName() + '!#!';

				invoice.Request_Completed_by_School_On__c = system.now();
				invoice.Invoice_Activities__c += activity;
				invoice.Total_Confirmed__c = totConfirmed;
				invoice.Total_Refused__c = totRefused;
				// invoice.Total_Unsure__c = totUnsure;
				invoice.Status__c = 'Request Completed by School';
				update confAndRef;
				// sendBackToRequest(unsure);
				// update instalments;

				if(invoice.Total_Refused__c==0){ //No Actions Left on Invoice (Close Request)
					invoice.Request_Confirmation_Completed_On__c = system.now();
					invoice.Request_Confirmation_Completed_by__c = UserInfo.getUserId();
					invoice.Status__c = 'Completed';
				}
				update invoice;

				//Create not for Paid Different Amount
				list<Custom_Note_Task__c> notes = new list<Custom_Note_Task__c>();
				String noteDesc;
				for(client_course_instalment__c cci : paidDiff){
					noteDesc = 'School Confirmed Student Paid Different amount $' + cci.Paid_PDS_Value__c;

					if(cci.School_PDS_Notes__c != null && cci.School_PDS_Notes__c.length()>0)
						noteDesc += '.<br/>School Notes:'+cci.School_PDS_Notes__c;

					notes.add(new Custom_Note_Task__c(isNote__c=true, Instalment__c = cci.id, Related_Contact__c = cci.Client_Course__r.Client__c, Comments__c= noteDesc, Subject__c = 'Additional Information'));
				}//end for

				if(notes.size()>0)
					insert notes;

				//Cancel PDS with status --> Course Cancelled and Commission Paid
				if(cancelPDS.size()>0){
					ff.cancelPayment(new list<String>(cancelPDS), null, null, true);
				}

				String body = label.PDS_Confirmation_Answered;
				body += '<br/><div style="margin-top: 20px;" class="sig">'+getSignature()+'</div>';


				//Send email to School and Invoice "owner"
				mandrillSendEmail mse = new mandrillSendEmail();
				mandrillSendEmail.messageJson email_md = new mandrillSendEmail.messageJson();

				DateTime currentTime = DateTime.now();
				String emailHifyId = '' + currentTime.getTime();
				
				email_md.setFromEmail(userDetails.Email);
				email_md.setFromName(userDetails.Name);
				String emailService = IpFunctions.getS3EmailService();
				email_md.setSubject(invoice.Request_Confirmation_Number__c + ' - PDS Confirmation Completed');
				email_md.setTo(invoice.Sent_Email_To__c, '');
				email_md.setTag(invoice.Sent_Email_To__c);
				email_md.setCc(userDetails.Email, '');
				email_md.setHtml(body);
				email_md.setMetadata('hify_id', emailHifyId);
				
				PageReference pr = Page.payment_school_request_confirmation;
				pr.getParameters().put('i', string.valueOf(invoice.id));
				pr.getParameters().put('result', 'show'); //show results
				blob fileBody;
				if(!Test.isRunningTest()){
					fileBody = pr.getContentAsPDF();
					email_md.setAttachment('application/pdf', invoice.Request_Confirmation_Number__c + ' - PDS Confirmation Completed.pdf', fileBody);
				}
				mse.sendMail(email_md);

				String myDate = currentTime.format('dd-MM-yyyy HH:mm:ss');
				EmailToS3Controller s3 = new EmailToS3Controller();
				s3.reqConfirmEmailS3(Blob.valueof(body), fileBody, invoice.Request_Confirmation_Number__c + ' - PDS Confirmation Completed', invoice.School_Id__c, myDate,invoice.Sent_By__c, invoice.Request_Confirmation_Number__c,invoice.Sent_Email_To__c, emailHifyId);
			}
			catch(Exception e){
				invoice.Request_Completed_by_School_On__c = null;
				invoice.Total_Confirmed__c = null;
				invoice.Total_Refused__c = null;
				invoice.Total_Unsure__c = null;
				invoice.Status__c = null;
				Database.rollback(sp);
				system.debug('Error==>' + e.getMessage());
				system.debug('Line==>' + e.getLineNumber());
				// ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error==>' + e.getMessage() + '       Line==>' + e.getLineNumber()));
			}
		}
	}

	//Save changes
	public void saveAll(){
		list<client_course_instalment__c> toUpdate = new list<client_course_instalment__c>();
		for(Result rs : instResult){
			// for(courseDetails cd: rs.courses)
			for(Instalment i : rs.instalments){
				if(i.inst.School_Request_Confirmation_Status__c != i.previousStatus || i.inst.School_PDS_Notes__c != i.previousNotes){
					i.inst.PDS_Confirmed_On__c = system.now();
					i.inst.PDS_Confirmed_By__c = UserInfo.getUserId();

					i.previousStatus = i.inst.School_Request_Confirmation_Status__c;
					i.previousNotes = i.inst.School_PDS_Notes__c;

					toUpdate.add(i.inst);
				}
			}

			if(toUpdate.size()>0)
				update toUpdate;

			if(invoice.Status__c!='Waiting Response'){
				invoice.Status__c='Waiting Response';
				update invoice;
			}
		}
	}



	/********************** Inner Classes **********************/
	//Result by Campus
	public class result{
		public String campusId {get;set;}
		public String campusName {get;set;}
		public list<Instalment> instalments {get{if(instalments==null) instalments = new list<Instalment>(); return instalments;}set;}
		//public list<courseDetails> courses {get;set;}
		// private map<string, courseDetails> mapCourses {get{if(mapCourses==null) mapCourses = new map<string,courseDetails>(); return mapCourses;}set;}


		private result(String campusId, String campusName, instalment i){
			this.campusId = campusId;
			this.campusName = campusName;
			this.instalments.add(i);
		}

		private void addInstalment(instalment i){
			this.instalments.add(i);
		}

		// private result(String campusId, String campusName,String courseName, courseDetails course){
		// 	this.campusId = campusId;
		// 	this.campusName = campusName;
		// 	this.mapCourses.put(courseName, course);
		// }

		// private void setListCourses(){
		// 	courses = new list<courseDetails>();
		//
		// 	for(String c: mapCourses.keySet())
		// 		courses.add(mapCourses.get(c));
		// }

	}

	//Course Details
	// public class courseDetails{
	// 	public String courseName {get;set;}
	// 	public list<Instalment> instalments {get{if(instalments == null) instalments = new list<Instalment>(); return instalments;}set;}
	//
	// 	private courseDetails(String courseName, Instalment inst){
	// 		this.courseName = courseName;
	// 		this.instalments.add(inst);
	// 	}
	//
	// 	private void addInstalment(Instalment inst){
	// 		this.instalments.add(inst);
	// 	}
	// }

	//Instalment Details
	public class Instalment{
		public client_course_instalment__c inst {get;set;}
		public boolean hasError {get{if(hasError==null) hasError = false; return hasError;}set;}
		public list<feeInstallmentDetail> fees {get{if(fees == null) fees = new list<feeInstallmentDetail>(); return fees;}set;}
		public list<feeInstallmentDetail> keepFees {get{if(keepFees == null) keepFees = new list<feeInstallmentDetail>(); return keepFees;}set;}
		public decimal totalFees {get{if(totalFees==null) totalFees = 0; return totalFees;}set;}
		public decimal totalKeepFee {get{if(totalKeepFee==null) totalKeepFee = 0; return totalKeepFee;}set;}
		public String previousStatus {get;set;}
		public String previousNotes {get;set;}
	}

	//Fees Details
	public class feeInstallmentDetail{
		public string feeName{get; set;}
		public decimal feeValue{get; set;}
		public boolean isPromotion{get; set;}
		public decimal keepFeeValue{get; set;}

		public feeInstallmentDetail(string feeName, decimal feeValue, boolean isPromotion, decimal keepFeeValue){
			this.feeName = feeName;
			this.feeValue = feeValue;
			this.isPromotion = isPromotion;
			this.keepFeeValue = keepFeeValue;
		}
	}


	private Contact userDetails{
		get{
			if(userDetails == null){
				if(!Test.isRunningTest()){
					string contactId = [Select ContactId from user where id = :invoice.Sent_By__c limit 1].ContactId;
					userDetails = [select id, Name, Email, Signature__c, Account.Name, Account.ParentId, Account.Global_Link__c from Contact where id = :contactId];
				} else {
					userDetails = new Contact();
				}
			}
			return userDetails;
		}
		set;}

	/** Signature **/
	public string getSignature(){
	    if(userDetails.Signature__c != null && userDetails.Signature__c != '' )
	      return userDetails.Signature__c;
	    else return textSignature;
	}

	public String textSignature {
  	get{
  		if(textSignature == null){
  			textSignature = '<div style="margin-top: 20px;" class="sig">' + userdetails.Name;
            textSignature += '<br />' + userDetails.Account.Name;
            textSignature += '<br />' + userDetails.Email + ' </div>';
  		}
  		return textSignature;
  	}
  	set;}

	//TODO: Delete after deploy
	public list<SelectOption> campusOptions{get;set;}
}