/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class client_course_instalment_receipt_test {

    static testMethod void myUnitTest() {
         
       TestFactory tf = new TestFactory();
       
       Account school = tf.createSchool();
       Account agency = tf.createAgency();
       Contact emp = tf.createEmployee(agency);
       User portalUser = tf.createPortalUser(emp);
       Account campus = tf.createCampus(school, agency);
       Course__c course = tf.createCourse();
       Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
       
       Test.startTest();
       system.runAs(portalUser){
       	   Contact client = tf.createLead(agency, emp);
	       client_course__c booking = tf.createBooking(client);
	       client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       cc.Enroled_by_User__c = UserInfo.getUserId();
	       update cc;
	       
	       List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
	       
	       client_course_instalment_pay instPay = new client_course_instalment_pay(new ApexPages.StandardController(instalments[0]));
	       instPay.newPayDetails.Value__c = 2230;
		   instPay.newPayDetails.Date_Paid__c = system.today();
		   instPay.newPayDetails.Payment_Type__c = 'Cash';
		   instPay.addPaymentValue();
		   instPay.savePayment();
	       
	       client_product_service__c product =  tf.createCourseProduct(booking, agency);
		   client_product_service__c product2 =  tf.createCourseProduct(booking, agency);
		   client_product_service__c product3 =  tf.createCourseProduct(booking, agency);
	       
	       client_course_product_pay prodPay = new client_course_product_pay(new ApexPages.StandardController(product2));
	       prodPay.newPayDetails.Value__c = 50;
		   prodPay.newPayDetails.Payment_Type__c = 'CreditCard';
		   prodPay.addPaymentValue();
		   prodPay.savePayment();
		   
		   ApexPages.currentPage().getParameters().put('cs', instalments[3].id);
	       ApexPages.currentPage().getParameters().put('pd', product.id);
	       ApexPages.currentPage().getParameters().put('ct', client.id);
	       
	       client_course_create_invoice invoicePay = new client_course_create_invoice();
	       invoicePay.createInvoice();
	       invoicePay.newPayDetails.Value__c = 1600;
		   invoicePay.newPayDetails.Payment_Type__c = 'Creditcard';
		   invoicePay.addPaymentValue();
		   invoicePay.savePayment();
	       
	       String invoiceId = invoicePay.invoice.id;
	       Invoice__c updateInvoice = new Invoice__c(id = invoiceId, Instalments__c = instalments[3].id +';');
		   update updateInvoice;
		   
	       ApexPages.currentPage().getParameters().put('inst', instalments[0].id);
	       client_course_instalment_receipt testClass = new client_course_instalment_receipt(new ApexPages.StandardController(instalments[0]));
	       
	       
	       ApexPages.currentPage().getParameters().remove('inst');
	       ApexPages.currentPage().getParameters().put('prod', product2.id);
	       testClass = new client_course_instalment_receipt(new ApexPages.StandardController(instalments[0]));
	       
	       ApexPages.currentPage().getParameters().remove('prod');
	       ApexPages.currentPage().getParameters().put('inv', invoiceId);
	       testClass = new client_course_instalment_receipt(new ApexPages.StandardController(instalments[0]));
	       
	       
	       
	       
	       client_course_instalment_amendment amendmentClass = new client_course_instalment_amendment(new ApexPages.StandardController(instalments[2]));
		   amendmentClass.newTuition = amendmentClass.instalment.Tuition_value__c + 100; //Change tuition > original tuition
	       amendmentClass.saveAmendment();
	       
	       client_course_instalment__c amendmentInst = amendmentClass.amendment;
	       
	       client_course_amendment_pay amendPay = new client_course_amendment_pay(new ApexPages.StandardController(amendmentInst));
	       amendPay.newPayDetails.Value__c = amendmentInst.Instalment_Value__c;
		   amendPay.newPayDetails.Date_Paid__c = system.today();
		   amendPay.newPayDetails.Payment_Type__c = 'Cash';
		   amendPay.addPaymentValue();
		   amendPay.savePayment();
	       
	       ApexPages.currentPage().getParameters().remove('inst');
	       ApexPages.currentPage().getParameters().put('inst', amendmentInst.id);
	       testClass = new client_course_instalment_receipt(new ApexPages.StandardController(amendmentInst));
	       
	       
       }
       Test.stopTest();
        
    }
}