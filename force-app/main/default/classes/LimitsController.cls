public without sharing class LimitsController {
	
	
	public Integer getQueries() { return Limits.getQueries(); }
	public Integer getLimitQueries() { return Limits.getLimitQueries(); }
	public Decimal getQueryPerc() { return (Decimal.valueOf(getQueries()) / Decimal.valueOf(getLimitQueries())) * 100; }
	
	public Integer getQueryRows() { return Limits.getQueryRows(); }
	public Integer getLimitQueryRows() { return Limits.getLimitQueryRows(); }
	public Decimal getQueryRowsPerc() { return (Decimal.valueOf(getQueryRows()) / Decimal.valueOf(getLimitQueryRows())) * 100; }
	
	public Integer getDmlStatements() { return Limits.getDmlStatements(); }
	public Integer getLimitDmlStatements() { return Limits.getLimitDmlStatements(); }
	public Decimal getDmlStatementsPerc() { return (Decimal.valueOf(getDmlStatements()) / Decimal.valueOf(getLimitDmlStatements())) * 100; }
	
	public Integer getDmlRows() { return Limits.getDmlRows(); }
	public Integer getLimitDmlRows() { return Limits.getLimitDmlRows(); }
	public Decimal getDmlRowsPerc() { return (Decimal.valueOf(getDmlRows()) / Decimal.valueOf(getLimitDmlRows())) * 100; }
	
	//public Integer getScriptStatements() { return Limits.getScriptStatements(); }
	//public Integer getLimitScriptStatements() { return Limits.getLimitScriptStatements(); }
	//public Decimal getScriptStatementsPerc() { return (Decimal.valueOf(getScriptStatements()) / Decimal.valueOf(getLimitScriptStatements())) * 100; }
	
	public Integer getCpuTime() { return Limits.getCpuTime(); }
	public Integer getLimitCpuTime() { return Limits.getLimitCpuTime(); }
	public Decimal getCpuTimePer() { return (Decimal.valueOf(getCpuTime()) / Decimal.valueOf(getLimitCpuTime())) * 100; }
	
	public Integer getHeapSize() {return Limits.getHeapSize();}
	public Integer getLimitHeapSize() {return Limits.getLimitHeapSize();}
	public Decimal getLimitHeapSizePer() { return (Decimal.valueOf(getHeapSize()) / Decimal.valueOf(getLimitHeapSize())) * 100; }

}