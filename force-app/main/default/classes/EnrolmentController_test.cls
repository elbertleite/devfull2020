@isTest
private class EnrolmentController_test {

	
		public static TestFactory tf = new TestFactory();

		public static Account agencyGroup = tf.createAgencyGroup();

		public static Account agency = tf.createSimpleAgency(agencyGroup);

		public static Contact employee = tf.createEmployee(agency);

		public static  User portalUser = tf.createPortalUser(employee);



		public static Account school = tf.createSchool();

		public static Account campus = tf.createCampus(school, agency);

		public static Course__c course = tf.createCourse();

		public static Campus_Course__c cc = tf.createCampusCourse(campus, course);

		public static Course__c course2 = tf.createCourse();

		public static Campus_Course__c cc2 = tf.createCampusCourse(campus, course);

		public static Commission__c commission = tf.createCommission(school);

	

	static testMethod void firstHalf(){

		Test.startTest();

		system.runAs(portalUser){

			Contact client = tf.createLead(agency, employee);
			client.Owner__c = portalUser.id;
			update client;
			client_course__c clientCourseBooking = tf.createBooking(client);

			client_course__c clientCourse = tf.createClientCourse(client, school, campus, course, cc, clientCourseBooking);

			client_course__c clientCourse2 = tf.createClientCourse(client, school, campus, course2, cc2, clientCourseBooking);

			client_course__c clientCourse3 = tf.createClientCourse(client, school, campus, course2, cc2, clientCourseBooking);
		List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(clientCourse3);


			EnrolmentController ec = new EnrolmentController();
			List<SelectOption> dummy = ec.agencyGroupOptions;
			ec.changeGroup();
			dummy = ec.schoolFilter;
			ec.changeSchool();
			dummy = ec.campusFilter;
			ec.selectedDepartment = 'all';
			ec.search();

			ApexPages.currentPage().getParameters().put('courseid', clientCourse3.id);

			ApexPages.currentPage().getParameters().put('clientId', clientCourse3.Client__c);
			ApexPages.currentPage().getParameters().put('noteSubj', 'note subject');
			ApexPages.currentPage().getParameters().put('noteDesc', 'note description');
			ApexPages.currentPage().getParameters().put('noteComments', 'note description');
			ApexPages.currentPage().getParameters().put('firstInst', instalments[0].id);
			ApexPages.currentPage().getParameters().put('courseRedirect', 'true');

			ec.createCourseNote();
			ec.requestCOE();
			ec.advSignLOO();
			ec.requestLOO();
			ec.moveToWaitingVisa();
			ec.saveVisa();
			ec.updateVisa();
			ec.confirmVisaEnrolment();
			ec.enrol();
			ec.deleteCourse();
			dummy = ec.countries;

			ec.ENROLMENT_TYPE = ec.TYPE_LOO_REQUESTED;
			ec.search();
			ec.ENROLMENT_TYPE = ec.TYPE_LOO_RECEIVED;
			ec.search();
			ec.ENROLMENT_TYPE = ec.TYPE_LOO_CONFIRM_SIGNED;
			ec.search();
			ec.ENROLMENT_TYPE = ec.TYPE_AWAITING_PAYMENT;
			ec.search();
		}

		Test.stopTest();
	}
	static testMethod void secondHalf(){

		Test.startTest();

		system.runAs(portalUser){
			EnrolmentController ec = new EnrolmentController();
			List<SelectOption> dummy = ec.agencyGroupOptions;

			ec.ENROLMENT_TYPE = ec.TYPE_COE_REQUESTED;
			ec.search();
			ec.ENROLMENT_TYPE = ec.TYPE_COE_RECEIVED;
			ec.search();
			ec.ENROLMENT_TYPE = ec.TYPE_VISA;
			ec.search();
			ec.ENROLMENT_TYPE = ec.TYPE_ENROLLED;
			ec.search();
			ec.ENROLMENT_TYPE = ec.TYPE_CANCELLED;
			ec.search();
			ec.ENROLMENT_TYPE = ec.TYPE_URGENT;
			ec.search();
			ec.ENROLMENT_TYPE = ec.TYPE_MISSING_INFORMATION;
			ec.search();
			ec.ENROLMENT_TYPE = ec.TYPE_VISA_EXPIRING;
			ec.search();
			ec.ENROLMENT_TYPE = ec.TYPE_TRAVEL_DATE;
			ec.search();
			ec.ENROLMENT_TYPE = ec.TYPE_WAITING_CLI_PAYMENT;
			ec.search();
			ec.ENROLMENT_TYPE = ec.TYPE_PAID_BY_CLIENT;
			ec.search();

			String types = ec.TYPE_LOO_REQUESTED;
			types = ec.TYPE_LOO_RECEIVED;
			types = ec.TYPE_LOO_CONFIRM_SIGNED;
			types = ec.TYPE_AWAITING_PAYMENT;
			types = ec.TYPE_COE_REQUESTED;
			types = ec.STATUS_LOO_REQUESTED_TO_BO;
			types = ec.STATUS_LOO_REQUESTED_TO_SCH;
			types = ec.STATUS_MISSING_DOCUMENTS;
			types = ec.STATUS_LOO_RECEIVED;
			types = ec.STATUS_LOO_SENT_TO_CLI;
			types = ec.STATUS_WAITING_CLI_PAYMENT;
			types = ec.STATUS_PAID_BY_CLIENT;
			types = ec.STATUS_COE_REQUESTED_TO_BO;
			types = ec.STATUS_COE_REQUESTED_TO_SCH;
			types = ec.STATUS_COE_RECEIVED;
			types = ec.STATUS_COE_SENT_TO_CLI;
			types = ec.STATUS_ENROLLED;
			types = ec.STATUS_MISSING_DOCUMENTS;
			types = ec.ENROLMENT_TITLE;
			types = ec.STRING_SEPARATOR;
			types = ec.STYLE_MISSING_DOCUMENTS;
			types = ec.STYLE_COMPLETED;
			types = ec.STATUS_WAITING_CLI_PAYMENT2 ;
			double d = ec.offset;
			boolean bol = ec.isDeletable;
			types = ec.bucket;
		}
		Test.stopTest();
	}
}