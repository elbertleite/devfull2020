public with sharing class mysettings {
	public PageReference getRedir() {
		try {
			user userAccount = [Select AccountId, ContactId from User where  id = :UserInfo.getUserId() limit 1];
	 
			
			PageReference newPage;
		
			newPage = new PageReference('/contact_overview?id=' + userAccount.ContactId);
			System.debug('==>newPage: '+newPage);
			
			return newPage.setRedirect(true);
		} catch (Exception e){
			
		}
		return null;

	}
}