@isTest
private class coursecompareproducts_test {

    
        
     @testSetup static void setup(){
        TestFactory testFactory = new TestFactory();
        
		Account agency = TestFactory.createAgency();
		
		Quotation_Disclaimer__c qDisclaimer = new Quotation_Disclaimer__c(Destination__c = 'all', Agency__c = agency.id);
		Quotation_Disclaimer_Other_Nationality__c Disclaimer = new Quotation_Disclaimer_Other_Nationality__c(Quotation_Disclaimer__c = qDisclaimer.id);

		Currency_rate__c cr = new Currency_rate__c();
		cr.Agency__c = agency.id;
		cr.CurrencyCode__c = 'CAD';
		cr.Value__c = 1.5; 
		cr.High_Value__c = 1.8;
		insert cr;
		
		Contact employee = TestFactory.createEmployee(agency);
		User portalUser = TestFactory.createPortalUser(employee);
		Account school = TestFactory.createSchool();
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;
		insert p3;
		
		
		Account campus = TestFactory.createCampus(school, agency);
		    
		Course__c course = TestFactory.createCourse();
		
		Course__c course2 = TestFactory.createLanguageCourse();
				
		Campus_Course__c cc = TestFactory.createCampusCourse(campus, course);
		
		Campus_Course__c cc2 = TestFactory.createCampusCourse(campus, course2);
       
		Course_Price__c cp = TestFactory.createCoursePrice(cc, 'Latin America');
		
		Course_Price__c cp2 = TestFactory.createCoursePrice(cc2, 'Published Price');		
		
		Course_Intake_Date__c cid = TestFactory.createCourseIntakeDate(cc);
		
		Course_Intake_Date__c cid2 = TestFactory.createCourseIntakeDate(cc2);
        
		Course_Extra_Fee__c cef = TestFactory.createCourseExtraFee(cc, 'Latin America', p);
		
		Course_Extra_Fee__c cef2 = TestFactory.createCourseExtraFee(cc2, 'Published Price', p);
		
		Course_Extra_Fee_Dependent__c cefd = TestFactory.createCourseRelatedExtraFee(cef, p2);
		
		Course_Extra_Fee_Dependent__c cefd2 = TestFactory.createCourseRelatedExtraFee(cef2, p2);
		
		Course_Extra_Fee__c combinedfee = TestFactory.createCourseExtraFeeCombined(cc, 'Latin America', p, p2);
		
		Course_Extra_Fee__c opt1 = new Course_Extra_Fee__c();
		opt1.Campus__c = campus.id;
		opt1.Nationality__c = 'Latin America';
		opt1.Availability__c = 3.0;
		opt1.From__c = 1;
		opt1.Value__c = 150;		
		opt1.Optional__c = true;
		opt1.Product__c = p2.id;
		opt1.date_paid_from__c = system.today();
		opt1.date_paid_to__c = system.today().addDays(+31);		
		insert opt1;
		
		Start_Date_Range__c sdrOpt1 = new Start_Date_Range__c();
		sdrOpt1.Course_Extra_Fee__c = opt1.id;
		sdrOpt1.Campus__c = campus.id;
		sdrOpt1.From_Date__c = system.today();
		sdrOpt1.To_Date__c =  system.today().addDays(+31);
		sdrOpt1.Value__c = 300;
		insert sdrOpt1;
		
		Course_Extra_Fee_Dependent__c dep = new Course_Extra_Fee_Dependent__c();
		dep.Course_Extra_Fee__c = opt1.id;
		dep.Date_Paid_From__c = system.today();
		dep.Date_Paid_To__c = system.today().addDays(+31);
		dep.Value__c = 200;
		dep.Product__c = p3.id;
		dep.Optional__c = true;
		insert dep;
		
		Start_Date_Range__c sdrDep = new Start_Date_Range__c();
		sdrDep.Course_Extra_Fee_Dependent__c = dep.id;
		sdrDep.Campus__c = campus.id;
		sdrDep.From_Date__c = system.today();
		sdrDep.To_Date__c =  system.today().addDays(+31);
		sdrDep.Value__c = 300;
		insert sdrDep;
		
		
        
		Course_Extra_Fee__c cef3 = new Course_Extra_Fee__c();
		cef3.Campus__c = campus.Id;
		cef3.Nationality__c = 'Published Price';
		cef3.Availability__c = 3;
		cef3.From__c = 1;
		cef3.Value__c = 100;		
		cef3.Optional__c = false;
		cef3.Product__c = p.id;
		cef3.Allow_change_units__c = true;
		cef3.ExtraFeeInterval__c = 'Week';
		cef3.date_paid_from__c = system.today();
		cef3.date_paid_to__c = system.today().addDays(+31);
		insert cef3;
		
		
		
		Deal__c d = new Deal__c();
		d.Availability__c = 3;
		d.Campus_Account__c = campus.id;
		d.From__c = 1;
		d.Extra_Fee__c = cef2.id;
		d.Promotion_Type__c = 3;
		d.From_Date__c = system.today();
		d.To_Date__c = system.today().addDays(60);
		d.Extra_Fee_Type__c = 'Cash';
		d.Extra_Fee_Value__c = 60;
		d.Product__c = p.id;
		d.Nationality_Group__c = 'Publish Price';
		insert d;
		
		
		Deal__c d2 = new Deal__c();
		d2.Availability__c = 3;
		d2.Campus_Course__c = cc2.id;
		d2.From__c = 1;		
		d2.Promotion_Type__c = 2;
		d2.From_Date__c = system.today();
		d2.To_Date__c = system.today().addDays(60);
		d2.Promotion_Weeks__c = 1;
		d2.Number_of_Free_Weeks__c = 1;
		d2.Nationality_Group__c = 'Latin America';
		d2.Promotion_Name__c = '1+1';
		d2.Promo_Price__c = 50;		
		insert d2;
		
		Start_Date_Range__c sdr = new Start_Date_Range__c();
		sdr.Promotion__c = d2.id;
		sdr.Campus__c = cc2.Campus__c;
		sdr.From_Date__c = system.today();
		sdr.To_Date__c = system.today().addDays(15);
		sdr.Number_of_Free_Weeks__c = 2;
		insert sdr;		
		
		
		
		Deal__c d3 = new Deal__c();
		d3.Availability__c = 3;
		d3.Campus_Course__c = cc.id;
		d3.From__c = 1;
		d3.Extra_Fee__c = cef.id;
		d3.Promotion_Type__c = 3;
		d3.From_Date__c = system.today();
		d3.To_Date__c = system.today().addDays(60);
		d3.Extra_Fee_Type__c = 'Cash';
		d3.Extra_Fee_Value__c = 50;
		d3.Nationality_Group__c = 'Latin America';
		insert d3;
		
		Deal__c d4 = new Deal__c();
		d4.Availability__c = 3;
		d4.Campus_Course__c = cc.id;
		d4.From__c = 1;		
		d4.Promotion_Type__c = 2;
		d4.Promotion_Name__c = '5+2';
		d4.Promo_Price__c = 50;		
		d4.From_Date__c = system.today();
		d4.To_Date__c = system.today().addDays(61);
		d4.Promotion_Weeks__c = 5;
		d4.Number_of_Free_Weeks__c = 2;
		d4.Nationality_Group__c = 'Published Price';
		insert d4;
		
		
		Contact client = TestFactory.createClient(agency);
		
		
		Web_Search__c ws = new Web_Search__c();
		ws.Location__c = 3.0;
		ws.Nationality__c = 'Brazil';
		ws.Client__c = client.id;
		insert ws;
		
		Search_Courses__c sc1 = new Search_Courses__c();
		sc1.Campus_Course__c = cc.id;
		sc1.Web_Search__c = ws.id;
		sc1.Number_of_Units__c = 5;
		sc1.Nationality__c = 'Brazil';
		sc1.Custom_Fees__c = 'Fee 1:#123.0:#add:&Discount:#12.0:#subtract:&Fee 2:#1230.0:#add:&Discount2:#112.0:#subtract';
		sc1.Instalments__c = '1::100:20;2::100:20';

		sc1.Required_Fee_Changeable__c = cef2.id+':5';
		sc1.Optional_Fee_Ids__c = opt1.id + ':5';
		sc1.Optional_Fee_Related_Ids__c = opt1.id + ':' + dep.id + ':1';
		sc1.Selected_Combined_Fees__c = combinedfee.product__r.Name__c + ':' + combinedfee.product__c;
		sc1.Start_Date__c = system.today();
		sc1.Total_Course__c = 1000;
		sc1.Total_Extra_Fees__c = 500;
		sc1.Total_Products__c = 500;
		sc1.Total_Products_Other__c = 0;
		sc1.Total_Tuition__c = 500;
		sc1.Unit_Type__c = 'Week';
		sc1.Value_First_Instalment__c = 500;
		sc1.Payment_Date__c = system.today();
		insert sc1;
		
		Quotation_Products_Services__c qps = [select id from Quotation_Products_Services__c where Use_list_of_products__c = true and agency__c = :agency.id limit 1];
		
		Search_Course_Product__c scProd = new Search_Course_Product__c();
		scProd.Category__c = 'Insurance';
		scProd.Currency__c = 'AUD';
		scProd.Description__c = 'some description';
		scProd.Name__c = 'OSHC';
		scProd.Price__c = 405;
		scProd.Quantity__c = 1;
		scProd.Total__c = 405;
		scProd.Unit_Description__c = 'Month';
		scProd.Quotation_Products_Services__c = qps.id;
		scProd.Search_Course__c = sc1.id;
		scProd.Web_Search__c = ws.id;
		insert scProd;
		
		
		Search_Courses__c sc2 = new Search_Courses__c();
		sc2.Campus_Course__c = cc2.id;
		sc2.Web_Search__c = ws.id;
		sc2.Number_of_Units__c = 10;
		sc2.Nationality__c = 'Brazil';
		sc2.Custom_Fees__c = 'Fee 1:#123.0:#add:&Discount:#12.0:#subtract';
        sc2.Instalments__c = '1::100:20;2::100:20';
		sc2.Required_Fee_Deleted__c = cef2.id;
		sc2.Start_Date__c = system.today();
		sc2.Start_Date__c = system.today();
		sc2.Total_Course__c = 1000;
		sc2.Total_Extra_Fees__c = 500;
		sc2.Total_Products__c = 500;
		sc2.Total_Products_Other__c = 0;
		sc2.Total_Tuition__c = 500;
		sc2.Unit_Type__c = 'Week';
		sc2.Optional_Fee_Ids__c = opt1.id + ':5';
		sc2.Optional_Fee_Related_Ids__c = opt1.id + ':' + dep.id + ':1';
		sc2.Value_First_Instalment__c = 500;
		sc2.Payment_Date__c = system.today();
		insert sc2;
		
		Search_Courses__c sc3 = new Search_Courses__c();
        sc3.UseAsCustomTemplate__c = true;
        sc3.isCustomCourse__c = true;
        sc3.Custom_Campus__c = 'Custom Campus';
        sc3.Custom_City__c = 'Sydney';
        sc3.Custom_Country__c = 'Australia';
        sc3.Custom_Course__c = 'General english';
        sc3.Custom_Currency__c = 'AUD';
        sc3.Custom_NumberOfUnits__c = 20;
        sc3.Custom_StartDate__c = system.today();
        sc3.Custom_PriceOption__c = 'Per Unit';
        sc3.Custom_Fees__c = 'Fee 1:#123.0:#add:&Discount:#12.0:#subtract';
        sc3.Instalments__c = '1::100:20;2::100:20';
        sc3.Custom_Value__c = 300;
        sc3.Web_Search__c = ws.id;
        insert sc3;
        
        Search_Course_Extra_Fee__c scef = new Search_Course_Extra_Fee__c();
        scef.Fee_Value__c = 450;
        scef.Extra_Fee_Interval__c = 'Week';
        scef.Number_Units__c = 1;
        scef.Custom_Fee_Name__c = 'THe custom fee';
        scef.Search_Course__c = sc3.id;
        insert scef;
        
    }
		
    @isTest static void myUnitTest() {
        
        Web_Search__c ws = [Select id from Web_Search__c where Nationality__c = 'Brazil' limit 1];
        Search_Courses__c sc1 = [Select id from Search_Courses__c limit 1];
        contact client = [Select id from contact where email = 'margarete@googlerose.com.test' limit 1];

        ApexPages.currentPage().getParameters().put('id', ws.id);
        Apexpages.Standardcontroller controller = new ApexPages.Standardcontroller(ws);
        coursecompareproducts compare = new coursecompareproducts(controller);
        Test.startTest();

        List<SelectOption> InstalmentInterval = compare.InstalmentInterval;
        List<SelectOption> instalmentIntervalUnits = compare.instalmentIntervalUnits;
        List<SelectOption> getUnitsRange = compare.getUnitsRange();

        
        compare.getUserDetails();
        
        compare.getCourseDetails();
        
        compare.getAgencyCurrencies();
        Datetime lastmod = compare.currencyLastModifiedDate;
        
        compare.updateAgencyCurrencies();
        compare.getSchoolInstalments();

        ApexPages.currentPage().getParameters().put('courseId',sc1.id);
		ApexPages.currentPage().getParameters().put('numInstal','2');
		ApexPages.currentPage().getParameters().put('splitFee', 'false');
		ApexPages.currentPage().getParameters().put('interval', '1');
		ApexPages.currentPage().getParameters().put('intervalOption','Week');
		ApexPages.currentPage().getParameters().put('dtFisrtInstalment',system.today().format());
        compare.calculateInstalments();

       

        compare.updateInstalments();
        for(searchcoursecountry.courseDetails cd : compare.getCourseDetails()){
        	 for(Search_Course_Product__c scp : cd.listProducts)
        		scp.selected__c = true;
        			
        			
        	for(searchcoursecountry.retrievedExtraFees ref : cd.validExtraFees.values()){
        		ref.extraFee.SelectedFee__c = true;
        		if(ref.extraFee.Value__c == null) ref.extraFee.Value__c = 150; 
        		
        		
        		if(ref.combinedFeesDetails != null && !ref.combinedFeesDetails.isEmpty()){
        			for(String cb : ref.combinedFeesDetails.keySet())
        				for(searchcoursecountry.combinedFees cf : ref.combinedFeesDetails.get(cb))
        					ref.selectedCombinedFee = cf.extraFee.Product__c;
        		}
        		       		
        	}		
        			
        }
        	
             

        ApexPages.currentPage().getParameters().put('courseID', sc1.id);
        compare.removeProducts();

        ApexPages.currentPage().getParameters().put('courseID',sc1.id);
        compare.removeSchoolInstalments();

	        
    }

    @isTest static void myUnitTest2() {
        Web_Search__c ws = [Select id from Web_Search__c where Nationality__c = 'Brazil' limit 1];
        Search_Courses__c sc1 = [Select id from Search_Courses__c limit 1];
        contact client = [Select id from contact where email = 'margarete@googlerose.com.test' limit 1];
        //user portalUser = [Select id from user where contact.email = 'johndoe@educationhify.com.test' limit 1];
        ws.Combine_Quotation__c = true;
        update ws;

        ApexPages.currentPage().getParameters().put('id', ws.id);
        Apexpages.Standardcontroller controller = new ApexPages.Standardcontroller(ws);
        coursecompareproducts compare = new coursecompareproducts(controller);
		Test.startTest();
		compare.totalCombined = null;
        compare.getCourseDetails();
        compare.getSchoolInstalments();
        
		//system.runAs(portalUser){
        //}
        ApexPages.currentPage().getParameters().put('courseUpdate',sc1.id);
        compare.saveUpdate();
        compare.cancelUpdate();
        
		coursecompareproducts.totalCombineQuote totalCombined = compare.totalCombined;
		ApexPages.currentPage().getparameters().put('clientIDs', client.id);
		compare.saveQuotation();

		compare.returnToCompare();
    }

}