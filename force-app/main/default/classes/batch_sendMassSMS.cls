global class batch_sendMassSMS implements Database.Batchable<CourseInstalmentsEmailController.smsInfo>, Database.AllowsCallouts{

	List<CourseInstalmentsEmailController.smsInfo> lSMS;
	global batch_sendMassSMS(List<CourseInstalmentsEmailController.smsInfo> sms){

		lSMS = sms;
	}

	global Iterable<CourseInstalmentsEmailController.smsInfo> start(Database.BatchableContext bc) {
		return lSMS;
	}

	global void execute(Database.BatchableContext BC, List<CourseInstalmentsEmailController.smsInfo> lSMS){

		System.debug('lSMS: '+lSMS);

		TwilioRestClient client = new TwilioRestClient(IPFunctions.twAccount, IPFunctions.twKey);

		List<Email_SMS__c> lEmailSMS = new List<Email_SMS__c>();

		User userDetails = [Select Id, Name from User where id = :UserInfo.getUserId()];


		/** instalment activity **/
		DateTime currentTime = DateTime.now();
		String activity;
		map<string,string> instError = new map<string,string>();
		map<string,string> invError = new map<string,string>();

		for(CourseInstalmentsEmailController.smsInfo em: lSMS){

			try{
				// activity = 'Sending SMS;;' + em.params.get('to') + ';;' + em.selectedTemplate +';;' + currentTime.format('yyyy-MM-dd HH:mm:ss') + ';;pending;;-;;' + userDetails.Name + '!#!';
				TwilioMessage sms = client.getAccount().getMessages().create(em.params);
				system.debug('status ===' + sms.getStatus());

				/** Save information in a Object until get the callback to save in S3 **/
				Email_SMS__c s = new Email_SMS__c();
				s.from__c = Userinfo.getUserId();

				if(em.typeId =='instalment'){
					s.client_course_instalment__c = em.instalment;
					// mapInstalments.put(em.instalment,activity);
				}

				else  if(em.typeId =='invoice'){
					s.Invoice__c = em.instalment;
					// mapInvoices.put(em.instalment,activity);
				}

				s.Client__c = em.contactId;
				s.msgId__c = sms.getSid();
				s.Type__c = 'SMS';
				s.Body__c = em.msgBody;
				s.Subject__c = em.selectedTemplate;

				lEmailSMS.add(s);
				/** END -- Save information in a Object until get the callback to save in S3 **/


			}catch(Exception e){
				activity = 'Error SMS;;' + em.params.get('to') + ';;' + e.getMessage() +';;' + currentTime.format('yyyy-MM-dd HH:mm:ss') + ';;error;;-;;' + userDetails.Name + '!#!';

				if(em.typeId =='instalment'){
					instError.put(em.instalment, activity);
				}

				else  if(em.typeId =='invoice'){
					invError.put(em.instalment, activity);
				}

			}//end catch handling

		}//end for

    insert lEmailSMS;


		//Add activities
		List<SObject> toUpdate = new List<SObject>();

		if(instError.keySet().size()>0)
			for(client_course_instalment__c cci : [Select Id, instalment_activities__c From client_course_instalment__c where id in :instError.keySet()]){
				if(cci.instalment_activities__c != null)
					cci.instalment_activities__c += instError.get(cci.id);
				else
					cci.instalment_activities__c = instError.get(cci.id);

				toUpdate.add(cci);
			}//end for instalment

		if(invError.keySet().size()>0)
			for(Invoice__c inv : [Select Id, Invoice_Activities__c From Invoice__c where id in :invError.keySet()]){
				if(inv.Invoice_Activities__c != null)
					inv.Invoice_Activities__c += invError.get(inv.id);
				else
					inv.Invoice_Activities__c = invError.get(inv.id);

				toUpdate.add(inv);
			}//end for invoices

		if(toUpdate.size()>0)
			update toUpdate;


	}//end execute


	 global void finish(Database.BatchableContext BC)
     {

     }


}