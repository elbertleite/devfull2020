public class batch_AccountDocuments_IP_Urls{
	public batch_AccountDocuments_IP_Urls(){}
}

/*global class batch_AccountDocuments_IP_Urls implements Database.Batchable<sObject>, Database.AllowsCallouts {
    
    
    S3Controller S3 {get;set;}
  String query;
  
  global batch_AccountDocuments_IP_Urls(String qr) {
    
    query = qr;
  }
  
  global Database.QueryLocator start(Database.BatchableContext BC) {

    return Database.getQueryLocator(query);
  }

     global void execute(Database.BatchableContext BC, List<Account_document_File__c> scope) {
    // try{
      S3 = new S3Controller();
      S3.constructor();
      String prefix = '';
      for(Account_document_File__c apf : scope){
        prefix = apf.SDrive_Account__c + '/' + apf.SDrive_File__c;

        s3.listBucket('ehfsdrive', prefix , null, null, null);

        if(s3.bucketList != null && !s3.bucketList.isEmpty()){  
          for(S3.ListEntry file : s3.bucketList){
            system.debug('file===>' + file);

            String url = IPFunctions.GenerateAWSLink('ehfsdrive', file.key, S3.S3Key, s3.S3Secret);
            system.debug('url===>' + url); 
            apf.Preview_Link__c = url;
            //apf.isUrlUpdated__c = true;
          }
        }
      }//end for

      update scope;
    // }
    // catch(Exception e){
    //   system.debug('error==' + e.getMessage());
    //   system.debug('error line==' + e.getLineNumber());
    // }

  }
  
  global void finish(Database.BatchableContext BC) {
    system.debug('Url update finished...');
  }
  
}*/
/*
    batch_updateSDrive_previewLink_doc batch = new batch_updateSDrive_previewLink_doc('Select id,  preview_link__c, SDrive_Account__c, SDrive_File__c from account_document_file__c where createdby.name = null and SDrive_File__c != null and Content_Type__c != \'Folder\'');
    Id batchId = Database.executeBatch(batch, 80); //Define batch size
    */