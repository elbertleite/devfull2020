public with sharing class payment_plan_ep {

	user currentUser = [Select id, Contact.AccountId, contact.account.name, Contact.Account.Global_Link__c from user where id =:userInfo.getUserId() limit 1];
	public String bucketName {get;set;}
	public String gbLink {get;set;}
	public payment_plan_ep() {
		bucketName = IPFunctions.s3bucket;
		gbLink = currentUser.Contact.Account.Global_Link__c;
		retrievePaymentPlans();
	}
	string ct;

	public date dateFrom{get{if(dateFrom == null) dateFrom = System.today(); return dateFrom;} set;}
	public date dateTo{get{if(dateTo == null) dateTo = System.today().addMonths(1); return dateTo;} set;}
	
	public map<string,list<planItem>> listPaymentPlans{get; set;}

	public map<string, integer> listSize{get; set;}

	public void retrievePaymentPlans(){
		string employee = ApexPages.CurrentPage().getParameters().get('userId');
		string agencyId = ApexPages.CurrentPage().getParameters().get('agencyId');
		string departmentId = ApexPages.CurrentPage().getParameters().get('departmentId');
		string groupId = ApexPages.CurrentPage().getParameters().get('groupId');
		string dateFrom = ApexPages.CurrentPage().getParameters().get('dateFrom');
		string dateTo = ApexPages.CurrentPage().getParameters().get('dateTo');
		date dtFrom = System.today();
		date dtTo = System.today().addMonths(1);
		if(dateFrom != null && dateFrom.trim() != '')
			dtFrom = date.parse(dateFrom);
		if(dateTo != null && DateTo.trim() != '')
			dtTo = date.parse(dateTo);
		
		planItem item;
		listPaymentPlans = new map<string,list<planItem>>();
		listPaymentPlans.put('bucket', new list<planItem>());
		listPaymentPlans.put('instalments', new list<planItem>());
		string  sql = 'Select id, Instalments__r.Plan_Type__c, due_date__c, Client__r.name, Value__c, Instalments__c, Instalments__r.Value__c, Total_Paid__c, Value_Paid__c, Currency_Code__c, preview_link__c, ';
				sql += ' Currency_Paid__c, Currency_Rate__c, Value_in_Local_Currency__c, Instalments__r.Description__c, Notes__c, 	Received_By_Agency__r.name, Received_By__r.name, Received_On__c, instalment_Number__c, Instalments__r.Number_of_Instalments__c, ';
				sql += ' Reminder_Date__c, Instalments__r.Reminder_Date__c from Payment_Plan__c where ';
				if(employee != null && employee.trim() != '' && employee != 'all' )
					sql += ' (client__r.owner__c = :employee) and ';
				if(departmentId != null && departmentId.trim() != '' && departmentId != 'all' )
					sql += ' (client__r.owner__r.Contact.Department__c = :departmentId) and ';
				else if(agencyId != null && agencyId.trim() != '' && agencyId != 'all' )
					sql += ' (client__r.current_Agency__c = :agencyId) and ';
				else if(groupId != null && groupId.trim() != '' && groupId != 'all' )
					sql += ' (client__r.current_Agency__r.ParentId = :groupId) and ';
				sql += ' ((Instalments__c != null and Received_On__c = null) or (Received_On__c != null and Payment_To_Amend__c = true)) ';
				sql += ' and ((((due_date__c >= :dtFrom and due_date__c <= :dtTo) or (due_date__c < today and Received_On__c = null)) and Instalments__r.Plan_Type__c = \'instalments\' and Instalments__r.Loan_Status__c = \'approved\') ';
				sql += ' or (Instalments__r.Plan_Type__c = \'bucket\' and (Reminder_Date__c = null or (Reminder_Date__c  >= :dtFrom and Reminder_Date__c <= :dtTo))) ';
				sql += ' ) ';
				
				sql += ' order by Reminder_Date__c nulls last,  Received_On__c nulls last, due_date__c, instalment_Number__c ';
		system.debug('==>dateFrom: '+dateFrom);
		system.debug('==>dateTo: '+dateTo);
		system.debug('==>sql: '+sql);
		for (Payment_Plan__c pp:database.query(sql)){
			item = new planItem();
			item.itemPlan = pp;
			if(pp.preview_link__c != null){ 
				for(string pl:pp.preview_link__c.split(';;'))
					item.listFiles.add(new fileLinks(pl.split('!#!')[1],pl.split('!#!')[0]));
			}
			if(pp.notes__c != null && pp.notes__c.trim() != ''){
				item.listNotes.addAll(new list<noteDetails>((list<noteDetails>)JSON.deserialize(pp.notes__c,list<noteDetails>.class)));
				item.listNotes.sort();
			}
			if(pp.Instalments__r.Plan_Type__c != null)
				listPaymentPlans.get(pp.Instalments__r.Plan_Type__c).add(item);
		}
		listSize = new map<string, integer>();
		for(string s:listPaymentPlans.keySet())
			listSize.put(s,listPaymentPlans.get(s).size());
	}

	public void confirmPayment(){
		string payId = ApexPages.CurrentPage().getParameters().get('payId');
		Payment_Plan__c payPlan = new Payment_Plan__c(id = payId, Payment_To_Amend__c = false);
		update payPlan;
		retrievePaymentPlans();
	}

	public class planItem{
		public Payment_Plan__c itemPlan{get{if(itemPlan == null) itemPlan = new Payment_Plan__c(); return itemPlan;} set;} 
		public list<fileLinks> listFiles{get{if(listFiles == null) listFiles = new list<fileLinks>(); return listFiles;} set;} 
		public list<noteDetails> listNotes{get{if(listNotes == null) listNotes = new list<noteDetails>(); return listNotes;} set;} 
	}

	public class noteDetails implements Comparable{
		public string noteOn{get;set;}
		public string noteBy{get;set;}
		public string noteAgency{get;set;}
		public string noteDesc{get;set;}
		public noteDetails(string noteOn, string noteBy, string noteAgency, string noteDesc){
			this.noteOn = noteOn;
			this.noteBy = noteBy;
			this.noteAgency = noteAgency;
			this.noteDesc = noteDesc;
		}

		public Integer compareTo(object compareTo) {
			noteDetails noteDetailsToCompare = (noteDetails)compareTo;
			
			if(datetime.parse(noteOn) > datetime.parse(noteDetailsToCompare.noteOn)){
				return -1;
			}else if(datetime.parse(noteOn) < datetime.parse(noteDetailsToCompare.noteOn)){
				return 1;
			}
			return 0;
		}
	}

	public class fileLinks{
		public string fileName{get;set;}
		public string previewLInk{get;set;}
		public fileLinks(string fileName, string previewLInk){
			this.fileName = fileName;
			this.previewLInk = previewLInk;
		}
	}

    
    @RemoteAction
    public static list<Account> changeAccount() {
		// return [SELECT Id, Name FROM Account WHERE recordType.name = 'Agency Group' order by name];
		
		list<Account> agencyGroupOptions = new List<Account>();  
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group  
			user localUser = [Select id, Contact.AccountId from user where id =:userInfo.getUserId() limit 1];		
			for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:localUser.Contact.AccountId order by Parent.Name]){
				if(ag.Parent.Name != null && ag.ParentId != null)
					agencyGroupOptions.add(new Account(id = ag.ParentId, name = ag.Parent.Name));
			}
		}else{
			for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
				agencyGroupOptions.add(new Account(id = ag.id, name = ag.Name));
			}
		}
		return agencyGroupOptions;
    }

    @RemoteAction
    public static list<Account> changeAgency(string groupId) {
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
			user localUser = [Select id, Contact.AccountId from user where id =:userInfo.getUserId() limit 1];
			return [Select Id, Name from Account where id =:localUser.Contact.AccountId order by name];
		}else{
			return [SELECT Id, Name FROM Account WHERE parentId = :groupId order by name];
		}
	}
	
	@RemoteAction
	public static list<Department__c> changeDepartment(string agencyId){
		return [Select Id, Name from Department__c where Agency__c = :agencyId and Inactive__c = false];
	}
	
    @RemoteAction
    public static list<User> changeEmployee(string agencyId, string departmentId) {
		if(departmentId != null && departmentId != 'all')
        	return [SELECT Id, Name FROM User WHERE contact.Accountid = :agencyId and contact.Department__c = :departmentId order by name];
        else return [SELECT Id, Name FROM User WHERE contact.Accountid = :agencyId order by name];
    }


	private string groupID{
		get{
			if(groupID == null){
				try {
					groupID = [select Contact.Account.ParentId from User where id =:UserInfo.getUserId() limit 1].Contact.Account.ParentId;
				} catch (Exception e){
					groupID = '';
				}
			}
			return groupID;
		}
		set;
	}

	public string groupBgColor{
		get{
			
			UserDetails.GroupSettings gs = new UserDetails.GroupSettings();
			groupBgColor = gs.getBackgroundColour(groupID);
			
			return groupBgColor;
		}
		set;
	}
	
	public string groupLabelColor{
		get{
			UserDetails.GroupSettings gs = new UserDetails.GroupSettings();
			groupLabelColor = gs.getLabelColour(groupID);
			
			return groupLabelColor;
		}
		set;
	}


	public string error{get; set;}
	public string newNote{get; set;}
	public list<noteDetails> newlistNotes{get{if(newlistNotes == null) newlistNotes = new list<noteDetails>(); return newlistNotes;} set;} 
	public Payment_Plan__c planUpdate{get; set;}
	public Payment_Plan__c nextPayment{get; set;}
	public string updateSection{get;set;}
	//private decimal planUpdateOriginalValue;
	public void selectItemUpdate(){
		newNote = '';
		string itemId = ApexPages.currentPage().getParameters().get('itemId');
		updateSection =  ApexPages.currentPage().getParameters().get('sectionType');
		planUpdate = [select id, preview_link__c, Payment_Type__c, Value__c, BO_Value__c, Instalments__c, Value_Remaining__c, Total_Paid__c, Total_Available__c, description__c, Client__c, Value_Paid__c, Currency_Code__c, Currency_Paid__c, Currency_Rate__c, Value_in_Local_Currency__c, 
		Instalments__r.Value_Remaining__c, Instalments__r.Total_Available__c, Instalments__r.Currency_Code__c, Reminder_Date__c, Instalments__r.Reminder_Date__c, Client__r.name, Instalments__r.Description__c,
		Received_On__c, Notes__c from Payment_Plan__c where id = :itemId]; 
		system.debug('==>itemId: '+itemId );
		system.debug('==>planUpdate.Instalments__c: '+planUpdate.Instalments__c );
		try{
			nextPayment = [select id, Value__c from Payment_Plan__c where Instalments__c = :planUpdate.Instalments__c and Value_Paid__c = null]; 
		}catch(Exception e){
			system.debug('Error===' + e);
			system.debug('Error Line ===' + e.getLineNumber());
		}
		//planUpdateOriginalValue = planUpdate.Value_in_Local_Currency__c;
		newlistNotes = new list<noteDetails>();
		if(planUpdate.notes__c != null && planUpdate.notes__c.trim() != ''){
			newlistNotes.addAll(new list<noteDetails>((list<noteDetails>)JSON.deserialize(planUpdate.notes__c,list<noteDetails>.class)));
		} 
	} 

	public void saveRequestAndConfirm(){
		planUpdate.Payment_To_Amend__c = false;
		saveRequest();
	}

	public void saveRequest(){
		System.debug('==>planUpdate.Value_Paid__c: '+planUpdate.Value_Paid__c);
		System.debug('==>planUpdate.BO_Value__c: '+planUpdate.BO_Value__c);
		boolean updateValue = true;
		if(planUpdate.Instalments__r.Value_Remaining__c == 0 && planUpdate.Value_Paid__c > planUpdate.BO_Value__c){
			Payment_Plan__c instalment = new Payment_Plan__c(Client__c = planUpdate.client__c, Instalments__c = planUpdate.Instalments__c, value__c = planUpdate.Value_Paid__c.setScale(4) - planUpdate.BO_Value__c.setScale(4), Currency_Code__c = planUpdate.Instalments__r.Currency_Code__c);
			System.debug('==>instalment: '+instalment);
			insert instalment;
			System.debug('==>instalment: '+instalment);
		}else if((planUpdate.Instalments__r.Value_Remaining__c > 0 && math.abs(planUpdate.BO_Value__c - planUpdate.Value_Paid__c) > planUpdate.Instalments__r.Value_Remaining__c) || (planUpdate.Instalments__r.Value_Remaining__c == 0 && planUpdate.Value_Paid__c < planUpdate.BO_Value__c)){
			//add error msg
			updateValue = false;
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Value is bigger than total plan!');
            ApexPages.addMessage(myMsg);
			System.debug('==>error: ');
		}
		//else{
		if(updateValue){
			System.debug('==>updateValue: ');
			if(planUpdate.Value_Paid__c > planUpdate.BO_Value__c){
				planUpdate.Instalments__r.Value_Remaining__c = planUpdate.Instalments__r.Value_Remaining__c.setScale(4) + (planUpdate.Value_Paid__c.setScale(4) - planUpdate.BO_Value__c.setScale(4));
				planUpdate.Instalments__r.Total_Available__c = planUpdate.Instalments__r.Total_Available__c.setScale(4) - (planUpdate.Value_Paid__c.setScale(4) - planUpdate.BO_Value__c.setScale(4));
			}else{
				planUpdate.Instalments__r.Value_Remaining__c = planUpdate.Instalments__r.Value_Remaining__c.setScale(4) -  math.abs(planUpdate.Value_Paid__c.setScale(4) - planUpdate.BO_Value__c.setScale(4));
				planUpdate.Instalments__r.Total_Available__c = planUpdate.Instalments__r.Total_Available__c.setScale(4) +  math.abs(planUpdate.Value_Paid__c.setScale(4) - planUpdate.BO_Value__c.setScale(4));
			}

			if(nextPayment != null && nextPayment.id != null){
				if(planUpdate.Instalments__r.Value_Remaining__c != 0){
					nextPayment.Value__c = planUpdate.Instalments__r.Value_Remaining__c;
					nextPayment.Value_Paid__c = null;
					update nextPayment;
				}else{
					delete [Select id from Payment_Plan__c where id = :nextPayment.id];
				}
			}
			planUpdate.Value_Paid__c = planUpdate.BO_Value__c;
			planUpdate.Instalments__r.Reminder_Date__c = planUpdate.Reminder_Date__c;

			if(newNote != null && newNote.trim() != ''){
				newlistNotes.add(new noteDetails( System.now().format(), userInfo.getName(), currentUser.contact.account.name, newNote));
				planUpdate.notes__c = JSON.serialize(newlistNotes);
			}

			update planUpdate;
			update planUpdate.Instalments__r;
			update_remainingValue();
			planUpdate = null;
			retrievePaymentPlans();
		}
	}

	public void saveInstallUpdate(){
		if(newNote != null && newNote.trim() != ''){
			newlistNotes.add(new noteDetails( System.now().format(), userInfo.getName(), currentUser.contact.account.name, newNote));
			planUpdate.notes__c = JSON.serialize(newlistNotes);
		}

		update planUpdate;

		planUpdate = null;
		retrievePaymentPlans();
	}

	private void update_remainingValue(){
		list<Payment_Plan__c> listInstall = [Select id, Value__c, Value_Paid__c from Payment_Plan__c where Instalments__c = :planUpdate.instalments__c order by createddate];
		for(integer i = 1; i < listInstall.size(); i++)
			listInstall[i].Value__c = listInstall[i-1].Value__c - listInstall[i-1].Value_Paid__c;
		update listInstall;
	}

}