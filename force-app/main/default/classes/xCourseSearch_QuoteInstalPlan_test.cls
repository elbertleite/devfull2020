/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class xCourseSearch_QuoteInstalPlan_test {

    static testMethod void myUnitTest() {
        
		TestFactory testFactory = new TestFactory();
		
		Account agency = TestFactory.createAgency();
		
		Contact employee = TestFactory.createEmployee(agency);
		
		Instalment_Plan__c ip = new Instalment_Plan__c();
		ip.Agency__c = agency.id;
		ip.Deposit__c = 30.0;
		ip.Description__c = 'A test';
		ip.Group_Instalments__c = false;
		ip.Instalments_Interest__c = '1:0.68;2:1.13;3:1.36;4:1.49;5:1.59;6:1.65;7:1.70;8:1.74;9:1.77;10:1.79;11:1.81;12:1.83';
		ip.Interest__c = 0;
		ip.Interest_Type__c = 'Credit Card Interest';
		ip.isSelected__c = true;
		ip.Name__c = 'Finance 12x';
		ip.Number_of_Instalments__c = 12;
		insert ip;
		
		Instalment_Plan__c simplePlan = new Instalment_Plan__c();
		simplePlan.Agency__c = agency.id;
		simplePlan.Deposit__c = 30.0;
		simplePlan.Description__c = 'A test';
		simplePlan.Group_Instalments__c = true;		
		simplePlan.Interest__c = 0;
		simplePlan.Interest_Type__c = 'Simple';
		simplePlan.isSelected__c = true;
		simplePlan.Name__c = 'Finance 12x';
		simplePlan.Number_of_Instalments__c = 12;
		insert simplePlan;
		
		Instalment_Plan__c ccPlan = new Instalment_Plan__c();
		ccPlan.Agency__c = agency.id;
		ccPlan.Deposit__c = 30.0;
		ccPlan.Description__c = 'A test';
		ccPlan.Group_Instalments__c = true;		
		ccPlan.Interest__c = 2;
		ccPlan.Interest_Type__c = 'Credit Card Interest';
		ccPlan.isSelected__c = true;
		ccPlan.Name__c = 'Finance 12x';
		ccPlan.Number_of_Instalments__c = 12;
		insert ccPlan;
		
        
		Account school = TestFactory.createSchool();
		
		Account campus = TestFactory.createCampus(school, agency);
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Cozy Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;
		insert p3;
		
		
		Course__c course = TestFactory.createCourse();
		
		Campus_Course__c cc = TestFactory.createCampusCourse(campus, course);
		
		Course_Price__c cp = TestFactory.createCoursePrice(cc, 'Latin America');
		
		Course_Intake_Date__c cid = TestFactory.createCourseIntakeDate(cc);
		
		Course_Extra_Fee__c cef = TestFactory.createCourseExtraFee(cc, 'Latin America', p);
		
		Course_Extra_Fee_Dependent__c cefd = TestFactory.createCourseRelatedExtraFee(cef, p2);
		
		Deal__c extraFeeDeal = TestFactory.createCourseExtraFeePromotion(cc, 'Latin America', p);
		
		Deal__c freeUnits = TestFactory.createCourseFreeUnitsPromotion(cc, 'Latin America');
		
		Web_Search__c ws = TestFactory.createWebSearch(3, cc, 'Brazil', false);
		
		Search_Courses__c sc = TestFactory.createSearchCourse(ws, cc);
		
		Search_Courses__c custom = TestFactory.createCustomSearchCourse(ws); 
		
		TestFactory.createAgencyInstalmentPlans(agency);
		
		
				
		ApexPages.Standardcontroller controller = new Apexpages.Standardcontroller(ws);
		xCourseSearch_QuoteInstalPlan plan = new xCourseSearch_QuoteInstalPlan(controller);
		
		plan.getlistInstalmentPlanDetails();
		
		for(xCourseSearch_QuoteInstalPlan.agencyPlanOption apo : plan.getlistInstalmentPlanDetails())
			apo.isSelected = true;
		
		plan.addAgencyPlan();
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		Web_Search__c wsCOmbined = TestFactory.createWebSearch(3, cc, 'Brazil', true);
		
		Search_Courses__c sc1 = TestFactory.createSearchCourse(wsCOmbined, cc);
		
		Search_Courses__c customc = TestFactory.createCustomSearchCourse(wsCOmbined); 
		
		TestFactory.createAgencyInstalmentPlans(agency);
		
		
				
		ApexPages.Standardcontroller controller2 = new Apexpages.Standardcontroller(wsCOmbined);
		xCourseSearch_QuoteInstalPlan plan2 = new xCourseSearch_QuoteInstalPlan(controller2);
		
		plan2.getlistInstalmentPlanDetails();
		
		for(xCourseSearch_QuoteInstalPlan.agencyPlanOption apo : plan2.getlistInstalmentPlanDetails())
			apo.isSelected = true;
		
		plan2.addAgencyPlan();
		
    }
}