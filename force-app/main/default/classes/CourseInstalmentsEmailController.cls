global class CourseInstalmentsEmailController {

	public boolean showMsg {get{if(showMsg == null) showMsg = false; return showMsg;}set;}
	public boolean useSignature {get{if(useSignature == null) useSignature = true; return useSignature;}set;}
	public String theMsg {get;set;}

	private map<string,Contact> mapContactsEmail {get;set;}
	private List<client_course_instalment__c> allInstalments {get;set;}
	private List<Invoice__c> allInvoices {get;set;}

	private String action {get;set;}
	private Account agency {get;set;}

	/** Constructor **/
	public CourseInstalmentsEmailController(){

		action = ApexPages.currentPage().getParameters().get('action');
		agency = [SELECT Id, Clients_Payments_Email__c FROM Account WHERE id = :ApexPages.currentPage().getParameters().get('agency')];

	}
	/** END -- Constructor **/


	public void loadIds(){
		list<String> instalments = ApexPages.currentPage().getParameters().get('instalmentsIds').replace('[','').replace(']','').split(',');
		system.debug('instalmentsIds==' + instalments);

		list<String> invoices = ApexPages.currentPage().getParameters().get('invoicesIds').replace('[','').replace(']','').split(',');
		system.debug('invoicesIds==' + invoices);

		mapFieldsInst = IPFunctions.findTokens(IPFunctions.tokensInstalment);
		mapFieldsInv = IPFunctions.findTokens(IPFunctions.tokensInvoice);

		system.debug('mapFieldsInv==>' + mapFieldsInv);
		
		if(instalments!=null)
			allInstalments = emailTemplateDynamic.createListResult(' , instalment_activities__c FROM client_course_instalment__c WHERE Id IN ( \''+ String.join(instalments, '\',\'') + '\' ) ', mapFieldsInst, null, new list<String>(mapFieldsInst.keySet()));	
			// allInstalments =  [Select Id, Client_Course__r.Client__c, Client_Course__r.Client__r.Name, Client_Course__r.Client__r.Email, instalment_activities__c  From client_course_instalment__c where id in :instalments ];

		if(invoices!=null)
			// allInvoices =  [Select Id, Client__c, Client__r.Name, Client__r.Email, Invoice_Activities__c  From Invoice__c where id in:invoices];
			allInvoices = emailTemplateDynamic.createListResult(' , invoice_activities__c FROM Invoice__c WHERE Id IN ( \''+ String.join(invoices, '\',\'') + '\' ) ', mapFieldsInv, null, new list<String>(mapFieldsInv.keySet()));	

		mapContactsEmail = new map<string,Contact>();
		set<Id> ctIds = new set<Id>();

		//Get Build contact map with details and mobile number
		if(allInstalments!=null)
			for(client_course_instalment__c i : allInstalments)
				ctIds.add(i.Client_Course__r.Client__c);

		if(allInvoices!=null)
			for(Invoice__c i : allInvoices)
				ctIds.add(i.Client__c);

		for(Contact c : [Select Id, Email, Name, Current_Agency__r.BillingCountry, (Select Country__c, detail__c, Contact__c, isDefault__c from forms_of_contact__r where Type__c = 'Mobile' and isDefault__c = true) from Contact where id in :ctIds])
			mapContactsEmail.put(c.Email, c);


		if(allInstalments!=null && allInstalments.size()>0)
			objTempInst = (sObject) allInstalments[0];

		if(allInvoices!=null && allInvoices.size()>0)
			objTempInv = (sObject) allInvoices[0];


		String tpCategory = action == 'email' ? 'Clients Payments Email' : 'Client Payments SMS';
		findEmailTemplate(userDetails.AccountID, tpCategory);
	}



	/** Send Emails **/
	public void sendEmails(){

		try{
			map<string, string> instalmentActivity = new map<string,string>();
			map<string, string> invoiceActivity = new map<string,string>();
		
			/** Send email via Mandrill **/
			String emailBody;
			String instalmentInfo;

			List<mandrillSendEmail.messageJson> listEmails = new list<mandrillSendEmail.messageJson>();
			Schema.SObjectType objType;
			Id objId;
			String emailService = IpFunctions.getS3EmailService();
			EmailToS3Controller s3 = new EmailToS3Controller();

			map<string,string> tpSubject; 
			map<string,string> tpBody; 

			// 	I N S T A L M E N T S 		E M A I L S
			if(allInstalments!=null){
				fieldsTemplate = emailTemplateDynamic.extractFieldsFromFile(mEmailTemplate.get(selectedTemplate).Template__c, mapFieldsInst); 
				tpSubject = emailTemplateDynamic.createTemplate(mapFieldsInst, mEmailTemplate.get(selectedTemplate).Email_Subject__c, fieldsTemplate, allInstalments);
				tpBody = emailTemplateDynamic.createTemplate(mapFieldsInst, mEmailTemplate.get(selectedTemplate).Template__c, fieldsTemplate, allInstalments);
				
				for(client_course_instalment__c cci : allInstalments){

					mandrillSendEmail mse = new mandrillSendEmail();
					mandrillSendEmail.messageJson email_md = new mandrillSendEmail.messageJson();
					email_md.setTo(cci.Client_Course__r.client__r.email, cci.client_course__r.client__r.name);
					
					if(agency.Clients_Payments_Email__c!=null)
						email_md.setFromEmail(agency.Clients_Payments_Email__c);
					else
						email_md.setFromEmail(userDetails.Email);

					email_md.setFromName(userDetails.Name);

					String subject = tpSubject.get(cci.id);
					email_md.setSubject(subject);
					emailBody = tpBody.get(cci.id);
					
					if(useSignature) emailBody += '<br /><div style="margin-top: 20px;" class="sig">'+getSignature()+'</div>';
					email_md.setHtml(emailBody);

					system.debug('mandrill ==' + email_md);

					listEmails.add(email_md);

					/** instalment activity **/
					DateTime currentTime = DateTime.now();
					instalmentInfo = 'Email;;' + cci.Client_Course__r.client__r.email + ';;' + subject +';;' + currentTime.format('yyyy-MM-dd HH:mm:ss') + ';;sent;;-;;' + userDetails.Name + '!#!';

					if(cci.instalment_activities__c==null || cci.instalment_activities__c=='')
						cci.instalment_activities__c = instalmentInfo;
					else
						cci.instalment_activities__c += instalmentInfo;

				
					/** Save to S3 **/
					Datetime myDT = Datetime.now();
					String myDate = myDT.format('dd-MM-yyyy HH:mm:ss');
					Blob bodyblob = Blob.valueof(emailBody);//Blob body, String subject, String instalment, Contact client, Contact employee, String dateCreated
					s3.generateEmailToS3(mapContactsEmail.get(cci.Client_Course__r.client__r.email), userDetails, true, subject, bodyblob, false, myDate);
					/** END -- Save to S3 **/
				}//end for
			}
			// END -- Instalment Emails



			// 	I N V O I C E 		E M A I L S
			if(allInvoices!=null){

				fieldsTemplate = emailTemplateDynamic.extractFieldsFromFile(mEmailTemplate.get(selectedTemplate).Template__c, mapFieldsInv); 
				tpSubject = emailTemplateDynamic.createTemplate(mapFieldsInv, mEmailTemplate.get(selectedTemplate).Email_Subject__c, fieldsTemplate, allInvoices);
				tpBody = emailTemplateDynamic.createTemplate(mapFieldsInv, mEmailTemplate.get(selectedTemplate).Template__c, fieldsTemplate, allInvoices);
				
				for(Invoice__c cci : allInvoices){

					mandrillSendEmail mse = new mandrillSendEmail();
					mandrillSendEmail.messageJson email_md = new mandrillSendEmail.messageJson();
					email_md.setTo(cci.client__r.email, cci.client__r.name);
					
					if(agency.Clients_Payments_Email__c!=null)
						email_md.setFromEmail(agency.Clients_Payments_Email__c);
					else
						email_md.setFromEmail(userDetails.Email);

					email_md.setFromName(userDetails.Name);

					String subject = tpSubject.get(cci.id);
					email_md.setSubject(subject);
					emailBody = tpBody.get(cci.id);
					
					if(useSignature) emailBody += '<br /><div style="margin-top: 20px;" class="sig">'+getSignature()+'</div>';
					email_md.setHtml(emailBody);

					system.debug('mandrill ==' + email_md);

					listEmails.add(email_md);

					/** instalment activity **/
					DateTime currentTime = DateTime.now();
					instalmentInfo = 'Email;;' + cci.client__r.email + ';;' + subject +';;' + currentTime.format('yyyy-MM-dd HH:mm:ss') + ';;sent;;-;;' + userDetails.Name + '!#!';

					if(cci.invoice_activities__c==null || cci.invoice_activities__c=='')
						cci.invoice_activities__c = instalmentInfo;
					else
						cci.invoice_activities__c += instalmentInfo;



					/** Save to S3 **/
					Datetime myDT = Datetime.now();
					String myDate = myDT.format('dd-MM-yyyy HH:mm:ss');
					Blob bodyblob = Blob.valueof(emailBody);//Blob body, String subject, String instalment, Contact client, Contact employee, String dateCreated
					s3.generateEmailToS3(mapContactsEmail.get(cci.client__r.email), userDetails, true, subject, bodyblob, false, myDate);
					/** END -- Save to S3 **/
				}//end for
			}

			// END -- Invoice Emails

			//Batch to send emails
			if(!Test.isRunningTest()){
				batch_sendMassEmail batchSendMass = new batch_SendMassEmail(listEmails);
				Database.executeBatch(batchSendMass,99);
			}
			/** END -- Send email via Mandrill **/


			if(allInstalments != null && allInstalments.size()>0)
				update allInstalments;
			if(allInvoices != null && allInvoices.size()>0)
				update allInvoices;

			theMsg = 'Email sent.';
		    showMsg = true;

		}catch(Exception e){
			system.debug('Send Emails Error ==='+ e + ' line ==>' + e.getLineNUmber());
			theMsg = e.getMessage();
		    showMsg = true;
		}

	}
	/** END -- Send Emails **/



	/** Send SMS **/
	public void sendSMSMessages(){
		try{
			Messaging.SingleEmailMessage fakeEmail;

			map<string, string> instalmentActivity = new map<string,string>();
			map<string, string> invoiceActivity = new map<string,string>();
			String instalmentInfo;
			Map<String,String> params;


			/** Create Fake Email Instalment/
				List<Messaging.SingleEmailMessage> allMsgs = new List<Messaging.SingleEmailMessage>();
				Schema.SObjectType objType;
				Id objId;
				String typeId;
				String instalmentInfo;
				
				for(client_course_instalment__c i : allInstalments){

					fakeEmail = new Messaging.SingleEmailMessage();

					fakeEmail.setTemplateId(selectedTemplate);
					fakeEmail.setWhatId(i.Id);
					fakeEmail.setUseSignature(true);
					fakeEmail.setTargetObjectId(i.Client_Course__r.Client__c);
					fakeEmail.setSaveAsActivity(false);
					fakeEmail.setToAddresses(new List<String>{i.Client_Course__r.Client__r.Email});

					allMsgs.add(fakeEmail);

				}//end for
				/** END -- Create Fake Email Instalment**/

				/** Create Fake Email Invoice/
				for(Invoice__c i : allInvoices){

					fakeEmail = new Messaging.SingleEmailMessage();

					fakeEmail.setTemplateId(invoiceTemplate.Id);
					fakeEmail.setWhatId(i.Id);
					fakeEmail.setUseSignature(true);
					fakeEmail.setTargetObjectId(i.Client__c);
					fakeEmail.setSaveAsActivity(false);
					fakeEmail.setToAddresses(new List<String>{i.Client__r.Email});

					allMsgs.add(fakeEmail);

				}//end for
				/** END -- Create Fake Email Invoice/

				Savepoint sp = Database.setSavepoint();
				Messaging.sendEmail(allMsgs);
				Database.rollback(sp);

			/** Send SMS **/

			
			String emailBody;
		    list<smsInfo> lSmsInfo = new list<smsInfo>();

		  	DateTime currentTime = DateTime.now();

			map<string,string> tpBody; 
			map<string,string> tpSubject; 

			if(allInstalments != null){
				fieldsTemplate = emailTemplateDynamic.extractFieldsFromFile(mEmailTemplate.get(selectedTemplate).Template__c, mapFieldsInst); 
				tpSubject = emailTemplateDynamic.createTemplate(mapFieldsInst, mEmailTemplate.get(selectedTemplate).Email_Subject__c, fieldsTemplate, allInstalments);
				tpBody = emailTemplateDynamic.createTemplate(mapFieldsInst, mEmailTemplate.get(selectedTemplate).Template__c, fieldsTemplate, allInstalments);

				for(client_course_instalment__c cci : allInstalments){

					for(forms_of_contact__c n : mapContactsEmail.get(cci.client_course__r.client__r.email).forms_of_contact__r){
						if(n.Country__c == mapContactsEmail.get(cci.client_course__r.client__r.email).Current_Agency__r.BillingCountry){
							String statusCallBack;
								if(Userinfo.getOrganizationId() == '00D90000000rVKlEAM')
									statusCallBack = 'https://educationhify.force.com/ip/services/apexrest/sms';

							else if(Userinfo.getOrganizationId() == '00DO00000055066MAA')
								statusCallBack = 'https://devpartial-educationhify.cs5.force.com/hifyplatform/services/apexrest/sms';
							else
								statusCallBack = 'https://devfull-educationhify.cs57.force.com/hifyplatform/services/apexrest/sms';

								instalmentInfo = 'Sending SMS;;' + n.Detail__c + ';;' + tpSubject.get(cci.id) +';;' + currentTime.format('yyyy-MM-dd HH:mm:ss') + ';;pending;;-;;' + userDetails.Name + '!#!';

								if(cci.instalment_activities__c==null || cci.instalment_activities__c=='')
									cci.instalment_activities__c = instalmentInfo;
								else
									cci.instalment_activities__c += instalmentInfo;

								params = new Map<String,String> {
								'To'   => n.Detail__c,
								'From' => '+61439559941',
								'Body' => tpBody.get(cci.id).replaceAll('[^a-zA-Z0-9\\,!-~]+', ' '),
								'StatusCallback' => statusCallBack
							};

							system.debug('sms params==' + params);
							smsInfo smsParam = new smsInfo(params, cci.id,'instalment', tpBody.get(cci.id), n.Contact__c, tpSubject.get(cci.id));
							lSmsInfo.add(smsParam);
						}
					}//end for
				}//end for
			}

			if(allInvoices != null){
				fieldsTemplate = emailTemplateDynamic.extractFieldsFromFile(mEmailTemplate.get(selectedTemplate).Template__c, mapFieldsInv); 
				tpSubject = emailTemplateDynamic.createTemplate(mapFieldsInv, mEmailTemplate.get(selectedTemplate).Email_Subject__c, fieldsTemplate, allInvoices);
				tpBody = emailTemplateDynamic.createTemplate(mapFieldsInv, mEmailTemplate.get(selectedTemplate).Template__c, fieldsTemplate, allInvoices);

				for(Invoice__c cci : allInvoices){
					for(forms_of_contact__c n : mapContactsEmail.get(cci.client__r.email).forms_of_contact__r){
						if(n.Country__c == mapContactsEmail.get(cci.client__r.email).Current_Agency__r.BillingCountry){
							String statusCallBack;
								if(Userinfo.getOrganizationId() == '00D90000000rVKlEAM')
									statusCallBack = 'https://educationhify.force.com/ip/services/apexrest/sms';

							else if(Userinfo.getOrganizationId() == '00DO00000055066MAA')
								statusCallBack = 'https://devpartial-educationhify.cs5.force.com/hifyplatform/services/apexrest/sms';
							else
								statusCallBack = 'https://devfull-educationhify.cs57.force.com/hifyplatform/services/apexrest/sms';

								instalmentInfo = 'Sending SMS;;' + n.Detail__c + ';;' + tpSubject.get(cci.id) +';;' + currentTime.format('yyyy-MM-dd HH:mm:ss') + ';;pending;;-;;' + userDetails.Name + '!#!';

								if(cci.invoice_activities__c==null || cci.invoice_activities__c=='')
									cci.invoice_activities__c = instalmentInfo;
								else
									cci.invoice_activities__c += instalmentInfo;

								params = new Map<String,String> {
								'To'   => n.Detail__c,
								'From' => '+61439559941',
								'Body' => tpBody.get(cci.id).replaceAll('[^a-zA-Z0-9\\,!-~]+', ' '),
								'StatusCallback' => statusCallBack
							};

							system.debug('sms params==' + params);
							smsInfo smsParam = new smsInfo(params, cci.id,'invoice', tpBody.get(cci.id), n.Contact__c, tpSubject.get(cci.id));
							lSmsInfo.add(smsParam);
						}
					}//end for
				}//end for
			}

		//Batch to send SMS
		if(!Test.isRunningTest()){
			batch_sendMassSMS batchSendSMS = new batch_sendMassSMS(lSmsInfo);
			Database.executeBatch(batchSendSMS,99);
		}
		/** END -- Send SMS **/

		if(allInstalments != null && allInstalments.size()>0)
			update allInstalments;
		if(allInvoices != null && allInvoices.size()>0)
			update allInvoices;


			theMsg = 'SMS sent.';
		    showMsg = true;

		}catch(Exception e){
			system.debug('Send Emails Error ==='+ e + ' line ==>' + e.getLineNUmber());
			theMsg = e.getMessage();
		    showMsg = true;
		}

	}
	/** END -- Send SMS **/



	/** Send SMS Via Twilio /
	private list<smsInfo> lSmsInfo {get;set;}
	@future (callout=true)
	public static void sendSMS(map<string,string> params, String instalment, String msgBody, String contactId, String selectedTemplate){
		TwilioRestClient client = new TwilioRestClient(IPFunctions.twAccount, IPFunctions.twKey);

		TwilioSMS sms = client.getAccount().getSMSMessages().create(params);
		system.debug('status ===' + sms.getStatus());

		/** Save information in a Object until get the callback to save in S3 /
	    Email_SMS__c s = new Email_SMS__c();
	    s.from__c = Userinfo.getUserId();
	    s.client_course_instalment__c = instalment;
	    s.Client__c = contactId;
	    s.msgId__c = sms.getSid();
	    s.Type__c = 'SMS';
	    s.Body__c = msgBody;
	    s.Subject__c = selectedTemplate;

	    insert s;
	    /** END -- Save information in a Object until get the callback to save in S3 /

	}
	/** END -- Send SMS Via Twilio **/

	global class smsInfo{
		public map<string,string> params {get{return this.params;}set;}
		public String instalment {get;set;}
		public String typeId {get;set;}
		public String msgBody {get;set;}
		public String contactId {get;set;}
		public String selectedTemplate {get;set;}

		public smsInfo(map<string,string> params, String instalment, String typeId, String msgBody, String contactId, String selectedTemplate ){
			this.params = params;
			this.instalment = instalment;
			this.typeId = typeId;
			this.msgBody = msgBody;
			this.contactId = contactId;
			this.selectedTemplate = selectedTemplate;
		}
	}

	public String instBodyEx {get;set;}
	public String invBodyEx {get;set;}

	public String instSubjectEx {get;set;}
	public String invSubjectEx {get;set;}


	/** Get Group Invoice Email Template /
	private EmailTemplate invoiceTemplate {get;set;} 

	public void findInvoiceTemplate(){
		system.debug('cloneTemaplate==' + mapTemplateName.get(selectedTemplate));
		String accEmailFolder;
		String accSMSFolder;
		if(!Test.isRunningTest()){
			accEmailFolder = '%' +  String.valueOf(agency.id).substring(0, 15) + '_Email_Inv';
			accSMSFolder = '%' +  String.valueOf(agency.id).substring(0, 15) + '_SMS_Inv';
			system.debug('accEmailFolder==' + accEmailFolder);
			system.debug('accSMSFolder==' + accSMSFolder);
		}

		if(!Test.isRunningTest() && action=='email')
			invoiceTemplate = [Select Id, Name, HtmlValue, Subject from EmailTemplate where FolderId in (Select Id from Folder where DeveloperName Like :accEmailFolder)
									and name =:mapTemplateName.get(selectedTemplate) limit 1]; //to get the template equivalent to the instalment template

		else if(!Test.isRunningTest() && action=='sms')
			invoiceTemplate = [Select Id, Name, HtmlValue, Subject from EmailTemplate where FolderId in (Select Id from Folder where DeveloperName Like :accSMSFolder)
									and name =:mapTemplateName.get(selectedTemplate) limit 1]; //to get the template equivalent to the instalment template

		else if(Test.isRunningTest())
			invoiceTemplate = [Select Id, Name, HtmlValue, Subject from EmailTemplate where name =:mapTemplateName.get(selectedTemplate) and isActive = true limit 1]; //to get the template equivalent to the instalment template
	}
	/** END -- Get Group Invoice Email Template **/


	/** Get Email Templates /
	
	public void findEmailTemplates(){

		Id folderId;
		String accEmailFolder;
		String accSMSFolder;
		if(!Test.isRunningTest()){
			accEmailFolder = '%' +  String.valueOf(agency.id).substring(0, 15) + '_Email_Inst';
			accSMSFolder = '%' +  String.valueOf(agency.id).substring(0, 15) + '_SMS_Inst';
			system.debug('accEmailFolder==' + accEmailFolder);
			system.debug('accSMSFolder==' + accSMSFolder);
		}

		if(!Test.isRunningTest() && action=='email')
			folderId = [Select Id from Folder where DeveloperName Like :accEmailFolder].Id;
		else if(!Test.isRunningTest() && action=='sms')
			folderId = [Select Id from Folder where DeveloperName Like :accSMSFolder].Id;
		mapTemplateName = new map<string,string>();

		List<EmailTemplate> templates;
		if(!Test.isRunningTest())
			templates = [Select Id, Name, HtmlValue, Subject from EmailTemplate where FolderId =:folderId order by Name];
		else
			templates = [Select Id, Name, HtmlValue, Subject from EmailTemplate order by createdDate desc limit 5];

		templateOptions = new List<SelectOption>();
		templateOptions.add(new SelectOption('', '-- Select Template --'));
		for(EmailTemplate t : templates){
			templateOptions.add(new SelectOption(t.id, t.Name));
			mapTemplateName.put(t.id, t.Name);
		}

	}
	/** END -- Get Email Templates **/ 


	/** Email Template */
	private map<String,String> mapTemplateName {get;set;}
	public String selectedTemplate {get;set;}
	public list<SelectOption> templateOptions {get;set;}
	public map<Id, Email_Template__c> mEmailTemplate {get;set;}

	private String paymentRef;
	private sObject objTempInst;
	private sObject objTempInv;
	private Back_Office_Control__c userBo;
	private list<string> fieldsTemplate;
	private map<string, Object> mapFieldsInst;
	private map<string, Object> mapFieldsInv;
	private map<Id,String> mapSubject;

	private void findEmailTemplate(String agencyId, String category){
		templateOptions = new list<SelectOption>();
		mEmailTemplate = new map<Id, Email_Template__c>();
		mapSubject = new map<Id, String>();
		String mSubject;

		for(Email_Template__c et : [SELECT Email_Subject__c, Template__c, Template_Description__c FROM Email_Template__c WHERE Agency__c = :agencyId and Category__c = :category order by Email_Subject__c]){
			try{
				
				if(objTempInst != null){
					fieldsTemplate = emailTemplateDynamic.extractFieldsFromFile(et.Email_Subject__c, mapFieldsInst); 
					instSubjectEx = emailTemplateDynamic.createSingleTemplate(mapFieldsInst, et.Email_Subject__c, fieldsTemplate, objTempInst);
					mapSubject.put(et.id, instSubjectEx);
				}

				if(objTempInv != null){
					fieldsTemplate = emailTemplateDynamic.extractFieldsFromFile(et.Email_Subject__c, mapFieldsInv); 
					invSubjectEx = emailTemplateDynamic.createSingleTemplate(mapFieldsInv, et.Email_Subject__c, fieldsTemplate, objTempInv);
					mapSubject.put(et.id, invSubjectEx);
				}


				templateOptions.add(new SelectOption(et.Id, et.Template_Description__c));
				mEmailTemplate.put(et.id, et);
			}catch(Exception e){
				system.debug('fieldsTemplate==>' + fieldsTemplate);
				system.debug('et.Email_Subject__c==>' + et.Email_Subject__c);
			}
		}//end for

		if(templateOptions.size()>0){
			selectedTemplate = templateOptions[0].getValue();
			changeTemplate();
		}
	}

	public void changeTemplate(){
		if(selectedTemplate != null && selectedTemplate != ''){

			if(objTempInst != null){
				instSubjectEx = mapSubject.get(selectedTemplate);
				fieldsTemplate = emailTemplateDynamic.extractFieldsFromFile(mEmailTemplate.get(selectedTemplate).Template__c, mapFieldsInst); 
				instBodyEx = emailTemplateDynamic.createSingleTemplate(mapFieldsInst, mEmailTemplate.get(selectedTemplate).Template__c, fieldsTemplate, objTempInst);	

				if(action == 'email')
				instBodyEx += getSignature();
			}

			if(objTempInv != null){	
				invSubjectEx = mapSubject.get(selectedTemplate);
				fieldsTemplate = emailTemplateDynamic.extractFieldsFromFile(mEmailTemplate.get(selectedTemplate).Template__c, mapFieldsInv); 
				invBodyEx = emailTemplateDynamic.createSingleTemplate(mapFieldsInv, mEmailTemplate.get(selectedTemplate).Template__c, fieldsTemplate, objTempInv);

				if(action == 'email')
				invBodyEx += getSignature();
			}

		}
		else{
			instSubjectEx = '';
			invSubjectEx = '';

			instBodyEx = '';
			invBodyEx = '';
		}
	}
	/** END -- Email Template */



	public string getSignature(){
	    if(useSignature && userDetails.Signature__c != null && userDetails.Signature__c != '' )
	      return userDetails.Signature__c;
	    else return textSignature;
	}

	public String textSignature {
	  	get{
	  		if(textSignature == null){
	  			textSignature = '<div style="margin-top: 20px;" class="sig">' + userdetails.Name;
	            textSignature += '<br />' + userDetails.Account.Name;
	            textSignature += '<br />' + userDetails.Email + ' </div>';
	  		}
	  		return textSignature;
	  	}
	  	set;
	  }

	public Contact userDetails{
	    get{
	      if(userDetails == null){
	      	if(!Test.isRunningTest()){
	      		string contactId = [Select ContactId from user where id = :UserInfo.getUserId() limit 1].ContactId;
	       		userDetails = [select id, Name, Email, Signature__c, AccountId, Account.Name, Account.ParentId, Account.Global_Link__c, Account.Clients_Payments_Email__c from Contact where id = :contactId];
	      	} else {
	      		userDetails = new Contact();
	      	}
	      }
	      return userDetails;
	    }
	    set;
	}
}