@isTest
private class filter_newcourse_test {

    public static User portalUser {get{
		if (null == portalUser) {
			portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
		}
		return portalUser;
	}set;}
    
    @testSetup static void setup() {
		TestFactory tf = new TestFactory();
		Account schGroup = tf.createSchoolGroup();

		Account school = tf.createSchool();
		school.ParentId = schGroup.Id;
		update school;

		Account agency = tf.createAgency();
		Contact emp = tf.createEmployee(agency);
		portalUser = tf.createPortalUser(emp);
		Account campus = tf.createCampus(school, agency);
		Course__c course = tf.createCourse();
		Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);

		Test.startTest();
			system.runAs(portalUser){
				Contact client = tf.createClient(agency);
				client_course__c clientCourseBooking = tf.createBooking(client);
				client_course__c clientCourse = tf.createClientCourse(client, school, campus, course, campusCourse, clientCourseBooking);
			}
		Test.stopTest();
	}

    @isTest static void testAddPackage(){
        Contact client = [SELECT Id FROM Contact WHERE RecordType.Name = 'Client' limit 1];
        Account school = [SELECT Id FROM Account WHERE RecordType.Name = 'school' limit 1];
        Account campus = [SELECT Id FROM Account WHERE RecordType.Name = 'campus' limit 1];

        client_course__c clientCourse = [SELECT Id FROM client_course__c WHERE isBooking__c = false];
        filter_newcourse.searchSchools('Australia');
        filter_newcourse.searchCampuses(school.Id);
        filter_newcourse.searchCourses(campus.Id);

        ApexPages.currentPage().getParameters().put('nc', clientCourse.Id );
        filter_newcourse test = new filter_newcourse(new Apexpages.Standardcontroller(client));
        test.addCourse();

    }

}