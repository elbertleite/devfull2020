@isTest
private class client_documents_list_test {
	
	static testMethod void myUnitTest() {
		TestFactory tf = new TestFactory();
		Account school = tf.createSchool();
		Account agencyGroup1 = tf.createAgencyGroup();
		Account agency = tf.createSimpleAgency(agencyGroup1);
		Contact emp = tf.createEmployee(agency);
		User portalUser = tf.createPortalUser(emp);
		Account campus = tf.createCampus(school, agency);
		Course__c course = tf.createCourse();
		Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
		Contact client = tf.createClient(agency);

		client_course__c booking = tf.createBooking(client);
		client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);


		client_documents_list toTest = new client_documents_list(new ApexPages.StandardController(client));

		List<SelectOption> documentsCategory = toTest.documentsCategory;
		List<SelectOption> countries = toTest.countries;

		toTest.selectedCategory = 'Travel;#;Flight Ticket, Enrolment;#;LOO, Enrolment;#;Enrolment Form';
		toTest.createDocument();
		String docId = toTest.toCreateDocument[0].id;
		toTest.toCreateDocument[0].client_course__c = cc.Id;
		toTest.toCreateDocument[1].client_course__c = cc.Id;
		toTest.toCreateDocument[2].client_course__c = cc.Id;
		toTest.toCreateDocument[0].preview_link__c = 'https://s3.amazonaws.com/test/docs!#!test.pdf';
		toTest.toCreateDocument[1].preview_link__c = 'https://s3.amazonaws.com/test/docs!#!test.pdf';
		toTest.toCreateDocument[2].preview_link__c = 'https://s3.amazonaws.com/test/docs!#!test.pdf';
		

		toTest.saveNCloseDocuments();

		ApexPages.currentPage().getParameters().put('cId', docId);
		toTest.editDocument();

		List<Client_Flight_Details__c> flights = toTest.mapDocFlights.get(docId);

		ApexPages.currentPage().getParameters().put('docId', docId);
		ApexPages.currentPage().getParameters().put('flightID', flights[0].id);

		toTest.deleteFlight();

		ApexPages.currentPage().getParameters().put('docID', docId);
		toTest.addNewFlight();

		flights = toTest.mapDocFlights.get(docId);
		
		ApexPages.currentPage().getParameters().put('docId', docId);
		ApexPages.currentPage().getParameters().put('flightID', string.valueOf(flights[0].Custom_ID__c));

		toTest.deleteNewFlight();

		toTest.deleteDocument();

		

	}
	
}