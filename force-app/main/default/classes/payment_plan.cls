public with sharing class payment_plan {
	string ct;
	string pl;
	public String bucketName {get;set;}
	public String gbLink {get;set;}
	public String userCurrency {get;set;}
	user currentUser = [Select id, Contact.AccountId, Contact.Account.Account_Currency_Iso_Code__c, contact.account.name, Contact.Account.Global_Link__c from user where id =:userInfo.getUserId() limit 1];
	//currentUser = [Select Id, Name, Contact.Account.ParentId, Contact.AccountId, Contact.Department__c, Contact.Account.Account_Currency_Iso_Code__c from User where id = :UserInfo.getUserId() limit 1];
		
	public payment_plan(ApexPages.StandardController ctrl) {
		ct = ctrl.getId();
		pl = ApexPages.CurrentPage().getParameters().get('pl');
		retriveNumInstalList();
		bucketName = IPFunctions.s3bucket;
		gbLink = currentUser.Contact.Account.Global_Link__c;
		userCurrency = currentUser.Contact.Account.Account_Currency_Iso_Code__c;
		if(pl != null)
			retrievePaymentPlans();
		else listPaymentPlans = new planDetails();
		listPaymentPlans.nextPayment.Currency_Paid__c = currentUser.Contact.Account.Account_Currency_Iso_Code__c;
		retrieveMainCurrencies();
		startCurrencyRate(listPaymentPlans.nextPayment);
	}

	private void startCurrencyRate(Payment_Plan__c paymentPlan){
		paymentPlan.Value_in_Local_Currency__c = 1;
		//paymentPlan.Currency_Rate__c = agencyCurrencies.get(currentUser.Contact.Account.Account_Currency_Iso_Code__c);
		paymentPlan.Value_in_Local_Currency__c = null;
		paymentPlan.Value_Paid__c = null;
		recalculateRate();
	}

	public class fileLinks{
		public string fileName{get;set;}
		public string previewLInk{get;set;}
		public string previewLinkDownload{get;set;}
		public fileLinks(string fileName, string previewLInk, string previewLinkDownload){
			this.fileName = fileName;
			this.previewLInk = previewLInk;
			this.previewLinkDownload = previewLinkDownload;
		}
	}

	public class instalmentPaymentBreak{
		public string payment_type{get; set;}
		public date paid_date{get; set;}
		public decimal payment_value{get; set;}
		public instalmentPaymentBreak(string payment_type, date paid_date, decimal payment_value){
			this.payment_type = payment_type;
			this.paid_date = paid_date;
			this.payment_value = payment_value;
		}
	}

	public class planItem{
		public Payment_Plan__c itemPlan{get{if(itemPlan == null) itemPlan = new Payment_Plan__c(); return itemPlan;} set;} 
		public list<fileLinks> listFiles{get{if(listFiles == null) listFiles = new list<fileLinks>(); return listFiles;} set;} 
		public list<noteDetails> listNotes{get{if(listNotes == null) listNotes = new list<noteDetails>(); return listNotes;} set;} 
		public list<instalmentPaymentBreak> installBreak{get{if(installBreak == null) installBreak = new list<instalmentPaymentBreak>(); return installBreak;} set;} 
		public boolean isEditing{get{if(isEditing == null) isEditing = false; return isEditing;} set;}
		public boolean isSaved{get{if(isSaved == null) isSaved = false; return isSaved;} set;}
		public decimal instalRemaining{get; set;}
		public string errorMsg{get; set;}
	}
	
	public class planDetails{
		public Payment_Plan__c installPlan{get{if(installPlan == null) installPlan = new Payment_Plan__c(); return installPlan;} set;} 
		public Payment_Plan__c nextPayment{get{if(nextPayment == null) nextPayment = new Payment_Plan__c(); return nextPayment;} set;} 
		public list<noteDetails> listNotes{get{if(listNotes == null) listNotes = new list<noteDetails>(); return listNotes;} set;} 
		public map<string,planItem> listInstalments{get{if(listInstalments == null) listInstalments = new map<string,planItem>(); return listInstalments;} set;} 
		public integer listSize{get{if(listSize == null) listSize = 0; return listSize;} set;}
		public string errorMsgPlan{get; set;}
	}

	public class noteDetails implements Comparable{
		public string noteOn{get;set;}
		public string noteBy{get;set;}
		public string noteAgency{get;set;}
		public string noteDesc{get;set;}
		public noteDetails(string noteOn, string noteBy, string noteAgency, string noteDesc){
			this.noteOn = noteOn;
			this.noteBy = noteBy;
			this.noteAgency = noteAgency;
			this.noteDesc = noteDesc;
		}

		public Integer compareTo(object compareTo) {
			noteDetails noteDetailsToCompare = (noteDetails)compareTo;
			
			if(datetime.parse(noteOn) > datetime.parse(noteDetailsToCompare.noteOn)){
				return -1;
			}else if(datetime.parse(noteOn) < datetime.parse(noteDetailsToCompare.noteOn)){
				return 1;
			}
			return 0;
		}
	}

	public Payment_Plan__c plan{
		get{
			if(plan == null){
				plan = new Payment_Plan__c();
				plan.Reminder_Date__c = System.today();
				plan.Plan_Type__c = 'instalments';
				plan.Currency_Code__c = currentUser.Contact.Account.Account_Currency_Iso_Code__c;
			}  
			return plan;
		} 
		set;
	}

	public string errorDesc{get; set;}
	public string errorValue{get; set;}
	public pageReference savePlan(){
		errorDesc = null;
		errorValue = null;
		if(plan.Description__c == null || plan.Description__c.trim() == ''){
			errorDesc = 'Payment Description is Required';
			return null;
		}
		if(plan.value__c == null || plan.value__c <= 0){
			errorValue = 'Value is Required';
			return null;
		}
		plan.Client__c = ct;
		plan.Value_Remaining__c = plan.value__c;
		plan.Total_Paid__c = 0;
		plan.Created_By_Agency__c = currentUser.Contact.AccountId;
		insert plan;


		list<Payment_Plan__c> instalments = new list<Payment_Plan__c>();
		integer numInstalments = integer.valueOf(plan.Number_of_Instalments__c);
		date dt;
		decimal isntalValue = 0;
		if(listPaymentPlans.installPlan.Plan_Type__c == 'instalments' || plan.Plan_Type__c == 'instalments'){
			for(integer i = 1; i <= numInstalments; i++){
				if(dt == null)
					dt = System.today();
				else dt = System.today().addDays(15);
				System.debug('===>i: '+i);  
				System.debug('===>dt: '+dt);  
				if(i < numInstalments)
					isntalValue = (plan.value__c/numInstalments).setScale(2);
				else isntalValue = (plan.value__c - isntalValue * (i-1)).setScale(2);
				instalments.add(new Payment_Plan__c(Client__c = ct, Instalments__c = plan.id, due_date__c = dt, instalment_Number__c = i, value__c = isntalValue, Value_Remaining__c = plan.value__c, Total_Paid__c = plan.Total_Paid__c, Currency_Code__c = plan.Currency_Code__c));
			}
		} else instalments.add(new Payment_Plan__c(Client__c = ct, Instalments__c = plan.id, instalment_Number__c = 1,  value__c = plan.value__c, Value_Remaining__c = plan.value__c, Total_Paid__c = plan.Total_Paid__c, Currency_Code__c = plan.Currency_Code__c));
		insert instalments;
		PageReference newplan = new PageReference('/apex/contact_new_payment_plan?id='+ct+'&pl='+plan.id);
		newplan.setRedirect(true);

		return newplan;
	}

	public planDetails listPaymentPlans{get; set;}

	private void retrievePaymentPlans(){
		listPaymentPlans = new planDetails(); 
		planItem item;
		for(Payment_Plan__c pp:[Select id, BO_Date__c, preview_link__c, Payment_Type__c, BO_Notes__c, BO_Value__c, Value__c, Instalments__c, Value_Remaining__c, Total_Paid__c, Total_Available__c, description__c, Client__c, Value_Paid__c, Currency_Code__c, Currency_Paid__c, Currency_Rate__c, Value_in_Local_Currency__c, 
									Received_By__r.name, Received_By_Agency__r.name, Received_On__c, Payment_Confirmed_On__c, Payment_Confirmed_By__r.name, Loan_Status__c, Loan_Approved_On__c,
									Notes__c, Reminder_Date__c, 
									instalment_Number__c, Plan_Type__c, due_date__c, Number_of_Instalments__c,
									(Select id, Received_By_Agency__c, Received_By__c, Type_of_Payment__c, Payment_Type__c, Received_On__c, Value__c, Date_Paid__c from client_course_instalment_payments__r)
									// lastModifiedDate, lastModifiedBy.Name, lastModifiedBy.SmallPhotoURL, 
									from Payment_Plan__c where client__c = :ct and (id = :pl or instalments__c = :pl) order by instalments__c nulls first, createdDate, instalment_Number__c]){
			item = new planItem();
			System.debug('===>pp: '+pp);	
			if(pp.Instalments__c == null){
				if(pp.notes__c != null && pp.notes__c.trim() != ''){
					listPaymentPlans.listNotes.addAll(new list<noteDetails>((list<noteDetails>)JSON.deserialize(pp.notes__c,list<noteDetails>.class)));
					listPaymentPlans.listNotes.sort();
				}
				listPaymentPlans.installPlan = pp;
			}
			else if(pp.Value_Paid__c == null && pp.Instalments__c != null){
				
				// date dt = listPaymentPlans.nextPayment.BO_Date__c;
				// string dts = dt.format();
				// listPaymentPlans.nextPayment.BO_Date__c = date.valueOf(dts);
				System.debug('===>listPaymentPlans.installPlan.Plan_Type__c: '+listPaymentPlans.installPlan.Plan_Type__c);
				if(listPaymentPlans.installPlan.Plan_Type__c == 'instalments'){
					pp.Currency_Paid__c = currentUser.Contact.Account.Account_Currency_Iso_Code__c;
					pp.Reminder_Date__c = null;
					if(agencyCurrencies != null)
						 startCurrencyRate(pp);
					item.itemPlan = pp;
					if(pp.client_course_instalment_payments__r.size() > 0){
						for(client_course_instalment_payment__c lp:pp.client_course_instalment_payments__r){
							item.installBreak.add(new instalmentPaymentBreak(lp.Type_of_Payment__c, lp.Date_Paid__c, lp.Value__c));
						}
						item.isSaved = true;
						item.instalRemaining = 0;
					}
					if(pp.notes__c != null && pp.notes__c.trim() != ''){ //ONLY ONE NOTE SAVED
						for(noteDetails ln:(list<noteDetails>)JSON.deserialize(pp.notes__c,list<noteDetails>.class))
							item.itemPlan.Notes__c = ln.noteDesc;
					}
					listPaymentPlans.listInstalments.put(pp.id,item); 
				}else{
					listPaymentPlans.nextPayment = pp;
					if(pp.notes__c != null && pp.notes__c.trim() != ''){
						listPaymentPlans.listNotes.addAll(new list<noteDetails>((list<noteDetails>)JSON.deserialize(pp.notes__c,list<noteDetails>.class)));
						listPaymentPlans.listNotes.sort();
						pp.notes__c = '';
					}
				}
			}
			else if(pp.Value_Paid__c != null && pp.Instalments__c != null){
				item.itemPlan = pp;
				if(pp.preview_link__c != null){ 
					for(string pl:pp.preview_link__c.split(';;'))
						if(pl.split('!#!').size() > 2)
							item.listFiles.add(new fileLinks(pl.split('!#!')[1],pl.split('!#!')[0],pl.split('!#!')[2]));
						else item.listFiles.add(new fileLinks(pl.split('!#!')[1],pl.split('!#!')[0],null));
				}
				System.debug('==>notes__c: '+pp.notes__c);
				if(pp.notes__c != null && pp.notes__c.trim() != ''){
					item.listNotes.addAll(new list<noteDetails>((list<noteDetails>)JSON.deserialize(pp.notes__c,list<noteDetails>.class)));
					item.listNotes.sort();
				}
				listPaymentPlans.listInstalments.put(pp.id,item);
			}

			listPaymentPlans.listSize = listPaymentPlans.listInstalments.size();
		}
	}

	public string error{get; set;}
	public void addPayment(){
		error = null;
		date dt = listPaymentPlans.nextPayment.Reminder_Date__c;
		if(listPaymentPlans.installPlan.Reminder_Date__c != null)
			listPaymentPlans.nextPayment.Reminder_Date__c = listPaymentPlans.installPlan.Reminder_Date__c;
		//listPaymentPlans.installPlan.notes__c = listPaymentPlans.nextPayment.notes__c;
		recalculateRate();
		if(listPaymentPlans.nextPayment.Payment_Type__c == null){
			error = 'Error: Please select the payment type';
		}else 
		if(listPaymentPlans.nextPayment.Value_Paid__c > listPaymentPlans.nextPayment.value__c){
			error = 'Error: Value Paid cannot be bigger than Total Remaing';
		}else if(listPaymentPlans.nextPayment.Value_in_Local_Currency__c == null || listPaymentPlans.nextPayment.Value_in_Local_Currency__c <= 0){
			error = 'Error: Value Paid cannot be empty or less/Equal Zero';
		// }else if(listPaymentPlans.nextPayment.Reminder_Date__c < System.today()){
		// 	listPaymentPlans.nextPayment.Reminder_Date__c.addError('Remainder cannot be empty or before current date');
		// }
		}else{
			listPaymentPlans.nextPayment.Received_By_Agency__c = currentUser.Contact.AccountId;
			listPaymentPlans.nextPayment.Received_By__c = currentUser.id; 
			listPaymentPlans.nextPayment.Received_On__c = System.today();
			if(listPaymentPlans.nextPayment.notes__c != null && listPaymentPlans.nextPayment.notes__c.trim() != ''){
				// listPaymentPlans.nextPayment.notes__c = JSON.serialize(new list<noteDetails>{new noteDetails( System.now().format(), userInfo.getName(), currentUser.contact.account.name, listPaymentPlans.nextPayment.notes__c)});
				listPaymentPlans.listNotes.add(new noteDetails( System.now().format(), userInfo.getName(), currentUser.contact.account.name, listPaymentPlans.nextPayment.notes__c));
				listPaymentPlans.listNotes.sort();
			}
			if(listPaymentPlans.listNotes.size() > 0)
				listPaymentPlans.nextPayment.notes__c = JSON.serialize(listPaymentPlans.listNotes);
			update listPaymentPlans.nextPayment;

			listPaymentPlans.installPlan.Value_Remaining__c = listPaymentPlans.installPlan.Value_Remaining__c.setScale(4) - listPaymentPlans.nextPayment.Value_Paid__c.setScale(4);
			listPaymentPlans.installPlan.Reminder_Date__c = dt;
			listPaymentPlans.installPlan.Total_Available__c = listPaymentPlans.installPlan.Total_Available__c.setScale(4) + listPaymentPlans.nextPayment.Value_Paid__c.setScale(4);
			// listPaymentPlans.installPlan.Received_By_Agency__c = currentUser.Contact.AccountId;
			// listPaymentPlans.installPlan.Received_By__c = currentUser.id; 
			// listPaymentPlans.installPlan.Received_On__c = System.today();
			if(listPaymentPlans.nextPayment.value__c - listPaymentPlans.nextPayment.Value_Paid__c > 0){
				integer nextInstall = listPaymentPlans.listInstalments.size()+2;
				Payment_Plan__c instalment = new Payment_Plan__c(Client__c = ct, Instalments__c = listPaymentPlans.installPlan.id, instalment_Number__c = nextInstall, value__c = listPaymentPlans.nextPayment.value__c.setScale(4) - listPaymentPlans.nextPayment.Value_Paid__c.setScale(4), Currency_Code__c = listPaymentPlans.installPlan.Currency_Code__c, Reminder_Date__c = dt);

				insert instalment;
				
				// client_course_instalment_payment__c	pp = new client_course_instalment_payment__c();
				// pp.Received_By_Agency__c = currentUser.Contact.AccountId;
				// pp.Received_By__c = UserInfo.getUserId();
				// pp.Received_On__c = System.today();
				// pp.Date_Paid__c = lp.paid_date;
				// pp.Type_of_Payment__c = lp.payment_type;
				// pp.Value__c = lp.payment_value;
				// pp.Payment_Plan__c = instalment.id;
				// listPayments.add(pp);
			}else{
				listPaymentPlans.installPlan.Reminder_Date__c = null;
			}
			update listPaymentPlans.installPlan;
			retrievePaymentPlans(); 
		}

	}

	

	public void deleteAll(){
		delete [select id from Payment_Plan__c where Client__c = :ct];
		plan = new Payment_Plan__c();
		retrievePaymentPlans();
	}


	//================CURRENCY=============================
	private Map<String, double> agencyCurrencies;
	public List<SelectOption> MainCurrencies{get; set;}
	public decimal groupCurrencyRate{get; set;}
	public Datetime currencyLastModifiedDate {get;set;}
	public void retrieveMainCurrencies(){
 
	    MainCurrencies = fin.retrieveMainCurrencies();

        currencyLastModifiedDate = fin.currencyLastModifiedDate;

		agencyCurrencies = fin.retrieveAgencyCurrencies();
	}
	private financeFunctions fin = new financeFunctions();
	public void recalculateRate(){
		boolean isnullValue = false;
		if(listPaymentPlans.nextPayment.Value_in_Local_Currency__c == null || listPaymentPlans.nextPayment.Value_in_Local_Currency__c == 0){
			listPaymentPlans.nextPayment.Value_in_Local_Currency__c = 1;
			isnullValue = true;
		}
		//if(listPaymentPlans.nextPayment.Currency_Rate__c != null && listPaymentPlans.nextPayment.Value_in_Local_Currency__c != null){
			if(groupCurrencyRate == null || groupCurrencyRate == listPaymentPlans.nextPayment.Currency_Rate__c)
				fin.convertCurrencyMaster(listPaymentPlans.nextPayment, listPaymentPlans.installPlan.Currency_Code__c, listPaymentPlans.nextPayment.Value_in_Local_Currency__c, null, 'Currency_Paid__c', 'Currency_Rate__c', 'Value_Paid__c');
			else fin.convertCurrencyMaster(listPaymentPlans.nextPayment, listPaymentPlans.installPlan.Currency_Code__c, listPaymentPlans.nextPayment.Value_in_Local_Currency__c, listPaymentPlans.nextPayment.Currency_Rate__c, 'Currency_Paid__c', 'Currency_Rate__c', 'Value_Paid__c');
			groupCurrencyRate = listPaymentPlans.nextPayment.Currency_Rate__c;
		//}

		if(isnullValue){
			listPaymentPlans.nextPayment.Value_in_Local_Currency__c = null;
		}


	}


	public string newNote{get; set;}
	public list<noteDetails> newlistNotes{get{if(newlistNotes == null) newlistNotes = new list<noteDetails>(); return newlistNotes;} set;} 
	public Payment_Plan__c planUpdate{get; set;}
	//private decimal planUpdateOriginalValue;
	public void selectItemUpdate(){
		newNote = '';
		string itemId = ApexPages.currentPage().getParameters().get('itemId');
		planUpdate = [select id, preview_link__c, Payment_Type__c, Value__c, BO_Value__c, Instalments__c, Value_Remaining__c, Total_Paid__c, Total_Available__c, description__c, Client__c, Value_Paid__c, Currency_Code__c, Currency_Paid__c, Currency_Rate__c, Value_in_Local_Currency__c, 
		Received_On__c, Notes__c from Payment_Plan__c where id = :itemId]; 
		//planUpdateOriginalValue = planUpdate.Value_in_Local_Currency__c;
		newlistNotes = new list<noteDetails>();
		if(planUpdate.notes__c != null && planUpdate.notes__c.trim() != ''){
			newlistNotes.addAll(new list<noteDetails>((list<noteDetails>)JSON.deserialize(planUpdate.notes__c,list<noteDetails>.class)));
		} 
	} 

	public void saveRequest(){
		System.debug('==>listPaymentPlans.installPlan.Value_Remaining__c: '+listPaymentPlans.installPlan.Value_Remaining__c);
		System.debug('==>planUpdate.Value_Paid__c: '+planUpdate.Value_Paid__c);
		System.debug('==>planUpdate.BO_Value__c: '+planUpdate.BO_Value__c);
		boolean updateValue = true;
		if(listPaymentPlans.installPlan.Value_Remaining__c == 0 && planUpdate.Value_Paid__c > planUpdate.BO_Value__c){
			Payment_Plan__c instalment = new Payment_Plan__c(Client__c = ct, Instalments__c = planUpdate.Instalments__c, value__c = planUpdate.Value_Paid__c.setScale(4) - planUpdate.BO_Value__c.setScale(4), Currency_Code__c = listPaymentPlans.installPlan.Currency_Code__c);
			System.debug('==>instalment: '+instalment);
			insert instalment;
			System.debug('==>instalment: '+instalment);
		}else if((listPaymentPlans.installPlan.Value_Remaining__c > 0 && (planUpdate.BO_Value__c - planUpdate.Value_Paid__c) > listPaymentPlans.installPlan.Value_Remaining__c) || (listPaymentPlans.installPlan.Value_Remaining__c == 0 && planUpdate.Value_Paid__c < planUpdate.BO_Value__c)){
			//add error msg
			updateValue = false;
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Value is bigger than total plan!');
            ApexPages.addMessage(myMsg);
			System.debug('==>error: ');
		}
		//else{
		if(updateValue){
			System.debug('==>updateValue: ');
			if(planUpdate.Value_Paid__c > planUpdate.BO_Value__c){
				listPaymentPlans.installPlan.Value_Remaining__c = listPaymentPlans.installPlan.Value_Remaining__c.setScale(4) + (planUpdate.Value_Paid__c.setScale(4) - planUpdate.BO_Value__c.setScale(4));
				listPaymentPlans.installPlan.Total_Available__c = listPaymentPlans.installPlan.Total_Available__c.setScale(4) - (planUpdate.Value_Paid__c.setScale(4) - planUpdate.BO_Value__c.setScale(4));
			}else{
				listPaymentPlans.installPlan.Value_Remaining__c = listPaymentPlans.installPlan.Value_Remaining__c.setScale(4) -  math.abs(planUpdate.Value_Paid__c.setScale(4) - planUpdate.BO_Value__c.setScale(4));
				listPaymentPlans.installPlan.Total_Available__c = listPaymentPlans.installPlan.Total_Available__c.setScale(4) +  math.abs(planUpdate.Value_Paid__c.setScale(4) - planUpdate.BO_Value__c.setScale(4));
			}

			if(listPaymentPlans.nextPayment != null && listPaymentPlans.nextPayment.id != null){
				if(listPaymentPlans.installPlan.Value_Remaining__c != 0){
					listPaymentPlans.nextPayment.Value__c = listPaymentPlans.installPlan.Value_Remaining__c;
					listPaymentPlans.nextPayment.Value_Paid__c = null;
					update listPaymentPlans.nextPayment;
				}else{
					delete [Select id from Payment_Plan__c where id = :listPaymentPlans.nextPayment.id];
				}
			}
			planUpdate.Value_Paid__c = planUpdate.BO_Value__c;


			if(newNote != null && newNote.trim() != ''){
				newlistNotes.add(new noteDetails( System.now().format(), userInfo.getName(), currentUser.contact.account.name, newNote));
				planUpdate.notes__c = JSON.serialize(newlistNotes);
			}

			update planUpdate;
			update listPaymentPlans.installPlan;
			update_remainingValue();
			planUpdate = null;
			retrievePaymentPlans();
		}
	}

	private void update_remainingValue(){
		list<Payment_Plan__c> listInstall = [Select id, Value__c, Value_Paid__c from Payment_Plan__c where Instalments__c = :pl order by createddate];
		for(integer i = 1; i < listInstall.size(); i++)
			listInstall[i].Value__c = listInstall[i-1].Value__c - listInstall[i-1].Value_Paid__c;
		update listInstall;
	}


	public void payInstalment(){
		string instalId = ApexPages.currentPage().getParameters().get('instalId');
		planItem pi = listPaymentPlans.listInstalments.get(instalId);
		System.debug('===> pi: '+pi);
		decimal maxValueToPay;
		if(pi.itemPlan.Payment_Type__c == null){
			pi.errorMsg = 'Please select Payment Type';
		}else if(pi.itemPlan.Value_in_Local_Currency__c == null || pi.itemPlan.Value_in_Local_Currency__c <= 0){
			pi.errorMsg = 'Value Paid need to be bigger than 0';
		}
		else{
			if(pi.instalRemaining == null){
				pi.instalRemaining = pi.itemPlan.Value__c - pi.itemPlan.Value_in_Local_Currency__c;
				maxValueToPay = pi.itemPlan.Value__c;
			}
			else{
				maxValueToPay = pi.instalRemaining;
				pi.instalRemaining -= pi.itemPlan.Value_in_Local_Currency__c;
			} 
			if(pi.instalRemaining >= 0){
				pi.installBreak.add(new instalmentPaymentBreak(pi.itemPlan.Payment_Type__c, pi.itemPlan.Received_On__c, pi.itemPlan.Value_in_Local_Currency__c));
				pi.isEditing = true;
				pi.itemPlan.Value_in_Local_Currency__c = pi.instalRemaining;
				pi.itemPlan.Payment_Type__c = null;
				pi.errorMsg = null;
			}else{
				pi.instalRemaining = maxValueToPay;
				pi.errorMsg = 'Value cannot be bigger than '+ maxValueToPay;
			}
		}

	}

	public list<selectOption> addPaymentsType{
		get{
			if(addPaymentsType == null){
				addPaymentsType = new list<selectOption>();
				Schema.DescribeFieldResult fieldResult = client_course__c.Deposit_Type__c.getDescribe();
				List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
				for( Schema.PicklistEntry f : ple)
					if(f.getValue().toLowerCase()=='client scholarship' || f.getValue().toLowerCase()=='school credit' || f.getValue().toLowerCase()=='Covered by Agency' || f.getValue().toLowerCase()=='offshore' || f.getValue().toLowerCase()=='covered bycAgency' || f.getValue().toLowerCase()=='agency instament' || f.getValue().toLowerCase()=='payment 3rd party')
						continue;

					else
						addPaymentsType.add(new SelectOption(f.getLabel(), f.getValue(), false));
			}
			return addPaymentsType;
		}
		set;
	}

	public void removePayment(){
		string instalId = ApexPages.currentPage().getParameters().get('instalId');
		integer pos = integer.valueOf(ApexPages.currentPage().getParameters().get('position'));
		planItem pi = listPaymentPlans.listInstalments.get(instalId);

		pi.instalRemaining += pi.installBreak.get(pos).payment_value;
		pi.itemPlan.Value_in_Local_Currency__c = pi.instalRemaining;
		pi.itemPlan.Payment_Type__c = null;
		pi.isSaved = false;
		pi.errorMsg = null;
		pi.installBreak.remove(pos);

	}

	public void cancelIsntalPayment(){
		retrievePaymentPlans();
	}

	public void saveIsntalPayment(){
		string instalId = ApexPages.currentPage().getParameters().get('instalId');

		planItem pi = listPaymentPlans.listInstalments.get(instalId);
		client_course_instalment_payment__c pp;
		list<client_course_instalment_payment__c> listPayments = new list<client_course_instalment_payment__c>();
		for(instalmentPaymentBreak lp:pi.installBreak){
			pp = new client_course_instalment_payment__c();
			pp.Received_By_Agency__c = currentUser.Contact.AccountId;
			pp.Received_By__c = UserInfo.getUserId();
			pp.Received_On__c = System.today();
			pp.Date_Paid__c = lp.paid_date;
			pp.Type_of_Payment__c = lp.payment_type;
			pp.Payment_Type__c = lp.payment_type;
			pp.client__c = ct;
			pp.Value__c = lp.payment_value;
			pp.Agency_Currency_Value__c = lp.payment_value;
			pp.Agency_Currency_rate__c = 1;
			pp.Payment_Plan__c = instalId;
			listPayments.add(pp);
		}
		// listPaymentPlans.installPlan.Value_Remaining__c = listPaymentPlans.installPlan.Value_Remaining__c - pi.itemPlan.Value__c;
		listPaymentPlans.installPlan.Total_Paid__c = listPaymentPlans.installPlan.Total_Paid__c + pi.itemPlan.Value__c;
		update new Payment_Plan__c(id = instalId,Received_On__c = System.today(), Received_By__c = UserInfo.getUserId(), Received_By_Agency__c = currentUser.Contact.AccountId, Received_Date__c = System.today(), Notes__c = JSON.serialize(new list<noteDetails>{new noteDetails( System.now().format(), userInfo.getName(), currentUser.contact.account.name, pi.itemPlan.Notes__c)}));
		insert listPayments;
		update listPaymentPlans.installPlan;
		pi.isSaved = true;
		retrievePaymentPlans();
	}
	

	public void addInstalment(){
		string planId = ApexPages.currentPage().getParameters().get('pl');
		integer nextInstall = listPaymentPlans.listInstalments.size()+1;
		Payment_Plan__c instalment = new Payment_Plan__c(Client__c = ct, Instalments__c = planId, due_date__c = System.today(), instalment_Number__c = nextInstall, value__c = 0, Currency_Code__c = listPaymentPlans.installPlan.Currency_Code__c);
		insert instalment;
		update new Payment_Plan__c(id = planId, Number_of_Instalments__c = nextInstall);
		retrievePaymentPlans();
	}

	public void saveUpdate(){
		string planId = ApexPages.currentPage().getParameters().get('pl');
		delete [Select id from Payment_Plan__c where instalments__c = :planId];

		list<Payment_Plan__c> instalments = new list<Payment_Plan__c>();
		integer numInstalments = integer.valueOf(listPaymentPlans.installPlan.Number_of_Instalments__c);
		date dt;
		decimal isntalValue = 0;
		for(integer i = 1; i <= numInstalments; i++){
			if(dt == null)
				dt = System.today();
			else dt = System.today().addMonths(i);
			System.debug('===>i: '+i);  
			System.debug('===>dt: '+dt);  
			if(i < numInstalments)
				isntalValue = (listPaymentPlans.installPlan.value__c/numInstalments).setScale(2);
			else isntalValue = (listPaymentPlans.installPlan.value__c - isntalValue * (i-1)).setScale(2);
			instalments.add(new Payment_Plan__c(Client__c = ct, Instalments__c = listPaymentPlans.installPlan.id, due_date__c = dt, instalment_Number__c = i, value__c = isntalValue, Value_Remaining__c = listPaymentPlans.installPlan.value__c, Total_Paid__c = 0, Currency_Code__c = listPaymentPlans.installPlan.Currency_Code__c));
		}
		insert instalments;
		update new Payment_Plan__c(id = planId, value__c = listPaymentPlans.installPlan.value__c, Value_Remaining__c = listPaymentPlans.installPlan.value__c, Number_of_Instalments__c = listPaymentPlans.installPlan.Number_of_Instalments__c, Loan_Approved_On__c = null, Loan_Status__c = 'updated');
		retrievePaymentPlans();
	}


	public decimal sumTotal{get; set;}
	public void saveInstalmentsValue(){
		string planId = ApexPages.currentPage().getParameters().get('pl');
		sumTotal = 0;
		list<Payment_Plan__c> instalToUpdate = new list<Payment_Plan__c>();
		for(planItem pi:listPaymentPlans.listInstalments.values()){
			sumTotal += pi.itemPlan.Value__c;
		}
		// sumTotal = listPaymentPlans.installPlan.Value__c - sumTotal;
		if(sumTotal == listPaymentPlans.installPlan.value__c){
			for(Payment_Plan__c pp:[Select id, Value__c, due_date__c from Payment_Plan__c where instalments__c = :planId]){
				planItem pi = listPaymentPlans.listInstalments.get(pp.id);
				
				if(pp.Value__c != pi.itemPlan.Value__c || pp.due_date__c != pi.itemPlan.due_date__c)
					instalToUpdate.add(new Payment_Plan__c(id = pp.id, due_date__c = pi.itemPlan.due_date__c, Value__c = pi.itemPlan.Value__c));
			}
			if(instalToUpdate.size() > 0)
				update instalToUpdate;

			listPaymentPlans.errorMsgPlan = null;
			retrievePaymentPlans();
		}else{
			listPaymentPlans.errorMsgPlan = 'Total value of instlaments should be <span style="font-weight:bold">'+listPaymentPlans.installPlan.value__c+'</span> and not <span style="font-weight:bold">'+ sumTotal+'</span> <br/> Please revise the instalment(s) value.';
		}
	}

	public void cancelUpdate(){
		retrievePaymentPlans();
	}

	public list<SelectOption> num_instalments{get; set;}
	public void retriveNumInstalList(){
		if(num_instalments == null){
			num_instalments = new list<SelectOption>();
			for(integer i = 1; i <= 20; i++){
				string inst_number = string.valueOf(i);
				num_instalments.add(new SelectOption(inst_number,inst_number));
			}
		}

	}
}