public with sharing class ctrNewCourseC{


	string selected = '1';

	public Course__c newCourseDetails {
		get{
		if(newCourseDetails==null){
			newCourseDetails = new Course__c();
			newCourseDetails.Minimum_length__c = 1;
			newCourseDetails.Maximum_length__c = 1;
			newCourseDetails.Course_Country__c = mainCampusCountry;
			newCourseDetails.Course_Unit_Type__c = 'Week';

		}
		return newCourseDetails;
	}
		set;
	}

	public Campus_Course__c newCampusCourse {
		get{
		if(newCampusCourse==null){
			newCampusCourse = new Campus_Course__c();
			newCampusCourse.Available_From__c = System.today();
			newCampusCourse.Campus_Country__c = mainCampusCountry;//campus.BillingCountry;
		}
		return newCampusCourse;
	}
		set;
	}

	public Account campus {
		get{
			if(campus ==null)
				campus = new Account();
			return campus;
		}
		set;
	}

	public String mainCampusCountry {get; set;}

	public ctrNewCourseC(){
		campus = [Select ID,Name,BillingCountry,ParentId,Main_Campus__c from Account where id = :ApexPages.currentPage().getParameters().get('cpID')];
		mainCampusCountry = campus.BillingCountry;

	/*	if(campus.Main_Campus__c)
			mainCampusCountry = campus.BillingCountry;
		else{
			try{
				mainCampusCountry = [select id,BillingCountry from Account where ParentId = :campus.ParentId and Main_Campus__C = true].BillingCountry;
			} catch(Exception e){
				mainCampusCountry = '';
			}
		}*/
	}


	public Course_Price__c newCoursePriceDetails {
		get{
		if(newCoursePriceDetails==null){
			newCoursePriceDetails = new Course_Price__c();
			newCoursePriceDetails.Price_valid_from__c = System.today();
			newCoursePriceDetails.Price_valid_until__c = date.newInstance(System.today().year(),12,31);
			newCoursePriceDetails.Price_per_week__c = 0;
			newCoursePriceDetails.From__c = 0;

		}
		return newCoursePriceDetails;
	}
		set;
	}



	public Campus_Course__c newCampusCourseDetails {
		get{
		if(newCampusCourseDetails==null)
			newCampusCourseDetails = new Campus_Course__c();
		return newCampusCourseDetails;
	}
		set;
	}

	private string param = ApexPages.currentPage().getParameters().get('id');

	public List<SelectOption> getPriceAvailabilityList() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('3.0','Onshore & Offshore'));
		options.add(new SelectOption('1.0','Onshore'));
		options.add(new SelectOption('2.0','Offshore'));
		return options;
	}

	// This method cancels the wizard, and returns the user to the
	// Opportunities tab
	public PageReference cancel() {
		PageReference providerPage = new PageReference('/apex/scSchoolCourses?cpID='+ApexPages.currentPage().getParameters().get('cpID')+'&currentPage='+ ApexPages.currentPage().getParameters().get('currentPage'));
		providerPage.setRedirect(true);
		return providerPage;
	}
	public String newId{get; set;}

	public List<listCampusItem> listCampus{
		get{if (listCampus == null){
			Account acco = [Select ID, ParentID from Account where id = :ApexPages.currentPage().getParameters().get('cpID')];
			listCampus = new List<listCampusItem>();
			for(Account c: [Select Name from Account where parentID = :acco.ParentID and BillingCountry = :campus.BillingCountry]){
				listCampusItem lci = new listCampusItem(c,(c.id==ApexPages.currentPage().getParameters().get('cpID')));
				listCampus.add(lci);}
			}
			return listCampus;}
		set;}


	public class listCampusItem{
		public Account Campus {get;set;}
		public boolean onCampus {get;set;}

		public listCampusItem(Account inCampus, boolean inOnCampus)
		{Campus = inCampus; onCampus = inOnCampus;}
	}

	public PageReference addCourse() {
		try{
			boolean hasSelected = false;
			for(listCampusItem lci : listCampus){
			if(lci.onCampus)
				hasSelected=true;
			}
			if(!hasSelected){
				ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'You must select at least one campus!');
				ApexPages.addMessage(Msg);
				return null;
			}

			if(newCourseDetails.Course_Category__c == 'Other' && newCourseDetails.Course_Unit_Length__c == null){
				ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Course Length is required for Category Other.');
	            ApexPages.addMessage(Msg);
	            return null;
			}
			if(newCourseDetails.Course_Category__c == 'Other' && newCourseDetails.Course_Lenght_in_Months__c == null){
				ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Course Length in Months is required for Category Other.');
	            ApexPages.addMessage(Msg);
	            return null;
			}


			if(!newCampusCourse.Is_Available__c && ( newCampusCourse.Unavailable_Reason__c == null || newCampusCourse.Unavailable_Reason__c.trim() == '' )){
				ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Enter reason why course is not available.');
				ApexPages.addMessage(Msg);
				return null;
			}

			if(selectedPeriods.isEmpty()){
	            ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Period is required.');
	            ApexPages.addMessage(Msg);
	            return null;
			}

			if(newCourseDetails.Course_Category__c == 'Language' && (newCourseDetails.Hours_Week__c == null || newCourseDetails.Hours_Week__c <= 0)){
	           newCourseDetails.Hours_Week__c.addError('Hours Per Week is Required for Category Language');
	            return null;
			}


			if(newCourseDetails.Package__c)
	            newCampusCourse.Package_Courses__c = fixPackageField(newCampusCourse.Package_Courses__c);
	        else
	            newCampusCourse.Package_Courses__c = null;

			if(newCourseDetails.Package__c && (newCampusCourse.Package_Courses__c == null || newCampusCourse.Package_Courses__c.trim() == '') ){
				ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'You must choose the courses in this package.');
	            ApexPages.addMessage(Msg);
	            return null;
			}


			if(newCampusCourse.Is_Available__c){
				newCampusCourse.Unavailable_Reason__c = null;
				newCampusCourse.Unavailable_By__c = null;
				newCampusCourse.Unavailable_Date__c = null;
			} else {
				Account userAccount = [Select Name from Account where User__c = :UserInfo.getuserid() limit 1];
				newCampusCourse.Unavailable_By__c = userAccount.Name;
				newCampusCourse.Unavailable_Date__c = System.today();
			}



			newCampusCourse.Period__c = String.join(selectedPeriods,';');

			newCourseDetails.School__c = campus.ParentId;

			INSERT newCourseDetails;
			string idCourse;
			for(listCampusItem lci : listCampus){
			if(lci.onCampus){
				Campus_Course__c cc = newCampusCourse.clone(true);
				cc.Campus__c = lci.Campus.id;
				cc.Course__c = newCourseDetails.id;
				INSERT cc;
				idCourse = cc.id;
				/*if (!newCourseDetails.Only_Sold_in_Blocks__c){
					Course_Price__c cp = newCoursePriceDetails.clone(true);
					cp.Campus_Course__c = cc.Id;
					cp.From__c = 1;
					cp.Nationality__c = 'Published Price';
					INSERT cp;
				}
				else{
					Course_Price__c cp = newCoursePriceDetails.clone(true);
					cp.Campus_Course__c = cc.Id;
					cp.Nationality__c = 'Published Price';
					INSERT cp;
				}*/

			}
			}

			PageReference providerPage = new PageReference('/apex/scSchoolCourse_View?id='+idCourse +'&cpID='+ApexPages.currentPage().getParameters().get('cpID')+'&currentPage='+ ApexPages.currentPage().getParameters().get('currentPage')+'&edit=false&pkg='+newCourseDetails.Package__c);
			providerPage.setRedirect(true);
			return providerPage;
		}catch(Exception e){
			return null;
		}
	}


	public PageReference cancelInsert() {
		PageReference providerPage = new PageReference('/apex/inc_Courses?id='+ ApexPages.currentPage().getParameters().get('cpID')+ param);
		providerPage.setRedirect(true);
		return providerPage;
	}

	public list<String> selectedPeriods{get{if(selectedPeriods == null) selectedPeriods = new list<String>(); return selectedPeriods;} Set;}
	private List<SelectOption> periods;
	public List<SelectOption> getPeriods(){
		if(Periods == null){
       		Periods = new List<SelectOption>();
        	Schema.DescribeFieldResult fieldResult = Campus_Course__c.Period__c.getDescribe();
       		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
	        for( Schema.PicklistEntry f : ple)
	            Periods.add(new SelectOption(f.getLabel(), f.getValue()));
		}
        return Periods;
    }



	 public class sDriveFiles{
        public String URL {get;set;}
        public Account_Document_File__c File {get;set;}
    }

    private Map<String, sDriveFiles> mapFiles = new Map<String, sDriveFiles>();

    public List<SelectOption> Files {
        get{
            if(Files== null){

				String campusID = ApexPages.currentPage().getParameters().get('id');
				if(campusID != null){
	                Files = new List<SelectOption>();
	                List<ID> parentIDs = new List<ID>();
	                List<ID> fileObjects = new List<ID>();
	                files.add(new SelectOption('','-- Select a File --'));

	                Map<String, List<Account_Document_File__c>> campusFiles = new Map<String, List<Account_Document_File__c>>();
	                Map<String, String> folderNames = new Map<String, String>();
	                folderNames.put('','Not Classified');
	                campusFiles.put( '' , new List<Account_Document_File__c>() );
	                for(Account_Document_File__c folder : [SELECT id, File_Name__c FROM Account_Document_File__c WHERE Parent__c = :campusID and Content_Type__c = 'Folder' and WIP__c = false order by Content_Type__c]){
	                    campusFiles.put( folder.id, new List<Account_Document_File__c>() );
	                    folderNames.put( Folder.id, Folder.File_Name__c );
	                }


	                for(Account_Document_File__c folder : [SELECT id, Parent_Folder_Id__c, Description__c, CreatedDate, File_Name__c,File_Size__c FROM Account_Document_File__c WHERE Parent__c = :campusID and Content_Type__c != 'Folder' and WIP__c = false order by Content_Type__c]){
	                    List<Account_Document_File__c> ladf = campusFiles.get(Folder.Parent_Folder_Id__c == null ? '' : Folder.Parent_Folder_Id__c);
	                    ladf.add(folder);
	                    campusFiles.put(Folder.Parent_Folder_Id__c == null ? '' : Folder.Parent_Folder_Id__c, ladf);
	                }

	                for(String folderid : campusFiles.keyset()){
	                    if(!campusFiles.get(folderid).isEmpty()){

	                        files.add(new SelectOption('Folder',folderNames.get(folderid)));

	                        for(Account_Document_File__c file : campusFiles.get(folderid)){
	                            parentIDs.add(campusID);
	                            fileObjects.add(file.id);
	                            sDriveFiles sdf = new sDriveFiles();
	                            sdf.File = file;
	                            mapFiles.put(file.id,sdf);
	                            files.add(new SelectOption(file.id, file.Description__c + ' [*' + file.File_Name__c + ' *]')  );

	                        }

	                        files.add(new SelectOption('disabled', ''));
	                    }
	                }

	                try {
	                    if(files.size() > 1)
	                        files.remove(files.size()-1);
	                } catch (Exception e){}


	                if(parentIDs.size() > 0 && fileObjects.size() > 0){
	                    try {
	                        List<String> urls = cg.SDriveTools.getAttachmentURLs( parentIDs , fileObjects, (100 * 60));

	                        for(string sd : mapFiles.keySet())
	                            for(String url : urls )
	                                if(url.contains(mapFiles.get(sd).file.id)){
	                                    mapFiles.get(sd).url = url;
	                                    break;
	                                }
	                    } catch (Exception e){}
	                }
				}
            }
            return Files;
        }
        set;
    }


	private List<SelectOption> courses;
    public List<SelectOption> getCourses(){
        if(courses == null){
            courses = new List<SelectOption>();
            for(Campus_Course__c cc : [select id, course__r.Name, course__r.Hours_Week__c, course__r.Optional_Hours_Per_Week__c, course__r.Course_Unit_Type__c, Is_Available__c
                                    from Campus_Course__c where campus__c = :campus.id and course__r.Package__c = false order by course__r.Name]){
				String courseName = cc.course__r.Name + getCourseHours(cc.course__r);
				if(!cc.Is_Available__c)
					courseName += ' (unavailable)';
                courses.add(new SelectOption( cc.id, courseName ));
            }
        }
        return courses;
    }



    private String getCourseHours(Course__c course){
        String courseHours = '';
        if(course.Hours_Week__c != null && course.Hours_Week__c != 0){
            courseHours = ' (' + Integer.valueOf(course.Hours_Week__c) + 'hrs';

            if(course.Optional_Hours_Per_Week__c != null && course.Optional_Hours_Per_Week__c != 0){
                courseHours += ' + ' + Integer.valueOf(course.Optional_Hours_Per_Week__c) + ' opt./' + course.Course_Unit_Type__c + ')';
            } else {
                courseHours += '/'+ course.Course_Unit_Type__c + ')';
            }

        }
        return courseHours;
    }

    private String fixPackageField(String toFix){
        if(toFix != null)
            return toFix.replaceAll('[^a-zA-Z0-9\\,]+', '');
        else return toFix;
    }

}