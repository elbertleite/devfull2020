public class ExtrafeeTranslation {
		
	public String selectedLanguage {get;set;}
	private List<SelectOption> languages;
	public List<SelectOption> getLanguages(){

		if(languages == null){
			languages = new List<SelectOption>();
			languages.add(new SelectOption('', '-- Select Language --'));
			Schema.DescribeFieldResult fieldResult = Contact.Preferable_Language__c.getDescribe();
   			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
   			for(Schema.PicklistEntry f : ple)
   				if(f.getLabel() != 'en_US')
      				languages.add(new SelectOption(f.getLabel(), IPFunctions.getLanguage(f.getLabel())));  			
		}
		
		return languages;
	}
	
	public void changeLanguage(){ 
		
		fees = null;
		getFees();
		
	}
	
	private Translation__c labelTranslation;
	private List<Fee> fees;
	public List<Fee> getFees(){

		if(fees == null){
			fees = new List<Fee>();
			
			
			Map<String, String> feeTranslations = new Map<String, String>();
			try {
				labelTranslation = [Select Extra_Fees__c from Translation__c where Language__c = :selectedLanguage and Type__c = 'label' Limit 1];
			} catch (Exception e) {
				labelTranslation = new Translation__c(Language__c = selectedLanguage, Type__c = 'label');
			}
			
			if(labelTranslation.Extra_Fees__c != null)
				for(String extraFee : labelTranslation.Extra_Fees__c.split('!#!')){
					String[] fee = extraFee.split('>');
					feeTranslations.put( fee[0], fee[1] );
				}
			
			
			
			
			Schema.DescribeFieldResult fieldResult = Product__c.Website_Fee_Type__c.getDescribe();
   			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
   			for(Schema.PicklistEntry f : ple){
	   				if(feeTranslations.containsKey(f.getLabel()))
	      				fees.add( new Fee( f.getLabel(), feeTranslations.get( f.getLabel() ) ) ) ; 
	      			else 
	      				fees.add( new Fee( f.getLabel(), ''));
   			}
		}
		
		return fees;
	}
	
	public void save(){
		List<String> listFees = new List<String>();
		for(Fee f : fees)			
			listFees.add(f.feeType+'>'+f.translation);			
		
		labelTranslation.Extra_Fees__c = String.join(listFees, '!#!');
		upsert labelTranslation;
		
		
		ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.CONFIRM, 'Extra fee translations saved.'));
		
	}

	public class Fee {
		public String feeType {get;set;}
		public String translation {get;set;}
		
		public Fee(String feeType, String translation){
			this.feeType = feeType;
			this.translation = translation;
		}
	}
	
    
}