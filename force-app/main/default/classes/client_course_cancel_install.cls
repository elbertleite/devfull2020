public with sharing class client_course_cancel_install {

	private string ccId;
	public client_course__c course{get; set;}
	public client_course_instalment__c reason{get{if(reason == null) reason = new client_course_instalment__c(); return reason;} set;}
	public boolean hasGroupedInvoice {get{if(hasGroupedInvoice==null) hasGroupedInvoice = false; return hasGroupedInvoice;}set;}
	
	
	public client_course_cancel_install(ApexPages.StandardController ctr){
		ccId = ctr.getId();
		loadCourse();
	}
	
	public void loadCourse(){
		course = [Select Campus_Name__c, Client__r.Name, Course_Length__c, Course_Name__c, End_Date__c, Free_Units__c, Start_Date__c, Total_Course__c, Total_Tax__c, Total_Discount__c, Total_School_Payment__c, 
						Total_Scholarship_Taken__c, Total_Keep_Fee__c, Total_Extra_Fees__c, Total_Extra_Fees_Promotion__c, Total_Tuition__c, Unit_Type__c, hasCancelledInstall__c, Total_Commissions__c, Total_Balance__c,
						School_Name__c, CurrencyIsoCode__c
					, ( Select Commission_Value__c, Commission_Tax_Value__c, Discount__c, Total_School_Payment__c, Scholarship_Taken__c, Due_Date__c, Extra_Fee_Discount_Value__c, Kepp_Fee__c, Extra_Fee_Value__c, Number__c, 
						Tuition_Value__c, Instalment_Value__c, Cancelled_By__c, Cancelled_On__c, Cancelled_Reason__c, isCancelled__c, Related_Fees__c, Related_Promotions__c, Received_Date__c, Balance__c, Invoice__c, isPDS__c,Split_Number__c
							from client_course_instalments__r ORDER BY Number__c)//where Received_Date__c = null and isCancelled__c = false ORDER BY Number__c) 
					from client_course__c WHERE Id = :ccID];
					
		for(client_course_instalment__c cci : course.client_course_instalments__r)
			if(cci.Invoice__c != null){
				hasGroupedInvoice = true;
				break;
			}
	}
	
	public pageReference cancelInstalments(){
		
		Savepoint sp = Database.setSavepoint();
		try{
			map<string, decimal> feesIdUpdate = new map<string, decimal>();
			for(client_course_instalment__c cci:course.client_course_instalments__r){
				if(cci.isCancelled__c && cci.Received_Date__c == null && cci.Cancelled_On__c == null){
					cci.Cancelled_On__c = System.today();
					cci.isCancelled__c = true;
					cci.Cancelled_By__c = Userinfo.getUserId();
					cci.Cancelled_Reason__c = reason.Cancelled_Reason__c;
					course.hasCancelledInstall__c = true;
					
					course.Total_Tuition__c -= cci.Tuition_Value__c;
					course.Total_Course__c -= cci.Instalment_Value__c;
					course.Total_Extra_Fees__c -= cci.Extra_Fee_Value__c;
					
					course.Total_Commissions__c -= cci.Commission_Value__c;
					course.Total_Balance__c -= cci.Balance__c;
					
					course.Total_Tax__c -= cci.Commission_Tax_Value__c;
					course.Total_Discount__c -= cci.Discount__c;
					course.Total_School_Payment__c -= cci.Total_School_Payment__c;
					course.Total_Scholarship_Taken__c -= cci.Scholarship_Taken__c;
					course.Total_Keep_Fee__c -= cci.Kepp_Fee__c;
					
					if(cci.Related_Fees__c != null){
						for(string st:cci.Related_Fees__c.split('!#')){
							if(!feesIdUpdate.containsKey(st.split(';;')[0]))
								feesIdUpdate.put(st.split(';;')[0], decimal.valueOf(st.split(';;')[2]));
							else {
								decimal subTot = feesIdUpdate.get(st.split(';;')[0]) + decimal.valueOf(st.split(';;')[2]);
								feesIdUpdate.put(st.split(';;')[0], subTot);
							}
						}
					}
				}
			}
			update course.client_course_instalments__r;
			update course;
			
			list<client_course_fees__c> ccfUpdate = new list<client_course_fees__c>([Select id, Total_Value__c, Value__c from client_course_fees__c where id in :feesIdUpdate.keySet()]);
			for(client_course_fees__c cc:ccfUpdate){
				cc.Total_Value__c -= feesIdUpdate.get(cc.id);
				cc.Value__c -= feesIdUpdate.get(cc.id);
			}
			if(ccfUpdate.size() > 0)
				update ccfUpdate;
			
			reason = new client_course_instalment__c();
			loadCourse();
		}catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			Database.rollback(sp);
		}
		return null;
	}
}