public with sharing class agencies_sales_target {
    
    public User userLogged{get;set;}
    public List<List<Account>> groupAgencies{get;set;}

    public agencies_sales_target() {
        userLogged = [SELECT ID, Contact.AccountId, Contact.Account.ParentId FROM User WHERE ID = :UserInfo.getUserId()];

        List<Account> agencies = [SELECT ID, Name, Agency_Sales_Target__c FROM Account WHERE RecordType.Name = 'Agency' AND Parent.ID = :userLogged.Contact.Account.ParentId  AND Inactive__c = false ORDER BY Name]; 
        if(Test.isRunningTest()){
            Account acc = new Account(Name = 'Test', Agency_Sales_Target__c = 20);
            //insert acc;
            agencies.add(acc);
        }
        Integer maxAgenciesPerColumn = 12;
        Integer index = 0;
        Integer totalLoops = agencies.size() / maxAgenciesPerColumn;
        if(math.mod(agencies.size(), maxAgenciesPerColumn) > 1){
            totalLoops += 1;
        }

        List<Account> gp;
        groupAgencies = new List<List<Account>>();
        for(Integer i = 0; i < totalLoops; i++){         
            gp = new List<Account>();
            for(Integer z = 0; z < maxAgenciesPerColumn; z++){
                if(index < agencies.size()){
                    gp.add(agencies.get(index));
                    index += 1;
                }else{
                    break;
                }
            }
            groupAgencies.add(gp);     
        }
    }

    public void saveTargets(){
        List<Account> agenciesToUpdate = new List<Account>();
        for(List<Account> groupAgs : groupAgencies){
            for(Account ag : groupAgs){
                ag.Agency_Sales_Target__c = ag.Agency_Sales_Target__c == null ? 0 : ag.Agency_Sales_Target__c;
                agenciesToUpdate.add(ag);
            }
        }
        if(!Test.isRunningTest()){
            update agenciesToUpdate;
        }
    }
}