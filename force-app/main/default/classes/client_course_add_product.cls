public with sharing class client_course_add_product {


	public client_course__c clientCourse {get;set;}
	private string contactId;
	public client_course_add_product(ApexPages.standardController controller){
		contactId =ApexPages.currentPage().getParameters().get('cid');
		getUserDetails();
		try{
			clientCourse = [Select id, name, client__r.name	from client_course__c  where id = :controller.getId()];
		}catch(Exception e){
			
		}
	}
	
	
	/*private map<string, string> relatedPromotions = new map<string, string>();
	private map<string, client_course_fees__c> mapFees = new map<string, client_course_fees__c>();
	public void loadFees(){
		
		for(client_course_fees__c rp:clientCourse.client_courses_fees__r){
			if(rp.isPromotion__c && rp.relatedPromotionFee__c != null)
				relatedPromotions.put(rp.relatedPromotionFee__c, rp.id);
			if(!rp.isPromotion__c)
				mapFees.put(rp.Fee_Name__c, rp);
		}
	}*/
	
	
	public String selectedCategory {get;Set;}
	public List<SelectOption> productCategories {
		get{
			if(productCategories == null){
				productCategories = new List<SelectOption>();
				productCategories.add(new SelectOption('','-- All --'));
				
				for(String cat : products.keySet())
					productCategories.add(new SelectOption(cat,cat));
			}
			productCategories.sort();
			return productCategories;
		}
		Set;
	}
	
	
	public Map<String, List<Quotation_Products_Services__c>> products {
		get{
			if(products == null){
				products = new Map<String, List<Quotation_Products_Services__c>>();
				
				String sql = 'select Selected__c,  Agency__c, Product_Name__c,Quantity__c,Price__c,Description__c, Use_list_of_products__c,Unit_description__c ,Category__c, Currency__c, Group_Other__c, Commission__c, Commission_Type__c, ' +
            				 ' ( Select Quantity__c, Quotation_Products_Services__c, Value__c from Quotation_list_products__r order by Quantity__c) ' +
            				 ' from Quotation_Products_Services__c where (Agency__c = \'' + userDetails.Agency_Id + '\' or Agency__c = \'' + userDetails.AgencyGroup_Id + '\') ';
				
				if(selectedCategory != null && selectedCategory.trim() != '')
					sql += ' and Category__c = \'' + selectedCategory + '\' ';
					
				sql += ' order by Category__c, Product_Name__c ';
				
				for(Quotation_Products_Services__c qps : Database.query(sql)){
		
					if(qps.Use_list_of_products__c && qps.Quotation_list_products__r != null && qps.Quotation_list_products__r.size() > 0){
                		qps.Quantity__c = qps.Quotation_list_products__r[0].Quantity__c.round(System.RoundingMode.UNNECESSARY);
						qps.Price__c = qps.Quotation_list_products__r[0].Value__c;
						List<SelectOption> options = new List<SelectOption>();
                
	                    for(Quotation_list_products__c qlp : qps.Quotation_list_products__r)
	                        options.add(new SelectOption(''+qlp.Quantity__c+'.0',''+qlp.Quantity__c.round(System.RoundingMode.UNNECESSARY)));
	                    
	                    prodQtdList.put(qps.id, options);
					}
					
					if(products.containsKey(qps.Category__c))
						products.get(qps.Category__c).add(qps);						
					else
						products.put(qps.Category__c, new List<Quotation_Products_Services__c>{qps});
					
				}

			}
			
			return products;
		}
		Set;
	}
	
	public Map<String, List<SelectOption>> prodQtdList {
        get{
	        if(prodQtdList == null)
	            prodQtdList = new Map<String, List<SelectOption>>();
	        return prodQtdList;
	    }
        set;
    }
	
	public class CustomProductWrapper {
	
		public Integer Id {Get;Set;}
		public client_product_service__c product {get;Set;}
		
	}
	
	public List<CustomProductWrapper> customProductList {
		get{
			if(customProductList == null)
				customProductList = new List<CustomProductWrapper>();
			return customProductList;
		}
		Set;
	}

	public CustomProductWrapper customProduct {
		get{
			if(customProduct == null){
				customProduct = new CustomProductWrapper();
				customProduct.product = new client_product_service__c();
			}
			return customProduct;
		}
		Set;
	}
	
	public void getValue(){
		
		String productID = ApexPages.currentPage().getParameters().get('productID');		
		String category = ApexPages.currentPage().getParameters().get('category');
		
        for(Quotation_Products_Services__c qps : products.get(category))
            if(qps.id == productID)
                for(Quotation_list_products__c qlp : qps.Quotation_list_products__r)
                    if(qps.Quantity__c == qlp.Quantity__c)
                        qps.Price__c = qlp.Value__c;
    }
    
    public void filterProducts(){
		
		products = null;
		
	}
	
	private xCourseSearchPersonDetails.UserDetails UserDetails;
	public xCourseSearchPersonDetails.UserDetails getUserDetails(){
		if(UserDetails == null){
			xCourseSearchPersonDetails spd = new xCourseSearchPersonDetails();
			UserDetails = spd.getAgencyDetails();
		}
		return UserDetails;
	}
	
	public List<SelectOption> getUnitsRange(){    	
        return xCourseSearchFunctions.getUnitsRange();
    }
	
	public List<SelectOption> getCurrencies() {
		return xCourseSearchFunctions.getAgencyCurrencies(userDetails.Agency_Id, null);
	}
	
	private User currentUser {get{if(currentUser==null) currentUser = [Select Id, Name, Contact.Account.ParentId, Contact.AccountId, Contact.Department__c from User where id = :UserInfo.getUserId() limit 1]; return currentUser;}set;}
	
	
	public Boolean productsAdded {get;Set;}
	public void addProducts(){
		
		productsAdded = false;
		
		Client_Course__c booking;
		if(clientCourse == null){
			//Create Booking
	 		booking = new Client_Course__c(client__c = contactId, isBooking__c = true);
	 		insert booking;
		}
		
		List<client_product_service__c> listProducts = new List<client_product_service__c>();
		client_product_service__c scp = new client_product_service__c();
		
		Boolean allValidated = true;


		// PRODUCTS
		for(String key : products.keySet()){
			for(Quotation_Products_Services__c qps : products.get(key)){
				if(qps.Selected__c){
					if(!validate(qps)){
						allValidated = false;
						continue;
					}
						
						
					scp = new client_product_service__c();
					if(clientCourse != null){
						scp.Booking_Number__c = clientCourse.id;
						scp.client__c = clientCourse.Client__c;
					}else{
						scp.Booking_Number__c = booking.id;
						scp.client__c = booking.Client__c;
					}
					
					scp.Product_From_Agency__c = qps.Agency__c;
					scp.Sold_By_Agency__c = currentUser.Contact.AccountId;
					scp.Products_Services__c = qps.id;
					
					scp.Category__c = qps.Category__c;
					scp.Currency__c = qps.Currency__c;
					scp.Description__c = qps.Description__c;
					scp.Product_Name__c = qps.Product_Name__c;
					scp.Quantity__c = qps.Quantity__c;
					scp.Price_Unit__c = qps.Price__c;
					scp.Unit_Description__c = qps.Unit_description__c;
					
					if(qps.Use_list_of_products__c)
						scp.Price_Total__c = scp.Price_Unit__c;
					else
						scp.Price_Total__c = scp.Price_Unit__c * scp.Quantity__c;
						
					if(qps.Commission__c != null && qps.Commission__c > 0){
						if(qps.Commission_Type__c == 0){
							scp.Commission_Value__c = scp.Price_Total__c * (qps.Commission__c/100);
						}else{
							scp.Commission_Value__c = qps.Commission__c;
						}
						scp.Commission__c = qps.Commission__c;
						scp.Commission_Type__c = qps.Commission_Type__c;
							
					}
					
					
					//Double totalProducts = searchCourses.get(searchcourseid).Total_Products__c != null ? searchCourses.get(searchcourseid).Total_Products__c : 0;
					//double totalProductsOther = searchCourses.get(searchcourseid).Total_Products_Other__c != null ? searchCourses.get(searchcourseid).Total_Products_Other__c : 0;
					Decimal totalProductConverted = 0;
					
					
					
					/*if(scp.Currency__c == campusCurrency.get(searchCourseid))
						totalProductConverted = scp.Total__c;
					else if(scp.Currency__c == userDetails.Agency_account_currency_iso_code)
						totalProductConverted = scp.Total__c / currencyRates.get( campusCurrency.get(searchCourseid) );
					else
						totalProductConverted = (scp.Total__c * currencyRates.get(scp.Currency__c)) / currencyRates.get(campusCurrency.get(searchCourseid));*/
					
					
					//searchCourses.get(searchcourseid).Total_Products__c = totalProducts + totalProductConverted.setScale(2);							
					
					if(qps.Group_Other__c){							
						//totalProductsOther += totalProductConverted.setScale(2);
						//searchCourses.get(searchcourseid).Total_Products_Other__c = totalProductsOther;
					}
					
					
					
					System.debug('@@@@ totalProductConverted: ' + totalProductConverted.setScale(2));
					//System.debug('@@@@ searchCourses.get(searchcourseid).Total_Products__c: ' + searchCourses.get(searchcourseid).Total_Products__c );
					listProducts.add(scp);
					
				}
			}
		}
			
			
		//CUSTOM PRODUCTS
		for(CustomProductWrapper custom : customProductList){
			if(!validateProduct(custom.product)){
				allValidated = false;
				continue;
			}
			
			scp = new client_product_service__c();
			
			if(clientCourse != null){
				scp.Booking_Number__c = clientCourse.id;
				scp.client__c = clientCourse.Client__c;
			}else{
				scp.Booking_Number__c = booking.id;
				scp.client__c = booking.Client__c;
			}
			
			scp.Product_From_Agency__c = currentUser.Contact.AccountId;
			scp.Sold_By_Agency__c = currentUser.Contact.AccountId;
			
			//scp.client_course__c = clientCourse.id;
			//scp.client__c = clientCourse.Client__c;
			scp.isCustom__c = true;
			scp.Currency__c = custom.product.Currency__c;
			scp.Description__c = custom.product.Description__c;
			scp.Product_Name__c = custom.product.Product_Name__c;
			scp.Quantity__c = custom.product.Quantity__c;
			scp.Price_Unit__c = custom.product.Price_Unit__c;
			scp.Price_Total__c = custom.product.Price_Unit__c * custom.product.Quantity__c;
			
			
		//	Double totalProducts = searchCourses.get(searchcourseid).Total_Products__c != null ? searchCourses.get(searchcourseid).Total_Products__c : 0;
					
			//Decimal totalProductConverted = 0;
			
			/*if(scp.Currency__c == campusCurrency.get(searchCourseid))
				totalProductConverted = scp.Total__c;
			else if(scp.Currency__c == userDetails.Agency_account_currency_iso_code)
				totalProductConverted = scp.Total__c / currencyRates.get( campusCurrency.get(searchCourseid) );
			else
				totalProductConverted = (scp.Total__c * currencyRates.get(scp.Currency__c)) / currencyRates.get(campusCurrency.get(searchCourseid));
			
			searchCourses.get(searchcourseid).Total_Products__c = totalProducts + totalProductConverted.setScale(2);*/
			
			
			listProducts.add(scp);
				
				
		}
			
		
		if(allValidated){
			insert listProducts;
			productsAdded = true;
		} else
			productsAdded = false;
			
		
		
	}
	
	private Integer count = 0;
	
	public void addCustomProduct(){
		
		if(validateProduct(customProduct.product)){
			customProduct.product.Price_Total__c = customProduct.product.Price_Unit__c * customProduct.product.Quantity__c;
			customProduct.id = count;
			customProductList.add(customProduct);		
			customProduct = null;
			count++;
		}
		
	}
	
	
	private boolean validateProduct(client_product_service__c prod){
		Boolean validated = true;
		if(prod.Product_Name__c == null || prod.Product_Name__c.trim() == ''){
			prod.Product_Name__c.addError('Required Field');
			validated = false;
		}
		if(prod.Quantity__c == null || prod.Quantity__c < 0){
			prod.Quantity__c.addError('Required Field');
			validated = false;
		}
		if(prod.Price_Unit__c == null){
			prod.Price_Unit__c.addError('Required Field');
			validated = false;
		}
		if(prod.Currency__c == null || prod.Currency__c == ''){
			prod.Currency__c.addError('Required Field');
			validated = false;
		}
		return validated;
	}
	
	public void deleteCustomProduct(){
		Integer cpid = Integer.valueOf(ApexPages.currentPage().getParameters().get('cpid'));
		
		for(integer i = 0; i < customProductList.size(); i++){
			CustomProductWrapper cpw = customProductList.get(i);
			if(cpw.id == cpid)
				customProductList.remove(i);
		}
	}
	
	private Boolean validate(Quotation_Products_Services__c prod){
		Boolean validated = true;
		if(prod.Quantity__c == null || prod.Quantity__c == 0){
			prod.Quantity__c.addError('Required Field');
			validated = false;
		}
		if(prod.Price__c == null || prod.Price__c == 0){
			prod.Price__c.addError('Required Field');
			validated = false;
		}
		if(prod.Currency__c == null || prod.Currency__c == ''){
			prod.Currency__c.addError('Required Field');
			validated = false;
		}
		return validated;
	}
}