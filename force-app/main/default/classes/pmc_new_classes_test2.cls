@isTest
private class pmc_new_classes_test2 {
    static testMethod void myUnitTest() {
		TestFactory tf = new TestFactory();
        
        Account agency = tf.createAgency();
        Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
        
        Map<String,String> recordTypes = new Map<String,String>();
        for( RecordType r :[select id, name from recordtype where isActive = true] ){
            recordTypes.put(r.Name,r.Id);
        }

        Contact client = tf.createClient(agency);

        Account schoolGroup = tf.createSchoolGroup();
        
        Account school = tf.createSchool();
        school.parentId = schoolGroup.id;
        update school;

        Account campus = tf.createCampus(school, agency);

        Course__c course = tf.createCourse();
        Campus_Course__c cc = tf.createCampusCourse(campus, course);

        Course_Price__c price = tf.createCoursePrice(cc, 'Published Price');

        Start_Date_Range__c sd = tf.createProdDateRange(agency, new Quotation_List_Products__c());

        Quotation__c quote = tf.createQuotation(client, cc);

        testSchoolPictures(school);
        testSchoolTuitionFees(campus, school, client, course, price, sd);
        testPMCHelper(school);
        testSchoolDocuments(campus, school, client);
	}
    public static void testSchoolPictures(Account school){
        Apexpages.currentPage().getParameters().put('id', school.id);
        school_campus_pictures controller = new school_campus_pictures();
        controller.maxUploadSize = '';
        controller.maxUploadSizeInKB = '';
        controller.eraseSchoolImages = true;
        controller.loadSchoolImages();
        controller.saveNewOrderImages();
	}
    public static void testPMCHelper(Account school){
        PMCHelper.retrieveSchoolDocumentsTreeFormat(new List<String>{school.ID}, true);
        PMCHelper.isFileExpired('01/01/2017');
    }
    public static void testSchoolTuitionFees(Account campus, Account school, Contact ctt, Course__c course, Course_Price__c price, Start_Date_Range__c sd){
        school_price_report controller = new school_price_report();
        school_price_report.changeCourses(school.Id, null,  null, true);
        school_price_report.changeCourseTypes(school.Id, null, true);
        school_price_report.changeCourseCategories(school.Id, true);
        school_price_report.changeCampus(school.Id);
        school_price_report.changeNationlityGroup(school.Id);
        school_price_report.changeSchool('Australia');
        school_price_report.changeCountry();
        school_price_report.retrievePrices(school.Id, null, null, new List<String>{'Published Price'}, '01/01/2000', '01/01/2030', null, '0', 'nation', null, true);
        school_price_report.retrievePrices(school.Id, null, null, new List<String>{'Published Price'}, '01/01/2000', '01/01/2030', null, '0', 'course', null, true);
        school_price_report.removeDuplicatedCourses(school.ID);
        school_price_report.disableCourses(school.ID, new List<String>{course.ID});
        school_price_report.updatePrices(school.ID, '[{"From__c":12,"isSelected__c":false,"Campus_Course__c":"a0A9000000TVchHEAT","Availability__c":3,"LastModifiedDate":1548967953000,"LastModifiedById":"005900000054AjaAAE","Price_valid_from__c":1546300800000,"Price_valid_until__c":1577750400000,"Nationality__c":"Published Price","To__c":0,"Unavailable__c":false,"Price_per_week__c":6600,"Id":"a0F2v00001JS57dEAD","Campus_Course__r":{"Course__c":"a0G9000000dIkO9EAK","Campus__c":"00190000019Td6hAAC","Id":"a0A9000000TVchHEAT","Course__r":{"Name":"Adv Diploma of Accounting","Id":"a0G9000000dIkO9EAK"}},"LastModifiedBy":{"Name":"Hana Strakova","Id":"005900000054AjaAAE"},"$$hashKey":"object:271","Price_valid_until_expired__c":"2019-12-31"}]', '[{"Availability__c":1,"Price_valid_from__c":"2019-05-01","Price_valid_until__c":"2019-05-31","Price_valid_until_expired__c":"2019-05-31","From__c":"12","Price_per_week__c":"7000","unit_description__c":"","Comments__c":"","Campus_Course__c":"a0A9000000TVchHEAT","Nationality__c":"Published Price","Campus__c":"00190000019Td6hAAC","CoursePriceID__c":"a0G9000000dIkO9EAK","Account_Document_File__c":"","Account_Document_File__r":null,"isSelected__c":true,"$$hashKey":"object:3226"}]','[]','[]');
        //school_price_report.generatePricePerWeek(Decimal.valueOf(10), Double.valueOf(30), 'test', 'percentage');
        school_price_report.clonePrices(school.ID, 'nationality', new List<String>{price.ID}, new List<String>{sd.ID}, new List<String>{'Published Price'}, '01/01/2017', '01/01/2019', '01/01/2019', '200', 'test', 'percentege', 'abc', null);
        school_price_report.clonePrices(school.ID, 'campus', new List<String>{price.ID}, new List<String>{sd.ID}, new List<String>{campus.id}, '01/01/2017', '01/01/2019', '01/01/2019', '200', 'test', 'percentege', 'abc', null);
        school_price_report.deletePrices(new List<String>{price.ID}, new List<String>{sd.ID});
	}
    public static void testSchoolDocuments(Account campus, Account school, Contact ctt){
        school_page_documents controller = new school_page_documents();
        controller.idAccount = school.ID;
        controller.init();
        controller.saveNewLink();
        controller.loadDocumentsForReading();
        controller.openCloseModalUploadFile();
        controller.closeModalAfterUpload();
        controller.closeModalMovingFiles();
        controller.openModalMovingFiles();
        controller.openCloseModalNewLink();
        controller.openCloseEdit();
        controller.openCloseModalNewFolder();
        Apexpages.currentPage().getParameters().put('idFolder', controller.currentOpenFolder.id);
        controller.openFolder();
        controller.saveNewFolder();
        controller.saveEdition();
        controller.moveIntoFolder();
        controller.deleteSelectedFiles();
        controller.retrieveFilesFromS3('folder', true);
	}
}