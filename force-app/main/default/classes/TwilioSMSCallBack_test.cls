/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TwilioSMSCallBack_test {

    static testMethod void myUnitTest() {
       
       TestFactory tf = new TestFactory();
       
       Account school = tf.createSchool();
       Account agency = tf.createAgency();
       Contact emp = tf.createEmployee(agency);
       User portalUser = tf.createPortalUser(emp);
       Account campus = tf.createCampus(school, agency);
       Course__c course = tf.createCourse();
       Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
       
       Test.startTest();
       system.runAs(portalUser){
       	
       	   Contact client = tf.createLead(agency, emp);
       	   client_course__c booking = tf.createBooking(client);
           client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
           client_course__c courseCredit =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
           courseCredit.isCourseCredit__c = true;
           courseCredit.Credit_Available__c = 1000;
           courseCredit.Credit_Valid_For_School__c = school.id;
           update courseCredit;
       
           client_course__c deposit =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
           deposit.isDeposit__c = true;
           deposit.Deposit_Total_Available__c = 1000;
           deposit.Deposit_Total_Used__c = 0;
           update deposit;
           
           client_product_service__c bkproduct =  tf.createCourseProduct(booking, agency);
       	   List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
       	
	       ApexPages.currentPage().getParameters().put('cs', instalments[0].id);
	       ApexPages.currentPage().getParameters().put('pd', bkproduct.id);
	       ApexPages.currentPage().getParameters().put('ct', client.id);
	       
	       client_course_create_invoice createInvoiceClass = new client_course_create_invoice();
	       
	       createInvoiceClass.createInvoice();
	       String invoiceId = createInvoiceClass.invoice.id;
	        
	       RestRequest req = new RestRequest();
		   RestResponse res = new RestResponse();
		   
		   
		   
			
		   req.requestURI = '/sms/';  //Request URL
		   req.httpMethod = 'POST';//HTTP Request Type
		   req.resourcePath='/services/apexrest/sms/';
		   req.params.put('From','+61439559941'); 
		   req.params.put('MessageSid','test1'); 
		   req.params.put('MessageStatus','sent'); 
		   req.params.put('SmsSid','SM5b114511cf734b71a6903efd8b6425f6'); 
		   req.params.put('SmsStatus','sent'); 
		   req.params.put('To','+61404005269'); 
		   req.params.put('remoteAddress','52.91.166.142'); 
		   req.params.put('requestBody', 'Test'); 
		   req.params.put('requestURI','/sms'); 
		   req.params.put('resourcePath','/services/apexrest/sms/'); 
		   String JsonMsg=JSON.serialize(req);
		   RestContext.request = req;
		   RestContext.response= res;
			
		   Email_SMS__c sms  = new Email_SMS__c (Body__c ='Test', Subject__c ='Test', Client__c = client.id, client_course_instalment__c = instalments[1].id, From__c = UserInfo.getUserId(), msgId__c = 'test1'); 
	       Email_SMS__c sms2  = new Email_SMS__c (Body__c ='Test', Subject__c ='Test', Client__c = client.id, Invoice__c = invoiceId, From__c = UserInfo.getUserId(), msgId__c = 'test2'); 
	       
	       insert sms; insert sms2;
	       
	       TwilioSMSCallBack.dopost();
	       
	       req.params.put('MessageSid','test2'); 
	       TwilioSMSCallBack.dopost();
	       
	       
       }
       Test.stopTest();
    }
    
}