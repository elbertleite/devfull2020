global class batch_clientDocument_PreviewLink implements Database.Batchable<sObject>, Database.AllowsCallouts {
	// Select Id, preview_link__c, isPreviewLink_generated__c From client_document__c where client__c != null and isPreviewLink_generated__c = false
	// DEACTIVATE VISA WORKFLOW

	S3Controller S3 {get;set;}
	String query;
	
	global batch_clientDocument_PreviewLink(String qr) {
		query = qr;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {

		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<Client_Document__c> scope) {
		AWSKeys credentials = new AWSKeys(IPFunctions.awsCredentialName);
		S3 = new S3Controller();
		S3.constructor();
		String prefix = '';
		String docId;
		for(Client_Document__c doc : scope){

			if(doc.preview_link__c == null){

				docId = string.valueOf(doc.id).LEFT(15);	
				prefix = 'cloudpondconnector/Client_Document__c/'+ docId + ' - ' + docId;
				doc.preview_link__c = '';
				s3.listBucket('ehfprod', prefix , null, null, null);
				if(s3.bucketList != null && !s3.bucketList.isEmpty()){  

					List<String> allDocs = new list<String>();
					String filename;
					String url;
					String[] decon;
					String shortfilename;

					for(S3.ListEntry file : s3.bucketList){
						system.debug('file===>' + file);

						filename = file.key.remove(prefix+'/');
						url = AWSHelper.generateAWSLink('ehfprod', file.key, credentials.key, credentials.secret);
						decon = FileUPload.breakFilename(filename);
						shortfilename = decon[1];
						allDocs.add(url + FileUPload.SEPARATOR_URL + EncodingUtil.urlDecode(shortfilename, 'UTF-8'));
					}//end for

					doc.preview_link__c = string.join(allDocs, FileUpload.SEPARATOR_FILE);
				}
				doc.isPreviewLink_generated__c = true;
			}
			else{
				doc.isPreviewLink_generated__c = true;
			}
		}//end for

		update scope;

	}
	
	global void finish(Database.BatchableContext BC) {
		system.debug('Url update finished...');
	}
}