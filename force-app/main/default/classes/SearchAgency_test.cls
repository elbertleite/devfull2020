/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SearchAgency_test {

    static testMethod void myUnitTest() {
    	TestFactory factory = new TestFactory();
    	
    	UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
		Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
		User portalAccountOwner1 = new User(
			UserRoleId = portalRole.Id,
			ProfileId = profile1.Id,
			Username = System.now().millisecond() + 'SearchAgency_test@test.com',
		   	Alias = 'batman',
			Email='bruce.wayne3@wayneenterprises.com',
			EmailEncodingKey='UTF-8',
			Firstname='Bruce',
			Lastname='Wayne',
			LanguageLocaleKey='en_US',
			LocaleSidKey='en_US',
			TimeZoneSidKey='America/Chicago'
		);
		Database.insert(portalAccountOwner1);
		
		Account agency;
		
		System.RunAs(new User(id = UserInfo.getUserId())){ 		
			//Create Agency
			agency = factory.createPortalAgency(portalAccountOwner1.id);    	
		
			SearchAgencyController a = new SearchAgencyController();
			
			a.ScountryFilter='--All--';
			a.ScityFilter='--All--';
			
			a.research();
			a.changeCountryFilter();
		
		}
		
        	
        	
    }
}