@isTest
private class products_search_test
{
	public static User portalUser {get{
	if (null == portalUser) {
	portalUser = [Select Id, Name, Contact.AccountId from User where email = 'test12345@test.com' limit 1];
	} return portalUser;} set;}

	@testSetup static void setup() {
		TestFactory tf = new TestFactory();
		tf.setupProdcutsTest();

	}

	static testMethod void recreateProductQuote(){
       	TestFactory TestFactory = new TestFactory();

		Account school = TestFactory.createSchool();
		Account campus = TestFactory.createCampus(school, new Account(Id=portalUser.Contact.AccountId));
		Course__c course = TestFactory.createCourse();
		Campus_Course__c cc = TestFactory.createCampusCourse(campus, course);
		Web_Search__c ws = TestFactory.createWebSearch(3, cc, 'Brazil', false);

		system.runAs(portalUser){
			ApexPages.currentPage().getParameters().put('wsid', ws.Id);
			products_search p = new products_search();


			//Select all courses from the cart
			for(SelectOption op : p.courses)
				p.selectedCourses.add(op.getValue());

			//Add products to cart
			for(String av : p.result.keySet())
				for(String cat : p.result.get(av).keyset())
					for(products_search.products prod : p.result.get(av).get(cat)){
						prod.product.Selected__c = true;
						for(Quotation_List_Products__c f : prod.prodFees)	
							f.isSelected__c = true;
					}

			p.saveProduct();

			ApexPages.currentPage().getParameters().remove('wsid');


			Search_Courses__c sc = [SELECT Id FROM Search_Courses__c limit 1];

			//Call constructor for recreate quote
			list<Search_Course_Product__c> scp = [SELECT Category__c, Currency__c, Description__c, End_Date__c, isCustom__c, isOtherGroupProduct__c, Name__c, Payment_Date__c, Price__c, Quantity__c, Quotation_Products_Services__c, Related_to_Product__c, Related_to_Product_Name__c, Search_Course__c, Selected__c, Start_Date__c, Total__c, Unit_Description__c, Web_Search__c FROM Search_Course_Product__c WHERE Search_Course__c = :sc.Id];

			system.debug('scp===' + scp);

			p = new products_search(scp[0]); //Compare Page
			p = new products_search(scp); // Recreate Quote




			//Add courses for Combined Quotation

			ws.Combine_Quotation__c = true;
			update ws;
			ApexPages.currentPage().getParameters().put('wsid', ws.Id);
			p = new products_search();

			//Add products to cart
			for(String av : p.result.keySet())
				for(String cat : p.result.get(av).keyset())
					for(products_search.products prod : p.result.get(av).get(cat)){
						prod.product.Selected__c = true;
						for(Quotation_List_Products__c f : prod.prodFees)	
							f.isSelected__c = true;
					}

			p.saveProduct();


		}
	}

	static testMethod void productsSearch(){

		Contact client = [Select Id From Contact limit 1];

		system.runAs(portalUser){

			//Call from Client Pop UP
			ApexPages.currentPage().getParameters().put('id', client.Id);
			products_search p = new products_search();

			Integer hasItems = p.hasItems;
			Web_Search__c webSearch = p.webSearch;
			List<SelectOption> courses = p.courses;
			String combinedCurrency = p.combinedCurrency;
			List<String> selectedCourses = p.selectedCourses;
			String selProvider = p.selProvider;
			List<SelectOption> providers = p.providers;
			List<SelectOption> countries = p.countries;
			List<SelectOption> categories = p.categories;
			map<String, list<products_search.payDates>> mFees = p.mFees;
			map<String, list<products_search.payDates>> validDates = p.validDates;
			map<String, Boolean> hasFee = p.hasFee;
			Quotation_Products_Services__c product = p.product;
			boolean hasDates = p.hasDates;
			boolean productsAdded = p.productsAdded;
			p.dateSearchValue = new Contact(Expected_Travel_Date__c = system.today().addDays(1));
			Contact dateSearchValue = p.dateSearchValue;
			p.searchProducts();

			for(String av : p.result.keySet())
				for(String cat : p.result.get(av).keyset())
					for(products_search.products prod : p.result.get(av).get(cat)){
					ApexPages.currentPage().getParameters().put('pId', prod.product.Id);
					ApexPages.currentPage().getParameters().put('cat', cat);
					ApexPages.currentPage().getParameters().put('av', av);
					break;
				}

			p.showPrices();

			for(String av : p.result.keySet())
				for(String cat : p.result.get(av).keyset())
					for(products_search.products prod : p.result.get(av).get(cat)){
					ApexPages.currentPage().getParameters().put('pId', prod.product.Id);
					ApexPages.currentPage().getParameters().put('cat', cat);
					ApexPages.currentPage().getParameters().put('av', av);
					break;
				}

			p.findPrice();

			for(String av : p.result.keySet())
				for(String cat : p.result.get(av).keyset())
					for(products_search.products prod : p.result.get(av).get(cat)){
					ApexPages.currentPage().getParameters().put('pId', prod.product.Id);
					ApexPages.currentPage().getParameters().put('cat', cat);
					ApexPages.currentPage().getParameters().put('fee', prod.prodFees[0].Id);
					ApexPages.currentPage().getParameters().put('av', av);

					break;
				}

			p.findPrice();


			for(String av : p.result.keySet())
				for(String cat : p.result.get(av).keyset())
					for(products_search.products prod : p.result.get(av).get(cat)){
					prod.product.Selected__c = true;
					for(Quotation_List_Products__c f : prod.prodFees)	
						f.isSelected__c = true;
				}

			p.saveProduct();
				
		}
	}
}