/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TaskCalendar_Test {

    static testMethod void myUnitTest() {
        TestFactory factory = new TestFactory();
				
		Account agency = factory.createAgency();
		Contact employee = factory.createEmployee(agency);
		Contact client;
		Quotation__c quote;
		User userEmp = factory.createPortalUser(employee);
		
		Account school = factory.createSchool();
  		Account campus = factory.createCampus(school, agency);
  		Course__c course = factory.createCourse();
		Campus_Course__c cc = factory.createCampusCourse(campus, course);
		
	    Date dueDate = Date.today();
	    TasksCalendarController cal = new TasksCalendarController();
	    
		system.runAs(userEmp){
			
			client = factory.createClient(agency); //Client			
			quote = factory.createQuotation(client, cc); //Quotation	
			
			
			Custom_Note_Task__c t = new Custom_Note_Task__c();
			t.Comments__c='bla bla bla';
			t.Subject__c = 'Follow Up';
			t.Due_Date__c = dueDate;
			t.Assign_To__c =  userEmp.id;
			t.AssignedOn__c = dueDate;
			t.AssignedBy__c = userEmp.id;
			t.Status__c = 'In Progress';
			t.Selected__c = true;
			insert t;
			
			cal.listView = 'calendar';
			
			cal.pageLoad();
			cal.changedView();
			cal.dayButton();
			cal.todayButton();
			cal.nextButton();
			
			
		}
    }
}