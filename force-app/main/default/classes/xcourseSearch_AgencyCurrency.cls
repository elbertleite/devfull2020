public class xcourseSearch_AgencyCurrency{

	public Account agency {get;set;}
	private boolean isbaseCurrencySet = false;
	private string optionalCurrencyAgency;
	public xcourseSearch_AgencyCurrency(ApexPages.StandardController controller){
		this.agency = getDetails(controller.getRecord().id);
		optionalCurrencyAgency = agency.Optional_Currency__c;
		retrieveAgencyOptions();
		getCurrentCurrencies();	
		retrieveAgenciesCurrency();	
	}
	
	public string accRTName {get;set;}
	private Account getDetails(String id){
		Account acc = [Select Name,RecordType.Name, CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name, Nickname__c, Agency_Type__c, Group_Type__c, Phone, Skype__c, Optional_Currency__c, account_currency_iso_code__c, 
						BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,  ParentId, Parent.Name, 
						Registration_name__c, Year_established__c, BusinessEmail__c, Website,
						( Select Account_Nickname__c, Account_Name__c, Account_Number__c, Bank__c, Branch_Address__c, Branch_Name__c, Branch_Phone__c, BSB__c, IBAN__c, Notes__c, Swift_Code__c from Bank_Details1__r)
				from Account where id = :id];
				
		if(acc.RecordType.Name=='campus')
			accRTName = 'School';
		if(acc.RecordType.Name=='Agency')
			accRTName = 'Agency';
				
		return acc;
	}

	public PageReference refreshCurrency(){
		//agency = [Select id, Optional_Currency__c, account_currency_iso_code__c from Account where id = :agency.id];
		//mapCurrency = null;
		CurrenciesRate = null;
		retrieveAgenciesCurrency();
		return null;
	}
	
	private List<Currency_rate__c> CurrenciesRate;
	public List<Currency_rate__c> getCurrenciesRate(){	
		if(CurrenciesRate == null)
			CurrenciesRate = [Select CurrencyCode__c, Value__c, High_Value__c, CreatedDate, Agency_Optional_Currency__c from Currency_rate__c where agency__c = :agency.Id and CurrencyCode__c != null order by CurrencyCode__c];
		return CurrenciesRate;
	}
	
	
	
	public PageReference changeAgencyCurrency(){
		//mapCurrency = null;
		///CurrenciesRate = null;
		
		
		return null;
		
	}
	
	
	
	public pageReference saveCurrencies(){
		Try{
			update CurrenciesRate; 
			if(agency.Optional_Currency__c != null && optionalCurrencyAgency != agency.Optional_Currency__c){
				delete [Select Id from Currency_rate__c where Agency__c = :agency.id and CurrencyCode__c = :optionalCurrencyAgency];
				list<Currency_rate__c> clearOptionalCurrency = [Select Id, Agency_Optional_Currency__c from Currency_rate__c WHERE Agency__c = :agency.id];
				for(Currency_rate__c clear :clearOptionalCurrency)
					clear.Agency_Optional_Currency__c = false;
				update clearOptionalCurrency;

				Currency_rate__c cr = new Currency_rate__c(agency__c = agency.id, value__c = 1,	High_Value__c = 1, CurrencyCode__c = agency.Optional_Currency__c, Agency_Optional_Currency__c = true, Description__c = currencyMap.get(agency.Optional_Currency__c));
				insert cr;
				CurrenciesRate.add(cr);
				mapCurrency.put(agency.Optional_Currency__c, 1);
				optionalCurrencyAgency = agency.Optional_Currency__c;
				
			}
			if(agency.Optional_Currency__c == null || agency.Optional_Currency__c == ''){
				delete [Select Id from Currency_rate__c where Agency__c = :agency.id and Agency_Optional_Currency__c = true];
			}
			
			update agency;
			CurrenciesRate = null;
			getCurrenciesRate();
			getCurrentCurrencies();
			retrieveAgenciesCurrency();
		}catch(Exception e){}
		
		return null;
		}
	

	public List<SelectOption> optionalCurrency {
		get{
		if(optionalCurrency == null){
			optionalCurrency = new List<SelectOption>();
			Schema.DescribeFieldResult F = Account.fields.account_currency_iso_code__c.getDescribe();
			List<Schema.PicklistEntry> P = F.getPicklistValues();
			optionalCurrency.add(new SelectOption('','--None--'));
			for (Schema.PicklistEntry lst:P)
				optionalCurrency.add(new SelectOption(lst.getValue(),lst.getLabel()));
		}
		return optionalCurrency;
	}
		set;
	}
	
	

	public map<string,double> getAgencyCurrencyConvertion(){
		System.debug('==>agency.account_currency_iso_code__c: '+agency.account_currency_iso_code__c);
		return CurrencyConvertionYahoo(agency.account_currency_iso_code__c);
	}
		
	public map<string,double> getAgencyOptionalCurrencyConvertion(){
		return CurrencyConvertionYahoo(agency.Optional_Currency__c);
	}		
	
	private map<string,double> mapCurrency;
	
	
	private map<string,double> CurrencyConvertionYahoo(string source){
		System.debug('==>source: '+source);
		if(mapCurrency == null && CurrenciesRate != null && CurrenciesRate.size() > 0){
			mapCurrency = new map<string,double>();
            
			string agencyCurrencies = '';
			
			System.debug('==>CurrenciesRate: '+CurrenciesRate);
				for(Currency_rate__c ci : CurrenciesRate){
					
					mapCurrency.put(ci.CurrencyCode__c ,0.0);
					if(source != ci.CurrencyCode__c){
						String endpoint = 'https://api.exchangeratesapi.io/latest?base='+ci.CurrencyCode__c+'&symbols='+source;
            
						Http http = new Http();
						HttpRequest req = new HttpRequest();
						req.setEndpoint(endpoint);
						req.setMethod('GET');
						HttpResponse res = http.send(req);
						String body = res.getBody();

						system.debug('==>body: ' + body);
						FixIoCurrencyRate obj = (FixIoCurrencyRate) System.JSON.deserialize(body.replace('"date":', '"date_x":'), FixIoCurrencyRate.class);
						if(obj.rates != null && obj.rates.size() > 0)
							mapCurrency.put(obj.base, obj.rates.get(source));
						// agencyCurrencies += ci.CurrencyCode__c+ ',';
					}
				}
					
				
               // String endpoint = 'https://api.exchangeratesapi.io/latest?base='+source+'&symbols='+agencyCurrencies.substringBeforeLast(',');
                // String endpoint = 'https://api.exchangeratesapi.io/latest?base='+source;
            
                // Http http = new Http();
                // HttpRequest req = new HttpRequest();
                // req.setEndpoint(endpoint);
                // req.setMethod('GET');
                // HttpResponse res = http.send(req);
                // String body = res.getBody();

                // system.debug('==>body: ' + body);
                // FixIoCurrencyRate obj = (FixIoCurrencyRate) System.JSON.deserialize(body.replace('"date":', '"date_x":'), FixIoCurrencyRate.class);
				
                // if(obj.rates != null && obj.rates.size() > 0)
                //     mapCurrency.putAll(obj.rates);
                // else{

                // }
					
				
				//System.debug('==>obj: '+obj);
			
		}
		return mapCurrency;
		
	}

    
	public pageReference updateCurrencyRate(){
		mapCurrency = null;
		CurrenciesRate = null;
		return null;
	}
	
	
	public pageReference copyCurrencyRate(){
		Decimal unitPrice = 0;
		decimal valueSpread = decimal.ValueOf(ApexPages.currentPage().getParameters().get('lowSpread')); 
		System.debug('==> valueSpread: '+valueSpread);
		
		for(Currency_rate__c ci : CurrenciesRate){
			System.debug('==> mapCurrency.get(ci.CurrencyCode__c): '+mapCurrency.get(ci.CurrencyCode__c));
			
			if(valueSpread != null && valueSpread != 0 && mapCurrency.get(ci.CurrencyCode__c) != null){
				unitPrice = mapCurrency.get(ci.CurrencyCode__c) * (1+ valueSpread/100);
				if(unitPrice != null)
					ci.Value__c = unitPrice.SetScale(4);
			}else{ 
				System.debug('==> CurrencyCode: '+ci.CurrencyCode__c);
				unitPrice = mapCurrency.get(ci.CurrencyCode__c);
				System.debug('==> unitPrice: '+unitPrice);
				if(unitPrice != null)
					ci.Value__c = unitPrice.SetScale(4);
			}
		}
		return null;
	}

	public pageReference copyHighCurrencyRate(){
		decimal valueSpread = decimal.ValueOf(ApexPages.currentPage().getParameters().get('highSpread')); 
		for(Currency_rate__c ci : CurrenciesRate){
			if(valueSpread != null && valueSpread != 0){
				ci.High_Value__c = ci.Value__c * (1+ valueSpread/100);
			}else{ 
				ci.High_Value__c = ci.Value__c;
			}
		}
		return null;
	}
	
	//==============================================================================================
	
	
	private set<string> setCurrentCurrencies = new set<string>();
	private map<string, string> currencyMap = new map<string, string>();
	public list<string> selectedCurrentCurrencies{get{if(selectedCurrentCurrencies == null) selectedCurrentCurrencies = new list<string>(); return selectedCurrentCurrencies;} Set;}
	
	public List<SelectOption> getCurrentCurrencies(){
		List<SelectOption> options = new List<SelectOption>();
		setCurrentCurrencies = new set<string>();
		for(Currency_rate__c cr:[Select Agency__c, Value__c, High_Value__c, CurrencyCode__c, Description__c, Agency_Optional_Currency__c  from Currency_rate__c  WHERE Agency__c = :agency.Id and CurrencyCode__c != null order by Description__c]){
			setCurrentCurrencies.add(cr.CurrencyCode__c);
			if(!cr.Agency_Optional_Currency__c)
				options.add(new SelectOption(cr.CurrencyCode__c,cr.CurrencyCode__c +  ' - ' + cr.description__c));
		}
			
		return options;
		
	}
	
	public list<string> selectedAvailableCurrency{get{if(selectedAvailableCurrency == null) selectedAvailableCurrency = new list<string>(); return selectedAvailableCurrency;} Set;}
		
	public List<SelectOption> getCurrencies() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('','--None--'));
		for(SelectOption op: xCourseSearchFunctions.getCurrencies()){
			if((op.getValue() != '' && !setCurrentCurrencies.contains(op.getValue()))  || (optionalCurrencyAgency == op.getValue()) ){
				options.add(new SelectOption(op.getValue(),op.getLabel()));
				currencyMap.put(op.getValue(), op.getLabel().remove(op.getValue() + ' - '));
			}	
		}
		return options;
	}
	
	public List<SelectOption> getAvailableCurrencies() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('','--None--'));
		for(SelectOption op: xCourseSearchFunctions.getCurrencies()){
			// if(op.getValue() != '' && !setCurrentCurrencies.contains(op.getValue())){
			if(op.getValue() != ''){
				options.add(new SelectOption(op.getValue(),op.getLabel()));
				currencyMap.put(op.getValue(), op.getLabel().remove(op.getValue() + ' - '));
			}	
		}
		return options;
	}
	
	public PageReference addAgencyCurrency(){
		System.debug(selectedAvailableCurrency);
		if(mapCurrency == null)
			mapCurrency = new map<string,double>();
		list<Currency_rate__c> listCurrencies = new list<Currency_rate__c>();
		for(String sc: selectedAvailableCurrency){
			listCurrencies.add(new Currency_rate__c(agency__c = agency.Id, CurrencyCode__c = sc, description__c = currencyMap.get(sc), value__c = 1, High_Value__c = 1));
			mapCurrency.put(sc, 1);
		}
		System.debug(listCurrencies);
		insert listCurrencies;
		
		getCurrentCurrencies();	
		getCurrencies();
		CurrenciesRate = null;
		return null;
	}

	public PageReference removeAgencyCurrency(){
		delete [Select Id from Currency_rate__c WHERE Agency__c = :agency.Id and CurrencyCode__c in :selectedCurrentCurrencies];
		
		getCurrentCurrencies();
		getCurrencies();
		CurrenciesRate = null;
		return null;
	}
	
	
	//========================================================================
	
	public class CurrencyRate {
		public cls_query query;
	}
	public class cls_query {
		public Integer count;	//8
		public String created;	//2016-07-11T04:45:04Z
		public String lang;	//en-US
		public cls_results results;
	}
	public class cls_results {
		public cls_rate[] rate;
	}
	public class cls_rate {
		public String id;	//EURAUD
		public String Name;	//EUR/AUD
		public String Rate;	//1.4619
		//public String Date;	//7/11/2016
		//public String Time;	//2:11am
		public String Ask;	//1.4620
		public String Bid;	//1.4619
	}


    public class FixIoCurrencyRate {
		public string base;
		public date date_x;
		public map<string,double> rates;
	}

   

	
		/*static testMethod void testParse() {
			String json=		'{"query":{"count":8,"created":"2016-07-11T04:45:04Z","lang":"en-US","results":{"rate":[{"id":"EURAUD","Name":"EUR/AUD","Rate":"1.4619","Date":"7/11/2016","Time":"2:11am","Ask":"1.4620","Bid":"1.4619"},{"id":"EURBRL","Name":"EUR/BRL","Rate":"3.6408","Date":"7/8/2016","Time":"10:50pm","Ask":"3.6433","Bid":"3.6408"},{"id":"EURCAD","Name":"EUR/CAD","Rate":"1.4422","Date":"7/11/2016","Time":"2:44am","Ask":"1.4425","Bid":"1.4422"},{"id":"EURDKK","Name":"EUR/DKK","Rate":"7.4388","Date":"7/11/2016","Time":"3:47am","Ask":"7.4397","Bid":"7.4388"},{"id":"EURGBP","Name":"EUR/GBP","Rate":"0.8535","Date":"7/11/2016","Time":"3:31am","Ask":"0.8535","Bid":"0.8535"},{"id":"EURNZD","Name":"EUR/NZD","Rate":"1.5124","Date":"7/8/2016","Time":"10:28pm","Ask":"1.5134","Bid":"1.5124"},{"id":"EURSEK","Name":"EUR/SEK","Rate":"9.4870","Date":"7/11/2016","Time":"1:25am","Ask":"9.4889","Bid":"9.4870"},{"id":"EURUSD","Name":"EUR/USD","Rate":"1.1052","Date":"7/11/2016","Time":"3:20am","Ask":"1.1052","Bid":"1.1052"}]}}}';
			CurrencyRate obj = parse(json);
			System.assert(obj != null);
		}*/
	

	public map<string,map<string,decimal>> allAgenciesCurrency{get; set;}
	public set<string> allCurrenciesCode{get; set;}

	public void retrieveAgenciesCurrency(){
		allAgenciesCurrency = new map<string,map<string,decimal>>();
		allCurrenciesCode = new set<string>();
		selectedCurrentCurrencies = new list<string>();
		for(Currency_rate__c cr:[Select agency__c, agency__r.name, CurrencyCode__c, Value__c, High_Value__c, CreatedDate, Agency_Optional_Currency__c from Currency_rate__c where agency__r.parentId = :agency.parentId and CurrencyCode__c != null order by agency__r.name, CurrencyCode__c]){
			if(!allAgenciesCurrency.containsKey(cr.agency__r.name))
				allAgenciesCurrency.put(cr.agency__r.name, new map<string,decimal>{cr.CurrencyCode__c => cr.value__c});
			else if(!allAgenciesCurrency.get(cr.agency__r.name).containsKey(cr.CurrencyCode__c))
				allAgenciesCurrency.get(cr.agency__r.name).put(cr.CurrencyCode__c, cr.value__c);
			allCurrenciesCode.add(cr.CurrencyCode__c);
			if(agency.id == cr.agency__c)
				selectedCurrentCurrencies.add(cr.CurrencyCode__c);
		}
		//populate map with missing isocodes
		for(string ag:allAgenciesCurrency.keySet())
			for(string iso:allCurrenciesCode)
				if(!allAgenciesCurrency.get(ag).containsKey(iso))
					allAgenciesCurrency.get(ag).put(iso, -1);
		
	}


	public void cloneRates(){
		list<Currency_rate__c> toUpdate;
		if(selectedAgency.size() > 0)
			toUpdate = [Select agency__r.name, CurrencyCode__c, Value__c, High_Value__c, Agency_Optional_Currency__c from Currency_rate__c where agency__c in :selectedAgency];
		else toUpdate = [Select agency__r.name, CurrencyCode__c, Value__c, High_Value__c, Agency_Optional_Currency__c from Currency_rate__c where agency__r.parentId = :agency.parentId and agency__c != :agency.id];
		for(Currency_rate__c cr:toUpdate){
			if(allAgenciesCurrency.get(agency.name).containsKey(cr.CurrencyCode__c) && allAgenciesCurrency.get(agency.name).get(cr.CurrencyCode__c) != -1)
				cr.Value__c = allAgenciesCurrency.get(agency.name).get(cr.CurrencyCode__c);
		}
		update toUpdate;
		retrieveAgenciesCurrency();
	}

	public void addRemoveAgencyCurrency(){
		delete [Select Id from Currency_rate__c WHERE Agency__c = :agency.Id and CurrencyCode__c not in :selectedCurrentCurrencies];
		set<string> setCurrency = new set<string>();
		for(Currency_rate__c cr:[Select CurrencyCode__c from Currency_rate__c WHERE Agency__c = :agency.Id and CurrencyCode__c in :selectedCurrentCurrencies])
			setCurrency.add(cr.CurrencyCode__c);

		list<Currency_rate__c> listCurrencies = new list<Currency_rate__c>();
		for(String sc: selectedCurrentCurrencies){
			if(!setCurrency.contains(sc))
				listCurrencies.add(new Currency_rate__c(agency__c = agency.Id, CurrencyCode__c = sc, description__c = currencyMap.get(sc), value__c = 1, High_Value__c = 1));
			mapCurrency.put(sc, 1);
		}
		insert listCurrencies;

		CurrenciesRate = null;
		getCurrenciesRate();
		getCurrentCurrencies();
		retrieveAgenciesCurrency();
	}

	IpFunctions.userNoSharing uns = new IpFunctions.userNoSharing(); 
	private User currentUser = uns.getUserInformationNoSharing(UserInfo.getUserId());
	public list<String> selectedAgency {get{if(selectedAgency == null) selectedAgency = new list<String>(); return selectedAgency;} set;}
	public List<SelectOption> agencyOptions {get;set;}
	public void retrieveAgencyOptions(){
		
			agencyOptions = new List<SelectOption>();
			for(Account a: [Select Id, Name from Account where ParentId = :currentUser.Contact.Account.ParentId and id != :currentUser.Contact.AccountId and RecordType.Name = 'agency' order by name]){
				agencyOptions.add(new SelectOption(a.Id, a.Name));
				
			}
			
	}
	
}