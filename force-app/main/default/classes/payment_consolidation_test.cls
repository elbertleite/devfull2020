/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class payment_consolidation_test {

    static testMethod void myUnitTest() {
         
       /*TestFactory tf = new TestFactory();
       
       Account school = tf.createSchool();
       Account agency = tf.createAgency();
       Contact emp = tf.createEmployee(agency);
       User portalUser = tf.createPortalUser(emp);
       Account campus = tf.createCampus(school, agency);
       Course__c course = tf.createCourse();
       Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
       
       Test.startTest();
       system.runAs(portalUser){
       	   Contact client = tf.createLead(agency, emp);
	       client_course__c booking = tf.createBooking(client);
	       client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       
	       
	       List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
	       
	       client_course_instalment_pay instPay = new client_course_instalment_pay(new ApexPages.StandardController(instalments[0]));
	       instPay.newPayDetails.Value__c = 2230;
		   instPay.newPayDetails.Date_Paid__c = system.today();
		   instPay.newPayDetails.Payment_Type__c = 'Cash';
		   instPay.addPaymentValue();
		   instPay.savePayment();
		   
		   client_course_instalment_pay instPay2 = new client_course_instalment_pay(new ApexPages.StandardController(instalments[1]));
	       instPay2.newPayDetails.Value__c = 1550;
		   instPay2.newPayDetails.Date_Paid__c = system.today();
		   instPay2.newPayDetails.Payment_Type__c = 'CreditCard';
		   instPay2.addPaymentValue();
		   instPay2.savePayment();
		   
		   
		   
		   client_product_service__c product =  tf.createCourseProduct(booking, agency);
		   client_product_service__c product2 =  tf.createCourseProduct(booking, agency);
		   client_product_service__c product3 =  tf.createCourseProduct(booking, agency);
	       
	       client_course_product_pay prodPay = new client_course_product_pay(new ApexPages.StandardController(product));
	       prodPay.newPayDetails.Value__c = 50;
		   prodPay.newPayDetails.Payment_Type__c = 'CreditCard';
		   prodPay.addPaymentValue();
		   prodPay.savePayment();
		   
	       client_course_product_pay prodPay2 = new client_course_product_pay(new ApexPages.StandardController(product2));
	       prodPay2.newPayDetails.Value__c = 50;
		   prodPay2.newPayDetails.Payment_Type__c = 'Cash';
		   prodPay2.addPaymentValue();
		   prodPay2.savePayment();
	       
	       
	       
	       ApexPages.currentPage().getParameters().put('cs', instalments[3].id);
	       ApexPages.currentPage().getParameters().put('pd', product2.id);
	       ApexPages.currentPage().getParameters().put('ct', client.id);
	       
	       client_course_create_invoice invoicePay = new client_course_create_invoice();
	       invoicePay.createInvoice();
	       invoicePay.newPayDetails.Value__c = 1600;
		   invoicePay.newPayDetails.Payment_Type__c = 'Creditcard';
		   invoicePay.addPaymentValue();
		   invoicePay.savePayment();
	       
	       
	       List<client_course_instalment_payment__c> updatePayments = new List<client_course_instalment_payment__c>();
		   for(client_course_instalment_payment__c ccip : [Select Id, selected__c from client_course_instalment_payment__c]){
		   		ccip.selected__c = true;
		   		updatePayments.add(ccip);
		   }//end for
		   
		   update updatePayments;
		   
	       payment_consolidation testClass = new payment_consolidation();
		   
		   Contact filterDate = testClass.filterDate;
		   List<SelectOption> paymentTypes = testClass.paymentTypes;
		   String selectedPaymentStatus = testClass.selectedPaymentStatus;
		   List<SelectOption> paymentStatus = testClass.paymentStatus;
		   
		   String selectedAgencyGroup = testClass.selectedAgencyGroup;
		   List<SelectOption> agencyGroupOptions = testClass.agencyGroupOptions;
		   testClass.getAgencies();
		   
		   testClass.changeGroup();
		   testClass.selectedAgency = agency.id;
		   list<SelectOption> schools = testClass.schools;
		   testClass.getPeriods();
		   testClass.getdateCriteriaOptions();
		   client_course_instalment__c dateFilter = testClass.dateFilter;
		   
		   testClass.SelectedPeriod = 'TODAY';
		   testClass.confirmPayments2();
		   
		   
		   String instPayId;
		   String prodPayId;
		   String invPayId;
		   
		   for(payment_consolidation.consolidation cns : testClass.result){
		   		if(cns.invoice!=null)
		   			for(client_course_instalment_payment__c ccip : cns.invoice.client_course_instalment_payments__r){
						invPayId = ccip.Id;
						break;
					}
					
				if(cns.instalment!=null)
		   			for(client_course_instalment_payment__c ccip : cns.instalment.client_course_instalment_payments__r){
						instPayId = ccip.Id;
						break;
					}
					
				if(cns.product!=null)
		   			for(client_course_instalment_payment__c ccip : cns.product.client_course_instalment_payments__r){
						prodPayId = ccip.Id;
						break;
					}
		   }//end for
		   
		   
		   
		   ApexPages.currentPage().getParameters().put('paymentid', instPayId);
		   ApexPages.currentPage().getParameters().put('type', 'instalment');
		   testClass.SelectedPeriod = 'THIS_WEEK';
		   testClass.unconfirmPayment();
		    
		    
		   ApexPages.currentPage().getParameters().remove('paymentid');
		   ApexPages.currentPage().getParameters().remove('type');
		   ApexPages.currentPage().getParameters().put('paymentid', invPayId);
		   ApexPages.currentPage().getParameters().put('type', 'invoice');
		   testClass.SelectedPeriod = 'THIS_MONTH';
		   testClass.unconfirmPayment();
		   
		   
		   ApexPages.currentPage().getParameters().remove('paymentid');
		   ApexPages.currentPage().getParameters().remove('type');
		   ApexPages.currentPage().getParameters().put('paymentid', prodPayId);
		   ApexPages.currentPage().getParameters().put('type', 'product');
		   testClass.dateCriteria = 'pr';
		   testClass.unconfirmPayment();
		   
		   testClass.selectedPaymentTypes = 'Cash';
		   testClass.SelectedPeriod = 'THIS_WEEK';
		   testClass.searchPayments();
		   testClass.SelectedPeriod = 'TODAY'; 
		   testClass.searchPayments();
		   
		   testClass.SelectedPeriod = 'THIS_MONTH'; 
		   testClass.searchPayments();
		   
	       
       }
       Test.stopTest();*/
        
    }
}