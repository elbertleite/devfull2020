@isTest
private class mapping_rdstation_talesforce_test{
	static testMethod void myUnitTest() {
		TestFactory tf = new TestFactory();
       	Account agencyGroup = tf.createAgencyGroup();

		agencyGroup.RDStation_Salesforce_mapping__C = '[{"salesforceOptions":[],"salesforcefieldRequireMapping":false,"salesforceField":"Level of Education","rdStationUuid":"674998e4-ed8e-44c9-bb1d-28edb47b7570","rdStationOptions":[],"rdStationname":"Escolaridade","rdStationField_identifier":"cf_escolaridade","mapping":{"Curso técnico – completo":"Curso técnico – completo","Curso técnico – cursando":"Curso técnico – cursando","Graduação – completo":"Graduação – completo","Graduação – trancada":"Graduação – trancada","Graduação – cursando":"Graduação – cursando","Ensino médio – completo":"Ensino médio – completo","Ensino médio – cursando":"Ensino médio – cursando"},"fieldType":"COMBO_BOX","fieldContentType":"STRING"},{"salesforceOptions":[],"salesforcefieldRequireMapping":true,"salesforceField":"Destination_Country__c","rdStationUuid":"01bd6d94-5762-4917-9960-f3cd3def95b5","rdStationOptions":[],"rdStationname":"Destinos de Interesse","rdStationField_identifier":"cf_destinos_de_interesse","mapping":{"Austrália":"Australia","Canadá":"Canada","Nova Zelândia":"New Zealand","Irlanda":"Ireland","Reino Unido":"United Kingdom","Estados Unidos":"United States"},"fieldType":"MULTIPLE_CHOICE","fieldContentType":"STRING[]"},{"salesforceOptions":[],"salesforcefieldRequireMapping":false,"salesforceField":"Comments_web_enquiry","rdStationUuid":"b2aaeee1-04fc-420c-88d9-a9ca64aaf765","rdStationOptions":[],"rdStationname":"Comentários","rdStationField_identifier":"cf_comentarios","mapping":{},"fieldType":"TEXT_INPUT","fieldContentType":"STRING"},{"salesforceOptions":[],"salesforcefieldRequireMapping":true,"salesforceField":"Agency","rdStationUuid":"2d9ffefb-98c8-4062-8778-b3702a75480c","rdStationOptions":[],"rdStationname":"Agência","rdStationField_identifier":"cf_agencia","mapping":{"ABC Paulista":"0019000001MjqHuAAJ","Itaim - Zona Sul":"0019000001MjqI3AAJ","Tatuapé - Zona Leste":"0019000001MjqITAAZ","Paulista - Zona Sul":"0019000001MjqIPAAZ","Brooklin - Zona Sul":"0019000001MjqIVAAZ"},"fieldType":"COMBO_BOX","fieldContentType":"STRING"}]';
       	
		agencyGroup.RDStation_Access_Token__c = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwczovL2FwaS5yZC5zZXJ2aWNlcyIsInN1YiI6Ii05RVNGODlDSEh6R2pDZ3ktcktXLWNBbFVwemlHbVIwZ2NQMkxFeUJCVzhAY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vYXBwLnJkc3RhdGlvbi5jb20uYnIvYXBpL3YyLyIsImV4cCI6MTUyOTcyMzQ0MiwiaWF0IjoxNTI5NjM3MDQyLCJzY29wZSI6IiJ9.KVptT_I58GPbLajX3MHRw8KTNcQv7OmhbXbaFDLN6ySxUlKtXwxisLnR48fPuHGwry3Q6biv354cUsoQdHun1zW7FrYWsnn24EhGWLq_9usGYGRsoxFeuWx-v5gX33m_29BiXAPQ-OjseYtcVu-n09u4bUqz7aJn5SBCCaqx1gsUIg9ku6PYZZfH6wSNXo5OCTT-CDJXvraBxpg1l_gMe4tmdhkIrgpkNTJQQqwFpf40RrfjSKf1L-57kj1NznpwdxkQYrvQdJh1-PCtbECnM8a9K1blbdBowCFqcLYImQRmeDhsC1FhUJM0m0OPCZS5JoBAZK8yLJkOJU3rQfYO1w';
		agencyGroup.RDStation_authorization__c = '5334a1f9c758260f3ee10aa918f242f1';
		agencyGroup.RDStation_Refresh_Token__c = 'z_x029aJlIIup4bdU1t2dxeqoZaXUWjkgiSdMwKErzg';
		agencyGroup.RDStation_credential__c = 'ace3f68387c03c8f6d2b685f2e383eb7';
		agencyGroup.RDStation_Token_Expiration__c = Date.today();
		
		update agencyGroup;

		Account agency = tf.createAgency();
		agency.parentID = agencyGroup.id;

		Contact emp = tf.createEmployee(agency);

		Contact clt = tf.createClient(agency);

		Contact lead = tf.createClient(agency);

		Account school = tf.createSchool();

		emp.account = agency;

       	User portalUser = tf.createPortalUser(emp);

		Test.startTest();
     	system.runAs(portalUser){

			Apexpages.currentPage().getParameters().put('id', emp.id);
				
			mapping_rdstation_salesforce mapp = new mapping_rdstation_salesforce();

			mapp.retrieveFieldsRDStation();

			Apexpages.currentPage().getParameters().put('rdStationUuid', 'fdeaba6ec-6794-4b13-b2ea-e93d47c0d828');
			Apexpages.currentPage().getParameters().put('rdOptionValue', '12345');
			Apexpages.currentPage().getParameters().put('salesForceValue', '12345');
			Apexpages.currentPage().getParameters().put('salesforceField', 'Client_Classification__c');

			mapp.changeOptionFieldSalesforceMapping();

			mapp.rdStationFieldSelected = 'fdeaba6ec-6794-4b13-b2ea-e93d47c0d828';
			mapp.createNewMapping();
			mapp.saveFields();
			Apexpages.currentPage().getParameters().put('rdStationUuid', 'f0a3dd8a-2342-432c-a1ce-bbg1bb559d6edf4');
			
			mapp.rdStationFieldSelected = 'f0a3dd8a-2342-432c-a1ce-bbg1bb559d6edf4';
			
			mapp.createNewMapping();
			mapp.selectFieldSalesforce();
			//mapp.removeMapping();

			

			Apexpages.currentPage().getParameters().put('salesforceField', 'Lead_Destinations__c');			
			mapp.selectFieldSalesforce();

			Apexpages.currentPage().getParameters().put('salesforceField', 'Preferable_Language__c');
			mapp.selectFieldSalesforce();
			
			Apexpages.currentPage().getParameters().put('salesforceField', 'Age_Range__c');
			mapp.selectFieldSalesforce();

			Apexpages.currentPage().getParameters().put('salesforceField', 'Occupation__c');
			mapp.selectFieldSalesforce();

			Apexpages.currentPage().getParameters().put('salesforceField', 'Marital_Status__c');
			mapp.selectFieldSalesforce();

			Apexpages.currentPage().getParameters().put('salesforceField', 'Highest_Education_Completed__c');
			mapp.selectFieldSalesforce();

			Apexpages.currentPage().getParameters().put('salesforceField', 'Gender__c');
			mapp.selectFieldSalesforce();

			Apexpages.currentPage().getParameters().put('salesforceField', 'Travel_Duration__c');
			mapp.selectFieldSalesforce();

			Apexpages.currentPage().getParameters().put('salesforceField', 'When_are_you_planning_to_travel__c');
			mapp.selectFieldSalesforce();

			Apexpages.currentPage().getParameters().put('salesforceField', 'Lead_Product_Type__c');
			mapp.selectFieldSalesforce();

			Apexpages.currentPage().getParameters().put('salesforceField', 'Lead_Study_Type__c');
			mapp.selectFieldSalesforce();

			Apexpages.currentPage().getParameters().put('salesforceField', 'LeadSource');
			mapp.selectFieldSalesforce();

			mapp.removeMapping();

			mapp.checkExpiredToken();

			mapp.changeOptionMarkAsOpportunity();

			Apexpages.currentPage().getParameters().put('field', 'conversionFieldsToHide');
			mapp.deleteField();

			Apexpages.currentPage().getParameters().put('field', 'rdStationTagsToFilter');
			mapp.deleteField();

			Apexpages.currentPage().getParameters().put('field', 'emailsRegisteredToSendSync');
			mapp.deleteField();

			Apexpages.currentPage().getParameters().put('field', 'conversionFieldsToHide');
			mapp.addFieldFilter();

			Apexpages.currentPage().getParameters().put('field', 'rdStationTagsToFilter');
			mapp.addFieldFilter();

			Apexpages.currentPage().getParameters().put('field', 'emailsRegisteredToSendSync');
			mapp.addFieldFilter();

			mapp.changeOptionChangeOwnership();


			mapping_rdstation_salesforce.refreshMapping(agencyGroup, null);

			mapp.getOwners();

			mapp.getSameFieldsRDStation(null);

			String emails = lead.email+';';

			Apexpages.currentPage().getParameters().put('emails', 'emails');
			mapp.synchronizeContacts();

		}
	}
}