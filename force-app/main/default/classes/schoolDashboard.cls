public class schoolDashboard {

	private string userAgency;
	public schoolDashboard(){
		userAgency = [Select AccountId from user where id = :UserInfo.getUserId() limit 1].AccountId;
	}
	
	public string fromDate{get{if(fromDate == null) fromDate = System.now().addMonths(-1).format('dd/MM/yyyy') ;  return fromDate;} set;}
	public string toDate{get{if(toDate == null) toDate =  System.now().format('dd/MM/yyyy'); return toDate;} set;}

	public string searchOption{get{if(searchOption == null) searchOption = 'ct'; return searchOption;} set;}
	private list<WS_restCallouts.groupCourses> quotesPerCampus;
	public list<groupCourses> getQuotesPerCampus(){
		map<string, map<string, WS_restCallouts.courseDetails>> mapCourses = new map<string, map<string, WS_restCallouts.courseDetails>>();
		list<string> groupId = new list<string>();
		list<groupCourses> lgc = new list<groupCourses>();
		
		if(quotesPerCampus == null && selectedCampus != ''){
			
			quotesPerCampus = WS_restCallouts.getCoursesPerNationality(selectedCampus, searchOption, fromDate, toDate);
			
		}
		if(searchOption == 'nt'){
			for(AggregateResult qd: [Select Quotation__r.Client__r.Nationality__c country, Campus_Course__r.course__c cid, Course_Name__c course,  COUNT(id) tot from Quotation_Detail__c 
											WHERE Campus__c = :localCampusId.get(selectedCampus) AND CreatedDate >= :date.parse(fromDate) and CreatedDate <= : date.parse(toDate)
											GROUP BY Quotation__r.Client__r.Nationality__c, Campus_Course__r.course__c, Course_Name__c order by Quotation__r.Client__r.Nationality__c, COUNT(Id)]){
				if(!mapCourses.containsKey((string) qd.get('country'))){
					mapCourses.put((string) qd.get('country'), new map<string, WS_restCallouts.courseDetails>{(string) qd.get('cid') => new WS_restCallouts.courseDetails((string) qd.get('course'),(integer) qd.get('tot'))} );
					groupId.add((string) qd.get('country'));
				}else if(!mapCourses.get((string) qd.get('country')).containsKey((string) qd.get('cid'))){
					mapCourses.get((string) qd.get('country')).put((string) qd.get('cid'), new WS_restCallouts.courseDetails((string) qd.get('course'),(integer) qd.get('tot')));
				}else{
					mapCourses.get((string) qd.get('country')).get((string) qd.get('cid')).total += (integer) qd.get('tot');
				}
			}
		}
		else
		if(searchOption == 'ag'){
			
			for(AggregateResult qd: [Select Quotation__r.Agency__r.name agency, Campus_Course__r.Course__c cid, Course_Name__c course,  COUNT(id) tot from Quotation_Detail__c 
										WHERE Campus__c = :localCampusId.get(selectedCampus) AND CreatedDate >= :date.parse(fromDate) and CreatedDate <= : date.parse(toDate)
										GROUP BY Quotation__r.Agency__r.name, Campus_Course__r.Course__c, Course_Name__c order by Quotation__r.Agency__r.name, COUNT(Id)])
				if(!mapCourses.containsKey((string) qd.get('agency'))){
					mapCourses.put((string) qd.get('agency'), new map<string, WS_restCallouts.courseDetails>{(string) qd.get('cid') => new WS_restCallouts.courseDetails((string) qd.get('course'),(integer) qd.get('tot'))} );
					groupId.add((string) qd.get('agency'));
				}else if(!mapCourses.get((string) qd.get('agency')).containsKey((string) qd.get('cid'))){
					mapCourses.get((string) qd.get('agency')).put((string) qd.get('cid'), new WS_restCallouts.courseDetails((string) qd.get('course'),(integer) qd.get('tot')));
				}else{
					mapCourses.get((string) qd.get('agency')).get((string) qd.get('cid')).total += (integer) qd.get('tot');
				}
					
			
		}
		else if(searchOption == 'cp'){
			for(AggregateResult qd: [Select Campus_Course__r.Course__r.type__c courseType, Campus_Course__r.Course__c cid, Course_Name__c course,  COUNT(id) tot from Quotation_Detail__c 
										WHERE Campus__c = :localCampusId.get(selectedCampus) AND CreatedDate >= :date.parse(fromDate) and CreatedDate <= : date.parse(toDate)
										GROUP BY Campus_Course__r.Course__r.type__c, Campus_Course__r.Course__c, Course_Name__c order by  Campus_Course__r.Course__r.type__c, COUNT(Id)])
								
				if(!mapCourses.containsKey((string) qd.get('courseType'))){
						mapCourses.put((string) qd.get('courseType'), new map<string, WS_restCallouts.courseDetails>{(string) qd.get('cid') => new WS_restCallouts.courseDetails((string) qd.get('course'),(integer) qd.get('tot'))} );
						groupId.add((string) qd.get('courseType'));
					}else if(!mapCourses.get((string) qd.get('courseType')).containsKey((string) qd.get('cid'))){
						mapCourses.get((string) qd.get('courseType')).put((string) qd.get('cid'), new WS_restCallouts.courseDetails((string) qd.get('course'),(integer) qd.get('tot')));
					}else{
						mapCourses.get((string) qd.get('courseType')).get((string) qd.get('cid')).total += (integer) qd.get('tot');
					}
					
			
		}
		else
		if(searchOption == 'ct'){
			
			for(AggregateResult qd: [Select Quotation__r.Agency__r.BillingCountry country, Campus_Course__r.Course__c cid, Course_Name__c course,  COUNT(id) tot from Quotation_Detail__c 
										WHERE Campus__c = :localCampusId.get(selectedCampus) AND CreatedDate >= :date.parse(fromDate) and CreatedDate <= : date.parse(toDate)
										GROUP BY Quotation__r.Agency__r.BillingCountry, Campus_Course__r.Course__c, Course_Name__c order by Quotation__r.Agency__r.BillingCountry, COUNT(Id)])
				if(!mapCourses.containsKey((string) qd.get('country'))){
					mapCourses.put((string) qd.get('country'), new map<string, WS_restCallouts.courseDetails>{(string) qd.get('cid') => new WS_restCallouts.courseDetails((string) qd.get('course'),(integer) qd.get('tot'))} );
					groupId.add((string) qd.get('country'));
				}else if(!mapCourses.get((string) qd.get('country')).containsKey((string) qd.get('cid'))){
					mapCourses.get((string) qd.get('country')).put((string) qd.get('cid'), new WS_restCallouts.courseDetails((string) qd.get('course'),(integer) qd.get('tot')));
				}else{
					mapCourses.get((string) qd.get('country')).get((string) qd.get('cid')).total += (integer) qd.get('tot');
				}
					
			
		}
		if(selectedCampus != ''){
			if(quotesPerCampus == null)
				quotesPerCampus = new list<WS_restCallouts.groupCourses>();
			for(string gi:groupId){
				integer countQuotes = 0;
				for(WS_restCallouts.courseDetails cd:mapCourses.get(gi).values())
					countQuotes += cd.Total;
				quotesPerCampus.add(new WS_restCallouts.groupCourses(gi, countQuotes, mapCourses.get(gi)));
			}
			
			map<string, WS_restCallouts.groupCourses> orderedGroupCourses = new map<string, WS_restCallouts.groupCourses>();
			list<string> listOrderedGroupCourses = new list<string>();
			for(WS_restCallouts.groupCourses gc:quotesPerCampus){
				if(gc.name != null)
					orderedGroupCourses.put(gc.name.toLowerCase(), gc);
				
			}
			
			listOrderedGroupCourses.addAll(orderedGroupCourses.keySet());
			listOrderedGroupCourses.sort();
			
			list<courseDetails> lcd;
			for(string s:listOrderedGroupCourses){
				WS_restCallouts.groupCourses gc = orderedGroupCourses.get(s);
				lcd = new list<courseDetails>();
				list<string> orderedCourses = new list<string>(gc.listCourses.keySet());
				orderedCourses.sort();
				for(string loc:orderedCourses)
					lcd.add(new courseDetails(gc.listCourses.get(loc).course, gc.listCourses.get(loc).total));
				lgc.add(new groupCourses(gc.name, gc.totalQuotes, lcd));
			}
			
		}
		
		
		
		return lgc;
	}
	
	public pageReference searchCampus(){
		quotesPerCampus = null;
		return null;
	}
	
	map<string, string> localCampusId = new map<string, string>();
	public string selectedCampus{get{if(selectedCampus == null) selectedCampus = ''; return selectedCampus;} set;}
	/*public list<selectOption> getlistCampuses(){
		list<selectOption> options = new list<selectOption>();
		options.add(new selectOption('', 'Select Campus'));
		options.add(new selectOption('0012000000eA8w6', 'APC'));
		localCampusId.put('0012000000eA8w6', '0012000000eA8w6');
		for(Account acc: [Select id, remoteId__c, name from Account where recordType.name = 'campus']){
			options.add(new selectOption(acc.remoteId__c, acc.name));
			localCampusId.put(acc.remoteId__c, acc.id);
		}
		return options;
	}*/
	
	public class groupCourses{
	  public string name {get;set;}
	  public integer totalQuotes {get;set;}
	  public list<courseDetails> listCourses{get{if(listCourses == null) listCourses = new list<courseDetails>(); return listCourses;} set;}
	  public groupCourses(string name, integer totalQuotes, list<courseDetails> listCourses){
	  	this.name = name;
	  	this.totalQuotes = totalQuotes;
	  	this.listCourses = listCourses;
	  }
	}
	
	public class courseDetails{
	  public string Course {get;set;}
	  public integer Total {get;set;}
	  public courseDetails(string Course, integer total){
	  	this.Course = course;
	  	this.total = total;
	  }
	}
	
	public map<string,string> campusName {get; set;}
	
	private list<Supplier__c> Campuses;
	 public list<Supplier__c> getCampuses(){
	 	if(Campuses == null && userAgency != null){
	      	Account acc = [Select id, parentId, remoteId__c from Account where id = :userAgency limit 1];       
	        selectedCampus = acc.remoteId__c;
	        return [Select Supplier__c, Supplier__r.name, Supplier__r.remoteId__c from Supplier__c WHERE Agency__c = :userAgency and Record_Type__c = 'campus' and Supplier__r.ParentId = :acc.parentId order by Supplier__r.name];
	 	}
	 	return Campuses;   
    }
	
	private List<SelectOption> ListCampuses;
	public List<SelectOption> getListCampuses() {
		if(ListCampuses == null){
			ListCampuses = new List<SelectOption>();
			campusName = new map<string, string>();
			for (Supplier__c lc: getCampuses()){
				ListCampuses.add(new SelectOption(lc.Supplier__r.remoteId__c, lc.Supplier__r.name));
				localCampusId.put(lc.Supplier__r.remoteId__c, lc.Supplier__c);
				campusName.put(lc.Supplier__r.remoteId__c, lc.Supplier__r.name);
			}
		}
		return ListCampuses;
	}

}