@isTest
private class commissions_send_school_invoice_test {

	public static Account school {get;set;}
	public static Account agency {get;set;}
	public static Contact emp {get;set;}
	public static Contact client {get;set;}
	public static User portalUser {get{
		if (null == portalUser) {
			portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
		}
		return portalUser;
	}set;}
	public static Account campus {get;set;}
	public static Course__c course {get;set;}
	public static Campus_Course__c campusCourse {get;set;}
	public static client_course__c clientCourseBooking {get;set;}
	public static client_course__c clientCourse {get;set;}
	public static client_course__c clientCourse2 {get;set;}


	@testSetup static void setup() {
		TestFactory tf = new TestFactory();

		Account sg = tf.createSchoolGroup();
		school = tf.createSchool();
		school.ParentId = sg.Id;
		update school;

		agency = tf.createAgency();
		emp = tf.createEmployee(agency);
		portalUser = tf.createPortalUser(emp);
		campus = tf.createCampus(school, agency);
		course = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);

		Back_Office_Control__c bo = new Back_Office_Control__c(Agency__c = agency.id, Country__c = 'Australia', Backoffice__c = agency.id, Services__c = 'PDS_PCS_PFS_Chase');
		insert bo;

		system.runAs(portalUser){
			client = tf.createClient(agency);
			clientCourseBooking = tf.createBooking(client);
			clientCourse = tf.createClientCourse(client, school, campus, course, campusCourse, clientCourseBooking);
			tf.createClientCourseFees(clientCourse, false);
			List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(clientCourse);

			//TO REQUEST PDS
			instalments[0].Received_By_Agency__c = agency.id;
			instalments[0].Received_Date__c =  system.today();
			instalments[0].Paid_To_School_On__c =  system.today();
			instalments[0].Paid_to_School_By_Agency__c =  agency.id;
			instalments[0].isPFS__c = true;

			instalments[1].Received_By_Agency__c = agency.id;
			instalments[1].Received_Date__c =  system.today();
			instalments[1].isPCS__c = true;

			instalments[2].Received_By_Agency__c = agency.id;
			instalments[2].Received_Date__c =  system.today();
			instalments[2].isPDS__c = true;
			instalments[2].PDS_Confirmed__c = true;

			update instalments;
		}
	}

	static testMethod void noCommissionClaim(){
		Test.startTest();
		system.runAs(portalUser){
			commissions_send_school_invoice toTest = new commissions_send_school_invoice();
			toTest.selectedCountry = 'Australia';
			toTest.selectedSchoolGP = 'all';
			toTest.selectedSchool = 'all';
			toTest.dates.Commission_Paid_Date__c = system.today().addYears(1);
			toTest.searchName = '';
			toTest.findInstalments();

			String instalmentId = toTest.result[0].campuses[0].instalments[0].installment.id;
			ApexPages.currentPage().getParameters().put('cancelId', instalmentId);
			toTest.setIdCancel();
			toTest.noCommissionClaim();

			client_course_instalment__c inst = [SELECT Id, Commission_Not_Claimable__c FROM client_course_instalment__c WHERE id = :instalmentId limit 1];
			system.assertEquals(inst.Commission_Not_Claimable__c, true);
		}
		Test.stopTest();
	}

	static testMethod void requestPDS(){
		Test.startTest();
		system.runAs(portalUser){
			commissions_send_school_invoice toTest = new commissions_send_school_invoice();
			toTest.selectedCountry = 'Australia';
			toTest.selectedSchoolGP = 'all';
			toTest.selectedSchool = 'all';
			toTest.searchName = '';
			toTest.dates.Commission_Paid_Date__c = system.today().addYears(1);
			list<SelectOption> dateFilterBy = toTest.dateFilterBy;
			toTest.findInstalments();

			String instId = toTest.result[0].campuses[0].instalments[0].installment.id;
			toTest.result[0].campuses[0].instalments[0].installment.isSelected__c = true;
			ApexPages.currentPage().getParameters().put('schId', string.valueOf(toTest.result[0].schoolId));
			toTest.setToRequest();
			User userFields = [SELECT Id, Contact.PDS_to_Request__c FROM  User WHERE id = :portalUser.id limit 1];
			system.assertEquals(userFields.Contact.PDS_to_Request__c, instId);
			
			ApexPages.currentPage().getParameters().put('schId', string.valueOf(toTest.result[0].schoolId));
			toTest.invoiceAll();
		}
		Test.stopTest();
	}

	static testMethod void cancelPayment(){
		Test.startTest();
		system.runAs(portalUser){
			commissions_send_school_invoice toTest = new commissions_send_school_invoice();
			toTest.selectedCountry = 'Australia';
			toTest.selectedSchoolGP = 'all';
			toTest.selectedSchool = 'all';
			toTest.searchName = '';
			toTest.dates.Commission_Paid_Date__c = system.today().addYears(1);
			toTest.findInstalments();

			String instId = toTest.result[0].campuses[0].instalments[0].installment.id;
			ApexPages.currentPage().getParameters().put('cancelId', instId);
			toTest.setIdCancel();
			toTest.cancelInst.Cancel_Payment_Reason__c = 'Test';
			toTest.cancelReason = 'Cancel Instalment Payment Test';
			toTest.cancelPayment();

			client_course_instalment__c cancelled = [Select Id, Received_Date__c from client_course_instalment__c Where id = :instId];
			system.assertEquals(cancelled.Received_Date__c, null);

		}
		Test.stopTest();
	}



	static testMethod void testFilters(){
		Test.startTest();
		system.runAs(portalUser){
			commissions_send_school_invoice toTest = new commissions_send_school_invoice();

			totest.changeBackOffice();
			totest.changeCountry();
			totest.changeSchoolGP();
			totest.changeSchool();
			list<SelectOption> paymentOptions  = totest.paymentOptions;
			
		}
	}


}