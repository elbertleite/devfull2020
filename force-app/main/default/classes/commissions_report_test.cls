/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class commissions_report_test {

    static testMethod void myUnitTest() {
		
		TestFactory tf = new TestFactory();	
		Account school = tf.createSchool();
		Account agency = tf.createAgency();
		Account campus = tf.createCampus(school, agency);
		Course__c course = tf.createCourse();
		Campus_Course__c campusCourse = tf.createCampusCourse(campus,course);
		
		Commission__c commission = tf.createCommission(school);
		
		Commission__c commission2 = commission;
		commission2.id = null;
		commission2.School_Id__c = null;
		commission2.Campus_Id__c = campus.id;
		insert commission2;
		
		Commission__c commission3 = commission;
		commission3.id = null;
		commission3.School_Id__c = null;
		commission3.Campus_Id__c = null;
		commission3.CampusCourseTypeId__c = 'test';
		insert commission3;
		
		Commission__c commission4 = commission;
		commission4.id = null;
		commission4.School_Id__c = null;
		commission4.Campus_Id__c = null;
		commission4.CampusCourseTypeId__c = null;
		commission4.Rules__c = 'Test';
		commission4.Campus_Course__c = campusCourse.id;
		insert commission4;
		
		
		commissions_report testClass = new commissions_report();
		
		testClass.getlistCountries();
		testClass.selectedCountry = 'Australia';
		testClass.retrievelistSchools();
		
		testClass.selectedSchool = school.id;
		testClass.retrieveCommissions();
		
		
    }
}