public class xOptionalFees{
	
	private string campusCourseId;
	private string schoolId;
	private string Nationality;
	private string nationalityGroupShool;
	private String cartid;
	public list<string> listOrderedCourses{get{if(listOrderedCourses == null) listOrderedCourses = new list<string>(); return listOrderedCourses;} set;}
	
	public xOptionalFees(ApexPages.StandardController controller){
		campusCourseId = controller.getId();
		setParams();
		
	}
	
	
	private Web_Search__c objCart;
	
	public xOptionalFees(){
		setParams();
	}
	
	private void setParams(){
		schoolId = ApexPages.currentPage().getParameters().get('sch');
		Nationality = ApexPages.currentPage().getParameters().get('nation');
		cartid =  ApexPages.currentPage().getParameters().get('cartid');
		objCart = [Select id, visits__c from Web_Search__c where id = :cartid limit 1];
		listCourses = [Select id, Campus_Course__r.Campus__r.ParentId from Search_Courses__c where Web_Search__c = :objCart.id and Course_Deleted__c = false and isCustomCourse__c = false ORDER BY Course_Order__c NULLS LAST, CreatedDate];
		if(schoolId != null)
			nationalityGroupShool = [Select Name from Nationality_Group__c where Country__c = :Nationality and Account__c = :schoolId].Name;
		
		for(Search_Courses__c sc:listCourses ){
			listOrderedCourses.add(sc.id);
		}

	}

	private map<string,list<Course_Extra_Fee__c>> fees;
	public map<string,list<Course_Extra_Fee__c>> getoptionalFees(){
		if(fees == null){
			fees = new map<string,list<Course_Extra_Fee__c>>();
			
			//================== Naionality Group  =======================================
			for(Course_Extra_Fee__c cf:[Select Allow_change_units__c, Availability__c, Product__c, Product__r.name__c, Extra_Fee_Country__c, ExtraFeeInterval__c, Product__r.Product_Type__c, Extra_info__c, From__c, 
			LastModifiedDate, Nationality__c, Value__c, selectedFee__c from Course_Extra_Fee__c where Campus_Course__c = :campusCourseId and optional__c = true and  Nationality__c = :nationalityGroupShool order by Product__r.Product_Type__c, From__c])
			if(!fees.containsKey(cf.Product__r.Product_Type__c))
				fees.put(cf.Product__r.Product_Type__c, new list<Course_Extra_Fee__c>{cf});
			else fees.get(cf.Product__r.Product_Type__c).add(cf);
				
			//================== Published Price  =======================================
			for(Course_Extra_Fee__c cf:[Select Allow_change_units__c, Availability__c, Product__c, Product__r.name__c, Extra_Fee_Country__c, ExtraFeeInterval__c, Product__r.Product_Type__c, Extra_info__c, From__c, 
			LastModifiedDate, Nationality__c, Value__c, selectedFee__c from Course_Extra_Fee__c where Campus_Course__c = :campusCourseId and optional__c = true and Nationality__c = 'Published Price' 	order by Product__r.Product_Type__c, From__c])
			if(!fees.containsKey(cf.Product__r.Product_Type__c))
				fees.put(cf.Product__r.Product_Type__c, new list<Course_Extra_Fee__c>{cf});
			else //if(!checkFeeNameAdded(cf.Product__r.name__c, fees.get(cf.Product__r.Product_Type__c))) 
				fees.get(cf.Product__r.Product_Type__c).add(cf);
		}
		return fees;	
	}
	
	
	public void addOptionalFees(){
		/*list<Search_Course_Extra_Fee__c> listFees = new list<Search_Course_Extra_Fee__c>();
		Search_Course_Extra_Fee__c newFee;
		string courseId = [Select id from Search_Courses__c where Web_Search__c = :objCart.id and Campus_Course__c = :campusCourseId limit 1].id;
		for(String mf:fees.keySet()){
			for(Course_Extra_Fee__c cef: fees.get(mf)){
				if(cef.selectedFee__c){
					newFee = new Search_Course_Extra_Fee__c();
					newFee.Search_Course__c = courseId;
					newFee.Course_Extra_Fee__c = cef.id;
					newFee.Fee_Value__c = cef.Value__c;
					listFees.add(newFee);
				}
			}
		}
		insert listFees;*/
	}
	
	private boolean checkFeeNameAdded(String feeName, List<extraFeeDetails> listFees){
		/*for(Course_Extra_Fee__c cef:listFees)
			if(cef.Product__r.name__c == feeName)
				return true;*/
		
		for(integer i = listFees.size()-1; i >=0; i--)
			if(listFees.get(i).extraFee.Product__r.name__c == feeName){
				listFees.remove(i);
				posFee = i;
				break;
			}
		/*if(posFee == -1)
			posFee = 0;
		else if(posFee >= listFees.size())
			posFee = listFees.size()-1;*/
				
		return false;
	}
	
	private list<Search_Courses__c> listCourses;
	
	private Set<id> listSchools(){
		Set<id> lstSchool = new Set<id>();
		for(Search_Courses__c sc:listCourses ){
			lstSchool.add(sc.Campus_Course__r.Campus__r.ParentId);
		}
		return lstSchool;
	}
	
	Map<String,String> schoolClientNationality;
	private Map<String,String> getSchoolClientNationality(){
		if(schoolClientNationality == null){
			schoolClientNationality = new Map<String,String>();
			for(Nationality_Group__c ng:[Select Account__c, Country__c, Name from Nationality_Group__c N WHERE Country__c = :Nationality and Account__c in :listSchools()])
				if(!schoolClientNationality.containsKey(ng.Account__c)){
					schoolClientNationality.put(ng.Account__c, ng.Name);
				}
		}
		return schoolClientNationality;
	}
	
	public Map<String, Search_Courses__c> mapCampusName{get; set;}
	
	private Map<String, string> mapCourseId;
	private Map<String, Search_Courses__c> mapCampusCourseDetails;
	private Map<String, string> mapCampusId;
	private Set<String> listCampusCourseIds;
	private Set<String> listCampusIds;
	private Map<String, string> listCourseCartIds;
	private Set<String> getlistCampusCourseIds(){
		if(listCampusCourseIds == null){
			listCampusCourseIds = new Set<String>();
			listCampusIds = new Set<String>();
			listCourseCartIds = new Map<String, string>();
			
			mapCourseId = new Map<String, string>();
			mapCampusId = new Map<String, string>();
			mapCampusName = new Map<String, Search_Courses__c>();
			mapCampusCourseDetails = new Map<String, Search_Courses__c>();
			for(Search_Courses__c sc: [Select id, Campus_Course__c, Campus_Course__r.campus__c, Campus_Course__r.Campus__r.Name, Campus_Course__r.Campus__r.ParentId, Campus_Course__r.Course__r.Name, Start_Date__c, Payment_Date__c, Number_of_Units__c, Unit_Type__c, Free_Units__c from Search_Courses__c where Web_Search__c = :objCart.id and Course_Deleted__c = false and isCustomCourse__c = false ORDER BY Course_Order__c NULLS LAST, CreatedDate]){
				listCampusCourseIds.add(sc.Campus_Course__c);
				listCampusIds.add(sc.Campus_Course__r.campus__c);
				listCourseCartIds.put(sc.id, sc.Campus_Course__c);
				
				mapCourseId.put(sc.Campus_Course__c, sc.id);
				mapCampusCourseDetails.put(sc.Campus_Course__c, sc);
				mapCampusId.put(sc.Campus_Course__c, sc.Campus_Course__r.campus__c);
				//mapCampusName.put(sc.id, '<span style="color: red; font-weight:bold;">'+sc.Campus_Course__r.Campus__r.Name + ' - '+sc.Number_of_Units__c + '</span><br>' + sc.Campus_Course__r.Course__r.Name);
				mapCampusName.put(sc.id, sc);
			}
		}
		return listCampusCourseIds;
		
	}
	
	
	private Map<String, set<id>> selectedFeesPerCourse;
	private Map<String, map<string, integer>> selectedFeesNumUnits;
	
	private Map<String, set<id>> getselectedFeesPerCourse(){
		if(selectedFeesPerCourse == null){
			selectedFeesPerCourse = new Map<String, set<id>>();
			selectedFeesNumUnits = new Map<String, map<string, integer>>();
			list<string> listOptionalDetail;
			list<string> listRelatedOptionalDetail;
			List<String> listDeletedRequired;
			for(Search_Courses__c cs:[Select id, Campus_Course__c, Optional_Fee_Ids__c, Optional_Fee_Related_Ids__c, Required_Related_Fee_Deleted__c from Search_Courses__c WHERE id in :listCourseCartIds.keySet()  and Course_Deleted__c = false and isCustomCourse__c = false ORDER BY Course_Order__c NULLS LAST, CreatedDate]){
				
				if(cs.Optional_Fee_Ids__c != null)
					for(String ef:cs.Optional_Fee_Ids__c.split(',')){
						listOptionalDetail = new list<string>(ef.split(':'));
						if(!selectedFeesNumUnits.containsKey(cs.id))
							selectedFeesNumUnits.put(cs.id, new map<string, integer>{listOptionalDetail[0] => integer.valueOf(listOptionalDetail[1])});
						else selectedFeesNumUnits.get(cs.id).put(listOptionalDetail[0],integer.valueOf(listOptionalDetail[1]));
						
						
						if(!selectedFeesPerCourse.containsKey(cs.id))
							selectedFeesPerCourse.put(cs.id, new Set<id>{listOptionalDetail[0]});
						else selectedFeesPerCourse.get(cs.id).add(listOptionalDetail[0]);
					}
					
				if(cs.Optional_Fee_Related_Ids__c != null)
					for(String ref:cs.Optional_Fee_Related_Ids__c.split(',')){
						listRelatedOptionalDetail = new list<string>(ref.split(':'));
						if(!selectedFeesNumUnits.containsKey(cs.id))
							selectedFeesNumUnits.put(cs.id, new map<string, integer>{listRelatedOptionalDetail[1] => integer.valueOf(listRelatedOptionalDetail[2])});
						else selectedFeesNumUnits.get(cs.id).put(listRelatedOptionalDetail[1],integer.valueOf(listRelatedOptionalDetail[2]));
							
						if(!selectedFeesPerCourse.containsKey(cs.id))
							selectedFeesPerCourse.put(cs.id, new Set<id>{listRelatedOptionalDetail[1]});
						else selectedFeesPerCourse.get(cs.id).add(listRelatedOptionalDetail[1]);
					}
					
				/*if(cs.Required_Related_Fee_Deleted__c != null)
					for(String str : cs.Required_Related_Fee_Deleted__c.split(',')){
						listDeletedRequired = new List<String>(str.split(':'));
						if(!selectedFeesPerCourse.containsKey(cs.id))
							selectedFeesPerCourse.put(cs.id, new Set<id>{listDeletedRequired[1]});
						else selectedFeesPerCourse.get(cs.id).add(listDeletedRequired[1]);
					}*/

			}
		}
		return selectedFeesPerCourse;
		
	}
	
	

	private map<string,Map<String,List<Course_Extra_Fee__c>>> allCourses;
	
	private map<String,Map<String,List<Course_Extra_Fee__c>>> FeesAllCourses;
	private  map<string,Map<String,List<Course_Extra_Fee__c>>> getFeesAllCourses(){
		if(FeesAllCourses == null){
			FeesAllCourses = new map<String,Map<String,List<Course_Extra_Fee__c>>>();
			//allCourses = new map<String,Map<String,List<Course_Extra_Fee__c>>>();
			map<string,list<Course_Extra_Fee__c>> listFees;
			getSchoolClientNationality();
			getlistCampusCourseIds();
			getselectedFeesPerCourse();
			System.debug('==>listCampusCourseIds: '+listCampusCourseIds);
			System.debug('==>schoolClientNationality.values(): '+schoolClientNationality.values());
			System.debug('==>listCampusIds: '+listCampusIds);
			//Initialize FeesAllCourses with all campuses courses
			
			for(String cc:listCampusCourseIds)
				if(!FeesAllCourses.containsKey(cc)){
					FeesAllCourses.put(cc,new map<string,list<Course_Extra_Fee__c>>());
				}
			//================== Naionality Group  =======================================
			for(Course_Extra_Fee__c cf:[Select Allow_change_units__c, Availability__c, Product__c, Product__r.name__c, Product__r.Product_Type__c, date_paid_from__c, date_paid_to__c, Extra_Fee_Country__c, ExtraFeeInterval__c, Extra_info__c, From__c, Campus_Course__c, Campus__c, Campus_Course__r.Campus__r.ParentId, 
				LastModifiedDate, Nationality__c, Value__c, selectedFee__c,
				( Select Product__r.name__c, Allow_Change_Units__c, Course_Extra_Fee__c, Date_Paid_From__c, Date_Paid_To__c, Date_Start_From__c, Date_Start_To__c, Details__c, Extra_Fee_Interval__c, From__c, Keep_Fee__c, NumberUnitsSelected__c, Optional__c, Product__c, Selected__c, TotalFee__c, Value__c from Course_Extra_Fees_Dependent__r  order by Product__r.name__c, From__c)
				from Course_Extra_Fee__c where Campus_Course__c in :listCampusCourseIds and optional__c = true and Nationality__c in :schoolClientNationality.values()	order by Product__r.Product_Type__c, Product__r.name__c, From__c]){
				if(schoolClientNationality.get(cf.Campus_Course__r.Campus__r.ParentId) == cf.Nationality__c){
					if(cf.Date_Paid_From__c <= mapCampusCourseDetails.get(cf.Campus_Course__c).Payment_Date__c && cf.Date_Paid_To__c >= mapCampusCourseDetails.get(cf.Campus_Course__c).Payment_Date__c  /*&& cf.From__c <= mapCampusCourseDetails.get(cf.Campus_Course__c).Number_of_Units__c*/){
						if(!FeesAllCourses.containsKey(cf.Campus_Course__c)){
							listFees = new map<string,list<Course_Extra_Fee__c>>();
							listFees.put(cf.Product__r.Product_Type__c, new list<Course_Extra_Fee__c>{cf.clone(true,true)});
							FeesAllCourses.put(cf.Campus_Course__c,listFees.clone());
						}else {
							if(!FeesAllCourses.get(cf.Campus_Course__c).containsKey(cf.Product__r.Product_Type__c)){
								FeesAllCourses.get(cf.Campus_Course__c).put(cf.Product__r.Product_Type__c, new list<Course_Extra_Fee__c>{cf});
							}else FeesAllCourses.get(cf.Campus_Course__c).get(cf.Product__r.Product_Type__c).add(cf.clone(true,true));
						}
					}
				}
			}
				
			for(Course_Extra_Fee__c cf:[Select Allow_change_units__c, Availability__c, Product__c, Product__r.name__c, Product__r.Product_Type__c, date_paid_from__c, date_paid_to__c, Extra_Fee_Country__c, ExtraFeeInterval__c, Extra_info__c, From__c, Campus_Course__c, Campus__c, Campus__r.ParentId, 
				LastModifiedDate, Nationality__c, Value__c, selectedFee__c,
				( Select Product__r.name__c, Allow_Change_Units__c, Course_Extra_Fee__c, Date_Paid_From__c, Date_Paid_To__c, Date_Start_From__c, Date_Start_To__c, Details__c, Extra_Fee_Interval__c, From__c, Keep_Fee__c, NumberUnitsSelected__c, Optional__c, Product__c, Selected__c, TotalFee__c, Value__c from Course_Extra_Fees_Dependent__r  order by Product__r.name__c, From__c)
				from Course_Extra_Fee__c where campus__c in :listCampusIds and optional__c = true and Nationality__c in :schoolClientNationality.values() order by Product__r.Product_Type__c, Product__r.name__c, From__c]){
				if(schoolClientNationality.get(cf.Campus__r.ParentId) == cf.Nationality__c){
					for(String cc:listCampusCourseIds)
						if(mapCampusId.get(cc) == cf.campus__c){
							if(cf.Date_Paid_From__c <= mapCampusCourseDetails.get(cc).Payment_Date__c && cf.Date_Paid_To__c >= mapCampusCourseDetails.get(cc).Payment_Date__c /*&& cf.From__c <= mapCampusCourseDetails.get(cc).Number_of_Units__c*/){
								if(!FeesAllCourses.get(cc).containsKey(cf.Product__r.Product_Type__c)){
									FeesAllCourses.get(cc).put(cf.Product__r.Product_Type__c, new list<Course_Extra_Fee__c>{cf.clone(true,true)});
								}else FeesAllCourses.get(cc).get(cf.Product__r.Product_Type__c).add(cf.clone(true,true));
							}
						}
				}
			}
				
			//================== Published Price  =======================================	
			for(Course_Extra_Fee__c cf:[Select Allow_change_units__c, Availability__c, Product__c, Product__r.name__c, Product__r.Product_Type__c, date_paid_from__c, date_paid_to__c, Extra_Fee_Country__c, ExtraFeeInterval__c, Extra_info__c, From__c, Campus_Course__c, Campus__c, Campus_Course__r.Campus__r.ParentId, 
				LastModifiedDate, Nationality__c, Value__c, selectedFee__c, 
				( Select Product__r.name__c, Allow_Change_Units__c, Course_Extra_Fee__c, Date_Paid_From__c, Date_Paid_To__c, Date_Start_From__c, Date_Start_To__c, Details__c, Extra_Fee_Interval__c, From__c, Keep_Fee__c, NumberUnitsSelected__c, Optional__c, Product__c, Selected__c, TotalFee__c, Value__c from Course_Extra_Fees_Dependent__r  order by Product__r.name__c, From__c)
				from Course_Extra_Fee__c where Campus_Course__c in :listCampusCourseIds and optional__c = true and Nationality__c = 'Published Price' order by Product__r.Product_Type__c, Product__r.name__c, From__c]){
					if(cf.Date_Paid_From__c <= mapCampusCourseDetails.get(cf.Campus_Course__c).Payment_Date__c && cf.Date_Paid_To__c >= mapCampusCourseDetails.get(cf.Campus_Course__c).Payment_Date__c /* && cf.From__c <= mapCampusCourseDetails.get(cf.Campus_Course__c).Number_of_Units__c*/){
						if(!FeesAllCourses.containsKey(cf.Campus_Course__c)){
							listFees = new map<string,list<Course_Extra_Fee__c>>();
							listFees.put(cf.Product__r.Product_Type__c, new list<Course_Extra_Fee__c>{cf.clone(true,true)});
							FeesAllCourses.put(cf.Campus_Course__c,listFees.clone());
						}
						else{ 
							if(!FeesAllCourses.get(cf.Campus_Course__c).containsKey(cf.Product__r.Product_Type__c)){
								FeesAllCourses.get(cf.Campus_Course__c).put(cf.Product__r.Product_Type__c, new list<Course_Extra_Fee__c>{cf.clone(true,true)});
							}else //if(!checkFeeNameAdded(cf.Product__r.name__c, FeesAllCourses.get(cf.Campus_Course__c).get(cf.Product__r.Product_Type__c)))
								FeesAllCourses.get(cf.Campus_Course__c).get(cf.Product__r.Product_Type__c).add(cf.clone(true,true));
						}
					}
			}
				
			for(Course_Extra_Fee__c cf:[Select Allow_change_units__c, Availability__c, Product__c, Product__r.name__c, Product__r.Product_Type__c, date_paid_from__c, date_paid_to__c, Extra_Fee_Country__c, ExtraFeeInterval__c, Extra_info__c, From__c, Campus_Course__c, Campus__c, Campus_Course__r.Campus__r.ParentId, 
				LastModifiedDate, Nationality__c, Value__c, selectedFee__c, 
				( Select Product__r.name__c, Allow_Change_Units__c, Course_Extra_Fee__c, Date_Paid_From__c, Date_Paid_To__c, Date_Start_From__c, Date_Start_To__c, Details__c, Extra_Fee_Interval__c, From__c, Keep_Fee__c, NumberUnitsSelected__c, Optional__c, Product__c, Selected__c, TotalFee__c, Value__c from Course_Extra_Fees_Dependent__r order by Product__r.name__c, From__c)
				from Course_Extra_Fee__c where campus__c in :listCampusIds and optional__c = true and Nationality__c = 'Published Price' order by Product__r.Product_Type__c, Product__r.name__c, From__c]){
				for(String cc:listCampusCourseIds)
					if(mapCampusId.get(cc) == cf.campus__c){
						if(cf.Date_Paid_From__c <= mapCampusCourseDetails.get(cc).Payment_Date__c && cf.Date_Paid_To__c >= mapCampusCourseDetails.get(cc).Payment_Date__c  /*&& cf.From__c <= mapCampusCourseDetails.get(cc).Number_of_Units__c*/){
							if(!FeesAllCourses.get(cc).containsKey(cf.Product__r.Product_Type__c)){
								FeesAllCourses.get(cc).put(cf.Product__r.Product_Type__c, new list<Course_Extra_Fee__c>{cf.clone(true,true)});
							}else //if(!checkFeeNameAdded(cf.Product__r.name__c, FeesAllCourses.get(cc).get(cf.Product__r.Product_Type__c)))
								FeesAllCourses.get(cc).get(cf.Product__r.Product_Type__c).add(cf.clone(true,true));
						}
					}
			}
		}
		
		/*for(String scourse:listCourseCartIds.keySet()){
			for(String feeType:FeesAllCourses.get(listCourseCartIds.get(scourse)).keySet())
				for(Course_Extra_Fee__c courseFee:FeesAllCourses.get(listCourseCartIds.get(scourse)).get(feeType)){
					if(selectedFeesPerCourse.containsKey(scourse)){
						courseFee.selectedFee__c = selectedFeesPerCourse.get(scourse).contains(courseFee.id);	
						for(Course_Extra_Fee_Dependent__c cef:courseFee.Course_Extra_Fees_Dependent__r)
							cef.Selected__c = selectedFeesPerCourse.get(scourse).contains(cef.id);	
					}
					if(selectedFeesNumUnits.containsKey(scourse))
						if(selectedFeesNumUnits.get(scourse).containsKey(courseFee.id))
							courseFee.From__c = selectedFeesNumUnits.get(scourse).get(courseFee.id);
				}
			
			if(!allCourses.containsKey(scourse)){
				allCourses.put(scourse,FeesAllCourses.get(listCourseCartIds.get(scourse)).clone());
			}
		}
		return allCourses;*/

		return FeesAllCourses;
	}
	
	public class extraFeeDetails{
		public Course_Extra_Fee__c extraFee{get; set;}
		public list<Course_Extra_Fee_Dependent__c> relatedFee{get{if(relatedFee == null) relatedFee = new list<Course_Extra_Fee_Dependent__c>(); return relatedFee;} set;} 
	}
	
	public  map<string,Map<String,List<extraFeeDetails>>> FeesPerCampusCourse;
	public  map<string,Map<String,List<extraFeeDetails>>> getFeesPerCampusCourse(){
		if(FeesPerCampusCourse == null){
			set<string> nationalityFees = new set<string>();
			getFeesAllCourses();
			extraFeeDetails courseFee;
			Course_Extra_Fee_Dependent__c dependentFee;
			Double searchUnitsValue;
			Double searchRelatedUnitsValue;
			FeesPerCampusCourse = new map<String,Map<String,List<extraFeeDetails>>>();
			for(String scourse:listCourseCartIds.keySet()){
				if(!FeesPerCampusCourse.containsKey(scourse))
					FeesPerCampusCourse.put(scourse,new map<string,list<extraFeeDetails>>());
				for(String feeType:FeesAllCourses.get(listCourseCartIds.get(scourse)).keySet()){
					nationalityFees = new set<string>();
					for(Course_Extra_Fee__c thecourseFee:FeesAllCourses.get(listCourseCartIds.get(scourse)).get(feeType)){
						courseFee = new extraFeeDetails();
						courseFee.extraFee = thecourseFee.clone(true, true);
						System.debug('==>scourse: '+scourse + ' -courseFee.extraFee.id: '+courseFee.extraFee.id +' - selectedFeesNumUnits: '+selectedFeesNumUnits);
						if(selectedFeesNumUnits.containsKey(scourse) && selectedFeesNumUnits.get(scourse).containsKey(courseFee.extraFee.id)){
							courseFee.extraFee.From__c = selectedFeesNumUnits.get(scourse).get(courseFee.extraFee.id);
							searchUnitsValue = courseFee.extraFee.From__c;
						}else if(courseFee.extraFee.Allow_change_units__c)
							searchUnitsValue = courseFee.extraFee.from__c;//1;
						else searchUnitsValue = mapCampusName.get(scourse).Number_of_Units__c;
						
						//if(thecourseFee.from__c >= searchUnitsValue){//right one
						if(courseFee.extraFee.from__c <= searchUnitsValue && (schoolClientNationality.get(courseFee.extraFee.campus__r.ParentId) == courseFee.extraFee.Nationality__c || schoolClientNationality.get(courseFee.extraFee.campus_course__r.campus__r.ParentId) == courseFee.extraFee.Nationality__c || (courseFee.extraFee.Nationality__c == 'Published Price' && !nationalityFees.contains(courseFee.extraFee.Product__c)))) {
							if(schoolClientNationality.get(courseFee.extraFee.campus__r.ParentId) == courseFee.extraFee.Nationality__c || schoolClientNationality.get(courseFee.extraFee.campus_course__r.campus__r.ParentId) == courseFee.extraFee.Nationality__c)
								nationalityFees.add(courseFee.extraFee.Product__c);
							if(selectedFeesPerCourse.containsKey(scourse)){
								courseFee.extraFee.selectedFee__c = selectedFeesPerCourse.get(scourse).contains(courseFee.extraFee.id);
								
							}
							searchRelatedUnitsValue = courseFee.extraFee.from__c;
							for(Course_Extra_Fee_Dependent__c cef:courseFee.extraFee.Course_Extra_Fees_Dependent__r){
								Course_Extra_Fee_Dependent__c theCef = cef.clone(true, true);
								if(selectedFeesPerCourse.containsKey(scourse) && selectedFeesPerCourse.get(scourse).contains(theCef.id) && selectedFeesNumUnits.containsKey(scourse) && selectedFeesNumUnits.get(scourse).containsKey(courseFee.extraFee.id)){
									theCef.From__c = selectedFeesNumUnits.get(scourse).get(theCef.id);
									searchRelatedUnitsValue = theCef.From__c;
								}else if(theCef.Allow_change_units__c)
									searchRelatedUnitsValue = courseFee.extraFee.from__c; //1;
								else searchRelatedUnitsValue = courseFee.extraFee.from__c;

								if(theCef.Date_Paid_From__c <= mapCampusCourseDetails.get(listCourseCartIds.get(scourse)).Payment_Date__c && theCef.Date_Paid_To__c >= mapCampusCourseDetails.get(listCourseCartIds.get(scourse)).Payment_Date__c && theCef.from__c <= searchRelatedUnitsValue){
									if(selectedFeesPerCourse.containsKey(scourse)){
										theCef.Selected__c = selectedFeesPerCourse.get(scourse).contains(theCef.id);
									}
									
									removeAddedRelatedFee(theCef.Product__c, courseFee.relatedFee);
									courseFee.relatedFee.add(theCef);
									
								}
							}
							
							/*if(selectedFeesNumUnits.containsKey(scourse))
								if(selectedFeesNumUnits.get(scourse).containsKey(courseFee.extraFee.id))
									courseFee.extraFee.From__c = selectedFeesNumUnits.get(scourse).get(courseFee.extraFee.id);*/
						
							if(!FeesPerCampusCourse.get(scourse).containsKey(courseFee.extraFee.Product__r.Product_Type__c)){
								FeesPerCampusCourse.get(scourse).put(courseFee.extraFee.Product__r.Product_Type__c, new list<extraFeeDetails>{courseFee});
							}else if(!checkFeeNameAdded(courseFee.extraFee.Product__r.name__c, FeesPerCampusCourse.get(scourse).get(courseFee.extraFee.Product__r.Product_Type__c)))
								FeesPerCampusCourse.get(scourse).get(courseFee.extraFee.Product__r.Product_Type__c).add(courseFee);

						}
						
					}
				}
			/*if(!FeesPerCampusCourse.containsKey(scourse)){
				FeesPerCampusCourse.put(scourse,FeesAllCourses.get(listCourseCartIds.get(scourse)).clone());
			}*/
			}
		}
		return FeesPerCampusCourse;
	}
	
	Integer posFee = 0;
	public pageReference updateAllowChangeExtraFee(){
		/*String campusCourseId = ApexPages.currentPage().getParameters().get('selCampusCourse');	
		String selFeeType = ApexPages.currentPage().getParameters().get('selFeeType');
		String selProduct = ApexPages.currentPage().getParameters().get('selProduct');
		String selNumberUnits = ApexPages.currentPage().getParameters().get('selNumberUnits');*/
		
		String selectedNumUnits = ApexPages.currentPage().getParameters().get('paramNumUnits');
		String selectedRelatedNumUnits = ApexPages.currentPage().getParameters().get('paramRelatedNumUnits');
		String paramCart = ApexPages.currentPage().getParameters().get('paramCartId');
		String productId = ApexPages.currentPage().getParameters().get('paramProductId');
		String selFeeType = ApexPages.currentPage().getParameters().get('selFeeType');
		map<string, decimal> mapRelatedProductsAdded  = new map<string, decimal>();
		
		extraFeeDetails courseFee;
		/*Map<String, double> productsAdded = new Map<String, double>();
		
		set<string> nationalityFees = new set<string>();
		extraFeeDetails courseFee;
		for(Course_Extra_Fee__c thecourseFee:FeesAllCourses.get(listCourseCartIds.get(campusCourseId)).get(selFeeType)){
			//nationalityFees = new set<string>();
			
			courseFee = new extraFeeDetails();
			courseFee.extraFee = thecourseFee.clone(true, true);
			productsAdded.put(courseFee.extraFee.Product__c, courseFee.extraFee.from__c);
			if(courseFee.extraFee.Product__c == selProduct && courseFee.extraFee.from__c <= Double.valueOf(selNumberUnits) && (schoolClientNationality.get(courseFee.extraFee.campus__r.ParentId) == courseFee.extraFee.Nationality__c || schoolClientNationality.get(courseFee.extraFee.campus_course__r.campus__r.ParentId) == courseFee.extraFee.Nationality__c || (courseFee.extraFee.Nationality__c == 'Published Price' && !nationalityFees.contains(courseFee.extraFee.Product__c)))){
				if(schoolClientNationality.get(courseFee.extraFee.campus__r.ParentId) == courseFee.extraFee.Nationality__c || schoolClientNationality.get(courseFee.extraFee.campus_course__r.campus__r.ParentId) == courseFee.extraFee.Nationality__c)
					nationalityFees.add(courseFee.extraFee.Product__c);
				if(!checkFeeNameAdded(courseFee.extraFee.Product__r.name__c, FeesPerCampusCourse.get(campusCourseId).get(selFeeType)))
					courseFee.extraFee.From__c = integer.valueOf(selNumberUnits);
					courseFee.extraFee.selectedFee__c = productsAdded.containsKey(courseFee.extraFee.Product__c);
					for(Course_Extra_Fee_Dependent__c cef:courseFee.extraFee.Course_Extra_Fees_Dependent__r){
						Course_Extra_Fee_Dependent__c theCef = cef.clone(true, true);
						if(theCef.Date_Paid_From__c <= mapCampusCourseDetails.get(listCourseCartIds.get(campusCourseId)).Payment_Date__c && theCef.Date_Paid_To__c >= mapCampusCourseDetails.get(listCourseCartIds.get(campusCourseId)).Payment_Date__c && theCef.from__c <= Double.valueOf(selNumberUnits)){
							//cef.Selected__c = selectedFeesPerCourse.get(campusCourseId).contains(cef.id);	
							productsAdded.put(theCef.Product__c, theCef.from__c);
							removeAddedRelatedFee(theCef.Product__c, courseFee.relatedFee);
							
							courseFee.relatedFee.add(theCef);
						}
					}
					if(posFee >= FeesPerCampusCourse.get(campusCourseId).get(selFeeType).size() || FeesPerCampusCourse.get(campusCourseId).get(selFeeType).size() == 0)
						FeesPerCampusCourse.get(campusCourseId).get(selFeeType).add(courseFee);
					else FeesPerCampusCourse.get(campusCourseId).get(selFeeType).add(posFee, courseFee);
			}

		}*/
		
		Date payDt = mapCampusName.get(paramCart).Payment_Date__c;
		Map<String, extraFeeDetails> mapRetrieveFees = new Map<String, extraFeeDetails>();
		Map<String, Course_Extra_Fee_Dependent__c> mapRelatedFees = new Map<String, Course_Extra_Fee_Dependent__c>();
		for(extraFeeDetails refd:FeesPerCampusCourse.get(paramCart).get(selFeeType)){
			if(refd.extraFee.selectedFee__c && refd.extraFee.product__c == productId){
				mapRelatedProductsAdded.put(refd.extraFee.product__c, refd.extraFee.from__c);
				for(Course_Extra_Fee_Dependent__c rf: refd.relatedFee)
					if(rf.selected__c)
						mapRelatedProductsAdded.put(rf.product__c, rf.from__c);
			}
		}
		
		for(Course_Extra_Fee__c cfee:[Select Campus__c, Nationality__c, Campus_Course__c, From__c, date_paid_from__c, date_paid_to__c, Product__c, Product__r.Name__c, Product__r.Product_Type__c, Allow_change_units__c, Optional__c, Value__c, Extra_info__c, Account_Document_File__c, ExtraFeeInterval__c,
										( Select Allow_Change_Units__c, Date_Paid_From__c, Date_Paid_To__c, Extra_Fee_Interval__c, From__c, NumberUnitsSelected__c, Optional__c, Details__c,
										Product__c, Product__r.Name__c, Selected__c, Value__c from Course_Extra_Fees_Dependent__r where Date_Paid_From__c <=:payDt and Date_Paid_To__c >= :payDt order by product__r.name, From__c) 
										from Course_Extra_Fee__c WHERE (Campus_Course__c = :mapCampusName.get(paramCart).campus_course__c OR Campus__c = :mapCampusName.get(paramCart).Campus_Course__r.campus__c) AND Product__c = :productId 
										and (Nationality__c = 'Published Price' OR Nationality__c = :schoolClientNationality.get(mapCampusName.get(paramCart).Campus_Course__r.Campus__r.ParentId)) and date_paid_from__c <=:payDt and date_paid_to__c >= :payDt and from__c <= :decimal.valueOf(selectedNumUnits) order by from__c]){
			mapRelatedFees = new Map<String, Course_Extra_Fee_Dependent__c>();
			Course_Extra_Fee__c pf = cfee.clone(true, true);
			for(Course_Extra_Fee_Dependent__c cdf: pf.Course_Extra_Fees_Dependent__r){ //missing availabilty
				//if(!cdf.Optional__c || mapRelatedProductsAdded.containsKey(cdf.product__c)){
					cdf.NumberUnitsSelected__c = cdf.from__c;
					if(cdf.Allow_Change_Units__c && mapRelatedProductsAdded.containsKey(cdf.product__c)){
						cdf.from__c = mapRelatedProductsAdded.get(cdf.product__c);
						selectedRelatedNumUnits = string.valueOf(cdf.from__c);
						cdf.selected__c = true;
					} else if(decimal.valueOf(selectedRelatedNumUnits) == 0)
						selectedRelatedNumUnits = string.valueOf(cdf.from__c);
					else if(mapRelatedProductsAdded.containsKey(cdf.product__c) && !cdf.Allow_Change_Units__c)
						cdf.selected__c = true;
					
					
					if(cdf.Allow_Change_Units__c && cdf.NumberUnitsSelected__c <= decimal.valueOf(selectedRelatedNumUnits)){
						cdf.from__c = decimal.valueOf(selectedRelatedNumUnits);
						mapRelatedFees.put(cdf.Product__c, cdf);
					}else if(!cdf.Allow_Change_Units__c && cdf.from__c <= decimal.valueOf(selectedNumUnits) ){
						mapRelatedFees.put(cdf.Product__c, cdf);
					}
				}
			//}
			pf.from__c = decimal.valueOf(selectedNumUnits);
			courseFee = new extraFeeDetails();
			courseFee.extraFee = pf.clone(true, true);
			courseFee.extraFee.selectedFee__c = mapRelatedProductsAdded.containsKey(courseFee.extraFee.product__c);
			courseFee.relatedFee.addAll(mapRelatedFees.values().clone());
			mapRetrieveFees.put(pf.Nationality__c, courseFee);
			
		}

		removeFee(productId, FeesPerCampusCourse.get(paramCart).get(selFeeType));

		if(mapRetrieveFees.containsKey(schoolClientNationality.get(mapCampusName.get(paramCart).Campus_Course__r.Campus__r.ParentId))){		
			if(posFee >= FeesPerCampusCourse.get(paramCart).get(selFeeType).size() || FeesPerCampusCourse.get(paramCart).get(selFeeType).size() == 0)
		  		FeesPerCampusCourse.get(paramCart).get(selFeeType).add(mapRetrieveFees.get(schoolClientNationality.get(mapCampusName.get(paramCart).Campus_Course__r.Campus__r.ParentId)));
			else FeesPerCampusCourse.get(paramCart).get(selFeeType).add(posFee, mapRetrieveFees.get(schoolClientNationality.get(mapCampusName.get(paramCart).Campus_Course__r.Campus__r.ParentId)));
		}else{
			if(posFee >= FeesPerCampusCourse.get(paramCart).get(selFeeType).size() || FeesPerCampusCourse.get(paramCart).get(selFeeType).size() == 0)
				FeesPerCampusCourse.get(paramCart).get(selFeeType).add(mapRetrieveFees.get('Published Price'));
			else FeesPerCampusCourse.get(paramCart).get(selFeeType).add(posFee, mapRetrieveFees.get('Published Price'));
		}
				
										
		return null;
	}
	
	
	private void removeFee(String product, List<extraFeeDetails> listFees){
		for(integer i = listFees.size()-1; i >=0; i--)
			if(listFees.get(i).extraFee.Product__c == product){
				System.debug('==>listFees: '+listFees);
				listFees.remove(i);
				System.debug('==>listFees2: '+listFees);
				System.debug('==>posFee: '+posFee);
				posFee = i;
				break;
			}
	}
	
	private void removeAddedRelatedFee(String product, List<Course_Extra_Fee_Dependent__c> listFees){
		for(integer i = listFees.size()-1; i >=0; i--)
			if(listFees.get(i).Product__c == product){
				listFees.remove(i);
				break;
			}
	}
	
	public void addAllOptionalFees(){
		Search_Courses__c newFee;
		List<Search_Courses__c> listFees = new List<Search_Courses__c>();
		map<string,list<string> >searchExtraFeeIds = new map<string,list<string>>();
		map<string,list<string> >searchRelatedExtraFeeIds = new map<string,list<string>>();
		System.debug('==>FeesPerCampusCourse: '+FeesPerCampusCourse);
		for(String af:FeesPerCampusCourse.keySet()){
			for(String mf:FeesPerCampusCourse.get(af).keySet()){
				System.debug('==>FeesPerCampusCourse.get(af).get(mf): '+FeesPerCampusCourse.get(af).get(mf));
				for(extraFeeDetails cef: FeesPerCampusCourse.get(af).get(mf)){
					if(cef.extraFee.selectedFee__c){
						if(!searchExtraFeeIds.containsKey(af)){
							searchExtraFeeIds.put(af, new List<string>{cef.extraFee.id+':'+cef.extraFee.From__c});
						}else searchExtraFeeIds.get(af).add(cef.extraFee.id+':'+cef.extraFee.From__c);
						
						for(Course_Extra_Fee_Dependent__c rcf:cef.relatedFee){
							if((rcf.Selected__c && rcf.optional__c) || !rcf.optional__c){
								if(!searchRelatedExtraFeeIds.containsKey(af)){
									searchRelatedExtraFeeIds.put(af, new List<string>{cef.extraFee.id+':'+rcf.id+':'+rcf.from__c});
								}else searchRelatedExtraFeeIds.get(af).add(cef.extraFee.id+':'+rcf.id+':'+rcf.from__c);
							}
							
						}
					}
				}
			}
		}
		System.debug('==>searchExtraFeeIds: '+searchExtraFeeIds);
		
		for(Search_Courses__c cs:[Select id, Campus_Course__c, Optional_Fee_Ids__c, Optional_Fee_Related_Ids__c from Search_Courses__c WHERE id in :listCourseCartIds.keySet() and Course_Deleted__c = false and isCustomCourse__c = false ORDER BY Course_Order__c NULLS LAST, CreatedDate]){
			cs.Optional_Fee_Ids__c = null;
			cs.Optional_Fee_Related_Ids__c = null;
			cs.Required_Related_Fee_Deleted__c = null;
			listFees.add(cs);	
		}
		update listFees;
		listFees.clear();
		System.debug('==>searchExtraFeeIds: '+searchExtraFeeIds);
		for(String ef:searchExtraFeeIds.keySet()){
			System.debug('==>ef: '+ef);
			newFee = new Search_Courses__c(id = ef);
			System.debug('==>searchRelatedExtraFeeIds.get(ef): '+searchRelatedExtraFeeIds.get(ef));
			System.debug('==>searchRelatedExtraFeeIds: '+searchRelatedExtraFeeIds);
			if(searchExtraFeeIds.containsKey(ef))
				newFee.Optional_Fee_Ids__c = string.join(searchExtraFeeIds.get(ef),',');
			if(searchRelatedExtraFeeIds.containsKey(ef))
				newFee.Optional_Fee_Related_Ids__c = string.join(searchRelatedExtraFeeIds.get(ef),',');
			listFees.add(newFee);
		}
		update listFees;
	}
	
	
	

	public List<SelectOption> extraFeesUnitRange{
		get{
			if(extraFeesUnitRange == null){ 
				extraFeesUnitRange = new List<SelectOption>(); 
				for(integer i = 1; i <= 52; i++)
   			    	extraFeesUnitRange.add(new SelectOption(string.valueOf(i),string.valueOf(i)));
			}
			return extraFeesUnitRange;
		} 
		set;
	}
	 
}