@IsTest
private class onlineappointments_test {
    static testMethod void myUnitTest() {
		TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();

        Contact lead = tf.createClient(agency);
        Contact lead2 = tf.createClient(agency);
        Contact lead3 = tf.createClient(agency);
        Contact lead4 = tf.createClient(agency);

        Contact emp = tf.createEmployee(agency);
        User portalUser = tf.createPortalUser(emp);

        Account ag = tf.createAgencyGroup();
        
		Test.startTest();
        system.runAs(portalUser){
            Client_Booking__c cb = new Client_Booking__c();
            cb.Date_Booking__c = Date.today(); 
            cb.Time_Booking__c = Datetime.now().time();
            cb.Services_Requested__c = 'aaaa';
            cb.Mobile_Number__c = 'aaa';
            cb.Book_Email__c = 'aaa@aaa.com';
            cb.Description__c = 'aaaa';
            cb.Status__c = 'Incomplete';
            cb.User__c = portalUser.ID;
            cb.Client_First_Name__c = 'aaa';
            cb.Client_Last_Name__c = 'aaa';
            cb.Visa_Expiring_Date__c = Date.today();
            insert cb;

            cb = new Client_Booking__c();
            cb.Date_Booking__c = Date.today(); 
            cb.Time_Booking__c = Datetime.now().time();
            cb.Services_Requested__c = 'aaaa';
            cb.Mobile_Number__c = 'aaa';
            cb.Book_Email__c = 'aaa@aaa.com';
            cb.Description__c = 'aaaa';
            cb.Status__c = 'Incomplete';
            cb.User__c = portalUser.ID;
            cb.Client__c = lead.id;
            cb.Client_First_Name__c = 'aaa';
            cb.Client_Last_Name__c = 'aaa';
            cb.Visa_Expiring_Date__c = Date.today();
            insert cb;

            onlineappointments oa = new onlineappointments();
            oa.loadExistingAppointments();

            oa.getAgenciesUser(ag.id);

            Apexpages.currentPage().getParameters().put('filter', 'agencyGroup');
            Apexpages.currentPage().getParameters().put('filterValue', ag.id);
            oa.updateFilters();
            Apexpages.currentPage().getParameters().put('filter', 'agency');
            Apexpages.currentPage().getParameters().put('filterValue', agency.id);
            oa.updateFilters();
            oa.showPastAppointments();
            oa.showCompleteAppointments();
            
            Apexpages.currentPage().getParameters().put('bookingId', cb.id);
            oa.completeBooking();
            oa.cancel();

            onlineappointments.BookingWrapper bw = new onlineappointments.BookingWrapper(cb);
		}
	}
}