public with sharing class Batch_Fix_Checked_Dates{
	public Batch_Fix_Checked_Dates(){
		system.debug('');
	}
}
/*global with sharing class Batch_Fix_Checked_Dates implements Database.Batchable<sObject>, Database.Stateful{
    
    global Map<String, Client_Stage__c> stageZeroItens;
    global String groupID;
    global List<String> agencies;

    global Batch_Fix_Checked_Dates(String agencyGroup, List<String> agencies) {
        this.groupID = agencyGroup;
        //String groupID = '0019000001MjqDX';
        this.agencies = agencies;
        stageZeroItens = new Map<String, Client_Stage__c>();
        for(Client_Stage__c stage : [SELECT ID, Stage_description__c, Stage__c, itemOrder__c FROM Client_Stage__c WHERE Stage__c = 'Stage 0' AND Agency_Group__c = :groupID AND Stage_description__c != 'LEAD - Not Interested'  ORDER BY itemOrder__c DESC]){
			if(!stage.Stage_description__c.contains('xxx')){
				stageZeroItens.put(stage.Stage_description__c, stage);
			}
		}
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        //String [] ids = new String [] {'0032v00002a7ixSAAQ','0039000002UeWUGAA3','0039000001bojwcAAA','0039000002ZxnihAAB','0039000002WvOsEAAV','0032v00002a7L4IAAU','0039000001bohFnAAI','0039000002VrCEwAAN'};
        //String query = 'SELECT ID, Client__c FROM Destination_Tracking__c WHERE Agency_Group__c = :groupID AND Current_Cycle__c = true AND Client__c IN :ids';  
        String query = 'SELECT ID, Client__c FROM Destination_Tracking__c WHERE Agency_Group__c = :groupID AND Current_Cycle__c = true AND Client__r.Current_Agency__c IN :agencies AND Client__r.Lead_Stage__c = \'Stage 0\' AND Client__r.RecordType.Name IN (\'Lead\',\'Client\')';  
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Destination_Tracking__c> cycles) {
        List<String> idCycles = new List<String>();
        Map<String, List<Client_Stage_Follow_up__c>> contactsChecklist = new Map<String, List<Client_Stage_Follow_up__c>>();
        
        for(Destination_Tracking__c cycle : cycles){
            idCycles.add(cycle.ID);
        }

		List<Client_Stage_Follow_up__c> itensToUpdate = new List<Client_Stage_Follow_up__c>();
        for(Client_Stage_Follow_up__c check : [SELECT ID, Client__c, Client__r.Name, Agency_Group__c, Agency__c, Destination__c, Destination_Tracking__c, Stage_Item__c, Stage_Item_Id__c, Stage__c, Last_Saved_Date_Time__c, Checked_By__c FROM Client_Stage_Follow_up__c WHERE Stage__c = 'Stage 0' AND Agency_Group__c = :groupID AND Destination_Tracking__c in :idCycles ORDER BY Client__c, Last_Saved_Date_Time__c DESC]){
			if(!contactsChecklist.containsKey(check.Client__c)){
				contactsChecklist.put(check.Client__c, new List<Client_Stage_Follow_up__c>()); 
			}
			contactsChecklist.get(check.Client__c).add(check);
		}

		List<ReferenceDateByCheck> referenceDates;
		Integer totalDays;
        boolean exitLoop = false;
		for(String contact : contactsChecklist.keySet()){
			referenceDates = generateReferenceDates(stageZeroItens, contactsChecklist.get(contact));
            exitLoop = false;
			while(!exitLoop){
                exitLoop = true;
                for(Integer i = 0; i < referenceDates.size(); i++){
                    if(i + 1 < referenceDates.size()){
                        totalDays = referenceDates.get(i).item.Last_Saved_Date_Time__c.date().daysBetween(referenceDates.get(i+1).item.Last_Saved_Date_Time__c.date());
                        if(totalDays < 0){
                            exitLoop = false;
                            referenceDates.get(i).item.Last_Saved_Date_Time__c = referenceDates.get(i+1).item.Last_Saved_Date_Time__c.addSeconds(-1);
                            itensToUpdate.add(referenceDates.get(i).item);
                        }
                    }
                }
            }
		}

        if(!itensToUpdate.isEmpty()){
            update itensToUpdate;
        }
    }

    global void finish(Database.BatchableContext BC) {}

    private List<ReferenceDateByCheck> generateReferenceDates(Map<String, Client_Stage__c> stageZeroItens, List<Client_Stage_Follow_up__c> checklist){
        List<ReferenceDateByCheck> itens = new List<ReferenceDateByCheck>();
        for(Client_Stage_Follow_up__c checked : checklist){
            if(stageZeroItens.get(checked.Stage_Item__c) != null){
                itens.add(new ReferenceDateByCheck(stageZeroItens.get(checked.Stage_Item__c).itemOrder__c, checked.Last_Saved_Date_Time__c, checked));
            }   
        }
        itens.sort();
        return itens;
    }

	public class ReferenceDateByCheck implements Comparable {
        public Decimal itemOrder{get;set;}
        public Datetime dateChecked{get;set;}
		public Client_Stage_Follow_up__c item{get;set;}

        public ReferenceDateByCheck(Decimal itemOrder, Datetime dateChecked, Client_Stage_Follow_up__c item){
            this.itemOrder = itemOrder;
            this.dateChecked = dateChecked;
            this.item = item;
        }

        public Integer compareTo(Object compareTo) {
			ReferenceDateByCheck oh1 = (ReferenceDateByCheck)compareTo;
			if(this.itemOrder > oh1.itemOrder){
				return 1;
			}else if(this.itemOrder < oh1.itemOrder){
				return -1;
			}
			return 0;
		}
        public Boolean equals(Object obj) {
            if (obj instanceof ReferenceDateByCheck) {
                ReferenceDateByCheck s = (ReferenceDateByCheck)obj;
				return itemOrder == s.itemOrder;
            }
            return false;
        }

        public Integer hashCode() {
            return itemOrder.intValue();
        }
    }
}*/