public without sharing class CampusContactsController {
	 
	public Contact newContact {get;set;}
	public string campusContactRT {get;set;}
	public string idContact {get;set;}
	public Account idCampus {get;set;}	
	public list<Account> campuses{get;set;}
	public list<Contact> lContacts {get;set;}
	public boolean edit {get;set;}
	public boolean showError {get;set;}

	public List<Forms_of_Contact__c> otherFormsOfContact{get;set;}
	public List<Forms_of_Contact__c> otherFormsOfContactToDelete{get;set;}

	public CampusContactsController(){
		if(ApexPages.currentPage().getParameters().get('ct')!=null && ApexPages.currentPage().getParameters().get('ct')!='')
			idContact = ApexPages.currentPage().getParameters().get('ct');
		if(ApexPages.currentPage().getParameters().get('id')!=null && ApexPages.currentPage().getParameters().get('id')!='')
			idCampus = [Select Id, Name, ParentId from Account where id= :ApexPages.currentPage().getParameters().get('id') limit 1];			
	}
	
	/*** Constructor ***/
	public string selectedTypes {get;set;}		
	public list<String> selTypes {get;set;}		
	public list<String> allTypes {get;set;}		

	public void setupCampusContact(){		
		showError = false;
		newContact = new Contact();		

		// All Contact Types
		allTypes = new list<String>();
		Schema.DescribeFieldResult fieldResult = Contact.fields.Contact_Type__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		system.debug('ple==>' + ple);
		for( Schema.PicklistEntry f : ple)
			allTypes.add(f.getValue());
		// 
		
		otherFormsOfContact = new List<Forms_of_Contact__c>();

		//Edit Contact
		if(idContact!=null && idContact!=''){
			edit = true;
			
			newContact = [Select Id, Name, Contact_Type__c, Position_Title__c, Email, Phone, MobilePhone, AccountId, FirstName, LastName, Nickname__c, Gender__c, Birthdate, Fax, campusIds__c From Contact where Id = :idContact];	

			// Selected Contact Types
			selTypes = new list<String>();
			if(newContact.Contact_Type__c != null){
				selTypes.addAll(newContact.Contact_Type__c.split(';'));		
				selectedTypes = String.join(selTypes,',');
			}
			//

			otherFormsOfContact = [SELECT ID, Type__c, Detail__c FROM Forms_of_Contact__c WHERE Contact__c = :idContact];
			otherFormsOfContactToDelete = new List<Forms_of_Contact__c>();

			//Get Campus
			schoolCampus(newContact.AccountId);			
		}
		
		//New Contact
		else{
			edit = false;
			//Get Campus
			schoolCampus(idCampus.ParentId);			
			//Get Record Type Campus Contact		
			campusContactRT = [Select Id From RecordType where Name = 'School Campus Contact' and IsActive = true Limit 1].Id;
			newContact.recordTypeId = campusContactRT;
		}

		if(otherFormsOfContact.size() == 0){
			Forms_of_Contact__c otherForm = new Forms_of_Contact__c(); 
			if(edit){
				otherForm.Contact__c = idContact;
			}
			otherFormsOfContact.add(otherForm);
		}
	}
	
	/*** Get School Campus***/		
	public void schoolCampus(String idSchool){
		campuses = new list<Account>();		
		set<string> ids = new set<string>();
		for(Account c : [Select Name, Id, isSelected__c, contactsId__c from Account where RecordType.Name = 'Campus' and ParentId = :idSchool and IsDeleted = false]){
			if(!edit && c.Id==idCampus.Id){
				c.isSelected__c= true;
			}		
			else{
				ids.clear();
				if(c.contactsId__c!=null){
					list<string> l = c.contactsId__c.split(';');
	    			ids.addAll(l);					
				}
    			if(ids.contains(newContact.Id))
    				c.isSelected__c =true;
			}
			campuses.add(c);
		}		
	}
	
	/*** Redirect Add Contat ***/		
	public PageReference redirectNewContact(){
		PageReference newContactPage = new PageReference('/apex/add_schoolContact?id='+idCampus.Id);
		newContactPage.setRedirect(true);
		return newContactPage;
	}
	
	/*** Redirect View Contacts ***/		
	public PageReference redirectView(){
		PageReference viewContacts = new PageReference('/apex/view_campusContacts?id='+idCampus.Id);
		viewContacts.setRedirect(true);
		return viewContacts;
	}
	
	
	
	/*** View Contacts ***/
	public void viewContacts(){		
		lContacts = new list<Contact>();		
		set<string> ids = new set<string>();
		
		String contacts = [Select contactsId__c from Account where Id = :ApexPages.currentPage().getParameters().get('id') limit 1].contactsId__c;
		system.debug('List Contacts===>' + contacts);
		if(contacts !=null){
			list<string> l = contacts.split(';');
    		ids.addAll(l);
		}
		lContacts = [Select Id, Name, Contact_Type__c, Position_Title__c, Email, Phone, MobilePhone, Fax, campusIds__c From Contact where RecordType.Name = 'School Campus Contact' and Id in :ids order by Name];
		system.debug('Result ===>' + lContacts);	
	}
	
	public boolean checkSelectedCampus(){
		system.debug('Verify Campuses==>' + campuses);
		for(Account c : campuses){
			if(c.isSelected__c)
				return true;
		}
		return false;
	}
	
	/*** Save Contact ***/
	public PageReference saveContact(){				
		system.savepoint save;
		showError = true;
		try{
			
			if(!checkSelectedCampus()){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please selected at least one campus for the contact.'));
				return null;
			}
			else{		
				save = database.setSavepoint();
				
				newContact.AccountId = idCampus.ParentId;
				newContact.recordTypeId = campusContactRT;
				newContact.Full_Name__c = newContact.FirstName + ' ' + newContact.LastName;
				newContact.Contact_Type__c = selectedTypes.replace(',',';');
				
				//Associate selected campus on contact
				String campusIds ='';
				for(Account c : campuses){
					if(c.isSelected__c){
						campusIds += c.Id + ';';		
					}
				}
				newContact.campusIds__c = campusIds;
				insert newContact;

				for(Forms_of_Contact__c form : otherFormsOfContact){
					form.Contact__c = newContact.ID;
				}

				insert otherFormsOfContact;

				list<Account> upCampus = new List<Account>();
				
				//Save contact into Campus			
				for(Account c : campuses){
					//if(c.isSelected__c){
						system.debug('Campus Name & Id==>' + c.Name + ' ' + c.Id);
						if(c.contactsId__c==null)
							c.contactsId__c= newContact.id +';';
						else{
							if(c.contactsId__c.right(1).equals(';'))							
								c.contactsId__c+= newContact.id +';';
							else
								c.contactsId__c+= ';' + newContact.id +';';
						}
							
						c.isSelected__c = false;
						upCampus.add(c);
					//}	
				}//end for
				
				update upCampus;
				return null;
			}
			
		}catch(Exception e){
			database.rollback(save);
			system.debug('Error==>' + e);			
			return null;
		}	
	}
	
	/*** Update Contact ***/
	public void updateContact(){
		system.savepoint save;
		showError = true;
		try{			
			set<string> ids = new set<string>();
			
			if(!checkSelectedCampus()){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please selected at least one campus for the contact.'));	
				showError = false;
			}
			else{
				save = database.setSavepoint();
				newContact.Full_Name__c = newContact.FirstName + ' ' + newContact.LastName;
				newContact.Contact_Type__c = selectedTypes.replace(',',';');
	
				//Update campus ids on contact		
				/*if(newContact.campusIds__c != null){
					list<string> allCampus = newContact.campusIds__c.split(';');			
					ids.addAll(allCampus);			
					for(Account c : campuses){
						if(!c.isSelected__c ){
							ids.remove(c.id);		
						}else{
							if(!ids.contains(c.id)){
								ids.add(c.id);
							}						
						}
						String campusIds='';
						//Update Contact
						for(string s:ids){
							campusIds += s + ';';
						}
						newContact.campusIds__c = campusIds;
					}
				}*/
				
				update newContact;	

				delete otherFormsOfContactToDelete;
				upsert otherFormsOfContact;	

				/*list<Account> upCampus = new list<Account>();	
				//Update Contacts into Campus			
				for(Account c : campuses){
					ids.clear();
					if(c.contactsId__c!=null){
						list<string> l = c.contactsId__c.split(';');			
	    				ids.addAll(l);							
					}
					if(!c.isSelected__c ){
						if(c.contactsId__c !=null){
							ids.remove(newContact.id);		
						}	
					}else{
						if(!ids.contains(newContact.id)){
							ids.add(newContact.id);
						}						
					}
					String upContacts='';
					//Update Contact
					for(string s:ids){
						upContacts += s + ';';
					}
					c.contactsId__c = upContacts;
					c.isSelected__c = false;
					upCampus.add(c);
				}
				
				update upCampus;*/
				
			}
			
		}catch(Exception e){
			database.rollback(save);
			system.debug('Error==>' + e);		
		}	
	}
	
	public void deleteContact(){
		List<Account> allCampus = [Select Name, Id, isSelected__c, contactsId__c from Account where RecordType.Name = 'Campus' and ParentId = :idCampus.ParentId and IsDeleted = false];
		List<Account> updateCampuses = new List<Account>();
		
		try{
			String deletedContactId = ApexPages.currentPage().getParameters().get('delId');
			List<String> updatedContacts = new list<String>();

			otherFormsOfContact = [SELECT ID, Type__c, Detail__c FROM Forms_of_Contact__c WHERE Contact__c = :deletedContactId];

			delete otherFormsOfContact;
			
			//Delete Contact
			Database.delete(deletedContactId);
			
			//Update all campus with this contact
			for(Account a : allCampus){
				set<String> campusContacts = new set<String>(a.contactsId__c.split(';'));
				updatedContacts.clear();
				if(campusContacts.contains(deletedContactId)){
					campusContacts.remove(deletedContactId);
					updatedContacts.addAll(campusContacts);
					a.contactsId__c = String.join(updatedContacts, ';');					
					updateCampuses.add(a);
				}
			}	
			
			update updateCampuses;		
			
			
			
		}catch(Exception e){
			system.debug('Error==' + e);
		}	
	}

	public List<SelectOption> getListTypesOtherContact(){
		List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Forms_of_Contact__c.fields.Type__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple){
			options.add(new SelectOption(f.getValue(), f.getLabel()));
		}
		return options;
	}

	public void addNewOtherFormOfContact(){
		Forms_of_Contact__c otherForm = new Forms_of_Contact__c(); 
		if(edit){
			otherForm.Contact__c = idContact;
		}
		otherFormsOfContact.add(otherForm);
	}
	public void deleteOtherFormOfContact(){
		String indexStr = ApexPages.currentPage().getParameters().get('index');
		Integer index = Integer.valueOf(indexStr);

		Forms_of_Contact__c otherForm = otherFormsOfContact.get(index);
		otherFormsOfContact.remove(index);

		if(edit){
			otherFormsOfContactToDelete.add(otherForm);
		}

		if(otherFormsOfContact.size() == 0){
			otherForm = new Forms_of_Contact__c(); 
			if(edit){
				otherForm.Contact__c = idContact;
			}
			otherFormsOfContact.add(otherForm);
		}
	}

	public void updateTypeFormOfContact(){
		String type = ApexPages.currentPage().getParameters().get('type');
		String indexStr = ApexPages.currentPage().getParameters().get('index');
		Integer index = Integer.valueOf(indexStr);
		otherFormsOfContact.get(index).Type__c = type;
	}
}