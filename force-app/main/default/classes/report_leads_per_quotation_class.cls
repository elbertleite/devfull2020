@isTest
private class report_leads_per_quotation_class {
	static testMethod void myUnitTest() {
		TestFactory tf = new TestFactory();
		
		Account agency = tf.createAgency();

		tf.createChecklists(agency.Parentid);

		Contact emp = tf.createEmployee(agency);
		
		Contact lead = tf.createLead(agency, emp);
		lead.Ownership_History_Field__c = '[{"toUserID":"005O0000004bBaSIAU","toUser":"Patricia Greco","toAgencyID":"0019000001MjqIIAAZ","toAgency":"IP Australia Sydney (HO)","fromUserID":null,"fromUser":null,"fromAgencyID":null,"fromAgency":null,"createdDate":"2018-09-18T04:17:12.024Z","createdByUserID":"005O0000004bBaSIAU","createdByUser":"Patricia Greco","action":"Taken"}]';
		lead.Lead_Stage__c = 'Stage 1';
		lead.Lead_Area_of_Study__c = 'Test 1; Test 2';
		update lead;

		Contact lead2 = tf.createClient(agency);
		Contact lead3 = tf.createClient(agency);

		List<String> leads = new List<String>();
		leads.add(lead.ID);
		leads.add(lead2.ID);
		leads.add(lead3.ID);

		Contact client = tf.createClient(agency);

		User portalUser = tf.createPortalUser(emp);

		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);

       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);

		Quotation__c q1 = tf.createQuotation(lead, campusCourse);
		Quotation__c q2 = tf.createQuotation(lead, campusCourse);
		Quotation__c q3 = tf.createQuotation(client, campusCourse);
		Quotation__c q4 = tf.createQuotation(client, campusCourse);

		Destination_Tracking__c dt = new Destination_tracking__c();
		dt.Client__c = lead.id;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
		dt.Expected_Travel_Date__c = system.today().addMonths(3);
		dt.lead_Cycle__c = false;
		insert dt;

		Client_Checklist__c checklist = new Client_Checklist__c();
		checklist.Agency__c = agency.id;
		checklist.Agency_Group__c = agency.Parentid;
		checklist.Destination__c = lead.Destination_Country__c;
		checklist.Checklist_Item__c = 'aaa';
		checklist.Client__c = lead.id;
		checklist.Destination_Tracking__c = dt.id;
		insert checklist;

		Test.startTest();
     	system.runAs(portalUser){
		
			emp.Account = agency;
			portalUser.contact = emp;
			update portalUser;

			report_leads_per_quotation controller = new report_leads_per_quotation();

			controller.getGroups();
			controller.loadEmployees(agency.ID);
			controller.loadCampuses('');

			Apexpages.currentPage().getParameters().put('idAgency', agency.ID);
			controller.updateEmployees();
			
			Apexpages.currentPage().getParameters().put('idSchool', school.ID);
			controller.updateCampuses();
			
			Apexpages.currentPage().getParameters().put('idGroup', agency.Parent.ID);
			controller.updateAgencies();

			Apexpages.currentPage().getParameters().put('export', 'true');
			//Apexpages.currentPage().getParameters().put('begin', Date.today().format());
			//Apexpages.currentPage().getParameters().put('end', Date.today().addMonths(-1).format());
			Apexpages.currentPage().getParameters().put('group', agency.Parentid);
			//Apexpages.currentPage().getParameters().put('agency', agency.id);
			//Apexpages.currentPage().getParameters().put('employee', emp.ID);
			//Apexpages.currentPage().getParameters().put('school', school.ID);
			//Apexpages.currentPage().getParameters().put('campus', campus.ID);

			controller = new report_leads_per_quotation();

			//new report_leads_per_quotation.MethodsWithoutSharing().loadSchools(school.ID);
			//new report_leads_per_quotation.MethodsWithoutSharing().loadCampuses(agency.ID, agency.Parent.ID);
			
			/*report_conversion_rate.AverageTimeReport atr = new report_conversion_rate.AverageTimeReport();
			atr = new report_conversion_rate.AverageTimeReport('');
			atr.status = '';
			atr.averageDays = 5;
			atr.totalClients = 7;
			atr.equals(atr);
			atr.hashCode();

			report_conversion_rate.ReportResult rr1 = new report_conversion_rate.ReportResult();
			report_conversion_rate.ReportResult rr2 = new report_conversion_rate.ReportResult('');
			report_conversion_rate.ReportResult rr3 = new report_conversion_rate.ReportResult('',0);


			system.debug(rr3.colorWon);
			system.debug(rr3.colorCurrent);
			system.debug(rr3.colorStagnated);
			system.debug(rr3.colorLost);

			rr3.equals(rr2);
			rr3.hashCode();
			rr3.compareTo(rr2);

			controller.getReasons();
			controller.getDestinations();
			controller.getGroups();
			controller.getTimeInSeconds(Datetime.now());
			controller.updateAgencies();
			controller.updateEmployees();
			controller.updateSelectedCampaign();
			controller.updateEmployees();
			controller.typeOfSearch = ';;;';
			controller.generateReport();
			

			controller = new report_conversion_rate();*/
		}
	}
}