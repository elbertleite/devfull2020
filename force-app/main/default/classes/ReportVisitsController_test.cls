/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ReportVisitsController_test {

    static testMethod void myUnitTest() {
        
		TestFactory tf = new TestFactory();
		Account agency = tf.createAgency();
		Contact employee = tf.createEmployee(agency);
		User portalUser = tf.createPortalUser(employee);		
		
		Department__c dept = [Select id, Services__c  from Department__c where id = :employee.Department__c];
		
		Test.startTest();
		system.runAs(portalUser){
			Contact ct = tf.createLead(agency, employee);
			
			String paramStaff = portalUser.id + '!#' + dept.id + '!#Translations,'+portalUser.id + '!#' + dept.id + '!#STC';
			
			Visit__c v = new Visit__c();
			v.Contact__c = ct.id;
			v.Staff_Requested__c = paramStaff;
			v.status__c = 'Waiting';
			v.Department_Requested__c = dept.id;
			v.Department__c = dept.id;
			v.Accepted_By__c = portalUser.id;
			v.Accepted_Date__c = system.now();
			v.Closed_By__c = portalUser.id;
			v.Close_Date__c = system.now();
			v.Department_Requested__c = dept.id;
			v.Staff__c = employee.Name;
			insert v;
			
			v = new Visit__c();
			v.Contact__c = ct.id;
			v.Staff_Requested__c = paramStaff;
			v.status__c = 'Completed';
			v.Department_Requested__c = dept.id;
			v.Department__c = dept.id;
			v.Accepted_By__c = portalUser.id;
			v.Accepted_Date__c = system.now();
			v.Closed_By__c = portalUser.id;
			v.Close_Date__c = system.now();
			v.Department_Requested__c = dept.id;
			v.Staff__c = employee.Name;
			insert v;
			
			ReportVisitsController report = new ReportVisitsController();
			report.changeAgencyGroup();
			report.changeAgency();
			report.changeDepartment();
			
			List<SelectOption> status = report.status;
			List<SelectOption> periods = report.periods;
			List<SelectOption> services = report.services;
			List<SelectOption> userOptions = report.userOptions;
			report.selectedAgency = agency.parentid;
			List<SelectOption> departments = report.departments;
			List<SelectOption> agencyOptions = report.agencyOptions;
			List<SelectOption> agencyGroupOptions = report.agencyGroupOptions;
			
			report.fromDate.Expected_Travel_Date__c = system.today().addDays(-15);
			report.toDate.Expected_Travel_Date__c = system.today().addDays(15);		
			report.selectedDepartment = 'all';	
			report.selectedAgency = 'all';
			report.searchClientVisits();
			
			report.selectedAgency = agency.id;
			report.selectedStatus = 'pending';
			report.searchClientVisits();
		}
		Test.stopTest();




    }
}