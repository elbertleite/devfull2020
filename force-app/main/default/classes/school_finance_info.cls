public with sharing class school_finance_info {
	public Boolean blockChangeOptions{get;set;}

	public Account accSelected{get;set;}
	public Account schoolGroupSelected{get;set;}
	public Account schoolSelected{get;set;}
	public Account campusSelected{get;set;}
	
	public List<SelectOption> allCountries {get;set;}
	/*

	public List<SelectOption> listSchoolsGroup {get;set;}
	public List<SelectOption> listSchools {get;set;}
	public List<SelectOption> listCampus {get;set;}

	*/
	
	public school_finance_info() {

		String id = ApexPages.currentPage().getParameters().get('id');

		accSelected = [SELECT ID, Name, RecordType.Name, Parent.ID, Parent.Parent.ID, ShippingCountry, ShippingState, ShippingCity, ShippingStreet, ShippingPostalCode, Registration_name__c, Type_of_Business_Number__c, Business_Number__c FROM Account WHERE ID = :id];

		if(accSelected.RecordType.Name == 'School Group'){
			schoolGroupSelected = accSelected;
		}else if(accSelected.RecordType.Name == 'School'){
			schoolGroupSelected = [SELECT ID, Name FROM Account WHERE ID = :accSelected.Parent.ID];
			schoolSelected = accSelected;
		}else{
			schoolGroupSelected = [SELECT ID, Name FROM Account WHERE ID = :accSelected.Parent.Parent.ID];
			schoolSelected = [SELECT ID, Name FROM Account WHERE ID = :accSelected.Parent.ID];
			campusSelected = accSelected;

		}
		allCountries = new list<SelectOption>(IPFunctions.getAllCountries());

	}	
	
		/*listSchoolsGroup = new list<SelectOption>(IPFunctions.getSchoolsGroup());
		listSchoolsGroup.add(0, new SelectOption('','Choose one Group'));

	
	}

	/*public void searchSchools(){
		schoolSelected = null;
		schoolGroupSelected = ApexPages.currentPage().getParameters().get('id');
		listSchools = new List<SelectOption>();
		if(!String.isEmpty(schoolGroupSelected)){
			listSchools.add(new SelectOption('','Choose School'));
			for(Account school : [SELECT ID, Name FROM Account WHERE RecordType.Name = 'School' AND Parent.ID = :schoolGroupSelected ORDER BY Name ASC]){
				listSchools.add(new SelectOption(school.ID, school.Name));
			}
		}else{
			schoolGroupSelected = null;
		}
	}

	public void searchCampuses(){
		campusSelected = null;
		schoolSelected = ApexPages.currentPage().getParameters().get('id');
		listCampus = new List<SelectOption>();
		if(!String.isEmpty(schoolSelected)){
			listCampus.add(new SelectOption('','Choose Campus'));
			for(Account school : [SELECT ID, Name FROM Account WHERE RecordType.Name = 'Campus' AND Parent.ID = :schoolSelected ORDER BY Name ASC]){
				listCampus.add(new SelectOption(school.ID, school.Name));
			}
		}else{
			schoolSelected = null;
		}
	}

	public void updateCampuses(){
		campusSelected = ApexPages.currentPage().getParameters().get('id');
		if(campusSelected == ''){
			campusSelected = null;
		}
	}

	public void searchFinanceData(){
		accSelected = null;
		String searchID;
		if(!String.isEmpty(campusSelected)){
			searchID = campusSelected;
			system.debug('SEARCHING FOR CAMPUS '+searchID);
		}else if(!String.isEmpty(schoolSelected)){
			searchID = schoolSelected;
			system.debug('SEARCHING FOR SCHOOL '+searchID);
		}else{
			searchID = schoolGroupSelected;
			system.debug('SEARCHING FOR SCHOOL GROUP '+searchID);
		}
		accSelected = [SELECT ID, Name, RecordType.Name, ShippingCountry, ShippingState, ShippingCity, ShippingStreet, ShippingPostalCode, Registration_name__c, Type_of_Business_Number__c, Business_Number__c FROM Account WHERE ID = :searchID];
	}*/

	public void saveFinanceInfo(){
		try{
			update accSelected;
		}catch(Exception e){
			system.debug('Error on saveFinanceInfo() ===> ' + e.getLineNumber() + ' <=== '+e.getMessage());
		}
	}
}