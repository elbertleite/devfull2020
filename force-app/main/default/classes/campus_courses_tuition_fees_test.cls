@isTest
private class campus_courses_tuition_fees_test {
    static testMethod void myUnitTest() {
		TestFactory tf = new TestFactory();
        
        Account agency = tf.createAgency();
        Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
        
        Map<String,String> recordTypes = new Map<String,String>();
        for( RecordType r :[select id, name from recordtype where isActive = true] ){
            recordTypes.put(r.Name,r.Id);
        }

        Contact client = tf.createClient(agency);

        Account schoolGroup = tf.createSchoolGroup();
        
        Account school = tf.createSchool();
        school.parentId = schoolGroup.id;
        update school;

        Account campus = tf.createCampus(school, agency);

        Course__c course = tf.createCourse();
        course.School__c = school.id;

        update course;

        Campus_Course__c cc = tf.createCampusCourse(campus, course);

        Course_Price__c price = tf.createCoursePrice(cc, 'Published Price');

        Start_Date_Range__c sd = tf.createProdDateRange(agency, new Quotation_List_Products__c());

        Quotation__c quote = tf.createQuotation(client, cc);

        Apexpages.currentPage().getParameters().put('campusID', campus.id);
        Apexpages.currentPage().getParameters().put('courseID', course.id);
        Apexpages.currentPage().getParameters().put('idSchool', school.id);

        campus_courses_tuition_fees controller = new campus_courses_tuition_fees();
        controller.schoolId = school.id;
        //controller.initAddPrice();
        controller.loadCourses();
        controller.addNewPrice();
        controller.loadFiles();
        controller.loadNationalityGroups();
        controller.getPriceAvailabilityList();
        controller.coursesAvailable = '';
        controller.dateToExpire = '';
        controller.pricesToEdit = new List<Course_Price__c>();
	}
}