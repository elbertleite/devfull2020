/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class account_balance_summary_test {

    static testMethod void myUnitTest() {
         TestFactory tf = new TestFactory();

       	Account school = tf.createSchool();

       	Account agencyGroup = tf.createAgencyGroup();

       	Account agency = tf.createAgency();

       	agency.parentID = agencyGroup.id;

      	Contact emp = tf.createEmployee(agency);

       	User portalUser = tf.createPortalUser(emp);

       	Account campus = tf.createCampus(school, agency);
       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
        Department__c department = tf.createDepartment(agency);

		Contact client = tf.createLead(agency, emp);

		client_course__c booking = tf.createBooking(client);
       	client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
		client_course__c cc2 =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);

		List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
		List<client_course_instalment__c> instalments2 =  tf.createClientCourseInstalments(cc2);

       	cc.Enrolment_Date__c = System.today();
       	cc.Enroled_by_Agency__c = agency.id;
       	cc.Commission_Type__c = 0;
		update cc;

		cc2.Enrolment_Date__c = System.today();
       	cc2.Enroled_by_Agency__c = agency.id;
       	cc2.Commission_Type__c = 0;
		update cc2;

		instalments[0].Received_By_Agency__c = agency.id;
		instalments[0].Received_By_Agency__c = agency.id;
		instalments[0].Paid_to_School_By_Agency__c = agency.id;
		instalments[0].Paid_To_School_On__c = System.today();
		instalments[0].Received_Date__c = System.today();
		instalments[0].Confirmed_Date__c = System.today();
		instalments[0].isselected__c = true;

		instalments[1].Received_By_Agency__c = agency.id;
		instalments[1].Received_By_Agency__c = agency.id;
		instalments[1].Paid_to_School_By_Agency__c = agency.id;
		instalments[1].Paid_To_School_On__c = System.today();
		instalments[1].Received_Date__c = System.today();
		instalments[1].Confirmed_Date__c = System.today();
		instalments[1].isselected__c = true;

		instalments[2].Received_By_Agency__c = agency.id;
		instalments[2].Received_By_Agency__c = agency.id;
		instalments[2].Received_Date__c = System.today();
		instalments[2].Confirmed_Date__c = System.today();
		instalments[2].isselected__c = true;

		update instalments;

		instalments2[0].Received_By_Agency__c = agency.id;
		instalments2[0].Received_By_Agency__c = agency.id;
		instalments2[0].Received_Date__c = System.today();
		instalments2[0].isPDS__c = true;
		instalments2[0].Confirmed_Date__c = System.today();
		instalments2[0].Commission_Paid_Date__c = System.today();
		instalments2[0].isselected__c = true;

		instalments2[1].Received_By_Agency__c = agency.id;
		instalments2[1].Paid_to_School_By_Agency__c = agency.id;
		instalments2[1].Received_Date__c = System.today();
		instalments2[1].isPFS__c = true;
		instalments2[1].Commission_Paid_Date__c = System.today();
		instalments2[1].Confirmed_Date__c = System.today();
		instalments2[1].isselected__c = true;

		instalments2[2].Received_By_Agency__c = agency.id;
		instalments2[2].Paid_to_School_By_Agency__c = agency.id;
		instalments2[2].Received_Date__c = System.today();
		instalments2[2].isPCS__c = true;
		instalments2[2].Commission_Paid_Date__c = System.today();
		instalments2[2].Confirmed_Date__c = System.today();
		instalments2[2].isselected__c = true;

		update instalments2;

		client_course_instalment_payment__c installPayment = new client_course_instalment_payment__c();
		installPayment.Received_By__c = UserInfo.getUserId();
		installPayment.Received_On__c = System.today();
		installPayment.Received_By_Agency__c = agency.id;
		installPayment.Date_Paid__c = System.today();
		installPayment.Confirmed_Date__c = System.today();
		installPayment.Payment_Type__c = 'Creditcard';
		installPayment.Value__c = 100;
		installPayment.client_course_instalment__c = instalments[0].id;
		installPayment.selected__c = true;
		insert installPayment;

		installPayment = new client_course_instalment_payment__c();
		installPayment.Received_By__c = UserInfo.getUserId();
		installPayment.Received_On__c = System.today();
		installPayment.Received_By_Agency__c = agency.id;
		installPayment.Date_Paid__c = System.today();
		installPayment.Confirmed_Date__c = System.today();
		installPayment.Payment_Type__c = 'School Credit';
		installPayment.Value__c = 100;
		installPayment.client_course_instalment__c = instalments[1].id;
		installPayment.selected__c = true;
		insert installPayment;

		installPayment = new client_course_instalment_payment__c();
		installPayment.Received_By__c = UserInfo.getUserId();
		installPayment.Received_On__c = System.today();
		installPayment.Received_By_Agency__c = agency.id;
		installPayment.Date_Paid__c = System.today();
		installPayment.Confirmed_Date__c = System.today();
		installPayment.Payment_Type__c = 'School Credit';
		installPayment.Value__c = 120;
		installPayment.client_course_instalment__c = instalments[2].id;
		installPayment.selected__c = true;
		insert installPayment;


		Bank_Detail__c bank = new Bank_Detail__c();
		bank.Account_Number__c = '1234';
		bank.Account__c = agency.id;
		insert bank;

		Bank_Deposit__c bankDeposit = new Bank_Deposit__c();
		bankDeposit.Amount__c = 1000;
		bankDeposit.Bank__c = Bank.id;
		bankDeposit.Type__c = 'Cash';
		bankDeposit.Deposit_Date__c = System.today();
		bankDeposit.Deposited_By_Agency__c = agency.id;
		insert bankDeposit;

		installPayment = new client_course_instalment_payment__c();
		installPayment.Received_By__c = UserInfo.getUserId();
		installPayment.Received_On__c = System.today();
		installPayment.Received_By_Agency__c = agency.id;
		installPayment.Date_Paid__c = System.today();
		installPayment.Confirmed_Date__c = System.today();
		installPayment.Payment_Type__c = 'Cash';
		installPayment.Value__c = 100;
		installPayment.Bank_Deposit__c = bankDeposit.id;
		installPayment.client_course_instalment__c = instalments[2].id;
		installPayment.selected__c = true;
		insert installPayment;

		installPayment = new client_course_instalment_payment__c();
		installPayment.Received_By__c = UserInfo.getUserId();
		installPayment.Received_On__c = System.today();
		installPayment.Received_By_Agency__c = agency.id;
		installPayment.Date_Paid__c = System.today();
		installPayment.Confirmed_Date__c = System.today();
		installPayment.Payment_Type__c = 'Cash';
		installPayment.Value__c = 111;
		installPayment.Bank_Deposit__c = bankDeposit.id;
		installPayment.client_course_instalment__c = instalments[1].id;
		installPayment.selected__c = true;
		insert installPayment;



		installPayment = new client_course_instalment_payment__c();
		installPayment.Received_By__c = UserInfo.getUserId();
		installPayment.Received_On__c = System.today();
		installPayment.Received_By_Agency__c = agency.id;
		installPayment.Date_Paid__c = System.today();
		installPayment.Confirmed_Date__c = System.today();
		installPayment.Payment_Type__c = 'Covered by Agency';
		installPayment.Value__c = 100;
		installPayment.client_course_instalment__c = instalments2[0].id;
		installPayment.selected__c = true;
		insert installPayment;

		installPayment = new client_course_instalment_payment__c();
		installPayment.Received_By__c = UserInfo.getUserId();
		installPayment.Received_On__c = System.today();
		installPayment.Received_By_Agency__c = agency.id;
		installPayment.Date_Paid__c = System.today();
		installPayment.Confirmed_Date__c = System.today();
		installPayment.Payment_Type__c = 'Creditcard';
		installPayment.Value__c = 100;
		installPayment.client_course_instalment__c = instalments2[1].id;
		installPayment.selected__c = true;
		insert installPayment;


		installPayment = new client_course_instalment_payment__c();
		installPayment.Received_By__c = UserInfo.getUserId();
		installPayment.Received_On__c = System.today();
		installPayment.Received_By_Agency__c = agency.id;
		installPayment.Paid_to_Client_By_Agency__c = agency.id;
		installPayment.Date_Paid__c = System.today();
		installPayment.Confirmed_Date__c = System.today();
		installPayment.Payment_Type__c = 'Money Transfer';
		installPayment.Value__c = 100;
		installPayment.isClient_Refund__c = true;
		installPayment.client_course_instalment__c = instalments2[2].id;
		installPayment.selected__c = true;
		insert installPayment;

     	Test.startTest();
     	system.runAs(portalUser){

			account_balance_summary testClass = new account_balance_summary();

			List<SelectOption> Periods = testClass.getPeriods();
			List<SelectOption> agencyGroupOptions = testClass.agencyGroupOptions;
			list<SelectOption> Agencies = testClass.getAgencies();

			testClass.SelectedPeriod = 'THIS_MONTH';
			testClass.searchPaymentreconciled();

			ApexPages.currentPage().getParameters().put('typePay','Creditcard');
			// testClass.confirmePaymentType();

			testClass.confirmFirstPayments();
			testClass.confirmRepayments();
			

			//testClass.SelectedPeriod = 'notConf';
			//testClass.searchPaymentreconciled();


			//ApexPages.currentPage().getParameters().put('refType','schoolCourseRefund');
			//testClass.confirmRefunds();
     	}

    }
}