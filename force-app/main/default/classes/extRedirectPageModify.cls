public class extRedirectPageModify{

    public  extRedirectPageModify(ApexPages.StandardController controller) {
        this.acco = controller.getRecord(); 

    }

    sObject acco;

    public string redirectPage{
        get{
        redirectPage = ApexPages.currentPage().getParameters().get('pg');
        return redirectPage;
    }
        set;
    }
	
	public PageReference save() {
		try {
			upsert  acco;
			PageReference acctPage = new PageReference('/apex/'+redirectPage);
			acctPage.setRedirect(true);
			return acctPage;
		} catch(Exception e){
			ApexPages.addMessages(e);
		}
		return null;
		
	}
    
    public PageReference del() {
		try {
	        delete  acco;
	        PageReference acctPage = new PageReference('/apex/'+redirectPage);
	        acctPage.setRedirect(true);
	        return acctPage;
		} catch(Exception e){
			ApexPages.addMessages(e);
		}
		return null;
    }	
    
}