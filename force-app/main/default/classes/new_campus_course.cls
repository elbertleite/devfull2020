public without sharing class new_campus_course {

	public Campus_Course__c course{get;set;}
	public String idCampus{get;set;}
	public String campusName{get;set;}
	public Boolean edit{get;set;}

	public String courseCategory{get;set;}

	public boolean onlySoldInBlocks{get;set;}
	public boolean courseAvailable{get;set;}
	public boolean isPackage{get;set;}

	public boolean userHasRestrictedAccess{get;set;}

	public new_campus_course() {
		String idCourse = ApexPages.currentPage().getParameters().get('idCourse');
		idCampus = ApexPages.currentPage().getParameters().get('idCampus');

		if(!String.isEmpty(idCourse)){		
			edit = true;
			course = [Select Course__r.Course_Qualification__c, Campus__r.Name, Campus__r.Parent.Name, Course__r.Why_choose_this_course__c, Course__r.Academic_requirements__c, Course__r.Course_Lenght_in_Months__c, Course__r.Course_Code__c, Course__r.School_Course_Name__c, Course__r.Course_Category__c, Course__r.Type__c, Course__r.Name, Course__r.Sub_Type__c, Course__r.Course_Type__c, Course__r.Course_Unit_Type__c, Course__r.Course_Unit_Length__c, Course__r.Hours_Week__c, Course__r.Language__c, Course__r.Only_Sold_in_Blocks__c, Course__r.Optional_Hours_Per_Week__c, Course__r.Package__c, Course__r.Id, Course__r.Maximum_length__c, Course__r.Course_Country__c, Course__r.Minimum_length__c, Period__c, Is_Available__c, Unavailable_Reason__c, Package_Courses__c, Available_From__c, Campus_Country__c, Language_Level_Required__c, Length_of_each_Lesson__c, Minimum_age_requirement__c, Course__r.Form_of_Delivery__c, Course__r.Instalment_Option__c, Course__r.Instalment_Term_Option__c, Course__r.Instalment_Term_Interval__c, Course__r.Payment_Rules__c, Course__r.Important_information__c, Course__r.Instalment_Interval__c FROM Campus_Course__c WHERE ID = :idCourse ];

			courseAvailable = course.Is_Available__c;
			courseCategory = course.Course__r.Course_Category__c;
			isPackage = course.Course__r.Package__c;
			onlySoldInBlocks = course.Course__r.Only_Sold_in_Blocks__c;
			campusName = course.Campus__r.Name + ' - ' + course.Campus__r.Parent.Name;
		}else{
			Account campus = [SELECT ID, Name, Parent.Name, Parent.ID, RecordType.Name FROM Account WHERE ID = :idCampus];
			edit = false;
			courseAvailable = true;
			onlySoldInBlocks = false;
			isPackage = false;
			course = new Campus_Course__c();
			course.Course__r = new Course__c(School__c = campus.Parent.ID);
			course.Campus__c = campus.ID;
			courseCategory = 'Language';
		}

		userHasRestrictedAccess = PMCHelper.hasUserRestrictedAccessPMCFunctionalities();
	}

	public void changeCourseCategory(){
		courseCategory = ApexPages.currentPage().getParameters().get('category');
	}
	public void changeCourseAvailable(){
		courseAvailable = ApexPages.currentPage().getParameters().get('available') == 'true';
	}
	public void changeCourseIsPackage(){
		isPackage = ApexPages.currentPage().getParameters().get('isPackage') == 'true';
	}
	public void changeSoldInBlocks(){
		onlySoldInBlocks = ApexPages.currentPage().getParameters().get('onlySoldInBlocks') == 'true';
	}

	public void saveCourse(){
		course.Is_Available__c = courseAvailable;
		course.Course__r.Course_Category__c = courseCategory;
		course.Course__r.Package__c = isPackage;
		if(isPackage && !String.isEmpty(course.Package_Courses__c)){
			course.Package_Courses__c = course.Package_Courses__c.replaceAll('\\[','').replaceAll('\\]','').deleteWhitespace();
			//system.debug('THE PACKAGE '+course.Package_Courses__c);
		}
		if(courseCategory == 'Language'){
			course.Course__r.Course_Unit_Length__c = null;
			course.Course__r.Course_Lenght_in_Months__c = null;
		}else{
			course.Course__r.Hours_Week__c = null;
			course.Course__r.Optional_Hours_Per_Week__c = null;
		}
		if(edit){
			update course.Course__r;
			update course;
		}else{
			Course__c courseC = course.Course__r;
			insert courseC;
			course.Course__c = courseC.ID;
			insert course;
		}
	}

	public List<SelectOption> getCourseCategories(){
		List<SelectOption> categories = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Course__c.Course_Category__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple){
			categories.add(new SelectOption(f.getLabel(), f.getValue()));
		}
        return categories;
	}

	public List<SelectOption> getCourses(){
        List<SelectOption> courses = new List<SelectOption>();
        for(Campus_Course__c cc : [select id, course__r.Name, course__r.Hours_Week__c, course__r.Optional_Hours_Per_Week__c, course__r.Course_Unit_Type__c, Is_Available__c from Campus_Course__c where campus__c = :idCampus and course__r.Package__c = false order by course__r.Name]){
			String courseName = cc.course__r.Name + getCourseHours(cc.course__r);
			if(!cc.Is_Available__c)
				courseName += ' (unavailable)';
			courses.add(new SelectOption(cc.id, courseName));
		}
        return courses;
    }

	private String getCourseHours(Course__c course){
        String courseHours = '';
        if(course.Hours_Week__c != null && course.Hours_Week__c != 0){
            courseHours = ' (' + Integer.valueOf(course.Hours_Week__c) + 'hrs';

            if(course.Optional_Hours_Per_Week__c != null && course.Optional_Hours_Per_Week__c != 0){
                courseHours += ' + ' + Integer.valueOf(course.Optional_Hours_Per_Week__c) + ' opt./' + course.Course_Unit_Type__c + ')';
            } else {
                courseHours += '/'+ course.Course_Unit_Type__c + ')';
            }

        }
        return courseHours;
    }

	public List<SelectOption> getPeriods(){
       	List<SelectOption> Periods = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Campus_Course__c.Period__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple)
			Periods.add(new SelectOption(f.getLabel(), f.getValue()));
        return Periods;
    }
}