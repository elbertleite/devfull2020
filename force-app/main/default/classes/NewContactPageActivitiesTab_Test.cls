@isTest
private class NewContactPageActivitiesTab_Test{
	static testMethod void myUnitTest() {

		TestFactory tf = new TestFactory();
		
		Account agency = tf.createAgency();

		Account agencyGroup = tf.createAgencyGroup();

		tf.createChecklists(agency.Parentid);

		Contact emp = tf.createEmployee(agency);
		
		Contact lead = tf.createLead(agency, emp);
		lead.Ownership_History_Field__c = '[{"toUserID":"005O0000004bBaSIAU","toUser":"Patricia Greco","toAgencyID":"0019000001MjqIIAAZ","toAgency":"IP Australia Sydney (HO)","fromUserID":null,"fromUser":null,"fromAgencyID":null,"fromAgency":null,"createdDate":"2018-09-18T04:17:12.024Z","createdByUserID":"005O0000004bBaSIAU","createdByUser":"Patricia Greco","action":"Taken"}]';
		lead.Lead_Stage__c = 'Stage 1';
		update lead;

		Contact client = tf.createClient(agency);

		User portalUser = tf.createPortalUser(emp);

		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);

       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);

		Quotation__c q1 = tf.createQuotation(lead, campusCourse);
		Quotation__c q2 = tf.createQuotation(lead, campusCourse);

		Destination_Tracking__c dt = new Destination_tracking__c();
		dt.Client__c = lead.id;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
		dt.Expected_Travel_Date__c = system.today().addMonths(3);
		dt.lead_Cycle__c = false;
		insert dt;

		Client_Checklist__c checklist = new Client_Checklist__c();
		checklist.Agency__c = agency.id;
		checklist.Agency_Group__c = agency.Parentid;
		checklist.Destination__c = lead.Destination_Country__c;
		checklist.Checklist_Item__c = 'aaa';
		checklist.Client__c = lead.id;
		checklist.Destination_Tracking__c = dt.id;
		insert checklist;
		
		Test.startTest();
     	system.runAs(portalUser){
		
			List<Client_Stage_Follow_up__c> followUps = new List<Client_Stage_Follow_up__c>();
			for(Client_Stage__c stage : [SELECT ID, Stage_Description__c FROM Client_Stage__c WHERE Agency_Group__c = : agency.Parentid]){
				Client_Stage_Follow_up__c cs = new Client_Stage_Follow_up__c();
				cs.Agency__c = agency.id;
				cs.Agency_Group__c = agency.Parentid;
				cs.Client__c = lead.id;
				cs.Destination__c = 'Australia';
				cs.Stage_Item__c = stage.Stage_Description__c;
				cs.Stage_Item_Id__c = stage.id;
				cs.Stage__c = 'Stage 1';
				cs.Destination_Tracking__c = dt.id;
				followUps.add(cs);
			}

			insert followUps;
		
			emp.Account = agency;
			portalUser.contact = emp;
			update portalUser;

			 Apexpages.currentPage().getParameters().put('id', lead.id);

			Client_Stage__c csc = new Client_Stage__c();
			csc.Stage_description__c = 'LEAD - Not Interested';
			csc.Agency_Group__c = portalUser.Contact.Account.ParentId;
			csc.Stage_Sub_Options__c = '["Option Not Interested 1","Option Not Interested 2","Option Not Interested 3"]';
			insert csc;

			NewContactPageActivitiesTab controller = new NewContactPageActivitiesTab();
			controller.getEmployeesForAgency();
			controller.getAgencies();
			controller.getTaskPriorities();
			controller.closeAlertTaskSaved();
			controller.getTaskSubjects();
			controller.initNewTask();
			controller.newTask.Due_Date__c = Date.today();
			controller.saveNewNoteTask();
			controller.loadContactChecklist();
			controller.autoCheckPreviousStages = true;
			controller.saveChecklistActivitiesTab();
			controller.updateChecklistActivitiesTab();
			controller.autoCheckPreviousStages = false;
			controller.saveChecklistActivitiesTab();

		}
	}
}