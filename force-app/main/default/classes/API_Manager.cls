global virtual class API_Manager {

	global static String getRequestParam(RestRequest request, String param){
    	return api_manager.isEmpty(request.params.get(param)) ? null : request.params.get(param);
    }

    global static boolean isEmpty(String str){
    	return str != null && str.trim() != '' ? false : true;
    }

    global static map<string, string> getNationalityGroups(String country){
		Map<String, String> nationalityGroupAllShools = new map<string, string>();
		if(country != null){
			for(Nationality_Group__c ng:[Select Account__c, N.Country__c, N.Name from Nationality_Group__c N where Country__c = :country])
				nationalityGroupAllShools.put(ng.Account__c, ng.name);
		}
		return nationalityGroupAllShools;

	}

	global static boolean isBadRequest(List<String> requiredFields){
		for(String str : requiredFields)
			if(isEmpty(str))
				return true;

		return false;
	}

	global static String getExceptionMessage(Exception e){
		String HTMLbreak = '<br/>';
		return 'Apex script exception by user/organization: ' + Userinfo.getUserID() + '/' + UserInfo.getOrganizationId() + HTMLbreak + 'Caused by: ' + e.getTypeName() + '. ' + e.getMessage() + HTMLbreak + e.getStackTraceString();
	}

	global static void sendExceptionEmail(String body){
		IPFunctions.sendEmail('EHF API', 'develop@educationhify.com', 'EHF API', 'develop@educationhify.com', 'EHF API Exception', body);
	}


	@future
	global static void logCall(String userid, Date callDate, String country, String city, String courseCategory){
		if( !isEmpty(userid) && callDate != null && !isEmpty(country) && !isEmpty(city) && !isEmpty(courseCategory) ){
			try {
				List<APILog__c> logs = [select count__c from APILog__c where User__c = :userid and Date__c = :callDate and Country__c = :country and City__c = :city and Course_Category__c = :courseCategory];
				if(logs != null && !logs.isEmpty()){
					logs[0].count__c++;
					update logs;
				} else{
					APILog__c log = new APILog__c(User__c = userid, Date__c = callDate, Country__c = country, City__c = city, Course_Category__c = courseCategory, Type__c = 'search');
					insert log;
				}

			} catch (Exception e){ system.debug('@Error logging call: ' + getExceptionMessage(e)) ; }
		}
	}
    
    
    @future
    global static void logProductCall(String userid, Date callDate, Set<String> campusCourses, String nationality){
        if( !isEmpty(userid) && callDate != null && campusCourses != null && !campusCourses.isEmpty() ){
            try {
                
                List<APILog__c> logs = new List<APILog__c>();
                for(APILog__c log : [select Campus_Course__c, count__c from APILog__c where User__c = :userid and Date__c = :callDate and Campus_Course__c in :campusCourses and Nationality__c = :nationality]){
                    campusCourses.remove(log.Campus_Course__c);
                    log.Count__c++;
                    logs.add(log);
                }
                if(!campusCourses.isEmpty()){
                    for(String ccid : campusCourses)
						logs.add(new APILog__c(User__c = userid, Date__c = callDate, Campus_Course__c = ccid, Type__c = 'product', Nationality__c = nationality));
                }                
                upsert logs;
				
            } catch (Exception e) { system.debug('@Error logging call: ' + getExceptionMessage(e)) ; }
        }
    }



	global static final integer CODE_OK = 200;
	global static final String DESC_OK = 'OK';

	global static final integer CODE_BAD_REQUEST = 400;
	global static final String DESC_BAD_REQUEST = 'BAD_REQUEST';

	global static final integer CODE_INTERNAL_SERVER_ERROR = 500;
	global static final String DESC_INTERNAL_SERVER_ERROR = 'INTERNAL_SERVER_ERROR';

    global static final double LOCATION_ONSHORE = 1;
    global static final double LOCATION_OFFSHORE = 2;
    global static final double LOCATION_ONSHORE_OFFSHORE = 3;




    /* INNER CLASSES */


    global class School {
		public String id;
		public String name;
		public Decimal ranking;
		public Decimal studentRanking;
	}

	global class Campus {
		public String id;
		public String name;
		public String country;
		public String city;
		public boolean favourite;
	}

	global class Course{
		public School schoolDetails;
		public Campus campusDetails;
		public CourseDetails courseDetails;
	}

	global class CourseDetails {

		public String campusCourseId;
		public boolean onlySoldInBlocks;
		public String name;
		public String category;
		public String type;
		public String field;
		public String unitType;

		public String formOfStudy;
		public String period;
		public double hoursPerWeek;
		public double optionalHoursPerWeek;
		public String languageLevelRequired;

		public integer courseLengthInMonths;
		public integer courseLengthRemainingWeeks;

		public String courseCurrency;
		public double units;
		public double totalUnits;
		public double pricePerUnit;
		public double totalTuition;
		public double totalPrice;
		public double totalSavings;
		public double totalExtraFees;
		public double totalExtraFeePromotions;
		public double extraFreeWeek;
		public double freeUnits;
		public double fullPrice;
		public List<extraFee> extraFees;
		public List<ExtraFeePromotion> promotions;

		public boolean hasInstalments;
		public double firstInstalmentValue;
		public double numberRemainingInstalments;
		public double instalmentsValue;

	}

	global class ExtraFeePromotion {
		public String name;
		public double type;

		public String feeName;
		//public double feeDiscountValue;

		public Integer quantity;
		//public Integer fromUnits;
		public double value;
	}

	global class ExtraFee {
		public String name;
		public double units;
		public double pricePerUnit;
		public double totalPrice;
		public String unitType;
		public String type;
		public boolean allowChangeUnits;
		public List<ExtraFee> relatedFees;
	}



	global class SchoolProduct {
		public String campusCourseId;
		public List<ExtraFee> extraFees;
	}

	global class Agency{
		public String id;
		public String name;
		public String country;
		public String state;
		public String city;
		public String street;
		public String postalCode;
		public String phone;
		public String website;
		public String email;
		public String openingHours;
		public decimal latitude;
		public decimal longitude;

	}

    global class CampusSupplier {
    	public String id;
    	public String name;
    	public Decimal agencyranking;
		public Decimal studentRanking;

    }

    global class AgencyCurrency {
    	public String agencyID;
    	public String agencyCurrencyCode;
    	public List<CurrencyRate> rates;
    }

    global class CurrencyRate {
    	public String code;
    	public decimal value;
    }
}