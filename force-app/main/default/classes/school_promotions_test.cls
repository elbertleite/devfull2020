@isTest
global without sharing class school_promotions_test {
    static testMethod void myUnitTest() {
        TestFactory tf = new TestFactory();
        
        Account agency = tf.createAgency();
        Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
        
        Map<String,String> recordTypes = new Map<String,String>();
        for( RecordType r :[select id, name from recordtype where isActive = true] ){
            recordTypes.put(r.Name,r.Id);
        }

        Contact client = tf.createClient(agency);

        Account schoolGroup = tf.createSchoolGroup();
        
        Account school = tf.createSchool();
        school.parentId = schoolGroup.id;
        update school;

        Account campus = tf.createCampus(school, agency);

        Course__c course = tf.createCourse();
        Campus_Course__c cc = tf.createCampusCourse(campus, course);

        Course_Price__c price = tf.createCoursePrice(cc, 'Published Price');

        Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;

        Course_Extra_Fee__c cef = tf.createCourseExtraFee(cc, 'Published Price', p);
        cef.To__c = Decimal.valueOf(20);

        update cef;

        Course_Extra_Fee_Dependent__c dep = tf.createCourseRelatedExtraFee(cef, p);

        dep.From__c = Decimal.valueOf(20);

        update dep;

        Quotation__c quote = tf.createQuotation(client, cc);

        Deal__c coursepromo = tf.createCourseExtraFeePromotion(cc, 'Published Price', p);
        Deal__c freeunit = tf.createCourseFreeUnitsPromotion(cc, 'Published Price');

        Start_Date_Range__c sdr = new Start_Date_Range__c();
		sdr.Promotion__c = coursepromo.id;
		sdr.Campus__c = cc.Campus__c;
		sdr.From_Date__c = system.today();
		sdr.To_Date__c = system.today().addDays(30);
		sdr.Value__c = 125;
		insert sdr;

        Start_Date_Range__c sdr2 = new Start_Date_Range__c();
		sdr2.Promotion__c = freeunit.id;
		sdr2.Campus__c = cc.Campus__c;
		sdr2.From_Date__c = system.today();
		sdr2.To_Date__c = system.today().addDays(30);
		sdr2.Number_of_Free_Weeks__c = 2;
		insert sdr2;

        new school_promotions();

        school_promotions.init(true, null, null, null, null);
        school_promotions.retrieveFromCountry('Australia', true);
        school_promotions.retrieveFromSchool(school.ID, 'Australia', true);
        school_promotions.retrieveCourseTypes(school.ID, null, true);
        school_promotions.retrieveInfoToAddPromotion(school.ID);


        school_promotions.Promotion promo = new school_promotions.Promotion(coursepromo, 'extra-fee-discount', null);
        school_promotions.Promotion free = new school_promotions.Promotion(freeunit, 'free-unit', null);

        school_promotions.StartDate start = new school_promotions.StartDate(sdr, 'extra-fee-discount', promo.id);
        school_promotions.StartDate start2 = new school_promotions.StartDate(sdr2, 'free-unit', free.id);
        
        school_promotions.search('free-unit', 'Australia', school.id, new List<String>(), new List<String>(), new List<String>(), new List<String>(), new List<String>(), null, null, null);
        school_promotions.search('extra-fee-discount', 'Australia', school.id, new List<String>(), new List<String>(), new List<String>(), new List<String>(), new List<String>(), 'course', null, null);
        school_promotions.search('extra-fee-discount', 'Australia', school.id, new List<String>(), new List<String>(), new List<String>(), new List<String>(), new List<String>(), 'all', null, null);
        school_promotions.search('extra-fee-discount', 'Australia', school.id, new List<String>(), new List<String>(), new List<String>(), new List<String>(), new List<String>(), 'campus', null, null);

        school_promotions.updatePromotions('extra-free-discount', new List<school_promotions.Promotion>{promo}, new List<school_promotions.StartDate>{start});
        school_promotions.updatePromotions('free-unit', new List<school_promotions.Promotion>{free}, new List<school_promotions.StartDate>{start2});

        school_promotions.retrieveExtraFeeDiscountObjectForUpdate(promo, 'extra-free-discount');
        school_promotions.retrieveExtraFeeDiscountobject(promo, 'Published price', campus.id, p.id, cc.id);
        school_promotions.retrieveFreeUnitObject(free, 'Published price', cc.id);

        promo.nationalityGroups = new List<String>{'Published Price'};
        school_promotions.clonePromotions(school.ID, 'free-unit', new List<String>{freeunit.ID}, new List<String>(), promo);

        start.id = null;
        school_promotions.saveNewStartDate('extra-fee-discount', start, new List<String>{promo.id});

        promo.id = null;
        promo.courses = new List<String>{course.id};
        promo.campuses = new List<String>{campus.id};
        promo.nationalityGroups = new List<String>{'Published Price'};
        promo.products = new List<string>{p.id};
        school_promotions.saveNewPromotion('extra-fee-discount', promo);

        //school_promotions.deletePromotions(new List<String>{coursepromo.id, freeunit.id}, new List<String>{sdr2.id, sdr.id});

    }
}