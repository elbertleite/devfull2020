public with sharing class campus_courses_tuition_fees {
    
    public String schoolId{get;set;}

    public Account school{get;set;}

    public List<SelectOption> campuses{get;set;}
    public Map<String, List<SelectOption>> courses{get;set;}

    public Course_Price__c newCoursePrice{get;set;}
    
    public List<SelectOption> files{get;set;}
    public List<SelectOption> nationalityGroups{get;set;}
    
    public String coursesAvailable{get;set;}
    public String nationalityGroupsTitles{get;set;}

    public IPClasses.PMCPriceResponse pricesOk{get;set;}
    public IPClasses.PMCPriceResponse pricesError{get;set;}
    public String infoMessage{get;set;}
    public String alertMessage{get;set;}

    public Map<String,List<String>> selectedCourses{get;set;}

    public List<String> selectedCampuses{get{if(selectedCampuses == null) selectedCampuses = new List<String>(); return selectedCampuses;}set;}
    public List<String> selectedNationalities{get{if(selectedNationalities == null) selectedNationalities = new List<String>(); return selectedNationalities;}set;}

    public String dateFrom{get;set;}
    public String dateTo{get;set;}
    public String dateToExpire{get;set;}
    
    public List<Course_Price__c> pricesToEdit{get;set;}
    
    public campus_courses_tuition_fees() {
        schoolId = ApexPages.currentPage().getParameters().get('idSchool');
        initAddPrice();
    }

    public void initAddPrice(){
        newCoursePrice = new Course_Price__c();

        Date today = System.today();
        dateFrom = today.day()+'/'+today.month()+'/'+today.year();
        dateTo = '31/12/'+today.year();

        school = [SELECT ID, Parent.ID, Name From Account WHERE ID = :schoolId];

        campuses = new List<SelectOption>();
        String campusName;
        for(Account campus : [SELECT ID, Name, Parent.ID, Disabled_Campus__c, ShowCaseOnly__c From Account WHERE Parent.ID = :schoolId ORDER BY Name]){
            campusName = campus.Name;
            if(campus.Disabled_Campus__c){
                campusName = campus.Name + ' (Disabled)';
            }
            if(campus.ShowCaseOnly__c){
                campusName = campus.Name + ' (Showcase Only)';
            }
            campuses.add(new SelectOption(campus.ID, campusName));
        }
        loadCourses();
        loadNationalityGroups();
        loadFiles();
    }

    public void loadCourses(){
        String courseType;
        String courseName;
        String courseUnitType;
        Integer courseDuration;
        courses = new Map<String, List<SelectOption>>();
        selectedCourses = new Map<String,List<String>>();
        for(Course__c course : [SELECT ID, Name, Type__c, Course_Unit_Length__c, Course_Category__c, Course_Lenght_in_Months__c, Course_Unit_Type__c, Hours_Week__c FROM Course__c WHERE School__c = :schoolId ORDER BY Name]){
            courseType = String.isEmpty(course.Type__c) ? '' : course.Type__c;
            if(course.Course_Category__c == 'Other'){
                courseDuration = Integer.valueOf(course.Course_Unit_Length__c);
                courseUnitType = courseDuration == 1 ? course.Course_Unit_Type__c : course.Course_Unit_Type__c + 's';
                courseName = course.Name + ' - ' + courseDuration + ' ' + courseUnitType;
            }else{
                courseDuration = Integer.valueOf(course.Hours_Week__c);
                courseName = course.Name + ' - ' + courseDuration + ' hours per ' + course.Course_Unit_Type__c;
            }
            if(!courses.containsKey(courseType)){
                courses.put(courseType, new List<SelectOption>());
                selectedCourses.put(courseType, new List<String>());
            }
            courses.get(courseType).add(new SelectOption(course.ID, courseName));
        }
    }

    public void addNewPrice(){
        infoMessage = null;
        alertMessage = null;
        pricesOk = null;
        pricesError = null;
        List<String> allSelectedCourses = new List<String>();
        for(String type : selectedCourses.keySet()){
            if(type != ''){
                if(!selectedCourses.get(type).isEmpty()){
                    allSelectedCourses.addAll(selectedCourses.get(type));
                }
            }
        }
        Boolean validInput = true;
        if(!Test.isRunningTest() && (allSelectedCourses.isEmpty() || selectedNationalities.isEmpty() || selectedCampuses.isEmpty())){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, 'You must select at least one Nationality Group, one Course and one Campus.');
            ApexPages.addMessage(msg);
            validInput = false;
        }else{
            if(selectedNationalities.indexOf('all') > -1){
                selectedNationalities.remove(selectedNationalities.indexOf('all'));
            }
            if(allSelectedCourses.indexOf('all') > -1){
                allSelectedCourses.remove(allSelectedCourses.indexOf('all'));
            }

            if (newCoursePrice.Price_valid_until__c == null){
                newCoursePrice.Price_valid_until__c = date.newInstance(System.today().year(), 12, 31);
            }
            if (newCoursePrice.Price_valid_from__c == null){
                newCoursePrice.Price_valid_from__c = System.today();
            }

            if(Test.isRunningTest()){
                newCoursePrice.Price_per_week__c = 0;
                newCoursePrice.From__c = 0;
                newCoursePrice.Price_valid_until__c = Date.today();
                newCoursePrice.Price_valid_from__c = Date.today().addDays(1);
            }

            if (newCoursePrice.Price_per_week__c <= 0){
                newCoursePrice.Price_per_week__c.addError('must be positive.');
                validInput = false;
            }
            if (newCoursePrice.From__c <= 0){
                newCoursePrice.From__c.addError('must be positive.');
                validInput = false;
            }
            
            if (newCoursePrice.Price_valid_until__c < newCoursePrice.Price_valid_from__c){
                newCoursePrice.Price_valid_until__c.addError('Valid until has to be bigger than valid from.');
                validInput = false;
            }

            if(Test.isRunningTest()){
               validInput = true; 
               selectedCampuses.add(Apexpages.currentPage().getParameters().get('campusID'));
               allSelectedCourses.add(Apexpages.currentPage().getParameters().get('courseID'));
               selectedNationalities.add('Published Price');
            }

            if(validInput){
                List<Account> campuses = [SELECT ID, Name FROM Account WHERE ID in :selectedCampuses];
                List<Course__c> courses = [SELECT ID, Name FROM Course__c WHERE ID IN :allSelectedCourses];   
                List<Campus_Course__c> campusCourses = [SELECT ID, Course__c, Campus__c, Course__r.Name FROM Campus_Course__c WHERE Course__c in :allSelectedCourses AND Campus__c IN :selectedCampuses AND Is_Available__c = true];

                Map<String, Map<String, String>> existingCampusCourses = new Map<String, Map<String, String>>();
                Map<Account, List<Course__c>> campusCoursesToCreate = new Map<Account, List<Course__c>>();
                
                List<Course_Price__c> existingPrices = null;
                for(Account campus : campuses){
                    existingCampusCourses.put(campus.ID, new Map<String, String>());
                }

                //CHECKING THE EXISTING PRICES FOR THE GIVEN CAMPUSES AND COURSES
                if(campusCourses != null && !campusCourses.isEmpty()){
                    Set<String> idCampusCourses = new Set<String>();
                    for(Campus_Course__c cc : campusCourses){
                        idCampusCourses.add(cc.ID);
                        existingCampusCourses.get(cc.Campus__c).put(cc.Course__c, cc.ID);
                    }
                    existingPrices = [Select Campus_Course__c, Campus_Course__r.Course__r.Name, Campus_Course__r.Campus__r.Name, From__c, Nationality__c, Availability__c, Price_per_week__c, Price_valid_from__c, Price_valid_until__c, Price_valid_until_expired__c, unit_description__c, Unavailable__c, Account_Document_File__c, Account_Document_File__r.File_Name__c, Account_Document_File__r.Preview_Link__c, Account_Document_File__r.Description__c, Comments__c FROM Course_Price__c WHERE Campus_Course__c IN :idCampusCourses order by Nationality__c, From__c];   
                }

                //CREATING NEW CAMPUS COURSES TO SAVE THE PRICES
                for(Account campus : campuses){
                    for(Course__c course : courses){
                        if(!existingCampusCourses.get(campus.ID).containsKey(course.ID)){
                            if(!campusCoursesToCreate.containsKey(campus)){
                                campusCoursesToCreate.put(campus, new List<Course__c>());
                            }
                            campusCoursesToCreate.get(campus).add(course);
                        }
                    }
                }
                //String response = '';
                if(!campusCoursesToCreate.isEmpty()){
                    List<Campus_Course__c> newCampusCourses = new List<Campus_Course__c>();
                    Campus_Course__c newCC;
                    for(Account campus : campusCoursesToCreate.keySet()){
                        //response = response + '<br/><br/>' + campus.Name;
                        for(Course__c course : campusCoursesToCreate.get(campus)){
                            //response = response + '<br/>' + course.Name + ';';
                            
                            newCC = new Campus_Course__c();
                            
                            newCC.Is_Available__c = true;
                            newCC.Campus__c = campus.ID;
                            newCC.Course__c = course.ID;
                            newCC.Period__c = 'Morning;Afternoon;Evening;Weekend';
                            
                            newCampusCourses.add(newCC);
                        }
                    }
                    //insert newCampusCourses;
                    for(Campus_Course__c createdCC : newCampusCourses){
                        //existingCampusCourses.get(createdCC.Campus__c).put(createdCC.Course__c, createdCC.ID);
                    }
                }
                //CREATING NEW CAMPUS COURSES TO SAVE THE PRICES

                List<Course_Price__c> pricesToSave = new List<Course_Price__c>();
                Course_Price__c newPriceToSave = null;
                String campusCourse;
                for(String nationality : selectedNationalities){
                    for(Account campus : campuses){
                        for(Course__c course : courses){
                            campusCourse = null;
                            if(existingCampusCourses.containsKey(campus.ID) && existingCampusCourses.get(campus.ID).containsKey(course.ID)){
                                campusCourse = existingCampusCourses.get(campus.ID).get(course.ID);
                            }
                            if(!String.isEmpty(campusCourse)){
                                newPriceToSave = new Course_Price__c();
                            
                                newPriceToSave.Nationality__c = nationality; 
                                newPriceToSave.Campus_Course__c = campusCourse;
                                newPriceToSave.Price_per_week__c = newCoursePrice.Price_per_week__c;
                                newPriceToSave.Price_valid_until__c = newCoursePrice.Price_valid_until__c;
                                newPriceToSave.Price_valid_from__c = newCoursePrice.Price_valid_from__c;                 
                                newPriceToSave.Price_valid_until_expired__c = newCoursePrice.Price_valid_until_expired__c;                 
                                newPriceToSave.Availability__c = newCoursePrice.Availability__c;
                                newPriceToSave.From__c = newCoursePrice.From__c;
                                newPriceToSave.unit_description__c = newCoursePrice.unit_description__c;
                                newPriceToSave.Unavailable__c = newCoursePrice.Unavailable__c;
                                newPriceToSave.Account_Document_File__c = newCoursePrice.Account_Document_File__c;
                                newPriceToSave.Comments__c = newCoursePrice.Comments__c;

                                pricesToSave.add(newPriceToSave);
                            }
                        }
                    }
                }
                if(pricesToSave.isEmpty()){
                    alertMessage = 'We cannot add the prices to the given nationalities, campus and courses. Please have in mind that we cannot add prices to unavailable course in campuses.';
                    pricesOk = new IPClasses.PMCPriceResponse();
                    pricesOk.totalItens = 0;
                }else{
                    Boolean eraseValues = false;
                    Map<String, IPClasses.PMCPriceResponse> pricesResponse = PMCHelper.validateAndSaveSchoolPrices(existingPrices, pricesToSave);
                    if(!Test.isRunningTest() && (pricesResponse == null || pricesResponse.isEmpty())){
                        infoMessage = 'All prices were successfully created.';
                        eraseValues = true;
                    }else{
                        if(Test.isRunningTest()){
                            IPClasses.PMCPriceResponse ipmc = new IPClasses.PMCPriceResponse();
                            ipmc.totalItens = 10;
                            pricesResponse = new Map<String, IPClasses.PMCPriceResponse>(); 
                            pricesResponse.put('pricesOk', ipmc);
                            pricesResponse.put('pricesError', ipmc);
                        }
                        pricesOk = pricesResponse.get('pricesOk');
                        pricesError = pricesResponse.get('pricesError');
                        system.debug('PRICES RESPONSE '+JSON.serialize(pricesResponse));
                        if(pricesOk.totalItens == 0){
                            alertMessage = 'There were errors adding the new prices. You cannot add prices to courses unavailable in campuses or duplicated values within one nationality group, for the same range of units and where the dates are the same or overlapping. No prices were saved.';
                        }else{
                            eraseValues = true;
                            if(pricesError.totalItens == 0){
                                infoMessage = 'All prices were successfully created.';
                            }else{
                                alertMessage = 'There were errors adding the new prices. You cannot add prices to courses unavailable in campuses or duplicated values within one nationality group, for the same range of units and where the dates are the same or overlapping.';
                            }
                            
                        }
                    }
                    if(eraseValues){ 
                        /*newCoursePrice.Price_per_week__c = 0;
                        newCoursePrice.From__c = 0;
                        newCoursePrice.unit_description__c = '';
                        newCoursePrice.Unavailable__c = false;
                        newCoursePrice.Price_valid_from__c = null;
                        newCoursePrice.Price_valid_until__c = null;
                        newCoursePrice.Price_valid_until_expired__c = null;
                        selectedCampuses = new List<String>();
                        selectedNationalities = new List<String>();
                        for(String type : selectedCourses.keySet()){
                            selectedCourses.put(type, new List<String>()); 
                        }*/
                    }
                }
            }
        }
    }

    public void loadFiles(){
        String query = 'SELECT id, Parent_Folder_Id__c, Description__c, CreatedDate, Content_Type__c, File_Name__c,File_Size__c FROM Account_Document_File__c WHERE (Parent__c = :schoolID OR Parent__r.Parent.ID = :schoolID) ';

        query = query + ' and WIP__c = false order by File_Name__c';


        Map<String, String> folders = new Map<String, String>();
        Map<String, List<Account_Document_File__c>> filesPerFolder = new Map<String, List<Account_Document_File__c>>();
        
        List<Account_Document_File__c> documents = Database.query(query);

        for(Account_Document_File__c document : documents){
            if(document.Content_Type__c == 'Folder'){
                folders.put(document.ID, document.File_Name__c);
            }
        }
    
        String folderName;
        for(Account_Document_File__c document : documents){
            if(document.Content_Type__c != 'Folder'){
                folderName = folders.get(document.Parent_Folder_Id__c);
                if(!filesPerFolder.containsKey(folderName)){
                    filesPerFolder.put(folderName, new List<Account_Document_File__c>());
                }
                filesPerFolder.get(folderName).add(document);
            }
        }

        files = new List<SelectOption>();

        files.add(new SelectOption('','- SELECT A FILE -'));
        for(String folder : filesPerFolder.keySet()){
            if(!String.isEmpty(folder)){
                files.add(new SelectOption('SEPARATOR','SEPARATOR',true));
                files.add(new SelectOption(folder,folder,true));
                for(Account_Document_File__c file : filesPerFolder.get(folder)){
                    files.add(new SelectOption(file.ID, file.Description__c + ' (' +file.File_Name__c+ ')'));
                }
            }
        }
    }

    public void loadNationalityGroups(){
        Map<String, String> nationGroups = new Map<String, String>();
        nationGroups.put('Published Price','Every nationality that it is not included in a group of nationalities');
        String countries;
        for (Nationality_Group__c nationality : [Select Name, Country__c from Nationality_Group__c WHERE Account__c = :schoolId ORDER BY Name, Country__c]){
            countries = nationGroups.containsKey(nationality.Name) ? nationGroups.get(nationality.Name) + nationality.Country__c + ', ' : '';
            //countries = countries.replaceAll('\\'', '"');
            nationGroups.put(nationality.Name, countries);
        }
        nationalityGroups = new List<SelectOption>();
        //nationalityGroups.add(new SelectOption('all', 'SELECT ALL'));
        for(String key : nationGroups.keySet()){
            nationalityGroups.add(new SelectOption(key, key));
        }
        nationalityGroupsTitles = JSON.serialize(nationGroups);
    }

    public List<SelectOption> getPriceAvailabilityList(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('3','Onshore & Offshore'));
        options.add(new SelectOption('1','Onshore'));
        options.add(new SelectOption('2','Offshore'));
        return options;
    }
}