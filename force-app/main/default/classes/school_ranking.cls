public without sharing class school_ranking {
	
	public String agencyID {get;set;}
	public String agencyGroupID {get;set;}
	private String webAgencyID;
	
	public school_ranking(){
		setAgencyInfo();
	}
	
	
	private void setAgencyInfo(){		
		User u = [select contact.accountid, contact.account.parentid from user where id = :UserInfo.getUserId()];
		agencyID = u.contact.accountid;
		agencyGroupID = u.contact.account.parentid;
		
		try {
			webAgencyID = [select id, name from account where Parentid = :agencyGroupID and view_web_leads__c = true LIMIT 1].id;
		} catch (Exception e){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'You don\'t have any web agency. Please contact support.');
       	 	ApexPages.addMessage(myMsg);
		}
		
	}
    
    public String selectedDestinationCountry {get;set;}
	private List<SelectOption> destinationCountries;
	public List<SelectOption> getDestinationCountries(){
		if(destinationCountries == null){
			
			destinationCountries = new List<SelectOption>();
			destinationCountries.add(new SelectOption('all', 'All'));
			
			for(AggregateResult ar : [select Supplier__r.BillingCountry from Supplier__c where Record_Type__c = 'campus' and agency__c = :agencyID and available__c = true group by Supplier__r.BillingCountry order by Supplier__r.BillingCountry])
				destinationCountries.add(new SelectOption( (String) ar.get('BillingCountry') , (String) ar.get('BillingCountry') ) );					
			
			
			
		}
		
		return destinationCountries;
	}
	
	public String selectedDestinationCity {get;set;}
	private List<SelectOption> destinationCities;
	public List<SelectOption> getDestinationCities(){
		if(destinationCities == null || (selectedDestinationCountry != null && selectedDestinationCountry != '') ){
			
			destinationCities = new List<SelectOption>();
			destinationCities.add(new SelectOption('all', 'All'));
			
			if(selectedDestinationCountry != 'all')
				for(AggregateResult ar : [select Supplier__r.BillingCity from Supplier__c 
											where Record_Type__c = 'campus' and agency__c = :agencyID and Supplier__r.BillingCountry = :selectedDestinationCountry and available__c = true 
											group by Supplier__r.BillingCity order by Supplier__r.BillingCity])
					destinationCities.add(new SelectOption( (String) ar.get('BillingCity') , (String) ar.get('BillingCity') ) );					
			
		}
		
		return destinationCities;
	}
	
	
	public String selectedRanking {get;set;}
	private List<SelectOption> rankingOptions;
	public List<SelectOption> getRankingOptions(){
		if(rankingOptions == null){
			rankingOptions = new List<SelectOption>();
			rankingOptions.add(new SelectOption('all', 'All'));
			rankingOptions.add(new SelectOption('-1', 'Not Ranked'));
			rankingOptions.add(new SelectOption('0', '0+ Stars'));
			rankingOptions.add(new SelectOption('1', '1+ Star'));
			rankingOptions.add(new SelectOption('2', '2+ Stars'));
			rankingOptions.add(new SelectOption('3', '3+ Stars'));
			rankingOptions.add(new SelectOption('4', '4+ Stars'));
			rankingOptions.add(new SelectOption('5', '5 Stars'));
		}
		return rankingOptions;
	}
	
	
	public String selectedSchool {get;set;}
	private List<SelectOption> schools;
	public List<SelectOption> getSchools(){
		
		schools = new List<SelectOption>();
		schools.add(new SelectOption('all', 'All'));
		
		String sql = 'select Supplier__r.ParentID,  Supplier__r.Parent.Name from Supplier__c where Record_Type__c = \'campus\' and agency__c = \'' + agencyID + '\' ';
		
		if(selectedDestinationCountry != null && selectedDestinationCountry != 'all')
			sql += ' and Supplier__r.BillingCountry = \'' + selectedDestinationCountry + '\' ';
		
		if(selectedDestinationCity != null && selectedDestinationCity != 'all')
			sql += ' and Supplier__r.BillingCity = \'' + selectedDestinationCity + '\' ';
		
		sql += ' and available__c = true group by Supplier__r.ParentID, Supplier__r.Parent.Name order by Supplier__r.Parent.Name';
		
		
		for(AggregateResult ar : Database.query(sql))
			schools.add(new SelectOption( (String) ar.get('ParentID') , (String) ar.get('Name') ) );					
		
		return schools;
	}
	
	
	public Map<String, Map<String, School>> suppliers {get;set;}
	public Map<String, Integer> countryTotals {get;set;}
	public boolean foundSuppliers {get{if(foundSuppliers == null) foundSuppliers = false; return foundSuppliers;}set;}
	public String noSuppliersMSG {get;set;}
	
	public void search(){
		
		foundSuppliers = false;	
		suppliers = new Map<String, Map<String, School>>();
		countryTotals = new Map<String, Integer>(); 
		
		String sql = 'SELECT Agency__c, Supplier__r.BillingCountry, Supplier__r.Parent.Name, Supplier__r.ParentId, Rank__c FROM Supplier__c where Record_Type__c = \'campus\' and agency__c = \'' + agencyID + '\' and available__c = true and Supplier__r.Disabled_Campus__c=false ';
		
		if(selectedDestinationCountry != null && selectedDestinationCountry != 'all')
			sql += ' and Supplier__r.BillingCountry = \'' + selectedDestinationCountry + '\' ';
		
		if(selectedDestinationCity != null && selectedDestinationCity != 'all')
			sql += ' and Supplier__r.BillingCity = \'' + selectedDestinationCity + '\' ';
		
		if(selectedSchool != null && selectedSchool != 'all')
			sql += ' and Supplier__r.ParentID = \'' + selectedSchool + '\' ';
			
		if(selectedRanking != null && selectedRanking != 'all'){
			decimal rank = Decimal.valueOf(selectedRanking);
			decimal rankNextInt = rank + 1;
			
			if(rank == -1)
				sql += ' and (Rank__c = 0 or Rank__c = null)';
			else if(rank == 0)
				sql += ' and (Rank__c > :rank and Rank__c < :rankNextInt)';
			else
				sql += ' and (Rank__c >= :rank and Rank__c < :rankNextInt) ';
		}
		
		sql += ' order by Supplier__r.BillingCountry, Supplier__r.Parent.Name, Supplier__r.Name ';
		
		for(Supplier__c sp : Database.query(sql)){
			
			if(suppliers.containsKey(sp.Supplier__r.BillingCountry)){
				
				if(suppliers.get(sp.supplier__r.BillingCountry).containsKey(sp.supplier__r.parent.Name)){
					suppliers.get(sp.supplier__r.BillingCountry).get(sp.supplier__r.parent.Name).suppliers.add(sp);
				} else {
					suppliers.get(sp.supplier__r.BillingCountry).put(sp.supplier__r.parent.Name, createSchool(sp));
					countryTotals.put(sp.Supplier__r.BillingCountry, countryTotals.get(sp.Supplier__r.BillingCountry)+1);
				}				
				
			} else {
				suppliers.put(sp.Supplier__r.BillingCountry, new Map<String, School>{ sp.supplier__r.parent.name => createSchool(sp) });
				countryTotals.put(sp.Supplier__r.BillingCountry, 1);					
			}
					
		}
		
		
		if(suppliers.isEmpty()){
			foundSuppliers = false;
			noSuppliersMSG = 'There are no schools available.';
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, noSuppliersMSG);
            ApexPages.addMessage(myMsg);
		} else {
			foundSuppliers = true;			
		}
		
	}
	
	
	private School createSchool(Supplier__c sp){
		School sc = new School();
		sc.id = sp.supplier__r.parentid;
		sc.name = sp.supplier__r.Parent.Name;
		sc.rank = sp.rank__c == null ? 0 : sp.rank__c;
		sc.suppliers = new List<Supplier__c>{sp};
		return sc;
	}
	
	public class School {
		public String id {get;set;}
		public String name {get;set;}
		public Decimal rank {get;set;}
		public List<Supplier__c> suppliers;
	}
	
	
	
	
	public void save(){
		
		List<Supplier__c> sps = new List<Supplier__c>();
		Map<String, Decimal> rankingMap = new Map<String, Decimal>();
		
		for(String country : suppliers.keySet())
			for(String schoolid : suppliers.get(country).keySet()){
				School sc = suppliers.get(country).get(schoolid);
				for(Supplier__c sp : sc.suppliers){
					sp.rank__c = sc.rank;
					sps.add(sp);
					rankingMap.put(sp.supplier__c, sc.rank);
				}		
			}
		
		
		update sps;	
		
		//update other agencies in the group.
		Map<String, Account> otherAgencies = new Map<String, Account>([select id from Account where ParentId = :agencyGroupID and id != :agencyID and recordtype.name = 'Agency']);
		Database.executeBatch(new batch_supplier( otherAgencies.keySet(), 'Rank__c', rankingMap ));
			
		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Items saved successfully.');
        ApexPages.addMessage(myMsg);
			
	}
	
	
	
	
	
	
	
	
	
	
	
	
    
}