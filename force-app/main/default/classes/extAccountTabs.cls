public class extAccountTabs{
    
	public String currTab{get;set;} 
	public String playerTab{get;set;}
    
	public String s {
		get{        
        
		PageReference page = ApexPages.currentPage(); 
		Map<string, string> keys = page.getHeaders(); 
		s=keys.get('Referer');
        
		return s;
            
	}
		set;}

	/* sss */
	private  Account acco;
	

	public String getParam(String name) {
		return ApexPages.currentPage().getParameters().get(name);   
	}
    
	public String getpageUrl() {
		return Url.getCurrentRequestUrl().getPath().substringBefore('/apex');
	}
	
	
	String accoID; 
	public extAccountTabs(ApexPages.StandardController stdController){


		acco = [select id, Name,  User__c , ParentID, RecordTypeId  from account where id = :stdController.getRecord().id];
		accoID = acco.id;
		
		/*if(acco != null && acco.Original_Agency__c == null && userDetails != null) 
			acco.Original_Agency__c = userDetails.Agency__pc;*/
		if(userDetails != null)
			acco.OwnerId = userDetails.User__c;	
		
		if(ApexPages.currentPage().getParameters().get('quoteID') != null )
			currTab = 'quoteEmail';
		else currTab = getParam('currTab');
		if(currTab==null || currTab=='') 
			currTab = 'campusshowcase';
        
		playerTab = getParam('playerTab');
		if(playerTab==null || playerTab=='') 
			playerTab = 'Gallery';
        
        
	}
	
	/*public Integer numberOfDocuments {
		get {
			
			try {
				numberOfDocuments = acco.Client_Documents__r.size() + acco.Client_visas__r.size();			
			} catch(Exception e){
				numberOfDocuments = 0;
			}
			
			return numberOfDocuments;
		}
		Set;
	}*/
	
    
	public Account getAcco(){
        
		acco =  [Select 
		A.Id,
		A.Name,
		A.parent.Name, 
		A.CreatedById,
		A.CreatedDate,		
		A.LastModifiedById,
		A.LastModifiedDate,		
		//A.PersonTitle,
		A.ParentId, 
		A.RecordTypeId,
		//A.Agency__pc,		
		//A.Full_Name__pc,
		A.Destination__c,
		A.City_Destination__c,
		A.Expected_Travel_date__c, 
		(Select Id from Social_Networks__r),
		//(Select Id from Forms_of_Contact__r),
		(Select Id from Addresses__r),
		//(Select Id from Client_Family_Friends__r),
		//(Select Id from AccounttoAccounts__r),
		//(Select Id from AccounttoAccounts1__r),
		(Select Id from Campus_Courses__r),
		//(Select Id from Flight_Details__r),
		//(Select ID from Client_visas__r),
		(Select Id from Bank_Details1__r),
		//(Select Id from Educational_History__r),
		//(Select Id from Employments_History__r),
		//(Select Id from Client_Document1__r),
		//(Select Id from Account_Extra_Information_File__r),
		(Select Id from Videos__r),
		//(Select Id from cg__Account_Files__r) ,
		//(Select Id from Emails__r where Read__c = false and Received__c = true),
		//(Select Id from Client_Documents__r),
		(Select Id from Account_Document_Files__r where WIP__c = false),
		(Select Id from Account_Picture_Files__r where WIP__c = false and Content_Type__c LIKE 'image/%'),
		(Select Id from Account_Logos_File__r where WIP__c = false and Content_Type__c LIKE 'image/%')
		//(Select Id from Partners_Relationship__r)
            
		from Account A
			where id= :accoID];
		return acco;
	}
	
	/*public Integer agreements {
		get {
			List<Account_Agreement_File__c> aafs = [Select id from Account_Agreement_File__c where parent__c = :acco.id];
			return aafs.size();
		}
		Set;
	}*/
	
	public Account testimonials {
		get{
			if(testimonials == null){
				testimonials =  [Select A.Id, (Select Id from Testimonials__r) from Account A where id= :accoID];
			}
			return testimonials;
		}
		set;
	}
    
	public Note[] getNotes() {
		return  [Select N.Id from Note N WHERE N.ParentId = :acco.id And N.IsDeleted=false ]; 
	}
    
    

	public List<String> AboutAsList
	{
		get {if(AboutAsList==null)
		{
			Account a = [SELECT About__c FROM Account where id=:acco.id];
            
			if (a.About__c!=Null && a.About__c!='')

				AboutAsList =  a.About__c.split('\n');
		}
		return AboutAsList;}
    
		set;}
    
    
	public List<String> SellingPointsAsList
	{
		get {if(SellingPointsAsList==null)
		{
			Account a = [SELECT Selling_Points__c FROM Account where id=:acco.id];
            
			if (a.Selling_Points__c!=Null && a.Selling_Points__c!='')

				SellingPointsAsList =  a.Selling_Points__c.split('\n');
		}
		return SellingPointsAsList;}
    
		set;}
    
    
    
	public List<Address__c> getAddresses()
	{
		return [Select 
			A.Account__c, A.Name, A.Address_Type__c, A.City__c,
		A.Comments__c, A.Country__c, A.CreatedById, A.CreatedDate,
		A.IsDeleted, A.LastModifiedById, A.LastModifiedDate, A.Post_Code__c,
		A.Id, A.State__c, A.Street__c, A.Suburb__c, A.SystemModstamp
			from Address__c A
		where A.Account__r.id in :campuses order by Name];
	}
    
	public List<Campus_Course__c> getCourses()
	{
		return [Select
			C.Campus__c, C.Name, C.Is_Available__c, C.Available_From__c, C.Course__c, 
		C.LastModifiedById, C.LastModifiedDate, C.OwnerId, C.Id, 
		(  Select Name, Price_per_week__c from Course_Prices__r LIMIT 1)
		from Campus_Course__c C 
			where Campus__c = :acco.ID];
	}
    
    
    
	public List<Address__c> getLocation()
	{
		return [Select 
			A.Street__c, A.Post_Code__c, A.City__c, A.State__c, A.Country__c
			from 
		Address__c A
		where 
			Account__r.id = :acco.ID
			limit 1
		];
	}
    
	public List<Video__c> getVideos()
	{
		return [
		Select 
			V.Account__c, V.Id, V.Name, V.VideoID__c, V.LastModifiedDate 
			From 
		Video__c V
		Where 
		Account__c = :acco.ID
			Order by
			V.LastModifiedDate DESC
		Limit 1
		];
	}
    
    
    

    
    
	public List<SchoolTestimonial__c> getSchoolTestimonials()
	{
		List<SchoolTestimonial__c> Testimonials = new List<SchoolTestimonial__c>();
        
		Testimonials = [Select 
			S.Content__c, S.CreatedById, S.CreatedDate, 
		S.IsDeleted, S.LastModifiedById, S.LastModifiedDate, S.Id, 
		S.School__c, S.Name, S.Student__c, S.SystemModstamp, 
		S.Student__r.Name/*, S.Student__r.FirstName, S.Student__r.Full_Name__pc, S.Student__r.LastName*/
			From 
		SchoolTestimonial__c S 
		Where 
			S.School__c = :acco.ID 
			ORDER BY 
			S.LastModifiedDate DESC];
    
		return Testimonials; 

                
	}   
    
    
    
	//return the user's agency
	public Account userDetails{
		get{
		if (userDetails == null)
			try{
				userDetails = [select id,User__c from Account where user__c = :UserInfo.getUserId()];
			}catch(Exception e){
				userDetails = null;
			}
		return userDetails;
	}
		set;
	}
    
	//private string idAgency = UserDetails == null ?'':UserDetails.Agency__pc;
    
	public List<Account> campuses {
		get{if(campuses==null)
			campuses = new List<Account>();
        
		campuses = [Select 
			A.Id, 
		A.Name, 
		A.Website,
		A.BusinessEmail__c,
		A.General_Description__c,
		A.How_to_get_there__c
			from Account A
		WHERE 
			A.Parent.id = :acco.id and A.RecordType.Name = 'Campus' order by name];
        
		return campuses;
    
	}
		set;
    
	}
    
	public List<Account> SiblingCampuses {
        
		get{if(SiblingCampuses==null)
			SiblingCampuses = new List<Account>();
        
		SiblingCampuses = [Select 
			A.Id, 
		A.Name, 
		A.Website
			from Account A
		WHERE 
			A.ParentId = :acco.ParentID and A.RecordType.Name = 'Campus' and Id != :acco.Id];
		//and id in (Select Supplier__c from Supplier__c S where Agency__c = :idAgency and record_type__c = 'campus')
        
		return SiblingCampuses;
    
	}
		set;
    
	}
    
	
}