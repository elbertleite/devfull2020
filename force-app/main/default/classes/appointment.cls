public with sharing class appointment {
    public appointment() {}

    @RemoteAction
    @ReadOnly
    public static Map<String, Object> init(String agency){
        Map<String, Object> response = new Map<String, Object>();
        Map<String, String> departments = new Map<String, String>();
        Map<String, String[]> departmentServices = new Map<String, String[]>();
        Account agencyFound = [SELECT Name, ID, Phone, BusinessEmail__c, showcase_timezone__c, BillingStreet, BillingPostalCode, BillingCity, BillingState, BillingCountry FROM Account WHERE ID = :agency ];
        for(Department__c dp:[SELECT Agency__c, Agency__r.name, Id, Name,Services__c FROM Department__c where agency__c  = :agency and Inactive__c = false and Allow_Booking__c = true]){
            departments.put(dp.Id, dp.Name);
            if(!String.isEmpty(dp.Services__c)){
                departmentServices.put(dp.Id, dp.Services__c.split(';'));
            }
        }
        Map<String, List<EmployeeAppointment>> employees = new Map<String, List<EmployeeAppointment>>();
        for(User user : [Select Id, AccountId, ContactId, Contact.Name, Contact.Department__c, Contact.Department__r.name, SmallPhotoURL, Break_From__c, Break_To__c, Contact.Title, Work_from__c, Work_to__c from User where Contact.AccountId = :agency and Contact.RecordType.name = 'Employee' and Contact.Department__c != null and isActive = true and Contact.Chatter_Only__c = false ORDER BY Contact.Name]){
            if(!employees.containsKey(user.contact.Department__c)){
                employees.put(user.contact.Department__c, new List<EmployeeAppointment>());
            }
            employees.get(user.contact.Department__c).add(new EmployeeAppointment(user));
        }
        response.put('agency', agencyFound);
        response.put('employees', employees);
        response.put('departments', departments);
        response.put('departmentServices', departmentServices);
        return response;
    }

    @RemoteAction
    public static Object bookAppointment(Map<String, Object> clientDetails, Map<String, Object> bookingDetails, List<String> services){
        List<String> errors = new List<String>(); 
        Integer validAppointment = 200;
        //String [] clientMandatoryFields = new String[]{'firstName', 'lastName', 'mobilePhone', 'email'};
        Map<String, String> mandatoryFields = new Map<String, String>();
        mandatoryFields.put('firstName','First Name is mandatory.');
        mandatoryFields.put('lastName','Last Name is mandatory.');
        mandatoryFields.put('mobilePhone','Mobile Phone is mandatory.');
        mandatoryFields.put('email','Email is mandatory.');
        for(String field : mandatoryFields.keySet()){
            if(!clientDetails.containsKey(field) || String.isEmpty((String)clientDetails.get(field))){
                errors.add(mandatoryFields.get(field));
            }
        }
        mandatoryFields = new Map<String, String>();
        mandatoryFields.put('employee','Please choose a provider for this appointment.');
        mandatoryFields.put('date','Please pick a date for this appointment in the calendar.');
        mandatoryFields.put('time','Please choose a time for this appointment.');
        for(String field : mandatoryFields.keySet()){
            if(!bookingDetails.containsKey(field) || String.isEmpty((String)bookingDetails.get(field))){
                errors.add(mandatoryFields.get(field));
            }
        }
        if(errors.isEmpty()){
            String employee = (String)bookingDetails.get('employee');
            Date dateAppointment = IPFunctions.getDateFromString((String)bookingDetails.get('date'));    
            String [] tm = ( (String) bookingDetails.get('time') ).split(':');
            Time timeAppointment = Time.newInstance(Integer.valueOf(tm[0]), Integer.valueOf(tm[1]),0,0);

            List<Client_Booking__c> existingAppointments = [SELECT ID, Date_Booking__c, Time_Booking__c FROM Client_Booking__c WHERE User__c = :employee AND Date_Booking__c = :dateAppointment AND Time_Booking__c = :timeAppointment];

            if(!existingAppointments.isEmpty()){
                errors.add('Sorry, the time slot of '+bookingDetails.get('time')+' is not available anymore for the day you choose. Please select another time/day.');
            }
            if(errors.isEmpty()){
                //Save booking
                User user = [Select Id, Break_From__c, Break_To__c, Work_from__c, Work_to__c, Contact.Account.Parent.ID from User WHERE id = :employee];
                String emailClient = (String) clientDetails.get('email');
                list<Contact> listAcco = [Select id, Email from Contact WHERE Email = :emailClient and current_agency__r.ParentId = :user.Contact.Account.Parent.ID limit 1]; 

                String servicesStr = '';

                if(services != null && !services.isEmpty()){
                    for(String service : services){
                        servicesStr += service + ', ';
                    }
                }

                Client_Booking__c cb = new Client_Booking__c();
                cb.Date_Booking__c = dateAppointment;
                cb.User__c = employee;
                cb.Time_Booking__c = timeAppointment;
                cb.Services_Requested__c = servicesStr;
                cb.Mobile_Number__c = '+' + clientDetails.get('mobilePhone');
                cb.Book_Email__c = emailClient;
                cb.Description__c = clientDetails.containsKey('extraInfo') ? '' + clientDetails.get('extraInfo') : null;
                cb.Client_First_Name__c = (String) clientDetails.get('firstName');
                cb.Client_Last_Name__c = (String) clientDetails.get('lastName'); 
                cb.Visa_Expiring_Date__c = IPFunctions.getDateFromString((String)bookingDetails.get('ved'));
                //cb.Email_Confirmation__c = 
                //cb.Contact_Type__c =  
                //cb.Contact_Type_Detail__c =  
                //cb.Facebook__c =                
                if(listAcco != null && !listAcco.isEmpty()){
                    cb.Client__c = listAcco[0].Id;
                }
                insert cb;
            }
        }
        return errors;
    }

    @RemoteAction
    @ReadOnly
    public static Map<String, Object> retrieveEmployeePlanner(String employee, String appointmentDate){
        Integer start = 9;
        Integer finish = 18;
        Date dateAppointment = IPFunctions.getDateFromString(appointmentDate);
        Datetime today = Datetime.now();
        
        Map<String, Object> response = new Map<String, Object>();
        
        if(dateAppointment != null && dateAppointment >= today.date()){
            User user = [Select Id, Break_From__c, Break_To__c, Work_from__c, Work_to__c, Contact.Account.ID from User WHERE id = :employee];
            Account agency = [SELECT ID, Full_Hour_Booking__c FROM Account WHERE ID = :user.Contact.Account.ID];

            Datetime dt = DateTime.newInstance(dateAppointment, Time.newInstance(0, 0, 0, 0));
            String weekday = dt.format('EEEE');

            if(weekday != 'Sunday'){
                if(weekday == 'Saturday'){
                    finish = 13;
                }
                Map<String, List<AppointmentTime>> planner = new Map<String, List<AppointmentTime>>(); 
                planner.put('Morning', new List<AppointmentTime>());
                planner.put('Afternoon', new List<AppointmentTime>());
                planner.put('Evening', new List<AppointmentTime>());
                
                Integer maxItens;
                
                String interval = agency.Full_Hour_Booking__c ? 'hour' : '30min';
                Integer[] minutes;
                if(interval == 'hour'){
                    maxItens = 10;
                    minutes = new Integer[]{00};
                }else if(interval == '30min'){
                    maxItens = 6;
                    minutes = new Integer[]{00,30};
                }else if(interval == '15min'){
                    maxItens = 12;
                    minutes = new Integer[]{00,15,30,45};
                }
        

                Boolean checkBreak = !String.isEmpty(user.Break_From__c) && !String.isEmpty(user.Break_To__c);
                Boolean verifyStartWorkingHour = user.Work_from__c != null && user.Work_to__c != null;
                Boolean onBreak = false;
                Boolean availableSlot;
                Set<String> existingAppointments = new Set<String>();
                String bkhour;
                String appMinute;
                for(Client_Booking__c booking : [SELECT ID, Date_Booking__c, Time_Booking__c FROM Client_Booking__c WHERE User__c = :employee AND Date_Booking__c = :dateAppointment]){
                    bkhour = booking.Time_Booking__c.hour() >= 10 ? ''+booking.Time_Booking__c.hour() : '0'+booking.Time_Booking__c.hour();
                    appMinute = booking.Time_Booking__c.minute() >= 10 ? ''+booking.Time_Booking__c.minute() : '0'+booking.Time_Booking__c.minute();
                    existingAppointments.add(bkhour + ':' + appMinute);
                }
                
                String dayTime;
                String appointmentTime;
                AppointmentTime newAppointmentTime;
                Integer currentHour = today.hour();
                Integer currentMinute = today.minute();
                for(Integer hour = start; hour < finish; hour++){
                    for(Integer minute : minutes){
                        availableSlot = true;
                        appointmentTime = (hour < 10 ? '0'+hour : ''+hour) + ':' + (minute < 10 ? '0'+minute : ''+minute);
                        if(hour < 12){
                            dayTime = 'Morning';
                        }else if(hour < 18){
                            dayTime = 'Afternoon';
                        }else{
                            dayTime = 'Evening';
                        }
                        newAppointmentTime = new AppointmentTime();

                        if(checkBreak){
                            if(appointmentTime == user.Break_From__c){
                                onBreak = true;
                            }else if(appointmentTime == user.Break_To__c){
                                onBreak = false;
                            }
                            availableSlot = !onBreak;
                            if(onBreak){
                                appointmentTime = 'Break';
                            }
                        }
                        if(availableSlot && existingAppointments.size() > 0){
                            availableSlot = !existingAppointments.contains(appointmentTime);
                        }
                        if(availableSlot && dateAppointment == today.date()){
                            if(currentHour > hour){
                                availableSlot = false;
                            }else if(currentHour == hour && currentMinute > minute){
                                availableSlot = false;
                            }
                        }
                        if(availableSlot && verifyStartWorkingHour){
                            availableSlot = hour >= user.Work_from__c && hour < user.Work_to__c;
                        }
                        newAppointmentTime.appointmentTime = appointmentTime;
                        newAppointmentTime.available = availableSlot;

                        planner.get(dayTime).add(newAppointmentTime);
                    }
                }
                Map<String, List<List<AppointmentTime>>> employeePlanner = new Map<String, List<List<AppointmentTime>>>(); 
                List<AppointmentTime> timeSlots;
                Integer currentElement = 0;
                Integer loops;
                for(String key : planner.keySet()){
                    if(planner.get(key).size() > 0){
                        employeePlanner.put(key, new List<List<AppointmentTime>>());
                        loops = planner.get(key).size() / maxItens;
                        if(math.mod(planner.get(key).size(), maxItens) > 1){
                            loops += 1;
                        }
                        currentElement = 0;
                        for(Integer i = 0; i < loops; i++){
                            timeSlots = new List<AppointmentTime>();
                            for(Integer index = 0; index < maxItens; index++){
                                if(currentElement < planner.get(key).size()){
                                    timeSlots.add(planner.get(key).get(currentElement));
                                }
                                currentElement += 1;
                            }
                            employeePlanner.get(key).add(timeSlots);
                        }
                    }
                }
                response.put('planner', employeePlanner);
                response.put('currentHour', currentHour);
                response.put('existingAppointments', existingAppointments);
            }else{
                response.put('invalidDate', true);
            }  
        }else{
            response.put('invalidDate', true);
        }

        return response;
    }
    
    public class AppointmentTime{
        public String appointmentTime{get;set;}
        public Boolean available{get;set;}
    }

    public class EmployeeAppointment{
        public String id{get;set;}
        public String name{get;set;}
        public String photoUrl{get;set;}
        public String breakFrom{get;set;}
        public String breakTo{get;set;}
        public String workFrom{get;set;}
        public String workTo{get;set;}
        public String department{get;set;}
        public String title{get;set;}
        public EmployeeAppointment(User user){
            this.id = user.Id;
            this.name = user.Contact.Name;
            this.photoUrl = user.SmallPhotoURL;
            this.breakFrom = user.Break_From__c;
            this.breakTo = user.Break_To__c;
            this.department = user.Contact.Department__r.name;
            this.title = user.Contact.title;
            this.workFrom = String.valueOf(user.Work_from__c);
            this.workTo = String.valueOf(user.Work_to__c);
        }
    }
}