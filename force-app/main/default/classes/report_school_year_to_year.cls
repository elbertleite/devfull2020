public without sharing class report_school_year_to_year {

	public String years {get;set;}
	public String schools {get;set;}
	public String destinations {get;set;}

	//CONSTRUCTOR
	public report_school_year_to_year() {

		filters();
		
	}
	private static set<Integer> setYears {get;set;}
	private static map<Integer, map<Integer,rTotals>> prepDefaultMap(Integer endDis, Integer startYear, Integer endYear, Integer disKey, Integer year, Decimal amount){
		setYears = new set<Integer>();
		map<Integer, map<Integer,rTotals>> defaultMap = new map<Integer, map<Integer,rTotals>>();
		for(Integer dis = 1; dis <= endDis; dis++){
			defaultMap.put(dis, new map<Integer,rTotals>());		

			for(Integer i = startYear; i <= endYear; i++){
				defaultMap.get(dis).put(i, new rTotals());
				setYears.add(i);
			}
		}//end for

		if(disKey != null)
			defaultMap.get(disKey).put(year, new rTotals(amount));

		return defaultMap;
	}

	private static Integer endDisplay(String displayBy){
		Integer endDis;
		
		//DISPLAY BY QUARTERS
		if(displayBy == 'quarter'){
			endDis = 4;
		}
		//DISPLAY BY SEMESTER
		else if(displayBy == 'semester'){
			endDis = 2;
		}
		//DISPLAY BY YEAR
		else if(displayBy == 'year'){
			endDis = 1;
		}

		return endDis;
	}

	@RemoteAction
	public static String runReport(String searchType, String searchBy, String school, String groupBy, Integer startYear, Integer endYear, String displayBy){
		User userDetails = [SELECT Contact.Account.Global_Link__c FROM User WHERE Id = :userInfo.getUserId() limit 1];
		String sql;
		String sqlGroupdBy;
		String sqlOrderBy;

		Integer display =  endDisplay(displayBy);

		//SEARCH BY
			if(searchBy == 'destination'){
				sql = 'SELECT client_course__r.campus_course__r.campus__r.Parent.BillingCountry groupNm, ';
				sqlGroupdBy = ' client_course__r.campus_course__r.campus__r.Parent.BillingCountry ';
				sqlOrderBy = ' client_course__r.campus_course__r.campus__r.Parent.BillingCountry desc ';
			}
			else if(searchBy == 'school'){
				sql = 'SELECT client_course__r.campus_course__r.campus__r.Parent.Name groupNm, ';
				sqlGroupdBy = ' client_course__r.campus_course__r.campus__r.Parent.Name ';
				sqlOrderBy = ' client_course__r.campus_course__r.campus__r.Parent.Name desc ';
			}

		//GROUP SEARCH BY
			String varBy;
			String varId = '';
			if(groupBy == 'agencyGroup'){
				if(searchType == 'courses' || searchType == 'paidEnroll' ||  searchType == 'weeksSold'){
					varBy = ' client_course__r.Enroled_by_Agency__r.Parent.Name ';
					varId = ' , client_course__r.Enroled_by_Agency__r.ParentId ';
				}
				else if(searchType == 'weeksPaid' || searchType == 'netTuition'){
					varBy = ' Received_By_Agency__r.Parent.Name ';
					varId = ' , Received_By_Agency__r.ParentId ';
				}
			}
			else if(groupBy == 'campus'){
				varBy = ' client_course__r.campus_course__r.Campus__r.Name ';
				varId = ' , client_course__r.campus_course__r.Campus__c ';
			}
			// else if(groupBy == 'course'){
			// 	varBy = ' client_course__r.campus_course__r.Course__r.Name ';
			// 	varId = ' , client_course__r.campus_course__r.Course__c ';
			// }
			else if(groupBy == 'courseType'){
				varBy = ' client_course__r.campus_course__r.Course__r.Course_Type__c ';
			}

		String selVarId='';
		if(varId != '') selVarId = varId + ' subId';

		//SEARCH TYPE
			if(searchType == 'courses' || searchType == 'paidEnroll'){
				sql += ' ' + varBy + ' subgroup' + selVarId + ',  CALENDAR_YEAR(client_course__r.Enrolment_Date__c) year, CALENDAR_QUARTER(client_course__r.Enrolment_Date__c) quarter, COUNT_DISTINCT(client_course__c) amount ';
				sqlGroupdBy += ' , CALENDAR_YEAR(client_course__r.Enrolment_Date__c), CALENDAR_QUARTER(client_course__r.Enrolment_Date__c), ' + varBy + varId;
				sqlOrderBy += ' , ' + varBy + ' , CALENDAR_YEAR(client_course__r.Enrolment_Date__c), CALENDAR_QUARTER(client_course__r.Enrolment_Date__c) ';
			}
			else if(searchType == 'weeksSold'){
				sql +=  ' ' + varBy + ' subgroup' + selVarId + ', CALENDAR_YEAR(client_course__r.Enrolment_Date__c) year, CALENDAR_QUARTER(client_course__r.Enrolment_Date__c) quarter, SUM(client_course__r.course_length_in_weeks__c) amount ';
				sqlGroupdBy += ' , CALENDAR_YEAR(client_course__r.Enrolment_Date__c), CALENDAR_QUARTER(client_course__r.Enrolment_Date__c), ' + varBy + varId;
				sqlOrderBy += ' , ' + varBy + ' , CALENDAR_YEAR(client_course__r.Enrolment_Date__c), CALENDAR_QUARTER(client_course__r.Enrolment_Date__c) ';
			}
			else if(searchType == 'weeksPaid'){
				sql +=  ' ' + varBy + ' subgroup' + selVarId + ', CALENDAR_YEAR(convertTimezone(Received_Date__c)) year, CALENDAR_QUARTER(convertTimezone(Received_Date__c)) quarter,  SUM(Average_Weeks_Per_Instalment__c) amount ';
				sqlGroupdBy += ' , CALENDAR_YEAR(convertTimezone(Received_Date__c)), CALENDAR_QUARTER(convertTimezone(Received_Date__c)), ' + varBy + varId;
				sqlOrderBy += ' , ' + varBy + ' , CALENDAR_YEAR(convertTimezone(Received_Date__c)), CALENDAR_QUARTER(convertTimezone(Received_Date__c)) ';
			}
			else if(searchType == 'netTuition'){
				sql +=  ' ' + varBy + ' subgroup' + selVarId + ', CALENDAR_YEAR(convertTimezone(Received_Date__c)) year, CALENDAR_QUARTER(convertTimezone(Received_Date__c)) quarter, SUM(Net_Paid_To_School__c) amount ';
				sqlGroupdBy += ' , CALENDAR_YEAR(convertTimezone(Received_Date__c)), CALENDAR_QUARTER(convertTimezone(Received_Date__c)), ' + varBy + varId;
				sqlOrderBy += ' , ' + varBy + ' , CALENDAR_YEAR(convertTimezone(Received_Date__c)), CALENDAR_QUARTER(convertTimezone(Received_Date__c)) ';
			}
			
		//WHERE
			sql += ' FROM client_course_instalment__c WHERE ';

			if(searchBy == 'school' && school != 'none')
				sql+= ' client_course__r.campus_course__r.campus__r.Parent.ParentId = :school AND';

			if(searchType == 'courses' || searchType == 'weeksSold' || searchType == 'paidEnroll'){
				sql += ' client_course__r.Enrolment_Date__c != null AND (CALENDAR_YEAR(client_course__r.Enrolment_Date__c) >= :startYear AND CALENDAR_YEAR(client_course__r.Enrolment_Date__c) <= :endYear)';
				
				if(searchType == 'paidEnroll')
					sql += ' AND Received_Date__c != null AND isCancelled__c = false AND isPaymentConfirmed__c = true ';

				sql+= ' AND client_course__r.Enroled_by_Agency__r.Parent.Global_Link__c = \''+userDetails.Contact.Account.Global_Link__c + '\'';
			}
			else if(searchType == 'weeksPaid' || searchType == 'netTuition')
				sql += ' (CALENDAR_YEAR(convertTimezone(Received_Date__c)) >= :startYear AND CALENDAR_YEAR(convertTimezone(Received_Date__c)) <= :endYear) AND Received_By_Agency__r.Parent.Global_Link__c = \''+userDetails.Contact.Account.Global_Link__c + '\' AND Received_Date__c != null AND client_course__r.Enrolment_Date__c != null  AND isCancelled__c = false AND isPaymentConfirmed__c = true';

			// if(groupBy == 'courseType')
			// 	sql += ' AND client_course__r.campus_course__r.Course__r.Type__c != NULL ';

			// sql += '  AND isMigrated__c = false ';


		// GROUP BY 
			sql += ' GROUP BY ' + sqlGroupdBy;

		// ORDER BY
			sql += ' ORDER BY ' + sqlOrderBy;

	
		system.debug('sql ==>' + sql);



		//SET DEFAULT FOR GROUP TOTALS
		map<String, map<Integer,map<Integer,rTotals>>> totals = new map<String, map<Integer,map<Integer,rTotals>>>();
		if(searchBy == 'destination'){
			for(AggregateResult ar : [SELECT BillingCountry FROM Account WHERE RecordType.Name = 'School Group' group by BillingCountry])
				totals.put((String) ar.get('BillingCountry'), prepDefaultMap(display, startYear, endYear, null, null, null));
		}
		else{
			for(Account ar : [SELECT Name FROM Account WHERE ParentId = :school AND RecordType.Name = 'School'])
				totals.put(ar.Name, prepDefaultMap(display, startYear, endYear, null, null, null));
		}
		//

		//BUILD RESULT
		map<String,map<String, map<Integer,map<Integer,rTotals>>>> mResult = new map<String,map<String, map<Integer,map<Integer,rTotals>>>>();

		Integer displayKey;

		map<String,String> ids = new map<String,String>();
		Decimal reportTotal = 0;
	
		for(AggregateResult ag : Database.query(sql)){

			String groupNm = string.valueOf(ag.get('groupNm'));
			String subGroupNm = string.valueOf(ag.get('subgroup'));
			String subGpId;
			if(groupBy != 'courseType')
				subGpId = string.valueOf(ag.get('subId'));
			Integer year = Integer.valueOf(ag.get('year'));
			Integer quarter = Integer.valueOf(ag.get('quarter'));
			Decimal amount = (decimal) ag.get('amount');

			ids.put(subGroupNm, subGpId);

			if(displayBy == 'year'){
				displayKey = 1;
			}
			else if(displayBy == 'semester'){
				if(quarter == 1 || quarter == 2)
					displayKey = 1;
				else
					displayKey = 2;
			}
			else{
				displayKey = quarter;
			}

			if(!mResult.containsKey(groupNm)){
				mResult.put(groupNm, new map<String, map<Integer, map<Integer, rTotals>>>{subGroupNm => prepDefaultMap(display, startYear, endYear, displayKey, year, amount)});
				system.debug('groupNm==>' + groupNm);
				system.debug('displayKey==>' + displayKey);
				system.debug('year==>' + year);
				system.debug('year==>' + year);
				system.debug('amount==>' + amount);
				totals.get(groupNm).get(displayKey).get(year).amount = amount;
				reportTotal+= amount;
			}
			else if(!mResult.get(groupNm).containsKey(subGroupNm)){
				mResult.get(groupNm).put(subGroupNm, prepDefaultMap(display, startYear, endYear, displayKey, year, amount));
				totals.get(groupNm).get(displayKey).get(year).amount += amount;
				reportTotal+= amount;
				
			}
			else{
				mResult.get(groupNm).get(subGroupNm).get(displayKey).get(year).amount += amount;
				totals.get(groupNm).get(displayKey).get(year).amount += amount;
				reportTotal+= amount;
			}
			
		}//end for

		map<String,String> mNames = new map<String,String>();

		if(searchBy == 'destination'){
			for(String nm : mResult.keyset())
				mNames.put(nm, nm);
		}
		else{
			for(Account ac : [Select Id, Name from Account where name in :mResult.keySet()])
				mNames.put(ac.Name, ac.Id);
		}

		//CALCULATE GROWTH
		list<GroupDetails> lGroupDetail = new list<GroupDetails>();
		list<SubGroupDetails> lSubDetail;
		for(String groupNm : mResult.keySet()){
			lSubDetail = new list<SubGroupDetails>();
			for(String subName : mResult.get(groupNm).keyset()){
				lSubDetail.add(new SubGroupDetails(ids.get(subName), subName, calculateGrowth(mResult.get(groupNm).get(subName))));
			}//end for
			lGroupDetail.add(new GroupDetails(mNames.get(groupNm), groupNm, calculateGrowth(totals.get(groupNm)), lSubDetail));
		}//end for
		
		return JSON.serialize(new ReportResult(reportTotal, display, setYears, lGroupDetail));
	}

	//CALCULATE GROWTH
	private static map<Integer,map<Integer,rTotals>> calculateGrowth(map<Integer,map<Integer,rTotals>> result){
		
		for(Integer dis : result.keySet()){
			for(Integer year : result.get(dis).keySet()){
				if(result.get(dis).containsKey(year-1)){
					Decimal lastYear = result.get(dis).get(year-1).amount;
					Decimal curYear = result.get(dis).get(year).amount;
					Decimal growth;
					if(lastYear > 0)
						growth = ((curYear - lastYear)/lastYear)*100;
					else
						growth = curYear * 100;

					result.get(dis).put(year, new rTotals(curYear, growth));
				}else continue;
			}//end for
		}//end for

		return result;
	}

	public Integer lastYear {get;set;}
	public Integer currYear {get;set;}
	private void filters(){
		// YEARS
		currYear = system.now().year();
		lastYear = system.now().addYears(-1).year();
		List<IPFunctions.opIntWrapper> opInt = new list<IPFunctions.opIntWrapper>();
		for(Integer i = 2016; i <= currYear; i++)
			opInt.add(new IPFunctions.opIntWrapper(i,i));

		years = JSON.serialize(opInt);

		//SCHOOLS
		list<IPFunctions.opWrapper> op = new list<IPFunctions.opWrapper>{new IPFunctions.opWrapper('none', '-- Select School Group --')};
		for(Account a : [SELECT Id, Name FROM Account WHERE RecordType.Name = 'School Group' order by Name])
			op.add(new IPFunctions.opWrapper(a.Id,a.name));
		schools = JSON.serialize(op);
		
		//DESTINATION
		op = new list<IPFunctions.opWrapper>();
		for(AggregateResult a : [SELECT BillingCountry FROM Account WHERE RecordType.Name = 'School' group by BillingCountry ])
			op.add(new IPFunctions.opWrapper(string.valueOf(a.get('BillingCountry')),string.valueOf(a.get('BillingCountry'))));

		destinations = JSON.serialize(op);		
	}

	private class ReportResult{
		private Decimal total {get;set;}
		private Integer endDisplay {get;set;}
		private set<Integer> years {get;set;}
		private list<GroupDetails> groups {get;set;}

		public ReportResult(Decimal total, Integer endDisplay, set<Integer> years, list<GroupDetails> groups){
			this.total = total;
			this.endDisplay = endDisplay;
			this.years = years;
			this.groups = groups;
		}
	}

	private class GroupDetails{
		private String groupId {get;set;}
		private String groupName {get;set;}
		private map<Integer,map<Integer, rTotals>> totals {get;set;}
		private list<SubGroupDetails> subgroup {get;set;}

		public GroupDetails(String groupId, String groupName, map<Integer,map<Integer, rTotals>> totals, list<SubGroupDetails> subgroup){
			this.groupId = groupId;
			this.groupName = groupName;
			this.totals = totals;
			this.subgroup = subgroup;
		}
	}

	private class SubGroupDetails{
		private String objId {get;set;}
		private String name {get;set;}
		private map<Integer,map<Integer, rTotals>> totals {get;set;}

		public SubGroupDetails(String objId, String name, map<Integer,map<Integer, rTotals>> totals){
			this.objId = objId;
			this.name = name;
			this.totals = totals;
		}
	}

	private class rTotals{
		private Decimal amount {get;set;}
		private Decimal growth {get;set;}

		private rTotals(){
			amount = 0;
			growth = null;
		}

		private rTotals(Decimal amount){
			this.amount = amount;
			growth = null;
		}

		private rTotals(Decimal amount, Decimal growth){
			this.amount = amount;
			this.growth = growth;
		}
	}
}