/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TaskController_test {

    static testMethod void myUnitTest() {
    	
    	TestFactory factory = new TestFactory();

		Account agency = factory.createAgency();
		Contact employee = factory.createEmployee(agency);
		Contact client ;
		Quotation__c quote;
		User userEmp = factory.createPortalUser(employee);
		
        Account school = factory.createSchool();
  		Account campus = factory.createCampus(school, agency);
  		Course__c course = factory.createCourse();
		Campus_Course__c cc = factory.createCampusCourse(campus, course);	
        
	    Date dueDate = Date.today();
	    
		system.runAs(userEmp){
			Test.startTest();
			TaskController c = new TaskController();
	        c.cAction= 'List';
	        
	                
	        c.getFilterDueDate();
	        c.getFilterRelated();
	        c.getFilterTasks();
			
			client = factory.createClient(agency); //Client			
			quote = factory.createQuotation(client, cc); //Quotation	
			
	    	c.employeeSelected = UserInfo.getUserId();
	    
	        //GenerateTask
	        c.generateTask(userEmp.id, 'test method task', 'task description', quote.id, dueDate, 'In Progress', userEmp.id, null);
		
			Custom_Note_Task__c t = new Custom_Note_Task__c();
			t.Comments__c='bla bla bla';
			t.Subject__c = 'Follow Up';
			t.Due_Date__c = dueDate;
			t.Assign_To__c =  UserInfo.getUserId();
			t.AssignedOn__c = dueDate;
			t.AssignedBy__c = UserInfo.getUserId();
			t.Status__c = 'In Progress';
			t.Selected__c = true;
			insert t;
			
			List<Custom_Note_Task__c> tasks = new List<Custom_Note_Task__c>();
			tasks.add(t);

			//LimitComments
			c.limitComments();
			
			//PickAddMe
			c.pickAddMe();
			
			//SaveTask
			c.saveTask();
			c.TaskC.Comments__c='bla bla bla';
			c.TaskC.Subject__c = 'Follow Up';
			c.TaskC.Due_Date__c = dueDate;
			c.TaskC.Assign_To__c =  UserInfo.getUserId();
			c.TaskC.AssignedOn__c = dueDate;
			c.TaskC.AssignedBy__c = UserInfo.getUserId();
			c.TaskC.Status__c = 'In Progress';
			c.TaskC.Selected__c = true;
			c.saveTask();
			c.TaskC.Priority__c = '1-High';
			c.saveTask();
			c.assignedEmployees = new List<String>{UserInfo.getUserId()};
			c.saveTask();
			
			
			c.createTask();
						
			//SaveNote	
			c.saveNote();
			
			//CompleteTask
			c.completeTask();
			
			//GetSelectedTasks
			c.getSelectedTasks();
			
			//SetTasksInProgress
			c.setTasksInProgress();
			
			//CompleteSelectedTasks
			c.completeSelectedTasks();
			
			//UpdateNote
			c.taskC = t;
			c.updateNote();
			
			//UpdateTask
			c.TaskC.Priority__c = '2-Medium';
			c.TaskC.Related_Contact__c = client.id;
			c.updateTask();
			
			//ListTasks
			c.listTasks();
			
			c.idClient= userEmp.id;
			c.listTasks();
			
			//TaskDetails
			c.taskId = t.id;
			c.taskDetails();
			
			//SearchTasks
			c.SfilterTasks = 'all';
			c.SfilterSubject = 'Follow Up';
			c.SfilterRelated = 'client';
			c.SfilterDueDate = 'OVERDUE';
			
			c.searchTasks();
			
			//ResetResearch						
			c.resetSearch();
			
			//SetUpTransferTasks
			c.setUpTransferTasks();
			
			//TranferMassTasks
		//	c.getSelectedTaskstoTransfer();
			c.tranferMassTasks();
			
			
			c.getDisablePrevious();
			c.getDisableNext();
			c.getTotal_size();
			c.getPageNumber(); 
			c.getTotalPages();
			
			ApexPages.currentPage().getParameters().put('tk', '['+c.taskId+']');
			c.getSelectedTaskstoTransfer();
			
			Test.stopTest();
		}
		
		
		
		
        
    }
}