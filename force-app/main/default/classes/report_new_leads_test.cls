@isTest
private class report_new_leads_test{
	static testMethod void myUnitTest() {
		TestFactory tf = new TestFactory();
		
		Account agency = tf.createAgency();

		tf.createChecklists(agency.Parentid);

		Contact emp = tf.createEmployee(agency);
		
		Contact lead = tf.createLead(agency, emp);
		lead.Ownership_History_Field__c = '[{"toUserID":"005O0000004bBaSIAU","toUser":"Patricia Greco","toAgencyID":"0019000001MjqIIAAZ","toAgency":"IP Australia Sydney (HO)","fromUserID":null,"fromUser":null,"fromAgencyID":null,"fromAgency":null,"createdDate":"2018-09-18T04:17:12.024Z","createdByUserID":"005O0000004bBaSIAU","createdByUser":"Patricia Greco","action":"Taken"}]';
		lead.Lead_Stage__c = 'Stage 1';
		lead.Lead_Area_of_Study__c = 'Test 1; Test 2';
		update lead;

		Contact lead2 = tf.createClient(agency);
		Contact lead3 = tf.createClient(agency);

		List<String> leads = new List<String>();
		leads.add(lead.ID);
		leads.add(lead2.ID);
		leads.add(lead3.ID);

		Contact client = tf.createClient(agency);

		User portalUser = tf.createPortalUser(emp);

		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);

       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);

		Quotation__c q1 = tf.createQuotation(lead, campusCourse);
		Quotation__c q2 = tf.createQuotation(lead, campusCourse);

		Destination_Tracking__c dt = new Destination_tracking__c();
		dt.Client__c = lead.id;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
		dt.Expected_Travel_Date__c = system.today().addMonths(3);
		dt.lead_Cycle__c = false;
		insert dt;

		Client_Checklist__c checklist = new Client_Checklist__c();
		checklist.Agency__c = agency.id;
		checklist.Agency_Group__c = agency.Parentid;
		checklist.Destination__c = lead.Destination_Country__c;
		checklist.Checklist_Item__c = 'aaa';
		checklist.Client__c = lead.id;
		checklist.Destination_Tracking__c = dt.id;
		insert checklist;

		Test.startTest();
     	system.runAs(portalUser){
		
			emp.Account = agency;
			portalUser.contact = emp;
			update portalUser;

			Client_Stage__c csc = new Client_Stage__c();
			csc.Stage_description__c = 'LEAD - Not Interested';
			csc.Agency_Group__c = portalUser.Contact.Account.Parentid;
			csc.Stage__c = 'Stage 0';
			csc.Stage_Sub_Options__c = '["Option Not Interested 1","Option Not Interested 2","Option Not Interested 3"]';
			insert csc;

			Client_Stage_Follow_up__c cs = new Client_Stage_Follow_up__c();
			cs.Agency__c = portalUser.Contact.Account.id;
			cs.Agency_Group__c = portalUser.Contact.Account.Parentid;
			cs.Client__c = lead.id;
			cs.Destination__c = 'Australia';
			cs.Stage_Item__c = csc.Stage_Description__c;
			cs.Stage_Item_Id__c = csc.id;
			cs.Stage__c = 'Stage 0';
			cs.Destination_Tracking__c = dt.id;
			cs.Last_Saved_Date_Time__c = Datetime.now();
			insert cs;
			
			csc = new Client_Stage__c();
			csc.Stage_description__c = 'Test';
			csc.Agency_Group__c = portalUser.Contact.Account.Parentid;
			csc.Stage__c = 'Stage 0';
			csc.Stage_Sub_Options__c = '["Option Not Interested 1","Option Not Interested 2","Option Not Interested 3"]';
			insert csc;

			cs = new Client_Stage_Follow_up__c();
			cs.Agency__c = portalUser.Contact.Account.id;
			cs.Agency_Group__c = portalUser.Contact.Account.Parentid;
			cs.Client__c = lead.id;
			cs.Destination__c = 'Australia';
			cs.Stage_Item__c = csc.Stage_Description__c;
			cs.Stage_Item_Id__c = csc.id;
			cs.Stage__c = 'Stage 0';
			cs.Destination_Tracking__c = dt.id;
			cs.Last_Saved_Date_Time__c = Datetime.now();
			insert cs;
			
			csc = new Client_Stage__c();
			csc.Stage_description__c = 'Test 2';
			csc.Agency_Group__c = portalUser.Contact.Account.Parentid;
			csc.Stage__c = 'Stage 0';
			insert csc;

			cs = new Client_Stage_Follow_up__c();
			cs.Agency__c = portalUser.Contact.Account.id;
			cs.Agency_Group__c = portalUser.Contact.Account.Parentid;
			cs.Client__c = lead.id;
			cs.Destination__c = 'Australia';
			cs.Stage_Item__c = csc.Stage_Description__c;
			cs.Stage_Item_Id__c = csc.id;
			cs.Stage__c = 'Stage 0';
			cs.Destination_Tracking__c = dt.id;
			cs.Last_Saved_Date_Time__c = Datetime.now();
			insert cs;
			
			csc = new Client_Stage__c();
			csc.Stage_description__c = 'Test 3';
			csc.Agency_Group__c = portalUser.Contact.Account.Parentid;
			csc.Stage__c = 'Stage 0';
			insert csc;

			cs = new Client_Stage_Follow_up__c();
			cs.Agency__c = portalUser.Contact.Account.id;
			cs.Agency_Group__c = portalUser.Contact.Account.Parentid;
			cs.Client__c = lead.id;
			cs.Destination__c = 'Australia';
			cs.Stage_Item__c = csc.Stage_Description__c;
			cs.Stage_Item_Id__c = csc.id;
			cs.Stage__c = 'Stage 0';
			cs.Destination_Tracking__c = dt.id;
			cs.Last_Saved_Date_Time__c = Datetime.now();
			insert cs;
			

			report_new_leads controller2 = new report_new_leads();

			controller2.getGroups();

			controller2.loadAgencies(agency.id, agency.Parentid);

			//controller2.generateReport();
			controller2.generateExcel();

			Apexpages.currentPage().getParameters().put('export', 'true');
			Apexpages.currentPage().getParameters().put('beginDate', '01/01/2001');
			Apexpages.currentPage().getParameters().put('endDate', '01/01/2030');
			Apexpages.currentPage().getParameters().put('groupSelected', portalUser.Contact.Account.Parentid);
			Apexpages.currentPage().getParameters().put('agencySelected', '["'+portalUser.Contact.Account.id+'"]');
			controller2 = new report_new_leads();

			Apexpages.currentPage().getParameters().put('idGroup', portalUser.Contact.Account.Parentid);
			controller2.updateAgencies();

			report_new_leads.ReportResult rr = new report_new_leads.ReportResult();
			rr.monthYear = 'null';
			rr.newLeadOffline = 0;
			rr.newLeadWeb = 0;
			rr.oldLeadNewCycle = 0;
			rr.total = 0;

			rr.hashCode();

			report_new_leads.ContactReport cr = new report_new_leads.ContactReport();
			cr.createdDate = '';
			cr.cycleCreatedDate = '';
			cr.cycleCreatedBy = '';
			cr.contactOrigin = '';
			cr.rdStationCampaign = '';
		}
	}
}