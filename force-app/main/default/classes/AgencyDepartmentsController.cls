public class AgencyDepartmentsController{
	
	public Account agency {get;set;}
	public List<Department__c> departments {get;set;}
	
	public map<String,list<User>> mapDeptUsers {get;set;}

	public List<User> staffList {get{
		if(staffList == null){
			staffList = new list<User>();
			staffList = [Select Id, AccountId, Contact.Name, Contact.Department__c from User where Contact.AccountId = :agency.Id and Contact.RecordType.name = 'Employee' and Contact.Department__c = null and isActive = true and Contact.Chatter_Only__c = false];
		}
		return staffList;
	}set;}
	
	public Department__c newDepartment {  
		get{
			if( newDepartment == null ){
				newDepartment = new Department__c();
			}
			return newDepartment;	
		}
		set;
	}
	
	
	
	
	//Extention Constructor
	public AgencyDepartmentsController(ApexPages.StandardController controller){		
		if(controller.getRecord().id != null){
			agency = [Select id from Account where id = :controller.getRecord().id];
		
			if(agency != null){				
				getDepartmentStaff();
				newDepartment.Agency__c = agency.id;
			}			 
		}		
	}
	
	
	
	public void getDepartmentStaff(){
		
		mapDeptUsers = new map<String, List<User>>();
		departments = new List<Department__c>();
		departments = [Select Id, Name, Services__c, Show_Visits__c from Department__c where Agency__c = :agency.Id and Inactive__c = false];
		for(Department__c d : departments)
			mapDeptUsers.put(string.valueOf(d.Id), new list<User>());
		
		system.debug(':agency.Id====>' + agency.Id);
		
			
		List<User> deptStaff= [Select Id, AccountId, ContactId, Contact.Name, Contact.Department__c from User where Contact.AccountId = :agency.Id and Contact.RecordType.name = 'Employee' and Contact.Department__c != null and isActive = true and Contact.Chatter_Only__c = false];
		
		system.debug('mapDeptUsers====>' + mapDeptUsers.keySet());
		
		for(User u : deptStaff)
			if(u.Contact.Department__c!=null){
				system.debug('u.Contact.Department__c====>' + u);
				if(mapDeptUsers.containsKey(string.valueOf(u.Contact.Department__c)))
					mapDeptUsers.get(string.valueOf(u.Contact.Department__c)).add(u);	
				else
					staffList.add(u);
			}
	}
	
	
	private void refreshDepartments(){
		getDepartmentStaff();
	}
	
	
	public void saveDepartments(){
		
		string deptList = ApexPages.currentpage().getParameters().get('deptList');
		
		if(deptList != null && deptList != ''){
			system.savepoint save;	
		
			try{
				save = database.setSavepoint();
			
				List<String> deptMembers = deptList.split(';');
				List<String> splitDeptMembers;
				List<Contact> updateContacts = new list<Contact>();
				map<string,string> mapUsersDepartment = new map<string,string>();

				for(String d : deptMembers ){
					splitDeptMembers = d.split('-');
					mapUsersDepartment.put(splitDeptMembers[0], splitDeptMembers[1]);
				}
				
				list<User> lUsers = [Select Id, ContactID from User where id in :mapUsersDepartment.keyset()];
				
				for(User u : lUsers){
					Contact c = new Contact(id=u.ContactId, Department__c = mapUsersDepartment.get(u.Id));
					updateContacts.add(c);
				}
				
				//Users that no longer belongs to a department
				set<string> updatedUsersSet = mapUsersDepartment.keyset();
				List<String> freeUsers = new list<String>();
				for(List<User> lu : mapDeptUsers.values())
					for(User u : lu)
						if(!updatedUsersSet.contains(u.Id)){
							Contact c = new Contact(id=u.ContactId, Department__c = null);
							updateContacts.add(c);
						}
			
			
				update updateContacts;
				
			}catch(Exception e){
				database.rollback(save);
				system.debug('Error===' + e);
			}	
		}else{
			//Remove Users that no longer belongs to any department
			List<Contact> updateContacts = new list<Contact>();
			List<String> freeUsers = new list<String>();
			for(List<User> lu : mapDeptUsers.values())
				for(User u : lu){
					Contact c = new Contact(id=u.ContactId, Department__c = null);
					updateContacts.add(c);
				}
			
			update updateContacts;
		}
		
		getDepartmentStaff();
		staffList = null;		
	}
			
	
	
	public void deleteDept(){
		
		String deptID = ApexPages.currentpage().getParameters().get('deptID');
		Department__c dept;
		
		if(deptID != null){
			
			for(Department__c ad : departments)
				if(ad.id == deptID){
					ad.Inactive__c = true;
					update ad;
					break;
				}
		
			list<Contact> contacts = [Select Id, Department__c from Contact where Department__c= :deptID];
			List<Contact> updateContact = new list<Contact>();
			
			for(Contact c: contacts ){
				c.Department__c = null;
				updateContact.add(c);	
			}
			update updateContact;
			
			getDepartmentStaff();
			staffList = null;
		}		
	}
}