@isTest
private class client_course_cancel_prod_paid_test {

	public static User portalUser {get{
	if (null == portalUser) {
	portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
	} return portalUser;} set;}

	@testSetup static void setup() {
		TestFactory tf = new TestFactory();
		tf.setupProdcutsTest();

		Contact client = [Select Id, Accountid From Contact limit 1];
	
		portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
		system.runAs(portalUser){

			system.debug('all deposits=' + [Select Id, Deposit_Total_Available__c, Client__c FROM client_course__c WHERE isDeposit__c = true]);

			//Add product to booking
			ApexPages.currentPage().getParameters().put('id', client.Id);
			products_search ps = new products_search();

			for(String av : ps.result.keySet())
				for(String cat : ps.result.get(av).keyset())
					for(products_search.products p : ps.result.get(av).get(cat)){
					p.product.Selected__c = true;

					for(Quotation_List_Products__c f : p.prodFees)
						f.isSelected__c = true;
				}

			ps.saveProduct();

			//Pay Product
			client_product_service__c cliProduct = [Select Id from client_product_service__c WHERE Related_to_Product__c = null limit 1];

			client_course_product_pay payProd = new client_course_product_pay(new ApexPages.StandardController(cliProduct));

			payProd.newPayDetails.Payment_Type__c = 'Creditcard';
			payProd.newPayDetails.Value__c = payProd.totalPay;
			payProd.addPaymentValue();
			payProd.paidByAgency.Paid_by_Agency__c = true;
			payProd.savePayment();
		}
	}

	static testMethod void cancelPayment(){
		system.runAs(portalUser){

			client_product_service__c cliProduct = [Select Id, client__c from client_product_service__c WHERE Received_Date__c != NULL AND Related_to_Product__c = null limit 1];

			ApexPages.CurrentPage().getParameters().put('id', cliProduct.Id);
			client_course_cancel_product_paid p = new client_course_cancel_product_paid();

			boolean showError = p.showError;
			String errorMsg = p.errorMsg;

			for(client_product_service__c prod : p.product)
				prod.isSelected__c = true;

			p.cancelPayment();

			client_course_product_pay payProd = new client_course_product_pay(new ApexPages.StandardController(cliProduct));

			//Simulate client deposit
			payprod.clientDeposit =  new client_course__c(Client__c = cliProduct.client__c, isDeposit__c = true, Deposit_Total_Available__c = 1000, Deposit_Total_Value__c = 1000, Deposit_Total_Used__c = 0, Deposit_Total_Refund__c = 0, Deposit_Reconciliated__c = true);

			payProd.newPayDetails.Payment_Type__c = 'Deposit';
			payProd.newPayDetails.Value__c = payProd.totalPay;
			payProd.addPaymentValue();
			payProd.savePayment();

			//Cancel payment with deposit
			p = new client_course_cancel_product_paid();
			 for(client_product_service__c prod : p.product)
				prod.isSelected__c = true;

			p.cancelPayment();

		}
	}
}