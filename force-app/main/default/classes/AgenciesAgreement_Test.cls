/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private without sharing class AgenciesAgreement_Test {

    static testMethod void myUnitTest() {
    	
    	TestFactory factory = new TestFactory();
       
		Map<String,String> recordTypes = new Map<String,String>();       
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
		
	    Account school = factory.createSchool();
  		Account agency = factory.createAgency();
  		
  		Account agency2 = new Account();
  		agency2.Name = 'Second agency 2';
  		agency2.recordtypeid = recordTypes.get('Agency');
  		agency2.ParentId = agency.ParentId;
  		insert agency2;
  		
		Account campus = factory.createCampus(school, agency);		
		Contact employee = factory.createEmployee(campus);		
		User userEmp = factory.createPortalUser(employee);
		
	 	 	
 		School_Agreement__c agreement1 = new School_Agreement__c();
       	agreement1.Agency_Agreement__c = agency.Parentid;
       	agreement1.commission__c = 20;
       	agreement1.deal__c ='Deal..';
       	agreement1.School_Manager__c = 'Manager 1';
       	agreement1.School_Account__c = school.Id;
       	agreement1.Signed_on__c = Date.today();
       	agreement1.Valid_till__c = Date.today();
   		agreement1.long__c = String.valueOf(Date.today());
   		agreement1.document_link__c = 'thelink';
       	insert agreement1;
       	
       
       	
	 	System.RunAs(userEmp){ 
	 		
	 		
	 		
	       	//Begin test
			AgenciesAgreementController a = new AgenciesAgreementController();
			AWSKey__c testKey = new AWSKey__c(name=IPFunctions.awsCredentialName,key__c='key',secret__c='secret');
			insert testKey;   
     	  	//ApexPages.currentPage().getParameters().put('agriD', agreement1.id);
			a.setUpSearchPage();
			//a.sShow= 'agreement';
			a.sCountryFilter = 'Australia';
			a.sCityFilter = 'Sydney';
			
			a.setUpCreateAgreement();
			a.agreement = agreement1;
			list<String> docs = a.documents;
			a.saveAgreement();
			a.updateAgreement();
			a.searchAgencies();
			//a.sRegionFilter = '(Australia, Spain)';
			//a.breakCountries();
			
			
			
			String urlprefix = a.urlprefix;
			
			a.setCityFilter();
			a.Next();
			a.Previous();
			a.Beginning();
			a.getDisableNext();
			a.getDisablePrevious();
			a.End();
			a.getTotal_size();
			a.getPageNumber(); 
			a.getTotalPages();
			a.deleteAgreeDocs();
		}
       
    }
}