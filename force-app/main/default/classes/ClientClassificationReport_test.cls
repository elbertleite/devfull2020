/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class ClientClassificationReport_test {

    static testMethod void myUnitTest() {
    	
    	User portalUser = [Select id, name from User where UserType = 'CspLitePortal' and isActive = true and ContactID != null and Contact.Account.Name like '%IP%Sydney%' limit 1];
    	
    	system.runAs(portalUser){
    		
    		ApexPages.currentPage().getParameters().put('userID', portalUser.id);
    		
	    	ClientClassificationReport ccr = new ClientClassificationReport(null);
	    	List<SelectOption> dummy = ccr.agencyGroupOptions;
	    	dummy = ccr.userOptions;
	    	dummy = ccr.agencyOptions;	    	
	    	dummy = ccr.destinations;
	    	dummy = ccr.clientClassifications;
	    	dummy = ccr.searchTypeOptions;
			List<SelectOption> stageOptions = ccr.stageOptions;
	    	
	    	ccr.selectedStageSearch = 'ticked';
	    	
	    	ccr.fromDate.Expected_Travel_Date__c = System.today().addDays(-10);
	    	ccr.toDate.Expected_Travel_Date__c = System.today().addDays(10);
	    	ccr.search();
	    	
	    	ccr.searchAgencies();
	    	ccr.searchUsers();
	    	
	    	ccr.selectedReturnType = 'classification';
	    	
	    	ccr.selectedDestination = 'Canada';
	    	ccr.selectedClassification = 'P10 - (more than 6 months)';
	    	ccr.selectedSearchType = '';
	    	ccr.fromDate.Expected_Travel_Date__c = System.today().addDays(-10);
	    	ccr.toDate.Expected_Travel_Date__c = System.today().addDays(10);
	    	ccr.searchClientsByUser();
	    	
	    	ccr.selectedDestination = 'Australia';
	    	ccr.selectedClassification = '';
	    	ccr.selectedSearchType = 'travelDate';
	    	ccr.fromDate.Expected_Travel_Date__c = System.today().addDays(-10);
	    	ccr.toDate.Expected_Travel_Date__c = System.today().addDays(10);
	    	ccr.searchClientsByUser();
	    	
	    	ccr.getClassficationTotals();
	    	ccr.getClassficationTotalsPerAgency();
	    	
	    	ccr.selectedSearchType = 'quotationCreatedDate';
	    	ccr.fromDate.Expected_Travel_Date__c = System.today().addDays(-10);
	    	ccr.toDate.Expected_Travel_Date__c = System.today().addDays(10);
	    	ccr.searchClientsByUser();
	    	
	    	ccr.getClassficationTotals();
	    	ccr.getClassficationTotalsPerAgency();
	    	
	    	ccr.selectedSearchType = 'createdDate';
	    	ccr.fromDate.Expected_Travel_Date__c = System.today().addDays(-10);
	    	ccr.toDate.Expected_Travel_Date__c = System.today().addDays(10);
	    	ccr.selectedStage = 'Stage 1';
	    	//ccr.selectedStageSearch = 'ticked';
	    	ccr.searchClientsByUser();
	    	
	    	ccr.getClassficationTotals();
	    	ccr.getClassficationTotalsPerAgency();
	    	
	    	ccr.selectedSearchType = 'visaExpiryDate';
	    	ccr.fromDate.Expected_Travel_Date__c = System.today().addDays(-10);
	    	ccr.toDate.Expected_Travel_Date__c = System.today().addDays(10);
	    	ccr.getStages();    	
	    	ccr.selectedStageSearch = 'lastTicked';
	    	ccr.selectedStage = 'Stage 1';	    	
	    	ccr.searchClientsByUser();
	    	
	    	ccr.getClassficationTotals();
	    	ccr.getClassficationTotalsPerAgency();
	    	
	    	ccr.selectedSearchType = 'convertedDate';
	    	ccr.fromDate.Expected_Travel_Date__c = System.today().addDays(-10);
	    	ccr.toDate.Expected_Travel_Date__c = System.today().addDays(10);
	    	ccr.getStages();    	
	    	ccr.selectedStageSearch = 'lastTicked';
	    	ccr.selectedStage = 'Stage 1';	    	
	    	ccr.searchClientsByUser();
	    	
	    	ccr.getClassficationTotals();
	    	ccr.getClassficationTotalsPerAgency();
	    	
	    	ccr.selectedSearchType = 'newsLetter';
	    	ccr.fromDate.Expected_Travel_Date__c = System.today().addDays(-10);
	    	ccr.toDate.Expected_Travel_Date__c = System.today().addDays(10);
	    	ccr.getStages();    	
	    	ccr.selectedStageSearch = 'lastTicked';
	    	ccr.selectedStage = 'Stage 1';	    	
	    	ccr.searchClientsByUser();
	    	
	    	ccr.getClassficationTotals();
	    	ccr.getClassficationTotalsPerAgency();
	    	
	    	ccr.selectedSearchType = 'arrivalDate';
	    	ccr.fromDate.Expected_Travel_Date__c = System.today().addDays(-10);
	    	ccr.toDate.Expected_Travel_Date__c = System.today().addDays(10);
	    	ccr.getStages();
	    	ccr.selectedStage = 'Stage 1';	    	
	    	ccr.searchClientsByUser();
	    	
	    	ccr.getClassficationTotals();
	    	ccr.getClassficationTotalsPerAgency();
	    	
	    	stageOptions = ccr.stageOptions;
	    	ccr.getStageItens();
	    	List<SelectOption> stageItemOptions = ccr.stageItemOptions;
	    	ccr.setStageSearch();
	    	List<SelectOption> stageSearchOptions = ccr.stageSearchOptions;
	    	//dummy = ccr.agencyGroupChecklistOptions;
	    	
	    	
    	} 
    }
}