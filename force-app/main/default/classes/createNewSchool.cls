public with sharing class createNewSchool {

	public String rollbackPage{get;set;}
	public String idRedirect{get;set;}
	
	public Account schoolGroup {get;set;}
	public Account school {get;set;}
	public Account campus {get;set;}

	public String schoolCampusCategory{get;set;}

	public boolean showModalNewSchoolGroup{get{if(showModalNewSchoolGroup == null) showModalNewSchoolGroup = false; return showModalNewSchoolGroup;} set;}

	public list<SelectOption> allCountries {get;set;}
	public list<SelectOption> listSchoolsGroup {get;set;}

	public createNewSchool() {
		rollbackPage = ApexPages.currentPage().getParameters().get('page');
		idRedirect = ApexPages.currentPage().getParameters().get('idRedirect');
		String idGroup = ApexPages.currentPage().getParameters().get('idGroup');
		map<string, string> recordTypes = new map<string, string>();
		for(RecordType rt:[Select id, name from RecordType where name in ('School','Campus','School Group') and isActive = true]){
			recordTypes.put(rt.name, rt.id);
		}
		schoolGroup = new Account(recordtypeid = recordTypes.get('School Group'));
		school = new Account(recordtypeid = recordTypes.get('School'));
		campus = new Account(recordtypeid = recordTypes.get('Campus'));

		if(!String.isEmpty(idGroup)){
			school.parentId = idGroup;
		}

		allCountries = new list<SelectOption>(IPFunctions.getAllCountries());

		listSchoolsGroup  = new list<SelectOption>(IPFunctions.getSchoolsGroup());
	}

	public void openCloseModalNewSchoolGroup(){
		this.showModalNewSchoolGroup = !showModalNewSchoolGroup;
	}

	public PageReference saveSchool(){
		PageReference redirect;
		try{
			upsert school;
			campus.School_campus_category__c = schoolCampusCategory;
			campus.ParentId = school.id;
			upsert campus;

			redirect = Page.school_campus_page;
			redirect.getParameters().put('id', school.id);
			redirect.setRedirect(true);
		}catch(Exception e){
           // ApexPages.addMessages(e);
		   system.debug('Error on saveSchool() ===> ' + e.getLineNumber() + ' <=== '+e.getMessage());
        }
		return redirect;
	}

	public void updateFieldDisabledReason(){
		this.schoolCampusCategory = ApexPages.currentPage().getParameters().get('schoolCampusCategory');
	}

	public void saveSchoolGroup(){
		try{
            upsert schoolGroup;

			school.parentID = schoolGroup.id;

			listSchoolsGroup.add(new SelectOption(schoolGroup.id, schoolGroup.name + ' (' + schoolGroup.billingCountry + ')'));
			this.showModalNewSchoolGroup = !showModalNewSchoolGroup;
        }catch(Exception e){
           // ApexPages.addMessages(e);
		   system.debug('Error on saveSchoolGroup() ===> ' + e.getLineNumber() + ' <=== '+e.getMessage());
        }
	}

	public List<SelectOption> getCurrencies() {
	    List<SelectOption> options = new List<SelectOption>();
	    for(SelectOption op: xCourseSearchFunctions.getCurrencies())
	      options.add(new SelectOption(op.getValue(),op.getLabel()));
	        
	    return options;
	}

	public List<SelectOption> getSchoolCampusCategories(){
		List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Schema.Account.School_campus_category__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple){
			options.add(new SelectOption( f.getValue(), f.getLabel() ));
		}
		return options;
	}
}