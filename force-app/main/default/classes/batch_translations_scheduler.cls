global class batch_translations_scheduler implements Schedulable {

	global void execute(SchedulableContext sc){
		Database.executeBatch(new batch_translations(Userinfo.getSessionID()));
	}
}