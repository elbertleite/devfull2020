public with sharing class School_products {
    private string accoId;
    public School_products(ApexPages.StandardController controller){
        //accoId = controller.getId();
        accoId =  Apexpages.currentPage().getParameters().get('id');
        
        setFeeIds();
    }
    
    public Product__c newProductFee{
        get{
            if(newProductFee == null)
                newProductFee = new Product__c();
            return newProductFee;
        }
        Set;
    }
    
    public Map<String, List<Product__c>> mapProducts {
		get{
			if(mapProducts == null)
				mapProducts = getProducts();
			return mapProducts;
		}
		Set;
	}
	
	private Map<String, List<Product__c>> getProducts(){
		Map<String, List<Product__c>> mprod = new Map<String, List<Product__c>>();
		
		List<Product__c> prds = [Select Accommodation_Meals__c, Accommodation_Room_Type__c, Accommodation_Type__c, Address__c, Allow_change_units__c, Available__c, Description__c, Name__c, Optional__c, People_per_room__c, Name,
										Product_Type__c, Id, Special_conditions__c, Supplier__c, Unit_Type__c, From_Age__c, To_Age__c from Product__c where Supplier__c = :accoId and Product_Type__c != 'Accommodation' order by Product_Type__c, Name__c];
		
		for(Product__c p : prds){
			if(mprod.containsKey(p.Product_Type__c)){
				List<Product__c> lp = mprod.get(p.Product_Type__c);
				lp.add(p);
				mprod.put(p.Product_Type__c, lp);
			} else
				mprod.put(p.Product_Type__c, new List<Product__c>{p});
		}
		
		return mprod;
	}
    
    public Boolean editMode{get{if(editMode == null) editMode = false; return editMode;} Set;}
    
    
    public List<Product__c> getProductsOther(){
        return [Select Accommodation_Meals__c, Accommodation_Room_Type__c, Accommodation_Type__c, Address__c, Allow_change_units__c, Available__c, Description__c, Name__c, Optional__c, People_per_room__c, Name, 
                Product_Type__c, Id, Special_conditions__c, Supplier__c, Unit_Type__c, From_Age__c, To_Age__c, Website_Fee_Type__c from Product__c where Supplier__c = :accoId and Product_Type__c != 'Accommodation' order by Name__c];  
    }
    
    public List<Product__c> getProductsAccommodation(){
        return [Select Accommodation_Meals__c, Accommodation_Room_Type__c, Accommodation_Type__c, Address__c, Allow_change_units__c, Available__c, Description__c, Name__c, Optional__c, People_per_room__c, Name, 
                Product_Type__c, Id, Special_conditions__c, Supplier__c, Unit_Type__c, From_Age__c, To_Age__c, Website_Fee_Type__c from Product__c where Supplier__c = :accoId and Product_Type__c = 'Accommodation' order by Name__c];   
    }
    //Select Accommodation_Meals__c, Accommodation_Room_Type__c, Accommodation_Type__c, Address__c, Allow_change_units__c, Available__c, Description__c, Name__c, Optional__c, People_per_room__c, Name, 
    //Product_Type__c, Id, Special_conditions__c, Supplier__c, Unit_Type__c from Product__c 
    
    public pageReference addProduct(){
        if(newProductFee.Supplier__c == null)
            newProductFee.Supplier__c = accoId;
        upsert newProductFee;
        //if(fileName == 'fileName' || fileName == null || fileName == '')
            newProductFee = new Product__c();
        editMode = false;
        mapProducts = null;
        return null;
    }
    
    public pageReference cancelEdit(){
        newProductFee = null;
        editMode = false;
        return null;
    }
    
    
    public pageReference editProduct(){
        String selectProd =  Apexpages.currentPage().getParameters().get('selProdId');
        newProductFee = [Select Accommodation_Meals__c, Accommodation_Room_Type__c, Accommodation_Type__c, Address__c, Allow_change_units__c, Available__c, Description__c, Name__c, Optional__c, People_per_room__c, Name,
                Product_Type__c, Id, Special_conditions__c, Supplier__c, Unit_Type__c, From_Age__c, To_Age__c from Product__c where id = :selectProd];          
        editMode = true;
        return null;
    }
    
    public boolean foundFees { get { if(foundFees == null) foundFees = false; return foundFees; } Set;}
    
    public List<Campus_Course> listFees {
        get{
            if(listFees == null)
                listFees = new List<Campus_Course>();
            return listFees;
        }
        Set;
    }
    
    public class Campus_Course {
        public String Type {get;Set;}
        public String campus {get;Set;}
        public String course {get;Set;}
    }
    
    public pageReference deleteProduct(){
        foundFees = false;
        String delProduct =  Apexpages.currentPage().getParameters().get('delProduct');
        
        Set<String> Campus_Course = new Set<String>();
        
        if(delProduct != null && delProduct.trim() != ''){
            
            listFees = new List<Campus_Course>();
            
            List<Course_Extra_Fee__c> fees = [Select Campus__r.Name, campus_Course__r.Campus__r.Name, campus_Course__r.Course__r.Name, Product__c  from Course_Extra_Fee__c where Product__c = :delProduct];
            List<Deal__c> promotions = [Select Campus_Account__r.name, campus_Course__r.Campus__r.Name, campus_Course__r.Course__r.Name from Deal__c where Product__c = :delProduct];
            List<Course_Extra_Fee_Dependent__c> feesDependent = [Select Course_Extra_Fee__r.campus__r.name, Course_Extra_Fee__r.campus_course__r.campus__r.name, Course_Extra_Fee__r.campus_Course__r.Course__r.Name, Product__c, Product__r.Name__c from Course_Extra_Fee_Dependent__c where Product__c = :delProduct];
            
            if(!fees.isEmpty() || !promotions.isEmpty() || !feesDependent.isEmpty()){
                
                for(Course_Extra_Fee__c cef : fees){                    
                    String campusName = cef.Campus__r.Name != null ? cef.Campus__r.Name : cef.campus_Course__r.Campus__r.Name;
                    if(!Campus_Course.contains(campusName + '-' + cef.campus_Course__r.Course__r.Name  + '-Fee')){
                        Campus_Course cc = new Campus_Course();
                        cc.campus = campusName;
                        cc.course = cef.campus_Course__r.Course__r.Name;
                        cc.Type = 'Fee';
                        listFees.add(cc);
                        Campus_Course.add(campusName + '-' + cef.campus_Course__r.Campus__r.Name +  '-Fee');
                    }
                        
                }
                
                for(Course_Extra_Fee_Dependent__c df : feesDependent){                    
                    String campusName = df.Course_Extra_Fee__r.campus__r.name != null ? df.Course_Extra_Fee__r.campus__r.name : df.Course_Extra_Fee__r.campus_course__r.campus__r.name;
                    if(!Campus_Course.contains(campusName + '-' + df.Course_Extra_Fee__r.campus_Course__r.Course__r.Name + '-Related Fee')){
                        Campus_Course cc = new Campus_Course();
                        cc.campus = campusName;
                        cc.course = df.Course_Extra_Fee__r.campus_Course__r.Course__r.Name;
                        cc.Type = 'Related Fee';
                        listFees.add(cc);
                        Campus_Course.add(campusName + '-' + df.Course_Extra_Fee__r.campus_course__r.campus__r.name + '-Related Fee');
                    }
                        
                }
                
                for(Deal__c d : promotions){
                	String campusName = d.Campus_Account__r.name != null ? d.Campus_Account__r.name : d.campus_Course__r.Campus__r.Name;
                    if(!Campus_Course.contains(campusName + '-' + d.campus_Course__r.Course__r.Name + '-Promotion')){
                        Campus_Course cc = new Campus_Course();
                        cc.campus = campusName;
                        cc.course = d.campus_Course__r.Course__r.Name;
                        cc.Type = 'Promotion';
                        listFees.add(cc);
                        Campus_Course.add(campusName + '-' + d.campus_Course__r.Campus__r.Name + '-Promotion');
                    }
                        
                }
                
                foundFees = true;
                return null;
                
                
            } else {
                
                delete [select id from Product__c where id = :delProduct];
            
            }
            mapProducts = null;
        }
        
        return null;
        
    }
    
    
    
    List<SelectOption> ageList;
    public List<SelectOption> getAgeList(){
        if(ageList == null){
            ageList = new List<SelectOption>();
            ageList.add(new SelectOption('0','Any'));
            for(Integer i = 10; i <= 100; i++)
                ageList.add(new SelectOption(i+'',i+''));
        }
        return ageList;
    }
    
    public pageReference resetFormParams(){
        if (Apexpages.currentPage().getParameters().get('prodType') != null)
           newProductFee.Product_Type__c = Apexpages.currentPage().getParameters().get('prodType');
        if (Apexpages.currentPage().getParameters().get('prodId') != null)
            newProductFee.Id = Apexpages.currentPage().getParameters().get('prodId');
        if (Apexpages.currentPage().getParameters().get('schoolId') != null)
            accoId= Apexpages.currentPage().getParameters().get('schoolId');
        return null;
        
    }
    
     
    
    //WEBSITE FEES
    public String enrolmentFeeID {get;set;}
    public String materialFeeID {get;set;}
    
    public String homestay_privateRoom {get;set;}
    public String homestay_privateRoom_under18 {get;set;}
    public String homestay_sharedRoom {get;set;}
    public String homestay_sharedRoom_under18 {get;set;}
    public String sharedAccommodation_sharedRoom {get;set;}
    public String sharedAccommodation_sharedRoom_under18 {get;set;}
    public String sharedAccommodation_privateRoom{get;set;}
    public String sharedAccommodation_privateRoom_under18 {get;set;}
    
    public String placementFee {get;set;}
    public String guardianshipFee {get;set;} 
    
    public void setFeeIds(){
    	
		for(Product__c pr : getProductsOther()){    		
    		if(pr.Website_Fee_Type__c == 'Enrolment Fee')
    			enrolmentFeeID = pr.id;
    		else if(pr.Website_Fee_Type__c == 'Material Fee')
    			materialFeeID = pr.id;    		    		
    	}
    	
    	for(Product__c pr : getProductsAccommodation()){
    		
    		if(pr.website_fee_type__c == 'Homestay - Private Room')
    			homestay_privateRoom = pr.id;
	    	else if(pr.website_fee_type__c == 'Homestay - Private Room (under 18yrs)')
	    		homestay_privateRoom_under18 = pr.id;
	    	else if(pr.website_fee_type__c == 'Homestay - Shared Room')
	    		homestay_sharedRoom = pr.id;
	    	else if(pr.website_fee_type__c == 'Homestay - Shared Room (under 18yrs)') 
	    		homestay_sharedRoom_under18 = pr.id;
	    	else if(pr.website_fee_type__c == 'Shared Accommodation - Shared Room')
	    		sharedAccommodation_sharedRoom = pr.id;
	    	else if(pr.website_fee_type__c == 'Shared Accommodation - Shared (under 18yrs)')
	    		sharedAccommodation_sharedRoom_under18 = pr.id;
	    	else if(pr.website_fee_type__c == 'Shared Accommodation - Private Room')
	    		sharedAccommodation_privateRoom = pr.id;
	    	else if(pr.website_fee_type__c == 'Shared Accommodation - Private Room (under 18yrs)')
	    		sharedAccommodation_privateRoom_under18 = pr.id;
	    	else if(pr.website_fee_type__c == 'Placement Fee')
	    		placementFee = pr.id;
	    	else if(pr.website_fee_type__c == 'Guardianship Fee')
	    		guardianshipFee = pr.id;
    	}
    	
    }
    
    
    private List<SelectOption> listOtherProducts {get;set;}
    public List<SelectOption> getListOtherProducts() {
    	if(listOtherProducts == null){
    		listOtherProducts = new List<SelectOption>();
    		listOtherProducts.add(new SelectOption('', '-- Select--'));
    		for(Product__c p : getProductsOther())
    			listOtherProducts.add(new SelectOption(p.id, p.Name__c));
    		
    	}
    	return listOtherProducts;	
    }
    
    
    private List<SelectOption> listAccommodationProducts {get;set;}
    public List<SelectOption> getListAccommodationProducts() {
    	if(listAccommodationProducts == null){
    		listAccommodationProducts = new List<SelectOption>();
    		listAccommodationProducts.add(new SelectOption('', '-- Select--'));
    		for(Product__c p : getProductsAccommodation())
    			listAccommodationProducts.add(new SelectOption(p.id, p.Name__c));
    		
    	}
    	return listAccommodationProducts;	
    }
    
    public void saveWebsiteFees(){
    	dupelist = new Set<String>();
    	
    	if(hasDuplicateIDS()){
    		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'You cannot assign the same fee for multiple items.');
    		ApexPages.addMessage(myMsg);
    		return;
    	}
    		
    	
    	
    	
    	List<Product__c> prds = new List<Product__c>();
    	for(Product__c pr : getProductsOther()){
    		
    		pr.Website_Fee_Type__c = null;
    		
    		if(enrolmentFeeID == pr.id)
    			pr.Website_Fee_Type__c = 'Enrolment Fee';    			
    		else if(materialFeeID == pr.id)
    			pr.Website_Fee_Type__c = 'Material Fee';
    		
    		
    		prds.add(pr);    		
    	}
    	
    	
    	for(Product__c pr : getProductsAccommodation()){
    		pr.Website_Fee_Type__c = null;
    		
    		if(pr.id == homestay_privateRoom)
    			pr.website_fee_type__c = 'Homestay - Private Room';
    		else if(pr.id == homestay_privateRoom_under18)
    			pr.website_fee_type__c = 'Homestay - Private Room (under 18yrs)';
		    else if(pr.id == homestay_sharedRoom)
		    	pr.website_fee_type__c = 'Homestay - Shared Room';
		   	else if(pr.id == homestay_sharedRoom_under18)
		    	pr.website_fee_type__c = 'Homestay - Shared Room (under 18yrs)';    		 
		    else if(pr.id == sharedAccommodation_sharedRoom)
		    	pr.website_fee_type__c = 'Shared Accommodation - Shared Room';    		 
		    else if(pr.id == sharedAccommodation_sharedRoom_under18)
		    	pr.website_fee_type__c = 'Shared Accommodation - Shared (under 18yrs)';    		 
		    else if(pr.id == sharedAccommodation_privateRoom)
		    	pr.website_fee_type__c = 'Shared Accommodation - Private Room';    		
		    else if(pr.id == sharedAccommodation_privateRoom_under18)
		    	pr.website_fee_type__c = 'Shared Accommodation - Private Room (under 18yrs)';
    		else if(pr.id == placementFee)
		    	pr.website_fee_type__c = 'Placement Fee';
		    else if(pr.id == guardianshipFee)
		    	pr.website_fee_type__c = 'Guardianship Fee';			    	
		    
    		prds.add(pr);
    	}
    	
    	
    	update prds;
    	
    	
    }
    
    private Set<String> dupelist = new Set<String>();
    private boolean hasDuplicateIDS(){
    	
    	Set<String> ids = new Set<String>();
    	if(!insertID(enrolmentFeeID) || !insertID(materialFeeID) || !insertID(homestay_privateRoom) || !insertID(homestay_privateRoom_under18) || !insertID(homestay_sharedRoom) || 
    		!insertID(homestay_sharedRoom_under18) || !insertID(sharedAccommodation_sharedRoom) || !insertID(sharedAccommodation_sharedRoom_under18) || !insertID(sharedAccommodation_privateRoom) ||
    			!insertID(sharedAccommodation_privateRoom_under18) || !insertID(placementFee) || !insertID(guardianshipFee) )
    		return true;
    	
    	return false;
    }
    
    private boolean insertID(String str){
    	if(str != null && str != '' && dupelist.contains(str))
    		return false;
    	
    	dupelist.add(str);
    	return true;
    	
    }

     
}