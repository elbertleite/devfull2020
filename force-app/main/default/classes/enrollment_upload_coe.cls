global with sharing class enrollment_upload_coe {

	public String bucket {get{return EnrolmentManager.bucket;} private set;}
	public String courses {get;set;}

	public enrollment_upload_coe() {

		String mainCourse = ApexPages.currentPage().getParameters().get('id');
		list<result> lcourses = new list<result>();

		for(client_course__c cc : [SELECT Client__r.Name, Course_Name__c, School_Name__c, Start_Date__c, End_Date__c, (SELECT Id FROM client_documents__r WHERE Document_Type__c = 'COE' AND Document_Category__c = 'Enrolment' limit 1) FROM client_course__c WHERE id = :mainCourse OR course_package__c = :mainCourse order by course_package__c nulls first, Start_Date__c]){

			if(cc.client_documents__r != null && cc.client_documents__r.size()>0)
				lcourses.add(new result(cc, cc.client_documents__r[0].Id));
			else
				lcourses.add(new result(cc, cc.Id));

		}
		
		courses = JSON.serialize(lcourses);
	}

	private class result{
		private client_course__c course {get;set;}
		private String coeId {get;set;}

		public result(client_course__c course, String coeId){
			this.course = course;
			this.coeId = coeId;
		}
	}

	@RemoteAction
	global static String confirmCOE(List<String> cIds){

		list<Client_Document__c> coe = new list<Client_Document__c>();
		list<client_course__c> courses = new list<client_course__c>();

		for(client_course__c cc : [SELECT Id, Client__c, (SELECT Id, client_course__c FROM client_documents__r WHERE Document_Type__c = 'COE' AND Document_Category__c = 'Enrolment' limit 1) FROM client_course__c WHERE id IN :cIds order by course_package__c nulls first, Start_Date__c]){
			
			//Update course
			cc.COE_Received__c = true;
			cc.COE_Received_On__c =  System.now();
			courses.add(cc);

			if(cc.client_documents__r != null && cc.client_documents__r.size()>0)
				coe.add(cc.client_documents__r[0]);
			else
				coe.add(new Client_Document__c (Client__c = cc.Client__c, Document_Category__c = 'Enrolment', Document_Type__c = 'COE', Client_course__c = cc.Id)); //Create COE doc

		}//end for

		update courses;
		upsert coe;
		

		map<String,String> result = new map<String,String>();
		for(Client_Document__c c : coe){
			result.put(c.client_course__c, string.valueOf(c.Id).substring(0, 15));
		}//end for
	
		return JSON.serialize(result);

	}
}