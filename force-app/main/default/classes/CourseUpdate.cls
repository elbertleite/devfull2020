public with sharing class CourseUpdate{

	public String destination{get{if(destination == null) destination = ''; return destination;} Set;}
	public String destinationCity{get{if(destinationCity == null) destinationCity = ''; return destinationCity;} Set;}
	public map<String,String> mapSelectedSchool{get{if(mapSelectedSchool == null) mapSelectedSchool = new map<String,String>(); return mapSelectedSchool;} Set;}
	
	private List<SelectOption> listDestinations;
	private List<SelectOption> listDestinationsCity;
	
	
	public List<SelectOption> getlistDestinations(){
		if(listDestinations == null){
			listDestinations = new List<SelectOption>();
			listDestinationsCity = new List<SelectOption>();
			CityDestination = new map<String,list<SelectOption>>();
			Set<string> countryDestination = new Set<string>();
			listDestinations.add(new SelectOption('','--Select a Country--'));
			for(AggregateResult acco:[Select BillingCountry, BillingCity from Account where recordType.name = 'Campus' group by BillingCountry, BillingCity order by BillingCountry, BillingCity])
				if(!countryDestination.contains((String)acco.get('BillingCountry'))){
					if(destination == '')
						destination = (String)acco.get('BillingCountry');
					listDestinations.add(new SelectOption((String)acco.get('BillingCountry'),(String)acco.get('BillingCountry')));
					CityDestination.put((String)acco.get('BillingCountry'),new List<SelectOption>{new SelectOption((String)acco.get('BillingCity'),(String)acco.get('BillingCity'))});
					CityDestination.get((String)acco.get('BillingCountry')).add(0,new SelectOption('','--Select a City--'));
					countryDestination.add((String)acco.get('BillingCountry'));
				} else	CityDestination.get((String)acco.get('BillingCountry')).add(new SelectOption((String)acco.get('BillingCity'),(String)acco.get('BillingCity')));
		}
		return listDestinations;
	}	
	
	private map<String,list<SelectOption>> CityDestination;
	public map<String,list<SelectOption>> getCityDestination(){
		if(CityDestination == null){
			CityDestination = new map<String,list<SelectOption>>();
			getlistDestinations();
		}
		return CityDestination;
	}
	
	
	public String selectedSchool{get{if(selectedSchool == null) selectedSchool = ''; return selectedSchool;} Set;}
	private List<SelectOption> listSchools;
	public List<SelectOption> getlistSchools(){
		if(listSchools == null && destinationCity != ''){
			listSchools = new List<SelectOption>();
			listSchools.add(new SelectOption('','--Select a School--'));
			for(AggregateResult school:[Select ParentId schoolId, Parent.Name school from Account where recordType.name = 'Campus' and BillingCountry = :destination and BillingCity = :destinationCity group by ParentId, Parent.Name order by Parent.Name]){
				listSchools.add(new SelectOption((String)school.get('schoolId'),(String)school.get('school')));
			}
		}
		return listSchools;
	}
	
	public String selectedCampus{get{if(selectedCampus == null) selectedCampus = ''; return selectedCampus;} Set;}
	private List<SelectOption> listCampus;
	public List<SelectOption> getlistCampus(){
		if(listCampus == null && selectedSchool != ''){
			listCampus = new List<SelectOption>();
			listCampus.add(new SelectOption('','--Select a Campus--'));
			for(Account campus:[Select id, Name from Account where parentid = :selectedSchool order by Name]){
				listCampus.add(new SelectOption(campus.id, campus.name));
				mapSelectedSchool.put(campus.id, campus.name);
			}
		}
		return listCampus;
	}
	
	public String selectedOption{get{if(selectedOption == null) selectedOption = ''; return selectedOption;} Set;}
	private List<SelectOption> Options;
	public List<SelectOption> getOptions(){
		if(Options == null){
			Options = new List<SelectOption>();
			Options.add(new SelectOption('','--Select an Option--'));
			Options.add(new SelectOption('price','--Prices--'));
			Options.add(new SelectOption('extraFee','--Extra Fees--'));
			Options.add(new SelectOption('promotion','--Promotions--'));
			Options.add(new SelectOption('courses','--Courses--'));
			Options.add(new SelectOption('report','--Report--'));
		}
		return Options;
	}

	public PageReference openUpdateDetails(){
		selectedOption = ApexPages.currentPage().getParameters().get('option');
		return null;	
	}
	
	public void refreshScools(){
		listSchools = null;
		listCampus = null;
	}
	
	public void refreshCampus(){
		listCampus = null;
	}
	
	public String getSfInstance(){
	  return ApexPages.currentPage().getHeaders().get('Host');
	}
}