/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class FileUpload_test {
	
	static testMethod void myUnitTest() {

		TestFactory tf = new TestFactory();
		Account agency = tf.createAgency();
		Contact ct = tf.createClient(agency);

		Client_Document__c cd = new Client_Document__c(Client__c = ct.id, Document_Category__c = 'Test', Document_Type__c = 'Test');
		insert cd;
		AWSKey__c cred = new AWSKey__c(name='s3Amazon',key__c='key',secret__c='secret');
		insert cred;

		FileUpload.getAuthorizationObject('ehfdev', 'folder', 'filename.txt', 'txt', '');
		FileUpload.listFiles('ehfdev', 'folder', true,'Preview_Link__c', cd.id);
		FileUpload.deleteObject('ehfdev', 'folder', 'filename.txt');
		AWSHelper.getObjectMetadata(cred.key__c, cred.secret__c, 'ehfdev', 'folder', 'filename.txt');

	}

}