public without sharing class report_cancelled_courses {

	public list<client_course__c> result {get;set;}

	private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}
	private User currentUser {get{if(currentUser==null) currentUser = ff.currentUser; return currentUser;}set;}

	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}
	
	public report_cancelled_courses() {

		selectedAgency = 'all';
	
		findCourses();
	}

	public void findCourses(){
		String fromDate = IPFunctions.FormatSqlDateTimeIni(dateFilter.Original_Due_Date__c); 
		String toDate = IPFunctions.FormatSqlDateTimeFin(dateFilter.Discounted_On__c);

		String sql = 'SELECT Status_Formula__c, Cancelled_on__c, Cancelled_By__r.Name, Cancelled_By__r.Contact.Account.Name, Client__c, Client__r.Name, Course_Name__c, Campus_Name__c, Start_Date__c, End_Date__c, Enroled_by_Agency__r.Name, Credit_Reason__c, Credit_Description__c, course_cancel_request__r.Credit_Reason__c , course_cancel_request__r.Credit_Description__c FROM client_course__c ';
		
		if(selectedAgency != 'all')
			sql += ' WHERE Enroled_by_Agency__c = \'' + selectedAgency + '\'';
		else
			sql += ' WHERE Enroled_by_Agency__r.ParentId = \'' + selectedAgencyGroup + '\'';

		if(selectedTypeFilter == 'range'){
			sql += ' AND Cancelled_on__c >= ' + fromDate;
			sql += ' AND Cancelled_on__c <= ' + toDate;
		}
		else if(selectedTypeFilter == 'THIS_MONTH')
			sql += ' AND Cancelled_on__c = THIS_MONTH ';
		else if(selectedTypeFilter == 'LAST_MONTH')
			sql += ' AND Cancelled_on__c = LAST_MONTH ';


		sql += ' AND isCancelled__c = true AND isCancelRequest__c = false order by Cancelled_on__c desc';

		system.debug('sql==>' + sql);

		result = Database.query(sql);

	}

	public string selectedAgencyGroup{
    	get{
    		if(selectedAgencyGroup == null)
    			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
    		return selectedAgencyGroup;
    	}set;}

	private set<Id> allGroups {get{if(allGroups == null){agencyGroupOptions = null;}return allGroups;}set;}
	public List<SelectOption> agencyGroupOptions {
  		get{
   			if(agencyGroupOptions == null){
				allGroups = new set<id>();
   				agencyGroupOptions = new List<SelectOption>();
   				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group
					agencyGroupOptions.add(new SelectOption(currentUser.Contact.Account.ParentId, currentUser.Contact.Account.Parent.Name));
					selectedAgencyGroup = currentUser.Contact.Account.ParentId;
					allGroups.add(currentUser.Contact.Account.ParentId);
   				}
				   else{
   					for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name]){
		     			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
						allGroups.add(ag.Id);
		    		}//end for
   				}
	   		}
	   		return agencyGroupOptions;
	  }set;}

	public void changeGroup(){
		selectedAgency = '';
		getAgencies();
	}

	public String selectedAgency {get;set;}
	public list<Id> allAgencies {get;set;}
	public list<SelectOption> getAgencies(){
		List<SelectOption> result = new List<SelectOption>();
		allAgencies = new list<Id>();
		if(selectedAgencyGroup != null){
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
				selectedAgency = currentUser.Contact.AccountId;
				result.add(new SelectOption(currentUser.Contact.AccountId, currentUser.Contact.Account.Name));
				if(currentUser.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
						result.add(new SelectOption(ac.id, ac.name));
					}
				}
			}else{
				result.add(new SelectOption('all', ' -- All -- '));
				selectedAgency = 'all';
				for(Account a: [Select Id, Name from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
					result.add(new SelectOption(a.Id, a.Name));
					allAgencies.add(a.Id);
				}
			}
		}return result;}


	public string selectedCountry{get;set;}
	public List<SelectOption> destinationCountries{
		get{
			if(destinationCountries == null){
				destinationCountries = new List<SelectOption>();
				for(AggregateResult ar : [SELECT Course_Country__c ct FROM client_course__c WHERE Enroled_by_Agency__r.ParentId IN :allGroups AND isCancelled__c = true AND isCancelRequest__c = false group by Course_Country__c order by Course_Country__c ])
					destinationCountries.add(new SelectOption((String) ar.get('ct'), (String) ar.get('ct')));

				if(destinationCountries.size()>0)
					selectedCountry = destinationCountries[0].getLabel();
			}
			return destinationCountries;
		}set;}


	public String selectedTypeFilter {get{if(selectedTypeFilter == null) selectedTypeFilter = 'range'; return selectedTypeFilter;}set;}
	public list<SelectOption> typeFilter {get{
		if(typeFilter==null){
			typeFilter = new list<SelectOption>();
			typeFilter.add(new SelectOption('range', 'Range'));
			typeFilter.add(new SelectOption('THIS_MONTH','This Month'));
			typeFilter.add(new SelectOption('LAST_MONTH','Last Month'));
		}
		return typeFilter;
	}set;}

	public client_course_instalment__c dateFilter{
		get{
			if(dateFilter == null){
				dateFilter = new client_course_instalment__c();
				Date myDate = Date.today();
				dateFilter.Discounted_On__c = system.today();
				dateFilter.Original_Due_Date__c = dateFilter.Discounted_On__c.addDays(-30);
			}
			return dateFilter;
		}set;}
}