@isTest
private class share_commissions_track_test {

  static testMethod void myUnitTest() {
    TestFactory tf = new TestFactory();
    Account school = tf.createSchool();
    Account agencyGroup1 = tf.createAgencyGroup();
    Account agencyGroup2 = tf.createAgencyGroup();
    Account agency = tf.createSimpleAgency(agencyGroup1);
    Account agency2 = tf.createSimpleAgency(agencyGroup2);
    Contact emp = tf.createEmployee(agency);
    Contact emp2 = tf.createEmployee(agency2);
    User portalUser = tf.createPortalUser(emp);
    Account campus = tf.createCampus(school, agency);
    Course__c course = tf.createCourse();
    Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
    Contact client = tf.createClient(agency);

    client_course__c booking = tf.createBooking(client);
    client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);

    List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);

    agencyGroup1.name = 'Group1';
    agencyGroup2.name = 'Group2';
    agency.name = 'Agency1';
    agency2.name = 'Agency2';

    update agencyGroup1;
    update agencyGroup2;
    update agency;
    update agency2;

    cc.Enrolment_Date__c = System.today();
    cc.Enroled_by_Agency__c = agency.id;
    cc.Commission_Type__c = 0;
    update cc;

    instalments[0].Received_By_Agency__c = agency2.id;
    instalments[0].Received_Date__c = System.today();
    instalments[0].Commission_Confirmed_On__c = System.today();
    instalments[0].isShareCommissionSplited__c = true;

    instalments[1].Received_By_Agency__c = agency2.id;
    instalments[1].Received_Date__c = System.today();
    instalments[1].Commission_Confirmed_On__c = System.today();
    instalments[1].isShareCommissionSplited__c = true;
    update instalments;

    Test.startTest();
    system.runAs(portalUser){
      commission_agency_request req = new commission_agency_request();
      
      ApexPages.currentpage().getparameters().put('toAgency', agency.id);
      ApexPages.currentpage().getparameters().put('ctName', agency.BillingCountry);
      ApexPages.currentpage().getparameters().put('ctCurrency', agency.account_currency_iso_code__c);
      
      req.selectedIds = agency.id + '-' + instalments[0].id + '-1;';
      req.setIdToCreate();
      req.dueDate.Due_Date__c = system.today();
      req.inv.Summary__c = 'test 1';
      req.createInvoice();
      
      req.selectedIds = agency.id + '-' + instalments[1].id + '-1;';
      req.setIdToCreate();
      req.dueDate.Due_Date__c = system.today();
      req.inv.Summary__c = 'test 2';
      req.createInvoice();
      
      share_commissions_track track = new share_commissions_track();
      
      List<SelectOption> agencyGroupOptions = track.agencyGroupOptions;
      List<SelectOption> groupAsOptions = track.groupAsOptions;
      
      track.selectedTab = 'pendingRequest';
      track.isPds = true;
      track.isPcs = true;
      track.isPfs = true;
      track.isNet = false;
      track.searchShareCommissions();
      track.isNet = true;
      track.searchShareCommissions();
      
      track.selectedTab = 'pendingReceive';
      track.searchShareCommissions();
      
      track.showConfirmed = true;
      track.searchShareCommissions();
      
      // -------------------------------
      track.showConfirmed = false;
      track.selectedTab = 'instOverdue';
      track.selectedAgency = agency.id;
      track.searchShareCommissions();
      
      track.selectedTab = 'pendingRequest';
      track.searchShareCommissions();

      track.selectedTab = 'pendingRequest';
      track.searchShareCommissions();
      
      track.selectedTab = 'pendingReceive';
      track.searchShareCommissions();
      
      track.showConfirmed = true;
      track.searchShareCommissions();
      
      track.totPendingSplit();
      track.totPendingRequest();
      track.totPendingReceive();

       List<SelectOption> destinationCountries = track.destinationCountries;
       List<SelectOption> schoolOptions = track.schoolOptions;
       List<SelectOption> commAgencies  = track.commAgencies ;
       List<SelectOption> schools  = track.schools ;
       track.changeCommAgency();
       track.changeFromGroup();

       track.checkMonthDesc(1);
       track.checkMonthDesc(2);
       track.checkMonthDesc(3);
       track.checkMonthDesc(4);
       track.checkMonthDesc(5);
       track.checkMonthDesc(6);
       track.checkMonthDesc(7);
       track.checkMonthDesc(8);
       track.checkMonthDesc(9);
       track.checkMonthDesc(10);
       track.checkMonthDesc(11);
       track.checkMonthDesc(12);
    }
  }
}