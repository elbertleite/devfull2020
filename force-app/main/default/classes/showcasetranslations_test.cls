/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class showcasetranslations_test {

    static testMethod void myUnitTest() {
        
        TestFactory tf = new TestFactory();
        
        Account school = tf.createSchool();
       	Account agency = tf.createAgency();
       	Account campus = tf.createCampus(school, agency); 
        
        Account country = tf.createCountryShowcase();
        Account city = tf.createCityShowcase(country);
        
        List<Translation__c> lt = new List<Translation__c>();
        
        Translation__c t1 = tf.populateContentTranslation();
        t1.account__c = country.id; 
        t1.language__c = 'pt_BR';
        lt.add(t1);        
        Translation__c t2 = tf.populateContentTranslation();
        t2.account__c = city.id; 
        t2.language__c = 'cs';
        lt.add(t2);
        Translation__c t3 = tf.populateLabelTranslation();
        t3.Language__c = 'pt_BR';
        lt.add(t3);
        Translation__c t4 = tf.populateLabelTranslation();
        t4.Language__c = 'cs';
        lt.add(t4);
        Translation__c t5 = tf.populateContentTranslation();
        t5.account__c = campus.id; 
        t5.language__c = 'pt_BR';
        lt.add(t5);
        Translation__c t6 = tf.populateContentTranslation();
        t6.account__c = city.id; 
        t6.language__c = 'pt_BR';
        lt.add(t6);
        
        insert lt;
        
        Test.startTest();
        
        showcasetranslations sct = new showcasetranslations();
        sct.loadUserInfo();
        sct.getCountries();
        sct.selectedCountry = 'Australia';
        sct.refreshCities();
        sct.getCities();
        sct.getSchools();
        sct.getLanguages();
        sct.getLanguageResults();
        sct.getStatuses();
        sct.search();
        
        sct.selectedStatus = 'Completed';
        sct.selectedLanguage = 'pt_BR';
        sct.search();
        
        Contact employee = tf.createEmployee(agency);
        User userEmp = tf.createPortalUser(employee);
        system.runAs(userEmp){
        	showcasetranslations portalSCT = new showcasetranslations();
	        portalSCT.loadUserInfo();
	        portalSCT.getCountries();
	        portalSCT.selectedCountry = 'Australia';
	        portalSCT.refreshCities();
	        portalSCT.getCities();
	        portalSCT.getSchools();
	        portalSCT.getLanguages();
	        portalSCT.getLanguageResults();
	        portalSCT.getStatuses();
	        portalSCT.search();
	        
	        portalSCT.selectedStatus = 'Completed';
	        portalSCT.selectedLanguage = 'pt_BR';
	        portalSCT.search();
        }
        
        Test.stopTest();
        
    }
    
    
    
    
    
    
}