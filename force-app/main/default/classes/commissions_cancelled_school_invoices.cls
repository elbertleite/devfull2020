public with sharing class commissions_cancelled_school_invoices {
	
	public list<Invoices> result {get;set;}
	
	public String selectedAgency {get{
		if(selectedAgency==null) 
			selectedAgency = currentUser.Contact.AccountId; 
		return selectedAgency;
		}set;}

	public String reqNumber {get;set;}

	private S3Controller S3 {get{if(S3== null){S3 = new S3Controller(); S3.constructor();} return s3;}set;}
	
	public commissions_cancelled_school_invoices(){
		findGroups();
	}
 
	String prefix;
	String url;
	public void findCancelledInvoices(){

		if(reqNumber==null) reqNumber = '';

		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && currentUser.Aditional_Agency_Managment__c == null)
			selectedAgency = currentUser.Contact.AccountId;

		result = new list<Invoices>();
		S3Controller cS3 = new S3Controller();
		cS3.constructor(); //Generate Credentials	

		if(!Test.isRunningTest()){

			prefix = currentUser.Contact.Account.Global_Link__c + '/'+ selectedAgency + '/Commissions/RequestedSchool/Cancelled/'+reqNumber;
			cS3.listBucket(Userinfo.getOrganizationId(), prefix, null, null, null);
			if(cS3.bucketList != null && !cS3.bucketList.isEmpty()){	

				for(S3.ListEntry file : cS3.bucketList){
					system.debug('file.key ==>' + file.key);
					list<String> fileDetails = file.key.substringAfter('/Cancelled/').split(';#;');
					
					url = IPFunctions.GenerateAWSLink(Userinfo.getOrganizationId(), file.key, S3.S3Key, s3.S3Secret);

					result.add(new Invoices(fileDetails[0], fileDetails[1], url));
				}//end for
			}

			prefix = currentUser.Contact.Account.Global_Link__c + '/'+ selectedAgency + '/Commissions/RequestedProvider/Cancelled/'+reqNumber;
			system.debug('THE ORGANIZATION ID '+Userinfo.getOrganizationId());
			cS3.listBucket(Userinfo.getOrganizationId(), prefix, null, null, null);
			if(cS3.bucketList != null && !cS3.bucketList.isEmpty()){	

				for(S3.ListEntry file : cS3.bucketList){
					system.debug('file.key ==>' + file.key);
					list<String> fileDetails = file.key.substringAfter('/Cancelled/').split(';#;');
					
					url = IPFunctions.GenerateAWSLink(Userinfo.getOrganizationId(), file.key, S3.S3Key, s3.S3Secret);

					result.add(new Invoices(fileDetails[0], fileDetails[1], url));
				}//end for
			}
		}
		else {
			result.add(new Invoices('InvNumber', 'SchoolName', 'url'));
		}
	}


	public class Invoices{
		public String invNumber {get;set;}
		public String schoolName {get;set;}
		public String url {get;set;}

		public Invoices (String invNumber, String schoolName, String url){
			this.invNumber = invNumber;
			this.schoolName = schoolName;
			this.url = url;
		}
	}


	private User currentUser {get{
		if(currentUser ==null)
			currentUser = [SELECT Aditional_Agency_Managment__c, Contact.Account.ParentId, Contact.Account.Parent.Name, Contact.Account.Global_Link__c, Contact.AccountId, Contact.Account.Name FROM User WHERE id = :UserInfo.getUserId()];

		return currentUser;
	}set;}


	public string selectedAgencyGroup{
    	get{
    		if(selectedAgencyGroup == null)
    			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
    		return selectedAgencyGroup;
    	}  
    	set;
    }

	public list<SelectOption> agencyGroupOptions {get;set;}
	private void findGroups(){

		agencyGroupOptions = new List<SelectOption>();  
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group  
			agencyGroupOptions.add(new SelectOption(currentUser.Contact.Account.ParentId, currentUser.Contact.Account.Parent.Name));
		}
		else{
			for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name])
				agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
		}

		findAgencies();
	}

	public void changeGroup(){
		findAgencies();
	}

	public list<SelectOption> agencyOptions {get;set;}
	private void findAgencies(){
		if(selectedAgencyGroup != null){
			agencyOptions = new List<SelectOption>();  
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency  
				agencyOptions.add(new SelectOption(currentUser.Contact.AccountId, currentUser.Contact.Account.Name));
				selectedAgency = currentUser.Contact.AccountId;
				if(currentUser.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
						agencyOptions.add(new SelectOption(ac.id, ac.name));
					}
				}
			}
			else{
				for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency' AND ParentId = :selectedAgencyGroup order by Name])
					agencyOptions.add(new SelectOption(ag.id, ag.Name));

				if(agencyOptions != null && agencyOptions.size()>0){
					if(selectedAgencyGroup == currentUser.Contact.Account.ParentId)
						selectedAgency = currentUser.Contact.AccountId;
					else
						selectedAgency = agencyOptions[0].getValue();
				}
			}
		}
	}
}