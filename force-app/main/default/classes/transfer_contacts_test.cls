/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class transfer_contacts_test {

    static testMethod void myUnitTest() {
        
		TestFactory tf = new TestFactory();
		
		Map<String,String> recordTypes = new Map<String,String>();       
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
		
		Account agency = tf.createAgency();
		Contact employee = tf.createEmployee(agency);
		User portalUser = tf.createPortaluser(employee);
		
		Account anotherAgency = new Account();
		anotherAgency.ParentId = agency.Parentid;
		anotherAgency.name = 'EHF PERTH';
		anotherAgency.RecordTypeId = recordTypes.get('Agency');
		anotherAgency.BillingCountry = 'Australia';
		anotherAgency.BillingCity = 'Perth';
		anotherAgency.account_currency_iso_code__c = 'AUD';
		anotherAgency.Optional_Currency__c = 'USD';
		anotherAgency.OwnerId = UserInfo.getUserId();
		anotherAgency.Account_Link__c = agency.account_link__c;
		anotherAgency.Services__c = 'Resumes;Visas;';
		anotherAgency.Global_Link__c = agency.Global_Link__c;
		insert anotherAgency;
		
		
		
		Contact anotherEmployee = new Contact();
		anotherEmployee.FirstName = 'Johnny';
		anotherEmployee.LastName = 'Bravo';
		anotherEmployee.Email = 'johnnybravo@educationhify.com.test';
		anotherEmployee.recordtypeid = recordTypes.get('Employee');		
		anotherEmployee.Preferable_nationality__c = 'Brazil';
		anotherEmployee.Preferable_School_City__c = 'Sydney';
		anotherEmployee.Preferable_School_Country__c = 'Australia';
		anotherEmployee.Accountid = anotherAgency.id;
		anotherEmployee.Nationality__c = 'Brazil';
		insert anotherEmployee;
		
		Profile portalProfile = [Select id from Profile where Name = 'Agency Global Manager' order by LastModifiedDate DESC LIMIT 1];
		User anotherUser = new User(
			Username = System.now().millisecond() + 'test12345@test.com',
			ContactId = anotherEmployee.Id,
			ProfileId = portalProfile.Id,
			Alias = 'testeS',
			Email = '12325@test.com',
			EmailEncodingKey = 'UTF-8',
			LastName = 'McTesty',
			CommunityNickname = '12312345',
			TimeZoneSidKey = 'America/Los_Angeles',
			LocaleSidKey = 'en_US',
			LanguageLocaleKey = 'en_US'			
		);
		
		Database.insert(anotherUser);
		
		system.runAs(portalUser){
			Test.startTest();
			
			Contact client = new Contact();
			client.FirstName = 'Margarete';
			client.LastName = 'Rose';
			client.Email = 'margarete@googlerose.com.test';
			client.recordtypeid = recordTypes.get('Client');		
			client.Preferable_nationality__c = 'Brazil';
			client.Preferable_School_City__c = 'Sydney';
			client.Preferable_School_Country__c = 'Australia';
			client.Accountid = agency.id;
			client.Nationality__c = 'Brazil';
			client.Owner__c = portalUser.id;
			insert client;
			Contact lead = tf.createLead(agency, employee);
			
			transfer_contacts tc = new transfer_contacts();
			tc.getListAgencies();
			tc.transferFromAgency = agency.id;
			tc.refreshFromUsers();
			tc.getListUsersFrom();
			tc.transferFromUser = portalUser.id;			
			tc.transferToAgency = anotherAgency.id;
			tc.getListUsersTo();
			tc.transferToUser = anotherUser.id;
			tc.getListDestinations();
			tc.loadContacts();
			
			for(Contact ct : tc.listContacts)
				ct.isSelected__c = true;
			
			
			tc.transferSelectedContacts();
			
			for(Custom_Note_Task__c task : tc.listTasks)
				task.selected__c = true;
			
			tc.transferSelectedTasks();
			tc.refreshToUsers();
			
			tc.loadContacts();
			
			Test.stopTest();	
		}
		
    }
}