/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seealldata=true)
private class API_v1_courseSearch_test {

    static testMethod void myUnitTest() {
        
        TestFactory tf = new TestFactory();
       
		Account agency = tf.createAgency();
		
		Contact employee = tf.createEmployee(agency);
        User portalUser = tf.createPortalUser(employee);
       
       			
        Account school = tf.createSchool();
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		
		insert p;
		
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;
		insert p3;
		
		
		Account campus = tf.createCampus(school, agency);
		    
		Course__c course = tf.createCourse();
		
		Course__c course2 = tf.createLanguageCourse();
				
		Campus_Course__c cc = tf.createCampusCourse(campus, course);
		
		Campus_Course__c cc2 = tf.createCampusCourse(campus, course2);
		
		tf.populateCourseTranslation(cc);
		tf.populateCourseTranslation(cc2);
       
		Course_Price__c cp = tf.createCoursePrice(cc, 'Latin America');
		
		Course_Price__c cp2 = tf.createCoursePrice(cc2, 'Published Price');
		
		
		Course_Intake_Date__c cid = tf.createCourseIntakeDate(cc);
		
		Course_Intake_Date__c cid2 = tf.createCourseIntakeDate(cc2);
        
		Course_Extra_Fee__c cef = tf.createCourseExtraFee(cc, 'Latin America', p);
		
		Course_Extra_Fee_Dependent__c cefd = tf.createCourseRelatedExtraFee(cef, p2);
		
		tf.createCourseExtraFeeCombined(cc, 'Latin America', p, p2);
        
		Course_Extra_Fee__c cef2 = new Course_Extra_Fee__c();
		cef2.Campus__c = campus.Id;
		cef2.Nationality__c = 'Published Price';
		cef2.Availability__c = 3;
		cef2.From__c = 2;
		cef2.Value__c = 100;		
		cef2.Optional__c = false;
		cef2.Product__c = p.id;
		cef2.date_paid_from__c = system.today();
		cef2.date_paid_to__c = system.today().addDays(+31);
		insert cef2;
		
		
		
		
		
		
		Deal__c d = new Deal__c();
		d.Availability__c = 3;
		d.Campus_Account__c = campus.id;
		d.From__c = 1;
		d.Extra_Fee__c = cef2.id;
		d.Promotion_Type__c = 3;
		d.From_Date__c = system.today();
		d.To_Date__c = system.today().addDays(60);
		d.Extra_Fee_Type__c = 'Cash';
		d.Extra_Fee_Value__c = 60;
		d.Product__c = p.id;
		d.Nationality_Group__c = 'Publish Price';
		insert d;
		
		
		Deal__c d2 = new Deal__c();
		d2.Availability__c = 3;
		d2.Campus_Course__c = cc2.id;
		d2.From__c = 1;		
		d2.Promotion_Type__c = 2;
		d2.From_Date__c = system.today();
		d2.To_Date__c = system.today().addDays(60);
		d2.Promotion_Weeks__c = 1;
		d2.Number_of_Free_Weeks__c = 1;
		d2.Nationality_Group__c = 'Latin America';
		d2.Promotion_Name__c = '1+1';
		d2.Promo_Price__c = 50;		
		insert d2;
		
		Start_Date_Range__c sdr = new Start_Date_Range__c();
		sdr.Promotion__c = d2.id;
		sdr.Campus__c = cc2.campus__c;
		sdr.From_Date__c = system.today();
		sdr.To_Date__c = system.today().addDays(15);
		sdr.Number_of_Free_Weeks__c = 2;
		insert sdr;		
		
		
		
		Deal__c d3 = new Deal__c();
		d3.Availability__c = 3;
		d3.Campus_Course__c = cc.id;
		d3.From__c = 1;
		d3.Extra_Fee__c = cef.id;
		d3.Promotion_Type__c = 3;
		d3.From_Date__c = system.today();
		d3.To_Date__c = system.today().addDays(60);
		d3.Extra_Fee_Type__c = 'Cash';
		d3.Extra_Fee_Value__c = 50;
		d3.Nationality_Group__c = 'Latin America';
		insert d3;
		
		Deal__c d4 = new Deal__c();
		d4.Availability__c = 3;
		d4.Campus_Course__c = cc.id;
		d4.From__c = 1;		
		d4.Promotion_Type__c = 2;
		d4.Promotion_Name__c = '5+2';
		d4.Promo_Price__c = 50;		
		d4.From_Date__c = system.today();
		d4.To_Date__c = system.today().addDays(61);
		d4.Promotion_Weeks__c = 5;
		d4.Number_of_Free_Weeks__c = 2;
		d4.Nationality_Group__c = 'Published Price';
		insert d4;
		
		
		
		Deal__c d5 = new Deal__c();
		d5.Availability__c = 3;
		d5.Campus_Course__c = cc.id;
		d5.Extra_Fee_Dependent__c = cefd.id;
		d5.From__c = 1;		
		d5.Promotion_Type__c = 3;
		d5.From_Date__c = system.today();
		d5.To_Date__c = system.today().addDays(62);
		d3.Extra_Fee_Type__c = 'Cash';
		d3.Extra_Fee_Value__c = 50;
		d5.Nationality_Group__c = 'Latin America';
		d5.Product__c = p3.id;
		//insert d5;
		 
		tf.populateLabelTranslation();
		 
		  
        Test.startTest();
        
        //null request to force exception
        RestContext.request = null;
   		api_v1_courseSearch.doGet();
        
        
        
       	///MISSING PARAMETERS
   		RestRequest req = new RestRequest();
   		RestResponse resp = new RestResponse();   		
   		RestContext.request = req;
   		RestContext.response = resp;
   		api_v1_courseSearch.doGet();
   		
   		
   		
   		//good request
   		req = new RestRequest();
   		req.addParameter('units', '24');
   		req.addParameter('nationality', 'Brazil');
   		req.addParameter('country', 'Australia');
   		req.addParameter('city', 'Sydney');
   		req.addParameter('type', 'English/ELICOS');
   		req.addParameter('category', 'Language');
   		req.addParameter('language', 'pt_BR');
   		RestContext.request = req;   		
		api_v1_courseSearch.doGet();
		
		
		
		
		Test.stopTest();
        
                
    }
}