/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Website_Sharing_Configuration_test {

    static testMethod void myUnitTest() {
    	
    	
    	TestFactory tf = new TestFactory();
    	tf.createAgency();
    	Account school = tf.createSchool();
    	Account agency = tf.createAgency();
       	Account campus = tf.createCampus(school, agency);
    	Account country = tf.createCountryShowcase();
    	tf.createCityShowcase(country);
    	
    	/*UserRole role = new UserRole(name = 'TEST ROLE');
    	insert role;
    	User u = new User(alias = 'hasrole', email='userwithrole@roletest1.com', userroleid = role.id, emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(), timezonesidkey='America/Los_Angeles', username='userwithrole@testorg.com');
        insert u;                            
        */
        
        
        Group g = new Group();  
        g.name = 'the test group';      
        insert g;

		
    	
    	website_sharing_configuration wsc = new website_sharing_configuration();
    	 
    	wsc.refreshSharing();
    	wsc.groupByRole.put('testMethod', g.id);
    	wsc.selectedRole = 'testMethod';
    	
    	
    	for(Account acc : wsc.destinations){
    		acc.isSelected__c = true;
    		for(Account child : acc.childAccounts)
    			child.isSelected__c = true;
    	}
    	
    	for(Account acc : wsc.schools){
    		acc.isSelected__c = true;
    		for(Account child : acc.childAccounts)
    			child.isSelected__c = true;
    	}
    	
    	wsc.save();
    	
    }
}