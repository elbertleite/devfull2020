@isTest
private class report_incoming_leads_test{
	static testMethod void myUnitTest() {
		TestFactory tf = new TestFactory();
		
		Account agency = tf.createAgency();

		tf.createChecklists(agency.Parentid);

		Contact emp = tf.createEmployee(agency);
		
		Contact lead = tf.createLead(agency, emp);
		lead.Ownership_History_Field__c = '[{"toUserID":"005O0000004bBaSIAU","toUser":"Patricia Greco","toAgencyID":"0019000001MjqIIAAZ","toAgency":"IP Australia Sydney (HO)","fromUserID":null,"fromUser":null,"fromAgencyID":null,"fromAgency":null,"createdDate":"2018-09-18T04:17:12.024Z","createdByUserID":"005O0000004bBaSIAU","createdByUser":"Patricia Greco","action":"Taken"}]';
		lead.Lead_Stage__c = 'Stage 1';
		lead.Lead_Area_of_Study__c = 'Test 1; Test 2';
		update lead;

		Contact lead2 = tf.createClient(agency);
		Contact lead3 = tf.createClient(agency);

		List<String> leads = new List<String>();
		leads.add(lead.ID);
		leads.add(lead2.ID);
		leads.add(lead3.ID);

		Contact client = tf.createClient(agency);

		User portalUser = tf.createPortalUser(emp);

		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);

       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);

		Quotation__c q1 = tf.createQuotation(lead, campusCourse);
		Quotation__c q2 = tf.createQuotation(lead, campusCourse);

		Destination_Tracking__c dt = new Destination_tracking__c();
		dt.Client__c = lead.id;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
		dt.Expected_Travel_Date__c = system.today().addMonths(3);
		dt.lead_Cycle__c = false;
		insert dt;

		Client_Checklist__c checklist = new Client_Checklist__c();
		checklist.Agency__c = agency.id;
		checklist.Agency_Group__c = agency.Parentid;
		checklist.Destination__c = lead.Destination_Country__c;
		checklist.Checklist_Item__c = 'aaa';
		checklist.Client__c = lead.id;
		checklist.Destination_Tracking__c = dt.id;
		insert checklist;

		Test.startTest();
     	system.runAs(portalUser){
		
			emp.Account = agency;
			portalUser.contact = emp;
			update portalUser;		

			report_incoming_leads.initPage();
			report_incoming_leads.refreshEmployees(new List<String>{agency.ID});
			report_incoming_leads.refreshEmployees(new List<String>());
			report_incoming_leads.generateListIncomingContacts(new Set<String>{agency.ID}, agency.Parentid, new Set<String>{agency.ID}, portalUser.ID, 2019, 6, true);
			report_incoming_leads.generateReport(new List<String>{agency.ID}, agency.Parentid, new List<String>{agency.ID}, portalUser.ID, 2019, 6);
            
            IPClasses.OwnershipHistory ownership = new IPClasses.OwnershipHistory();
            ownership.createdDate = Date.today();
            report_incoming_leads.ContactReport ctt = new report_incoming_leads.ContactReport(new Contact(), ownership, 'dd/MM/yyyy'); 

            Apexpages.currentPage().getParameters().put('exporting', 'true');
            Apexpages.currentPage().getParameters().put('groupSelected', agency.Parentid);
            Apexpages.currentPage().getParameters().put('employeeSelected', portalUser.ID);
            Apexpages.currentPage().getParameters().put('yearSelected', '2019');
            Apexpages.currentPage().getParameters().put('monthSelected', '6');
            Apexpages.currentPage().getParameters().put('agenciesSelected', agency.ID+',');

            report_incoming_leads ct = new report_incoming_leads();
		}
	}
}