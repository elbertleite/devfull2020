public class DepartmentsController {
	public list<String> agencyServices {get;set;}	
	public list<string> selectedDeptServices {get;set;}
	public String selectedServices {get;set;}
	private User currentUser;
	
	public Department__c newDepartment {  
		get{
			if( newDepartment == null ){
				newDepartment = new Department__c();
			}
			return newDepartment;	
		}
		set;
	}
	
	
	public DepartmentsController(){		
		string deptID = ApexPages.currentpage().getParameters().get('id');
		currentUser = [Select Id, Contact.AccountId from User where id=: UserInfo.getUserId() limit 1];
		selectedDeptServices = new List<String>();
		
		String agencyID = ApexPages.currentPage().getParameters().get('idAgency');
		if(agencyID == null)
			agencyID = currentUser.Contact.AccountId;
		
		if(deptID!=null && deptId != ''){
			newDepartment = [Select Id, Name, Services__c, Show_Visits__c, Agency__c, Allow_Booking__c from Department__c where id= :deptID limit 1];
			
			if(newDepartment.Services__c!=null && newDepartment.Services__c !=''){
				newDepartment.Services__c = newDepartment.Services__c.replace('[', '');
				newDepartment.Services__c = newDepartment.Services__c.replace(']', '');
				selectedDeptServices = newDepartment.Services__c.split(';'); 
				selectedServices = newDepartment.Services__c.replace(';',','); 
			}
		}
		else
			newDepartment.Agency__c = agencyID;
			
		//Agency Services
		agencyServices(newDepartment.Agency__c);
	}
	
	public void agencyServices(String agencyId){
		agencyServices = new list<String>();
		String services = [Select Services__c from Account Where id= :agencyId limit 1].Services__c;
		
		if(services!=null && services != '')
			for(String s : services.split(';')){
				agencyServices.add(s);
			}
	}
	
	public void addDepartment(){
		newDepartment.Services__c = selectedServices != null ? selectedServices.replace(',',';') : '';		
		upsert newDepartment;		
	}
}