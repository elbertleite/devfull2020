public with sharing class visitNotify {
	
	public visitNotify(){	}
	
	public boolean poll {
		get{
			if(poll == null){
				try{
					poll = [select Department__r.Show_Visits__c from Contact WHERE Id = :UserDetails.getMyContactDetails().id].Department__r.Show_Visits__c;
				} catch (Exception e){
					poll = false;
				}
			}
			return poll;				
		}
		set;
	}
	
	private Set<String> alreadyNotified = new Set<String>();
	private List<Visit__c> originalVisits; //populate only once
	public List<VisitWrapper> visits { get; set; }
	//public List<Visit__c> visits { get; set; }
	public void checkVisits(){
		try {		
			visits = new List<VisitWrapper>();			
			String notifyStaff = UserInfo.getUserId()+':false';
			
			List<Visit__c> visitList = [Select id, Contact__r.Name, Staff_Notification__c, Status__c, Staff_Requested__c from Visit__c WHERE  (Status__c = 'Waiting' OR Status__c = 'Transferred') AND Staff_Notification__c INCLUDES (:notifyStaff) and id not in :alreadyNotified];
			
			if(!visitList.isEmpty()){
				
				if(originalVisits == null)
					originalVisits = visitList;
				
				String agencyID = [select AccountID from User where id = :UserInfo.getUserId()].AccountID;
				Map<String, User> listUsers = new Map<String, User>([select id, name from user where AccountID = :agencyID and isActive = true]);
				
				for(Visit__c v: visitList)
					visits.add(populateWrapper(v, listUsers));
				
				Map<String, Visit__c> mapVisits = new Map<String, Visit__c>(visitList);
				alreadyNotified.addAll(mapVisits.keySet());
				
				
			}
		} catch (Exception e){
			system.debug('@#$ Exception: ' + e.getMessage());
		}
	}
	
	public VisitWrapper populateWrapper(Visit__c v, Map<String, User> listUsers){
		VisitWrapper vw = new VisitWrapper();
		vw.visit = v;
		vw.requestedServices = '';
		vw.requestedStaff = '';
		
		if(v.Staff_Requested__c != null){
			for(String theString : v.Staff_Requested__c.split(',')){
				String[] str = theString.split('!#');
				vw.requestedServices += str[2] + ', ';
				vw.requestedStaff += listUsers.containsKey(str[0]) ? listUsers.get(str[0]).Name + ', ' : '';
			}
			vw.requestedServices = vw.requestedServices.removeEnd(', ');
			vw.requestedStaff = vw.requestedStaff.removeEnd(', ');
		}
		return vw;	
	}
	
	public void closeNotification(){
		
		//005N0000001EAm8IAG:false;005N0000001IYtQIAW:false;005N0000001Ijn4IAC:false;005N0000001J7ceIAC:false;
		String updatedField = '';
		String visitid = ApexPages.currentPage().getParameters().get('visitid');
		
		system.debug('visitid: ' + visitid);
		
		
		try {
			
			if(visitid != null && visitid != ''){
				
				Visit__c v = [select Staff_Notification__c from Visit__c where id = :visitid];

				String[] notifyUser = v.Staff_Notification__c.split(';');
				for(String userid : notifyUser){
					if(userid.split(':')[0] == UserInfo.getUserId())
						updatedField += UserInfo.getUserId() + ':true;';
					else updatedField += userid + ';';
				}	
				v.Staff_Notification__c = updatedField;
				update v;
				
			}
			checkVisits();
		} catch (Exception e){
			system.debug('@#$ Exception: ' + e.getMessage());
		}
	}
	
	public class VisitWrapper {
		
		public Visit__c visit {get;set;}
		public String requestedServices {get;set;}
		public String requestedStaff {get;set;}
	}
	    
}