/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class client_course_fees_new_test {

    static testMethod void myUnitTest() {
        
		TestFactory tf = new TestFactory();
       
       	Account school = tf.createSchool();
       	Account agency = tf.createAgency();
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
       	Account campus = tf.createCampus(school, agency);
       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
       
       
       	Test.startTest();
       	system.runAs(portalUser){
			Contact client = tf.createLead(agency, emp);
			client_course__c booking = tf.createBooking(client);
	       	client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       	client_Course_fees__c ccFees  = tf.createClientCourseFees(cc, true);
	       	client_Course_fees__c ccFees2  = tf.createClientCourseFees(cc, false);
	       	ccFees.relatedPromotionFee__c = ccFees2.id;
	       	update ccFees;
	       	
	       	
			List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
			
			ApexPages.currentPage().getParameters().put('id', cc.id);
			client_course_fees_new testClass = new client_course_fees_new(new ApexPages.StandardController(cc));
			
			testClass.loadFees();
			
			client_course_fees__c newFee = testClass.newFee;
			client_course_fees__c newDiscount = testClass.newDiscount;
			testClass.getExtraFeeNames();
			testClass.getNumberOfUnits();
			testClass.getUnitTypes();
			testClass.getQuoteFees();
			boolean isMigration = testClass.isMigration;

			testClass.clientCourse.Original_Total_Course__c = 0;
			testClass.clientCourse.Original_Total_Extra_Fees__c = 0;
			testClass.clientCourse.Original_Total_Extra_Fees_Promotion__c = 0;
			
			testClass.addNewFee();
			testClass.newFee.Fee_Name__c = 'Test Fee';
			testClass.newFee.Value__c = 300;
			testClass.newFee.Total_Value__c = 300;
			testClass.addNewFee();
			
			
			testClass.newDiscount.Fee_Name__c = 'Test Fee';
			testClass.newDiscount.Value__c = 10000;
			testClass.saveDiscount();
			testClass.newDiscount.Promotion_Name__c = 'Test Promotion';
			testClass.newDiscount.Value__c = 100;
			testClass.saveDiscount();
			
		}
       	Test.stopTest();
    }
}