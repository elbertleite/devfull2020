global without sharing class  dmc_schools {



	 public class schoolWrap {

        public schoolWrap() {
           campuses = new list<account>();
        }
        public string id;
        public string school;
		public string schoolGroup;
        public string website;
        public string category;
        public list<account> campuses;

    }

    @RemoteAction
    global static String getlstContact(String accountName, string country, string city) { 
        String param = '%'+accountName+'%';
        List < schoolWrap > lstwrap = new List < schoolWrap > ();
        List <account> lstacc;

        if(accountName.length() > 1){


            String sql = 'Select parent.parent.name, parentid, parent.name, Id, Name, BillingCity, BillingCountry, Website, Disabled_Campus__c, ShowCaseOnly__c, School_Campus_Category__c from Account where RecordType.name = \'campus\' ';

            if(country != null && country != '')
            	sql += ' and BillingCountry =  \''+country+'\'';

             if(city != null && city != '')
            	sql += ' and BillingCity =  \''+city+'\'';

            sql += ' and (name like  \''+param+'\' or parent.name like \''+param+'\' or parent.parent.name like \''+param+'\')';

            sql += ' order by BillingCountry, parent.parent.name, parent.name';

            System.debug('==>sql: '+sql);
            lstacc = Database.query(sql);

        } else if(accountName.length() == 0){

            String sql = 'Select parent.parent.name, parentid, parent.name, Id, Name, BillingCity, BillingCountry, Website, Disabled_Campus__c, ShowCaseOnly__c, School_Campus_Category__c from Account WHERE RecordType.name = \'campus\' ';

             if(country != null && country != '')
            	sql += ' and BillingCountry =  \''+country+'\'';

             if(city != null && city != '')
            	sql += ' and BillingCity =  \''+city+'\'';

            sql += ' order by BillingCountry, parent.parent.name, parent.name';

            System.debug('==>sql: '+sql);

            lstacc = Database.query(sql);

        }

        if(lstacc != null){
            list<string> orderedSchool = new  list<string>();
            map<string,account> schoolName = new map<string,account>();
            map<string, list<account>> schoolCampus = new map<string, list<account>>();
            for (Account a : lstacc) {
            	if(!schoolName.containsKey(a.parentid)){
               		schoolName.put(a.parentid, a);
               		orderedSchool.add(a.parentid);
            	}
               	if(!schoolCampus.containsKey(a.parentid))
               		schoolCampus.put(a.parentid, new list<account>{a});
               	else schoolCampus.get(a.parentid).add(a);
        	}

        	for(string sc:orderedSchool){
        		schoolWrap swrap = new schoolWrap();
                swrap.id = schoolName.get(sc).id;
                swrap.schoolGroup = schoolName.get(sc).parent.parent.name;
                swrap.school = schoolName.get(sc).parent.name;
                swrap.website = schoolName.get(sc).Website;
                swrap.category = schoolName.get(sc).School_Campus_Category__c;
                for(Account ac:schoolCampus.get(sc))
                	swrap.campuses.add(ac);
                lstwrap.add(swrap);
        	}

      	}
        return JSON.serialize(lstwrap);

	}

	private map<string, list<string>> MapCountries;
	private list<string> Countries;
	private map<string, list<string>> getMapCountries(){
		if(MapCountries == null){
			MapCountries = new map<string, list<string>>();
			Countries = new list<string>();
			for(AggregateResult ac: [Select BillingCountry, BillingCity from Account where RecordType.name = 'campus' GROUP BY BillingCountry, BillingCity ORDER BY BillingCountry, BillingCity])
				if (!MapCountries.containsKey((string) ac.get('BillingCountry'))){
					Countries.add((string) ac.get('BillingCountry'));
					MapCountries.put((string) ac.get('BillingCountry'), new list<string>{(string) ac.get('BillingCity')});
				}
				else MapCountries.get((string) ac.get('BillingCountry')).add((string) ac.get('BillingCity'));
			}
		return MapCountries;
	}

	public string SelectedCountry{get{if(SelectedCountry == null) SelectedCountry = ''; return SelectedCountry;} set;}
	private List<SelectOption> DestinationsCountry;
	public List<SelectOption> getDestinationsCountry() {
    	if(DestinationsCountry == null){
    		getMapCountries();
        	DestinationsCountry = new List<SelectOption>();
        	DestinationsCountry.add(new SelectOption('','Select Country'));
        	for(String ct:Countries){
        		if(SelectedCountry == null)
        			SelectedCountry = ct;
        		if(ct != null)
	           		DestinationsCountry.add(new SelectOption(ct,ct));
        	}
    	}
        return DestinationsCountry;
    }

	public string SelectedCity{get{if(SelectedCity == null) SelectedCity = ''; return SelectedCity;} set;}
	private List<SelectOption> DestinationsCity;
	public List<SelectOption> getDestinationsCity() {
		SelectedCity = '';
    	if(SelectedCountry != null && SelectedCountry != ''){
        	DestinationsCity = new List<SelectOption>();
        		DestinationsCity.add(new SelectOption('','All Cities'));
        	for(String ct:MapCountries.get(SelectedCountry)){
        		if(SelectedCity == null)
        			SelectedCity = ct;
	           	DestinationsCity.add(new SelectOption(ct,ct));
        	}
    	}
        return DestinationsCity;
    }

    public PageReference redirectToNewSchool(){
    	String newid = ApexPages.currentPage().getParameters().get('newSchoolID');
    	PageReference pr = new PageReference('/apex/scshowcasecampus?id=' + newid);
    	pr.setRedirect(true);
    	return pr;

    }

}