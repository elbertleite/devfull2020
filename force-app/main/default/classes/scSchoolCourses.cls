public class scSchoolCourses{
	
	public Account campus { get;set; }

	private string accoId;
	private String schoolId;
	public scSchoolCourses(ApexPages.StandardController controller){
		accoId = ApexPages.currentPage().getParameters().get('cpID');
		schoolId = ApexPages.currentPage().getParameters().get('id');
		
		campus = [select id, ParentID, BillingCountry, BillingCity from Account where id = :accoid];
		
	}
	
	private map<string, map<string,list<campus_course__c>>> listCourses;
	public map<string, map<string,list<campus_course__c>>> getListCourses(){
		if(listCourses == null){
			listCourses = new map<string, map<string,list<campus_course__c>>>();
			for(Campus_Course__c cc:[Select Course__r.Course_Category__c, Course__r.Type__c, Course__r.Name, Course__r.Sub_Type__c, Course__r.Course_Type__c, Course__r.Course_Unit_Type__c, Course__r.Course_Unit_Length__c, Course__r.Hours_Week__c, Course__r.Language__c, 
									Course__r.Only_Sold_in_Blocks__c, Course__r.Optional_Hours_Per_Week__c, Course__r.Package__c, Course__r.Id, Course__r.Maximum_length__c, Course__r.Minimum_length__c, Period__c, Is_Available__c, Unavailable_Reason__c from Campus_Course__c WHERE Campus__c = :accoId order by Course__r.Course_Category__c, Is_Available__c desc, Course__r.Course_Type__c, Course__r.Type__c, Course__r.Name]){
				if(cc.Course__r.Optional_Hours_Per_Week__c == null)	
					cc.Course__r.Optional_Hours_Per_Week__c = 0;				
				if(!listCourses.containsKey(cc.Course__r.Course_Category__c))
					listCourses.put(cc.Course__r.Course_Category__c, new Map<String, list<campus_course__c>>{cc.Course__r.Type__c => new list<Campus_Course__c>{cc}});
				else if(!listCourses.get(cc.Course__r.Course_Category__c).containsKey(cc.Course__r.Type__c) )
						listCourses.get(cc.Course__r.Course_Category__c).put(cc.Course__r.Type__c,new list<Campus_Course__c>{cc});
					else listCourses.get(cc.Course__r.Course_Category__c).get(cc.Course__r.Type__c).add(cc);
			}
		}
		return listCourses;
	
	}
	
	public pageReference editCourse(){
		PageReference page = new PageReference('/apex/scSchoolCourse_View?id='+ ApexPages.currentPage().getParameters().get('idCourse')+'&cpID='+ApexPages.currentPage().getParameters().get('cpID')+'&currentPage='+ ApexPages.currentPage().getParameters().get('currentPage')+'&edit='+true);
        page.setRedirect(true);
        return page;
	}
	
	public pageReference addCourse(){
		PageReference page = new PageReference('/apex/newCourse?cpid='+ApexPages.currentPage().getParameters().get('cpID')+'&currentPage='+ ApexPages.currentPage().getParameters().get('currentPage'));
        page.setRedirect(true);
        return page;
	}
	
	public pageReference copyCourse(){
		PageReference page = new PageReference('/apex/scCloneSchoolCourse?id=' + schoolId + '&cpid='+ApexPages.currentPage().getParameters().get('cpID')+'&currentPage='+ ApexPages.currentPage().getParameters().get('currentPage'));
        page.setRedirect(true);
        return page;
	}
	
	

}