/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CampusDetails_test {

    static testMethod void myUnitTest() {
       /* TODO: CREATE
       		- Campus (Near_to__c, Why_choose_this_campus__c, Features__c, Extra_Activities__c, Videos__c, Social_network_url__c)
       		- Nationality_Mix__c
       		- Nationality_Group__c - OK
       		- Campus_Course__c - OK
       		- Testimonial_Picture__c - OK
       		- Account_Document_File__c - OK
       		- Web_Search__c - OK
       		
       		????S-Drive????
       */ 
       
       TestFactory tf = new TestFactory();
       
       Account school = tf.createSchool();
       
       Account agency = tf.createAgency();
       
       Account campus = tf.createCampus(school, agency);
       
       Course__c course = tf.createCourse();
       
       Campus_Course__c cc = tf.createCampusCourse(campus, course);
       
       Web_Search__c webSearch = tf.createWebSearch(3, cc, 'Brazil', false);
   	   
   	   Nationality_Mix__c nm = new Nationality_Mix__c();
       nm.Account__c = campus.id;
       nm.Percentage__c = 10;
       nm.Nationality__c = 'South America';
       insert nm;
       
       ApexPages.currentPage().getParameters().put('id', campus.Id);
       CampusDetails c = new CampusDetails(new ApexPages.Standardcontroller(campus));
   	
   		c.saveAccount();
   		
   		c.nearTo = 'city';
   		c.neartoHide = 'test';
   		c.addNearTo();
   		c.getCurrencies();
   		c.nearToKey = '1';
   		c.delNearTo();
   		c.whyChoose = 'really good teachers';
   		c.whyHide='test';
   		c.addWhyChoose();
   		c.whyChooseCampusKey = '1';
   		c.delWhyChooseCampus();
   		c.features = 'feature1';
   		c.featuresHide = 'test';
   		c.addFeatures();
   		c.featuresKey = '1';
   		c.delFeatures();
   		c.extraAct = 'extra activity 1';
   		c.extraActHide = 'test';
   		c.addExtraAct();
   		c.extraActKey = '1';
   		c.delExtraAct();
   		c.natio = 'Brazil';
   		c.percent = '22';
   		c.addNatioPercent();
   		//c.natioKey = '2';
   		//c.delNatioPercent();
   		
   		c.videoHostName = 'videoHost';
   		c.addVideo();
   		c.videoListKey = '1';
   		c.delVideo();
   		c.networkName = 'LinkedIn';
   		c.addSocialNetwork();
   		c.networkKey = '1';
   		c.delSocialNetwork();
   		c.cancelEdition();
		c.getListLocation();
		c.resetSearchCourses();
		c.getFolderMap();
		//c.viewFile();
		c.getcampusInfo();
		c.getlistNationalityGroup();
		c.getListUnitsSearch();
		c.location=null;
		c.courses=null;
		c.listContacts =null;
		c.mapContacts = null;
		c.getTestimonials();
		c.getFiles();
		//c.generFolderList();
		c.openRetrievedCoursesDetails();
		
		c.editAccount();
		
		List<CampusDetails.sDrivePictures> imgs = c.imgs;
		map<string, string> videosOut = c.videosOut;
		String location = c.location;
		
		 List<CampusDetails.CourseByField> courses = c.courses;
		 //c.getCart();
		 String destination = c.destination;
		 map<string, list<Contact>> mapContacts = c.mapContacts;
		 List<String> orderedFolders = c.orderedFolders;
		  List<Contact> listContacts = c.listContacts;
    }
}