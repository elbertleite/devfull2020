@isTest
private class agency_creditcard_payment_test{

	public static User portalUser {get{
	if (null == portalUser) {
	portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
	} return portalUser;} set;}

	@testSetup static void setup() {
		TestFactory tf = new TestFactory();
		tf.setupProdcutsTest();

		Contact client = [Select Id From Contact limit 1];
	
		portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
		system.runAs(portalUser){

			ApexPages.currentPage().getParameters().put('id', client.Id);
			products_search ps = new products_search();

			for(String av : ps.result.keySet())
				for(String cat : ps.result.get(av).keyset())
					for(products_search.products p : ps.result.get(av).get(cat)){
					p.product.Selected__c = true;

					for(Quotation_List_Products__c f : p.prodFees)
						f.isSelected__c = true;
				}

			ps.saveProduct();

			client_product_service__c cliProduct = [Select Id from client_product_service__c WHERE Related_to_Product__c = null limit 1];

			client_course_product_pay payProd = new client_course_product_pay(new ApexPages.StandardController(cliProduct));

			payProd.newPayDetails.Payment_Type__c = 'Cash';
			payProd.newPayDetails.Value__c = payProd.totalPay;
			payProd.addPaymentValue();
			payProd.paidByAgency.Paid_by_Agency__c = true;
			payProd.savePayment();
		}
	}


	

	static testMethod void creditCardReconcile(){

		Test.startTest();
		system.runAs(portalUser){
			products_paid_by_agency t = new products_paid_by_agency();

			List<SelectOption> agencyGroupOptions = t.agencyGroupOptions;
			t.changeGroup();
			t.changeDepartment();
			t.getPeriods();
			t.getdateCriteriaOptions();
			t.dateCriteria = 'pr';
			t.searchProductsExcel();
			t.generateExcel();
			t.editMode();
			t.saveEdit();

			for(client_product_service__c p : t.listProducts)
				p.isSelected__c = true;
			
			t.confirmPay();

			client_product_service__c cliProduct = [Select Id, Received_By_Agency__c, Received_By_Agency__r.ParentId from client_product_service__c WHERE Creditcard_reconciled_on__c != null and Related_to_Product__c = null limit 1];
		
			products_creditcard_payment p = new products_creditcard_payment();

			Date payDate = p.payDate;
			Decimal totalInvoice = p.totalInvoice;
			Integer countProds = p.countProds;
			String selectedAgencyGroup = p.selectedAgencyGroup;
			p.selectedAgencyGroup = cliProduct.Received_By_Agency__r.ParentId;
			p.selectedAgency  = cliProduct.Received_By_Agency__c;
			List<SelectOption> pagencyGroupOptions = p.agencyGroupOptions;
			List<SelectOption> pagencyOptions = p.agencyOptions;
			List<SelectOption> pagencyProducts = p.agencyProducts;
			List<SelectOption> pperiods = p.periods;
			map<String,String> pmapAgCurrency = p.mapAgCurrency;

			p.searchProducts();


			for(client_product_service__c prod : p.listProducts)
        		prod.isSelected__c = true;

			
			p.calcInvoice();
			p.confirmPay();

						
			p.changeGroup();
			p.changeAgency();
		
		}
		Test.stopTest();

	}




}