public class ctrCpNationalityGroupCRUD{

	public Account acco {
		get{
		if(acco == null)
			acco = new Account();
		return acco;
	}
		set;
	}
	
	
	public boolean saved {get;set;}
	
	private Map<String, String> countryMap
	{
		get{if(countryMap == null) countryMap = new Map<String, String>();
		return countryMap;}
		set;	
	}
	
	private Map<String, NationalityGroup> NationalityGroupMap
	{
		get{if(NationalityGroupMap == null) NationalityGroupMap = new Map<String, NationalityGroup>();
		return NationalityGroupMap;}
		set;	
	}
	
	public List<String> selectedCountries {get{if(selectedCountries==null)selectedCountries=new List<String>();return selectedCountries;}set;}
	
	public List<SelectOption> countryList {
		get{
			if(countryList == null){
				countryList = new List<SelectOption>();
				Map<String, Schema.SObjectField> M = Nationality_Group__c.sObjectType.getDescribe().fields.getMap();
				Schema.SObjectField field = M.get('Country__c');
				Schema.DescribeFieldResult fieldDesc = field.getDescribe();
				List<Schema.PicklistEntry> ple = fieldDesc.getPicklistValues();
    			
		   		for(Schema.PicklistEntry f : ple){
		   			if(!foundEqual(f.getValue()))
		   				countryList.add(new SelectOption(f.getLabel(), f.getValue()));
		   		}
			}
			
			return countryList;
		}
		set;
	}
	
	
	public PageReference addCountry(){
		
		String groupName = getParam('groupName');
		
		if(groupName!= null && !groupName.equals('')){
			
			for(NationalityGroup n : nationGroupList){
				if(n.groupName.equals(groupName)){
					for(String country : n.selectedCountries){
						n.Countries.add(country);
						countryMap.put(country,country);
					}
					countryList = null;
					saved = false;
					break;
				}
			}
			
			
		}		
		return null;
	}
	
	private void clearFields(){		
		countryList = null;
		countryMap = null;
	}
	
	private String getParam(String name) {
		return ApexPages.currentPage().getParameters().get(name);
	}
	
	
	public PageReference removeCountry(){
		
		String countryName = getParam('countryName');
		String groupName = getParam('groupName');
		
		if(groupName!= null && !groupName.equals('')){
			
			for(integer i=0;i<nationGroupList.size();i++){
				NationalityGroup n = nationGroupList.get(i);
				if(n.groupName.equals(groupName)){
					n.Countries.remove(countryName);
					countryMap.put(countryName,countryName);
					clearFields();
					saved = false;
				}
			}
			
		}
		return null;
		
	}
	
	public PageReference removeGroup(){
		boolean toDelete = false;
		String groupName = getParam('groupName');
		String groupId = getParam('groupId');
		if(groupName!= null && !groupName.equals('')){
			System.debug('==> groupId: '+groupId);
			System.debug('==> groupName: '+groupName);
			if(groupId != null){
				if([Select count() from Course_Price__c where Campus_Course__r.Campus__r.parent.id = :groupId and Nationality__c = :groupName] > 0){
					ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'There are prices with this nationality group. You cannot delete it.');
					ApexPages.addMessage(msg);
					toDelete = false;
				}else if([Select count() from Course_Extra_Fee__c where (Campus__r.ParentID = :groupId or Campus_Course__r.Campus__r.parent.id = :groupId) and Nationality__c = :groupName] > 0){
					ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'There are extra fees with this nationality group. You cannot delete it.');
					ApexPages.addMessage(msg);
					toDelete = false;
				}else if([Select count() from Deal__c where (Campus_Account__r.ParentID = :groupID or Campus_Course__r.Campus__r.parent.id = :groupId) and Nationality_Group__c = :groupName] > 0){
					ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'There are promotions with this nationality group. You cannot delete it.');
					ApexPages.addMessage(msg);
					toDelete = false;
				}else toDelete = true;
			}else toDelete = true;
			if(toDelete)
				for(integer i=0;i<nationGroupList.size();i++){
					NationalityGroup n = nationGroupList.get(i);
					if(n.groupName.equals(groupName)){
						nationGroupList.remove(i);
						saved = false;
					}
				}
		}
		
		return null;
		
	}
	
	
	public String newGroupName {get;set;}
	
	public PageReference addNewGroup(){
		
		if(newGroupName!= null && !newGroupName.trim().equals('')){
			
			NationalityGroup newng = new NationalityGroup();
			newng.groupName = newGroupName;
			nationGroupList.add(newng);
			newGroupName = '';
			
			saved = false;
			
		} else {
			
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Enter a group name');
			ApexPages.addMessage(msg);	
		}
		
		return null;
	}
	
	
	
	private boolean foundEqual(String country){
		
		if(countryMap.containsKey(country))
			return true;
		
		return false;
				
	}
	
	public List<NationalityGroup> nationGroupList
	{
		get{
			if(nationGroupList == null) 
				nationGroupList = getNationalityGroups();
			return nationGroupList;}
		set;
	}
	
	
	public void saveName(){
		String newName = Apexpages.currentPage().getParameters().get('newName').trim();
		String oldName = Apexpages.currentPage().getParameters().get('oldName').trim();
		
		//oldName = oldName.replaceAll('&amp;', '&').replaceAll('&nbsp;', ' ');

		System.debug('$$> newName: ' + newName);
		System.debug('$$> oldName: ' + oldName);
		
		String schoolID = acco.id;		
		
		if(newName != null && oldName != null){
			
			List<Nationality_Group__c> ngList = [Select Name from Nationality_Group__c where Name = :oldName and Account__c = :schoolID];
			for(Nationality_Group__c ng : ngList)
				ng.Name = newName;
			
			update ngList;
			
			List<Deal__c> promotions = [Select Nationality_Group__c from Deal__c D where (campus_course__r.campus__r.Parentid = :schoolID or campus_account__r.Parentid = :schoolID) and Nationality_Group__c = :oldName];
			for(Deal__c d : promotions)
				d.Nationality_Group__c = newName;
				
			update promotions;
			
			
			List<Course_Extra_Fee__c> fees = [Select Nationality__c from Course_Extra_Fee__c  where (campus_course__r.campus__r.Parentid = :schoolID or campus__r.Parentid = :schoolID) and Nationality__c = :oldName];
			for(Course_Extra_Fee__c cef : fees)
				cef.Nationality__c = newName;
				
			update fees;
			
			
			List<Course_Price__c> prices = [Select Nationality__c from Course_Price__c  where campus_course__r.campus__r.Parentid = :schoolID and Nationality__c = :oldName];
			for(Course_Price__c p : prices)
				p.Nationality__c = newName;
				
			update prices;
		
		}
	}
	
	public boolean hasNationalityGroups {get;set;}
	private List<NationalityGroup> getNationalityGroups(){
		saved = false;
		List<NationalityGroup> nationalityGroupList;
		
		NationalityGroupMap = new Map<String, NationalityGroup>();
		List<Nationality_Group__c> ngroups = [Select N.Id, N.Country__c, N.Name, N.Account__c from Nationality_Group__c N where N.Account__c = :acco.id order by Name, N.Country__c];
		
		if(ngroups != null && ngroups.size() > 0){
			hasNationalityGroups = true;
			for(Nationality_Group__c c : ngroups)
			{
				if(!NationalityGroupMap.containsKey(c.Name))
				{
					NationalityGroup ng = new NationalityGroup();
					ng.groupName = c.Name;
					ng.id = c.id;
					ng.accountId = c.Account__c;
					ng.Countries.add(c.Country__c);
					NationalityGroupMap.put(ng.groupName,ng);
					countryMap.put(c.Country__c,c.Country__c);
				}
				else{
					NationalityGroupMap.get(c.Name).Countries.add(c.Country__c);
					countryMap.put(c.Country__c,c.Country__c);
				}
			}	
			nationalityGroupList = NationalityGroupMap.values();
			saved = true;
			
		} else { //Default nationalities
			hasNationalityGroups = false;			
			List<NationalityGroup> standardList = new List<NationalityGroup>();
					
			if(!saved){
				
				NationalityGroup n = new NationalityGroup();
				n.groupName = 'Latin America';
				n.Countries = new Set<String>{'Argentina','Bolivia','Brazil','Chile','Colombia','Cuba','Ecuador','Mexico','Paraguay','Peru','Uruguay','Venezuela'};
				standardList.add(n);
				NationalityGroupMap.put(n.groupName,n);
				for(String s : n.Countries)
					countryMap.put(s,s);
				
				n = new NationalityGroup();
				n.groupName = 'Eastern Europe';
				n.Countries = new Set<String>{'Belarus','Bulgaria','Czech Republic','Estonia','Hungary','Latvia','Lithuania','Poland','Slovakia','Slovenia'};
				standardList.add(n);
				NationalityGroupMap.put(n.groupName,n);
				for(String s : n.Countries)
					countryMap.put(s,s);
				
				n = new NationalityGroup();
				n.groupName = 'Western Europe';
				n.Countries = new Set<String>{'Austria','Belgium','Denmark','Finland','France','Germany','Ireland','Italy','Netherlands',
												'Norway','Portugal','Spain','Sweden','Switzerland','United Kingdom'};
				standardList.add(n);
				NationalityGroupMap.put(n.groupName,n);
				for(String s : n.Countries)
					countryMap.put(s,s);
					
			}
			
			nationalityGroupList = standardList;
			
		}
		
		return nationalityGroupList;
	}
	
	
	
	
	public class NationalityGroup{
	
		public ID id {
			get;
			set;
		}
		
		public Integer numberOfCountries {
			get {
				return countries.size();
			}
		}
		
		public ID accountId {
			get;
			set;
		}		
		public String groupName {
			get{if(groupName == null) groupName = '';
			return groupName;}
			set;
		}
		
		public integer countriesSize {
			get {
				if(countries != null)
					countriesSize = countries.size();
				return countriesSize;
			}
			set;
		}
		
		public Set<String> Countries {
			get{if(Countries == null) Countries = new Set<String>();
			return Countries;}
			set;	
		}
		
		public list<string> selectedCountries {get;set;}
		
	}
	
		
	public PageReference saveChanges(){
		
		List<Nationality_Group__c> originals = [Select N.Id, N.Country__c, N.Name from Nationality_Group__c N where N.Account__c = :acco.id order by Name, N.Country__c];
		List<Nationality_Group__c> toDelete = new List<Nationality_Group__c>();
		List<Nationality_Group__c> toInsert = new List<Nationality_Group__c>();
		set<string> idList = new set<string>();
		//set<string> nationList = new set<string>();
		//Checking new Nationality groups and Countries.
		for(NationalityGroup n : nationGroupList){
			for(String country : n.Countries ){
				boolean found = false;
				
				for(Nationality_Group__c nOriginal : originals){
					if(n.groupName == nOriginal.Name && country == nOriginal.Country__c){
						found = true;
						break;
					}
				}
				
				if(!found){
					Nationality_Group__c ng = new Nationality_Group__c();
					ng.Account__c = acco.id;
					ng.Country__c = country;
					ng.Name = n.groupName;
					toInsert.add(ng);
				}
			}
		}
		insert toInsert;
		
		
		//Checking removed Nationality groups and Countries.
		
		originals = [Select N.Id, N.Country__c, N.Name, N.Account__c from Nationality_Group__c N where N.Account__c = :acco.id order by Name, N.Country__c];
		
		for(Nationality_Group__c nOriginal : originals){
			boolean found = false;
			
			for(NationalityGroup n : nationGroupList){
				for(String country : n.Countries ){
					if(nOriginal.Name == n.groupName && nOriginal.Country__c == country){
						found = true;
						break;
					}					
				}
			}
			
			if(!found){
				toDelete.add(nOriginal);
				idList.add(nOriginal.Account__c);
				//nationList.add(nOriginal.name);
			}
			
		}
		
	/*	System.debug('==> idList.size: '+idList);
		System.debug('==> idList.size: '+nationList);
		if(idList.size() > 0){
			if([Select count() from Course_Price__c where Campus_Course__r.Campus__r.parent.id in :idList and Nationality__c in :nationList] > 0){
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'There are prices with this nationality group. You cannot delete it.');
				ApexPages.addMessage(msg);
				System.debug('==> error: '+msg);
			}//else
		}*/
		//if(idList.size() > 0)
			delete toDelete;
		nationGroupList = null;
		clearFields();
		saved = true;
		return null;
	}
	
	/*public PageReference saveGroups(){
		
		removeNGroups();
		
		List<Nationality_Group__c> ngl = new List<Nationality_Group__c>();
		
		for(NationalityGroup n : nationGroupList){
			Nationality_Group__c ng;
			for(String country : n.Countries ){
				ng = new Nationality_Group__c();
				ng.Account__c = acco.id;
				ng.Country__c = country;
				ng.Name = n.groupName;
				ngl.add(ng);					
			}
		}
		
		
		try {
			UPSERT ngl;			
			nationGroupList = null;
			clearFields();
			saved = true;
		} catch(Exception e) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
			ApexPages.addMessage(msg);					
		}
			
		
		return null;
	}
	
	
	private void removeNGroups(){
		
		List<Nationality_Group__c> n = [Select N.Id from Nationality_Group__c N where N.Account__c = :acco.id];
		
		try {
			delete n;
		} catch(Exception e) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
			ApexPages.addMessage(msg);					
		}
		
	}*/

	
}