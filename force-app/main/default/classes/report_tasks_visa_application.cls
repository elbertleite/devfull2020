public with sharing class report_tasks_visa_application {
	public report_tasks_visa_application() {
		
	}

	private set<string> profileAnalizeTriggerSet = new set<string>{'BSB Profile Analyze'};
	private set<string> profileAnalizeResultSet = new set<string>{'BSB Profile Attention','BSB Profile OK','BSB Profile Risk'};
	private set<string> documentAnalizeTriggerSet = new set<string>{'BSB Documents Analyze'};
	private set<string> documentAnalizeResultSet = new set<string>{'BSB Docs Reviw','BSB Documents OK','BSB Letter Review'};
	private set<string> visaApplicationTriggerSet = new set<string>{'Visa Application'};
	private set<string> visaApplicationResultSet = new set<string>{'Visa Applied','Visa Confirmation','Visa Danied'};
	private set<string> fullServiceSet = new set<string>{'BSB Full Service'};
	private list<string> listStatus  = new list<string>{'BSB Profile Analyze','BSB Profile Attention','BSB Profile OK','BSB Profile Risk','BSB Documents Analyze','BSB Docs Reviw','BSB Documents OK','BSB Letter Review','Visa Application','Visa Applied','Visa Confirmation','Visa Danied','BSB Full Service'};

	public class visaClassification{
		public string currentTask{get; set;}
		public date startDate{get; set;}
		public date endDate{get; set;}
	}

	public integer totalItems{get; set;}
	public integer totalItemsNoTasks{get; set;}

	public class reportVisa{
		public string clientId{get; set;}
		public string client{get; set;}
		public string owner{get; set;}
		public string originalAgency{get; set;}
		public string destinationCity{get; set;}
		public string agency{get; set;}
		public string destination{get; set;}
		public date startDate{get; set;}
		public visaClassification profileAnalize{get; set;}
		public visaClassification documentAnalize{get; set;}
		public visaClassification visaApplication{get; set;}
		public visaClassification fullService{get; set;}
	}

	public map<string, reportVisa> listClientsVisa{get; set;}
	
	// private date returnDate(string vl){
	// 	List<String> dt = vl.split('/');
	// 	return date.newinstance(integer.valueOf(dt[2]), integer.valueOf(dt[1]), integer.valueOf(dt[0]);
	// }

	public set<string> clientsId{get; set;}
	public map<string, Client_Course_Overview__c> clientsCourse{get; set;}
	public map<string, Contact> clientsVisa{get; set;}
	public void retrieveReportVisas(){
		string agencyId = ApexPages.CurrentPage().getParameters().get('agencyId');
		string groupId = ApexPages.CurrentPage().getParameters().get('groupId');
		date dateFrom = date.parse(ApexPages.CurrentPage().getParameters().get('dateFrom'));
		date dateTo = date.parse(ApexPages.CurrentPage().getParameters().get('dateTo'));
		string destination = ApexPages.CurrentPage().getParameters().get('destination');
		clientsId = new set<string>();
		clientsCourse = new map<string, Client_Course_Overview__c>();
		string sqlCli = 'Select Client__c, Course_Start_Date__c  from Client_Course_Overview__c where ';
		if(agencyId != null && agencyId.trim() != '' && agencyId != 'all' )
			sqlCli += ' Client__r.current_Agency__c = :agencyId ';
		else if(groupId != null && groupId.trim() != '' && groupId != 'all' )
			sqlCli += ' (Client__r.current_Agency__r.ParentId = :groupId) ';
		if(destination != null && destination != 'all')
			sqlCli += ' and Client__r.Destination_Country__c = :destination ';
		sqlCli += ' and Course_Start_Date__c >= :dateFrom and Course_Start_Date__c <= :dateTo order by Client__c, Course_Start_Date__c';

		System.debug('==>groupId '+groupId);
		System.debug('==>dateFrom '+ dateFrom);
		System.debug('==>dateTo '+dateTo);
		System.debug('==>sqlCli '+sqlCli);
		for(Client_Course_Overview__c cco:dataBase.query(sqlCli))
			if(!clientsCourse.containsKey(cco.Client__c))
				clientsCourse.put(cco.Client__c,cco);

		clientsId.addAll(clientsCourse.keySet());

		clientsVisa = new map<string, Contact>([Select id, (SELECT Visa_type__c, Visa_subclass__c, Visa_country_applying_for__c, Date_granted__c, Expiry_Date__c, Visa_Date_Applied__c, Visa_Immigration_Officer__c, Visa_Status__c FROM Client_Documents__r WHERE Document_Category__c = 'Visa' AND Document_Type__c = 'Visa' AND Expiry_Date__c != NULL order by Expiry_Date__c desc limit 1) from contact where id in :clientsId]); 
		

		totalItems = 0;

		listClientsVisa = new map<string, reportVisa>();
		reportVisa client;
		string sql = 'SELECT related_Contact__c, related_Contact__r.name, related_Contact__r.owner__r.name, related_Contact__r.original_agency__r.name, related_Contact__r.Destination_Country__c, related_Contact__r.Destination_City__c, related_Contact__r.current_agency__r.name, CreatedDate, lastModifiedDate,Due_Date__c,Id,Status__c,Subject__c FROM Custom_Note_Task__c where ';
		sql += ' related_Contact__c in :clientsId ';
		if(agencyId != null && agencyId.trim() != '' && agencyId != 'all' ){
			// sql += ' and related_Contact__r.current_Agency__c = :agencyId ';
			sql += ' and (Assign_To__r.contact.account.ParentId = :groupId) ';
			// sql += ' (Assign_To__r.contact.accountid = :agencyId) and ';
		}
		else if(groupId != null && groupId.trim() != '' && groupId != 'all' )
			sql += ' and (Assign_To__r.contact.account.ParentId = :groupId) ';
		// sql += ' ((createdDate >= :dateFrom and ';
		// sql += ' createdDate <= :dateTo)  or ';
		// sql += ' (lastModifiedDate >= :dateFrom and ';
		// sql += ' lastModifiedDate <= :dateTo))  ';
		sql += ' and Subject__c in :listStatus and related_Contact__c != null ';
		sql += ' order by related_Contact__r.name, Subject__c, CreatedDate, lastModifiedDate';
		System.debug('==> listStatus: '+listStatus);
		System.debug('==> sql: '+sql);
		for(Custom_Note_Task__c nt:dataBase.query(sql)){
			if(!listClientsVisa.containsKey(nt.related_Contact__c)){
				clientsId.remove(nt.related_Contact__c);
				client = new reportVisa();
				client.clientId = nt.related_Contact__c;
				client.owner = nt.related_Contact__r.owner__r.name;
				client.originalAgency = nt.related_Contact__r.original_agency__r.name;
				client.destinationCity = nt.related_Contact__r.Destination_City__c;
				client.client = nt.related_Contact__r.name;
				client.agency = nt.related_Contact__r.current_agency__r.name;
				client.startDate = clientsCourse.get(nt.related_Contact__c).Course_Start_Date__c;
				client.destination = nt.related_Contact__r.Destination_Country__c;
				if(profileAnalizeTriggerSet.contains(nt.Subject__c)){
					client.profileAnalize = new visaClassification();
					client.profileAnalize.currentTask = nt.Subject__c;
					client.profileAnalize.startDate = date.valueOf(nt.CreatedDate);
				}else if(profileAnalizeResultSet.contains(nt.Subject__c)){
					if(client.profileAnalize == null)
						client.profileAnalize = new visaClassification();
					if(nt.Status__c == 'Completed')
						client.profileAnalize.endDate = date.valueOf(nt.lastModifiedDate);
					else client.profileAnalize.endDate = null;
				}else if(documentAnalizeTriggerSet.contains(nt.Subject__c)){
					client.documentAnalize = new visaClassification();
					client.documentAnalize.currentTask = nt.Subject__c;
					client.documentAnalize.startDate = date.valueOf(nt.CreatedDate);
				}else if(documentAnalizeResultSet.contains(nt.Subject__c)){
					if(client.documentAnalize == null)
						client.documentAnalize = new visaClassification();
					if(nt.Status__c == 'Completed')
						client.documentAnalize.endDate = date.valueOf(nt.lastModifiedDate);
					else client.documentAnalize.endDate = null;
				}else if(visaApplicationTriggerSet.contains(nt.Subject__c)){
					client.visaApplication = new visaClassification();
					client.visaApplication.currentTask = nt.Subject__c;
					client.visaApplication.startDate = date.valueOf(nt.CreatedDate);
				}else if(visaApplicationResultSet.contains(nt.Subject__c)){
					if(client.visaApplication == null)
						client.visaApplication = new visaClassification();
					if(nt.Status__c == 'Completed')
						client.visaApplication.endDate = date.valueOf(nt.lastModifiedDate);
					else client.visaApplication.endDate = null;
				}else if(fullServiceSet.contains(nt.Subject__c)){
					client.fullService = new visaClassification();
					client.fullService.currentTask = nt.Subject__c;
					if(client.fullService.startDate == null)
						client.fullService.startDate = date.valueOf(nt.CreatedDate);
					if(nt.Status__c == 'Completed')
						client.fullService.endDate = date.valueOf(nt.lastModifiedDate);
					else client.fullService.endDate = null;
				}
					
				listClientsVisa.put(nt.related_Contact__c, client);
			}else{
				if(profileAnalizeTriggerSet.contains(nt.Subject__c)){
					if(listClientsVisa.get(nt.related_Contact__c).profileAnalize == null)
						listClientsVisa.get(nt.related_Contact__c).profileAnalize = new visaClassification();
					listClientsVisa.get(nt.related_Contact__c).profileAnalize.currentTask = nt.Subject__c;
					if(listClientsVisa.get(nt.related_Contact__c).profileAnalize.startDate == null)
						listClientsVisa.get(nt.related_Contact__c).profileAnalize.startDate = date.valueOf(nt.CreatedDate);
				}else if(profileAnalizeResultSet.contains(nt.Subject__c)){
					if(listClientsVisa.get(nt.related_Contact__c).profileAnalize == null)
						listClientsVisa.get(nt.related_Contact__c).profileAnalize = new visaClassification();
					if(nt.Status__c == 'Completed')
						listClientsVisa.get(nt.related_Contact__c).profileAnalize.endDate = date.valueOf(nt.lastModifiedDate);
					else listClientsVisa.get(nt.related_Contact__c).profileAnalize.endDate = null;
				}else if(documentAnalizeTriggerSet.contains(nt.Subject__c)){
					if(listClientsVisa.get(nt.related_Contact__c).documentAnalize == null)
						listClientsVisa.get(nt.related_Contact__c).documentAnalize = new visaClassification();
					listClientsVisa.get(nt.related_Contact__c).documentAnalize.currentTask = nt.Subject__c;
					if(listClientsVisa.get(nt.related_Contact__c).documentAnalize.startDate == null)
						listClientsVisa.get(nt.related_Contact__c).documentAnalize.startDate = date.valueOf(nt.CreatedDate);
				}else if(documentAnalizeResultSet.contains(nt.Subject__c)){
					if(listClientsVisa.get(nt.related_Contact__c).documentAnalize == null)
						listClientsVisa.get(nt.related_Contact__c).documentAnalize = new visaClassification();
					if(nt.Status__c == 'Completed')
						listClientsVisa.get(nt.related_Contact__c).documentAnalize.endDate = date.valueOf(nt.lastModifiedDate);
					else listClientsVisa.get(nt.related_Contact__c).documentAnalize.endDate = null;
				}else if(visaApplicationTriggerSet.contains(nt.Subject__c)){
					if(listClientsVisa.get(nt.related_Contact__c).visaApplication == null)
						listClientsVisa.get(nt.related_Contact__c).visaApplication = new visaClassification();
					listClientsVisa.get(nt.related_Contact__c).visaApplication.currentTask = nt.Subject__c;
					if(listClientsVisa.get(nt.related_Contact__c).visaApplication.startDate == null)
						listClientsVisa.get(nt.related_Contact__c).visaApplication.startDate = date.valueOf(nt.CreatedDate);
				}else if(visaApplicationResultSet.contains(nt.Subject__c)){
					if(listClientsVisa.get(nt.related_Contact__c).visaApplication == null)
						listClientsVisa.get(nt.related_Contact__c).visaApplication = new visaClassification();
					if(nt.Status__c == 'Completed')
						listClientsVisa.get(nt.related_Contact__c).visaApplication.endDate = date.valueOf(nt.lastModifiedDate);
					else listClientsVisa.get(nt.related_Contact__c).visaApplication.endDate = null;
				}else if(fullServiceSet.contains(nt.Subject__c)){
					if(listClientsVisa.get(nt.related_Contact__c).fullService == null)
						listClientsVisa.get(nt.related_Contact__c).fullService = new visaClassification();
					listClientsVisa.get(nt.related_Contact__c).fullService.currentTask = nt.Subject__c;
					if(listClientsVisa.get(nt.related_Contact__c).fullService.startDate == null)
						listClientsVisa.get(nt.related_Contact__c).fullService.startDate = date.valueOf(nt.CreatedDate);
					if(nt.Status__c == 'Completed')
						listClientsVisa.get(nt.related_Contact__c).fullService.endDate = date.valueOf(nt.lastModifiedDate);
					else listClientsVisa.get(nt.related_Contact__c).fullService.endDate = null;
				}
			}
		}
		totalItems = listClientsVisa.size();
		totalItemsNoTasks = clientsId.size();
		if(clientsId.size() > 0)
			retireveClientsNoTasks();
		else clientsNoTasks = new list<contact>();
	}

	public list<contact> clientsNoTasks{get; set;}
	public void retireveClientsNoTasks(){
		clientsNoTasks = new list<contact>([Select id, name, Destination_Country__c, current_agency__r.name from contact where id in:clientsId]);
	}

	public class tasksDetails{
		public string destination{get; set;}
		public string createdDate{get; set;}
		public string lastModifiedDate{get; set;}
		public string subject{get; set;}
		public string status{get; set;}
		public string description{get; set;}
		public tasksDetails(string destination, string createdDate, string lastModifiedDate, string subject, string status, string description){
			this.destination = destination;
			this.createdDate = createdDate;
			this.lastModifiedDate = lastModifiedDate;
			this.subject = subject;
			this.status = status;
			this.description = description;
		}
	}
	@RemoteAction
	public static list<tasksDetails> retrieveClientTasks(string clientId, string dtFrom, string dtTo, string groupId){
		date dateFrom = date.parse(dtFrom);
		date dateTo = date.parse(dtTo);
		list<string> listTasksStatus  = new list<string>{'BSB Profile Analyze','BSB Profile Attention','BSB Profile OK','BSB Profile Risk','BSB Documents Analyze','BSB Docs Reviw','BSB Documents OK','BSB Letter Review','Visa Application','Visa Applied','Visa Confirmation','Visa Danied','BSB Full Service'};
		list<tasksDetails> listTaks = new list<tasksDetails>();
		string sql = 'SELECT related_Contact__c, related_Contact__r.name, related_Contact__r.owner__r.name, related_Contact__r.original_agency__r.name, related_Contact__r.current_agency__r.name, related_Contact__r.Destination_Country__c, related_Contact__r.Destination_City__c,CreatedDate,lastModifiedDate,Due_Date__c,Id,Status__c,Subject__c, Comments__c FROM Custom_Note_Task__c ';
		sql += ' where related_Contact__c = :clientId and Subject__c in :listTasksStatus ';
		sql += ' and Assign_To__r.contact.account.ParentId = :groupId ';
		// sql += ' and ((createdDate >= :dateFrom and createdDate <= :dateTo)   ';
		// sql += ' or (lastModifiedDate >= :dateFrom and lastModifiedDate <= :dateTo))  ';
		sql += ' order by Subject__c, CreatedDate, lastModifiedDate';
		for(Custom_Note_Task__c ctn:dataBase.query(sql))
			listTaks.add(new tasksDetails(ctn.related_Contact__r.Destination_Country__c, ctn.CreatedDate.format('dd/MM/yyyy'), ctn.lastModifiedDate.format('dd/MM/yyyy'), ctn.Subject__c, ctn.Status__c, ctn.Comments__c));
		return listTaks; 
	}

	@RemoteAction
    public static list<Account> changeAccount() {
        return [SELECT Id, Name FROM Account WHERE recordType.name = 'Agency Group' order by name];
    }

    @RemoteAction
    public static list<Account> changeAgency(string groupId) {
        return [SELECT Id, Name FROM Account WHERE parentId = :groupId order by name];
    }
	
	 @RemoteAction
	public static list<AggregateResult> changeDestination() {
		noSharing ns = new noSharing();
		return ns.changeDestination();
	}
	
   
	private string groupID{
		get{
			if(groupID == null){
				try {
					groupID = [select Contact.Account.ParentId from User where id =:UserInfo.getUserId() limit 1].Contact.Account.ParentId;
				} catch (Exception e){
					groupID = '';
				}
			}
			return groupID;
		}
		set;
	}

	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}		

	public without sharing class noSharing{
		public list<AggregateResult> changeDestination() {
			return [Select BillingCountry from Account where recordType.name = 'campus' group by BillingCountry order by BillingCountry];
		}
	}

	public PageReference generateExcel(){
		string agencyId = ApexPages.CurrentPage().getParameters().get('agencyId');
		string groupId = ApexPages.CurrentPage().getParameters().get('groupId');
		string dateFrom = ApexPages.CurrentPage().getParameters().get('dateFrom');
		string dateTo = ApexPages.CurrentPage().getParameters().get('dateTo');
		string destination = ApexPages.CurrentPage().getParameters().get('destination');
		PageReference pr = new Pagereference('/apex/report_tasks_visa_application_excel?agencyId='+agencyId+'&groupId='+groupId+'&dateFrom='+dateFrom+'&dateTo='+dateTo+'&destination='+destination);
		// Page.;
		// pr.getParameters().put('agencyId',agencyId);
		// pr.getParameters().put('groupId',groupId);
		// pr.getParameters().put('dateFrom',dateFrom);
		// pr.getParameters().put('dateTo',dateTo);
		// pr.getParameters().put('destination',destination);
		pr.setRedirect(true);
		return pr;
	}


}