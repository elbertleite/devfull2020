@isTest
private class report_conversion_rate_test{
	static testMethod void myUnitTest() {
		TestFactory tf = new TestFactory();
		
		Account agency = tf.createAgency();

		tf.createChecklists(agency.Parentid);

		Contact emp = tf.createEmployee(agency);
		
		Contact lead = tf.createLead(agency, emp);
		lead.Ownership_History_Field__c = '[{"toUserID":"005O0000004bBaSIAU","toUser":"Patricia Greco","toAgencyID":"0019000001MjqIIAAZ","toAgency":"IP Australia Sydney (HO)","fromUserID":null,"fromUser":null,"fromAgencyID":null,"fromAgency":null,"createdDate":"2018-09-18T04:17:12.024Z","createdByUserID":"005O0000004bBaSIAU","createdByUser":"Patricia Greco","action":"Taken"}]';
		lead.Lead_Stage__c = 'Stage 1';
		lead.Lead_Area_of_Study__c = 'Test 1; Test 2';
		update lead;

		Contact lead2 = tf.createClient(agency);
		Contact lead3 = tf.createClient(agency);

		List<String> leads = new List<String>();
		leads.add(lead.ID);
		leads.add(lead2.ID);
		leads.add(lead3.ID);

		Contact client = tf.createClient(agency);

		User portalUser = tf.createPortalUser(emp);

		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);

       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);

		Quotation__c q1 = tf.createQuotation(lead, campusCourse);
		Quotation__c q2 = tf.createQuotation(lead, campusCourse);

		Destination_Tracking__c dt = new Destination_tracking__c();
		dt.Client__c = lead.id;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
		dt.Expected_Travel_Date__c = system.today().addMonths(3);
		dt.lead_Cycle__c = false;
		insert dt;

		Client_Checklist__c checklist = new Client_Checklist__c();
		checklist.Agency__c = agency.id;
		checklist.Agency_Group__c = agency.Parentid;
		checklist.Destination__c = lead.Destination_Country__c;
		checklist.Checklist_Item__c = 'aaa';
		checklist.Client__c = lead.id;
		checklist.Destination_Tracking__c = dt.id;
		insert checklist;

		Test.startTest();
     	system.runAs(portalUser){
		
			emp.Account = agency;
			portalUser.contact = emp;
			update portalUser;

			Client_Stage__c csc = new Client_Stage__c();
			csc.Stage_description__c = 'LEAD - Not Interested';
			csc.Agency_Group__c = portalUser.Contact.Account.Parentid;
			csc.Stage__c = 'Stage 0';
			csc.Stage_Sub_Options__c = '["Option Not Interested 1","Option Not Interested 2","Option Not Interested 3"]';
			insert csc;

			Client_Stage_Follow_up__c cs = new Client_Stage_Follow_up__c();
			cs.Agency__c = portalUser.Contact.Account.id;
			cs.Agency_Group__c = portalUser.Contact.Account.Parentid;
			cs.Client__c = lead.id;
			cs.Destination__c = 'Australia';
			cs.Stage_Item__c = csc.Stage_Description__c;
			cs.Stage_Item_Id__c = csc.id;
			cs.Stage__c = 'Stage 0';
			cs.Destination_Tracking__c = dt.id;
			cs.Last_Saved_Date_Time__c = Datetime.now();
			insert cs;
			
			csc = new Client_Stage__c();
			csc.Stage_description__c = 'Test';
			csc.Agency_Group__c = portalUser.Contact.Account.Parentid;
			csc.Stage__c = 'Stage 0';
			csc.Stage_Sub_Options__c = '["Option Not Interested 1","Option Not Interested 2","Option Not Interested 3"]';
			insert csc;

			cs = new Client_Stage_Follow_up__c();
			cs.Agency__c = portalUser.Contact.Account.id;
			cs.Agency_Group__c = portalUser.Contact.Account.Parentid;
			cs.Client__c = lead.id;
			cs.Destination__c = 'Australia';
			cs.Stage_Item__c = csc.Stage_Description__c;
			cs.Stage_Item_Id__c = csc.id;
			cs.Stage__c = 'Stage 0';
			cs.Destination_Tracking__c = dt.id;
			cs.Last_Saved_Date_Time__c = Datetime.now();
			insert cs;
			
			csc = new Client_Stage__c();
			csc.Stage_description__c = 'Test 2';
			csc.Agency_Group__c = portalUser.Contact.Account.Parentid;
			csc.Stage__c = 'Stage 0';
			insert csc;

			cs = new Client_Stage_Follow_up__c();
			cs.Agency__c = portalUser.Contact.Account.id;
			cs.Agency_Group__c = portalUser.Contact.Account.Parentid;
			cs.Client__c = lead.id;
			cs.Destination__c = 'Australia';
			cs.Stage_Item__c = csc.Stage_Description__c;
			cs.Stage_Item_Id__c = csc.id;
			cs.Stage__c = 'Stage 0';
			cs.Destination_Tracking__c = dt.id;
			cs.Last_Saved_Date_Time__c = Datetime.now();
			insert cs;
			
			csc = new Client_Stage__c();
			csc.Stage_description__c = 'Test 3';
			csc.Agency_Group__c = portalUser.Contact.Account.Parentid;
			csc.Stage__c = 'Stage 0';
			insert csc;

			cs = new Client_Stage_Follow_up__c();
			cs.Agency__c = portalUser.Contact.Account.id;
			cs.Agency_Group__c = portalUser.Contact.Account.Parentid;
			cs.Client__c = lead.id;
			cs.Destination__c = 'Australia';
			cs.Stage_Item__c = csc.Stage_Description__c;
			cs.Stage_Item_Id__c = csc.id;
			cs.Stage__c = 'Stage 0';
			cs.Destination_Tracking__c = dt.id;
			cs.Last_Saved_Date_Time__c = Datetime.now();
			insert cs;

			report_conversion_rate_2 controller2 = new report_conversion_rate_2();
			
			Apexpages.currentPage().getParameters().put('export', 'true');
			Apexpages.currentPage().getParameters().put('begin', Date.today().format());
			Apexpages.currentPage().getParameters().put('end', Date.today().addMonths(-1).format());
			Apexpages.currentPage().getParameters().put('group', agency.Parentid);
			Apexpages.currentPage().getParameters().put('agency', agency.id);
			Apexpages.currentPage().getParameters().put('destination', 'Australia');
			Apexpages.currentPage().getParameters().put('typeOfSearh', 'firstClick');

			controller2 = new report_conversion_rate_2();

			controller2.getReasons();
			controller2.getDestinations();
			controller2.getGroups();
			controller2.getTimeInSeconds(Datetime.now());
			controller2.updateAgencies();
			controller2.updateEmployees();
			//controller2.updateSelectedCampaign();
			controller2.updateEmployees();
			controller2.typeOfSearch = ';;;';
			controller2.generateReport();
			controller2.generateExcel();

			Apexpages.currentPage().getParameters().put('export', 'true');
			controller2 = new report_conversion_rate_2();

			controller2.checkedDate = Date.today();
			controller2.checkedBy = '';

			report_conversion_rate_2.ContactsForExcel cex = new report_conversion_rate_2.ContactsForExcel();
			cex.contactID = '';
			cex.contactEmail = '';
			cex.contactName = '';
			cex.contactStatus = '';
			cex.status = '';
			cex.stageStatus = '';
			cex.destination = '';
			cex.checkedDate = Date.today();
			cex.checkedBy = '';
			
			report_conversion_rate_2.AverageTimePerContact atc = new report_conversion_rate_2.AverageTimePerContact();
			atc.item = '';
			atc.dateSaved = null;
			atc.itemOrder = 1;
			atc.daysInStatus = null;

			atc = new report_conversion_rate_2.AverageTimePerContact('', null, 1);

			atc.equals(atc);
			atc.compareTo(atc);
			atc.hashCode();

			report_conversion_rate_2.ReportResult r2 = new report_conversion_rate_2.ReportResult();
			report_conversion_rate_2.ReportResult r2_1 = new report_conversion_rate_2.ReportResult('');
			

			system.debug(r2.colorWon);
			system.debug(r2.colorCurrent);
			system.debug(r2.colorStagnated);
			system.debug(r2.colorLost);

			r2.equals(r2_1);
			r2_1.hashCode();
			r2.compareTo(r2_1);
			
			r2.statusID = '';
			r2.status = '';
			r2.total = 0;
			r2.current = 0;
			r2.lost = 0;
			r2.stagnated  = 0;
			r2.itemOrder = 0;
			r2.won = 0;

			r2.totalDaysInStatus = 0;
			r2.averageDays = 0;

			r2.colorWon = '';
			r2.colorCurrent = '';
			r2.colorStagnated = '';
			r2.colorLost = '';

			report_conversion_rate.initPage();
			report_conversion_rate.refreshGroup(agency.Parentid);
			report_conversion_rate.refreshEmployees(new List<String>{agency.ID});
			report_conversion_rate.generateReport(agency.Parentid, new List<String>{agency.ID}, emp.ID, 'Australia', 'firstClick', new List<String>{'Test'},'26/03/2019', '26/03/2019', true, new List<String>{agency.ID});
			report_conversion_rate.generateReport(agency.Parentid, new List<String>{agency.ID}, emp.ID, 'Australia', 'firstClick', null,'26/03/2019', '26/03/2019', true, new List<String>{agency.ID});
			report_conversion_rate.saveFieldsForExcel(agency.Parentid, new List<String>{agency.ID}, emp.ID, 'Australia', 'firstClick', new List<String>{'Test'},'26/03/2019', '26/03/2019', true, new List<String>{agency.ID});
			new report_conversion_rate().generateExcelFile();

			report_conversion_rate.ContactsForExcel cfe = new report_conversion_rate.ContactsForExcel();
			cfe.contactID = '';
			cfe.contactEmail = '';
			cfe.contactName = '';
			cfe.contactStatus = '';
			cfe.contactOrigin = '';
			cfe.rdStationCampaign = '';
			cfe.enrolledCourse = '';
			cfe.status = '';
			cfe.stageStatus = '';
			cfe.destination = '';
			cfe.checkedDate = '';
			cfe.createdDate = '';
			cfe.conversionDate = '';
			cfe.checkedBy = '';
			cfe.currentAgency = '';
			cfe.lostReason = '';
			cfe.lostComment = ''; 
		}
	}
}