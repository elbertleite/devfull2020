public with sharing class agency_staff_list {

	private String accoId;
	public agency_staff_list(ApexPages.StandardController controller){
		accoId = controller.getId();
	}

	public Boolean isActiveUser{
		get{
			if(isActiveUser == null)
				isActiveUser = true;
			return isActiveUser;
		}
		Set;
	}
	public string accRTName {
		get{
			String rectype = [Select RecordType.Name From Account where id= :accoId].RecordType.Name;

			if(rectype=='campus')
				return 'School';
			else
				return 'Agency';
		}
		set;}
	private list<User> listStaff;
	private List<ID> activeContacts;
	public List<User> getListStaff(){
		if(listStaff == null){
			activeContacts = new List<ID>();
			accessTypes = new Map<String, String>();

			listStaff = [SELECT id, LastLoginDate, SmallPhotoURL, ContactID, Contact.Name, Contact.Account.Name, Contact.Email, Contact.Title, Contact.MobilePhone, Contact.Department, Contact.Photo__c, ProfileID,
							Contact.Agency_View_Permission__c, Contact.Group_View_Permission__c, Contact.Export_Report_Permission__c, Contact.Chatter_Only__c, Contact.Create_Agreement__c, Contact.View_Full_Agreement__c, Contact.View_Basic_Agreement__c, Contact.Allow_Manage_Dept_Users_Tasks__c
							FROM User WHERE Contact.AccountID = :accoID and Contact.RecordType.Name = 'Employee' and api_user__c = false and IsActive = :isActiveUser order by Name];

			List<Profile> lp = [Select id, Name, Description from Profile where UserType = 'CSPLitePortal'];


			for(User u : listStaff){
				activeContacts.add(u.ContactID);
				for(Profile p : lp)
					if(u.ProfileID == p.id){
						accessTypes.put(p.id, p.Description != null ? p.Description : '');
						break;
					}

			}

		}
		return listStaff;
	}

	public Map<String, String> accessTypes {get;set;}

	private List<Contact> listContacts;
	public List<Contact> getListContacts(){
		if(listContacts == null){
			listContacts = [Select id, Name, MobilePhone, Email, Title, Department from Contact where RecordType.Name = 'Employee' and AccountID = :accoID and id not in :activeContacts order by Name];
		}
		return listContacts;

	}

	public PageReference refreshStaffList(){
		//employeeDep = null;
		listStaff = null;
		return null;
	}


	/*public Map<String, String> employeeDep{
		get{
			if(employeeDep == null){
				employeeDep = new Map<String, String>();
				for(account ac:listStaff)
					employeeDep.put(ac.id, 'Not Defined');

				for(Agency_Departments__c ad: [Select Id, Name, ( Select Employee_fk__c from Agency_Department_Members__r where Agency_fk__c != null ) from Agency_Departments__c A where Agency_fk__c = :accoId])
					for(Agency_Department_Member__c md: ad.Agency_Department_Members__r)
						employeeDep.put(md.Employee_fk__c, ad.name);
			}
			return employeeDep;
		}
		Set;
	}*/



}