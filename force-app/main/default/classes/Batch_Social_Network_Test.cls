@isTest
private class Batch_Social_Network_Test
{
	static testMethod void myUnitTest() {

		TestFactory tf = new TestFactory();
		
		Account agency = tf.createAgency();

		Account agencyGroup = tf.createAgencyGroup();

		Contact emp = tf.createEmployee(agency);
		
		Contact lead = tf.createLead(agency, emp);
		Contact client = tf.createClient(agency);

		User portalUser = tf.createPortalUser(emp);

		

		Destination_Tracking__c dt = new Destination_tracking__c();
		dt.Client__c = lead.id;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
		dt.Expected_Travel_Date__c = system.today().addMonths(3);
		dt.lead_Cycle__c = false;
		insert dt;

		

		Test.startTest();
     	system.runAs(portalUser){

			//Batch_Social_Network batch = new Batch_Social_Network();
			//Database.executeBatch(batch);
		}

	}
}