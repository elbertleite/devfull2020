public class help_link {

     public string FileHelp () {
        try{
            string fl = ApexPages.currentPage().getParameters().get('fl');

            document doc = [Select Id, name FROM DOCUMENT WHERE Name = :fl limit 1];
            string fileId = doc.id;
            fileId = fileId.substring(0,15);
            return '/servlet/servlet.FileDownload?file=' + fileId;
        }catch(Exception e){
            return '/help_missing';
        }
    }

    public pageReference openHelp(){
        PageReference pr = new PageReference(FileHelp());
        pr.setRedirect(true);
        return pr;
    }

}