public with sharing class client_course_product_pay {

	public Decimal totalPay {get;set;}
	public String prodCurrency {get;set;}
	public client_product_service__c paidByAgency {get;set;}
	public list<client_product_service__c> product{get; set;}
	public String Agency_Currency {get;set;}
	public decimal Agency_Currency_Rate {get;set;}
	public decimal Agency_Currency_Value {get;set;}

	private financeFunctions fin = new financeFunctions();
	private String clientId;
	private decimal totalpaidAgencyCurrency = 0;
	public Boolean providerPaid {get{if(providerPaid == null) providerPaid = false; return providerPaid;}set;}

	public client_course_product_pay(ApexPages.standardController ctr){

		paidByAgency = new client_product_service__c();

		
		product = [SELECT Related_to_Product__c, Start_Date__c, End_Date__c, Products_Services__r.isProductPaidByAgency__c, Products_Services__r.isAlreadyPaidByAgency__c, Products_Services__r.Provider__r.Provider_Name__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, Category__c, Client__c, Client__r.Name, Client__r.FirstName, Currency__c, Invoice__c, Quantity__c, Price_Total__c, Product_Name__c, Received_by__r.Name, Received_Date__c, Unit_Description__c, Paid_by_agency__c, Client__r.Owner__r.AccountId, Client__r.Owner__r.Contact.Department__c, isSelected__c FROM client_product_service__c WHERE Id = :ctr.getId() OR Related_to_Product__c = :ctr.getId() order by Related_to_Product__c nulls first, Product_Name__c];
		
		clientId = product[0].Client__c;

		retrieveDeposit(clientId);
		retrieveLoan(clientId);
		retrieveMainCurrencies();
		
		for(client_product_service__c p : product){
			p.isSelected__c = false;

			if(p.Products_Services__r.isProductPaidByAgency__c)
				paidByAgency.Paid_by_Agency__c = true;

			if(p.Products_Services__r.isAlreadyPaidByAgency__c)
				providerPaid = true;

			if(p.Related_to_Product__c == NULL)
				prodCurrency = p.Currency__c;

			if(p.Received_Date__c == NULL && p.Invoice__c == NULL)
				p.isSelected__c = true;

			p.Agency_Currency__c = fin.currentUser.Contact.Account.Account_Currency_Iso_Code__c;
		}//end for

		calcTotPay();
		//recalculateRate();

		//TODO: Be sure to always get this values from the Main Product
		System.debug('==> product[0]: '+product[0]);
		// Agency_Currency_Rate = product[0].Agency_Currency_Rate__c;
		// Agency_Currency_Value = product[0].Agency_Currency_Value__c;
		Agency_Currency = product[0].Agency_Currency__c;

		newPayDetails.Value__c = totalPay;
		
	}

	public void calcTotPay(){
		totalPay = 0;

		for(client_product_service__c p : product)
			if(p.isSelected__c)
				totalPay += p.Price_Total__c;

		if(listPayDetails.size()>0){
			listPayDetails = null;
			totalpaid = 0;
			retrieveDeposit(clientId);
		}

		newPayDetails.Value__c = totalPay;
		recalculateRate();

	}

	//===============================================================PRODUCT PAYMENT==================================================================

	public client_course__c clientDeposit{get;set;}

	private decimal currentDepositUsed = 0;

	private void retrieveDeposit(string contactId){
		try{
		 	clientDeposit = [Select Deposit_Description__c, Deposit_Total_Used__c, Deposit_Total_Value__c, Deposit_Total_Available__c, Deposit_Total_Refund__c, Deposit_Type__c, Deposit_Value_Track__c, Id, client__c, Client__r.name	from client_course__c where client__c = :contactId and isDeposit__c = true and Deposit_Total_Available__c > 0 ];
		}catch(Exception e){
			clientDeposit = new client_course__c();
		}
	}

	// ============ PAYMENT PLAN ===============================
	map<string, Payment_Plan__c> mapLoan;
	private void retrieveLoan(string contactId){
		mapLoan = new map<string, Payment_Plan__c>([Select id, Value__c, Value_Remaining__c from Payment_Plan__c where client__c = :contactId and Created_By_Agency__c = :fin.currentUser.Contact.AccountId and Loan_Status__c = 'approved' and Plan_Type__c = 'instalments' and Value_Remaining__c > 0]);
	}

	public Contact receivedOn {get
		{
		if(receivedOn == null){
			receivedOn = new Contact();
		}return receivedOn;}set; }

	private list<selectOption> depositListType;
	public list<selectOption> getdepositListType(){
		if(depositListType == null){// && installment != null){
			depositListType = new list<selectOption>();
			depositListType.add(new SelectOption('','Select Type'));
			if(clientDeposit.Deposit_Total_Used__c == null)
				clientDeposit.Deposit_Total_Used__c = 0;
			if(clientDeposit.Deposit_Total_Value__c != null && (clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c - clientDeposit.Deposit_Total_Refund__c) > 0){
				depositListType.add(new SelectOption('Deposit','Deposit'));
				newPayDetails.Payment_Type__c = 'Deposit';
				newPayDetails.Date_Paid__c = System.today();
				// newPayDetails.Value__c = clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c - clientDeposit.Deposit_Total_Refund__c;
			}

			for(Payment_Plan__c pp:mapLoan.values()){
				depositListType.add(new SelectOption('loan-'+pp.id,'Loan - '+ pp.Value_Remaining__c));
			}

			Schema.DescribeFieldResult fieldResult = client_course__c.Deposit_Type__c.getDescribe();
			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
			for( Schema.PicklistEntry f : ple)
				depositListType.add(new SelectOption(f.getLabel(), f.getValue()));
			depositListType.sort();
		}
		return depositListType;
	}

	public class paymentValue{
		public integer payIndex{get; set;}
		public client_course_instalment_payment__c productPayment{get; set;}
		public string payPlanId{get; set;}
		public paymentValue(){}
		public paymentValue(integer depositIndex, client_course_instalment_payment__c productPayment, string payPlanId){
			this.payIndex = depositIndex;
			this.productPayment = productPayment;
			this.payPlanId = payPlanId;
		}
	}

	public list<paymentValue> listPayDetails{
		get{
			if(listPayDetails == null){
				/*if(installment.client_course_instalment_payments__r.size() > 0){
					for(client_course_instalment_payment__c ip:installment.client_course_instalment_payments__r)
						listPayDetails.add(new paymentValue(countNumberPaymentValues++, ip));
				}else */listPayDetails = new list<paymentValue>();
			}
			return listPayDetails;
		}
		set;
	}

	public decimal totalpaid{
		get{
			if(totalpaid == null)
				totalpaid = 0;
			return totalpaid;
		}
		set;
	}

	public client_course_instalment_payment__c newPayDetails{
		get{
			if(newPayDetails == null && product != null){
				newPayDetails = new client_course_instalment_payment__c(Date_Paid__c = System.today());
				// newPayDetails.client_product_service__c = product.id;
			}
			return newPayDetails;
		}
		set;
	}

	public Boolean invoiceSaved{get{if(invoiceSaved == null) invoiceSaved = true; return invoiceSaved;} Set;}

	private integer countNumberPaymentValues = 0;

	public pageReference addPaymentValue(){

		if(newPayDetails.Value__c <= 0){
			newPayDetails.Value__c.addError('Value need to be bigger than 0');
			return null;
		}

		if(newPayDetails.Payment_Type__c != 'pds' && newPayDetails.Payment_Type__c != 'offShore' && (newPayDetails.Value__c > totalPay || newPayDetails.Value__c > (totalPay - totalpaid))){
			newPayDetails.Value__c.addError('Value cannot be bigger than ' + (totalPay - totalpaid));
			return null;
		}

		if(newPayDetails.Date_Paid__c > system.today()){
			newPayDetails.Date_Paid__c.addError('Date paid cannot be future date. ');
			return null;
		}

		if(newPayDetails.Date_Paid__c < Date.today().addDays(-14)){
			newPayDetails.Date_Paid__c.addError('Date paid cannot be lower than 2 weeks. ');
			return null;
		}

		if(newPayDetails.Payment_Type__c.startsWith('loan-')){
			if(clientLoan.Value_Remaining__c >= newPayDetails.Value__c)
				clientLoan.Value_Remaining__c -= newPayDetails.Value__c;
			else {
				newPayDetails.Value__c.addError('Loan value available is insufficient');
				return null;
			}
		}

		if(newPayDetails.Payment_Type__c == 'Deposit'){
			if((clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c - clientDeposit.Deposit_Total_Refund__c) >= 0 && newPayDetails.Value__c <= (clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c - clientDeposit.Deposit_Total_Refund__c))
				clientDeposit.Deposit_Total_Used__c += newPayDetails.Value__c;
			else {
				newPayDetails.Value__c.addError('Deposit Value Invalid');
				return null;
			}
		}


		newPayDetails.Client__c = clientId;

		if(newPayDetails.Payment_Type__c == 'OffShore'){
			listPayDetails.clear();
			newPayDetails.Agency_Currency_Value__c = newPayDetails.Value__c;
			listPayDetails.add(new paymentValue(countNumberPaymentValues++, newPayDetails, null));
			totalpaid = newPayDetails.Value__c;
			clientDeposit.Deposit_Total_Used__c = currentDepositUsed;

			hasOffShorePayment = true;
			//product.isPaidOffShore__c = true;//TODO:
		}else{

			//Relate deposit ID to the payment
			if(newPayDetails.Payment_Type__c=='deposit'){
				newPayDetails.Paid_From_Deposit__c = clientDeposit.id;
			}


			boolean isSameTypeDate = false;
			for(integer i = 0; i < listPayDetails.size(); i++)
				if(listPayDetails[i].productPayment.Payment_Type__c == newPayDetails.Payment_Type__c && listPayDetails[i].productPayment.Date_Paid__c == newPayDetails.Date_Paid__c){

					listPayDetails[i].productPayment.Value__c += newPayDetails.Value__c;
					totalpaid += newPayDetails.Value__c;
					isSameTypeDate = true;
					
					//============	CONVERT PAYMENT TO AGENCY CURRENCY ===================================
					if(Agency_Currency_Rate < 0)
						newPayDetails.Agency_Currency_Value__c = newPayDetails.Value__c / Agency_Currency_Rate.setScale(6);
					else newPayDetails.Agency_Currency_Value__c = newPayDetails.Value__c * Agency_Currency_Rate.setScale(6);

					// if(totalPay - totalpaid == 0){
					// 	newPayDetails.Agency_Currency_Value__c = Agency_Currency_Value.setScale(2) - totalpaidAgencyCurrency;
					// }

					listPayDetails[i].productPayment.Agency_Currency_Value__c += newPayDetails.Agency_Currency_Value__c.setScale(2);
					totalpaidAgencyCurrency += newPayDetails.Agency_Currency_Value__c.setScale(2);

					break;
				}

			if(!isSameTypeDate){
				string payPlanId;
				if(newPayDetails.Payment_Type__c.startsWith('loan-')){
					payPlanId = newPayDetails.Payment_Type__c.remove('loan-');
					newPayDetails.Payment_Type__c = 'Client Loan';
					newPayDetails.Payment_Plan__c = payPlanId;
				}
				listPayDetails.add(new paymentValue(countNumberPaymentValues++, newPayDetails, payPlanId));
				totalpaid += newPayDetails.Value__c;

				//============	CONVERT PAYMENT TO AGENCY CURRENCY ===================================
				if(Agency_Currency_Rate < 0)
					newPayDetails.Agency_Currency_Value__c = newPayDetails.Value__c / Agency_Currency_Rate.setScale(6);
				else newPayDetails.Agency_Currency_Value__c = newPayDetails.Value__c * Agency_Currency_Rate.setScale(6);

				// if(totalPay - totalpaid == 0){
				// 	newPayDetails.Agency_Currency_Value__c = Agency_Currency_Value.setScale(2) - totalpaidAgencyCurrency;
				// }
				totalpaidAgencyCurrency += newPayDetails.Agency_Currency_Value__c.setScale(2);
			}
		}


		newPayDetails = new client_course_instalment_payment__c();
		newPayDetails.Date_Paid__c = System.today();
		newPayDetails.Payment_Type__c = '';
		newPayDetails.Value__c = totalPay - totalpaid;
		//newPayDetails.client_product_service__c = product.id;

		invoiceSaved = false;

		return null;
	}

	public pageReference removePaymentValue(){
		integer index = integer.valueOf(ApexPages.currentPage().getParameters().get('index'));

		for(integer i = 0; i < listPayDetails.size(); i++){
			if(index == listPayDetails[i].payIndex){

				totalpaid -= listPayDetails[i].productPayment.Value__c;
				totalpaidAgencyCurrency -= listPayDetails[i].productPayment.Agency_Currency_Value__c;

				if(listPayDetails[i].productPayment.Payment_Type__c == 'Deposit')
					clientDeposit.Deposit_Total_Used__c -= listPayDetails[i].productPayment.Value__c;

				if(listPayDetails[i].productPayment.Payment_Type__c == 'offShore'){
					hasOffShorePayment = false;
					//product.isPaidOffShore__c = false;
				}

				//PAYMENT PLAN
				if(listPayDetails[i].payPlanId != null && mapLoan.containsKey(listPayDetails[i].payPlanId))
					mapLoan.get(listPayDetails[i].payPlanId).Value_Remaining__c += listPayDetails[i].productPayment.Value__c;

				listPayDetails.remove(i);
				break;
			}
		}
		invoiceSaved = false;

		newPayDetails.Value__c = totalPay - totalpaid;

		return null;
	}

	private Boolean isGeneralUser {get;set;}
	public Boolean isSaved {get;set;}
	public pageReference savePayment(){
		isSaved = false;
		errorMsg = '';

		//Check selected products
		boolean hasSelected = false;
		list<Id> ids = new list<Id>();
		Id mainProduct;
		for(client_product_service__c p : product){
			if(p.isSelected__c){
				hasSelected = true;
				ids.add(p.Id);
			}
		}

		if(!hasSelected){
			errorMsg = 'You must select at least one product for payment.';
		}
		else
			mainProduct = ids[0];


		/** Double Check if Product is paid **/
		boolean hasPaid = false;
		for(client_product_service__c p : [Select Received_Date__c, Received_by__r.Name from client_product_service__c where Id in :ids])
			if(p.Received_Date__c!=null){
				hasPaid = true;
				break;
			}

		if(hasPaid){
			errorMsg = 'Some of the selected products has been already paid. Please refresh your page and try again.';
		}

		if(hasOffShorePayment){

	        if(selectedUser == null || selectedUser =='noUser'){
	            errorMsg += '<span style="display:block">Please fill the field <b>Payment Received By</b>.</span>';
	        }

	        if(receivedOn.Expected_Travel_Date__c == null){
	            errorMsg += '<span style="display:block">Please fill the field <b>Payment Received On</b>.</span>';
	        }

	        if(receivedOn.Expected_Travel_Date__c > system.today()){
	        	errorMsg += '<span style="display:block"><b>Payment Received On</b> cannot be future date.</span>';
			}
		}

		system.debug('errorMsg==' + errorMsg);

		if(errorMsg.length()>0){
			return null;
		}

		else{
			Savepoint sp = Database.setSavepoint();
			try{

				Decimal coveredByAgency = 0;

				if(clientDeposit != null && clientDeposit.id != null && currentDepositUsed != null && currentDepositUsed != clientDeposit.Deposit_Total_Value__c){
					if(clientDeposit.Deposit_Total_Refund__c==null) clientDeposit.Deposit_Total_Refund__c = 0;

					clientDeposit.Deposit_Total_Available__c = clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c - clientDeposit.Deposit_Total_Refund__c;
					update clientDeposit;
				}

				list<client_course_instalment_payment__c> paymentList = new list<client_course_instalment_payment__c>();
				//for(paymentValue lpd:listPayDetails)
				//	paymentList.add(lpd.productPayment);

				String receivedByAgency;
				String receivedByDepartment;

				isGeneralUser = FinanceFunctions.isGeneralUser(userAgency.General_User_Agencies__c, product[0].Client__r.Owner__r.AccountId);
								
				if(!isGeneralUser){
					receivedByAgency = userAgency.Contact.AccountId;
					receivedByDepartment = userAgency.Contact.Department__c;
				}
				else{
					receivedByAgency = product[0].Client__r.Owner__r.AccountId;
					receivedByDepartment = product[0].Client__r.Owner__r.Contact.Department__c;
				}

				boolean isReconciled = true; //Used to automatically reconcile Third Party Payments 

				for(paymentValue lpd:listPayDetails){
					lpd.productPayment.client_product_service__c = mainProduct; //Relate payments to the main product selected
					lpd.productPayment.Received_By__c = UserInfo.getUserId();
					lpd.productPayment.Received_On__c = DateTime.now();
					lpd.productPayment.Received_By_Agency__c = receivedByAgency;

					if(lpd.productPayment.Payment_Type__c.toLowerCase() != 'payment 3rd party')
						isReconciled = false;
					else{
						lpd.productPayment.Confirmed_By__c = UserInfo.getUserId();
						lpd.productPayment.Confirmed_Date__c = DateTime.now();
						lpd.productPayment.Confirmed_By_Agency__c = receivedByAgency;
					}

					if(lpd.productPayment.Payment_Type__c.toLowerCase()=='covered by agency')
						coveredByAgency += lpd.productPayment.Value__c;
					paymentList.add(lpd.productPayment);

				}//end for


				insert paymentList;

				//Update products
				list<client_product_service__c> toUpdate = new list<client_product_service__c>();
				for(client_product_service__c p :product)
					if(p.isSelected__c){

						p.Currency__c = prodCurrency;
						if(isReconciled){
							p.Confirmed_By__c = Userinfo.getuserId();
							p.Confirmed_Date__c = System.now();
						}

						if(!hasOffShorePayment){
							p.Received_By_Agency__c = receivedByAgency;
							p.Received_by_Department__c = receivedByDepartment;
							p.Received_by__c = Userinfo.getUserId();
							p.Received_Date__c = System.now();
						}else{
							p.Received_By_Agency__c = selectedAgency;
							p.Received_by__c = selectedUser;
							p.Received_by_Department__c = userDept.get(selectedUser);

							p.Received_Date__c = DateTime.newInstance(receivedOn.Expected_Travel_Date__c.year(), receivedOn.Expected_Travel_Date__c.month(), receivedOn.Expected_Travel_Date__c.day(), 0, 0, 0);
						}

						
						//Related Products
						fin.convertCurrency(p, p.Currency__c, p.Price_Total__c, null, 'Agency_Currency__c', 'Agency_Currency_Rate__c', 'Agency_Currency_Value__c');
						if(p.Id != mainProduct){
								p.Paid_with_product__c = mainProduct;
						}
						//Main Product
						else{
							p.Covered_By_Agency__c = coveredByAgency;
							system.debug('paidByAgency.Paid_by_Agency__c ===>' + paidByAgency.Paid_by_Agency__c);
							p.Paid_by_Agency__c = paidByAgency.Paid_by_Agency__c;

							if(p.Paid_by_Agency__c)
								p.Creditcard_debit_date__c = system.today();
						}

						if(paidByAgency.Paid_by_Agency__c && providerPaid){
							p.Paid_to_Provider_by__c = UserInfo.getUserId();
							p.Paid_to_Provider_by_Agency__c = receivedByAgency;
							p.Paid_to_Provider_on__c = system.now();
						}

						p.isSelected__c = false;
						toUpdate.add(p);
					}

				// UPDATE PAYMENT PAN - CLIENT LOAN
				if(mapLoan.size() > 0){
					list<Payment_Plan__c> listPlanUpdate = new list<Payment_Plan__c>();
					map<string, Payment_Plan__c> mapLoanCompare = new map<string, Payment_Plan__c>([Select id, Value_Remaining__c from Payment_Plan__c where id in :mapLoan.keySet()]);
					for(string mk:mapLoan.keySet()){
						if(mapLoan.get(mk).Value_Remaining__c != mapLoanCompare.get(mk).Value_Remaining__c)
							listPlanUpdate.add(mapLoan.get(mk));
					}
					if(listPlanUpdate.size() > 0)
						update listPlanUpdate;
				}	

				update toUpdate;
				showError = true;
				isSaved = true;

			}catch(Exception e){
				Database.rollback(sp);
			}

			return null;
		}
	}

	public pageReference cancelPayment(){

		return null;
	}


	public boolean hasOffShorePayment {get{if(hasOffShorePayment==null) hasOffShorePayment = false; return hasOffShorePayment;}set;}
	public String errorMsg {get{if(errorMsg == null) errorMsg = ''; return errorMsg;}set;}
	public boolean showError {get{if(showError==null) showError = false; return showError;}set;}
	private User userAgency = [Select General_User_Agencies__c, Contact.AccountId, Contact.Account.ParentId, Contact.Department__c, Contact.Account.account_currency_iso_code__c from user where id = :UserInfo.getUserId() limit 1];


	public String selectedAgencyGroup {get{if(selectedAgencyGroup==null) selectedAgencyGroup = userAgency.Contact.Account.ParentId; return selectedAgencyGroup;}set;}
	    public List<SelectOption> agencyGroupOptions {
			get{
				if(agencyGroupOptions == null){
					agencyGroupOptions = agencyNoSharing.getAllGroups();
				}
				return agencyGroupOptions;
			}
			set;
		}

		public void changeAgencyGroup(){
			selectedAgency = 'noAgency';
			agencyOptions = null;
			selectedUser = 'noUser';
			userOptions = null;
			system.debug('agencyOptions==' + agencyOptions);

		}

		public void changeAgency(){
			selectedUser = 'noUser';
			userOptions = null;
		}

		private IPFunctions.agencyNoSharing agencyNoSharing {get{if(agencyNoSharing == null)agencyNoSharing = new IPFunctions.agencyNoSharing();return agencyNoSharing;}set;}
		public String selectedAgency {get{if(selectedAgency==null) selectedAgency = 'noAgency'; return selectedAgency;}set;}
		public List<SelectOption> agencyOptions {
			get{
				if(agencyOptions == null){
					 agencyOptions = agencyNoSharing.getAgenciesByParentNoSharing(selectedAgencyGroup);
				}
				return agencyOptions;
			}
			set;
		}

		public String selectedUser {get{if(selectedUser==null) selectedUser = 'noUser'; return selectedUser;}set;}
		private map<string,string> userDept {get;set;}
		public List<SelectOption> userOptions {
			get{
				if(userOptions == null){
					userDept = new map<string,string>();
					userOptions = new List<SelectOption>();
					userOptions.add(new SelectOption('noUser', '-- Select User --'));
					if(selectedAgency!=null && selectedAgency!='')
						for(User ac : [SELECT id, Name, Contact.Department__c FROM User WHERE Contact.AccountId = :selectedAgency and IsActive = true and Contact.Chatter_Only__c = false order by name]){
							userOptions.add(new SelectOption(ac.Id, ac.Name));
							userDept.put(ac.Id, ac.Contact.Department__c);
						}
				}
				return userOptions;
			}
			set;
		}

		//================CURRENCY=============================
		public List<SelectOption> MainCurrencies{get; set;}
		private Map<String, double> agencyCurrencies;
		public Datetime currencyLastModifiedDate {get;set;}
		public void retrieveMainCurrencies(){

			MainCurrencies = fin.retrieveMainCurrencies();
			currencyLastModifiedDate = fin.currencyLastModifiedDate;
		}

		public void recalculateRate(){
			// for(client_product_service__c p : product)
			// 	if(p.isSelected__c)
			// 		fin.convertCurrency(p, p.Currency__c, totalPay, null, 'Agency_Currency__c', 'Agency_Currency_Rate__c', 'Agency_Currency_Value__c');
			fin.convertCurrency(product[0], product[0].Currency__c, totalPay, null, 'Agency_Currency__c', 'Agency_Currency_Rate__c', 'Agency_Currency_Value__c');
			Agency_Currency_Rate = product[0].Agency_Currency_Rate__c;
			Agency_Currency_Value = product[0].Agency_Currency_Value__c;
		}

	// =================== PAYMENT PLAN =====================================	
	public Payment_Plan__c clientLoan{get; set;}
	public void openLoan(){
		string loanId = ApexPages.currentPage().getParameters().get('loanId');
		System.debug('==>loanId: '+loanId);
		if(mapLoan.containsKey(loanId)){
			clientLoan = mapLoan.get(loanId);
			// if(clientLoan.Value_Remaining__c >= product.Price_Total__c - totalpaid){
			// 	newPayDetails.Value__c = product.Price_Total__c - totalpaid;
			// }else if(clientLoan.Value_Remaining__c > 0){
			// 	newPayDetails.Value__c = clientLoan.Value_Remaining__c;
			// } else newPayDetails.Value__c = 0;
		}

	}
}