public with sharing class xCourseSearchStyle {
	
	public xCourseSearchStyle(ApexPages.StandardController controller) {}
	public xCourseSearchStyle() {}
	
	public boolean showVisits {
		get{
			if(showVisits == null)
				showVisits = false;
			return showVisits;
		}
		set;
	}
	
	public void renderVisits(){
		showVisits = true;
	}
		
	public string groupID{
		get{
			if(groupID == null){
				try {
					groupID = [select Contact.Account.ParentId from User where id =:UserInfo.getUserId() limit 1].Contact.Account.ParentId;
				} catch (Exception e){
					groupID = '';
				}
			}
			return groupID;
		}
		set;
	}
	
	public string communityPrimaryColor{
		get{
			if(communityPrimaryColor == null){
				try {
					communityPrimaryColor = [Select AssetCategory, TextAsset from CustomBrandAsset WHERE CustomBrand.ParentId = :Network.getNetworkId() AND AssetCategory = 'MotifPrimaryColor' limit 1].TextAsset;
				} catch (Exception e){
					communityPrimaryColor = '';
				}
			}
			return communityPrimaryColor;
		}
		set;
	}
	
	public string groupBgColor{
		get{
			
			UserDetails.GroupSettings gs = new UserDetails.GroupSettings();
			groupBgColor = gs.getBackgroundColour(groupID);
			
			return groupBgColor;
		}
		set;
	}
	
	public string groupLabelColor{
		get{
			UserDetails.GroupSettings gs = new UserDetails.GroupSettings();
			groupLabelColor = gs.getLabelColour(groupID);
			
			return groupLabelColor;
		}
		set;
	}
	
	public void refreshColors(){
		groupBgColor = null;
		groupLabelColor = null;
	}
	
}