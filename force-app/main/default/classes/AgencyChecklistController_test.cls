/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AgencyChecklistController_test {
	
	static testMethod void myUnitTest() {
        
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();
        Contact employee = tf.createEmployee(agency);
        User portalUser = tf.createPortalUser(employee);
        
        Account agencyGroup = [select id, parentid from account where id = :agency.parentid];
        tf.createChecklists(agencyGroup.id);
        
        Test.startTest();
        
       // system.runAs(portalUser){
        	
        	AgencyChecklistController acc = new AgencyChecklistController(new ApexPages.StandardController(agencyGroup));        	
        	Agency_Checklist__c acheck = acc.agency;
        	acc.agency.Destination__c = 'Australia';
        	
        	
        	
        	        	
        	list<AgencyChecklistController.checkListperStage> checkList = acc.checkList;
        	
        	String theid;
        	String str = '';
        	for(Agency_Checklist__c ac : [Select id from Agency_Checklist__c where agency_group__c = :agencygroup.id]){
        		str += ac.id + ',';
        		theid = ac.id;
        	}
        	
        	ApexPages.currentPage().getParameters().put('listtoOrder',str);
        	acc.saveReorderedList();
        	
        	
        	acc.refreshChecklist();
        	acc.refreshStageList();
        	
        	acc.agency.Checklist_Stage__c =  null;
        	acc.addItemCheckList();
        	
        	acc.agency.Checklist_Stage__c = 'Stage 1';
        	acc.agency.Checklist_Item__c =  null;
        	acc.addItemCheckList();
        	
        	acc.agency.Checklist_Item__c =  'My First Item';
        	acc.addItemCheckList();
        	
        	
        	acc.addChecklistToAgency();
        	
        	acc.deleteCheckListToAgency();
        	
        	ApexPages.currentPage().getParameters().put('itemToDelte',theid);
        	acc.deleteItem();
        	acc.getDestinations();
        	
        	
        	
        	
        	/**     
        	* CLIENT STAGE
        	*/       	
        	
        	acc.cStage.Destination__c = 'Australia';
        	
        	list<AgencyChecklistController.clientStage> listStages = acc.listStages;
        	str = '';
        	for(Client_Stage__c ac : [Select id from Client_Stage__c where agency_group__c = :agencygroup.id]){
        		str += ac.id + ',';
        		theid = ac.id;
        	}
        	
        	ApexPages.currentPage().getParameters().put('listtoOrder',str);
        	acc.saveReorderedClientStageList();
        	
        	
        	acc.cStage.Stage__c = null;
        	acc.addItemClientStage();
        	
        	acc.cStage.Stage__c = 'Stage 1';
        	acc.cStage.Stage_Description__c = null;
        	acc.addItemClientStage();
        	
        	acc.cStage.Stage_Description__c = 'Follow up';
        	acc.addItemClientStage();
        	
        	ApexPages.currentPage().getParameters().put('itemToDelte', theid);
        	acc.deleteStageItem();
        	
        	
        	
        	
        	/*
        	* NOTES AND SUBJECTS
        	*/
        	
        	list<String> listSubjects = acc.listSubjects;
        	acc.subject = 'new subject';
        	acc.addSubject();
        	
        	ApexPages.currentPage().getParameters().put('subjectToDelte', 'new subject');
        	acc.deleteSubject();
        	
        	
        	/*
        	* LEAD SOURCE
        	*/
        	
        	List<SelectOption> listleadSource = acc.listleadsource;
        	list<string> ListSourceItems = acc.listsourceitems;
        	list<Lead_Status_Source__c> getlistLeadsSource = acc.getlistLeadsSource();
        	
        	for(Lead_Status_Source__c lss : getlistLeadsSource)
        		theid = lss.id;
        	
        	ApexPages.currentPage().getParameters().put('itemToDelte', theid);
        	acc.deleteSpecificSource();
        	
        	acc.addLeadSource();
        	
        	for(Lead_Status_Source__c lss : getlistLeadsSource)
        		theid = lss.id;
        		
        	ApexPages.currentPage().getParameters().put('leadStatusToDelete', theid);
        	acc.deleteLeadSource();
        	
        	acc.refreshlistLeadsSource();
        	
       // }
        Test.stopTest();
        
    }

}