/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ShowCaseDestination_test {

    static testMethod void myUnitTest() {
                
        TestFactory tf = new TestFactory();
        
        Account school = tf.createSchool();
       	Account agency = tf.createAgency();
       	Account campus = tf.createCampus(school, agency); 
        
       
        
        Test.startTest();
        
        ApexPages.currentPage().getParameters().put('ct','Ireland');
        ShowCaseDestination scd = new ShowCaseDestination();
        scd.getCountries();
        scd.getCities();
        scd.getCurrencies();
        
        scd.showcase.name = 'Ireland';
        scd.showcase.account_currency_iso_code__c = 'EUR';
        scd.showcase.showcase_population__c = '1231341234';
        scd.showcase.showcase_total_area__c = '123415678';
        scd.showcase.showcase_tel_area_code__c = '33';
        scd.showcase.showcase_timezone__c = '(GMT+00:00) Western European Time (Europe/Lisbon)';
        scd.showcase.website = 'www.ireland.com';
        scd.showcase.unemployment__c = 10;
        scd.showcase.minimum_wage_per_hour__c = 15;
        scd.showcase.general_Description__c = 'the general description.';
        scd.showcase.videos__c = 'qweqeqeqwe>YouTube;q89 tr0q9e7 t>YouTube';
		scd.showcase.showcase_cost_of_living__c = 'Accommodation>500.0;Extras>200.0;Entertainment>50.0;Food>100.0;Health Cover>150.0;Services(gas, electricity, phone, internet)>60.0;Transport>40.0;1234>100.0';
		scd.showcase.showcase_cost_of_living_unit__c = 'per month';
        scd.saveAccount();
        
        ApexPages.currentPage().getParameters().put('id', campus.id);
        scd = new ShowCaseDestination();
        scd.getCountries();
        scd.getCities();
        scd.getCurrencies();
        scd.getCOLItems();
        scd.getListCOLItems();
        scd.colname = 'Something';
        scd.colPrice = 500;
        scd.addCostOfLiving();
        scd.colkey = 'Something';
        scd.delCostOfLiving();
        
        
        Account country = tf.createCountryShowcase();
        Account city = tf.createCityShowcase(country);
        
        ApexPages.currentPage().getParameters().put('id', city.id);
        scd = new ShowCaseDestination();
        scd.getCountries();
        scd.getCities();
        scd.editAccount();
        scd.cancelEdit();
        List<SelectOption> opts = scd.videoPL;
        scd.videoHostName = 'YouTube';
        scd.videoKey = '1234ouerht731';
        scd.addVideo();
        scd.videoListKey = '1234ouerht731';
        scd.delVideo();
        
        boolean isInternalUser = scd.isInternalUser;
        
        Test.stopTest();
        
        
    }
}