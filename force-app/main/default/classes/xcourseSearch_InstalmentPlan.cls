public with sharing class xcourseSearch_InstalmentPlan{
	
	public Account agency {get;set;}	
	string accoId;
	
	public xcourseSearch_InstalmentPlan(ApexPages.StandardController controller){
		accoId = controller.getId();
		this.agency = getDetails(accoId);
	}

	private Account getDetails(String id){
		return [Select Name, CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name, Nickname__c, Agency_Type__c, Group_Type__c, Phone, Skype__c, Optional_Currency__c, account_currency_iso_code__c, 
						BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,  ParentId, Parent.Name, 
						Registration_name__c, Year_established__c, BusinessEmail__c, Website,
						( Select Account_Nickname__c, Account_Name__c, Account_Number__c, Bank__c, Branch_Address__c, Branch_Name__c, Branch_Phone__c, BSB__c, IBAN__c, Notes__c, Swift_Code__c from Bank_Details1__r)
				from Account where id = :id];
				
	}
	
	public String interestTypeMSG {
		get {
			String openSpan = '<label style="font-weight: bold;text-decoration: underline;" >';
			String closeSpan = '</label>';
			String msg = openSpan + 'Simple:' + closeSpan + '<br/>';
			msg += 'When the same interest is applied to all instalments.<br/><br/>';
			
			msg += openSpan + 'Compound:' + closeSpan + '<br/>';
			msg += 'When interest is added to the principal of a deposit or loan, so that, from that moment on, the interest that has been added also earns interest. <br/>';
			msg += 'Take the remaining value and add interest multiplied by the number of instalments you wish to have <br/>';
			msg += '(e.g. if interest is 1% and you want to have 4 instalments, add 4% on the remaining value).<br/>';
			msg += 'Then divide this amount by the number of instalments (e.g. 4).<br/>';
			msg += 'Example: Remaining value is $1000, 4 instalments plan = (1000 + 4%)/4instalments = price for each instalment.<br/><br/>';
			
			msg += openSpan + 'Tabela Price:' + closeSpan + '<br/>';
			msg += 'TBC';
			
			return msg;
		}
	}
	
	public list<instalmentPlanDetails> listInstalmentPlan{
		get{
			if(listInstalmentPlan == null){
				listInstalmentPlan = new list<instalmentPlanDetails>(); 	
				instalmentPlanDetails instalmentPlan;
				for(Instalment_Plan__c ip: [Select Agency__c, Deposit__c, Description__c, Interest__c, Name__c, Number_of_Instalments__c, Id, Group_Instalments__c, Instalments_Interest__c, Interest_Type__c, Surcharge_Value__c from Instalment_Plan__c where Agency__c = :accoId order by Number_of_Instalments__c]){
					instalmentPlan = new instalmentPlanDetails();
					instalmentPlan.planDetail = ip;
					if(!ip.Group_Instalments__c && ip.Instalments_Interest__c != null){
						for(String s:ip.Instalments_Interest__c.split(';')){
							instalmentPlan.listGroupInstalment.add(new groupInstalment(integer.valueOf(s.split(':')[0]), Decimal.valueOf(s.split(':')[1])));
						}
						
					}
					listInstalmentPlan.add(instalmentPlan);	
				}
					
			} 
			return listInstalmentPlan;
		} 
		Set;
	}
	
	public class groupInstalment{
		public Integer instalmentNumber{get; Set;}
		public decimal instalmentInterest{get; Set;}
		public groupInstalment(Integer numberInstal, Decimal interest){
			instalmentNumber = numberInstal;
			instalmentInterest = interest;
		}
	}
	
	public class instalmentPlanDetails{
		public Instalment_Plan__c planDetail{get{if(planDetail == null){planDetail = new Instalment_Plan__c();} return planDetail;} Set;}
		public list<groupInstalment> listGroupInstalment{get{if(listGroupInstalment == null) listGroupInstalment = new list<groupInstalment>(); return listGroupInstalment;} Set;}
	}
	
	public instalmentPlanDetails newInstalmentPlan{get{if(newInstalmentPlan == null){ newInstalmentPlan = new instalmentPlanDetails(); newInstalmentPlan.planDetail.agency__c = accoId; newInstalmentPlan.planDetail.Number_of_Instalments__c = 0;} return newInstalmentPlan;} Set;}

	public pageReference addListInterestNewPlan(){
		if(!newInstalmentPlan.planDetail.Group_Instalments__c){
			for(Integer i = 1; i <= newInstalmentPlan.planDetail.Number_of_Instalments__c; i++){
				newInstalmentPlan.listGroupInstalment.add(new groupInstalment(i, 0));
			}
		}
		return null;
	}
	
	public PageReference addInstalmentPlan(){
		/*Boolean showError = false;
		if(newInstalmentPlan.planDetail.Name__c == null || newInstalmentPlan.planDetail.Name__c == ''){
			newInstalmentPlan.planDetail.Name__c.addError('Required field.');
			showError = true;
		}
		if(newInstalmentPlan.planDetail.Number_of_Instalments__c != 0 && newInstalmentPlan.planDetail.Deposit__c == null){
			newInstalmentPlan.planDetail.Deposit__c.addError('Required field.');
			showError = true;
		}
		if(newInstalmentPlan.planDetail.Number_of_Instalments__c != 0 && newInstalmentPlan.planDetail.Interest__c == null){
			newInstalmentPlan.planDetail.Interest__c.addError('Required field.');
			showError = true;
		}
		if(newInstalmentPlan.planDetail.Number_of_Instalments__c != 0 && newInstalmentPlan.planDetail.Interest_Type__c == null){
			newInstalmentPlan.planDetail.Interest_Type__c.addError('Required field.');
			showError = true;
		}*/
		if(validateFields(newInstalmentPlan))
			return null;
			
		newInstalmentPlan.planDetail.agency__c = accoId;
		newInstalmentPlan.planDetail.Instalments_Interest__c = '';
		if(!newInstalmentPlan.planDetail.Group_Instalments__c){
			list<string> interestDetails = new list<string>();
			for(groupInstalment gi:newInstalmentPlan.listGroupInstalment)
				interestDetails.add(gi.instalmentNumber + ':' + gi.instalmentInterest);
				
			newInstalmentPlan.planDetail.Instalments_Interest__c = string.join(interestDetails, ';');
		}
		listInstalmentPlan.add(newInstalmentPlan);
		insert newInstalmentPlan.planDetail;
		newInstalmentPlan = new instalmentPlanDetails();
		newInstalmentPlan.planDetail.Number_of_Instalments__c = 0;
		return null;
	}
	
	public List<SelectOption> getUnitsRange(){    	
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('0','0'));
		options.addAll(xCourseSearchFunctions.getUnitsRange());
        return options;
    }
	
	public boolean showError{get{if(showError == null) showError = false; return showError;} set;}
	private boolean validateFields(instalmentPlanDetails validatePlanDetail){
		showError = false;
		if(validatePlanDetail.planDetail.Name__c == null || validatePlanDetail.planDetail.Name__c == ''){
			validatePlanDetail.planDetail.Name__c.addError('Required field.');
			showError = true;
		}
		if(validatePlanDetail.planDetail.Number_of_Instalments__c != 0 && validatePlanDetail.planDetail.Deposit__c == null){
			validatePlanDetail.planDetail.Deposit__c.addError('Required field.');
			showError = true;
		}
		if(validatePlanDetail.planDetail.Number_of_Instalments__c != 0 && validatePlanDetail.planDetail.Group_Instalments__c && validatePlanDetail.planDetail.Interest__c == null){
			validatePlanDetail.planDetail.Interest__c.addError('Required field.');
			showError = true;
		}
		if(validatePlanDetail.planDetail.Number_of_Instalments__c != 0 && (validatePlanDetail.planDetail.Interest_Type__c == null || validatePlanDetail.planDetail.Interest_Type__c == '')){
			validatePlanDetail.planDetail.Interest_Type__c.addError('Required field.');
			showError = true;
		}
		return showError;
	}
	
	public pagereference savePlan(){   
		list<Instalment_Plan__c> planDetailUpdate = new list<Instalment_Plan__c>();
		map<integer, decimal> instalmentsInterest;
		decimal valueInterest;
		for(instalmentPlanDetails ip:listInstalmentPlan){
			if(!ip.planDetail.Group_Instalments__c){
				if(ip.planDetail.Number_of_Instalments__c > 0){
					if(validateFields(ip))
						return null;
					
					ip.planDetail.Instalments_Interest__c = '';
					instalmentsInterest = new map<integer, decimal>();
					for(groupInstalment gi:ip.listGroupInstalment)
						instalmentsInterest.put(gi.instalmentNumber,gi.instalmentInterest);
					 
					
					if(!ip.planDetail.Group_Instalments__c){
						
						for(Integer i = 1; i <= ip.planDetail.Number_of_Instalments__c; i++){
							if(instalmentsInterest.containsKey(i))
								valueInterest = instalmentsInterest.get(i);
							else valueInterest = ip.planDetail.Interest__c;
							ip.listGroupInstalment.add(new groupInstalment(i, valueInterest));
							ip.planDetail.Instalments_Interest__c += i + ':' + valueInterest;
							if(i < ip.planDetail.Number_of_Instalments__c)
								ip.planDetail.Instalments_Interest__c += ';';
						}
					}
						
				}else{
					ip.planDetail.Deposit__c = 0;
					ip.planDetail.Interest__c = 0;
					ip.planDetail.Interest_Type__c = '';
					ip.planDetail.Group_Instalments__c = false;
					ip.planDetail.Instalments_Interest__c = '';
				}
				
			}else{
				if(validateFields(ip))
					return null;
			}
			planDetailUpdate.add(ip.planDetail);
		}
		
		update planDetailUpdate;
		listInstalmentPlan = null;
        return null;
    }
	
	public PageReference deletePlan(){
		String planId = ApexPages.currentPage().getparameters().get('planId');
		delete [Select Id from Instalment_Plan__c where Id = :planId];
		listInstalmentPlan = null;
		return null;
	}
	
}