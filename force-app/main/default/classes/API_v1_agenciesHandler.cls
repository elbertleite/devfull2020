global class API_v1_agenciesHandler extends API_Manager {
	
	
	
	global without sharing class NoSharing {
		
		public List<Agency> getAllAgencies(String globalLink){
			
			List<Agency> agencies = new List<Agency>();
			
			
			for(Account agency : [Select id, website_agency_name__c, BillingCountry, BillingCity, BillingState, BillingPostalCode, BillingStreet, opening_hours__c, phone, website, BusinessEmail__c,
										Location__Latitude__s, Location__Longitude__s
									from Account where RecordType.Name = 'Agency' and Global_Link__c = :globalLink and Show_On_Website__c = true
									order by BillingCountry, BillingState, BillingCity, website_agency_name__c])
				agencies.add(createAgencyWrapper(agency));
				
			return agencies;
			
		}
		
		
		private Agency createAgencyWrapper(Account agency){
			Agency ag = new Agency();
			ag.id = agency.id;
			ag.name = agency.website_agency_name__c;
			ag.country = agency.billingCountry;
			ag.state = agency.billingstate;
			ag.city = agency.billingCity;
			ag.street = agency.billingStreet;
			ag.postalCode = agency.billingPostalCode;
			ag.phone = agency.phone;
			ag.website = agency.website;
			ag.email = agency.BusinessEmail__c;
			ag.openingHours = agency.opening_hours__c;
			ag.latitude = agency.Location__Latitude__s;
			ag.longitude = agency.Location__Longitude__s;
			return ag;
		}
		
	}
    
}