/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class commissions_test {

    static testMethod void myUnitTest() {
        
        TestFactory tf = new TestFactory();
        
		Account school = tf.createSchool();
		Account agency = tf.createAgency();
		
		Account campus = tf.createCampus(school, agency);
		
		Account campus2 = campus.clone();
		campus2.id = null;
		insert campus2;
		
		
		Course__c course = tf.createCourse();
		Course__c course2 = course.clone();
		course2.id = null;
		course2.Course_Type__c = 'Test Vet';
		insert course2;
		
		Campus_Course__c campusCourse = tf.createCampusCourse(campus,course);
		
		Campus_Course__c campusCourse2 = campusCourse.clone();
		campusCourse2.id = null;
		campusCourse2.Campus__c = campus2.id;
		insert campusCourse2;
		
		Campus_Course__c campusCourse3 = campusCourse2.clone();
		campusCourse3.id = null;
		insert campusCourse3;
		
		Campus_Course__c campusCourse4 = campusCourse.clone();
		campusCourse4.id = null;
		campusCourse4.Course__c = course2.id;
		insert campusCourse4;
		
		
		Commission__c commission = tf.createCommission(school);
		
		Commission__c commission2 = commission.clone();
		commission2.id = null;
		commission2.School_Id__c = null;
		commission2.Campus_Id__c = campus.id;
		insert commission2;
		
		Commission__c commission3 = commission.clone();
		commission3.id = null;
		commission3.School_Id__c = null;
		commission3.Campus_Id__c = null;
		commission3.CampusCourseTypeId__c = 'test';
		insert commission3;
		
		Commission__c commission4 = commission.clone();
		commission4.id = null;
		commission4.School_Id__c = null;
		commission4.Campus_Id__c = null;
		commission4.CampusCourseTypeId__c = null;
		commission4.Rules__c = '12335!@!123213213!@!1233132!@!123123!@!123213213!@!12321';
		commission4.Campus_Course__c = campusCourse.id;
		insert commission4;
		
		commissions testClass = new commissions();
		
		system.debug('campusCourse=====>'   + ' Parent===>' + campusCourse.Campus__r.parentId + ' Campus===>' + campusCourse.Campus__c + ' Course_Type__c===>' + campusCourse.Course__r.Course_Type__c);
		system.debug('campusCourse2=====>' + ' Parent===>' + campusCourse2.Campus__r.parentId + ' Campus===>' + campusCourse2.Campus__c + ' Course_Type__c===>' + campusCourse2.Course__r.Course_Type__c);
		system.debug('campusCourse3=====>'   + ' Parent===>' + campusCourse3.Campus__r.parentId + ' Campus===>' + campusCourse3.Campus__c + ' Course_Type__c===>' + campusCourse3.Course__r.Course_Type__c);
		system.debug('campusCourse4=====>'  + ' Parent===>' + campusCourse4.Campus__r.parentId + ' Campus===>' + campusCourse4.Campus__c + ' Course_Type__c===>' + campusCourse4.Course__r.Course_Type__c);
		
		testClass.selectedSchool = school.id;
		testClass.getCoursesCommission();
		
		
		testClass.refreshCommissions();
		
		testClass.getlistCountries();
		testClass.retrievelistSchools();
		testClass.getItemsControl();
		testClass.getItemsAction();
		testClass.getItemCommissionType();
		testClass.getmapAction();
		testClass.getmapControl();
		testClass.getCoursesCommission();
		testClass.saveCommission();
		
    }
}