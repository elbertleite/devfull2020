public class xcourseSearch_Instalments{

	string accoId;
	private list<Search_Courses__c> listCoursesCart;
	private set<String> setCampus = new set<String>();
	public xcourseSearch_Instalments(ApexPages.StandardController controller){
		accoId = controller.getId();
		
	}
	
	public class instalmentDetails{
		public Integer intalmentNumber{get; Set;}
		public string intalmentDate{get; Set;}
		public decimal intalmentValue{get; Set;}
		public decimal extraFeeValue{get; Set;}
		public date instalmentDateValue {get; Set;}
	}
	
	public class courseIntalments{
		
		public decimal courseTotalTuition {get{if(courseTotalTuition == null) courseTotalTuition = 0; return courseTotalTuition;} Set;}
		public decimal courseTotalExtraFees {get{if(courseTotalExtraFees == null) courseTotalExtraFees = 0; return courseTotalExtraFees;} Set;}
		
		public Search_Courses__c courseDetail{get; Set;} 
		public list<instalmentDetails> intalments{get{if(intalments == null) intalments = new list<instalmentDetails>(); return intalments;} set;}
		public String intalmentDetail{get; Set;}
		public decimal totalInstalment{get; Set;}
		public decimal totalExtraFees{get{if(totalExtraFees == null) totalExtraFees = 0; return totalExtraFees;} Set;}
		
		public Boolean splitFee{get{if(splitFee == null) splitFee = false; return splitFee;} Set;}
		public List<SelectOption> unitsRange{get; Set;}
		public integer instalmentOptionInterval{get; Set;}
		public string instalmentOption{get{if(instalmentOption == null) instalmentOption = ''; return instalmentOption;} Set;}
		public List<SelectOption> instalmentCalendarFrom{get{if(instalmentCalendarFrom == null) instalmentCalendarFrom = new List<SelectOption>();return instalmentCalendarFrom;} Set;}
		public integer posInstalmentDate{get; Set;}
		public string errorMsg{get{if(errorMsg == null) errorMsg = ''; return errorMsg;} Set;}
		public Boolean courseSaved{get{if(courseSaved == null) courseSaved = true; return courseSaved;} Set;}
		public string dateFirstInstalment{
			get{
				if(dateFirstInstalment == null)
					dateFirstInstalment = Date.today().format();
				return dateFirstInstalment;
			}
			Set;
		}
	}
	
	private list<courseIntalments> listCoursesInstalments;
	public list<courseIntalments> getlistCoursesInstalments(){
		if(listCoursesInstalments == null){
			listCoursesCart = [Select Campus_Course__c, Campus_Course__r.Campus__c, Campus_Course__r.Campus__r.Name, Campus_Course__r.Campus__r.Campus_Photo_URL__c, Campus_Course__r.Course__r.name, Custom_Campus__c, Custom_City__c, Custom_Country__c, Custom_Course__c, Custom_Currency__c, Custom_School__c, Custom_StartDate__c, Custom_Unit__c, Custom_Value__c, 
								Custom_Fees__c , isCustomCourse__c, Optional_Fee_Ids__c, Optional_Fee_Related_Ids__c, Required_Fee_Changeable__c, Start_Date__c, Total_Course__c, Total_Tuition__c, Total_Extra_fees__c, Number_Instalments__c, Instalments__c, 
								Campus_Course__r.Course__r.Payment_Rules__c, Campus_Course__r.Course__r.Important_information__c, Campus_Course__r.Course__r.Instalment_Interval__c, Campus_Course__r.Course__r.Instalment_Option__c, End_Date__c, 
								Campus_Course__r.Course__r.Instalment_Term_Option__c, Campus_Course__r.Course__r.Instalment_Term_Interval__c,
								Campus_Course__r.Course__r.Account_Document_File__c, Campus_Course__r.Course__r.Account_Document_File__r.Description__c, Campus_Course__r.Course__r.Account_Document_File__r.Preview_Link__c, Campus_Course__r.Course__r.Account_Document_File__r.File_Name__c,
								//( Select Details__c, Instalments__c from Search_Course_Instalments__r), 
								Instalment_Option__c, Instalment_Interval__c,
								( Select Course_Extra_Fee__c, Custom_Fee_Name__c, Extra_Fee_Interval__c, Fee_Value__c, isOptional__c, Number_Units__c from Search_Course_Extra_Fees__r) 
								from Search_Courses__c WHERE Web_Search__c = :accoId and Course_Deleted__c = false ORDER BY Course_Order__c NULLS LAST, CreatedDate];
			listCoursesInstalments = new list<courseIntalments>();
			
			for(Search_Courses__c sc:listCoursesCart){
				
				if(sc.isCustomCourse__c){
					//sc.Total_Tuition__c = sc.Total_Course__c;
					sc.Start_Date__c = sc.Custom_StartDate__c;
					//sc.End_Date__c = sc.Custom_End_Date__c;
					//for(Search_Course_Extra_Fee__c ef:sc.Search_Course_Extra_Fees__r)
					//	sc.Total_Extra_Fees__c = ef.Fee_Value__c * ef.Number_Units__c;
					
				}
				setCampus.add(sc.Campus_Course__r.Campus__c);
				listCoursesInstalments.add(createCourse(sc));
				createInstalmentFromList(sc.id, Integer.valueOf(sc.Number_Instalments__c));
			}
		}
		return listCoursesInstalments;
	}
	
	/*public pagereference viewFile(){
		String fileId =  Apexpages.currentPage().getParameters().get('fileId');
		String campusFileId =  Apexpages.currentPage().getParameters().get('campusFileId');
		return new pageReference(cg.SDriveTools.getAttachmentURL( campusFileId, fileId, (100 * 60)));	
	}*/
	
	private courseIntalments createCourse(Search_Courses__c sc){
		instalmentDetails itd;
		courseIntalments course = new courseIntalments();
		
		
		
		
		Decimal tuitionCustomFees = 0;
							
		if(sc.Custom_Fees__c != null){
			List<String> cFees = sc.Custom_Fees__c.split(':&');
			for(String cfee : cFees){
				List<String> customFeeStr = cfee.split(':#');
				
				double feevalue = Double.valueOf( customFeeStr[1] );
				String Type = customFeeStr[2];
				String applyTo = customFeeStr[3];
				
				if(applyTo == 'tuition')
					if(Type == 'subtract')
						tuitionCustomFees -= feevalue;
					else
						tuitionCustomFees += feevalue;

			}
		}
		
		system.debug('@@@ tuitionCustomFees: ' + tuitionCustomFees);
		
		course.courseTotalTuition = sc.Total_Tuition__c + tuitionCustomFees;
		
		course.courseTotalExtraFees = sc.Total_Extra_Fees__c - tuitionCustomFees ;
		
		course.totalInstalment = 0;
		course.totalExtraFees = 0;
		course.courseDetail = sc;
		
			
		if(course.courseDetail.Total_Extra_Fees__c == null)
			course.courseDetail.Total_Extra_Fees__c = 0;
		// if(course.courseDetail.Campus_Course__r.Course__r.Important_information__c != null)
		// 	course.courseDetail.Campus_Course__r.Course__r.Important_information__c = course.courseDetail.Campus_Course__r.Course__r.Important_information__c.replace('\n','<br/> <br/>');
		// if(course.courseDetail.Campus_Course__r.Course__r.Payment_Rules__c != null)
		// 	course.courseDetail.Campus_Course__r.Course__r.Payment_Rules__c = course.courseDetail.Campus_Course__r.Course__r.Payment_Rules__c.replace('\n','<br/> <br/>');
		
		//Get tuition instalments ===  number:date:tuitionValue:feeValue
		integer countInstal = 0;
		if(sc.Instalments__c != null && sc.Instalments__c.trim() != ''){
			for(String s :sc.Instalments__c.split(';')){
				itd = new instalmentDetails();	
				itd.intalmentNumber = integer.valueOf(s.split(':')[0]);
				itd.intalmentDate = s.split(':')[1];
				itd.instalmentDateValue = Date.parse(itd.intalmentDate);
				itd.intalmentValue = decimal.valueOf(s.split(':')[2]);
				itd.extraFeeValue = decimal.valueOf(s.split(':')[3]);
				
				course.totalInstalment += itd.intalmentValue;
				course.totalExtraFees += itd.extraFeeValue;
				
				countInstal++;
				if(course.courseDetail.Number_Instalments__c == countInstal){
					itd.intalmentValue -= course.totalInstalment - course.courseTotalTuition;
					course.totalInstalment -= course.totalInstalment - course.courseTotalTuition;
					
					itd.extraFeeValue -= course.totalExtraFees - course.courseTotalExtraFees;
					course.totalExtraFees -= course.totalExtraFees - course.courseTotalExtraFees;
				}
				
				course.intalments.add(itd);
				if(itd.instalmentDateValue > sc.End_Date__c)
					course.errorMsg += '<p /> '+itd.intalmentDate;
			}
		}
		
		if(course.errorMsg != '')
			course.errorMsg = '<span style="color:red; font-weight: bold;">'+course.errorMsg+'</span>';
		
		course.unitsRange = new  List<SelectOption>();
		/*if(sc.Campus_Course__r.Course__r.Instalment_Interval__c != null && integer.valueOf(sc.Campus_Course__r.Course__r.Instalment_Interval__c) > 0)
			course.unitsRange.addAll(createUnitsRange(integer.valueOf(sc.Campus_Course__r.Course__r.Instalment_Interval__c)));
		else */course.unitsRange.addAll(createUnitsRange(52));
		
		if(sc.Instalment_Interval__c != null){
			course.instalmentOption = sc.Instalment_Option__c;
			course.instalmentOptionInterval = integer.valueOf(sc.Instalment_Interval__c);
		}else{

			if(sc.Campus_Course__r.Course__r.Instalment_Option__c != null)
				course.instalmentOption = sc.Campus_Course__r.Course__r.Instalment_Option__c;
			else course.instalmentOption = 'Month';
			if(sc.Campus_Course__r.Course__r.Instalment_Interval__c != null && course.instalmentOptionInterval == null)
				course.instalmentOptionInterval = integer.valueOf(sc.Campus_Course__r.Course__r.Instalment_Interval__c);
			else course.instalmentOptionInterval = 1;
		}
		
		if(course.intalments.size() == 0){
			itd = new instalmentDetails();
			itd.intalmentNumber = 1;
			//itd.extraFeeValue = sc.Total_Extra_Fees__c;
			itd.extraFeeValue = course.courseTotalExtraFees;
			//itd.intalmentValue = sc.Total_Tuition__c;
			itd.intalmentValue = course.courseTotalTuition;
			itd.intalmentDate = System.today().format();
			itd.instalmentDateValue = System.today();
			//course.totalInstalment = sc.Total_Tuition__c;
			course.totalInstalment = course.courseTotalTuition;			
			//course.totalExtraFees = sc.Total_Extra_Fees__c;
			course.totalExtraFees = course.courseTotalExtraFees;
			
			course.intalments.add(itd);
			
		}
		return course;
	}
	
	
	
	public PageReference saveUpdate(){
		String courseUpdate =  ApexPages.currentPage().getParameters().get('courseUpdate');
		for(courseIntalments ci:listCoursesInstalments){
			if(ci.courseDetail.id == courseUpdate){
				list<string> listInstalment = new list<string>();
				integer instalNumber = 0;
				for(instalmentDetails itd:ci.intalments){
					listInstalment.add(itd.intalmentNumber + ':' + itd.intalmentDate + ':' + itd.intalmentValue + ':' + itd.extraFeeValue);
					if(instalNumber == 0){
						ci.courseDetail.Value_First_Instalment__c = itd.intalmentValue + itd.extraFeeValue;
						instalNumber = 1;
					}
				}
				ci.courseDetail.Number_Instalments__c = ci.intalments.size();			
				ci.courseDetail.Instalments__c = String.join(listInstalment, ';');
				ci.courseDetail.Instalment_Interval__c = ci.instalmentOptionInterval;
				ci.courseDetail.Instalment_Option__c = ci.instalmentOption;
				ci.courseSaved = true;
				update ci.courseDetail;
				listCoursesInstalments = null;
				break;
			}
			
		}
		
		return null;
	}
	
	public PageReference cancelUpdate(){
		String courseId =  ApexPages.currentPage().getParameters().get('courseUpdate');
	
		integer coursePosition;
		for(integer i =0; i < listCoursesInstalments.size(); i++)
			if(listCoursesInstalments[i].courseDetail.id == courseId){
				listCoursesInstalments.remove(i);
				coursePosition = i;
				break;
			}
		
		if(coursePosition != null){
			Search_Courses__c sc = [Select Custom_Fees__c, Campus_Course__c, Campus_Course__r.Campus__c, Campus_Course__r.Campus__r.Name, Campus_Course__r.Campus__r.Campus_Photo_URL__c, Campus_Course__r.Course__r.name, Custom_Campus__c, Custom_City__c, Custom_Country__c, Custom_Course__c, Custom_Currency__c, Custom_School__c, Custom_StartDate__c, Custom_Unit__c, Custom_Value__c, 
									isCustomCourse__c, Optional_Fee_Ids__c, Optional_Fee_Related_Ids__c, Required_Fee_Changeable__c, Start_Date__c, Total_Course__c, Total_Tuition__c, Total_Extra_fees__c, Number_Instalments__c, Instalments__c, 
									Campus_Course__r.Course__r.Payment_Rules__c, Campus_Course__r.Course__r.Important_information__c, Campus_Course__r.Course__r.Instalment_Interval__c, Campus_Course__r.Course__r.Instalment_Option__c, End_Date__c, 
									Campus_Course__r.Course__r.Instalment_Term_Option__c, Campus_Course__r.Course__r.Instalment_Term_Interval__c ,
									Campus_Course__r.Course__r.Account_Document_File__c, Campus_Course__r.Course__r.Account_Document_File__r.Description__c, Campus_Course__r.Course__r.Account_Document_File__r.Preview_Link__c, Campus_Course__r.Course__r.Account_Document_File__r.File_Name__c,
									Instalment_Option__c, Instalment_Interval__c,
									//( Select Details__c, Instalments__c from Search_Course_Instalments__r), 
									( Select Course_Extra_Fee__c, Custom_Fee_Name__c, Extra_Fee_Interval__c, Fee_Value__c, isOptional__c, Number_Units__c from Search_Course_Extra_Fees__r) 
									from Search_Courses__c where Id = :courseId];
			
			courseIntalments ci = createCourse(sc);
			ci.courseSaved = true;
			if(coursePosition == listCoursesInstalments.size())
				listCoursesInstalments.add(ci);
			else listCoursesInstalments.add(coursePosition, ci);
		}
		
		return null;
	}
	
	private map<string,map<integer,map<integer,list<Campus_Instalment_Payment_Date__c>>>> courseCalendar;
	public Map<String, Boolean> campusHasPaymentDate{
		get{
			if(campusHasPaymentDate == null){
				getCourseCalendar();
			}
			
			return campusHasPaymentDate;
		} 
		Set;
	}
	public map<string, map<integer, map<integer,list<Campus_Instalment_Payment_Date__c>>>> getCourseCalendar(){
		if(courseCalendar == null){
			courseCalendar = new map<string, map<integer, map<integer,list<Campus_Instalment_Payment_Date__c>>>>();
			campusHasPaymentDate = new  Map<String, Boolean>();
			for(String s:setCampus){
				courseCalendar.put(s, new map<integer, map<integer,list<Campus_Instalment_Payment_Date__c>>>());
				campusHasPaymentDate.put(s, false);
			}
			for(Campus_Instalment_Payment_Date__c cci: [Select Campus__c, Instalment_Payment_Date__c, isSelected__c from Campus_Instalment_Payment_Date__c where Campus__c = :setCampus and Instalment_Payment_Date__c >= today order by Campus__c, Instalment_Payment_Date__c]){
				
				if(!courseCalendar.containsKey(cci.campus__c)){
					
					map<integer,list<Campus_Instalment_Payment_Date__c>> m = new map<integer,list<Campus_Instalment_Payment_Date__c>>{ cci.Instalment_Payment_Date__c.month() => new list<Campus_Instalment_Payment_Date__c>{cci} };
					courseCalendar.put(cci.campus__c, new map<integer, map<integer,list<Campus_Instalment_Payment_Date__c>>>{ cci.Instalment_Payment_Date__c.year() => m });
					campusHasPaymentDate.put(cci.campus__c, true);
					
				} else if(!courseCalendar.get(cci.campus__c).containsKey(cci.Instalment_Payment_Date__c.year())){										
					
					courseCalendar.get(cci.campus__c).put(cci.Instalment_Payment_Date__c.year(), new Map<Integer, list<Campus_Instalment_Payment_Date__c>>{ cci.Instalment_Payment_Date__c.month() => new list<Campus_Instalment_Payment_Date__c>{cci} });										
					campusHasPaymentDate.put(cci.campus__c, true);
					
					
				} else if(!courseCalendar.get(cci.campus__c).get(cci.Instalment_Payment_Date__c.year()).containsKey(cci.Instalment_Payment_Date__c.month())){
					
					courseCalendar.get(cci.campus__c).get(cci.Instalment_Payment_Date__c.year()).put( cci.Instalment_Payment_Date__c.month(), new list<Campus_Instalment_Payment_Date__c>{cci} );
					campusHasPaymentDate.put(cci.campus__c, true);
					
				} else 
				
					courseCalendar.get(cci.campus__c).get(cci.Instalment_Payment_Date__c.year()).get(cci.Instalment_Payment_Date__c.month()).add(cci);
				
				
				
				
			}
		}
		return courseCalendar;
		
	}
	
	
		
	public list<SelectOption> createUnitsRange(Integer numUnits){
    	List<SelectOption> options = new List<SelectOption>();
        for(Integer i = 1; i <= numUnits; i++){
            options.add(new SelectOption(string.valueOf(i), string.valueOf(i)));
        }
        return options;
    }
	
	
	
	public PageReference calculateInstalments(){
		String courseId = ApexPages.currentPage().getParameters().get('courseId');
		Decimal numInstal =  Decimal.valueOf(ApexPages.currentPage().getParameters().get('numInstal'));
		Boolean splitFee = Boolean.valueOf(ApexPages.currentPage().getParameters().get('splitFee'));
		Integer interval = Integer.valueOf(ApexPages.currentPage().getParameters().get('interval'));
		string intervalOption = ApexPages.currentPage().getParameters().get('intervalOption');
		String dtFisrtInstalment = ApexPages.currentPage().getParameters().get('dtFisrtInstalment');
		System.debug('==>interval: '+interval);
		Search_Courses__c sc = [Select Custom_Fees__c, Campus_Course__c, Campus_Course__r.Campus__c, Campus_Course__r.Campus__r.Name, Campus_Course__r.Campus__r.Campus_Photo_URL__c, Campus_Course__r.Course__r.name, Custom_Campus__c, Custom_City__c, Custom_Country__c, Custom_Course__c, Custom_Currency__c, Custom_School__c, Custom_StartDate__c, Custom_Unit__c, Custom_Value__c, 
								isCustomCourse__c, Optional_Fee_Ids__c, Optional_Fee_Related_Ids__c, Required_Fee_Changeable__c, Start_Date__c, Total_Course__c, Total_Tuition__c, Total_Extra_fees__c, Number_Instalments__c, Instalments__c, 
								Campus_Course__r.Course__r.Payment_Rules__c, Campus_Course__r.Course__r.Important_information__c, Campus_Course__r.Course__r.Instalment_Interval__c, Campus_Course__r.Course__r.Instalment_Option__c, End_Date__c, 
								Campus_Course__r.Course__r.Instalment_Term_Option__c, Campus_Course__r.Course__r.Instalment_Term_Interval__c,
								Campus_Course__r.Course__r.Account_Document_File__c, Campus_Course__r.Course__r.Account_Document_File__r.Description__c, Campus_Course__r.Course__r.Account_Document_File__r.Preview_Link__c, Campus_Course__r.Course__r.Account_Document_File__r.File_Name__c,
								//( Select Details__c, Instalments__c from Search_Course_Instalments__r), 
								Instalment_Option__c, Instalment_Interval__c,
								( Select Course_Extra_Fee__c, Custom_Fee_Name__c, Extra_Fee_Interval__c, Fee_Value__c, isOptional__c, Number_Units__c from Search_Course_Extra_Fees__r) 
								from Search_Courses__c where Id = :courseId];
		
		
		
		Decimal tuitionCustomFees = 0;
		Decimal tuition = 0;
		Decimal extrafees = 0;
		if(sc.Custom_Fees__c != null){
			List<String> cFees = sc.Custom_Fees__c.split(':&');
			for(String cfee : cFees){
				List<String> customFeeStr = cfee.split(':#');
				
				double feevalue = Double.valueOf( customFeeStr[1] );
				String Type = customFeeStr[2];
				String applyTo = customFeeStr[3];
				
				if(applyTo == 'tuition')
					if(Type == 'subtract')
						tuitionCustomFees -= feevalue;
					else
						tuitionCustomFees += feevalue;

			}
		}
		
		system.debug('@@@ tuitionCustomFees: ' + tuitionCustomFees);
		
		tuition = sc.Total_Tuition__c + tuitionCustomFees;
		
		extrafees = sc.Total_Extra_Fees__c - tuitionCustomFees ;
		
		
		
		
		Decimal tuitionInstalValue = (tuition/numInstal).setScale(2, RoundingMode.HALF_UP);
		Decimal feeInstalValue;
		if(extrafees != null && extrafees != 0 && splitFee)
			feeInstalValue = (extrafees/numInstal).setScale(2, RoundingMode.HALF_UP);
		else if(extrafees != null && extrafees != 0 && !splitFee)
			feeInstalValue = extrafees;
		else feeInstalValue = 0;
		list<string> listInstalment = new list<string>();
		Date payDate = Date.parse(dtFisrtInstalment);
		for(Integer i = 1; i <= numInstal; i++){
			if(splitFee){
				listInstalment.add(i + ':' + payDate.format() + ':' +tuitionInstalValue + ':' + feeInstalValue);
				sc.Value_First_Instalment__c = tuitionInstalValue + feeInstalValue;
				sc.Value_Instalments__c = tuitionInstalValue + feeInstalValue;
			}else{
				if(i == 1){
					listInstalment.add(i + ':' + payDate.format() + ':' +tuitionInstalValue + ':' + feeInstalValue);
					sc.Value_First_Instalment__c = tuitionInstalValue + feeInstalValue;
				}
				else {
					listInstalment.add(i + ':' + payDate.format() + ':' +tuitionInstalValue + ':' + 0);
					sc.Value_Instalments__c = tuitionInstalValue;
				}
			}
			
			if (intervalOption == 'Month')
                payDate = payDate.addMonths(interval);
            else if (intervalOption == 'Quarter')
                payDate = payDate.addMonths(interval*3); 
            else if (intervalOption == 'Semester')
                payDate = payDate.addMonths(interval*6); 
            else if (intervalOption == 'Year')
                payDate = payDate.addYears(interval); 
            else if (intervalOption == 'Week') 
                payDate = payDate.addDays(interval*7); 
			/*else if (intervalOption == 'Term'){
				string termIntervalOption = sc.Campus_Course__r.Course__r.Instalment_Term_Option__c;
				integer termInterval = Integer.valueOf(sc.Campus_Course__r.Course__r.Instalment_Term_Interval__c);
				
				if (termIntervalOption == 'Month')
	                payDate = payDate.addMonths(termInterval);
	            else if (termIntervalOption == 'Quarter')
	                payDate = payDate.addMonths(termInterval*3); 
	            else if (termIntervalOption == 'Semester')
	                payDate = payDate.addMonths(termInterval*6); 
	            else if (termIntervalOption == 'Year')
	                payDate = payDate.addYears(termInterval); 
	            else if (termIntervalOption == 'Week') 
	                payDate = payDate.addDays(termInterval*7); 
			} */
			
		}
		
		sc.Instalments__c = String.join(listInstalment, ';');
		sc.Number_Instalments__c = numInstal;
		
		integer coursePosition;
		for(integer i =0; i < listCoursesInstalments.size(); i++)
			if(listCoursesInstalments[i].courseDetail.id == courseId){
				listCoursesInstalments.remove(i);
				coursePosition = i;
				break;
			}
		
		if(sc.isCustomCourse__c){
			//sc.Total_Tuition__c = sc.Total_Course__c;
			sc.Start_Date__c = sc.Custom_StartDate__c;
			//sc.End_Date__c = sc.Custom_End_Date__c;
			//for(Search_Course_Extra_Fee__c ef:sc.Search_Course_Extra_Fees__r)
			//	sc.Total_Extra_Fees__c = ef.Fee_Value__c * ef.Number_Units__c;
			
		}
		setCampus.add(sc.Campus_Course__r.Campus__c);
		
		courseIntalments ci = createCourse(sc);
		ci.courseSaved = false;
		if(coursePosition == listCoursesInstalments.size())
			listCoursesInstalments.add(ci);
		else listCoursesInstalments.add(coursePosition, ci);
		
		createInstalmentFromList(sc.id, Integer.valueOf(sc.Number_Instalments__c));
		for(courseIntalments cInst:listCoursesInstalments){
			if(cInst.courseDetail.id == courseId){
				cInst.instalmentOptionInterval = interval;
				cInst.instalmentOption = intervalOption;
				break;
			}
		}
		/*update sc;
		
		listCoursesInstalments = null;
		getlistCoursesInstalments();
		createInstalmentFromList(courseId, Integer.valueOf(numInstal));*/
		return null;
	}
	
	
	//InstalmentInterval
    public List<SelectOption> InstalmentInterval{
		get{
            if(InstalmentInterval == null){
                InstalmentInterval = new List<SelectOption>();
            
                InstalmentInterval.add(new SelectOption('Week','Week')); 
                InstalmentInterval.add(new SelectOption('Month','Month')); 
                InstalmentInterval.add(new SelectOption('Quarter','Quarter')); 
                InstalmentInterval.add(new SelectOption('Semester','Semester')); 
                InstalmentInterval.add(new SelectOption('Year','Year')); 
				//InstalmentInterval.add(new SelectOption('Term','Term')); 
            }
            return InstalmentInterval;
        }
        set;
    }
        

    public List<SelectOption> instalmentIntervalUnits{
    	get{
            if(instalmentIntervalUnits == null){
                instalmentIntervalUnits = new List<SelectOption>();
                for(integer nUnit = 1; nUnit <= 18; nUnit++)
                    instalmentIntervalUnits.add(new SelectOption(String.valueOf(nUnit),String.valueOf(nUnit)));        
            }
            return instalmentIntervalUnits;
        }
        set;
    }
		
	public void createInstalmentFromList(String courseId, Integer numInstalments){
		for(courseIntalments ci:listCoursesInstalments)
			if(ci.courseDetail.id == courseId){
				ci.instalmentCalendarFrom.clear();
				ci.instalmentCalendarFrom.addAll(createUnitsRange(numInstalments));
				break;
			}
			
	}
	
	public PageReference addInstalmentDates(){
		String courseId = ApexPages.currentPage().getParameters().get('courseId');
		String campusId = ApexPages.currentPage().getParameters().get('campusId');
		Integer posInstalment;// = Integer.valueOf(ApexPages.currentPage().getParameters().get('posInstalment'));
		list<date> selectedDates = new list<date>();
		
		System.debug('==>courseId: '+courseId);
		System.debug('==>posInstalment: '+posInstalment);
		System.debug('==>courseCalendar: '+courseCalendar);
		
		for(integer s : courseCalendar.get(campusId).keySet())
			for(integer y : courseCalendar.get(campusId).get(s).keySet())
				for(Campus_Instalment_Payment_Date__c cpd: courseCalendar.get(campusId).get(s).get(y)){
					if(cpd.isSelected__c)
						selectedDates.add(cpd.Instalment_Payment_Date__c);
				}
		
		selectedDates.sort();
		System.debug('==> selectedDates: '+selectedDates);
		Integer dateCounter = 0;
		for(courseIntalments ci:listCoursesInstalments){
			System.debug('==> ci: '+ci);
			System.debug('==> ci.courseDetail.id == courseId: ' + ci.courseDetail.id + ' == '+ courseId);
			if(ci.courseDetail.id == courseId){
				list<string> listInstalment = new list<string>();
				posInstalment = ci.posInstalmentDate;
				for(Integer i = 0; i < ci.intalments.size(); i++){
					if(dateCounter < selectedDates.size() && i >= posInstalment-1){
						ci.intalments[i].intalmentDate = selectedDates.get(dateCounter).format();
						ci.intalments[i].instalmentDateValue = selectedDates.get(dateCounter);
						dateCounter++;
					}
					System.debug('==> ci.intalments[i].intalmentDate: '+ci.intalments[i].intalmentDate);
					listInstalment.add(ci.intalments[i].intalmentNumber + ':' + ci.intalments[i].intalmentDate + ':' + ci.intalments[i].intalmentValue + ':' + ci.intalments[i].extraFeeValue);
					
				}
				System.debug('==> listInstalment: '+listInstalment);
				ci.courseDetail.Instalments__c = String.join(listInstalment, ';');
				update ci.courseDetail;
				break;
			}
			
		}
		listCoursesInstalments = null;
		getlistCoursesInstalments();
		
		return null;
	}
	
}