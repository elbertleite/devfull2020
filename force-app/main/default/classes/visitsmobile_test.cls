/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class visitsmobile_test {

    static testMethod void myUnitTest() {
        
		TestFactory tf = new TestFactory();
		Account agency = tf.createAgency();
		Contact employee = tf.createEmployee(agency);
		
		User portalUser = tf.createPortalUser(employee);
		
		system.runAs(portalUser){
			
			Contact client = tf.createClient(agency);
			Visit__c vis = tf.createVisit(client);
			vis.Status__c = 'Waiting';
			update vis;
			visitsmobile.searchEmail(client.email);
			visitsmobile.hasVisit(client.id);			
		}
    }

	@isTest static void hasVisitTest(){  
       TestFactory tf = new TestFactory();
		Account agency = tf.createAgency();
		Contact employee = tf.createEmployee(agency);
		
		User portalUser = tf.createPortalUser(employee);
		
		system.runAs(portalUser){
			
			Contact client = tf.createClient(agency);		
			visitsmobile.hasVisit(client.id);			
		}     
    }

	@isTest static void searchEmailTest(){  
        string em = 'gmialg3@tg.com';
        visitsmobile.searchEmail(em);       
    }

	@isTest static void verifyPasswordTest(){  
        string password = '';
        Boolean c = visitsmobile.verifyPassword(password);                
        //System.assertEquals(c, 'Invalid Password');
    }

    @isTest static void verifyPassword1Test(){  
        string password = '1234';
        Boolean c = visitsmobile.verifyPassword(password);                
       // System.assertEquals(c, 'Password has to be setted');
    }
        
    @isTest static void verifyPassword2Test(){  
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();
        agency.Form_Password__c = '1234';
        update agency;
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);

        Test.startTest();
     	system.runAs(portalUser){
            string password = '1234';
            Boolean c = visitsmobile.verifyPassword(password);                
            //System.assertEquals(c, 'success');
        }
    }
}