/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class extRedirectPage_test {

    static testMethod void verifyRedirectPage(){
		
		
		Map<String,String> recordTypes = new Map<String,String>();
		
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
		
		Account acc = new Account();
        acc.RecordTypeId = recordTypes.get('Agency');
        acc.Name = 'IP Sydney'; 
        insert acc;
        
        Contact ct = new Contact();
        ct.accountid = acc.id;
        ct.FirstName = 'John';
        ct.LastName = 'Doe';
        insert ct;
		
		Account agency = new Account();
		agency.Name = 'IP Australia';
		agency.RecordTypeId = recordTypes.get('Agency');
		insert agency;
		
		Account school = new Account();
		school.RecordTypeId = recordTypes.get('School');
		school.Name = 'School CBD';
		insert school;		
		
		
		Account campus = new Account();
		campus.RecordTypeId = recordTypes.get('Campus');
		campus.Name = 'Campus CBD';
		campus.ParentId = school.id;
		insert campus;		
		
		Course__c course = new Course__c();
		course.Name = 'Advanced Math';
		course.School__c = school.id;
		insert course;
		
		Campus_Course__c cc = new Campus_Course__c();		
		cc.Campus__c = campus.Id;
		cc.Course__c = course.Id;
		insert cc;
		
		
		ApexPages.StandardController controller = new ApexPages.StandardController(acc);
		extRedirectPage rp = new extRedirectPage(controller);		
		Apexpages.currentPage().getParameters().put('retURL','return');		
		Apexpages.currentPage().getParameters().put('mid', acc.Id);
		Apexpages.currentPage().getParameters().put('id', acc.Id);		
		rp.RedirectToPage();
		
		rp.edit();
		rp.Cancel();
		rp.Save();
		
		
		rp.selectedUser = UserInfo.getUserId();
		
		
		
		String rpg = rp.rpg;
		//rp.SaveCampusCourse();
		rp.SaveEmployeeUser();
		rp.del();
		
		course = new Course__c();
		course.Name = 'Advanced Math';
		course.School__c = school.id;
		upsert course;
		
		cc = new Campus_Course__c();		
		cc.Campus__c = campus.Id;
		cc.Course__c = course.Id;
		upsert cc;
		
		ApexPages.StandardController controller2 = new ApexPages.StandardController(cc);
		extRedirectPage rp2 = new extRedirectPage(controller2);
		rp2.delCourse();
		
		course = new Course__c();
		course.Name = 'Advanced Math';
		course.School__c = school.id;
		upsert course;
		
		cc = new Campus_Course__c();		
		cc.Campus__c = campus.Id;
		cc.Course__c = course.Id;
		upsert cc;
		
		ApexPages.StandardController controller3 = new ApexPages.StandardController(cc);
		extRedirectPage rp3 = new extRedirectPage(controller3);
		
		//rp2.SaveCampusCourse();
		rp3.delCampusCourse();
		
		
	}
}