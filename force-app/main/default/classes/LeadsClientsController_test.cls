@isTest
private class LeadsClientsController_test{
	static testMethod void myUnitTest() {
		TestFactory tf = new TestFactory();
		
		Account agency = tf.createAgency();

		Account agencyGroup = tf.createAgencyGroup();
		agencyGroup.RDStation_Campaigns__c = '["leading-page3","leading-australia","insercao-manual","leading-teste2"]';
		update agencyGroup;

		tf.createChecklists(agency.Parentid);

		Contact emp = tf.createEmployee(agency);
		
		Contact lead = tf.createLead(agency, emp);
		lead.Ownership_History_Field__c = '[{"toUserID":"005O0000004bBaSIAU","toUser":"Patricia Greco","toAgencyID":"0019000001MjqIIAAZ","toAgency":"IP Australia Sydney (HO)","fromUserID":null,"fromUser":null,"fromAgencyID":null,"fromAgency":null,"createdDate":"2018-09-18T04:17:12.024Z","createdByUserID":"005O0000004bBaSIAU","createdByUser":"Patricia Greco","action":"Taken"}]';
		lead.Lead_Stage__c = 'Stage 1';
		lead.Lead_Area_of_Study__c = 'Test 1; Test 2';
		update lead;

		Contact lead2 = tf.createClient(agency);
		Contact lead3 = tf.createClient(agency);

		List<String> leads = new List<String>();
		leads.add(lead.ID);
		leads.add(lead2.ID);
		leads.add(lead3.ID);

		Contact client = tf.createClient(agency);

		User portalUser = tf.createPortalUser(emp);

		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);

       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);

		Quotation__c q1 = tf.createQuotation(lead, campusCourse);
		Quotation__c q2 = tf.createQuotation(lead, campusCourse);

		Destination_Tracking__c dt = new Destination_tracking__c();
		dt.Client__c = lead.id;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
		dt.Expected_Travel_Date__c = system.today().addMonths(3);
		dt.lead_Cycle__c = false;
		insert dt;

		Client_Checklist__c checklist = new Client_Checklist__c();
		checklist.Agency__c = agency.id;
		checklist.Agency_Group__c = agency.Parentid;
		checklist.Destination__c = lead.Destination_Country__c;
		checklist.Checklist_Item__c = 'aaa';
		checklist.Client__c = lead.id;
		checklist.Destination_Tracking__c = dt.id;
		insert checklist;

		Test.startTest();
     	system.runAs(portalUser){

			List<Client_Stage_Follow_up__c> followUps = new List<Client_Stage_Follow_up__c>();
			String followUpID;
			for(Client_Stage__c stage : [SELECT ID, Stage_Description__c FROM Client_Stage__c WHERE Agency_Group__c = : agency.Parentid]){
				Client_Stage_Follow_up__c cs = new Client_Stage_Follow_up__c();
				cs.Agency__c = agency.id;
				cs.Agency_Group__c = agency.Parentid;
				cs.Client__c = lead.id;
				cs.Destination__c = 'Australia';
				cs.Stage_Item__c = stage.Stage_Description__c;
				cs.Stage_Item_Id__c = stage.id;
				cs.Stage__c = 'Stage 1';
				cs.Destination_Tracking__c = dt.id;
				followUps.add(cs);
			}

			insert followUps;
		
			followUpID = followUps.get(0).ID;

			emp.Account = agency;
			portalUser.contact = emp;
			update portalUser;

			Client_Stage__c csc = new Client_Stage__c();
			csc.Stage_description__c = 'LEAD - Not Interested';
			csc.Agency_Group__c = portalUser.Contact.Account.ParentId;
			csc.Stage_Sub_Options__c = '["Option Not Interested 1","Option Not Interested 2","Option Not Interested 3"]';
			insert csc;

			
			LeadsClientsController controller = new LeadsClientsController();

			LeadsClientsController.init();
			
			LeadsClientsController.searchClientsPerStatus(new List<String>(), agencyGroup.id, agency.id,'xxx', 'Stage 1', 'xxx', '456', '2', '12345', 'xxx', 'xxx', 'xxx', 'Enquiry Date', '10/10/2010', '10/10/2058', 'aaa', 'aaa', 'aaa', 0, 50000, 'String school', 'All', 0, 0, 'Contact Name', '');

			LeadsClientsController.searchClientsPerStatus(new List<String>(), agencyGroup.id, agency.id,'xxx', 'Stage 1', 'xxx', '456', '2', '12345', 'xxx', 'xxx', 'xxx', 'Expected Travel Date', '10/10/2010', '10/10/2058', 'aaa', 'aaa', 'aaa', 0, 50000, 'String school', 'All', 0, 0, 'Contact Name', '');

			LeadsClientsController.searchClientsPerStatus(new List<String>(), agencyGroup.id, agency.id,'xxx', 'Stage 1', 'xxx', '456', '2', '12345', 'xxx', 'xxx', 'xxx', 'Enquiry Date Test', '10/10/2010', '10/10/2058', 'aaa', 'aaa', 'aaa', 0, 50000, 'String school', 'All', 0, 0, 'Contact Name', '');

			LeadsClientsController.searchClients(new List<String>(), agencyGroup.id, agency.id,'xxx', 'Stage 1', 'xxx', '654',  'xxx', 'xxx', 'xxx', 'Enquiry Date', '10/10/2010', '10/10/2058', 'aaa', 'aaa', 'aaa', 0, 50000, 'String school', 'All', 0, 0, 'Contact Name', '');

			LeadsClientsController.refreshOwners(agency.id, emp.Department__c);

			LeadsClientsController.refreshAll(agencyGroup.id);

			LeadsClientsController.markLeadsAsLost(leads ,'','');
			LeadsClientsController.deleteLeads(leads);

			LeadsClientsController.refreshAll(agencyGroup.id);
			LeadsClientsController.saveFilterPreference(true, true, true, true, true, true, true, true, '');
			LeadsClientsController.refreshSchools(agencyGroup.ID, agency.ID, 'Australia', 'Stage 1');
			LeadsClientsController.refreshStages(agencyGroup.ID, agency.ID, 'Australia');
		}
	}
}