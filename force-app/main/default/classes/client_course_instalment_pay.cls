public with sharing class client_course_instalment_pay {

	private string instId;

	public client_course_instalment_pay(ApexPages.standardController controller){
		instId = controller.getId();

	}

	private decimal currentDepositUsed = 0;
	private decimal currentCourseCredit = 0;
	private Boolean findCommission = false;
	public client_course_instalment__c installment{
		get{
			if(installment == null){
				courseCreditDetails cd = new courseCreditDetails();
				installment = cd.retirveInstallment(instId);

				if(installment.client_course__r.isMigrated__c && installment.client_course__r.Enrolment_Date__c == null)
					isMigration = true;
				else
					isMigration = false;

				//Defines if system must search for the commission on agreement
				if(installment.client_course__r.Enrolment_Date__c == null)
					findCommission = true;

				retrieveDeposit(installment.client_course__r.Client__c);
				retrieveLoan(installment.client_course__r.Client__c);
				if(clientDeposit.Deposit_Total_Used__c == null)
					clientDeposit.Deposit_Total_Used__c = 0;
				else currentDepositUsed = clientDeposit.Deposit_Total_Used__c;

				system.debug('installment.client_course__r.Campus_Course__r.Campus__r.Parent.ParentId==' + installment.client_course__r.Campus_Course__r.Campus__r.Parent.ParentId);

				try{
					courseCredit = [Select id, Credit_Related_To__c, Credit_Courses__c, School_Name__c, Credit_Used_By__c, Credit_Value__c, Credit_Available__c,
										Deposit_Description__c from client_course__c WHERE Client__c = :installment.client_course__r.Client__c and isCourseCredit__c = true and Credit_Valid_For_School__c = :installment.client_course__r.Campus_Course__r.Campus__r.Parent.ParentId and Credit_Available__c > 0 limit 1];
					currentCourseCredit = courseCredit.Credit_Available__c;
				}catch(Exception e){

				}

				/** Instalment Currency **/
				retrieveMainCurrencies();
				//system.debug('==>agencyCurrencies: '+agencyCurrencies);
				installment.Agency_Currency__c = fin.currentUser.Contact.Account.Account_Currency_Iso_Code__c;
				recalculateRate();
				/** Client scholarship **/
				if(installment.Client_Scholarship__c!=null && installment.Client_Scholarship__c>0){

					newPayDetails.Payment_Type__c = 'Client Scholarship';
					newPayDetails.Date_Paid__c = system.today();
					newPayDetails.Value__c = installment.Client_Scholarship__c;
					addPaymentValue();

				}
				//
			}
			return installment;
		}
		set;
	}

	public without sharing class courseCreditDetails{
		public client_course_instalment__c retirveInstallment(string iId){
			return [Select client_course__c, client_course__r.Campus_Name__c,
						client_course__r.Enrolment_Date__c,
						client_course__r.Course_Type__c,
						client_course__r.Campus_Id__c,
						client_course__r.School_Id__c,
						client_course__r.Client__r.Owner__r.AccountId,
						client_course__r.Client__r.Owner__r.Contact.Account.BillingCountry,
						client_course__r.Client__r.Owner__r.Contact.Department__c,
						client_course__r.School_Name__c,
						client_course__r.Client__c,
						client_course__r.Client__r.name,
						client_course__r.Course_Name__c,
						client_course__r.Client__r.Current_agency__c,
						client_course__r.Client__r.Current_agency__r.required_pay_intall_to_agency__c,
						client_course__r.Client__r.Current_agency__r.BillingCountry,
						client_course__r.Campus_Course__r.Campus__r.Parent.ParentId,
						client_course__r.Deposit_Description__c,
						client_course__r.Deposit_Total_Used__c,
						client_course__r.Deposit_Total_Value__c,
						client_course__r.Deposit_Type__c,
						client_course__r.Deposit_Value_Track__c,
						client_course__r.isPackage__c,
						client_course__r.Total_Paid__c,
						client_course__r.Total_PDS_Commission_Pending__c,
						client_course__r.Total_Paid_Deposit__c,
						client_course__r.Total_Paid_PDS__c,
						client_course__r.Total_Tuition__c,
						client_course__r.CurrencyIsoCode__c,
						client_course__r.Total_Extra_Fees__c,
						client_course__r.Total_Course__c,
						client_course__r.Total_Paid_Credit__c,
						client_course__r.Course_City__c,
						client_course__r.Course_Country__c,
						client_course__r.isMigrated__c,
						client_course__r.Campus_Course__r.Campus__r.Parent.isTaxDeductible__c,
						pay_to_agency__c,
						Agency_Currency__c,
						Agency_Currency_Rate__c,
						Agency_Currency_Value__c,
						Commission_Tax_Rate__c,
						Due_Date__c, Extra_Fee_Value__c, Instalment_Value__c, Number__c, Id, Related_Fees__c, Client_Document__c, client_course__r.Enroled_by_Agency__c, Invoice__c, Client_Scholarship__c,
						Tuition_Value__c, Received_by__c,Received_by__r.Name, Received_Date__c, isPDS__c, isPCS__c, client_course__r.Course_Length__c, client_course__r.Start_Date__c, client_course__r.End_Date__c, client_course__r.Unit_Type__c,
						Commission_Value__c, isSharedCommission__c, isPFS__c, Discount__c, Kepp_Fee__c, Scholarship_Taken__c, instalment_activities__c, isFirstPayment__c, Required_for_COE__c, Total_Paid_School_Credit__c, covered_by_agency__c,
								( Select client_course_instalment__c, Date_Paid__c, Payment_Type__c, Id, Value__c, Agency_Currency_Value__c from client_course_instalment_payments__r)
								from client_course_instalment__c  WHERE Id = :iId];
		}
	}

	public client_course__c courseCredit{get; set;}


	public client_course__c clientDeposit{get; set;}

	private void retrieveDeposit(string contactId){
		try{
		 	clientDeposit = [Select Deposit_Description__c, Deposit_Total_Used__c, Deposit_Total_Value__c, Deposit_Total_Available__c, Deposit_Type__c, Deposit_Value_Track__c, Id, client__c, Client__r.name, Deposit_Total_Refund__c,
		 						(SELECT Id, Value__c, Received_By__r.Name, Received_On__c, Received_By_Agency__r.Name, Payment_Type__c, Date_Paid__c FROM client_course_payments_deposit__r)
 							FROM client_course__c where client__c = :contactId and isDeposit__c = true and Deposit_Total_Available__c > 0];
		}catch(Exception e){
			clientDeposit = new client_course__c();
		}
	}

	// ============ PAYMENT PLAN ===============================
	map<string, Payment_Plan__c> mapLoan;
	private void retrieveLoan(string contactId){
		mapLoan = new map<string, Payment_Plan__c>([Select id, Value__c, Value_Remaining__c from Payment_Plan__c where client__c = :contactId and Created_By_Agency__c = :fin.currentUser.Contact.AccountId and Loan_Status__c = 'approved' and Plan_Type__c = 'instalments' and Value_Remaining__c > 0]);
	}

	public list<client_course_instalment_payment__c> deposit{
		get{
			if(deposit == null && clientDeposit != null && clientDeposit.Deposit_Total_Value__c != null){
				deposit = clientDeposit.client_course_payments_deposit__r;
			}

			return deposit;
		}
		set;
	}

	private list<selectOption> depositListType;
	public list<selectOption> getdepositListType(){
		if(depositListType == null && installment != null){
			depositListType = new list<selectOption>();
			depositListType.add(new SelectOption('','Select Type'));

			if(installment.Discount__c <= 0 && installment.Kepp_Fee__c <= 0 && installment.Scholarship_Taken__c <= 0){{
				depositListType.add(new SelectOption('Pds','PDS'));
				depositListType.add(new SelectOption('notContacted','PDS / Client not Contacted'));
			}
				if(Schema.sObjectType.User.fields.Finance_Allow_Pay_PCS__c.isAccessible())
					depositListType.add(new SelectOption('Pcs','PCS'));

				/*SelectOption op;
				op = new SelectOption('Pcs','PCS' + ' <span style="color:#000;">(Payment Confirmed by School)</span>');
				op.setEscapeItem(false);
				depositListType.add(op);*/

			}

			///** Migration Rules **/
			//if(isMigration)
			//	depositListType.add(new SelectOption('offShore','Offshore'));

			if(clientDeposit.Deposit_Total_Used__c == null)
				clientDeposit.Deposit_Total_Used__c = 0;
			if(clientDeposit.Deposit_Total_Value__c != null && (clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c - clientDeposit.Deposit_Total_Refund__c) > 0 ){ //&& installment.client_course__r.Enrolment_Date__c != null
				depositListType.add(new SelectOption('Deposit','Deposit'));
				newPayDetails.Payment_Type__c = 'Deposit';
				newPayDetails.Date_Paid__c = System.today();
				if(clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c - clientDeposit.Deposit_Total_Refund__c > installment.Instalment_Value__c)
					newPayDetails.Value__c = installment.Instalment_Value__c;
				else newPayDetails.Value__c = clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c - clientDeposit.Deposit_Total_Refund__c;
			}

			for(Payment_Plan__c pp:mapLoan.values()){
				depositListType.add(new SelectOption('loan-'+pp.id,'Loan - '+ pp.Value_Remaining__c));
			}

			if(courseCredit != null && courseCredit.Credit_Available__c > 0){
				depositListType.add(new SelectOption('School Credit','School Credit'));

				if(newPayDetails.Payment_Type__c== null){
					newPayDetails.Payment_Type__c = 'School Credit';
					newPayDetails.Date_Paid__c = System.today();
					if(courseCredit.Credit_Available__c > installment.Instalment_Value__c)
						newPayDetails.Value__c = installment.Instalment_Value__c;
					else newPayDetails.Value__c = courseCredit.Credit_Available__c;
				}
			}

			Schema.DescribeFieldResult fieldResult = client_course__c.Deposit_Type__c.getDescribe();
			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
			for( Schema.PicklistEntry f : ple)
				depositListType.add(new SelectOption(f.getValue(), f.getLabel()));
			depositListType.sort();
		}
		return depositListType;
	}

	public class feeInstallmentDetail{
		public string feeName{get; set;}
		public decimal feeValue{get; set;}
		public feeInstallmentDetail(string feeName, decimal feeValue){
			this.feeName = feeName;
			this.feeValue = feeValue;
		}
	}

	private list<feeInstallmentDetail> feesInstallment;

	public list<feeInstallmentDetail> getFeesInstallment(){
		if(feesInstallment == null && installment != null){
			feesInstallment = new list<feeInstallmentDetail>();
			if(installment.Related_Fees__c != null && installment.Related_Fees__c != ''){
				for(string lf:installment.Related_Fees__c.split('!#'))
					feesInstallment.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2])));
			}
		}
		return feesInstallment;
	}


	public class paymentValue{
		public integer payIndex{get; set;}
		public client_course_instalment_payment__c installmentPayment{get; set;}
		public string payPlanId{get; set;}
		public paymentValue(){}
		public paymentValue(integer depositIndex, client_course_instalment_payment__c installmentPayment, string payPlanId){
			this.payIndex = depositIndex;
			this.installmentPayment = installmentPayment;
			this.payPlanId = payPlanId;
		}
	}

	public list<paymentValue> listPayDetails{
		get{
			if(listPayDetails == null){
				/*if(installment.client_course_instalment_payments__r.size() > 0){
					for(client_course_instalment_payment__c ip:installment.client_course_instalment_payments__r)
						listPayDetails.add(new paymentValue(countNumberPaymentValues++, ip));
				}else */listPayDetails = new list<paymentValue>();
			}
			return listPayDetails;
		}
		set;
	}


	public decimal totalpaid{
		get{
			if(totalpaid == null)
				totalpaid = 0;
			return totalpaid;
		}
		set;
	}

	private decimal totalpaidAgencyCurrency = 0;

	public client_course_instalment_payment__c newPayDetails{
		get{
			if(newPayDetails == null && installment != null){
				newPayDetails = new client_course_instalment_payment__c();
				newPayDetails.client_course_instalment__c = installment.id;
			}
			return newPayDetails;
		}
		set;
	}

	public Boolean installmentSaved{get{if(installmentSaved == null) installmentSaved = true; return installmentSaved;} Set;}

	public Boolean isPDS{get{if(isPDS == null) isPDS = false; return isPDS;} Set;}
	public Boolean isPCS{get{if(isPCS == null) isPCS = false; return isPCS;} Set;}

	private integer countNumberPaymentValues = 0;

	public pageReference addPaymentValue(){

		if(installment.Instalment_Value__c >0  && newPayDetails.Value__c <= 0)
			newPayDetails.Value__c.addError('Value need to be bigger than 0');

		if(newPayDetails.Payment_Type__c != 'Pds' && newPayDetails.Payment_Type__c != 'Pcs' && newPayDetails.Payment_Type__c != 'offShore' && (newPayDetails.Value__c > installment.Instalment_Value__c || newPayDetails.Value__c > (installment.Instalment_Value__c - totalpaid))){
			newPayDetails.Value__c.addError('Value cannot be bigger than ' + (installment.Instalment_Value__c - totalpaid));
			return null;
		}


		if(newPayDetails.Payment_Type__c == 'Deposit'){
			if((clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c - clientDeposit.Deposit_Total_Refund__c) >= 0 && newPayDetails.Value__c <= (clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c - clientDeposit.Deposit_Total_Refund__c)){
				clientDeposit.Deposit_Total_Used__c += newPayDetails.Value__c;
				installment.client_course__r.Total_Paid_Deposit__c += newPayDetails.Value__c;
			}else {
				newPayDetails.Value__c.addError('Deposit Value Invalid');
				return null;
			}
		}

		if(newPayDetails.Payment_Type__c.startsWith('loan-')){
			if(clientLoan.Value_Remaining__c >= newPayDetails.Value__c)
				clientLoan.Value_Remaining__c -= newPayDetails.Value__c;
			else {
				newPayDetails.Value__c.addError('Loan value available is insufficient');
				return null;
			}
		}

		if(newPayDetails.Date_Paid__c > system.today()){
			newPayDetails.Date_Paid__c.addError('Date paid cannot be future date. ');
			return null;
		}

		if(!isMigration && newPayDetails.Date_Paid__c < Date.today().addDays(-14)){
			newPayDetails.Date_Paid__c.addError('Date paid cannot be lower than 2 weeks. ');
			return null;
		}

		if(newPayDetails.Payment_Type__c == 'School Credit'){
			if(courseCredit.Credit_Available__c > 0 && newPayDetails.Value__c <= courseCredit.Credit_Available__c){
				courseCredit.Credit_Available__c -= newPayDetails.Value__c;
				installment.client_course__r.Total_Paid_Credit__c += newPayDetails.Value__c;
			}else {
				newPayDetails.Value__c.addError('Credit Value Invalid');
				return null;
			}
		}


		newPayDetails.client__c = installment.client_course__r.Client__c;

		isPDS = false;
		isPCS = false;

		if(newPayDetails.Payment_Type__c == 'Pds'|| newPayDetails.Payment_Type__c == 'notContacted'|| newPayDetails.Payment_Type__c == 'Pcs'|| newPayDetails.Payment_Type__c == 'offShore'){
			listPayDetails.clear();
			listPayDetails.add(new paymentValue(countNumberPaymentValues++, newPayDetails, null));
			totalpaid = newPayDetails.Value__c;
			newPayDetails.Agency_Currency_Value__c = newPayDetails.Value__c;
			installment.client_course__r.Total_Paid__c += newPayDetails.Value__c;
			clientDeposit.Deposit_Total_Used__c = currentDepositUsed;

			if(newPayDetails.Payment_Type__c == 'notContacted'){
				installment.Client_not_Contacted__c = true;
				newPayDetails.Payment_Type__c = 'Pds'; // Change type to follow the PDS rules
			}


			if(newPayDetails.Payment_Type__c == 'Pds'){
				installment.isPDS__c = true;
				installment.isPFS__c = false;
				installment.isPCS__c = false;
				isPDS = true;
			}
			else if(newPayDetails.Payment_Type__c == 'Pcs'){
				installment.isPDS__c = false;
				installment.isPFS__c = false;
				installment.isPCS__c = true;
				isPCS = true;
			}
			/** Migration Rules **/
			else if(newPayDetails.Payment_Type__c == 'offShore'){
				hasOffShorePayment = true;
				installment.isPaidOffShore__c = true;
			}


		}else{

			//Relate deposit ID to the payment
			if(newPayDetails.Payment_Type__c=='deposit'){
				newPayDetails.Paid_From_Deposit__c = clientDeposit.id;
			}

			//Relate school credit ID to the payment
			if(newPayDetails.Payment_Type__c=='School Credit'){
				newPayDetails.Related_Credit__c = courseCredit.id;
			}

			boolean isSameTypeDate = false;
			for(integer i = 0; i < listPayDetails.size(); i++)
				if(listPayDetails[i].installmentPayment.Payment_Type__c == newPayDetails.Payment_Type__c && listPayDetails[i].installmentPayment.Date_Paid__c == newPayDetails.Date_Paid__c){

					listPayDetails[i].installmentPayment.Value__c += newPayDetails.Value__c;
					installment.client_course__r.Total_Paid__c += newPayDetails.Value__c;
					totalpaid += newPayDetails.Value__c;
					isSameTypeDate = true;

					//============	CONVERT PAYMENT TO AGENCY CURRENCY ===================================
					if(installment.Agency_Currency_Rate__c < 0)
						newPayDetails.Agency_Currency_Value__c = newPayDetails.Value__c / installment.Agency_Currency_Rate__c.setScale(6);
					else newPayDetails.Agency_Currency_Value__c = newPayDetails.Value__c * installment.Agency_Currency_Rate__c.setScale(6);

					if(installment.Instalment_Value__c - totalpaid == 0){
						newPayDetails.Agency_Currency_Value__c = installment.Agency_Currency_Value__c.setScale(2) - totalpaidAgencyCurrency;
					}
					listPayDetails[i].installmentPayment.Agency_Currency_Value__c += newPayDetails.Agency_Currency_Value__c.setScale(2);
					totalpaidAgencyCurrency += newPayDetails.Agency_Currency_Value__c.setScale(2);


					break;
				}

			if(!isSameTypeDate){
				string payPlanId;
				if(newPayDetails.Payment_Type__c.startsWith('loan-')){
					payPlanId = newPayDetails.Payment_Type__c.remove('loan-');
					newPayDetails.Payment_Type__c = 'Client Loan';
					newPayDetails.Payment_Plan__c = payPlanId;
				}
				listPayDetails.add(new paymentValue(countNumberPaymentValues++, newPayDetails, payPlanId));
				totalpaid += newPayDetails.Value__c;
				installment.client_course__r.Total_Paid__c += newPayDetails.Value__c;

				//============	CONVERT PAYMENT TO AGENCY CURRENCY ===================================
				if(installment.Agency_Currency_Rate__c < 0)
					newPayDetails.Agency_Currency_Value__c = newPayDetails.Value__c / installment.Agency_Currency_Rate__c.setScale(6);
				else newPayDetails.Agency_Currency_Value__c = newPayDetails.Value__c * installment.Agency_Currency_Rate__c.setScale(6);

				if(installment.Instalment_Value__c - totalpaid == 0){
					newPayDetails.Agency_Currency_Value__c = installment.Agency_Currency_Value__c.setScale(2) - totalpaidAgencyCurrency;
				}
				totalpaidAgencyCurrency += newPayDetails.Agency_Currency_Value__c.setScale(2);
			}
		}





		newPayDetails = new client_course_instalment_payment__c();
		newPayDetails.Date_Paid__c = System.today();
		newPayDetails.Payment_Type__c = '';
		newPayDetails.client_course_instalment__c = installment.id;



		installmentSaved = false;

		depositListType = null;
		getdepositListType();

		return null;
	}


	public pageReference removePaymentValue(){
		integer index = integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
		system.debug('index====>' + index);
		system.debug('listPayDetails.size()====>' + listPayDetails.size());


		for(integer i = 0; i < listPayDetails.size(); i++){
			if(index == listPayDetails[i].payIndex){
				if(listPayDetails[i].installmentPayment.Payment_Type__c == 'Pds'){
					isPDS = false;
					installment.Client_not_Contacted__c = false;
					installment.client_course__r.Total_Paid_PDS__c -= listPayDetails[i].installmentPayment.Value__c;
				}
				if(listPayDetails[i].installmentPayment.Payment_Type__c == 'Pcs'){
					isPCS = false;
					installment.client_course__r.Total_Paid_PDS__c -= listPayDetails[i].installmentPayment.Value__c;
				}

				system.debug('==>listPayDetails[i].installmentPayment: ' + listPayDetails[i].installmentPayment);
				totalpaid -= listPayDetails[i].installmentPayment.Value__c;
				totalpaidAgencyCurrency -= listPayDetails[i].installmentPayment.Agency_Currency_Value__c;

				if(listPayDetails[i].installmentPayment.Payment_Type__c == 'Deposit'){
					clientDeposit.Deposit_Total_Used__c -= listPayDetails[i].installmentPayment.Value__c;
					installment.client_course__r.Total_Paid_Deposit__c -= listPayDetails[i].installmentPayment.Value__c;
				}
				if(listPayDetails[i].installmentPayment.Payment_Type__c == 'School Credit'){
					courseCredit.Credit_Available__c += listPayDetails[i].installmentPayment.Value__c;
					installment.client_course__r.Total_Paid_Credit__c -= listPayDetails[i].installmentPayment.Value__c;
				}

				/** Migration Rules **/
				if(listPayDetails[i].installmentPayment.Payment_Type__c == 'offShore'){
					hasOffShorePayment = false;
					installment.isPaidOffShore__c = false;
				}
				System.debug('==>listPayDetails[i]: '+listPayDetails[i]);
				System.debug('==>listPayDetails[i].payPlanId: '+listPayDetails[i].payPlanId);
				if(listPayDetails[i].payPlanId != null && mapLoan.containsKey(listPayDetails[i].payPlanId))
					mapLoan.get(listPayDetails[i].payPlanId).Value_Remaining__c += listPayDetails[i].installmentPayment.Value__c;

				system.debug('installment.client_course__r.Total_Paid__c' + installment.client_course__r.Total_Paid__c);
				system.debug('newPayDetails.Value__c' + newPayDetails.Value__c);

				installment.client_course__r.Total_Paid__c -= listPayDetails[i].installmentPayment.Value__c;
				listPayDetails.remove(i);
				break;
			}
		}//end for
		installmentSaved = false;
		depositListType = null;
		getdepositListType();

		return null;
	}

	IPFunctions.SearchNoSharing searchNoSharing = new IPFunctions.SearchNoSharing();
   	private Account taxDetails;

	private void retrieveTaxRate(){
		String sql = 'Select Id, Parent.name, Name, Tax_Name__c, Tax_Rate__c from Account WHERE RecordType.name = \'city\' and name = \'' + installment.client_course__r.Course_City__c + '\'  and Parent.name = \'' + installment.client_course__r.Course_Country__c + '\' limit 1';

		if(installment.client_course__r.Course_Country__c == fin.currentUser.Contact.Account.billingCountry && !installment.client_course__r.Campus_Course__r.Campus__r.Parent.isTaxDeductible__c){ //used to avoid offshore enrolments to claim tax and schools that do not pay tax to agency
			try{
				taxDetails = searchNoSharing.NSAccount(sql);

				if(taxDetails.Tax_Rate__c== null)
					taxDetails.Tax_Rate__c= 0;

			}catch(Exception e){
				//No tax found
				taxDetails = new Account(Tax_Name__c = '', Tax_Rate__c= 0);
			}
		}else{
			taxDetails = searchNoSharing.NSAccount(sql);
			taxDetails = new Account(Tax_Name__c = taxDetails.Tax_Name__c, Tax_Rate__c= 0);
		}

	}

	private Boolean isGeneralUser {get;set;}
	public pageReference savePayment(){
		Savepoint sp = Database.setSavepoint();
		//try{

			errorMsg = '';

		 	/** Double Check if Installment is paid **/
		 	Client_Course_Instalment__c updatedInstallment = [Select Received_Date__c, Received_by__r.Name from Client_Course_Instalment__c where id = :installment.id limit 1];

			if(updatedInstallment.Received_Date__c!=null){
				errorMsg = 'This installment has been already paid by ' +updatedInstallment.Received_by__r.Name + ' on ' + updatedInstallment.Received_Date__c.format() + '. Please refresh your page.';
				return null;
			}

			/** Migration Rules **/
			if(hasOffShorePayment){

		        if(selectedUser == null || selectedUser =='noUser'){
		            errorMsg += '<span style="display:block">Please fill the field <b>Payment Received By</b>.</span>';
		        }

		        if(receivedOn.Expected_Travel_Date__c == null){
		            errorMsg += '<span style="display:block">Please fill the field <b>Payment Received On</b>.</span>';
		        }

		        if(receivedOn.Expected_Travel_Date__c > system.today()){
		        	errorMsg += '<span style="display:block"><b>Payment Received On</b> cannot be future date.</span>';
				}
			}

			if(errorMsg.length()>0){

				return null;
			}

			else{

				if(clientDeposit != null && clientDeposit.id != null && currentDepositUsed != null && currentDepositUsed != clientDeposit.Deposit_Total_Value__c){
					if(clientDeposit.Deposit_Total_Refund__c==null) clientDeposit.Deposit_Total_Refund__c = 0;

					clientDeposit.Deposit_Total_Available__c = clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c - clientDeposit.Deposit_Total_Refund__c;
					update clientDeposit;
				}

				if(courseCredit != null && currentCourseCredit != courseCredit.Credit_Available__c)
					update courseCredit;

				list<client_course_instalment_payment__c> paymentList = new list<client_course_instalment_payment__c>();

				boolean isPDS = false;
				boolean isPCS = false;

				String receivedByAgency;
				String receivedByDepartment;
				
				isGeneralUser = FinanceFunctions.isGeneralUser(fin.currentUser.General_User_Agencies__c, installment.Client_course__r.Client__r.Owner__r.AccountId);
				
				if(!isGeneralUser){
					receivedByAgency = fin.currentUser.Contact.AccountId;
					receivedByDepartment = fin.currentUser.Contact.Department__c;
				}
				else{
					receivedByAgency = installment.Client_course__r.Client__r.Owner__r.AccountId;
					receivedByDepartment = installment.Client_course__r.Client__r.Owner__r.Contact.Department__c;
				}

				boolean isReconciled = true; //Used to automatically reconcile Third Party Payments 

				for(paymentValue lpd:listPayDetails){

					lpd.installmentPayment.Received_By__c = UserInfo.getUserId();
					lpd.installmentPayment.Received_On__c = DateTime.now();
					lpd.installmentPayment.Received_By_Agency__c = receivedByAgency;


					if(lpd.installmentPayment.Payment_Type__c.toLowerCase() != 'payment 3rd party')
						isReconciled = false;
					else{
						lpd.installmentPayment.Confirmed_By__c = UserInfo.getUserId();
						lpd.installmentPayment.Confirmed_Date__c = DateTime.now();
						lpd.installmentPayment.Confirmed_By_Agency__c = receivedByAgency;
					}

					paymentList.add(lpd.installmentPayment);

					if(lpd.installmentPayment.Payment_Type__c.toLowerCase() == 'covered by agency')
						installment.Covered_By_Agency__c = lpd.installmentPayment.Value__c;

					else if(lpd.installmentPayment.Payment_Type__c.toLowerCase() == 'pds'){
						installment.client_course__r.Total_Paid__c -= installment.Client_Scholarship__c;
						isPDS = true;
					}

					else if(lpd.installmentPayment.Payment_Type__c.toLowerCase() == 'pcs'){
						installment.client_course__r.Total_Paid__c -= installment.Client_Scholarship__c;
						isPCS = true;
					}

					else if(lpd.installmentPayment.Payment_Type__c.toLowerCase() == 'school credit')
						installment.Total_Paid_School_Credit__c += lpd.installmentPayment.Value__c;

				}//end for

				insert paymentList;

				if(isReconciled){
					installment.Confirmed_By__c = Userinfo.getuserId();
					installment.Confirmed_Date__c = System.now();
				}

				//Create Client Document for Installment Receipt
				if(!isMigration && !isPDS && !isPCS && (installment.isFirstPayment__c || installment.Required_for_COE__c)){
					Client_Document__c receiptDoc = new Client_Document__c (Client__c = installment.client_course__r.client__c, Document_Category__c = 'Finance', Document_Type__c = 'School Installment Receipt',Client_course__c = installment.client_course__c, client_course_instalment__c = installment.id);
					
					insert receiptDoc;

					installment.Client_Document__c = receiptDoc.id;
				}

				/** Migration Rules **/
				if(!hasOffShorePayment){
					installment.Received_By_Agency__c = receivedByAgency;
					installment.Received_by__c = Userinfo.getUserId();
					installment.Received_by_Department__c = receivedByDepartment;
					installment.Received_Date__c = System.now();
				}else{
					installment.Received_By_Agency__c = selectedAgency;
					installment.Received_by__c = selectedUser;
					installment.Received_by_Department__c = userDept.get(selectedUser);

					installment.Received_Date__c = DateTime.newInstance(receivedOn.Expected_Travel_Date__c.year(), receivedOn.Expected_Travel_Date__c.month(), receivedOn.Expected_Travel_Date__c.day(), 0, 0, 0);

					// if(installment.client_course__r.Client__r.Current_agency__r.required_pay_intall_to_agency__c && installment.client_course__r.Client__r.Current_agency__c != selectedAgency){
					// 	installment.pay_to_agency__c = installment.client_course__r.Client__r.Current_agency__c;
					// }

					if(!isMigration){
						if(installment.client_course__r.Client__r.Current_agency__r.BillingCountry != fin.currentUser.Contact.Account.billingCountry)
							installment.pay_to_agency__c = installment.client_course__r.Client__r.Current_agency__c;
					}
				}

				installment.Status__c = 'Paid';

				/** Create COE Client Document **/
				if((isPDS || isPCS) && installment.isFirstPayment__c){
					list<Client_Document__c> listDoc = new 	list<Client_Document__c>();

					listDoc.add(new Client_Document__c (Client__c = installment.client_course__r.client__c, Document_Category__c = 'Enrolment', Document_Type__c = 'COE', Client_course__c = installment.client_course__c));

					System.debug('==>installment.client_course__r.isPackage__c: '+installment.client_course__r.isPackage__c);

					if(installment.client_course__r.isPackage__c){
						System.debug('==>installment.Client_course__c: '+installment.Client_course__c);
						for(Client_Course_Instalment__c pk:[Select id, client_course__c FROM Client_Course_Instalment__c WHERE client_course__r.course_package__c = :installment.Client_course__c and Number__c= 1])
							listDoc.add(new Client_Document__c (Client__c = installment.client_course__r.client__c, Document_Category__c = 'Enrolment', Document_Type__c = 'COE', Client_course__c = pk.client_course__c));
					}
					insert listDoc;
				}

				/** Migration Rules **/
				if(isMigration)
					installment.isMigrated__c = true;

				String typePay;
				if(installment.isPDS__c){
					typePay = 'Marked as PDS';
				}
				else if(installment.isPCS__c)
					typePay = 'Marked as PCS';
				else typePay = 'Paid';

				if(installment.instalment_activities__c != null)
					installment.instalment_activities__c += typePay + ';;-;;' + '-' +';;' + System.now().format('yyyy-MM-dd HH:mm:ss') + ';;-;;-;;' + UserInfo.getName() + '!#!';
				else installment.instalment_activities__c = typePay +';;-;;' + '-' +';;' + System.now().format('yyyy-MM-dd HH:mm:ss') + ';;-;;-;;' + UserInfo.getName() + '!#!';


				if(installment.client_course__r.Enroled_by_Agency__c!= null && (installment.client_course__r.Enroled_by_Agency__c != installment.Received_By_Agency__c))
					installment.isSharedCommission__c = true;
				//

				// if(installment.isPCS__c || installment.isPDS__c){
				// 	//Commission Due Date
				// 	Date commissionDate;
				// 	if(installment.Due_Date__c <= installment.client_course__r.Start_Date__c){
				// 			commissionDate = installment.client_course__r.Start_Date__c;
				// 			commissionDate = commissionDate.addDays(7);
				// 	}
				// 	else{
				// 			commissionDate = system.today();
				// 			commissionDate = commissionDate.addDays(7);
				// 	}
				// 	installment.Commission_Due_Date__c = commissionDate;
				// 	system.debug('installment.Commission_Due_Date__c===>>'+ installment.Commission_Due_Date__c);
				// }

				System.debug('==>installment.Commission_Tax_Rate__c: '+installment.Commission_Tax_Rate__c);
				System.debug('==>installment.client_course__r.Course_Country__c: '+installment.client_course__r.Course_Country__c);
				System.debug('==>fin.currentUser.Contact.Account.billingCountry: '+fin.currentUser.Contact.Account.billingCountry);
				System.debug('==>installment.client_course__r.Campus_Course__r.Campus__r.Parent.isTaxDeductible__c: '+installment.client_course__r.Campus_Course__r.Campus__r.Parent.isTaxDeductible__c);
				
				if((installment.Commission_Tax_Rate__c != null && installment.Commission_Tax_Rate__c > 0 && installment.client_course__r.Course_Country__c != fin.currentUser.Contact.Account.billingCountry) 
					|| installment.client_course__r.Campus_Course__r.Campus__r.Parent.isTaxDeductible__c){ //used to set currency rate for courses enrolled destination with with payment offshore to tax rate = 0
					installment.Commission_Tax_Rate__c = 0;
				} 
				else if((installment.Commission_Tax_Rate__c == null || installment.Commission_Tax_Rate__c == 0) && installment.client_course__r.Course_Country__c == fin.currentUser.Contact.Account.billingCountry){ //used to set currency rate for courses enrolled offshore with no tax rate
					retrieveTaxRate();
					installment.Commission_Tax_Rate__c = taxDetails.Tax_Rate__c;
					if(isGeneralUser && installment.client_course__r.Client__r.Owner__r.Contact.Account.BillingCountry != installment.client_course__r.Course_Country__c)
						installment.isPaidOffShore__c = true;
				}

				

				if(installment.Discount__c!=null && installment.Discount__c>0)
					installment.client_course__r.Total_Paid__c += installment.Discount__c;


				if(installment.isPDS__c){
					installment.client_course__r.Total_Paid_PDS__c += installment.Instalment_Value__c;
					installment.client_course__r.Total_PDS_Commission_Pending__c += installment.Commission_Value__c;
				}

				update installment.client_course__r;

				//Get Commission from Agreement in case of payments before the enrolment and not migrated
				system.debug('findCommission==>' + findCommission);
				if(findCommission){
					client_course_new.optionLists res = new client_course_new.optionLists();

					client_course_new.validCommission vc = res.commissionValue(installment.client_course__c, installment.client_course__r.Course_Type__c, installment.client_course__r.Campus_Id__c, installment.client_course__r.School_Id__c, installment.Received_By_Agency__c, fin.currentUser);

					installment.Commission__c = vc.value;

					if(!installment.isPDS__c && !installment.isPCS__c){

						if(vc.pfsSchool == 'PFS Only')
							installment.isPFS__c = true;
						else if(vc.pfsSchool == 'PFS First Instalment' && installment.Number__c == 1 && (installment.Split_Number__c == null || installment.Split_Number__c == 1))
							installment.isPFS__c = true;
					}

					system.debug('installment.Commission__c==>' + installment.Commission__c);
					system.debug('installment.isPFS__c==>' + installment.isPFS__c);
				}
				update installment;

				// UPDATE PAYMENT PAN - CLIENT LOAN
				if(mapLoan.size() > 0){
					list<Payment_Plan__c> listPlanUpdate = new list<Payment_Plan__c>();
					map<string, Payment_Plan__c> mapLoanCompare = new map<string, Payment_Plan__c>([Select id, Value_Remaining__c from Payment_Plan__c where id in :mapLoan.keySet()]);
					for(string mk:mapLoan.keySet()){
						if(mapLoan.get(mk).Value_Remaining__c != mapLoanCompare.get(mk).Value_Remaining__c)
							listPlanUpdate.add(mapLoan.get(mk));
					}
					if(listPlanUpdate.size() > 0)
						update listPlanUpdate;
				}				

				//Update course and instalments
				client_course_new clientCourseNew = new client_course_new(installment.client_course__r.Client__c, installment.client_course__c);
				clientCourseNew.updateCourseInstalmentsValue(clientCourseNew.enrollCourse);

			//}catch(Exception e){
			//	Database.rollback(sp);
			//}
			showError = true;
			return null;
			}
	}

	public pageReference cancelPayment(){

		return null;
	}


	/************************************** MIGRATION METHODS **************************************/

	public boolean hasOffShorePayment {get{if(hasOffShorePayment==null) hasOffShorePayment = false; return hasOffShorePayment;}set;}
	public String errorMsg {get{if(errorMsg == null) errorMsg = ''; return errorMsg;}set;}
	public boolean showError {get{if(showError==null) showError = false; return showError;}set;}

   	private boolean isMigration {get;set;}
   	// private boolean isMigration {get
	// 	{
	// 		if(isMigration==null){
	// 			if(ApexPages.currentPage().getParameters().get('mi')=='true')
	// 				isMigration = true;
	// 			else
	// 				isMigration = false;
	// 		}
	// 		return isMigration;
	// 	}set;}

	public Contact receivedOn {get
		{
		if(receivedOn == null){
			receivedOn = new Contact();
		}return receivedOn;}set; }


    public String selectedAgencyGroup {get{if(selectedAgencyGroup==null) selectedAgencyGroup = fin.currentUser.Contact.Account.ParentId; return selectedAgencyGroup;}set;}
    public List<SelectOption> agencyGroupOptions {
		get{
			if(agencyGroupOptions == null){
				agencyGroupOptions = agencyNoSharing.getAllGroups();
			}
			return agencyGroupOptions;
		}
		set;
	}

	public void changeAgencyGroup(){
		selectedAgency = 'noAgency';
		agencyOptions = null;
		selectedUser = 'noUser';
		userOptions = null;
	}

	public void changeAgency(){
		selectedUser = 'noUser';
		userOptions = null;
	}

	private IPFunctions.agencyNoSharing agencyNoSharing {get{if(agencyNoSharing == null)agencyNoSharing = new IPFunctions.agencyNoSharing();return agencyNoSharing;}set;}
	public String selectedAgency {get{if(selectedAgency==null) selectedAgency = 'noAgency'; return selectedAgency;}set;}
	public List<SelectOption> agencyOptions {
		get{
			if(agencyOptions == null){
				 agencyOptions = agencyNoSharing.getAgenciesByParentNoSharing(selectedAgencyGroup);
			}
			return agencyOptions;
		}
		set;
	}

	public String selectedUser {get{if(selectedUser==null) selectedUser = 'noUser'; return selectedUser;}set;}
	private map<string,string> userDept {get;set;}
	public List<SelectOption> userOptions {
		get{
			if(userOptions == null){
				userDept = new map<string,string>();
				userOptions = new List<SelectOption>();
				userOptions.add(new SelectOption('noUser', '-- Select User --'));
				if(selectedAgency!=null && selectedAgency!='')
					for(User ac : [SELECT id, Name, Contact.Department__c FROM User WHERE Contact.AccountId = :selectedAgency and IsActive = true and Contact.Chatter_Only__c = false order by name]){
						userOptions.add(new SelectOption(ac.Id, ac.Name));
						userDept.put(ac.Id, ac.Contact.Department__c);
					}
			}
			return userOptions;
		}
		set;
	}


	//================CURRENCY=============================
	public List<SelectOption> MainCurrencies{get; set;}
	public Datetime currencyLastModifiedDate {get;set;}
	public void retrieveMainCurrencies(){

	    MainCurrencies = fin.retrieveMainCurrencies();

        currencyLastModifiedDate = fin.currencyLastModifiedDate;

	}
	private financeFunctions fin = new financeFunctions();
	public void recalculateRate(){
		system.debug('==>installment.client_course__r.CurrencyIsoCode__c: '+installment.client_course__r.CurrencyIsoCode__c);
		system.debug('==>installment: '+installment);
		fin.convertCurrency(installment, installment.client_course__r.CurrencyIsoCode__c, installment.Instalment_Value__c, null, 'Agency_Currency__c', 'Agency_Currency_Rate__c', 'Agency_Currency_Value__c');

	}

	// =================== PAYMENT PLAN =====================================	
	public Payment_Plan__c clientLoan{get; set;}
	public void openLoan(){
		string loanId = ApexPages.currentPage().getParameters().get('loanId');
		System.debug('==>loanId: '+loanId);
		// clientLoan = [Select id, Value__c, Value_Remaining__c from Payment_Plan__c where id = :loanId];
		if(mapLoan.containsKey(loanId)){
			clientLoan = mapLoan.get(loanId);
			if(clientLoan.Value_Remaining__c >= installment.Instalment_Value__c - totalpaid){
				newPayDetails.Value__c = installment.Instalment_Value__c - totalpaid;
			}else if(clientLoan.Value_Remaining__c > 0){
				newPayDetails.Value__c = clientLoan.Value_Remaining__c;
			} else newPayDetails.Value__c = 0;
		}

	}

}