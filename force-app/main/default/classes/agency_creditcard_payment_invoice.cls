public with sharing class agency_creditcard_payment_invoice {
    public Invoice__c invoice {get;set;}
    public list<client_product_service__c> products {get;set;}
    
    public agency_creditcard_payment_invoice() {
        try{
            invoice = [SELECT Creditcard_Payment_Number__c, Paid_by__r.Contact.Account.Name, Paid_Date__c, CurrencyIsoCode__c FROM Invoice__c WHERE Id = :ApexPages.currentPage().getParameters().get('id') AND isCreditCard_payment__c = TRUE limit 1];

            products = [SELECT Products_Services__r.Provider__r.Provider_Name__c, Creditcard_paid_on__c, Client__r.name, Creditcard_debit_date__c, Received_By_Department__r.name, Price_Total__c, Quantity__c, Received_By_Agency__r.name, Received_by__r.name, Product_Name__c, Currency__c, Agency_Currency_Value__c, Agency_Currency__c, Agency_Currency_Rate__c, (SELECT Product_Name__c, Price_Total__c, Agency_Currency_Value__c FROM paid_products__r) FROM client_product_service__c WHERE Creditcard_Invoice__c = :invoice.id AND Paid_with_product__c = NULL order by Product_Name__c, Client__r.name];
        }
        catch(Exception e){

            invoice = new Invoice__c();
            products = new list<client_product_service__c>();

            system.debug('Error===' + e);
			system.debug('Error Line ===' + e.getLineNumber());
        }
    }
}