public without sharing class payment_school_request_confirmation {

	private String invoiceID {get;set;}
	public boolean hasExtraFee {get;set;}
	public boolean hasKeepFee {get;set;}
	public boolean showResults {get;set;}
	public Account agencyDetails {get;set;}
  public String invNumber {get;set;}
	public String invDueDate {get;set;}
	public Invoice__c invoice {get;set;}
	public list<Result> instResult {get;set;}
	public Account campusDetails {get;set;}
	private String schoolId {get;set;}
	//Constructor
	public payment_school_request_confirmation() {
		invoiceID = ApexPages.currentPage().getParameters().get('i');
		system.debug('invoiceID===>' + invoiceID);
		User currentUser;
		try{
			list<client_course_instalment__c> result;
			if(invoiceID!=null && invoiceID!=''){

				invoice = [SELECT Request_Confirmation_Number__c, Due_Date__c, createdDate, Sent_By__r.Contact.AccountId, Sent_Email_To__c, Request_Confirmation_Completed_On__c, Instalments__c, School_Id__c
					FROM Invoice__c WHERE id = :invoiceID];

				if(invoice.Request_Confirmation_Completed_On__c != null || ApexPages.currentPage().getParameters().get('result') == 'show')
					showResults = true;
				else
					showResults = false;
				schoolId = invoice.School_Id__c;
				result = findInvInstalments(invoice.Instalments__c.split(';'));
			}
			else{

				currentUser = [SELECT Contact.AccountId, Contact.PDS_to_Request__c, Contact.PDS_School_To_Request__c FROM user WHERE id = :userInfo.getUserId()];

				list<String> ids = currentUser.Contact.PDS_to_Request__c.split(';');
				schoolId = currentUser.Contact.PDS_School_To_Request__c;
				result = findInvInstalments(ids);
			}


			buildResult(result);
		}
		catch(Exception e ){
			instResult = new list<Result>();
			system.debug('Error1==>' + e.getMessage());
			system.debug('Line==>' + e.getLineNumber());
		}

		try{
			if(invoiceID!=null && invoiceID!=''){
				agencyDetails = [SELECT Id, Name, Logo__c, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode, Registration_name__c, Trading_Name__c,
				 												Business_Number__c, Type_of_Business_Number__c

																FROM Account WHERE Id = :invoice.Sent_By__r.Contact.AccountId limit 1];
			}
			else{
				agencyDetails = [SELECT Id, Name, Logo__c, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode, Registration_name__c, Trading_Name__c,
				 												Business_Number__c, Type_of_Business_Number__c

																FROM Account WHERE Id = :currentUser.Contact.AccountId limit 1];
			}

		}catch(Exception e){
			agencyDetails = new Account();
		}


		try{

			IPFunctions.SearchNoSharing NS = new IPFunctions.SearchNoSharing();
			campusDetails = ns.NSAccount('SELECT Id, Name, Parent.Name, Registration_Name__c, Type_of_Business_Number__c, Business_Number__c, '+
											' (SELECT BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode FROM ChildAccounts WHERE Main_Campus__c = true order by createdDate desc limit 1) ' +
												' From Account where Id= \''+ schoolId + '\' limit 1 ');
		}catch(Exception e){
			campusDetails = new Account();
		}
	}

	//Get Invoice Instalments
	public list<client_course_instalment__c> findInvInstalments(list<String> ids){

		String sql = 'SELECT id, Name, Instalment_Value__c, Due_Date__c, Commission_Due_Date__c, Related_Fees__c, Scholarship_Taken__c, Commission__c,  Commission_Value__c, Tuition_Value__c, '+
									'Received_By_Agency__c, Received_By_Agency__r.Name, Received_By__r.Name, Total_School_Payment__c, Number__c, Commission_Tax_Value__c, Commission_Tax_Rate__c, Received_Date__c, '+
									'Split_Number__c, Agent_Deal_Name__c, Agent_Deal_Value__c,Related_Promotions__c, School_Invoice_Number__c, Paid_PDS_Value__c,'+
									'School_Request_Confirmation_Status__c, School_PDS_Notes__c,'+

										'isInstalment_Amendment__c, Amendment_Related_Fees__c,'+

										'client_course__r.Commission_Tax_Name__c,'+
										'client_course__r.CurrencyIsoCode__c,'+
										'client_course__r.Commission_Type__c,'+
										'client_course__r.Start_Date__c,'+
										'client_course__r.client__c,'+
										'client_course__r.School_Student_Number__c,'+
										'client_course__r.client__r.Name,'+
										'client_course__r.client__r.Birthdate,'+
										'client_course__r.Course_Name__c,'+
										'client_course__r.Campus_Id__c,'+
										'client_course__r.Campus_Name__c '+

										'FROM client_course_instalment__c ';

		// if(hasInvoice)
		// 	sql += 'WHERE PDS_Confirmation_Invoice__c = :invoiceID ';
		// else{
			sql += 'WHERE id in :ids ';
		// }

		sql += 'order by client_course__r.Campus_Name__c, client_course__r.Course_Name__c, client_course__r.Client__r.Name, Due_Date__c';

		system.debug('sql===>' + sql);
		return Database.query(sql);
	}

	//Build Result List
	private void buildResult(list<client_course_instalment__c> result){
		instResult = new list<Result>();

		Instalment i;
		courseDetails course;
		map<string, result> resultMap = new map<string,result>();
		for(client_course_instalment__c cci : result){
			i = new instalment();
			i.inst = cci;

			/** F E E S **/
			if(cci.Related_Fees__c!=null && cci.Related_Fees__c!=''){
				for(string lf:cci.Related_Fees__c.split('!#')){

					/** Original Fees  **/
					if(lf.split(';;').size() >= 7){ //It's amended

						i.fees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[6]), false,  decimal.valueOf(lf.split(';;')[7])));

						if(decimal.valueOf(lf.split(';;')[7])>0){ // Has keep fee
							i.KeepFees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[6]), false,  decimal.valueOf(lf.split(';;')[7])));
							i.totalKeepFee += decimal.valueOf(lf.split(';;')[7]);
						}

						else if(decimal.valueOf(lf.split(';;')[7])<0){ // Decreased keep fee
							i.KeepFees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[6]), false,  decimal.valueOf(lf.split(';;')[7]) + decimal.valueOf(lf.split(';;')[5])));
							i.totalKeepFee += decimal.valueOf(lf.split(';;')[7]);
						}
							i.totalFees +=	decimal.valueOf(lf.split(';;')[6]);
					}
					else {
						i.fees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2]), false,  decimal.valueOf(lf.split(';;')[5])));
						i.totalFees +=	decimal.valueOf(lf.split(';;')[2]);

						if(decimal.valueOf(lf.split(';;')[5])>0){ // Has keep fee
							i.KeepFees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2]), false,  decimal.valueOf(lf.split(';;')[5])));
							i.totalKeepFee += decimal.valueOf(lf.split(';;')[5]);
						}
					}
				}//end for
			}

			/** Amended Fees **/
			if(cci.Amendment_Related_Fees__c!=null && cci.Amendment_Related_Fees__c!=''){
				for(string lf:cci.Amendment_Related_Fees__c.split('!#')){

					i.fees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2]), false,  decimal.valueOf(lf.split(';;')[5])));
					i.totalFees +=	decimal.valueOf(lf.split(';;')[2]);
					if(decimal.valueOf(lf.split(';;')[5])>0){ // Has keep fee
						i.KeepFees.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2]), false,  decimal.valueOf(lf.split(';;')[5])));
						i.totalKeepFee += decimal.valueOf(lf.split(';;')[5]);
					}
				}//end for
			}

			//Flag to show Fees Columns
			if(i.fees.size()>0)
				hasExtraFee = true;
			if(i.KeepFees.size()>0)
				hasKeepFee = true;

			//Group by Campus and Course
			if(!resultMap.containsKey(cci.client_course__r.Campus_Id__c)){ //New Campus
				course = new courseDetails(cci.client_course__r.Course_Name__c, i);
				resultMap.put(cci.Client_Course__r.Campus_Id__c, new result(cci.Client_Course__r.Campus_Id__c, cci.client_course__r.Campus_Name__c,
																																		cci.client_course__r.Course_Name__c,course));
			}
			else{
				result res = resultMap.get(cci.Client_Course__r.Campus_Id__c);

				if(!res.mapCourses.containsKey(cci.client_course__r.Course_Name__c)){ //New Course
					res.mapCourses.put(cci.client_course__r.Course_Name__c, new courseDetails(cci.client_course__r.Course_Name__c, i));
				}
				else{ //Add Instalment to Course
					res.mapCourses.get(cci.client_course__r.Course_Name__c).addInstalment(i);
				}
				resultMap.put(res.campusId, res);

			}

		}//end for

		result rm;
		for(String cp : resultMap.keySet()){
			rm = resultMap.get(cp);
			rm.setListCourses();

			instResult.add(rm);
		}//end for

	}


	/********************** Inner Classes **********************/

	//Result by Campus
	public class result{
		public String campusId {get;set;}
		public String campusName {get;set;}
		public list<courseDetails> courses {get;set;}
		private map<string, courseDetails> mapCourses {get{if(mapCourses==null) mapCourses = new map<string,courseDetails>(); return mapCourses;}set;}

		private result(String campusId, String campusName,String courseName, courseDetails course){
			this.campusId = campusId;
			this.campusName = campusName;
			this.mapCourses.put(courseName, course);
		}

		private void setListCourses(){
			courses = new list<courseDetails>();

			for(String c: mapCourses.keySet())
				courses.add(mapCourses.get(c));
		}

	}

	//Course Details
	public class courseDetails{
		public String courseName {get;set;}
		public list<Instalment> instalments {get{if(instalments == null) instalments = new list<Instalment>(); return instalments;}set;}

		private courseDetails(String courseName, Instalment inst){
			this.courseName = courseName;
			this.instalments.add(inst);
		}

		private void addInstalment(Instalment inst){
			this.instalments.add(inst);
		}
	}

	//Instalment Details
	public class Instalment{
		public client_course_instalment__c inst {get;set;}
		public list<feeInstallmentDetail> fees {get{if(fees == null) fees = new list<feeInstallmentDetail>(); return fees;}set;}
		public list<feeInstallmentDetail> keepFees {get{if(keepFees == null) keepFees = new list<feeInstallmentDetail>(); return keepFees;}set;}
		public decimal totalFees {get{if(totalFees==null) totalFees = 0; return totalFees;}set;}
		public decimal totalKeepFee {get{if(totalKeepFee==null) totalKeepFee = 0; return totalKeepFee;}set;}
	}

	//Fees Details
	public class feeInstallmentDetail{
		public string feeName{get; set;}
		public decimal feeValue{get; set;}
		public boolean isPromotion{get; set;}
		public decimal keepFeeValue{get; set;}

		public feeInstallmentDetail(string feeName, decimal feeValue, boolean isPromotion, decimal keepFeeValue){
			this.feeName = feeName;
			this.feeValue = feeValue;
			this.isPromotion = isPromotion;
			this.keepFeeValue = keepFeeValue;
		}
	}


}