public without sharing class agency_commission_pds {

  private string ag {get;set;}
  private string controlId {get;set;}

  public boolean noErrors {get{if(noErrors == null) noErrors = false; return noErrors;}set;}

  public Account agencyDetails{get;set;}
  public Back_Office_Control__c boc {get;set;}
  public list<SelectOption> services {get;set;}
  public list<Back_Office_Control__c> otherBoc {get;set;}

  private map<String,String> allServices {get;set;}
  private  list<Back_Office_Control__c> allBackoffices {get;set;}
  private map<Id,Account> allAgencies {get;set;}

  //CONSTRUCTOR
  public agency_commission_pds(){
    controlId = ApexPages.currentPage().getParameters().get('id');
    ag = ApexPages.currentPage().getParameters().get('ag');


    //Get all Back Offices
    allBackoffices = [SELECT Id, Name, Country__c, Services__c, Back_Office_Name__c FROM Back_Office_Control__c WHERE Agency__c = :ag];

    //Get Services options
    allServices = new map<String,String>();
    Schema.DescribeFieldResult fieldResult = Back_Office_Control__c.Services__c.getDescribe();
    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

    for( Schema.PicklistEntry f : ple)
      allServices.put(f.getValue(), f.getLabel());

    //Add or Edit Back Office
    if(controlId == null)
      boc = new Back_Office_Control__c(Agency__c = ag);
    else{
      boc = [SELECT Country__c, Backoffice__c, Services__c FROM Back_Office_Control__c WHERE id = :controlId limit 1];
      // boc.Services__c = boc.Services__c.replace(';',',');
      changeCountry();
    }

  }


  public void changeAgency(){
    agencyDetails = allAgencies.get(boc.Backoffice__c);
    otherBoc = new list<Back_Office_Control__c>();
    boc.Country__c = null;
  }

  public void changeCountry(){

    services = new list<SelectOption>();
    otherBoc = new list<Back_Office_Control__c>();

    set<String> usedServices = new set<String>();

    if(boc.id!=null){
      for(Back_Office_Control__c bc : allBackoffices)
        if(bc.Country__c == boc.Country__c && bc.id != boc.id  && bc.Services__c != null){
          usedServices.addAll(bc.Services__c.replace('[','').replace(']','').split(';'));
          otherBoc.add(bc);
        }
    }
    else{
      boc.Services__c = null;
      for(Back_Office_Control__c bc : allBackoffices)
        if(bc.Country__c == boc.Country__c && bc.Services__c != null){
          System.debug('==>bc: '+bc);
          usedServices.addAll(bc.Services__c.replace('[','').replace(']','').split(';'));
          otherBoc.add(bc);
        }
    }


    for(String sv : allServices.keySet())
      if(!usedServices.contains(sv))
        services.add(new SelectOption(sv, allServices.get(sv)));
  }

    /********************** Filters **********************/

  public list<selectOption> countries{
    get{
        if(countries == null){

            countries = new list<selectOption>();
            for(AggregateResult bc:[Select BillingCountry country from Account where recordType.name = 'campus' and BillingCountry != null group by BillingCountry order by BillingCountry])
                countries.add(new selectOption((string)bc.get('country'), (string)bc.get('country')));
        }
        countries.sort();
        return countries;
    }
    set;
  }

   public list<selectOption> agencies{
    get{
        if(agencies == null){
            agencies = new list<selectOption>();
        allAgencies = new map<Id, Account>();
            agencies.add(new selectOption('', '-- Select Agency -- '));
            for(Account bc:[Select id, name, BillingCountry, BackOffice_Email__c from Account where recordType.name = 'agency' order by name]){
                agencies.add(new selectOption(bc.id,bc.name));
          allAgencies.put(bc.Id, bc);
            }//end for
        }
        return agencies;
    }
    set;
  }


  public void saveBackoffice(){
    boc.Services__c = boc.Services__c.replace(',',';').replace(']','').replace('[','');
    upsert boc;
    noErrors = true;
  }

   public void deleteBackoffice(){
     system.debug('deleting===>' + boc);
    delete boc;
  }
}