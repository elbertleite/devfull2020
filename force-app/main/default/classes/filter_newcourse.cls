global without sharing class filter_newcourse {


	public String courseId {get;set;}
	private Boolean isMigration {get;set;}
	private String bookingId {get;set;}
	private String clientId {get;set;}
	private String nc;
	private String pkg;

	public transient List<listOptions> listCountry{get; set;}
	public transient List<listOptions> listSchools{get; set;}
	public transient List<listOptions> listCampus{get; set;}
	public transient map<string,list<listOptions>> listCourses{get; set;}

	public string selectedCountry {get;set;}
	public string selectedSchool {get;set;}
	public string schoolGroup {get;set;}
	public string selectedCampus {get{if(selectedCampus==null) selectedCampus = 'all'; return selectedCampus;}set;}
	public string selectedCourse {get{if(selectedCourse==null) selectedCourse = 'all'; return selectedCourse;}set;}
	private map<string,String> isPackage {get;set;}
	public list<Client_course__c> allCourses {get;set;}

	public filter_newcourse(ApexPages.StandardController controller) {
		allCourses = new list<client_course__c>();
		clientId = controller.getId();
		isMigration = (ApexPages.currentPage().getParameters().get('mi') != null && ApexPages.currentPage().getParameters().get('mi') != '' ) ? boolean.valueOf(ApexPages.currentPage().getParameters().get('mi')) : false;
		bookingId = (ApexPages.currentPage().getParameters().get('bk') != null && ApexPages.currentPage().getParameters().get('bk') != '' ) ? ApexPages.currentPage().getParameters().get('bk') : null;
		pkg = (ApexPages.currentPage().getParameters().get('pk') != null && ApexPages.currentPage().getParameters().get('pk') != '' ) ? ApexPages.currentPage().getParameters().get('pk') : null;
		retrieveCourses();

		nc = ApexPages.currentPage().getParameters().get('nc');

		selectedCountry = null;
		selectedSchool = null;

		// Add course to package
		if(nc != NULL && nc != ''){
			client_course__c cc;

			String pkId = pkg != null ? pkg : nc;
			
			for(Client_course__c c : [SELECT Booking_Number__c, Client__c, Campus_Country__c, School_Id__c, School_Name__c, Course_Name__c, isMigrated__c, isPackage__c, course_package__c FROM Client_course__c WHERE id = :pkId OR course_package__c = :pkId order by course_package__c nulls first, Course_Name__c]){

				if(c.Id == nc)
					cc = c;

				allCourses.add(c);

			}//end for
			selectedCountry = cc.Campus_Country__c;


			schoolGroup = [SELECT ParentId FROM Account WHERE Id = :cc.School_Id__c].ParentId;

			if(!String.isEmpty(selectedCountry)){
				set<string> schools = new set<string>();
				listSchools = new List<listOptions>(); 
				for(Campus_Course__c ce:[SELECT campus__r.parentId, campus__r.parent.name FROM Campus_Course__c where campus__r.Parent.ParentId = :schoolGroup AND  campus__c != null AND Campus__r.billingCountry = :selectedCountry ORDER BY campus__r.parent.name]){
					if(!schools.contains(ce.campus__r.parentId)){
						schools.add(ce.campus__r.parentId);
						listSchools.add(new listOptions(ce.campus__r.parentId, ce.campus__r.parent.name));	
					}
				}
			}
			//selectedSchool = cc.School_Id__c + ';;' + cc.School_Name__c;
			selectedSchool = cc.School_Id__c;
			if(!String.isEmpty(selectedSchool)){
				set<string> campuses = new set<string>();
				listCampus = new List<listOptions>();  
				listCampus.add(new listOptions('none', '-- Select Campus --'));
				for(Campus_Course__c ce:[SELECT campus__c, campus__r.name, campus__r.Parent.ParentId FROM Campus_Course__c where campus__c != null AND campus__r.parentId = :selectedSchool ORDER BY campus__r.parent.name]){
					
					if(!campuses.contains(ce.campus__c)){
						listCampus.add(new listOptions(ce.campus__c, ce.campus__r.name));
						campuses.add(ce.campus__c);
					}	
				}
			}
			bookingId = cc.Booking_Number__c;
			clientId = cc.Client__c;
			isMigration = cc.isMigrated__c;
			
		}
	}

	public PageReference addCourse(){
		PageReference pr;

		if(courseId=='none'){
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'You must select a course.'));
			return null;
		}

		String newPageContact = ApexPages.currentPage().getParameters().get('page');

		if(isPackage.containsKey(courseId)){
			if(!String.isEmpty(newPageContact) && newPageContact == 'contactNewPage'){
				pr  = new PageReference('/apex/contact_new_add_packages');
            	pr.getParameters().put('page', 'contactNewPage');
            	pr.getParameters().put('id', clientId);
			}else{
				pr  = Page.client_course_add_packages;
			}
            pr.getParameters().put('cid', clientId);
            pr.getParameters().put('bk', bookingId);
			pr.getParameters().put('nc', nc);
            pr.getParameters().put('pk', pkg);
            pr.getParameters().put('cs', isPackage.get(courseId));
            pr.getParameters().put('mi',string.valueOf(isMigration));
            pr.setRedirect(true);
            
		}
		else{
			if(!String.isEmpty(newPageContact) && newPageContact == 'contactNewPage'){
				pr  = new PageReference('/apex/contact_new_bookings');
            	pr.getParameters().put('page', 'contactNewPage');
			}else{
				pr  = Page.client_course_new;
			}
            pr.getParameters().put('id', clientId);
            pr.getParameters().put('bk', bookingId);
            pr.getParameters().put('nc', nc);
            pr.getParameters().put('pk', pkg);
            pr.getParameters().put('cs', courseId);
            pr.getParameters().put('mi',string.valueOf(isMigration));
			pr.getParameters().put('addNew','true');
            pr.setRedirect(true);
		}
		
		return pr;
	}

	public class SelectOptionWrapper
	{        
		public String label { get; set; }
		public String value { get; set; }
		public boolean isDisabled { get; set; }

		public SelectOptionWrapper(SelectOption o)
		{
			label = o.getLabel();
			value = o.getValue();
			isDisabled = o.getDisabled();
		}
	}

	@RemoteAction
	global static String searchSchools(String countrySelected){
		list<SelectOptionWrapper> listSchools = new list<SelectOptionWrapper>();	
		set<string> schools = new set<string>();

		listSchools.add(new SelectOptionWrapper(new SelectOption('none', '-- Select School --')));	

		for(Campus_Course__c ce:[SELECT campus__r.parentId, campus__r.parent.name FROM Campus_Course__c where campus__c != null AND Campus__r.billingCountry = :countrySelected ORDER BY campus__r.parent.name]){
			if(!schools.contains(ce.campus__r.parentId)){
				listSchools.add(new SelectOptionWrapper(new SelectOption(ce.campus__r.parentId, ce.campus__r.parent.name)));
				schools.add(ce.campus__r.parentId);
			}	
		}//end for

		return JSON.serialize(listSchools);
	}
	@RemoteAction
	global static String searchCampuses(String schoolID){
		list<SelectOptionWrapper> listCampus = new list<SelectOptionWrapper>();	
		set<string> campuses = new set<string>();

		listCampus.add(new SelectOptionWrapper(new SelectOption('none', '-- Select Campus --')));	

		for(Campus_Course__c ce:[SELECT campus__c, campus__r.name FROM Campus_Course__c where campus__c != null AND campus__r.parentId = :schoolID ORDER BY campus__r.parent.name]){
			if(!campuses.contains(ce.campus__c)){
				listCampus.add(new SelectOptionWrapper(new SelectOption(ce.campus__c, ce.campus__r.name)));
				campuses.add(ce.campus__c);
			}	
		}

		return JSON.serialize(listCampus);
	}

	@RemoteAction
	global static String searchCourses(String campusId){

		list<SelectOptionWrapper> courseOpt = new list<SelectOptionWrapper>();
		set<String> cType = new set<String>();

		courseOpt.add(new SelectOptionWrapper(new SelectOption('none', '-- Select Course --')));	

		for(Campus_Course__c ce:[SELECT id, Course__r.name, course__r.School_Course_Name__c, course__r.Type__c, Course__r.Package__c, Package_Courses__c FROM Campus_Course__c where campus__c = :campusId Order by course__r.Type__c, Course__r.name]){

			if((!ce.Course__r.Package__c) || (ce.Course__r.Package__c && ce.Package_Courses__c != null)){

				if(!cType.contains(ce.course__r.Type__c) && ce.course__r.Type__c != null){
					cType.add(ce.course__r.Type__c);
					courseOpt.add(new SelectOptionWrapper(new SelectOption(ce.id, ce.course__r.Type__c, true)));	
				}

				String schCourseNm = ce.course__r.School_Course_Name__c != NULL ? ce.course__r.School_Course_Name__c : '';
				courseOpt.add(new SelectOptionWrapper(new SelectOption(ce.id, ce.Course__r.name + ' ::: ' + schCourseNm)));	
			}

		}//end for

		//return courseOpt;
		return JSON.serialize(courseOpt);
	}


	public class listOptions{
		public string optLabel{get; set;}
		public string optValue{get; set;}
		public listOptions(string optValue, string optLabel){
			this.optLabel = optLabel;
			this.optValue = optValue;
		}
	}

	

	public void retrieveCourses(){
		selectedCountry = 'all';
		selectedSchool = 'all';
		selectedCampus = 'all';

		listCountry = new List<listOptions>();
		listSchools = new List<listOptions>();
		listCampus = new List<listOptions>();
		listCourses = new map<string,list<listOptions>>();
		set<string> countries = new set<string>();
		set<string> schools = new set<string>();
		set<string> campuses = new set<string>();

		listSchools.add(new listOptions('', ''));
		listCampus.add(new listOptions('', ''));
		//listCourses.add(new listOptions('', ''));
		// stagesItems.add(new listOptions('all', '-- Select Stage Item --'));
		// destinationsCountry.add(new listOptions('all', '-- Select Destination --'));

		isPackage  = new map<string, String>();

		String countrySelected = null;
		String schoolSelected = null;

		for(Campus_Course__c ce : [SELECT Course__r.Package__c, Package_Courses__c FROM Campus_Course__c where campus__c != null AND Course__r.Package__c = true order by Campus__r.billingCountry,campus__r.parent.name, Campus__r.name, course__r.Type__c, Course__r.name]){
			if(ce.Course__r.Package__c && ce.Package_Courses__c != null){
				isPackage.put(ce.id, ce.Package_Courses__c);
			}
		}

		for(Campus_Course__c ce:[SELECT Campus__r.billingCountry FROM Campus_Course__c where campus__c != null order by Campus__r.billingCountry]){
			if(!countries.contains(ce.Campus__r.billingCountry)){
				if(countries.isEmpty()){
					countrySelected = ce.Campus__r.billingCountry;
				}
				listCountry.add(new listOptions(ce.Campus__r.billingCountry, ce.Campus__r.billingCountry));
				countries.add(ce.Campus__r.billingCountry);
			}
		}

		// Add single course
		if(!ApexPages.currentPage().getParameters().containsKey('nc')){

			for(Campus_Course__c ce: [SELECT campus__r.parentId, campus__r.parent.name FROM Campus_Course__c WHERE campus__c != null AND Campus__r.billingCountry = :countrySelected ORDER BY campus__r.parent.name]){
				if(!schools.contains(ce.campus__r.parentId)){
					if(schools.isEmpty()){
						schoolSelected = ce.campus__r.parentId;
					}
					listSchools.add(new listOptions(ce.campus__r.parentId, ce.campus__r.parent.name));
					schools.add(ce.campus__r.parentId);
				}
			}

			for(Campus_Course__c ce:[SELECT campus__c, campus__r.name FROM Campus_Course__c where campus__c != null AND campus__r.parentId = :schoolSelected ORDER BY campus__r.parent.name]){
				if(!campuses.contains(ce.campus__c)){
					listCampus.add(new listOptions(ce.campus__c, ce.campus__r.name));
					campuses.add(ce.campus__c);
				}
			}
		}

		
		/*for(Campus_Course__c ce:[SELECT id,Campus__r.billingCountry,campus__r.parentId, campus__r.parent.name, campus__c, Campus__r.name, Course__r.name, course__r.School_Course_Name__c, course__r.Type__c, Course__r.Package__c, Package_Courses__c FROM Campus_Course__c where  campus__c != null order by Campus__r.billingCountry,campus__r.parent.name, Campus__r.name, course__r.Type__c, Course__r.name]){

			if(ce.Course__r.Package__c && ce.Package_Courses__c != null)
				isPackage.put(ce.id, ce.Package_Courses__c);
			
			if(!countries.contains(ce.Campus__r.billingCountry)){
				listCountry.add(new listOptions(ce.Campus__r.billingCountry, ce.Campus__r.billingCountry));
				countries.add(ce.Campus__r.billingCountry);
			}

			if(!schools.contains(ce.campus__r.parentId)){
				//listSchools.add(new listOptions(ce.campus__r.parentId, ce.campus__r.parent.name));
				listSchools.add(new listOptions(ce.Campus__r.billingCountry, ce.campus__r.parentId +';;'+ce.campus__r.parent.name));
				schools.add(ce.campus__r.parentId);
				listCampus.add(new listOptions(ce.campus__r.parentId +';;'+ce.campus__r.parent.name, 'none;;-- Select Campus --'));
			}
			
			if(!campuses.contains(ce.campus__c)){
				listCampus.add(new listOptions(ce.campus__r.parentId +';;'+ce.campus__r.parent.name, ce.campus__c+';;'+ce.campus__r.name));
				campuses.add(ce.campus__c);
			}
			// //listCourses.add(new listOptions(ce.id, ce.Course__r.name));
			// String schCourseNm = ce.course__r.School_Course_Name__c != NULL ? ce.course__r.School_Course_Name__c : '';
			// // listOptions lto = new listOptions(ce.campus__c+';;'+ce.campus__r.name, ce.Course__c+';;'+ce.Course__r.name + ' ::: '+ schCourseNm);
			// listOptions lto = new listOptions(ce.campus__c+';;'+ce.campus__r.name, ce.id+';;'+ce.Course__r.name + ' ::: '+ schCourseNm);
			// if(!listCourses.containsKey(ce.campus__c))
			// 	listCourses.put(ce.campus__c, new list<listOptions>{lto});
			// else listCourses.get(ce.campus__c).add(lto);
		}*/

		System.debug('==>countries: '+JSON.serialize(listCountry));
		System.debug('==>listCampus: '+JSON.serialize(listCampus));
		// for(string st:countries)
		// 	listCountry.add(new listOptions(st, st));
	}
	
}