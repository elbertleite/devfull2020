/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Client_Movements_test {

    static testMethod void myUnitTest() {
		
		
	   TestFactory factory = new TestFactory();
        
       	Map<String,String> recordTypes = new Map<String,String>();       
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
		
		//Create Agency
		Account agency = factory.createAgency();
		
		//Create Employee 
		Contact employee = factory.createEmployee(agency); 	
		
		//Create user
		Profile portalProfile = [SELECT Id FROM Profile WHERE Name = 'Customer Community Agency' Limit 1];
		User userEmp = factory.createPortalUser(employee);
		// User userEmp = new User(
		// 	Username = System.now().millisecond() + 'test12345@test.com',
		// 	ContactId = employee.Id,
		// 	ProfileId = portalProfile.Id,
		// 	Alias = 'test123',
		// 	Email = 'test12345@test.com',
		// 	EmailEncodingKey = 'UTF-8',
		// 	LastName = 'McTesty',
		// 	CommunityNickname = 'test12345',
		// 	TimeZoneSidKey = 'America/Los_Angeles',
		// 	LocaleSidKey = 'en_US',
		// 	LanguageLocaleKey = 'en_US'
		// );
		//Database.insert(userEmp);
		  
        Test.startTest();
        Contact account = new Contact();
		account.RecordTypeId = recordTypes.get('Client');
		account.LastName = 'Nakamura';
		account.Full_Name__c = 'Bruno';
		account.MailingCity = 'Sydney';
		account.MailingCountry = 'AUS';
		account.MailingState = 'NSW';
		account.MailingStreet = 'Alexander ST';
		account.MailingPostalCode = '2010';
		//account.User__c = u.id;
		account.AccountID= agency.Id;
		account.Email = 'chananan@ip.com.ay';
		account.isLead__c = true;
		account.Lead_Skype__c = 'leadSkypeTest';
		account.MobilePhone = '04040005987';
		insert account;  
		
  		account.Owner__c = userEmp.Id;
		account.AccountID= agency.Id;
		update account;
	
	}
}