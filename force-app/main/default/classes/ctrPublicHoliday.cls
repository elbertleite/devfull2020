public class ctrPublicHoliday {
	Account acco;
	
	public ctrPublicHoliday(ApexPages.StandardController controller){
		
	}
	
	public String apexMsg{get;set;}
	
	public void clearState(){
		newAddress.State__c = null;
	}
	public List<Public_Holiday__c> holidayDesc{get;set;}
	public List<Public_Holiday__c> origHolidays{
		get{			
		if(origHolidays == null)
			origHolidays = retrieveHolidays();			
		return origHolidays;
	}
		set;
	}
	
	public void getSavedHolidays(){
		loadSelectedDatesList = true;
		savedHolidays = '';
		origHolidays = null;
		
		system.debug('@@@@@@@@@@@@@@@ newAddress.Country__c ' + newAddress.Country__c);
		system.debug('@@@@@@@@@@@@@@@ newAddress.State__c ' + newAddress.State__c);
		
		
		for(Public_Holiday__c ph : origHolidays){
			Date d = date.valueOf(ph.Date__c + '00:00:00');
			if(savedHolidays.length()>0)
				savedHolidays+=',';
			savedHolidays += d.year() + '/' + d.month() + '/' + d.day();
		}
		
		updatedHolidays = savedHolidays;
		//holidayDesc = origHolidays;
	}
	
	private List<Public_Holiday__c> retrieveHolidays(){
		
		Date beginOfYear = date.newInstance(year, 1, 1);
		Date endOfYear = date.newInstance(year, 12, 31);
		
		if(newAddress.Country__c == null || newAddress.State__c == null)			
			return new List<Public_Holiday__c>();
		
		return [select id,Country__c, State__c, Date__c, Description__c from	Public_Holiday__c 
		where Country__c = :newAddress.Country__c and State__c = :newAddress.State__c 
			and Date__c>=:beginOfYear and Date__c<=:endOfYear ORDER BY Date__c ];
		
		
	}
	
	public integer year
	{
		get
	{
		if(year == null)
			year = System.today().year();
		return year; 
	}
		set;
	}
	
	public String savedHolidays {
		get{
		savedHolidays = '';
		for(Public_Holiday__c ph : origHolidays){
			Date d = date.valueOf(ph.Date__c + '00:00:00');
			if(savedHolidays.length()>0)
				savedHolidays+=',';
			savedHolidays += d.year() + '/' + d.month() + '/' + d.day();
		}
			
		return savedHolidays;
	}
		set;
	}
	
	private boolean loadSelectedDatesList{get{if(loadSelectedDatesList==null)loadSelectedDatesList=true;return loadSelectedDatesList;}set;}
	public String updatedHolidays{
		get{
		if(loadSelectedDatesList)
			updatedHolidays = savedHolidays;	
		return updatedHolidays;
	}
		set;
	}
	
	public PageReference saveExtras()
	{	
		UPDATE origHolidays;
		return null;
	}
	
	public PageReference save(){
		loadSelectedDatesList = false;
		List<date> dl = filterByYear(StringListToDateList(updatedHolidays));
		loadSelectedDatesList = true;
		List<Public_Holiday__c> lOriginal =  new List<Public_Holiday__c>();
		List<Public_Holiday__c> lEdited = new List<Public_Holiday__c>();
		List<Public_Holiday__c> lDifference;
        
		//Fill lOriginal list
		if(origHolidays != null && origHolidays.size()>0)
			lOriginal = origHolidays;
        
		//Fill  lEdited list
		for(Date d:dl){
			Public_Holiday__c ph = new Public_Holiday__c();
			ph.Country__c = newAddress.Country__c;
			ph.State__c = newAddress.State__c;
			ph.Date__c = d;
			lEdited.add(ph);       
		}
        
		//diference between lOriginal - lEdited, to find the deleted dates
		lDifference = new List<Public_Holiday__c>();
		for(Public_Holiday__c lo : lOriginal){
			boolean found = false;
			for(Public_Holiday__c le : lEdited){
				if(lo.Date__c == le.Date__c)
				{found=true; break;}
                
			}
			if(!found)
				lDifference.add(lo);
		}

		DELETE lDifference;
        
        
		//diference between lEdited - lOriginal, to find the added dates
		lDifference = new List<Public_Holiday__c>();
		for(Public_Holiday__c le : lEdited){
			boolean found = false;
			for(Public_Holiday__c lo : lOriginal){
				if(le.Date__c == lo.Date__c)
					found=true;
			}
			if(!found){
				le.Country__c = newAddress.Country__c;
				le.State__c = newAddress.State__c;
				lDifference.add(le);
			}
		}

		INSERT lDifference; 
        
		savedHolidays = '';
		updatedHolidays = '';
        
		getSavedHolidays();
		apexMsg = 'Saved.';
		return null;
	}
	
	
	/* Calendar First date*/
	public date FromDate{
		get{
		if(FromDate==null)
		{
			FromDate = date.newInstance(System.today().year(), 1, 1);
		}
		return FromDate;}
		set;
	}
	
	public void addYear()
	{	
		FromDate = date.newInstance(FromDate.year()+1, 1, 1);
		origHolidays = null;
	}
	
	public void removeYear()
	{	
		FromDate = date.newInstance(FromDate.year()-1, 1, 1);
		origHolidays = null;
	}
	
	/* return the FromDate month index number */
	public integer getFromMonthIndex()
	{return FromDate.month()-1;}
	
	/* return the FromDate year*/
	public integer getFromYear()
	{return FromDate.year();}
	
	public Address__c newAddress {
		get{
		if(newAddress == null)
			newAddress = new Address__c();
		return newAddress;	
	}
		set;
	}
	
	private boolean resetList = true;
	public List<String> selectedDatesStrList {
		get{			
		if(resetList){
			try {
				selectedDatesStrList=new List<String>(); 
				selectedDatesStrList.clear();
				if(updatedHolidays.length()>0){			
					for (date d:convertedToList())			
						selectedDatesStrList.add( d.year() + '/' + d.month() + '/' + d.day());
				}			
			} catch(Exception e){System.debug('=====> erro 1: ' + e);}
		}
		resetList = true;
		return selectedDatesStrList;
	}
		set;
	}
	
	/* Converts inStringDateListCSV into a List of Dates*/
	private List<Date> convertedToList()
	{
		List<Date> s = new List<Date>(); 
		
		if(updatedHolidays.length() < 1)
			return s;
				
		List<String> strDates = updatedHolidays.split(',');
		
		for(String str : strDates)
		{
			str = str.replace('/','-');
			Date d = date.valueOf(str + ' 00:00:00');
			s.add(d);
		}	
		
		return s;
	}
	
	
	/*	This function returns to the YUI Calendar the dates that are out of bound on the start of the calendars,
		E.g. FromDate is the 5th day of the month, so days from 1 to 4 are out of bound.
			 if SelectPastDates is true it will bring all dates from the 1st day of the month in
			 FromDate till yesterday as out of bound
		Below is how YUI expects the string. Teh date format used is YYYY/MM/DD
		YAHOO.CC.calendar.cal1.addRenderer("2009/12/01-2009/12/11", YAHOO.CC.calendar.cal1.renderOutOfBoundsDate); 	 */
	public string getFromDateOutOfBounds()
	{
		date nFromDate = FromDate;
		
		date f =  FromDate.toStartOfMonth();
	
		if(nFromDate==f)
			return '';
	
		String s =  f.year() + '/' + f.month() + '/' + f.day() + '-' + nFromDate.year() +'/'+ nFromDate.month() + '/' + (nFromDate.day()-1);
		return 'YAHOO.CC.calendar.cal1.addRenderer("' + s + '", YAHOO.CC.calendar.cal1.renderOutOfBoundsDate);';
	}

	/*	This function returns to the YUI Calendar the dates that are out of bound on the end of the calendars,
			E.g. ToDate is the 5th day of the month, so days from 6 to the last day of that month are out of bound.
		Below is how YUI expects the string. Teh date format used is YYYY/MM/DD
		YAHOO.CC.calendar.cal1.addRenderer("2009/12/01-2009/12/11", YAHOO.CC.calendar.cal1.renderOutOfBoundsDate); 	 */
	public string getToDateOutOfBounds()
	{
		date f =  ToDate.toStartOfMonth().addMonths(1).addDays(-1);
	
		if(ToDate==f)
			return '';
	
		String s =  ToDate.year() +'/'+ ToDate.month() + '/' + (ToDate.day()+1) + '-' + f.year() + '/' + f.month() + '/' + f.day();
		return 'YAHOO.CC.calendar.cal1.addRenderer("' + s + '", YAHOO.CC.calendar.cal1.renderOutOfBoundsDate);';
	}
	
	/* Calendar Last date*/
	public date ToDate{
		get{if(ToDate==null)
			ToDate = date.newInstance(System.today().year(), 12, 31);
		return ToDate;}
		set;
	}	
	
	private List<date> StringListToDateList(string sl){
		List<date> dl = new List<date>();
        
		if(sl.Length()>0){
			List<String> strDates = sl.split(',');
			for(String s : strDates)
			{
				s = s.replace('/','-');
				Date d = date.valueOf(s + ' 00:00:00');
				dl.add(d);
			}}
        
		return dl;
	}
	
	
	private List<Date> filterByYear(List<Date> dl)
	{
		List<Date> filteredDateList = new List<Date>();
		for(Date d:dl)
		{
			if(d.year() == year)
				filteredDateList.add(d);
		}
		return filteredDateList;
	}
	
	
	
	
	
}