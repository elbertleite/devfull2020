@isTest
private class  lwcDashboardTest {

    @isTest static void getDashboardReportsExceptionTest(){
        string selectedAgencyGroup ='';
        string selectedAgency = '';
        string selectedDepartment = '';
        string selectedUser = ''; 
        string reportSettings = '[{"id":"1","name":"Contact","period":"Last 180 days","criteria":"LAST_N_DAYS:180","category":"TWO"},{"id":"4","name":"Enrolment","period":"Last 180 days","criteria":"LAST_N_DAYS:180","category":"THREE"},{"id":"5","name":"Instalments","period":"Last 180 days","criteria":"LAST_N_DAYS:180","category":"FOUR"},{"id":"6","name":"Payments","period":"Last 180 days","criteria":"LAST_N_DAYS:180","category":"FIVE"},{"id":"7","name":"Repayments","period":"Last 180 days","criteria":"LAST_N_DAYS:180","category":"SIX"},{"category":"ONE","criteria":"LAST_N_MONTHS:6","id":"2","name":"StageStatus","period":"Last 6 months"},{"id":"9","name":"StageStatusNZ","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"SEVEN"},{"id":"8","name":"Target","period":"Last 180 days","criteria":"LAST_N_DAYS:180","category":"EIGHT"},{"id":"3","name":"Task","period":"Last 180 days","criteria":"LAST_N_DAYS:180","category":"NINE"';
        lwcDashboard.getReportsWrp r = lwcDashboard.getDashboardReports(selectedAgencyGroup, selectedAgency, selectedDepartment, selectedUser, reportSettings);  
        System.assertNotEquals(r, null);
    }

    @isTest static void getDashboardReportsUserExceptionTest(){
        TestFactory tf = new TestFactory();        
        Account agency = tf.createAgency();   
        Contact cli = tf.createClient(agency);     
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);

        Test.startTest();
     	system.runAs(portalUser){  
            portalUser.DashboardSettings__c = '{"reports":[{"category":"LIST","criteria":"LAST_WEEK","period":"Last week","name":"StageStatus","id":"2","name":"Juliana Sousa","id":"005O0000003nhNTIAY"}';
            update portalUser;

            List<lwcDashboard.wrpDashReports> r = lwcDashboard.getDashboardReportsUser(agency.id);
        }
    }

    @isTest static void updateDashboardReportExceptionTest(){

        string objJson = '{"reports":[{"category":"LIST","criteria":"LAST_WEEK","period":"Last week","name":"StageStatus","id":"2","name":"Juliana Sousa","id":"005O0000003nhNTIAY"}';
        TestFactory tf = new TestFactory();        
        Account agency = tf.createAgency();   
        Contact cli = tf.createClient(agency);     
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);

        Test.startTest();
     	system.runAs(portalUser){  
            portalUser.DashboardSettings__c = '{"reports"}';
            update portalUser;
            string r = lwcDashboard.updateDashboardReport(objJson);
        }
    }

    @isTest static void getDashboardReportsTest(){
       
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency(); 
        Contact cli = tf.createClient(agency);    
        cli.Current_Agency__c = agency.Id; 
        cli.Visa_Expiry_Date__c = system.today().addMonths(-6);
        cli.Expected_Travel_Date__c = system.today().addMonths(-6);
        update cli;
        
      	Contact emp = tf.createEmployee(agency);       
       	User portalUser = tf.createPortalUser(emp);
        
        client_course__c x = new client_course__c();
		x.client__c = cli.id;
		x.isBooking__c = false;        
        x.isDeposit__c = false;
        x.isCancelRequest__c = false;  
        x.isCourseCredit__c = false; 
        x.isMigrated__c = false;
        x.Enrolment_Start_Date__c = system.today();
        x.isSoftDeleted__c = false; 
        x.course_package__c = null;
        x.isCancelled__c = false;
        x.isEnrolment_on_hold__c = false;                        
        x.Enrolment_Agency__c = agency.Id; 
        x.Enroled_by_Agency__c = agency.Id; 
        x.Enrolment_Date__c = system.today().addMonths(-6);
        x.Total_Paid__c = 0; // 
        x.Visa_Confirmed__c = false; //        
        x.Moved_to_Visa_On__c = system.today();            
		insert x;

        client_course_instalment__c cci1 = new client_course_instalment__c();		
		cci1.CLIENT_COURSE__C = x.id;
		cci1.NUMBER__C = 2;
		cci1.INSTALMENT_VALUE__C = 0;
		cci1.TUITION_VALUE__C = 0;
        cci1.Received_By_Agency__c = '0019000001MjqIIAAZ';
        cci1.Total_Paid_School_Credit__c = null;		
		cci1.COMMISSION_VALUE__C = 10;
        cci1.Shared_Comm_Agency_Enroll_Value__c = 10;
        cci1.Shared_Comm_Agency_Receive_Value__c = 20;
		cci1.DUE_DATE__C = system.today();		
		cci1.ISPDS__C = FALSE;
		cci1.ISMIGRATED__C = false;
		cci1.ORIGINAL_DUE_DATE__C = system.today();
		cci1.EXTRA_FEE_DISCOUNT_VALUE__C = 0;
		cci1.ISEXTRAINSTALLMENT__C = false;
		cci1.ORIGINAL_TUITION_VALUE__C = 0;
		cci1.ORIGINAL_INSTALMENT_VALUE__C = 0;
		cci1.ORIGINAL_EXTRA_FEE_VALUE__C = 0;
		cci1.ISAMENDMENT__C = FALSE;
		cci1.ISCANCELLED__C = FALSE;
		cci1.ISSELECTED__C = FALSE;
		cci1.COMMISSION_TAX_RATE__C =0;
		cci1.COMMISSION_TAX_VALUE__C = 0;
		cci1.DISCOUNT__C = 0;
		cci1.KEPP_FEE__C = 0;
		cci1.TOTAL_SCHOOL_PAYMENT__C = 0;
		cci1.SCHOLARSHIP_TAKEN__C = 0;
		cci1.STATUS__C = 'Open';
		cci1.BALANCE__C = 403;
        insert cci1;

        client_course__c x1 = new client_course__c();
		x1.client__c = cli.id;
		x1.isBooking__c = false;        
        x1.isDeposit__c = false;
        x1.isCancelRequest__c = false;  
        x1.isCourseCredit__c = false; 
        x1.isMigrated__c = false;
        x1.Enrolment_Start_Date__c = system.today();
        x1.isSoftDeleted__c = false; 
        x1.course_package__c = null;
        x1.isCancelled__c = false;
        x1.isEnrolment_on_hold__c = false;                        
        x1.Enrolment_Agency__c = agency.Id; 
        x1.LOO_Requested_to_BO_On__c = system.today().addMonths(-6);
        x1.LOO_Received_On__c = system.today().addMonths(-6);
        x1.LOO_Requested_to_CLI_On__c = system.today().addMonths(-6);
        x1.Enrolment_Date__c = system.today().addMonths(-6);
        x1.Total_Paid__c = 10; 
        x1.Visa_Confirmed__c = true;
        x1.LOO_Missing_Documents__c = true;
        x1.LOO_Missing_Documents_On__c = system.today();
        x1.COE_Requested_On__c = system.today().addMonths(-6);
        x1.COE_Requested__c = false;
        x1.COE_Received_On__c = system.today().addMonths(-6);
        x1.COE_Sent_to_CLI_On__c = system.today().addMonths(-6);
        x1.Moved_to_Visa_On__c = system.today();
        x1.Enrolment_Completed_On__c = system.today();
               
		insert x1;

        client_course_instalment__c cci = new client_course_instalment__c();		
		cci.CLIENT_COURSE__C = x1.id;
		cci.NUMBER__C = 1;
		cci.INSTALMENT_VALUE__C = 2230;
		cci.TUITION_VALUE__C = 1550;
		cci.EXTRA_FEE_VALUE__C = 680;
		cci.COMMISSION__C = 20;
		cci.COMMISSION_VALUE__C = 310;
		cci.DUE_DATE__C = system.today().addDays(10);
		cci.RELATED_FEES__C = 'a1IN00000030XPiMAM;;Material Fee;;250.00;;0;;250.00;;0!#a1IN00000030XPjMAM;;2016 Enrolment Fee;;220.00;;0;;220.00;;0!#a1IN00000030XPkMAM;;Enrolment Fee;;210.00;;0;;210.00;;0';
		cci.ISPDS__C = FALSE;
		cci.ISMIGRATED__C = false;
		cci.ORIGINAL_DUE_DATE__C = system.today().addDays(10);
		cci.EXTRA_FEE_DISCOUNT_VALUE__C = 0;
		cci.ISEXTRAINSTALLMENT__C = false;
		cci.ORIGINAL_TUITION_VALUE__C = 1550;
		cci.ORIGINAL_INSTALMENT_VALUE__C = 2230;
		cci.ORIGINAL_EXTRA_FEE_VALUE__C = 680;
		cci.ISAMENDMENT__C = FALSE;
		cci.ISCANCELLED__C = FALSE;
		cci.ISSELECTED__C = FALSE;
		cci.COMMISSION_TAX_RATE__C = 30;
		cci.COMMISSION_TAX_VALUE__C = 93;
		cci.DISCOUNT__C = 0;
		cci.KEPP_FEE__C = 0;
		cci.TOTAL_SCHOOL_PAYMENT__C = 1827;
		cci.SCHOLARSHIP_TAKEN__C = 0;
		cci.STATUS__C = 'Open';
		cci.BALANCE__C = 403;
		cci.ISFIRSTPAYMENT__C = true;
		cci.Agency_Currency__c = 'AUD';
		cci.Agency_Currency_Rate__c = 1;
		cci.Agency_Currency_Value__c = 1;
        cci.Total_Paid_School_Credit__c = 90;
        cci.Received_By_Agency__c = agency.id;
        cci.Commission_Confirmed_On__c  = system.today().addMonths(-6);
        cci.isPfs__c = false;
        cci.Received_Date__c = system.today().addMonths(-6);
        insert cci;

        Destination_Tracking__c dt1 = new Destination_tracking__c();        
		dt1.Client__c = cli.id;
        dt1.Agency_Group__c = agency.ParentId;
		dt1.Arrival_Date__c = system.today().addMonths(3);
		dt1.Destination_Country__c = 'Australia';
		dt1.Destination_City__c = 'Sydney';
        dt1.Expected_Travel_Date__c = system.today().addMonths(-6);        
        dt1.Lead_Last_Change_Status_Cycle__c = system.today().addMonths(-6);
		dt1.lead_Cycle__c = false;
        dt1.Current_Cycle__c = true;
        dt1.Lead_Last_Status_Cycle__c = 'Test Novo';
        dt1.Lead_Last_Stage_Cycle__c = 'Stage 0';        

		insert dt1;

        Destination_Tracking__c dt = new Destination_tracking__c();        
		dt.Client__c = cli.id;
        dt.Agency_Group__c = agency.ParentId;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
        dt.Expected_Travel_Date__c = system.today().addMonths(-6);        
        dt.Lead_Last_Change_Status_Cycle__c = system.today().addMonths(-6);
		dt.lead_Cycle__c = false;
        dt.Current_Cycle__c = true;
        dt.Lead_Last_Status_Cycle__c = 'Test Show Lead Novo';
        dt.Lead_Last_Stage_Cycle__c = 'Stage 1';        
       
		insert dt;

        Destination_Tracking__c dt3 = new Destination_tracking__c();        
		dt3.Client__c = cli.id;
        dt3.Agency_Group__c = agency.ParentId;
		dt3.Arrival_Date__c = system.today().addMonths(3);
		dt3.Destination_Country__c = 'Australia';
		dt3.Destination_City__c = 'Sydney';
        dt3.Expected_Travel_Date__c = system.today().addMonths(-6);        
        dt3.Lead_Last_Change_Status_Cycle__c = system.today().addMonths(-6);
		dt3.lead_Cycle__c = false;
        dt3.Current_Cycle__c = true;
        dt3.Lead_Last_Status_Cycle__c = 'Test Show Lead';
        dt3.Lead_Last_Stage_Cycle__c = 'Stage 1';        

		insert dt3;

        Destination_Tracking__c dt2 = new Destination_tracking__c();        
		dt2.Client__c = cli.id;
        dt2.Agency_Group__c = agency.ParentId;
		dt2.Arrival_Date__c = system.today().addMonths(3);
		dt2.Destination_Country__c = 'New Zealand';
		dt2.Destination_City__c = 'Sydney';
        dt2.Expected_Travel_Date__c = system.today().addMonths(-6);        
        dt2.Lead_Last_Change_Status_Cycle__c = system.today().addMonths(-6);
		dt2.lead_Cycle__c = false;
        dt2.Current_Cycle__c = true;
        dt2.Lead_Last_Status_Cycle__c = 'Test Show';
        dt2.Lead_Last_Stage_Cycle__c = 'Stage 2';        

		insert dt2;

        Destination_Tracking__c dt4 = new Destination_tracking__c();        
		dt4.Client__c = cli.id;
        dt4.Agency_Group__c = agency.ParentId;
		dt4.Arrival_Date__c = system.today().addMonths(3);
		dt4.Destination_Country__c = 'Brazil';
		dt4.Destination_City__c = 'Sydney';
        dt4.Expected_Travel_Date__c = system.today().addMonths(-6);        
        dt4.Lead_Last_Change_Status_Cycle__c = system.today().addMonths(-6);
		dt4.lead_Cycle__c = false;
        dt4.Current_Cycle__c = true;
        dt4.Lead_Last_Status_Cycle__c = 'Test Show';
        dt4.Lead_Last_Stage_Cycle__c = 'Stage 2';        
     
		insert dt4;

        Client_Stage__c csc2 = new Client_Stage__c();
        csc2.Stage_description__c = 'Test Novo';
        csc2.Destination__c = 'Australia';
        csc2.Agency_Group__c = agency.ParentId;         
        csc2.Stage__c = 'Stage 0';    
        insert csc2;

        Client_Stage__c csc = new Client_Stage__c();
        csc.Stage_description__c = 'Test Show Lead Novo';
        csc.Agency_Group__c = agency.ParentId; 
        csc.Destination__c = 'Australia';
        csc.Stage__c = 'Stage 1';    
        insert csc;

        Client_Stage__c csc1 = new Client_Stage__c();
        csc1.Stage_description__c = 'Test Show Lead';
        csc1.Agency_Group__c = agency.ParentId; 
        csc1.Destination__c = 'Australia';
        csc1.Stage__c = 'Stage 1';    
        insert csc1;

        Client_Stage__c csc3 = new Client_Stage__c();
        csc3.Stage_description__c = 'Test Show';
        csc3.Agency_Group__c = agency.ParentId; 
        csc3.Destination__c = 'New Zealand';
        csc3.Stage__c = 'Stage 2';    
        insert csc3;

        Client_Stage__c csc4 = new Client_Stage__c();
        csc4.Stage_description__c = 'Test Show';
        csc4.Agency_Group__c = agency.ParentId; 
        csc4.Destination__c = 'Brazil';
        csc4.Stage__c = 'Stage 2';    
        insert csc4;

        Custom_Note_Task__c task = new Custom_Note_Task__c();                
        task.Assign_To__c = portalUser.Id;
        task.isNote__c = false;        
        task.Due_Date__c = system.today().addMonths(-6);     
        task.AssignedOn__c = system.today().addDays(2);       
        task.Status__c = 'In Progress';
        task.Subject__c = '1st Follow up';
        task.Priority__c = '1 - Medium';  
        task.Related_Contact__c = cli.id;
        insert task; 

        Custom_Note_Task__c task1 = new Custom_Note_Task__c();                
        task1.Assign_To__c = portalUser.Id;
        task1.isNote__c = false;        
        task1.Due_Date__c = system.today().addMonths(-6);        
        task1.AssignedOn__c = system.today().addDays(2);		
        task1.Status__c = 'In Progress';
        task1.Subject__c = '1st Follow up';
        task1.Priority__c = '1 - Medium';  
        task.Related_Contact__c = cli.id;
        insert task1; 

        Department__c dep = tf.createDepartment(agency);
        dep.Department_Sales_Target__c = 225;
        dep.Inactive__c = false;
        update dep;

        Invoice__c inv = new Invoice__c();
        inv.Requested_by_Agency__c = agency.id;
        inv.isPDS_Confirmation__c = true; 
        inv.Sent_On__c = system.today().addDays(-60); 
        insert inv; 
        Invoice__c inv1 = new Invoice__c();
        inv1.Requested_by_Agency__c = agency.id;
        inv1.isCommission_Invoice__c = true; 
        inv1.Sent_On__c = system.today().addDays(-60); 
        insert inv1;
        Invoice__c inv2 = new Invoice__c();
        inv2.Requested_by_Agency__c = agency.id;
        inv2.isCommission_Invoice__c = true; 
        inv2.Commission_Paid_Date__c = system.today().addDays(-60); 
        insert inv2;         

        Test.startTest();        
     	system.runAs(portalUser){  
            string selectedAgencyGroup = agency.ParentId;
            string selectedAgency = agency.Id;
            string selectedDepartment = 'all';
            string selectedUser = 'all'; 
            string reportSettings =  '[{"id":"11","name":"Commission Invoice Overview","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"ONE"},'
            + '{"id":"19","name":"Contacts with Travel Date","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"SEVEN"},'
            + '{"id":"18","name":"Contacts with Visa Expiry","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"ELEVEN"},'
            + '{"id":"4","name":"Enrolment","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"FOURTEEN"},'
            + '{"id":"1","name":"New Leads Evolution","period":"","criteria":"","category":"NINE"},'
            + '{"id":"2","name":"Pipeline Overview Australia","period":"","criteria":"","category":"EIGHT"},'
            + '{"id":"9","name":"Pipeline Overview New Zealand","period":"","criteria":"","category":"TEN"},'
            + '{"id":"10","name":"Quantity of Enrolments","period":"Payment Date","criteria":"Payment_Date","category":"THIRTEEN"},'
            + '{"id":"5","name":"Sales overview - First Instalments","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"TWO"},'
            + '{"id":"6","name":"Sales overview - First Payments","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"FIVE"},'
            + '{"id":"7","name":"Sales overview - First Repayments","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"FOUR"},'
            + '{"id":"13","name":"Sales Revenue","period":"Enrolment Date","criteria":"Enrolment_Date","category":"SIX"},'
            + '{"id":"8","name":"Sales Target","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"THREE"},'
            + '{"id":"3","name":"Task","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"TWELVE"}]';

            lwcDashboard.getReportsWrp r = lwcDashboard.getDashboardReports(selectedAgencyGroup, selectedAgency, selectedDepartment, selectedUser, reportSettings);
        }
    }

    @isTest static void getDashboardReports1Test(){
       
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency(); 
        Contact cli = tf.createClient(agency);    
        cli.Current_Agency__c = agency.Id; 
        update cli;
        
      	Contact emp = tf.createEmployee(agency);       
       	User portalUser = tf.createPortalUser(emp);
        
        client_course__c x1 = new client_course__c();
		x1.client__c = cli.id;
		x1.isBooking__c = false;        
        x1.isDeposit__c = false;
        x1.isCancelRequest__c = false;  
        x1.isCourseCredit__c = false; 
        x1.isMigrated__c = false;
        x1.Enrolment_Start_Date__c = system.today();
        x1.isSoftDeleted__c = false; 
        x1.course_package__c = null;
        x1.isCancelled__c = false;
        x1.isEnrolment_on_hold__c = false;                        
        x1.Enroled_by_Agency__c  = agency.Id; 
        x1.Enrolment_Agency__c = agency.Id; 
        x1.LOO_Requested_to_BO_On__c = system.today().addMonths(-6);
        x1.LOO_Received_On__c = system.today().addMonths(-6);
        x1.LOO_Requested_to_CLI_On__c = system.today().addMonths(-6);
        x1.Enrolment_Date__c = system.today().addMonths(-6);
        x1.Total_Paid__c = 10; 
        x1.Visa_Confirmed__c = true;
        x1.LOO_Missing_Documents__c = true;
        x1.LOO_Missing_Documents_On__c = system.today().addMonths(-6);
        x1.COE_Requested_On__c = system.today().addMonths(-6);
        x1.COE_Requested__c = false;
        x1.COE_Received_On__c = system.today().addMonths(-6);
        x1.COE_Sent_to_CLI_On__c = system.today().addMonths(-6);
        x1.Moved_to_Visa_On__c = system.today();
        x1.Enrolment_Completed_On__c = system.today();
                       
		insert x1;

        client_course_instalment__c cci = new client_course_instalment__c();		
		cci.CLIENT_COURSE__C = x1.id;
		cci.NUMBER__C = 1;
		cci.INSTALMENT_VALUE__C = 2230;
		cci.TUITION_VALUE__C = 1550;
		cci.EXTRA_FEE_VALUE__C = 680;
		cci.COMMISSION__C = 20;
		cci.COMMISSION_VALUE__C = 310;
		cci.DUE_DATE__C = system.today().addDays(10);
		cci.RELATED_FEES__C = 'a1IN00000030XPiMAM;;Material Fee;;250.00;;0;;250.00;;0!#a1IN00000030XPjMAM;;2016 Enrolment Fee;;220.00;;0;;220.00;;0!#a1IN00000030XPkMAM;;Enrolment Fee;;210.00;;0;;210.00;;0';
		cci.ISPDS__C = FALSE;
		cci.ISMIGRATED__C = false;
		cci.ORIGINAL_DUE_DATE__C = system.today().addDays(10);
		cci.EXTRA_FEE_DISCOUNT_VALUE__C = 0;
		cci.ISEXTRAINSTALLMENT__C = false;
		cci.ORIGINAL_TUITION_VALUE__C = 1550;
		cci.ORIGINAL_INSTALMENT_VALUE__C = 2230;
		cci.ORIGINAL_EXTRA_FEE_VALUE__C = 680;
		cci.ISAMENDMENT__C = FALSE;
		cci.ISCANCELLED__C = FALSE;
		cci.ISSELECTED__C = FALSE;
		cci.COMMISSION_TAX_RATE__C = 30;
		cci.COMMISSION_TAX_VALUE__C = 93;
		cci.DISCOUNT__C = 0;
		cci.KEPP_FEE__C = 0;
		cci.TOTAL_SCHOOL_PAYMENT__C = 1827;
		cci.SCHOLARSHIP_TAKEN__C = 0;
		cci.STATUS__C = 'Open';
		cci.BALANCE__C = 403;
		cci.ISFIRSTPAYMENT__C = true;
		cci.Agency_Currency__c = 'AUD';
		cci.Agency_Currency_Rate__c = 1;
		cci.Agency_Currency_Value__c = 1;
        cci.Total_Paid_School_Credit__c = 90;        
        cci.Commission_Confirmed_On__c  = system.today();
        cci.Received_By_Agency__c = agency.Id; 
        cci.isPfs__c = false;
        cci.Received_Date__c = system.today().addMonths(-6);
        
        insert cci;

        client_course__c x2 = new client_course__c();
		x2.client__c = cli.id;
		x2.isBooking__c = false;        
        x2.isDeposit__c = false;
        x2.isCancelRequest__c = false;  
        x2.isCourseCredit__c = false; 
        x2.isMigrated__c = false;
        x2.Enrolment_Start_Date__c = system.today();
        x2.isSoftDeleted__c = false; 
        x2.course_package__c = null;
        x2.isCancelled__c = false;
        x2.isEnrolment_on_hold__c = false;                        
        x2.Enroled_by_Agency__c  = agency.Id; 
        x2.LOO_Requested_to_BO_On__c = system.today();
        x2.LOO_Received_On__c = system.today();
        x2.LOO_Requested_to_CLI_On__c = system.today();
        x2.Enrolment_Date__c = system.today().addMonths(-6);
        x2.Total_Paid__c = 10; 
        x2.Visa_Confirmed__c = true;
        x2.LOO_Missing_Documents__c = true;
        x2.LOO_Missing_Documents_On__c = system.today();
        x2.COE_Requested_On__c = system.today().addMonths(-6);
        x2.COE_Requested__c = false;
        x2.COE_Received_On__c = system.today().addMonths(-6);
        x2.COE_Sent_to_CLI_On__c = system.today().addMonths(-6);
        x2.Moved_to_Visa_On__c = system.today();
        x2.Enrolment_Completed_On__c = system.today();
        
               
		insert x2;

        client_course_instalment__c c2 = new client_course_instalment__c();		
		c2.CLIENT_COURSE__C = x2.id;
		c2.NUMBER__C = 1;
		c2.INSTALMENT_VALUE__C = 2230;
		c2.TUITION_VALUE__C = 1550;
		c2.EXTRA_FEE_VALUE__C = 680;
		c2.COMMISSION__C = 20;
		c2.COMMISSION_VALUE__C = 310;
		c2.DUE_DATE__C = system.today().addDays(10);
		c2.RELATED_FEES__C = 'a1IN00000030XPiMAM;;Material Fee;;250.00;;0;;250.00;;0!#a1IN00000030XPjMAM;;2016 Enrolment Fee;;220.00;;0;;220.00;;0!#a1IN00000030XPkMAM;;Enrolment Fee;;210.00;;0;;210.00;;0';
		c2.ISPDS__C = FALSE;
		c2.ISMIGRATED__C = false;
		c2.ORIGINAL_DUE_DATE__C = system.today().addDays(10);
		c2.EXTRA_FEE_DISCOUNT_VALUE__C = 0;
		c2.ISEXTRAINSTALLMENT__C = false;
		c2.ORIGINAL_TUITION_VALUE__C = 1550;
		c2.ORIGINAL_INSTALMENT_VALUE__C = 2230;
		c2.ORIGINAL_EXTRA_FEE_VALUE__C = 680;
		c2.ISAMENDMENT__C = FALSE;
		c2.ISCANCELLED__C = FALSE;
		c2.ISSELECTED__C = FALSE;
		c2.COMMISSION_TAX_RATE__C = 30;
		c2.COMMISSION_TAX_VALUE__C = 93;
		c2.DISCOUNT__C = 0;
		c2.KEPP_FEE__C = 0;
		c2.TOTAL_SCHOOL_PAYMENT__C = 1827;
		c2.SCHOLARSHIP_TAKEN__C = 0;
		c2.STATUS__C = 'Open';
		c2.BALANCE__C = 403;
		c2.ISFIRSTPAYMENT__C = true;
		c2.Agency_Currency__c = 'AUD';
		c2.Agency_Currency_Rate__c = 1;
		c2.Agency_Currency_Value__c = 1;
        c2.Total_Paid_School_Credit__c = 90;        
        c2.Commission_Confirmed_On__c  = system.today().addMonths(-6);
        c2.Received_By_Agency__c =  '0019000001MjqIIAAZ';
        c2.isPfs__c = false;
        c2.Received_Date__c = system.today().addMonths(-6);
        
        insert c2;
 
        Test.startTest();        
     	system.runAs(portalUser){  
            portalUser.Finance_Access_All_Agencies__c = false;
            portalUser.Aditional_Agency_Managment__c = null;
            portalUser.Finance_Access_All_Groups__c = false;
            update portalUser;

            string selectedAgencyGroup = agency.ParentId;
            string selectedAgency = agency.Id;
            string selectedDepartment = 'all';
            string selectedUser = 'all'; 
            string reportSettings =  '[{"id":"11","name":"Commission Invoice Overview","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"ONE"},'
                                    + '{"id":"19","name":"Contacts with Travel Date","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"SEVEN"},'
                                    + '{"id":"18","name":"Contacts with Visa Expiry","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"ELEVEN"},'
                                    + '{"id":"4","name":"Enrolment","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"FOURTEEN"},'
                                    + '{"id":"1","name":"New Leads Evolution","period":"","criteria":"","category":"NINE"},'
                                    + '{"id":"2","name":"Pipeline Overview Australia","period":"","criteria":"","category":"EIGHT"},'
                                    + '{"id":"9","name":"Pipeline Overview New Zealand","period":"","criteria":"","category":"TEN"},'
                                    + '{"id":"10","name":"Quantity of Enrolments","period":"Payment Date","criteria":"Payment_Date","category":"THIRTEEN"},'
                                    + '{"id":"5","name":"Sales overview - First Instalments","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"TWO"},'
                                    + '{"id":"6","name":"Sales overview - First Payments","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"FIVE"},'
                                    + '{"id":"7","name":"Sales overview - First Repayments","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"FOUR"},'
                                    + '{"id":"13","name":"Sales Revenue","period":"Enrolment Date","criteria":"Enrolment_Date","category":"SIX"},'
                                    + '{"id":"8","name":"Sales Target","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"THREE"},'
                                    + '{"id":"3","name":"Task","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"TWELVE"}]';
/*
    "id":"0059000000PqzURAAZ", "name":"Patricia Greco"

            { "reports":[{"id":"11","name":"Commission Invoice Overview","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"ONE"},{"id":"19","name":"Contacts with Travel Date","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"SEVEN"},{"id":"18","name":"Contacts with Visa Expiry","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"ELEVEN"},{"id":"4","name":"Enrolment","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"FOURTEEN"},{"id":"1","name":"New Leads Evolution","period":"","criteria":"","category":"NINE"},{"id":"2","name":"Pipeline Overview Australia","period":"","criteria":"","category":"EIGHT"},{"id":"9","name":"Pipeline Overview New Zealand","period":"","criteria":"","category":"TEN"},{"id":"10","name":"Quantity of Enrolments","period":"Payment Date","criteria":"Payment_Date","category":"THIRTEEN"},{"id":"5","name":"Sales overview - First Instalments","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"TWO"},{"id":"6","name":"Sales overview - First Payments","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"FIVE"},{"id":"7","name":"Sales overview - First Repayments","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"FOUR"},{"id":"13","name":"Sales Revenue","period":"Enrolment Date","criteria":"Enrolment_Date","category":"SIX"},{"id":"8","name":"Sales Target","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"THREE"},{"id":"3","name":"Task","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"TWELVE"}],"id":"0059000000PqzURAAZ", "name":"Patricia Greco"}
*/
            lwcDashboard.getReportsWrp r = lwcDashboard.getDashboardReports(selectedAgencyGroup, selectedAgency, selectedDepartment, selectedUser, reportSettings);
        }
    }

    @isTest static void getDashboardReports2Test(){
       
        string selectedAgencyGroup ='0019000001MjqDYAAZ';
        string selectedAgency = '0019000001MjqIIAAZ';
        string selectedDepartment =  'a0K9000000QFbSzEAL';
        string selectedUser = '0059000000PqzURAAZ'; 
        string reportSettings = '[{"id":"11","name":"Commission Invoice Overview","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"ONE"},'
                                + '{"id":"19","name":"Contacts with Travel Date","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"SEVEN"},'
                                + '{"id":"18","name":"Contacts with Visa Expiry","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"ELEVEN"},'
                                + '{"id":"4","name":"Enrolment","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"FOURTEEN"},'
                                + '{"id":"1","name":"New Leads Evolution","period":"","criteria":"","category":"NINE"},'
                                + '{"id":"2","name":"Pipeline Overview Australia","period":"","criteria":"","category":"EIGHT"},'
                                + '{"id":"9","name":"Pipeline Overview New Zealand","period":"","criteria":"","category":"TEN"},'
                                + '{"id":"10","name":"Quantity of Enrolments","period":"Payment Date","criteria":"Payment_Date","category":"THIRTEEN"},'
                                + '{"id":"5","name":"Sales overview - First Instalments","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"TWO"},'
                                + '{"id":"6","name":"Sales overview - First Payments","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"FIVE"},'
                                + '{"id":"7","name":"Sales overview - First Repayments","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"FOUR"},'
                                + '{"id":"13","name":"Sales Revenue","period":"Enrolment Date","criteria":"Enrolment_Date","category":"SIX"},'
                                + '{"id":"8","name":"Sales Target","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"THREE"},'
                                + '{"id":"3","name":"Task","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"TWELVE"}]';
        lwcDashboard.getReportsWrp r = lwcDashboard.getDashboardReports(selectedAgencyGroup, selectedAgency, selectedDepartment, selectedUser, reportSettings);
    }

    @isTest static void getCurrentUserTest(){
        User r = lwcDashboard.getCurrentUser();
    }

    @isTest static void getDashboardReportsUserTest(){
        TestFactory tf = new TestFactory();        
        Account agency = tf.createAgency();   
        Contact cli = tf.createClient(agency);     
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);

        Test.startTest();
     	system.runAs(portalUser){  
            portalUser.DashboardSettings__c = '{"reports":[{"id":"2","name":"Pipeline Overview Australia","period":"","criteria":"","category":"EIGHT"}],"name":"Juliana Sousa","id":"005O0000003nhNTIAY"}';
            update portalUser;
            List<lwcDashboard.wrpDashReports> r = lwcDashboard.getDashboardReportsUser(agency.id);
        }
    }

    @isTest static void getDashboardReportsUser1Test(){
        TestFactory tf = new TestFactory();        
        Account agency = tf.createAgency();   
        Contact cli = tf.createClient(agency);     
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);

        Test.startTest();
     	system.runAs(portalUser){  
            portalUser.DashboardSettings__c = '{"reports":[{"id":"5","name":"Sales overview - First Instalments","period":"Last 6 months","criteria":"LAST_N_MONTHS:6","category":"TWO"}],"name":"Juliana Sousa","id":"005O0000003nhNTIAY"}';
            update portalUser;
            List<lwcDashboard.wrpDashReports> r = lwcDashboard.getDashboardReportsUser(agency.id);
        }
    }

    @isTest static void updateDashboardReportTest(){
        string objJson = '';
        string r = lwcDashboard.updateDashboardReport(objJson);
    }

    @isTest static void getAgencyGroupTest(){
        TestFactory tf = new TestFactory();        
        Account agency = tf.createAgency();   
        Contact cli = tf.createClient(agency);     
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);       

        Test.startTest();
     	system.runAs(portalUser){ 
            List<lwcDashboard.wrpGeneric> r = lwcDashboard.getAgencyGroup();
        }
    }

    @isTest static void getAgencyTest(){
        string selectedAgencyGroup = '';
        List<lwcDashboard.wrpGeneric> r = lwcDashboard.getAgency(selectedAgencyGroup);
    }

    @isTest static void getAgency1Test(){
        TestFactory tf = new TestFactory();        
        Account agency = tf.createAgency();   
        Contact cli = tf.createClient(agency);     
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);       

        Test.startTest();
     	system.runAs(portalUser){ 
        List<lwcDashboard.wrpGeneric> r = lwcDashboard.getAgency(agency.ParentId);
        }
    }

    @isTest static void getDepartmentTest(){
        string selectedAgency = '';
        List<lwcDashboard.wrpGeneric> r = lwcDashboard.getDepartment(selectedAgency);
    }

    @isTest static void getDepartment1Test(){
        TestFactory tf = new TestFactory();        
        Account agency = tf.createAgency();   
        Contact cli = tf.createClient(agency);     
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
        Department__c dep = tf.createDepartment(agency);
        dep.Department_Sales_Target__c = 225;
        dep.Inactive__c = false;
        update dep;

        Test.startTest();
     	system.runAs(portalUser){ 
            List<lwcDashboard.wrpGeneric> r = lwcDashboard.getDepartment(agency.Id);
        }
    }

    @isTest static void getMonthNameTest(){
        for (Integer i = 0; i < 12; i++) {
            string d1 = lwcDashboard.getMonthName(i+1);
        }      
    }
    
    @isTest static void getUserTest(){
        string selectedAgency = '';
        string selectedDepartment = '';
        
        List<lwcDashboard.wrpGeneric> r = lwcDashboard.getUser(selectedAgency, selectedDepartment);
    }

    @isTest static void getUser1Test(){
       
        TestFactory tf = new TestFactory();        
        Account agency = tf.createAgency();   
        Contact cli = tf.createClient(agency);     
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
        Department__c dep = tf.createDepartment(agency);
        dep.Department_Sales_Target__c = 225;
        dep.Inactive__c = false;
        update dep;

        Test.startTest();
     	system.runAs(portalUser){ 
            List<lwcDashboard.wrpGeneric> r = lwcDashboard.getUser(agency.id, dep.id);
        }
    }

     @isTest static void getUser2Test(){        
        string selectedAgency = '0019000001MjqIIAAZ';
        string selectedDepartment =  'a0K9000000QFbSzEAL';
        
        List<lwcDashboard.wrpGeneric> r = lwcDashboard.getUser(selectedAgency, selectedDepartment);        
    }
}