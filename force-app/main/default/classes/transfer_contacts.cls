public without sharing class transfer_contacts {
	
	public String transferFromAgency {get;set;}
	public String transferFromUser {get;set;}
	public String transferToAgency {get;set;}
	public String transferToUser {get;set;}	
	public Account agency {get;set;}
	
	public transfer_contacts(){
		totalContacts = 0;		
		setAgencyInfo();
		listContacts = new List<Contact>();
		listTasks = new List<Custom_Note_Task__c>();
	}
	
	private void setAgencyInfo(){
		
		User u = [select contact.accountid, contact.account.parentid from user where id = :UserInfo.getUserId()];
		
		
		try {
			agency = [SELECT id, Name, Account_Link__c, ParentID FROM Account where id = :u.contact.accountid];
			
			transferFromAgency = agency.ID;
			transferToAgency = agency.ID;
						
		} catch (Exception e){
		}
	}
	
	private List<SelectOption> listAgencies;
	public List<SelectOption> getListAgencies(){		
		if(listAgencies == null){			
			listAgencies = new List<SelectOption>();
			for(Account agency : [Select Id, Name from Account where Account_Link__c = :agency.Account_Link__c and RecordType.Name = 'Agency' ORDER BY name]){
				listAgencies.add(new SelectOption(agency.Id,agency.Name));				
			}
							
		}
		return listAgencies;		
	}
	
	public void refreshFromUsers(){
		listUsersFrom = null;
	}
	public Map<String, String> mapUsers {get;set;}
	private List<SelectOption> listUsersFrom;
	public List<SelectOption> getListUsersFrom(){
		if(listUsersFrom == null){
			mapUsers = new Map<String, String>();
			listUsersFrom = new List<SelectOption>();			
			listUsersFrom.add(new SelectOption('', '-- Select User --'));
			for(User u : [Select Id, Name from User where AccountID = :transferFromAgency and Contact.chatter_only__c = false ORDER BY name]){
				listUsersFrom.add(new SelectOption(u.id, u.Name));				
				mapUsers.put(u.id, u.Name);
			}			
		}
		return listUsersFrom;		
	}
	
	public boolean showContactFilters {get {if(showContactFilters == null) showContactFilters = false; return showContactFilters;} set;}
	
	public void refreshToUsers(){
		listUsersTo = null;
		selectedNationality = null;
		selectedDestination = null;
		clientName = null;
		listNationalities = null;
		listDestinations = null;
		
		if( getListNationalities().size() > 1 || getListDestinations().size() > 1 )
			showContactFilters = true;
		else
			showContactFilters = false;
		
	}
	
	private List<SelectOption> listUsersTo;
	public List<SelectOption> getListUsersTo(){
		if(listUsersTo == null){
			listUsersTo = new List<SelectOption>();
			listUsersTo.add(new SelectOption('', '-- Select User --'));
			String filterFromUser = '';
			if(transferFromUser != null && transferFromUser != '')
				filterFromUser = ' and id != \'' + transferFromUser + '\' ';
				
			String sql = 'Select Id, Name from User where AccountID = :transferToAgency ' + filterFromUser + ' and Contact.chatter_only__c = false and IsActive = true ORDER BY Name';
			
			for(User u : Database.query(sql)){
				listUsersTo.add(new SelectOption(u.id, u.Name));				
			}			
		}
		return listUsersTo;		
	}
	
	public string selectedNationality {get;set;}
	private List<SelectOption> listNationalities;
	public List<SelectOption> getListNationalities(){
		if(listNationalities == null && transferFromUser != null){
			listNationalities = new List<SelectOption>();
			listNationalities.add(new SelectOption('', '-- All Nationalities --'));
				
			String sql = 'SELECT Nationality__c from CONTACT where Owner__c = :transferFromUser and Nationality__c != null group by Nationality__c  order by Nationality__c';
			
			for(AggregateResult ar : Database.query(sql)){
				listNationalities.add(new SelectOption( (String) ar.get('Nationality__c') , (String) ar.get('Nationality__c') ));				
			}			
		}
		return listNationalities;		
	}
	
	public string clientName {get;set;}
	public string selectedDestination {get;set;}
	private List<SelectOption> listDestinations;
	public List<SelectOption> getListDestinations(){
		if(listDestinations == null && transferFromUser != null){
			listDestinations = new List<SelectOption>();
			listDestinations.add(new SelectOption('', '-- All Destinations --'));
				
			String sql = 'SELECT Destination_Country__c from CONTACT where Owner__c = :transferFromUser and Destination_Country__c != null group by Destination_Country__c  order by Destination_Country__c';
			
			for(AggregateResult ar : Database.query(sql)){
				listDestinations.add(new SelectOption( (String) ar.get('Destination_Country__c') , (String) ar.get('Destination_Country__c') ));				
			}			
		}
		return listDestinations;		
	}
	
	
	public List<Contact> listContacts {get;set;}
	public List<Custom_Note_Task__c> listTasks {get;set;}
	public Integer totalContacts {get;set;}
	public Integer totalTasks {get;set;}
	public boolean isFilteringContacts {get;set;}
	public void loadContacts(){
	
		listContacts = new List<Contact>();
		listTasks = new List<Custom_Note_Task__c>();
		
		String filterContacts = '';		
		if(notBlank(selectedDestination))
			filterContacts += ' and Destination_Country__c = :selectedDestination ';
		if(notBlank(selectedNationality))
			filterContacts += ' and Nationality__c = :selectedNationality ';
		if(notBlank(clientName)){
			if(clientName.contains(';')){
				list<string> listCOntactIds = new list<string>(clientName.split(';'));
				filterContacts += ' and id in :listCOntactIds ';
			}
			else filterContacts += ' and name = :clientName ';
		}
		isFilteringContacts = filterContacts.length() > 0;
		
		String sqlWhere = 'where Owner__c = :transferFromUser and RecordType.Name in (\'Client\', \'Lead\') ' + filterContacts;
		
		String sqlCount = 'select COUNT(ID) ct from Contact ' + sqlWhere;		
		AggregateResult ar = Database.query(sqlCount);
		totalContacts = Integer.valueOf(ar.get('ct'));		
		
		if(totalContacts > 0){
		
			String orderBy = ' order by Name ';
			
			String sql = 'Select id, isSelected__c, Name, CreatedDate, Destination_Country__c, Nationality__c, Expected_Travel_Date__c, Visa_Expiry_Date__c, ' +
							'( Select id from Custom_Notes_Tasks__r where Assign_To__c = :transferFromUser and Status__c != \'Completed\') ' + 
							'from Contact ' + sqlWhere + orderBy + ' LIMIT 400 ';
			
			listContacts = Database.query(sql);
			
		} else if(!isFilteringContacts) {
			
			ar = [select COUNT(ID) ct from Custom_Note_Task__c where Assign_To__c = :transferFromUser and Status__c != 'Completed'];
			totalTasks = Integer.valueOf(ar.get('ct'));
			
			listTasks = [Select Due_Date__c, Selected__c, Subject__c, Related_To_Quote__c, Related_Contact__c, Related_Contact__r.Name, Status__c from Custom_Note_Task__c where Assign_To__c = :transferFromUser and Status__c != 'Completed' order by Due_Date__c desc LIMIT 400];
			
			if(!ApexPages.hasMessages() && (listTasks == null || listTasks.isEmpty()) ){
				listTasks = new List<Custom_Note_Task__c>();
				Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No contacts and tasks found for this user.'));
			}
		}
		
	}

	public List<Contact> contactsToRdStation{get;set;}
 
    public void updateRDStation(){
        User u = [select contact.accountid, contact.account.parentid, contact.account.parent.RDStation_Client_ID__c, Contact.Account.Parent.Synchronize_RDStation_Mass_Transfer__c, Contact.Account.Parent.RDStation_Status_To_Block_Mass_Transfer__c from user where id = :UserInfo.getUserId()];
        boolean transferContacts = false;
		//system.debug('CALLED UPDATE RD STATION '+contactsToRdStation);
		if(contactsToRdStation != null && !contactsToRdStation.isEmpty()){
        	transferContacts = !String.isEmpty(u.contact.account.parent.RDStation_Client_ID__c) && u.Contact.Account.Parent.Synchronize_RDStation_Mass_Transfer__c;
			if(transferContacts){
				Map<String, Map<String, String>> statusToBlock; 
				Set<String> ids;
				Boolean skipStatusValidation;
				Boolean skipSyncContact;
				Map<String, Datetime> contactCheckedByDate = new Map<String, Datetime>();
				if(String.isEmpty(u.Contact.Account.Parent.RDStation_Status_To_Block_Mass_Transfer__c)){
					skipStatusValidation = true;
				}else{
					statusToBlock = (Map<String, Map<String, String>>) JSON.deserialize(u.Contact.Account.Parent.RDStation_Status_To_Block_Mass_Transfer__c, Map<String, Map<String, String>>.class);
					skipStatusValidation = statusToBlock.isEmpty(); 
					if(!skipStatusValidation){
						ids = new Set<String>();
						Map<String, String[]> statuses = new Map<String, String[]>();
						for(Contact ctt : contactsToRdStation){
							ids.add(ctt.ID);
							statuses.put(ctt.ID, new String [] {ctt.Lead_Stage__c, ctt.Status__c});
						}
						for(Client_Stage_Follow_Up__c checked : [SELECT Stage__c, Stage_Item__c , Client__c, Last_Saved_Date_Time__c FROM Client_Stage_Follow_Up__c WHERE Client__c IN :ids AND Agency_Group__c = :u.contact.account.parentid order by Client__c, Last_Saved_Date_Time__c]){
							if(statuses.get(checked.Client__c)[0] == checked.Stage__c && statuses.get(checked.Client__c)[1] == checked.Stage_Item__c){
								contactCheckedByDate.put(checked.Client__c, checked.Last_Saved_Date_Time__c);
							}
						}
					}
				}
				ids = new Set<String>();
				Date toCompare;
				String statusLimitDate;
				String[] dates;
				for(Contact ctt : contactsToRdStation){
					if(skipStatusValidation){
						ids.add(ctt.ID);
					}else{
						statusLimitDate = null;
						if(statusToBlock.get(ctt.Lead_Stage__c) != null && statusToBlock.get(ctt.Lead_Stage__c).containsKey(ctt.Status__c)){
							statusLimitDate = statusToBlock.get(ctt.Lead_Stage__c).get(ctt.Status__c);
							if(String.isEmpty(statusLimitDate)){
								skipSyncContact = true;
							}else{
								dates = statusLimitDate.split('/');
								toCompare = Date.newInstance(Integer.valueOf(dates[2]), Integer.valueOf(dates[1]), Integer.valueOf(dates[0]));
								skipSyncContact = contactCheckedByDate.get(ctt.ID) == null || contactCheckedByDate.get(ctt.ID).date() < toCompare;
							}
						}else{
							skipSyncContact = false;
						}
						
						if(!skipSyncContact){
							ids.add(ctt.ID);
						}else{
							system.debug('CONTACT SKIPPED '+ctt.ID);
						}
					}
				}
				if(!ids.isEmpty()){
					//system.debug('THE CONTACTS TO UPDATE '+JSON.serialize(ids));
					RDStationSaveLeadWebhook.updateBulkWithRDStation(ids, u.Contact.Account.ParentID);
				}
			}
        }
    }
	
	private boolean notBlank(String str){
		return str != null && str != '' ? true : false;
	}
	
	public void transferSelectedContacts(){

		contactsToRdStation = new List<Contact>();
		
		if(transferToUser != null){
			
			try {
			
				//User transferToContact = [Select AccountID, contactID from User where Id = :transferToUser];
				//String userContactID = [Select contactID from User where Id = :UserInfo.getUserId()].contactID;
				
				User transferToContact = new methodsWhithoutSharing().retrieveUser(transferToUser);
				User transferedBy = new methodsWhithoutSharing().retrieveUser(UserInfo.getUserId());
				User transferedFrom = new methodsWhithoutSharing().retrieveUser(transferFromUser);
				/*[SELECT AccountID, contactID, Contact.Account.Name, Contact.AccountId, ID, name FROM User WHERE ID = :transferToUser];
				User transferedBy = [SELECT contactID, Contact.Account.Name, Contact.AccountId, ID, name FROM User WHERE ID = :UserInfo.getUserId()];
				User transferedFrom = [SELECT contactID, Contact.Account.Name, Contact.AccountId, ID, name FROM User WHERE ID = :transferFromUser];*/
				
				User currentOwner = null;
				String userContactID = transferedBy.contactID;

				boolean isDestinationGroup = [select Parent.Destination_Group__c from Account where id = : transferToContact.AccountID].Parent.Destination_Group__c;
				
				Set<String> idContacts = new Set<String>();
				List<Contact> updateContacts = new List<Contact>();
				List<Custom_Note_Task__c> updateTasks = new List<Custom_Note_Task__c>();
				
				IPFunctions.ContactNoSharing cns = new IPFunctions.ContactNoSharing();
				String action = null;
				Map<String, IPClasses.RDStationHifyData> rdStationFields = null;
				list<string> listIds = new list<string>();
				for(Contact ctId : listContacts)
					if(ctId.isSelected__c)
						listIds.add(ctId.id);
				String sql = 'Select id, Email, isSelected__c, Name, Owner__r.ID, Ownership_History_Field__c, Status__c, Lead_Stage__c, CreatedDate, Destination_Country__c, Nationality__c, Expected_Travel_Date__c, Visa_Expiry_Date__c, ' +
							'( Select id from Custom_Notes_Tasks__r where Assign_To__c = :transferFromUser and Status__c != \'Completed\') ' + 
							'from Contact where id in :listIds ';
				for(Contact ct : Database.query(sql)){
					//if(ct.isSelected__c){
						for(Custom_Note_Task__c task : ct.Custom_Notes_Tasks__r){
							task.Assign_To__c = transferToUser;
							updateTasks.add(task);
						}
						
						ct.Lead_Assignment_To__c = transferToContact.ContactID;
						ct.Lead_Assignment_on__c = system.now();
						ct.Lead_assignment_By__c = userContactID;
						ct.Owner__c = transferToUser;
						if(!isDestinationGroup)
							ct.AccountID = transferToContact.AccountID;
						ct.Current_Agency__c = transferToContact.AccountID;
						ct.isSelected__c = false;

						action = 'Assigned';
						if(transferedFrom.ID == transferedBy.ID){
							action = 'Given';
						}
						if(transferToContact.ID == transferedBy.ID){
							action = 'Taken';
						}
				
						ct.Ownership_History_Field__c = JSON.serialize(cns.transferOwnerShip(ct, transferToContact, transferedFrom, transferedBy, action));

						idContacts.add(ct.ID);
						updateContacts.add(ct);

						//if(!String.isEmpty(ct.RDStation_Fields__c)){
                            //rdStationFields = (Map<String, IPClasses.RDStationHifyData>) JSON.deserialize(ct.RDStation_Fields__c, Map<String, IPClasses.RDStationHifyData>.class);
 
                            //if(rdStationFields.containsKey(transferedBy.Contact.Account.ParentID)){
                        if(ct.Email != null && !String.isEmpty(ct.Email.trim())){
							contactsToRdStation.add(ct); 
						}
                            //}
                            
                        //}
					//}				
				}
								
				update updateContacts;
				update updateTasks;

				List<Destination_Tracking__c> cycles = [SELECT ID, Name, Current_Agency__c FROM Destination_Tracking__c WHERE Client__c IN :idContacts AND Current_Cycle__c = true AND Agency_Group__c = :transferToContact.Contact.Account.ParentID];

				if(cycles != null && !cycles.isEmpty()){
					for(Destination_Tracking__c cycle : cycles){
						cycle.Current_Agency__c = transferToContact.Contact.AccountId;
					}
					update cycles;
				}

				Integer tContacts = totalContacts - updateContacts.size();
				Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, updateContacts.size() + ' contacts and ' + updateTasks.size() + ' tasks transferred successfully. ' + tContacts + ' contacts remaining.'));

				updateRDStation();
				
				loadContacts();
			} catch (Exception e) {
				Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			}
			
		} else {
			Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a user to transfer the selected contacts.'));
		}
	}
	
	public void transferSelectedTasks(){
		
		if(transferToUser != null){
			try {	
				
				List<Custom_Note_Task__c> updateTasks = new List<Custom_Note_Task__c>();
				
				for(Custom_Note_Task__c task : listTasks){
					if(task.Selected__c){
						task.Assign_To__c = transferToUser;
						task.Selected__c = false;
						updateTasks.add(task);
					}
				}
				
				update updateTasks;
				Integer tTasks = totalTasks - updateTasks.size();
				Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, updateTasks.size() + ' tasks transferred successfully. ' + tTasks + ' tasks remaining.'));
				
				loadContacts();
			} catch (Exception e) {
				Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			}
			
		}
		
	}

	public without sharing class methodsWhithoutSharing{
		public User retrieveUser(String id){
			return [SELECT AccountID, contactID, Contact.Account.Name, Contact.Account.ParentID, Contact.AccountId, ID, name FROM User WHERE ID = :id];
		}
	}
	
	
}