public class scCloneNationalityGroup {

	public Account school {get;set;}
	public String selectedNGFrom {get;set;}
	public List<SelectOption> optionsNGFrom {get;set;}
	public String selectedNGTo {get;set;}
	public List<SelectOption> optionsNGTo {get;set;}

	public boolean clonePrices { get { if(clonePrices == null) clonePrices = false; return clonePrices; } set; }
	public boolean cloneFees { get { if(cloneFees == null) cloneFees = false; return cloneFees; } set; }
	public boolean clonePromotions { get { if(clonePromotions == null) clonePromotions = false; return clonePromotions; } set; }

	public boolean clonedSuccess {get;set;}

	public integer totalPrices {get;set;}
	public integer totalFees {get;set;}
	public integer totalPromotions {get;set;}

	public integer TO_totalPrices {get;set;}
	public integer TO_totalFees {get;set;}
	public integer TO_totalPromotions {get;set;}

	public Map<String, Integer> mapTotalPrices { get;set; }
	public Map<String, Integer> mapTotalFees { get;set; }
	public Map<String, Integer> mapTotalPromotions { get;set; }

	public scCloneNationalityGroup(){ }

	public void setParams(){
		school = [select id, name from Account where id = :ApexPages.currentPage().getParameters().get('id')];

		setGroups();
	}

	private Set<String> groupsWithItems = new Set<String>();
	private Set<String> allGroups = new Set<String>();
	public void setGroups(){


		mapTotalPrices = new Map<String, Integer>();
		mapTotalFees = new Map<String, Integer>();
		mapTotalPromotions = new Map<String, Integer>();

		allgroups.add('Published Price');
		for(AggregateResult ar : [select Name from nationality_group__c where account__c = :school.id group by Name order by Name])
			allGroups.add( (String) ar.get('Name') );

		if(!allGroups.isEmpty()){

			//Check Prices
			for(AggregateResult ar : [select Nationality__c, count(id) total from Course_Price__c where Campus_Course__r.Campus__r.Parentid = :school.id and Nationality__c in :allGroups group by Nationality__c order by Nationality__c]){
				String nation = (String) ar.get('Nationality__c');
				groupsWithItems.add(nation);
				mapTotalPrices.put(nation, Integer.valueOf( ar.get('total') ) );
			}


			//Check Extra Fees
			for(AggregateResult ar : [select Nationality__c, count(id) total from Course_Extra_Fee__c where (Campus_Course__r.Campus__r.Parentid = :school.id or Campus__r.ParentId = :school.id) and Nationality__c in :allGroups group by Nationality__c order by Nationality__c]){
				String nation = (String) ar.get('Nationality__c');
				groupsWithItems.add(nation);
				mapTotalFees.put( nation, Integer.valueOf( ar.get('total') ) );
			}


			//Check Promotions
			for(AggregateResult ar : [select Nationality_Group__c, count(id) total from Deal__c where (Campus_Course__r.Campus__r.Parentid = :school.id or Campus_Account__r.ParentId = :school.id) and Nationality_Group__c in :allGroups group by Nationality_Group__c order by Nationality_Group__c]){
				String nation = (String) ar.get('Nationality_Group__c');
				groupsWithItems.add(nation);
				mapTotalPromotions.put( nation, Integer.valueOf( ar.get('total') ) );
			}


			if(!groupsWithItems.isEmpty()){
				optionsNGFrom = new List<SelectOption>();
				optionsNGFrom.add(new SelectOption('', '-- Select Nationality Group --'));
				for(String str : groupsWithItems)
					optionsNGFrom.add(new SelectOption(str, str));
			} else {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No nationality groups found.'));
			}

		}
	}

	public void changeNation(){
		totalPrices = mapTotalPrices.get(selectedNGFrom) == null ? 0 : mapTotalPrices.get(selectedNGFrom);
		totalFees = mapTotalFees.get(selectedNGFrom) == null ? 0 : mapTotalFees.get(selectedNGFrom);
		totalPromotions = mapTotalPromotions.get(selectedNGFrom) == null ? 0 : mapTotalPromotions.get(selectedNGFrom);

		optionsNGTo = new List<SelectOption>();
		optionsNGTo.add(new SelectOption('', '-- Select Nationality Group --'));
		for(String str : allGroups)
			if(str != selectedNGFrom)
				optionsNGTo.add(new SelectOption(str, str));

		if(selectedNGFrom == selectedNGTo){
			TO_totalPrices = 0;
			TO_totalFees = 0;
			TO_totalPromotions = 0;
		}

	}

	public void changeTONation(){
		TO_totalPrices = mapTotalPrices.get(selectedNGTo) == null ? 0 : mapTotalPrices.get(selectedNGTo);
		TO_totalFees = mapTotalFees.get(selectedNGTo) == null ? 0 : mapTotalFees.get(selectedNGTo);
		TO_totalPromotions = mapTotalPromotions.get(selectedNGTo) == null ? 0 : mapTotalPromotions.get(selectedNGTo);
	}

	public void cloneData(){
		clonedSuccess = false;
		SavePoint sp = Database.setSavepoint();
		Map<String, String> clonedItemsMap = new Map<String, String>();
		try {

			if(clonePrices){

				delete [select id from Course_Price__c where Campus_Course__r.Campus__r.Parentid = :school.id and Nationality__c = :selectedNGTo];

				List<Course_Price__c> clonedPrices = new List<Course_Price__c>();

				for(Course_Price__c cp : [select Account_Document_File__c, Availability__c, Campus_Course__c, Comments__c, CoursePriceID__c, Course_Available__c, Date_Start_From__c, Date_Start_To__c, Date_Start_Value__c,
												 From__c, Id, isSelected__c, isToCopy__c, Nationality__c, Price_per_week__c, Price_valid_from__c, Price_valid_until__c, To__c, Unavailable__c, unit_description__c, CloneCourseID__c
											from Course_Price__c where Campus_Course__r.Campus__r.Parentid = :school.id and Nationality__c = :selectedNGFrom]){

					Course_Price__c newprice = cp.clone(false, true, false, false);
					newPrice.CloneCourseID__c = cp.id;
					newPrice.Nationality__c = selectedNGTo;
					clonedPrices.add( newPrice );
				}

				Database.SaveResult[] MySaveResult = Database.insert(clonedPrices);

				List<String> priceids = new List<String>();
				for(Database.SaveResult sr : MySaveResult)
                    priceids.add(sr.getId());

	            for(Course_Price__c cp : [select id, CloneCourseID__c from Course_Price__c where id in :priceids])
	                clonedItemsMap.put(cp.CloneCourseID__c, cp.id);

			}

			if(cloneFees){

				delete [select id from Course_Extra_Fee__c where (Campus_Course__r.Campus__r.Parentid = :school.id or Campus__r.ParentId = :school.id) and Nationality__c = :selectedNGTo];

				List<Course_Extra_Fee__c> clonedFees = new List<Course_Extra_Fee__c>();
				for(Course_Extra_Fee__c cef : [Select Account_Document_File__c, Allow_change_units__c, Availability__c, Campus__c, Campus_Course__c, Campus_Parent__c, Combine_fees__c, Custom_Fee__c, date_paid_from__c,
													  date_paid_to__c, date_start_from__c, date_start_to__c, date_start_value__c, externalId__c, Extra_Fee__c, Extra_Fee_Country__c, ExtraFeeInterval__c,
													  Extra_fee_type__c, Extra_info__c, From__c, Keep_fee__c, Nationality__c, Optional__c, Product__c, To__c, Total_Value__c, Value__c
												from  Course_Extra_Fee__c where (Campus__r.ParentID = :school.id or Campus_Course__r.Campus__r.Parentid = :school.id) and Nationality__c = :selectedNGFrom]){

					Course_Extra_Fee__c newfee = cef.clone(false, true, false, false);
					newfee.externalID__c = cef.id;
					newfee.Nationality__c = selectedNGTo;
					clonedFees.add(newfee);
				}
				Database.SaveResult[] MySaveResult = Database.insert(clonedFees);

				List<String> feeids = new List<String>();
				for(Database.SaveResult sr : MySaveResult)
                    feeids.add(sr.getId());

				Map<String, String> mapfees = new Map<String, String>();
	            for(Course_Extra_Fee__c cf : [select id, externalID__c from Course_Extra_Fee__c where id in :feeids]){
	                clonedItemsMap.put(cf.externalID__c, cf.id);
					mapfees.put(cf.externalID__c, cf.id);
				}


				// related fees
				List<Course_Extra_Fee_Dependent__c> relatedFees = new List<Course_Extra_Fee_Dependent__c>();
				for(Course_Extra_Fee_Dependent__c dep : [select id, externalID__c, Account_Document_File__c, Course_Extra_Fee__c, Selected__c, Date_Paid_From__c, Date_Paid_To__c, Date_Start_From__c, Date_Start_To__c,
																Allow_Change_Units__c, Details__c, Product__c, From__c , Value__c, Optional__c, Keep_Fee__c, Extra_Fee_Interval__c, Course_Extra_Fee__r.Campus__c
                                                        from Course_Extra_Fee_Dependent__c where Course_Extra_Fee__c in :mapfees.keySet()]){

                    Course_Extra_Fee_Dependent__c cloneDep = dep.clone(false,true,false,false);
                    cloneDep.externalId__c = dep.id;
                    cloneDep.Course_Extra_Fee__c = mapfees.get(cloneDep.Course_Extra_Fee__c);
                    relatedFees.add(cloneDep);

                }

				MySaveResult = Database.insert(relatedFees);

				List<String> relatedFeeids = new List<String>();
				for(Database.SaveResult sr : MySaveResult)
                    relatedFeeids.add(sr.getId());

	            for(Course_Extra_Fee_Dependent__c cefd : [select id, externalId__c from Course_Extra_Fee_Dependent__c where id in :relatedFeeids])
	                clonedItemsMap.put(cefd.externalID__c, cefd.id);




			}

			if(clonePromotions){

				delete [select id from Deal__c where (Campus_Course__r.Campus__r.Parentid = :school.id or Campus_Account__r.ParentId = :school.id) and Nationality_Group__c = :selectedNGTo];

				List<Deal__c> clonedPromotions = new List<Deal__c>();
				for(Deal__c d : [Select Account_Document_File__c, Availability__c, Campus_Account__c, Campus_Course__c, Campus_Parent__c, Comments__c, Promotion_discount__c, Discounted_Promotion_Type__c,
										ExternalFileId__c, Extra_Fee__c, Extra_Fee_Dependent__c, Extra_Fee_Name__c, Extra_Fee_Type__c, Extra_Fee_Value__c, From__c, From_Date__c, Nationality_Group__c,
										Normal_Price__c, Number_of_Free_Weeks__c, Product__c, Promo_Price__c, Promotion_Name__c, Promotion_Type__c, Promotion_Weeks__c, School__c, School_Campus_course_name__c,
										Shown__c, To__c, To_Date__c, Total_Number_of_Weeks__c, Total_Price__c
								   from Deal__c where (Campus_Account__r.ParentID = :school.id or Campus_Course__r.Campus__r.Parentid = :school.id) and Nationality_Group__c = :selectedNGFrom]){

					Deal__c newdeal = d.clone(false, true, false, false);
					newdeal.ExternalFileId__c = d.id;
					newdeal.Nationality_Group__c = selectedNGTo;
					clonedPromotions.add(newdeal);
				}
				Database.SaveResult[] MySaveResult = Database.insert(clonedPromotions);

				List<String> promoids = new List<String>();
				for(Database.SaveResult sr : MySaveResult)
                    promoids.add(sr.getId());

	            for(Deal__c d : [select id, ExternalFileId__c from Deal__c where id in :promoids])
	                clonedItemsMap.put(d.ExternalFileId__c, d.id);

			}

			system.debug('@@ clonedItemsMap: ' + clonedItemsMap);

			//clone start dates
			if(!clonedItemsMap.isEmpty()){
				List<Start_Date_Range__c> clonedDates = new List<Start_Date_Range__c>();

				for(Start_Date_Range__c sdr : [select Account_Document_File__c, Campus__c, Comments__c, Course_Extra_Fee__c, Course_Extra_Fee_Dependent__c, Course_Price__c, From_Date__c, Number_of_Free_Weeks__c,
						  								Promotion__c, Promotion_Name__c, Promotion_Weeks__c, Selected__c, To_Date__c, Value__c
												from  Start_Date_Range__c
												where Course_Price__c in :clonedItemsMap.keySet() OR Course_Extra_Fee__c in :clonedItemsMap.keySet() OR Course_Extra_Fee_Dependent__c in :clonedItemsMap.keySet() OR
					  									Promotion__c in :clonedItemsMap.keySet()]){

					Start_Date_Range__c clonedSDR = sdr.clone(false, true, false, false);
					if(clonedSDR.Course_Price__c != null)
						clonedSDR.Course_Price__c = clonedItemsMap.get(clonedSDR.Course_Price__c);
					else if(clonedSDR.Course_Extra_Fee__c != null)
						clonedSDR.Course_Extra_Fee__c = clonedItemsMap.get(clonedSDR.Course_Extra_Fee__c);
					else if(clonedSDR.Course_Extra_Fee_Dependent__c != null)
						clonedSDR.Course_Extra_Fee_Dependent__c = clonedItemsMap.get(clonedSDR.Course_Extra_Fee_Dependent__c);
					else if(clonedSDR.Promotion__c != null)
						clonedSDR.Promotion__c = clonedItemsMap.get(clonedSDR.Promotion__c);

					clonedDates.add(clonedSDR);
				}

				Database.SaveResult[] MySaveResult = Database.insert(clonedDates);

			}

			setGroups();
			changeTONation();
			clonedSuccess = true;
			
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Information was successfully cloned.'));

		} catch (Exception e){
			Database.rollback(sp);
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
		}



	}


}