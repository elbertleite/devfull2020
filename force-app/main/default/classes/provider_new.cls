public with sharing class provider_new {

	public User currentUser {get{if(currentUser==null) currentUser = IpFunctions.getUserInformation(UserInfo.getUserId()); return currentUser;}set;}
	
	public Provider__c acco {get;set;}
	public list<String> strCountries {get;set;}
	public list<String> prodCountries {get{if(prodCountries == null) prodCountries = new list<String>(); return prodCountries;}set;}
	public list<SelectOption> allCountries {get;set;}
	private String pId {get;set;}
	public String selCountries {get;set;}
	public Boolean editM {get;set;}
	public Integer totProd {get;set;}
	private Integer counter {get{if(counter == null) counter = 0; return counter;}set;}
	public map<String, list<Start_Date_Range__c>> mapPriceRange {get;set;}
	public map<String, list<productDetails>> products {get;set;}
	public map<String, Boolean> hasDateRange {get;set;}
	public map<String, Boolean> hasRelProd {get;set;}
	private list<Quotation_list_products__c> lPriceItems {get{if(lPriceItems == null) lPriceItems = new list<Quotation_list_products__c>(); return lPriceItems;}set;}
	private set<Id> delPrice {get;set;}
	private set<Id> delStartDate {get;set;}


	//C O N S T R U C T O R
	public provider_new(){

		pId = ApexPages.currentPage().getParameters().get('id');

		if(pId != null && pId != ''){
			findProvider();
		}
		else{
			editM = true;
			acco = new Provider__c(Agency__c = ApexPages.currentPage().getParameters().get('acco'));
		}

		allCountries = new list<SelectOption>(IPFunctions.getAllCountries());
		strCountries = IPFunctions.getAllCountriesString();
	}

	//S A V E 	P R O V I D E R
	public PageReference saveProvider(){
		upsert acco;

		if(pId == null){
			PageReference pr = Page.provider_new;
			pr.getParameters().put('id', acco.id);
			pr.setRedirect(true);
			return pr;
		}
		else{
			editM = false;
			return null;
		}
	}
	
	//S H O W 	E D I T
	public void editMode(){
		editM = true;
	}

	//S E A R C H 	P R O V I D E R
	private void findProvider(){
		
		acco = [SELECT Provider_Name__c, Provider_Type__c, Require_File_to_Send_Email__c, Business_Email__c, Website__c, Business_Phone__c, Registration_Name__c, Type_of_Business_Number__c, Business_Number__c, Invoice_Street__c, Invoice_City__c, Invoice_State__c, Invoice_PostalCode__c, Invoice_Country__c, Invoice_to_Email__c, Agency__c FROM Provider__c WHERE Id = :pId];
		
		setupProducts();

		editM = false;
	}

	//E D I T 	P R O D U C T
	public void editProduct(){ 
		counter = 0;

		delPrice = new set<Id>();
		delStartDate = new set<Id>();

		product = [SELECT isProductPaidByAgency__c, isAlreadyPaidByAgency__c, Product_Name__c, Claim_Commission_Type__c, Description__c, Category__c, Group_Other__c, Currency__c, Quantity__c, Unit_description__c, Price__c, Commission_Type__c, Commission__c, Agency__c, Selected__c,Unit_type__c, Use_list_of_products__c, Countries__c, Available_for__c, isCommissionPerUnit__c FROM Quotation_Products_Services__c WHERE Id = :ApexPages.currentPage().getParameters().get('prod')];

		prodCountries = new list<String>();
		selCountries = null;

		if(product!= null){

			if(product.Countries__c != null){
				prodCountries = product.Countries__c.split(';');
				selCountries = product.Countries__c.replace(';', ',');
			}
			
			mapProductItem = new map<String, list<Quotation_list_products__c>>();
			// mapDateRange = new map<String, list<Start_Date_Range__c>>();

			String mapKey;
			for(Quotation_list_products__c qp : [SELECT Payment_Start_Date__c, Payment_End_Date__c, Quantity__c,  To__c, Quotation_Products_Services__c, Value__c, isSelected__c, counter__c,  (SELECT From_Date__c, To_Date__c, Value__c, Quotation_List_Product__c, Campus__c, Selected__c, counter__c FROM Start_Date_Ranges__r order by From_Date__c), Quotation_Products_Services__r.Currency__c FROM Quotation_List_Products__c WHERE Quotation_Products_Services__c = :product.Id AND Related_to_Payment__c = NULL order by Payment_Start_Date__c, Quantity__c]){

				lPriceItems.add(qp);

				mapKey = qp.Payment_Start_Date__c.format() + ' to ' + qp.Payment_End_Date__c.format();
				qp.counter__c = counter++;

				if(mapProductItem.containsKey(mapKey))
					mapProductItem.get(mapKey).add(qp);
				else
					mapProductItem.put(mapKey, new list<Quotation_List_Products__c>{qp});

				mapDateRange.put(mapKey+qp.counter__c, new list<Start_Date_Range__c>());
				

				for(Start_Date_Range__c dr : qp.Start_Date_Ranges__r){
					
					dr.counter__c = counter++;
					mapDateRange.get(mapKey+qp.counter__c).add(dr);

				}//end for
			}//end for
		}
	}

	//D E L E T E 	P R O D U C T
	public void deleteProds(){
		set<Id> prodIds = new set<Id>();
		list<Quotation_Products_Services__c> upProds = new list<Quotation_Products_Services__c>();

		for(productDetails p : products.get(ApexPages.currentPage().getParameters().get('cat')))
			if(p.product.Selected__c){
				prodIds.add(p.product.Id);
				p.product.isInactive__c = true;
				p.product.Selected__c = false;
				upProds.add(p.product);
			}

		//Search for Payment Dates
		map<Id,Quotation_List_Products__c> payIds = new map<Id,Quotation_List_Products__c>([SELECT Id FROM Quotation_List_Products__c WHERE Quotation_Products_Services__c IN :prodIds]);

		//Delete Start Dates Range
		Database.delete([SELECT Id FROM Start_Date_Range__c WHERE Quotation_List_Product__c IN :payIds.keySet()]);

		// Delete Payment Range
		Database.delete(new list<Id>(payIds.keySet()));

		//Delete Products + Payment Range as it has master
		//	Database.delete(new list<Id>(prodIds));
		update upProds;

		setupProducts();
	}

	//S E T U P 	P R O D U C T S
	public map<String, map<String, list<Quotation_list_products__c>>> mapRelated {get;set;}
	public void setupProducts(){
		refresh();

		totProd =  0;
		products = new map<String, list<productDetails>>();

		String mapKey;
		map<String, list<Quotation_list_products__c>> mPaymentRange = new map<String, list<Quotation_list_products__c>>();
		mapPriceRange = new map<String, list<Start_Date_Range__c>>();
		hasDateRange = new map<String, Boolean>();
		hasRelProd = new map<String, Boolean>();
		mapRelated = new map<String, map<String, list<Quotation_list_products__c>>>();

		//New Related fee
		// if(!mRelatedFees.containsKey(mapKey)){
		// 	mRelatedFees.put(mapKey, new map<String, list<Quotation_list_products__c>>{payKey => new list<Quotation_list_products__c>{productItem}});
		// 	hasFees = true;
		// }

		// //New Payment Range
		// else if(!mRelatedFees.get(mapKey).containsKey(payKey)){
		// 	mRelatedFees.get(mapKey).put(payKey, new list<Quotation_list_products__c>{productItem});
		// }

		// //Existing Payment Range
		// else{

		for(Quotation_Products_Services__c p : [SELECT Product_Name__c, Available_for__c, Claim_Commission_Type__c, Description__c, Category__c, Group_Other__c, Currency__c, Quantity__c, Unit_description__c, Price__c, Commission_Type__c, Commission__c, Agency__c, Countries__c, Unit_type__c, Use_list_of_products__c, Selected__c, Min_Payment_Start_Date__c, (SELECT Payment_Start_Date__c, Payment_End_Date__c, Quantity__c, To__c, Quotation_Products_Services__c, Quotation_Products_Services__r.Product_Name__c, Quotation_Products_Services__r.Unit_Type__c, Value__c, isSelected__c, Optional__c, Related_to_Payment__c, Fee__c, Fee__r.Product_Name__c, Fee__r.Unit_type__c, counter__c, Quotation_Products_Services__r.Currency__c FROM Quotation_list_products__r order by Related_to_Payment__c nulls first, Payment_Start_Date__c, Quantity__c) FROM Quotation_Products_Services__c WHERE Provider__c = :acco.Id AND isProduct_Fee__c = FALSE AND isInactive__c = FALSE order by Category__c, Product_Name__c]){

			mPaymentRange = new map<String, List<Quotation_list_products__c>>();

			
			for(Quotation_list_products__c qp : p.Quotation_list_products__r){

				if(qp.Related_to_Payment__c == null){
					mapKey = qp.Payment_Start_Date__c.format()+ ' to ' + qp.Payment_End_Date__c.format();

					if(mPaymentRange.containsKey(mapKey))
						mPaymentRange.get(mapKey).add(qp);
					else
						mPaymentRange.put(mapKey, new list<Quotation_list_products__c>{qp});

					// mapRelated.put(qp.Id, new list<Quotation_list_products__c>());
					mapRelated.put(qp.Id, new map<String, list<Quotation_list_products__c>>());
					hasRelProd.put(qp.Id, false);
				}
				else{
					String payKey = qp.Fee__r.Product_Name__c + ' -> Payment from ' + qp.Payment_Start_Date__c.format() + ' to ' + qp.Payment_End_Date__c.format();

					if(!mapRelated.get(qp.Related_to_Payment__c).containsKey(payKey))
						// mapRelated.get(qp.Related_to_Payment__c).add(qp);
						mapRelated.get(qp.Related_to_Payment__c).put(payKey, new list<Quotation_list_products__c>{qp});
					else
						mapRelated.get(qp.Related_to_Payment__c).get(payKey).add(qp);

					hasRelProd.put(qp.Related_to_Payment__c, true);
				}

				mapPriceRange.put(qp.Id, new list<Start_Date_Range__c>());
				hasDateRange.put(qp.Id, false);

			}//end for

			list<paymentRange> lpayrange = new list<paymentRange>();

			for(String k : mPaymentRange.keySet())
				lpayrange.add(new paymentRange(k, mPaymentRange.get(k)));
		

			if(products.containsKey(p.Category__c))
				products.get(p.Category__c).add(new productDetails(p, lpayrange));
			else
				products.put(p.Category__c, new list<productDetails>{new productDetails(p, lpayrange)});

			totProd++;

		}//end for


		if(!mapPriceRange.isEmpty()){
			for(Start_Date_Range__c dr : [SELECT From_Date__c, To_Date__c, Value__c, Quotation_List_Product__c, Selected__c, counter__c, Campus__c FROM Start_Date_Range__c WHERE Quotation_List_Product__c in :mapPriceRange.keyset() order by From_Date__c]){
				mapPriceRange.get(dr.Quotation_List_Product__c).add(dr);
				hasDateRange.put(dr.Quotation_List_Product__c, true);

			}//end for
		}
	}
	
	//S A V E 	P R O D U C T
	public PageReference saveproduct(){
		// if(selCountries == null || selCountries.trim() == ''){
		// 	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please select the countries.'));
		// 	return null;
		// }

		product.Provider__c = acco.Id;
		if(product.Agency__c == null)
			product.Agency__c = acco.Agency__c;
		product.Countries__c = selCountries.replace(',', ';');
		// product.Use_list_of_products__c = true;
		system.debug('product=====>' + product);
			
		upsert product;

		list<Quotation_list_products__c> listProductItem = new list<Quotation_list_products__c>();
		for(String key : mapProductItem.keyset()){
			for(Quotation_list_products__c qp : mapProductItem.get(key)){

				if(qp.Quotation_Products_Services__c == null)
					qp.Quotation_Products_Services__c = product.Id;
				listProductItem.add(qp);
			}//end for
		}//end for
		

		// Product Prices;
		upsert listProductItem;

		// Prices Date Range
		list<Start_Date_Range__c> allDates = new list<Start_Date_Range__c>();
		String mapKey;
		for(Quotation_list_products__c qp : listProductItem){
			mapKey = qp.Payment_Start_Date__c.format() + ' to ' + qp.Payment_End_Date__c.format()+qp.counter__c;

			for(Start_Date_Range__c dr : mapDateRange.get(mapKey)){
				dr.Quotation_List_Product__c = qp.Id;
				if(dr.Campus__c == null)
					dr.Campus__c = currentUser.Contact.AccountId; //TODO: CHECK IT
				allDates.add(dr);
			}//end for
		}//end for
		
		upsert allDates;

		if(delPrice != null && delPrice.size() > 0){

			for(Quotation_List_Products__c fee : [SELECT Id, (Select Id from Start_Date_Ranges__r) FROM Quotation_List_Products__c WHERE Related_to_Payment__c IN :delPrice]){
				delPrice.add(fee.Id);

				if(fee.Start_Date_Ranges__r != null)
					for(Start_Date_Range__c dr : fee.Start_Date_Ranges__r)
						delPrice.add(dr.Id);
			}


			Database.delete(new list<Id>(delPrice));
		}
		if(delStartDate != null && delStartDate.size() > 0)
			Database.delete(new list<Id>(delStartDate));

		// product = null;
		// productItem = null;
		// lPriceItems = null;
		// // mapProductItem.clear();
		// // mapDateRange.clear();
		// counter = null;

		// refresh();

		return null;
	}

	//C A N C E L 	P R O D U C T 	P O P	U P
	public void cancelProduct(){
		// product = null;
		// productItem = null;
		// lPriceItems = null;
		// // mapProductItem.clear();
		// // mapDateRange.clear();
		// counter = null;

		refresh();
	}

	private void refresh(){
		product = null;
		productItem = null;
		lPriceItems = null;
		counter = null;
		newDate = null;
		prodCountries = null;
		//strCountries = null;
		mapProductItem.clear();
		mapDateRange.clear();
		if(mRelatedFees != null)
			mRelatedFees.clear();
		// findProvider();
	}

	//A D D 	P R I C E
	public PageReference addPrice(){

		validateNewPrice(productItem);
		productItem = new Quotation_list_products__c(Payment_Start_Date__c = productItem.Payment_Start_Date__c, Payment_End_Date__c = productItem.Payment_End_Date__c);
		return null;
	}

	// V A L I D A T E 		N E W 		P R I C E	
	private boolean validateNewPrice(Quotation_list_products__c productItem){
		//Validate Date Range 
		system.debug('startDate==> ' + productItem.Payment_Start_Date__c);
		system.debug('endDate==> ' + productItem.Payment_End_Date__c);

		//Validate Date Range
		for(Quotation_list_products__c pi : lPriceItems){

			system.debug('compStart==> ' + pi.Payment_Start_Date__c);
			system.debug('compEnd==> ' + pi.Payment_End_Date__c);

			if(((pi.Payment_Start_Date__c >= productItem.Payment_Start_Date__c && pi.Payment_Start_Date__c <= productItem.Payment_End_Date__c) || (pi.Payment_End_Date__c >= productItem.Payment_Start_Date__c && pi.Payment_End_Date__c <= productItem.Payment_End_Date__c) || (pi.Payment_Start_Date__c < productItem.Payment_Start_Date__c && pi.Payment_End_Date__c > productItem.Payment_End_Date__c)) && !(pi.Payment_Start_Date__c == productItem.Payment_Start_Date__c && pi.Payment_End_Date__c == productItem.Payment_End_Date__c)){
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'The select payment date is covered by another range.'));
				return false;
			}
			
		}//end for

		String mapKey = productItem.Payment_Start_Date__c.format() + ' to ' + productItem.Payment_End_Date__c.format();
		productItem.counter__c = counter++;

		if(!mapProductItem.containsKey(mapKey))
			mapProductItem.put(mapKey, new list<Quotation_list_products__c>{productItem});
		else{
			
			list<Quotation_list_products__c> products = mapProductItem.get(mapKey);
			list<Quotation_list_products__c> upList = new list<Quotation_list_products__c>();

			//Validates Quantity  for the same price range
			boolean added = false;
			for(Integer i = 0; i < products.size(); i++){
				if(productItem.Quantity__c == products[i].Quantity__c){
					ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'The selected Quantity is already convered in the payment range.'));
					return false;
				}
				else{
					
					if(productItem.Quantity__c > products[i].Quantity__c && (productItem.Quantity__c <= products[i].To__c || products[i].To__c == null)){
						products[i].To__c = productItem.Quantity__c - 1;
						upList.add(products[i]);

						if(i + 1 == products.size())
							upList.add(productItem);
					}
				
					else if(productItem.Quantity__c < products[i].Quantity__c && !added){
						productItem.To__c = products[i].Quantity__c - 1;
						upList.add(productItem);
						upList.add(products[i]);
						added = true;
					}
					else{
						upList.add(products[i]);
					}
				}

				
			}//end for

			mapProductItem.put(mapKey, upList);
		}

		mapDateRange.put(mapKey+productItem.counter__c, new list<Start_Date_Range__c>());
		lPriceItems.add(productItem);

		return true;
	}

	//R E M O V E 	P R I C E
	public void removePrice(){
		try{
			String fee = ApexPages.currentPage().getParameters().get('fee');
			String dtGroup = ApexPages.currentPage().getParameters().get('dateGroup');
			Integer itemCounter = integer.valueOf(ApexPages.currentPage().getParameters().get('counter'));
			system.debug('fee=====>' + fee);
			system.debug('dateGroup=====>' + dtGroup);
			system.debug('counter=====>' + itemCounter);

			list<Quotation_list_products__c> upPrices = new list<Quotation_list_products__c>();
			list<Quotation_list_products__c> products;

			if(fee == null || fee == '')
				products = mapProductItem.get(dtGroup);
			else
				products = mRelatedFees.get(fee).get(dtGroup);


			for(Integer i = 0; i < products.size(); i++){
				system.debug('i=====>' + i);
				
				if( i + 1 == products.size() -1 && products[i + 1].counter__c == itemCounter){
					system.debug('1===>' + products[i].Related_Product_Name__c);

					products[i].To__c = null;
					upPrices.add(products[i]);

					if(products[i + 1].Id != null)
						delPrice.add(products[i + 1].Id);
				}
				else if(i + 1 < products.size()- 1 && products[i + 1].counter__c == itemCounter){
					system.debug('2===>' + products[i].Related_Product_Name__c);
					

					products[i].To__c = products[i + 1].To__c;
					upPrices.add(products[i]);

					if(products[i + 1].Id != null)
						delPrice.add(products[i + 1].Id);
				}
				else if(products[i].counter__c != itemCounter){
					system.debug('3===>' + products[i].Related_Product_Name__c);

					upPrices.add(products[i]);
				}
				else{
					if(products[i].Id != null)
						delPrice.add(products[i].Id);
				}
			}//end for

			// Deleting a Product Payment
			if(fee == null || fee == ''){
				
				if(upPrices.size() > 0)
					mapProductItem.put(dtGroup, upPrices);
				else
					mapProductItem.remove(dtGroup);

				for(Start_Date_Range__c dr : mapDateRange.get(dtGroup+itemCounter))
					if(dr.Id != null)
						delStartDate.add(dr.Id);

				mapDateRange.remove(dtGroup+itemCounter);

			}
			// Deleting an Extra Fee Payment
			else{
				if(upPrices.size() > 0)
					mRelatedFees.get(fee).put(dtGroup, upPrices);
				else{
					mRelatedFees.get(fee).remove(dtGroup);
				}

				if(mRelatedFees.get(fee).size()==0)
					mRelatedFees.remove(fee);

				system.debug('mRelatedFees===>' + mRelatedFees);

				for(Start_Date_Range__c dr : mapDateRange.get(fee+itemCounter))
					if(dr.Id != null)
						delStartDate.add(dr.Id);

				mapDateRange.remove(fee+itemCounter);

				system.debug('mapDateRange===>' + mapDateRange);
			}

			list<Quotation_list_products__c> oldPrices = new list<Quotation_list_products__c>(lPriceItems);
			
			lPriceItems = new list<Quotation_list_products__c>();
			for(Quotation_list_products__c q : oldPrices)
				if(!delPrice.contains(q.Id ))
					lPriceItems.add(q);
				else continue;
			

			system.debug('delPrice=====>' + delPrice);


		}
		catch(exception e){
			system.debug('error=====>' +e.getMessage() + '	line=====>' +e.getLineNumber());
		}
		finally{
			ApexPages.currentPage().getParameters().remove('dateGroup');
			ApexPages.currentPage().getParameters().remove('counter');
		}
	}

	//A D D 	S T A R T	D A T E
	public PageReference addStartDate(){
		try{
			String dtGroup = ApexPages.currentPage().getParameters().get('group');
			String itemCounter = ApexPages.currentPage().getParameters().get('counter');
			
			String mapKey = dtGroup + itemCounter;

			system.debug('mapKey===>' + mapKey);
			system.debug('mapDateRange===>' + mapDateRange);

			Start_Date_Range__c startDate = new Start_Date_Range__c(From_Date__c = newDate.From_Date__c, To_Date__c = newDate.To_Date__c, Value__c = newDate.Value__c, Counter__c = counter++);

			for(Start_Date_Range__c dr : mapDateRange.get(mapKey))

				if(((dr.From_Date__c >= startDate.From_Date__c && dr.From_Date__c <= startDate.To_Date__c) || (dr.To_Date__c >= startDate.From_Date__c && dr.To_Date__c <= startDate.To_Date__c) || (dr.From_Date__c < startDate.From_Date__c && dr.To_Date__c > startDate.To_Date__c)) && !(dr.From_Date__c == startDate.From_Date__c && dr.To_Date__c == startDate.To_Date__c)){
				// if((dr.From_Date__c >= startDate.From_Date__c && dr.From_Date__c <= startDate.To_Date__c) || (dr.To_Date__c >= startDate.From_Date__c && dr.To_Date__c <= startDate.To_Date__c)){
					ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'The select date is covered by another range.'));
					return null;
				}

			mapDateRange.get(mapKey).add(startDate);
			newDate = null;
			return null;
		}
		catch(exception e){
			system.debug(e.getMessage());
			return null;
		}
		finally{
			ApexPages.currentPage().getParameters().remove('group');
			ApexPages.currentPage().getParameters().remove('qtd');
			ApexPages.currentPage().getParameters().remove('stDt');
			ApexPages.currentPage().getParameters().remove('enDt');
			ApexPages.currentPage().getParameters().remove('value');
		}
	}

	// R E M O V E 	S T A R T	D A T E
	public void removeStartDate(){
			try{

			String dtGroup = ApexPages.currentPage().getParameters().get('dateGroup');
			Integer itemCounter = integer.valueOf(ApexPages.currentPage().getParameters().get('counter'));
			list<Start_Date_Range__c> upDates = new list<Start_Date_Range__c>();

			for(Start_Date_Range__c dr : mapDateRange.get(dtGroup)){
				if(dr.counter__c != itemCounter)
					upDates.add(dr);
				else if(dr.Id != null)
					delStartDate.add(dr.Id);
				else continue;
			}//end for

			mapDateRange.put(dtGroup, upDates);

			system.debug('mapDateRange=====>' + mapDateRange);

		}
		catch(exception e){
			system.debug('error=====>' +e.getMessage());
		}
		finally{
			ApexPages.currentPage().getParameters().remove('dateGroup');
			ApexPages.currentPage().getParameters().remove('counter');
		}
	}


	//R E T R I E V E 	P A Y M E N T 	D A T E
	public Quotation_list_products__c addRelated {get;set;}
	public void setupAddRelated(){
		String pId = ApexPages.currentPage().getParameters().get('prod');
		String pRange = ApexPages.currentPage().getParameters().get('range');
		String payId = ApexPages.currentPage().getParameters().get('pay');
		hasFees = false;

		for(productDetails p : products.get(ApexPages.currentPage().getParameters().get('cat'))){
			if(p.product.id == pId){
				for(paymentRange pr : p.lPayRange){
					if(pr.paymentRange == pRange){
						for(Quotation_list_products__c lp : pr.payItems){
							if(lp.Id == payId){
								addRelated = lp;
								mRelatedFees = new map<String, map<String, list<Quotation_list_products__c>>>();
								// mapDateRange = new map<String, list<Start_Date_Range__c>>();
								mFeesPayment = new map<String, list<Quotation_list_products__c>>();
								delPrice = new set<Id>();
								delStartDate = new set<Id>();
								String mapKey;
								String payKey;

								
								mRelatedFees.put(null, new map<String, list<Quotation_list_products__c>>{null => new list<Quotation_list_products__c>()});


								for(String key : mapRelated.get(lp.Id).keyset())
									for(Quotation_list_products__c qp : mapRelated.get(lp.Id).get(key)){

										// lPriceItems.add(qp);

										mapKey = qp.Fee__r.Product_Name__c;
										payKey = qp.Payment_Start_Date__c.format() + ' to ' + qp.Payment_End_Date__c.format();
										qp.counter__c = counter++;

										if(!mRelatedFees.containsKey(mapKey)){
											mRelatedFees.put(mapKey, new map<String, list<Quotation_list_products__c>>{payKey => new list<Quotation_list_products__c>{qp}});
											mFeesPayment.put(mapKey, new list<Quotation_list_products__c>{qp});
											hasFees = true;
										}
										//New Payment Range
										else if(!mRelatedFees.get(mapKey).containsKey(payKey)){
											mRelatedFees.get(mapKey).put(payKey, new list<Quotation_list_products__c>{qp});
											mFeesPayment.get(mapKey).add(qp);

										}
										//Existing Payment Range
										else{
											mRelatedFees.get(mapKey).get(payKey).add(qp);
											mFeesPayment.get(mapKey).add(qp);
										}

										mapDateRange.put(mapKey+qp.counter__c, new list<Start_Date_Range__c>());

										for(Start_Date_Range__c dr : mapPriceRange.get(qp.Id)){
											
											dr.counter__c = counter++;
											mapDateRange.get(mapKey+qp.counter__c).add(dr);

										}//end for
									}//end for
								break;
							}
						}//end for
						break;
					}
				}//end for
				break;
			}
		}//end for

		opFees = new list<SelectOption>{new SelectOption('none', '-- Select Fee --')};
		mFees = new map<String, Quotation_Products_Services__c>();
		

		for(Quotation_Products_Services__c f : [SELECT Id, Product_Name__c, Unit_type__c FROM Quotation_Products_Services__c WHERE Agency__c = :currentUser.Contact.AccountId AND isProduct_Fee__c = TRUE order by Product_Name__c]){
			opFees.add(new SelectOption(f.Id, f.Product_Name__c));
			mFees.put(f.Id, f);
		}//end for

		ApexPages.currentPage().getParameters().remove('prod');
		ApexPages.currentPage().getParameters().remove('range');
		ApexPages.currentPage().getParameters().remove('pay');

		productItem = new Quotation_list_products__c(Payment_Start_Date__c = addRelated.Payment_Start_Date__c, Payment_End_Date__c = addRelated.Payment_End_Date__c);
	}

	//A D D		R E L A T E D 		P R O D U C T
	public PageReference addRelatedProd(){

		list<Quotation_list_products__c> listProductItem = new list<Quotation_list_products__c>();
		for(String key : mFeesPayment.keyset()){
			system.debug('mFeesPayment.keyset===>' + key);
			system.debug('mFeesPayment.qp===>' + mFeesPayment.get(key));
			for(Quotation_list_products__c qp : mFeesPayment.get(key)){

				if(qp.Quotation_Products_Services__c == null){
					qp.Quotation_Products_Services__c = addRelated.Quotation_Products_Services__c;
					qp.Related_To_Payment__c = addRelated.Id;
				}
				listProductItem.add(qp);
			}//end for
		}//end for
		
			system.debug('listProductItem===>' + listProductItem);


		// Product Prices;
		if(listProductItem.size() > 0)
			upsert listProductItem;

		// Prices Date Range
		list<Start_Date_Range__c> allDates = new list<Start_Date_Range__c>();
		String mapKey;
		for(Quotation_list_products__c qp : listProductItem){
			mapKey = mFees.get(qp.Fee__c).Product_Name__c + qp.counter__c;

			system.debug('mapDateRange===>' + mapDateRange);
			system.debug('mapKey===>' + mapKey);

			if(mapDateRange.containsKey(mapKey))
				for(Start_Date_Range__c dr : mapDateRange.get(mapKey)){
					dr.Quotation_List_Product__c = qp.Id;
					if(dr.Campus__c == null)
						dr.Campus__c = currentUser.Contact.AccountId; 
					allDates.add(dr);
				}//end for
		}//end for
		
		if(allDates.size()>0)
			upsert allDates;

		system.debug('delPrice===>' + delPrice);
		if(delPrice != null && delPrice.size() > 0){

			for(Quotation_List_Products__c fee : [SELECT Id, (Select Id from Start_Date_Ranges__r) FROM Quotation_List_Products__c WHERE Related_to_Payment__c IN :delPrice]){
				delPrice.add(fee.Id);

				if(fee.Start_Date_Ranges__r != null)
					for(Start_Date_Range__c dr : fee.Start_Date_Ranges__r)
						delPrice.add(dr.Id);
			}


			Database.delete(new list<Id>(delPrice));
		}
		if(delStartDate != null && delStartDate.size() > 0)
			Database.delete(new list<Id>(delStartDate));

		// product = null;
		// productItem = null;
		// lPriceItems = null;
		// // mapProductItem.clear();
		// // mapDateRange.clear();
		// counter = null;

		// refresh();

		return null;
	}

	//A D D 	P R O D U C T 	F E E
	public void addProdFee(){
		insert productFee;

		opFees.add(new SelectOption(productFee.Id, productFee.Product_Name__c));
		mFees.put(productFee.Id, productFee);
		productItem.Fee__c = productFee.Id;

		productFee = null;
	}

	public map<String, map<String, list<Quotation_list_products__c>>> mRelatedFees {get;set;}
	private map<String, list<Quotation_list_products__c>> mFeesPayment {get;set;} //created only to facilitate loop when it is saving, not used on the page


	public PageReference addRelatedFee(){

		if(productItem.Fee__c == null){
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please select the related product name.'));
			return null;
		}

		String mapKey = mFees.get(productItem.Fee__c).Product_Name__c;
		productItem.counter__c = counter++;

		//Validate Date Range 
		system.debug('startDate==> ' + productItem.Payment_Start_Date__c);
		system.debug('endDate==> ' + productItem.Payment_End_Date__c);

		//Validate Date Range
		if(mFeesPayment.containsKey(mapKey)){
			for(Quotation_list_products__c pi : mFeesPayment.get(mapKey)){

				system.debug('compStart==> ' + pi.Payment_Start_Date__c);
				system.debug('compEnd==> ' + pi.Payment_End_Date__c);

				if(((pi.Payment_Start_Date__c >= productItem.Payment_Start_Date__c && pi.Payment_Start_Date__c <= productItem.Payment_End_Date__c) || (pi.Payment_End_Date__c >= productItem.Payment_Start_Date__c && pi.Payment_End_Date__c <= productItem.Payment_End_Date__c) || (pi.Payment_Start_Date__c < productItem.Payment_Start_Date__c && pi.Payment_End_Date__c > productItem.Payment_End_Date__c)) && !(pi.Payment_Start_Date__c == productItem.Payment_Start_Date__c && pi.Payment_End_Date__c == productItem.Payment_End_Date__c) && !delPrice.contains(pi.Id )){
					ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'The select payment date is covered by another range.'));
					return null;
				}
			}//end for

			// mFeesPayment.get(mapKey).add(productItem);
		}
		// else{
		// 	mFeesPayment.put(mapKey, new list<Quotation_list_products__c>{productItem});
		// }
		

		String payKey = productItem.Payment_Start_Date__c.format() + ' to ' + productItem.Payment_End_Date__c.format();

		//New Related fee
		if(!mRelatedFees.containsKey(mapKey)){
			mRelatedFees.put(mapKey, new map<String, list<Quotation_list_products__c>>{payKey => new list<Quotation_list_products__c>{productItem}});
			hasFees = true;
		}

		//New Payment Range
		else if(!mRelatedFees.get(mapKey).containsKey(payKey)){
			mRelatedFees.get(mapKey).put(payKey, new list<Quotation_list_products__c>{productItem});
		}

		//Existing Payment Range
		else{
			
			list<Quotation_list_products__c> products = mRelatedFees.get(mapKey).get(payKey);
			list<Quotation_list_products__c> upList = new list<Quotation_list_products__c>();

			system.debug('products==> ' + products);

			//Validates Quantity  for the same price range
			boolean added = false;
			for(Integer i = 0; i < products.size(); i++){
				if(productItem.Quantity__c == products[i].Quantity__c){
					ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'The selected Quantity is already covered for this product.'));
					return null;
				}
				else{
					
					if(productItem.Quantity__c > products[i].Quantity__c && (productItem.Quantity__c <= products[i].To__c || products[i].To__c == null)){
						products[i].To__c = productItem.Quantity__c - 1;
						upList.add(products[i]);

						if(i + 1 == products.size()){
							upList.add(productItem);
						}

					}
				
					else if(productItem.Quantity__c < products[i].Quantity__c && !added){
						productItem.To__c = products[i].Quantity__c - 1;
						upList.add(productItem);
						upList.add(products[i]);
						added = true;

					}
					else{
						upList.add(products[i]);
					}
				}
			}//end for

			mRelatedFees.get(mapKey).put(payKey, upList);

		}

		//Validate Date Range
		if(mFeesPayment.containsKey(mapKey)){
			mFeesPayment.get(mapKey).add(productItem);
		}
		else{
			mFeesPayment.put(mapKey, new list<Quotation_list_products__c>{productItem});
		}

		mapDateRange.put(mapKey+productItem.counter__c, new list<Start_Date_Range__c>());

		productItem = new Quotation_list_products__c(Fee__c = productItem.Fee__c, Optional__c = productItem.Optional__c, Payment_Start_Date__c = addRelated.Payment_Start_Date__c, Payment_End_Date__c = addRelated.Payment_End_Date__c);
		return null;
	}
	

	// S E T 	C H A N G E		P R O V I D E R
	public list<Quotation_Products_Services__c> toChangeProv {get;set;}
	public String selProvider {get;set;}
	public list<SelectOption> providers {get;set;}
	public void setChangeProviders(){
		
		toChangeProv = new list<Quotation_Products_Services__c>();

		for(productDetails p : products.get(ApexPages.currentPage().getParameters().get('cat')))
			if(p.product.Selected__c)
				toChangeProv.add(p.product);
			else continue;

		providers = new list<SelectOption>();
		set<String> agNames = new set<String>();
		for(Provider__c p : [SELECT Id, Provider_Name__c, Agency__r.Name FROM Provider__c WHERE Agency__c != null order by Agency__r.Name, Provider_Name__c]){
			
			if(!agNames.contains(p.Agency__r.Name)){
				agNames.add(p.Agency__r.Name);
				SelectOption op = new SelectOption(p.Id, '<optgroup label=\'Providers from '+ p.Agency__r.Name+'\' style=\'background-color:white;\'></optgroup>');
				op.setEscapeItem(false);
				providers.add(op);
			}

			SelectOption op = new SelectOption(p.Id, p.Provider_Name__c);
			op.setEscapeItem(false);

			providers.add(op);
		}//end for

		selProvider = pId;

	}

	public void saveProviderChange(){
		for(Quotation_Products_Services__c p :toChangeProv){
			p.Provider__c = selProvider;
			p.Selected__c = false;
		}

		update toChangeProv;
		refresh();
		findProvider();
	}

	public list<SelectOption> opFees {get;set;}
	public map<String, Quotation_Products_Services__c> mFees {get;set;}
	//CLASSES AND FILTERS
	public class productDetails{
		public Quotation_Products_Services__c product {get;set;}
		public list<paymentRange> lPayRange {get;set;}

		public productDetails(Quotation_Products_Services__c product, list<paymentRange> lPayRange){
			this.product = product;
			this.lPayRange = lPayRange;
		}
	}

	public class paymentRange{
		public String paymentRange {get;set;}
		public list<Quotation_list_products__c> payItems {get;set;}

		public paymentRange(String paymentRange, list<Quotation_list_products__c> payItems){
			this.paymentRange = paymentRange;
			this.payItems = payItems;
		}
	}

	public List<SelectOption> claimCommOpt {get{

		if(claimCommOpt == null){
			claimCommOpt = new List<SelectOption>();
		
			Schema.DescribeFieldResult fieldResult = Schema.Quotation_Products_Services__c.Claim_Commission_Type__c.getDescribe();
			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
			for( Schema.PicklistEntry f : ple)
				claimCommOpt.add(new SelectOption( f.getValue(), f.getLabel()));
			
		}
		return claimCommOpt;
	}set;}

	public List<SelectOption> availableForOpt {get{

		if(availableForOpt == null){
			availableForOpt = new List<SelectOption>();
		
			Schema.DescribeFieldResult fieldResult = Schema.Quotation_Products_Services__c.Available_for__c.getDescribe();
			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
			for( Schema.PicklistEntry f : ple)
				availableForOpt.add(new SelectOption( f.getValue(), f.getLabel()));
			
		}
		return availableForOpt;
	}set;}

	public Quotation_Products_Services__c product {get{
		if(product == null) 
			product = new Quotation_Products_Services__c(Provider__c = pId, Commission__c = 0); 
		return product;}set;}

	public Quotation_Products_Services__c productFee {get{
		if(productFee == null) 
			productFee = new Quotation_Products_Services__c(isProduct_Fee__c = true, Agency__c = currentUser.Contact.AccountId, Provider__c = pId); 
		return productFee;}set;}

	public List<SelectOption> unitsRange{get{ 
		if(unitsRange == null)
			unitsRange = xCourseSearchFunctions.getUnitsRange();
        return unitsRange;
	}set;}

	public List<SelectOption> currencies{get{
		if(currencies == null)
			if(ApexPages.currentPage().getParameters().get('gpid') != null)
				currencies = xCourseSearchFunctions.getAgencyCurrencies(currentUser.Contact.Account.ParentId, null);
			else
				currencies = xCourseSearchFunctions.getAgencyCurrencies(currentUser.Contact.AccountId, null);
		return currencies;
	}set;}

	public list<SelectOption> commissionType{
	get{
		if(commissionType == null){
			commissionType = new list<SelectOption>(); 
			commissionType.add(new SelectOption('0', '%'));
			commissionType.add(new SelectOption('1', 'Cash'));
		}
		return commissionType;
	}set;}

	public Quotation_list_products__c productItem{
	get{
		if(productItem == null) 
			productItem = new Quotation_list_products__c(); 
		return productItem;
	}set;}

	public map<String, list<Quotation_list_products__c>> mapProductItem{
	get{
		if(mapProductItem == null) 
			mapProductItem = new map<String, list<Quotation_list_products__c>>(); 
		return mapProductItem;
	}set;}
	public Integer hasItems { get { return mapProductItem.size(); } } 
	public Boolean hasFees {get;set;} 
	
	public map<String, list<Start_Date_Range__c>> mapDateRange{
	get{
		if(mapDateRange == null) 
			mapDateRange = new map<String, list<Start_Date_Range__c>>(); 
		return mapDateRange;
	}set;}

	public Start_Date_Range__c newDate{
	get{
		if(newDate == null) 
			newDate = new Start_Date_Range__c(Value__c = 0); 
		return newDate;
	}set;}
	
	// ------------------------			F I L E 	U P L O A D 		------------------------

	public transient String fileBody {get;set;}
	
	public void importCSVFile(){
		
	    String[] csvFileLines = fileBody.split('\n'); 
	    System.debug('==>csvFileLines: '+csvFileLines);
		try{
			for(Integer i=1;i<csvFileLines.size();i++){
				System.debug('==>csvFileLines[i]: '+csvFileLines[i]);

				string[] csvRecordData = csvFileLines[i].split(',');

					Quotation_list_products__c productItem = new Quotation_list_products__c(Payment_Start_Date__c = Date.valueOf(convertDate(csvRecordData[0].trim())), Payment_End_Date__c = Date.valueOf(convertDate(csvRecordData[1].trim())), Quantity__c = Integer.valueOf(csvRecordData[2].trim()), Value__c = Decimal.valueOf(csvRecordData[3].trim()));

					validateNewPrice(productItem);


			
			} //end for
		}catch(exception e){
			system.debug('error=====>' +e.getMessage() + '	line=====>' +e.getLineNumber());
		}
	}

	private String convertDate(String fDate){
		list<String> lDate = fDate.split('/');
		return  lDate[2] + '-' + lDate[1] + '-' + lDate[0];
	}


}