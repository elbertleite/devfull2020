public class visitsmobile extends VisitsController{
	
	public class ContactDetails{
		public String id {get;set;}
		public String name {get;set;}
		public String firstName {get;set;}
		public boolean hasVisits {get;set;}		
	}
	
	@RemoteAction
    public static ContactDetails searchEmail(String email){
		ContactDetails cd = null;
    	try{
    		system.debug('Email===' + email);    		
    		Contact result = [Select id, name, FirstName from contact where email = :email];   	    		   		
    		if(result != null){
				CD = new ContactDetails();  
    			cd.Id = result.Id;
    			cd.Name = result.Name;
    			cd.firstName = result.FirstName;
    			cd.hasVisits = hasVisit(cd.Id);
				system.debug('result===' + result);				   			
    		}    		
    		
    	}catch(Exception e){
    		return null;
    	}
		return cd; 
    }
    
    
    @RemoteAction
    public static Boolean hasVisit(String contactId){
    	// try{
			List<Visit__c> pending = new List<Visit__c>();
			pending = [Select Id, Contact__c, Contact__r.Name, Staff_Requested__c, Waiting_Time_Seconds__c , createdDate
						From Visit__c  
							Where contact__c = :contactId and Status__c in ('Waiting', 'Accepted', 'Transferred')];
							
			if(pending.size()>0)
				return true;
			else 
				return false;
    		
    	// }catch(Exception e){
    	// 	return null;
    	// }
    }

	@RemoteAction
    public static Boolean verifyPassword(String pwd){

		// try{
			String result = lwcFrontDesk.verifyPassword(pwd);							
			if (result == 'success'){
				return true;
			}
			else{ 
				return false;
			}
    		
    	// }catch(Exception e){
    	// 	return false;
    	// }
	}   
   
}