@isTest
private class agreement_details_test {

	public static Account school {get;set;}
	public static Account agency {get;set;}
	public static Account campus {get;set;}
	public static Course__c course {get;set;}
	public static Campus_Course__c campusCourse {get;set;}

	@testSetup static void setup() {
		TestFactory tf = new TestFactory();

		agency = tf.createAgency();
		school = tf.createSchool();
		campus = tf.createCampus(school, agency);
		course = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);

		//Agreement Parties
		agreements_new.CallResult result = agreements_new.saveAgreement(null, 'Agreement Test', agency.Id, 'Test User', '2018-02-12', 'Direct', 'School', school.Id + ';' + agency.id, 'All Regions', null, null, 'true', 'true');

		//Agreement Details
		result = agreements_new.saveGeneralInfo(result.agreementId, '2018-02-12', null, true, 'School Contact Details', 'Nationality Exc.', 'Agree Duration', 'Enroll Conditions', 'Course Payment', 'Refund Cancel', 'Extra Info', false, true, false, false, '');

		//Commission
		ApexPages.currentPage().getParameters().put('id', result.agreementId);
		agreement_commissions comm = new agreement_commissions();

		comm.coursesCommission[0].schSetup = '3;;true;;false';

		comm.coursesCommission[0].SchoolCommissionObj.School_Commission__c = 10;
		comm.coursesCommission[0].SchoolCommissionObj.School_Commission_Type__c = 0;

		comm.coursesCommission[0].listCampus[0].campusCommissionObj.Campus_Commission__c = 20;
		comm.coursesCommission[0].listCampus[0].campusCommissionObj.Campus_Commission_Type__c = 0;

		comm.coursesCommission[0].listCampus[0].listCourseType[0].courseTypeCommissionObj.Course_Type_Commission__c = 30;
		comm.coursesCommission[0].listCampus[0].listCourseType[0].courseTypeCommissionObj.Course_Type_Commission_Type__c = 0;

		comm.coursesCommission[0].listCampus[0].listCourseType[0].listCourseCommission[0].Course_Commission__c = 200;
		comm.coursesCommission[0].listCampus[0].listCourseType[0].listCourseCommission[0].Course_Commission_Type__c = 1;

		ApexPages.currentPage().getParameters().put('isAvailable', 'true');
		comm.saveCommission();

	}

	static testMethod void viewDetails(){
		String agreementId = [Select Id from Agreement__c limit 1].Id;

		ApexPages.currentPage().getParameters().put('id', agreementId);
		agreement_details test = new agreement_details();
	}

	static testMethod void cancelAgreement(){
		String agreementId = [Select Id from Agreement__c limit 1].Id;

		agreement_details.cancelAgreement(agreementId, 'Cancel Reason');
	}

	static testMethod void userProfile(){
		agreement_details.UserProfile uProfile = agreement_details.getUserProfile();
	}

	
}