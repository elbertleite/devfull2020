public class scCloneSchoolCourse {
	
	private String campusID;
    private String schoolID;
    
	public scCloneSchoolCourse(){
		
	}
 	
 	
    public void setParams(){
    	campusID = ApexPages.currentPage().getParameters().get('cpid');
    	schoolid = ApexPages.currentPage().getParameters().get('id');
    	getCampuses();
    }
    
    public String selectedCampus {get;set;}
    private List<SelectOption> campuses;
    public List<SelectOption> getCampuses(){
    	if(campuses == null){
    		campuses = new List<selectoption>();
    		campuses.add(new SelectOption('', '-- Select Campus --'));
    		for(Account campus : [select id, name from Account where RecordType.Name = 'Campus' and ParentID = :schoolID and id != :campusID order by Name])
    			campuses.add(new SelectOption(campus.id, campus.name));
    	}
    	return campuses;
    }
    
    public void searchCourses(){    	
    	populateCoursesMap();
    }
    
    public Map<String, Map<String, List<Campus_Course__c>>> courses {get;set;}
    private List<Campus_Course__c> campusCourses;
    public boolean hasCourses { get { if(hasCourses == null) hasCourses = false; return hasCourses; } set; }
    private void populateCoursesMap(){
    	campusCourses = new List<Campus_Course__c>();
    	courses = new Map<String, Map<String, List<Campus_Course__c>>>();
    	if(selectedCampus != ''){
	    	Set<ID> existingCourses = new Set<ID>();
	    	for(Campus_Course__c c: [select Course__c from Campus_Course__c where Campus__c = :campusID])
	    		existingCourses.add(c.Course__c);
	    	
	    	for(Campus_Course__c cc : [select id, Is_Available__c, Available_From__c, Average_Students_Class__c, Campus__c, Campus_Country__c, Language_Level_Required__c, Length_of_each_Lesson__c,
												Minimum_age_requirement__c, Period__c, Time_Table__c, Unavailable_By__c, Unavailable_Date__c, Unavailable_Reason__c, Visa_Type__c,
				    							Course__c, Course__r.Name, Course__r.Course_Type__c, Course__r.Sub_Type__c, Course__r.Only_Sold_in_Blocks__c, Course__r.Package__c, Course__r.Course_Unit_Length__c, 
				    							Course__r.Hours_Week__c, Course__r.Optional_Hours_Per_Week__c, Course__r.Minimum_length__c, Course__r.Maximum_length__c, Course__r.Course_Unit_Type__c, Course__r.Course_Category__c, Course__r.Type__c
				    					from Campus_Course__c where Campus__c = :selectedCampus and Course__c not in :existingCourses
				    					order by Course__r.Course_Category__c, Course__r.Type__c, Course__r.Name]){
				
				cc.is_Selected__c = false;	    							
	    		campusCourses.add(cc);
	    							
	    		if(courses.containsKey(cc.Course__r.Course_Category__c)){
	    			if(courses.get(cc.Course__r.course_category__c).containsKey(cc.Course__r.type__c))
	    				courses.get(cc.Course__r.course_category__c).get(cc.Course__r.type__c).add(cc);
	    			else
	    				courses.get(cc.Course__r.course_category__c).put(cc.Course__r.type__c, new List<Campus_Course__c>{cc});
	    		} else
	    			courses.put(cc.Course__r.Course_Category__c, new Map<String, List<Campus_Course__c>>{ cc.Course__r.type__c => new List<Campus_Course__c>{cc} });
									
	    	}
    	}
    	
    	
    	hasCourses = !campusCourses.isEmpty();
    	
    }
    
    public void saveCourses(){
    	
    	List<Campus_Course__c> newCC = new List<Campus_Course__c>();
    	for(Campus_Course__c cc : campusCourses){
    		if(cc.is_Selected__c){
    			Campus_Course__c newCampusCourse = cc.clone(false, true, false, false);
    			newCampusCourse.Campus__c = campusID;
    			newCC.add(newCampusCourse);
    		}	
    	}
    	
    	insert newCC;
    	
    	String successMessage = newCC.size() > 1 ? newCC.size() + ' courses created.' : '1 course created.';
    	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, successMessage);
       	ApexPages.addMessage(myMsg);
    	
    	populateCoursesMap();
    	
    	
    }
    
    public PageReference cancel(){
    	PageReference page = new PageReference('/apex/scSchoolCourses?id=' + schoolId + '&cpid='+ApexPages.currentPage().getParameters().get('cpID')+'&currentPage='+ ApexPages.currentPage().getParameters().get('currentPage'));
        page.setRedirect(true);
        return page;
    }
 	   
}