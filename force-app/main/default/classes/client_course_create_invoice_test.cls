/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class client_course_create_invoice_test {

    static testMethod void myUnitTest() {
       
       TestFactory tf = new TestFactory();
       
       Account school = tf.createSchool();
       Account agency = tf.createAgency();
       Contact emp = tf.createEmployee(agency);
       User portalUser = tf.createPortalUser(emp);
       Account campus = tf.createCampus(school, agency);
       Course__c course = tf.createCourse();
       Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
       
       Test.startTest();
       system.runAs(portalUser){
       	
       	   Contact client = tf.createLead(agency, emp);
       	   client_course__c booking = tf.createBooking(client);
           client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
           client_course__c courseCredit =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
           courseCredit.isCourseCredit__c = true;
           courseCredit.Credit_Available__c = 1000;
           courseCredit.Credit_Valid_For_School__c = school.id;
           update courseCredit;
       
           client_course__c deposit =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
           deposit.isDeposit__c = true;
           deposit.Deposit_Total_Available__c = 1000;
           deposit.Deposit_Total_Used__c = 0;
           update deposit;
           
           client_product_service__c bkproduct =  tf.createCourseProduct(booking, agency);
       	   List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
       	
	       ApexPages.currentPage().getParameters().put('cs', instalments[0].id);
	       ApexPages.currentPage().getParameters().put('pd', bkproduct.id);
	       ApexPages.currentPage().getParameters().put('ct', client.id);
	       
	       client_course_create_invoice testClass = new client_course_create_invoice();
	       
	       testClass.createInvoice();
	       
	       String invoiceId = testClass.invoice.id;
	       
	       ApexPages.currentPage().getParameters().put('id', invoiceId);
	       
	       testClass = new client_course_create_invoice();
	       
	       
	       Contact receivedOn = testClass.receivedOn;
	       List<SelectOption> agencyGroupOptions = testClass.agencyGroupOptions;
		   testClass.changeAgencyGroup();
		   testClass.changeAgency();
		   List<SelectOption> agencyOptions = testClass.agencyOptions;
		   List<SelectOption> userOptions = testClass.userOptions;
	
		   list<selectOption> depositListType =  testClass.getdepositListType();
			
		   system.debug('invoice.Total_Value__c===>' + testClass.invoice.Total_Value__c);	
			
			
		   testClass.newPayDetails.Payment_Type__c = 'Deposit';
		   testClass.newPayDetails.Value__c = 0;
		   testClass.addPaymentValue();
		   testClass.newPayDetails.Value__c = 60;
		   testClass.addPaymentValue();
		   testClass.newPayDetails.Value__c = 50;
		   testClass.newPayDetails.Date_Paid__c = Date.today().addDays(1);
		   testClass.addPaymentValue();
		   
		   testClass.newPayDetails.Date_Paid__c = Date.today();
		   testClass.addPaymentValue();
		   
		   
		   ApexPages.currentPage().getParameters().put('index', '0');
		   testClass.removePaymentValue();
		   
		   testClass.newPayDetails.Value__c = 50;
		   testClass.newPayDetails.Payment_Type__c = 'School Credit';
		   testClass.addPaymentValue();
		   ApexPages.currentPage().getParameters().remove('index');
		   ApexPages.currentPage().getParameters().put('index', '1');
		   testClass.removePaymentValue();
		   
		   testClass.newPayDetails.Value__c = 50;
		   testClass.newPayDetails.Payment_Type__c = 'Pds';
		   testClass.addPaymentValue();
		   ApexPages.currentPage().getParameters().put('index', '2');
		   testClass.removePaymentValue();
		   
		   testClass.newPayDetails.Value__c = 30;
		   testClass.newPayDetails.Payment_Type__c = 'offShore';
		   testClass.receivedOn.Expected_Travel_Date__c = system.today();
		   testClass.addPaymentValue();
		   ApexPages.currentPage().getParameters().put('index', '3');
		   testClass.removePaymentValue();
		   
		   testClass.newPayDetails.Value__c = 2200;
		   testClass.selectedUser = portalUser.id;
		   testClass.selectedAgency = agency.id;
		   testClass.newPayDetails.Payment_Type__c = 'School Credit';
		   testClass.addPaymentValue();
		   testClass.savePayment();
		
	       testClass.cancelGroupedInvoice();
       }
       Test.stopTest();
    }
}