public with sharing class providers_list {

	public User currentUser {get{if(currentUser==null) currentUser = IpFunctions.getUserInformation(UserInfo.getUserId()); return currentUser;}set;}
	public list<Provider__c> lproviders {get;set;}
	public Account agency {get;set;}
	public providers_list() {
		searchProvider();

		agency = [SELECT Name FROM Account WHERE Id = :ApexPages.CurrentPage().getParameters().get('id')];
	}

	public void searchProvider(){
		lproviders = [SELECT Id, Provider_Name__c, Business_Phone__c, Business_Email__c, Website__c, Provider_Type__c FROM Provider__c WHERE Agency__c = :ApexPages.CurrentPage().getParameters().get('id') order by Provider_Name__c];
	}
}