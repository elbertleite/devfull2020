@isTest
private class ClientPageContactDetailsController_Test{
	static testMethod void myUnitTest() {

		TestFactory tf = new TestFactory();
		
		Account agency = tf.createAgency();

		Account agencyGroup = tf.createAgencyGroup();

		tf.createChecklists(agency.Parentid);

		Contact emp = tf.createEmployee(agency);
		
		Contact lead = tf.createLead(agency, emp);
		lead.Ownership_History_Field__c = '[{"toUserID":"005O0000004bBaSIAU","toUser":"Patricia Greco","toAgencyID":"0019000001MjqIIAAZ","toAgency":"IP Australia Sydney (HO)","fromUserID":null,"fromUser":null,"fromAgencyID":null,"fromAgency":null,"createdDate":"2018-09-18T04:17:12.024Z","createdByUserID":"005O0000004bBaSIAU","createdByUser":"Patricia Greco","action":"Taken"}]';
		lead.Lead_Stage__c = 'Stage 1';
		update lead;

		Contact client = tf.createClient(agency);

		User portalUser = tf.createPortalUser(emp);

		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);

       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);

		Quotation__c q1 = tf.createQuotation(lead, campusCourse);
		Quotation__c q2 = tf.createQuotation(lead, campusCourse);

		Destination_Tracking__c dt = new Destination_tracking__c();
		dt.Client__c = lead.id;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
		dt.Expected_Travel_Date__c = system.today().addMonths(3);
		dt.lead_Cycle__c = false;
		insert dt;

		Client_Checklist__c checklist = new Client_Checklist__c();
		checklist.Agency__c = agency.id;
		checklist.Agency_Group__c = agency.Parentid;
		checklist.Destination__c = lead.Destination_Country__c;
		checklist.Checklist_Item__c = 'aaa';
		checklist.Client__c = lead.id;
		checklist.Destination_Tracking__c = dt.id;
		insert checklist;
		
		Test.startTest();
     	system.runAs(portalUser){
			Apexpages.currentPage().getParameters().put('id', lead.id);
			ClientPageContactDetailsController controller = new ClientPageContactDetailsController();
			controller.showHideFormEditClientDetails();
			controller.saveClientDetails();
			controller.openCloseFormNewContactDetail();
			controller.saveNewContactDetail();
			controller.saveAndNewContactDetail();
			controller.saveContactDetail();
			controller.loadFormsOfContactDetailsPage();
			Apexpages.currentPage().getParameters().put('idValue', '12345');
			Apexpages.currentPage().getParameters().put('type', 'webContact');
			controller.markObjectForDeletion();
			Apexpages.currentPage().getParameters().put('idValue', '12345');
			Apexpages.currentPage().getParameters().put('type', 'phoneContact');
			controller.markObjectForDeletion();
			Apexpages.currentPage().getParameters().put('idValue', '12345');
			Apexpages.currentPage().getParameters().put('type', 'socialNetwork');
			controller.markObjectForDeletion();
			controller.getLanguageOptions();
			controller.getCountries();
			controller.getPhoneContactTypes();
			controller.getSocialNetworkTypes();
			controller.getAllTypesOfContact();
			controller.getLeadSourceSpecific();
			controller.openCloseFormNewVisa();
			controller.saveNewVisa();
			controller.openCloseFormNewVisa();
			controller.saveAndNewVisa();
			controller.getVisaCountries();
			controller.getVisaTypes();
			controller.getVisaSubclass();
		}
	}
}