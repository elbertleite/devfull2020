public with sharing class client_course_create_invoice {
    private list<string> coursesId;
    private list<string> productsId;
    private string contactId;
    public Invoice__c invoice{get; set;}
    public courseInstalllmentDetails installmentsInvoice{get{if(installmentsInvoice == null) installmentsInvoice = new courseInstalllmentDetails(); return installmentsInvoice;} set;}
    public productsDetails productsInvoice{get{if(productsInvoice == null) productsInvoice = new productsDetails(); return productsInvoice;} set;}
    private Contact contactDetails {get;set;}
    public client_course_create_invoice(){
        string invId = ApexPages.currentPage().getParameters().get('id');
        string cs = ApexPages.currentPage().getParameters().get('cs');
        string pd = ApexPages.currentPage().getParameters().get('pd');
        contactId = ApexPages.currentPage().getParameters().get('ct');

        if(contactId!=null && contactId!='')
            contactDetails = [SELECT Owner__r.AccountId, Owner__r.Contact.Department__c FROM Contact WHERE id = :contactId limit 1];

        if(invId != null  && invId != ''){
            invoice = [Select Name, Id, Total_Value__c, Total_Products__c, Total_Instalments__c, Total_Discount__c, Total_Extra_Fee__c, Total_Tuition__c, Received_by__c, Received_By__r.Name, Received_Date__c, Client__c, Due_Date__c,  cancel_reason__c, isCancelled__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, CurrencyIsoCode__c,
                        (Select Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, Due_Date__c, Extra_Fee_Value__c, Invoice__c, Id, number__c, Split_Number__c, Tuition_Value__c, Instalment_Value__c, Discount__c, client_course__c, client_course__r.Client__c, client_course__r.CurrencyIsoCode__c, client_course__r.Total_Paid__c,
                            client_course__r.Course_Name__c, Received_by__c, Client_Course__r.Campus_Name__c, Received_Date__c, Client_Document__c, client_course__r.Enroled_by_Agency__c, client_scholarship__c, isFirstPayment__c, Required_for_COE__c
                                from client_course_instalments__r),
                        ( Select Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, Category__c, client_course__c, Currency__c, Price_Total__c, Price_Unit__c, Product_Name__c, Quantity__c, Unit_Description__c, Received_by__c, Received_Date__c, Paid_By_Agency__c from client_products_services__r)
                        from Invoice__c WHERE Id = :invId];

            installmentsInvoice = new courseInstalllmentDetails();
            for(client_course_instalment__c cci:invoice.client_course_instalments__r){
                System.debug('==>cci: '+cci);
                installmentsInvoice.installments.add(cci);
                installmentsInvoice.totalCourseValue += cci.Instalment_Value__c;
            }

            productsInvoice = new productsDetails();
            for(client_product_service__c cps:invoice.client_products_services__r){
                productsInvoice.products.add(cps);
                productsInvoice.totalProductValue += cps.Price_Total__c;
            }

            retrieveDeposit(invoice.Client__c);
        }else{
            invoice = new Invoice__c();
            invoice.Total_Value__c = 0;
            invoice.Total_Products__c = 0;
            invoice.Total_Instalments__c = 0;
            invoice.Total_Tuition__c = 0;
            invoice.Total_Extra_Fee__c = 0;
            invoice.Total_Discount__c = 0;
        }
        if(cs != null && cs != ''){
            coursesId = new list<string>(cs.split(','));

            IPFunctions.SearchNoSharing ns = new IPFunctions.SearchNoSharing(); // Without sharing cus user can't see School Account ID
            String sql = 'SELECT client_course__c, Number__c, Split_Number__c, client_course__r.CurrencyIsoCode__c, client_course__r.Course_Name__c, Discount__c, Due_Date__c, Extra_Fee_Value__c, Kepp_Fee__c, Scholarship_Taken__c, ' +
                            ' Tuition_Value__c, Instalment_Value__c, Client_Course__r.Campus_Name__c, isFirstPayment__c, Required_for_COE__c,client_course__r.Campus_Course__r.Campus__c, client_course__r.Campus_Course__r.Campus__r.ParentId, ' +
                            ' client_course__r.Total_Paid__c, client_course__r.Client__c, Client_Document__c, client_scholarship__c '+
                                ' FROM client_course_instalment__c WHERE id in  ( \''+ String.join(coursesId, '\',\'') + '\' ) ';


            installmentsInvoice = new courseInstalllmentDetails();
            for(client_course_instalment__c cci: ns.NSInstalments(sql)){
                installmentsInvoice.installments.add(cci);
                installmentsInvoice.totalCourseValue += cci.Instalment_Value__c;

                if(cci.client_scholarship__c != null && cci.client_scholarship__c >0)
                    installmentsInvoice.totalClientScholarship +=cci.client_scholarship__c;
            }
        }
        if(pd != null && pd != ''){
            productsInvoice = new productsDetails();
            productsId = new list<string>(pd.split(','));
            for(client_product_service__c cps:[Select Category__c, client_course__c, Currency__c, Related_to_Product__r.Currency__c, Price_Total__c, Price_Unit__c, Product_Name__c, Quantity__c, Unit_Description__c, Paid_By_Agency__c from client_product_service__c where id in:productsId OR Related_to_Product__c in :productsId]){
                if(cps.Related_to_Product__c != null)
                    cps.Currency__c = cps.Related_to_Product__r.Currency__c;
                productsInvoice.products.add(cps);
                productsInvoice.totalProductValue += cps.Price_Total__c;
            }
        }
        retrieveMainCurrencies();
        System.debug('==>invoice.Agency_Currency__c: '+invoice.Agency_Currency__c);
        recalculateRate();
    }



    public boolean showError {get{if(showError == null) showError = false; return showError;}set;}

    /** Cancel Grouped Invoice **/
    public void cancelGroupedInvoice(){
        System.Savepoint sp;
        try{
            String instalments = '';
            String products = '';

            for(client_course_instalment__c cci : invoice.client_course_instalments__r){
                cci.Invoice__c = null;
                cci.Agency_Currency__c = null;
                cci.Agency_Currency_Rate__c = null;
                cci.Agency_Currency_Value__c = null;
                instalments += cci.id + ';';

            }//end for


            for(client_product_service__c ccp : invoice.client_products_services__r){
                ccp.Invoice__c = null;
                ccp.Agency_Currency__c = null;
                ccp.Agency_Currency_Rate__c = null;
                ccp.Agency_Currency_Value__c = null;
                products += ccp.id + ';';

            }//end for


            invoice.instalments__c = instalments;
            invoice.products__c = products;
            invoice.isCancelled__c = true;

            //invoice.Agency_Currency__c = null;
            //invoice.Agency_Currency_Rate__c = null;
            //invoice.Agency_Currency_Value__c = null;
            //invoice.CurrencyIsoCode__c = null;



            update invoice;
            update invoice.client_course_instalments__r;
            update invoice.client_products_services__r;

            showError = true;
        }
        catch(Exception e){
            system.debug('Error==>' + e);
            Database.rollback(sp);
        }
    }

    /** END Cancel Grouped Invoice **/

    public class courseInstalllmentDetails{
        public decimal totalCourseValue{get{if(totalCourseValue == null) totalCourseValue = 0; return totalCourseValue;} set;}
        public decimal totalClientScholarship {get{if(totalClientScholarship == null) totalClientScholarship = 0; return totalClientScholarship;} set;}
        public boolean hasMuiltCurrency{get{if(hasMuiltCurrency == null) hasMuiltCurrency = false; return hasMuiltCurrency;} set;}
        public list<client_course_instalment__c> installments{get{if(installments == null) installments = new list<client_course_instalment__c>(); return installments;} set;}
    }

    public class productsDetails{
        public decimal totalProductValue{get{if(totalProductValue == null) totalProductValue = 0; return totalProductValue;} set;}
        public boolean hasMuiltCurrency{get{if(hasMuiltCurrency == null) hasMuiltCurrency = false; return hasMuiltCurrency;} set;}
        public list<client_product_service__c> products{get{if(products == null) products = new list<client_product_service__c>(); return products;} set;}
    }


    public pageReference createInvoice(){
        Savepoint sp = Database.setSavepoint();
        try{
            //string currencyCode = '';
            invoice.isClientInvoice__c = true;
            invoice.client__c = contactId;
            invoice.Status__c = 'Open';
            insert invoice;


            set<string> schoolIds = new set<string>();
            set<string> campusIds = new set<string>();

            retrieveDeposit(contactId);
            System.debug('==> invoice: '+invoice);

            decimal clientScholarship = 0;

            if(installmentsInvoice.installments.size() > 0){
                for(client_course_instalment__c itd:installmentsInvoice.installments){
                    System.debug('==> itd: '+itd);
                    itd.Invoice__c = invoice.id;
                    //invoice.Total_Value__c += itd.Instalment_Value__c;
                    //invoice.Total_Instalments__c += itd.Instalment_Value__c;
                    invoice.Total_Tuition__c += itd.Tuition_Value__c;
                    invoice.Total_Extra_Fee__c += itd.Extra_Fee_Value__c;
                    invoice.Total_Discount__c += itd.Discount__c;

                    System.debug('==> itd Id: '+itd.Id);
                    System.debug('==> isFirstPayment__c: '+itd.isFirstPayment__c);

                    schoolIds.add(itd.client_course__r.Campus_Course__r.Campus__r.ParentId);
                    campusIds.add(itd.client_course__r.Campus_Course__r.Campus__c);

                    //if(currencyCode == '')
                    //  currencyCode = itd.client_course__r.CurrencyIsoCode__c;

                    if(itd.isFirstPayment__c)
                        invoice.hasFirstPayment__c = true;
                }//end for

                //invoice.Total_Value__c -= installmentsInvoice.totalClientScholarship;
                update installmentsInvoice.installments;
            }

            if(productsInvoice.products.size() > 0){
                for(client_product_service__c cps:productsInvoice.products){
                    cps.Invoice__c = invoice.id;
                    //invoice.Total_Value__c += cps.Price_Total__c;
                    //invoice.Total_Products__c += cps.Price_Total__c;
                    //if(currencyCode == '')
                    //  currencyCode = cps.Currency__c;
                }
                update productsInvoice.products;
            }

            list<string> schIds= new list<String>(schoolIds);
            list<string> cpIds= new list<String>(campusIds);

            invoice.School_Ids__c = string.join(schIds, ';');
            invoice.Campus_Ids__c = string.join(cpIds, ';');

            invoice.Agency_Currency_Rate__c = 1;
            invoice.Agency_Currency_Value__c = invoice.Total_Value__c;
            invoice.CurrencyIsoCode__c = fin.currentUser.Contact.Account.account_currency_iso_code__c; //User agency Currency


            update invoice;

            invoice = [Select Name, Id, Total_Value__c, Total_Products__c, Total_Instalments__c, Total_Discount__c, Total_Extra_Fee__c, Total_Tuition__c, Received_by__c, Received_Date__c, Client__c, Due_Date__c,
                        Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, CurrencyIsoCode__c,
                        ( Select Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, Due_Date__c, Extra_Fee_Value__c, Invoice__c, Id, number__c, Tuition_Value__c, Instalment_Value__c, Discount__c, client_course__c, client_course__r.Client__c, client_course__r.Enroled_by_Agency__c, client_course__r.CurrencyIsoCode__c, client_course__r.Course_Name__c, Received_by__c, Client_Course__r.Campus_Name__c, Received_Date__c, isFirstPayment__c, Required_for_COE__c from client_course_instalments__r),
                        ( Select Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, Category__c, client_course__c, Currency__c, Price_Total__c, Price_Unit__c, Product_Name__c, Quantity__c, Unit_Description__c, Received_by__c, Received_Date__c from client_products_services__r) from Invoice__c WHERE Id = :invoice.id];


        }catch(Exception e){
            Database.rollback(sp);
        }
        return null;
    }

    //===============================================================INVOICE PAYMENT==================================================================

    public client_course__c clientDeposit{get;set;}

    private decimal currentDepositUsed = 0;

    private void retrieveDeposit(string contactId){
        try{
            clientDeposit = [Select Deposit_Description__c, Deposit_Total_Used__c, Deposit_Total_Value__c, Deposit_Total_Refund__c, Deposit_Total_Available__c, Deposit_Type__c, Deposit_Value_Track__c, Id, client__c, Client__r.name  from client_course__c where client__c = :contactId and isDeposit__c = true and Deposit_Total_Available__c > 0];
        }catch(Exception e){
            clientDeposit = new client_course__c();
        }
    }
    private list<selectOption> depositListType;
    public list<selectOption> getdepositListType(){
        if(depositListType == null && invoice != null && invoice.id != null){
            depositListType = new list<selectOption>();
            depositListType.add(new SelectOption('','Select Type'));
            if(clientDeposit.Deposit_Total_Used__c == null)
                clientDeposit.Deposit_Total_Used__c = 0;
            if(clientDeposit.Deposit_Total_Value__c != null && (clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c - clientDeposit.Deposit_Total_Refund__c) > 0){
                depositListType.add(new SelectOption('Deposit','Deposit'));
                newPayDetails.Payment_Type__c = 'Deposit';
                newPayDetails.Date_Paid__c = System.today();
                newPayDetails.Value__c = clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c  - clientDeposit.Deposit_Total_Refund__c;
            }

            Schema.DescribeFieldResult fieldResult = client_course__c.Deposit_Type__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry f : ple)
                if(f.getLabel()!='Covered by Agency')
                    depositListType.add(new SelectOption(f.getLabel(), f.getValue()));
            depositListType.sort();
        }
        return depositListType;
    }

    public class paymentValue{
        public integer payIndex{get; set;}
        public client_course_instalment_payment__c invoicePayment{get; set;}
        public paymentValue(){}
        public paymentValue(integer depositIndex, client_course_instalment_payment__c invoicePayment){
            this.payIndex = depositIndex;
            this.invoicePayment = invoicePayment;
        }
    }

    public list<paymentValue> listPayDetails{
        get{
            if(listPayDetails == null){
                /*if(installment.client_course_instalment_payments__r.size() > 0){
                    for(client_course_instalment_payment__c ip:installment.client_course_instalment_payments__r)
                        listPayDetails.add(new paymentValue(countNumberPaymentValues++, ip));
                }else */listPayDetails = new list<paymentValue>();
            }
            return listPayDetails;
        }
        set;
    }

    public decimal totalpaid{
        get{
            if(totalpaid == null)
                totalpaid = 0;
            return totalpaid;
        }
        set;
    }

    public client_course_instalment_payment__c newPayDetails{
        get{
            if(newPayDetails == null && invoice != null){
                newPayDetails = new client_course_instalment_payment__c();
                newPayDetails.Invoice__c = invoice.id;
            }
            return newPayDetails;
        }
        set;
    }

    public Boolean invoiceSaved{get{if(invoiceSaved == null) invoiceSaved = true; return invoiceSaved;} Set;}

    private integer countNumberPaymentValues = 0;

    public pageReference addPaymentValue(){

        if(newPayDetails.Value__c <= 0)
            newPayDetails.Value__c.addError('Value need to be bigger than 0');

        if(newPayDetails.Payment_Type__c != 'pds' && newPayDetails.Payment_Type__c != 'offShore' && (newPayDetails.Value__c > invoice.Total_Value__c || newPayDetails.Value__c > (invoice.Total_Value__c - totalpaid))){
            newPayDetails.Value__c.addError('Value cannot be bigger than ' + (invoice.Total_Value__c - totalpaid));
            return null;
        }

        if(newPayDetails.Date_Paid__c > system.today()){
            newPayDetails.Date_Paid__c.addError('Date paid cannot be future date. ');
            return null;
        }

        if(newPayDetails.Date_Paid__c < Date.today().addDays(-14)){
            newPayDetails.Date_Paid__c.addError('Date paid cannot be lower than 2 weeks. ');
            return null;
        }

        if(newPayDetails.Payment_Type__c == 'Deposit'){
            if((clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c - clientDeposit.Deposit_Total_Refund__c) >= 0 && newPayDetails.Value__c <= (clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c - clientDeposit.Deposit_Total_Refund__c))
                clientDeposit.Deposit_Total_Used__c += newPayDetails.Value__c;
            else {
                newPayDetails.Value__c.addError('Deposit Value Invalid');
                return null;
            }
        }


        newPayDetails.client__c = invoice.Client__c;


        if(newPayDetails.Payment_Type__c == 'offShore'){
            listPayDetails.clear();
            listPayDetails.add(new paymentValue(countNumberPaymentValues++, newPayDetails));
            totalpaid = newPayDetails.Value__c;
            clientDeposit.Deposit_Total_Used__c = currentDepositUsed;

            hasOffShorePayment = true;
            invoice.isPaidOffShore__c = true;

        }else{


            //Relate deposit ID to the payment
            if(newPayDetails.Payment_Type__c=='deposit'){
                newPayDetails.Paid_From_Deposit__c = clientDeposit.id;
            }

            boolean isSameTypeDate = false;
            for(integer i = 0; i < listPayDetails.size(); i++)
                if(listPayDetails[i].invoicePayment.Payment_Type__c == newPayDetails.Payment_Type__c && listPayDetails[i].invoicePayment.Date_Paid__c == newPayDetails.Date_Paid__c){

                    listPayDetails[i].invoicePayment.Value__c += newPayDetails.Value__c;
                    totalpaid += newPayDetails.Value__c;
                    isSameTypeDate = true;
                    listPayDetails[i].invoicePayment.Agency_Currency_Value__c = listPayDetails[i].invoicePayment.Value__c;
                    break;
                }

            if(!isSameTypeDate){
                listPayDetails.add(new paymentValue(countNumberPaymentValues++, newPayDetails));
                totalpaid += newPayDetails.Value__c;
                newPayDetails.Agency_Currency_Value__c = newPayDetails.Value__c;
            }

            
        }

        newPayDetails = new client_course_instalment_payment__c();
        newPayDetails.Date_Paid__c = System.today();
        newPayDetails.Payment_Type__c = '';
        newPayDetails.invoice__c = invoice.id;

        invoiceSaved = false;

        return null;
    }

    public pageReference removePaymentValue(){
        integer index = integer.valueOf(ApexPages.currentPage().getParameters().get('index'));

        for(integer i = 0; i < listPayDetails.size(); i++){
            if(index == listPayDetails[i].payIndex){

                totalpaid -= listPayDetails[i].invoicePayment.Value__c;
                if(listPayDetails[i].invoicePayment.Payment_Type__c == 'Deposit')
                    clientDeposit.Deposit_Total_Used__c -= listPayDetails[i].invoicePayment.Value__c;


                if(listPayDetails[i].invoicePayment.Payment_Type__c == 'offShore'){
                    hasOffShorePayment = false;
                    invoice.isPaidOffShore__c = false;
                }

                listPayDetails.remove(i);
                break;
            }
        }
        invoiceSaved = false;

        return null;
    }

    private Boolean isGeneralUser {get;set;}
    public pageReference savePayment(){

        errorMsg = '';


        /** Double Check if Invoice is paid **/
        Invoice__c updatedInvoice  = [Select Received_Date__c, Received_by__r.Name from Invoice__c where id = :invoice.id limit 1];

        if(updatedInvoice.Received_Date__c!=null){
            errorMsg = 'This Grouped Invoice has been already paid by ' +updatedInvoice.Received_by__r.Name + ' on ' + updatedInvoice.Received_Date__c.format() + '. Please refresh your page.';
            return null;
        }

        if(hasOffShorePayment){

            if(selectedUser == null || selectedUser =='noUser'){
                errorMsg += '<span style="display:block">Please fill the field <b>Payment Received By</b>.</span>';
            }

            if(receivedOn.Expected_Travel_Date__c == null){
                errorMsg += '<span style="display:block">Please fill the field <b>Payment Received On</b>.</span>';
            }

            if(receivedOn.Expected_Travel_Date__c > system.today()){
                errorMsg += '<span style="display:block"><b>Payment Received On</b> cannot be future date.</span>';
                return null;
            }
        }

        if(errorMsg.length()>0){

            return null;
        }

        else{


            Savepoint sp = Database.setSavepoint();
            //try{

                if(clientDeposit != null && clientDeposit.id != null && currentDepositUsed != null && currentDepositUsed != clientDeposit.Deposit_Total_Value__c){
                    if(clientDeposit.Deposit_Total_Refund__c==null) clientDeposit.Deposit_Total_Refund__c = 0;

                    clientDeposit.Deposit_Total_Available__c = clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c - clientDeposit.Deposit_Total_Refund__c;
                    update clientDeposit;
                }

                list<client_course_instalment_payment__c> paymentList = new list<client_course_instalment_payment__c>();

                String receivedByAgency;
                String receivedByDepartment;

                boolean isReconciled = true; //Used to automatically reconcile Third Party Payments 


                isGeneralUser = FinanceFunctions.isGeneralUser(fin.currentUser.General_User_Agencies__c, contactDetails.Owner__r.AccountId);
                
                if(!isGeneralUser){
                    receivedByAgency = fin.currentUser.Contact.AccountId;
                    receivedByDepartment = fin.currentUser.Contact.Department__c;
                }
                else{
                    receivedByAgency = contactDetails.Owner__r.AccountId;
                    receivedByDepartment = contactDetails.Owner__r.Contact.Department__c;
                }

                for(paymentValue lpd:listPayDetails){

                    lpd.invoicePayment.Received_By__c = UserInfo.getUserId();
                    lpd.invoicePayment.Received_On__c = DateTime.now();
                    lpd.invoicePayment.Received_By_Agency__c = receivedByAgency;

                    if(lpd.invoicePayment.Payment_Type__c.toLowerCase() != 'payment 3rd party')
                        isReconciled = false;
                    else{
                        lpd.invoicePayment.Confirmed_By__c = UserInfo.getUserId();
                        lpd.invoicePayment.Confirmed_Date__c = DateTime.now();
                        lpd.invoicePayment.Confirmed_By_Agency__c = receivedByAgency;
                    }

                    paymentList.add(lpd.invoicePayment);

                }//end for

                if(isReconciled){
                    invoice.Confirmed_By__c = Userinfo.getuserId();
                    invoice.Confirmed_Date__c = System.now();
                }

                insert paymentList;

                list<string> installmentIds = new list<string>();
                list<Client_Document__c> schoolReceipt = new list<Client_Document__c>();

                map<string, decimal> courseTotalPaid = new map<string,decimal>();

                if(invoice.client_course_instalments__r.size() > 0){
                    for(client_course_instalment__c cci:invoice.client_course_instalments__r){

                        if(isReconciled){   
                            cci.Confirmed_By__c = Userinfo.getuserId();
                            cci.Confirmed_Date__c = System.now();
                        }

                        //SUM Course Total Paid
                        if(!courseTotalPaid.containskey(cci.Client_Course__c))
                            courseTotalPaid.put(cci.Client_Course__c, cci.Instalment_Value__c + cci.Discount__c);
                        else
                            courseTotalPaid.put(cci.Client_Course__c, courseTotalPaid.get(cci.Client_Course__c) + cci.Instalment_Value__c + cci.Discount__c);


                        if(!hasOffShorePayment){
                            cci.Received_By_Agency__c = receivedByAgency;
                            cci.Received_by__c = Userinfo.getUserId();
                            cci.Received_Date__c = System.now();
                            cci.Received_by_Department__c = receivedByDepartment;
                            if(cci.client_course__r.Enroled_by_Agency__c!= null && (cci.client_course__r.Enroled_by_Agency__c != cci.Received_By_Agency__c))
                                cci.isSharedCommission__c = true;


                        }else{
                            cci.Received_By_Agency__c = selectedAgency;
                            cci.Received_by__c = selectedUser;
                            cci.Received_Date__c = DateTime.newInstance(receivedOn.Expected_Travel_Date__c.year(), receivedOn.Expected_Travel_Date__c.month(), receivedOn.Expected_Travel_Date__c.day(), 0, 0, 0);
                            cci.isPaidOffShore__c = true;
                            cci.Received_by_Department__c = userDept.get(selectedUser);
                            if(cci.client_course__r.Enroled_by_Agency__c!= null && (cci.client_course__r.Enroled_by_Agency__c != cci.Received_By_Agency__c))
                                cci.isSharedCommission__c = true;
                        }

                        cci.Status__c = 'Paid';

                        //Generate Receipt only for First Payments or Required COE
                        if(cci.isFirstPayment__c || cci.Required_for_COE__c){
                            schoolReceipt.add(new Client_Document__c(Client__c = cci.client_course__r.client__c, Document_Category__c = 'Finance', Document_Type__c = 'School Installment Receipt',
                                                                                Client_course__c = cci.client_course__c, client_course_instalment__c = cci.id));
                        }
                installmentIds.add(cci.id);

                    }//end for



                    // if(schoolReceipt.size()>0)
                    //  insert schoolReceipt;


                    /** Update Client Course Total Paid for all Instalments **/
                    list<client_course__c> updateCourse = new list<client_course__c>();
                    for(Client_Course__c cc : [Select Id, Total_Paid__c from Client_Course__c Where id in :courseTotalPaid.keyset()]){
                        cc.Total_Paid__c += courseTotalPaid.get(cc.id);

                        updateCourse.add(cc);
                    }//end for

                    update updateCourse;



                    map<string, string> docReceipt = new map<string, string>();
                    for(Client_Document__c cd:[Select id, client_course_instalment__c from Client_Document__c where client_course_instalment__c in :installmentIds])
                        docReceipt.put(cd.client_course_instalment__c, cd.id);

                    for(client_course_instalment__c cci:invoice.client_course_instalments__r)
                        cci.Client_Document__c = docReceipt.get(cci.id);

                    update invoice.client_course_instalments__r;
                }

                if(invoice.client_products_services__r.size() > 0){
                    for(client_product_service__c cps:invoice.client_products_services__r){
                        if(!hasOffShorePayment){
                            cps.Received_By_Agency__c = receivedByAgency;
                            cps.Received_by__c = Userinfo.getUserId();
                            cps.Received_Date__c = System.now();
                            cps.Received_by_Department__c = receivedByDepartment;
                        }else{
                            cps.Received_By_Agency__c = selectedAgency;
                            cps.Received_by__c = selectedUser;
                            cps.Received_by_Department__c = userDept.get(selectedUser);
                            cps.Received_Date__c = DateTime.newInstance(receivedOn.Expected_Travel_Date__c.year(), receivedOn.Expected_Travel_Date__c.month(), receivedOn.Expected_Travel_Date__c.day(), 0, 0, 0);
                            cps.isPaidOffShore__c = true;
                        }
                    }
                    update invoice.client_products_services__r;
                }


                if(!hasOffShorePayment){
                    invoice.Received_By_Agency__c = receivedByAgency;
                    invoice.Received_by__c = Userinfo.getUserId();
                    invoice.Received_Date__c = System.now();
                    invoice.Received_by_Department__c = receivedByDepartment;
                }else{
                    invoice.Received_By_Agency__c = selectedAgency;
                    invoice.Received_by__c = selectedUser;
                    invoice.Received_by_Department__c = userDept.get(selectedUser);
                    invoice.Received_Date__c = DateTime.newInstance(receivedOn.Expected_Travel_Date__c.year(), receivedOn.Expected_Travel_Date__c.month(), receivedOn.Expected_Travel_Date__c.day(), 0, 0, 0);
                }

                invoice.Status__c = 'Paid';

                update invoice;
                showError = true;

            //}catch(Exception e){
            //  Database.rollback(sp);
            //}
            return null;
        }
    }

    public pageReference cancelPayment(){

        return null;
    }






    public boolean hasOffShorePayment {get{if(hasOffShorePayment==null) hasOffShorePayment = false; return hasOffShorePayment;}set;}
    public String errorMsg {get{if(errorMsg == null) errorMsg = ''; return errorMsg;}set;}

    public Contact receivedOn {get
        {
        if(receivedOn == null){
            receivedOn = new Contact();
        }return receivedOn;}set; }


    public String selectedAgencyGroup {get{if(selectedAgencyGroup==null) selectedAgencyGroup = fin.currentUser.Contact.Account.ParentId; return selectedAgencyGroup;}set;}
    public List<SelectOption> agencyGroupOptions {
        get{
            if(agencyGroupOptions == null){
                agencyGroupOptions = agencyNoSharing.getAllGroups();
            }
            return agencyGroupOptions;
        }
        set;
    }

    public void changeAgencyGroup(){
        selectedAgency = 'noAgency';
        agencyOptions = null;
        selectedUser = 'noUser';
        userOptions = null;
    }

    public void changeAgency(){
        selectedUser = 'noUser';
        userOptions = null;
    }

    private IPFunctions.agencyNoSharing agencyNoSharing {get{if(agencyNoSharing == null)agencyNoSharing = new IPFunctions.agencyNoSharing();return agencyNoSharing;}set;}
    public String selectedAgency {get{if(selectedAgency==null) selectedAgency = 'noAgency'; return selectedAgency;}set;}
    public List<SelectOption> agencyOptions {
        get{
            if(agencyOptions == null){
                 agencyOptions = agencyNoSharing.getAgenciesByParentNoSharing(selectedAgencyGroup);
            }
            return agencyOptions;
        }
        set;
    }

    public String selectedUser {get{if(selectedUser==null) selectedUser = 'noUser'; return selectedUser;}set;}
    private map<string,string> userDept {get;set;}
    public List<SelectOption> userOptions {
        get{
            if(userOptions == null){
                userDept = new map<string,string>();
                userOptions = new List<SelectOption>();
                userOptions.add(new SelectOption('noUser', '-- Select User --'));
                if(selectedAgency!=null && selectedAgency!='')
                    for(User ac : [SELECT id, Name, Contact.Department__c FROM User WHERE Contact.AccountId = :selectedAgency and IsActive = true and Contact.Chatter_Only__c = false order by name]){
                        userOptions.add(new SelectOption(ac.Id, ac.Name));
                        userDept.put(ac.Id, ac.Contact.Department__c);
                    }
            }
            return userOptions;
        }
        set;
    }

    //================CURRENCY=============================
    private FinanceFunctions fin = new FinanceFunctions();
    public List<SelectOption> MainCurrencies{get; set;}
    public Datetime currencyLastModifiedDate {get;set;}
    public void retrieveMainCurrencies(){

        if(invoice.Agency_Currency__c == null)
            invoice.Agency_Currency__c = fin.currentUser.Contact.Account.account_currency_iso_code__c;

        MainCurrencies = fin.retrieveMainCurrencies();

        currencyLastModifiedDate = fin.currencyLastModifiedDate;


    }

    public void recalculateRateperItem(){
        invoice.Total_Value__c = 0;
        if(installmentsInvoice.installments.size() > 0){
            for(client_course_instalment__c itd:installmentsInvoice.installments){
                itd.Agency_Currency__c = invoice.Agency_Currency__c;
                fin.convertCurrency(itd, itd.client_course__r.CurrencyIsoCode__c, itd.Instalment_Value__c - itd.client_scholarship__c, itd.Agency_Currency_Rate__c, 'Agency_Currency__c', 'Agency_Currency_Rate__c', 'Agency_Currency_Value__c');
                invoice.Total_Value__c += itd.Agency_Currency_Value__c;
            }
        }
        if(productsInvoice.products.size() > 0){
            for(client_product_service__c cps:productsInvoice.products){
                cps.Agency_Currency__c = invoice.Agency_Currency__c;
                System.debug('==>Agency_Currency_Rate__c: '+cps.Agency_Currency_Rate__c);
                fin.convertCurrency(cps, cps.Currency__c, cps.Price_Total__c, cps.Agency_Currency_Rate__c, 'Agency_Currency__c', 'Agency_Currency_Rate__c', 'Agency_Currency_Value__c');
                invoice.Total_Value__c += cps.Agency_Currency_Value__c;
            }
        }
    }

    public void recalculateRate(){
        invoice.Total_Value__c = 0;
        if(installmentsInvoice.installments.size() > 0){
            for(client_course_instalment__c itd:installmentsInvoice.installments){
                itd.Agency_Currency__c = invoice.Agency_Currency__c;
                fin.convertCurrency(itd, itd.client_course__r.CurrencyIsoCode__c, itd.Instalment_Value__c - itd.client_scholarship__c, null, 'Agency_Currency__c', 'Agency_Currency_Rate__c', 'Agency_Currency_Value__c');
                invoice.Total_Value__c += itd.Agency_Currency_Value__c;
            }
        }
        if(productsInvoice.products.size() > 0){
            for(client_product_service__c cps:productsInvoice.products){
                cps.Agency_Currency__c = invoice.Agency_Currency__c;
                fin.convertCurrency(cps, cps.Currency__c, cps.Price_Total__c, null, 'Agency_Currency__c', 'Agency_Currency_Rate__c', 'Agency_Currency_Value__c');
                invoice.Total_Value__c += cps.Agency_Currency_Value__c;
            }
        }
    }



}