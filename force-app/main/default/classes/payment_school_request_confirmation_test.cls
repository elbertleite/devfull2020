@isTest
private class payment_school_request_confirmation_test {

	public static Account school {get;set;}
	public static Account agency {get;set;}
	public static Contact emp {get;set;}
	public static Contact client {get;set;}
	public static User portalUser {get{
		if (null == portalUser) {
			portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
			portalUser.Finance_Backoffice_User__c = true;
		}
		return portalUser;
	}set;}
	public static Account campus {get;set;}
	public static Course__c course {get;set;}
	public static Campus_Course__c campusCourse {get;set;}
	public static client_course__c clientCourseBooking {get;set;}
	public static client_course__c clientCourse {get;set;}
	public static client_course__c clientCourse2 {get;set;}


	@testSetup static void setup() {
		TestFactory tf = new TestFactory();

		Account sg = tf.createSchoolGroup();
		school = tf.createSchool();
		school.ParentId = sg.Id;
		update school;

		agency = tf.createAgency();
		list<Back_Office_Control__c> bo = new list<Back_Office_Control__c>();
		bo.add(new Back_Office_Control__c(Agency__c = agency.id, Country__c = 'Australia', Backoffice__c = agency.id, Services__c = 'PDS_PCS_PFS_Chase'));
		insert bo;
		emp = tf.createEmployee(agency);
		portalUser = tf.createPortalUser(emp);
		campus = tf.createCampus(school, agency);
		course = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);
		system.runAs(portalUser){
			client = tf.createClient(agency);
			clientCourseBooking = tf.createBooking(client);
			clientCourse = tf.createClientCourse(client, school, campus, course, campusCourse, clientCourseBooking);
			tf.createClientCourseFees(clientCourse, false);
			List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(clientCourse);

			//TO REQUEST PDS
			instalments[0].Received_By_Agency__c = agency.id;
			instalments[0].Received_Date__c =  system.today();
			instalments[0].Paid_To_School_On__c =  system.today();
			instalments[0].isPFS__c = true;
			instalments[0].commission_due_date__c = system.today();
			instalments[0].Paid_to_School_By_Agency__c = agency.id;

			instalments[1].Received_By_Agency__c = agency.id;
			instalments[1].Received_Date__c =  system.today();
			instalments[1].isPCS__c = true;
			instalments[1].commission_due_date__c = system.today();
			instalments[1].Paid_to_School_By_Agency__c = agency.id;

			instalments[2].Received_By_Agency__c = agency.id;
			instalments[2].Received_Date__c =  system.today();
			instalments[2].isPDS__c = true;
			instalments[2].PDS_Confirmed__c = true;
			instalments[2].commission_due_date__c = system.today();
			instalments[2].Paid_to_School_By_Agency__c = agency.id;
			update instalments;

			commissions_send_school_invoice req = new commissions_send_school_invoice();
			req.selectedCountry = 'Australia';
			req.selectedSchoolGP = sg.id;
			req.selectedSchool = school.id;
			req.searchName = '';
			
			req.dates.Commission_Paid_Date__c = system.today().addYears(1);
			req.findInstalments();

			req.result[0].campuses[0].instalments[0].installment.isSelected__c = true;
			req.result[0].campuses[0].instalments[1].installment.isSelected__c = true;
			req.result[0].campuses[0].instalments[2].installment.isSelected__c = true;
			ApexPages.currentPage().getParameters().put('schId', string.valueOf(req.result[0].schoolId));
			req.dates.Commission_Paid_Date__c = system.today().addYears(1);
			req.setToRequest();

			ApexPages.CurrentPage().getParameters().put('cn', 'Australia');
			ApexPages.currentPage().getParameters().put('type', 'sendInvoice');
			PaymentSchoolEmail invoice = new PaymentSchoolEmail();
			invoice.setupEmail();
			invoice.toAddress = 'test@educationhify.com';
			invoice.dueDate.Expected_Travel_Date__c= System.today().addDays(300);
			invoice.createInvoice();
		}
	}

	static testMethod void testConstructor(){
			Test.startTest();
			system.runAs(portalUser){

				Invoice__c inv = [Select Id from Invoice__c limit 1];
				ApexPages.currentPage().getParameters().put('i', inv.id);

				payment_school_request_confirmation toTest = new payment_school_request_confirmation();

				boolean hasKeepFee = toTest.hasKeepFee;
				String invNumber = toTest.invNumber;
				String invDueDate = toTest.invDueDate;

			}
			Test.stopTest();
		}


}