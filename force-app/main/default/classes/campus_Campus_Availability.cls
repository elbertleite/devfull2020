public with sharing class campus_Campus_Availability {
  public string selectedSchoolCountry{get; set;}
  public string selectedSchoolId{get; set;}
  
  // SELECT OPTION  -- Select Country (School filter)
  private List<SelectOption> schoolCountries;
  public List<SelectOption> getschoolCountries() {
    if(schoolCountries == null){
      schoolCountries = new List<SelectOption>();
          
      schoolCountries.add(new SelectOption('','Select Country'));
      for (AggregateResult ar:[Select BillingCountry country from Account where recordType.name = 'campus' and BillingCountry != null group by BillingCountry order by BillingCountry])
        schoolCountries.add(new SelectOption((string) ar.get('country'),(string) ar.get('country')));
    }
    return schoolCountries;
  }
  
  //SELECT OPTION  -- Select School :: list schools for one country  
  public list<SelectOption> getschoolsCountry(){    
    list<SelectOption> schoolsCountry = new List<SelectOption>();
    System.debug('==> selectedSchoolCountry: '+selectedSchoolCountry);
    
    schoolsCountry.add(new SelectOption('','Select School'));
    if(selectedSchoolCountry != null){
     
      for (AggregateResult sch:[Select parentid pid, parent.name pname from Account WHERE recordType.name='campus' and billingcountry = :selectedSchoolCountry GROUP BY parentid, parent.name order by parent.name])
        schoolsCountry.add(new SelectOption((string) sch.get('pid'), (string) sch.get('pname')));        
    }
    
    return schoolsCountry;
  }
  
  /* MAP SEARCH */
  //search action
  public pageReference searchListCampus(){
    mapCampusCampuses = null;
    mapCampuses = null;
    //supplierCampus = null;
    mapCampusViewNum = 0;    
    retrieveMapCampusCampuses();
    return null;
  }
  
  //campus supplier
  private map<String, set<string>> supplierCampus;
  private map<String, set<string>> createSupplierCampus(set<string> idcampus, String schoolCountry){
  	if(idcampus != null){
  		supplierCampus = new map<String, set<string>>();
  		System.debug('==========query: Select Agency__c from Supplier__c where Supplier__c in '+idcampus+' AND Supplier__r.BillingCountry = '+selectedSchoolCountry);
	  	
	  	List<Supplier__c> sps = new List<Supplier__c>();
	  	
	  	if(schoolCountry != null)  				
  			sps = [Select Supplier__c, Agency__c  from Supplier__c where Agency__c in :idcampus AND Agency__r.BillingCountry = :selectedSchoolCountry];
  		else
  			sps = [Select Supplier__c, Agency__c  from Supplier__c where Agency__c in :idcampus];
  		
  		for(Supplier__c sc: sps){
	  		if(supplierCampus.containsKey(sc.Agency__c)) 
	  			supplierCampus.get(sc.Agency__c).add(sc.Supplier__c);
	  		else
	  			supplierCampus.put(sc.Agency__c, new set<string>{sc.Supplier__c});
  		}
  		
  	}
  	
  	system.debug('=====supplierCampus: '+supplierCampus);
    return supplierCampus;	
  }
 
  	public integer mapCampusViewNum{get; set;}
  	public map<String, Account> mapCampuses{get{if(mapCampuses == null) mapCampuses = new map<string, Account>(); return mapCampuses;} set;}      
  
  	public map<String, map<String, list<Account>>> mapCampusCampuses{get;set;}
  	
  	public String selectedCampus {get;set;}
  	public List<SelectOption> listCampuses {get;set;}
  	
  	public boolean inSchoolGroup {get;set;}
  	
  	public Map<String, String> schoolNames {get;set;}
  	public String schoolGroupName {get;set;}
  
	private map<String, map<String, list<Account>>> retrieveMapCampusCampuses(){
  		if(mapCampusCampuses == null){
  			mapCampusCampuses = new map<String, map<String, list<Account>>>();
  			schoolNames = new Map<String, String>();
  			
	  		if(selectedSchoolId != null){
	  			
	  			Account school = [Select name, ParentID, Parent.Name, (Select id, Name, ShowCaseOnly__c from ChildAccounts) from Account where id = :selectedSchoolId order by name];
	  			
	  			schoolGroupName = school.Parent.Name;
	  			
	  			listCampuses = new List<Selectoption>();	  			
	  			listCampuses.add(new Selectoption('', '-- Select Campus --'));	
	  			
				map<string, list<Account>> campusParentChildren = new map<string, list<Account>>();
				
				if(school.parentID != null)
					inSchoolGroup = true;
				else 
	  	  			inSchoolGroup = false;
	  	  			
	  	  		if(inSchoolGroup){
	  	  			
	  	  			
	  	  			for(Account ac: [Select id, name, isSelected__c, Parentid, Parent.Name, BillingCountry, ShowCaseOnly__c from Account where parent.parentID = :school.parentID and Disabled_Campus__c = false order by BillingCountry, Parent.Name, Name]){
	  	  				
	  	  				listCampuses.add(new Selectoption(ac.id, ac.Name));
	  	  				
	  	  				if(selectedCampus != null && selectedCampus.trim() != ''){
		  	  				schoolNames.put(ac.Parent.Name, ac.ParentID);
		  	  				
		  	  				if(mapCampusCampuses.containsKey(ac.BillingCountry)){
		  	  					if(mapCampusCampuses.get(ac.BillingCountry).containsKey(ac.Parent.Name))
		  	  						mapCampusCampuses.get(ac.BillingCountry).get(ac.Parent.Name).add(ac);	  	  						
		  	  					else
		  	  						mapCampusCampuses.get(ac.BillingCountry).put(ac.Parent.Name, new List<Account>{ac});
		  	  				} else
		  	  					mapCampusCampuses.put(ac.BillingCountry, new Map<String, List<Account>>{ac.Parent.Name => new List<Account>{ac}});
		  	  				
		  	  				mapCampuses.put(ac.id, ac);
	  	  				}
	  	  						
	  	  			}
	  	  			
	  	  			if(mapCampuses != null){
		  	  			createSupplierCampus(mapCampuses.keyset(), null);
		  	  			
		  	  			for(String country : mapCampusCampuses.keySet())
	      					for(String schoolName : mapCampusCampuses.get(country).keySet())
	      						for(Account campus : mapCampusCampuses.get(country).get(schoolName))
		  	  						if(supplierCampus.get(selectedCampus) != null && supplierCampus.get(selectedCampus).contains(campus.id))
		  	  							campus.isSelected__c = true;
		  	  						else
		  	  							campus.isSelected__c = false;	  	  							
	  	  			}				
	  	  			
	  	  			
	  	  			
	  	  		} else if(!inSchoolGroup) {
	  	  			
	  	  			for(Account ac: [Select id, name, isSelected__c, ShowCaseOnly__c from Account where parentid = :selectedSchoolId AND BillingCountry = :selectedSchoolCountry])
	  	  				mapCampuses.put(ac.id, ac);
	  	  				
	  	  			createSupplierCampus(mapCampuses.keyset(), selectedSchoolCountry);	
	  	  			
	  	  			
	  	  			for(String parent: mapCampuses.keySet()){
		  	  			for(String children: mapCampuses.keySet()){
		  	  				mapCampuses.get(children).isSelected__c = (supplierCampus.get(parent) != null)?supplierCampus.get(parent).contains(mapCampuses.get(children).id):false;
		          
			          		if(campusParentChildren.containsKey(mapCampuses.get(parent).id))
			            		campusParentChildren.get(mapCampuses.get(parent).id).add(mapCampuses.get(children).clone(true));
			          		else
			            		campusParentChildren.put(mapCampuses.get(parent).id, new list<Account>{mapCampuses.get(children).clone(true)});        
		  	  			}
		  	  		}
		  	  		
		  	  		mapCampusViewNum = campusParentChildren.size();
  	    			mapCampusCampuses.put(school.name, campusParentChildren);	  	  			
	  	  			inSchoolGroup = false;
	  	  		}
	  	  
  	  			
  	  	
	  	  		
	  	  		
	  			
	  		}
  		}
  		return mapCampusCampuses;
  	}
  
   /* SAVE */
  
	public pageReference saveSupplier(){
    	
    	Savepoint sp = Database.setSavepoint() ;
    	try{
      		
      		if(inSchoolGroup){
      			
      			List<Supplier__c> suppliers = new List<Supplier__c>();
      			
      			
      			for(String country : mapCampusCampuses.keySet())
      				for(String schoolName : mapCampusCampuses.get(country).keySet())
      					for(Account campus : mapCampusCampuses.get(country).get(schoolName))
							if(campus.isSelected__c)
      							suppliers.add(new Supplier__c(agency__c = selectedCampus, supplier__c = campus.id, Record_Type__c = 'campus'));      							
      			
      			system.debug('suppliers: ' + suppliers);
      			
      			delete [Select id from Supplier__c where agency__c = :selectedCampus];
      			insert suppliers;
      			
      			
      		} else {
      		
	      		List<Supplier__c> newSuppliers = new List<Supplier__c>();
	      		List<String> delSuppliers = new List<String>();
	      		list<string> agencyDel = new list<string>();//campus child
	      
		      	for(string sid: mapCampusCampuses.keySet()){
		        	for(String campus : mapCampusCampuses.get(sid).keySet()){
		          		delSuppliers.add(campus);
		          		for(Account agency: mapCampusCampuses.get(sid).get(campus)){
			          		agencyDel.add(agency.id);
			          		System.debug('=====> agency: '+agency);
			          		if(agency.isSelected__c){       
			            		System.debug('=====> agency is selected');      
			            		Supplier__c sup = new Supplier__c();
			            		sup.Record_Type__c = 'campus';
			            		sup.Agency__c = campus;
			            		sup.Supplier__c = agency.id;
			            		newSuppliers.add(sup);
		            		}              
		          		}          
		        	}
		      	}
		      	
	      		delete [Select id from Supplier__c where agency__c in :agencyDel and Supplier__c in :delSuppliers];
	      		System.debug('=====> newSuppliers: '+newSuppliers);    
	      		insert newSuppliers;
      		}
      		
    	}catch(Exception e){
    		
      		Database.rollback(sp);
      		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
      		ApexPages.addMessage(msg);
      		
    	}
		return null;
	}
	
	
	
	
	
	
	
	
}