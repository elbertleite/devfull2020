public with sharing class SendMassEmail {
	public boolean checkSelectedFromParent {get{if(checkSelectedFromParent==null) checkSelectedFromParent = false; return checkSelectedFromParent;}set;}
	public List<Contact> clients {get;set;}
	public String toClients {get;set;}
	public boolean sentEmailOwner {get;set;}
	public boolean emailImportant {get;set;}
	public String cc {get;set;}
	public String bcc {get;set;}
	public String subject {get;set;}
	public String body {get;set;}
	public String theMsg {get;set;}
	public boolean showMsg {get;set;}
	public boolean useSignature {get{if(useSignature == null) useSignature = true; return useSignature;}set;}
	private String fileNameS3;
	private String emailAction;
    public map<string,boolean> renderDocs {get;set;}
	public String EHFShortnerHost{ get { return IPFunctions.EHFShortnerHost; } private set; }
	public String enrolmentDocuments {get;set;}
	public string typeEmail {get;set;}
	public boolean blockAttach {
		get{
			if(blockAttach == null){
				blockAttach = userDetails.Account.Block_Auto_Attach__c;
			}
			return blockAttach;
		}
		set;
	}
	public client_course__c clientCourse {get;set;}
	client_course__c booking;

	/** Email Template */
	public String selectedTemplate {get;set;}
	public list<SelectOption> emailOptions {get;set;}
	public map<Id, Email_Template__c> mEmailTemplate {get;set;}

	private String paymentRef;
	public String tempBody {get;set;}
	private sObject objTemplate;
	private Back_Office_Control__c userBo;
	private list<string> fieldsTemplate;
	private map<string, Object> mapFields;
	private map<Id,String> mapSubject;

	private void findEmailTemplate(String agencyId, String category){
		emailOptions = new list<SelectOption>();
		mEmailTemplate = new map<Id, Email_Template__c>();
		mapSubject = new map<Id, String>();
		String mSubject;

		for(Email_Template__c et : [SELECT Email_Subject__c, Template__c, Template_Description__c FROM Email_Template__c WHERE Agency__c = :agencyId and Category__c = :category order by Template_Description__c, Email_Subject__c]){
			try{
				fieldsTemplate = emailTemplateDynamic.extractFieldsFromFile(et.Email_Subject__c, mapFields); 
				mSubject = emailTemplateDynamic.createSingleTemplate(mapFields, et.Email_Subject__c, fieldsTemplate, objTemplate);
				mapSubject.put(et.id, mSubject);
				emailOptions.add(new SelectOption(et.Id, et.Template_Description__c));
				mEmailTemplate.put(et.id, et);
			}catch(Exception e){
				system.debug('ERROR FOUND ON findEmailTemplate() ===> ' + e.getLineNumber() + ' <=== ===> '+e.getMessage());
				system.debug('fieldsTemplate==>' + fieldsTemplate);
				system.debug('et.Email_Subject__c==>' + et.Email_Subject__c);
			}
		}//end for

		String typeEmail = ApexPages.currentPage().getParameters().get('type');

		if(emailOptions.size()>0){
			if(typeEmail == 'emailToStudent' || typeEmail == 'sendLOO' || typeEmail == 'sendCOE'){
				emailOptions.add(0, new SelectOption('', '– Select Email Template –'));
			}
			selectedTemplate = emailOptions[0].getValue();
			changeTemplate();
		}
	}

	public void changeTemplate(){
		String typeEmail = ApexPages.currentPage().getParameters().get('type');
		selectedTemplate = ApexPages.currentPage().getParameters().get('template');
		system.debug('CALLED CHANGE TEMPLATE '+selectedTemplate);
		body = '';
		subject = '';
		if(selectedTemplate != null && selectedTemplate != ''){
			subject =  mapSubject.get(selectedTemplate);
			
			fieldsTemplate = emailTemplateDynamic.extractFieldsFromFile(mEmailTemplate.get(selectedTemplate).Template__c, mapFields); 
			tempBody = emailTemplateDynamic.createSingleTemplate(mapFields, mEmailTemplate.get(selectedTemplate).Template__c, fieldsTemplate, objTemplate);
			// if(useSignature)
			// 	body += getSignature();
		}
		else{
			//body = '';
			if(typeEmail == 'emailToStudent'){
				body += getSignature();
				tempBody = '';
			}
		}
	}
	/** END -- Email Template */
	private client_course__c cancelRequest {get;set;}
	public SendMassEmail(){
		virtualId= 0;
		enrolmentDocuments = '';
		renderDocs = new map<string,boolean>();
		String checkFromParent = ApexPages.currentPage().getParameters().get('checkSelectedFromParent');
		if(checkFromParent != null && checkFromParent == 'true'){
			checkSelectedFromParent = true;

			if(body == null && useSignature)
				body = '<br /><div style="margin-top: 20px;" class="sig">'+getSignature()+'</div>';

		} else
			checkSelectedFromParent = false;

		fileNameS3 = ApexPages.currentPage().getParameters().get('key');
		emailAction = ApexPages.currentPage().getParameters().get('rt');
		List<String> clientIds;
		String ids = ApexPages.currentPage().getParameters().get('ids');
		system.debug('ids====>'+ids);
		if(ids!= null && ids!=''){
			ids = ids.replace('[','').replace(']','').replace(' ','');
			clientIds = ids.split(',');
			clients = [Select id, Name, Email, remoteContactId__c, Owner__c, Mandrill_Emails_Status__c from Contact where id in :clientIds];
		    setToClients();

		    //Send selected documents in Enrolment PRocess page
			String bookingID = ApexPages.currentPage().getParameters().get('bk');
			if(bookingID != null && bookingID.trim() != '' && bookingID !='undefined'){
				booking = [select Email_Documents__c, (select id, Enrolment_Activities__c from booking_number__r) from client_course__c where id = :bookingID];
				if(booking.Email_Documents__c != null){
					List<String> filestr = booking.Email_Documents__c.split('#;#');
					List<String> enrolmentFiles = new List<String>();
					for(String file : filestr){
						String[] f = file.split('#>#');
						enrolmentFiles.add('<a href="'+f[1]+'" target="_blank">' + f[0] + '</a>');
					}
					enrolmentDocuments = String.join(enrolmentFiles, '<br/>') + '<br/>';
					body = '<br /><div class="attachments"><br/><b>Attachments:</b><br/>' + enrolmentDocuments + '</div>';

					if(useSignature)
						body += '<br /><div style="margin-top: 20px;" class="sig">'+getSignature()+'</div>';
				}
			}

			if(ApexPages.currentPage().getParameters().containsKey('cancelRequest')){ // Cancel Request Email
				cancelRequest = [SELECT Cancel_Course_Status__c, previewLink__c FROM client_course__c WHERE id = :ApexPages.currentPage().getParameters().get('cancelRequest') limit 1];
				
			}
			else{
				//Send LOO and Coe
				String clientCourseid = ApexPages.currentPage().getParameters().get('ccid');
				typeEmail = ApexPages.currentPage().getParameters().get('type');

				if(clients.size() == 1){
					if(typeEmail == 'sendLOO'){
						mapFields = IPFunctions.findTokens(IPFunctions.tokensCourse);
						list<sObject> listResult = emailTemplateDynamic.createListResult(' FROM client_course__c WHERE id = \''+clientCourseid +'\'', mapFields, null, new list<String>(mapFields.keySet()));	
						objTemplate = listResult[0];
						findEmailTemplate(userDetails.Accountid, 'Send LOO Student');
					} else if(typeEmail == 'sendCOE'){

						mapFields = IPFunctions.findTokens(IPFunctions.tokensCourse);
						list<sObject> listResult = emailTemplateDynamic.createListResult(' FROM client_course__c WHERE id = \''+clientCourseid +'\'', mapFields, null, new list<String>(mapFields.keySet()));	
						objTemplate = listResult[0];
						findEmailTemplate(userDetails.Accountid, 'Send COE Student');
					} else if(typeEmail == 'emailToStudent'){
						mapFields = IPFunctions.findTokens(IPFunctions.tokensContact);
						list<sObject> listResult = emailTemplateDynamic.createListResult(' FROM CONTACT WHERE id = \''+clients[0].id +'\'', mapFields, null, new list<String>(mapFields.keySet()));	
						objTemplate = listResult[0];
						findEmailTemplate(userDetails.Accountid, 'Send Student Email');
						system.debug('EMAILS FOUND '+mapSubject);
					}
					if(clientCourseid != null)
						clientCourse = [SELECT Id, 	Client__c, Client__r.Name, isPackage__c, Enrolment_Activities__c, (select id from course_package__r)
										FROM Client_course__c WHERE id =:clientCourseid];
				}
			}


	    if(body == null && useSignature)
				body = '<br /><div style="margin-top: 20px;" class="sig">'+getSignature()+'</div>';

		} else if(emailAction != null && fileNameS3 != null){//call S3

		    List<String> part = fileNameS3.split('/');
		    clientIds = new List<String>();

		    if(emailAction == 're' || emailAction == 'fwd'){
		    	if(emailAction == 're'){
			    	clients = [Select id, Name, Email, remoteContactId__c, Owner__c, Mandrill_Emails_Status__c from Contact where id = :part.get(2)];
				    setToClients();
		    	}else{
		    		clients = new list<Contact>();
		    	}

	    	    emailReplyS3(emailAction);
			}

		}


	}

	public void removeFilesFromBooking(){
		if(booking != null && booking.Email_Documents__c != null){
			booking.Email_Documents__c = '';
			update booking;
		}
	}


	private void emailReplyS3(String emailAction){

		EmailFromS3Controller s3 = new EmailFromS3Controller();
	   	s3.downloadEmailData(fileNameS3);

	    if(emailAction == 'fwd'){
	        ToClients = '';
	     } else toClients = clients != null && clients.size() > 0? clients[0].Email : '';

	     subject = emailAction.capitalize() +': ' + s3.downloadEmail.subject;

	     body = '';
		 body += '<br /><br /><br /><div class="reply" style="margin-top: 20px;">';
		 body += '---------------------------------------------------------------<br />';
		 body += s3.downloadEmail.bodyStr;
		 body += '</div>';


	}

	public String textSignature {
	  	get{
	  		if(textSignature == null){
	  			textSignature = '<div style="margin-top: 20px;" class="sig">' + userdetails.Name;
	            textSignature += '<br />' + userDetails.Account.Name;
	            textSignature += '<br />' + userDetails.Email + ' </div>';
	  		}
	  		return textSignature;
	  	}
	  	set;
	  }


	public Contact userDetails{
	    get{
	      if(userDetails == null){
	      	if(!Test.isRunningTest()){
	      		string contactId = [Select ContactId from user where id = :UserInfo.getUserId() limit 1].ContactId;
	       		userDetails = [select id, Name, Email, Signature__c, AccountId, Account.Name, Account.ParentId, Account.Global_Link__c, Account.Block_Auto_Attach__c from Contact where id = :contactId];
	      	} else {
	      		userDetails = new Contact();
	      	}


	      }
	      return userDetails;
	    }
	    set;
	}



	public String docCategory {get;set;}
	public List<SelectOption> categories {
		get {
		if(categories == null){
			categories = new List<SelectOption>();
			categories.add(new SelectOption('all', 'Select Category'));
			List<AggregateResult> lar = [Select Doc_Document_Category__c from Client_Document_File__c where Client__c = :clients[0].id and Doc_Document_Category__c != 'EmailAttachments' group by Doc_Document_Category__c];
			for(AggregateResult ar : lar)
				categories.add(new SelectOption( (String) ar.get('Doc_Document_Category__c'), (String) ar.get('Doc_Document_Category__c') ) );
		}
		return categories;
	}
		set;
	}

	public void reloadTypes(){
		types = null;
		docType = null;
		reloadFiles();
	}

	public void reloadFiles(){
		files = null;
	}

	public void selectFile(){
		String fileID = ApexPages.currentPage().getParameters().get('fileID');

		if(!selectedFiles.containsKey(fileID)){
			for(Client_Document_File__c cdf : files)
				if(fileID == cdf.Id){
					selectedFiles.put(fileID, cdf);
					break;
				}
		} else {
			selectedFiles.remove(fileID);
		}

	}

	private Map<String,Client_Document_File__c> selectedFiles = new Map<String,Client_Document_File__c>();

	public String docType {get;set;}
	public List<SelectOption> types {
		get {
		if(types == null){
			types = new List<SelectOption>();
			types.add(new SelectOption('all', 'Select Type'));
			List<AggregateResult> lar = [Select Doc_Document_Type__c from Client_Document_File__c where Client__c = :clients[0].id and Doc_Document_Category__c = :docCategory group by Doc_Document_Type__c];
			for(AggregateResult ar : lar)
				types.add(new SelectOption( (String) ar.get('Doc_Document_Type__c'), (String) ar.get('Doc_Document_Type__c') ) );

		}
		return types;
	}
		set;
	}

	public List<Client_Document_File__c> files {
		get{
			if( files == null && clients != null && clients.size() == 1 ){

				String sqlCat = '';
				String sqlType = '';
				if(docCategory != null && docCategory != '' && docCategory != 'all')
					sqlCat = ' and Doc_Document_Category__c = \''+docCategory+'\'';

				if(docType != null && docType != '' && docType != 'all')
					sqlType = ' and Doc_Document_Type__c = \''+docType+'\'';

				String sql = ' Select C.File_Name__c,Doc_Document_Type__c, Doc_Document_Category__c, C.File_Size__c, C.File_Size_in_Bytes__c, C.Preview_Link__c, CreatedDate, CreatedBy.Name, Selected__C, Client__c ' +
					' from Client_Document_File__c C where Client__c =\''+clients[0].id+'\' and Doc_Document_Category__c != \'EmailAttachments\' ' + sqlCat + sqlType + ' order by Doc_Document_Category__c, Doc_Document_Type__c, CreatedDate desc';


				files = Database.query(sql);

				for(Client_Document_File__c cdf : files)
					if(selectedFiles.containsKey(cdf.id))
						cdf.Selected__c = true;

			}
			return files;
		}
		set;
	}

	public string getSignature(){
	    if(useSignature && userDetails.Signature__c != null && userDetails.Signature__c != '' )
	      return userDetails.Signature__c;
	    else return textSignature;
	}

	private void setToClients(){
		Boolean sendBCC = ApexPages.currentPage().getParameters().get('sendAsBcc') == null ? false : Boolean.valueOf(ApexPages.currentPage().getParameters().get('sendAsBcc'));
		toClients = '';
		integer i = 0;
		if(!sendBCC){
			for(Contact a : clients){
				toClients += a.Email;
				i++;
				if(clients.size() != i)
					toClients += ', ';
			}
		}else{
			Bcc = '';
			List<String> toBcc = new List<String>(); 
			for(; i < clients.size(); i++){
				if(i == 0){
					toClients += clients.get(i).Email;
				}else{
					toBcc.add(clients.get(i).Email);
				}
			}
			if(toBcc.size() > 0){
				Bcc = String.valueOf(toBcc).replace('(','').replace(')','').replace(',',';');
			}
		}

	}

	public void getToEmails(){
		String clientEmails = ApexPages.currentPage().getParameters().get('toemails');
		if(clientEmails != null && clientEmails != ''){
			clientEmails = clientEmails.replace('[','').replace(']','').replace(' ','');
			List<String> emailsList = clientEmails.split(',');

			for(Contact ac : [Select id, name, Email from Contact where Email in :emailsList]){
				listClientEmail.put(ac.id, new Client(ac.id, ac.Name, ac.Email));
			}

		}

		showButton = false;

	}

	/** EPs PC Files **/
	public string getOrgId(){
		return 	UserInfo.getOrganizationId();
	}

	public list<userAttachments> lUserAttachments {get;set;}

	public class userAttachments{
		public Boolean isChecked {get;set;}
		public String fileName {get;set;}
		public String fileKey {get;set;}
	}

	public void addUserAttachment(){
		if(lUserAttachments == null)
			lUserAttachments = new List<userAttachments>();

		userAttachments file = new userAttachments();
		file.fileName = ApexPages.CurrentPage().getParameters().get('fileName');
		file.fileKey = ApexPages.CurrentPage().getParameters().get('fileKey');
		file.isChecked = true;

		lUserAttachments.add(file);

		system.debug('lUserAttachment====' + lUserAttachments);

	}

	/** END EPs PC Files **/

	public PageReference sendEmail(){

		try {
			mandrillSendEmail mse = new mandrillSendEmail();
		    mandrillSendEmail.messageJson email_md = new mandrillSendEmail.messageJson();

		    //create json to mail
		    System.debug('Clients: '+clients);
		    for(Contact a : clients){
		      if(a != null){
		      	email_md.setTo(a.Email, a.Name);
				email_md.setTag(a.ID);
			  }
			}

		    email_md.setFromEmail(userDetails.Email);
		    email_md.setFromName(userDetails.Name);
		    String emailService = IpFunctions.getS3EmailService();
	     	//email_md.setHeaders('Reply-To', userDetails.email+';'+emailService);
		    email_md.setSubject(subject);

			list<String> events = new list<String>{'send', 'open', 'click'};
			// //Test CallBack
	     	// email_md.setHeaders('url', 'https://devpartial-educationhify.cs5.force.com/ip/services/apexrest/mandrill');
	     	// email_md.setHeaders('description', 'test Callback');
	     	// email_md.setHeaders('events', 'send');


			String pHtml = ApexPages.currentPage().getParameters().get('signature');
			if(pHtml != null && pHtml != '')
				body = pHtml;



		    //customer documents

		    //email.Body__c += getAttachments();

		    boolean first = true;
		   	if (clients.size() == 1 && !files.isEmpty()) {
		   		for(Client_Document_File__c cdf : files){
			    	if(cdf.Selected__c){
			      		if(first){
			        		body += '<br/><br/><b>Attachments:</b><br/>';
			        		first = false;
			      		}
			        	body += '<a href="'+cdf.Preview_Link__c+'" >' + cdf.File_Name__c + '</a> <i>('+cdf.File_Size__c+')</i> <br/>';
			      	}
			    }
			}

			body = body.replace('<style type="text/css">', '<head><style>').replace('</style>', '</style></head>');

		    email_md.setHtml(body);

		    String[] toBcc;
		    String[] toCc;
		    if (Bcc != null && Bcc != ''){
		      Bcc = Bcc.replaceAll(',',';').replaceAll('\n',';');
		      toBcc = Bcc.split(';', 0);
		      for(String b_cc : toBcc) email_md.setBcc(b_cc, '');
		    }
		    if (Cc != null && Cc != ''){
		      Cc = Cc.replaceAll(',',';').replaceAll('\n',';');
		      toCc = Cc.split(';', 0);
		      for(String c_c : toCc) email_md.setCc(c_c, '');
		    }
		    if (sentEmailOwner)
		      email_md.setCc(userDetails.Email, '');

		    email_md.setImportant(emailImportant);
		    email_md.preserve_recipients(true);

		    //save mail into S3
			String emailID = null;
		    if(!Test.isRunningTest())

		    	/** Save email on Cancel Course path /
		    	if(ApexPages.currentPage().getParameters().get('type')!= null){

		    		String cancelType =  ApexPages.currentPage().getParameters().get('type');


		    		EmailToS3Controller s3 = new EmailToS3Controller();
				    DateTime currentTime = DateTime.now();
				    String myDate = currentTime.format('dd-MM-yyyy HH:mm:ss');

		    		client_course__c clientCourse = [SELECT Id, Client__c from Client_course__c where id = :ApexPages.currentPage().getParameters().get('cc') limit 1];

		    		Blob bodyblob = Blob.valueof(body);
				    s3.saveRefundEmailS3(bodyblob,clientCourse,subject, myDate, UserInfo.getUserId(), clients[0].email, cancelType);
		    	}

		    	else*/
		    		emailID = saveToS3();


		    //send email through mandrill
		    email_md.setMetadata('hify_id', emailID);
			mse.sendMail(email_md);

			//email_md.setTag('TESTING TAG');
			//

			update clients;

			if(typeEmail == 'sendEnrolForm'){
				List<Client_Course__c> updatecc = new List<Client_Course__c>();
				for(Client_Course__c cc : booking.booking_number__r){
					cc.Enrolment_Activities__c += EnrolmentManager.createActivity(EnrolmentManager.ACTIVITY_TYPE_EMAIL, subject, system.now(), userDetails.Name + ' ('+ userDetails.Account.Name+')');
					updatecc.add(cc);
				}
				update updatecc;
			} else if(typeEmail == 'sendLOO'){

				List<Client_Course__c> updatecc = new List<Client_Course__c>();
				clientCourse.LOO_Requested_to_CLI__c = true;
				clientCourse.LOO_Requested_to_CLI_On__c = system.now();
				clientCourse.LOO_Requested_to_CLI_By__c = UserInfo.getUserId();
				clientCourse.Enrolment_Activities__c += EnrolmentManager.createActivity(EnrolmentManager.ACTIVITY_TYPE_SYSTEM, EnrolmentManager.STATUS_LOO_SENT_TO_CLI, system.now(), userDetails.Name + ' ('+ userDetails.Account.Name+')');
				updatecc.add(clientCourse);

				if(clientCourse.isPackage__c)
					for(Client_Course__c pack : clientCourse.course_package__r){
						pack.LOO_Requested_to_CLI__c = true;
						pack.LOO_Requested_to_CLI_On__c = system.now();
						pack.LOO_Requested_to_CLI_By__c = UserInfo.getUserId();
						updatecc.add(pack);
					}


				update updatecc;

			} else if(typeEmail == 'sendCOE'){

				List<Client_Course__c> updatecc = new List<Client_Course__c>();
				clientCourse.COE_Sent_to_CLI__c = true;
				clientCourse.COE_Sent_to_CLI_On__c = system.now();
				clientCourse.COE_Sent_to_CLI_By__c = UserInfo.getUserId();
				clientCourse.Enrolment_Activities__c += EnrolmentManager.createActivity(EnrolmentManager.ACTIVITY_TYPE_SYSTEM, EnrolmentManager.STATUS_COE_SENT_TO_CLI, system.now(), userDetails.Name + ' ('+ userDetails.Account.Name+')');
				updatecc.add(clientCourse);

				if(clientCourse.isPackage__c)
					for(Client_Course__c pack : clientCourse.course_package__r){
						pack.COE_Sent_to_CLI__c = true;
						pack.COE_Sent_to_CLI_On__c = system.now();
						pack.COE_Sent_to_CLI_By__c = UserInfo.getUserId();
						updatecc.add(pack);
					}


				update updatecc;

			}
			else if(cancelRequest != null){

				if(cancelRequest.Cancel_Course_Status__c == client_course_cancel.TYPE_COURSE_TRANSFERRED || cancelRequest.Cancel_Course_Status__c == client_course_cancel.TYPE_COURSE_REFUNDED){
					cancelRequest.Cancel_Request_Confirmed_to_Client__c = true;
					cancelRequest.Enrolment_Activities__c = client_course_cancel.addActivity(cancelRequest.Id, 'Sent Confirmation to Client', toClients, subject);
				}
				else{
					cancelRequest.Enrolment_Activities__c = client_course_cancel.addActivity(cancelRequest.Id, 'Sent Email to Client', toClients, subject);
				}

				update cancelRequest;
			}
			else if(typeEmail == 'sendPayEmail'){

				clientCourse.Enrolment_Activities__c += EnrolmentManager.createActivity(EnrolmentManager.ACTIVITY_TYPE_EMAIL, 'Sent Email to Student - ' + subject, system.now(), userDetails.Name + ' ('+ userDetails.Account.Name+')');

				update clientCourse;
			}

		    theMsg = 'Email sent.';
		    showMsg = true;

			String createTaskVisaExpiry = ApexPages.currentPage().getParameters().get('createTaskVisaExpiry');
			if(createTaskVisaExpiry == 'true'){
				String ids = ApexPages.currentPage().getParameters().get('ids');
				List<String> clientIds = ids.replace('[','').replace(']','').replace(' ','').split(',');
				List<Custom_Note_Task__c> tasks = new List<Custom_Note_Task__c>();
				Custom_Note_Task__c newTask;
				for(Contact ctt : [Select id, Name, Email, remoteContactId__c, Owner__c from Contact where id in :clientIds]){
					newTask = new Custom_Note_Task__c();
					newTask.Subject__c = 'Visa Follow Up';
					newTask.Comments__c = 'Visa Follow Up.';
					newTask.isNote__c = false;
					newTask.Due_Date__c = Date.today().addDays(7);
					newTask.Related_Contact__c = ctt.id;
					newTask.Status__c = 'In Progress';
					newTask.AssignedOn__c = DateTime.now();
					newTask.AssignedBy__c = UserInfo.getUserId();
					newTask.Assign_To__c = ctt.Owner__c;
					tasks.add(newTask);
				}
				insert tasks;
			}



		} catch (Exception e){
			system.debug(e.getMessage());
			theMsg = e.getMessage();
		}


	    return null;
	  }

	  //S3 emails
	  public String saveToS3(){

	    EmailToS3Controller s3 = new EmailToS3Controller();
	    Datetime myDT = Datetime.now();
	    String myDate = myDT.format('dd-MM-yyyy HH:mm:ss');
	    Blob bodyblob = Blob.valueof(body);

		String emailHifyId = '' + Datetime.now().getTime();

	    //generateEmailToS3(Contact client, Contact employee, Boolean sendByEmployee, String subject, Blob body, Boolean hasAtt, String dateCreated)
		Map<String, String> status;
		Map<String, Map<String, String>> clientsEmailsStatus; 
		for(Contact a : clients){
	    	s3.generateEmailToS3(a, userDetails, true, subject, bodyblob, false, myDate, emailHifyId);
			if(String.isEmpty(a.Mandrill_Emails_Status__c)){
				clientsEmailsStatus = new Map<String, Map<String, String>>();
			}else{
				clientsEmailsStatus = (Map<String, Map<String, String>>) JSON.deserialize(a.Mandrill_Emails_Status__c, Map<String, Map<String, String>>.class);
			}
			status = new Map<String, String>();
			status.put('status','Sent');
			status.put('mandrillMsg','');
			clientsEmailsStatus.put(emailHifyId, status);
			a.Mandrill_Emails_Status__c = JSON.serialize(clientsEmailsStatus); 
		}

		return emailHifyId;

	  }


	//addClientEmailListTo

	public void removeEmail(){

		String emailID = ApexPages.currentPage().getParameters().get('emailID');

		if(emailId != null && emailId != ''){
			listClientEmail.remove(emailId);
			showButton = true;
		}
	}


	public string accountID{get; set;}
	public string accountName{get; set;}
	public string accountEmail{get; set;}

	public PageReference loadClientEmails(){

		String clientEmailIDs = ApexPages.currentPage().getParameters().get('clientEmailIDS');

		system.debug('clientEmailIDs: ' + clientEmailIDs);

		if(clientEmailIDs != null){
			clientEmailIDs = clientEmailIDs.replace('[','').replace(']','').replace(' ','');
			List<String> clientIds = clientEmailIDs.split(',');

			clients = [Select id, Name, Email, remoteContactId__c, Owner__c, Mandrill_Emails_Status__c from Contact where id in :clientIds];

			files = null;
			setToClients();

			//hasDocuments = null;
			//contactDocuments = null;

		}
		return null;
	}

	public Map<String, Client> listClientEmail{get{if(listClientEmail == null) listClientEmail = new Map<String, Client>(); return listClientEmail;} set;}

	public Integer mapSize {
		get {
        	return listClientEmail.size();
    	}
	}

	public String clientEmailIDs {
		get {
			clientEmailIDs = '';
			for(String id : listClientEmail.keySet())
				clientEmailIDs += id + ', ';
			return clientEmailIDs;
		}
		set;
	}

	public pageReference addAddress(){
		if(accountId != null && accountEmail != null && accountId != '' && accountEmail != '' && accountName != null && accountName != ''){
			listClientEmail.put(accountId, new Client(accountId, accountName, accountEmail));
			accountName = null;
			accountId = null;
			accountEmail = null;
			showButton = true;
		}
		return null;
	}

	public Boolean showButton {get;set;}


	public class Client {

		public string clientID{get; set;}
		public string clientName{get; set;}
		public string clientEmail{get; set;}

		public Client(String id, String name, String email){
			clientID = id;
			clientName = name;
			clientEmail = email;
		}
	}



	/**public boolean hasDocuments {
		get{
			if(hasDocuments == null)
				findContactDocuments();
			return hasDocuments;
		}
		set;
	}

	public Map<String, List<IPFunctions.ContactDocument>> contactDocuments {get;set;}
	public Map<String, List<IPFunctions.ContactDocument>> findContactDocuments(){
		hasDocuments = false;
  		if(contactDocuments == null && clients != null && clients.size() == 1){
  			AWSKeys credentials = new AWSKeys(IPFunctions.awsCredentialName);
  			contactDocuments = new Map<String, List<IPFunctions.ContactDocument>>();

  			List<IPFunctions.ContactDocument> cds = IPFunctions.getContactDocuments(clients.get(0).id, credentials.key, credentials.secret);
  			integer newfilesCount = cds != null && !cds.isEmpty() ? cds.size()+1 : 0;
  			List<IPFunctions.ContactDocument> migratedFiles = IPFunctions.getDocumentFiles(clients.get(0).id, newfilesCount);


  			renderDocs.put(null, true);

  			if(cds != null && !cds.isEmpty()){
  				hasDocuments = true;
	  			for( IPFunctions.ContactDocument cd : cds ){
	  				if(contactDocuments.containsKey(cd.docCategory)){
	  					contactDocuments.get(cd.docCategory).add(cd);
	  				} else{
	  					renderDocs.put(cd.docCategory, false);
	  					contactDocuments.put(cd.docCategory,  new List<IPFunctions.ContactDocument>{cd} );
	  				}
	  			}
  			}


  			if(migratedFiles != null && !migratedFiles.isEmpty()){
  				hasDocuments = true;
	  			for( IPFunctions.ContactDocument cd : migratedFiles ){
	  				if(contactDocuments.containsKey(cd.docCategory)){
	  					contactDocuments.get(cd.docCategory).add(cd);
	  				} else{
	  					renderDocs.put(cd.docCategory, false);
	  					contactDocuments.put(cd.docCategory,  new List<IPFunctions.ContactDocument>{cd} );
	  				}
	  			}
  			}


  		}

  		return contactDocuments;

	}**/

  	public class Doc {
        public String category {get;Set;}
        public list<DocumentDetails> listDocumentDetails{get{if(listDocumentDetails == null) listDocumentDetails = new list<DocumentDetails>(); return listDocumentDetails;} set;}
     	public list<IPFunctions.ContactDocument> listFiles{get{if(listFiles == null) listFiles = new list<IPFunctions.ContactDocument>(); return listFiles;} set;}
    }


    public class DocumentDetails{
        public Client_Document__c clientDocument {get{if(clientDocument == null) clientDocument = new Client_Document__c(); return clientDocument;} set;}

        public list<IPFunctions.ContactDocument> listFiles{get{if(listFiles == null) listFiles = new list<IPFunctions.ContactDocument>(); return listFiles;} set;}
    }

    private Map<String, Doc> docsMap;
    private List<Client_Document__c> listdocs {get;set;}
    private List<IPFunctions.ContactDocument> oldIPFiles {get;set;}
	public set<string> agencyGroupDocsCategory{get; set;}
    public Map<String,Doc> getDocs(){

        if(docsMap == null && !checkSelectedFromParent){
					system.debug('===> no checkSelectedFromParent');
        	docsMap = new Map<String,Doc>();
        	renderDocs = new map<string,boolean>();
        	//system.debug('clients[0]====> ' + clients[0].id + '  Name===> ' + clients[0].Name);


			
			if(typeEmail=='sendCOE'){
				listdocs = [Select Document_Category__c, C.Document_Type__c, lastModifiedDate, isSelected__c, Name, Client_Course__c, preview_link__c
                                from Client_Document__c C where ((Document_Category__c = 'Enrolment' and (client_course__c = :clientCourse.id OR client_Course__r.Course_Package__c = :clientCourse.id) AND Document_Type__c='COE') OR (Client__c = :clientCourse.client__c and Document_Category__c NOT IN ('Finance', 'Enrolment'))) and (Agency_Access_Restriction__c = null or Agency_Access_Restriction__c includes ( :userDetails.Accountid ) )  order by  Document_Category__c, Document_Type__c, CreatedDate desc];
			}
			else if(typeEmail == 'sendLOO'){
				listdocs = [Select Document_Category__c, C.Document_Type__c, lastModifiedDate, isSelected__c, Name, Client_Course__c, preview_link__c
                                from Client_Document__c C where  ((Document_Category__c = 'Enrolment' AND client_course__c = :clientCourse.id AND Document_Type__c IN ('LOO', 'COE')) OR (Client__c = :clientCourse.client__c and Document_Category__c NOT IN ('Finance', 'Enrolment'))) and (Agency_Access_Restriction__c = null or Agency_Access_Restriction__c includes ( :userDetails.Accountid ) )  order by  Document_Category__c, Document_Type__c, CreatedDate desc];
			}
			else{
				listdocs = [Select Document_Category__c, C.Document_Type__c, lastModifiedDate, isSelected__c, Name, Client_Course__c, client_course__r.course_package__c, preview_link__c FROM Client_Document__c C where Client__c = :clients[0].id and (Agency_Access_Restriction__c = null or Agency_Access_Restriction__c includes ( :userDetails.Accountid ) )  order by  Document_Category__c, Document_Type__c, CreatedDate desc];
			}

			agencyGroupDocsCategory = new set<string>();
			for(Account_Document_File__c cd:[SELECT ID, Preview_Link__c, Category__c FROM Account_Document_File__c   WHERE Parent__c = :userdetails.Account.ParentId]){
				listdocs.add(new Client_Document__c(Document_Category__c = cd.Category__c, preview_link__c = cd.Preview_Link__c));
				agencyGroupDocsCategory.add(cd.Category__c);
			}
			// if(agencyGroupDocsCategory.size() > 0)
			// 	categoriesOrder.addAll(agencyGroupDocsCategory);
				
			for(Client_Document__c cd : listDocs){

				if(!docsMap.containsKey(cd.Document_Category__c)){
					Doc doc = new Doc();
					doc.Category = cd.Document_Category__c;
					DocumentDetails dd = new DocumentDetails();
					dd.clientDocument = cd;
					if(cd.preview_link__c!= null)
						doc.listFiles = IPFunctions.getFilesFromPreviewLink(cd.preview_link__c, virtualId);

					doc.listDocumentDetails.add(dd);
					docsMap.put(doc.Category, doc);

					renderDocs.put(doc.Category, false);
				}else{
					Doc d = docsMap.get(cd.Document_Category__c);

					DocumentDetails dd = new DocumentDetails();
					dd.clientDocument = cd;
					if(cd.preview_link__c!= null)
						d.listFiles.addAll(IPFunctions.getFilesFromPreviewLink(cd.preview_link__c, virtualId));

					d.listDocumentDetails.add(dd);

					docsMap.put(d.Category, d);
				}

				virtualId += docsMap.get(cd.Document_Category__c).listFiles.size();

			}//end for

			if(typeEmail!='sendCOE' && typeEmail != 'sendLOO'){
				//Migrated Clients from IP
				oldIPFiles = new list<IPFunctions.ContactDocument>();
				if(clients[0].remoteContactId__c!=null){

					oldIPFiles = IPFunctions.getDocumentFiles(clients[0].id, virtualId+1);
					
					if(oldIPFiles != null)
						for(IPFunctions.ContactDocument cdfile : oldIPFiles){

							if(!docsMap.containsKey(cdFile.docCategory)){
								Doc doc = new Doc();
								doc.Category = cdFile.docCategory;
								doc.listFiles = new list<IPFunctions.ContactDocument>{cdfile};
								docsMap.put(cdFile.docCategory, doc);
								renderDocs.put(cdFile.docCategory, false);	
							}
							else{
								Doc d = docsMap.get(cdFile.docCategory);
								d.listFiles.add(cdfile);
								docsMap.put(cdFile.docCategory, d);
							}
						}//end for
				}		
			}


			// Cancel Request Documents
			if(cancelRequest != null){
				Doc doc = new Doc();
				doc.Category = 'Cancel Request Documents';
				
				doc.listFiles = IPFunctions.getFilesFromPreviewLink(cancelRequest.previewLink__c, virtualId + 1);

				docsMap.put(doc.Category, doc);
				renderDocs.put('Cancel Request Documents', true);
				
			}
			// END -- Cancel Request Documents
			


			if(typeEmail=='sendCOE' || typeEmail=='sendLOO')
				renderDocs.put('Enrolment', true);

	        // if(typeEmail=='sendLOO'){
			// 	S3Controller s3 = new S3Controller();
			// 	s3.constructor();
			// 	renderDocs.put('Enrolment', true);
			// 	Doc d = docsMap.get('Enrolment');
			// 	system.debug('@@@ documents: ' + d);
			// 	for(DocumentDetails dd : d.listDocumentDetails){
			// 		if(dd.clientDocument.Client_Course__c == clientCourse.id && dd.clientDocument.Document_Type__c=='LOO'){
			// 			 if(!Test.isRunningTest()){
			// 				 system.debug('dd.clientDocument==' + dd.clientDocument);
			// 				 List<IPFunctions.ContactDocument> docFiles = IPFunctions.getDocument(String.valueOf(dd.clientDocument.id), dd.clientDocument.name, s3, virtualId);
			// 				 d.listFiles.addAll(docFiles);
			// 				 virtualId += docFiles.size();
			// 			 }
			// 			 break;
			// 		 }
			// 	}
			// } else if(typeEmail=='sendCOE'){
			// 	S3Controller s3 = new S3Controller();
			// 	s3.constructor();
			// 	renderDocs.put('Enrolment', true);
			// 	Doc d = docsMap.get('Enrolment');
			// 	system.debug('@@@ documents: ' + d);
			// 	for(DocumentDetails dd : d.listDocumentDetails){

			// 		if((dd.clientDocument.Client_Course__c == clientCourse.id || dd.clientDocument.Client_Course__r.Course_Package__c == clientCourse.id) && dd.clientDocument.Document_Type__c=='COE'){
			// 			 if(!Test.isRunningTest()){
			// 				 system.debug('dd.clientDocument==' + dd.clientDocument);
			// 				 List<IPFunctions.ContactDocument> docFiles = IPFunctions.getDocument(String.valueOf(dd.clientDocument.id), dd.clientDocument.name, s3, virtualId);
			// 				 d.listFiles.addAll(docFiles);
			// 				 virtualId += docFiles.size();
			// 			 }
			// 		 }
			// 	}
			//  }

			

        }
				else if(docsMap == null && checkSelectedFromParent){
					system.debug('===> yes checkSelectedFromParent');
					docsMap = new Map<String,Doc>();
        	renderDocs = new map<string,boolean>();
				}

		return docsMap;


    }


	public integer virtualId {get;set;}
	// public void renderDoc(){
	// 	String docCat = ApexPages.currentPage().getParameters().get('cat');
	// 	renderDocs.put(docCat, true);

    //     S3Controller s3 = new S3Controller();
    //     s3.constructor();
    //     renderDocs.put(null, true);

    //     Doc d = docsMap.get(docCat);
    //     system.debug('==>listDocumentDetails: '+d.listDocumentDetails);
    //     for(DocumentDetails dd : d.listDocumentDetails){
	//         List<IPFunctions.ContactDocument> docFiles = IPFunctions.getDocument(String.valueOf(dd.clientDocument.id), dd.clientDocument.name, s3, virtualId);
	//           system.debug('==>docFiles: '+docFiles);
	//         d.listFiles.addAll(docFiles);

	//         system.debug('==>virtualId: '+virtualId);
	//         system.debug('==>docFiles.size(): '+docFiles.size());

	//         virtualId += docFiles.size();
	//          system.debug('==>virtualId2: '+virtualId);

	//          if(oldIPFiles != null){
    //             for(IPFunctions.ContactDocument cdfile : oldIPFiles){
    //             	system.debug('==>cdfile.clientDocumentID: '+cdfile.clientDocumentID);
    //             	system.debug('==>dd.clientDocument.id: '+dd.clientDocument.id);
	//             	if(cdfile.clientDocumentID == dd.clientDocument.id){
	//             		d.listFiles.add(cdfile);
	// 					//virtualId ++;
	// 					system.debug('==>virtualId3: '+virtualId);
	//             	}
    //             }

	//          }

    //     }//end for

    //      system.debug('==>endVirtual: '+virtualId);

	// }

 	private List<String> order = new List<String>{'Visa', 'Passport', 'Personal', 'Enrolment', 'Education', 'Cancel Request Documents'};
	public List<String> categoriesOrder {
        get{
            if(categoriesOrder == null){
                categoriesOrder = new List<String>();
                Schema.DescribeFieldResult fieldResult = Client_Document__c.Document_Category__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();


                for(String orderCat : order)
                    for(String category : getDocs().keySet())
                        if(orderCat == category)
                            categoriesOrder.add(orderCat);


                for(Schema.PicklistEntry f : ple){

                    Boolean found = false;
                    for(String orderCat : order)
                        if(orderCat == f.getValue())
                            found = true;

                    if(!found)
                        for(String category : getDocs().keySet())
                            if(category == f.getValue())
                                categoriesOrder.add(f.getValue());
                }



            }
            return categoriesOrder;
        }
        Set;

    }


}