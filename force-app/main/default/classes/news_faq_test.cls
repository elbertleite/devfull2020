/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class news_faq_test {
	
	static testMethod void myUnitTest() {
		
		Backoffice_System_Information__c bsi = new Backoffice_System_Information__c();
		bsi.Main_Category__c = 'News';
		bsi.title__c = 'My News';
		bsi.City__c = 'Sydney';
		bsi.Country__c = 'Australia';
		bsi.Expire_on__c = system.today().addDays(5);
		bsi.description__c = 'this is a news test';
		insert bsi;
		
		news_faq nf = new news_faq();
		String str = nf.allNewsFAQ;
		
		news_faq.getNewsFAQ('Sydney','news','all');
		news_faq.getNewsFAQ('','news','all');
		
	}
    
}