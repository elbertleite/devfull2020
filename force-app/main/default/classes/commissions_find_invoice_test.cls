@isTest
private class commissions_find_invoice_test {
	
	@isTest static void test_method_one() {
		list<Invoice__c> allInvoices = new list<Invoice__c>();
		allInvoices.add(new Invoice__c(isCommission_Invoice__c = true));
		allInvoices.add(new Invoice__c(isPDS_Confirmation__c = true));
		allInvoices.add(new Invoice__c(isShare_Commission_Invoice__c = true));

		insert allInvoices;

		allInvoices = [SELECT Share_Commission_Number__c, Request_Confirmation_Number__c, School_Invoice_Number__c, isCommission_Invoice__c, isPDS_Confirmation__c, isShare_Commission_Invoice__c FROM Invoice__c];

		commissions_find_invoice test = new commissions_find_invoice();
		
		for(Invoice__c inv : allInvoices){

			if(inv.isCommission_Invoice__c)
				test.invoiceNumber = inv.Share_Commission_Number__c;

			else if(inv.isPDS_Confirmation__c)
				test.invoiceNumber = inv.Request_Confirmation_Number__c;

			else if(inv.isShare_Commission_Invoice__c)
				test.invoiceNumber = inv.School_Invoice_Number__c;


			test.searchInvoice();
		}//end for
		test.invoiceNumber = '1231';
		test.searchInvoice();
	}
}