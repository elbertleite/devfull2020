public with sharing class ContactVisaExpiryRules {

	@InvocableMethod(label='Set Contact Visa Expiry Date' description='Check the contact\'s visas on their current destination and pick the MAX date')
	public static void checkVisaDates (List<string> contactId	) {

		map<String, String> contactCountries = new map<string,string>();

		system.debug('contactId===>' + contactId);

		for(String ct : contactId){
			list<String> ctDetails = ct.split('#');
			contactCountries.put(ctDetails[0], ctDetails[1]);
		}//end for

		map<String, Date> contactDates = new map<String,Date>();	

		system.debug('contactCountries.keySet===>' + contactCountries.keySet());
		
		
		for(client_document__c doc : [SELECT Visa_country_applying_for__c, Expiry_Date__c, client__c FROM Client_Document__c WHERE client__c in :contactCountries.keySet() AND Document_Category__c = 'Visa' AND Document_Type__c = 'Visa' AND Expiry_Date__c != NULL]){ 

			system.debug('doc===>' + doc);
			
			if(doc.Visa_country_applying_for__c == contactCountries.get(doc.client__c)){
				
				if(!contactDates.containsKey(doc.client__c))
					contactDates.put(doc.client__c, doc.Expiry_Date__c);
				
				else if(doc.Expiry_Date__c > contactDates.get(doc.client__c))
					contactDates.put(doc.client__c, doc.Expiry_Date__c);

			}
			else continue;

		}//end for
		list<Contact> updateContacts = new list<Contact>();

		system.debug('contactDates===>' + contactDates);
		
		for(String ctId : contactDates.keySet())
			updateContacts.add(new Contact(Id = ctId, Visa_Expiry_Date__c = contactDates.get(ctId)));

		update updateContacts;
	}
}