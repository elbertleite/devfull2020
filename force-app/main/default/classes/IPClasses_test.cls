@isTest
private class IPClasses_test{
	static testMethod void myUnitTest() {
		TestFactory tf = new TestFactory();
       	Account agencyGroup = tf.createAgencyGroup();
	
		agencyGroup.RDStation_Access_Token__c = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwczovL2FwaS5yZC5zZXJ2aWNlcyIsInN1YiI6Ii05RVNGODlDSEh6R2pDZ3ktcktXLWNBbFVwemlHbVIwZ2NQMkxFeUJCVzhAY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vYXBwLnJkc3RhdGlvbi5jb20uYnIvYXBpL3YyLyIsImV4cCI6MTUyOTcyMzQ0MiwiaWF0IjoxNTI5NjM3MDQyLCJzY29wZSI6IiJ9.KVptT_I58GPbLajX3MHRw8KTNcQv7OmhbXbaFDLN6ySxUlKtXwxisLnR48fPuHGwry3Q6biv354cUsoQdHun1zW7FrYWsnn24EhGWLq_9usGYGRsoxFeuWx-v5gX33m_29BiXAPQ-OjseYtcVu-n09u4bUqz7aJn5SBCCaqx1gsUIg9ku6PYZZfH6wSNXo5OCTT-CDJXvraBxpg1l_gMe4tmdhkIrgpkNTJQQqwFpf40RrfjSKf1L-57kj1NznpwdxkQYrvQdJh1-PCtbECnM8a9K1blbdBowCFqcLYImQRmeDhsC1FhUJM0m0OPCZS5JoBAZK8yLJkOJU3rQfYO1w';
		agencyGroup.RDStation_authorization__c = '5334a1f9c758260f3ee10aa918f242f1';
		agencyGroup.RDStation_Refresh_Token__c = 'z_x029aJlIIup4bdU1t2dxeqoZaXUWjkgiSdMwKErzg';
		agencyGroup.RDStation_credential__c = 'ace3f68387c03c8f6d2b685f2e383eb7';
		agencyGroup.RDStation_Token_Expiration__c = Date.today();
		
		update agencyGroup;

		Account agency = tf.createAgency();

		agency.View_Web_Leads__c = True;
		agency.parentID = agencyGroup.id;

		update agency;

		Contact emp = tf.createEmployee(agency);

		Contact clt = tf.createClient(agency);
		clt.Lead_Assignment_On__c = Datetime.now();

		Contact lead = tf.createClient(agency);

		Account school = tf.createSchool();

		emp.account = agency; 

       	User portalUser = tf.createPortalUser(emp);

		Test.startTest();
		system.runAs(portalUser){
			String s1 = '111';
			String s2 = '111';
			Boolean b1 = true;

			IPClasses.getDefaultNewContactPageHeaderFields();
			IPClasses.getOptionsShowFieldHeaderNewContactPage();

			IPCLasses.SelectOptions so = new IPClasses.SelectOptions(s1, s2);
			so = new IPClasses.SelectOptions(s1, s2, b1);
			so.value = s1;
			so.label = s2;
			so.disabled = b1;
			so.compareTo(so);

			IPCLasses.ClientsPerStatus cs = new IPClasses.ClientsPerStatus();
			cs.status = '111';
			cs.itemOrder = 111;
			cs.countOrder = 111;
			cs.totalClients = 111;
			cs.currentPage = 111;
			cs.id = '111';
			cs.totalSales = Decimal.valueOf(10);

			cs.equals(cs);
			cs.hashCode();
			cs.compareTo(cs);
			cs.setTotalClientsAndPages(10, 10);

			IPCLasses.ClientFromContact cfc = new IPClasses.ClientFromContact();
			cfc = new IPClasses.ClientFromContact(clt.id);
			cfc = new IPClasses.ClientFromContact(clt, null, null);
			cfc.equals(cfc);
			cfc.hashCode();
			cfc.travelDate = null;
			cfc.visaExpiry = null;
			cfc.lastModifiedDate = null;
			cfc.destination = null;
			cfc.flag = null;
			cfc.expectedDateToClose = '';
			cfc.age = '';
			cfc.owner = '';
			cfc.assignedTo = '';
			cfc.potentialSchool = '';

			IPCLasses.ClientChecklist ccl = new IPClasses.ClientChecklist();
			ccl = new IPClasses.ClientChecklist('All');
			ccl.activeChecklist = true;
			ccl.equals(ccl);
			ccl.hashCode();

			IPCLasses.AgencyChecklist acl = new IPClasses.AgencyChecklist('');
			acl.agencyId = '111';
			acl.agencyName = '111';
			acl.blockedForEdition = true;
			acl.hideChecklist = true;
			acl.equals(acl);
			acl.hashCode();
			acl.labelChecklistHistory = '';

			acl = new IPClasses.AgencyChecklist();

			IPCLasses.Stage stg = new IPClasses.Stage();
			stg.stageName = '111';
			stg.index = 111;
			stg.equals(stg);
			stg.hashCode();

			stg = new IPClasses.Stage('');
			stg.stageName = '111';
			stg.equals(stg);
			stg.hashCode();
			stg.compareTo(stg);

			IPCLasses.StageChecklist scl = new IPClasses.StageChecklist();
			scl.checkListId = '111';
			scl.checkerId = '111';
			scl.description = '111';
			scl.isChecked = true;
			scl.checkerName = '111';
			scl.checkingDate = Datetime.now();
			scl.markedToCheck = true;
			scl.checkListFollowUpId = '111';
			scl.itemOrder = 1;
			scl.subOptions = '';
			scl.optionSelected = '';
			scl.equals(scl);
			scl.hashCode();

			scl = new IPClasses.StageChecklist('');

			scl.compareTo(scl);

			IPCLasses.RDStationValidationRule rvr = new IPClasses.RDStationValidationRule();
			rvr.valid_options = null;

			IPCLasses.InfoFieldRDStation ifrd = new IPClasses.InfoFieldRDStation();
			ifrd.ptBR = null;
			ifrd.en = null;
			ifrd.es = null;
			ifrd.defaultValue = null;
			ifrd.equals(ifrd);
			ifrd.hashCode();

			IPCLasses.FieldOptionRDStation ford = new IPClasses.FieldOptionRDStation();
			ford.value = null;
			ford.label = null;

			IPCLasses.ResponseFieldRDStation rfrd = new IPClasses.ResponseFieldRDStation();
			rfrd.fields = null;

			IPCLasses.ResponseRDStation rrd = new IPClasses.ResponseRDStation();
			rrd.leads = null;

			IPCLasses.ResponseErrorRDStation rerd = new IPClasses.ResponseErrorRDStation();
			rerd.errors = null;

			IPCLasses.ErrorRDStation scerdsl = new IPClasses.ErrorRDStation();
			scerdsl.error_type = null;
			scerdsl.error_message = null;


			IPCLasses.LeadRDStation leadrd = new IPClasses.LeadRDStation();
			leadrd.id = null;
			leadrd.email = null;
			leadrd.name = null;
			leadrd.company = null;
			leadrd.job_title = null;
			leadrd.bio = null;
			leadrd.public_url = null;
			leadrd.created_at = null;
			leadrd.opportunity = null;
			leadrd.number_conversions = null;
			leadrd.user = null;
			leadrd.lead_stage = null;
			leadrd.uuid = null;
			leadrd.fit_score = null;
			leadrd.interest = null;
			leadrd.website = null;
			leadrd.cf_salesforceagencygroup = null;
			leadrd.cf_salesforceid = null;
			leadrd.cf_hify_url = null;
			leadrd.personal_phone = null;
			leadrd.mobile_phone = null;
			leadrd.city = null;
			leadrd.state = null;
			leadrd.last_marked_opportunity_date = null;
			leadrd.tags = null;
			leadrd.custom_fields = null;
			leadrd.first_conversion = null;
			leadrd.last_conversion = null;

			IPClasses.CustomFieldRDStation cfrds = new IPClasses.CustomFieldRDStation();  
			cfrds.SalesforceID = null;
			cfrds.SalesforceAgencyGroup = null;
			cfrds.hify_url = null;

			IPClasses.ConversionRDStation crds = new IPClasses.ConversionRDStation(); 
			crds.created_at = null;
			crds.cumulative_sum = null;
			crds.source = null;
			crds.content = null;
			crds.conversion_origin = null;

			IPClasses.ContentRDStation cords = new IPClasses.ContentRDStation();
			cords.address = null;
			cords.identificador = null;
			cords.city = null;
			cords.email_lead = null;
			cords.state = null;  
			cords.nome = null;
			cords.website = null;
			cords.estado = null;
			cords.bio = null; 
			cords.user_id = null;
			cords.cargo = null;  
			cords.cidade = null;   
			cords.lead_info_attributes = null;
			cords.company_attributes = null;

			IPClasses.ConversionOriginRDStation ConversionOriginRD = new IPClasses.ConversionOriginRDStation(); 
			ConversionOriginRD.source = null;
			ConversionOriginRD.medium = null;
			ConversionOriginRD.value = null;
			ConversionOriginRD.campaign = null;
			ConversionOriginRD.channel = null;


			IPClasses.AttributesRDStation AttributesRDStation = new IPClasses.AttributesRDStation(); 
			AttributesRDStation.mobile_phone = null;
			AttributesRDStation.personal_phone = null;
			AttributesRDStation.twitter = null;
			AttributesRDStation.facebook = null;
			AttributesRDStation.linkedin = null;
			AttributesRDStation.website = null;
			AttributesRDStation.name = null;
			AttributesRDStation.site = null;
			AttributesRDStation.phone = null;
			AttributesRDStation.address = null;
			AttributesRDStation.email = null;
			AttributesRDStation.size = null;
			AttributesRDStation.company_sector_id = null;

			IPClasses.FieldRDStation FieldRDStation = new IPClasses.FieldRDStation(); 
			FieldRDStation.uuid = null;
			FieldRDStation.api_identifier = null;
			FieldRDStation.custom_field = null;
			FieldRDStation.data_type = null;
			FieldRDStation.name = null;
			FieldRDStation.label = null;
			FieldRDStation.presentation_type = null;
			FieldRDStation.validation_rules = null;

			FieldRDStation = new IPClasses.FieldRDStation('1111');

			FieldRDStation.equals(FieldRDStation);
			FieldRDStation.hashCode();

			IPClasses.MappingRDStationSalesforce MappingRDStation = new IPClasses.MappingRDStationSalesforce();
			MappingRDStation.rdStationUuid = 'null';
			MappingRDStation.rdStationname = null;
			MappingRDStation.rdStationField_identifier = null;
			MappingRDStation.salesforceField = null;
			MappingRDStation.salesforcefieldRequireMapping = null;
			MappingRDStation.fieldType = null;
			MappingRDStation.fieldContentType = null;

			MappingRDStation.rdStationOptions = null;
			MappingRDStation.salesforceOptions = null;
			
			MappingRDStation.mapping = null;

			MappingRDStation.equals(MappingRDStation);
			MappingRDStation.hashCode();

			MappingRDStation = new IPClasses.MappingRDStationSalesforce('');
						

			IPClasses.LeadStatusTimeline LeadStatusTimeline = new IPClasses.LeadStatusTimeline('111'); 
			LeadStatusTimeline.stage = null;
			LeadStatusTimeline.status = 'null';
			LeadStatusTimeline.dateMarked = null;
			LeadStatusTimeline.daysInStatus = null;
			LeadStatusTimeline.statusMark = null;
			LeadStatusTimeline.itemOrder = null;

			LeadStatusTimeline.equals(LeadStatusTimeline);
			LeadStatusTimeline.compareTo(LeadStatusTimeline);
			LeadStatusTimeline.hashCode();

			IPClasses.ClientChecklistHistory cch = new IPClasses.ClientChecklistHistory();
			cch = new IPClasses.ClientChecklistHistory('');

			cch.idDtracking = '';
			cch.createdDate = Datetime.now();
			cch.destination = 'Australia';
			cch.activeChecklist = true;
			cch.city = '';
			cch.dateToClose = Date.today();
			cch.travelDate = Date.today();
			cch.arrivalDate = Date.today();
			cch.rdStationCampaign = '';
			cch.saleEstimatedValue = '';

			cch.equals(cch);
			cch.hashCode();
			cch.compareTo(cch);

			IPClasses.DataReport dr = new IPClasses.DataReport();
			dr = new IPClasses.DataReport('');
			dr = new IPClasses.DataReport('', 0);

			system.debug(dr.title);
			system.debug(dr.color);
			system.debug(dr.value);

			dr.equals(dr);
			dr.hashCode();
			dr.compareTo(dr);

			IPClasses.IPCloudMaterialLink ipml = new IPClasses.IPCloudMaterialLink();
			system.debug(ipml.type);
			system.debug(ipml.name);
			system.debug(ipml.link);

			IPClasses.IPCloudMaterialFile ipml2 = new IPClasses.IPCloudMaterialFile();
			system.debug(ipml2.name);
			system.debug(ipml2.link);

			IPClasses.IPCloudMaterialModule ipmm = new IPClasses.IPCloudMaterialModule();
			ipmm = new IPClasses.IPCloudMaterialModule('');
			ipmm = new IPClasses.IPCloudMaterialModule('','');
			ipmm = new IPClasses.IPCloudMaterialModule('','','');
			ipmm.files  = null;
			ipmm.links = null;
			ipmm.comments = null;
			ipmm.equals(ipmm);
			ipmm.hashCode();

			IPClasses.IPCloudTrainingMaterial ipctgm = new IPClasses.IPCloudTrainingMaterial(); 
			ipctgm.selected = ipmm;
			ipctgm.materialModules = new List<IPClasses.IPCloudMaterialModule>();
			ipctgm.materialModules.add(ipmm);
			ipctgm.materialModules.add(ipmm);

			ipctgm.init();
			ipctgm.openNext();

			IPClasses.IPCloudMaterial ipcm = new IPClasses.IPCloudMaterial();			
			ipcm.id  = '';
			ipcm.name = '';
			ipcm.type = '';
			ipcm.description = '';
			ipcm.createdBy = '';
			ipcm.parentFolderID = '';
			ipcm.parentFolderName = '';
			ipcm.materialAgencyGroupName = '';
			ipcm.lastModifiedDate = Datetime.now();
			ipcm.permissionLevelRequired = 0;
			ipcm.language = '';
			ipcm.previewLink = '';
			ipcm.materialLinks = '';
			ipcm.materialUrl = '';
			ipcm.estimatedHours = 0;
			ipcm.estimatedMinutes = 0;

			IPClasses.IPCloudLink ipcl = new IPClasses.IPCloudLink();
			ipcl = new IPClasses.IPCloudLink('','');			
			ipcl = new IPClasses.IPCloudLink('','','');	

			IPClasses.IPCloudLinkBreadCrumb iplbc = new IPClasses.IPCloudLinkBreadCrumb();
			iplbc = new IPClasses.IPCloudLinkBreadCrumb('','');
			iplbc.equals(iplbc);
			iplbc.hashCode();

			IPClasses.IPCloudFolder ipcf = new IPClasses.IPCloudFolder();
			ipcf = new IPClasses.IPCloudFolder('');
			ipcf = new IPClasses.IPCloudFolder('', '', '', '', 0, '', true);
			ipcf.equals(ipcf);
			ipcf.subFolders = null;
			ipcf.hashCode();

			IPClasses.ContactEmail contactEmail = new IPClasses.ContactEmail();
			contactEmail = new IPClasses.ContactEmail('');

			contactEmail.virtualId = '';
			contactEmail.subject = '';
			contactEmail.key = '';
			contactEmail.isFromEmployee = true;
			contactEmail.ipMail = true;
			contactEmail.hasAtt = true;
			contactEmail.idEmployee = '';
			contactEmail.ipdClient = '';
			contactEmail.employeeName = '';
			contactEmail.employeeEmail = '';
	        contactEmail.dateTimeOrder = Datetime.now();
			contactEmail.dateSent = '';
			contactEmail.dateOnlySent = '';
			contactEmail.clientName = '';
			contactEmail.clientEmail = '';
			contactEmail.bodyStr = '';
			contactEmail.body = '';
			contactEmail.toAddress = '';
			contactEmail.idClient = '';
			contactEmail.instalmentId = '';
			contactEmail.courseName = '';
			contactEmail.campusName = '';
			contactEmail.instNumber = '';
			contactEmail.invoice = '';
			contactEmail.invoiceNumber = '';
			contactEmail.splitNumber = '';
			contactEmail.showPFS = true;
			contactEmail.showNet = true;
			contactEmail.emailType = ''; 
			contactEmail.showEmail = true; 
			
			contactEmail.equals(contactEmail);
			contactEmail.compareTo(contactEmail);
			contactEmail.hashCode();

			IPClasses.OwnershipHistory ownershipHistory = new IPClasses.OwnershipHistory();
			ownershipHistory = new IPClasses.OwnershipHistory('');
			ownershipHistory.fromAgency = '';
			ownershipHistory.fromUser = '';
			ownershipHistory.fromAgencyID = '';
			ownershipHistory.fromUserID = '';
			ownershipHistory.toAgency = '';
			ownershipHistory.toUser = '';
			ownershipHistory.toAgencyID = '';
			ownershipHistory.toUserID = '';
			ownershipHistory.action = '';
			ownershipHistory.createdByUser = '';
			ownershipHistory.createdByUserID = '';
			ownershipHistory.createdDate = null;
		
			ownershipHistory.compareTo(ownershipHistory);

			IPClasses.RDStationHifyData rdhd = new IPClasses.RDStationHifyData();
			rdhd.createdDate = null;
			rdhd.uuid = null;
			rdhd.owner = null;
			rdhd.publicURL = null;
			rdhd.tags= null;
			rdhd.conversions = null;
		}
			IPClasses.PMCCourse pMCCourse = new IPClasses.PMCCourse();
			pMCCourse = new IPClasses.PMCCourse(new Campus_Course__c());

			IPClasses.PMCCourseCategory pMCCourseCategory = new IPClasses.PMCCourseCategory();
			pMCCourseCategory.category = '';
			pMCCourseCategory = new IPClasses.PMCCourseCategory('');
			pMCCourseCategory.equals(pMCCourseCategory);
			pMCCourseCategory.hashCode();
			pMCCourseCategory.compareTo(pMCCourseCategory);

			IPClasses.PMCCourseContainer PMCCourseContainer = new IPClasses.PMCCourseContainer();
			PMCCourseContainer = new IPClasses.PMCCourseContainer('');
			PMCCourseContainer.equals(PMCCourseContainer);
			PMCCourseContainer.hashCode();
			PMCCourseContainer.compareTo(PMCCourseContainer);


			IPClasses.PMCFile PMCFile = new IPClasses.PMCFile();
			PMCFile.ID = '';
			PMCFile.name = '';
			PMCFile.description = '';
			PMCFile.parentID = '';
			PMCFile.createdDate = Datetime.now();
			PMCFile.createdBy = '';
			PMCFile.lastModifiedDate = Datetime.now();
			PMCFile.lastModifiedBy = '';
			PMCFile.type = '';
			PMCFile.url = '';
			PMCFile.fileKey = '';
			PMCFile.folder = '';
			PMCFile.downloadUrl = '';
			PMCFile.isSelected = true;
			PMCFile.metadataType = '';

			IPClasses.TaskWrapper TaskWrapper = new IPClasses.TaskWrapper();

			TaskWrapper.id = '';
			TaskWrapper.subject = '';
			TaskWrapper.comments = '';
			TaskWrapper.status = '';
			TaskWrapper.taskStatus = '';
			TaskWrapper.createdBy = '';
			TaskWrapper.assignedTo = '';
			TaskWrapper.lastModifiedBy = '';
			
			TaskWrapper.showTask = true;
			TaskWrapper.dueDate = Date.today();
			TaskWrapper.createdDate = Datetime.now();
			TaskWrapper.assignedOn = Datetime.now();
			TaskWrapper.lastModifiedDate = Datetime.now();

			TaskWrapper.history = '';
			TaskWrapper.relatedQuoteID = '';
			TaskWrapper.relatedQuoteName = '';
			TaskWrapper.relatedQuoteStatus = '';

			TaskWrapper.equals(TaskWrapper);
			TaskWrapper.hashCode();
			TaskWrapper.compareTo(TaskWrapper);
	
			IPClasses.NewContactPageHeaderFields NewContactPageHeaderFields = new IPClasses.NewContactPageHeaderFields();
			NewContactPageHeaderFields.fieldName = '';
			NewContactPageHeaderFields.fieldLabel = '';
			NewContactPageHeaderFields.show = 'show';
			NewContactPageHeaderFields.equals(NewContactPageHeaderFields);
			NewContactPageHeaderFields.hashCode();
			NewContactPageHeaderFields.getShowFieldLabel();
			NewContactPageHeaderFields.show = 'hidden';
			NewContactPageHeaderFields.getShowFieldLabel();
			NewContactPageHeaderFields.show = 'hiddenf';
			NewContactPageHeaderFields.getShowFieldLabel();

			IPClasses.Paginator Paginator = new IPClasses.Paginator();
			Paginator.page = 1;
			Paginator.firstResult = 1;
			Paginator.lastResult = 1;
			Paginator = new IPClasses.Paginator(1);
			Paginator = new IPClasses.Paginator(1, 1, 1);
			Paginator.equals(Paginator);
			Paginator.hashCode();

			IPClasses.S3File S3File = new IPClasses.S3File();
			S3File.previewLink = '1';
			S3File.fileName = '1';
			S3File.downloadURL = '1';
			S3File = new IPClasses.S3File('1');
			S3File = new IPClasses.S3File('1', '1', '1');
			S3File.equals(S3File);
			S3File.hashCode();

			IPClasses.SavedItemCampus SavedItemCampus = new IPClasses.SavedItemCampus();
			SavedItemCampus.item = '';
			SavedItemCampus.value = '';
	}	
}