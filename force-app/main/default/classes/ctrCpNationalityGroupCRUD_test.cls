/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ctrCpNationalityGroupCRUD_test {

    static testMethod void verifyNationalityGroup(){
		
		
		Map<String,String> recordTypes = new Map<String,String>();
       
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
			
		Account account = new Account();
		account.name = 'IP School';
		account.RecordTypeId = recordTypes.get('School');	 
		account.BillingCity = 'Sydney';
		account.BillingCountry = 'AUS';
		account.BillingState = 'NSW';
		account.BillingStreet = 'Alexander ST';
		account.BillingPostalCode = '2010';
		insert account;
		
		Nationality_Group__c ng = new Nationality_Group__c();
		ng.Country__c = 'Tasmania';
		ng.Name = 'Oceania';
		
		
		
		ctrCpNationalityGroupCRUD ngc = new ctrCpNationalityGroupCRUD();
		
		ngc.acco = account;
		
		List<ctrCpNationalityGroupCRUD.NationalityGroup> lis = ngc.nationGroupList;
		
		Nationality_Group__c ngInsert = new Nationality_Group__c();
		ngInsert.Account__c = account.Id;		
		ngInsert.Country__c = 'Brazil';
		ngInsert.Name = 'South America';
		insert ngInsert;
		
		
		
		List<String> selectedCountries = ngc.selectedCountries;
		
		Apexpages.currentPage().getParameters().put('groupName','South America');		
		ngc.selectedCountries = new List<String>{'Argentina', 'Chile'};
		ngc.addCountry();
		
		lis = ngc.nationGroupList;
		
		ngc.newGroupName = 'Europe';
		ngc.addNewGroup();
		
		List<SelectOption> listcountries = ngc.countryList;

		Apexpages.currentPage().getParameters().put('groupName','South America');
		Apexpages.currentPage().getParameters().put('countryName','Argentina');
		ngc.removeCountry();
		
		Apexpages.currentPage().getParameters().put('oldName','South America');
		Apexpages.currentPage().getParameters().put('newName', 'Latin America');
		ngc.saveName();
		
		Apexpages.currentPage().getParameters().put('groupName','South America');
		Apexpages.currentPage().getParameters().put('groupId',ngInsert.id);
		ngc.removeGroup();
		
		ngc.saveChanges();
		
		
		
	
		
		
		
	}
}