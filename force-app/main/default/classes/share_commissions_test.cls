/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class share_commissions_test {

    static testMethod void myUnitTest() {
      TestFactory tf = new TestFactory();

     	Account school = tf.createSchool();

     	Account agencyGroup = tf.createAgencyGroup();

     	Account agency = tf.createSimpleAgency(agencyGroup);
     	Account agency2 = tf.createSimpleAgency(agencyGroup);

    	Contact emp = tf.createEmployee(agency);

     	User portalUser = tf.createPortalUser(emp);

     	Account campus = tf.createCampus(school, agency);
     	Course__c course = tf.createCourse();
     	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
      Department__c department = tf.createDepartment(agency);

	    Contact client = tf.createLead(agency, emp);

	    client_course__c booking = tf.createBooking(client);
     	client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);

	    List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);

     	cc.Enrolment_Date__c = System.today();
     	cc.Enroled_by_Agency__c = agency2.id;
     	cc.Commission_Type__c = 0;
	    update cc;

  		instalments[0].Received_By_Agency__c = agency.id;
  		instalments[0].Received_Date__c = System.today();
  		instalments[0].Commission_Confirmed_On__c = System.today();
  		update instalments;

     	Test.startTest();
       //	system.runAs(portalUser){

			share_commissions testClass = new share_commissions();
			testClass.groupShareCommission();

			for(string ag:testClass.listCourses.keySet())
	    	for(client_course_instalment__c cps:testClass.listCourses.get(ag))
	    		cps.Commission__c = 7;

			testClass.saveUpdates();

			for(string ag:testClass.listCourses.keySet())
	    	for(client_course_instalment__c cps:testClass.listCourses.get(ag))
	    		cps.client_course__r.Commission_Type__c = 1;

	    	testClass.saveUpdates();


			List<SelectOption> periods = testClass.getPeriods();
			List<SelectOption> CommissionAction =  testClass.getCommissionAction();
			List<SelectOption> dateCondition = testClass.getdateCondition();
			string selectedAgencyGroup = testClass.selectedAgencyGroup;
			List<SelectOption> agencyGroupOptions = testClass.agencyGroupOptions;
			List<SelectOption> agencyOptions = testClass.getAgencies();
			//list<SelectOption>  Departments = testClass.getDepartments();

			testClass.changeGroup();

			testClass.SelectedPeriod = 'NEXT_MONTH';
			testClass.SelectedDateCondition = 'confirmed';
			testClass.cancelUpdates();

			testClass.SelectedDateCondition = 'unconfirmed';
			testClass.searchCoursesSharing();

			testClass.SelectedPeriod = 'THIS_MONTH';
			testClass.SelectedDateCondition = 'confirmed';
			testClass.searchCoursesSharing();

			testClass.SelectedDateCondition = 'unconfirmed';
			testClass.searchCoursesSharing();

			testClass.SelectedPeriod = 'LAST_MONTH';
			testClass.searchCoursesSharing();

			testClass.isPds = false;
			testClass.isPcs = true;
			testClass.isNet = true;
			testClass.SelectedDateCondition = 'unconfirmed';
			testClass.searchCoursesSharing();

			testClass.getListCoursesEmpty();
			List<SelectOption> searchType =  testClass.getSearchType();
			testClass.changeSearchType();
			boolean showConfirmed = testClass.showConfirmed;

			String SelectedCommissionAction = testClass.SelectedCommissionAction;
       //	}
    }
}