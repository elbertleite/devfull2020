/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ContactUserController_test {

    static testMethod void myUnitTest() {
    	
    	Map<String,String> recordTypes = new Map<String,String>();
    	for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
        
		TestFactory tf = new TestFactory();
		Account agency = tf.createAgency();
		Contact employee = tf.createEmployee(agency);
		User portalUser = tf.createPortalUser(employee);		
		
		Contact employeeWithoutUser = new Contact();
		employeeWithoutUser.FirstName = 'Johnny';
		employeeWithoutUser.LastName = 'Doe';
		employeeWithoutUser.Email = 'johnnydoe@educationhify.com.test';
		employeeWithoutUser.recordtypeid = recordTypes.get('Employee');		
		employeeWithoutUser.Preferable_nationality__c = 'Brazil';
		employeeWithoutUser.Preferable_School_City__c = 'Sydney';
		employeeWithoutUser.Preferable_School_Country__c = 'Australia';
		employeeWithoutUser.Accountid = agency.id;
		employeeWithoutUser.Nationality__c = 'Brazil';		 
		insert employeeWithoutUser;
		
		
		Test.startTest();
		system.runAs(portalUser){
			ApexPages.currentPage().getParameters().put('id', agency.id);
			ContactUserController cuc = new ContactUserController();
			cuc.nContact.FirstName = 'Johnny';
			cuc.nContact.LastName = 'Bravo';
			cuc.nContact.email = 'johnnybravo@educationhify.com';
			cuc.saveContact();
			cuc.getProfiles();
			
			
			ApexPages.currentPage().getParameters().remove('id');
			ApexPages.currentPage().getParameters().put('ct', employee.id);
			cuc = new ContactUserController();
			cuc.saveContact();
			
			
			ApexPages.currentPage().getParameters().remove('id');
			ApexPages.currentPage().getParameters().put('ct', employeeWithoutUser.id);
			cuc = new ContactUserController();
			cuc.saveContact();
		}
		Test.stopTest(); 

    }
}