public with sharing class Batch_Fill_Agency_On_Cycles{
	public Batch_Fill_Agency_On_Cycles(){
		system.debug('');
	}
}
/*global class Batch_Fill_Agency_On_Cycles implements Database.Batchable<sObject> {
    global Batch_Fill_Agency_On_Cycles() {}

    global Database.QueryLocator start(Database.BatchableContext BC) {
		String query = 'SELECT ID, CreatedBy.Name, CreatedBy.Contact.Account.ID, CreatedBy.Contact.Account.Parent.ID, Agency_Group__c, Client__r.Account.ID, Client__r.Account.Parent.ID, Client__r.Current_Agency__c, Client__r.Current_Agency__r.Parent.ID, Client__r.Original_Agency__c, Client__r.Original_Agency__r.Parent.ID, Current_Agency__c FROM Destination_Tracking__c';
				
		return Database.getQueryLocator(query);
	}
	
   	global void execute(Database.BatchableContext BC, List<Destination_Tracking__c> cycles) {
		String currentAgency;
        String originalAgency;
        for(Destination_Tracking__c cycle : cycles){
            if(!String.isEmpty(cycle.CreatedBy.Contact.Account.ID) && cycle.Agency_Group__c == cycle.CreatedBy.Contact.Account.Parent.ID){
                originalAgency = cycle.CreatedBy.Contact.Account.ID;
            }else if(!String.isEmpty(cycle.Client__r.Original_Agency__c) && cycle.Agency_Group__c == cycle.Client__r.Original_Agency__r.Parent.ID){
                originalAgency = cycle.Client__r.Original_Agency__c;
            }else if(!String.isEmpty(cycle.Client__r.Account.ID) && cycle.Agency_Group__c == cycle.Client__r.Account.Parent.ID){
                originalAgency = cycle.Client__r.Account.ID;
            }else if(!String.isEmpty(cycle.Client__r.Current_Agency__c) && cycle.Agency_Group__c == cycle.Client__r.Current_Agency__r.Parent.ID){
                originalAgency = cycle.Client__r.Current_Agency__c;
            }

            if(!String.isEmpty(cycle.Client__r.Current_Agency__c) && cycle.Agency_Group__c == cycle.Client__r.Current_Agency__r.Parent.ID){
                currentAgency = cycle.Client__r.Current_Agency__c;
            }else if(!String.isEmpty(cycle.Client__r.Account.ID) && cycle.Agency_Group__c == cycle.Client__r.Account.Parent.ID){
                currentAgency = cycle.Client__r.Account.ID;
            }else if(!String.isEmpty(cycle.Client__r.Original_Agency__c) && cycle.Agency_Group__c == cycle.Client__r.Original_Agency__r.Parent.ID){
                currentAgency = cycle.Client__r.Original_Agency__c;
            }
            
            cycle.Current_Agency__c = currentAgency;
            cycle.Created_By_Agency__c = originalAgency;
        }
        update cycles;
	}
	
	global void finish(Database.BatchableContext BC) {}
}*/