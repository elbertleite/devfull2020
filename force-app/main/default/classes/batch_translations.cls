global class batch_translations implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful {

	global String sessionID;
	global batch_translations(String sessionID){
		this.sessionID = sessionID;				
	}
	
   	global Database.QueryLocator start(Database.BatchableContext BC){
   		
   		String query = 'Select id, Language__c, course_field__c, course_category__c, course_form_of_delivery__c, course_language_level_required__c, course_period__c, course_type__c, '+
								'Campus_Course__r.Course__r.Course_Category__c, Campus_Course__r.Course__r.Course_Type__c, Campus_Course__r.Course__r.Type__c, Campus_Course__r.Course__r.Form_of_Delivery__c, '+ 
								' Campus_Course__r.Period__c, Campus_Course__r.Language_Level_Required__c '+
								' from translation__c where type__c = \'course\' ';
   		
    	return Database.getQueryLocator(query);
   	}

   	global void execute(Database.BatchableContext BC, List<translation__c> translations){
     	
     	TranslationService.updateWorkbenchTranslations(sessionID, translations);
     	
   	}

   	global void finish(Database.BatchableContext BC){
   		
   	}
	
}