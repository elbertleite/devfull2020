/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class IPFunctionsGlobal_test {

    static testMethod void myUnitTest() {

		Map<String,String> recordTypes = new Map<String,String>();
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);

		Account globalLink = new Account();
		globalLink.Name = 'Global Link';
		globalLink.RecordTypeId = recordTypes.get('Global Link');
		insert globalLink;

    
    	TestFactory tf = new TestFactory();
		Account agency = tf.createAgency();
		Contact employee = tf.createEmployee(agency);
		User portalUser = tf.createPortalUser(employee);		
		
		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);
		Course__c course = tf.createCourse();
		Campus_Course__c cc = tf.createCampusCourse(campus, course);

		Contact client = tf.createClient(agency);

		Quotation__c quote = tf.createQuotation(client, cc);

		client_course__c booking = tf.createBooking(client);
		
		Test.startTest();
		system.runAs(portalUser){
			
			Contact ct = tf.createLead(agency, employee);
			Web_Search__c ws = new Web_Search__c();
			ws.Client__c = ct.id;
			ws.Location__c = 2;
			ws.Nationality__c = 'Australia';
			ws.Combine_Quotation__c = false;			
			ws.Total_Products__c = 0;
			ws.Total_Products_Other__c = 0;
			insert ws;
			
			IPFunctionsGlobal.deleteClientCart(ws.id);
			IPFunctionsGlobal.CountryDestinations();

			IPFunctionsGlobal.getAgencyGroups(globalLink.id);
			IPFunctionsGlobal.recreateQuote(quote.id, 'Brazil');
			IPFunctionsGlobal.errorCoursesModified(new list<client_course__c>{booking});
		}
		Test.stopTest(); 
    
    }
}