public class showcasedestinationMenu {
	
	public String accoID {get; Set;}
	public String showcaseType {get; Set;}  


	private Account showcase;
	public Account getShowcase(){
		
		if(accoId == null || ApexPages.currentPage().getParameters().get('act') == 'new')
			accoId = ApexPages.currentPage().getParameters().get('country');
		
		if(accoid != null){
			showcase =  [Select Id, Name, recordtype.name, Parentid, (Select id, Name from ChildAccounts where recordtype.name = 'City') from Account A where id= :accoID];
			showcaseType = showcase.recordtype.name;
		}
		return showcase;
	}
	
	private String fileURL;
	public String getfileURL(){
   		if(fileURL == null){
			string accountPicture;
			try{
				accountPicture = [Select id from Account_Picture_File__c where WIP__c = false and Content_Type__c LIKE 'image/%' and Parent__c = :accoID ORDER BY LastModifiedDate DESC LIMIT 1].id;
				// URL for pictures
				if(accountPicture != null && accountPicture != ''){
					fileURL = cg.SDriveTools.getAttachmentURL(accoID, accountPicture, (100 * 60));
				} else {
					fileURL = 'NoPhoto';
				}
			}catch(Exception e){ fileURL = 'NoPhoto'; }
		}	
		return fileURL;
		
	}
	
    
}