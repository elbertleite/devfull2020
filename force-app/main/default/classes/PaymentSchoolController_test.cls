/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PaymentSchoolController_test {

    static testMethod void myUnitTest() {
        TestFactory tf = new TestFactory();
       
       	Account school = tf.createSchool();
       	Account agency = tf.createAgency();
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
       	Account campus = tf.createCampus(school, agency);
       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
        Department__c department = tf.createDepartment(agency);
       
       	Test.startTest();
       	system.runAs(portalUser){
			Contact client = tf.createLead(agency, emp);
			client_course__c booking = tf.createBooking(client);
	       	client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       	client_course__c cc2 =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       	client_product_service__c product = tf.createCourseProduct(booking, agency);
	       	
	       	
			List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
			instalments[0].Received_By_Agency__c = agency.id;
			instalments[0].Received_Date__c = system.today();
			
			instalments[1].Received_By_Agency__c = agency.id;
			instalments[1].Received_Date__c = system.today();
			instalments[1].isPFS__c = true;
			
			update instalments;
			
	       	Client_Document__c receiptDoc = new Client_Document__c (Client__c = client.id, Document_Category__c = 'Finance', Document_Type__c = 'School Installment Receipt', Client_course__c = cc.id, client_course_instalment__c = instalments[0].id);
			insert receiptDoc;
			
			cc.Enrolment_Date__c = System.today();
			update cc;
			
			PaymentSchoolController testClass = new PaymentSchoolController();
			
			list<SelectOption> agencyGroupOptions = testClass.agencyGroupOptions;
	       	testClass.getAgencies();
	       	
			testClass.setupPaymentPage();
			
			
			ApexPages.currentPage().getParameters().put('id', instalments[0].id);
			testClass.saveChanges();
			
			ApexPages.currentPage().getParameters().remove('id');
			ApexPages.currentPage().getParameters().put('id', receiptDoc.id);
			ApexPages.currentPage().getParameters().put('inst', instalments[0].id);
			testClass.generatePreviewLink();
			
			ApexPages.currentPage().getParameters().remove('inst');
			ApexPages.currentPage().getParameters().put('inst', instalments[1].id);
			testClass.generatePreviewLink();
			
			ApexPages.currentPage().getParameters().remove('id');
			ApexPages.currentPage().getParameters().put('id', instalments[0].id);
			testClass.saveChanges();
			
			ApexPages.currentPage().getParameters().put('id', instalments[1].id);
			testClass.cancelChanges();
			
			
			list<SelectOption> Schools = testClass.getSchools();
			Contact ct = testClass.filterDate;
			list<SelectOption> paymentTypes = testClass.paymentTypes;
			String selectedPaymentStatus =  testClass.selectedPaymentStatus;
			List<SelectOption> paymentStatus = testClass.paymentStatus;
			
			testClass.changeGroup();
			testClass.changeAgency();
			
			list<SelectOption> Departments = testClass.getDepartments();
			List<SelectOption> schoolOptions = testClass.schoolOptions;
			testClass.changeSchool();
			List<SelectOption> campusOptions = testClass.campusOptions;
			List<SelectOption> instNumberOptions = testClass.instNumberOptions;
			List<SelectOption> schoolPaymentOptions = testClass.schoolPaymentOptions;
			
			testClass.Beginning();
			testClass.Next();
			testClass.Previous();
			testClass.End();
			testClass.getDisablePrevious();
			testClass.getDisableNext();
			testClass.getTotal_size();
			testClass.getPageNumber();
			testClass.getTotalPages();
			
			
			
       	}
        
    }
}