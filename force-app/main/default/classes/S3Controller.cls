public class S3Controller {
	
    public String AWSCredentialName = IPFunctions.awsCredentialName;
    public String IPAWSCredentialName = IPFunctions.IPawsCredentialName;
    public String bucketNameToCreate {get;set;}
    public String bucketNameToDelete {get;set;} 
    public String bucketSelected {get;set;}
    public String bucketToList {get;set;}
    public String bucketToUploadObject {get;set;}
    public S3.ListEntry[] bucketList {get;set;}    
    public String bucketNameForCreate {get;set;}
    public String bucketNameForDelete {get;set;}
    public S3.ListAllMyBucketsEntry[] allBucketList {get;set;}
    public String accessTypeSelected {get;set;}
    public String bucketNameToModifyAccess {get;set;}
    public String OwnerId {get;set;}
    public Boolean renderListBucketResults {get;set;}
    public String listBucketErrorMsg {get;set;}
    public String createBucketErrorMsg {get;set;}
    public String deleteBucketErrorMsg {get;set;}
    public String deleteObjectErrorMsg {get;set;}
    public String uploadObjectErrorMsg {get;set;}
    public String uploadObjectErrorMsg2 {get;set;}
    public String objectToDelete {get;set;}
    public String folderIdSelected {get;set;}
    public String bucketSelectToUploadForceDoc {get;set;}
    public String docToUploadName {get;set;}
    public String docToUploadId {get;set;}
    public Blob fileBlob {get;set;}
    public Integer fileSize {get;set;}
    public String fileName {get;set;}
    public S3.GetObjectResult resultSingleObject {get;set;}
    
    
	/* Constructor
	   Sets the AWS key and secret
	*/
	public S3Controller() {
		
		
    }
    
    /*
       This method is called when the AWS_S3_Examples Visualforce page is loaded. It verifies that the AWS Keys can be found
       in the AWSKeys__c custom object by the specified name, as set in the string variable AWSCredentialsName. 
       
       Any errors are added to the ApexPage and displayed in the Visualforce page. 
    */
    
    public void IpCredentials(){
    	try{
			
			AWSKeys credentials = new AWSKeys(IPAWSCredentialName);
			as3 = new S3.AmazonS3(credentials.key,credentials.secret);
			S3Key= credentials.key;
			S3Secret = credentials.secret;
			renderListBucketResults = false;
			listBucketErrorMsg =null;
			createBucketErrorMsg=null;
			deleteBucketErrorMsg=null;
		
		}catch(AWSKeys.AWSKeysException AWSEx){
		     System.debug('Caught exception in AWS_S3_ExampleController: ' + AWSEx);
		     ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, AWSEx.getMessage());
			 ApexPages.addMessage(errorMsg);			    
		}
    }
    
    public PageReference constructor(){
    	try{
			
			AWSKeys credentials = new AWSKeys(AWSCredentialName);
			as3 = new S3.AmazonS3(credentials.key,credentials.secret);
			S3Key= credentials.key;
			S3Secret = credentials.secret;
			renderListBucketResults = false;
			listBucketErrorMsg =null;
			createBucketErrorMsg=null;
			deleteBucketErrorMsg=null;
		
		}catch(AWSKeys.AWSKeysException AWSEx){
		     System.debug('Caught exception in AWS_S3_ExampleController: ' + AWSEx);
		     ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, AWSEx.getMessage());
			 ApexPages.addMessage(errorMsg);    
		}
	   return null;	
    }
    
    public String S3Key {get;set;}
    public String S3Secret {get;set;}
	public S3.AmazonS3 as3 { get; private set; } //This object represents an instance of the Amazon S3 toolkit and makes all the Web Service calls to AWS. 
	
	
	public S3.ListBucketResult listbucket {get;set;}
	public String selectedBucket {get;set;}
	
	
	//Method to return a string array for all the buckets in your AWS S3 account
	public String[] allBuckets { get {
		try{
	    
		Datetime now = Datetime.now();
		
		//This performs the Web Service call to Amazon S3 and retrieves all the Buckets in your AWS Account. 
		System.debug('Key===>' + as3.key);
		
		S3.ListAllMyBucketsResult allBuckets = as3.ListAllMyBuckets(as3.key,now,as3.signature('ListAllMyBuckets',now));
		
		//Store the Canonical User Id for your account
		OwnerId = allBuckets.Owner.Id;
		System.debug('Owner ID===>' + OwnerID);
		S3.ListAllMyBucketsList bucketList = allBuckets.Buckets;
		S3.ListAllMyBucketsEntry[] buckets = bucketList.Bucket;
		allBucketList = buckets;
		
		String[] bucketNames = new String[]{};
		
		
		//Loop through each bucket entry to get the bucket name and store in string array. 
		for(S3.ListAllMyBucketsEntry bucket: buckets){
			 System.debug('Found bucket with name: ' + bucket.Name);
			 
			 bucketNames.add(bucket.name);
			
		}
		
    	return bucketNames;
    	
		}catch (System.NullPointerException e) {
		   return null;
		}catch(Exception ex){
		   //System.debug(ex);
		   System.debug('caught exception in listallmybuckets===>' + ex);
		   //ApexPages.addMessages(ex);
		   return null;	
		}
        
		}//end getter
		set;
	 }
	 
	 
	 //This is used by the sample Visualforce page to display a list of all folders in the org
	 public List<SelectOption> getFolders(){
	    List<SelectOption> options = new List<SelectOption>();
	    try{
		    Folder[] folders = [select id, name from Folder where Type='Document'];
	   	 	for(Folder f: folders){
	   	 	   System.debug('folder id['+f.id+'] name['+ f.name +']');
	   	 	   options.add(new SelectOption(f.id,f.name));	
	   	 	}
	   	 	return options;
	    }catch(Exception ex){
	       return null;	
	    }
	 }
	 
	 public List<Document> docs {get {
	   
	     if(folderIdSelected!=null){
		     List<Document> results = [select id, name from Document where FolderId = :folderIdSelected];
		     return results;	
	 	 }else
	 	   return null;
	 	
	 	}
	 }
	 
	 
	 //This is used by the sample Visualforce page to display a list of access control options to the user
	 public List<SelectOption> getAccessTypes(){
	    List<SelectOption> options = new List<SelectOption>();
   	 	options.add(new SelectOption('private','private'));
   	 	options.add(new SelectOption('public-read','public-read'));	
   	 	options.add(new SelectOption('public-write','public-write'));
   	 	//options.add(new SelectOption('authenticated-read','authenticated-read'));
   	 	return options;
	 }
	 
	 
	 //This is used by the sample Visualforce page to display the list of all buckets created in your AWS account. 
	 public List<SelectOption> getBucketNames() {
   	 	List<SelectOption> options = new List<SelectOption>();
   	 	
   	 	String[] bckts = allBuckets;
   	 	if(bckts!=null){
	   	 	for(String bucket : allBuckets){
	   	 	   options.add(new SelectOption(bucket,bucket));	
	   	 	}
	   	 	return options;
   	 	}
   	 	else
   	 	  return null;
     }
	 
	
	/*
	   Method to create a bucket on AWS S3 
	
	*/
	public PageReference createBucket(String bucketName){
	  try{	 
		   bucketName = IPFunctions.isOrgBucket() ? bucketName.toLowerCase() : bucketName;

	  	   createBucketErrorMsg= null;
		   Datetime now = Datetime.now();        
		   System.debug('about to create S3 bucket called: ' + bucketName);
		   System.debug('about to create S3 bucket called: ' + as3.key);
		  
		   		   
		   //This performs the Web Service call to Amazon S3 and create a new bucket.
	       S3.CreateBucketResult createBucketReslt = as3.CreateBucket(bucketName,null,as3.key,now,as3.signature('CreateBucket',now));
	       System.debug('Successfully created a Bucket with Name: ' + createBucketReslt.BucketName);
	       createBucketErrorMsg='Success';
	       return null;
       }
       catch(System.CalloutException callout){
		  System.debug('CALLOUT EXCEPTION: ' + callout);
		  //ApexPages.addMessages(callout);
		  createBucketErrorMsg = callout.getMessage();
		  return null;	
	   }
       catch(Exception ex){
		   System.debug(ex);
		   //ApexPages.addMessages(ex);
		   createBucketErrorMsg = ex.getMessage();
		   return null;	
	   }
       
	}
	
	
	/*
	   Method to delete a bucket on AWS S3 
	
	*/
	public PageReference deleteBucket(){
	  try{	 
	  	   deleteBucketErrorMsg= null;
		   Datetime now = Datetime.now();        
		   System.debug('about to delete S3 bucket called: ' + bucketNameToDelete);

		   bucketNameToDelete = IPFunctions.isOrgBucket() ? bucketNameToDelete.toLowerCase() : bucketNameToDelete;		   
		   
		   //This performs the Web Service call to Amazon S3 and create a new bucket.
	       S3.Status deleteBucketReslt = as3.DeleteBucket(bucketNameToDelete,as3.key,now,as3.signature('DeleteBucket',now), as3.secret);
	       deleteBucketErrorMsg = deleteBucketReslt.Description;
	       		
	       System.debug('Successfully deleted a Bucket: ' + deleteBucketReslt.Description);
	       return null;
       }
       catch(System.CalloutException callout){
		  System.debug('CALLOUT EXCEPTION: ' + callout);
		  //ApexPages.addMessages(callout);
		  deleteBucketErrorMsg = callout.getMessage();
		  return null;	
	   }
       catch(Exception ex){
		   System.debug(ex);
		   //ApexPages.addMessages(ex);
		   deleteBucketErrorMsg = ex.getMessage();
		   return null;	
	   }
       
	}
	
	
	/*
	   Method to create an object on AWS S3 
	
	*/
	public PageReference deleteObject(){
		try{
		   deleteObjectErrorMsg= null;
		   Datetime now = Datetime.now();   

		   bucketToList = IPFunctions.isOrgBucket() ? bucketToList.toLowerCase() : bucketToList;	     
		   
		   objectToDelete = ApexPages.currentPage().getParameters().get('keyToDelete');
		   System.debug('about to delete S3 object with key: ' + objectToDelete);
		   //This performs the Web Service call to Amazon S3 and create a new bucket.
	       S3.Status deleteObjectReslt= as3.DeleteObject(bucketToList,objectToDelete,as3.key,now,as3.signature('DeleteObject',now), as3.secret);
	       deleteObjectErrorMsg = 'Success';
	       		
	       System.debug('Successfully deleted a Bucket: ' + deleteObjectReslt.Description);
	       return null;
       } catch(System.CalloutException callout){
		  System.debug('CALLOUT EXCEPTION: ' + callout);
		  //ApexPages.addMessages(callout);
		  deleteObjectErrorMsg = callout.getMessage();
		  return null;	
	   }
       catch(Exception ex){
		   System.debug(ex);
		   ////ApexPages.addMessages(ex);
		   deleteObjectErrorMsg = ex.getMessage();
		   return null;	
	   }
	 	
	}
	
	
	
	
	public PageReference deleteObject(String bucketName, String path){
		try{
		   deleteObjectErrorMsg= null;
		   Datetime now = Datetime.now();    

		   bucketName = IPFunctions.isOrgBucket() ? bucketName.toLowerCase() : bucketName;    
		   
		   objectToDelete = path;
		   System.debug('about to delete S3 object with key: ' + objectToDelete);
		   //This performs the Web Service call to Amazon S3 and create a new bucket.
	       S3.Status deleteObjectReslt= as3.DeleteObject(bucketName,objectToDelete,as3.key,now,as3.signature('DeleteObject',now), as3.secret);
	       deleteObjectErrorMsg = 'Success';
	       		
	       System.debug('Successfully deleted an object: ' + deleteObjectReslt.Description);
	       return null;
       } catch(System.CalloutException callout){
		  System.debug('CALLOUT EXCEPTION: ' + callout);
		  //ApexPages.addMessages(callout);
		  deleteObjectErrorMsg = callout.getMessage();
		  return null;	
	   }
       catch(Exception ex){
		   System.debug(ex);
		   //ApexPages.addMessages(ex);
		   deleteObjectErrorMsg = ex.getMessage();
		   return null;	
	   }
	 	
	}
	
	
	/*
	   Method to list a bucket on AWS S3 
	
	*/
	public PageReference listBucket(String bucketName, String Prefix, Integer maxNumberToList, String Marker, String Delimiter){
		try{
			listBucketErrorMsg=  null;
		    Datetime now = Datetime.now();
		
			System.debug('Going to execute S3 ListBucket service for bucket: ' + bucketName);
			system.debug('@ as3: ' + as3);

			bucketName = IPFunctions.isOrgBucket() ? bucketName.toLowerCase() : bucketName;
			
			//This performs the Web Service call to Amazon S3 and retrieves all the objects in the specified bucket
			S3.ListBucketResult objectsForBucket = as3.ListBucket(bucketName, Prefix, Marker,maxNumberToList, Delimiter,as3.key,now,as3.signature('ListBucket',now),as3.secret);
	        bucketList = objectsForBucket.Contents;
	    
		    return null;
		}catch(System.CalloutException callout){
		  System.debug('CALLOUT EXCEPTION: ' + callout);
		  //ApexPages.addMessages(callout);
		  listBucketErrorMsg = 	callout.getMessage();
		  return null; 	
	    }catch(Exception ex){
		    System.debug('EXCEPTION: ' + ex);
		    listBucketErrorMsg = 	ex.getMessage();
		    //ApexPages.addMessages(ex);
		    return null;	
		}
	}
	
		
	public PageReference updateBucketToUpload(){
	    policy = getPolicy();
	    System.debug('tempPolicy: ' + tempPolicy);
	    return null;	
	}
	
	
	public PageReference updateAccessTypeSelected(){
	   //policy=getPolicy();
	   //System.debug('tempPolicy: ' + tempPolicy);
	   return null;	
	}
	
	public PageReference updateFolderId(){
		return null;
	}	
	
			
	/*
	    This method uploads a file from the filesystem and puts it in S3. 
	    It also supports setting the Access Control policy. 
	*/
	public pageReference syncFilesystemDoc(){
		try{  
		  Datetime now = Datetime.now();
		  
		  String docBody = EncodingUtil.base64Encode(fileBlob);

		  bucketToUploadObject = IPFunctions.isOrgBucket() ? bucketToUploadObject.toLowerCase() : bucketToUploadObject;
		  
		  //TODO - make sure doc.bodyLength is not greater than 100000 to avoid apex limits
		  System.debug('body length: ' + fileSize);
		  uploadObjectErrorMsg = 'Error';
		  Boolean putObjResult = as3.PutObjectInline_ACL(bucketToUploadObject,fileName,null,docBody,fileSize,accessTypeSelected,as3.key,now,as3.signature('PutObjectInline',now),as3.secret, OwnerId, null);
		  if(putObjResult==true){
		  	  System.debug('putobjectinline successful');
		  	  uploadObjectErrorMsg = 'Success';
		  }
		  
	  
	  	}catch(System.CalloutException callout){
		  System.debug('CALLOUT EXCEPTION: ' + callout);
		  uploadObjectErrorMsg = 	callout.getMessage();
 	
	    }catch(Exception ex){
		    System.debug('EXCEPTION: ' + ex);
		    uploadObjectErrorMsg = 	ex.getMessage();
		}
	   
	  return null;	
	}
	
	
	/*
		Customized method to insert a file in S3
	*/
	public pageReference insertFileS3(Blob emailBody, String bucketName, String fileName, String contentType){
		try{  
		  Datetime now = Datetime.now();
		  accessTypeSelected='public-read';
		  
		  String docBody = EncodingUtil.base64Encode(emailBody);
		  fileSize=docBody.length();

		  bucketName = IPFunctions.isOrgBucket() ? bucketName.toLowerCase() : bucketName;
	
		  System.debug('bucketName: ' + bucketName);
		  System.debug('fileName: ' + fileName);
		  System.debug('fileSize: ' + fileSize);

		  uploadObjectErrorMsg = 'Error';
		  
		  Boolean putObjResult = as3.PutObjectInline_ACL(bucketName,fileName,null,docBody,fileSize,accessTypeSelected,as3.key,now,as3.signature('PutObjectInline',now),as3.secret, OwnerId, contentType);
		  
		  if(putObjResult==true){
		  	  System.debug('putobjectinline successful');
		  	  uploadObjectErrorMsg = 'Success';
		  }
		  	  
	  	}catch(System.CalloutException callout){
		  System.debug('CALLOUT EXCEPTION: ' + callout);
		  uploadObjectErrorMsg = 	callout.getMessage();
 	
	    }catch(Exception ex){
		    System.debug('EXCEPTION: ' + ex);
		    uploadObjectErrorMsg = 	ex.getMessage();
		}
	   
	  return null;	
	}
	
	/*public pageReference insertMassFileS3(String bucketName, list<Email__c> emails){
		try{  
		  Datetime now = Datetime.now();
		  accessTypeSelected='public-read';
		  String emailBody;
		  String fileName;
		  String subject;
		  String hasAtt;
		  List<Account> email;
		  
		  
		  	for(Email__c e : emails){
		  		subject=e.Subject__c;
		  		
		  		if(e.Subject__c==null && e.Subject__c==''){
					subject='(no subject)';
				}else{
					subject=subject.replace('/', '-');
					subject=subject.replace(':-:', '-');
					subject=subject.replace('|', '-');
					subject=subject.replace('<', '');
					subject=subject.replace('>', '');
				}
				
				if(e.hasAttachment__c){
					hasAtt='AttY';
				}else{
					hasAtt='AttN';
				}
				
				system.debug('Created Date ===>' + e.CreatedDate);
				String myDatetimeStr = e.CreatedDate.format('dd-MM-yyyy HH:mm:ss');
				system.debug('After Format DateTime ===>' +myDatetimeStr);
			
				
		  			
		 		//Verify who sent the emails
		 		if(e.From__c==e.Client__r.PersonEmail){
		 			email = new List<Account>([SELECT a.Id FROM Account A WHERE PersonEmail=:E.To__c and a.RecordType.name='Employee']);
		 			if(email !=null && email.size()>0){
			 			fileName=e.Client__c+'/Emails/'+'R:-:'+email[0].id+':-:'+subject+':-:'+hasAtt+':-:'+myDatetimeStr;
		 			}else{
		 				fileName=e.Client__c+'/Emails/'+'R:-:Employee not found:-:'+subject+':-:'+hasAtt+':-:'+myDatetimeStr;
		 			}
		 		}
		 		if(e.From__c!=e.Client__r.PersonEmail){
		 			email = new List<Account>([SELECT a.Id FROM Account A WHERE PersonEmail=:E.From__c and a.RecordType.name='Employee']);
		 			if(email !=null && email.size()>0){
			 			fileName=e.Client__c+'/Emails/'+'S:-:'+email[0].id+':-:'+subject+':-:'+hasAtt+':-:'+myDatetimeStr;
		 			}else{
		 				fileName=e.Client__c+'/Emails/'+'S:-:Employee not found:-:'+subject+':-:'+hasAtt+':-:'+myDatetimeStr;
		 			}
		 		}
		 		
		 		//In this case the Email Body was described in the Attachment Object
		 		List<Attachment> att = new List<Attachment>([SELECT a.Body FROM Attachment a WHERE ParentID=:e.Id]);
		 		
		 		if(att!=null && att.size()>0){
		 			emailBody=EncodingUtil.base64Encode(att[0].Body);
		 		}else{
		 			emailBody=EncodingUtil.base64Encode(Blob.valueOf(e.Body__c));
		 		}
		 		
		 	  fileSize=emailBody.length();
			  uploadObjectErrorMsg = 'Error';
			  
			  Boolean putObjResult = as3.PutObjectInline_ACL(bucketName,fileName,null,emailBody,fileSize,accessTypeSelected,as3.key,now,as3.signature('PutObjectInline',now),as3.secret, OwnerId);
			  
			  	  if(putObjResult==true){
			  	  System.debug('putobjectinline successful');
			  	  uploadObjectErrorMsg = 'Success';
			  }  
		 	}
	  	}catch(System.CalloutException callout){
		  System.debug('CALLOUT EXCEPTION: ' + callout);
		  uploadObjectErrorMsg = 	callout.getMessage();
 	
	    }catch(Exception ex){
		    System.debug('EXCEPTION: ' + ex);
		    uploadObjectErrorMsg = 	ex.getMessage();
		}
	   
	  return null;	
	}
	*/
	
   public pageReference getObject(String Key, String bucket){
		//	String Bucket,String Key,Boolean GetMetadata,Boolean GetData,Boolean InlineData,String AWSAccessKeyId,DateTime Timestamp,String Signature,String Credential
		try{  
		  Datetime now = Datetime.now();
		  uploadObjectErrorMsg = 'Error';
		  System.debug('Going to get the Object on the bucket: ' + bucket);
		  System.debug('Name of the file: ' + key);

		  bucket = IPFunctions.isOrgBucket() ? bucket.toLowerCase() : bucket;

		  resultSingleObject = as3.GetObject(bucket,Key,false,true,true,as3.key,now,as3.signature('GetObject',now), OwnerId);
		  System.debug('Retorno SOAP:::'+resultSingleObject);	  
	  	}catch(System.CalloutException callout){
		  System.debug('CALLOUT EXCEPTION: ' + callout);
		  uploadObjectErrorMsg = 	callout.getMessage();
 	
	    }catch(Exception ex){
		    System.debug('EXCEPTION: ' + ex);
		    uploadObjectErrorMsg = 	ex.getMessage();
		}
	  return null;	
	}
	
	
	/*
	    This method uploads a file from the Document object in Force.com and puts it in S3. 
	    It also supports setting the Access Control policy. 
	*/
	public pageReference syncForceDoc(){
		try{  
		  Datetime now = Datetime.now();
		  
		  docToUploadId = ApexPages.currentPage().getParameters().get('docIdToUpload');
		  Document doc= [select id, body, bodyLength , name from Document where id = :docToUploadId];
		  String docBody = EncodingUtil.base64Encode(doc.body);
		  
		  //TODO - make sure doc.bodyLength is not greater than 100000 to avoid apex limits 
		  System.debug('body length: ' + doc.bodyLength);
		  
		  /*
		   
		  NOTE: This is purposely commented out. But it shows a valid example of how to construct metadata entries
		  
		  S3.MetadataEntry[] metadataentries = new S3.MetadataEntry[]{};
		  S3.MetadataEntry entry = new S3.MetadataEntry();
		  entry.Name='sampleName';
		  entry.Value='sampleValue';
		  metadataentries.add(entry);
		  */
		  
		  uploadObjectErrorMsg2 = 'Error';
		  Boolean putObjResult = as3.PutObjectInline_ACL(bucketToUploadObject,doc.name,null,docBody,doc.bodyLength,accessTypeSelected,as3.key,now,as3.signature('PutObjectInline',now),as3.secret, OwnerId,null);
		  if(putObjResult==true){
		  	  System.debug('putobjectinline successful');
		  	  uploadObjectErrorMsg2 = 'Success';
		  }
		  
	  
	  	}catch(System.CalloutException callout){
		  System.debug('CALLOUT EXCEPTION: ' + callout);
		  uploadObjectErrorMsg2 = 	callout.getMessage();

	    }catch(Exception ex){
		    System.debug('EXCEPTION: ' + ex);
		    uploadObjectErrorMsg2 = 	ex.getMessage();
		    
		}
	   
	  return null;	
	}
	
	
	public pageReference redirectToS3Key() {
        
        //get the filename in urlencoded format
        String filename = EncodingUtil.urlEncode(ApexPages.currentPage().getParameters().get('filename'), 'UTF-8');
        //String bucket = EncodingUtil.urlEncode(ApexPages.currentPage().getParameters().get('bucket'), 'UTF-8');
        System.debug('redirectToS3Key filename: ' + filename);
        Datetime now = DateTime.now();
        Datetime expireson = now.AddSeconds(120);
        Long Lexpires = expireson.getTime()/1000;
        
        System.debug('key: ' + as3.key);
        System.debug('secret: ' + as3.secret);
        //String codedFilename=  EncodingUtil.urlEncode(filename,'UTF-8');
       // System.debug('codedFilename: '+codedFilename); 
        String stringtosign = 'GET\n\n\n'+Lexpires+'\n/'+bucketToList+'/'+filename;
        System.debug('redirectToS3Key stringstosign: ' + stringtosign);
        String signed = make_sig(stringtosign);
        System.debug('signed: ' + signed);
        String codedsigned = EncodingUtil.urlEncode(signed,'UTF-8');
        System.debug('codedsigned: ' + codedsigned);
        String url = 'http://'+bucketToList+'.s3.amazonaws.com/'+filename+'?AWSAccessKeyId='+as3.key+'&Expires='+Lexpires+'&Signature='+signed;
        System.debug('url: ' + url);
        PageReference newPage = new PageReference(url);
        System.debug('newPage url: ' + newPage.getUrl());
        return newPage;
        
    }
	
	
	/*
	   --------------------------------------------------------------------------------
	   This section of Apex Code below are for the HTML FORM POST to upload a large file. 
	
	*/
	
	public String policy = '{ "expiration": "2008-12-12T12:00:00.000Z","conditions": [  {"acl": "'+accessTypeSelected+'" },{"bucket": "'+bucketToUploadObject+'" }, ["starts-with", "$key", ""] ]}';
	//public String policy = '';
	
	public string tempPolicy { get ; set; }
	
	public String getPolicy() {
		String temp= '{ "expiration": "2008-12-12T12:00:00.000Z","conditions": [ ';
		temp+='{"bucket": "' + bucketToUploadObject + '" },';
		temp+=' ["starts-with", "$key", ""],';
		temp +='  {"acl": "'+accessTypeSelected+'" }';
		temp+=']}';
		tempPolicy = temp;
		policy=temp;
		System.debug('inside getPolicy...:' + policy);
        return EncodingUtil.base64Encode(Blob.valueOf(policy));
    }
    
    
    public String getSignedPolicy() {    
        return make_sig(EncodingUtil.base64Encode(Blob.valueOf(policy)));        
    }
    
    //just to run some test with amazon's signature's tester
    public String getHexPolicy() {
        String p = getPolicy();
        return EncodingUtil.convertToHex(Blob.valueOf(p));
    }
    

    //method that will sign
    public String make_sig(string canonicalBuffer) {        
        String macUrl ;
        String signingKey = EncodingUtil.base64Encode(Blob.valueOf(as3.secret));
        Blob mac = Crypto.generateMac('HMacSHA1', blob.valueof(canonicalBuffer),blob.valueof(as3.secret)); 
        macUrl = EncodingUtil.base64Encode(mac);                
        return macUrl;
    }
    
    
     public String createTestCredentials(){
       
        AWSKey__c testKey = new AWSKey__c(name='test keys',key__c='key',secret__c='secret');
        insert testKey;
        return testKey.name;
         
     } 
     
 
 
}