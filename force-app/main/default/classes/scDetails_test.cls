/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class scDetails_test {

    static testMethod void myUnitTest() {
       	
       	TestFactory tf = new TestFactory();
       	Account school = tf.createSchool();
		Account schoolGroup = tf.createSchool();
       	
       	ApexPages.currentPage().getParameters().put('id', schoolGroup.id);
       	ApexPages.currentPage().getParameters().put('scgp', 'true');
       	scDetails scg = new scDetails();
		scg.addingScGroup = true;
       	scg.save();

       	ApexPages.currentPage().getParameters().remove('scgp');
       	ApexPages.currentPage().getParameters().put('id', school.id);
       	scDetails sc = new scDetails();
       	sc.save();
       	sc.edit();
		sc.addingScGroup = true;
       	sc.cancel();
       	
       	ApexPages.currentPage().getParameters().remove('id');
       	ApexPages.currentPage().getParameters().put('ac', 'new');
       	sc = new scDetails();
       	sc.save();
       	
       	sc.getCurrencies();
       	
    }
}