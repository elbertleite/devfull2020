/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class xCourseSearch_test {

    static testMethod void myUnitTest() {
    	
    	TestFactory tf = new TestFactory();
       
		Account agency = tf.createAgency();
		
		Contact employee = tf.createEmployee(agency);
				
		
		
		/*//Create portal account owner
		UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
		Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
		User portalAccountOwner1 = new User(
			UserRoleId = portalRole.Id,
			ProfileId = profile1.Id,
			Username = System.now().millisecond() + 'test2@test.com',
		   	Alias = 'batman',
			Email='bruce.wayne@wayneenterprises.com',
			EmailEncodingKey='UTF-8',
			Firstname='Bruce',
			Lastname='Wayne',
			LanguageLocaleKey='en_US',
			LocaleSidKey='en_US',
			TimeZoneSidKey='America/Chicago'
		);
		Database.insert(portalAccountOwner1);
		
		//Create account
		Account portalAccount1 = tf.createPortalAgency(portalAccountOwner1.id);
		    	
		//Create contact
		Contact contact1 = new Contact(
		   	FirstName = 'Test',
		    	Lastname = 'McTesty',
			AccountId = portalAccount1.Id,
		    	Email = System.now().millisecond() + 'test@test.com'
		);
		Database.insert(contact1);
		    	
		//Create user
		Profile portalProfile = [SELECT Id FROM Profile WHERE Name = 'Customer Community Agency' Limit 1];
		User user1 = new User(
			Username = System.now().millisecond() + 'test12345@test.com',
			ContactId = contact1.Id,
			ProfileId = portalProfile.Id,
			Alias = 'test123',
			Email = 'test12345@test.com',
			EmailEncodingKey = 'UTF-8',
			LastName = 'McTesty',
			CommunityNickname = 'test12345',
			TimeZoneSidKey = 'America/Los_Angeles',
			LocaleSidKey = 'en_US',
			LanguageLocaleKey = 'en_US'
		);
		Database.insert(user1);*/
		
		Account school = tf.createSchool();
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;
		insert p3;
		
		
		Account campus = tf.createCampus(school, agency);
		    
		Course__c course = tf.createCourse();
		
		Course__c course2 = tf.createLanguageCourse();
				
		Campus_Course__c cc = tf.createCampusCourse(campus, course);
		
		Campus_Course__c cc2 = tf.createCampusCourse(campus, course2);
       
		Course_Price__c cp = tf.createCoursePrice(cc, 'Latin America');
		
		Course_Price__c cp2 = tf.createCoursePrice(cc2, 'Published Price');
		
		
		Course_Intake_Date__c cid = tf.createCourseIntakeDate(cc);
		
		Course_Intake_Date__c cid2 = tf.createCourseIntakeDate(cc2);
        
		Course_Extra_Fee__c cef = tf.createCourseExtraFee(cc, 'Latin America', p);
		
		Course_Extra_Fee_Dependent__c cefd = tf.createCourseRelatedExtraFee(cef, p2);
		
		tf.createCourseExtraFeeCombined(cc, 'Latin America', p, p2);
        
		Course_Extra_Fee__c cef2 = new Course_Extra_Fee__c();
		cef2.Campus__c = campus.Id;
		cef2.Nationality__c = 'Published Price';
		cef2.Availability__c = 3;
		cef2.From__c = 2;
		cef2.Value__c = 100;		
		cef2.Optional__c = false;
		cef2.Product__c = p.id;
		cef2.date_paid_from__c = system.today();
		cef2.date_paid_to__c = system.today().addDays(+31);
		insert cef2;
		
		
		
		
		
		
		Deal__c d = new Deal__c();
		d.Availability__c = 3;
		d.Campus_Account__c = campus.id;
		d.From__c = 1;
		d.Extra_Fee__c = cef2.id;
		d.Promotion_Type__c = 3;
		d.From_Date__c = system.today();
		d.To_Date__c = system.today().addDays(60);
		d.Extra_Fee_Type__c = 'Cash';
		d.Extra_Fee_Value__c = 60;
		d.Product__c = p.id;
		d.Nationality_Group__c = 'Publish Price';
		insert d;
		
		
		Deal__c d2 = new Deal__c();
		d2.Availability__c = 3;
		d2.Campus_Course__c = cc2.id;
		d2.From__c = 1;		
		d2.Promotion_Type__c = 2;
		d2.From_Date__c = system.today();
		d2.To_Date__c = system.today().addDays(60);
		d2.Promotion_Weeks__c = 1;
		d2.Number_of_Free_Weeks__c = 1;
		d2.Nationality_Group__c = 'Latin America';
		d2.Promotion_Name__c = '1+1';
		d2.Promo_Price__c = 50;		
		insert d2;
		
		Start_Date_Range__c sdr = new Start_Date_Range__c();
		sdr.Promotion__c = d2.id;
		sdr.Campus__c = cc2.campus__c;
		sdr.From_Date__c = system.today();
		sdr.To_Date__c = system.today().addDays(15);
		sdr.Number_of_Free_Weeks__c = 2;
		insert sdr;		
		
		
		
		Deal__c d3 = new Deal__c();
		d3.Availability__c = 3;
		d3.Campus_Course__c = cc.id;
		d3.From__c = 1;
		d3.Extra_Fee__c = cef.id;
		d3.Promotion_Type__c = 3;
		d3.From_Date__c = system.today();
		d3.To_Date__c = system.today().addDays(60);
		d3.Extra_Fee_Type__c = 'Cash';
		d3.Extra_Fee_Value__c = 50;
		d3.Nationality_Group__c = 'Latin America';
		insert d3;
		
		Deal__c d4 = new Deal__c();
		d4.Availability__c = 3;
		d4.Campus_Course__c = cc.id;
		d4.From__c = 1;		
		d4.Promotion_Type__c = 2;
		d4.Promotion_Name__c = '5+2';
		d4.Promo_Price__c = 50;		
		d4.From_Date__c = system.today();
		d4.To_Date__c = system.today().addDays(61);
		d4.Promotion_Weeks__c = 5;
		d4.Number_of_Free_Weeks__c = 2;
		d4.Nationality_Group__c = 'Published Price';
		insert d4;
		
		
		
		Deal__c d5 = new Deal__c();
		d5.Availability__c = 3;
		d5.Campus_Course__c = cc.id;
		d5.Extra_Fee_Dependent__c = cefd.id;
		d5.From__c = 1;		
		d5.Promotion_Type__c = 3;
		d5.From_Date__c = system.today();
		d5.To_Date__c = system.today().addDays(62);
		d3.Extra_Fee_Type__c = 'Cash';
		d3.Extra_Fee_Value__c = 50;
		d5.Nationality_Group__c = 'Latin America';
		d5.Product__c = p3.id;
		//insert d5;
		  
        Test.startTest();
        
        //system.runAs(user1){
        	        	
        	
        
	        xCourseSearch cs = new xCourseSearch();
	        cs.checkUserLoged();
	        
	        String n = cs.Nationalities;
	       
	        
	        //FILTERS
	        cs.getNationalityGroup();
	        cs.Nationalities = 'Brazil';        
	        
	        cs.getlistDestinations();
	        cs.destination = 'Australia';        
	        
	        cs.getCityDestination();
	        cs.destinationCity = 'Sydney';
	        cs.refreshSchools();
	        
	        
	        cs.getcourseCategories();
	        cs.selectedCourseCategory = 'Language';
	        cs.refreshCategory();
	        
	        cs.getlistcoursesArea();
	        cs.selectedCourseArea = 'English/ELICOS';
	        cs.refreshCoursesArea();
	        
	        
	        cs.getlistcoursesQualification();
	        cs.refreshCourseField();
	        
	        cs.getlistcoursesType();
	        cs.refreshCoursesType();
	        
	        cs.getlistcoursesSpecialization();
	        cs.refreshCoursesSpecialization();
	        cs.refreshSchools();
	        cs.getlistSchools();
	        
	        cs.getPeriods();
	        
	        cs.getListUnitsSearch();
	        cs.numberStudyUnitsSearch = 10;
	        
	        cs.hourFrom = 10;
	        cs.hourTo = 30;
	        
	        cs.getlocationOptions();
	        double location = cs.location;
	        cs.location = 3.0;
	        
	        cs.SearchDate = System.today();
	        
	        
	        cs.refreshSearch();
	        
	        
	        cs.getCampusCourses();
	        
	        
	        
	        
	        
	        
	        //save and compare
	        
	        cs.getCompareList();
	        
	        
	        ApexPages.currentPage().getParameters().put('campusCourse', cc.id);
	        ApexPages.currentPage().getParameters().put('campus', campus.id);
	        ApexPages.currentPage().getParameters().put('numUnits', '10');
	        
	        cs.addCourseToCart();
	        
	        String iddel;
	        for(Search_Courses__c sc : cs.getCompareList())
	        	iddel = sc.id;
	        
	        
	        ApexPages.currentPage().getParameters().put('courseId', iddel);
	        cs.deleteCourseFromCart();
	        
	        
	        cs.getCountries();
	        
	        cs.loginUser();
	        cs.getCart();
	        
	        cs.logUser(cs.objCart.id);        
	        
	        
	        cs.changeView();
	        cs.getUserType();
	        
	        
		
			
	        cs.newSearch();
	        
	       
	       	try {
	        	cs.addUser();
	       	} catch (Exception e){}
	       	
	       	
	       	
	       	cs.getSchoolType();
	       	cs.getsearchView();
	       	cs.getGraphOption();
	       	cs.getSearchOrder();
	       	
	       	
	       	
	       	cs.compareCourses();
	       	cs.changeSearch();
	       	
	       	
	       	
	       	cs.selectedView = 'chart';
	        List<xCourseSearch.courseGraph> graph = cs.CampusPricesGraph;
	       	cs.refreshSearchChart();
	        
	        cs.getMostVisitedCourses();
	        
	        
	        cs.selectedView = 'list';
	        cs.selectedSearchOrder = 'totpriceUp';
	        cs.selectedSchoolType = 'all';
	        cs.getCampusCourses2(school.id, campus.id, '10', '', '', 'Brazil', 'Australia','Sydney', 'Language', '', '');
	        
	        
	        cs.calculateEndDate(system.today(), 5, 'Month');
	        cs.calculateEndDate(system.today(), 4, 'Semester');
	        cs.calculateEndDate(system.today(), 3, 'Year');
	        cs.calculateEndDate(system.today(), 2, 'Subjects');
	        cs.calculateEndDate(system.today(), 1, 'Quarter');
	        
	        
	        xCourseSearch.courseDetails cd = new xCourseSearch.courseDetails();
	        List<SelectOption> extraFeesUnitRange = cd.extraFeesUnitRange;
	        
	        cs.getlistMaincoursesType();
	        Integer maxCourseLength = cs.maxCourseLenght;
	        
	        
	        cs.selectedSchoolType = 'selectedschools';
	        cs.hasSelectedSchools = true;
	        cs.searchCourse();
        //}
        
        Test.stopTest();
        
    }
    
    
    
    
    static testMethod void testTemplates() {
        
        TestFactory tf = new TestFactory();
			
		
		Account agency = tf.createAgency();
		
		Contact employee = tf.createEmployee(agency);
		
		//User u = tf.createUserEmployee(agency, employee);
		
		Instalment_Plan__c ip = new Instalment_Plan__c();
		ip.Agency__c = agency.id;
		ip.Deposit__c = 30.0;
		ip.Description__c = 'A test';
		ip.Group_Instalments__c = false;
		ip.Instalments_Interest__c = '1:0.68;2:1.13;3:1.36;4:1.49;5:1.59;6:1.65;7:1.70;8:1.74;9:1.77;10:1.79;11:1.81;12:1.83';
		ip.Interest__c = 0;
		ip.Interest_Type__c = 'Credit Card Interest';
		ip.isSelected__c = true;
		ip.Name__c = 'Finance 12x';
		ip.Number_of_Instalments__c = 12;
		insert ip;
		
		Instalment_Plan__c simplePlan = new Instalment_Plan__c();
		simplePlan.Agency__c = agency.id;
		simplePlan.Deposit__c = 30.0;
		simplePlan.Description__c = 'A test';
		simplePlan.Group_Instalments__c = true;		
		simplePlan.Interest__c = 0;
		simplePlan.Interest_Type__c = 'Simple';
		simplePlan.isSelected__c = true;
		simplePlan.Name__c = 'Finance 12x';
		simplePlan.Number_of_Instalments__c = 12;
		insert simplePlan;
		
		Instalment_Plan__c ccPlan = new Instalment_Plan__c();
		ccPlan.Agency__c = agency.id;
		ccPlan.Deposit__c = 30.0;
		ccPlan.Description__c = 'A test';
		ccPlan.Group_Instalments__c = true;		
		ccPlan.Interest__c = 2;
		ccPlan.Interest_Type__c = 'Credit Card Interest';
		ccPlan.isSelected__c = true;
		ccPlan.Name__c = 'Finance 12x';
		ccPlan.Number_of_Instalments__c = 12;
		insert ccPlan;
		
        
		Account school = tf.createSchool();
		
		Account campus = tf.createCampus(school, agency);
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Cozy Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;
		insert p3;
		
		
		Course__c course = tf.createCourse();
		
		Campus_Course__c cc = tf.createCampusCourse(campus, course);
		
		Course_Price__c cp = tf.createCoursePrice(cc, 'Latin America');
		
		Course_Intake_Date__c cid = tf.createCourseIntakeDate(cc);
		
		Course_Extra_Fee__c cef = tf.createCourseExtraFee(cc, 'Latin America', p);
		
		Course_Extra_Fee_Dependent__c cefd = tf.createCourseRelatedExtraFee(cef, p2);
		
		Deal__c extraFeeDeal = tf.createCourseExtraFeePromotion(cc, 'Latin America', p);
		
		Deal__c freeUnits = tf.createCourseFreeUnitsPromotion(cc, 'Latin America');
		
		Web_Search__c ws = tf.createWebSearch(3, cc, 'Brazil', false);
		
		Search_Courses__c sc = tf.createSearchCourse(ws, cc);
		
		Search_Courses__c custom = tf.createCustomSearchCourse(ws);
		
		Search_Course_Product__c product = tf.createSearchCourseProduct(ws, sc);
						
		Web_Search__c wsTemplate = new Web_Search__c();
		wsTemplate.WS_Template__c = true;
		insert wsTemplate;
        
        Test.startTest();
        
        Search_Course_Template__c tp = new Search_Course_Template__c();
		tp.Courses__c = sc.id;
		insert tp;
		
		Web_Search__c wsCombined = new Web_Search__c();
		wsCombined.Combine_Quotation__c = true;		
		insert wsCombined;
		
		Search_Courses__c sc1 = new Search_Courses__c();
		sc1.Campus_Course__c = cc.id;
		sc1.Web_Search__c = wsCombined.id;
		sc1.Number_of_Units__c = 10;
		sc1.Custom_Fees__c = 'fee1:#160.0:#add:#extraFee:&fee2:#100.0:#subtract:#tuition';
		insert sc1;
		
		Search_Courses__c sc2 = new Search_Courses__c();
		sc2.Campus_Course__c = cc.id;
		sc2.Web_Search__c = wsCombined.id;
		sc2.Number_of_Units__c = 10;
		sc2.Custom_Fees__c = 'fee1:#160.0:#add:#extraFee:&fee2:#100.0:#subtract:#tuition';
		insert sc2;
		
		Search_Courses__c sc3 = new Search_Courses__c();
        sc3.UseAsCustomTemplate__c = true;
        sc3.isCustomCourse__c = true;
        sc3.Web_Search__c = wsCombined.id;
        sc3.Custom_Currency__c = 'AUD';
        sc3.Custom_Fees__c = 'fee1:#160.0:#add:#extraFee:&fee2:#100.0:#subtract:#tuition';
        insert sc3;
		
		
        
        Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(ws);
        xCourseSearch cs = new xCourseSearch(controller);
        cs.logUser(ws.id);
        
        ApexPages.currentPage().getParameters().put('templateId', tp.id);
        cs.openTemplate();
        
		
		xCourseSearch courseSearch = new xCourseSearch();
		courseSearch.logUser(wsCombined.id);
		courseSearch.getCompareList();
		String combinedQuoteCountry = courseSearch.combinedQuoteCountry;
		String combinedQuoteCurrency = courseSearch.combinedQuoteCurrency;
		
        
        Test.stopTest();
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}