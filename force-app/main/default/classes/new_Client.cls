public class new_Client{
	
	//private Account agency = new Account( Id = '0019000000n3JLF' );
	
	public Contact newContact {
		get {
			if(newContact == null){
				newContact = new Contact();
				//newContact.AccountID = agency.id;
			}
			return newContact;
		}
		Set;
	}
	
	public Address__c newAddress {
		get {
			if(newAddress == null)
				newAddress = new Address__c();
			return newAddress;
		}
		Set;
	}

	public new_Client(ApexPages.StandardController controller){
	}
	
	public PageReference saveContact(){
		
		User u = [Select accountid from User where Id = :UserInfo.getuserid()];
		
		newContact.AccountId = u.AccountId;
		
		INSERT newContact;
		
		if (newAddress.Street__c != null){
			newAddress.Contact__c = newContact.id;
			INSERT newAddress;		
		}	
	
		return new PageReference('/clientDetails?Id=' + newContact.id);
		
	}
	
	private global_class gc = new global_class();
	public List<SelectOption> getLanguages() {
		List<SelectOption> options = new List<SelectOption>();
		options.addAll(gc.getLanguages());
		return options;
	}

	public List<SelectOption> getDestinationsCountry() {
		Set<string> destinations = new Set<String>();
		List<SelectOption> options = new List<SelectOption>();
		for(SelectOption op:ipFunctionsGlobal.CountryDestinations()){			
			options.add(new SelectOption(op.getValue(),op.getLabel()));
			destinations.add(op.getValue());		
		}
		return options;
	}
}