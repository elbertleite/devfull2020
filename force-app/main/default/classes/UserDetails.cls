public with sharing class UserDetails {


	public static String getMyAgencyGroupID(){
		String groupID;
		try {
			groupID = [select Contact.Account.AgencyGroup__c from User where id =:UserInfo.getUserId() limit 1].Contact.Account.AgencyGroup__c;
		} catch (Exception e){
			groupID = '';
		}

		return groupID;

	}

	public static Contact getMyContactDetails(){
		string contactId = [Select ContactId from user where id = :UserInfo.getUserId() limit 1].ContactId;
	    return [select id, Name, Email, Signature__c, Export_Report_Permission__c, Account.Hify_Agency__c, AccountId, Account.Name, Account.ParentId, Account.Parent.Destination_Group__c, Account.Parent.Name, Account.BillingCountry, Account.Parent.Global_Link__c,	 Global_Manager__c, Preferable_Language__c, Management_Report_Permission__c, Marketing_Report_Permission__c, Students_Coupons_Report__c, Sales_Dashboard_Permission__c
    			from Contact where id = :contactId];
	}


	public without sharing class GroupSettings {

		public list<SelectOption> getLeadStatus(String agencyGroupID, String country){
			list<SelectOption> options = new list<SelectOption>();
			options.add(new SelectOption( 'all', '--All--'));

			for(AggregateResult ar : [Select Stage_description__c from Client_Stage__c where Destination__c = :country and Agency_Group__c = :agencyGroupID and Lead_Stage__c = true group by Stage_description__c order by Stage_description__c]){
				if(!string.valueOf(ar.get('Stage_description__c')).containsIgnoreCase('xxx'))
					options.add(new SelectOption((String)ar.get('Stage_description__c'),(String)ar.get('Stage_description__c')));
			}
			return options;
		}

		public list<SelectOption> getClientStatus(String agencyGroupID, String country){
			list<SelectOption> options = new list<SelectOption>();
			options.add(new SelectOption( 'all', '--All--'));

			for(AggregateResult ar : [Select Stage_description__c from Client_Stage__c where Destination__c = :country and Agency_Group__c = :agencyGroupID group by Stage_description__c order by Stage_description__c]){
				if(!string.valueOf(ar.get('Stage_description__c')).containsIgnoreCase('xxx'))
					options.add(new SelectOption((String)ar.get('Stage_description__c'),(String)ar.get('Stage_description__c')));
			}
			return options;
		}


		public list<SelectOption> getLeadSourceSpecifics(String agencyGroupId, String leadSource){
			list<SelectOption> options = new list<SelectOption>();
			options.add(new SelectOption( '', '--Select Option--'));

			for(Lead_Status_Source__c ar : [Select Subject__c From Lead_Status_Source__c where Agency_Group__c = :agencyGroupId and Lead_Source__c = :leadSource order by Subject__c]){
				options.add(new SelectOption(ar.Subject__c,ar.Subject__c));
			}
			return options;
		}


		public list<SelectOption> getTaskSubjects(String agencyGroupID){

			list<SelectOption> options = new list<SelectOption>();
			String ts;
			try {
				ts = [Select Task_Subjects__c from Account where id = :agencyGroupID limit 1].Task_Subjects__c;
			} catch (Exception e){
				system.debug('ERROR: No Task_Subjects__c found for agencygroup: ' + agencyGroupID);
			}


			if(ts != null){
				list<String> subjects = ts.split(';');
				for(string s: subjects)
					options.add(new SelectOption(s.trim(),s.trim()));
				options.sort();
			}

			return options;
		}

		public String getBackgroundColour(String agencyGroupID){
			String groupBgColor;
			try {
				groupBgColor = [Select bgcolor__c FROM ACCOUNT Where id= :agencyGroupID limit 1].bgcolor__c;
				if(groupBgColor==null || groupBgColor == '')
					groupBgColor = '#DC2628';
			} catch (Exception e){
				groupBgColor = '#DC2628';
			}

			return groupBgColor;
		}

		public String getLabelColour(String agencyGroupID){
			String groupLabelColor;
			try {
				groupLabelColor = [Select labelColor__c FROM ACCOUNT Where id= :agencyGroupID limit 1].labelColor__c;
				if(groupLabelColor==null || groupLabelColor == '')
					groupLabelColor = '#FFFFFF';
			} catch (Exception e){
				groupLabelColor = '#FFFFFF';
			}

			return groupLabelColor;
		}

		public Map<String, Map<String, List<Client_Stage__c>>> getAgencyGroupChecklist(String agencyGroup){
			Map<String, Map<String, List<Client_Stage__c>>> response = new Map<String, Map<String, List<Client_Stage__c>>>();
			response.put('All Destinations', new Map<String, List<Client_Stage__c>>());
			String destination;
			String stage;
			for(Client_Stage__c status : [SELECT Stage__c, Stage_description__c, itemOrder__c, Destination__c, Agency_Group__r.name, Agency_Group__r.ID, Id, Stage_Sub_Options__c FROM Client_Stage__c WHERE Agency_Group__c = :agencyGroup order by Destination__c, Stage__c, ItemOrder__c, createdDate ]){
				destination = status.Stage__c == 'Stage 0' ? 'All Destinations' : status.Destination__c;
				stage = status.Stage__c;				
				if (!string.valueOf(status.Stage_description__c).containsIgnoreCase('xxx')) {
					if(!response.containsKey(destination)){
						response.put(destination, new Map<String, List<Client_Stage__c>>());
						response.get(destination).put('Stage 0', new List<Client_Stage__c>());
					}
					if(!response.get(destination).containsKey(stage)){
						response.get(destination).put(stage, new List<Client_Stage__c>());
					}
					response.get(destination).get(stage).add(status);
				}
			}
			return response;
		}

		public List<Client_Stage__c> getClientStageList(Contact contact, set<id> cachl, User currentUser, boolean isLead){
			List<Client_Stage__c> stages = new List<Client_Stage__c>();


			if(isLead){
				stages.addAll([SELECT Stage__c, Stage_description__c, itemOrder__c, Destination__c, Agency_Group__r.name, Agency_Group__r.ID, Id, Stage_Sub_Options__c FROM Client_Stage__c WHERE Stage__c = 'Stage 0' and Lead_Stage__c = true and (Agency_Group__c in :cachl or Agency_Group__c = :currentUser.Contact.Account.ParentId) order by Agency_Group__r.name, Stage__c, ItemOrder__c, createdDate ]);
				
				stages.addAll([SELECT Stage__c, Stage_description__c, itemOrder__c, Destination__c, Agency_Group__r.name, Agency_Group__r.ID, Id, Stage_Sub_Options__c FROM Client_Stage__c WHERE Destination__c = :contact.Destination_Country__c AND Stage__c != 'Stage 0' and Lead_Stage__c = true and (Agency_Group__c in :cachl or Agency_Group__c = :currentUser.Contact.Account.ParentId) order by Agency_Group__r.name, Stage__c, ItemOrder__c, createdDate ]);
			}else{
				stages.addAll([SELECT Stage__c, Stage_description__c, itemOrder__c, Destination__c, Agency_Group__r.name, Agency_Group__r.ID, Id, Stage_Sub_Options__c FROM Client_Stage__c WHERE Stage__c = 'Stage 0' and (Agency_Group__c in :cachl or Agency_Group__c = :currentUser.Contact.Account.ParentId) order by Agency_Group__r.name, Stage__c, ItemOrder__c, createdDate ]);

				stages.addAll([SELECT Stage__c, Stage_description__c, itemOrder__c, Destination__c, Agency_Group__r.name, Agency_Group__r.ID, Id, Stage_Sub_Options__c FROM Client_Stage__c WHERE Destination__c = :contact.Destination_Country__c AND Stage__c != 'Stage 0' and (Agency_Group__c in :cachl or Agency_Group__c = :currentUser.Contact.Account.ParentId) order by Agency_Group__r.name, Stage__c, ItemOrder__c, createdDate ]);
			}

			return stages;
		}

		public List<Agency_Checklist__c> getAgencyChecklist(Contact contact, set<id> cachl, User currentUser, String destination){
			return [Select CreatedDate, Checklist_Item__c, Checklist_stage__c, Agency_Group__r.name, Id, ItemOrder__c from Agency_Checklist__c where Destination__c = :destination and (Agency_Group__c in :cachl or Agency_Group__c = :currentUser.Contact.Account.ParentId) order by Agency_Group__r.name, Checklist_stage__c, ItemOrder__c, createdDate ];
		}

		public List<Client_Stage__c> getClientStageZeroMultipleDestinations(Contact contact, set<id> cachl, User currentUser, boolean isLead){
			if(isLead){
				return [SELECT Stage__c, Stage_description__c, Destination__c, Agency_Group__r.name, Agency_Group__r.ID, Id, Stage_Sub_Options__c FROM Client_Stage__c WHERE Stage__c = 'Stage 0' AND Lead_Stage__c = true AND (Agency_Group__c in :cachl OR Agency_Group__c = :currentUser.Contact.Account.ParentId) ORDER BY Agency_Group__r.name, Stage__c, ItemOrder__c, createdDate];
			}else{
				return [SELECT Stage__c, Stage_description__c, Destination__c, Agency_Group__r.name, Agency_Group__r.ID, Id, Stage_Sub_Options__c FROM Client_Stage__c WHERE Stage__c = 'Stage 0' AND (Agency_Group__c in :cachl OR Agency_Group__c = :currentUser.Contact.Account.ParentId) ORDER BY Agency_Group__r.name, Stage__c, ItemOrder__c, createdDate];
			}
			
		}

		public List<Client_Stage__c> getClientStageListMultipleDestinations(Contact contact, set<id> cachl, User currentUser, boolean isLead, set<String> destination){
			List<Client_Stage__c> stages = new List<Client_Stage__c>();

			if(isLead){
				stages.addAll([SELECT Stage__c, Stage_description__c, Destination__c, Agency_Group__r.name, Agency_Group__r.ID, Id, Stage_Sub_Options__c FROM Client_Stage__c WHERE Stage__c != 'Stage 0' AND Destination__c in :destination AND Lead_Stage__c = true AND (Agency_Group__c in :cachl OR Agency_Group__c = :currentUser.Contact.Account.ParentId) ORDER BY Agency_Group__r.name, Stage__c, ItemOrder__c, createdDate]);
			}else{
				stages.addAll([SELECT Stage__c, Stage_description__c, Destination__c, Agency_Group__r.name, Agency_Group__r.ID, Id, Stage_Sub_Options__c FROM Client_Stage__c WHERE Stage__c != 'Stage 0' AND Destination__c in :destination AND (Agency_Group__c in :cachl OR Agency_Group__c = :currentUser.Contact.Account.ParentId) ORDER BY Agency_Group__r.name, Stage__c, ItemOrder__c, createdDate]);
			}
			return stages;
			
		}
	}

}