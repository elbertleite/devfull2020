/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class course_timetable_test {

    static testMethod void myUnitTest() {
        
        Map<String,String> recordTypes = new Map<String,String>();
       
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
		
		Account agency = new Account();
		agency.name = 'IP Sydney';
		agency.RecordTypeId = recordTypes.get('Agency');
		agency.BillingCountry = 'Australia';
		insert agency;
		
		
		Account school = new Account();
		school.recordtypeid = recordTypes.get('School');
		school.name = 'Test School';
		school.BillingCountry = 'Australia';
		school.BillingCity = 'Sydney';		
		insert school;
		
        Account campus = new Account();
		campus.RecordTypeId = recordTypes.get('Campus');
		campus.Name = 'Test Campus CBD';
		campus.BillingCountry = 'Australia';
		campus.BillingCity = 'Sydney';
		campus.ParentId = school.id;		
		insert campus;
        
        Course__c course = new Course__c();
		course.Name = 'Certificate III in Business';        
		course.Type__c = 'Business';
		course.Sub_Type__c = 'Business';
		course.Course_Qualification__c = 'Certificate III';
		course.Course_Type__c = 'English/ELICOS';
		course.Course_Unit_Type__c = 'Week';
		course.Only_Sold_in_Blocks__c = true;
		course.Period__c = 'Morning';
		course.Minimum_length__c = 1;
		course.Maximum_length__c = 52;
		course.School__c = school.id;
		insert course;
		
		Course__c course2 = new Course__c();
		course2.Name = 'Certificate I in Business';        
		course2.Type__c = 'Business';
		course2.Sub_Type__c = 'Business';
		course2.Course_Qualification__c = 'Certificate III';
		course2.Course_Type__c = 'English/ELICOS';
		course2.Course_Unit_Type__c = 'Week';
		course2.Only_Sold_in_Blocks__c = false;
		course2.Period__c = 'Evening';
		course2.Minimum_length__c = 1;
		course2.Maximum_length__c = 52;
		course2.School__c = school.id;
		insert course2;
       
		Campus_Course__c cc = new Campus_Course__c();
		cc.Course__c = course.Id;
		cc.Campus__c = campus.Id;        
		cc.Is_Available__c = true;
		cc.is_Selected__c = true;
		cc.Time_Table__c = 'Mon to Friday';
		insert cc;
		
		Campus_Course__c cc2 = new Campus_Course__c();
		cc2.Course__c = course2.Id;
		cc2.Campus__c = campus.Id;        
		cc2.Is_Available__c = true;
		cc2.is_Selected__c = true;
		cc2.Time_Table__c = 'Mon to Friday';
		insert cc2;
        
        
        course_timetable ct = new course_timetable(new Apexpages.Standardcontroller(campus));
        ct.getListNewTimeTable();
        ct.setEditMode();
        ct.saveCourseInfo();
        ct.cancel();
        List<Campus_Course__c> lcc = ct.ListNewTimeTable;
        
    }
}