public without sharing class courses_find {
	public courses_find() {
		
	}

	@RemoteAction
    public static list<AggregateResult> changeNationalityGroup() { 
        return [Select Country__c from Nationality_Group__c GROUP BY Country__c ORDER BY Country__c];
    }

	@RemoteAction
    public static list<AggregateResult> changeDestinationCountry(list<string> supplierIds) { 
		//list<string> supplierIds = (List<String>)JSON.deserialize(supplierIds, list<string>.class);
        return [Select Campus__r.BillingCountry from Campus_Course__c where Campus__c in :supplierIds and is_available__c = true GROUP BY Campus__r.BillingCountry order by Campus__r.BillingCountry];
    }

	@RemoteAction
    public static list<AggregateResult> searchCountryFilters(list<string> supplierIds, list<string> country, list<string> lstDestinations, list<string> listCourseCategory, list<string> listCourseType, list<string> listCourseQualification, list<string> listType, list<string> listSpecialization, list<string> listPeriods, list<string> listSchools) { 
		string sql = 'SELECT campus__r.BillingCountry, count(id) tot FROM Campus_Course__c where  Campus__c in :supplierIds and is_available__c = true ';
		if(country != null && country.size() > 0)
			sql += ' and campus__r.BillingCountry in :country ' ;
		if(lstDestinations != null && lstDestinations.size() > 0)
			sql += ' and campus__r.BillingCity in :lstDestinations ';
		if(listCourseCategory != null && listCourseCategory.size() > 0)
			sql += ' and course__r.Course_Category__c in :listCourseCategory ';
		if(listCourseType != null && listCourseType.size() > 0) 
			sql += ' and Course__r.Course_Type__c = :listCourseType ';
		if(listCourseQualification != null && listCourseQualification.size() > 0)  
			sql += ' and Course__r.Course_Qualification__c = :listCourseQualification ';
		if(listType != null && listType.size() > 0)  
			sql += ' and Course__r.Type__c = :listType ';
		if(listSpecialization != null && listSpecialization.size() > 0)  
			sql += ' and Course__r.Sub_Type__c = :listSpecialization ';
		if(listPeriods != null && listPeriods.size() > 0)  
			sql += ' and Period__c includes ('+ listToString(listPeriods) + ') ';
		if(listSchools != null && listSchools.size() > 0)  
			sql += ' and campus__r.ParentId in :listSchools ';
		sql += ' and campus__r.BillingCountry != null group by campus__r.BillingCountry';
        return Database.Query(sql);
    }

	private static String listToString(list<String> ls){
		string newString  = '';
		for(String s: ls)
			newString += '\'' + s + '\',';
		return newString.removeEnd(',');
	}

	
	@RemoteAction
    public static list<AggregateResult> changeDestinationCity(list<string> supplierIds, list<string> country) { 
		//list<string> supplierIds = (List<String>)JSON.deserialize(supplierIds, list<string>.class);
        return [Select Campus__r.BillingCountry, Campus__r.BillingCity from Campus_Course__c where Campus__c in :supplierIds and Campus__r.BillingCountry in :country and is_available__c = true GROUP BY Campus__r.BillingCountry, Campus__r.BillingCity order by Campus__r.BillingCountry, Campus__r.BillingCity];
    }

	@RemoteAction
    public static list<AggregateResult> changeSchool(list<string> supplierIds, list<string> country, list<string> lstDestinations, list<string> listCourseCategory, list<string> listCourseType, list<string> listCourseQualification, list<string> listType, list<string> listSpecialization) { 
		//list<string> supplierIds = (List<String>)JSON.deserialize(supplierIds, list<string>.class);
		string sql = 'Select campus__r.BillingCountry country, campus__r.ParentId schoolId, Campus__r.Parent.Name schoolName from Campus_Course__c C where Campus__c in :supplierIds and Is_Available__c = true and campus__r.recordType.name = \'Campus\' ';
			if(country != null && country.size() > 0)
				sql += ' and campus__r.BillingCountry in :country ' ;

			if(lstDestinations != null && lstDestinations.size() > 0)
				sql += ' and campus__r.BillingCity in :lstDestinations ';

			//if(listCourseCategory != null && listCourseCategory.size() > 0)
			if(listCourseCategory != null && listCourseCategory.size() > 0)
				sql += ' and Course__r.Course_Category__c in :listCourseCategory ';
			if(listCourseType != null && listCourseType.size() > 0)
				sql += ' and Course__r.Course_Type__c in :listCourseType ';
			if(listCourseQualification != null && listCourseQualification.size() > 0)
				sql += ' and Course__r.Course_Qualification__c in :listCourseQualification ';
			if(listType != null && listType.size() > 0)
				sql += ' and Course__r.Type__c in :listType ';
			if(listSpecialization != null && listSpecialization.size() > 0)
				sql += ' and Course__r.Sub_Type__c in :listSpecialization ';
			sql += '  GROUP BY campus__r.BillingCountry, campus__r.ParentId, Campus__r.Parent.Name order by campus__r.BillingCountry, Campus__r.Parent.Name';
        return Database.query(sql);
    }


	@RemoteAction
    public static list<AggregateResult> changeCourseCategories(list<string> supplierIds, list<string> country, list<string> lstDestinations) { 
		//list<string> supplierIds = (List<String>)JSON.deserialize(supplierIds, list<string>.class);
		string sql = 'Select Course__r.Course_Category__c from Campus_Course__c WHERE Campus__c in :supplierIds and is_available__c = true ';
		if(country != null && country.size() > 0){
			sql += ' and Campus_Course__c.Campus__r.BillingCountry in :country ';
		}
		if(lstDestinations != null && lstDestinations.size() > 0){
			sql += ' AND Campus_Course__c.Campus__r.BillingCity in :lstDestinations ';
		}
		sql += ' and Course__r.Course_Category__c != null ';
		sql += ' GROUP BY Course__r.Course_Category__c order by Course__r.Course_Category__c ';
        return Database.query(sql);
    }

	@RemoteAction
    public static list<AggregateResult> changeCourseArea(list<string> supplierIds, list<string> country, list<string> lstDestinations, list<string> listCourseCategory) { 
		//list<string> supplierIds = (List<String>)JSON.deserialize(supplierIds, list<string>.class);
		string sql = 'Select Course__r.Course_Type__c from Campus_Course__c C where Campus__c in :supplierIds and Is_Available__c = true ';
		System.debug('==>country: '+country);
		if(country != null && country.size() > 0)
			sql += ' and campus__r.BillingCountry in :country ' ;
		if(lstDestinations != null && lstDestinations.size() > 0)
			sql += ' and campus__r.BillingCity in :lstDestinations ';
		sql += ' and Course__r.Course_Type__c != null ';
		if(listCourseCategory != null && listCourseCategory.size() > 0)
			sql += ' and Course__r.Course_Category__c in :listCourseCategory ';
		sql += ' group by Course__r.Course_Type__c order by Course__r.Course_Type__c';
		system.debug('===>sql: '+sql);
        return Database.query(sql);
    }

	@RemoteAction
    public static list<AggregateResult> changeCourseQualification(list<string> supplierIds, list<string> country, list<string> lstDestinations, list<string> listCourseCategory, list<string> listCourseType) { 
		//list<string> supplierIds = (List<String>)JSON.deserialize(supplierIds, list<string>.class);
		String sql = 'Select Course__r.Course_Qualification__c from Campus_Course__c C where Campus__c in :supplierIds and Is_Available__c = true ';
		if(country != null && country.size() > 0)
			sql += ' and campus__r.BillingCountry in :country ' ;
		if(lstDestinations != null && lstDestinations.size() > 0)
			sql += ' and campus__r.BillingCity  in :lstDestinations ';
		if(listCourseCategory != null && listCourseCategory.size() > 0)
			sql += ' and Course__r.Course_Category__c in :listCourseCategory ';
		if(listCourseType != null && listCourseType.size() > 0)
			sql += ' and Course__r.Course_Type__c in :listCourseType '; 
		sql += ' and Course__r.Course_Qualification__c != null group by Course__r.Course_Qualification__c order by Course__r.Course_Qualification__c ';
        return Database.query(sql);
    }

	@RemoteAction
    public static list<AggregateResult> changeCourseType(list<string> supplierIds, list<string> country, list<string> lstDestinations, list<string> listCourseCategory, list<string> listCourseType, list<string> listCourseQualification) { 
		//list<string> supplierIds = (List<String>)JSON.deserialize(supplierIds, list<string>.class);
		System.debug('==>country: '+country);
		String sql = 'Select Course__r.Type__c from Campus_Course__c C where Campus__c in :supplierIds and Is_Available__c = true';
		if(country != null && country.size() > 0)
			sql += ' and campus__r.BillingCountry in :country ' ;
		if(lstDestinations != null && lstDestinations.size() > 0)
			sql += ' and campus__r.BillingCity  in :lstDestinations ';
		if(listCourseCategory != null && listCourseCategory.size() > 0)
			sql += ' and Course__r.Course_Category__c in :listCourseCategory ';
		if(listCourseType != null && listCourseType.size() > 0)
			sql += ' and Course__r.Course_Type__c in :listCourseType ';
		if(listCourseQualification != null && listCourseQualification.size() > 0)
			sql += ' and Course__r.Course_Qualification__c in :listCourseQualification ';

		sql += ' and Course__r.Type__c != null group by Course__r.Type__c order by Course__r.Type__c ';
        return Database.query(sql);
    }

	@RemoteAction
    public static list<AggregateResult> changeCourseSpecialization(list<string> supplierIds, list<string> country, list<string> lstDestinations, list<string> listCourseCategory, list<string> listCourseType, list<string> listCourseQualification, list<string> listType) { 
		//list<string> supplierIds = (List<String>)JSON.deserialize(supplierIds, list<string>.class);
		string sql = 'Select Course__r.Sub_Type__c from Campus_Course__c WHERE Campus__c in :supplierIds and Is_Available__c = true ';
			if(country != null && country.size() > 0)
				sql += ' and campus__r.BillingCountry in :country ' ;

			if(lstDestinations != null && lstDestinations.size() > 0)
				sql += ' and campus__r.BillingCity in :lstDestinations ';

			if(listCourseCategory != null && listCourseCategory.size() > 0)
				sql += ' and Course__r.Course_Category__c in :listCourseCategory ';
			if(listCourseType != null && listCourseType.size() > 0)
				sql += ' and Course__r.Course_Type__c in :listCourseType ';
			if(listCourseQualification != null && listCourseQualification.size() > 0)
				sql += ' and Course__r.Course_Qualification__c in :listCourseQualification ';
			if(listType != null && listType.size() > 0)
				sql += ' and Course__r.Type__c in :listType ';
			sql += ' and Course__r.Sub_Type__c != NULL  GROUP BY Course__r.Sub_Type__c order by Course__r.Sub_Type__c';
        return Database.query(sql);
    }

	@RemoteAction
    public static list<SelectOption> changePeriods() { 
		list<SelectOption> Periods = new list<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Campus_Course__c.Period__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple)
			Periods.add(new SelectOption(f.getValue(), f.getLabel()));
        return Periods;
    }

	@RemoteAction
    public static list<SelectOption> changeClientNationality() { 
		list<SelectOption> clientNationality = new list<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Nationality_Group__c.Country__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple)
			clientNationality.add(new SelectOption(f.getValue(), f.getLabel()));
        return clientNationality;
    }

	// @RemoteAction
	// public static List<SelectOption> changeUnitsSearch() {
	// 	list<SelectOption> listUnitsSearch = new List<SelectOption>();
	// 	for(integer i = 1; i <= 52; i++)
	// 		listUnitsSearch.add(new SelectOption(string.valueOf(i),string.valueOf(i)));
    //     return listUnitsSearch;
    // }

	@RemoteAction
	public static integer maxCourseLength(){
		integer maxCourseLength = 1;
		for(AggregateResult ar :[Select MAX(Course_Lenght_in_Months__c) max from Course__c where Course_Category__c = 'other'])
			maxCourseLength = integer.valueOf((decimal)ar.get('max'));
		return maxCourseLength + 1;
	}

	@RemoteAction
    public static map<string,boolean> supplierIds_json_angular() {
        
			map<string,boolean> supplierIds = new map<string,boolean>();

			User u = [Select UserType, Name, Contact.AccountId from User where id = :Userinfo.getUserId() limit 1];

			if(u.UserType == 'Standard' || u.Name == 'HiFy Platform Site Guest User' ){
				for(AggregateResult ac : [select campus__c from campus_course__c where is_available__c = true group by campus__c])
					supplierIds.put((string)ac.get('campus__c'),false);
			} else{

				String supplierSQL = 'Select Supplier__c,Preferable__c from Supplier__c S where Agency__c = \'' + u.Contact.AccountId + '\' and record_type__c = \'Campus\' and Available__c = true and Supplier__r.Disabled_Campus__c = false and supplier__r.showCaseOnly__c = false';

				for (Supplier__c s : Database.query(supplierSQL)){
					supplierIds.put(s.Supplier__c, s.Preferable__c);
				}
			}
			return supplierIds;
	    
    }

	@RemoteAction
    public static list<Currency_rate__c> retrieveAgencyCurrencies(){
		user UserDetails = IpFunctions.getUserInformation(UserInfo.getUserId());
		list<Currency_rate__c> rates = [Select CurrencyCode__c, Value__c from Currency_rate__c where Agency__c = :UserDetails.Contact.Account.id and CurrencyCode__c != null order by CurrencyCode__c];
		rates.add(new Currency_rate__c(CurrencyCode__c= UserDetails.contact.account.Account_Currency_Iso_Code__c, Value__c = 1));
		return rates;
	}

	private list<id> supplierIds;
	private user UserDetails = IpFunctions.getUserInformation(UserInfo.getUserId());
    public  string supplierIds_json {
        get{
			
	        if (supplierIds_json == null){

	            supplierIds = new list<id>();

				
				User u = [Select UserType, Name from User where id = :Userinfo.getUserId() limit 1];

	            if(u.UserType == 'Standard' || u.Name == 'HiFy Platform Site Guest User' ){
		            for(Account ac : [select id, ParentID from Account where recordtype.Name = 'Campus'])
		            	supplierIds.add(ac.id);
	            } else{

	            	String supplierSQL = 'Select Supplier__c from Supplier__c S where Agency__c = \'' + UserDetails.Contact.AccountId + '\' and record_type__c = \'Campus\' and Available__c = true and Supplier__r.Disabled_Campus__c = false and supplier__r.showCaseOnly__c = false';

		            for (Supplier__c s : Database.query(supplierSQL)){
		            	supplierIds.add(s.Supplier__c);
		            }

		       }

	        }
			return JSON.Serialize(supplierIds);
	    }
        set;
    }


	public class course{
		public string flagUrl{get; set;}
		public boolean hasDeal{get; set;}
		public string lastCampusCourseId {get; set;}
		public string courseName{get; set;}
		public string campusCurrency{get; set;}
		public string campusCountry{get; set;}
		public string campusCity{get; set;}
		public string schoolCampusName{get; set;}
		public string campusId{get; set;}
		public string courseId{get; set;}
		public string schoolID{get; set;}
		public string campusCourseId{get; set;}
		public string SchoolImage{get; set;}
		public double totalTuition{get; set;}
		public double totalExtraFees{get; set;}
		public double totalValueCourse{get; set;}
		public double coursePrice{get; set;}
		public string courseUnitType{get; set;}
		public string priceValidUntil{get; set;}
		public string priceValidUntilExpired{get; set;}
		public double unitBasedCoursePrice{get; set;}
		public double unitFullCoursePrice{get; set;}
		public boolean onlySoldInBlocks{get; set;}
		public boolean favourite{get; set;}
		public String validPromotionsDesc{get; Set;}
		public string priceNationality{get; set;}
		public string clientNationality{get; set;}
		public string courseCategory{get; set;}

		public string priceDocumentDescription {get; set;}
		public string priceDocumentName {get; set;}
		public string priceDocumentURL {get; set;}

		public integer numberUnitsStudy{get{if(numberUnitsStudy == null) numberUnitsStudy = 0; return numberUnitsStudy;} set;}
		public searchcoursecountry.promotionDetails bestpromotionDeal{get{if(bestpromotionDeal == null) bestpromotionDeal = new searchcoursecountry.promotionDetails(); return bestpromotionDeal;} set;}
		public double totalValueCourseCurrency{get{if(totalValueCourseCurrency == null) totalValueCourseCurrency = 0; return 0;} set;}
		public double courseExtraFreeWeek{get{if(courseExtraFreeWeek == null) courseExtraFreeWeek = 0; return courseExtraFreeWeek;} set;}
		public string courseLenghtInMonths{get{if(courseLenghtInMonths == null) courseLenghtInMonths = ''; return courseLenghtInMonths;} set;}
		public Double pricePerUnit {get{if(pricePerUnit == null) pricePerUnit = 0; return pricePerUnit;} set;}
		public double totalExtraFeePromotions{get{if(totalExtraFeePromotions == null) totalExtraFeePromotions = 0; return totalExtraFeePromotions;} set;}
		public double soldInBlocksDiscountValue{get{if(soldInBlocksDiscountValue == null) soldInBlocksDiscountValue = 0; return soldInBlocksDiscountValue;} set;}
		public Double firstInstalmentValue {get{if(firstInstalmentValue == null) firstInstalmentValue = 0; return firstInstalmentValue;} set;}
		public Double numberRemainingInstalments {get{if(numberRemainingInstalments == null) numberRemainingInstalments = 0; return numberRemainingInstalments;} set;}
		public map<string,searchcoursecountry.retrievedExtraFees> validExtraFees{get{if(validExtraFees == null) validExtraFees = new map<string,searchcoursecountry.retrievedExtraFees>(); return validExtraFees;} set;}
		public map<string,searchcoursecountry.promotionDetails> promotionExtraFeeDiscount{get{if(promotionExtraFeeDiscount == null) promotionExtraFeeDiscount = new map<string,searchcoursecountry.promotionDetails>(); return promotionExtraFeeDiscount;} set;}
		public list<Course_Intake_Date__c> courseNextIntakeDate{get{ if(courseNextIntakeDate == null) courseNextIntakeDate = new list<Course_Intake_Date__c>(); return courseNextIntakeDate;} set;}
		public Double InstalmentsValue {get{if(InstalmentsValue == null) InstalmentsValue = 0; return InstalmentsValue;} set;}
		// public course(string flagUrl, boolean hasDeal, string courseName, string campusCountry, string schoolCampusName, string campusId, string campusCourseId, string SchoolImage, double totalExtraFees, 
		// 					double totalTuition, double totalValueCourse, string campusCurrency, string courseUnitType, integer numberUnitsStudy, searchcoursecountry.promotionDetails bestpromotionDeal, 
		// 					string lastCampusCourseId, double courseExtraFreeWeek, string courseLenghtInMonths, Double pricePerUnit, string priceValidUntilExpired, double unitBasedCoursePrice, double unitFullCoursePrice, double totalExtraFeePromotions, 
		// 					double soldInBlocksDiscountValue, Double firstInstalmentValue, Double numberRemainingInstalments, Double InstalmentsValue, boolean onlySoldInBlocks, boolean favourite, String validPromotionsDesc, 
		// 					map<string,searchcoursecountry.retrievedExtraFees> validExtraFees, map<string,searchcoursecountry.promotionDetails> promotionExtraFeeDiscount, list<Course_Intake_Date__c> courseNextIntakeDate){
		public course(string flagUrl, searchcoursecountry.courseDetails rc){
			if(rc.campusCountry != null && rc.campusCountry != '')
				this.flagUrl = flagUrl + '/flags/24/' +rc.campusCountry.replaceAll(' ','') + '.png';
			// this.hasDeal = rc.bestpromotionDeal.quantity > 0 || rc.totalExtraFeePromotions > 0;
			this.hasDeal = rc.bestpromotionDeal.quantity > 0 || rc.totalExtraFeePromotions > 0 || rc.courseExtraFreeWeek > 0 || rc.unitFullCoursePrice > 0;
			this.lastCampusCourseId= rc.lastCampusCourseId;
			this.courseName = rc.courseName;
			this.campusCountry = rc.campusCountry;
			this.campusCity = rc.campusCity;
			this.schoolCampusName = rc.schoolCampusName;
			this.campusId = rc.campusId; 
			this.courseId = rc.courseId; 
			this.schoolID = rc.schoolID;
			this.campusCourseId = rc.campusCourseId;
			this.SchoolImage = rc.SchoolImage;
			this.totalExtraFees = rc.totalExtraFees;
			this.totalTuition = rc.totalTuition;			
			this.totalValueCourse = rc.totalValueCourse; //
			Decimal toround = 3.14159265;
			Decimal rounded = toround.setScale(2);

			this.campusCurrency = rc.campusCurrency;
			this.totalValueCourseCurrency = 0;
			this.courseUnitType = rc.courseUnitType;
			this.numberUnitsStudy = rc.numberUnitsStudy;
			this.bestpromotionDeal = rc.bestpromotionDeal;
			this.courseExtraFreeWeek = rc.courseExtraFreeWeek;
			this.courseLenghtInMonths = rc.courseLenghtInMonths;
			this.pricePerUnit = rc.pricePerUnit;
			this.priceValidUntil = rc.priceValidUntil;
			this.priceValidUntilExpired = rc.priceValidUntilExpired;
			this.unitBasedCoursePrice = rc.unitBasedCoursePrice;
			this.unitFullCoursePrice = rc.unitFullCoursePrice;
			this.totalExtraFeePromotions = rc.totalExtraFeePromotions;
			this.soldInBlocksDiscountValue = rc.soldInBlocksDiscountValue;
			this.firstInstalmentValue = rc.firstInstalmentValue;
			this.numberRemainingInstalments = rc.numberRemainingInstalments;
			this.InstalmentsValue = rc.InstalmentsValue;
			this.onlySoldInBlocks = rc.onlySoldInBlocks;
			this.favourite = rc.favourite;
			this.validPromotionsDesc = rc.validPromotionsDesc;
			this.validExtraFees = rc.validExtraFees;
			this.promotionExtraFeeDiscount = rc.promotionExtraFeeDiscount;
			this.courseNextIntakeDate = rc.courseNextIntakeDate;
			this.priceNationality = rc.priceNationality;
			this.clientNationality = rc.clientNationality; 
			this.courseCategory = rc.courseCategory; 

			this.priceDocumentDescription = rc.priceDocumentDescription;
			this.priceDocumentName = rc.priceDocumentName;
			this.priceDocumentURL = rc.priceDocumentURL; 

		}
		public course(string lastCampusCourseId){
			this.lastCampusCourseId= lastCampusCourseId;
		}
	} 

	@RemoteAction
	public static list<course> searchCourses(map<string,boolean> supplierIdsMap, list<string> supplierIds, string clientNationality, string country, list<string>  cities, list<string>  listCourseCategory,  list<string> listCourseType, list<string> listCourseQualification, 
									list<string> listType, list<string> listSpecialization, list<string> listSchool, list<string> listPeriods, string paymentDate, integer duration, integer hourFrom, 
									integer hourTo, string location, integer lenghtFrom, integer lenghtTo, string offsetPos, integer batchSize, list<string>  listSchools){ 
		searchcoursecountry rt = new searchcoursecountry(); 
		list<course> courses = new list<course>(); 
		boolean hasDeal = false;
		string urlFlag = GetResourceURL('flags'); 
		system.debug('==>favourite: '+supplierIdsMap); 
		for(searchcoursecountry.courseDetails rc:rt.searchCoursesJson(supplierIdsMap, supplierIds, clientNationality, country, cities, listCourseCategory, listCourseType, listCourseQualification, listType, listSpecialization, listSchool, listPeriods, paymentDate, duration, hourFrom, hourTo, location, lenghtFrom, lenghtTo, offsetPos, batchSize, listSchools, new list<string>(), false, null)){ 
			//System.debug('==>rc.courseName: '+rc.courseName);
			if(rc.campusCourseId != null)
				courses.add(new course(urlFlag, rc));
			else 
			courses.add(new course(rc.lastCampusCourseId));
		}
		System.debug('==>courses: '+courses);
		return courses; 

	}

	public pageReference addCourseToCart(){
		string campusCourseId = ApexPages.currentPage().getParameters().get('campusCourse');
		string campusId = ApexPages.currentPage().getParameters().get('campus');
		string numUnits = ApexPages.currentPage().getParameters().get('numUnits');
		
		string clientNationality = ApexPages.currentPage().getParameters().get('clientNationality');
		string cartId = ApexPages.currentPage().getParameters().get('cartId');
		string location = ApexPages.currentPage().getParameters().get('location');

		string pd = ApexPages.currentPage().getParameters().get('paymentDate');
		date paymentDate;
		system.debug('==>pd: '+pd);
		if(pd != null)
			paymentDate = date.parse(pd);
		else paymentDate = System.today();
		system.debug('==>paymentDate: '+paymentDate);

		Web_Search__c courseCart = [Select id, Nationality__c, location__c from Web_Search__c where id = :cartId limit 1];
		
		boolean isSameNationalitySearch = courseCart.Nationality__c == clientNationality;
		boolean isSameLocation = courseCart.location__c == decimal.valueOf(location);
		

		searchcoursecountry rt = new searchcoursecountry(); 
		rt.addItemCart(new list<string>{campusCourseId}, integer.valueOf(numUnits), new list<string>{campusId}, new map<string, double>(), null, cartId, clientNationality, location, paymentDate);


		if(!isSameNationalitySearch || !isSameLocation){

			courseCart.Nationality__c = clientNationality;
			courseCart.Location__c = decimal.valueOf(location);
			update courseCart;

			
		}

		return null;
	}

	public void deleteCoursesCart(){
		string cartId = ApexPages.currentPage().getParameters().get('cartId');
		delete [Select id from Web_Search__c where id = :cartId];
	}

	public void deleteCourseFromCart(){
		string courseId = ApexPages.currentPage().getParameters().get('courseId');
		list<Search_Courses__c> itemsDelete = new list<Search_Courses__c>([Select id, Web_Search__c from Search_Courses__c where id = :courseId limit 1]);
		string cartId = itemsDelete[0].Web_Search__c;
		delete itemsDelete;
		//Delete Cart when empty
		integer totItemsCart = [Select count() from Search_Courses__c where Web_Search__c = :cartId];
		if(totItemsCart == 0)
			delete [Select id from Web_Search__c where id = :cartId];
	}

	public static String GetResourceURL(String resourceName){ 

		Profile p = [Select UserType  from Profile where Id =: userinfo.getProfileid()];
		// String addCommunity = '';

		// if(p.UserType  == 'CspLitePortal') 
		// 	addCommunity = '/ip';

		// Fetching the resource
		List<StaticResource> resourceList= [SELECT Name, NamespacePrefix, SystemModStamp FROM StaticResource WHERE Name = :resourceName];

		// Checking if the result is returned or not
		if(resourceList.size() == 1){

			// Getting namespace
			String namespace = resourceList[0].NamespacePrefix;
			// Resource URL
			 
			return Site.getPathPrefix() + '/resource/' + String.valueOf(((DateTime)resourceList[0].get('SystemModStamp')).getTime()) + '/' + (namespace != null && namespace != '' ? namespace + '__' : '') + resourceName; 
		}
		else return '';
	}

	//=========================================== SHOPPING CART ==================================

	@RemoteAction
	public static  Web_Search__c retrieveCart(string clientId, string Nationality){
		Web_Search__c courseCart;
		try{
			String sql = 'Select id, Email__c, Visits__c, Country__c, Combine_Quotation__c, Total_Products__c, client__c, IP_Client__c, Nationality__c, location__c, ';
				sql += ' (Select Selected__c, Category__c, Currency__c, Description__c, isCustom__c, Name__c, Price__c, Quantity__c, Search_Course__c, Total__c, Unit_Description__c from Search_Course_Products__r) ';
				sql += ' from Web_Search__c where ';
				if(clientId != null && clientId != '')
					sql += ' Client__c = \'' + clientId + '\'';
				else {
					//sql += ' Email__c = \'' + u.email + '\'';
					sql += ' createdById =  \'' + UserInfo.getUserId() + '\'';
					sql += ' and Client__c = null ';
				}
				sql += ' and active__c = true limit 1';
			System.debug('==>sql: '+sql);
			courseCart = database.query(sql);
		}catch(Exception e){
			if(clientId != null && clientId != '')
				courseCart = new Web_Search__c(Client__c = clientId, country__c = Nationality, visits__c = 1, active__c = true);
			else courseCart = new Web_Search__c(country__c = Nationality, visits__c = 1, active__c = true);
			insert courseCart;
		}
		return courseCart;
	}

	public class cartDetails{
		public Search_Courses__c course{get; set;}
		public string schoolLogo{get{if(schoolLogo == null) schoolLogo = ''; return schoolLogo;} set;}
		public  cartDetails(Search_Courses__c course, string schoolLogo){
			this.course = course;
			this.schoolLogo = schoolLogo;
		}

	}

	@RemoteAction
	public static list<cartDetails> retrieveCoursesCart(string cartId){
		list<cartDetails> cart = new list<cartDetails>();
		for(Search_Courses__c c:[Select isCustomCourse__c, Campus_Course__r.Campus__r.Campus_Photo_URL__c, Campus_Course__c, Campus_Course__r.Campus__r.Name, Campus_Course__r.Course__r.Name, Free_Units__c, Number_of_Units__c, Total_Course__c, Unit_Type__c,
								Campus_Course__r.Campus__r.Account_Currency_ISO_Code__c, Custom_Country__c, Campus_Course__r.Campus__r.BillingCountry, Campus_Course__r.Course__r.Only_Sold_in_Blocks__c,
								Custom_Campus__c, Custom_Course__c, Custom_Currency__c, Custom_NumberOfUnits__c, Custom_Unit__c, Extra_Units__c
						from Search_Courses__c
						WHERE Web_Search__c = :cartId AND Course_Deleted__c = false order by Course_Order__c NULLS LAST, CreatedDate]){
			System.debug('===>photo: '+c.Campus_Course__r.Campus__r.Campus_Photo_URL__c);
			string photo = '';
			if(!c.isCustomCourse__c && c.Campus_Course__r.Campus__r.Campus_Photo_URL__c != null && c.Campus_Course__r.Campus__r.Campus_Photo_URL__c != '')
				photo = EncodingUtil.urlEncode(c.Campus_Course__r.Campus__r.Campus_Photo_URL__c, 'UTF-8');
			if(!c.isCustomCourse__c)
				c.Campus_Course__r.Campus__r.Campus_Photo_URL__c = null;
			cart.add(new cartDetails(c, photo));
		}
		 return cart;


	}

	public void openTemplate(){
		string clientNationality = ApexPages.currentPage().getParameters().get('clientNationality');
		string cartId = ApexPages.currentPage().getParameters().get('cartId');
		string location = ApexPages.currentPage().getParameters().get('location');

		courseCompare csc = new courseCompare(cartId);
		csc.openTemplate(clientNationality, decimal.valueOf(location)); 
	}

	@RemoteAction
	public static Contact retrieveClientDetails(string clientId){
		//string clientId = ApexPages.currentPage().getParameters().get('clientid');
		if(clientId != null && clientid != ''){
			return [Select Id, Nationality__c, Name, Current_Agency__r.Name, Owner__c from Contact where Id = :clientId];
		}
		return null;
	}

	@RemoteAction
	public static User retrieveUserDetails(){
		return [Select Id, contact.Preferable_School_Country__c, contact.Preferable_Instalment_Option__c, contact.Preferable_Tuition_Option__c, contact.Preferable_School_City__c, contact.Preferable_Nationality__c, contact.Preferable_Hours_Per_Week_From__c, contact.account.Account_Currency_Iso_Code__c, contact.account.Parent.destination_group__c, contact.Preferable_Currency_Column__c from User where Id = :Userinfo.getUserId()]; 
	}


	/*
		Select course__r.Course_Category__c, course__r.course_type__c, course__r.Course_Qualification__c, course__r.type__c, course__r.sub_type__c
from campus_course__c where course__r.Course_Category__c = 'Language' and campus__r.billingCountry = 'Australia' and 
(name like '%Cambridge%' or course__r.Course_Category__c like '%Cambridge%' or course__r.course_type__c like '%Cambridge%' or course__r.Course_Qualification__c like '%Cambridge%' or course__r.type__c like '%Cambridge%' or course__r.sub_type__c like '%Cambridge%')
group by course__r.Course_Category__c, course__r.course_type__c, course__r.Course_Qualification__c, course__r.type__c, course__r.sub_type__c
	*/
}