/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class contact_document_test {

    static testMethod void myUnitTest() {
        
        Map<String,String> recordTypes = new Map<String,String>();
  		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
			
		TestFactory tf = new TestFactory();	
        
        Account agency = tf.createAgency();
        Contact client1 = tf.createClient(agency);
        
        
        Client_Document__c cd = new Client_Document__c();
        cd.Document_Category__c = 'Passport';
        cd.Document_Type__c = 'Passport';
        cd.Client__c = client1.id;
        insert cd;
        
        Client_Document_File__c cdf = new Client_Document_File__c();
        cdf.Client__c = client1.id;
        cdf.File_Name__c = 'SomeFile.txt';
        cdf.ParentId__c = cd.id;
        insert cdf;
        
        Client_Document__c cd2 = new Client_Document__c();
        cd2.Document_Category__c = 'Travel';
        cd2.Document_Type__c = 'Flight Ticket';
        cd2.Client__c = client1.id;
        insert cd2;
        
        Client_Flight_Details__c cfd = new Client_Flight_Details__c();
        cfd.Client_Document__c = cd2.id;
        insert cfd;
        
        Client_Document_File__c cdf2 = new Client_Document_File__c();
        cdf2.Client__c = client1.id;
        cdf2.File_Name__c = 'SomeFile.txt';
        cdf2.ParentId__c = cd2.id;
        insert cdf2;
        
        Client_Document__c cd3 = new Client_Document__c();
        cd3.Document_Category__c = 'Personal';
        cd3.Document_Type__c = 'Bank Details';
        cd3.Client__c = client1.id;
        insert cd3;
        
        Client_Document__c cd4 = new Client_Document__c();
        cd4.Document_Category__c = 'Visa';
        cd4.Document_Type__c = 'Visa';
        cd4.Client__c = client1.id;
        insert cd4;
        
        
        Client_Document_File__c cdfVisa = new Client_Document_File__c();
        cdfVisa.Client__c = client1.id;
        cdfVisa.File_Name__c = 'SomeFile.txt';
        cdfVisa.ParentId__c = cd4.id;
        insert cdfVisa;
        
        
        ApexPages.currentPage().getParameters().put('cid', client1.id);
        ApexPages.currentPage().getParameters().put('cat', 'Visa');
        
        contact_document docs1 = new contact_document(null);
        
        docs1.document.Visa_country_applying_for__c = 'Australia';
        docs1.document.Expiry_date__c = system.today().addDays(60);
        
        docs1.save();
        
        ApexPages.currentPage().getParameters().put('cid', client1.id);
        ApexPages.currentPage().getParameters().put('id', cd.id);
        contact_document docs = new contact_document(null);
        Client_Document__c dummy =  docs.document;
        docs.clearFields();
        List<SelectOption> theList = docs.countries;
        docs.editDocument();
        docs.getDocumentFiles();
        docs.getOrgId();
        boolean b = docs.isBankDetails;
        b = docs.isDefaultDetails;
        b = docs.isFlightDetails;
        b = docs.isVisaDetails;
        docs.cancel();
        
        
        
        ApexPages.currentPage().getParameters().put('cid', client1.id);
        ApexPages.currentPage().getParameters().put('id', cd2.id);
        contact_document docs3 = new contact_document(null);
        docs3.addNewFlight();
        ApexPages.currentPage().getParameters().put('flightID', cfd.id);
        docs3.deleteFlight();
        ApexPages.currentPage().getParameters().put('flightID', '0');
        docs3.deleteNewFlight();
        docs3.save();
        
        ApexPages.currentPage().getParameters().put('cid', client1.id);
        ApexPages.currentPage().getParameters().put('visaID', cd4.id);
        contact_document docs4 = new contact_document(null);        
        
        docs4.document = null;
        docs4.getFields();
        
        
        
    }
}