public with sharing class AgencyMaterialsController extends TrainingMaterialsHelper {

	public List<LinkBreadCrumb> breadCrumb{get;set;}
	//public List<LinkBreadCrumb> breadCrumbSaveMaterial{get;set;}

	public Folder folderGroup{get;set;}
	public List<Folder> rootFolders{get;set;}
	public Folder selectedFolder{get;set;}
	public Folder newFolder{get;set;}
	//public video_upload__c newSubfolder{get;set;}
	public Folder selectedFolderToSaveMaterial{get;set;}
	
	public boolean showModalFullListFiles{get{if(showModalFullListFiles == null) showModalFullListFiles = false; return showModalFullListFiles;} set;}

	public boolean editingMaterial{get;set;}
	public boolean changeFolderForMaterial{get;set;}
	public boolean saveFolderForMaterial{get;set;}
	public boolean editingFolder{get;set;}
	public video_upload__c newMaterial{get;set;}

	public List<Material> materials{get;set;}

	public MaterialModule materialSelected{get;set;}
	public TrainingMaterial materialTrainingSelected{get;set;}

	public String nameDescriptionSearch{get;set;}
	public String languageSearch{get;set;}


	public static String bucketName {get{if(bucketName == null) bucketName = IPFunctions.s3bucket; return bucketName;}set;}
	public String folderS3Material {get;set;}


	public List<Material> listFilesInFolder{get;set;}

	public boolean showAsGrid{get;set;}

	public AgencyMaterialsController(){
		try{
			super();
			showAsGrid = false;
			isSearchResult = false;
			
			String objectID = ApexPages.currentPage().getParameters().get('id');
			if(IPFunctions.isValidObject(objectID)){
				init(objectID);
			}else{
				loadDefaultPage();
			}
		}catch(Exception e){
			system.debug('Error on Init() ===> ' + e.getLineNumber());	
		}
	}

	public void init(String objectID){	
		try{
			if(objectID.startsWith('001')){
				loadRootFolder(objectID);			
			}else{
				video_upload__c obj = retrieveObject(objectID);
				if(obj == null){
					loadDefaultPage();
				}else{
					if(obj.objectType__c == 'Materials Folder'){
						loadSubFolder(objectID);
					}else{
						Material material = loadFullMaterial(objectID);
						if(material == null){
							loadDefaultPage();
						}else{
							if(material.type.equals('Material')){
								materialSelected = (MaterialModule) material;		
							}else if(material.type.equals('Training')){		
								materialTrainingSelected = (TrainingMaterial) material;
								materialTrainingSelected.init();
							}
						}
						if(material.parentFolderID.startsWith('001')){
							loadRootFolder(material.parentFolderID);
						}else{
							loadSubFolder(material.parentFolderID);
						}
					}
				}
			}
			if(folderGroup == null){
				getGroupFolderUser(this.selectedFolder);
			}
			this.materials = retrieveMaterials(this.selectedFolder.id);
			generateReverseBreadCrumb();
		}catch(Exception e){
			system.debug('Error with ID '+objectID+ ' Line '+e.getLineNumber());
		}
		
	}

	public void changeSortBy(){
		String sortType = ApexPages.currentPage().getParameters().get('sortType');
		List<Material> result = new List<Material>();
		if(sortType.equals('updatedBy')){
			List<SortMaterialByDate> sorted = new List<SortMaterialByDate>();
			for(Material mat : this.materials){
				sorted.add(new SortMaterialByDate(mat));
			}
			sorted.sort();
			for(SortMaterialByDate material : sorted){
				result.add(material.mat);
			}
		}else if(sortType.equals('materialName')){
			List<SortMaterialByName> sorted = new List<SortMaterialByName>();
			for(Material mat : this.materials){
				sorted.add(new SortMaterialByName(mat));
			}
			sorted.sort();
			for(SortMaterialByName material : sorted){
				result.add(material.mat);
			}
		}
		else if(sortType.equals('language')){
			List<SortMaterialByLanguage> sorted = new List<SortMaterialByLanguage>();
			for(Material mat : this.materials){
				sorted.add(new SortMaterialByLanguage(mat));
			}
			sorted.sort();
			for(SortMaterialByLanguage material : sorted){
				result.add(material.mat);
			}
		}
		this.materials = result;
	}

	public class SortMaterialByName implements Comparable{
		public Material mat;

		public SortMaterialByName(Material mat){
			this.mat = mat;
		}

		public Integer compareTo(Object compareTo) {
			SortMaterialByName compareToMat = (SortMaterialByName)compareTo;
			
			return this.mat.name.toLowerCase().compareTo(compareToMat.mat.name.toLowerCase());
		}
	}

	public class SortMaterialByDate implements Comparable{
		public Material mat;

		public SortMaterialByDate(Material mat){
			this.mat = mat;
		}

		public Integer compareTo(Object compareTo) {
			SortMaterialByDate compareToMat = (SortMaterialByDate)compareTo;
			
			if (this.mat.lastModifiedDate < compareToMat.mat.lastModifiedDate){
				return 1;
			}else if (this.mat.lastModifiedDate > compareToMat.mat.lastModifiedDate){
				return -1;
			}
			return 0;
		}
	}

	public class SortMaterialByLanguage implements Comparable{
		public Material mat;

		public SortMaterialByLanguage(Material mat){
			this.mat = mat;
		}

		public Integer compareTo(Object compareTo) {
			SortMaterialByLanguage compareToMat = (SortMaterialByLanguage)compareTo;
			
			if(this.mat.type == 'Training' || compareToMat.mat.type == 'Training'){
				return -1;
			}

			return this.mat.language.toLowerCase().compareTo(compareToMat.mat.language.toLowerCase());
		}
	}

	public void changeViewType(){
		this.showAsGrid = !showAsGrid;
	}

	private void getGroupFolderUser(Folder folder){
		system.debug('GETTING GROUP FOLDER '+folder.id);
		if(folder.id.startsWith('001')){
			Account acc = [select parent.id, parent.name from Account where id = :folder.id];
			folderGroup = new Folder(acc.parent.id, acc.parent.name, null, null, null, null, false, false, false);
		}else{
			video_upload__c vd = [SELECT CreatedBy.Contact.Account.Parent.Name, CreatedBy.Contact.Account.Parent.Id, Is_Training_Subfolder__c, Available_Only_To_Group__c from video_upload__c where id = :folder.id];
			system.debug('VIDEO UPLOAD FOUND '+vd);
			system.debug('VIDEO UPLOAD FOUND PARENT NAME '+vd.CreatedBy.Contact.Account.Parent.Name);
			system.debug('VIDEO UPLOAD FOUND PARENT ID '+vd.CreatedBy.Contact.Account.Parent.Id);

			folderGroup = new Folder(vd.CreatedBy.Contact.Account.Parent.Id, vd.CreatedBy.Contact.Account.Parent.Name, null, null, null, null, false, vd.Is_Training_Subfolder__c, vd.Available_Only_To_Group__c);
			system.debug('THE FOLDER GROUP '+folderGroup);
		}
	}

	private void loadDefaultPage(){
		
		loadRootFolder(currentUser.Contact.Account.ParentId);

		this.breadCrumb = new List<LinkBreadCrumb>();
		breadCrumb.add(new LinkBreadCrumb('Home', null));
		
		generateBreadcrumb();

		this.materials = retrieveMaterials(selectedFolder.id);
	}

	private void generateReverseBreadCrumb(){	
		this.breadCrumb = new List<LinkBreadCrumb>();
		this.breadCrumb.add(new LinkBreadCrumb(selectedFolder.name, selectedFolder.id));
		Folder folder = selectedFolder;
		while(true){
			if(folder != null && IPFunctions.isValidObject(folder.parentId)){
				system.debug('SEARCHING FOR PARENT '+folder.parentId);
				if(folder.parentId.startsWith('001')){
					folder = retrieveRootFolder(folder.parentId);
				}else{
					folder = retrieveSubFolder(folder.parentId, true);
				}
				if(folder != null){
					this.breadCrumb.add(0, new LinkBreadCrumb(folder.name, folder.id));
				}
			}else{
				this.breadCrumb.add(0, new LinkBreadCrumb('Home', null));
				break;
			}
		}
	}

	private void generateBreadcrumb(){

		if(this.selectedFolder != null){
			LinkBreadCrumb link = new LinkBreadCrumb(selectedFolder.name, selectedFolder.id);
			if(this.breadCrumb.contains(link)){
				Integer indexElement = this.breadCrumb.indexOf(link);
				List<LinkBreadCrumb> newBreadCrumb = new List<LinkBreadCrumb>();
				for(Integer i = 0; i <= indexElement; i++){
					newBreadCrumb.add(this.breadCrumb.get(i));
				}
				this.breadCrumb = newBreadCrumb;
			}else{
				this.breadCrumb.add(link);
			}
		}else{//Home page
			this.breadCrumb = new List<LinkBreadCrumb>();
			this.breadCrumb.add(new LinkBreadCrumb('Home', null));
		}
	}

	public void loadFolderMaterials(){
		String idFolder = ApexPages.currentPage().getParameters().get('idFolder');
		
		if(idFolder != null && !''.equals(idFolder)){
			if(idFolder.startsWith('001')){
				loadRootFolder(idFolder);
			}else{
				loadSubFolder(idFolder);
			}
			this.materials = retrieveMaterials(this.selectedFolder.id);
		}else{
			this.selectedFolder = null;
			this.rootFolders = retrieveRootParentfolders();
			this.materials = retrieveMaterials(null);
		}
		this.isSearchResult = false;

		//generateCompleteListFiles();

		generateBreadcrumb();
	}

	public void generateCompleteListFiles(){

		listFilesInFolder = new List<Material>();

		List<Folder> subFolders = new List<Folder>();
		Folder parent;
		Set<String> ids = new Set<String>();
		subFolders.add(this.selectedFolder);
		if(Test.isRunningTest()){
			this.selectedFolder = new Folder();
			this.selectedFolder.id = '';
		}
		ids.add(this.selectedFolder.id);
		for(Folder subFolder : retrieveAllSubfolders()){
			Integer index = subFolders.indexOf(new Folder(subFolder.parentId));
			if(index >= 0){
				parent = subFolders.get(index);
				subFolder.name = parent.name + ' > ' + subFolder.name;
				subFolders.add(subFolder);
				ids.add(subFolder.id);
			}
		}

		if(!subFolders.isEmpty()){
			List<FolderWrapper> folderList = new List<FolderWrapper>();
			for(Folder folder : subFolders){
				folderList.add(new FolderWrapper(folder));
			}
			folderList.sort();
			subFolders = new List<Folder>();
			for(FolderWrapper wrapper : folderList){
				subFolders.add(wrapper.folder);
			}
		}

		List<video_upload__c> materials = [SELECT ID, Name, parentFolderID__c, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name, materialLanguage__c FROM video_upload__c WHERE parentFolderID__c in :ids AND objectType__c IN ('Material','Training') AND permissionLevelRequired__c <= :Integer.valueOf(currentUser.PermissionLevelAccessFolders__c) ORDER BY Name];

		Material mat;
		boolean isFolderEmpty = true;
		for(Folder folder : subFolders){
			if(Test.isRunningTest()){
				folder = new Folder();
				folder.id = '';
				folder.name = '';
			}
			mat = new Material();
			mat.id = folder.id;
			mat.name = folder.name;
			mat.trainingFolder = folder.trainingFolder;
			mat.type = 'folder';
			listFilesInFolder.add(mat);
			isFolderEmpty = true;
			for(video_upload__c material : materials){
				if(material.parentFolderID__c == folder.id){
					isFolderEmpty = false;
					mat = new Material();
					mat.name = material.name;
					mat.id = material.id;
					mat.type = 'material';
					mat.language = material.materialLanguage__c;
					mat.createdBy = material.CreatedBy.Name;
					mat.lastModifiedDate = material.LastModifiedDate;
					listFilesInFolder.add(mat);
				}
			}
			if(isFolderEmpty){
				listFilesInFolder.remove(listFilesInFolder.size()-1);
				/*mat = new Material();
				mat.name = 'Empty Folder';
				mat.type = 'empty';
				listFilesInFolder.add(mat);*/

			}
		}
		
		showModalFullListFiles = true;
	}

	public void closeModalFullListFiles(){
		showModalFullListFiles = false;
	}

	private void loadSubFolder(String id){
		this.selectedFolder = retrieveSubFolder(id, true);
		this.selectedFolder.subFolders = retrieveSubfolders(this.selectedFolder.id);
	}

	private void loadRootFolder(String id){
		this.selectedFolder = retrieveRootFolder(id);
		this.selectedFolder.subFolders = new List<Folder>();
		if(this.selectedFolder.parentId == null || this.selectedFolder.recordType.equals('Agency Group')){
			this.selectedFolder.subFolders.addAll(retrieveRootSubfolders(this.selectedFolder.id));
			folderGroup = selectedFolder;
		}
		this.selectedFolder.subFolders.addAll(retrieveSubfolders(this.selectedFolder.id));
	}

	public void openMaterial(){
		String idMaterial = ApexPages.currentPage().getParameters().get('idMaterial');
		Material material = loadFullMaterial(idMaterial);
		if(material == null){
			loadDefaultPage();
		}else{
			if(material.type.equals('Material')){
				materialSelected = (MaterialModule) material;		
			}else if(material.type.equals('Training')){		
				materialTrainingSelected = (TrainingMaterial) material;
				materialTrainingSelected.init();
			}
		}
	}

	public void deleteFolder(){
		String idFolder = ApexPages.currentPage().getParameters().get('idFolder');
		if(!idFolder.startsWith('001')){
			Folder f = retrieveSubFolder(idFolder, false);
			if(!Test.isRunningTest()){
				String parentId = f.parentId;
				super.deleteFolder(idFolder);
				Apexpages.currentPage().getParameters().put('idFolder', parentId);

				loadFolderMaterials();	
			}
		}
	}

	public void openModuleTraining(){
		String idMaterial = ApexPages.currentPage().getParameters().get('idMaterial');
		materialTrainingSelected.openModule(idMaterial);
	}

	public void closeMaterial(){
		materialSelected = null;
		materialTrainingSelected = null;
		this.changeFolderForMaterial = false;
	}

	public void searchMaterials(){
		this.materials = searchMaterials(nameDescriptionSearch, languageSearch);
	}

	public void openNewMaterial(){
		this.changeFolderForMaterial = false;
		this.editingMaterial = false;
		this.newMaterial = new video_upload__c();
		this.newMaterial.materialLanguage__c = IPFunctions.getLanguage(IPFunctions.getLocaleID(currentUser.Contact.Account.BillingCountry));
		this.selectedFolderToSaveMaterial = this.selectedFolder;
		this.maxVisibilitySaveEditFolder = this.selectedFolder.permission;
		//this.newSubfolder = new video_upload__c();
		/*if(this.selectedFolder != null){
		}else{
			this.selectedFolderToSaveMaterial = getRootFolder(currentUser.Contact.Account.ParentId);
		}*/
		//breadCrumbSaveMaterial = breadCrumb;
	}

	public void openMaterialForEdition(){
		editingMaterial = true;
		this.changeFolderForMaterial = false;
		String idMaterial = ApexPages.currentPage().getParameters().get('idMaterial');
		newMaterial = [SELECT ID, Name, Description__c, materialLanguage__c, permissionLevelRequired__c, parentFolderID__c, parentFolderName__c, previewLink__c, materialLinks__c, Is_Training_Subfolder__c, Available_Only_To_Group__c FROM video_upload__c WHERE ID = :idMaterial];
		this.selectedFolderToSaveMaterial = new Folder(newMaterial.parentFolderID__c, newMaterial.parentFolderName__c, null, null, newMaterial.permissionLevelRequired__c, getLabelPermissionAccess(newMaterial.permissionLevelRequired__c), checkFolderCanBeModified(newMaterial.id), newMaterial.Is_Training_Subfolder__c, newMaterial.Available_Only_To_Group__c);
		//breadCrumbSaveMaterial = breadCrumb;
	}

	public void openMaterialForEditionFromBoxPage(){
		openMaterialForEdition();
		closeMaterial();
	}

	public void openEditFolder(){
		this.newFolder = new Folder();
		this.newFolder.id = this.selectedFolder.id;
		this.newFolder.name = this.selectedFolder.name;
		this.newFolder.parentId = this.selectedFolder.parentId;
		this.newFolder.permission = this.selectedFolder.permission;
		this.newFolder.trainingFolder = this.selectedFolder.trainingFolder;
		this.newFolder.availableOnlyToGroup = this.selectedFolder.availableOnlyToGroup;
		this.saveFolderForMaterial = false;
		this.editingFolder = true;
	}

	public void openNewFolder(){
		this.newFolder = new Folder();
		this.newFolder.parentId = this.selectedFolder.id;
		this.saveFolderForMaterial = false;
		this.editingFolder = false;
		this.maxVisibilitySaveEditFolder = this.selectedFolder.permission;
	}

	public void openNewFolderToSaveMaterial(){
		this.newFolder = new Folder();
		this.newFolder.parentId = this.selectedFolder.id;
		this.saveFolderForMaterial = true;
		this.editingFolder = false;
		this.maxVisibilitySaveEditFolder = this.selectedFolder.permission;
	}

	public void cancelSavingMaterial(){
		this.newFolder = null;
		this.saveFolderForMaterial = false;
	}

	public void editFolder(){
		video_upload__c newSubfolder = new video_upload__c();
		newSubfolder.parentFolderID__c = newFolder.parentId;
		newSubfolder.name = newFolder.name;
		newSubfolder.Is_Training_Subfolder__c = newFolder.trainingFolder;
		newSubfolder.permissionLevelRequired__c = newFolder.permission;
		newSubfolder.Available_Only_To_Group__c = newFolder.availableOnlyToGroup;
		if(!Test.isRunningTest()){
			
			newSubfolder.id = newFolder.id;
			save(newSubfolder);

			Apexpages.currentPage().getParameters().put('idFolder', newSubfolder.id);

			loadFolderMaterials();
		}

		this.newFolder = null;
	}

	public void saveNewFolder(){
		video_upload__c newSubfolder = new video_upload__c();
		
		newSubfolder.objectType__c = 'Materials Folder';
		newSubfolder.parentFolderID__c = newFolder.parentId;
		newSubfolder.name = newFolder.name;
		newSubfolder.Is_Training_Subfolder__c = newFolder.trainingFolder;
		newSubfolder.permissionLevelRequired__c = newFolder.permission;
		newSubfolder.Available_Only_To_Group__c = newFolder.availableOnlyToGroup;
		newSubfolder = save(newSubfolder);

		if(!Test.isRunningTest()){
			Apexpages.currentPage().getParameters().put('idFolder', newSubfolder.id);

			loadFolderMaterials();
		}

		if(this.saveFolderForMaterial){
			this.selectedFolderToSaveMaterial = this.selectedFolder;
			if(this.selectedFolderToSaveMaterial != null){
				this.maxVisibilitySaveEditFolder = this.selectedFolderToSaveMaterial.permission;
			}
		}

		this.newFolder = null;
	}

	public void saveMaterial(){
		newMaterial.parentFolderID__c = selectedFolderToSaveMaterial.id;
		newMaterial.parentFolderName__c = selectedFolderToSaveMaterial.name;
		
		if(!editingMaterial){
			newMaterial.objectType__c = 'Material';
			newMaterial.materialAgencyGroup__c = /*this.breadCrumb.get(1).id;*/currentUser.Contact.Account.ParentId;
		}	
		newMaterial = save(newMaterial);
		this.folderS3Material = globalLink+'/MATERIALS/' + newMaterial.id;

		Apexpages.currentPage().getParameters().put('idFolder', selectedFolderToSaveMaterial.id);

		loadFolderMaterials();

		/*if(newSubfolder != null && newSubfolder.name != null){
			newSubfolder = save(newSubfolder);
			newMaterial.parentFolderID__c = newSubfolder.id;
			newMaterial.parentFolderName__c = newSubfolder.name;
		}else{*/
		//}
	}

	public void openChangeFolderToSaveMaterial(){
		this.changeFolderForMaterial = true;
	}

	public void cancelFolderToSaveMaterial(){
		this.changeFolderForMaterial = false;
	}

	public void changeFolderToSaveMaterial(){
		if(this.selectedFolderToSaveMaterial.id.startsWith('001')){
				this.selectedFolderToSaveMaterial = retrieveRootFolder(this.selectedFolderToSaveMaterial.id);
			}else{
				this.selectedFolderToSaveMaterial = retrieveSubFolder(this.selectedFolderToSaveMaterial.id, false);
			}
		this.changeFolderForMaterial = false;
		if(this.selectedFolderToSaveMaterial != null){
			this.maxVisibilitySaveEditFolder = this.selectedFolderToSaveMaterial.permission;
		}
	}

	public boolean getPermissionToCreate(){
		return currentUser.PermissionCreateFoldersPackages__c && isWithinOwnGroup();
	}

	public boolean isWithinOwnGroup(){
		if(selectedFolder == null){
			return false;
		}else{
			//system.debug('THE FOLDER GROUP '+folderGroup);
			return folderGroup != null && folderGroup.id == currentUser.Contact.Account.ParentId;
		}
	}

	public void deleteMaterial(){
		String idMaterial = ApexPages.currentPage().getParameters().get('idMaterial');
		try{
			video_upload__c material = [SELECT ID, previewLink__c FROM video_upload__c WHERE ID = :idMaterial];

			if(material.previewLink__c != null && !''.equals(material.previewLink__c)){
				String folder = 'MATERIALS/' + idMaterial;
				//String jsonFiles = FileUpload.listFiles(bucketName, folder, false, null, null, true);	
				String jsonFiles = FileUpload.listFiles(bucketName, folder, false, null, null); 	
				List<AWSHelper.File> files = (List<AWSHelper.File>) JSON.deserialize(jsonFiles, List<AWSHelper.File>.class);
				AWSKeys credentials = new AWSKeys(IPFunctions.awsCredentialName);
				for(AWSHelper.File file : files){
					AWSHelper.deleteObjectFromS3(credentials.key, credentials.secret, bucketName, folder, file.fullname);
				}
			}
			super.deleteIPCloudMaterial(idMaterial);
			Apexpages.currentPage().getParameters().put('idFolder', selectedFolder.id);
			loadFolderMaterials();
			
		}catch(Exception e){
			system.debug('Error at deleting ===> ' + e);	
		}
	} 

	@RemoteAction
	public static String saveTrainingPackage(String packageParentId, String packageParentName, String packageDescription, String packageName, 
										   String packageEstimatedHours, String packageEstimatedMinutes, String packageMaterialsIds, String packageId,
										   String packageVisibility, String newSubfolderName, String newSubfolderVisibility, String packageMaterialsLanguages){		
		User usr = [Select Id, Contact.Account.ParentId from User where id = :UserInfo.getUserId()];
		video_upload__c newPackage = new video_upload__c();		
		if(IPFunctions.isValidObject(packageId)){
			newPackage.id = packageId;
		}
		if(IPFunctions.isValidObject(newSubfolderName)){
			video_upload__c subfolder = new video_upload__c();
			subfolder.name = newSubfolderName;
			subfolder.parentFolderID__c = packageParentId;
			subfolder.parentFolderName__c = packageParentName;
			subfolder.permissionLevelRequired__c = Integer.valueOf(newSubfolderVisibility);	
			subfolder.objectType__c = 'Materials Folder';
			upsert subfolder;

			newPackage.parentFolderID__c = subfolder.id;
		    newPackage.parentFolderName__c = subfolder.name;
		}else{
			newPackage.parentFolderID__c = packageParentId;
			newPackage.parentFolderName__c = packageParentName;
		}

		newPackage.objectType__c = 'Training';
		newPackage.Name = packageName;
		newPackage.permissionLevelRequired__c = Integer.valueOf(packageVisibility);
		newPackage.Description__c = packageDescription;
		newPackage.materialAgencyGroup__c = usr.Contact.Account.ParentId;
		newPackage.trainingMaterialsRelated__c = packageMaterialsIds;
		String languages = '';
		for(String language : packageMaterialsLanguages.split(';;')){
			if(language != null && !''.equals(language) && !languages.contains(language)){
				languages = language + ';;' + languages;		
			}
		}
		newPackage.trainingLanguages__c = languages;
		newPackage.trainingEstimatedHours__c = Decimal.valueOf(packageEstimatedHours);
		newPackage.trainingEstimatedMinutes__c = Decimal.valueOf(packageEstimatedMinutes);

		upsert newPackage;

		String currentEnvironment = IPFunctions.getCurrentEnvironment();
		
		if(currentEnvironment == 'production')
			return 'https://educationhify.force.com/ip/apex/agency_materials?id='+newPackage.ID;
		else if(currentEnvironment == 'devpartial')
			return 'https://devpartial-educationhify.cs5.force.com/ip/apex/agency_materials?id='+newPackage.ID;
		else
			return 'https://devfull-educationhify.cs57.force.com/ip/apex/agency_materials?id='+newPackage.ID;
	}

	@RemoteAction
	public static String openPackageForEdition(String id){
		TrainingMaterial material = TrainingMaterialsHelper.loadPackageForEdition(id);
		return JSON.serialize(material);
	}

	@RemoteAction
	public static String searchMaterialsForTraining(String materialName){
		materialName = '%' + materialName + '%';
		List<video_upload__c> materials = [SELECT ID, Name, materialLanguage__c, parentFolderName__c, materialAgencyGroup__r.Name FROM video_upload__c WHERE objectType__c = 'Material' AND Name like :materialName ORDER BY LastModifiedDate];
		return JSON.serialize(materials);
	}

	@RemoteAction
	public static String searchMaterialsForTrainingByAgency(String idAgency){
		List<video_upload__c> materials = [SELECT ID, Name, materialLanguage__c, parentFolderName__c, materialAgencyGroup__r.Name FROM video_upload__c WHERE objectType__c = 'Material' AND materialAgencyGroup__c = :idAgency ORDER BY LastModifiedDate];
		return JSON.serialize(materials);
	}

	@RemoteAction
	public static String createNewComment(String idMaterial, String comment){
		User user = [Select Id, Name, Contact.Account.Name from User where Id = :UserInfo.getUserId()];	
		
		IPFunctions.noteWrapper comm = new IPFunctions.noteWrapper(user.Id, comment, user.Name, user.Contact.Account.Name, Datetime.now().format('dd/MM/yyyy HH:mm')); 
		String jsonComment = JSON.serialize(comm);
		
		video_upload__c material = [Select Id, Name, materialComments__c, CreatedBy.ID, CreatedBy.Name, CreatedBy.IsActive FROM video_upload__c WHERE Id = :idMaterial];
		if(IPFunctions.isValidObject(material.materialComments__c)){
			material.materialComments__c = jsonComment + ';!;' + material.materialComments__c;
		}else{
			material.materialComments__c = jsonComment;
		}
		if(user.ID != material.CreatedBy.ID && material.CreatedBy.IsActive){			
			Custom_Note_Task__c newTask = new Custom_Note_Task__c();
			newTask.Subject__c = 'Follow Up';
			newTask.Comments__c = user.Name+' sent a new comment on your material:\n\n "'+comment+'"';
			newTask.isNote__c = false;
			newTask.Due_Date__c = Date.today().addDays(1);
			newTask.Status__c ='In Progress';
			newTask.AssignedOn__c = DateTime.now();
			newTask.AssignedBy__c = user.Id;
			newTask.Assign_To__c = material.CreatedBy.ID;
			newTask.Priority__c = '1 - Medium';
			insert newTask;
		}

		update material;
		return jsonComment;
	}

	public List<SelectOption> getAllFolders(){
		List<Folder> result = new List<Folder>();
		Folder rootFolder = retrieveRootFolder(currentUser.Contact.Account.ParentId);
		List<Folder> subFolders = retrieveAllSubfolders();
		Folder parent;
		result.add(rootFolder);
		for(Folder subFolder : subFolders){
			//if(!editingFolder || (newFolder.id != subFolder.id)){
				Integer index = result.indexOf(new Folder(subFolder.parentId));
				if(index >= 0){
					parent = result.get(index);
					subFolder.name = parent.name + ' > ' + subFolder.name;
					result.add(subFolder);
				}
			//}
			
		}	

		List<SelectOption> options = new List<SelectOption>();
		for(Folder folder : result){
			options.add(new SelectOption(folder.id, folder.name));
		}
		return options;	
	}
	
}