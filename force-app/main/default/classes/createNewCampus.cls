public with sharing class createNewCampus {

	public String rollbackPage{get;set;}
	public String idRedirect{get;set;}

	public Account campus{get;set;}
	public Account schoolSelected{get;set;}

	public boolean sameAddress{get{if(sameAddress == null) sameAddress = false; return sameAddress;} set;}
	public boolean schoolHasMainCampus{get{if(schoolHasMainCampus == null) schoolHasMainCampus = false; return schoolHasMainCampus;} set;}
	public String mainCampus{get;set;}

	public list<SelectOption> allCountries {get;set;}

	public String pathways{get;set;}
	public String nearTo{get;set;}
	public String whyChooseThisCampus{get;set;}
	public String features{get;set;}
	public String nationalities{get;set;}
	public String socialNetworksToSave{get;set;}
	public String videos{get;set;}

	public String schoolCampusCategory{get;set;}

	public List<String> savedNearTo{get;set;}
	public List<String> savedWhyChooseThisCampus{get;set;}
	public List<String> savedFeatures{get;set;}
	public List<IPClasses.SavedItemCampus> savedNationalities{get;set;}
	public List<IPClasses.SavedItemCampus> savedSocialNetworks{get;set;}
	public List<IPClasses.SavedItemCampus> savedVideos{get;set;}
	public List<IPClasses.SavedItemCampus> savedPathways{get;set;}
	
	public createNewCampus() {
		rollbackPage = ApexPages.currentPage().getParameters().get('page');
		idRedirect = ApexPages.currentPage().getParameters().get('idRedirect');
		String idCampus = ApexPages.currentPage().getParameters().get('campus');

		if(!String.isEmpty(idCampus)){
			loadCampusForUpdate(idCampus);
		}else{
			Map<string, string> recordTypes = new Map<string, string>();
			for(RecordType rt:[Select id, name from RecordType where name in ('School','Campus','School Group') and isActive = true]){
				recordTypes.put(rt.name, rt.id);
			}
			campus = new Account(recordtypeid = recordTypes.get('Campus'));
			String school = ApexPages.currentPage().getParameters().get('school');	
			if(!String.isEmpty(school)){
				try{
					schoolSelected = [SELECT ID, Name FROM Account WHERE ID = :school AND RecordType.Name = 'School'];
					campus.ParentId = schoolSelected.ID;
					for(Account campusSaved : [SELECT ID, Name, Main_Campus__c FROM Account WHERE Parent.ID = :schoolSelected.ID AND RecordType.Name = 'Campus']){
						if(campusSaved.Main_Campus__c){
							schoolHasMainCampus = true;
							mainCampus = campusSaved.Name;
							break;
						}
					}
				}catch(Exception e){
					system.debug('No School Found <=== '+e.getMessage());
				}
			}
		}

		allCountries = new list<SelectOption>(IPFunctions.getAllCountries());
	}

	public void loadCampusForUpdate(String idCampus){
		campus = [SELECT ID, Name, Parent.ID, Website, BusinessEmail__c, Pathways_URL__c, Account_Currency_Iso_Code__c, Main_Campus__c, Disabled_Campus__c, ShowCaseOnly__c, Students_per_class__c, Total_Number_of_Students__c, Phone, Registration_name__c, Type_of_Business_Number__c, Business_Number__c, BillingCountry, BillingState, BillingCity, BillingStreet, BillingPostalCode, ShippingCountry, ShippingState, ShippingCity, School_campus_category__c, Reason_Disabled_Campus__c, ShippingStreet, ShippingPostalCode, General_Description__c, Videos__c, Social_network_url__c, School_Pathways__c, Nationality_Mix__c, Near_to__c, Why_choose_this_campus__c, Features__c FROM Account WHERE ID = :idCampus AND RecordType.Name = 'Campus'];

		schoolSelected = [SELECT ID, Name FROM Account WHERE ID = :campus.Parent.ID AND RecordType.Name = 'School'];
		for(Account campusSaved : [SELECT ID, Name, Main_Campus__c FROM Account WHERE Parent.ID = :campus.Parent.ID AND RecordType.Name = 'Campus']){
			if(campusSaved.Main_Campus__c && campusSaved.ID != campus.ID){
				schoolHasMainCampus = true;
				mainCampus = campusSaved.Name;
				break;
			}
		}

		loadExtraItens();

		this.schoolCampusCategory = campus.School_campus_category__c;
	}

	public void loadExtraItens(){
		if(!String.isEmpty(campus.Near_to__c)){
			savedNearTo = new List<String>();
			for(String value : campus.Near_to__c.split('!#!')){
				if(!String.isEmpty(value)){
					savedNearTo.add(value);
				}
			}
		}
		if(!String.isEmpty(campus.Why_choose_this_campus__c)){
			savedWhyChooseThisCampus = new List<String>();
			for(String value : campus.Why_choose_this_campus__c.split('!#!')){
				if(!String.isEmpty(value)){
					savedWhyChooseThisCampus.add(value);
				}
			}
		}
		if(!String.isEmpty(campus.Features__c)){
			savedFeatures = new List<String>();
			for(String value : campus.Features__c.split('!#!')){
				if(!String.isEmpty(value)){
					savedFeatures.add(value);
				}
			}
		}
		if(!String.isEmpty(campus.Nationality_Mix__c)){
			savedNationalities = new List<IPClasses.SavedItemCampus>();
			IPClasses.SavedItemCampus item;
			for(String value : campus.Nationality_Mix__c.split('!#!')){
				if(!String.isEmpty(value)){
					item = new IPClasses.SavedItemCampus();
					item.item = value.split('>')[0];
					item.value = value.split('>')[1];
					savedNationalities.add(item);
				}
			}
		}
		if(!String.isEmpty(campus.Videos__c)){
			savedVideos = new List<IPClasses.SavedItemCampus>();
			IPClasses.SavedItemCampus item;
			for(String value : campus.Videos__c.split(';')){
				if(!String.isEmpty(value)){
					item = new IPClasses.SavedItemCampus();
					item.item = value.split('>')[0];
					item.value = value.split('>')[1];
					savedVideos.add(item);
				}
			}
		}
		if(!String.isEmpty(campus.Social_network_url__c)){
			savedSocialNetworks = new List<IPClasses.SavedItemCampus>();
			IPClasses.SavedItemCampus item;
			for(String value : campus.Social_network_url__c.split(';')){
				if(!String.isEmpty(value)){
					item = new IPClasses.SavedItemCampus();
					item.item = value.split('>')[0];
					item.value = value.split('>')[1];
					savedSocialNetworks.add(item);
				}
			}
		}
		if(!String.isEmpty(campus.School_Pathways__c)){
			savedPathways = (List<IPClasses.SavedItemCampus>) JSON.deserialize(campus.School_Pathways__c, List<IPClasses.SavedItemCampus>.class);
		}

	}

	public PageReference saveCampus(){
		PageReference redirect;
		
		generateNearTo();
		generateWhychoose();
		generateFeatures();
		generateNationalities();
		generateSocialNetworks();
		generateVideos();
		generatePathways();

		checkAddress();

		chcekMainCampus();

		campus.School_campus_category__c = this.schoolCampusCategory;

		try{
			upsert campus;

			redirect = Page.school_campus_page;
			redirect.getParameters().put('id', campus.id);
			redirect.setRedirect(true);
		}catch(Exception e){
           // ApexPages.addMessages(e);
		   system.debug('Error on saveCampus() ===> ' + e.getLineNumber() + ' <=== '+e.getMessage());
        }
		return redirect;
	}

	public void chcekMainCampus(){
		if(campus.Main_Campus__c){
			List<Account> campuses = [SELECT ID, Name, Main_Campus__c FROM Account WHERE Parent.ID = :schoolSelected.ID AND RecordType.Name = 'Campus'];
			for(Account campusSaved : campuses){
				campusSaved.Main_Campus__c = false;
			}
			update campuses;
		}
	}

	public void checkAddress(){
		if(this.sameAddress){
			campus.ShippingCountry = campus.BillingCountry;
			campus.ShippingState = campus.BillingState;
			campus.ShippingCity = campus.BillingCity;
			campus.ShippingStreet = campus.BillingStreet;
			campus.ShippingPostalCode = campus.BillingPostalCode;
		}
	}

	public void updateFieldDisabledReason(){
		this.schoolCampusCategory = ApexPages.currentPage().getParameters().get('schoolCampusCategory');
	}

	public void generatePathways(){
		if(!String.isEmpty(pathways)){
			List<IPClasses.SavedItemCampus> toSave = new List<IPClasses.SavedItemCampus>(); 
			for(IPClasses.SavedItemCampus pathway : (List<IPClasses.SavedItemCampus>) JSON.deserialize(pathways, List<IPClasses.SavedItemCampus>.class)){
				if(pathway.value.startsWith('http://') || !pathway.value.startsWith('https://')){
					pathway.value = 'https://' + pathway.value.removeStart('http://'); 
				}
				toSave.add(pathway);
			}		
			campus.School_Pathways__c = JSON.serialize(toSave);
		}
	}
	
	public void generateVideos(){
		if(!String.isEmpty(videos)){
			List<IPClasses.SavedItemCampus> videosList = (List<IPClasses.SavedItemCampus>) JSON.deserialize(videos, List<IPClasses.SavedItemCampus>.class);
			videos = '';
			String nameVideo;
			if(videosList != null && !videosList.isEmpty()){
				for(IPClasses.SavedItemCampus video : videosList){
					nameVideo = video.value;
					if(video.value.startsWith('http://') || !video.value.startsWith('https://')){
						nameVideo = 'https://' + video.value.removeStart('http://'); 
					}
					videos = videos + ';' + video.item + '>' + nameVideo;
				}
				videos = videos.removeStart(';');
				campus.Videos__c = videos;
			}
		}
	}

	public void generateSocialNetworks(){
		if(!String.isEmpty(socialNetworksToSave)){
			List<IPClasses.SavedItemCampus> socialNetworksToSaveList = (List<IPClasses.SavedItemCampus>) JSON.deserialize(socialNetworksToSave, List<IPClasses.SavedItemCampus>.class);
			socialNetworksToSave = '';
			String url;
			if(socialNetworksToSaveList != null && !socialNetworksToSaveList.isEmpty()){
				for(IPClasses.SavedItemCampus nationality : socialNetworksToSaveList){
					if(nationality.value.startsWith('http://') || !nationality.value.startsWith('https://')){
						nationality.value = 'https://' + nationality.value.removeStart('http://'); 
					}
					socialNetworksToSave = socialNetworksToSave + ';' + nationality.item + '>' + nationality.value;
				}
				socialNetworksToSave = socialNetworksToSave.removeStart(';');
				campus.Social_network_url__c = socialNetworksToSave;
			}
		}
	}

	public void generateNationalities(){
		if(!String.isEmpty(nationalities)){
			List<IPClasses.SavedItemCampus> nationalitiesList = (List<IPClasses.SavedItemCampus>) JSON.deserialize(nationalities, List<IPClasses.SavedItemCampus>.class);
			nationalities = '';
			if(nationalitiesList != null && !nationalitiesList.isEmpty()){
				for(IPClasses.SavedItemCampus nationality : nationalitiesList){
					nationalities = nationalities + '!#!' + nationality.item + '>' + nationality.value;
				}
				nationalities = nationalities.removeStart('!#!');
				campus.Nationality_Mix__c = nationalities; 
			}
		}
	}

	public void generateNearTo(){
		if(!String.isEmpty(nearTo)){
			List<String> nearToList = (List<String>) JSON.deserialize(nearTo, List<String>.class);
			nearTo = '';
			if(nearToList != null && !nearToList.isEmpty()){
				for(String item : nearToList){
					nearTo = nearTo + '!#!' + item;
				}
				nearTo = nearTo.removeStart('!#!');
				campus.Near_to__c = nearTo;
			}
		}
	}

	public void generateWhychoose(){
		if(!String.isEmpty(whyChooseThisCampus)){
			List<String> whyChooseThisCampusList = (List<String>) JSON.deserialize(whyChooseThisCampus, List<String>.class);
			whyChooseThisCampus = '';
			if(whyChooseThisCampusList != null && !whyChooseThisCampusList.isEmpty()){
				for(String item : whyChooseThisCampusList){
					whyChooseThisCampus = whyChooseThisCampus + '!#!' + item;
				}
				whyChooseThisCampus = whyChooseThisCampus.removeStart('!#!');
				campus.Why_choose_this_campus__c = whyChooseThisCampus;
			}
		}
	}

	public void generateFeatures(){
		if(!String.isEmpty(features)){
			List<String> featuresList = (List<String>) JSON.deserialize(features, List<String>.class);
			features = '';
			if(featuresList != null && !featuresList.isEmpty()){
				for(String item : featuresList){
					features = features + '!#!' + item;
				}
				features = features.removeStart('!#!');
				campus.Features__c = features;
			}
		}
	}

	public List<SelectOption> getCurrencies() {
	    List<SelectOption> options = new List<SelectOption>();
	    for(SelectOption op: xCourseSearchFunctions.getCurrencies())
	      options.add(new SelectOption(op.getValue(),op.getLabel()));
	        
	    return options;
	}

	public List<SelectOption> getSocialNetworks() {
	    List<SelectOption> options = new List<SelectOption>();
	    Schema.DescribeFieldResult F = Account.fields.Social_network__c.getDescribe();
		List<Schema.PicklistEntry> P = F.getPicklistValues();
		for (Schema.PicklistEntry lst:P)
			options.add(new SelectOption(lst.getValue(),lst.getLabel())); 
	    return options;
	}

	public List<SelectOption> getSchoolCampusCategories(){
		List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Schema.Account.School_campus_category__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple){
			options.add(new SelectOption( f.getValue(), f.getLabel() ));
		}
		return options;
	}

	public void refreshAddress(){
		this.sameAddress = !this.sameAddress;
	}

}