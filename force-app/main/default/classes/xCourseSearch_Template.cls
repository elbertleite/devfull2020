public class xCourseSearch_Template{

	public list<Search_Courses__c> listCourses{get; Set;}
	private Web_Search__c userTemplates;
	
	public xCourseSearch_Template(ApexPages.StandardController controller){
		
		listCourses = new list<Search_Courses__c>([Select id, Campus_Course__r.Campus__r.Name, Campus_Course__r.Course__r.Name, Campus_Course__c, Client__c, Course_Deleted__c, Custom_Campus__c, Custom_City__c, Custom_Country__c, Custom_Course__c,
													Custom_Currency__c, Custom_NumberOfUnits__c, Custom_PriceOption__c, Custom_School__c, Custom_StartDate__c, Custom_Timetable__c, Custom_Unit__c, Custom_Value__c, Custom_Website__c, Free_Units__c, 
													isCustomCourse__c, isTemplate__c, Nationality__c, UseAsCustomTemplate__c, Number_of_Units__c, Optional_Fee_Ids__c, Optional_Fee_Related_Ids__c, Payment_Date__c, Price_per_Unit__c, Product_Ids__c, 
													Required_Fee_Changeable__c, Name, Selected__c, ConnectionSentId, Start_Date__c, Total_Course__c, Total_Products__c, Unit_Type__c, Course_Order__c, Web_Search__c, End_Date__c, Total_Tuition__c, 
													(Select Category__c, Currency__c, Description__c, isCustom__c, Name__c, Price__c, Quantity__c, Quotation_Products_Services__c, Id, Search_Course__c, Selected__c, Total__c, Unit_Description__c from Search_Course_Products__r),
													(Select Custom_Fee_Name__c, Extra_Fee_Interval__c, Fee_Value__c, Number_Units__c from Search_Course_Extra_Fees__r )
													from Search_Courses__c WHERE Web_Search__c = :controller.getId() AND Course_Deleted__c = false order by Course_Order__c NULLS LAST, CreatedDate]);
		getUserDetails();
		
		
		agencyShared = UserDetails.Agency_Id;
		
	}
	
	private xCourseSearchPersonDetails.UserDetails UserDetails;
	public xCourseSearchPersonDetails.UserDetails getUserDetails(){
		if(UserDetails == null){
			xCourseSearchPersonDetails spd = new xCourseSearchPersonDetails();
			UserDetails = spd.getAgencyDetails();
		}
		return UserDetails;
	}
	
	private string agencyShared;
	
	private Web_Search__c getUserTemplate(){
		if(userTemplates == null){
			Try{
				userTemplates = [Select id from Web_Search__c where createdById = :UserInfo.getUserId() and WS_Template__c = true];	
			}catch(Exception e){
				userTemplates = new Web_Search__c(WS_Template__c = true);
				insert userTemplates;
			}
		}
		return 	userTemplates;
	}
	
	
	public Search_Course_Template__c newTemplate{get{if(newTemplate == null) newTemplate = new Search_Course_Template__c(); return newTemplate;} Set;}
	
	public PageReference saveTemplate(){
		
		if(newTemplate.Name__c == null || newTemplate.Name__c == ''){
			newTemplate.Name__c.addError('Required field');
			return null;
		}
			
		
		
		list<Search_Courses__c> newtemplateCourses = new list<Search_Courses__c>();
		list<Search_Course_Product__c> newtemplateProducts = new list<Search_Course_Product__c>();
		list<Search_Course_Extra_Fee__c> newtemplateFees = new list<Search_Course_Extra_Fee__c>();
		
		Search_Courses__c newCourse;
		Search_Course_Product__c newProduct;
		Search_Course_Extra_Fee__c customFee;
		
		getUserTemplate();
		
		list<string> coursesId = new list<String>();
		newTemplate.Courses_Details__c = '<span style="text-decoration: underline; color:#FFFF00;">'+newTemplate.Name__c.toUpperCase()+'</span> <br /> <br />';
		newTemplate.Agency_Shared__c = agencyShared;
		for(Search_Courses__c sc:listCourses){
			//coursesId.add(sc.id);
			newCourse = sc.clone(false, true);
			newCourse.isTemplate__c = true;
			newCourse.externalId__c = sc.id;
			newCourse.Web_Search__c = userTemplates.id;
			newCourse.UseAsCustomTemplate__c = false;
			
			if(!newCourse.isCustomCourse__c)
				newTemplate.Courses_Details__c += '<span style="padding: 3px 0; display:block;"><span style="color: #00FF7F">'+ newCourse.Campus_Course__r.Campus__r.Name + '</span> - ' + newCourse.Campus_Course__r.Course__r.Name + '</span>';
			else newTemplate.Courses_Details__c += '<span style="padding: 3px 0; display:block;"><span style="color: #00FF7F">'+ newCourse.Custom_Campus__c + '</span> - ' + newCourse.Custom_Course__c + ' (Custom Course)</span>';
			
			newtemplateCourses.add(newCourse);
			
			
			
		}
		
		Database.SaveResult[] MySaveResult = Database.insert(newtemplateCourses);
		for(Database.SaveResult sr:MySaveResult)
            coursesId.add(sr.getId());
		
		Map<String, string> coursesRelationship = new Map<String, string>();
		for(Search_Courses__c sc:[Select Id, externalId__c from Search_Courses__c where Id in :coursesId])
			coursesRelationship.put(sc.externalId__c, sc.id);
		for(Search_Courses__c sc:listCourses){
			
			for(Search_Course_Product__c sp:sc.Search_Course_Products__r){
				newProduct = sp.clone(false, true);
				newProduct.Search_Course__c =  coursesRelationship.get(sc.id);
				newProduct.Web_Search__c =  userTemplates.id;
				newtemplateProducts.add(newProduct);
			}
			
			for(Search_Course_Extra_Fee__c fee : sc.Search_Course_Extra_Fees__r){
				customFee = fee.clone(false, true);
				customFee.Search_Course__c = coursesRelationship.get(sc.id);
				newtemplateFees.add(customFee);
			}
		}
		
		insert newtemplateProducts;
		insert newtemplateFees;
		
		newTemplate.courses__c = String.join(coursesId, ',');
		insert newTemplate;
		
		
		return null;
		
	}
	

}