public with sharing class CashDepositController {
	
	public list<SelectOption> agencies {get;set;}
	public list<SelectOption> banks {get;set;}
	public decimal totalCash {get;set;}
	public string selectedAgency {get;set;}
	public string selectedBank {get;set;}
	
	public Bank_Deposit__c bankDeposit {get;set;}
	public list<client_course_instalment_payment__c> lCash {get;set;}

	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}
	
	private User currentUser {get;set;}
	public CashDepositController(){
		currentUser = [Select Id, Name, Aditional_Agency_Managment__c, Contact.AccountId, Contact.Account.ParentId, Contact.Department__c from User where id = :UserInfo.getUserId() limit 1];
		banks = new List<SelectOption>();
		selectedAgency = currentUser.Contact.AccountId;
		agencies = findAgencies();
		findCashToDeposit();
		refreshAgencyBanks();
		
		bankDeposit = new Bank_Deposit__c();
	}
	
	
	/** Find all confirmed cash **/
	public void findCashToDeposit(){
		lCash = new list<client_course_instalment_payment__c>();
		totalCash = 0;
		
		String DatefromDate;
		String DatetoDate;
		if(fromDate.Expected_Travel_Date__c != null)
			DatefromDate =  IPFunctions.FormatSqlDateIni(fromDate.Expected_Travel_Date__c);
		
		if(toDate.Expected_Travel_Date__c != null)
			DatetoDate = IPFunctions.FormatSqlDateFin(toDate.Expected_Travel_Date__c);  
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && currentUser.Aditional_Agency_Managment__c == null) //set the user to see only his own agency
			selectedAgency = currentUser.Contact.AccountId;
		
		String sql = ' SELECT Id,Bank_Deposit__c, selected__c, Value__c, Date_Paid__c, Confirmed_By__r.Name, Confirmed_Date__c, Client__c, Client__r.Name, Client__r.Owner__r.Name, ' +
						' Deposit__c, Received_By__r.Name, Received_On__c, Payment_Type__c, ' +
						
						' client_course_instalment__c, '+
						' client_course_instalment__r.Name, '+
						' client_course_instalment__r.client_course__r.Campus_Name__c, '+
						' client_course_instalment__r.client_course__r.Course_Name__c, '+
						
						' Invoice__c, ' +
						' Invoice__r.Name, ' +
						
						' client_product_service__c, '+
						' client_product_service__r.Name, '+
						' client_product_service__r.Product_Name__c  '+
						
						 ' FROM client_course_instalment_payment__c ' +
							 'WHERE Received_By_Agency__c = :selectedAgency  ' ;
								
		
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
		 	sql+= ' AND Received_By_Agency__r.ParentId = \'' +selectedAgencyGroup +'\'';
		 	
		sql+=  'AND Bank_Deposit__c = null  AND Payment_Type__c IN (\'Cash\', \'Cheque\') AND Confirmed_Date__c != null   ';
		
		if(fromDate.Expected_Travel_Date__c!= null)
			sql += ' and Date_Paid__c >= ' + DatefromDate;
		if(toDate.Expected_Travel_Date__c!= null)
			sql += ' and Date_Paid__c <= ' + DatetoDate;
		

		sql += ' order by Confirmed_Date__c ';
					 
		lCash = Database.query(sql);
		
		for(client_course_instalment_payment__c cci : lCash)
			totalCash += cci.Value__c;
			
		refreshAgencyBanks();
	}
	/** END -- Find all confirmed cash **/
	
	
	
	
	
	
	
	/** Confirm Deposit **/
	public PageReference confirmDeposit(){
		boolean hasError = false;
		String errors = '<ul>';
		if(selectedBank == null || selectedBank == ''){
			errors = '<li>Please select a bank to deposit.</li>';
			hasError = true;
		}
		if(bankDeposit.Deposit_Date__c == null){
			hasError = true;
			errors += '<li>Please fill the deposit date.</li>';
		}
		errors = '</ul>';
		
		if(hasError){
			
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errors));
			return null;
		}
		else {
			SavePoint sp;
			
			try{
				sp = Database.setSavepoint();
				decimal totalAmmount = 0;
				
				list<client_course_instalment_payment__c> instPayUpdate = new list<client_course_instalment_payment__c>();
				
				/** Get total ammount and relate to the bank deposit **/
				for(client_course_instalment_payment__c c : lCash)
					if(c.selected__c){
						totalAmmount += c.Value__c;
						c.selected__c=false;
						instPayUpdate.add(c);
					}	
				/** END -- Get total ammount and relate to the bank deposit **/
				
				bankDeposit.Amount__c = totalAmmount;	
				bankDeposit.Bank__c = selectedBank;		
				bankDeposit.Type__c = 'Cash';	
				bankDeposit.Deposited_By_Agency__c = currentUser.Contact.AccountId;
				insert bankDeposit;
				
				for(client_course_instalment_payment__c c : instPayUpdate)
					c.Bank_Deposit__c = bankDeposit.id;
				
				
				update instPayUpdate;
				
				PageReference pageRef = new PageReference('/payment_cash_deposit');
				pageRef.setRedirect(true);
				return pageRef;
				
			} catch(Exception e){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
				Database.rollback(sp);
				return null;
			}
		}
	}
	/** END -- Confirm Deposit **/
	
	
	
	
	
	public string selectedAgencyGroup{
    	get{
    		if(selectedAgencyGroup == null)
    			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
    		return selectedAgencyGroup;
    	}  
    	set;
    }
    
	 public List<SelectOption> agencyGroupOptions {
  		get{
   			if(agencyGroupOptions == null){
   				
   				agencyGroupOptions = new List<SelectOption>();  
   				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group  
	    			for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:currentUser.Contact.AccountId order by Parent.Name]){
		     			agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
		    		}
   				}else{
   					for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
		     			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
		    		}
   				}
	   		}
	   		return agencyGroupOptions;
	  }
	  set;
 	}
	
	
	public void changeGroup(){
		selectedAgency = '';
		agencies = findAgencies();
		
		//changeAgency();
	}
	
	
	/** Get agencies and its bank details **/
	public map<string, list<Bank_Detail__c>> agencyBanks {get;set;}
	public list<SelectOption> findAgencies(){
		List<SelectOption> result = new List<SelectOption>();
		agencyBanks = new map<string, list<Bank_Detail__c>>();
		list<Bank_Detail__c> banks = new list<Bank_Detail__c>();
		if(selectedAgencyGroup != null){
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency 
				list<string> ls = new list<string>{currentUser.Contact.AccountId};
				if(currentUser.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c))
						ls.add(ac.id);
				}
				for(Account a: [Select Id, Name, (Select Id, Account_Name__c, Account_Nickname__c, Account_Number__c from Bank_Details1__r ) From Account where id in :ls order by name]){
					if(a.Name!=null && a.name!='')
						result.add(new SelectOption(a.Id, a.Name));
					
					for(Bank_Detail__c b : a.Bank_Details1__r) //get agency banks
						banks.add(b);
					
					agencyBanks.put(a.Id, banks);
				}//end for
				
			}else{							
				
				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
					selectedAgencyGroup = currentUser.Contact.Account.ParentId;
				
				for(Account a: [Select Id, Name, (Select Id, Account_Name__c, Account_Nickname__c, Account_Number__c from Bank_Details1__r ) From Account where ParentId = :selectedAgencyGroup  and RecordType.Name = 'agency' order by name]){
					if(a.Name!=null && a.name!='')
						result.add(new SelectOption(a.Id, a.Name));
					
					for(Bank_Detail__c b : a.Bank_Details1__r) //get agency banks
						banks.add(b);
					
					agencyBanks.put(a.Id, banks);
				}//end for
				
			}
		}
		
		return result;
	}
		
	
	/** Get agencies and its bank details/
	public list<SelectOption> findAgencies(){
		List<SelectOption> result = new List<SelectOption>();
		agencyBanks = new map<string, list<Bank_Detail__c>>();
		list<Bank_Detail__c> banks = new list<Bank_Detail__c>();
		
		for(Account a: [Select Id, Name, (Select Id, Account_Name__c from Bank_Details1__r ) From Account where RecordType.Name = 'agency']){
			if(a.Name!=null && a.name!='')
				result.add(new SelectOption(a.Id, a.Name));
			
			for(Bank_Detail__c b : a.Bank_Details1__r) //get agency banks
				banks.add(b);
			
			agencyBanks.put(a.Id, banks);
		}
		return result;
	}
	/** END -- Get agencies and its bank details **/
	
	
	public void refreshAgencyBanks(){
		banks.clear();
		
		banks.add(new SelectOption('', '-- Select Bank --'));
		for(Bank_Detail__c b : agencyBanks.get(selectedAgency))
			if(b.Account_Name__c!=null && b.Account_Name__c!='')
				banks.add(new SelectOption(b.Id, b.Account_Name__c + ' (acc: '+ b.Account_Number__c +')'));
	}	
	
	public Contact fromDate {
		get{
			if(fromDate == null){
				fromDate = new Contact();
				//fromDate.Expected_Travel_Date__c = system.today();
			}
			return fromDate;}
		set;}
			
	public Contact toDate {
		get{
			if(toDate == null){
				toDate = new Contact();
				//toDate.Expected_Travel_Date__c = system.today();
				//toDate.Expected_Travel_Date__c = toDate.Expected_Travel_Date__c.addDays(7);
			}
			return toDate;}
			set;}	

}