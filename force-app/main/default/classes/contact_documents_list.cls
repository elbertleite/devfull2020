public with sharing class contact_documents_list {

    Contact contact;
    
    public contact_documents_list(ApexPages.StandardController controller){
        contact = [Select Id, remoteContactId__c from Contact where id = :controller.getId()];
    }
    
    private List<String> order = new List<String>{'Visa', 'Passport', 'Personal', 'Enrolment', 'Education', 'Finance'};
    
    public List<String> categoriesOrder {
        get{
            if(categoriesOrder == null){
                categoriesOrder = new List<String>();
                Schema.DescribeFieldResult fieldResult = Client_Document__c.Document_Category__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues(); 
                
                
                for(String orderCat : order)
                    for(String category : getDocs().keySet())
                        if(orderCat == category)
                            categoriesOrder.add(orderCat);
                
                        
                for(Schema.PicklistEntry f : ple){
                    
                    Boolean found = false;
                    for(String orderCat : order)
                        if(orderCat == f.getValue())
                            found = true;
                    
                    if(!found)
                        for(String category : getDocs().keySet())
                            if(category == f.getValue())
                                categoriesOrder.add(f.getValue());                      
                }
                        
                
                
            }
            return categoriesOrder;
        }
        Set;
        
    }
    
    
    public class Doc {
        public String category {get;Set;}       
        public list<DocumentDetails> listDocumentDetails{get{if(listDocumentDetails == null) listDocumentDetails = new list<DocumentDetails>(); return listDocumentDetails;} set;}
    }
    
    
    public class DocumentDetails{
        public Client_Document__c clientDocument {get{if(clientDocument == null) clientDocument = new Client_Document__c(); return clientDocument;} set;}
        
        public list<IPFunctions.ContactDocument> listFiles{get{if(listFiles == null) listFiles = new list<IPFunctions.ContactDocument>(); return listFiles;} set;}
    }
    
    public map<string,boolean> renderDocs {get;set;}
    private Map<String, Doc> docsMap;
    private List<Client_Document__c> listdocs {get;set;}
    private List<IPFunctions.ContactDocument> oldIPFiles {get;set;}
    public Map<String,Doc> getDocs(){
    	
        if(docsMap == null){
        	
        	docsMap = new Map<String,Doc>();
        	renderDocs = new map<string,boolean>();
        	
	       listdocs = [Select C.Account_Name__c, C.Account_Number__c, C.Bank__c, C.Bank_Country__c, C.Branch_Address__c, C.Branch_Name__c, C.BSB__c, 
	                                                    C.Client__c, C.Name, C.Comment__c, C.Country_of_Issue__c, C.CreatedById, C.CreatedDate, C.Date_of_Issue__c, C.IsDeleted, C.Document_Category__c, 
	                                                    C.Document_Number__c, C.Document_Type__c, C.eTicket_Number__c, C.Expiry_Date__c, C.Fax__c, C.IBAN__c, C.LastModifiedById, C.LastModifiedDate, C.Notes__c, C.Phone__c, 
	                                                    C.Id, C.Swift_Code__c, createdBy.Name, Issuing_Authority__c , Visa_conditions__c, Visa_country_applying_for__c, Visa_number__c, Visa_subclass__c, Visa_type__c,
	                                					TRN__c, Date_granted__c, 														
														client_course__r.Course_Name__c, 
														client_course__r.Campus_Name__c, 
														client_course__r.School_Name__c, 
														client_course__r.Start_Date__c, 
														client_course__r.End_Date__c, 
														client_course_instalment__r.Name,
														client_course_instalment__r.Number__c,
														client_course_instalment__r.client_course__c, 
														client_course_instalment__r.client_course__r.Name, 
														client_course_instalment__r.client_course__r.Course_Name__c, 
														client_course_instalment__r.client_course__r.Campus_Name__c, 
														client_course_instalment__r.client_course__r.School_Name__c,			
														client_course_instalment__r.client_course__r.Start_Date__c, 
														client_course_instalment__r.client_course__r.End_Date__c, 											 
	                                                    ( Select Airline_Reference_Number__c, Arrival_Airline_Company__c, Arrival_Airport__c, Arrival_Country__c, Arrival_Date__c, Arrival_Flight_Number__c, Arrival_Hour__c, 
	                                                             Arrival_Minute__c, Arrival_Time__c, Departure_Airline_Company__c, Departure_Airport__c, Departure_Country__c, Departure_Date__c, Departure_Flight_Number__c,
	                                                             Departure_Hour__c, Departure_Minute__c, Departure_Time__c from Client_Flight_Details__r)
	                                            from Client_Document__c C where Client__c = :contact.id order by  Document_Category__c, Document_Type__c, CreatedDate desc];
	          docsMap.put(null,null);
	          renderDocs.put(null, false);
	          for(Client_Document__c cd : listDocs){
	          	
	          	if(!docsMap.containsKey(cd.Document_Category__c)){
		          	Doc doc = new Doc();
	                doc.Category = cd.Document_Category__c;
	                DocumentDetails dd = new DocumentDetails();
	                dd.clientDocument = cd;   
	                	              
	                doc.listDocumentDetails.add(dd);
	                docsMap.put(doc.Category, doc);
	               
	                renderDocs.put(doc.Category, false);
	          		
	          	}else{
	          		Doc d = docsMap.get(cd.Document_Category__c);
	          		
	          		DocumentDetails dd = new DocumentDetails();
	                dd.clientDocument = cd;   
	                	              
	                d.listDocumentDetails.add(dd);
	          	}
	                
                if(cd.client_course_instalment__r.client_course__c!=null) //Client Course
        			renderDocs.put(cd.client_course_instalment__r.client_course__c, false);
            			
	          }//end for
	          
	          
            oldIPFiles = new list<IPFunctions.ContactDocument>();
	        if(contact.remoteContactId__c!=null)
	        	oldIPFiles = IPFunctions.getDocumentFiles(contact.id, 0);
	       
        }
        
		return docsMap;
        
    
    }
    
    

	public void renderDoc(){
		String docCat = ApexPages.currentPage().getParameters().get('cat');
		renderDocs.put(docCat, true);
		        
        S3Controller s3 = new S3Controller();
        s3.constructor();
        renderDocs.put(null, true);
                    
        Doc d = docsMap.get(docCat);
        for(DocumentDetails dd : d.listDocumentDetails){
	        List<IPFunctions.ContactDocument> docFiles = IPFunctions.getDocument(String.valueOf(dd.clientDocument.id), dd.clientDocument.name, s3, 0);
	        
	        dd.listFiles = docFiles;
	        
	         if(oldIPFiles != null)    
                for(IPFunctions.ContactDocument cdfile : oldIPFiles)
	            	if(cdfile.clientDocumentID == dd.clientDocument.id)
	            		dd.listFiles.add(cdfile);
        	
        }//end for
        
	}  
	
	public boolean showReceipt {get;set;}
	public void renderReceipt(){
		showReceipt = true;
		String docCat = ApexPages.currentPage().getParameters().get('cat');
		String clientCourse = ApexPages.currentPage().getParameters().get('cc');
		
		renderDocs.put(clientCourse, true);
		        
        S3Controller s3 = new S3Controller();
        s3.constructor();
        renderDocs.put(null, true);
                    
        Doc d = docsMap.get(docCat);
        for(DocumentDetails dd : d.listDocumentDetails){
        	
        	if(dd.clientDocument.client_course__c == clientCourse){
        		
		        List<IPFunctions.ContactDocument> docFiles = IPFunctions.getDocument(String.valueOf(dd.clientDocument.id), dd.clientDocument.name, s3, 0);
		        
		        dd.listFiles = docFiles;
		        
		         if(oldIPFiles != null)    
	                for(IPFunctions.ContactDocument cdfile : oldIPFiles)
		            	if(cdfile.clientDocumentID == dd.clientDocument.id)
		            		dd.listFiles.add(cdfile);
        	}
        	
        	
        }//end for
        
	}  
    

}