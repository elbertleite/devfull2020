/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class StartDates_test {
	
    static testMethod void testExtraFeeStartDates() {
        
		TestFactory testFactory = new TestFactory();
		
		Account agency = TestFactory.createAgency();
		
		//Account employee = TestFactory.createEmployee(recordTypes, agency);
        
		Account school = TestFactory.createSchool();
		
		Account campus = TestFactory.createCampus(school,agency);
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Cozy Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;
		insert p3;
		
		
		Course__c course = TestFactory.createCourse();
		
		Campus_Course__c cc = TestFactory.createCampusCourse(campus, course);
		
		Course_Price__c cp = TestFactory.createCoursePrice(cc, 'Latin America');
		
		Course_Extra_Fee__c cef = TestFactory.createCourseExtraFee(cc, 'Latin America', p);
		
		Course_Extra_Fee_Dependent__c cefd = TestFactory.createCourseRelatedExtraFee(cef, p2);
		
		Deal__c extraFeeDeal = TestFactory.createCourseExtraFeePromotion(cc, 'Latin America', p);
		
		Deal__c freeUnits = TestFactory.createCourseFreeUnitsPromotion(cc, 'Latin America');
		
		Test.startTest();
        
        ApexPages.currentPage().getParameters().put('id', cef.id);
        ApexPages.currentPage().getParameters().put('type', 'extraFee');
        
        StartDates sd = new StartDates();
        
        List<Start_Date_Range__c> listStartDates = sd.listStartDates;
        sd.newStartDate.From_Date__c = System.today();
        sd.newStartDate.To_Date__c = System.today().addDays(15);
        sd.newStartDate.Value__c = 199;        
        sd.addStartDateRange();
        
        sd.newStartDate.From_Date__c = System.today().addDays(31);
        sd.newStartDate.To_Date__c = System.today().addDays(60);
        sd.newStartDate.Selected__c = true;
        sd.addStartDateRange();
        
        List<SelectOption> Files = sd.files;
        
        sd.editStartDates();
        sd.saveStartDates();
        sd.cancelEdit();
        sd.deleteStartDates();
        
        try {
        	sd.viewFile();        	
        } catch (Exception e){}
        
        Test.stopTest();
    }
    
    
    
    static testMethod void testPriceStartDates() {
    	
    	TestFactory testFactory = new TestFactory();
			
		
		Account agency = TestFactory.createAgency();
		
		//Account employee = TestFactory.createEmployee(recordTypes, agency);
        
		Account school = TestFactory.createSchool();
		
		Account campus = TestFactory.createCampus(school, agency);
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Cozy Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;
		insert p3;
		
		
		Course__c course = TestFactory.createCourse();
		
		Campus_Course__c cc = TestFactory.createCampusCourse(campus, course);
		
		Course_Price__c cp = TestFactory.createCoursePrice(cc, 'Latin America');
		
		Course_Extra_Fee__c cef = TestFactory.createCourseExtraFee(cc, 'Latin America', p);
		
		Course_Extra_Fee_Dependent__c cefd = TestFactory.createCourseRelatedExtraFee(cef, p2);
		
		Deal__c extraFeeDeal = TestFactory.createCourseExtraFeePromotion(cc, 'Latin America', p);
		
		Deal__c freeUnits = TestFactory.createCourseFreeUnitsPromotion(cc, 'Latin America');
		
		Test.startTest();
        
        ApexPages.currentPage().getParameters().put('id', cp.id);
        ApexPages.currentPage().getParameters().put('type', 'coursePrice');
        
        StartDates sd = new StartDates();
        
        List<Start_Date_Range__c> listStartDates = sd.listStartDates;
        sd.newStartDate.From_Date__c = System.today();
        sd.newStartDate.To_Date__c = System.today().addDays(15);
        sd.newStartDate.Value__c = 199;        
        sd.addStartDateRange();
        
        sd.newStartDate.From_Date__c = System.today().addDays(31);
        sd.newStartDate.To_Date__c = System.today().addDays(60);
        sd.newStartDate.Selected__c = true;
        sd.addStartDateRange();
        
        List<SelectOption> Files = sd.files;
        
        sd.editStartDates();
        sd.saveStartDates();
        sd.cancelEdit();
        sd.deleteStartDates();
        
        
        
        Test.stopTest();
     
    }
    
    
    
    
    
    
    
    
    
    
    static testMethod void testPromotionStartDates() {
    	
    	TestFactory testFactory = new TestFactory();			
		
		Account agency = TestFactory.createAgency();
		
		//Account employee = TestFactory.createEmployee(recordTypes, agency);
        
		Account school = TestFactory.createSchool();
		
		Account campus = TestFactory.createCampus(school, agency);
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Cozy Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;
		insert p3;
		
		
		Course__c course = TestFactory.createCourse();
		
		Campus_Course__c cc = TestFactory.createCampusCourse(campus, course);
		
		Course_Price__c cp = TestFactory.createCoursePrice(cc, 'Latin America');
		
		Course_Extra_Fee__c cef = TestFactory.createCourseExtraFee(cc, 'Latin America', p);
		
		Course_Extra_Fee_Dependent__c cefd = TestFactory.createCourseRelatedExtraFee(cef, p2);
		
		Deal__c extraFeeDeal = TestFactory.createCourseExtraFeePromotion(cc, 'Latin America', p);
		
		Deal__c freeUnits = TestFactory.createCourseFreeUnitsPromotion(cc, 'Latin America');
		
		Test.startTest();
        
        ApexPages.currentPage().getParameters().put('id', extraFeeDeal.id);
        ApexPages.currentPage().getParameters().put('type', 'promotion');
        
        StartDates sd = new StartDates();
        
        List<Start_Date_Range__c> listStartDates = sd.listStartDates;
        sd.newStartDate.From_Date__c = System.today();
        sd.newStartDate.To_Date__c = System.today().addDays(15);
        sd.newStartDate.Value__c = 199;        
        sd.addStartDateRange();
        
        sd.newStartDate.From_Date__c = System.today().addDays(31);
        sd.newStartDate.To_Date__c = System.today().addDays(60);
        sd.newStartDate.Selected__c = true;
        sd.addStartDateRange();
        
        List<SelectOption> Files = sd.files;
        
        sd.editStartDates();
        sd.saveStartDates();
        sd.cancelEdit();
        sd.deleteStartDates();
        
        
        
        Test.stopTest();
     
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    static testMethod void testRelatedStartDates() {
    	
    	TestFactory testFactory = new TestFactory();
			
		
		Account agency = TestFactory.createAgency();
		
		//Account employee = TestFactory.createEmployee(recordTypes, agency);
        
		Account school = TestFactory.createSchool();
		
		Account campus = TestFactory.createCampus(school, agency);
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Cozy Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;
		insert p3;
		
		
		Course__c course = TestFactory.createCourse();
		
		Campus_Course__c cc = TestFactory.createCampusCourse(campus, course);
		
		Course_Price__c cp = TestFactory.createCoursePrice(cc, 'Latin America');
		
		Course_Extra_Fee__c cef = TestFactory.createCourseExtraFee(cc, 'Latin America', p);
		
		Course_Extra_Fee_Dependent__c cefd = TestFactory.createCourseRelatedExtraFee(cef, p2);
		
		Deal__c extraFeeDeal = TestFactory.createCourseExtraFeePromotion(cc, 'Latin America', p);
		
		Deal__c freeUnits = TestFactory.createCourseFreeUnitsPromotion(cc, 'Latin America');
		
		Test.startTest();
        
        ApexPages.currentPage().getParameters().put('id', cef.id);
        ApexPages.currentPage().getParameters().put('type', 'relatedFee');
        
        StartDates sd = new StartDates();
        
        List<Start_Date_Range__c> listStartDates = sd.listStartDates;
        sd.newStartDate.From_Date__c = System.today();
        sd.newStartDate.To_Date__c = System.today().addDays(15);
        sd.newStartDate.Value__c = 199;        
        sd.addStartDateRange();
        
        sd.newStartDate.From_Date__c = System.today().addDays(31);
        sd.newStartDate.To_Date__c = System.today().addDays(60);
        sd.newStartDate.Selected__c = true;
        sd.addStartDateRange();
        
        sd.editStartDates();
        
        for(Course_Extra_Fee_Dependent__c dep : sd.relatedFees)
        	for(Start_Date_Range__c sdr : dep.Start_Date_Ranges__r){
        		sdr.From_Date__c = system.today().addDays(60);
        		sdr.To_date__c = system.today().addDays(90);
        		sdr.Value__c = 299;
        	}
        		
        
        
        sd.saveStartDates();
        sd.cancelEdit();
        sd.deleteStartDates();
        
        
        
        Test.stopTest();
     
    }
    
    
    
    
    
    
    
}