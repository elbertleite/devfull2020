public without sharing class search_validation {
    public search_validation() {

    }

	public string accountId{get; set;}

	public class accountClass{ 
		public string school{get; set;}
		public string campus{get; set;}
	}

    @RemoteAction
	public static accountClass currentSchool(string accId){
		accountClass selAccount = new accountClass();
		if(accId != null && accId != ''){
			account ac = [Select id, parentId, recordType.Name from Account where id = :accId];
			if(ac.recordType.Name == 'campus'){
				selAccount.school = ac.parentId;
				selAccount.campus = ac.Id;
			}else{
				selAccount.school = ac.id;
			}
		}
		return selAccount;
	}

    @RemoteAction
	public static list<courses_find.course> searchCourses(map<string,boolean> supplierIdsMap, list<string> supplierIds, string clientNationality, string country, list<string>  cities, list<string>  listCourseCategory,  list<string> listCourseType, list<string> listCourseQualification, 
									list<string> listType, list<string> listSpecialization, string listSchool, list<string> listPeriods, string paymentDate, integer duration, integer hourFrom, 
									integer hourTo, string location, integer lenghtFrom, integer lenghtTo, string offsetPos, integer batchSize, string  listSchools, list<string> listCourses, boolean nextIntakeDateAvailable, string intakeDateFrom){ 
		searchcoursecountry rt = new searchcoursecountry(); 
		list<courses_find.course> courses = new list<courses_find.course>(); 
		boolean hasDeal = false;
		string urlFlag = courses_find.GetResourceURL('flags'); 
		system.debug('==>favourite: '+supplierIdsMap);
		map<string, Campus_Course__c> mapCampusCourse;
		list<string> listCampusCourseIds = new list<string>();
		if(listCourses != null && listCourses.size() > 0){
			mapCampusCourse = new map<string, Campus_Course__c>([Select id from Campus_Course__c where course__c in :listCourses and campus__c in :supplierIds]);
			listCampusCourseIds.addAll(mapCampusCourse.KeySet());
		}
		for(searchcoursecountry.courseDetails rc:rt.searchCoursesJson(supplierIdsMap, supplierIds, clientNationality, country, cities, listCourseCategory, listCourseType, listCourseQualification, listType, listSpecialization, new list<string>{listSchool}, listPeriods, paymentDate, duration, hourFrom, hourTo, location, lenghtFrom, lenghtTo, offsetPos, batchSize, new list<string>{listSchools}, listCampusCourseIds, nextIntakeDateAvailable, intakeDateFrom)){  
			//System.debug('==>rc.courseName: '+rc.courseName);
			if(rc.campusCourseId != null)
				courses.add(new courses_find.course(urlFlag, rc));
			else 
			courses.add(new courses_find.course(rc.lastCampusCourseId)); 
		}
		System.debug('==>courses: '+courses);
		return courses;  

	}


	@RemoteAction
    public static list<AggregateResult> changeSchool() { 
		string sql = 'Select campus__r.ParentId schoolId, Campus__r.Parent.Name schoolName from Campus_Course__c C where Is_Available__c = true and campus__r.recordType.name = \'Campus\' ';
		sql += '  GROUP BY campus__r.ParentId, Campus__r.Parent.Name order by Campus__r.Parent.Name';
        return Database.query(sql);
    }

	@RemoteAction
    public static list<AggregateResult> changeCampus(string schoolId) { 
		string sql = 'Select id, name, Disabled_Campus__c from Account where parentId = :schoolId ';
		sql += '  order by Name';
        return Database.query(sql);
    }

	public class nations{
		public string nation{get;set;}
		public string country{get;set;}
		public nations(string nation,string country){
			this.nation = nation;
			this.country = country;
		}
	}
	@RemoteAction
	public static list<nations> changeNationalities(string schoolId){
		set<string> listNation = new set<string>();
		list<nations> ln = new list<nations>();
		ln.add(new nations('Published Price','published'));
		for(AggregateResult ar:[SELECT Name nt, Country__c ct FROM Nationality_Group__c where Account__c = :schoolId group by name, country__c order by  Name, Country__c])
			if(!listNation.contains((string)ar.get('nt'))){
				listNation.add((string)ar.get('nt'));
				ln.add(new nations((string)ar.get('nt'),(string)ar.get('ct')));
			}
		return ln;
	}

	@RemoteAction
    public static list<AggregateResult> changeCourseCategories(list<string> supplierIds ) { 
		//list<string> supplierIds = (List<String>)JSON.deserialize(supplierIds, list<string>.class);
		string sql = 'Select Course__r.Course_Category__c from Campus_Course__c WHERE Campus__c in :supplierIds and is_available__c = true ';
		sql += ' and Course__r.Course_Category__c != null ';
		sql += ' GROUP BY Course__r.Course_Category__c order by Course__r.Course_Category__c ';
        return Database.query(sql);
    }

	@RemoteAction
    public static list<AggregateResult> changeCourseArea(list<string> supplierIds, list<string> listCourseCategory) { 
		//list<string> supplierIds = (List<String>)JSON.deserialize(supplierIds, list<string>.class);
		string sql = 'Select Course__r.Course_Type__c from Campus_Course__c C where Campus__c in :supplierIds and Is_Available__c = true ';
		sql += ' and Course__r.Course_Type__c != null ';
		if(listCourseCategory != null && listCourseCategory.size() > 0)
			sql += ' and Course__r.Course_Category__c in :listCourseCategory ';
		sql += ' group by Course__r.Course_Type__c order by Course__r.Course_Type__c';
		system.debug('===>sql: '+sql);
        return Database.query(sql);
    }

	@RemoteAction
	public static list<AggregateResult> changeListCourses(list<string> campusId, list<string> listCourseCategory, list<string> listCourseType){
		string sql = 'SELECT course__c courseId, course__r.Name courseName FROM Campus_Course__c where campus__c in :campusId ';
		if(listCourseCategory != null && listCourseCategory.size() > 0)
			sql += ' and Course__r.Course_Category__c in :listCourseCategory ';
		if(listCourseType != null && listCourseType.size() > 0)
			sql += ' and Course__r.Course_Type__c in :listCourseType ';	
		sql += ' group by course__c, course__r.Name order by course__r.Name';
		return Database.query(sql);
	}
}