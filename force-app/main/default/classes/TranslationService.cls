public class TranslationService {
	
	static MetadataService.MetadataPort service;
    
    public static MetadataService.MetadataPort createService(String sessionID){
        service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = sessionID;
        return service;
    }
	
	
	
	private static String getTranslatedValue(String objName, String language, String fieldName, String masterValue, Map<String, Map<String, Map<String, Map<String, String>>>> workbenchTranslations){
		String theval;
		try {
			theval = workbenchTranslations.get(objName).get(language).get(fieldName).get(masterValue);
		} catch (Exception e){ 
			system.debug(e);
			theval = ''; 
		}
		return theval;
	}
		
	public static void updateWorkbenchTranslations(String sessionID, List<Translation__c> translations){
		
		if(service == null)
			createService(sessionID);
										
		List<Translation__c> updateTranslations = new List<Translation__c>();
											 
		Map<String, Map<String, Map<String, Map<String, String>>>> workbench = getTranslationsMap();
		
		for(Translation__c tr : translations){
			boolean changed = false;
			//COURSE CATEGORY
			
			try {
				String wbCategory = getTranslatedValue('Course__c', tr.Language__c, 'Course_Category__c', tr.Campus_Course__r.Course__r.Course_Category__c, workbench);
				if(wbCategory != tr.course_category__c){
					tr.course_category__c = wbCategory;
					changed = true;
				}
			} catch (Exception e){ system.debug(e); }
			
			
			//COURSE TYPE
			try {
				String wbType = getTranslatedValue('Course__c', tr.Language__c, 'Course_Type__c', tr.Campus_Course__r.Course__r.Course_Type__c, workbench);
				if(wbType != tr.Course_Type__c){
					tr.Course_Type__c = wbType;
					changed = true;
				}
			} catch (Exception e){ system.debug(e); }
			
			
			
			//COURSE FIELD
			try {
				String wbField = getTranslatedValue('Course__c', tr.Language__c, 'Type__c', tr.Campus_Course__r.Course__r.Type__c, workbench);
				if(wbField != tr.course_field__c){
					tr.course_field__c = wbField;
					changed = true;
				}
			} catch (Exception e){ system.debug(e); }
			
			
			
			//COURSE FORM OF DELIVERY
			try {
				String wbFOD = getTranslatedValue('Course__c', tr.Language__c, 'Form_of_Delivery__c', tr.Campus_Course__r.Course__r.Form_of_Delivery__c, workbench);
				if(wbFOD != tr.course_form_of_delivery__c){
					tr.course_form_of_delivery__c = wbFOD;
					changed = true;
				}
			} catch (Exception e){ system.debug(e); }
			
			
			//CAMPUS COURSE PERIOD
			try {
				
				if(tr.Campus_Course__r.Period__c != null){
					
					List<String> translatedPeriods = new List<String>();
					for(String thePeriod : tr.Campus_Course__r.Period__c.split(';')){
						String wbPeriod = getTranslatedValue('Campus_Course__c', tr.Language__c, 'Period__c', thePeriod, workbench);
						translatedPeriods.add(wbPeriod);
					}
					
					String periods = String.join(translatedPeriods, ';');
					
					if(periods != tr.course_period__c){
						tr.course_period__c = periods;
						changed = true;
					}
						
				}
					
				
			} catch (Exception e){ system.debug(e); }
			
				
			//CAMPUS COURSE LANGUAGE LEVEL REQUIRED
			try {
				String wbLLR = getTranslatedValue('Campus_Course__c', tr.Language__c, 'Language_Level_Required__c', tr.Campus_Course__r.Language_Level_Required__c, workbench);
				if(wbLLR != tr.course_language_level_required__c){
					tr.course_language_level_required__c = wbLLR;
					changed = true;
				}
			} catch (Exception e){ system.debug(e); }
					
			if(changed) 
				updateTranslations.add(tr);
				
		}
		
		update updateTranslations;
		
		
		
	}
	
	
	
	public static Map<String, Map<String, Map<String, Map<String, String>>>> getTranslationsMap(){
		
		//Object, Language, Field, Value , TranslatedValue
		Map<String, Map<String, Map<String, Map<String, String>>>> translations = new Map<String, Map<String, Map<String, Map<String, String>>>>();
		
		//Objects and Fields to return translations
		Map<String, Set<String>> objectFields = new Map<String, Set<String>>();
		objectFields.put('Course__c', new Set<String>{'Course_Category__c','Type__c','Course_Type__c','Form_of_Delivery__c'});
		objectFields.put('Campus_Course__c', new Set<String>{'Period__c','Language_Level_Required__c'});
		
		
		//LIST OF LANGUAGES
		List<String> languages = new List<String>();		
		Schema.DescribeFieldResult fieldResult = Contact.Preferable_Language__c.getDescribe();
   		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
   		for(Schema.PicklistEntry f : ple)      		
      		languages.add(f.getLabel());

    	
    	for(String objName : objectFields.keySet()){
    		translations.put(objName, new Map<String, Map<String, Map<String, String>>>());
	    	for(String language : languages)
				translations.get(objName).put(language, getPicklistTranslationsByObjectLanguage(objName, objectFields.get(objName), language));	
	    	
    	}
        
       	return translations;
        
    }
    
    /**
    * Retrieves values from translation workbench given an object and language. Filtering out picklists without translation
    * returns a map of fields with original value and translated value
    */
    
    public static Map<String, Map<String, String>> getPicklistTranslationsByObjectLanguage(String objName, Set<String> objFields, String language){
    	
    	Map<String, Map<String, String>> translations = new Map<String, Map<String, String>>();    	
    	MetadataService.CustomObjectTranslation translation = (MetadataService.CustomObjectTranslation ) service.readMetadata('CustomObjectTranslation', new String[] { objName + '-' + language}).getRecords()[0];
    	
    	if(translation != null && translation.fields != null && !translation.fields.isEmpty())
	    	for(MetadataService.CustomFieldTranslation field : translation.fields){
				if(objFields.contains(field.name) && field.picklistValues != null && !field.picklistValues.isEmpty()){
					Map<String, String> fieldTranslations = new Map<String, String>();
					for(MetadataService.PicklistValueTranslation pvt : field.picklistValues)
						fieldTranslations.put(pvt.masterLabel, pvt.translation);
					
					translations.put(field.name, fieldTranslations);
				}
	    	}
					
		return translations;	
					
    	
    }
    
    
    public static boolean isCampusCourseTranslationRequired(List<Campus_Course__c> oldCourses, List<Campus_Course__c> newCourses){
    	boolean updateRequired = false;
    	
    	for(Campus_Course__c oldCourse : oldCourses){
    		for(Campus_Course__c newCourse : newCourses){
    			if( (oldCourse.id == newCourse.id) && 
    				(oldCourse.Language_Level_Required__c != newCourse.Language_Level_Required__c || oldCourse.Period__c != newCourse.Period__c))
    				updateRequired = true;
    		}
    	}
    	
    	
    	return updateRequired;
    }
    
    
    public static boolean isCourseTranslationRequired(List<Course__c> oldCourses, List<Course__c> newCourses){
    	boolean updateRequired = false;
    	
    	for(Course__c oldCourse : oldCourses){
    		for(Course__c newCourse : newCourses){
    			if( (oldCourse.id == newCourse.id) && 
    				(oldCourse.Course_Category__c != newCourse.Course_Category__c || oldCourse.Course_Type__c != newCourse.Course_Type__c || oldCourse.Type__c != newCourse.Type__c || oldCourse.Form_of_Delivery__c != newCourse.Form_of_Delivery__c))
    				updateRequired = true;
    		}
    	}
    	
    	
    	return updateRequired;
    }
    
    
     public static void updateTranslations(List<Course__c> oldCourses, List<Course__c> newCourses){
    	
    	List<String> courseids = new List<String>();
    	
    	for(Course__c oldCourse : oldCourses)
    		for(Course__c newCourse : newCourses)
    			if( oldCourse.id == newCourse.id && oldCourse.name != newCourse.name)
    				courseids.add(newCourse.id);
    	if(!courseids.isEmpty()){	
    		
    		List<Translation__c> updatedTranslations = new List<Translation__c>();
    		
    		for(Translation__c tr : [select Updated_Fields__c, Status__c from Translation__c where Campus_Course__r.Course__c in :courseids]){
    			tr.Updated_Fields__c = 'course_name__c';
    			tr.Status__c = 'Update Required';
    			updatedTranslations.add(tr);
    		}
    		
    		update updatedTranslations;
    		
    	}
    	
    	
    }
    
    
    
    
    
    
}