public with sharing class AgencyBankAccounts {

	public Account agency {get;set;}


	/** Set Invoices **/
	public list<SelectOption> bankOptions {get;set;}
	public void invoices(){
		agencyDetails();

		bankOptions = new list<SelectOption>();
		bankOptions.add(new SelectOption('', '-- Select Option --'));
		for(Bank_Detail__c bank : agency.Bank_Details1__r){
			if(bank.Id != null && bank.Account_Name__c != null)
				bankOptions.add(new SelectOption(bank.Id, bank.Account_Name__c));
		}

	}

	public void saveSetInvoices(){
		update agency;
	}

	/** END -- Set Invoices **/


	


	/** Set Bank Accounts **/
	public void viewBanks(){
		agencyDetails();
	}

	public Bank_Detail__c bank {get;set;}
	public void setupBankDetail(){
		String bankId = ApexPages.CurrentPage().getParameters().get('id');

		system.debug('bankId==>' + bankId);

		if(bankId==null || bankId==''){
			bank = new Bank_Detail__c(Account__c=ApexPages.currentPage().getParameters().get('ac'));
		}else{
			bank = [SELECT Account__c, Account_Nickname__c, Account_Name__c, Account_Number__c, Bank__c, Branch_Address__c, Branch_Name__c, Branch_Phone__c, BSB__c, IBAN__c, Notes__c, Swift_Code__c
						FROM Bank_Detail__c WHERE id =:bankId limit 1];
		}

		system.debug('bank==>' + bank);
	}

	public boolean showError {get{if(showError==null) showError = false; return showError;}set;}
	public void saveBank(){
		upsert bank;
		showError = true;
	}

	public void agencyDetails(){
		agency = [SELECT Id, Name, ParentId, Bank_Client_Invoice__c, Bank_PDS_PFS_Invoice__c, Bank_Receive_Share_Comm__c, Bank_School_Invoice__c, 	Product_Commission_Bank__c, Bank_Sub_Agency__c, Hify_Agency__c,
					(SELECT Id, Account_Nickname__c, Account_Name__c, Account_Number__c, Bank__c,  BSB__c, Notes__c FROM Bank_Details1__r)
						FROM Account WHERE id =:ApexPages.currentPage().getParameters().get('id') limit 1];
	}

	/** END -- Set Bank Accounts **/

}