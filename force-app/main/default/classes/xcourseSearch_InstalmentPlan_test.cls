/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class xcourseSearch_InstalmentPlan_test {

    static testMethod void myUnitTest() {
       	
       	TestFactory testFactory = new TestFactory(); 
       
		Account agency = TestFactory.createAgency();
		
		Contact employee = TestFactory.createEmployee(agency);
		
		Instalment_Plan__c ip = new Instalment_Plan__c();
		ip.Agency__c = agency.id;
		ip.Deposit__c = 30.0;
		ip.Description__c = 'A test';
		ip.Group_Instalments__c = false;
		ip.Instalments_Interest__c = '1:0.68;2:1.13;3:1.36;4:1.49;5:1.59;6:1.65;7:1.70;8:1.74;9:1.77;10:1.79;11:1.81;12:1.83';
		ip.Interest__c = 0;
		ip.Interest_Type__c = 'Credit Card Interest';
		ip.isSelected__c = true;
		ip.Name__c = 'Finance 12x';
		ip.Number_of_Instalments__c = 12;
		insert ip;
		
		ApexPages.Standardcontroller controller = new Apexpages.Standardcontroller(agency);
		xcourseSearch_InstalmentPlan plan = new xcourseSearch_InstalmentPlan(controller);
		
		list<xcourseSearch_InstalmentPlan.instalmentPlanDetails> listInstalmentPlan = plan.listInstalmentPlan;
		
        plan.newInstalmentPlan.planDetail.Name__c = 'asdasd';
        plan.getUnitsRange();
        plan.newInstalmentPlan.planDetail.Number_of_Instalments__c = 5;
        plan.addListInterestNewPlan();
       	plan.newInstalmentPlan.planDetail.Deposit__c = 10;
       	plan.newInstalmentPlan.planDetail.group_instalments__c = true;
       	plan.newInstalmentPlan.planDetail.Interest__c = 5;
       	plan.newInstalmentPlan.planDetail.Interest_Type__c = 'Simple';
       	plan.newInstalmentPlan.planDetail.Surcharge_Value__c = 1;
       	plan.newInstalmentPlan.planDetail.Description__c = 'Test Test Test Test Test Test Test ';
       	plan.addInstalmentPlan();
       	
       	String interestTypeMSG = plan.interestTypeMSG;
       	
       	plan.savePlan();
       	
       	Apexpages.currentPage().getParameters().put('planId', ip.id);
       	plan.deletePlan();
        
    }
}