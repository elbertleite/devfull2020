/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ShowCaseCampus_test {

    static testMethod void myUnitTest() {
        
        Map<String,String> recordTypes = new Map<String,String>();
       
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
        
		Account school = new Account();
		school.recordtypeid = recordTypes.get('School');
		school.name = 'Test School';
		school.BillingCountry = 'Australia';
		school.BillingCity = 'Sydney';
		school.Pathways_offered__c = 'Diploma';
		insert school;
       
		Account campus = new Account();
		campus.RecordTypeId = recordTypes.get('Campus');
		campus.Name = 'Test Campus CBD';
		campus.BillingCountry = 'Australia';
		campus.BillingCity = 'Sydney';
		campus.Extra_Activities__c = 'bla bla ; bla bla ; bla bla ; bla bla ;';
		campus.ParentId = school.id;
		campus.Available_Services__c = 'bla bla ; bla bla ; bla bla ; bla bla ;';
    	campus.Features__c = 'bla bla ; bla bla ; bla bla ; bla bla ;';
    	campus.Why_choose_this_campus__c = '1231231231adasdadasd';
    	campus.Near_to__c = 'Sydney Town Hall';
		insert campus;
		
		
		Course__c course = new Course__c();
		course.Name = 'Certificate III in Business';        
		course.Type__c = 'Business';
		course.Sub_Type__c = 'Business';
		course.Course_Qualification__c = 'Certificate III';
		course.Course_Type__c = 'VET';
		course.Course_Unit_Type__c = 'Week';
		course.Only_Sold_in_Blocks__c = true;
		course.School__c = school.id;
		insert course;
       
		Campus_Course__c cc = new Campus_Course__c();
		cc.Course__c = course.Id;
		cc.Campus__c = campus.Id;        
		cc.Is_Available__c = true;        
		insert cc;
		
		Testimonial__c t = new Testimonial__c();
		t.Content__c = 'Bla bla bla bla';
		t.School__c = school.id;
		t.StudentName__c = 'John dow';
		insert t;
		
		Testimonial_Picture__c tp = new Testimonial_Picture__c();
		tp.Parent__c = t.id;
		tp.Description__c = 'asoidj89q2uaoisjd';
		insert tp;
		
        		
    	Social_Network__c sn = new Social_Network__c();
    	sn.Account__c = campus.id;
    	sn.Type__c = 'skype';
    	sn.Detail__c = 'ip.sydney';
    	insert sn;
    	
    	Bank_Detail__c bd = new Bank_Detail__c();
    	bd.Account__c = campus.id;
    	bd.Account_Name__c = 'Asdwqe';
    	bd.Account_Number__c = '122312asd';
    	bd.Bank__c = 'St George';
    	bd.BSB__c = '1541';
    	insert bd;
    	
        
        Video__c v = new Video__c();
    	v.Account__c = campus.id;
    	v.Description__c  = 'asdasda';
    	v.VideoID__c = 'OIAWOU123as';
    	insert v;
    	
    	Account_Document_File__c adf = new Account_Document_File__c();
    	adf.Description__c = 'new doc';
    	adf.Parent__c = campus.id;
    	insert adf;
    	
    	
    	Account agency = new Account();
    	agency.RecordTypeId = recordTypes.get('Agency');
    	agency.name = 'IP Sydney';    	
    	agency.BillingCity = campus.BillingCity;
    	insert agency;
    	
    	Nationality_Mix__c nm = new Nationality_Mix__c();
    	nm.Account__c = campus.id;
    	nm.Percentage__c = 10;
    	nm.Nationality__c = 'South America';
    	insert nm;
    	
    	nm = new Nationality_Mix__c();
    	nm.Account__c = campus.id;
    	nm.Percentage__c = 25;    	
    	nm.Nationality__c = 'Australia';
    	insert nm;
    	
    	
        Account_Document_File__c folder = new Account_Document_File__c();
		folder.File_Name__c = 'The Folder';
		folder.Parent__c = campus.id;
		folder.Content_Type__c = 'Folder';
		insert folder;
		
		Account_Document_File__c adf3 = new Account_Document_File__c();
		adf3.File_Name__c = 'Campus File';
		adf3.Parent__c = campus.id;
		adf3.Parent_Folder_Id__c = folder.id;
		insert adf3;
		
		Account_Document_File__c folder2 = new Account_Document_File__c();
		folder2.File_Name__c = 'The Second Folder';
		folder2.Parent__c = campus.id;
		folder2.Content_Type__c = 'Folder';
		insert folder2;
		
		Account_Document_File__c adf4 = new Account_Document_File__c();
		adf4.File_Name__c = 'Second Campus File';
		adf4.Parent__c = campus.id;
		adf4.Parent_Folder_Id__c = folder2.id;
		insert adf4;
		
		Test.startTest();
    	
        Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(campus);
    	ShowCaseCampus scc = new ShowCaseCampus(controller);
       // Address__c Address = scc.Address;
        //List<ShowCaseCampus.sDriveFiles> Files = scc.Files;
        //List<ShowCaseCampus.sDriveExtrInfoFiles> extraInfoFiles = scc.extraInfoFiles;
        //List<ShowCaseCampus.sDriveExtrInfoFiles> oldExtraInfoFiles = scc.oldExtraInfoFiles;
        //List<Forms_of_Contact__c> Contacts = scc.Contacts;
        List<Video__c> Videos = scc.Videos;
        //City_Profile__c city = scc.city;
        Account agencyDummy = scc.agency ;
        scc.getCampusDetails();
        scc.getBankDetails();
        scc.getCampuses();
        //scc.getCourses();
        scc.getExtraActivities();
        scc.getFeatures();
        scc.getGLabels();
        scc.getGTooltips();
        scc.getGValues();
        scc.getNationalityMix();
        scc.getNearTo();
        scc.getPathways();
        //scc.getTestimonials();
        scc.getWhychooseThisCampus();
        List<ShowCaseCampus.CourseByField> l = scc.courses;
        
        
        
		List<String> orderedFolders = scc.orderedFolders;
		scc.getFiles();
		Apexpages.currentPage().getParameters().put('fileId', adf3.id);
		
		List<ShowCaseCampus.sDrivePictures> imgs = scc.imgs;
		scc.setReferrer();
	
		try { scc.viewFile(); } catch(Exception e){}
		
		string[] Coordinates = scc.Coordinates;	
		
		scc.getListCampuses();
		
		scc.resetCampus();
		
		try { scc.getTestimonials(); } catch(Exception e){}
	
        Test.stopTest();
        
    }
}