public with sharing class ClientPageContactDetailsController{
	
	public List<Forms_of_contact__C> webContacts{get;set;}
	public List<Social_Network__c> socialNetworks{get;set;}
	public List<Social_Network__c> rdStationOtherDetails{get;set;}
	public List<Client_Document__c> visas{get;set;}
	public IPClasses.RDStationHifyData rdStationFields {get;set;}

	public string mainNumberAddress {get;set;}
	public string otherNumberAddress {get;set;}	
	public String preferableLanguage {get;set;}

	public Contact client{get;set;}
	public List<Forms_of_contact__C> phoneContacts{get;set;}

	public String messageErrorOnUpdate{get;set;}

	public boolean errorOnUpdate{get{if(errorOnUpdate == null) errorOnUpdate = false; return errorOnUpdate;} set;}	

	public Client_Document__c newVisa{get;set;}
	public String visaGranted{get;set;}
	public String visaExpiryDate{get;set;}
	
	public String idClient{get;set;}

	public User user{get;set;}
	public ClientPageContactDetailsController() {
		this.idClient = ApexPages.currentPage().getParameters().get('id');
		this.client = [SELECT ID, Email, RecordType.Name, RDStation_Fields__c, Name, FirstName, LastName, Nickname__c, Gender__c, Marital_Status__c, Status__c, Preferable_Language__c, Highest_Education_Completed__c, Terms_and_Conditions__c, Nationality__c, Secondary_Passport__c, Country_of_Birth__c, Birthdate, Age__c, Age_Range__c, Occupation__c, Email_Subscription__c, Promotions_Conditions__c, Sale_Estimated_Value__c, Lead_Status_Reason__c, Client_Classification__c, Lead_Level_of_Interest__c, LeadSource, Lead_Reference_Type__c, Lead_Source_Specific__c, Lead_Reference_Name__c, Lead_Reference_Id__c, Estimated_Date_to_Close__c, Original_Agency__r.Name, Current_Agency__r.Name, Trip_Organized_by_Competitor__c, Lead_First_Contact__c, Lead_First_Contact_Date__c, Added_on_Newsletter__c, MailingLatitude, MailingLongitude, MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, OtherLatitude, OtherLongitude, OtherStreet, OtherCity, OtherState, OtherPostalCode, OtherCountry FROM Contact WHERE ID = :idClient];
		
		user = [Select ID, Contact.Name, Contact.Account.Name, Contact.Account.Parent.RDStation_Send_Email_New_Secondary_Email__c, Contact.Account.Parent.RDStation_Emails_To_Send__c, Hide_Convert_to_Client_Button__c, Contact.Account.Auto_Check_Previous_Statuses__c, Contact.Account.Account_Currency_Iso_Code__c, Contact.Account.ID, Contact.Account.Parent.ID, Contact.Account.Parent.Check_New_Lead_On_New_Cycle__c, Contact.Account.Parent.Block_Checklist_Tasks_No_Owner__c From User where ID = :UserInfo.getUserId() limit 1];

		if(!String.isEmpty(this.client.Preferable_Language__c)){
			this.preferableLanguage = IPFunctions.getLanguage(this.client.Preferable_Language__c) != null ? IPFunctions.getLanguage(this.client.Preferable_Language__c) : this.client.Preferable_Language__c; 
		}
		loadFormsOfContact(idClient);
		loadRDStationData(client);
		loadVisas();
	
	}

	public String typeNewContactDetail{get;set;}
	public String detailNewContactDetail{get;set;}
	public String countryNewContactDetail{get;set;}
	public String commentsNewContactDetail{get;set;}
	public Boolean isDefaultWhatsAppNewContactDetail{get;set;}

	public List<Sobject> contactObjectsToDelete{get;set;}
	public boolean showFormEditClientDetails{get{if(showFormEditClientDetails == null) showFormEditClientDetails = false; return showFormEditClientDetails;} set;}
	public boolean showFormNewContactDetail{get{if(showFormNewContactDetail == null) showFormNewContactDetail = false; return showFormNewContactDetail;} set;}
	public boolean showFormNewVisa{get{if(showFormNewVisa == null) showFormNewVisa = false; return showFormNewVisa;} set;}
	public boolean hasVisaBeingUpdated{get;set;}

	public void showHideFormEditClientDetails(){
		this.showFormEditClientDetails = !this.showFormEditClientDetails;
	}

	public void loadVisas(){
		this.visas = [SELECT id, Date_granted__c, Expiry_Date__c, Visa_conditions__c, Visa_country_applying_for__c, Visa_number__c, Visa_subclass__c, Visa_type__c, TRN__c FROM Client_Document__c where Client__r.id = :client.id AND Document_Category__c = 'Visa' and Document_Type__c = 'Visa' order by Expiry_Date__c desc, Visa_country_applying_for__c, Visa_type__c, Visa_subclass__c];
		this.hasVisaBeingUpdated = this.visas != null && !this.visas.isEmpty();
	}

	public void loadRDStationData(Contact client){
		rdStationFields = new IPClasses.RDStationHifyData();
		if(Test.isRunningTest()){
			client.RDStation_Fields__c = JSON.serialize(new Map<String, IPClasses.RDStationHifyData>());
		}
		if(!String.isEmpty(client.RDStation_Fields__c)){
			Map<String, IPClasses.RDStationHifyData> rdStationData = (Map<String, IPClasses.RDStationHifyData>) JSON.deserialize(client.RDStation_Fields__c, Map<String, IPClasses.RDStationHifyData>.class);
			rdStationFields = rdStationData.get(user.Contact.Account.Parent.ID);
		}
	}

	public void loadFormsOfContact(String idClient){
		try{
			this.phoneContacts = new List<Forms_of_contact__C>();	
			this.webContacts = new List<Forms_of_contact__C>();	
			this.socialNetworks = new List<Social_Network__c>();
			this.rdStationOtherDetails = new List<Social_Network__c>();

			for(Forms_of_contact__C contact : [SELECT id, Type__c, Country__c, detail__c, comments__c, From_Website__c, Is_Whatsapp_number__c from Forms_of_contact__C WHERE Contact__r.id = :idClient ORDER BY Type__c DESC, createdDate]){
				if('Secondary e-mail'.equals(contact.type__c) || 'Email'.equals(contact.type__c)){
					this.webContacts.add(contact);
				}else{
					this.phoneContacts.add(contact);
				}
			}
			for(Social_Network__c info : [SELECT id, Type__c, Detail__c, Object_Type__c, RDStation_Custom_Field_Type__c from Social_Network__c WHERE Contact__r.id = :idClient]){
			if(info.Object_Type__c == 'Social Network'){
				socialNetworks.add(info);
			}else{
				rdStationOtherDetails.add(info);
			}
		}
		}catch(Exception e){
			system.debug('Error on loadFormsOfContact() ===> ' + e.getLineNumber() + ' <=== '+e.getMessage());
		}
	}

	public void saveClientDetails(){
		Boolean updateContactFromVisa =  ApexPages.currentPage().getParameters().get('updateContactFromVisa') == 'true';
		try{
			if(!String.isEmpty(mainNumberAddress)){
				client.MailingStreet = mainNumberAddress + ' ' + client.MailingStreet;
			}
			if(!String.isEmpty(otherNumberAddress)){
				client.OtherStreet = otherNumberAddress + ' ' + client.OtherStreet;
			}
			update client;
			if(!String.isEmpty(this.client.Preferable_Language__c)){
				this.preferableLanguage = IPFunctions.getLanguage(this.client.Preferable_Language__c) != null ? IPFunctions.getLanguage(this.client.Preferable_Language__c) : this.client.Preferable_Language__c; 
			}
			if(phoneContacts != null && !phoneContacts.isEmpty()){
				update phoneContacts;
			}
			if(webContacts != null && !webContacts.isEmpty()){
				Map<String, String> secondaryEmails = new Map<String, String>(); 
				for(Forms_of_contact__c fc : webContacts){
					if('Secondary e-mail'.equals(fc.type__c)){
						secondaryEmails.put(fc.ID, fc.Detail__c);
					}
				}
				if(!secondaryEmails.isEmpty()){
					for(Forms_of_contact__c fc : [SELECT ID, Detail__c FROM Forms_of_contact__c WHERE ID IN :secondaryEmails.keySet()]){
						if(fc.Detail__c != secondaryEmails.get(fc.ID)){
							sendEmailAlertNewSecondaryEmail(fc.Detail__c, secondaryEmails.get(fc.ID));
						}
					}
				}
				update webContacts;
			}
			if(socialNetworks != null && !socialNetworks.isEmpty()){
				update socialNetworks;
			}
			if(visas != null && !visas.isEmpty()){
				update visas;
				if(updateContactFromVisa){
					Client_Document__c visaToUpdateCycle;
					for(Client_Document__c visa : visas){
						if(visa.Expiry_Date__c != null){
							if(visaToUpdateCycle == null || visaToUpdateCycle.Expiry_Date__c < visa.Expiry_Date__c){
								visaToUpdateCycle = visa;
							}
						}
					}
					if(visaToUpdateCycle != null){
						updateCycleFromVisa(visaToUpdateCycle);
					}
				}
			}

			if(this.contactObjectsToDelete != null && !this.contactObjectsToDelete.isEmpty()){
				delete this.contactObjectsToDelete;
			}
		}catch(DmlException e){
			messageErrorOnUpdate = '';
			errorOnUpdate = true;
			for(Integer i = 0; i < e.getNumDml(); i++){
				messageErrorOnUpdate = messageErrorOnUpdate + e.getDmlMessage(i);
			}
		}catch(Exception e){
			system.debug('Error on saveClientDetails() ===> ' + e.getLineNumber() + ' <=== '+e.getMessage());	
			errorOnUpdate = true;
			messageErrorOnUpdate = e.getMessage();
		}
		this.showFormEditClientDetails = !this.showFormEditClientDetails;
	}

	public void updateCycleFromVisa(Client_Document__C visa){
		Destination_Tracking__c currentDTracking = new IPFunctions().getContactCurrentCycle(this.client.id, this.user.Contact.Account.Parent.ID);
		if(currentDTracking == null || currentDTracking.Destination_Country__c != visa.Visa_country_applying_for__c){
			Destination_Tracking__c newCycle = new Destination_Tracking__c();
			newCycle.Destination_Country__c = visa.Visa_country_applying_for__c; 
			new IPFunctions().createNewContactCycle(this.client, this.user, true, newCycle, true, this.user.Contact.Account.Parent.Check_New_Lead_On_New_Cycle__c);
		}
		Contact ctt = new Contact(ID = this.client.ID);
		ctt.Destination_Country__c = visa.Visa_country_applying_for__c;
		ctt.Visa_Expiry_Date__c = visa.Expiry_Date__c;
		update ctt;
	}

	public void saveNewVisa(){
		saveVisa(false);
	}

	public void saveAndNewVisa(){
		saveVisa(true);
	}

	public void saveVisa(Boolean openVisaForm){
		try{
			String [] dates;
			Boolean visaExpiryDateFilled = false;
			if(!String.isEmpty(visaGranted)){
				dates = visaGranted.split('/');
				newVisa.Date_granted__c = Date.newInstance(Integer.valueOf(dates[2]), Integer.valueOf(dates[1]), Integer.valueOf(dates[0]));
			}
			if(!String.isEmpty(visaExpiryDate)){
				dates = visaExpiryDate.split('/');
				newVisa.Expiry_Date__c = Date.newInstance(Integer.valueOf(dates[2]), Integer.valueOf(dates[1]), Integer.valueOf(dates[0]));
				visaExpiryDateFilled = true;
			}
			boolean isValid = validateVisaFields(newVisa);
			if(isValid){
				newVisa.Client__c = this.idClient;
				upsert newVisa;
				if(visaExpiryDateFilled){
					Boolean updateContactWithVisaInfo = ApexPages.currentPage().getParameters().get('updateContactWithVisaInfo') == 'true';
					if(updateContactWithVisaInfo){
						updateCycleFromVisa(newVisa);
						/*Destination_Tracking__c currentDTracking = new IPFunctions().getContactCurrentCycle(this.client.id, this.user.Contact.Account.Parent.ID);
						if(currentDTracking == null || currentDTracking.Destination_Country__c != newVisa.Visa_country_applying_for__c){
							Destination_Tracking__c newCycle = new Destination_Tracking__c();
							newCycle.Destination_Country__c = newVisa.Visa_country_applying_for__c; 
							new IPFunctions().createNewContactCycle(this.client, this.user, true, newCycle, true, this.user.Contact.Account.Parent.Check_New_Lead_On_New_Cycle__c);
						}
						Contact ctt = new Contact(ID = this.client.ID);
						ctt.Destination_Country__c = newVisa.Visa_country_applying_for__c;
						ctt.Visa_Expiry_Date__c = newVisa.Expiry_Date__c;
						update ctt;*/
					}
				}
				loadVisas();
				openCloseFormNewVisa();
				if(openVisaForm){
					this.showFormNewVisa = true;
				}
			}
		}catch(Exception e){
			system.debug('Error on saving new visa() ===> ' + e.getLineNumber() + ' <=== '+e.getMessage());
		}
	}

	private boolean validateVisaFields(Client_Document__c visa){
		boolean validateVisaFields = true;
		if(visa.Visa_country_applying_for__c == null || visa.Visa_country_applying_for__c == ''){
			visa.Visa_country_applying_for__c.addError(IPFunctions.MESSAGE_FIELD_REQUIRED);
			validateVisaFields = false;
		}
		/*if(visa.Visa_type__c != 'Resident' && visa.Expiry_Date__c == null){
			visa.Expiry_Date__c.addError(IPFunctions.MESSAGE_FIELD_REQUIRED);
			validateVisaFields = false;
		}*/

		return validateVisaFields;

	}

	public void openCloseFormNewVisa(){
		this.showFormNewVisa = !this.showFormNewVisa;
		newVisa = new Client_Document__c();
		newVisa.Document_Category__c = 'Visa';
		newVisa.Document_Type__c = 'Visa';
		this.visaGranted = '';
		this.visaExpiryDate = '';

	}

	public void openCloseFormNewContactDetail(){
		this.showFormNewContactDetail = !this.showFormNewContactDetail;

		this.typeNewContactDetail = null;
		this.detailNewContactDetail = null;
		this.countryNewContactDetail = null;
		this.commentsNewContactDetail = null;
		this.isDefaultWhatsAppNewContactDetail = false;
	}
	
	public void saveNewContactDetail(){
		saveContactDetail();
		openCloseFormNewContactDetail();
	}
	
	public void saveAndNewContactDetail(){
		saveContactDetail();
		this.typeNewContactDetail = null;
		this.detailNewContactDetail = null;
		this.countryNewContactDetail = null;
		this.commentsNewContactDetail = null;
		this.isDefaultWhatsAppNewContactDetail = false;
	}

	public void saveContactDetail(){
		if(isSocialNetwork(this.typeNewContactDetail)){
			Social_Network__c s = new Social_Network__c();
			s.Detail__c = this.detailNewContactDetail;
			s.Type__c = this.typeNewContactDetail;
			s.Contact__c = client.id;
			insert s;
			// clientPageController.socialNetworks.add(s);
			socialNetworks.add(s);
		}else{
			Forms_of_contact__C f = new Forms_of_contact__C();
			f.Contact__c = client.id;
			f.Type__c = this.typeNewContactDetail;
			f.Detail__c = this.detailNewContactDetail;
			f.Country__c = this.countryNewContactDetail;
			f.Comments__c = this.commentsNewContactDetail;
			if(f.type__c != 'Secondary e-mail'){
				f.Is_Whatsapp_number__c = this.isDefaultWhatsAppNewContactDetail;
				boolean removePreviousDefaultWhatsappNumber = false;

				if(f.Is_Whatsapp_number__c){
					removePreviousDefaultWhatsappNumber = true;
				}else{
					if(this.typeNewContactDetail == 'WhatsApp'){
						removePreviousDefaultWhatsappNumber = true;
						for(Forms_of_contact__C fc : this.phoneContacts){
							if(fc.Is_Whatsapp_number__c){
								removePreviousDefaultWhatsappNumber = false;
							}
						}
					}
				}
				if(removePreviousDefaultWhatsappNumber){
					f.Is_Whatsapp_number__c = true;	
					for(Forms_of_contact__C fc : this.phoneContacts){
						fc.Is_Whatsapp_number__c = false;
					}
					update this.phoneContacts;
				}
			}
			insert f;

			if('Secondary e-mail'.equals(f.type__c) || 'Email'.equals(f.type__c)){
				// clientPageController.webContacts.add(f);
				webContacts.add(f);
				if('Secondary e-mail'.equals(f.type__c)){
					sendEmailAlertNewSecondaryEmail(null, f.Detail__c);
				}
			}else{
				phoneContacts.add(f);
			}
		}
	}

	public void sendEmailAlertNewSecondaryEmail(String oldSecondaryEmail, String email){
		if(user.Contact.Account.Parent.RDStation_Send_Email_New_Secondary_Email__c && !String.isEmpty(user.Contact.Account.Parent.RDStation_Emails_To_Send__c)){
			String emailTo = null;
			Map<String, String> emailsCopy = new Map<String, String>();
			for(String emailToSend : (List<String>) JSON.deserialize(user.Contact.Account.Parent.RDStation_Emails_To_Send__c, List<String>.class)){
				if(emailTo == null){
					emailTo = emailToSend;
				}else{
					emailsCopy.put(emailToSend, emailToSend);
				}
			}
			if(emailTo != null){
				String url = IPFunctions.getContactURLByEnvironment(client.ID, 'contact_new_details');
				String subject;
				String bodyEmail = 'A secondary email was saved for the contact '+client.Name+'.<br/>';

				bodyEmail += '<br/><b>Contact</b>: '+url;
				bodyEmail += '<br/><b>Current Agency</b>: '+client.Current_Agency__r.Name;
				bodyEmail += '<br/><b>Email</b>: '+client.Email;
				if(!String.isEmpty(oldSecondaryEmail)){
					bodyEmail += '<br/><b>Old Secondary Email</b>: '+oldSecondaryEmail;
					bodyEmail += '<br/><b>New Secondary Email</b>: '+email;
					subject = 'Secondary Email updated saved for Contact '+client.Name;
				}else{
					bodyEmail += '<br/><b>Secondary Email</b>: '+email;
					subject = 'New Secondary Email saved for Contact '+client.Name;
				}
				bodyEmail += '<br/><b>Saved By</b>: '+user.Contact.Name+' - '+user.Contact.Account.Name;
				//IPFunctions.sendEmail('Education Hify', 'alerts.rdstation@educationhify.com', 'Guilherme Abiz', 'vitor.machado@educationhify.com', subject, bodyEmail);	
				IPFunctions.sendEmail('Education Hify', 'alerts.rdstation@educationhify.com', emailTo, emailTo, emailsCopy, subject, bodyEmail);
			}
		}
	}

	public void loadFormsOfContactDetailsPage(){
		this.webContacts = new List<Forms_of_contact__C>();
		this.socialNetworks = new List<Social_Network__c>();
		this.rdStationOtherDetails = new List<Social_Network__c>();

		for(Forms_of_contact__C contact : [SELECT id, Type__c, Country__c, detail__c, comments__c, From_Website__c from Forms_of_contact__C WHERE Contact__r.id = :client.id ORDER BY Type__c DESC, createdDate]){
			if('Secondary e-mail'.equals(contact.type__c) || 'Email'.equals(contact.type__c)){
				this.webContacts.add(contact);
			}else{
				//this.phoneContacts.add(contact);
			}
		}

		for(Social_Network__c info : [SELECT id, Type__c, Detail__c, Object_Type__c, RDStation_Custom_Field_Type__c from Social_Network__c WHERE Contact__r.id = :client.id]){
			if(info.Object_Type__c == 'Social Network'){
				socialNetworks.add(info);
			}else{
				rdStationOtherDetails.add(info);
			}
		}

		this.visas = [SELECT id, Date_granted__c, Expiry_Date__c, Visa_conditions__c, Visa_country_applying_for__c, Visa_number__c, Visa_subclass__c, Visa_type__c, TRN__c FROM Client_Document__c where Client__r.id = :client.id AND Document_Category__c = 'Visa' and Document_Type__c = 'Visa' order by Expiry_Date__c desc, Visa_country_applying_for__c, Visa_type__c, Visa_subclass__c];

		system.debug('VISAS FOUND '+json.serialize(this.visas));
	}


	public void markObjectForDeletion(){
		String idObj = ApexPages.currentPage().getParameters().get('idValue');
		String type = ApexPages.currentPage().getParameters().get('type');

		if(this.contactObjectsToDelete == null){
			this.contactObjectsToDelete = new List<Sobject>();
		}
		Integer indexElement;
		if(type.equals('webContact')){
			for(Forms_of_contact__C contact : webContacts){
				if(idObj.equals(contact.id)){
					indexElement = webContacts.indexOf(contact);
					this.contactObjectsToDelete.add(contact);
					break;
				}
			}
			if(indexElement > -1){
				webContacts.remove(indexElement);
			}
		}else if(type.equals('phoneContact')){
			for(Forms_of_contact__C contact : phoneContacts){
				if(idObj.equals(contact.id)){
					indexElement = phoneContacts.indexOf(contact);
					this.contactObjectsToDelete.add(contact);
					break;
				}
			}
			if(indexElement > -1){
				phoneContacts.remove(indexElement);
			}
		}else if(type.equals('socialNetwork')){
			for(Social_Network__c contact : socialNetworks){
				if(idObj.equals(contact.id)){
					indexElement = socialNetworks.indexOf(contact);
					this.contactObjectsToDelete.add(contact);
					break;
				}
			}
			if(indexElement > -1){
				socialNetworks.remove(indexElement);
			}
		}else if(type.equals('visa')){
			for(Client_Document__c visa : visas){
				if(idObj.equals(visa.id)){
					indexElement = visas.indexOf(visa);
					this.contactObjectsToDelete.add(visa);
					break;
				}
			}
			if(indexElement > -1){
				visas.remove(indexElement);
			}
		}
	}

	public boolean isSocialNetwork(String typeNewContact){
		Schema.DescribeFieldResult fieldResult = Schema.Social_Network__c.Type__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple){
			if(typeNewContact != 'WhatsApp' && f.getValue().equals(typeNewContact)){
				return true;
			}
		}
		return false;
	}

	public List<SelectOption> getLanguageOptions(){
		List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Schema.Contact.Preferable_Language__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		options.add(new SelectOption( '', '-- Select Language --' ));
		for( Schema.PicklistEntry f : ple){
			options.add(new SelectOption( f.getValue(), IPFunctions.getLanguage(f.getValue()) ));
		}
		return options;
	}

	public List<SelectOption> getVisaCountries(){
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('','-- Select --'));
		Schema.DescribeFieldResult fieldResult = Schema.Client_Document__c.Visa_country_applying_for__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple){
			options.add(new SelectOption( f.getValue(), f.getLabel() ));
		}
		return options;
	}

	public List<SelectOption> getVisaTypes(){
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('','-- Select --'));
		Schema.DescribeFieldResult fieldResult = Schema.Client_Document__c.Visa_type__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple){
			options.add(new SelectOption( f.getValue(), f.getLabel() ));
		}
		return options;
	}

	public List<SelectOption> getVisaSubclass(){
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('','-- Select --'));
		Schema.DescribeFieldResult fieldResult = Schema.Client_Document__c.Visa_subclass__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple){
			options.add(new SelectOption( f.getValue(), f.getLabel() ));
		}
		return options;
	}

	public List<SelectOption> getCountries(){
		List<SelectOption> options = IPFunctions.getAllCountries();
		options.add(0, new SelectOption('','-- Select --'));
		return options;
	}

	public List<SelectOption> getPhoneContactTypes(){
		List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Schema.Forms_of_Contact__c.Type__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple){
			if(!f.getValue().contains('email') && !f.getValue().contains('e-mail')){
				options.add(new SelectOption( f.getValue(), f.getLabel() ));
			}
		}
		return options;
	}

	public List<SelectOption> getSocialNetworkTypes(){
		List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Schema.Social_Network__c.Type__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple){
			options.add(new SelectOption( f.getValue(), f.getLabel() ));
		}
		return options;
	}
    
	public List<SelectOption> getLeadSourceSpecific(){
		List<SelectOption> leadSourceSpecific = new List<SelectOption>();
		if(client.LeadSource!= null && client.LeadSource != ''){
			UserDetails.GroupSettings gs = new UserDetails.GroupSettings();
			leadSourceSpecific = gs.getLeadSourceSpecifics(user.Contact.Account.Parent.ID, client.LeadSource);
		}else{
			leadSourceSpecific.add(new SelectOption( '', '--Select Option --'));
		}
		return leadSourceSpecific;
	}

	public List<SelectOption> getAllTypesOfContact(){
		List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Schema.Forms_of_Contact__c.Type__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple){
			options.add(new SelectOption( f.getValue(), f.getLabel() ));
		}
		options.add(New SelectOption('' , ' -- Social Networks -- ', true));
		options.addAll(getSocialNetworkTypes());
		return options;
	}

	@RemoteAction
	public static SObject[] findSObjects(string recordType, string obj, string qry, string addFields) {
		// more than one field can be passed in the addFields parameter
		// split it into an array for later use
		List<String> fieldList;
		if (addFields != null) fieldList = addFields.split(',');
		// check to see if the object passed is valid
		Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
		Schema.SObjectType sot = gd.get(obj);
		if (sot == null) {
			// Object name not valid
			return null;
		}
		// create the filter text
		String filter = ' like \'%' + String.escapeSingleQuotes(qry) + '%\'';
		//begin building the dynamic soql query
		String soql = 'select id, Name';
		// if an additional field was passed in add it to the soql
		if (fieldList != null) {
			for (String s : fieldList) {
				soql += ', ' + s;
			}
		}
		
		
		
		// add the object and filter by name to the soql
		soql += ' from ' + obj;
	
		soql += ' where name' + filter;
		
		// add the filter by additional fields to the soql
		if (fieldList != null) {
			for (String s : fieldList) {
				soql += ' or ' + s + filter;
			}
		}
		soql += '  order by Name limit 15';
		system.debug('autoCOmplete==='  +soql);
		List<sObject> L = new List<sObject>();
		try {
			L = Database.query(soql);
		}
		catch (QueryException e) {
			return null;
		}
		return L;
	}
}