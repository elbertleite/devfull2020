/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class client_course_instalment_new_test {

    static testMethod void myUnitTest() {
        
		TestFactory tf = new TestFactory();
       
       	Account school = tf.createSchool();
       	Account agency = tf.createAgency();
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
       	Account campus = tf.createCampus(school, agency);
       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
       
		
		Contact client = tf.createLead(agency, emp);
		client_course__c booking = tf.createBooking(client);
       	client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
		client_course_instalment__c cci = new client_course_instalment__c();
		cci.CLIENT_COURSE__C = cc.id;
		cci.NUMBER__C = 1;
		cci.INSTALMENT_VALUE__C = 1000;
		cci.TUITION_VALUE__C = 1000;
		cci.EXTRA_FEE_VALUE__C = 0;
		cci.COMMISSION__C = 20;
		cci.COMMISSION_VALUE__C = 310;
		cci.DUE_DATE__C = system.today().addDays(30);
		cci.ORIGINAL_DUE_DATE__C = system.today().addDays(30);
		cci.ORIGINAL_TUITION_VALUE__C = 1000;
		cci.ORIGINAL_INSTALMENT_VALUE__C = 1000;
		cci.COMMISSION_TAX_RATE__C = 30;
		cci.COMMISSION_TAX_VALUE__C = 93;
		cci.TOTAL_SCHOOL_PAYMENT__C = 1147;
		cci.STATUS__C = 'Open';
		cci.Agency_Currency__c = 'AUD';
		cci.Agency_Currency_Rate__c = 1;
		cci.Agency_Currency_Value__c = 1;
		cci.BALANCE__C = 403;
		insert cci;

       	client_Course_fees__c ccFees  = tf.createClientCourseFees(cc, false);
       	//ccFees.total_value__c = 10;
       	//ccFees.value__c = 10;
       	//update ccFees;
       	
       	client_Course_fees__c ccFees2  = tf.createClientCourseFees(cc, false);
       	
		//List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
		
		ApexPages.currentPage().getParameters().put('id', cc.id);
		client_course_instalment_new testClass = new client_course_instalment_new(new ApexPages.StandardController(cc));
		
		boolean campusHasPaymentDate = testClass.campusHasPaymentDate;
		testClass.getCourseCalendar();
		
		ApexPages.currentPage().getParameters().put('courseId', cc.id);
		ApexPages.currentPage().getParameters().put('numInstal', '2');
		ApexPages.currentPage().getParameters().put('splitFee', 'true');
		ApexPages.currentPage().getParameters().put('interval', '1');
		ApexPages.currentPage().getParameters().put('intervalOption', 'Month');
		ApexPages.currentPage().getParameters().put('dtFisrtInstalment', '20/02/2016');
		
		ApexPages.currentPage().getParameters().remove('intervalOption');
		ApexPages.currentPage().getParameters().put('intervalOption', 'Quarter');
		testClass.calculateInstalments();
		
		ApexPages.currentPage().getParameters().remove('intervalOption');
		//ApexPages.currentPage().getParameters().remove('splitFee');
		//ApexPages.currentPage().getParameters().put('splitFee', 'true');
		ApexPages.currentPage().getParameters().put('intervalOption', 'Semester');
		testClass.calculateInstalments();
		
		ApexPages.currentPage().getParameters().remove('intervalOption');
		ApexPages.currentPage().getParameters().put('intervalOption', 'Year');
		testClass.calculateInstalments();
		
		ApexPages.currentPage().getParameters().remove('intervalOption');
		ApexPages.currentPage().getParameters().put('intervalOption', 'Week');
		testClass.calculateInstalments();
		testClass.saveUpdate();
		
		
		
		ApexPages.currentPage().getParameters().remove('splitFee');
		ApexPages.currentPage().getParameters().put('splitFee', 'false');
		ApexPages.currentPage().getParameters().put('intervalOption', 'Week');
		testClass.calculateInstalments();
		testClass.saveUpdate();
		
		List<SelectOption> InstalmentInterval = testClass.InstalmentInterval;
		List<SelectOption> instalmentIntervalUnits = testClass.instalmentIntervalUnits;
		
		ApexPages.currentPage().getParameters().put('campusId', campus.id);
		testClass.addInstalmentDates();
		
		
		testClass.getcoursesInstalments();
		
		for(client_course_instalment_new.installFeeGroup ifg : testClass.listInstallmentFees){
			ifg.feeValue= 150;
			ifg.selectedFeeName = '2016 Enrolment Fee';
		}//end for
		
		ApexPages.currentPage().getParameters().put('instNumber', '1');
		testClass.addFeeInstallment();
		testClass.removeFeeInstallment();
		
		testClass.cancelUpdate();
		
		list<Campus_Instalment_Payment_Date__c> result = [Select Campus__c, Instalment_Payment_Date__c, isSelected__c from Campus_Instalment_Payment_Date__c order by Campus__c, Instalment_Payment_Date__c];
		system.debug('result===> '+ result);
		system.debug('cc.Campus===> '+ testClass.clientCourse.Campus_Course__r.Campus__c);
			
    }
}