@isTest
private class client_course_request_loo_test {

	@isTest static void test_method_one() {

		TestFactory tf = new TestFactory();
		Account agencyGroup = tf.createAgencyGroup();
		Account agency = tf.createSimpleAgency(agencyGroup);
		Contact employee = tf.createEmployee(agency);
		User portalUser = tf.createPortalUser(employee);
		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);
		Course__c course = tf.createCourse();
		Campus_Course__c cc = tf.createCampusCourse(campus, course);
		Course__c course2 = tf.createCourse();
		Campus_Course__c cc2 = tf.createCampusCourse(campus, course);
		Commission__c commission = tf.createCommission(school);

		Back_Office_Control__c bo = new Back_Office_Control__c(Agency__c = agency.id, Country__c = 'Australia', Backoffice__c = agency.id, Services__c = 'Admissions');
		insert bo;

		Test.startTest();

		system.runAs(portalUser){

			Contact client = tf.createLead(agency, employee);
			client.Owner__c = portalUser.id;
			update client;
			client_course__c clientCourseBooking = tf.createBooking(client);
			client_course__c clientCourse = tf.createClientCourse(client, school, campus, course, cc, clientCourseBooking);
			client_course__c clientCourse2 = tf.createClientCourse(client, school, campus, course2, cc2, clientCourseBooking);
			client_course__c clientCourse3 = tf.createClientCourse(client, school, campus, course2, cc2, clientCourseBooking);
			clientCourse.LOO_Requested_To_BO__c = true;
			clientCourse.LOO_Requested_to_BO_By__c = portalUser.id;
			clientCourse.LOO_Requested_to_BO_On__c = system.now();
			clientCourse.isPackage__c =true;
			update clientCourse;

			clientCourse2.Course_Package__c = clientCourse.id;
			clientCourse2.LOO_Requested_To_BO__c = true;
			clientCourse2.LOO_Requested_to_BO_By__c = portalUser.id;
			clientCourse2.LOO_Requested_to_BO_On__c = system.now();
			update clientCourse2;



			client_course_request_loo req = new client_course_request_loo();
			req.setParams();
			List<SelectOption> dummy = req.agencyGroupOptions;
			req.changeGroup();
			dummy = req.schoolOptions;
			dummy = req.campusOptions;
			Contact dateFrom = req.dateFrom;
			Contact dateTo = req.dateTo;
			req.search();


			ApexPages.currentPage().getParameters().put('courseid', clientCourse.id);
			ApexPages.currentPage().getParameters().put('noteComments', 'note comment goes here');
			req.onlineApplication();
			req.missingDocuments();

			ApexPages.currentPage().getParameters().put('type', 'acc');
			req.setParams();

			ApexPages.currentPage().getParameters().put('type', 'ins');
			req.setParams();

			ApexPages.currentPage().getParameters().put('type', 'loo');
			req.setParams();

			double d = req.offSet;
			String st = req.bucket;
			st = req.requestCourseID;

		}

		Test.stopTest();




	}



}