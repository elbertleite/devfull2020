global class batch_updateClientDoc_previewLink_doc implements Database.Batchable<sObject>,   Database.AllowsCallouts {
	
        String query;
        
        global batch_updateClientDoc_previewLink_doc(string qr) {
            query = qr;
        }
        
        global Database.QueryLocator start(Database.BatchableContext BC) {
            return Database.getQueryLocator(query);
        }
    
        global void execute(Database.BatchableContext BC, List<Client_Document__c> scope) {
            string folder = '';
            for(Client_Document__c ld:scope){
                if(ld.preview_link__c != null && ld.preview_link__c.trim() != ''){
                    System.debug('==> ld.preview_link__c'+ld.preview_link__c);
                    folder = 'cloudpondconnector/Client_Document__c/'+string.valueOf(ld.id).left(15)+' - '+string.valueOf(ld.id).left(15);
                    string lf = fileUpload.listFiles(IPFunctions.s3bucket, folder, true, 'preview_link__c', ld.id);
                    System.debug('==> lf'+lf);
                }
                
            }
        }
         
        global void finish(Database.BatchableContext BC) { 
            System.debug('Batch Process Complete');   
        }
        
    }
    
    /*open up the Debug Log and run this
           batch_updateClientDoc_previewLink_doc batch = new batch_updateClientDoc_previewLink_doc('SELECT id, preview_link__c FROM Client_Document__c where day_only(createdDate) >= 2018-01-01'');
        Id batchId = Database.executeBatch(batch, 1); //Define batch size
 
    */