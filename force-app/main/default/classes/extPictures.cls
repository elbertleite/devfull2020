public class extPictures{
    private Account acct;
    public String fileURL {get; set;}
    public String logoURL {get; set;}
    private String picID; 

    
    public extPictures(ApexPages.StandardController controller)
    {
   
        acct = [Select Id, Parent.ID, RecordType.Name/*, isPersonAccount*/ from Account where id = :controller.getRecord().id];
        
        //in case of campuses the logo derives from school
        if (acct.RecordType.Name == 'campus')
            picID = acct.Parent.Id;
        else
            picID = acct.Id;
        
        // list of pictures (for persons and school/campuses/agencies)
        List<Account_Picture_File__c> accountPictures = [Select id 
        from Account_Picture_File__c 
        where 
            WIP__c = false and 
            Content_Type__c LIKE 'image/%' and 
            Parent__c = :picId
            ORDER BY LastModifiedDate DESC
            LIMIT 5
        ];
        
        // list of logos (just 1, the most recent)
        List<Account_Logo_File__c> accountLogo = [Select id 
        from Account_Logo_File__c 
        where 
            WIP__c = false and 
            Content_Type__c LIKE 'image/%' and 
            Parent__c = :picId
            ORDER BY LastModifiedDate DESC
            LIMIT 1
        ];
        
        // URL for pictures
        if(accountPictures.size() > 0)
        {
            fileURL = cg.SDriveTools.getAttachmentURL(picID, accountPictures[0].id, (100 * 60));
        }
        else
        {
            fileURL = 'NoPhoto';
        
        }
        

        // URL for logo
        if(accountLogo.size() > 0)
        {
            logoURL = cg.SDriveTools.getAttachmentURL(picID, accountLogo[0].id, (100 * 60));
        }
        else
        {
            logoURL = 'NoLogo';
        }

        
        }
   
    public List<sDrivePictures> imgs {
        get{        
            if(imgs == null){
                        
               /* List<ID> parentIDs = new List<ID>();
                List<ID> fileObjects = new List<ID>();
                imgs = new List<sDrivePictures>();
                List<Account_Picture_File__c> pl = [Select id, Description__c FROM Account_Picture_File__c
                                                    WHERE WIP__c = false and Content_Type__c LIKE 'image/%' and Parent__c = :acct.Id ORDER BY LastModifiedDate DESC];
                
                for(Account_Picture_File__c apf : pl){
                    parentIDs.add(acct.id);
                    fileObjects.add(apf.id);
                }
                if(parentIDs.size() > 0 && fileObjects.size()  > 0 ){
                    for(String url :  cg.SDriveTools.getAttachmentURLs( parentIDs , fileObjects, (100 * 60))){                  
                        sDrivePictures sdp = new sDrivePictures();                  
                        sdp.url = url;
                        imgs.add(sdp);
                    }
                }*/
                for(Account_Picture_File__c apf:[Select url__c, Description__c FROM Account_Picture_File__c WHERE WIP__c = false and Content_Type__c LIKE 'image/%' and Parent__c = :acct.Id ORDER BY LastModifiedDate DESC]){
                    sDrivePictures sdp = new sDrivePictures();                  
                    sdp.url = apf.url__c;
                    imgs.add(sdp);
                }
                                          
            }
            return imgs;
        }
        set;
    }
    
    public class sDrivePictures{
        public string url {get;set;}
        public string description {get;set;}
    } 
    
    

    
}