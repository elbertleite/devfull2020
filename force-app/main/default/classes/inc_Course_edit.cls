public  class inc_Course_edit{

    public Campus_Course__c cc {get;Set;}
    public boolean edit {get;set;}
    public boolean showPackageMessage {get;set;}
    public String msgCampusNames {get;set;}

    public inc_Course_edit(ApexPages.StandardController controller){
        if(ApexPages.currentPage().getParameters().get('edit')=='true')
        	edit = true;
    	else
    		edit = false;
        cc = [Select C.Is_Available__c,  C.Minimum_age_requirement__c, C.Available_From__c, C.Average_Students_Class__c, C.Campus__c, C.Campus_Country__c, C.Name, C.Course__c, C.CreatedById,
                    C.CreatedDate, C.Language_Level_Required__c, C.LastModifiedById, C.LastModifiedDate, C.Length_of_each_Lesson__c, C.Id, C.is_Selected__c,
                    C.Unavailable_By__c, C.Unavailable_Date__c, C.Unavailable_Reason__c, C.Visa_Type__c, Period__c,
                    Course__r.Name,
                    Course__r.School_Course_Name__c,
                    Course__r.Course_Code__c,
                    Course__r.Package__c, Course__r.Course_Unit_Type__c, Course__r.Only_Sold_in_Blocks__c , Course__r.Language__c, Course__r.Course_Country__c,
                    Course__r.Course_Type__c,
                    Course__r.Type__c,
                    Course__r.Sub_Type__c,
                    Course__r.Course_Qualification__c,
                    Course__r.Period__c,
                    Course__r.Form_of_Delivery__c,
                    Course__r.Minimum_length__c,
                    Course__r.Maximum_length__c,
                    Course__r.Instalment_Option__c,
                    Course__r.Instalment_Interval__c,
                    Course__r.Payment_Rules__c,
                    Course__r.Important_information__c,
                    Course__r.Academic_requirements__c,
                    Course__r.Extra_requirements__c,
                    Course__r.IP_Tips__c,
                    Course__r.Why_choose_this_course__c,
					Course__r.Course_Category__c,
					Course__r.Hours_Week__c,
					Course__r.Optional_Hours_Per_Week__c,
					Course__r.Instalment_Term_Option__c,
					Course__r.Instalment_Term_Interval__c,
					Course__r.Course_Unit_Length__c,
					Course__r.Course_Lenght_in_Months__c,
					Course__r.Course_Description_Link__c,
					Course__r.Course_Description_File__c,
					campus__r.Parentid,
                    Package_Courses__c
                from Campus_Course__c C where id = :controller.getRecord().id];
        if(cc.Period__c != null){
			selectedPeriods.addAll(cc.Period__c.split(';'));
			viewPeriods = cc.Period__c;
        }

        if( ApexPages.currentPage().getparameters().get('pkg') != null && ApexPages.currentPage().getparameters().get('pkg') == 'true' )
        	showPackageMessage = true;
        else
        	showPackageMessage = false;

        getCampusCourses();
    }

	public boolean dataSaved {get{if(dataSaved == null) dataSaved = false; return dataSaved;}set;}

    public PageReference SaveCampusCourse() {

		dataSaved = false;

        if(cc.course__r.Package__c)
            cc.Package_Courses__c = fixPackageField(cc.Package_Courses__c);
        else
            cc.Package_Courses__c = null;

        system.debug('@# cc.Package_Courses__c: ' + cc.Package_Courses__c);

        if(cc.Course__r.Package__c && (cc.Package_Courses__c == null || cc.Package_Courses__c.trim() == '') ){
			ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'You must choose the courses in this package.');
            ApexPages.addMessage(Msg);
            return null;
		}

		if(cc.Course__r.Course_Category__c == 'Other' && cc.Course__r.Course_Unit_Length__c == null){
			ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Course Length is required for Category Other.');
            ApexPages.addMessage(Msg);
            return null;
		}
		if(cc.Course__r.Course_Category__c == 'Other' && cc.Course__r.Course_Lenght_in_Months__c == null){
			ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Course Length in Months is required for Category Other.');
            ApexPages.addMessage(Msg);
            return null;
		}

        if(!cc.Is_Available__c && ( cc.Unavailable_Reason__c == null || cc.Unavailable_Reason__c.trim() == '' )){
            ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Enter reason why course is not available.');
            ApexPages.addMessage(Msg);
            return null;
        }


		if(cc.Course__r.Course_Category__c == 'Language' && (cc.Course__r.Hours_Week__c == null || cc.Course__r.Hours_Week__c <= 0)){
           cc.Course__r.Hours_Week__c.addError('Hours Per Week is Required for Category Language');
            return null;
		}

		 if(selectedPeriods.isEmpty()){
	            ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Period is required.');
	            ApexPages.addMessage(Msg);
	            return null;
			}

        if(cc.Is_Available__c){
            cc.Unavailable_Reason__c = null;
            cc.Unavailable_By__c = null;
            cc.Unavailable_Date__c = null;
        } else {
           // Account userAccount = [Select Name from Account where User__c = :UserInfo.getuserid() limit 1];
            cc.Unavailable_By__c = UserInfo.getName();
            cc.Unavailable_Date__c = System.today();
        }
        cc.Period__c = String.join(selectedPeriods,';');

        UPDATE cc.course__r;
        UPDATE cc;

        /*PageReference acctPage = new PageReference('/apex/inc_Course_details?id='+cc.id);
        acctPage.setRedirect(true);
        return acctPage;*/


		boolean newCampusSelected = false;
        List<Campus_Course__c> newCampusCourses = new List<Campus_Course__c>();
        msgCampusNames = '<br/>';
        for(CampusCourseWrapper ccw : campusCourses){
        	if(ccw.isNew && ccw.selected){
        		Campus_Course__c newcc = cc.clone(false, true, false, false);
        		newcc.campus__c = ccw.campusid;
        		newCampusCourses.add(newcc);
        		newCampusSelected = true;
        		msgCampusNames += ccw.campusName + '<br/>';
        	}
    	}
    	insert newCampusCourses;


    	if(newCampusSelected){
    		showPackageMessage = true;
    	} else showPackageMessage = false;




		dataSaved = true;
		edit=false;
		viewPeriods = cc.Period__c;
		return null;


    }

    public void cancel(){
		edit=false;
		showPackageMessage = false;
	}

    /*public void cancel(){

        edit = false;

    }*/

	public list<String> selectedPeriods{get{if(selectedPeriods == null) selectedPeriods = new list<String>(); return selectedPeriods;} Set;}
	public string viewPeriods{get;set;}
	private List<SelectOption> periods;
	public List<SelectOption> getPeriods(){
		if(Periods == null){
       		Periods = new List<SelectOption>();
        	Schema.DescribeFieldResult fieldResult = Campus_Course__c.Period__c.getDescribe();
       		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
	        for( Schema.PicklistEntry f : ple)
	            Periods.add(new SelectOption(f.getLabel(), f.getValue()));
		}
        return Periods;
    }

    public class sDriveFiles{
    	public String URL {get;set;}
        public Account_Document_File__c File {get;set;}
    }

    private Map<String, sDriveFiles> mapFiles = new Map<String, sDriveFiles>();

    public List<SelectOption> Files {
        get{
            if(Files== null){

				String campusID = ApexPages.currentPage().getParameters().get('id');
				if(campusID != null){
	                Files = new List<SelectOption>();
	                List<ID> parentIDs = new List<ID>();
	                List<ID> fileObjects = new List<ID>();
	                files.add(new SelectOption('','-- Select a File --'));

	                Map<String, List<Account_Document_File__c>> campusFiles = new Map<String, List<Account_Document_File__c>>();
	                Map<String, String> folderNames = new Map<String, String>();
	                folderNames.put('','Not Classified');
	                campusFiles.put( '' , new List<Account_Document_File__c>() );
	                for(Account_Document_File__c folder : [SELECT id, File_Name__c FROM Account_Document_File__c WHERE Parent__c = :cc.Campus__c and Content_Type__c = 'Folder' and WIP__c = false order by Content_Type__c]){
	                    campusFiles.put( folder.id, new List<Account_Document_File__c>() );
	                    folderNames.put( Folder.id, Folder.File_Name__c );
	                }


	                for(Account_Document_File__c folder : [SELECT id, Parent_Folder_Id__c, Description__c, CreatedDate, File_Name__c,File_Size__c FROM Account_Document_File__c WHERE Parent__c = :cc.Campus__c and Content_Type__c != 'Folder' and WIP__c = false order by Content_Type__c]){
	                    List<Account_Document_File__c> ladf = campusFiles.get(Folder.Parent_Folder_Id__c == null ? '' : Folder.Parent_Folder_Id__c);
	                    ladf.add(folder);
	                    campusFiles.put(Folder.Parent_Folder_Id__c == null ? '' : Folder.Parent_Folder_Id__c, ladf);
	                }

	                for(String folderid : campusFiles.keyset()){
	                    if(!campusFiles.get(folderid).isEmpty()){

	                        files.add(new SelectOption('Folder',folderNames.get(folderid)));

	                        for(Account_Document_File__c file : campusFiles.get(folderid)){
	                            parentIDs.add(cc.Campus__c);
	                            fileObjects.add(file.id);
	                            sDriveFiles sdf = new sDriveFiles();
	                            sdf.File = file;
	                            mapFiles.put(file.id,sdf);
	                            files.add(new SelectOption(file.id, file.Description__c + ' [*' + file.File_Name__c + ' *]')  );

	                        }

	                        files.add(new SelectOption('disabled', ''));
	                    }
	                }

	                try {
	                    if(files.size() > 1)
	                        files.remove(files.size()-1);
	                } catch (Exception e){}


	                if(parentIDs.size() > 0 && fileObjects.size() > 0){
	                    try {
	                        List<String> urls = cg.SDriveTools.getAttachmentURLs( parentIDs , fileObjects, (100 * 60));

	                        for(string sd : mapFiles.keySet())
	                            for(String url : urls )
	                                if(url.contains(mapFiles.get(sd).file.id)){
	                                    mapFiles.get(sd).url = url;
	                                    break;
	                                }
	                    } catch (Exception e){}
	                }
				}
            }
            return Files;
        }
        set;
    }

	public void editCourse(){
		edit=true;
		showPackageMessage = false;
	}




	private List<CampusCourseWrapper> campusCourses;
	public List<CampusCourseWrapper> getCampusCourses(){
		if(campusCourses == null){
			campusCourses = new List<CampusCourseWrapper>();

			if(showPackageMessage)
				msgCampusNames = '<br/>';

			Map<ID, String> campusNames = new Map<ID, String>();
			for(Account campus : [select id, name from account where parentid = :cc.campus__r.Parentid order by Name])
				campusNames.put(campus.id, campus.name);

			for(campus_course__c ccc: [select id, is_selected__c, campus__c, campus__r.Name from campus_course__c where course__c = :cc.course__c order by campus__r.name]){
				campusCourses.add(new CampusCourseWrapper(true, ccc.campus__r.name, false, ccc.campus__c));
				campusNames.remove(ccc.campus__c);
				if(ccc.campus__c != cc.campus__c)
					msgCampusNames += ccc.Campus__r.Name + '<br/>';

			}
			for(String str : campusNames.keySet())
				campusCourses.add(new CampusCourseWrapper(false, campusNames.get(str), true, str));

		}
		return campusCourses;
	}


	public class CampusCourseWrapper {
		public boolean selected {get;set;}
		public boolean isNew {get;set;}
		public String campusName {get;set;}
		public String campusid {get;set;}
		public CampusCourseWrapper(boolean selected, String campusName, boolean isNew, String campusid){
			this.selected = selected;
			this.campusName = campusName;
			this.isNew = isNew;
			this.campusid = campusid;
		}
	}

    private List<SelectOption> courses;
    public List<SelectOption> getCourses(){
        if(courses == null){
            courses = new List<SelectOption>();
            for(Campus_Course__c cc : [select id, course__r.Name, course__r.Hours_Week__c, course__r.Optional_Hours_Per_Week__c, course__r.Course_Unit_Type__c, Is_Available__c
                                    from Campus_Course__c where campus__c = :cc.campus__c and course__r.Package__c = false order by course__r.Name]){
                String courseName = cc.course__r.Name + getCourseHours(cc.course__r);
    			if(!cc.Is_Available__c)
    				courseName += ' (unavailable)';
                courses.add(new SelectOption(cc.id, courseName));
            }
        }
        return courses;
    }



    private String getCourseHours(Course__c course){
        String courseHours = '';
        if(course.Hours_Week__c != null && course.Hours_Week__c != 0){
            courseHours = ' (' + Integer.valueOf(course.Hours_Week__c) + 'hrs';

            if(course.Optional_Hours_Per_Week__c != null && course.Optional_Hours_Per_Week__c != 0){
                courseHours += ' + ' + Integer.valueOf(course.Optional_Hours_Per_Week__c) + ' opt./' + course.Course_Unit_Type__c + ')';
            } else {
                courseHours += '/'+ course.Course_Unit_Type__c + ')';
            }

        }
        return courseHours;
    }

    private String fixPackageField(String toFix){
        if(toFix != null)
            return toFix.replaceAll('[^a-zA-Z0-9\\,]+', '');
        else return toFix;
    }





















}