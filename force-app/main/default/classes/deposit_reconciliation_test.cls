/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class deposit_reconciliation_test {

    static testMethod void myUnitTest() {
         
       TestFactory tf = new TestFactory();
       
       Account school = tf.createSchool();
       Account agency = tf.createAgency();
       Contact emp = tf.createEmployee(agency);
       User portalUser = tf.createPortalUser(emp);
       Account campus = tf.createCampus(school, agency);
       Course__c course = tf.createCourse();
       Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
       
       Test.startTest();
       system.runAs(portalUser){
       	   Contact client = tf.createLead(agency, emp);
	       client_course__c booking = tf.createBooking(client);
	       client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
	       
	       client_course__c deposit =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       deposit.isDeposit__c = true;
	       deposit.Deposit_Total_Available__c = 1000;
	       deposit.Deposit_Total_Used__c = 0;
	       deposit.Deposit_Total_Value__c = 1000;
	       update deposit;
	       
	       
	       client_course_instalment_payment__c pay1 = tf.addClientDepositPayment('Cash', agency, client, deposit, 500.0);
	       client_course_instalment_payment__c pay2 = tf.addClientDepositPayment('Creditcard', agency, client, deposit, 500.0);
	       
	       
	       deposit_reconciliation testClass = new deposit_reconciliation();
	       
	       List<SelectOption> paymentTypes = testClass.paymentTypes;
	       List<SelectOption> paymentStatus = testClass.paymentStatus;
	       List<SelectOption> agencyGroupOptions = testClass.agencyGroupOptions;
	       String selectedAgencyGroup = testClass.selectedAgencyGroup;
	       boolean showAll = testClass.showAll;
	       
	       testClass.changeGroup();
	       testClass.findDeposits();
	       
	       ApexPages.currentPage().getParameters().put('id', deposit.id);
	       testClass.confirmPayments();
	       
	       pay1.selected__c = true;
	       update pay1;

	       testClass.selectedPaymentStatus = 'confirmed';
	       testClass.SelectedPeriod = 'THIS_MONTH';
	       testClass.findDeposits();
	       
	       ApexPages.currentPage().getParameters().put('paymentid', pay1.id);
	       testClass.unconfirmPayment();
	       
       }
       Test.stopTest();
        
    }
}