/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class contact_school_emails_test {

    static testMethod void myUnitTest() {
    
    	TestFactory factory = new TestFactory();
		
		Account agency = factory.createAgency();
		Contact employee = factory.createEmployee(agency);
		Account school = factory.createSchool();
		Account campus = factory.createCampus(school, agency);		    
		Course__c course = factory.createCourse();
		Campus_Course__c cc = factory.createCampusCourse(campus, course);
		
		User userEmp = factory.createPortalUser(employee);
		
			
       system.runAs(userEmp){
	    	AWSKey__c testKey = new AWSKey__c(name='S3 Credential',key__c='key',secret__c='secret');
			insert testKey;
			
			Contact client = factory.createClient(agency);
			Quotation__c quote = factory.createQuotation(client, cc);		
			
			test.startTest();	        
		
			contact_school_emails emailProcess = new contact_school_emails(new ApexPages.StandardController(client));       
			emailProcess.emailsFromClient(client.Id);
			String allEmails = emailProcess.emailsJSON;
			emailProcess.qttEmails = 5;
			emailProcess.emailsJSON = null;
			allEmails = emailProcess.emailsJSON;
			test.stopTest();
	    	
		}
    }
}