public with sharing class filePath {
	
	public String path {get;set;}
	
	public filePath(ApexPages.StandardController controller){
		
		String obid = controller.getRecord().id;
		
		if(obid.startsWith('003')){
			
			Contact ct = [select id, Current_Agency__r.AgencyGroup__c from contact where id = :obid];
			path = ct.Current_Agency__r.AgencyGroup__c + '/Clients/' + ct.id + '/Pictures/'; 
			
		} else if(obid.startsWith('001')){
			
			Account agency = [select id, agencyGroup__c from account where id = :obid];
			path = agency.AgencyGroup__c + '/Logo/';
			
		}
		
	}	
}