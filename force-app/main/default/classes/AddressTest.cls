/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AddressTest {

    static testMethod void myUnitTest() {
    	
       	TestFactory tf = new TestFactory();
       
		Account agency = tf.createAgency();
        
       	Contact ct = tf.createEmployee(agency);
        
        Address__c addr = new Address__c();
        addr.Contact__c = ct.id;
        addr.Country__c = 'Australia';
        addr.State__c = 'NSW';
        addr.City__c = 'Sydney';
        addr.Street__c = '285 Clarence Street';
        addr.Suburb__c = 'CBD';
        addr.Post_Code__c = '2000';
        insert addr;
        
        addr.Street__c = '656 George Street';
        update addr;
        
        
        Apexpages.currentPage().getParameters().put('id',agency.id);
        Address ad = new Address();
        System.assertNotEquals(ad.acco, null);
        
        
        
       	Apexpages.currentPage().getParameters().put('id',ct.id);
        Address adContact = new Address();
        System.assertNotEquals(adContact.cont, null);
        System.assertNotEquals(adContact.contactAddress, null);
   
        
    }
}