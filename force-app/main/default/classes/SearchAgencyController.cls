public class SearchAgencyController {
	
	
	//public map<string, map<string, list<Account>>> mapAgencies {get;set;}
	
	public map<string, list<Account>> mapAgencies {get;set;}
	public map<string, list<string>> mapcity {get;set;}
	
	//Filters
	public List<SelectOption> countryFilter {get;set;}
	public List<SelectOption> cityFilter {get;set;}
	
	public String ScountryFilter {get;set;}
	public String ScityFilter {get;set;}
	
	public integer agNumber {get;set;}
	public integer groupNumber {get;set;}
	
	public SearchAgencyController(){
		getMapAgencies();
		getCountryFilter();
	}
	
	public void research(){
		mapAgencies = new map<string, list<Account>>();
		mapAgencies = getMapAgencies();
	}
	
	
	/*
	 * Get the list of Agencies grouped by agency group 
	 */
	public map<string, list<Account>> getMapAgencies(){
		List<Account> acc = new List<Account>();
		
		String sql = 'SELECT BillingCity, BillingCountry, ID,  AgencyGroup__c,  AgencyGroupName__c, Name FROM Account WHERE Recordtype.Name = \'agency\'  ' ;
	 	String sqlWhere = '';
	 	
	 	if(ScountryFilter!=null && !ScountryFilter.equals('--All--')){
	 		sqlWhere += ' and BillingCountry = \'' + ScountryFilter + '\' ' ;
	 	}else{
	 		sqlWhere += ' and BillingCountry != null ';
	 	}
	 	if(ScityFilter!=null && !ScityFilter.equals('--All--')){
	 		sqlWhere += ' and BillingCity = \'' + ScityFilter + '\' ' ;
	 	}else{
	 		sqlWhere += ' and Billingcity != null ';
	 	}
	 	
 		sqlWhere += ' order BY BillingCountry, Billingcity';
		
		sql+=sqlWhere;
		
		acc = Database.query(sql);

		mapAgencies = new map<string, list<Account>>();
				
		agNumber = 0;
		groupNumber = 0;
		for(Account ac : acc){
			if(!mapAgencies.containsKey(ac.AgencyGroupName__c)){
				mapAgencies.put(ac.AgencyGroupName__c, new list<Account>{ac});
				groupNumber++;
				}				
			else mapAgencies.get(ac.AgencyGroupName__c).add(ac);
			agNumber++;
		}
		
		
		
		
		return mapAgencies;
  }
	
	/*
	 * Get the list of Agencies grouped by agency group and city
	 */
	//public map<string, map<string, list<Account>>> getMapAgencies(){

		//mapAgencies = new map<string, map <string, list<Account>>>();		
		//for(Account ac : [SELECT BillingCity, BillingCountry, ID,  AgencyGroup__c,  AgencyGroupName__c, Name FROM Account WHERE Recordtype.Name = 'agency' and BillingCountry != null and Billingcity != null]){
		//	if(!mapAgencies.containsKey(ac.BillingCity))
		//		mapAgencies.put(ac.BillingCity, new map<string,list<Account>>{ac.AgencyGroupName__c => new list<Account>{ac}});				
		//	else if(!mapAgencies.get(ac.BillingCity).containsKey(ac.AgencyGroupName__c))
		//		mapAgencies.get(ac.BillingCity).put(ac.AgencyGroupName__c, new list<Account>{ac});
		//	else mapAgencies.get(ac.BillingCity).get(ac.AgencyGroupName__c).add(ac);
	//	}
		
	//	return mapAgencies;
  //}
	
	/*
	 * Get the list of countries and prepare the city map
	 */
	 public void getCountryFilter(){
	 	
	 	countryFilter = new list<SelectOption>();
		countryFilter.add(new Selectoption('--All--', '--All--'));
		
	 	cityFilter = new list<SelectOption>();
 	 	cityFilter.add(new Selectoption('--All--', '--All--'));
 	 	
	 	mapcity = new map<string, list<string>>(); //city map
	 	
	 	
	 	for(AggregateResult ar : [SELECT BillingCountry bcn, BillingCity bc FROM Account WHERE recordtype.name = 'agency' and billingCountry != null and billingcity != null GROUP BY BillingCountry, BillingCity order BY BillingCountry, BillingCity]){
	 		if(!mapcity.containsKey((string)ar.get('bcn'))){
	 			mapcity.put((string)ar.get('bcn'), new list<string>{(string)ar.get('bc')});		 
	 			countryFilter.add(new Selectoption((string)ar.get('bcn'), (string)ar.get('bcn')));		
	 		}else{
	 			mapcity.get((string)ar.get('bcn')).add((string)ar.get('bc'));
	 		}	 		
	 	}
	 }	 
	 
	 /*
	  * Set up the city filter using the selected country
	  */
	  public void changeCountryFilter(){
	  	cityFilter = new List<SelectOption>();
	  	cityFilter.add(new Selectoption('--All--', '--All--'));
	  	
	  	if(!ScountryFilter.equals('--All--')){
	  		for(String s : mapcity.get(ScountryFilter)){
			  	cityFilter.add(new Selectoption(s, s));
		  	}
	  	}
	  	
	  	ScityFilter = '--All--';
	  	  	
	  	getMapAgencies();
	  	
	  }
	  
	
}