/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AgencyDepartmentsController_test {

    static testMethod void myUnitTest() {
        
		TestFactory tf = new TestFactory();
		Account agency = tf.createAgency();
		Contact employee = tf.createEmployee(agency);
		User portalUser = tf.createPortalUser(employee);		

		Test.startTest();
		system.runAs(portalUser){
			
			AgencyDepartmentsController adc = new AgencyDepartmentsController(new ApexPages.StandardController(agency));
			List<User> staffList = adc.staffList;
			Department__c newDepartment = adc.newDepartment;
			
			Department__c dpt = new Department__c();
			dpt.Agency__c = agency.id;
			dpt.Name = 'Front Desk';
			insert dpt;
			
			String param = dpt.id + '-' + contact.id;
			ApexPages.currentpage().getParameters().put('deptList', param);
			adc.saveDepartments();
			
			ApexPages.currentpage().getParameters().put('deptID', dpt.id);
			adc.deleteDept();
			
			ApexPages.currentpage().getParameters().remove('deptList');
			adc.saveDepartments();
		}
		Test.stopTest();









    }
}