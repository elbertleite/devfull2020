global without sharing class IPFunctionsGlobal {
	
	global static PageReference recreateQuote(String recreateQuoteID, String nationality){
		xCourseSearch xcs = new xCourseSearch();
		xcs.recreateQuote(recreateQuoteID, nationality);
		
		PageReference pr = new PageReference('/courseCompare');
		String newPageContact = ApexPages.currentPage().getParameters().get('page');
		pr.getParameters().put('id', xcs.objCart.id);
		pr.getParameters().put('rec', 'true');
		if(!String.isEmpty(newPageContact) && newPageContact == 'contactNewPage'){
			pr.getParameters().put('page', 'contactNewPage');
		}
		
		pr.setRedirect(true);
		
		if(xcs.recreateMessage != null)
			pr.setCookies(new Cookie[]{new Cookie('recMsg', xcs.recreateMessage ,null, 30, true)});
		
		
		return pr;
	}
	
	global static List<Account> getAgencyGroups(String globalLink){
		return [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' and Global_Link__c = :globalLink order by Name ];
	}
	
	
	global static void deleteClientCart(String clientids){
		
		 delete [Select id from Web_Search__c where Client__c = :clientids];
		
	}

	
	
	//return school Destinations 
	global static List<SelectOption> CountryDestinations() {
		Set<string> ldestination = new Set<string>();
		for(Account a :[Select A.Id, BillingCountry from Account A where recordType.name = 'Campus'])			
			ldestination.add(a.BillingCountry);
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('','--None--'));
		for(String ld:ldestination)
			options.add(new SelectOption(ld,ld));
		options.sort();
		return options;
	}
	
	//Return list of courses that have been modified during the user's session
	global static String errorCoursesModified (list<client_course__c> coursesToUpdate){
		set<id> allIds = new set<id>();
		for(client_course__c cc : coursesToUpdate)
			allIds.add(cc.id);

		String courseError = '';
		map<Id, client_course__c> checkCourses = new map<Id, client_course__c>([SELECT Id, Client__r.Name, LastModifiedDate, LastModifiedBy.Name, Course_Name__c FROM client_course__c WHERE id in : allIds]);

		for(client_course__c cc : coursesToUpdate)
			if(cc.lastModifiedDate != checkCourses.get(cc.id).lastModifiedDate)
				courseError += '<br/>&#8226; <b> ' + checkCourses.get(cc.id).Client__r.Name + ' - ' + checkCourses.get(cc.id).Course_Name__c + ' </b> <span style="color:grey;"> by  ' + checkCourses.get(cc.id).LastModifiedBy.Name + ' on ' + checkCourses.get(cc.id).LastModifiedDate.format() + '</span>.';

		
		if(courseError.length()>0)
			return 'This action could not be saved because the following courses were modified during your session: ' + courseError;
		else
			return null;
	}

	
	
	

}