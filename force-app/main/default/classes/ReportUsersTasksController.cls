public with sharing class ReportUsersTasksController{

	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}

	private User uDet {get{
		if(uDet==null){
			uDet = [Select Id, Contact.AccountId, Contact.Account.ParentId from User where id = :UserInfo.getUserId()];
		}
		return uDet;
	}set;}
	
	public ReportUsersTasksController(){
		selectedAgency = uDet.Contact.AccountId;
	}
    
	private static string FormatSqlDate(date d){
		string day = string.valueOf(d.day());
		string month = string.valueOf(d.month());
		if (day.length() == 1)  
			day = '0'+day;
		if (month.length() == 1)    
			month = '0'+month;
		return d.year()+'-'+month+'-'+day;
	}
	
	public Contact fromDate {
		get{
			if(fromDate == null){
				fromDate = new Contact();
				fromDate.Expected_Travel_Date__c = system.today();
			}
			return fromDate;}
		set;}
			
	public Contact toDate {
		get{
			if(toDate == null){
				toDate = new Contact();
				toDate.Expected_Travel_Date__c = system.today();
			}
			return toDate;}
			set;}

    
	public List<AgencyWrapper> agencyList {
		get{
		if(agencyList == null)
			agencyList = new List<AgencyWrapper>();
		return agencyList;
	}
		set;
	}
    
    
	public class AgencyWrapper {
		public String agencyID {get;set;}
		public String agencyName {get;set;}
		public Integer completedTaskCount {get{if(completedTaskCount == null) completedTaskCount = 0; return completedTaskCount;}set;}
		public Integer overdueTaskCount {get{if(overdueTaskCount == null) overdueTaskCount = 0; return overdueTaskCount;}set;}
		public Integer openTaskCount {get{if(openTaskCount == null) openTaskCount = 0; return openTaskCount;}set;}
		public List<UserWrapper> userList {
			get{
			if(userList == null)
				userList = new List<UserWrapper>();
			return userList;
		}
			set;
		}
	}
    
	public class UserWrapper {
		public String userID {get;set;}
		public String userName {get;set;}
		public Integer completedtaskCount {get{if(completedtaskCount == null) completedtaskCount = 0; return completedtaskCount;}set;}
		public Integer overdueTaskCount {get{if(overdueTaskCount == null) overdueTaskCount = 0; return overdueTaskCount;}set;}
		public Integer openTaskCount {get{if(openTaskCount == null) openTaskCount = 0; return openTaskCount;}set;}
		
		public List<Custom_Note_Task__c> taskList {
			get{
			if(taskList == null)
				taskList = new List<Custom_Note_Task__c>();
			return taskList;
		}
			set;
		}
        
	}
	
	public Map<String, Contact> clientDetails {
		get{
		if(clientDetails == null)
			clientDetails = new Map<String, Contact>();
		
		return clientDetails;
	}
		set;
	}        
    
	map<string,string> selectedGroupName; 
	public String selectedAgencyGroup {get{if(selectedAgencyGroup == null) selectedAgencyGroup = uDet.Contact.Account.ParentId; return selectedAgencyGroup;}set;}
	public List<SelectOption> agencyGroupOptions {
		get{
			if(agencyGroupOptions == null){
				selectedGroupName = new map<string,string>();
				agencyGroupOptions = new List<SelectOption>();
				//agencyGroupOptions.add(new SelectOption('', 'Select Agency Group'));    
				for(Account ag : [select id, name from Account where RecordType.Name = 'Agency Group' order by Name]){
				agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
				selectedGroupName.put(ag.id, ag.Name);
		}
			}
		return agencyGroupOptions;
	}
		set;
	}
    
	public string getGroupName(){
		return  selectedGroupName.get(selectedAgencyGroup);
	}
    
    
	public void searchAgencies(){
		agencyOptions = null;
		selectedAgency = null;
		userOptions = null;
		selectedUser = null;
	}    
   
    
	public String selectedAgency {get{if(selectedAgency == null) selectedAgency = uDet.Contact.AccountId; return selectedAgency;}set;}
	public List<SelectOption> agencyOptions {
		get{
		if(agencyOptions == null){
			agencyOptions = new List<SelectOption>();
			agencyOptions.add(new SelectOption('all', '--All--'));
			agenciesId = new Set<ID>();
			for(Account ag : [Select Id, Name from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'Agency' order by Name]){
				agencyOptions.add(new SelectOption(ag.Id, ag.Name));   
				agenciesId.add(ag.Id);
			}//end for
		}
		return agencyOptions;
	}
		set;
	}
    
	public void clearUser(){
		selectedUser = null;
		userOptions = null;
	}
    
	List<id> lEmployee = new List<id>();

	private set<id> agenciesId;
    
	public list<String> selectedUser {get{if(selectedUser == null) selectedUser = new list<String>(); return selectedUser;}set;}
	public List<SelectOption> userOptions {
		get{
		if(userOptions == null){
            
			userOptions = new List<SelectOption>();
			List<SelectOption> ao = agencyOptions;
            
			List<User> lUsers = new List<User>();
        
			if(selectedAgency == null || selectedAgency == 'all')
				lUsers = [Select id, Name from User where IsActive = true and Contact.Chatter_Only__c = false and Contact.AccountId in :agenciesId order by name];
			else
			lUsers = [Select id, Name from User where IsActive = true and Contact.Chatter_Only__c = false and Contact.AccountId = :selectedAgency order by name];
            
			for(User ac : lUsers){
				userOptions.add(new SelectOption(ac.Id, ac.Name)); 
				lEmployee.add(ac.Id);	
			}       
		}
		return userOptions;
	}
		set;
	}
 
	public string selectedTaskType{get{if(selectedTaskType == null) selectedTaskType = 'all'; return selectedTaskType;} set;}
	public List<SelectOption> getTaskType() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('all','--All--'));
		options.add(new SelectOption('quote','Quotes Only'));
		options.add(new SelectOption('other','Other Tasks'));
		return options;
	}    
	
	public string selectedTaskSubject{get{if(selectedTaskSubject == null) selectedTaskSubject = 'all'; return selectedTaskSubject;} set;}
	public List<SelectOption> getTaskSubject() {
		List<SelectOption> options = new List<SelectOption>();
		UserDetails.GroupSettings gs = new UserDetails.GroupSettings();		
        options = gs.getTaskSubjects(uDet.Contact.Account.ParentId);
		options.add(0,new SelectOption('all','--All--'));
		options.add(new SelectOption('Quote Follow Up', 'Quote Follow Up'));
        options.add(new SelectOption('Web Enquiry', 'Web Enquiry'));
		options.add(new SelectOption('RDStation Conversion', 'RDStation Conversion'));
        options.add(new SelectOption('COE Received', 'COE Received'));
		return options;
	}    
    
	public string selectedTaskStatus{get{if(selectedTaskStatus == null) selectedTaskStatus = 'all'; return selectedTaskStatus;} set;}
	public List<SelectOption> getTaskStatus() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('all','--All--'));
		options.add(new SelectOption('Completed','Completed'));
		options.add(new SelectOption('In Progress','In Progress'));
		options.add(new SelectOption('OVERDUE','Overdue'));
		return options;
	}       
    
	public Map<String, String> agenciesMap { get;set; }
	Set<String> userIDs = new Set<String>();
	public void search(){       
		clientDetails = null;
		agencyList = null;
		List<AggregateResult> agencies;
        
		if(selectedAgencyGroup != null && selectedAgencyGroup != ''){
            
			agenciesMap = new Map<string, String>();
			if(selectedAgency == null || selectedAgency == 'all'){			
				for(Account ar : [Select A.Id, A.Name from Account A where ParentId = :selectedAgencyGroup order by Name]){
					agenciesMap.put(String.valueOf(ar.get('Id')), String.valueOf(ar.get('Name')));				
					AgencyWrapper aw = new AgencyWrapper();
					aw.agencyID = String.valueOf(ar.get('Id'));
					aw.agencyName = String.valueOf(ar.get('Name'));			
					agencyList.add(aw);
				}							
			} else {		
				for(Account a : [Select id, Name  from Account A where id = :selectedAgency]){
					agenciesMap.put(a.Id, a.Name);
					AgencyWrapper aw = new AgencyWrapper();
					aw.agencyID = a.id;
					aw.agencyName = a.Name;
					agencyList.add(aw);
				}				
			}
            
			system.debug('@@@@@@@@@@@@@@ selectedUser: ' + selectedUser);
            
			List<User> users;
			if(selectedUser.size() > 0)
				users = [Select Id, Name, Contact.AccountId from User where Id in :selectedUser order by name];
			else
				users = [Select Id, Name, Contact.AccountId from User where IsActive = true and Contact.Chatter_Only__c = false and  Contact.AccountId in :agenciesMap.keySet() order by name];
                
			userIDs = new Set<String>();
			
			for(User a : users){
				userIDs.add(a.Id);
				UserWrapper uw = new UserWrapper();
				uw.userID = a.Id;
				uw.userName = a.Name;
				for(AgencyWrapper aw: agencyList)
					if(aw.agencyID == a.Contact.AccountID)                     
						aw.userList.add(uw);
			}
			getNumberofTasksPerUser(userIDs);

			system.debug('FINAL AGENCYLIST '+JSON.serialize(agencyList));
		}        
	}
    
    
	public void getNumberofTasksPerUser(Set<String> userIDs){
		String sql = '';
		System.debug('==>selectedTaskStatus: '+selectedTaskStatus);
		// COMPLETED
		if(selectedTaskStatus == 'all' || selectedTaskStatus == 'Completed'){
			sql = 'select Assign_To__c, count(id) from Custom_Note_Task__c where Assign_To__c in :userIDs and isNote__c = false and Status__c = \'Completed\' ';
        
			if(selectedTaskStatus == 'OVERDUE')
				sql += ' and Due_Date__c < ' +System.now().format('yyyy-MM-dd');
			else{
				if(fromDate.Expected_Travel_Date__c != null)
					sql += ' and Due_Date__c >= ' + FormatSqlDate(fromDate.Expected_Travel_Date__c) + ' ';
				
				if(toDate.Expected_Travel_Date__c != null)
					sql += ' and Due_Date__c <= ' + FormatSqlDate(toDate.Expected_Travel_Date__c) + ' ';	
			}
		
			if(selectedTaskStatus != 'all' && selectedTaskStatus != 'OVERDUE')
				sql += ' and Status__c = \''+selectedTaskStatus+'\'';
		
			if(selectedTaskType == 'other'){
				if(selectedTaskSubject != 'all')
					sql += ' and Subject__c = \''+selectedTaskSubject+'\'';
				else            
					sql += ' and Subject__c != \'Quote Follow Up\'';
			}
			else if(selectedTaskType == 'quote')
				sql += ' and (Subject__c = \'Quote Follow Up\')';
            else
       			if(selectedTaskSubject != 'all')
					sql += ' and Subject__c = \''+selectedTaskSubject+'\'';
                
			sql+= ' group by Assign_To__c ';
        
			List<AggregateResult> completed = Database.query(sql);
        
			for(AggregateResult ar : completed)
				for(AgencyWrapper aw: agencyList)
					for(UserWrapper uw : aw.userList)
						if(String.valueOf(ar.get('Assign_To__c')) == uw.userID)
							uw.completedtaskCount = Integer.valueOf(ar.get('expr0'));
		}
        
		// OVERDUE //
		if(selectedTaskStatus == 'all' || selectedTaskStatus == 'OVERDUE'){
			sql = 'select Assign_To__c, count(id) from Custom_Note_Task__c where Assign_To__c in :userIDs and isNote__c = false and  Status__c != \'Completed\' and Due_Date__c < TODAY';
        
			if(selectedTaskStatus == 'OVERDUE')
				sql += ' and Due_Date__c < ' +System.now().format('yyyy-MM-dd');
			else{
				if(fromDate.Expected_Travel_Date__c != null)
					sql += ' and Due_Date__c >= ' + FormatSqlDate(fromDate.Expected_Travel_Date__c) + ' ';
				
				if(toDate.Expected_Travel_Date__c != null)
					sql += ' and Due_Date__c <= ' + FormatSqlDate(toDate.Expected_Travel_Date__c) + ' ';	
			}
        
			if(selectedTaskStatus != 'all' && selectedTaskStatus != 'OVERDUE')
				sql += ' and Status__c = \''+selectedTaskStatus+'\'';
		
			if(selectedTaskType == 'other'){
				if(selectedTaskSubject != 'all')
					sql += ' and Subject__c = \''+selectedTaskSubject+'\'';
				else            
					sql += ' and Subject__c != \'Quote Follow Up\'';
			}
			else if(selectedTaskType == 'quote')
				sql += ' and (Subject__c = \'Quote Follow Up\')';
           else
       			if(selectedTaskSubject != 'all')
					sql += ' and Subject__c = \''+selectedTaskSubject+'\'';    
					
			sql+= ' group by Assign_To__c ';
        
			List<AggregateResult> overdue = Database.query(sql);
        
			for(AggregateResult ar : overdue)
				for(AgencyWrapper aw: agencyList)
					for(UserWrapper uw : aw.userList)
						if(String.valueOf(ar.get('Assign_To__c')) == uw.userID)
							uw.overduetaskCount = Integer.valueOf(ar.get('expr0'));
		}
        
		//OPEN 
		if(selectedTaskStatus == 'all' || selectedTaskStatus == 'In Progress'){
			sql = 'select Assign_To__c, count(id) from Custom_Note_Task__c where Assign_To__c in :userIDs and isNote__c = false and Status__c != \'Completed\' ';
        
			if(selectedTaskStatus == 'OVERDUE')
				sql += ' and Due_Date__c < ' +System.now().format('yyyy-MM-dd');
			else{
				if(fromDate.Expected_Travel_Date__c != null)
					sql += ' and Due_Date__c >= ' + FormatSqlDate(fromDate.Expected_Travel_Date__c) + ' ';
				
				if(toDate.Expected_Travel_Date__c != null)
					sql += ' and Due_Date__c <= ' + FormatSqlDate(toDate.Expected_Travel_Date__c) + ' ';	
			}
              
			if(selectedTaskStatus != 'all' && selectedTaskStatus != 'OVERDUE')
				sql += ' and Status__c = \''+selectedTaskStatus+'\'';
		
			if(selectedTaskType == 'other'){
				if(selectedTaskSubject != 'all')
					sql += ' and Subject__c = \''+selectedTaskSubject+'\'';
				else            
					sql += ' and Subject__c != \'Quote Follow Up\'';
			}
			else if(selectedTaskType == 'quote')
				sql += ' and (Subject__c = \'Quote Follow Up\')';
            else
       			if(selectedTaskSubject != 'all')
					sql += ' and Subject__c = \''+selectedTaskSubject+'\'';
					    
			sql+= ' group by Assign_To__c ';
        
			system.debug('@@@ sql: ' + sql);
        
			List<AggregateResult> open = Database.query(sql);
        
			for(AggregateResult ar : open)
				for(AgencyWrapper aw: agencyList)
					for(UserWrapper uw : aw.userList)
						if(String.valueOf(ar.get('Assign_To__c')) == uw.userID)
							uw.opentaskCount = Integer.valueOf(ar.get('expr0'));
		}        
        
		//COUNT PER AGENCY
		for(AgencyWrapper aw: agencyList)
			for(UserWrapper uw : aw.userList){                  
				aw.completedTaskCount += uw.completedTaskCount;
				aw.overdueTaskCount += uw.overdueTaskCount;
				aw.opentaskCount += uw.opentaskCount;
			}
	}
	
	
	public void searchTasksByUser(){
        
		String theUserID = ApexPages.currentPage().getParameters().get('userID');
        
		if(theUserID != null && theUserID != ''){
            
			string sql = 'Select Related_Contact__c,Related_Contact__r.Name, Assign_To__c, Assign_To__r.Name, Related_To_Quote__r.Name, Status__c, Subject__c, Related_To_Quote__c, Due_Date__c, Comments__c, createdDate, lastModifiedDate from Custom_Note_Task__c ' +
				' where ';
        
			sql += ' Assign_To__c = \'' + theUserID + '\'';
            
			sql += ' and isNote__c = false ';

			if(selectedTaskStatus == 'OVERDUE')
				sql += ' and Status__c != \'Completed\' and Due_Date__c < ' +System.now().format('yyyy-MM-dd');
			else{
				if(fromDate.Expected_Travel_Date__c != null)
					sql += ' and Due_Date__c >= ' + FormatSqlDate(fromDate.Expected_Travel_Date__c) + ' ';
				
				if(toDate.Expected_Travel_Date__c != null)
					sql += ' and Due_Date__c <= ' + FormatSqlDate(toDate.Expected_Travel_Date__c) + ' ';	
			}
			
			if(selectedTaskStatus != 'all' && selectedTaskStatus != 'OVERDUE')
				sql += ' and Status__c = \''+selectedTaskStatus+'\'';
			
			if(selectedTaskType == 'other'){
				if(selectedTaskSubject != 'all')
					sql += ' and Subject__c = \''+selectedTaskSubject+'\'';
				else            
					sql += ' and Subject__c != \'Quote Follow Up\'';
			}
			else if(selectedTaskType == 'quote')
				sql += ' and (Subject__c = \'Quote Follow Up\')';
            
			else
       			if(selectedTaskSubject != 'all')
					sql += ' and Subject__c = \''+selectedTaskSubject+'\'';
					            
			sql += ' order by Due_Date__c, Related_Contact__r.Name, Status__c';                
                
			List<Custom_Note_Task__c> ts = Database.query(sql);
			
			Set<String> clientIds = new Set<String>();
			        
			System.debug('==> sql: '+sql);
			for(Custom_Note_Task__c t : ts)
				for(AgencyWrapper aw: agencyList)
					for(UserWrapper uw : aw.userList)
						if(uw.userID == t.Assign_To__c){							
							uw.taskList.add(t);
							if(t.Related_Contact__c != null)
								clientIds.add(t.Related_Contact__r.id);
						}
			
			String debug = '';
			for(String str : clientIds)
				debug += '\'' + str + '\',';
			
			system.debug('@@@@@@@@@@@@@@ str: ' + debug);
			
			list<Contact> la = [Select Id, (Select Client__c, Checklist_Item__c, Checklist_stage__c, createdDate, CreatedBy.name from Client_Checklist__r order by createdDate desc limit 1),
									(Select Agency__r.Name, Client__c, Stage__c, Stage_Item__c, Stage_Item_Id__c, createdDate, CreatedBy.name from Client_Stage_Follow_Up__r order by Last_Saved_Date_Time__c desc limit 1)
								From Contact where id in :clientIds];
			
			
			for( Contact ac : la ){
				for(Client_Stage_Follow_Up__c csf : ac.Client_Stage_Follow_Up__r)
					system.debug('@@@@@@@@@@@@@@ csf: ' + csf.CreatedDate + ' ** ' + csf.CreatedDate.millisecond() );
				if(!clientDetails.containsKey(ac.id))
					clientDetails.put(ac.id, ac);
			}
			clientDetails.put('',new Contact());
			clientDetails.put(null,new Contact());
			
			system.debug('@@@@@@@@@@@@@ milis=' + System.CurrentTimeMillis());
			
		}
	}
    
    //Graph
	public class tasksPerAgency{
		public string agency { get; set; }
		public Integer total { get; set; }
		public Integer totalEnroll { get; set; }
		public Integer totalOpen {get;set;}
		public tasksPerAgency( string agency , Integer total , Integer totalEnroll, Integer totalOpen) {
			this.agency = agency;
			this.total = total;
			this.totalEnroll = totalEnroll;
			this.totalOpen = totalOpen;
		}
	}
	public list<tasksPerAgency> gettasksPerAgency(){
		list<tasksPerAgency> lqpc = new list<tasksPerAgency>();
		tasksPerAgency qpc;

		if(agencyList.size() > 0 && (selectedAgency != null && selectedAgency != 'all')){
			for(AgencyWrapper aw:agencyList)
				for(UserWrapper uw:aw.userList){                    
					qpc = new tasksPerAgency(uw.userName,uw.completedTaskCount, uw.overdueTaskCount, uw.openTaskCount);                 
					lqpc.add(qpc);
				}
		}else if(selectedAgencyGroup != null && selectedAgencyGroup != ''){
			for(AgencyWrapper aw:agencyList){               
				qpc = new tasksPerAgency(aw.agencyName,aw.completedTaskCount, aw.overdueTaskCount, aw.openTaskCount);
				lqpc.add(qpc);
			}
		}
		return lqpc;
	}
	
	
	/*
	*	EXPORT METHODS	
	*/
	public PageReference generateExport(){
		PageReference pr = Page.report_usersTasks_Excel;
		pr.setRedirect(false);
		return pr;
	}
	
	public transient List<Custom_Note_Task__c> taskList {get;set;}
	public transient Map<String, List<Forms_of_Contact__c>> contactMobile {get;set;}  
	public void exportSearch(){
		string sql = 'Select Related_Contact__c, Assign_To__r.Contact.Account.Name, Assign_To__r.Name, Related_Contact__r.Status__c, Related_Contact__r.Name, Related_Contact__r.Nationality__c, Related_Contact__r.Email, Subject__c, Comments__c, Due_Date__c, Status__c, Priority__c, createdDate, lastModifiedDate '+ 
					' from Custom_Note_Task__c ' +
					' where  ';
        
        if(selectedUser != null && selectedUser.size() > 0)
        	sql += ' Assign_To__c in :userIDs ';
        else {
	        if(selectedAgency != 'all')
				sql += ' Assign_To__r.Contact.Accountid = \'' + selectedAgency + '\'';
			else
				sql += ' Assign_To__r.Contact.Account.Parentid = \'' + selectedAgencyGroup + '\'';
        }

		sql += ' and isNote__c = false ';
        
		if(selectedTaskStatus == 'OVERDUE')
			sql += ' and Status__c != \'Completed\' and Due_Date__c < ' +System.now().format('yyyy-MM-dd');
		else{
			if(fromDate.Expected_Travel_Date__c != null)
				sql += ' and Due_Date__c >= ' + FormatSqlDate(fromDate.Expected_Travel_Date__c) + ' ';
			
			if(toDate.Expected_Travel_Date__c != null)
				sql += ' and Due_Date__c <= ' + FormatSqlDate(toDate.Expected_Travel_Date__c) + ' ';	
		}
		
		if(selectedTaskStatus != 'all' && selectedTaskStatus != 'OVERDUE')
			sql += ' and Status__c = \''+selectedTaskStatus+'\'';
		
		if(selectedTaskType == 'other'){
			if(selectedTaskSubject != 'all')
				sql += ' and Subject__c = \''+selectedTaskSubject+'\'';
			else            
				sql += ' and Subject__c != \'Quote Follow Up\'';
		}
		else if(selectedTaskType == 'quote')
			sql += ' and (Subject__c = \'Quote Follow Up\')';
        
		else
   			if(selectedTaskSubject != 'all')
				sql += ' and Subject__c = \''+selectedTaskSubject+'\'';
				            
		sql += ' order by Due_Date__c, Related_Contact__r.Name, Status__c';                
            
		taskList = Database.query(sql);
		
		
		//get those mobile numbers -.-
		contactMobile = new Map<String, List<Forms_of_Contact__c>>();
		
		for(Custom_Note_Task__c cnt : taskList)
			contactMobile.put(cnt.Related_Contact__c, new List<Forms_of_Contact__c>());
		
		if(contactMobile.keySet().size() > 0){
			
			for(Forms_of_Contact__c foc : [select detail__c, Contact__c from forms_of_Contact__c where country__c = :myDetails.Account.BillingCountry and Type__c = 'Mobile' and contact__c in :contactMobile.keySet()])				
				contactMobile.get(foc.contact__c).add(foc);
		}
		
		
	}
	
	public Contact myDetails {
		get{
			if(myDetails == null)
				myDetails = UserDetails.getMyContactDetails();
			return myDetails;
		}
		set;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    
}