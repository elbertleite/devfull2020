@isTest
private class report_leads_not_interested_test{
	static testMethod void myUnitTest() {
		TestFactory tf = new TestFactory();
		
		Account agency = tf.createAgency();

		Contact emp = tf.createEmployee(agency);
		
		Contact lead = tf.createLead(agency, emp);
		lead.Ownership_History_Field__c = '[{"toUserID":"005O0000004bBaSIAU","toUser":"Patricia Greco","toAgencyID":"0019000001MjqIIAAZ","toAgency":"IP Australia Sydney (HO)","fromUserID":null,"fromUser":null,"fromAgencyID":null,"fromAgency":null,"createdDate":"2018-09-18T04:17:12.024Z","createdByUserID":"005O0000004bBaSIAU","createdByUser":"Patricia Greco","action":"Taken"}]';
		lead.Lead_Stage__c = 'Stage 1';
		lead.Lead_Area_of_Study__c = 'Test 1; Test 2';
		update lead;

		Contact lead2 = tf.createClient(agency);
		Contact lead3 = tf.createClient(agency);

		List<String> leads = new List<String>();
		leads.add(lead.ID);
		leads.add(lead2.ID);
		leads.add(lead3.ID);

		Contact client = tf.createClient(agency);

		User portalUser = tf.createPortalUser(emp);

		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);

       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);

		Quotation__c q1 = tf.createQuotation(lead, campusCourse);
		Quotation__c q2 = tf.createQuotation(lead, campusCourse);

		Destination_Tracking__c dt = new Destination_tracking__c();
		dt.Client__c = lead.id;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
		dt.Expected_Travel_Date__c = system.today().addMonths(3);
		dt.lead_Cycle__c = false;
		insert dt;

		Client_Checklist__c checklist = new Client_Checklist__c();
		checklist.Agency__c = agency.id;
		checklist.Agency_Group__c = agency.Parentid;
		checklist.Destination__c = lead.Destination_Country__c;
		checklist.Checklist_Item__c = 'aaa';
		checklist.Client__c = lead.id;
		checklist.Destination_Tracking__c = dt.id;
		insert checklist;

		Test.startTest();
     	system.runAs(portalUser){
		
			emp.Account = agency;
			portalUser.contact = emp;
			portalUser.Report_Leads_Lost_Filters__c = '{"statusStageSelected":"[\"NEW LEAD\",\"PROSPECT\",\"CONTACT\",\"QUOTE SENT\",\"NEGOTIATION\",\"CLOSING\"]","currentStage":"Stage 0","multipleStages":"false","multipleAgencies":"true","agencySelected":"[\"0019000001MjqHtAAJ\",\"0019000001MjqIJAAZ\",\"0019000001MjqHwAAJ\",\"0019000001tV3GtAAK\",\"0019000001MjqHvAAJ\",\"0012v00002BA2tiAAD\",\"0019000001MjqIUAAZ\",\"0019000001MjqIAAAZ\",\"0019000001MjqI9AAJ\",\"0019000001MjqIBAAZ\",\"0019000001MjqIHAAZ\",\"0019000001yAnbsAAC\",\"00190000027SkmIAAS\",\"0012v00002BAgydAAD\",\"0019000001MjqILAAZ\",\"0019000001MjqHuAAJ\",\"0019000001MjqIVAAZ\",\"0019000001MjqI3AAJ\",\"00190000027xJV8AAM\",\"0019000002APrMxAAL\",\"0019000001MjqIPAAZ\",\"0019000001MjqI2AAJ\",\"0019000001MjqITAAZ\"]","endDate":"21/01/2019","beginDate":"21/01/2018","groupSelected":"0019000001MjqDXAAZ"}';
			update portalUser;

			tf.createChecklists(portalUser.Contact.Account.Parentid);

			Client_Stage__c csc = new Client_Stage__c();
			csc.Stage_description__c = 'LEAD - Not Interested';
			csc.Agency_Group__c = portalUser.Contact.Account.Parentid;
			csc.Stage__c = 'Stage 0';
			csc.Stage_Sub_Options__c = '["Option Not Interested 1","Option Not Interested 2","Option Not Interested 3"]';
			insert csc;

			Client_Stage_Follow_up__c cs = new Client_Stage_Follow_up__c();
			cs.Agency__c = portalUser.Contact.Account.id;
			cs.Agency_Group__c = portalUser.Contact.Account.Parentid;
			cs.Client__c = lead.id;
			cs.Destination__c = 'Australia';
			cs.Stage_Item__c = csc.Stage_Description__c;
			cs.Stage_Item_Id__c = csc.id;
			cs.Stage__c = 'Stage 0';
			cs.Destination_Tracking__c = dt.id;
			cs.Last_Saved_Date_Time__c = Datetime.now();
			insert cs;
			
			csc = new Client_Stage__c();
			csc.Stage_description__c = 'Test';
			csc.Agency_Group__c = portalUser.Contact.Account.Parentid;
			csc.Stage__c = 'Stage 0';
			csc.Stage_Sub_Options__c = '["Option Not Interested 1","Option Not Interested 2","Option Not Interested 3"]';
			insert csc;

			cs = new Client_Stage_Follow_up__c();
			cs.Agency__c = portalUser.Contact.Account.id;
			cs.Agency_Group__c = portalUser.Contact.Account.Parentid;
			cs.Client__c = lead.id;
			cs.Destination__c = 'Australia';
			cs.Stage_Item__c = csc.Stage_Description__c;
			cs.Stage_Item_Id__c = csc.id;
			cs.Stage__c = 'Stage 0';
			cs.Destination_Tracking__c = dt.id;
			cs.Last_Saved_Date_Time__c = Datetime.now();
			insert cs;
			
			csc = new Client_Stage__c();
			csc.Stage_description__c = 'Test 2';
			csc.Agency_Group__c = portalUser.Contact.Account.Parentid;
			csc.Stage__c = 'Stage 0';
			insert csc;

			cs = new Client_Stage_Follow_up__c();
			cs.Agency__c = portalUser.Contact.Account.id;
			cs.Agency_Group__c = portalUser.Contact.Account.Parentid;
			cs.Client__c = lead.id;
			cs.Destination__c = 'Australia';
			cs.Stage_Item__c = csc.Stage_Description__c;
			cs.Stage_Item_Id__c = csc.id;
			cs.Stage__c = 'Stage 0';
			cs.Destination_Tracking__c = dt.id;
			cs.Last_Saved_Date_Time__c = Datetime.now();
			insert cs;
			
			csc = new Client_Stage__c();
			csc.Stage_description__c = 'Test 3';
			csc.Agency_Group__c = portalUser.Contact.Account.Parentid;
			csc.Stage__c = 'Stage 0';
			insert csc;

			cs = new Client_Stage_Follow_up__c();
			cs.Agency__c = portalUser.Contact.Account.id;
			cs.Agency_Group__c = portalUser.Contact.Account.Parentid;
			cs.Client__c = lead.id;
			cs.Destination__c = 'Australia';
			cs.Stage_Item__c = csc.Stage_Description__c;
			cs.Stage_Item_Id__c = csc.id;
			cs.Stage__c = 'Stage 0';
			cs.Destination_Tracking__c = dt.id;
			cs.Last_Saved_Date_Time__c = Datetime.now();
			insert cs;
			
			report_leads_not_interested controller = new report_leads_not_interested();
			controller.updateAgencies();
			controller.updateEmployees();
			controller.getDestinations();
			controller.getGroups();
			controller.getReasons();
			//controller.getStagesString();
			controller.updateStages();
			controller.checkStages();
			controller.generateReports();
			controller.loadAgencies(agency.ID, agency.Parentid);

			Apexpages.currentPage().getParameters().put('export', 'true');
			Apexpages.currentPage().getParameters().put('begin', Date.today().format());
			Apexpages.currentPage().getParameters().put('end', Date.today().addMonths(-1).format());
			Apexpages.currentPage().getParameters().put('group', portalUser.Contact.Account.Parentid);
			Apexpages.currentPage().getParameters().put('agency', portalUser.Contact.Account.id);
			Apexpages.currentPage().getParameters().put('destination', 'Australia');
			Apexpages.currentPage().getParameters().put('stages', 'Stage 0');
			
			controller.generateListContacts();

			controller.updateFilterDates();
			controller.showHideExtraFilters();

			controller = new report_leads_not_interested();

			report_leads_not_interested.ContactReport cr = new report_leads_not_interested.ContactReport();
			cr.Id = '';
			cr.Name = null;
			cr.Email = null;
			cr.Phone = null;
			cr.CreatedDate = null;
			cr.CreatedBy = null;
			cr.LostCancelledDate = null;
			cr.visaExpiryDate = null;
			cr.TimebeforeLost = null;
			cr.Stage = null;				
			cr.StatusBeforeCancelledLost = null;							
			cr.Destination = null;				
			cr.AssignedTo = null;				
			cr.CurrentAgency = null;
			cr.CreatedAgency = null;
			cr.agencyGroup = null;
			cr.Reason = null;
			cr.Comment = null;
			cr.active = null;

			cr.equals(cr);
			cr.hashCode();

			report_leads_not_interested.ReportResult rr = new report_leads_not_interested.ReportResult();
			rr.status = 'null';
			rr.lost = null;
			rr.won = null;
			rr.colorWon = null;
			rr.colorLost = null;

			rr.equals(rr);
			rr.hashCode();


			report_leads_not_interested.StageFilter sf = new report_leads_not_interested.StageFilter();
			sf.stage = '';
			sf.compareTo(sf);

			report_leads_not_interested.StatusFilter sf2 = new report_leads_not_interested.StatusFilter();
			sf2.id = '';
			sf2.compareTo(sf2);

			report_leads_not_interested.ReferenceDateByCheck rdc = new report_leads_not_interested.ReferenceDateByCheck(Decimal.valueOf(10), Datetime.now(), null);
			rdc.itemOrder = Decimal.valueOf(10);
			rdc.dateChecked = Datetime.now();
			rdc.item = null;

			rdc.compareTo(rdc);
			rdc.equals(rdc);
			rdc.hashCode();

		}
	}
}