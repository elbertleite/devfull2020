public class ContactUserController {

	public Contact nContact {get;set;}
	public User nUser {get;set;}
	public boolean createUser {get;set;}
	public boolean showError {get;set;}
	private map<string,string> recordType;
	public boolean edit {get;set;}
	/*** Constructor ***/
	public ContactUserController(){
		showError = false;
		edit = false;
		recordType = IPFunctions.getRecordsType();
		
		if(ApexPages.currentPage().getParameters().get('id')!=null && ApexPages.currentPage().getParameters().get('id') != ''){
			nContact = new Contact();
			nContact.AccountId = ApexPages.currentPage().getParameters().get('id');
			nContact.RecordTypeId = recordType.get('Employee');
			system.debug('Agency ID==>' + nContact.AccountId);
			
			nUser = new User();
			nUser.EmailEncodingKey = 'ISO-8859-1';
			nUser.TimeZoneSidKey = 'Australia/Sydney';
			nUser.LocaleSidKey = 'en_AU';
			nUser.LanguageLocaleKey = 'en_US';
			
		}else if(ApexPages.currentPage().getParameters().get('ct')!=null && ApexPages.currentPage().getParameters().get('ct') != ''){
			edit = true;
			nContact = [Select Id, FirstName, LastName, Email, AccountId From Contact Where id = :ApexPages.currentPage().getParameters().get('ct')];		
			list<User> lUser = [Select Id, Alias, UserName,CommunityNickname, ProfileId, LocaleSidKey, TimeZoneSidKey, LanguageLocaleKey, EmailEncodingKey, IsActive, AccountId, ContactId From User where ContactId = :nContact.Id];			
			if(lUser.size()>0){
				nUser = lUser[0];
				createUser = true;
			}
			else{
				nUser = new User();
				nUser.EmailEncodingKey = 'ISO-8859-1';
				nUser.isActive = true;
				nUser.TimeZoneSidKey = 'Australia/Sydney';
				nUser.LocaleSidKey = 'en_AU';
				nUser.LanguageLocaleKey = 'en_US';
				createUser = false;
			}
		}else{
			return;
		}
		
	}
	
	/*** Save ***/
	public void saveContact(){
		showError = true;
		system.savepoint save;
		
		try{			
			save = database.setSavepoint();
			upsert nContact;			
			if(createUser){		
				if(nUser.ContactId!=null){
					List<String> jUser = new List<String>();
					nUser.FirstName = nContact.FirstName;
					nUser.LastName = nContact.LastName;
					nUser.Email = nContact.Email;		
					nUser.isCustomForm__c = true;
					jUser.add(JSON.serialize(nUser));
					
					updateUser(jUser);
				}else{
					nUser.ContactId = nContact.Id;	
					nUser.FirstName = nContact.FirstName;
					nUser.LastName = nContact.LastName;
					nUser.Email = nContact.Email;			
					insert nUser;
				}
			}
		}catch (Exception e){
			database.rollback(save);
			system.debug('Exception ==>' + e);
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,String.valueOf(e)));
			return;
		}
	}
	
	@future
  	static void updateUser(List<String> jUser){		
		try{
			for(String ser : jUser){
				User nUser =  (User) JSON.deserialize(ser, User.class);
				system.debug('User==>' + nuser);
				update nUser;
			}		
		}
		catch (Exception e){
			system.debug('Exception ==>' + e);
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,String.valueOf(e)));
			return;
		}
	}
	
	/*** Get Profiles ***/
	public list<SelectOption> getprofiles() {
		list<SelectOption> l = new list<SelectOption>();			
		for(Profile p : [SELECT Id, Name FROM Profile Where UserType = 'CspLitePortal' order by name]){
			l.add(new SelectOption(p.Id, p.Name));
		}
		
		return l;
	}
}