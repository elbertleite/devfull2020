/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class extPageDispatcher_test {

     static testMethod void myUnitTest() {
        
        
        Map<String,String> recordTypes = new Map<String,String>();
    	
    	for( RecordType r :[select id, name from recordtype where isActive = true] )
    		recordTypes.put(r.Name,r.Id);
    		
    	User u = [select AccountId from User where id = :Userinfo.getUserId()];
    	
    	Account school = new Account();
    	school.recordtypeid = recordTypes.get('School');
    	school.name = 'Test School';
    	school.User__c = u.id;
    	insert school;
    	
        Contact ct = new Contact();
        ct.accountid = school.id;
        ct.FirstName = 'John';
		ct.RecordTypeId = recordTypes.get('Lead');
        ct.LastName = 'Doe';
        ct.email = 'jt@test.com';       	
        insert ct;
        
    	Account agency = new Account();
    	agency.RecordTypeId = recordTypes.get('Agency');
    	agency.name = 'IP Sydney';    	 	
    	insert agency;
    	
    	Account campus = new Account();
    	campus.RecordTypeId = recordTypes.get('Campus');
    	campus.Name = 'Test Campus CBD';
    	campus.ParentId = school.id;
    	insert campus;
    	
    	
    	Supplier__c supplier = new Supplier__c();
    	supplier.Agency__c = agency.id;
    	supplier.Supplier__c = school.id;
    	insert supplier;
    	
    
    	extPageDispatcher epd = new extPageDispatcher(new Apexpages.Standardcontroller(ct));
    	ApexPages.currentPage().getParameters().put('id',school.id);
    	epd.getRedir();
    	ApexPages.currentPage().getParameters().put('id',agency.id);
    	epd.getRedir();
    	ApexPages.currentPage().getParameters().put('id',campus.id);
    	epd.getRedir();
    	ApexPages.currentPage().getParameters().put('id',ct.id);
    	epd.getRedir();

    	extPageDispatcherEdit epdEdit = new extPageDispatcherEdit(new Apexpages.Standardcontroller(ct));
    	ApexPages.currentPage().getParameters().put('id',school.id);
    	epdEdit.getRedir();
    	ApexPages.currentPage().getParameters().put('id',agency.id);
    	epdEdit.getRedir();
    	ApexPages.currentPage().getParameters().put('id',campus.id);
    	epdEdit.getRedir();
    	ApexPages.currentPage().getParameters().put('id',ct.id);
    	epdEdit.getRedir();
    	
    }
}