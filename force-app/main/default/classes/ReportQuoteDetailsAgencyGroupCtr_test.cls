/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ReportQuoteDetailsAgencyGroupCtr_test {

    static testMethod void myUnitTest() {
        
		Testfactory tf = new TestFactory();
		
		Account agency = tf.createAgency();
		
		Contact employee = tf.createEmployee(agency);
		User portalUser = tf.createPortalUser(employee);
		
		
        
		Account school = tf.createSchool();
		       
		Account campus = tf.createCampus(school, agency);
        
        Course__c course = new Course__c();
		course.Name = 'Certificate III in Business';        
		course.Type__c = 'Business';
		course.Sub_Type__c = 'Business';
		course.Course_Qualification__c = 'Certificate III';
		course.Course_Type__c = 'VET';
		course.Course_Unit_Type__c = 'Week';
		course.Only_Sold_in_Blocks__c = true;
		course.school__c = school.id;
		insert course;
       
		Campus_Course__c cc = new Campus_Course__c();
		cc.Course__c = course.Id;
		cc.Campus__c = campus.Id;        
		cc.Is_Available__c = true;        
		insert cc;
		
		
		
		
		
		system.runAs(portalUser){
			
			Contact lead = tf.createLead(agency, employee);
			Contact client = tf.createClient(agency);
			Quotation__c quote = tf.createQuotation(client, cc);
			Quotation__c quote2 = tf.createQuotation(lead, cc);
			
			ReportQuoteDetailsAgencyGroupController report = new ReportQuoteDetailsAgencyGroupController();
			Contact fromDate = report.fromDate;
			Contact toDate = report.toDate;
			
		 	report.fromDate.Expected_Travel_Date__c = system.today().addDays(-1);
			report.toDate.Expected_Travel_Date__c = system.today().addDays(1);
			
			report.searchCountry();
			
			report.searchCourseNames();
			report.sCourseFilter = course.Id;
			
			String selectedQuoteType = report.selectedQuoteType;
	       	List<SelectOption> quoteType = report.quoteType;
	       	report.selectedQuoteType = 'std';
			
			List<SelectOption> CountryOptions = report.CountryOptions;
			String selCountry = ReportQuoteDetailsAgencyGroupController.selectedQuoteCountry;
			ReportQuoteDetailsAgencyGroupController.selectedQuoteCountry = 'Australia';
			String selCity = ReportQuoteDetailsAgencyGroupController.selectedQuoteCity;
	        List<SelectOption> CityOptions = report.CityOptions;
	        ReportQuoteDetailsAgencyGroupController.selectedQuoteCity = 'Sydney';
	        
	        String selectedAgencyGroup = report.selectedAgencyGroup;
	        List<SelectOption> agencyGroupOptions = report.agencyGroupOptions;
	        report.selectedAgencyGroup = agency.parentId;
	        
	        report.getGroupName();
	        
	        report.searchAgencies();
	        
	        
	        String selectedAgency = report.selectedAgency;
	        List<SelectOption> agencyOptions = report.agencyOptions;
	        report.selectedAgency = agency.Id;
	        
	        report.searchUsers();
	        
	        list<String> selectedUser = report.selectedUser;
	        List<SelectOption> userOptions = report.userOptions;
	        report.selectedUser.add(portalUser.id);
	        
	        String selectedSchool = report.selectedSchool;
	        List<SelectOption> schools = report.schools;
	       	report.selectedSchool = school.id;
	       	List<SelectOption> coursesFilter = report.coursesFilter;
	       	
	       	
	       	
	       /*	String selectedCourseType = report.selectedCourseType;
	       	List<SelectOption> courseTypes = report.courseTypes;
	       	report.selectedCourseType = course.Course_Type__c;
	        
	        String selectedCourseField = report.selectedCourseField;
	       	List<SelectOption> courseFields = report.courseFields;
	       	*/
	       	
	        List<ReportQuoteDetailsAgencyGroupController.AgencyWrapper> agencyList = report.agencyList;
	        
	       
	        Apexpages.currentPage().getParameters().put('theUserID', portalUser.id);
	        
	        report.search();
	        report.searchQuotesByUser();
	        
	        
	        list<ReportQuoteDetailsAgencyGroupController.quotesPerAgency> quotesPerAgency = report.getquotesPerAgency();
	        
	        report.quotesPerCourseExcel();
	        report.quotesCourseExcelSearch();
	        
	        
	        
	        //report.fromDate.Expected_Travel_Date__c = null;
			//report.toDate.Expected_Travel_Date__c = null;
			ReportQuoteDetailsAgencyGroupController.selectedQuoteCountry = null;
			ReportQuoteDetailsAgencyGroupController.selectedQuoteCity = null;
			report.selectedSchool = null;
			
			report.search();
			
			ApexPages.currentPage().getParameters().put('cm', campus.id);
	       	ApexPages.currentPage().getParameters().put('cr', course.id);
	       	ApexPages.currentPage().getParameters().put('name', course.name);
	       	ApexPages.currentPage().getParameters().put('fk', 'SEA Sydney Manly');
	       	ApexPages.currentPage().getParameters().put('sk', 'IELTS');
	       	report.searchQuotesByCourse();
			
		}
        
    }
}