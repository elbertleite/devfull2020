@RestResource(urlMapping = '/sms/*')
global class TwilioSMSCallBack {

	@HttpPost
	global static String doPost(){

		RestRequest rec = restContext.request;
		//RestResponse res = restContext.response;

		system.debug('responsePost===' + rec);

		String status = rec.params.get('SmsStatus');
		String errorCode = (status=='failed'||status=='undelivered') ? rec.params.get('ErrorCode') : '-';
		String msgId = rec.params.get('MessageSid');
		String toNumber = rec.params.get('To');
		system.debug('MessageSid===' + msgId);
		try{

			Email_SMS__c sms = [Select Body__c, Subject__c, Client__c, client_course_instalment__c, Invoice__c, From__c, From__r.Name, Id from Email_SMS__c where msgId__c = :msgId limit 1]; // info on Object

			DateTime currentTime = DateTime.now();

			/** Save SMS into S3 **/
			String bucketName = Userinfo.getOrganizationId();

			User fromEP = [Select Contact.Account.Global_Link__c from User where id = :sms.From__c limit 1];

			String fileName = IPFunctions.SMSInstalmentlFileNameS3(fromEP.Contact.Account.Global_Link__c, sms.Client__c);

			String smsInfo =  sms.client_course_instalment__c + ';;' +toNumber+ ';;' + sms.Subject__c +';;' +currentTime.format('yyyy-MM-dd HH:mm:ss') + ';;' + status + ';;' + errorCode+ + ';;'+ sms.From__r.Name;

			fileName += smsInfo;
			system.debug('fileName===' + fileName);

			if(!Test.isRunningTest()){
				//Initialize controller to save file in Amazon S3
				S3Controller cS3 = new S3Controller();
				cS3.constructor(); //Generate Credentials

				List<String> allbuck= cS3.allBuckets;

				//Insert File
				cS3.insertFileS3(Blob.valueOf(sms.Body__c), bucketName, fileName, null);
			}


			/** END -- Save sms into S3 **/


			/** Save instalment activity **/
			//if(status=='sent' || status =='delivered'){

			if(sms.client_course_instalment__c!=null){
				client_course_instalment__c instalment = [Select Id, instalment_activities__c from client_course_instalment__c where id = :sms.client_course_instalment__c limit 1];

				if(instalment.instalment_activities__c==null || instalment.instalment_activities__c=='')
				instalment.instalment_activities__c = 'SMS;;' + toNumber+ ';;' + sms.Subject__c +';;' + currentTime.format('yyyy-MM-dd HH:mm:ss') + ';;' + status + ';;' + errorCode + ';;'+ sms.From__r.Name + '!#!' ;
				else
				instalment.instalment_activities__c += 'SMS;;' + toNumber+ ';;' + sms.Subject__c +';;' + currentTime.format('yyyy-MM-dd HH:mm:ss') + ';;' + status + ';;' + errorCode + ';;'+ sms.From__r.Name + '!#!' ;
				update instalment;
			}
			else{
				Invoice__c invoice = [Select Id, invoice_activities__c from Invoice__c where id = :sms.Invoice__c limit 1];

				if(invoice.invoice_activities__c==null || invoice.invoice_activities__c =='')
				invoice.invoice_activities__c = 'SMS;;' + toNumber+ ';;' + sms.Subject__c +';;' + currentTime.format('yyyy-MM-dd HH:mm:ss') + ';;' + status + ';;' + errorCode + ';;'+ sms.From__r.Name + '!#!' ;
				else
				invoice.invoice_activities__c += 'SMS;;' + toNumber+ ';;' + sms.Subject__c +';;' + currentTime.format('yyyy-MM-dd HH:mm:ss') + ';;' + status + ';;' + errorCode + ';;'+ sms.From__r.Name + '!#!' ;
				update invoice;
			}

			//}
			/** END -- Save instalment activity **/

			delete sms;//delete data on Object
			return '';
		}catch(Exception e ){
			String msg = 'Error===>' + e + ' Line:' + e.getLineNumber();
			return '';
		}
	}
}