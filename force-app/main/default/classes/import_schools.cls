public class import_schools {
	 public Blob csvFileBody{get;set;}
	 public string csvAsString{get;set;}
	 public String[] csvFileLines{get;set;}
	 map<string,string> listRecordsType;
	 map<string,string> period;
	 public import_schools(){
	    csvFileLines = new String[]{};
	    
	  }
	  
	  public void importCSVFile(){
	       //try{
	       		listRecordsType = IPFunctions.getRecordsType();
	       	   set<string> schools = new set<string>();
	       	   set<string> campus = new set<string>();
	       	   set<string> schoolCampus = new set<string>();
			   period = new map<string,string>();
	           csvAsString = csvFileBody.toString();
	           csvFileLines = csvAsString.split('\n'); 
	           for(Integer i=1;i<csvFileLines.size();i++){
	               string[] csvRecordData = csvFileLines[i].split(',');
		           System.debug('==>csvRecordData: '+csvRecordData);
				   System.debug('==>csvRecordData[20]: '+csvRecordData[20]);
				   System.debug('==>csvRecordData[21]: '+csvRecordData[21]);
				   System.debug('==>csvRecordData[22]: '+csvRecordData[22]);
				   System.debug('==>csvRecordData[23]: '+csvRecordData[23]);

	               //Add School
	               if(!schools.contains(csvRecordData[0].trim())){
	               		listSchools.add(new account(name = csvRecordData[0].trim(), Registration_name__c = csvRecordData[1].trim(), recordTypeId = listRecordsType.get('School')));
	               		schools.add(csvRecordData[0].trim());
	               }
	               //Add Campus
	               if(!mapCampuses.containsKey(csvRecordData[0].trim())){
	               		mapCampuses.put(csvRecordData[0], new list<account>{new account(
	               											recordTypeId = listRecordsType.get('Campus'),
										               		Name = csvRecordData[2].trim(),
															Website = csvRecordData[3].trim(),
															BusinessEmail__c = csvRecordData[4].trim(),
															Account_Currency_Iso_Code__c = csvRecordData[5].trim(),
															BillingCity = csvRecordData[6].trim(),
															BillingCountry = csvRecordData[7].trim(),
															Disabled_Campus__c = false,
															Main_Campus__c = false)});
						schoolCampus.add(csvRecordData[0].trim()+csvRecordData[2].trim());
						period.put(csvRecordData[2],csvRecordData[16]);
						System.debug('==>campus00: '+csvRecordData[0].trim()+csvRecordData[2].trim());
	               }else if(!schoolCampus.contains(csvRecordData[0].trim()+csvRecordData[2].trim())){
	               		System.debug('==>campus: '+csvRecordData[0].trim()+csvRecordData[2].trim()); 
	               		mapCampuses.get(csvRecordData[0].trim()).add(new account(
	               											recordTypeId = listRecordsType.get('Campus'),
										               		Name = csvRecordData[2].trim(),
															Website = csvRecordData[3].trim(),
															BusinessEmail__c = csvRecordData[4].trim(),
															Account_Currency_Iso_Code__c = csvRecordData[5].trim(),
															BillingCity = csvRecordData[6].trim(),
															BillingCountry = csvRecordData[7].trim(),
															Disabled_Campus__c = false,
															Main_Campus__c = false));
						schoolCampus.add(csvRecordData[0].trim()+csvRecordData[2].trim());
						period.put(csvRecordData[2],csvRecordData[16]);
	               }
	               
	               
	               if(!mapCoursesDetails.containsKey(csvRecordData[0].trim()+csvRecordData[2].trim())){
		               		mapCoursesDetails.put(csvRecordData[0]+csvRecordData[2], new list<courseDetails>{new courseDetails(csvRecordData[0],csvRecordData[2],csvRecordData[10],csvRecordData[11],csvRecordData[12],csvRecordData[13],csvRecordData[14],csvRecordData[15],csvRecordData[17],csvRecordData[18],csvRecordData[19],csvRecordData[20],csvRecordData[21],csvRecordData[22],csvRecordData[23],csvRecordData[24],csvRecordData[25])});
	               }else{
	               	 mapCoursesDetails.get(csvRecordData[0]+csvRecordData[2]).add(new courseDetails(csvRecordData[0],csvRecordData[2],csvRecordData[10],csvRecordData[11],csvRecordData[12],csvRecordData[13],csvRecordData[14],csvRecordData[15],csvRecordData[17],csvRecordData[18],csvRecordData[19],csvRecordData[20],csvRecordData[21],csvRecordData[22],csvRecordData[23],csvRecordData[24],csvRecordData[25]));
	               }
	           }
	           
	           Database.SaveResult[] srSchools = Database.insert(listSchools, true); //no partial proccess
               for (Database.SaveResult sr : srSchools)
               	  schoolIds.add(sr.getId());
               System.debug('==>schoolIds: '+schoolIds);  
               
               for(Account sch:[Select id, name from account where id in:schoolIds]){
               		if(mapCampuses.containsKey(sch.name)){
               			for(Account cp:mapCampuses.get(sch.name)){
               				account newCampi = cp;
               				newCampi.ParentId = sch.id;
               				listCampus.add(newCampi);
               				System.debug('==>newCampi: '+newCampi); 
               			}
               		}
               			
               }
               
               Database.SaveResult[] srCampus = Database.insert(listCampus, true); //no partial proccess
               for (Database.SaveResult sr : srCampus)
               	  campusIds.add(sr.getId());
	           System.debug('==>campusIds: '+campusIds);  
	           
	           set<string> schoolCourseNew = new set<string>();
	           
	           for(Account cs:[Select id, name, Parent.Name, ParentId, BillingCountry from account where id in:campusIds]){
               		if(mapCoursesDetails.containsKey(cs.Parent.Name+cs.name)){
               			for(courseDetails cd:mapCoursesDetails.get(cs.Parent.Name+cs.name)){
               				cd.campusid = cs.id;
               				cd.schoolId = cs.parentId;
							cd.campusCoursePeriod = period.get(cs.name);
               				if(!schoolCourseNew.contains(cs.parentId+cd.NameCourse)){
	               				Course__c newCourse = new Course__c(School__c = cs.ParentId,
																	Name = cd.NameCourse,
																	Course_Country__c = cs.BillingCountry,
																	School_Course_Name__c = cd.School_Course_Name,
																	Course_Category__c = cd.Course_Category,
																	Course_Unit_Type__c = cd.Course_Unit_Type,
																	Course_Type__c = cd.Course_Type,
																	Type__c = cd.Course_field,
																	Only_Sold_in_Blocks__c = cd.Only_Sold_in_Blocks,
																	Minimum_length__c = cd.Minimum_length,
																	Maximum_length__c = cd.Maximum_length,
																	Course_Unit_Length__c = cd.Course_Unit_Length,
																	Course_Lenght_in_Months__c = cd.Course_Lenght_in_Months, 
																	Language__c = cd.Language,
																	Course_Qualification__c = cd.Course_Qualification,
																	Form_of_Delivery__c = cd.Form_of_Delivery,
																	Sub_Type__c = cd.Course_Specialisation);
								System.debug('==>newCourse: '+newCourse);
	               				listCourses.add(newCourse);
	               				schoolCourseNew.add(cs.parentId+cd.NameCourse);
               				}
               			}
               		}
               			
               }
               
               Database.SaveResult[] srCourses = Database.insert(listCourses, true); //no partial proccess
               for (Database.SaveResult sr : srCourses)
               	  coursesIds.add(sr.getId());
               System.debug('==>coursesIds: '+coursesIds);
               	 
               set<string> campusCourse = new set<string>();	  
               for(Course__c c:[Select id, name, School__c, School__r.Name, Course_Country__c from Course__c where id in:coursesIds]){
               		for(string s:mapCoursesDetails.KeySet()){
	               		for(courseDetails cd:mapCoursesDetails.get(s)){
	               			if(c.School__c == cd.schoolId && c.name == cd.NameCourse && !campusCourse.contains(cd.campusId+c.id)){
	               				listCampusCourses.add(new Campus_Course__c(campus__c = cd.campusId, course__c = c.id, Period__c = cd.campusCoursePeriod, Is_Available__c = true, campus_country__c = c.Course_Country__c, Available_From__c =System.today() ));
	               				campusCourse.add(cd.campusId+c.id);
	               				break;
	               			}
	               		}
               		}
               }
               
	        	insert listCampusCourses;
	        	System.debug('==>listCampusCourses: '+listCampusCourses);
	       /* }
	        catch (Exception e)
	        {
	            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured while importing data Please make sure input csv file is correct');
	            ApexPages.addMessage(errorMessage);
	        }  */
	  }
	  
	  /*
	  Name
       School_Course_Name__c
       Course_Category__c
       Course_Unit_Type__c
       Course_Type__c
       Only_Sold_in_Blocks__c
       Minimum_length__c
       Maximum_length__c
       Course_Unit_Length__c
	  */
	  
	  private list<account> listSchools = new list<account>();
	  private list<string> schoolIds = new list<string>();
	  private map<string, list<account>> mapCampuses = new map<string, list<account>>();
	  private list<account> listCampus = new list<account>();
	  private list<string> campusIds = new list<string>();
	  
	  private map<string,list<courseDetails>> mapCoursesDetails = new map<string,list<courseDetails>>();
	  private list<string> coursesIds = new list<string>();
	  
	  private list<Course__c> listCourses = new list<Course__c>();
	  private list<Campus_Course__c> listCampusCourses = new list<Campus_Course__c>();
	  private map<string, account> mapSchools;
	  
	  private class courseDetails{
	  	private string campusId;
	  	private string schoolId;
		private string campusCoursePeriod;
	  	private string SchoolName;
	  	private string CampusName;
	  	private string NameCourse;
	    private string School_Course_Name;
	    private string Course_Category;
	    private string Course_Unit_Type;
	    private string Course_Type;
		private string Course_field; //Type__c


	    private boolean Only_Sold_in_Blocks;
	    private decimal Minimum_length;
	    private decimal Maximum_length;
	    private decimal Course_Unit_Length;

		private decimal Course_Lenght_in_Months;
		private string Language;
		private string Course_Qualification;
		private string Course_Specialisation;
		private string Form_of_Delivery;

	    private courseDetails(string SchoolName, string CampusName,	string NameCourse, string School_Course_Name, string Course_Category, string Course_Unit_Type, string Course_Type, string Course_field, string Only_Sold_in_Blocks,	string Minimum_length, string Maximum_length, string Course_Unit_Length, string Course_Lenght_in_Months, string Language, string Course_Qualification, string Form_of_Delivery, string Course_Specialisation){
	    	this.SchoolName = SchoolName;
		  	this.CampusName = CampusName;
		  	this.NameCourse = NameCourse;
		    this.School_Course_Name = School_Course_Name;
		    this.Course_Category = Course_Category;
		    this.Course_Unit_Type = Course_Unit_Type;
		    this.Course_Type = Course_Type;
			this.Course_field = Course_field;
		    this.Only_Sold_in_Blocks = Only_Sold_in_Blocks == '1' ? true:false;
			System.debug('==>School_Course_Name: '+School_Course_Name);
			System.debug('==>Minimum_length: '+Minimum_length);
		    if(Minimum_length != null && Minimum_length.trim() != '')
		    	this.Minimum_length = decimal.valueOf(Minimum_length.trim());
		    if(Maximum_length != null && Maximum_length.trim() != '')
		    	this.Maximum_length = decimal.valueOf(Maximum_length.trim());
		    System.debug('==>Course_Unit_Length: '+Course_Unit_Length.trim());
		    if(Course_Unit_Length != null && Course_Unit_Length.trim() != '')
		    	this.Course_Unit_Length = decimal.valueOf(Course_Unit_Length.trim());
				
			System.debug('==>Course_Lenght_in_Months: '+Course_Lenght_in_Months);
			if(Course_Lenght_in_Months != null && Course_Lenght_in_Months.trim() != '')
				this.Course_Lenght_in_Months = decimal.valueOf(Course_Lenght_in_Months.trim());
			this.Language = Language;
			this.Course_Qualification = Course_Qualification;
			this.Course_Specialisation = Course_Specialisation;
			this.Form_of_Delivery = Form_of_Delivery;
	    }
	  }
}