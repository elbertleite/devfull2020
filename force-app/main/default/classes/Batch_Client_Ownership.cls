public with sharing class Batch_Client_Ownership{
	public Batch_Client_Ownership(){
		system.debug('');
	}
}
/*global class Batch_Client_Ownership implements Database.Batchable<sObject> {
	
	String query;
	
	global Batch_Client_Ownership() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		String query = 'SELECT ID, (SELECT ID, Action__c, from_Agency__r.ID, from_Agency__r.Name, from_User__r.ID, from_User__r.Name, to_Agency__r.ID, to_Agency__r.Name, to_User__r.ID, to_User__r.Name, CreatedDate FROM Ownership_History__r) FROM Contact WHERE RecordType.Name in (\'Lead\',\'Client\') AND Owner__C != null AND Number_Ownerships__c > 0';
				
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Contact> ctt) {
		List<IPClasses.OwnershipHistory> ownershipHistory = null;
		IPClasses.OwnershipHistory ownership = null;
		for(contact c : ctt){
			ownershipHistory = new List<IPClasses.OwnershipHistory>();	
			if(c.Ownership_History__r != null && !c.Ownership_History__r.isEmpty()){
				for(Ownership_History__c onh : c.Ownership_History__r){
					ownership = new IPClasses.OwnershipHistory(onh.Action__c);
					
					ownership.fromAgency = onh.from_Agency__r.Name;
					ownership.fromUser = onh.from_User__r.Name;
					ownership.fromAgencyID = onh.from_Agency__r.ID;
					ownership.fromUserID = onh.from_User__r.ID;
					ownership.toAgency = onh.to_Agency__r.Name;
					ownership.toUser = onh.to_User__r.Name;
					ownership.toAgencyID = onh.to_Agency__r.ID;
					ownership.toUserID = onh.to_User__r.ID;
					ownership.createdDate = onh.CreatedDate;
					
					ownershipHistory.add(ownership);
				}
			}	
			c.Ownership_History_Field__c = JSON.serialize(ownershipHistory);
		}
		update ctt;
	}
	

	global void finish(Database.BatchableContext BC) {
		System.debug('Batch Process Complete');  
	}
	
}*/