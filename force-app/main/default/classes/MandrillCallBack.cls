@RestResource(urlMapping = '/mandrill')
global class MandrillCallBack {

	@HttpPost
	global static String doPost(){
		List<Object> webhookRequestEmails = new List<Object>();
		try{
			RestRequest rec = restContext.request;
			
			webhookRequestEmails = (List<Object>) JSON.deserializeUntyped(rec.params.get('mandrill_events'));

			Map<String, Map<String, Object>> emailsFromRequest = new Map<String, Map<String, Object>>();	
			Map<String, Object> emailsInfo;
			Map<String, Object> mandrilWebhookEmailInfo;
			Map<String, Object> mandrilWebhookEmailData;
			Map<String, String> emailMetadata;
			Set<String> emailTags;
			String emailID;
			Set<String> ids = new Set<String>();

			for(Object email : webhookRequestEmails){
				mandrilWebhookEmailInfo = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(email));
				mandrilWebhookEmailData = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(mandrilWebhookEmailInfo.get('msg')));
				if(mandrilWebhookEmailData.get('metadata') != null){
					emailMetadata = (Map<String, String>) JSON.deserialize(JSON.serialize(mandrilWebhookEmailData.get('metadata')), Map<String, String>.class);

					if(!String.isEmpty(emailMetadata.get('hify_id'))){
						system.debug('the request === ' + JSON.serialize(rec));
						system.debug('THE EMAIL DATA '+JSON.serialize(mandrilWebhookEmailData));
						system.debug('THE EMAIL INFO '+JSON.serialize(mandrilWebhookEmailInfo));

						emailTags = new Set<String>((List<String>) JSON.deserialize(JSON.serialize(mandrilWebhookEmailData.get('tags')), List<String>.class));

						emailsInfo = new Map<String, Object>();
						emailsInfo.put('contactIds', emailTags);
						emailsInfo.put('status', MandrillCallBack.getMandrilStatus( (String) mandrilWebhookEmailInfo.get('event') ));
						emailsInfo.put('sender', (String) mandrilWebhookEmailData.get('sender'));
						if(mandrilWebhookEmailData.containsKey('diag') && !String.isEmpty((String) mandrilWebhookEmailData.get('diag'))){
							emailsInfo.put('mandrillMsg', (String) mandrilWebhookEmailData.get('diag'));
						}
						emailsFromRequest.put(emailMetadata.get('hify_id'), emailsInfo);
						
						ids.addAll(emailTags);
					}						
				}
			}

			if(!ids.isEmpty()){
				List<Contact> contactsToUpdate = new List<Contact>();
				Map<String, Map<String, String>> clientEmailsStatus; 
				Map<String, String> newEmailStatus;
				Boolean updateContact;
				Custom_Note_Task__c newTask;
				String taskBody;
				String status;
				String sender;
				Map<String, String> emailsToCopy = new Map<String, String>();
				emailsToCopy.put('Education Hify','develop@educationhify.com');
				Map<String, List<Custom_Note_Task__c>> tasksPerContact = new Map<String, List<Custom_Note_Task__c>>();
				for(Contact ctt : [SELECT ID, Email, Name, Mandrill_Emails_Status__c, Owner__c, Owner__r.Contact.Email, Owner__r.Contact.Name FROM Contact WHERE ID IN :ids]){
					updateContact = false;
					if(String.isEmpty(ctt.Mandrill_Emails_Status__c)){
						clientEmailsStatus = new Map<String, Map<String, String>>();
					}else{
						clientEmailsStatus = (Map<String, Map<String, String>>) JSON.deserialize(ctt.Mandrill_Emails_Status__c, Map<String, Map<String, String>>.class);
					}
					for(String emailKey : emailsFromRequest.keySet()){
						emailTags = (Set<String>) emailsFromRequest.get(emailKey).get('contactIds');
						if(emailTags.contains(ctt.ID)){
							updateContact = true;
							newEmailStatus = new Map<String, String>();
							status = (String) emailsFromRequest.get(emailKey).get('status');
							newEmailStatus.put('status', status);
							if(emailsFromRequest.get(emailKey).containsKey('mandrillMsg')){
								newEmailStatus.put('mandrillMsg', (String) emailsFromRequest.get(emailKey).get('mandrillMsg'));
							}
							clientEmailsStatus.put(emailKey, newEmailStatus);
							
							sender = (String) emailsFromRequest.get(emailKey).get('sender');

							if(status == 'Refused' || status == 'Marked as Spam' || status == 'Bounced Back'){
								if(sender == ctt.Owner__r.Contact.Email){
									taskBody = 'An email to '+ctt.Name+' could not be delivered. Sent to '+ctt.Email+' from '+ctt.Owner__r.Contact.Email+'.<br/> Message status: '+status;
									if(emailsFromRequest.get(emailKey).containsKey('mandrillMsg')){
										taskBody += '<br/>Message sent about this error: '+(String) emailsFromRequest.get(emailKey).get('mandrillMsg');
									}
									newTask = new Custom_Note_Task__c();
									//newTask.Subject__c = 'Email to '+ctt.Name+' could not be delivered.';
									newTask.Subject__c = 'Email not delivered';
									newTask.Comments__c = taskBody;
									newTask.isNote__c = false;
									newTask.Due_Date__c = Date.today();
									newTask.Related_Contact__c = ctt.id;
									newTask.Status__c ='In Progress';
									newTask.AssignedOn__c = DateTime.now();
									newTask.AssignedBy__c = ctt.Owner__c;
									newTask.Assign_To__c = ctt.Owner__c;

									if(!tasksPerContact.containsKey(ctt.ID)){
										tasksPerContact.put(ctt.ID, new List<Custom_Note_Task__c>());
									}
									tasksPerContact.get(ctt.ID).add(newTask);
								}else{
									String subject = 'An email to '+ctt.Name+' could not be delivered.';
									String bodyEmail = 'Contact Name: '+ctt.Name;
									bodyEmail += '<br/>Contact: '+IPFunctions.getContactURLByEnvironment(ctt.ID, 'contact_new_email_history');
									bodyEmail += '<br/>Contact Email: '+ctt.Email;
									bodyEmail += '<br/>Sender: '+sender;
									bodyEmail += '<br/>Email ID: '+emailKey;
									bodyEmail += '<br/>Status: '+status;
									if(emailsFromRequest.get(emailKey).containsKey('mandrillMsg')){
										bodyEmail += '<br/>Mandrill Message: '+(String) emailsFromRequest.get(emailKey).get('mandrillMsg');
									}
									IPFunctions.sendEmail('Education Hify', 'develop@educationhify.com', 'To the Sender', sender, emailsToCopy, subject, bodyEmail);
								}

								// REMOVE THIS BLOCK IN THE FUTURE
								/*String subject = 'An email to '+ctt.Name+' could not be delivered.';
								String bodyEmail = 'Contact Name: '+ctt.Name;
								bodyEmail += '<br/>Contact: '+IPFunctions.getContactURLByEnvironment(ctt.ID, 'contact_new_email_history');
								bodyEmail += '<br/>Contact Email: '+ctt.Email;
								bodyEmail += '<br/>Sender: '+sender;
								bodyEmail += '<br/>Status: '+status;
								if(emailsFromRequest.get(emailKey).containsKey('mandrillMsg')){
									bodyEmail += '<br/>Mandrill Message: '+(String) emailsFromRequest.get(emailKey).get('mandrillMsg');
								}
								IPFunctions.sendEmail('Education Hify', 'develop@educationhify.com', 'Education Hify', 'develop@educationhify.com', subject, bodyEmail);*/
								// REMOVE THIS BLOCK IN THE FUTURE

							}
						}
					}
					if(updateContact){
						ctt.Mandrill_Emails_Status__c = JSON.serialize(clientEmailsStatus);
						contactsToUpdate.add(ctt);
					}
				}		
				if(!tasksPerContact.isEmpty()){
					List<Custom_Note_Task__c> tasksToCreate = new List<Custom_Note_Task__c>();
					for(String contact : tasksPerContact.keySet()){
						tasksToCreate.addAll(tasksPerContact.get(contact));
					}
					insert tasksToCreate;
				}
				if(!contactsToUpdate.isEmpty()){
					update contactsToUpdate;
				}	
			}
		}catch(Exception ex){
			String subject = 'There was an error with the MandrillCallback.';
			String bodyEmail = 'The request: '+JSON.serialize(webhookRequestEmails);
			bodyEmail += '<br/><br/><br/>The error: '+ex.getMessage()+' line: '+ex.getLineNumber();
			IPFunctions.sendEmail('Education Hify', 'develop@educationhify.com', 'Education Hify', 'develop@educationhify.com', subject, bodyEmail);
			return '200';
		}

		return '200';
	}

	@HttpGet
	global static String doGet(){
		RestRequest rec = restContext.request;
		system.debug('responsegett===' + JSON.serialize(rec.params));
		return '200';
	}

	global static String getMandrilStatus(String event){
		if(!String.isEmpty(event)){
			if(event == 'send'){
				return 'Delivered';
			}else if(event == 'reject' || event == 'deferral'){
				return 'Refused';
			}else if(event == 'click' || event == 'open'){
				return 'Opened';
			}else if(event == 'spam'){
				return 'Marked as Spam';
			}else if(event == 'hard_bounce' || event == 'soft_bounce'){
				return 'Bounced Back';
			}
		}
		return 'No Status Provided.';
	}

}