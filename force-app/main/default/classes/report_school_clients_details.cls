public without sharing class report_school_clients_details {
    
    public transient list<client_course__c> reportSales{get; set;}
    public transient map<string,searchPayDetails> reportSalesReceived{get; set;}
    
    public transient list<AggregateResult> reportSalesTotals{get; set;}
 	public transient map<string, integer> reportRepeatedClient{get; set;}
 	//public transient string reportRepeatedClientIds{get; set;}
 	
 	private user userDetails = IPFunctions.getUserInformation(userInfo.getUserId());

	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}
 	
    public void sales(){
    	
    	if(selectedSchoolGroup != null && selectedSchoolGroup != 'all'){
	 		periodReport = null;
	 		string fromDate = IPFunctions.FormatSqlDateIni(dateFilter.Original_Due_Date__c); 
			string toDate = IPFunctions.FormatSqlDateFin(dateFilter.Discounted_On__c);
			
			if(selectSearchBy != 'paydate'){		
				//============== INNER SQL ============================
		 		string innerSql = '';
				 if(selectedSchoolGroup != null && selectedSchoolGroup != 'all')
					innerSql += ' campus_course__r.campus__r.parent.ParentId = \''+selectedSchoolGroup+ '\' and ';
				if(selectedSchool != null && selectedSchool != 'all')
					innerSql += ' campus_course__r.campus__r.ParentId = \''+selectedSchool+ '\' and ';
				else if(selectedCountry !=null && selectedCountry != ''){
					innerSql += ' Course_Country__c = \''+selectedCountry+ '\' and ';
				}
				if(selectedCampus != null && selectedCampus != 'all')
					innerSql += ' campus_course__r.campus__c = \''+selectedCampus+ '\' and ';
					
					
				if(selectedGroup != null && selectedGroup != 'noAgency'){
					if(selectSearchBy == 'enrolment' || selectSearchBy == 'startdate'){
						innerSql += ' Enroled_by_Agency__r.ParentId = \''+selectedGroup+ '\' and ';
					}/*else if(selectSearchBy == 'payment')
						innerSql += ' Received_By_Agency__r.ParentId = \''+selectedGroup+ '\' and ';*/
				}else{
					if(selectSearchBy == 'enrolment' || selectSearchBy == 'startdate'){
						innerSql += ' Enroled_by_Agency__r.Parent.Global_Link__c = \''+userDetails.Contact.Account.Global_Link__c+ '\' and ';
					}/*else if(selectSearchBy == 'payment')
						innerSql += ' Received_By_Agency__r.Parent.Global_Link__c = \''+userDetails.Contact.Account.Global_Link__c+ '\' and ';*/
				}
				
				if(selectedAgency != null && selectedAgency != 'noAgency'){
					if(selectSearchBy == 'enrolment' || selectSearchBy == 'startdate')
						innerSql += ' Enroled_by_Agency__c = \''+selectedAgency+ '\' and ';
					/*else if(selectSearchBy == 'payment')
						innerSql += ' Received_By_Agency__c = \''+selectedAgency+ '\' and ';*/
				}
				
				if(selectedCourseCategory != null && selectedCourseCategory !='all')
					innerSql += ' campus_course__r.course__r.Course_Category__c = \''+selectedCourseCategory+ '\' and ';
				if(selectedCourseType != null && selectedCourseType !='all')
					innerSql += ' campus_course__r.course__r.Course_Type__c = \''+selectedCourseType+ '\' and ';
					
				if(selectSearchBy == 'enrolment'){
					if(SelectedPeriod == 'range')
						innerSql += ' Enrolment_Date__c >= '+ fromDate +' and Enrolment_Date__c <= '+toDate;
					else if(SelectedPeriod == 'THIS_WEEK')
						innerSql += ' Enrolment_Date__c = THIS_WEEK ';
					else if(SelectedPeriod == 'LAST_WEEK')
						innerSql += ' Enrolment_Date__c = LAST_WEEK ';
					else if(SelectedPeriod == 'NEXT_WEEK')
						innerSql += ' Enrolment_Date__c = NEXT_WEEK ';
					else if(SelectedPeriod == 'THIS_MONTH')
						innerSql += ' Enrolment_Date__c = THIS_MONTH ';
					else if(SelectedPeriod == 'LAST_MONTH')
						innerSql += ' Enrolment_Date__c = LAST_MONTH ';
					else if(SelectedPeriod == 'NEXT_MONTH')
						innerSql += ' Enrolment_Date__c = NEXT_MONTH ';
				}else if(selectSearchBy == 'startdate'){
					if(SelectedPeriod == 'range')
						innerSql += ' Start_Date__c >= '+ fromDate +' and Start_Date__c <= '+toDate;
					else if(SelectedPeriod == 'THIS_WEEK')
						innerSql += ' Start_Date__c = THIS_WEEK ';
					else if(SelectedPeriod == 'LAST_WEEK')
						innerSql += ' Start_Date__c = LAST_WEEK ';
					else if(SelectedPeriod == 'NEXT_WEEK')
						innerSql += ' Start_Date__c = NEXT_WEEK ';
					else if(SelectedPeriod == 'THIS_MONTH')
						innerSql += ' Start_Date__c = THIS_MONTH ';
					else if(SelectedPeriod == 'LAST_MONTH')
						innerSql += ' Start_Date__c = LAST_MONTH ';
					else if(SelectedPeriod == 'NEXT_MONTH')
						innerSql += ' Start_Date__c = NEXT_MONTH ';
				}
				//innerSql += ' and Enrolment_Date__c != null and isCancelled__c = false  ';
				
				//innerSql += ' and Enrolment_Date__c != null ';
				//innerSql += ' and ((isPDS__c = false and isPCS__c = false) or (isPDS__c = true and Commission_Confirmed_On__c != null) or (isPCS__c = true and Commission_Confirmed_On__c != null)) ';
				//===============================END INNER SQL====================================================================
				
		 		string sql = 'Select id, client__c, client__r.name, School_Student_Number__c, client__r.Birthdate, Course_Name__c, Campus_Name__c, Enrolment_Date__c, Start_Date__c, End_Date__c, Enroled_by_Agency__r.name, isCancelled__c ';
		 			
	 			sql += ' ,Total_Tuition__c, Total_Commissions__c, Total_Paid_Credit__c, Total_Commission_Received__c, TotalSchoolPaymentConfirmed__c, TotalSchoolCommissionConfirmed__c, Total_Net_Paid_To_School__c ';
	 				
			 	sql += ' from client_course__c where Enrolment_Date__c != null and  ';
					
				sql += innerSql;
						
				if(selectedOrderBy == 'client')
					sql += ' order by client__r.name ';
				else if(selectedOrderBy == 'enroll')
					sql += ' order by Enrolment_Date__c ';	
				else if(selectedOrderBy == 'start')
					sql += ' order by Start_Date__c ';	
				else if(selectedOrderBy == 'campus')
					sql += ' order by Campus_Name__c ';	
				else if(selectedOrderBy == 'agency')
					sql += ' order by Enroled_by_Agency__r.name ';	
								
				reportSales = new list<client_course__c>();
				System.debug('==>sql:' +sql);
				reportSales = Database.query(sql);
					
					
				string TotalSql = 'Select count(id) totIems, COUNT_DISTINCT(client__c) totClients, sum(Total_Tuition__c) totTuition, sum(Total_Commissions__c) totComm, sum(Total_Paid_Credit__c) totCredit, sum(Total_Commission_Received__c) totComReceived, sum(TotalSchoolPaymentConfirmed__c) tuitionConf, sum(TotalSchoolCommissionConfirmed__c) comConf, sum(Total_Net_Paid_To_School__c) totNetTuition  from client_course__c where Enrolment_Date__c != null and '; 
				TotalSql += innerSql;
				
				reportSalesTotals = new list<AggregateResult>();
				reportSalesTotals = database.query(TotalSql);
				
				
				string repeatedClient = 'Select client__c cli, COUNT(id) totClients  from client_course__c where Enrolment_Date__c != null and '; 
				repeatedClient += innerSql;
				repeatedClient += ' group by client__c ';
				
				//reportRepeatedClientIds = '';
				reportRepeatedClient = new map<string, integer>();
				for(AggregateResult ar:database.query(repeatedClient)){
					reportRepeatedClient.put((string)ar.get('cli'), (integer)ar.get('totClients'));
					//reportRepeatedClientIds += (string)ar.get('cli') + ';';
				}
			}else{
				//============== INNER SQL ============================
		 		string innerSql = '';
		 		if(selectedSchool != null && selectedSchool != 'all')
					innerSql += ' client_course__r.campus_course__r.campus__r.ParentId = \''+selectedSchool+ '\' and ';
				else if(selectedCountry !=null && selectedCountry != ''){
					innerSql += ' client_course__r.Course_Country__c = \''+selectedCountry+ '\' and ';
				}
				if(selectedCampus != null && selectedCampus != 'all')
					innerSql += ' client_course__r.campus_course__r.campus__c = \''+selectedCampus+ '\' and ';
					
					
				if(selectedGroup != null && selectedGroup != 'noAgency'){
					if(selectSearchBy == 'enrolment' || selectSearchBy == 'startdate'){
						innerSql += ' client_course__r.Enroled_by_Agency__r.ParentId = \''+selectedGroup+ '\' and ';
					}
				}else{
					if(selectSearchBy == 'enrolment' || selectSearchBy == 'startdate'){
						innerSql += ' client_course__r.Enroled_by_Agency__r.Parent.Global_Link__c = \''+userDetails.Contact.Account.Global_Link__c+ '\' and ';
					}
				}
				
				if(selectedAgency != null && selectedAgency != 'noAgency'){
					if(selectSearchBy == 'enrolment' || selectSearchBy == 'startdate')
						innerSql += ' client_course__r.Enroled_by_Agency__c = \''+selectedAgency+ '\' and ';
					
				}
				
				if(selectedCourseCategory != null && selectedCourseCategory !='all')
					innerSql += ' client_course__r.campus_course__r.course__r.Course_Category__c = \''+selectedCourseCategory+ '\' and ';
				if(selectedCourseType != null && selectedCourseType !='all')
					innerSql += ' client_course__r.campus_course__r.course__r.Course_Type__c = \''+selectedCourseType+ '\' and ';
					
				if(SelectedPeriod == 'range')
					innerSql += ' DAY_ONLY(convertTimezone(Received_Date__c)) >= '+ fromDate +' and DAY_ONLY(convertTimezone(Received_Date__c)) <= '+toDate;
				else if(SelectedPeriod == 'THIS_WEEK')
					innerSql += ' DAY_ONLY(convertTimezone(Received_Date__c)) = THIS_WEEK ';
				else if(SelectedPeriod == 'LAST_WEEK')
					innerSql += ' DAY_ONLY(convertTimezone(Received_Date__c)) = LAST_WEEK ';
				else if(SelectedPeriod == 'NEXT_WEEK')
					innerSql += ' DAY_ONLY(convertTimezone(Received_Date__c)) = NEXT_WEEK ';
				else if(SelectedPeriod == 'THIS_MONTH')
					innerSql += ' DAY_ONLY(convertTimezone(Received_Date__c)) = THIS_MONTH ';
				else if(SelectedPeriod == 'LAST_MONTH')
					innerSql += ' DAY_ONLY(convertTimezone(Received_Date__c)) = LAST_MONTH ';
				else if(SelectedPeriod == 'NEXT_MONTH')
					innerSql += ' DAY_ONLY(convertTimezone(Received_Date__c)) = NEXT_MONTH ';
				
				
				innerSql += ' and client_course__r.Enrolment_Date__c != null and Received_Date__c != null and isCancelled__c = false  ';
				innerSql += ' and ((isPDS__c = false and isPCS__c = false) or (isPDS__c = true and Commission_Confirmed_On__c != null) or (isPCS__c = true and Commission_Confirmed_On__c != null)) ';
				//innerSql += ' and client_course__r.Enrolment_Date__c != null ';
				//===============================END INNER SQL====================================================================
				
		 		string sql = 'Select id, client_course__c, client_course__r.client__c, client_course__r.client__r.name, client_course__r.School_Student_Number__c, client_course__r.client__r.Birthdate, client_course__r.Course_Name__c, client_course__r.Campus_Name__c, client_course__r.Enrolment_Date__c, client_course__r.Start_Date__c, client_course__r.End_Date__c, client_course__r.Enroled_by_Agency__r.name, client_course__r.isCancelled__c ';
		 			
	 			sql += ' ,client_course__r.Total_Tuition__c, client_course__r.Total_Commissions__c, client_course__r.Total_Paid_Credit__c, Tuition_Value__c, Commission_Value__c, Net_Paid_To_School__c ';
	 				
			 	sql += ' from client_course_instalment__c where client_course__r.Enrolment_Date__c != null and  ';
					
				sql += innerSql;
						
				if(selectedOrderBy == 'client')
					sql += ' order by client_course__r.client__r.name ';
				else if(selectedOrderBy == 'enroll')
					sql += ' order by client_course__r.Enrolment_Date__c ';	
				else if(selectedOrderBy == 'start')
					sql += ' order by client_course__r.Start_Date__c ';	
				else if(selectedOrderBy == 'campus')
					sql += ' order by client_course__r.Campus_Name__c ';	
				else if(selectedOrderBy == 'agency')
					sql += ' order by client_course__r.Enroled_by_Agency__r.name ';	
								
				reportSalesReceived = new map<string,searchPayDetails>();
				System.debug('==>sql:' +sql);
				
				for(client_course_instalment__c ci:Database.query(sql)){
					if(!reportSalesReceived.containsKey(ci.client_course__c+'-'+ci.client_course__r.client__c)){
						reportSalesReceived.put(ci.client_course__c+'-'+ci.client_course__r.client__c, new searchPayDetails(ci, 1, ci.Tuition_Value__c, ci.Commission_Value__c, ci.Net_Paid_To_School__c));
					}else {
						reportSalesReceived.get(ci.client_course__c+'-'+ci.client_course__r.client__c).totItems++;
						reportSalesReceived.get(ci.client_course__c+'-'+ci.client_course__r.client__c).tuitionConfirmed += ci.Tuition_Value__c;
						reportSalesReceived.get(ci.client_course__c+'-'+ci.client_course__r.client__c).commissionConfirmed += ci.Commission_Value__c;
						reportSalesReceived.get(ci.client_course__c+'-'+ci.client_course__r.client__c).netPayment += ci.Net_Paid_To_School__c;
					}
					
				}
					
					
				string TotalSql = 'Select COUNT_DISTINCT(client_course__c) totIems, COUNT_DISTINCT(client_course__r.client__c) totClients from client_course_instalment__c where client_course__r.Enrolment_Date__c != null and '; 
				TotalSql += innerSql;
				
				reportSalesTotals = new list<AggregateResult>();
				reportSalesTotals = database.query(TotalSql);
				
				
				string repeatedClient = 'Select client_course__r.client__c cli, COUNT_DISTINCT(client_course__c) totClients  from client_course_instalment__c where client_course__r.Enrolment_Date__c != null and '; 
				repeatedClient += innerSql;
				repeatedClient += ' group by client_course__r.client__c ';
				
				//reportRepeatedClientIds = '';
				reportRepeatedClient = new map<string, integer>();
				for(AggregateResult ar:database.query(repeatedClient)){
					reportRepeatedClient.put((string)ar.get('cli'), (integer)ar.get('totClients'));
					//reportRepeatedClientIds += (string)ar.get('cli') + ';';
				}
				
			}
	 			
    	}else{
    		string descMsg = 'Please select:';
    		if(selectedCountry == null || selectedCountry == '')
    			descMsg += ' <br>* Country ';
    		if(selectedSchoolGroup == null || selectedSchoolGroup == 'all')
    			descMsg += ' <br>* School Group';
    		System.debug('==>descMsg: '+descMsg);
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, descMsg));
    	}
 		
 	}
 	
    public class searchPayDetails{
    	public client_course_instalment__c course{get; set;}
    	public integer totItems{get; set;}
    	public decimal tuitionConfirmed{get; set;}
    	public decimal commissionConfirmed{get; set;}
    	public decimal netPayment{get; set;}
    	public searchPayDetails(client_course_instalment__c course, integer totItems, decimal tuitionConfirmed, decimal commissionConfirmed, decimal netPayment){
    		this.course = course;
    		this.totItems = totItems;
    		this.tuitionConfirmed = tuitionConfirmed;
    		this.commissionConfirmed = commissionConfirmed;
    		this.netPayment = netPayment;
    	}
    }
    
    //===============================FILTER OPTIONS=========================================
 	
 	    
    public string selectSearchBy{get {if(selectSearchBy == null) selectSearchBy = 'enrolment'; return selectSearchBy; } set;}
    private List<SelectOption> searchBy;
    public List<SelectOption> getsearchBy() {
        if(searchBy == null){
            searchBy = new List<SelectOption>();
            searchBy.add(new SelectOption('enrolment','--Enrolment Date--'));
            searchBy.add(new SelectOption('startdate','Course Start Date'));
            searchBy.add(new SelectOption('paydate','Payment Date'));
        }
        return searchBy;
    }
    
    IPFunctions.CampusAvailability result = new IPFunctions.CampusAvailability();
    IPFunctions.agencyNoSharing agencyAndGroup = new IPFunctions.agencyNoSharing();
    
    public string selectedSchool{get {if(selectedSchool == null) selectedSchool = 'all'; return selectedSchool; } set;}
    private List<SelectOption> schools;
    public List<SelectOption> getSchools() {
		 if(schools == null && selectedCountry != null){
			if(selectedSchoolGroup != null && selectedSchoolGroup != 'all')
				schools = result.getSchoolByCountryGroup(selectedCountry, selectedSchoolGroup);
			else schools = result.getSchoolByCountry(selectedCountry);
        }
       
        return schools;
    }

	public string selectedSchoolGroup{get {if(selectedSchoolGroup == null) selectedSchoolGroup = 'all'; return selectedSchoolGroup; } set;}
    private List<SelectOption> schoolGroup;
    public List<SelectOption> getschoolGroup() {
        if(schoolGroup == null && selectedCountry != null){
		   schoolGroup = result.getSchoolGroupByCountry(selectedCountry);
        }
        return schoolGroup;
    }
    
    public string getSelectedSchoolName(){
    	for(SelectOption so:schools)
    		if(so.getValue() == selectedSchool){
    			return so.getLabel();
    			break;
    		}
    	return '';		
    }
    
    public string selectedCountry{get {if(selectedCountry == null) selectedCountry = ''; return selectedCountry; } set;}
    private List<SelectOption> countries;
    public List<SelectOption> getCountries() {
        if(countries == null){
		   countries = IPFunctions.CountryDestinations();
		   if(selectedCountry == null)
		   		selectedCountry = countries[0].getValue();
        }
        return countries;
    }
    
    public void refreshCampus(){
    	campuses = null;
    	refreshCourseCategory();
    }
    
    public void refreshAgencies(){
    	agencies = null;
    }
    
    public void refreshCourseCategory(){
    	courseCategory = null;
    }
    
    public void refreshCourseType(){
    	courseType = null;
    }
    
	public void refreshSchoolGroup(){
		schoolGroup = null;
    	refreshSchool(); 
    }
	
    public void refreshSchool(){
    	schools = null;
    	refreshCampus();
    	refreshCourseType();
    }
    
    public string selectedCampus{get {if(selectedCampus == null) selectedCampus = 'all'; return selectedCampus; } set;}
    private List<SelectOption> campuses;
    public List<SelectOption> getCampuses() {
        if(campuses == null && selectedSchool != null && selectedSchool != 'all'){
		   campuses = result.getCampusBySchoolNA(selectedSchool);
        }
        return campuses;
    }
    
    public string selectedGroup{get {if(selectedGroup == null) selectedGroup = 'noAgency'; return selectedGroup; } set;}
    private List<SelectOption> agencyGroups;
    public List<SelectOption> getAgencyGroups() {
        if(agencyGroups == null){
		   agencyGroups = agencyAndGroup.getAllGroups();
        }
        return agencyGroups;
    }
 	
 	
 	public string selectedAgency{get {if(selectedAgency == null) selectedAgency = 'noAgency'; return selectedAgency; } set;}
    private List<SelectOption> agencies;
    public List<SelectOption> getAgencies() {
        if(agencies == null && selectedGroup != null && selectedGroup != 'noAgency'){
		   agencies = agencyAndGroup.getAgenciesByParentNoSharing(selectedGroup);
        }
        return agencies;
    }
 	   
 	public client_course_instalment__c dateFilter{
		get{
			if(dateFilter == null){
				dateFilter = new client_course_instalment__c(); 
				Date myDate = Date.today();
				Date weekStart = myDate.toStartofWeek();
				dateFilter.Original_Due_Date__c = weekStart;
				dateFilter.Discounted_On__c = dateFilter.Original_Due_Date__c.addDays(6);
			}
			return dateFilter;
		} 
		set;
	}
	
	public string SelectedPeriod{get {if(SelectedPeriod == null) SelectedPeriod = 'THIS_WEEK'; return SelectedPeriod; } set;}
    private List<SelectOption> periods;
    public List<SelectOption> getPeriods() {
        if(periods == null){
            periods = new List<SelectOption>();
            periods.add(new SelectOption('THIS_WEEK','This Week'));
            periods.add(new SelectOption('LAST_WEEK','Last Week'));
            periods.add(new SelectOption('THIS_MONTH','This Month'));
            periods.add(new SelectOption('LAST_MONTH','Last Month'));
            periods.add(new SelectOption('range','Range'));
        }
        return periods;
    }
    
    public string selectedOrderBy{get {if(selectedOrderBy == null) selectedOrderBy = 'enroll'; return selectedOrderBy; } set;}
    private List<SelectOption> orderBy;
    public List<SelectOption> getorderBy() {
        if(orderBy == null){
            orderBy = new List<SelectOption>();
            orderBy.add(new SelectOption('enroll','Enrolment Date'));
            orderBy.add(new SelectOption('client','Client Name'));
            orderBy.add(new SelectOption('start','Start Date'));
            orderBy.add(new SelectOption('campus','Campus Name'));
            orderBy.add(new SelectOption('agency','Agency Name'));
            
        }
        return orderBy;
    }
    
    public string selectedCourseCategory{get {if(selectedCourseCategory == null) selectedCourseCategory = 'all'; return selectedCourseCategory; } set;}
    private List<SelectOption> courseCategory;
    public List<SelectOption> getcourseCategory() {
        if(courseCategory == null && selectedCountry != null){
           string sql = 'Select course__r.Course_Category__c cat from campus_course__c ';
           sql += ' where campus__r.billingCountry = \''+selectedCountry+'\' ';	
           if(selectedSchool != null && selectedSchool != 'all')
           		sql += ' and campus__r.parentId = \''+selectedSchool+ '\' and course__r.Course_Type__c != null ';
           	sql += ' group by course__r.Course_Category__c ';
		   courseCategory = new List<SelectOption>();
		   courseCategory.add(new selectOption('all','All'));
		   for(AggregateResult cc:Database.query(sql))
		   	 courseCategory.add(new selectOption((string)cc.get('cat'),(string)cc.get('cat')));
        }
        return courseCategory;
    }
    
    public string selectedCourseType{get {if(selectedCourseType == null) selectedCourseType = 'all'; return selectedCourseType; } set;}
    private List<SelectOption> courseType;
    public List<SelectOption> getcourseType() {
        if(courseType == null && selectedCourseCategory != null && selectedCourseCategory != 'all'){
           string sql = 'Select course__r.Course_Type__c typ from campus_course__c ';
           sql += ' where campus__r.billingCountry = \''+selectedCountry+'\' ';	
           if(selectedSchool != null && selectedSchool != 'all')
           		sql += ' and campus__r.parentId = \''+selectedSchool+ '\' ';
           sql += ' and course__r.Course_Category__c = \''+selectedCourseCategory+ '\' and course__r.Course_Type__c != null ';
           sql += ' group by course__r.Course_Type__c';
		   courseType = new List<SelectOption>();
		   courseType.add(new selectOption('all','All'));
		   for(AggregateResult cc:Database.query(sql))
		   	 courseType.add(new selectOption((string)cc.get('typ'),(string)cc.get('typ')));
        }
        return courseType;
    }
    
    
    
    public string periodReport{
    	get{
    		IF(periodReport==null){
    			if(SelectedPeriod == 'range')
					periodReport =  IPFunctions.FormatSqlDateIni(dateFilter.Original_Due_Date__c) +' to '+IPFunctions.FormatSqlDateFin(dateFilter.Discounted_On__c);
				else if(SelectedPeriod == 'THIS_WEEK')
					periodReport = 'This Week';
				else if(SelectedPeriod == 'LAST_WEEK')
					periodReport = 'Last Week';
				else if(SelectedPeriod == 'NEXT_WEEK')
					periodReport = 'Next Week';
				else if(SelectedPeriod == 'THIS_MONTH')
					periodReport = 'This Month';
				else if(SelectedPeriod == 'LAST_MONTH')
					periodReport = 'Last Month';
    		}
    		return periodReport;
    	} 
    	set;
    }
    
    public boolean paidcredit { get{if(paidcredit == null) paidcredit = false; return paidcredit;} set; }
    public boolean totGross { get{if(totGross == null) totGross = false; return totGross;} set; } 
	public boolean totComm { get{if(totComm == null) totComm = false; return totComm;} set; } 
	public boolean totPaidSchool { get{if(totPaidSchool == null) totPaidSchool = false; return totPaidSchool;} set; } 
	public boolean birthDate { get{if(birthDate == null) birthDate = false; return birthDate;} set; }
	public boolean cancelled { get{if(cancelled == null) cancelled = false; return cancelled;} set; }
    
    /*
    Select id, client_course__r.client__c, client_course__r.client__r.name, client_course__r.School_Student_Number__c, client_course__r.client__r.Birthdate, client_course__r.Course_Name__c, client_course__r.Campus_Name__c, Received_Date__c, client_course__r.Start_Date__c, client_course__r.End_Date__c, client_course__r.Enroled_by_Agency__r.name, client_course__r.isCancelled__c  ,client_course__r.Total_Tuition__c, client_course__r.Total_Commissions__c, client_course__r.Total_Paid_Credit__c, client_course__r.Total_Commission_Received__c, client_course__r.TotalSchoolPaymentConfirmed__c, client_course__r.TotalSchoolCommissionConfirmed__c  
	from client_course_instalment__c where Received_Date__c != null and   client_course__r.School_Id__c = '00190000019TcobAAC' and  client_course__r.Campus_Id__c = '00190000019Td6qAAC' and  client_course__r.Enroled_by_Agency__r.Parent.Global_Link__c = '0019000001O9Y3XAAV' and  DAY_ONLY(Received_Date__c) >= 2016-01-01 and DAY_ONLY(Received_Date__c) <= 2016-11-05 and Received_Date__c != null  order by client_course__r.Campus_Name__c
	*/
}