public class xCourseSearchCustomCourse{

	private string webSearchId;
	private string customCourseId;	
	private Web_Search__c webSearch;
	
	public void checkUserWS(){
		getUserTemplate();
		createTemplates();
	}
	
	public xCourseSearchCustomCourse(ApexPages.StandardController controller){
		webSearchId = controller.getId();
		
		webSearch = [Select Id, Combine_Quotation__c, Currency_Rates__c, Total_Products__c,
							( Select Custom_Currency__c, Campus_Course__r.Campus__r.account_currency_iso_code__c, Campus_Course__r.Campus__r.BillingCountry, Custom_Country__c,isCustomCourse__c from Search_Courses__r WHERE Course_Deleted__c = false)
					from Web_Search__c where Id = :webSearchId];
		
		customCourseId = ApexPages.currentPage().getParameters().get('cid');
		if(customCourseId != null){
			/*newCustomCourse = [Select Custom_Campus__c, Custom_City__c, Custom_Country__c, Custom_Course__c, Custom_Currency__c, Custom_NumberOfUnits__c, Custom_PriceOption__c, Custom_School__c, Custom_StartDate__c, Custom_Timetable__c, Custom_Unit__c, Custom_Value__c, Custom_Website__c, 
			( Select Course_Extra_Fee__c, Custom_Fee_Name__c, Extra_Fee_Interval__c, Fee_Value__c, isOptional__c, Number_Units__c, Search_Course__c from Search_Course_Extra_Fees__r) from Search_Courses__c where Id = :customCourseId];
			newlistFees.addAll(newCustomCourse.Search_Course_Extra_Fees__r);*/
			
			updateCourse(customCourseId);			
			
		} /*else{
			createTemplates();
		}*/
	}
	
	public String combinedQuoteCurrency {
		get{
			if(webSearch.Combine_Quotation__c && combinedQuoteCurrency == null){
				for(Search_Courses__c sc : webSearch.Search_Courses__r)
					if(sc.isCustomCourse__c)
						combinedQuoteCurrency = sc.Custom_Currency__c;
					else 
						combinedQuoteCurrency = sc.Campus_Course__r.Campus__r.account_currency_iso_code__c;
			}
			return combinedQuoteCurrency;
		}
		set;
	}
	
	public String combinedQuoteCountry {
		get{
			if(webSearch.Combine_Quotation__c && combinedQuoteCountry == null){
				for(Search_Courses__c sc : webSearch.Search_Courses__r)
					if(sc.isCustomCourse__c)
						combinedQuoteCountry = sc.Custom_Country__c;
					else 
						combinedQuoteCountry = sc.Campus_Course__r.Campus__r.BillingCountry;	
			}
			return combinedQuoteCountry;
		}
		set;
	}
	
	
	public Double coursePrice{get{if(coursePrice == null) coursePrice = 0; return coursePrice;}Set;}
	public Double courseTotalFees{get{if(courseTotalFees == null) courseTotalFees = 0; return courseTotalFees;}Set;}
	public Double courseTotalPrice{get{if(courseTotalPrice == null) courseTotalPrice = 0; return courseTotalPrice;}Set;}
	
	public Map<String, Search_Courses__c> templateMap{get; Set;}
	private void createTemplates(){
		templateMap = new  Map<String, Search_Courses__c>([Select Custom_Campus__c, Custom_City__c, Custom_Country__c, Custom_Course__c, Custom_Currency__c, Custom_NumberOfUnits__c, Custom_PriceOption__c, Custom_School__c, Custom_StartDate__c, Custom_Timetable__c, Custom_Unit__c, Custom_Value__c, Custom_Website__c, Selected__c, UseAsCustomTemplate__c, 
			( Select Course_Extra_Fee__c, Custom_Fee_Name__c, Extra_Fee_Interval__c, Fee_Value__c, isOptional__c, Number_Units__c, Search_Course__c from Search_Course_Extra_Fees__r) from Search_Courses__c where Web_Search__c = :getUserTemplate().id and UseAsCustomTemplate__c = true]);
		
		CourseTemplate = new List<SelectOption>();
		CourseTemplate.add(new SelectOption('','--Select Custom Course--'));
		
		for(String csId :templateMap.keySet()){
			Search_Courses__c cs = templateMap.get(csId);
			String units = cs.Custom_NumberOfUnits__c > 1 ? cs.Custom_Unit__c + 's' : cs.Custom_Unit__c;
			
			CourseTemplate.add(new SelectOption(csId, cs.Custom_School__c + ', ' + cs.Custom_Campus__c + ' - ' + cs.Custom_Course__c + ' (' + cs.Custom_NumberOfUnits__c + ' ' + units + ')'));
		}
	
	} 
	
	public PageReference removeCoursesTemplate(){
		if(templateMap != null){
			for(String csId :templateMap.keySet()){
				Search_Courses__c cs = templateMap.get(csId);
				if(cs.Selected__c){
					cs.UseAsCustomTemplate__c = false;
					cs.Selected__c = false;
				}
			}
			update templateMap.values();
			createTemplates();
		}
		return null;
	}
	
	public String courseTemplateId{get; Set;}
	private List<SelectOption> CourseTemplate;
	public List<SelectOption> getCourseTemplate(){
		return CourseTemplate;
	}
	
	public boolean isTemplate {get{if(isTemplate == null) isTemplate = false; return isTemplate;}set;}
	
	private void retrieveCourse(String cid){
		newlistFees = new list<extraFeesDetails>();
		feePosition = 0;
		Search_Courses__c auxNewCustomCourse = [Select isCustomCourse__c, Custom_Campus__c, Custom_City__c, Custom_Country__c, Custom_Course__c, Custom_Currency__c, Custom_NumberOfUnits__c, Custom_PriceOption__c, Custom_School__c, Custom_StartDate__c, Custom_Timetable__c, Custom_Unit__c, Custom_Value__c, Custom_Website__c, UseAsCustomTemplate__c ,
			( Select Course_Extra_Fee__c, Custom_Fee_Name__c, Extra_Fee_Interval__c, Fee_Value__c, isOptional__c, Number_Units__c, Search_Course__c from Search_Course_Extra_Fees__r) from Search_Courses__c where Id = :cid];
		//newlistFees.addAll(newCustomCourse.Search_Course_Extra_Fees__r);
		
		newCustomCourse = auxNewCustomCourse.clone(false, true);
		newCustomCourse.UseAsCustomTemplate__c = false;
		Search_Course_Extra_Fee__c sFee;
		
		newCustomCourse.isCustomCourse__c = true;
		for(Search_Course_Extra_Fee__c cc: auxNewCustomCourse.Search_Course_Extra_Fees__r){
			sFee = cc.clone(false, true);
			sFee.Search_Course__c = null;
			newlistFees.add(new extraFeesDetails(sFee, feePosition++));
			if(sFee.Extra_Fee_Interval__c != null){
				courseTotalFees += sFee.Fee_Value__c * sFee.Number_Units__c;
			}
			else{
				courseTotalFees += sFee.Fee_Value__c;
			}
		}
		if(newCustomCourse.Custom_PriceOption__c == 'Per Unit')
			coursePrice = (newCustomCourse.Custom_Value__c * newCustomCourse.Custom_NumberOfUnits__c);
		else coursePrice = (newCustomCourse.Custom_Value__c);
		courseTotalPrice =  coursePrice + courseTotalFees;
			
		
	}
	
	private void updateCourse(String cid){
		newlistFees = new list<extraFeesDetails>();
		feePosition = 0;
		newCustomCourse = [Select isCustomCourse__c, Custom_Campus__c, Custom_City__c, Custom_Country__c, Custom_Course__c, Custom_Currency__c, Custom_NumberOfUnits__c, Custom_PriceOption__c, Custom_School__c, Custom_StartDate__c, Custom_Timetable__c, Custom_Unit__c, Custom_Value__c, Custom_Website__c, UseAsCustomTemplate__c ,
			( Select Course_Extra_Fee__c, Custom_Fee_Name__c, Extra_Fee_Interval__c, Fee_Value__c, isOptional__c, Number_Units__c, Search_Course__c from Search_Course_Extra_Fees__r) from Search_Courses__c where Id = :cid];
		
		
		for(Search_Course_Extra_Fee__c cc: newCustomCourse.Search_Course_Extra_Fees__r){
				newlistFees.add(new extraFeesDetails(cc, feePosition++));
				if(cc.Extra_Fee_Interval__c != null){
					courseTotalFees += cc.Fee_Value__c * cc.Number_Units__c;
				}
				else{
					courseTotalFees += cc.Fee_Value__c;
				}
			}
			if(newCustomCourse.Custom_PriceOption__c == 'Per Unit')
				coursePrice = (newCustomCourse.Custom_Value__c * newCustomCourse.Custom_NumberOfUnits__c);
			else coursePrice = (newCustomCourse.Custom_Value__c);
			courseTotalPrice =  coursePrice + courseTotalFees;
	}
	
	public PageReference openTemplate(){
		//String paramId = ApexPages.currentPage().getParameters().get('paramId');	
		System.debug('==>courseTemplateId: '+courseTemplateId);
		if(courseTemplateId != null)
			retrieveCourse(courseTemplateId);
		else 
			newCustomCourse = null;
		return null;
	}
	
	public Search_Courses__c newCustomCourse{
		get{
			if(newCustomCourse == null){
				newCustomCourse = new Search_Courses__c();
				newCustomCourse.Custom_Unit__c = 'Week';
				newCustomCourse.Custom_PriceOption__c = 'Per Unit';
				newCustomCourse.isCustomCourse__c = true;
				newCustomCourse.UseAsCustomTemplate__c = false;
			}
				
			return newCustomCourse;
		}
		Set;
	}
	
	
	public Search_Course_Extra_Fee__c newFee{
		get{
			if(newFee == null)
				newFee = new Search_Course_Extra_Fee__c();
			return newFee;
		}
		Set;
	}
	
	private Integer feePosition = 0;
	public class extraFeesDetails{
		public integer feeIdentifier{get; Set;}
		public double TotalFee{get; Set;}
		public Search_Course_Extra_Fee__c extraFee{get; Set;}
		public extraFeesDetails(Search_Course_Extra_Fee__c ef, Integer countPos){
			feeIdentifier = countPos;
			extraFee = ef;
			if(ef.Extra_Fee_Interval__c != null)
				TotalFee = ef.Fee_Value__c * ef.Number_Units__c;
			else TotalFee = ef.Fee_Value__c;
		}
	}
	
	public list<extraFeesDetails> newlistFees{
		get{
			if(newlistFees == null)
				newlistFees = new list<extraFeesDetails>();
			return newlistFees;
		}
		Set;
	}
	
	public PageReference addNewFee(){
		if(newFee.Custom_Fee_Name__c == null || newFee.Custom_Fee_Name__c == ''){
			newFee.Custom_Fee_Name__c.addError('Name is required.`');
			return null;
		}
		if(newFee.Fee_Value__c == null){
			newFee.Fee_Value__c.addError('Value is required.');
			return null;
		}
		
		courseTotalFees = 0;
		
		updateFeesTotal();
		newlistFees.add(new extraFeesDetails(newFee, feePosition++));
		if(newCustomCourse.Custom_PriceOption__c == 'Per Unit')
			coursePrice = (newCustomCourse.Custom_Value__c * newCustomCourse.Custom_NumberOfUnits__c);
		else coursePrice = (newCustomCourse.Custom_Value__c);
		if(newFee.Extra_Fee_Interval__c != null){
			courseTotalFees += newFee.Fee_Value__c * newFee.Number_Units__c;
		}else{
			courseTotalFees += newFee.Fee_Value__c;
		}
		
		courseTotalPrice = coursePrice + courseTotalFees;
				
		newFee = new Search_Course_Extra_Fee__c();
		return null;
		
	}
	
	private void updateFeesTotal(){
		for(extraFeesDetails fd:newlistFees){
			if(fd.extraFee.Extra_Fee_Interval__c != null){
				courseTotalFees += fd.extraFee.Fee_Value__c * fd.extraFee.Number_Units__c;
				fd.TotalFee = fd.extraFee.Fee_Value__c * fd.extraFee.Number_Units__c;
			}else{
				courseTotalFees += fd.extraFee.Fee_Value__c;
				fd.TotalFee = fd.extraFee.Fee_Value__c;
			}
		}
	}
	
	public PageReference deleteFee(){
		String feeToDelete = ApexPages.currentPage().getParameters().get('feeToDelete');
		for(integer i = 0; i < newlistFees.size(); i++){
			if(integer.valueOf(feeToDelete) == newlistFees[i].feeIdentifier){
				newlistFees.remove(i);
				break;
			}
		}
		updateFeesTotal();
		return null;
	}
	
	private Web_Search__c userTemplates;
	private Web_Search__c getUserTemplate(){
		if(userTemplates == null){
			try{
				userTemplates = [Select id from Web_Search__c where createdById = :UserInfo.getUserId() and WS_Template__c = true];	
			}catch(Exception e){
				userTemplates = new Web_Search__c(WS_Template__c = true);
				insert userTemplates;
			}
		}
		return 	userTemplates;
	}
	
	public boolean dataSaved{get{if(dataSaved == null) dataSaved = false; return dataSaved;} Set;}
	public PageReference saveNewCourse(){
		try{
			dataSaved = false;
			if(newCustomCourse.Custom_Country__c == null || newCustomCourse.Custom_Country__c == ''){
	 			newCustomCourse.Custom_Country__c.addError('Country is required.');
	 			return null;
	 		}
			
			//if(courseTotalPrice == 0){
				if(newCustomCourse.Custom_PriceOption__c == 'Per Unit')
					courseTotalPrice = (newCustomCourse.Custom_Value__c * newCustomCourse.Custom_NumberOfUnits__c);
				else 
					courseTotalPrice = (newCustomCourse.Custom_Value__c);
			//}
			
			newCustomCourse.Total_Extra_Fees__c = 0;
			for(extraFeesDetails ef :newlistFees)
				newCustomCourse.Total_Extra_Fees__c += ef.TotalFee;
			
			courseTotalPrice += newCustomCourse.Total_Extra_Fees__c;
			
			if(customCourseId == null)
	 			newCustomCourse.Web_Search__c = webSearchId;
			else{
				double priceAux = 0;
				if(newCustomCourse.Custom_PriceOption__c == 'Per Unit')
					priceAux = (newCustomCourse.Custom_Value__c * newCustomCourse.Custom_NumberOfUnits__c) + newCustomCourse.Total_Extra_Fees__c;
				else 
					priceAux = newCustomCourse.Custom_Value__c + newCustomCourse.Total_Extra_Fees__c;
				
				
				if(courseTotalPrice != priceAux)
					courseTotalPrice = priceAux;
				
			}
			
			
			
			
			newCustomCourse.Total_Course__c = courseTotalPrice;
			newCustomCourse.Total_Tuition__c = courseTotalPrice - newCustomCourse.Total_Extra_Fees__c;
			
			newCustomCourse.Value_Instalments__c = newCustomCourse.Total_Course__c;
			newCustomCourse.Value_First_Instalment__c = newCustomCourse.Total_Course__c;
			newCustomCourse.Number_Instalments__c = 1;
				
			newCustomCourse.End_Date__c = calculateEndDate(newCustomCourse.Custom_StartDate__c, integer.valueOf(newCustomCourse.Custom_NumberOfUnits__c), newCustomCourse.Custom_Unit__c);
			
			
			// Save Course as Template
	 		Search_Courses__c scTemplate;
	 		if(!isTemplate && newCustomCourse.UseAsCustomTemplate__c){
	 			
	 			scTemplate = new Search_Courses__c();
	 			scTemplate = newCustomCourse.clone();
	 			scTemplate.Web_Search__c = getUserTemplate().id;
	 			insert scTemplate;
	 			
	 		}
	 		
			newCustomCourse.UseAsCustomTemplate__c = false;
	 		upsert newCustomCourse;
	 		
	 		delete [Select Id from Search_Course_Extra_Fee__c where Search_Course__c = :newCustomCourse.Id];
	 		list<Search_Course_Extra_Fee__c> extraFeeUpdate = new list<Search_Course_Extra_Fee__c>();
	 		list<Search_Course_Extra_Fee__c> extraFeeTemplate = new list<Search_Course_Extra_Fee__c>();
	 		//Save extra fees
	 		for(extraFeesDetails ef :newlistFees){
	 			if(ef.extraFee.Search_Course__c == null)
	 				ef.extraFee.Search_Course__c = newCustomCourse.Id;
	 			extraFeeUpdate.add(ef.extraFee.clone());
	 			
	 			if(scTemplate != null){
	 				Search_Course_Extra_Fee__c scef = ef.extraFee.clone();
	 				scef.Search_Course__c = scTemplate.id;
	 				extraFeeTemplate.add(scef);
	 			}
	 			
	 			
	 		}
	 		
	 		insert extraFeeUpdate;
	 		insert extraFeeTemplate;
	 		
	 		system.debug('extraFeeUpdate: ' + extraFeeUpdate);
	 		
	 		
	 		
	 		
	 		
	 		
			dataSaved = true;
	 		return null;
		} catch (Exception e) {
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
			dataSaved = false;
		 	return null;
		}
	}
	
	
	public List<SelectOption> getCountries(){
		
		List<SelectOption> options = new List<SelectOption>();
		
		if(webSearch.Combine_Quotation__c){
			
			options.add(new Selectoption('','--Select Country--'));
			options.add(new SelectOption( combinedQuoteCountry, combinedQuoteCountry ));
			
		} else{
			options.add(new Selectoption('','--Select Country--'));
	        Schema.DescribeFieldResult fieldResult = Address__c.Country__c.getDescribe();
	        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
	        
	        for( Schema.PicklistEntry f : ple){
	            options.add(new SelectOption(f.getLabel(), f.getValue()));
	        }
		}
		
        return options;
    }
	 
	public List<SelectOption> getPriceOptions(){
    	List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Search_Courses__c.Custom_PriceOption__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;
    }
	
	public List<SelectOption> getListUnitType(){
    	List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Course__c.Course_Unit_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;
    }
	
	public List<SelectOption> getFeeInterval(){
    	List<SelectOption> options = new List<SelectOption>();
		options.add(new Selectoption('','--None--'));
        Schema.DescribeFieldResult fieldResult = Course_Extra_Fee__c.ExtraFeeInterval__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;
    }
	
	public List<SelectOption> getUnitsRange(){    	
        return xCourseSearchFunctions.getUnitsRange();
    }
	
	
	
	public List<SelectOption> getCurrencies() {
		List<SelectOption> options = new List<SelectOption>();
		
		if(webSearch.Combine_Quotation__c){
			options.add(new SelectOption('','--Select Currency--'));
			options.add(new SelectOption(combinedQuoteCurrency, xCourseSearchFunctions.getCurrencyDescription(combinedQuoteCurrency)));
		} else
			options = xCourseSearchFunctions.getcurrencies();
		
		return options;
	}
	
	public date calculateEndDate(Date startDate, Integer units, String unitType){
        
        try{
            if (startDate != null){
                //System.debug('==> startDate: '+startDate + ' units: '+units + ' unitType: '+unitType);  
                date baseDate = DATE.newinstance(1900, 1, 6);
                integer dts = baseDate.daysBetween(startDate);
                integer DayOfweek = math.mod( dts, 7);
                //System.debug('==> DayOfweek: '+DayOfweek);
                date endDateFriday;
                
                if(unitType.equals('Week'))
                    endDateFriday = startDate.addDays(units * 7);
                else if (unitType.equals('Month'))
                    endDateFriday = startDate.addMonths(units);
                else if (unitType.equals('Semester'))
                    endDateFriday = startDate.addMonths(units * 6);
                else if (unitType.equals('Year'))
                    endDateFriday = startDate.addYears(units);
                else if (unitType.equals('Subjects'))
                    endDateFriday = startDate.addMonths(units);
                else if (unitType.equals('Quarter'))
                    endDateFriday = startDate.addMonths(units * 3);
            /*  else if (unitType.equals('Term'))
                    endDateFriday = startDate.addMonths(units * 6);*/

                integer count = 1;
                while(math.mod(baseDate.daysBetween(endDateFriday), 7) != 6){
                    endDateFriday = endDateFriday.addDays(-1);
                    count++;
                    if(count == 8) //just to try avoid loop
                        break;
                    //System.debug('==> endDateFriday: '+endDateFriday);
                }

                return endDateFriday;
            }
        }catch(Exception e){        
                
        }
        return null;  
    }
	
}