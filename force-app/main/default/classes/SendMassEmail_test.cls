/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SendMassEmail_test {

    static testMethod void myUnitTest() {

		TestFactory tf = new TestFactory();

		Account agency = tf.createAgency();

		AWSKey__c testKey = new AWSKey__c(name='S3 Credential',key__c='key',secret__c='secret');
		insert testKey;

		Contact employee = tf.createEmployee(agency);

		User userEmp = tf.createPortalUser(employee);


		System.RunAs(userEmp){

			Contact client = tf.createClient(agency);

			Client_Document__c cd = new Client_Document__c();
	        cd.Document_Category__c = 'Passport';
	        cd.Document_Type__c = 'Passport';
	        cd.Client__c = client.id;
	        insert cd;

	        Client_Document_File__c cdf = new Client_Document_File__c();
	        cdf.Client__c = client.id;
	        cdf.Doc_Document_Category__c = 'Finance';
	        cdf.File_Name__c = 'SomeFile.txt';
	        cdf.ParentId__c = cd.id;
	        cdf.File_Size_in_Bytes__c = 12510550;
	        insert cdf;

	        Client_Document__c cd2 = new Client_Document__c();
	        cd2.Document_Category__c = 'Travel';
	        cd2.Document_Type__c = 'Flight Ticket';
	        cd2.Client__c = client.id;
	        insert cd2;


	        Client_Document_File__c cdf2 = new Client_Document_File__c();
	        cdf2.Client__c = client.id;
	        cdf2.Doc_Document_Category__c = 'Travel';
	        cdf2.File_Name__c = 'SomeFile.txt';
	        cdf2.ParentId__c = cd2.id;
	        cdf2.File_Size_in_Bytes__c = 12510550;
	        insert cdf2;

	        ApexPages.currentPage().getParameters().put('rt','re');
			ApexPages.currentPage().getParameters().put('key','00D110000008Zs0EAE/Clients/0012000000w05fdAAA/Emails/S:-:0012000000eA8KvAAK:-:132456:-:AttY:-:22-10-2014 16:37:51');
			ApexPages.currentPage().getParameters().put('ids',null);
			SendMassEmail sme = new SendMassEmail();

			sme.sentEmailOwner = true;

			String clientEmailIDs = sme.clientEmailIDs;

			ApexPages.currentPage().getParameters().put('ids','['+client.id+']');
			sme = new SendMassEmail();
			List<SelectOption> categories = sme.categories;
			sme.reloadTypes();
			sme.reloadFiles();
			Apexpages.currentPage().getParameters().put('fileID',cdf.id);
			sme.selectFile();

			List<SelectOption> types = sme.types;
			List<Client_Document_File__c> files = sme.files;

			sme.getSignature();
			ApexPages.currentPage().getParameters().put('toemails','['+client.id+']');
			sme.getToEmails();

			sme.getOrgId();

			ApexPages.currentPage().getParameters().put('fileName', 'Test');
			ApexPages.currentPage().getParameters().put('fileKey', '00D110000008Zs0EAE/Clients/0012000000w05fdAAA/Emails/S:-:0012000000eA8KvAAK:-:132456:-:AttY:-:22-10-2014 16:37:51');
			sme.addUserAttachment();


			sme.subject = 'Test';
			sme.sendEmail();

			// sme.saveToS3();

			// ApexPages.currentPage().getParameters().put('emailID', client.id);
			// sme.removeEmail();

			// ApexPages.currentPage().getParameters().put('clientEmailIDS','['+client.id+']');
			// sme.loadClientEmails();

			// sme.accountID = client.id;
			// sme.accountEmail = client.email;
			// sme.accountName = client.FirstName;
			// sme.addAddress();

			// sme.categoriesOrder = null;
			// List<String> categoriesOrder = sme.categoriesOrder;

			// sme.getDocs();
			//ApexPages.currentPage().getParameters().put('cat', 'Enrolment');
			//sme.renderDoc();


			//sme = new SendMassEmail();

    	}
	}
}