public class NoteController {
	
	public Integer cNoteSize {get;set;}
	public String clientId {get;set;}
	public List<User> user {get;set;}
	public List<SelectOption> subjects {get; set;}
	//Filters
	public String sFilterCreatedOn {get;set;}
	public String sFilterSubject {get;set;}
	public Boolean allNotes {get{if(allNotes==null) allNotes=false;return allNotes;}set;}

	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}

	
	public List<Custom_Note_Task__c> listNotes {
		get{
			if(listNotes == null){
					searchNotes();
			}
			return listNotes;
		}
		
		set;}
	
	public NoteController() {
		user = [Select u.ContactId, u.Contact.Name, Contact.Account.ParentId From User u where User.id = :UserInfo.getUserId() limit 1];
		sFilterCreatedOn='THIS_MONTH';
		SfilterSubject ='all';
		
		//Get subjects
		subjects=listAllSubjects();
	}
	
	public PageReference resetSearch(){
		 searchNotes();
		 Beginning();
		 return null;		 
	}
	
	private integer counter=0;  //keeps track of the offset
	private integer list_size=20; //sets the page size or number of rows
 	public integer total_size {get;Set;} //used to show user the total size of the list
	 
	public void searchNotes(){
		listNotes = new List<Custom_Note_Task__c>();
		String sql = 'Select Id, Subject__c, Related_Contact__r.Name, Related_Contact__c, CreatedById, CreatedBy.Name, CreatedDate,  ' +
					  'Comments__c, Selected__c, LastModifiedBy.Name, LastModifiedDate From Custom_Note_Task__c Where isNote__c=true ' ;
	 	String sqlWhere = '';
 		String sqlOrderBy = '';
 		
 		if(clientId!=null){
			sqlWhere += ' and Related_Contact__c = \''+ clientId + '\' ';
			sqlOrderBy = ' ORDER BY LastModifiedDate DESC ' ;			
		}else{
			sqlWhere += ' and CreatedById = \''+ UserInfo.getUserId() + '\' ';
		}
		
		if(sFilterCreatedOn != 'all'){
			sqlWhere += ' and LastModifiedDate ='+ sFilterCreatedOn ;
		}
		
		if(sFilterSubject != 'all'){
			sqlWhere += ' and Subject__c = \''+ sFilterSubject + '\' ';
		}
		
		sqlOrderBy = ' ORDER BY LastModifiedDate Desc ';		
		
		sql+=sqlWhere;
	 	
	 	if(cNoteSize!=null && cNoteSize>0){
			sqlOrderBy+= ' LIMIT '+ cNoteSize;
		}
	 	
	 	sql+=sqlOrderBy;
	 	if((cNoteSize==null || cNoteSize==0) && !allNotes){
		 	//LIMIT AND OffSet
		 	sql += ' LIMIT '  + list_size + ' OFFSET '+counter;	 	
		 	String sqlCount = 'Select count() FROM Custom_Note_Task__c WHERE isNote__c=true ';
			sqlCount += sqlWhere;		
			total_size = Database.countQuery(sqlCount);
	 	}else if (allNotes){
 			String sqlCount = 'Select count() FROM Custom_Note_Task__c WHERE isNote__c=true ';
			sqlCount += sqlWhere;		
			total_size = Database.countQuery(sqlCount);
	 	}
		
	 	listNotes =  Database.query(sql);
	 	limitComments();//Break the comments when its more than 40 caracters...
	 	
	}
	/*
	 *SELECT OPTION Filter: CreatedOn
	 */
	public List<SelectOption> getFilterCreatedOn() {
       List<SelectOption> options = new List<SelectOption>();    
	
	   options.add(new SelectOption('all','--All--'));
	   options.add(new SelectOption('TODAY','Today'));
       options.add(new SelectOption('THIS_WEEK','This week'));
       options.add(new SelectOption('THIS_MONTH','This month'));
       options.add(new SelectOption('LAST_WEEK','Last week'));
	   options.add(new SelectOption('LAST_N_DAYS:30','Last 30 days'));
       options.add(new SelectOption('LAST_MONTH','Last month'));
       options.add(new SelectOption('LAST_90_DAYS','Last 90 days'));
       return options;
    }
    
   /*
	 * SELECT OPTION: Task Subjects
	 */
	public List<SelectOption> listAllSubjects(){		
		UserDetails.GroupSettings gs = new UserDetails.GroupSettings();
		
		List<SelectOption> subjects = new List<SelectOption>();
		
        subjects = gs.getTaskSubjects(user[0].Contact.Account.ParentId);
        subjects.add(new SelectOption('Quote Follow Up', 'Quote Follow Up'));
        subjects.add(new SelectOption('Web Enquiry', 'Web Enquiry'));
		subjects.add(new SelectOption('RDStation Conversion', 'RDStation Conversion'));
        subjects.add(0,new SelectOption('all', '--All--'));
    
        return subjects;
	}
        
  
  public void limitComments(){
   for(Custom_Note_Task__c t : listNotes){
 	  if(t.Comments__c!= null){
 	     if(t.Comments__c.length() > 255)
	          t.Comments__c = t.Comments__c.substring(0,40) + '...';
	      }
	   }
   }
	
  //LIMITS AND OFFSETS
   public PageReference Beginning() { //user clicked beginning
   	counter = 0;
	searchNotes();
    return null;
   }
 
   public PageReference Previous() { //user clicked previous button
   	counter -= list_size;
	searchNotes();
    return null;
   }
 
   public PageReference Next() { //user clicked next button
    counter += list_size;
	searchNotes();
	return null;
   }
 
   public PageReference End() { //user clicked end
   	counter = total_size - math.mod(total_size, list_size);
	searchNotes();
    return null;
   }
 
   public Boolean getDisablePrevious() {
      //this will disable the previous and beginning buttons
      if (counter>0) return false; else return true;
   }
 
   public Boolean getDisableNext() { //this will disable the next and end buttons
      if (counter + list_size < total_size) return false; else return true;
   }
 
   public Integer getTotal_size() {
      return total_size;
   }
 
   public Integer getPageNumber() {
      return counter/list_size + 1;
   }
 
   public Integer getTotalPages() {
	   try{
      if (math.mod(total_size, list_size) > 0) {
         return total_size/list_size + 1;
      } else {
         return (total_size/list_size);
      }
	   }catch(Exception e){
			return 1;   
	   }
   }  
}