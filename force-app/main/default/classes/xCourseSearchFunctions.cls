global class xCourseSearchFunctions{
	
	public static List<SelectOption> getUnitsRange(){
    	List<SelectOption> options = new List<SelectOption>();
        for(Integer i = 1; i <= 52; i++){
            options.add(new SelectOption(string.valueOf(i), string.valueOf(i)));
        }
        return options;
    }
	
	/*public static string agencyId(){
		//if(UserInfo.getOrganizationId() == '00Dg00000006R9E' || UserInfo.getOrganizationId() == '00D20000000K9Eg')
			return [Select Agency__pc from Account WHERE User__c = :UserInfo.getUserId() limit 1].Agency__pc;
		//else return [SELECT AccountId FROM User where Id = :UserInfo.getUserId() limit 1].AccountId;
	}*/
	
	
	
	/*public static Account agencyDetails(){
		//if(UserInfo.getOrganizationId() == '00Dg00000006R9E' || UserInfo.getOrganizationId() == '00D20000000K9Eg')
			return [Select Name, Agency__pr.Name, Agency__pc, Agency__pr.Optional_Currency__c, Agency__pr.account_currency_iso_code__c, Agency__pr.BillingCountry, Preferable_nationality__c,Preferable_School_Country__c, Preferable_School_City__c from Account WHERE User__c = :UserInfo.getUserId() limit 1];
		//else return [SELECT AccountId FROM User where Id = :UserInfo.getUserId() limit 1].AccountId;
	}*/
		
	public static List<SelectOption> getAgencyCurrencies(ID agency, String label) {
		List<SelectOption> options = new List<SelectOption>();
		
		if(label != null)
			options.add(new SelectOption('', label));		
		else
			options.add(new SelectOption('', '--Select Currency--' ));
		
		for(Currency_rate__c cr : [Select CurrencyCode__c, Description__c from Currency_rate__c where Agency__c = :agency AND CurrencyCode__c != null order by Description__c]){
			String descpt = cr.Description__c == null ? getCurrencyDescription(cr.CurrencyCode__c) : cr.CurrencyCode__c + ' - ' + cr.Description__c;
			options.add(new SelectOption(cr.CurrencyCode__c, descpt ));
		}
			
		
		try{
		   Account ag = [Select Account_Currency_ISO_Code__C from Account WHERE Id = :agency];
		   
		   options.add(new SelectOption(ag.Account_Currency_ISO_Code__C, getCurrencyDescription(ag.Account_Currency_ISO_Code__C)));
		  }catch(exception e){
		   options.add(new SelectOption('USD', 'USD'));
		  }
		
		
		options.sort();
		return options;
	}
	
	public static String getCurrencyDescription(String currencyISOCode){
	
		for(SelectOption so : getCurrencies())
			if(currencyISOCode == so.getValue())
				return so.getLabel();
		return null;
	}
	
	public static List<SelectOption> getCurrencies() {
		
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('','--Select Currency--'));
		
		options.add(new SelectOption('AFN','AFN - Afghanistan Afghani'));
		options.add(new SelectOption('ALL','ALL - Albanian Lek'));
		options.add(new SelectOption('DZD','DZD - Algerian Dinar'));
		options.add(new SelectOption('AOA','AOA - Angola Kwanza'));
		options.add(new SelectOption('ARS','ARS - Argentine Peso'));
		options.add(new SelectOption('AMD','AMD - Armenian Dram'));
		options.add(new SelectOption('AWG','AWG - Aruba Florin'));
		options.add(new SelectOption('AUD','AUD - Australian Dollar'));
		options.add(new SelectOption('AZN','AZN - Azerbaijanian Manat'));
		options.add(new SelectOption('BSD','BSD - Bahamian Dollar'));
		options.add(new SelectOption('BHD','BHD - Bahraini Dinar'));
		options.add(new SelectOption('BDT','BDT - Bangladesh Taka'));
		options.add(new SelectOption('BBD','BBD - Barbados Dollar'));
		options.add(new SelectOption('BYR','BYR - Belarussian Ruble'));
		options.add(new SelectOption('BZD','BZD - Belize Dollar'));
		options.add(new SelectOption('BMD','BMD - Bermuda Dollar'));
		options.add(new SelectOption('BTN','BTN - Bhutan Ngultrum'));
		options.add(new SelectOption('BOV','BOV - Bolivia Mvdol'));
		options.add(new SelectOption('BOB','BOB - Bolivian Boliviano'));
		options.add(new SelectOption('BWP','BWP - Botswana Pula'));
		options.add(new SelectOption('BRL','BRL - Brazilian Real'));
		options.add(new SelectOption('GBP','GBP - British Pound'));
		options.add(new SelectOption('BND','BND - Brunei Dollar'));
		options.add(new SelectOption('BGN','BGN - Bulgaria Lev'));
		options.add(new SelectOption('BIF','BIF - Burundi Franc'));
		options.add(new SelectOption('KHR','KHR - Cambodia Riel'));
		options.add(new SelectOption('CAD','CAD - Canadian Dollar'));
		options.add(new SelectOption('CVE','CVE - Cape Verde Escudo'));
		options.add(new SelectOption('KYD','KYD - Cayman Islands Dollar'));
		options.add(new SelectOption('XOF','XOF - CFA Franc (BCEAO)'));
		options.add(new SelectOption('XAF','XAF - CFA Franc (BEAC)'));
		options.add(new SelectOption('CLP','CLP - Chilean Peso'));
		options.add(new SelectOption('CN','  CN - Chinese Renminbi'));
		options.add(new SelectOption('CNY','CNY - Chinese Yuan'));
		options.add(new SelectOption('COP','COP - Colombian Peso'));
		options.add(new SelectOption('KMF','KMF - Comoros Franc'));
		options.add(new SelectOption('BAM','BAM - Convertible Marks'));
		options.add(new SelectOption('CRC','CRC - Costa Rica Colon'));
		options.add(new SelectOption('HRK','HRK - Croatian Kuna'));
		options.add(new SelectOption('CUP','CUP - Cuban Peso'));
		options.add(new SelectOption('CZK','CZK - Czech Koruna'));
		options.add(new SelectOption('DKK','DKK - Danish Krone'));
		options.add(new SelectOption('DJF','DJF - Dijibouti Franc'));
		options.add(new SelectOption('DOP','DOP - Dominican Peso'));
		options.add(new SelectOption('XCD','XCD - East Caribbean Dollar'));
		options.add(new SelectOption('EGP','EGP - Egyptian Pound'));
		options.add(new SelectOption('ERN','ERN - Eritrea Nakfa'));
		options.add(new SelectOption('EEK','EEK - Estonian Kroon'));
		options.add(new SelectOption('ETB','ETB - Ethiopian Birr'));
		options.add(new SelectOption('EUR','EUR - Euro'));
		options.add(new SelectOption('FKP','FKP - Falkland Islands Pound'));
		options.add(new SelectOption('FJD','FJD - Fiji Dollar'));
		options.add(new SelectOption('CDF','CDF - Franc Congolais'));
		options.add(new SelectOption('GMD','GMD - Gambian Dalasi'));
		options.add(new SelectOption('GEL','GEL - Georgia Lari'));
		options.add(new SelectOption('GHS','GHS - Ghanian Cedi'));
		options.add(new SelectOption('GIP','GIP - Gibraltar Pound'));
		options.add(new SelectOption('GTQ','GTQ - Guatemala Quetzal'));
		options.add(new SelectOption('GNF','GNF - Guinea Franc'));
		options.add(new SelectOption('GYD','GYD - Guyana Dollar'));
		options.add(new SelectOption('HTG','HTG - Haiti Gourde'));
		options.add(new SelectOption('HNL','HNL - Honduras Lempira'));
		options.add(new SelectOption('HKD','HKD - Hong Kong Dollar'));
		options.add(new SelectOption('HUF','HUF - Hungarian Forint'));
		options.add(new SelectOption('ISK','ISK - Iceland Krona'));
		options.add(new SelectOption('INR','INR - Indian Rupee'));
		options.add(new SelectOption('IDR','IDR - Indonesian Rupiah'));
		options.add(new SelectOption('IRR','IRR - Iranian Rial'));
		options.add(new SelectOption('IQD','IQD - Iraqi Dinar'));
		options.add(new SelectOption('ILS','ILS - Israeli Shekel'));
		options.add(new SelectOption('JMD','JMD - Jamaican Dollar'));
		options.add(new SelectOption('JPY','JPY - Japanese Yen'));
		options.add(new SelectOption('JOD','JOD - Jordanian Dinar'));
		options.add(new SelectOption('KZT','KZT - Kazakhstan Tenge'));
		options.add(new SelectOption('KES','KES - Kenyan Shilling'));
		options.add(new SelectOption('KRW','KRW - Korean Won'));
		options.add(new SelectOption('KWD','KWD - Kuwaiti Dinar'));
		options.add(new SelectOption('KGS','KGS - Kyrgyzstan Som'));
		options.add(new SelectOption('LAK','LAK - Lao Kip'));
		options.add(new SelectOption('LVL','LVL - Latvian Lat'));
		options.add(new SelectOption('LBP','LBP - Lebanese Pound'));
		options.add(new SelectOption('LSL','LSL - Lesotho Loti'));
		options.add(new SelectOption('LRD','LRD - Liberian Dollar'));
		options.add(new SelectOption('LYD','LYD - Libyan Dinar'));
		options.add(new SelectOption('LTL','LTL - Lithuanian Lita'));
		options.add(new SelectOption('MOP','MOP - Macau Pataca'));
		options.add(new SelectOption('MKD','MKD - Macedonian Denar'));
		options.add(new SelectOption('MGA','MGA - Malagasy Ariary'));
		options.add(new SelectOption('MWK','MWK - Malawi Kwacha'));
		options.add(new SelectOption('MYR','MYR - Malaysian Ringgit'));
		options.add(new SelectOption('MVR','MVR - Maldives Rufiyaa'));
		options.add(new SelectOption('MRO','MRO - Mauritania Ougulya'));
		options.add(new SelectOption('MUR','MUR - Mauritius Rupee'));
		options.add(new SelectOption('MXN','MXN - Mexican Peso'));
		options.add(new SelectOption('MXV','MXV - Mexican Unidad de Inversion (UDI)'));
		options.add(new SelectOption('MDL','MDL - Moldovan Leu'));
		options.add(new SelectOption('MNT','MNT - Mongolian Tugrik'));
		options.add(new SelectOption('MAD','MAD - Moroccan Dirham'));
		options.add(new SelectOption('MZN','MZN - Mozambique Metical'));
		options.add(new SelectOption('MMK','MMK - Myanmar Kyat'));
		options.add(new SelectOption('NAD','NAD - Namibian Dollar'));
		options.add(new SelectOption('NPR','NPR - Nepalese Rupee'));
		options.add(new SelectOption('ANG','ANG - Neth Antilles Guilder'));
		options.add(new SelectOption('NZD','NZD - New Zealand Dollar'));
		options.add(new SelectOption('NIO','NIO - Nicaragua Cordoba'));
		options.add(new SelectOption('NGN','NGN - Nigerian Naira'));
		options.add(new SelectOption('KPW','KPW - North Korean Won'));
		options.add(new SelectOption('NOK','NOK - Norwegian Krone'));
		options.add(new SelectOption('OMR','OMR - Omani Rial'));
		options.add(new SelectOption('XPF','XPF - Pacific Franc'));
		options.add(new SelectOption('PKR','PKR - Pakistani Rupee'));
		options.add(new SelectOption('PAB','PAB - Panama Balboa'));
		options.add(new SelectOption('PGK','PGK - Papua New Guinea Kina'));
		options.add(new SelectOption('PYG','PYG - Paraguayan Guarani'));
		options.add(new SelectOption('PEN','PEN - Peruvian Nuevo Sol'));
		options.add(new SelectOption('PHP','PHP - Philippine Peso'));
		options.add(new SelectOption('PLN','PLN - Polish Zloty'));
		options.add(new SelectOption('QAR','QAR - Qatar Rial'));
		options.add(new SelectOption('RON','RON - Romanian Leu'));
		options.add(new SelectOption('RUB','RUB - Russian Rouble'));
		options.add(new SelectOption('RWF','RWF - Rwanda Franc'));
		options.add(new SelectOption('WST','WST - Samoa Tala'));
		options.add(new SelectOption('STD','STD - Sao Tome Dobra'));
		options.add(new SelectOption('SAR','SAR - Saudi Arabian Riyal'));
		options.add(new SelectOption('RSD','RSD - Serbian Dinar'));
		options.add(new SelectOption('SCR','SCR - Seychelles Rupee'));
		options.add(new SelectOption('SLL','SLL - Sierra Leone Leone'));
		options.add(new SelectOption('SGD','SGD - Singapore Dollar'));
		options.add(new SelectOption('SBD','SBD - Solomon Islands Dollar'));
		options.add(new SelectOption('SOS','SOS - Somali Shilling'));
		options.add(new SelectOption('ZAR','ZAR - South African Rand'));
		options.add(new SelectOption('LKR','LKR - Sri Lanka Rupee'));
		options.add(new SelectOption('SHP','SHP - St Helena Pound'));
		options.add(new SelectOption('SDG','SDG - Sudanese Pound'));
		options.add(new SelectOption('SRD','SRD - Surinam Dollar'));
		options.add(new SelectOption('SZL','SZL - Swaziland Lilageni'));
		options.add(new SelectOption('SEK','SEK - Swedish Krona'));
		options.add(new SelectOption('CHF','CHF - Swiss Franc'));
		options.add(new SelectOption('SYP','SYP - Syrian Pound'));
		options.add(new SelectOption('TWD','TWD - Taiwan Dollar'));
		options.add(new SelectOption('TJS','TJS - Tajik Ruble'));
		options.add(new SelectOption('TZS','TZS - Tanzanian Shilling'));
		options.add(new SelectOption('THB','THB - Thai Baht'));
		options.add(new SelectOption('TOP','TOP - Tonga Pa\'anga'));
		options.add(new SelectOption('TTD','TTD - Trinidad&Tobago Dollar'));
		options.add(new SelectOption('TND','TND - Tunisian Dinar'));
		options.add(new SelectOption('TRY','TRY - Turkish Lira'));
		options.add(new SelectOption('TMT','TMT - Turkmenistan Manat'));
		options.add(new SelectOption('USD','USD - U.S. Dollar'));
		options.add(new SelectOption('AED','AED - UAE Dirham'));
		options.add(new SelectOption('UGX','UGX - Ugandan Shilling'));
		options.add(new SelectOption('UAH','UAH - Ukraine Hryvnia'));
		options.add(new SelectOption('CLF','CLF - Unidades de fomento'));
		options.add(new SelectOption('UYU','UYU - Uruguayan New Peso'));
		options.add(new SelectOption('UZS','UZS - Uzbekistan Sum'));
		options.add(new SelectOption('VUV','VUV - Vanuatu Vatu'));
		options.add(new SelectOption('VEF','VEF - Venezuelan Bolivar Fuerte'));
		options.add(new SelectOption('VND','VND - Vietnam Dong'));
		options.add(new SelectOption('YER','YER - Yemen Riyal'));
		options.add(new SelectOption('ZMK','ZMK - Zambian Kwacha'));
		options.add(new SelectOption('ZWD','ZWD - Zimbabwe Dollar'));
		
		return options;
		
	}

}