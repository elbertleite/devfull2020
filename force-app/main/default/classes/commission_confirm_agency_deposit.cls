public with sharing class commission_confirm_agency_deposit {

	public String bucketName {get{if(bucketName == null) bucketName = IPFunctions.s3bucket; return bucketName;}set;}
	public String gbLink {get;set;}

	//CONSTRUCTOR
	public commission_confirm_agency_deposit() {

		if(currentUser.Finance_Backoffice_User__c){

			gbLink = currentUser.Contact.Account.Global_Link__c;

			boAgenciesMap = ff.findBOAgenciesPerService(currentUser.Contact.AccountId, new list<String>{'Request_Share_Commission'}); 

			//Country Options
			String pCountry = ApexPages.CurrentPage().getParameters().get('ct');

			if(pCountry == null || pCountry == '')
			pCountry = currentUser.Contact.Account.BillingCountry;

			countryOptions = new list<SelectOption>();

			set<Id> allAgencies = new set<Id>();
			for(String ct : boAgenciesMap.keySet()){
				for(String agBo : boAgenciesMap.get(ct)){
					allAgencies.add(agBo.split(';-;')[0]);
				}//end for
			}//end for

			for(AggregateResult ar : [SELECT client_course__r.Course_Country__c ct FROM client_course_instalment__c WHERE (client_course__r.Enroled_by_Agency__c IN :allAgencies OR pay_to_agency__c IN :allAgencies OR (isRequestedShareCommission__c = TRUE AND client_course__r.share_commission_decision_status__c = 'Accepted' AND client_course__r.share_commission_request_agency__c IN :allAgencies) OR Received_By_Agency__c IN :allAgencies) AND Share_Commission_Paid_On__c = NULL AND ((isNotNetCommission__c = TRUE AND isShare_Commission__c = FALSE ) OR (isShare_Commission__c = TRUE AND isShareCommissionSplited__c = TRUE)) AND (isMigrated__c = FALSE OR PFS_Pending__c = TRUE) AND Commission_Not_Claimable__c = FALSE AND client_course__r.Course_Country__c IN :boAgenciesMap.keySet() group by client_course__r.Course_Country__c ORDER BY client_course__r.Course_Country__c]){
			countryOptions.add(new SelectOption((String)ar.get('ct'), (String)ar.get('ct')));
			if((String)ar.get('ct') == pCountry)
				selectedCountry = (String)ar.get('ct');
			}//end for
			
			if(selectedCountry == null && countryOptions.size()>0)
				selectedCountry = countryOptions[0].getLabel();

			if(selectedCountry != null)
				changeCountry();
			//end -- country 

			pageResult = new list<countryCourses>();

			mainCurrencies = ff.retrieveMainCurrencies();
			agencyCurrencies = ff.retrieveAgencyCurrencies();
			currencyLastModifiedDate = ff.currencyLastModifiedDate;

			if(selectedAgency!=NULL)
				findInvoices();
		}


	}

	//F I N D     P E N D I N G     I N V O I C E S
	private set<String> searchedAgencies;
	public void findInvoices(){

		// if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()) //set the user to see only his own agency
		// 	selectedAgency = currentUser.Contact.AccountId;

		// if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()) //set the user to see only his own agency group
		// 	selectedAgencyGroup = currentUser.Contact.Account.ParentId;

		// System.debug('@selectedAgency==>' + selectedAgency);

		if(currentUser.Finance_Backoffice_User__c && agenciesList != null && agenciesList.size()>0){

			list<String> allAgencies;
			searchedAgencies = new set<String>();

			// if(selectedAgency == 'all'){
				allAgencies = new list<String>();

				for(SelectOption sp : agenciesList)
					if(sp.getValue() !='all'){
						allAgencies.add(sp.getValue());
						searchedAgencies.add(sp.getValue());
					}
			// }
			// else{
			//   searchedAgencies.add(selectedAgency);
			// }

			String sql = 'SELECT ID  ';

			// if(searchName.length()>0){
			// 	sql += ', (SELECT Id FROM cci_shared_commission_bank_dept_enrolled__r WHERE Client_Course__r.Client__r.Name LIKE  \'%'+ searchName +'%\' limit 1)';
			// 	sql += ', (SELECT Id FROM cci_shared_commission_bank_dept_received__r WHERE Client_Course__r.Client__r.Name LIKE  \'%'+ searchName +'%\' limit 1)';
			// 	sql += ', (SELECT Id FROM cci_shared_commission_bank_dept_request__r WHERE Client_Course__r.Client__r.Name LIKE  \'%'+ searchName +'%\' limit 1)';
			// 	sql += ', (SELECT Id FROM client_course_inst_bank_pay_Agency__r WHERE Client_Course__r.Client__r.Name LIKE  \'%'+ searchName +'%\' limit 1)';
			// }

			sql+= ' FROM Invoice__c WHERE isShare_Commission_Invoice__c = TRUE AND Requested_Commission_for_Agency__c = \'' + currentUser.Contact.AccountId + '\' AND Invoice__c = NULL AND Country__c = \'' + selectedCountry + '\' AND isCancelled__c = FALSE ';
			
			if(invoiceNumber.length()>0){
				invoiceNumber = invoiceNumber.replace(' ', '');
				list<String> allRequest = new list<String>(invoiceNumber.split(','));
				sql += ' AND Share_Commission_Number__c in ( \''+ String.join(allRequest, '\',\'') + '\' ) ';

				if(selectedPaymentStatus=='unconfirmed')
					sql += ' AND Confirmed_Date__c = NULL ';
				else
					sql += ' AND Confirmed_Date__c != NULL ';
			}
			else{
				if(selectedPaymentStatus=='unconfirmed'){

					sql += ' AND Confirmed_Date__c = NULL '; //Commission Not Confirmed

					sql+= ' AND ((Due_Date__c >= '+ IPFunctions.FormatSqlDateIni(dates.Commission_Due_Date__c); //Commission Due Date

					sql+= ' AND Due_Date__c <= '+ IPFunctions.FormatSqlDateFin(dates.Commission_Paid_Date__c); //Commission Due Date

					sql+= ' ) OR Due_Date__c < ' + System.now().format('yyyy-MM-dd') + ' )';//Overdue
				}
				else if(selectedPaymentStatus=='confirmed'){

					sql += ' AND Confirmed_Date__c != NULL '; //Commission Confirmed

					sql+= ' AND (Due_Date__c >= '+ IPFunctions.FormatSqlDateIni(dates.Commission_Due_Date__c); //Commission Due Date

					sql+= ' AND Due_Date__c <= '+ IPFunctions.FormatSqlDateFin(dates.Commission_Paid_Date__c) + ' )'; //Commission Due Date
				}
			}

			sql+= 'order by Country__c, Requested_Commission_To_Agency__r.Name, createdDate';

			system.debug('@SQL==>' + sql);

			list<Invoice__c> preResult = Database.query(sql); //used to double filter is there is a Name search
			list<Invoice__c> result;

			if(searchName.length()>0){
				result = new list<Invoice__c>();
				for(Invoice__c i : preResult)
					if(i.cci_shared_commission_bank_dept_enrolled__r.size()>0 || i.cci_shared_commission_bank_dept_received__r.size()>0 || i.cci_shared_commission_bank_dept_request__r.size()>0)
						result.add(i);
			}else{
				result = preResult;
			}

			totResult = result.size();

			/** C R E A T E 	P A G I N A T I O N **/
			Integer pageSize = 20;
			Integer counter = 1;
			Integer page = 1;
			list<ID> instId = new list<ID>();
			totPages = new list<Integer>();
			pageIds = new map<Integer, list<ID>>();

			for(Invoice__c i : result){
				instId.add(i.id);
				if(counter == pageSize){
					pageIds.put(page, instId);
					instId = new list<ID>();
					totPages.add(page);
					page++;
					counter = 1;
				}else
					counter++;
			}//end for


			//add the remaining ids
			if(counter > 1){
				pageIds.put(page, instId);
				totPages.add(page);
			}

			if(result.size()>0)
				findPaginationItems();
			else
				pageResult.clear();


			//T O T A L S     T A B
			totPendingRequest = ff.totReqShareComm(selectedCountry, new list<String>(searchedAgencies), searchName);
		}
	}

	//F I N D     P A G I N A T I O N     I T E M S
	public void findPaginationItems(){

		String paramPage = ApexPages.CurrentPage().getParameters().get('page');

		if(paramPage!=null && paramPage!=''){
			currentPage = integer.valueOf(paramPage);
			ApexPages.CurrentPage().getParameters().remove('page');
		}

		if(currentPage> totPages.size())
			currentPage = totPages.size();

		list<Id> allIds = pageIds.get(currentPage);

		String sql = 'SELECT ID, Invoice__c, Share_Commission_Number__c, Total_Value__c, Paid_On__c, Requested_by_Agency__r.Name, Requested_Commission_To_Agency__c, Requested_Commission_To_Agency__r.Name, Total_Instalments_Requested__c, createdBy.Name, Payment_Receipt__r.preview_link__c, Commission_Paid_Date__c, Invoice_Activities__c, Confirmed_Date__c, Paid_by__r.Name, Paid_by_Agency__r.Name, Paid_Date__c, Total_Emails_Sent__c, Sent_Email_To__c, Sent_On__c,CurrencyIsoCode__c, Country__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, Transfer_Send_Fee__c, Received_Currency__c, Received_Currency_Rate__c, Received_Currency_Value__c, Transfer_Receive_Fee__c, Transfer_Receive_Fee_Currency__c, Requested_Commission_for_Agency__r.Account_Currency_Iso_Code__c, Due_Date__c, Summary__c, Comments__c,';

		sql += '(SELECT Paid_On__c FROM Invoices__r) ';

		sql+= ' FROM Invoice__c WHERE ID in  ( \''+ String.join(allIds, '\',\'') + '\' ) order by Requested_Commission_To_Agency__r.Name, createdDate ';

		system.debug('@SQL==>' + sql);

		Invoice newInv;
		pageResult = new list<countryCourses>();
		// map<string, agencyInvoices> mapResult = new map<string,agencyInvoices>();

		map<String,countryCourses> countryMap = new map<String, countryCourses>();

		for(Invoice__c inv : nsm.noSharingInvoices(sql)){

			//C U R R E N C Y
			if(inv.Paid_On__c!=null && inv.Commission_Paid_Date__c == NULL){
				inv.Received_Currency__c = inv.Requested_Commission_for_Agency__r.Account_Currency_Iso_Code__c;
				inv.Transfer_Receive_Fee_Currency__c = inv.Requested_Commission_for_Agency__r.Account_Currency_Iso_Code__c;
				inv.Received_Currency_Rate__c = agencyCurrencies.get(inv.CurrencyIsoCode__c);

				system.debug('inv.Received_Currency__c===>' + inv.Received_Currency__c);
				system.debug('inv.Transfer_Receive_Fee_Currency__c===>' + inv.Transfer_Receive_Fee_Currency__c);
				system.debug('inv.Received_Currency_Rate__c===>' + inv.Received_Currency_Rate__c);

				ff.convertCurrency(inv, inv.CurrencyIsoCode__c, inv.Total_Value__c, null, 'Received_Currency__c', 'Received_Currency_Rate__c', 'Received_Currency_Value__c');
			}

			list<SelectOption> options = new list<SelectOption>{new SelectOption('none','-- Select Option --')};
			//Group by Agency
			if(inv.Paid_On__c != null){
				if(inv.Confirmed_Date__c == null){ // Not confirmed options
					options.add(new SelectOption('paymentReceived','Payment Received'));
					options.add(new SelectOption('sendEmail','Send Email'));
				}
				else { //Confirmed Options
					options.add(new SelectOption('unconfirmPayment','Unconfirm Payment'));
				}
			}
			else{ // Master not paid yet
				boolean hasPaid = false;
				for(Invoice__c i : inv.Invoices__r) // Check if there are mini invoices that are paid
					if(i.Paid_On__c != NULL){
						hasPaid = true; 
						break;
					} else continue;
					
				if(!hasPaid)
					options.add(new SelectOption('cancelInvoice','Cancel Invoice'));

				options.add(new SelectOption('sendEmail','Send Email'));
				// options.add(new SelectOption('addDocs','Add Extra Docs'));
			}

			newInv = new Invoice(inv, options);

			/** Invoice Activities **/
			if(inv.Invoice_Activities__c!=null && inv.Invoice_Activities__c != ''){

				List<String> allAc = inv.Invoice_Activities__c.split('!#!');

				for(Integer ac = (allAc.size() - 1); ac >= 0; ac--){//activity order desc

					IPFunctions.instalmentActivity instA = new IPFunctions.instalmentActivity();
					List<String> a = allAc[ac].split(';;');

					instA.acType = a[0];
					instA.acTo = a[1];
					instA.acSubject = a[2];
					if(instA.acType=='SMS') // saved date in system mode
						instA.acDate = new Contact(Lead_Accepted_On__c = Datetime.valueOfGmt(a[3]));
					else
						instA.acDate = new Contact(Lead_Accepted_On__c = Datetime.valueOf(a[3]));
					instA.acStatus = a[4];
					instA.acError = a[5];
					instA.acFrom = a[6];

					system.debug('single activity==' + instA);

					newInv.activities.add(instA);
				}
			}

			if(!countryMap.containsKey(inv.Country__c))
				countryMap.put(inv.Country__c, new countryCourses(inv.Requested_Commission_To_Agency__c, new agencyInvoices(inv.Requested_Commission_To_Agency__c, inv.Requested_Commission_To_Agency__r.Name, newInv), inv.Country__c));

			else if(countryMap.get(inv.Country__c).hasAgency(inv.Requested_Commission_To_Agency__c))
				countryMap.get(inv.Country__c).addInvoice(inv.Requested_Commission_To_Agency__c, newInv);

			else
				countryMap.get(inv.Country__c).addAgency(inv.Requested_Commission_To_Agency__c, new agencyInvoices(inv.Requested_Commission_To_Agency__c, inv.Requested_Commission_To_Agency__r.Name, newInv));

		}//end Database Result for

		for(String ct : countryMap.keySet()){
			countryMap.get(ct).orderAgencies();
			pageResult.add(countryMap.get(ct));
		}//end for

		// for(String ag : mapResult.keySet())
		// 	pageResult.add(mapResult.get(ag));
	}

	// R E C A L C U L A T E       R A T E
	public String paramCountry {get;set;}
	public String paramAgency {get;set;}
	public String paramInvoice {get;set;}
	public void recalculateRate(){

		// system.debug('@paramCountry==>'+ paramCountry);
		// system.debug('@paramAgency==>'+ paramAgency);
		// system.debug('@paramInvoice==>'+ paramInvoice);

		// for(countryCourses pr : pageResult)
		// 	if(pr.countryName == paramCountry)
		// 		for(agencyInvoices agInv : pr.allAgencies){
		// 			if(agInv.agencyId == paramAgency){
		// 				for(Invoice inv : agInv.invoices){
		// 					if(inv.invoice.id == paramInvoice){ //Unconfirm Invoice

		// 						ff.convertCurrency(inv.invoice, inv.invoice.CurrencyIsoCode__c, inv.invoice.Total_Value__c, null, 'Received_Currency__c', 'Received_Currency_Rate__c', 'Received_Currency_Value__c');

		// 						paramCountry = null;
		// 						paramAgency = null;
		// 						paramInvoice = null;
		// 						break;
		// 					}
		// 					else continue;
		// 				}//end invoice loop
		// 			}//agency
		// 			else continue;
		// 		}//end for
	}

	//C O N F I R M     I N V O I C E     D E P O S I T
	public void confirmDeposit(){
		Savepoint sp;

		try{
			sp = Database.setSavepoint();

			Boolean refresh = false;

			String country = ApexPages.currentPage().getParameters().get('country');
			String agencyId = ApexPages.currentPage().getParameters().get('agId');
			String invoiceId = ApexPages.currentPage().getParameters().get('invId');
			system.debug('@==> Confirm invoice  ' + invoiceId);
			Invoice__c toUpdate;
			list<client_course_instalment__c> listUpdate = new list<client_course_instalment__c>();
			for(countryCourses pr : pageResult)
				if(pr.countryName == country){
					for(agencyInvoices agInv : pr.allAgencies){
						if(agInv.agencyId == agencyId){
							for(Invoice inv : agInv.invoices){
								if(inv.invoice.id == invoiceId){ //Confirm Invoice
									inv.errorMsg='';
									if(inv.invoice.Commission_Paid_Date__c == null){
										inv.errorMsg += '<li>Please inform the Commission Received Date.</li>';
									}
									if(inv.invoice.Received_Currency__c == null){
										inv.errorMsg += '<li>Please inform the received currency.</li>';
									}
									if(inv.invoice.Received_Currency_Rate__c == null){
										inv.errorMsg += '<li>Please inform the received currency rate.</li>';
									}
									if(inv.invoice.Received_Currency_Value__c == null){
										inv.errorMsg += '<li>Please inform the received value.</li>';
									}
									if(inv.invoice.Commission_Paid_Date__c > system.today()){
										inv.errorMsg += '<li>Commission Received Date cannot be a future date.</li>';
									}

									if(inv.errorMsg.length()>0){
										inv.showError = true;
									}
									else{
										ff.confirmShareCommDeposit(inv.invoice);
										refresh = true;
									}
									break;
								}
								else continue;
							}//end invoice loop
						}//agency
						else continue;
					}//end for
				}

			if(refresh)
				findInvoices();
		}
		catch(Exception e){
			Database.rollback(sp);
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage() + ' => ' + e.getLineNumber());
			ApexPages.addMessage(myMsg);
		}
		finally{
			ApexPages.currentPage().getParameters().remove('country');
			ApexPages.currentPage().getParameters().remove('agId');
			ApexPages.currentPage().getParameters().remove('invId');
		}
	}

	//Set id to Unconfirm
	public void setIdToUnconfirm(){
		unconfirmCt = ApexPages.currentPage().getParameters().get('country');
		unconfirmAg = ApexPages.currentPage().getParameters().get('agId');
		unconfirmId = ApexPages.currentPage().getParameters().get('invId');
		ApexPages.currentPage().getParameters().remove('country');
		ApexPages.currentPage().getParameters().remove('agId');
		ApexPages.currentPage().getParameters().remove('invId');
	}

	//U N C O N F I R M     I N V O I C E
	public void unconfirmDeposit(){
		Savepoint sp;

		try{
			sp = Database.setSavepoint();
			showError = false;
			system.debug('@==> Unconfirm invoice  ' + unconfirmId);

			Invoice__c toUpdateInvoice;

			for(countryCourses pr : pageResult)
				if(pr.countryName == unconfirmCt){
					for(agencyInvoices agInv : pr.allAgencies){
						if(agInv.agencyId == unconfirmAg){
							for(Invoice inv : agInv.invoices){
								if(inv.invoice.id == unconfirmId){ //Unconfirm Invoice
									ff.unconfirmShareCommDeposit(inv.invoice, invReason);
									showError = true;
									break;
								}
								else continue;
							}//end invoice loop
						}//agency
						else continue;
					}//end for
				}
			

		}
		catch(Exception e){
			Database.rollback(sp);
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage() + ' => ' + e.getLineNumber());
			ApexPages.addMessage(myMsg);
		}
	}

	//C A N C E L     I N V O I C E
	public void cancelInvoice(){
		Savepoint sp;
		try{
			sp = Database.setSavepoint();
			showError = false;
			Invoice__c invEmail;
			String invNumber;

			system.debug('@==> Delete invoice  ' + unconfirmId);

			String sql = 'SELECT Invoice__c, invoice_activities__c, Share_Commission_Number__c, Sent_Email_To__c, Requested_by_Agency__r.Name FROM Invoice__c WHERE ID = \'' + unconfirmId + '\' OR Invoice__c = \''+ unconfirmId + '\'';

			List<Invoice__c> upInvoice = new list<Invoice__c>();
			list<Id> invIds = new list<Id>();

			map<String,String> mapActivities = new map<String,String>();

			for(Invoice__c i : sns.NSLInvoice(sql)){

				String activity = 'Agency Commission Invoice ' + i.Share_Commission_Number__c +' - Cancelled;;'+i.Sent_Email_To__c+';;'+ i.Requested_by_Agency__r.Name +' - ' +invReason.Cancel_Reason__c+ ': ' +invReason.Cancel_Description__c +';;' + system.now().format('yyyy-MM-dd HH:mm:ss') + ';;error;;-;;' + currentUser.Name + '!#!';

				system.debug('i.id ===> ' + i.id);
				i.isCancelled__c = true;
				i.Cancel_Description__c = invReason.Cancel_Description__c;
				i.Cancel_Reason__c = invReason.Cancel_Reason__c;
				
				i.invoice_activities__c += activity;

				if(i.Invoice__c == null)
					mapActivities.put('master', activity);
				else
					mapActivities.put(i.id, activity);

				upInvoice.add(i);
				invIds.add(i.id);
			}//end for
			system.debug('mapActivities===> ' + mapActivities);

			update upInvoice; //update cancelled invoices

			list<client_course_instalment__c> upInstalment = new list<client_course_instalment__c>();

			sql = 'SELECT instalment_activities__c, Shared_Commission_Bank_Dep_Enrolled__c, Shared_Commission_Bank_Dep_Pay_Agency__c, Shared_Commission_Bank_Dep_Received__c, Shared_Commission_Bank_Dep_Request__c FROM client_course_instalment__c WHERE Shared_Commission_Bank_Dep_Enrolled__c in ( \''+ String.join(invIds, '\',\'') + '\' ) OR Shared_Commission_Bank_Dep_Pay_Agency__c in ( \''+ String.join(invIds, '\',\'') + '\' ) OR Shared_Commission_Bank_Dep_Received__c in ( \''+ String.join(invIds, '\',\'') + '\' ) OR Shared_Commission_Bank_Dep_Request__c in ( \''+ String.join(invIds, '\',\'') + '\' )';
			

			for(client_course_instalment__c cci : sns.NSInstalments(sql)){

				String activityFor;
				
				if(mapActivities.containsKey(cci.Shared_Commission_Bank_Dep_Enrolled__c)){
					activityFor = cci.Shared_Commission_Bank_Dep_Enrolled__c;
					cci.Shared_Commission_Bank_Dep_Enrolled__c = null;
				}	

				if(mapActivities.containsKey(cci.Shared_Commission_Bank_Dep_Pay_Agency__c)){
					activityFor = cci.Shared_Commission_Bank_Dep_Pay_Agency__c;
					cci.Shared_Commission_Bank_Dep_Pay_Agency__c = null;
				}

				if(mapActivities.containsKey(cci.Shared_Commission_Bank_Dep_Received__c)){
					activityFor = cci.Shared_Commission_Bank_Dep_Received__c;
					cci.Shared_Commission_Bank_Dep_Received__c = null;
				}

				if(mapActivities.containsKey(cci.Shared_Commission_Bank_Dep_Request__c)){
					activityFor = cci.Shared_Commission_Bank_Dep_Request__c;
					cci.Shared_Commission_Bank_Dep_Request__c = null;
				}

				system.debug('cci.instalment_activities__c===> ' + cci.instalment_activities__c);

				cci.instalment_activities__c += mapActivities.get('master') + mapActivities.get(activityFor);

				system.debug('cci.instalment_activities__c===> ' + cci.instalment_activities__c);

				upInstalment.add(cci);

			}//end for

			nsm.saveInstalmetns(upInstalment);

			showError = true;

			for(Invoice__c inv : upInvoice)
				sendCancelEmail(inv);
		}
		catch(Exception e){
			Database.rollback(sp);
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage() + ' => ' + e.getLineNumber());
			system.debug('error==>' +  e.getMessage() + ' => ' + e.getLineNumber());
			ApexPages.addMessage(myMsg);
		}
		finally{
			ApexPages.CurrentPage().getParameters().remove('invId');
			ApexPages.CurrentPage().getParameters().remove('agId');
		}
	}

	private void sendCancelEmail(Invoice__c inv){
		mandrillSendEmail mse = new mandrillSendEmail();
		mandrillSendEmail.messageJson email_md = new mandrillSendEmail.messageJson();
		String body = label.Share_Commission_Cancel_Invoice;
		body = body.replace('#invoiceNumber#', inv.Share_Commission_Number__c);

		String subject = 'Agency Commission ' + inv.Share_Commission_Number__c + ' Cancelled ';
	
		email_md.setFromEmail(currentUser.Contact.Email);
		email_md.setFromName(currentUser.Name);
		String emailService = IpFunctions.getS3EmailService();
		email_md.setSubject(subject);
		email_md.setTo(inv.Sent_Email_To__c, '');
		email_md.setCc(currentUser.Contact.Email, '');
		email_md.setHtml(body);

		mse.sendMail(email_md);

		// DateTime currentTime = DateTime.now();
		// String myDate = currentTime.format('dd-MM-yyyy HH:mm:ss');
		// String activity = 'Email - Shared Commission Invoice;;' + toEmail+ ';;' + subject +';;' + currentTime.format('yyyy-MM-dd HH:mm:ss') + ';;sent;;-;;' + fromName + '!#!';

		// if(inv.Invoice_Activities__c!=null)
		// 	inv.Invoice_Activities__c += activity;
		// else
		// 	inv.Invoice_Activities__c = activity;

		// inv.Sent_On__c = system.now();

		// update inv;
	}





	//N O     S H A R I N G     M E T H O D S
	public without sharing class NSMethods{

		public void updateObj(list<SOBject> inv){
			update inv;
		}

		public void saveInstalmetns(list<client_course_instalment__c> instalments){
			update instalments;
		}

		public list<Invoice__c> noSharingInvoices(String sql){
			return Database.query(sql);
		}
	}

	//*************************  FILTERS *************************

	public String invoiceNumber {get{if(invoiceNumber == null) invoiceNumber = ''; return invoiceNumber;}set;}
	public String selectedCountry {get;set;}
	public String selectedAgency {get;set;}
	public String selectedAgencyGroup {get;set;}
	public String selectedPaymentStatus {get{if(selectedPaymentStatus==null) selectedPaymentStatus = 'unconfirmed'; return selectedPaymentStatus;}set;}
	public String searchName {get{if(searchName==null) searchName = ''; return searchName;}set;}
	public Integer totResult {get;set;}
	public Integer totPendingRequest {get;set;}
	public Integer currentPage {get{if(currentPage==null) currentPage = 1; return currentPage;}set;}
	public Boolean showError {get{if(showError == null) showError = false; return showError;}set;}
	public DateTime currencyLastModifiedDate {get;set;}
	public list<SelectOption> agencyGroupOptions {get;set;}
	public list<SelectOption> agenciesList {get;set;}
	public list<SelectOption> mainCurrencies {get;set;}
	public list<SelectOption> countryOptions {get;set;} 
	public list<countryCourses> pageResult {get;set;}

	public Invoice__c invReason {get{if(invReason==null) invReason = new Invoice__c(); return invReason;}set;}
	public list<Integer> totPages {get;set;}

	private User currentUser {get{if(currentUser==null) currentUser = ff.currentUser; return currentUser;}set;}
	private list<String> agenciesBO;
	private Map<String, double> agencyCurrencies;

	private String unconfirmCt {get;set;}
	private String unconfirmAg {get;set;}
	private String unconfirmId {get;set;}

	private map<String,list<SelectOption>> allFilters {get;set;}
	private map<String, list<String>> boAgenciesMap {get;set;}
	private map<Integer, list<ID>> pageIds;
	private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}
	private IPFunctions.SearchNoSharing sns {get{if(sns == null) sns = new IPFunctions.SearchNoSharing(); return sns;}set;}
	private NSMethods nsm {get{if(nsm==null) nsm = new NSMethods(); return nsm;}set;}

	public client_course_instalment__c dates{get{
		if(dates==null){

			dates = new client_course_instalment__c();

			String pIni = ApexPages.CurrentPage().getParameters().get('ini');
			String pFin = ApexPages.CurrentPage().getParameters().get('fin');
			
			if(pIni != null && pini!= ''){
				// list<String>dt = pIni.split('-');
				dates.Commission_Due_Date__c = date.valueOf(pini + ' 00:00:00');
			}
			else{
				dates.Commission_Due_Date__c = system.today();
			}

			if(pFin != null && pFin!= ''){
				// list<String>dt = pFin.split('-');
				dates.Commission_Paid_Date__c = date.valueOf(pFin + ' 00:00:00');
			}
			else{
				dates.Commission_Paid_Date__c = dates.Commission_Due_Date__c.addDays(14);
			}
		}
		return dates;
	}set;}

	private map<String,list<SelectOption>> unconfirmedFilters {get;set;}
	private map<String,list<SelectOption>> confirmedFilters {get;set;}

	public list<SelectOption> paymentStatus {get{
		if(paymentStatus == null){
			paymentStatus = new list<SelectOption>();
			paymentStatus.add(new SelectOption('unconfirmed','Unconfirmed'));
			paymentStatus.add(new SelectOption('confirmed','Confirmed'));
		}
		return paymentStatus;
	}set;}

	

	// public void changePaymentStatus(){
	//   if(selectedPaymentStatus=='unconfirmed'){
	//     if(unconfirmedFilters.size()>0){
	//       agencyGroupOptions = unconfirmedFilters.get('allGroups');
	//     }
	//   }

	//   else{   //   Unconfirmed
	//     // if(confirmedFilters==null){
	//       confirmedFilters = ff.confirmedSharedDeposit();
	//     // }
	//     if(confirmedFilters.size()>0){
	//       agencyGroupOptions = confirmedFilters.get('allGroups');
	//     }
	//   }
	//   // organizeGroups(); 
	// }
	public string agenciesUnderBackoffice{get{if(agenciesUnderBackoffice == null) agenciesUnderBackoffice = ''; return agenciesUnderBackoffice;} set;}
	public void changeCountry(){
		list<String> boAgencies = boAgenciesMap.get(selectedCountry);
		agenciesUnderBackoffice = '';
		agenciesBO = new list<String>(); 
		
		integer counter = 0;
		for(String agBo : boAgencies){
				
			list<String> ag = agBo.split(';-;');
			if(counter>0)
				agenciesUnderBackoffice += ', ' + ag[1];
			else
				agenciesUnderBackoffice += ag[1];
			agenciesBO.add(ag[0]);
			counter++;
		}//end for

		retrieveAgencyGroupOptions();
	}

	public void retrieveAgencyGroupOptions(){
			selectedAgencyGroup = null;
			allFilters = ff.reqCommissionBOFilters(selectedCountry, agenciesBO);

			if(allFilters.size()>0){
				agencyGroupOptions = allFilters.get('allGroups');

				// for(SelectOption sp : agencyGroupOptions){
				//   if(sp.getValue() == currentUser.Contact.Account.ParentId){
				//     selectedAgencyGroup = sp.getValue();
				//     break;
				//   }
				// }//end for

				if(selectedAgencyGroup == null && agencyGroupOptions.size()>0)
					selectedAgencyGroup = agencyGroupOptions[0].getValue();

			changeGroup();
		}
	}

	public void changeGroup(){
		selectedAgency = null;

		if(allFilters.get(selectedAgencyGroup) != null){

			agenciesList = allFilters.get(selectedAgencyGroup);

			if(selectedAgency == null && agenciesList.size()>0)
				selectedAgency = agenciesList[0].getValue();
		}
	}

	//************************* INNER CLASSES *************************
	public class countryCourses{
		public String countryName {get;set;}
		public list<agencyInvoices> allAgencies {get;set;}
		private map<String, agencyInvoices> mapAgencies {get;set;}

		public countryCourses (String agName, agencyInvoices ag, String countryName){
			mapAgencies = new map<String, agencyInvoices>();
			this.mapAgencies.put(agName, ag);
			this.countryName = countryName;
		}

		public boolean hasAgency(String agName){
			if(!mapAgencies.containsKey(agName))
				return false;
			else
				return true;
		}

		public void addAgency (String agName, agencyInvoices ag){
				mapAgencies.put(agName, ag);
		}

		public void addInvoice (String agName, Invoice i){
				mapAgencies.get(agName).addInvoice(i);
		}

		public void orderAgencies(){
			list<String> names = new list<String>();
			names.addAll(mapAgencies.keySet());
			names.sort();

			this.allAgencies = new list<agencyInvoices>();

			for(String nm : names)
				this.allAgencies.add(mapAgencies.get(nm));
		}
	}

	public class agencyInvoices{
		public String agencyId {get;set;}
		public String agencyName {get;set;}
		public list<Invoice> invoices {get{if(invoices==null) invoices= new list<Invoice>(); return invoices;}set;}
		public decimal totalAmount {get{if(totalAmount==null) totalAmount= 0; return totalAmount;}set;}

		public agencyInvoices (String agencyId, String agencyName, Invoice i){
				this.agencyId = agencyId;
				this.agencyName = agencyName;
				this.invoices.add(i);
				this.totalAmount = i.invoice.Total_Value__c;
		}

		public void addInvoice(Invoice i){
				this.invoices.add(i);
				this.totalAmount += i.invoice.Total_Value__c;
		}
	}

	public class Invoice{
		public Invoice__c invoice {get;set;}
		public list<SelectOption> options {get;set;}
		public list<s3Docs> invDocs {get;set;}
		public list<IPFunctions.instalmentActivity> activities {get{if(activities == null) activities = new list<IPFunctions.instalmentActivity>(); return activities;} set;}
		public Date lastEmail {get;set;}
		public boolean showError {get{if(showError == null) showError = false; return showError;}set;}
		public String errorMsg {get{if(errorMsg == null) errorMsg = ''; return errorMsg;}set;}

		public Invoice(Invoice__c inv, list<SelectOption> options){
			this.invoice = inv;
			this.options = options;

			//Preview Links
			invDocs = new list<s3Docs>();
			if(inv.Payment_Receipt__r.preview_link__c!=null)
				for(String doc : inv.Payment_Receipt__r.preview_link__c.split(FileUpload.SEPARATOR_FILE))
					invDocs.add(new S3Docs(doc.split(FileUpload.SEPARATOR_URL)));

		}//Invoice constructor
	}


	public class s3Docs{
		public String docName {get;set;}
		public String docUrl {get;set;}

		public s3Docs (List<String> docParts){
			this.docUrl = docParts[0];
			this.docName = docParts[1];
		}
	}

}