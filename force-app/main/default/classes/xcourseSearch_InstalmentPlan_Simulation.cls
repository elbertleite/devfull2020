public class xcourseSearch_InstalmentPlan_Simulation{
	
	string accoId;
	public xcourseSearch_InstalmentPlan_Simulation(ApexPages.StandardController controller){
		accoId = controller.getId();
		listAgencyPlans = [Select Agency__c, Deposit__c, Description__c, Group_Instalments__c, Instalments_Interest__c, Interest__c, Interest_Type__c, Name__c, Number_of_Instalments__c, Surcharge_Value__c, Surcharge_Option__c from Instalment_Plan__c where Agency__c = :accoId order by Group_Instalments__c nulls first, Number_of_Instalments__c desc];
	}
	
	private list<Instalment_Plan__c> listAgencyPlans;
	
	public class groupInstalment{
		public Integer instalmentNumber{get; Set;}
		public decimal instalmentValue{get; Set;}
		public groupInstalment(Integer numberInstal, Decimal valInstal){
			instalmentNumber = numberInstal;
			instalmentValue = valInstal;
		}
	}
	
	public class agencyInstalmentPlan{
		public Decimal totalCourse{get; Set;}
		public Decimal Deposit{get; Set;}
		public Decimal remainingValue{get; Set;}
		public integer numberInstalments{get; Set;}
		public list<groupInstalment> instalmentDetails{get{if(instalmentDetails == null) instalmentDetails = new list<groupInstalment>(); return instalmentDetails;} Set;}
		public string planDescription {get;Set;}
		public string name {get;Set;}
		public Decimal surchargeValue{get; Set;}
	}
	
	public Double ValueSimulation{get{if(ValueSimulation == null) ValueSimulation = 0; return ValueSimulation;} Set;}
	
	public PageReference simulate(){
		calculateListPlans(ValueSimulation);
		return null;
	}

	public list<agencyInstalmentPlan> listPlans{get; Set;}
	public list<agencyInstalmentPlan> calculateListPlans(Double totalCourseQuote){
		agencyInstalmentPlan agencyPlan;
		listPlans = new list<agencyInstalmentPlan>();
		double coefficient;
		
		groupInstalment intalDetails;

		for(Instalment_Plan__c ip:listAgencyPlans){
			agencyPlan = new agencyInstalmentPlan();
			agencyPlan.name = ip.Name__c;
			agencyPlan.totalCourse = totalCourseQuote;
			if(ip.Surcharge_Value__c != null)
				agencyPlan.surchargeValue = totalCourseQuote * (ip.Surcharge_Value__c/100);
			else agencyPlan.surchargeValue = 0;
			if(ip.Description__c != null)
				agencyPlan.planDescription = ip.Description__c.replace('\n','<br />');
			else agencyPlan.planDescription =  '';
			agencyPlan.numberInstalments = Integer.valueOf(ip.Number_of_Instalments__c);
			if(ip.Number_of_Instalments__c == 0){
				agencyPlan.instalmentDetails.add(new groupInstalment(0, agencyPlan.totalCourse + agencyPlan.surchargeValue));
				
			}else{
				agencyPlan.Deposit = totalCourseQuote * (ip.Deposit__c/100) + agencyPlan.surchargeValue;
				agencyPlan.remainingValue = totalCourseQuote - agencyPlan.Deposit + agencyPlan.surchargeValue;
				if(ip.Group_Instalments__c){
					if(ip.Interest_Type__c == 'Simple'){
						agencyPlan.instalmentDetails.add(new groupInstalment(agencyPlan.numberInstalments, (agencyPlan.remainingValue) * (1 + ip.Interest__c/100) / agencyPlan.numberInstalments));
					}else if(ip.Interest_Type__c == 'Compound'){
						agencyPlan.instalmentDetails.add(new groupInstalment(agencyPlan.numberInstalments, (agencyPlan.remainingValue * math.pow(  Double.valueOf(1 + ip.Interest__c/100) , Double.valueOf(agencyPlan.numberInstalments)))/agencyPlan.numberInstalments ));
					}else{
						if(ip.Interest__c == 0)
							agencyPlan.instalmentDetails.add(new groupInstalment(agencyPlan.numberInstalments, agencyPlan.remainingValue / Double.valueOf(agencyPlan.numberInstalments) ));
						else {
							Double interest = (ip.Interest__c/100);
							integer instalment = integer.valueOf(agencyPlan.numberInstalments);
							coefficient = interest / ( 1 - ( 1 / math.pow( ( 1 + interest), instalment )) );
							agencyPlan.instalmentDetails.add(new groupInstalment(instalment, agencyPlan.remainingValue * coefficient ));
							agencyPlan.instalmentDetails.add(new groupInstalment(agencyPlan.numberInstalments, agencyPlan.remainingValue * (ip.Interest__c/100) / (1 - (math.pow( Double.valueOf(1 + (ip.Interest__c/100)), - Double.valueOf(agencyPlan.numberInstalments))) ) ));
						}
					}
				}else if(ip.Instalments_Interest__c != null) { 
					for(String s:ip.Instalments_Interest__c.split(';')){
						Double instalment =  Double.valueOf(s.split(':')[0]);
						Double interest =  Double.valueOf(s.split(':')[1])/100;
						if(ip.Interest_Type__c == 'Simple'){
							agencyPlan.instalmentDetails.add(new groupInstalment(integer.valueOf(instalment), agencyPlan.remainingValue * (1 + interest) / integer.valueOf(instalment)) );
						}else if(ip.Interest_Type__c == 'Compound'){
							agencyPlan.instalmentDetails.add(new groupInstalment(integer.valueOf(instalment), (agencyPlan.remainingValue * math.pow(Double.valueOf(1 + interest) , instalment) )/instalment ));
						}
						else{
							if(interest == 0)
								agencyPlan.instalmentDetails.add(new groupInstalment(integer.valueOf(instalment), agencyPlan.remainingValue / instalment ));
							else {
								coefficient = interest / ( 1 - ( 1 / math.pow( ( 1 + interest), instalment )) );
								agencyPlan.instalmentDetails.add(new groupInstalment(integer.valueOf(instalment), agencyPlan.remainingValue * coefficient ));
								//agencyPlan.instalmentDetails.add(new groupInstalment(integer.valueOf(instalment), agencyPlan.remainingValue * interest / (1 - (math.pow(1 + interest, -instalment)) ) ));
							}
						}
					}
				}
			}
			listPlans.add(agencyPlan);
		}
		return listPlans;
	}
	


}