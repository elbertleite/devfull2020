public class ShowCaseDestination {
	
	private final String ACTION_NEW = 'new';
	private final String ACTION_EDIT = 'edit';
	private final String SHOWCASE_COUNTRY = 'Country';
	private final String SHOWCASE_CITY = 'City';
	
	public Account showcase {get;set;}
	public String showcaseType {get;set;}
	public boolean isAddingNew {get;set;}
	private String action;
	private String newShowcaseName;
	public boolean isInternalUser {
		get{
			if(isInternalUser == null){
				isInternalUser = IPFunctions.isInternalUser(Userinfo.getUserID());
			}
			return isInternalUser;
		}
		set;
	}
	
	public ShowCaseDestination(){
		
		if(ApexPages.currentPage().getParameters().get('id') != null && ApexPages.currentPage().getParameters().get('id') != ''){
			refresh(ApexPages.currentPage().getParameters().get('id'));			
		} else {
			showcase = new Account();
			action = ACTION_NEW;
			isAddingNew = true;
			showcaseType = ApexPages.currentPage().getParameters().get('type') != null && ApexPages.currentPage().getParameters().get('type') == SHOWCASE_CITY ? SHOWCASE_CITY : SHOWCASE_COUNTRY;
			String countryid = ApexPages.currentPage().getParameters().get('country');
			if(countryid != null)
				showcase.parentid = countryid;
				
			newShowcaseName = ApexPages.currentPage().getParameters().get('ct'); 
		}
			
				
	}
	
	private void refresh(String scid){
		showcase = getShowCase(scid);
		action = ACTION_EDIT;
		isAddingNew = false;
		showcaseType = showcase.recordtype.name;
		
		genCOL((String.isEmpty(showCase.showcase_cost_of_living__c))?new list<string>() : showcase.showcase_cost_of_living__c.split(';'));
		genVideos((String.isEmpty(showcase.Videos__c))?new list<string>():showcase.Videos__c.split(';'));
		
		if(showcase.recordtype.name == 'Campus'){
			generateNearTo(showcase.Near_to__c);
			generatewhyChooseCampus(showcase.Why_choose_this_campus__c);
			generateFeatures(showcase.Features__c);
			generateextraAct(showcase.Extra_Activities__c);
			generateNationalityMix((String.isEmpty(showcase.Nationality_Mix__c)) ? new list<string>() : showcase.Nationality_Mix__c.split('!#!'));
		}
		
	}
	
	private List<SelectOption> countries;		
    public List<SelectOption> getCountries(){
    	try {
	    	if(countries == null){
	    		countries = new List<SelectOption>();
	    		List<String> countryShowcases = new List<String>();
	    		
	    		countries.add(new SelectOption('', '-- Select Country --'));
	    		
	    		if(showcasetype == 'Country'){
	    			for(Account country : [select id, name from account where recordtype.name = 'country' order by Name])
						countryShowcases.add(country.name);
						
	    			for(AggregateResult ar : [select BillingCountry from account where recordtype.name = 'Campus' and BillingCountry not in :countryShowcases group by BillingCountry order by BillingCountry]){
	    				String billingCountry = (String) ar.get('BillingCountry');
		    			countries.add(new SelectOption(billingCountry, billingCountry));
		    			if(newShowcaseName == billingCountry)
	    					showcase.name = billingCountry;
	    			}
		    				
	    		} else if(showcasetype == 'City'){
	    			for(Account country : [select id, name from account where recordtype.name = 'country' order by Name])
						countries.add(new SelectOption(country.id, country.name));
	    		}	    		    			
	    		
    		
	    	}
    	} catch (Exception e){
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.Error, e.getMessage() + '<br/>' + e.getStackTraceString()));    		
    	}
    	
    	return countries;
    }
    
    
	
	private List<SelectOption> cities;		
    public List<SelectOption> getCities(){
    	try {
	    	if(showcasetype == 'City' && cities == null && showcase.parentid != null){
	    		cities = new List<SelectOption>();
	    		cities.add(new SelectOption('', '-- Select City --'));
	    		Account countryAcc = [select name, (select id, name from childAccounts) from account where recordtype.name = 'Country' and id = :showcase.parentid];
	    		List<String> cityShowcases = new List<String>();
	    		for(Account acc : countryAcc.ChildAccounts)
	    			cityShowcases.add(acc.name);	
	    		
    			for(AggregateResult ar : [select BillingCity from account where recordtype.name = 'Campus' and BillingCity not in :cityShowcases and BillingCountry = :countryAcc.Name group by BillingCity order by BillingCity]){
    				String billingCity = (String) ar.get('BillingCity');
	    			cities.add(new SelectOption(billingCity, billingCity));
	    			if(newShowcaseName == billingCity)
	    				showcase.name = billingCity;
    			}
    		
	    	}
    	} catch (Exception e){
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.Error, e.getMessage() + '<br/>' + e.getStackTraceString()));    		
    	}
    	
    	return cities;
    }
	
	
	
	
	private Account getShowCase(String accoID){
		
		return [select id, Name, recordtype.name, Parent.Name, General_Description__c, showcase_cost_of_living__c, showcase_cost_of_living_unit__c, showcase_population__c, showcase_tel_area_code__c,
						showcase_timezone__c, showcase_total_area__c, Website, Account_Currency_Iso_Code__c, Videos__c, Near_to__c, Why_choose_this_campus__c, Features__c, Extra_Activities__c,
						BusinessEmail__c, Pathways_URL__c, Students_per_class__c, Total_Number_of_Students__c, Phone, BillingStreet, BillingCity, BillingState, BillingCountry, Nationality_Mix__c,
						Social_network_url__c, unemployment__c, minimum_wage_per_hour__c, showcase_cost_of_living_total__c
				from Account where id = :accoID];
		
	}
	
	public void editAccount(){
		action = ACTION_EDIT;
		isAddingNew = true;
	}
	
	public void saveAccount(){
		try {
			if(isAddingNew && showcase.RecordTypeId == null){
				string rt = [select id from recordtype where name = :showcaseType and isActive = true limit 1].id;
				showcase.RecordTypeId = rt;
			}
			system.debug('videos: ' + videos);
			showcase.videos__c = generateVideos();
			system.debug('showcase.videos__c: ' + showcase.videos__c);
			showcase.showcase_cost_of_living__c = generateCOL();
			showcase.showcase_cost_of_living_total__c = getCOLTotal();
			
			upsert showcase;	
			isAddingNew = false;
			refresh(showcase.id);
		} catch (Exception e){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);
            return;
		}
	}
	
	public void cancelEdit(){
		action = ACTION_EDIT;
		isAddingNew = false;
	}
	
	public List<SelectOption> getCurrencies() {
		List<SelectOption> options = new List<SelectOption>();
	    for(SelectOption op: xCourseSearchFunctions.getCurrencies())
	      options.add(new SelectOption(op.getValue(),op.getLabel()));	        
	    return options;
	}
	
	
	// VIDEOS
	public string videoKey{get{if(videoKey==null) videoKey = ''; return videoKey;} set;}
 	public string videoHostName{get{ if(videoHostName == null) videoHostName = ''; return videoHostName; } set;}
  	public string videoListKey {get;set;}
	public map<string, string> videos { get{ if(videos == null) videos = new map<string, string>(); return videos; } set; }
	public list<SelectOption> videoPL {
	    get{
	    	if(videoPL == null){
	        	videoPL = new List<SelectOption>();        
	        	videoPL.add(new SelectOption('YouTube','YouTube')); 
	        	videoPL.add(new SelectOption('Vimeo','Vimeo')); 
	      	}	        
	      	return videoPL;
	    }
		set;
	}
	public void addVideo(){
		if(!String.isEmpty(videoHostName))
	   		videos.put(videoKey, videoHostName);
	}
	  
	public void delVideo(){
		if(!String.isEmpty(videoListKey))
	    	videos.remove(videoListKey);
	}
	
	
	private string generateVideos(){
	    list<string> aux = new list<string>();
	      
	    for (String key : videos.keyset()) {
	    	if(videos.get(key).contains('Vimeo')){
	        	String vkey = vimeoThumbnail(key);
	        	aux.add(key+'>'+videos.get(key)+':'+vkey);
	      	} else if(videos.get(key) == 'Youtube') aux.add(key+'>'+videos.get(key));
	    }
	    return String.join(aux,';');
  	}
  	
  	private void genVideos(list<string> vhs){
    	list<string> aux = new list<string>();
    	for(string v: vhs){
      		aux = v.split('>');
  			videos.put(aux[0], aux[1]);      
		}    
  	}
	
	
	private class jsonVimeo{
    	public string id, title, description, url, upload_date, mobile_url, thumbnail_small, thumbnail_medium, thumbnail_large, user_id, user_name, user_url, user_portrait_small,
      			user_portrait_medium, user_portrait_large, user_portrait_huge, duration, width, height, tags, embed_privacy;
  	}
  
	//VIMEO
	private string vimeoThumbnail(string vimeoCode){
	    HttpRequest req = new HttpRequest();
	    HttpResponse res = new HttpResponse();
	    Http http = new Http();
	    
	    req.setEndpoint('https://vimeo.com/api/v2/video/'+ vimeoCode +'.json');
	    req.setMethod('GET');
	    
	    List<jsonVimeo> listVimeo; 
	    String vk = '';
	    
	    try {
	    	res = http.send(req);
	      	system.debug('@#$ res.getBody(): ' + res.getBody());
	        
		    listVimeo = ( List<jsonVimeo> ) JSON.deserialize( res.getBody(), List<jsonVimeo>.class );
		    system.debug('list object: ' + listVimeo.get(0).thumbnail_small);
	      
	      	vk = vimeoKey(listVimeo.get(0).thumbnail_small);
	  
	    } catch(Exception e) {
	    	System.debug('Callout error: '+ e);
	      	System.debug(res.toString());
	      	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, e.getMessage());
            ApexPages.addMessage(myMsg);
	    }
	    
	    return vk;
	}
	
	private string vimeoKey(string thumbnail){
	    string key;
	    list<string> aux = new list<string>();
	    if(thumbnail != null){
	    	aux = thumbnail.split('video');
	      	aux = aux.get(1).split('/');
	      	aux = aux.get(1).split('_');
	      	key = aux.get(0);
	      	System.debug('--aux: '+aux);
	      	System.debug('--key: '+key);
	    }
	    return key; 
	}
	
	//VIMEO
	
	
	// VIDEOS
	
	
	
	//COST OF LIVING
	
	public string colName { get{if(colName == null) colName=''; return colName;} set; } 
  	public Double colPrice { get; set; } 
  	public string colKey {get;set;}
  	public map<string, Double> costOfLiving { get{if(costOfLiving == null) costOfLiving = new map<string, Double>(); return costOfLiving;} set; }
  	public Integer costOfLivingSize { get { return costOfLiving.size(); } private set; }
  	
  	public List<SelectOption> getCOLItems(){
  		List<SelectOption> options = new List<SelectOption>();
  		
   		Schema.DescribeFieldResult fieldResult = Account.showcase_cost_of_living_items__c.getDescribe();
   		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();        
   		for( Schema.PicklistEntry f : ple)
      		options.add(new SelectOption(f.getLabel(), f.getValue()));       
   		return options;
	}
	
	public List<String> getListCOLItems(){
  		List<String> options = new List<String>();
  		
   		Schema.DescribeFieldResult fieldResult = Account.showcase_cost_of_living_items__c.getDescribe();
   		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();        
   		for( Schema.PicklistEntry f : ple)
      		options.add(f.getLabel());       
   		return options;
	}
  	
  	public void addCostOfLiving(){
	  	if(!String.isEmpty(colName) && colPrice != null)
	  		costOfLiving.put(colName, colPrice);
	}
	  
	public void delCostOfLiving(){
		if(!String.isEmpty(colKey))
	    	costOfLiving.remove(colKey);
	    
	}
  	
  	private double getCOLTotal(){
  		double total = 0;
  		for(String key : costofliving.keyset())
	    	total += costofliving.get(key) != null ? costofliving.get(key) : 0;
	    
	    return total;
  	}
  	
  	private string generateCOL(){
	    list<string> aux = new list<string>();
	    for (String key : costofliving.keyset()) {
	    	aux.add(key+'>'+costofliving.get(key));
	    }
	    return String.join(aux,';');
  	}
  	private void genCOL(list<string> network){
		list<string> aux = new list<string>();
	    for(string n: network){
	    	aux = n.split('>');
	      	costOfLiving.put(aux[0], Double.valueof(aux[1]));
	    }    
	}
	//COST OF LIVING
	
	
	
	
	//NEAT TO
	public list<string> nearToList { get{if(nearToList == null) nearToList = new list<string>(); return nearToList;} set; }
	
	private void generateNearTo(String nt){
     	if(nt != null) nearToList = nt.split('!#!');    
  	}
	//NEAR TO
  
  	//WHY CHOOSE THIS CAMPUS
  	public list<string> whyChooseCampus { get{if(whyChooseCampus == null) whyChooseCampus = new list<string>(); return whyChooseCampus;} set; }
	private void generatewhyChooseCampus(String wctc){
		if(wctc != null) whyChooseCampus = wctc.split('!#!');  	
	}
    //WHY CHOOSE THIS CAMPUS
    
    
    //FEATURES
    public list<string> featuresList { get{if(featuresList == null) featuresList = new list<string>(); return featuresList;} set; }
	private void generateFeatures(string f){
		if(f != null) featuresList = f.split('!#!');
	}
    //FEATURES
    
    //EXTRA ACTIVITIES
    public list<string> extraActList { get{if(extraActList == null) extraActList = new list<string>(); return extraActList;} set; }
    private void generateExtraAct(string ea){    
    	if(ea != null) extraActList = ea.split('!#!');
  	}
    //EXTRA ACTIVITIES
    
    
    
    
    //NATIONALITY MIX
    public map<string, string> natioPercent2 { get{if(natioPercent2 == null) natioPercent2 = new map<string, string>(); return natioPercent2;} set; }
    public decimal totalPercentage {get{ if(totalPercentage == null) totalPercentage = 0; return totalPercentage; } set;}
    private void generateNationalityMix(list<string> nm){	    
	    list<string> auxList = new list<string>();
	    system.debug('nm: ' + nm); 
	    for(String nat: nm){
	    	system.debug('nat: ' + nat);
	      	auxList = nat.split('>');
	      	system.debug('auxList: ' + auxList);
	      	natioPercent2.put(auxList[0], auxList[1]);
	      	totalPercentage += decimal.valueOf(auxList[1]);
	      	system.debug('natioPercent2**: ' + natioPercent2); 
	    }   
	   system.debug('natioPercent2: ' + natioPercent2); 
	}
    //NATIONALITY MIX
    
   
    
    
    
}