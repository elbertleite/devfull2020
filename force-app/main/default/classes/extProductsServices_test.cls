/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class extProductsServices_test {

    static testMethod void myUnitTest() {
        
        TestFactory testFactory = new TestFactory();
		
        Account agency = TestFactory.createAgency();
        
        Contact createEmployee = TestFactory.createEmployee(agency);
        
        Quotation_Products_Services__c qps = new Quotation_Products_Services__c();
		qps.Agency__c = agency.id;
		qps.Category__c = 'Other';
		qps.Product_Name__c = 'Student Visa';
		qps.Quantity__c = 1;
		qps.Price__c = 333;
		qps.Unit_description__c = 'Unit';
		qps.Description__c = 'This fee applies to each dependant 18yrs and over in a Student Visa application.';
		qps.Currency__c = 'AUD';
		qps.Selected__c = true;
		insert qps;
        
        Quotation_List_Products__c qlp = new Quotation_List_Products__c();
		qlp.Quotation_Products_Services__c = qps.id;
		qlp.Quantity__c = 1;
		qlp.Value__c = 100;
		qlp.isSelected__c = true;
		insert qlp;
		
		Quotation_Products_Services__c qps2 = new Quotation_Products_Services__c();
		qps2.Agency__c = agency.id;
		qps2.Category__c = 'Other';
		qps2.Product_Name__c = 'Student Visa 2';
		qps2.Quantity__c = 1;
		qps2.Price__c = 345;
		qps2.Unit_description__c = 'Unit';
		qps2.Description__c = 'This fee applies to each dependant 18yrs and over in a Student Visa application.';
		qps2.Currency__c = 'AUD';
		qps2.Selected__c = true;
		insert qps2;
        
		
        
        ApexPages.Standardcontroller controller = new Apexpages.Standardcontroller(agency);
        extProductsServices eps = new extProductsServices(controller);
        
        ApexPages.currentPage().getParameters().put('prodId', qps.id);
        extProductsServices epsEdit = new extProductsServices(controller);
        
        eps.refreshProducts();
        
        eps.newProduct.product.Product_Name__c = 'Flight Ticket';
        eps.newProduct.product.Quantity__c = 1;
        eps.newProduct.product.Price__c = 100;
        eps.newProduct.product.Category__c = 'Travel';
        eps.newProduct.product.Unit_description__c = 'Unit';
        eps.newProduct.product.Currency__c = 'AUD';
        eps.newProduct.product.Group_Other__c = true;
        eps.newProduct.product.Description__c = 'flight ticket description';
        
        eps.productItem.Quantity__c = 1;
        eps.productItem.Value__c = 150;
        
        eps.addPrice();
        
        eps.productItem.Quantity__c = 2;
        eps.productItem.Value__c = 195;
        
        eps.addPrice();
        
        eps.saveproduct();

        eps.searchProduct();
        User currentUser = eps.currentUser;
        list<SelectOption> commissionType = eps.commissionType;
        
        
		
		ApexPages.currentPage().getParameters().put('prodId', qps.id);
		ApexPages.currentPage().getParameters().put('category', qps.Category__c);
        eps.deleteProdItems();
        
        
        ApexPages.currentPage().getParameters().put('category', qps.Category__c);
        eps.deleteProduct();
        
        eps.getUnitsRange();
        ApexPages.currentPage().getParameters().put('id', agency.id);
        eps.getCurrencies();
        
        
        extProductsServices.productPrices pp = new extProductsServices.productPrices(); 
        
        
        
        
        
        
    }
}