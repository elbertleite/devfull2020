/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class DeleteSynchronizedRecords_test {

    static testMethod void myUnitTest() {
        
		TestFactory tf = new TestFactory();
		String nationality =  'Published Price';
		Account agency = tf.createAgency();
		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);
		Course__c course = tf.createCourse();
		Campus_Course__c cc = tf.createCampusCourse(campus, course);		
		tf.createCoursePrice(cc, nationality);
		tf.createCourseFreeUnitsPromotion(cc, nationality);
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		tf.createCourseExtraFee(cc, nationality, p);
		tf.createCourseIntakeDate(cc);
		
		
		Account_Picture_File__c apf = new Account_Picture_File__c();
		apf.Parent__c = campus.id;
		insert apf;
		
		Nationality_Mix__c nm = new Nationality_Mix__c();
    	nm.Account__c = campus.id;
    	nm.Percentage__c = 10;
    	nm.Nationality__c = 'South America';
    	insert nm;
    	
    	Video__c v = new Video__c();
    	v.Account__c = campus.id;
    	v.VideoID__c = 'sjdinf7823';
    	insert v;

		Delete_Synchronization__c ds = new Delete_Synchronization__c();
		insert ds;		

    }
}