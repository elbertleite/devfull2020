public with sharing class Batch_Fill_Empty_Status{
	public Batch_Fill_Empty_Status(){
		system.debug('');
	}
}
/*global class Batch_Fill_Empty_Status implements Database.Batchable<sObject> {
	
	String query;
	
	global Batch_Fill_Empty_Status() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		//String query = 'SELECT ID, Name, CreatedDate, Status__c, Lead_Stage__c, (SELECT ID, Client__c, Stage_Item__c, Stage_Item_Id__c, Stage__c, Last_Saved_Date_Time__c FROM Client_Stage_Follow_up__r ORDER BY Last_Saved_Date_Time__c ) FROM Contact WHERE Status__c = null AND RecordType.Name IN (\'Lead\',\'Client\') ORDER BY CreatedDate DESC';
		
		Datetime cutDate = Datetime.newInstance(2018, 01, 01);

		String query = 'SELECT ID, Name, CreatedDate, Status__c, Lead_Stage__c, (SELECT ID, Client__c, Stage_Item__c, Stage_Item_Id__c, Stage__c, Last_Saved_Date_Time__c FROM Client_Stage_Follow_up__r ORDER BY Last_Saved_Date_Time__c) FROM Contact WHERE RecordType.Name IN (\'Client\') AND CreatedDate < :cutDate ORDER BY CreatedDate DESC';

		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Contact> ctts) {

		Map<String, Client_Stage_Follow_up__c> lastChecked = new Map<String, Client_Stage_Follow_up__c>();
		for(Contact ctt : ctts){
			if(ctt.Client_Stage_Follow_up__r != null && !ctt.Client_Stage_Follow_up__r.isEmpty()){
				for(Client_Stage_Follow_up__c check: ctt.Client_Stage_Follow_up__r){
					lastChecked.put(ctt.ID, check);
				}
			}
		}

		List<Contact> toUpdate = new List<Contact>(); 
		Client_Stage_Follow_up__c check;
		String stage;
		String status;
		for(Contact ctt : ctts){
			check = lastChecked.get(ctt.ID);
			if(check == null || check.Stage_Item__c == 'NEW LEAD' && check.Last_Saved_Date_Time__c == ctt.CreatedDate){
				ctt.Status__c = 'LEAD - Not Interested';
				toUpdate.add(ctt);
			}
			//if(check != null){
			//	stage = check.Stage__c;
			//	status = check.Stage_Item__c;
			//}else{
			//	stage = 'Stage 0';
			//	status = 'NEW LEAD';
			//}
			//if(String.isEmpty(ctt.Lead_Stage__c)){
			//	ctt.Lead_Stage__c = stage;
			//}
			//ctt.Status__c = status;
		}
		update toUpdate;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}*/