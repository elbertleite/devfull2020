public class SetupAgencyController {
	
	private map<string,string> recordType;
	public String groupId {get;set;}
	public Account groupLink {get;set;}
	public Account agencyGroup {get;set;}
	public Account agency {get;set;}
	public list<Account> listAgencies {get;set;}
	public String action {get;set;}
	public boolean showError {get;set;}
	
	
	/*** Constructor ***/
	public SetUpAgencyController(){
		showError = false;
		groupId = ApexPages.currentPage().getParameters().get('id');		
		String tAction = ApexPages.currentPage().getParameters().get('a');
		recordType = IPFunctions.getRecordsType();
		
		//Add Agency
		if (tAction!=null && tAction.equals('new') && groupId!= null  && groupId != '') { 
			action = 'newAgency';
			recordType = IPFunctions.getRecordsType();
			
			agencyGroup = [SELECT Account_Link__c FROM Account where Id = :groupId];						
			agency = new Account();	
			
		}
		//Edit Agency [groupID in this case is AgencyID]
		else if (tAction!=null && tAction.equals('edit') && groupId!= null  && groupId != '') { 
			action = 'edit';
									
			agency = [SELECT Id, Name, Registration_name__c, Agency_Type__c, Account_Currency_Iso_Code__c, Optional_Currency__c, BillingLatitude, BillingLongitude, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Website, BusinessEmail__c, Phone, Skype__c FROM Account where id =:groupId];
			
		}
		//Group Details
		else if(groupId!= null && groupId != ''){
			action = 'details';
			agencyGroup = [SELECT Name, Group_Type__c FROM Account where Id = :groupId];			
			listAgencies = [SELECT Id, Name, Registration_name__c, Agency_Type__c, Account_Currency_Iso_Code__c, Optional_Currency__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Website, BusinessEmail__c, Phone, Skype__c FROM Account where ParentId = :groupId order by name];
		}
		//Create New Group And Agency
		else{
			action = 'setup';
			groupLink = new Account();
			agencyGroup = new Account();
			agency = new Account();
		}
	}
	
	public void refreshDetails(){
		agencyGroup = [SELECT Name, Group_Type__c FROM Account where Id = :groupId];
		listAgencies = [SELECT Id, Name, Registration_name__c, Agency_Type__c, Account_Currency_Iso_Code__c, Optional_Currency__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, Website, BusinessEmail__c, Phone, Skype__c FROM Account where ParentId = :groupId order by name];
	}
	
	/*** Update Agency ***/
	public void updateAgency(){
		showError = true;
		try{
			update agency;
		}catch(Exception e){
			system.debug('Error Message=> ' + e);
		}
		
	}
	
	/*** Save Group + Agency ***/
	public PageReference saveAgency(){		
		showError = true;
		system.savepoint save;
		try{			
			save = database.setSavepoint();
			if(action.equals('setup')){
				//LINK
				groupLink.Name = agencyGroup.Name  + ' Link';
				groupLink.RecordTypeId = recordType.get('Account Link');
				insert groupLink;		
				
				//GROUP
				agencyGroup.Account_Link__c = groupLink.Id;
				agencyGroup.RecordTypeId = recordType.get('Agency Group');
				insert agencyGroup;
				
				//AGENCY
				agency.Account_Link__c = groupLink.Id;
				agency.ParentId = agencyGroup.Id;
				agency.RecordTypeId = recordType.get('Agency');
				insert agency;
				
				listAgencies = new list<Account>();
				listAgencies.add(agency);
				
				PageReference pageRef = new PageReference('/apex/setup_agency_details?id='+agencyGroup.id);
				pageRef.setredirect(true);
				return pageRef;
				
			}else if(action.equals('newAgency')){
				//AGENCY
				agency.Account_Link__c = agencyGroup.Account_Link__c;
				agency.ParentId = groupId;
				agency.RecordTypeId = recordType.get('Agency');
				insert agency;
				
				return null;
			}else{
				return null;
			}
			
		}
		catch(Exception e){
			database.rollback(save);
			system.debug('Error Message=> ' + e);
			return null;
		}
	}
	
	public list<string> orderContactMap {get;set;}
	public map<string, agencyContact> mapContacts {get;set;}
	
	/*** Contact Inner Class ***/
	public class agencyContact{		
		public string cId {get;set;}
		public string cName {get;set;}
		public string cTitle {get;set;}
		public string cEmail {get;set;}
		public string cProfile {get;set;}
		public DateTime lastLogin {get;set;}
		public boolean cIsActive {get;set;}
	}	

	/*** Refresh Contacts ***/
	public string pnRerender {get;set;}	
	public string paramAgencyId {get;set;}	
	public void refreshContacts(){
		paramAgencyId = ApexPages.currentPage().getParameters().get('firstParam');
		searchContacts();
		paramAgencyId = null;
	}
	
	/*** Get Agency Contacts ***/
	public void searchContacts(){
		String agencyId;
		if(paramAgencyId!=null)
			agencyId = paramAgencyId;
		else{
			agencyId = ApexPages.currentPage().getParameters().get('agencyId');
		}
		system.debug('Agency ID==>' + agencyId);
		
		mapContacts = new map<String,AgencyContact>();	
		orderContactMap = new list<String>();
		set<id> contactIds = new set<id>();
		
		for(Contact c : [Select Id, Name, Title, Email From Contact Where AccountId = :agencyId and RecordTypeId = :recordType.get('Employee') ]){
			
			agencyContact a = new agencyContact();
			
			a.cId = c.Id;
			a.cName = c.Name;
			a.cTitle = c.Title;
			a.cEmail = c.Email;
			a.cIsActive = false;

			mapContacts.put(c.Name, a);
			contactIds.add(c.Id);
			
			orderContactMap.add(c.Name);
			
		}
		
		for(User u : [Select Contact.Name, Profile.Name, LastLoginDate From User where IsActive = true and ContactId in :contactIds]){
			agencyContact a = mapContacts.get(u.Contact.Name);
			if(a!=null){
				a.cProfile = u.Profile.Name;
				a.cIsActive = true;
				a.lastLogin = u.LastLoginDate;
				
				mapContacts.put(u.Contact.Name, a);
			}
		}		
		orderContactMap.sort();
		
		system.debug('Order==>' + orderContactMap);
		system.debug('Map==>' + mapContacts);
	}
	

	/*** Get Countries ***/
	public list<SelectOption> destinations {
		get{
	    	if(destinations == null){
	        	destinations = new List<SelectOption>();        
	        	Schema.DescribeFieldResult F = Account.fields.Destination__c.getDescribe();
	        	List<Schema.PicklistEntry> P = F.getPicklistValues();
	        for (Schema.PicklistEntry lst:P)
	          	destinations.add(new SelectOption(lst.getValue(),lst.getLabel())); 
	      	}	        
      		return destinations;
	    }
	    set;
	  }
	  
	/*** Get Currencies ***/
	public List<SelectOption> getCurrencies(){
		return xCourseSearchFunctions.getCurrencies();
	}	
}