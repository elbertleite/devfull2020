/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class extPictures_test {

    static testMethod void verifyPictures() {
    	
    	Map<String,String> recordTypes = new Map<String,String>();
    	
    	for( RecordType r :[select id, name from recordtype where isActive = true] )
    		recordTypes.put(r.Name,r.Id);
    	
    	Account school = new Account();
    	school.recordtypeid = recordTypes.get('School');
    	school.name = 'Test School';
    	insert school;
    	
    	Account campus = new Account();
    	campus.RecordTypeId = recordTypes.get('Campus');
    	campus.Name = 'Test Campus CBD';
    	campus.ParentId = school.id;
    	insert campus;
    	
    	Account_Picture_File__c apf = new Account_Picture_File__c();
    	apf.Parent__c = school.id;
    	apf.WIP__c = false;
    	insert apf;
    	
    	Account_Picture_File__c apfa = new Account_Picture_File__c();
    	apfa.Parent__c = campus.id;
    	apfa.WIP__c = false;
    	insert apfa;
    	
    	Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(campus);
    	extPictures ep = new extPictures(controller);
    	
    	List<extPictures.sDrivePictures> imgs = ep.imgs;
    	
    	extPictures.sDrivePictures dp = new extPictures.sDrivePictures();
    	String descript = dp.description;
    	String url = dp.url;
    	
    	
    	Apexpages.Standardcontroller controller2 = new Apexpages.Standardcontroller(school);
    	extPictures ep2 = new extPictures(controller);
    	
    	
    }
}