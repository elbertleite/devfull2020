public with sharing class report_not_assigned_new_leads {
	
	public User user{get;set;}
	public String pageToRedirect{get;set;}

	public List<IPClasses.DataReport> dataGraph{get;set;}
	public List<Contact> contacts{get;set;}

	private static final Integer MAX_ITENS_PER_PAGE = 30;
	public Integer totalResult{get;set;}
	public Integer currentPage{get;set;}
	public List<Integer> pages{get;set;}
	public List<Integer> allPages{get;set;}

	public String agencySelected{get;set;}
	public String groupSelected{get;set;}
	public String endDate{get;set;}
	public String beginDate{get;set;}
	public String orderBy{get;set;}

	public Set<String> idAgencies{get;set;}

	public boolean canExportToExcel{get;set;}
	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}
	
	public List<SelectOption> agencies{get;set;}

	public report_not_assigned_new_leads(){
		try{
			boolean isExportingToExcel = false;
			user = [Select Id, Contact.Export_Report_Permission__c, Contact.AccountId, Contact.Account.Id, Contact.Account.Parent.Id, Contact.Group_View_Permission__c, Aditional_Agency_Managment__c, Contact.Account.Global_Link__c  from User where Id = :UserInfo.getUserId()];

			canExportToExcel = user.Contact.Export_Report_Permission__c;

			if(!String.isEmpty(ApexPages.currentPage().getParameters().get('export')) && Boolean.valueOf(ApexPages.currentPage().getParameters().get('export'))){
				isExportingToExcel = true;
				beginDate = ApexPages.currentPage().getParameters().get('begin'); 
				endDate = ApexPages.currentPage().getParameters().get('end'); 
				groupSelected = ApexPages.currentPage().getParameters().get('group'); 
				agencySelected = ApexPages.currentPage().getParameters().get('agency'); 
				orderBy = ApexPages.currentPage().getParameters().get('order');
			}else{
				/*List <SetupEntityAccess> SEA =[SELECT Id, Parent.Name, Parent.Profile.Name, Parent.Profile.Id FROM SetupEntityAccess WHERE Parent.Profile.Id = :UserInfo.getProfileId() AND SetupEntityId in (SELECT Id FROM ApexPage WHERE Name = 'client_page' AND NamespacePrefix = null)];
				if(SEA.isEmpty()){ //DONT HAVE ACCESS TO CLIENT PAGE
					pageToRedirect = 'activity_log';
				}else{
					pageToRedirect = 'client_page';
				}*/

				endDate = Date.today().format();
				beginDate = Date.today().addYears(-1).format();
				agencySelected = user.Contact.Account.Id;
				groupSelected = user.Contact.Account.ParentId;
				currentPage = 1;

				this.agencies = getAgenciesUser(user.Contact.Account.ParentId);
				//this.agencies = loadAgencies(user.Contact.AccountId, user.Contact.Account.Parent.Id);

				orderBy = 'createdDate';
			}

			agencySelected = agencySelected == null ? '' : agencySelected;
			generateReport(isExportingToExcel, true, beginDate, endDate, groupSelected, agencySelected, orderBy);

		}catch(Exception e){
			system.debug('Error on report_not_assigned_new_leads() ===> ' + e.getLineNumber() + ' '+e.getMessage());	
		}

	}

	public void changePage(){
		String page = ApexPages.currentPage().getParameters().get('page');
		if(String.isEmpty(page)){
			currentPage = 1;
		}else{
			currentPage = Integer.valueOf(page);
		}
		agencySelected = agencySelected == null ? '' : agencySelected;
		generateReport(false, false, beginDate, endDate, groupSelected, agencySelected, orderBy);
		generatePagination();
	}

	public void generateReport(Boolean isExportingToExcel, Boolean generateGraph, String beginDate, String endDate, String groupID, String agencyID, String orderBy){
		try{
			String globalLink = user.Contact.Account.Global_Link__c;
			String query = 'SELECT ID, Name, RecordType.Name, CreatedDate, Account.Name, Account.ID, Account.Parent.Name, Account.Parent.ID FROM Contact ';
			String queryWhere = 'WHERE RecordType.Name in (\'Lead\',\'Client\') AND Account.Global_Link__c = :globalLink AND Lead_Assignment_To__c = null AND Owner__c = null '; 
			
			Date fromDate = null;
			Date toDate = null;

			if(beginDate != null){
				fromDate = Date.parse(beginDate); 
				queryWhere = queryWhere + ' AND CreatedDate >= :fromDate ';
			}
			if(endDate != null){
				toDate = Date.parse(endDate); 
				queryWhere = queryWhere + ' AND CreatedDate <= :toDate ';
			}		
			
			String report = 'fullReport';
			if(!String.isEmpty(groupID) && !'noAgency'.equals(groupID)){
				queryWhere = queryWhere + ' AND Account.Parent.ID = :groupID ';
				report = 'reportOfAgencies';
				if(!String.isEmpty(agencyID)){
					queryWhere = queryWhere + ' AND Account.ID = :agencyID ';
					report = 'reportOfAgency';
				}else{
					queryWhere = queryWhere + ' AND Account.ID IN :idAgencies ';
					agencyID = '';
				}
			}

			String queryOrderBy = '';
			if(orderBy == 'createdDate'){
				queryOrderBy = queryOrderBy + ' ORDER BY CreatedDate, Account.Name, Account.Parent.Name, Name ';
			}else if(orderBy == 'name'){
				queryOrderBy = queryOrderBy + ' ORDER BY name, CreatedDate, Account.Name, Account.Parent.Name ';			
			}else if(orderBy == 'type'){
				queryOrderBy = queryOrderBy + ' ORDER BY RecordType.Name, CreatedDate, Account.Name, Account.Parent.Name ';		
			}else if(orderBy == 'agency'){
				queryOrderBy = queryOrderBy + ' ORDER BY Account.Name, Account.Parent.Name, CreatedDate, Name ';
			}else if(orderBy == 'agencyGroup'){
				queryOrderBy = queryOrderBy + ' ORDER BY Account.Parent.Name, Account.Name, CreatedDate, Name ';
			}

			if(isExportingToExcel){
				contacts = Database.query(query + queryWhere + queryOrderBy);
			}else{
				if(currentPage == null){
					currentPage = 1;
				}
				Integer maxItens = MAX_ITENS_PER_PAGE * currentPage;
				Integer firstResult;
				if(currentPage > 1){
					firstResult = MAX_ITENS_PER_PAGE * (currentPage - 1) + 1;
				}else{
					firstResult = 0;
				}				
				contacts = Database.query(query + queryWhere + queryOrderBy + ' LIMIT :maxItens OFFSET :firstResult ');
				
				if(generateGraph){
					totalResult = 0;
					dataGraph = new List<IPClasses.DataReport>();

					if(Test.isRunningTest()){
						contacts.add(New Contact());
					}
					if(contacts != null && !contacts.isEmpty()){
						String groupBy = '';		
						if(report == 'fullReport'){
							groupBy = 'Account.Parent.Name';
						}else if(report == 'reportOfAgencies' || report == 'reportOfAgency'){
							groupBy = 'Account.Name';
						}
						String queryGraph = 'SELECT '+groupBy+' axis, COUNT(ID) cnt from Contact '+queryWhere+' GROUP BY ' + groupBy;
						String axis = null;
						for(AggregateResult ar : Database.query(queryGraph)){
							axis = String.isEmpty((string)ar.get('axis')) ? 'Not defined.' : (string)ar.get('axis');
							axis = axis + '(' + (Integer)ar.get('cnt') + ')';
							dataGraph.add(new IPClasses.DataReport( axis , (Integer)ar.get('cnt')));
							totalResult = totalResult + (Integer)ar.get('cnt');
						}
					}
					generatePagination();
				}
			}
		}catch(Exception e){
			system.debug('Error on generateReport() ===> ' + e.getLineNumber() + ' '+e.getMessage());	
		}
	}

	public void generatePagination(){

		allPages = new List<Integer>();
		pages = new List<Integer>();

		Integer numPages = totalResult / MAX_ITENS_PER_PAGE;
		if(Math.mod(totalResult, MAX_ITENS_PER_PAGE) > 1){
			numPages = numPages + 1;
		}
		for(Integer i = 1; i <= numPages; i++){
			allPages.add(i);
		}

		Integer maxPagesShow = 4;
		Integer firstPageShow;
		Integer lastPageShow;
		if(currentPage == 1){
			firstPageShow = 1;
			lastPageShow = maxPagesShow * 2 + 1;
		}else if(currentPage == allPages.size()){
			firstPageShow = allPages.size() - (maxPagesShow * 2 + 1); 
			lastPageShow = allPages.size();
		}else{
			firstPageShow = currentPage - maxPagesShow < 1 ? 1 : currentPage - maxPagesShow; 
            lastPageShow = currentPage + maxPagesShow > allPages.size() ? allPages.size() : currentPage + maxPagesShow;
		}

		firstPageShow = firstPageShow < 1 ? 1 : firstPageShow;

		for(;firstPageShow <= lastPageShow; firstPageShow++){
			if(firstPageShow <= allPages.size()){
				pages.add(firstPageShow);
			}
		}
	}

	public void changeOrderBy(){
		currentPage = 1;
		orderBy = ApexPages.currentPage().getParameters().get('orderBy');
		agencySelected = agencySelected == null ? '' : agencySelected;
		generateReport(false, false, beginDate, endDate, groupSelected, agencySelected, orderBy);
	}

	public void generateNewReport(){
		agencySelected = agencySelected == null ? '' : agencySelected;
		generateReport(false, true, beginDate, endDate, groupSelected, agencySelected, orderBy);
	}

	public void updateAgencies(){
		String idGroup = ApexPages.currentPage().getParameters().get('idGroup');
		//this.agencies = loadAgencies(user.Contact.AccountId, idGroup);	
		this.agencies = getAgenciesUser(idGroup);	
	}

	public List<SelectOption> getGroups(){
		Set<String> idsAllGroups = new Set<String>();
		for(SelectOption option : IPFunctions.getAgencyGroupsByPermission(user.Contact.AccountId, Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())){
			idsAllGroups.add(option.getValue());
			//
		}
		List<SelectOption> options = new List<SelectOption>();
		for(Account option : [Select Id, Name from Account where id in :idsAllGroups AND Inactive__c = false order by name]){
			options.add(new SelectOption(option.ID, option.Name));
		}
		
		return options;
	}

	public List<SelectOption> getAgenciesUser(String parentID){
		Set<String> idsAllAgencies = new Set<String>();
		List<SelectOption> agencyOptions = new List<SelectOption>();
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && !user.Contact.Group_View_Permission__c){ //set the user to see only his own agency
			for(Account a: [Select Id, Name from Account where id =:user.Contact.AccountId order by name]){
				idsAllAgencies.add(a.ID);
				//agencyOptions.add(new SelectOption(a.Id, a.Name));
			}
			if(user.Aditional_Agency_Managment__c != null){
				for(Account ac:ipFunctions.listAgencyManagment(user.Aditional_Agency_Managment__c)){
					idsAllAgencies.add(ac.ID);
					//agencyOptions.add(new SelectOption(ac.id, ac.name));
				}
			}
		}else{
			for( Account ag : [Select Id, Name From Account Where Recordtype.Name = 'Agency' and ParentId = :parentID]){
				idsAllAgencies.add(ag.ID);
				//agencyOptions.add(new SelectOption(ag.id, ag.Name));
			}
		}

		idAgencies = new Set<String>();

		agencyOptions.add(new SelectOption('','All Agencies Available'));
		for(Account option : [Select Id, Name from Account where id in :idsAllAgencies AND Inactive__c = false order by name]){
			idAgencies.add(option.ID);
			agencyOptions.add(new SelectOption(option.ID, option.Name));
		}
		return agencyOptions;
	}

	public List<SelectOption> loadAgencies(String agencyID, String groupID){
		Set<String> idsAllAgencies = new Set<String>();
		for(SelectOption option : IPFunctions.getAgencyOptions(agencyID, groupID, Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible())){
			idsAllAgencies.add(option.getValue());
		}
		
		idAgencies = new Set<String>();
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('','All Agencies Available'));
		for(Account option : [Select Id, Name from Account where id in :idsAllAgencies AND Inactive__c = false order by name]){
			idAgencies.add(option.ID);
			options.add(new SelectOption(option.ID, option.Name));
		}
		return options;
	}

	public Contact myDetails {
		get{
			if(myDetails == null)
				myDetails = UserDetails.getMyContactDetails();
			return myDetails;
		}
		set;
	}
}