public with sharing class extCampusOverview{

        public Account acco {
            get{
            if(acco==null)
                acco = new Account();
            return acco;}
            set{acco = value;}
        }

        public List<Campus_Course__c> CampusCourses {
        get{
        if(CampusCourses==null)
            CampusCourses = new List<Campus_Course__c>();
        return CampusCourses;}
        set{CampusCourses = value;}
    } 
    
        public extCampusOverview(ApexPages.StandardController controller) {
            this.acco = [Select ID, ParentID from Account where id = :controller.getRecord().id];
            retrieveCampusCourses();
        }
    
        public string courseName {get{if (courseName == null) courseName = ''; return courseName;} set;}
    
        private void retrieveCampusCourses(){
        
            string sql = 'Select C.Available_From__c, C.Course__r.Name, C.Course__r.Course_Unit_Type__c, C.Is_Available__c, C.Id, '+
                'campus__r.Name, C.Course__r.Sub_Type__c, C.Course__r.Course_Type__c, C.Course__r.Language__c, C.Course__r.Type__c, '+
                '( Select Campus_Course__c, Name, CoursePriceID__c, CoursePriceIdFormula__c, From__c, Nationality__c,  '+
                'Price_per_week__c, Price_valid_from__c, Price_valid_until__c from Course_Prices__r)  '+
                'from Campus_Course__c C ';
            
                sql += ' WHERE C.Campus__r.ID = \'' +acco.id+ '\'';
                sql += ' AND C.Is_Available__c = true';
                sql += ' ORDER BY C.Available_From__c';

            this.CampusCourses = Database.query(sql);
        
        }
        
}