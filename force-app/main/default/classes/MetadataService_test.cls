/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class MetadataService_test {

    static testMethod void myUnitTest() {
        

		new MetadataService.SessionHeader_element();
		new MetadataService.SessionHeader_element();
		new MetadataService.DebuggingInfo_element();
		new MetadataService.DebuggingHeader_element();
		new MetadataService.CallOptions_element();
		new MetadataService.AllOrNoneHeader_element();
		new MetadataService.LogInfo();
		new MetadataService.readMetadata_element();		
		new MetadataService.ObjectNameCaseValue();
		new MetadataService.LookupFilterTranslation();
		new MetadataService.LayoutSectionTranslation();
		new MetadataService.LayoutTranslation();
		new MetadataService.QuickActionTranslation();
		new MetadataService.ReportTypeColumnTranslation();
		new MetadataService.ReportTypeSectionTranslation();
		new MetadataService.ReportTypeTranslation();
		new MetadataService.SharingReasonTranslation();
		new MetadataService.StandardFieldTranslation();
		new MetadataService.ValidationRuleTranslation();
		new MetadataService.WebLinkTranslation();
		new MetadataService.WorkflowTaskTranslation();
		new MetadataService.RecordTypeTranslation();
		new MetadataService.ReadCustomObjectTranslationResult();
		new MetadataService.readCustomObjectTranslationResponse_element();
		new MetadataService.CustomObjectTranslation();
		new MetadataService.CustomFieldTranslation();
		new MetadataService.PicklistValueTranslation();
		
		MetadataService.MetadataPort metadataPort  = new MetadataService.MetadataPort();
        metadataPort.readMetadata(null, null);
        
    }
}