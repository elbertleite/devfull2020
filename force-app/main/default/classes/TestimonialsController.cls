public class TestimonialsController {
	
	public list <Testimonial__c> lTestimonials {get;set;}
	public Testimonial__c testimonial {get;set;}
	public boolean showError {get;set;}
	public string schoolId {get;set;}
	  public TestimonialsController(ApexPages.StandardController controller){
	  	if(ApexPages.currentPage().getParameters().get('currentPage')!=null && ApexPages.currentPage().getParameters().get('currentPage').equals('testimonials')){
	  		schoolId = ApexPages.currentPage().getParameters().get('id');
	  		listTestimonials();
	  	}else if(ApexPages.currentPage().getParameters().get('currentPage')!=null && ApexPages.currentPage().getParameters().get('currentPage').equals('edit')){
	  		testimonial = [Select Name, StudentName__c, Content__c, School__c from Testimonial__c where id = :ApexPages.currentPage().getParameters().get('id')];
	  		showError = false;
	  	}else{
	  		testimonial = new Testimonial__c();
	  		schoolId = ApexPages.currentPage().getParameters().get('idSch');
	  	}
	  	
	  }
	
	public void listTestimonials(){
		lTestimonials = new list<Testimonial__c>();
		lTestimonials = [Select Id, Name, StudentName__c from Testimonial__c where School__c = :schoolId];
	}

	public void deleteTestimonial(){
		Database.delete(ApexPages.currentPage().getParameters().get('delId'));
	}
	
	public void updateTestimonial(){
		if(testimonial.id==null)
			testimonial.School__c = schoolId;
		
		upsert testimonial;
		showError = true;
	}
	
}