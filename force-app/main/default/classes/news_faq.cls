global class news_faq {
    
    public static string selectedMainCategory = 'news';
    public static string selectedFromType = 'all';
    
    @RemoteAction
    global static String getNewsFAQ(String searchQuery, String mainCategory, String fromType) {
    	
    	selectedFromType = fromType;
    	selectedMainCategory = mainCategory;
    	
        List<Backoffice_System_Information__c> thelist = new List<Backoffice_System_Information__c>();
        List<NewsFAQ> newsfaqList = new List<NewsFAQ>();
        
        String filterByType ='';
        if(selectedFromType != 'all')
        	filterByType = ' and From__c = :selectedFromType ';
        
        if(searchQuery.length() > 2){
            
            String sosl = 'FIND \''+searchQuery+'*\' IN ALL FIELDS RETURNING Backoffice_System_Information__c (Id where Main_Category__c = :selectedMainCategory ' + filterByType + ') ';
           
            system.debug('contact sosl: ' + sosl);
            
            List<List<SObject>> searchList = search.query(sosl);
            map<string, Backoffice_System_Information__c> newsStuff = new map<string, Backoffice_System_Information__c> ((List<Backoffice_System_Information__c>)searchList[0]);

         
            System.debug('==>newsStuff: '+newsStuff);
            list<string> listIds = new list<string>();
            if(newsStuff != null && newsStuff.size() >0)
                listIds.addAll(newsStuff.keySet());
                
            
            
            thelist = [Select Agency__r.Name, Answer__c, Category__c, City__c, Country__c, Date_From__c, Date_To__c, Description__c, Expire_On__c, From__c, Language__c, Main_Category__c, Nationality_Group__c, Priority_News__c, Question__c, 
                        		Read__c, School__r.Name, Title__c, Files__c from Backoffice_System_Information__c where Id in :listIds order by LastModifiedDate desc, Title__c, Question__c limit 100];
            
        } else if(searchQuery.length() == 0){
            String filters = '';
            if(selectedMainCategory == 'news')
            	filters = ' and Expire_on__c >= ' + IPFunctions.formatSqlDate(System.today());
            else if(selectedMainCategory == 'promotions')
            	filters = ' and Date_to__c >= ' + IPFunctions.formatSqlDate(System.today());
            
            String sql = ' Select Agency__r.Name, Answer__c, Category__c, City__c, Country__c, Date_From__c, Date_To__c, Description__c, Expire_On__c, From__c, Language__c, Main_Category__c, Nationality_Group__c, Priority_News__c, Question__c, ' +
            			 ' Read__c, School__r.Name, Title__c, Files__c from Backoffice_System_Information__c where Main_Category__c = :selectedMainCategory ' + filters + filterByType + ' order by LastModifiedDate desc ';
         
            thelist = Database.query(sql);
            
        }
        
        if(thelist != null && !thelist.isEmpty()){
            
            for(Backoffice_System_Information__c bsi : thelist)
            	newsfaqList.add(createObj(bsi));
        }
        
        return JSON.serialize(newsfaqList);
    }
    
    public String allNewsFAQ {
    	get{
    		List<NewsFAQ> lnf = new List<NewsFAQ>();
    		if(allNewsFAQ == null){
    			
    			String filterByType = '';
    			String filters = '';
		        if(selectedFromType != 'all')
		        	filterByType = ' and From__c = :selectedFromType ';
		        if(selectedMainCategory == 'news')
	            	filters = ' and Expire_on__c >= ' + IPFunctions.formatSqlDate(System.today());
	            else if(selectedMainCategory == 'promotions')
	            	filters = ' and Date_to__c >= ' + IPFunctions.formatSqlDate(System.today());
		        
    			String sql = 'Select Agency__r.Name, Answer__c, Category__c, City__c, Country__c, Date_From__c, Date_To__c, Description__c, Expire_On__c, From__c, Language__c, Main_Category__c, ' +
									'Nationality_Group__c, Priority_News__c, Question__c, Read__c, School__r.Name, Title__c, Files__c ' +
							 'from Backoffice_System_Information__c where Main_Category__c = :selectedMainCategory ' + filters + filterByType + 'order by LastModifiedDate desc limit 100';
							 
    			for(Backoffice_System_Information__c bsi : Database.query(sql))
    				lnf.add(createObj(bsi));
    																		
    		}
    		return JSON.serialize(lnf);
    	}
    	set;
    }
    
    private static NewsFAQ createObj(Backoffice_System_Information__c bsi){
    	NewsFAQ nf = new NewsFAQ();
    	nf.id = bsi.id;
    	nf.Agency = bsi.Agency__r.Name;
		nf.Answer = bsi.Answer__c;
		nf.Category = bsi.Category__c;
		nf.City = bsi.City__c;
		nf.Country = bsi.Country__c;
		nf.Date_From = bsi.Date_From__c != null ? bsi.Date_From__c.format() : '';
		nf.Date_To = bsi.Date_To__c != null ? bsi.Date_To__c.format() : '';
		nf.Description = bsi.Description__c;
		nf.Expire_On = bsi.Expire_On__c != null ? bsi.Expire_On__c.format() : '';
		nf.FromType = bsi.From__c;
		nf.Language = bsi.Language__c;
		nf.Main_Category = bsi.Main_Category__c;
		nf.Nationality_Group = bsi.Nationality_Group__c;
		//nf.Priority_News = bsi.Priority_News__c;
		nf.Question = bsi.Question__c;
		//nf.Read = bsi.Read__c;
		nf.School = bsi.School__r.Name;
		nf.Title = bsi.Title__c;
		try {
			if(bsi.Files__c != null){
				nf.files = new List<File>();
				for(String str : bsi.Files__c.split('@@')){
					String[] fnames = str.split('##');
					File f = new File();
					f.name = fnames[0];
					f.url = fnames[1];
					nf.files.add(f); 
				}
									
			}
		} catch (Exception e){
			
		}
		
		
		return nf;
    }
    
    public class NewsFAQ {
    	public String id {get;set;}
    	public String Agency {get;set;}
		public String Answer {get;set;}
		public String Category {get;set;}
		public String City {get;set;}
		public String Country {get;set;}
		public String Date_From {get;set;}
		public String Date_To {get;set;}
		public String Description {get;set;}
		public String Expire_On {get;set;}
		public String FromType {get;set;}
		public String Language {get;set;}
		public String Main_Category {get;set;}
		public String Nationality_Group {get;set;}
		public String Priority_News {get;set;}
		public String Question {get;set;}
		public String Read {get;set;}
		public String School {get;set;}
		public String Title {get;set;}
    	public List<File> files {get;set;}
    }
    
    public class File {
    	public String name {get;set;}
    	public String url {get;set;}
    }
    
}