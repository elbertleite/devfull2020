@isTest
private class commission_agency_request_test {

	static testMethod void myUnitTest() {
		TestFactory tf = new TestFactory();
		Account school = tf.createSchool();
		Account agencyGroup1 = tf.createAgencyGroup();
		Account agencyGroup2 = tf.createAgencyGroup();
		Account agencyGroup4 = tf.createAgencyGroup();
		Account agency = tf.createSimpleAgency(agencyGroup1);
		Account agency2 = tf.createSimpleAgency(agencyGroup2);
		Account agency3 = tf.createSimpleAgency(agencyGroup2);
		Account agency4 = tf.createSimpleAgency(agencyGroup2);
		Contact emp = tf.createEmployee(agency);
		Contact emp2 = tf.createEmployee(agency2);
		Contact emp4 = tf.createEmployee(agency4);
		User portalUser = tf.createPortalUser(emp);
		Account campus = tf.createCampus(school, agency);
		Course__c course = tf.createCourse();
		Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
		Contact client = tf.createClient(agency);

		list<Back_Office_Control__c> bo = new list<Back_Office_Control__c>();
		bo.add(new Back_Office_Control__c(Agency__c = agency.id, Country__c = 'Australia', Backoffice__c = agency.id, Services__c = 'Request_Share_Commission'));
		bo.add(new Back_Office_Control__c(Agency__c = agency2.id, Country__c = 'Australia', Backoffice__c = agency.id, Services__c = 'Request_Share_Commission'));
		bo.add(new Back_Office_Control__c(Agency__c = agency3.id, Country__c = 'Australia', Backoffice__c = agency.id, Services__c = 'Request_Share_Commission'));
		bo.add(new Back_Office_Control__c(Agency__c = agency4.id, Country__c = 'Australia', Backoffice__c = agency.id, Services__c = 'Request_Share_Commission'));


		bo.add(new Back_Office_Control__c(Agency__c = agency.id, Country__c = 'Australia', Backoffice__c = agency2.id, Services__c = 'Receive Share Comm. Request'));
		bo.add(new Back_Office_Control__c(Agency__c = agency2.id, Country__c = 'Australia', Backoffice__c = agency2.id, Services__c = 'Receive Share Comm. Request'));
		bo.add(new Back_Office_Control__c(Agency__c = agency3.id, Country__c = 'Australia', Backoffice__c = agency2.id, Services__c = 'Receive Share Comm. Request'));
		bo.add(new Back_Office_Control__c(Agency__c = agency4.id, Country__c = 'Australia', Backoffice__c = agency2.id, Services__c = 'Receive Share Comm. Request'));

		insert bo;

		client_course__c booking = tf.createBooking(client);
		client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);

		List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);

		agencyGroup1.name = 'Group1';
		agencyGroup2.name = 'Group2';
		agency.name = 'Agency1';
		agency2.name = 'Agency2';

		update agencyGroup1;
		update agencyGroup2;
		update agency;
		update agency2;

		cc.Enrolment_Date__c = System.today();
		cc.Enroled_by_Agency__c = agency.id;
		cc.Commission_Type__c = 0;
		cc.share_commission_request_agency__c = agency3.id;
		cc.share_commission_decision_status__c = 'Accepted';
		update cc;

		instalments[2].Received_By_Agency__c = agency.id;
		instalments[2].Received_Date__c = System.today();
		instalments[2].Commission_Confirmed_On__c = System.today();
		instalments[2].Commission_Paid_Date__c  = System.today();
		instalments[2].isPDS__c  = true;
		update instalments;


		client.current_agency__c = agency2.id;
		update client;

		instalments[0].Received_By_Agency__c = agency4.id;
		instalments[0].Received_Date__c = System.today();
		instalments[0].Commission_Confirmed_On__c = System.today();
		instalments[0].Commission_Paid_Date__c  = System.today();
		instalments[0].isShareCommissionSplited__c = true;
		instalments[0].isRequestedShareCommission__c  = true;

		instalments[1].Received_By_Agency__c = agency4.id;
		instalments[1].Received_Date__c = System.today();
		instalments[1].Commission_Confirmed_On__c = System.today();
		instalments[1].Commission_Paid_Date__c  = System.today();
		instalments[1].isShareCommissionSplited__c = true;
		instalments[1].isRequestedShareCommission__c  = true;
		instalments[1].isPDS__c  = true;

		instalments[3].Received_By_Agency__c = agency4.id;
		instalments[3].Received_Date__c = System.today();
		instalments[3].Commission_Confirmed_On__c = System.today();
		instalments[3].Commission_Paid_Date__c  = System.today();
		instalments[3].isShareCommissionSplited__c = true;

		update instalments;

		String invoiceId;
		
		Test.startTest();
		system.runAs(portalUser){
			// CREATE INVOICE
			commission_agency_request testClass = new commission_agency_request();

			testClass.getPeriods();
			client_course_instalment__c dates = testClass.dates;

			List<String> summaryOptions = testClass.getSummaryOptions();

			ApexPages.currentpage().getparameters().put('toAgency', agency2.id);
			ApexPages.currentpage().getparameters().put('ctName', agency.BillingCountry);
			ApexPages.currentpage().getparameters().put('ctCurrency', agency.account_currency_iso_code__c);

			testClass.selectedIds =  agency.id + '-' + instalments[0].id + '-1;'+ agency4.id + '-' + instalments[0].id + '-2;';
			// PAY INVOICE
			testClass.setIdToCreate();
			testClass.changeBank();

			testClass.dueDate.Due_Date__c = system.today().addDays(-1);
			testClass.createInvoice();

			testClass.dueDate.Due_Date__c = system.today();
			testClass.inv.Summary__c = 'test';
			testClass.createInvoice();


			ApexPages.currentpage().getparameters().put('toAgency', agency2.id);
			ApexPages.currentpage().getparameters().put('ctName', agency.BillingCountry);
			ApexPages.currentpage().getparameters().put('ctCurrency', agency.account_currency_iso_code__c);

			testClass.selectedIds = agency3.id + '-' + instalments[0].id + '-3;' + agency.id + '-' + instalments[1].id + '-1;' + agency4.id + '-' + instalments[1].id + '-3;' + agency3.id + '-' + instalments[2].id + '-4;' + agency2.id + '-' + instalments[3].id + '-5;';

			testClass.setIdToCreate();
			testClass.changeBank();

			testClass.dueDate.Due_Date__c = system.today().addDays(-1);
			testClass.createInvoice();

			testClass.dueDate.Due_Date__c = system.today();
			testClass.inv.Summary__c = 'test';
			testClass.createInvoice();
		
			// END -- Create Invoice

	 	}

		Profile portalProfile = [Select id from Profile where Name like 'Agency Global Manager' order by LastModifiedDate DESC LIMIT 1];

		List<User> users = new list<User>();

		User portalUser2 = new User(
			Username = System.now().millisecond() + 'portalUser2@test.com',
			ContactId = emp2.Id,
			ProfileId = portalProfile.Id,
			Alias = 'Emp2Usr',
			Email = 'emp2user@test.com',
			EmailEncodingKey = 'UTF-8',
			LastName = 'surname',
			CommunityNickname = 'emp2user',
			TimeZoneSidKey = 'America/Los_Angeles',
			LocaleSidKey = 'en_US',
			LanguageLocaleKey = 'en_US'
		);
		User portalUser4= new User(
			Username = System.now().millisecond() + 'portalUser4@test.com',
			ContactId = emp4.Id,
			ProfileId = portalProfile.Id,
			Alias = 'Emp4Usr',
			Email = 'emp4user@test.com',
			EmailEncodingKey = 'UTF-8',
			LastName = 'surname',
			CommunityNickname = 'emp4user',
			TimeZoneSidKey = 'America/Los_Angeles',
			LocaleSidKey = 'en_US',
			LanguageLocaleKey = 'en_US'
		);
		
		users.add(portalUser2);
		users.add(portalUser4);

		Database.insert(users);

		system.runAs(portalUser4){
			commission_pay_agency pay = new commission_pay_agency();

			ApexPages.currentpage().getparameters().put('country', pay.result[0].countryName);
			ApexPages.currentpage().getparameters().put('agencyId', pay.result[0].allAgencies[0].agencyId);
			ApexPages.currentpage().getparameters().put('invId', pay.result[0].allAgencies[0].invoices[0].invoice.id);

			invoiceId = pay.result[0].allAgencies[0].invoices[0].invoice.id;

			Client_Document__c document = [SELECT Id, Preview_Link__c FROM Client_Document__c WHERE Invoice__c = :pay.result[0].allAgencies[0].invoices[0].invoice.id];
			document.Preview_Link__c = 'testDoc.txt'+FileUpload.SEPARATOR_URL+'urlFile'+FileUpload.SEPARATOR_FILE;
			update document;

			pay.recalculateRate();

			ApexPages.currentpage().getparameters().put('country', pay.result[0].countryName);
			ApexPages.currentpage().getparameters().put('agId', pay.result[0].allAgencies[0].agencyId);
			ApexPages.currentpage().getparameters().put('invId', pay.result[0].allAgencies[0].invoices[0].invoice.id);

			pay.confirmPayment();
			// END -- Pay Invoice
		}
		
		system.runAs(portalUser2){
			
			// CONFIRM MINI
			ApexPages.currentpage().getparameters().put('id', invoiceId);
			commission_confirm_deposit_mini mini = new commission_confirm_deposit_mini();

			mini.invoice.Commission_Paid_Date__c = system.today();
			mini.confirmDeposit();
			mini.unconfirmDeposit();
			// END -- Confirm Mini


			// PAY BO 
			commission_pay_bo payBo = new commission_pay_bo();
			list<SelectOption> paymentStatus = payBO.paymentStatus;

			ApexPages.currentpage().getparameters().put('country', payBo.result[0].countryName);
			ApexPages.currentpage().getparameters().put('agId', payBo.result[0].allAgencies[0].agencyId);
			ApexPages.currentpage().getparameters().put('invId', payBo.result[0].allAgencies[0].invoices[0].invoice.id);

			Client_Document__c document = [SELECT Id, Preview_Link__c FROM Client_Document__c WHERE Invoice__c = :payBo.result[0].allAgencies[0].invoices[0].invoice.id];
			document.Preview_Link__c = 'testDoc.txt'+FileUpload.SEPARATOR_URL+'urlFile'+FileUpload.SEPARATOR_FILE;
			update document;

			payBO.confirmDeposit();

			payBO.selectedPaymentStatus='paid';
			payBO.findInvoices();

			ApexPages.currentpage().getparameters().put('country', payBo.result[0].countryName);
			ApexPages.currentpage().getparameters().put('agId', payBo.result[0].allAgencies[0].agencyId);
			ApexPages.currentpage().getparameters().put('invId', payBo.result[0].allAgencies[0].invoices[0].invoice.id);
			payBO.setIdToUnconfirm();
			payBO.invReason.unconfirm_commission_reason__c = 'Testing';
			payBO.unconfirmPayment();


			payBO.selectedPaymentStatus='unpaid';
			payBO.findInvoices();

			ApexPages.currentpage().getparameters().put('country', payBo.result[0].countryName);
			ApexPages.currentpage().getparameters().put('agId', payBo.result[0].allAgencies[0].agencyId);
			ApexPages.currentpage().getparameters().put('invId', payBo.result[0].allAgencies[0].invoices[0].invoice.id);

			payBO.confirmDeposit();

			// END -- Pay BO

		}


		system.runAs(portalUser){
			// CONFIRM DEPOSIT
			commission_confirm_agency_deposit conf = new commission_confirm_agency_deposit();

			conf.paramCountry = conf.pageResult[0].countryName;
			conf.paramAgency = conf.pageResult[0].allAgencies[0].agencyId;
			conf.paramInvoice = conf.pageResult[0].allAgencies[0].invoices[0].invoice.id;
			conf.pageResult[0].allAgencies[0].invoices[0].invoice.Received_Currency__c = 'AUD';
			conf.pageResult[0].allAgencies[0].invoices[0].invoice.Received_Currency_Rate__c = 2.5;
			conf.pageResult[0].allAgencies[0].invoices[0].invoice.Received_Currency_Value__c = 0;
			conf.recalculateRate();

			ApexPages.currentpage().getparameters().put('country', conf.pageResult[0].countryName);
			ApexPages.currentpage().getparameters().put('agId', conf.pageResult[0].allAgencies[0].agencyId);
			ApexPages.currentpage().getparameters().put('invId', conf.pageResult[0].allAgencies[0].invoices[0].invoice.id);


			// ApexPages.currentpage().getparameters().put('id', conf.pageResult[0].allAgencies[0].invoices[0].invoice.id);
			// share_commission_invoice tstInvoice = new share_commission_invoice();

			// conf.setIdToUnconfirm();
			// conf.cancelInvoice();

			// system.debug('conf.pageResult==>' + conf.pageResult);

			// ApexPages.currentpage().getparameters().put('country', conf.pageResult[0].countryName);
			// ApexPages.currentpage().getparameters().put('agId', conf.pageResult[0].allAgencies[0].agencyId);
			// ApexPages.currentpage().getparameters().put('invId', conf.pageResult[0].allAgencies[0].invoices[0].invoice.id);
			// conf.confirmDeposit();

			// ApexPages.currentpage().getparameters().put('country', conf.pageResult[0].countryName);
			// ApexPages.currentpage().getparameters().put('agId', conf.pageResult[0].allAgencies[0].agencyId);
			// ApexPages.currentpage().getparameters().put('invId', conf.pageResult[0].allAgencies[0].invoices[0].invoice.id);
			// conf.pageResult[0].allAgencies[0].invoices[0].invoice.Commission_Paid_Date__c = system.today().addDays(1);
			// conf.confirmDeposit();

			// ApexPages.currentpage().getparameters().put('country', conf.pageResult[0].countryName);
			// ApexPages.currentpage().getparameters().put('agId', conf.pageResult[0].allAgencies[0].agencyId);
			// ApexPages.currentpage().getparameters().put('invId', conf.pageResult[0].allAgencies[0].invoices[0].invoice.id);
			// conf.pageResult[0].allAgencies[0].invoices[0].invoice.Commission_Paid_Date__c = system.today();
			//conf.confirmDeposit();

			// system.debug('conf.pageResult==>' + conf.pageResult);
			// conf.dates.Commission_Due_Date__c = system.today().addDays(-360);
			// conf.dates.Commission_Paid_Date__c = system.today().addDays(360);
			// conf.selectedPaymentStatus = 'confirmed';
			// conf.findInvoices();
			// system.debug('conf.pageResult==>' + conf.pageResult);
			// ApexPages.currentpage().getparameters().put('country', conf.pageResult[0].countryName);
			// ApexPages.currentpage().getparameters().put('agId', conf.pageResult[0].allAgencies[0].agencyId);
			// ApexPages.currentpage().getparameters().put('invId', conf.pageResult[0].allAgencies[0].invoices[0].invoice.id);
			// conf.setIdToUnconfirm();
			// conf.unconfirmDeposit();

		}

		Test.stopTest();
	}
}