global class Batch_Synchronize_RDStation implements Database.Batchable<Contact>, Database.AllowsCallouts, Database.Stateful{

	List<Contact> contacts;
	global List<String> errors;
	global String groupID;
	global Map<Integer, String> result; 
	global Boolean repeatInCaseOfErrors;
	
	global Batch_Synchronize_RDStation(String [] ids, String groupID, Boolean repeatInCaseOfErrors) {
		contacts = new List<Contact>();
		for(String id : ids){
			contacts.add(new Contact(ID = id));
		}
		this.repeatInCaseOfErrors = repeatInCaseOfErrors;
		this.groupID = groupID;
		errors = new List<String>(); 
	}
	
	global Iterable<Contact> start(Database.BatchableContext BC) {
		return contacts;
	}

   	global void execute(Database.BatchableContext BC, List<Contact> ctts) {
		for(Contact ctt : ctts){
			result = RDStationSaveLeadWebhook.syncWithRDStation(ctt.id, groupID, false);	
			if(result.containsKey(500)){
				errors.add(ctt.ID);
			}else{
				ctt.Synchronized_RDStation__c = true;
			}
		}
		update ctts;
	}
	
	global void finish(Database.BatchableContext BC) {
		
		Account accGroup = [SELECT ID, Name, RDStation_Emails_To_Send__c FROM Account WHERE ID = :groupID];
		Map<String, String> emailsCopy = new Map<String, String>(); 
		if(!String.isEmpty(accGroup.RDStation_Emails_To_Send__c)){
			for(String email : (List<String>) JSON.deserialize(accGroup.RDStation_Emails_To_Send__c, List<String>.class)){
				emailsCopy.put(email, email);
			}
		}

		String emailSubject;
		String body;
		Boolean hasErrors;
		if(Test.isRunningTest()){
			hasErrors = true;
		}else{
			hasErrors = !errors.isEmpty();
		}
		if(hasErrors){
			if(Test.isRunningTest()){
				repeatInCaseOfErrors = false;
			}
			if(repeatInCaseOfErrors){
				system.debug('ERRORS FOUND '+errors);
				Batch_Synchronize_RDStation batch = new Batch_Synchronize_RDStation(errors, groupID, false);
				Database.executeBatch(batch, 1);
			}else{
				emailSubject = 'RD Station Sync - Mass Update Error - ' + accGroup.Name + ' - '+IPFunctions.getCurrentEnvironment();
				body = errors.size()+' Hify contacts were not synchronized with RDStation. The contacts emails:';
				for(Contact ctt : [SELECT Email FROM Contact WHERE id IN :errors]){
					body = body + ' ' + ctt.Email + ',';
				}
				try{
					IPFunctions.sendEmailNoFuture('Education Hify', 'alerts.rdstation@educationhify.com', 'Hify Team', 'alerts.rdstation@educationhify.com', emailsCopy, emailSubject, body);
				}catch(Exception ex){
					system.debug('IT WAS NOT POSSIBLE TO SEND THE CONFIRMATION EMAIL. ');
				}
			}
		}

		/*Set<String> ids = new Set<String>();
		for(Contact ctt : contacts){
			if(!errors.contains(ctt.ID)){
				ids.add(ctt.ID);
			}
		}

		emailSubject = 'Contacts successfully synchronized with RDStation. ';
		body = ids.size()+' Hify contacts were successfully synchronized with RDStation. The contacts emails:';
		for(Contact ctt : [SELECT Email FROM Contact WHERE id IN :ids]){
			body = body + ' ' + ctt.Email + ',';
		}
		try{
			IPFunctions.sendEmailNoFuture('Education Hify', 'develop@educationhify.com', 'Hify Team', 'develop@educationhify.com', emailsCopy, emailSubject, body);
		}catch(Exception ex){
			system.debug('IT WAS NOT POSSIBLE TO SEND THE CONFIRMATION EMAIL. ');
		}*/

	}
	
}