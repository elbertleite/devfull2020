/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class client_course_deposits_history_test {

    static testMethod void myUnitTest() {
       
       TestFactory tf = new TestFactory();
       
       Account school = tf.createSchool();
       Account agency = tf.createAgency();
       Contact emp = tf.createEmployee(agency);
       User portalUser = tf.createPortalUser(emp);
       Account campus = tf.createCampus(school, agency);
       Course__c course = tf.createCourse();
       Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);

       
       Test.startTest();
       system.runAs(portalUser){
       	   Contact client = tf.createLead(agency, emp);
	       client_course__c booking = tf.createBooking(client);
	       client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
	       
	       client_course_deposit toDeposit = new client_course_deposit(new ApexPages.StandardController(client));
	       
	       String clientName = toDeposit.clientName;
	       toDeposit.getdepositListType();
	       Account agencyDetails = toDeposit.agencyDetails;
	       
	       
	       toDeposit.newDepositValue.Value__c=2230;
	       toDeposit.newDepositValue.Date_Paid__c=system.today();
	       toDeposit.newDepositValue.Payment_Type__c = 'Money Transfer';
	       toDeposit.addDepositValue();
	       toDeposit.saveDeposit();
	       
	       client_course_instalment_pay toPay = new client_course_instalment_pay(new ApexPages.StandardController(instalments[0]));
	       
	       toPay.newPayDetails.Payment_Type__c = 'cash';
		   toPay.newPayDetails.Value__c = 2230;
		   toPay.newPayDetails.Date_Paid__c = Date.today();
		   toPay.addPaymentValue();
		   toPay.savePayment();
		   
		   
		   client_course_deposits_history testClass = new client_course_deposits_history(new ApexPages.StandardController(client)); 
		    	       
	       Test.stopTest();
       }
    }
}