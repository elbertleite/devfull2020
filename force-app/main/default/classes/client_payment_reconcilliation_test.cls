@isTest
private class client_payment_reconcilliation_test {
	
	public static User portalUser {get{
	if (null == portalUser) {
	portalUser = [Select Id, Name, Contact.AccountId from User where email = 'test12345@test.com' limit 1];
	} return portalUser;} set;}

	@testSetup static void setup() {
		TestFactory tf = new TestFactory();

		tf.setupClientProdcutsAndCoursesTest();
		
		system.runAs(portalUser){
			
			//Pay Product
			client_product_service__c cliProduct = [Select Id from client_product_service__c WHERE Related_to_Product__c = null limit 1];
			client_course_product_pay payProd = new client_course_product_pay(new ApexPages.StandardController(cliProduct));

			payProd.newPayDetails.Payment_Type__c = 'Creditcard';
			payProd.newPayDetails.Value__c = payProd.totalPay;
			payProd.addPaymentValue();
			payProd.paidByAgency.Paid_by_Agency__c = true;
			payProd.savePayment();

			//Pay instalment
			list<client_course_instalment__c> instalments = [Select id, Instalment_Value__c, Client_Course__r.Client__c from client_course_instalment__c where Received_Date__c  = null];

			client_course_instalment_pay instPay = new client_course_instalment_pay(new ApexPages.StandardController(instalments[0]));
			instPay.newPayDetails.Value__c = instalments[0].Instalment_Value__c;
			instPay.newPayDetails.Date_Paid__c = system.today();
			instPay.newPayDetails.Payment_Type__c = 'Cash';
			instPay.addPaymentValue();
			instPay.savePayment();

			//Simulate a paid client invoice to reduce soql queries
			Invoice__c cliInvoice = new Invoice__c(Client__c = instalments[0].Client_Course__r.Client__c, isClientInvoice__c = true, Received_By_Agency__c = portalUser.Contact.AccountId, Received_Date__c = system.today());
			insert cliInvoice;

			instalments[1].Invoice__c = cliInvoice.id;
			instalments[1].Received_By_Agency__c = portalUser.Contact.AccountId;
			instalments[1].Received_Date__c = system.today();
			instalments[1].isMigrated__c = false;
			instalments[1].isPaidOffShore__c  = false;
			instalments[1].isPDS__c = false;
			instalments[1].isPCS__c = false;
			instalments[1].Paid_split__c = false;
			update instalments[1];

			client_course_instalment_payment__c pay = new client_course_instalment_payment__c(Invoice__c = cliInvoice.Id,  Received_By_Agency__c = portalUser.Contact.AccountId, Received_On__c = system.now(), Date_Paid__c = system.today(), Payment_Type__c = 'creditcard');
			insert pay;

		}
	}

	static testMethod void testConstructor() {

		system.runAs(portalUser){
			client_payment_reconcilliation p = new client_payment_reconcilliation();

			boolean redirectNewContactPage = p.redirectNewContactPage;
			String checkedPayments = p.checkedPayments;
			// String selectedSchool = p.selectedSchool;
			List<SelectOption> depositListType = p.depositListType;
			List<SelectOption> paymentTypes = p.paymentTypes;
			List<SelectOption> agencyGroupOptions = p.agencyGroupOptions;
			client_course_instalment_payment__c editPayment = p.editPayment;
			boolean showError = p.showError;
			boolean showAll = p.showAll;
			string selectedAgencyGroup = p.selectedAgencyGroup;
			p.getAgencies();
			p.changeGroup();
			p.getPeriods();
			p.getdateCriteriaOptions();
			// List<SelectOption> schools = p.schools;

			p.selectedPaymentTypes = 'Cash';
			p.dateCriteria = 'not dc';
			p.searchPayments();

		}
	}

	static testMethod void confirmPayment() {
		system.runAs(portalUser){

			client_payment_reconcilliation p = new client_payment_reconcilliation();
			p.checkedPayments = '';
			for(client_payment_reconcilliation.consolidation c : p.result)
				if(c.instalment != null)
					for(client_course_instalment_payment__c cp :c.instalment.client_course_instalment_payments__r){
						p.checkedPayments += cp.Id + ';';
					}
				else if(c.product != null)
					for(client_course_instalment_payment__c cp :c.product.client_course_instalment_payments__r){
						p.checkedPayments += cp.Id + ';';
					}
				else if(c.invoice != null)
					for(client_course_instalment_payment__c cp :c.invoice.client_course_instalment_payments__r){
						p.checkedPayments += cp.Id + ';';
					}

			p.SelectedPeriod = 'THIS_WEEK';
			p.confirmPayments();

			test.startTest();
			for(client_payment_reconcilliation.consolidation c : p.result)
				if(c.instalment != null)
					for(client_course_instalment_payment__c cp :c.instalment.client_course_instalment_payments__r){
						ApexPages.currentpage().getparameters().put('paymentid', cp.Id);
						ApexPages.currentpage().getparameters().put('objUp', c.instalment.id);
						p.unconfirmPayment();
					}
				else if(c.product != null)
					for(client_course_instalment_payment__c cp :c.product.client_course_instalment_payments__r){
						ApexPages.currentpage().getparameters().put('paymentid', cp.Id);
						ApexPages.currentpage().getparameters().put('objUp', c.product.id);
						p.unconfirmPayment();
					}
				else if(c.invoice != null)
					for(client_course_instalment_payment__c cp :c.invoice.client_course_instalment_payments__r){
						ApexPages.currentpage().getparameters().put('paymentid', cp.Id);
						ApexPages.currentpage().getparameters().put('objUp', c.invoice.id);
						p.unconfirmPayment();
					}

		}
	}

}