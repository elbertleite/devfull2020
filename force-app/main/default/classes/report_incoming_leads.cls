public with sharing class report_incoming_leads {   
    
    public List<ContactReport> contacts{get;set;}

    public report_incoming_leads() {
        if(ApexPages.currentPage().getParameters().get('exporting') == 'true'){
            String groupSelected = ApexPages.currentPage().getParameters().get('groupSelected');
            String employeeSelected = ApexPages.currentPage().getParameters().get('employeeSelected');
            Integer yearSelected = Integer.valueOf(ApexPages.currentPage().getParameters().get('yearSelected'));
            Integer monthSelected = Integer.valueOf(ApexPages.currentPage().getParameters().get('monthSelected'));
            
            Set<String> agenciesSent = new Set<String>();
            if(!String.isEmpty(ApexPages.currentPage().getParameters().get('agenciesSelected'))){
                agenciesSent = new Set<String>(ApexPages.currentPage().getParameters().get('agenciesSelected').split(','));
            }
            contacts = report_incoming_leads.generateListIncomingContacts(null, groupSelected, agenciesSent, employeeSelected, yearSelected, monthSelected, false);
        }
    }

    @RemoteAction
    @ReadOnly
	public static Map<String, Object> generateReport(List<String> allAgencies, String groupSelected, List<String> agenciesSelected, String employeeSelected, Integer yearSelected, Integer monthSelected){
        Map<String, Object> response = new Map<String, Object>();
        response.put('response', generateListIncomingContacts(new Set<String>(allAgencies), groupSelected, new Set<String>(agenciesSelected), employeeSelected, yearSelected, monthSelected, true));
        return response;
    }

    @RemoteAction
    @ReadOnly
    public static List<ContactReport> generateListIncomingContacts(Set<String> allAgencies, String groupSelected, Set<String> agenciesSelected, String employeeSelected, Integer yearSelected, Integer monthSelected, boolean showFullDate){
        
        String dateFormat = showFullDate ? 'dd/MM/yyyy hh:mm:ss' : 'dd/MM/yyyy';
        Set<String> agenciesToSearch;
        String query = 'SELECT ID, Name, Email, Ownership_History_Field__c FROM Contact WHERE Latest_Owner_Group_ID__c = :groupSelected AND CALENDAR_YEAR(Last_Change_Ownership_Groups__c) = :yearSelected AND CALENDAR_MONTH(Last_Change_Ownership_Groups__c) = :monthSelected ';

        if(agenciesSelected != null && !agenciesSelected.isEmpty()){
            agenciesToSearch = agenciesSelected;
            /*query = query + ' AND Current_Agency__c IN :agenciesSelected ';
            if(!String.isEmpty(employeeSelected) && employeeSelected != 'all'){
                query = query + ' AND Owner__c = :employeeSelected ';
            }*/
        }else{
            agenciesToSearch = allAgencies;
            //query = query + ' AND Current_Agency__c IN :allAgencies';
        }
        //query = query + ' AND CALENDAR_YEAR(LastModifiedDate) <= :yearSelected AND CALENDAR_MONTH(LastModifiedDate)';

        List<ContactReport> ctts = new List<ContactReport>();
        List<IPClasses.OwnershipHistory> ownershipHistory;
        Datetime ownershipDate;
        for(Contact ctt : Database.query(query)){
            if(!String.isEmpty(ctt.Ownership_History_Field__c)){
				ownershipHistory = (List<IPClasses.OwnershipHistory>) JSON.deserialize(ctt.Ownership_History_Field__c, List<IPClasses.OwnershipHistory>.class);
                if(ownershipHistory != null && !ownershipHistory.isEmpty()){
                    for(IPClasses.OwnershipHistory ownership : ownershipHistory){
                        ownershipDate = ownership.createdDate;
                        if(yearSelected == ownershipDate.year()){
                            system.debug('PASSOU PELO ANO ');
                            if(monthSelected == ownershipDate.month()){
                                system.debug('PASSOU PELO MES ');
                                if(ownership.toAgencyID != null && agenciesToSearch.contains(ownership.toAgencyID)){
                                    system.debug('PASSOU PELA AGENCIA ');
                                    if(employeeSelected != 'all'){
                                        if(ownership.toUserID == employeeSelected){
                                            ctts.add(new ContactReport(ctt, ownership, dateFormat));      
                                        }
                                    }else{
                                        ctts.add(new ContactReport(ctt, ownership, dateFormat));
                                    }
                                }
                            }
                        }
                        /*if(ownership.fromAgencyID != null && !agenciesToSearch.contains(ownership.fromAgencyID)){
                            if(ownership.toAgencyID != null && agenciesToSearch.contains(ownership.toAgencyID)){
                                if(yearSelected == ownershipDate.year()){
                                    if(monthSelected == ownershipDate.month()){
                                        system.debug('GOT HERE ');
                                        if(employeeSelected != 'all'){
                                            if(ownership.toUserID == employeeSelected){
                                                ctts.add(new ContactReport(ctt, ownership));      
                                            }
                                        }else{
                                            ctts.add(new ContactReport(ctt, ownership));
                                        }
                                    }
                                }
                            }
                        }*/
                    }
                }
            }
        }
        return ctts;
    }

    @RemoteAction
    @ReadOnly
	public static Map<String, String> initPage(){
		User user = [Select Id, Name, Report_Convertion_Rate_Filters__c, Contact.Export_Report_Permission__c, Contact.AccountId, Contact.Name, Contact.Account.Id, Contact.Account.Name, Contact.Account.Parent.RDStation_Campaigns__c, Contact.Account.Parent.Name, Contact.Account.Parent.Id, Aditional_Agency_Managment__c, Contact.Group_View_Permission__c, Contact.Agency_View_Permission__c from User where Id = :UserInfo.getUserId()];
		Map<String, String> response = new Map<String, String>();
		
        Date today = Date.today();

		response.put('groupSelected', user.Contact.Account.ParentId);
		response.put('canExportToExcel', String.valueOf(user.Contact.Export_Report_Permission__c));
		response.put('agencySelected', user.Contact.Account.Id);
		response.put('groups', loadGroups(user.Contact.AccountId));
		response.put('agencies', loadAgencies(user.Contact.Account.Id, user.Contact.Account.ParentId, Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()));
        response.put('months', loadMonths());
        response.put('years', loadYears(today.year()));
        response.put('monthSelected', String.valueOf(today.month()));
        response.put('yearSelected', String.valueOf(today.year()));
        
        response.put('employeeSelected', user.ID);
        response.put('employees', loadEmployees(user.Contact.Account.Id));
		response.put('hasPermissionGroup', String.valueOf(user.Contact.Group_View_Permission__c));  
      	response.put('hasAditionalAgency', String.valueOf(!String.isEmpty(user.Aditional_Agency_Managment__c)));  
      	response.put('hasPermissionAgency', String.valueOf(user.Contact.Agency_View_Permission__c));
          
        /*
		response.put('destinationSelected', '-- All Destinations --');
		response.put('typeOfSearch', 'firstClick');
		response.put('endDate', Date.today().format());
		response.put('beginDate', Date.today().addMonths(-1).format());
		response.put('showClientsEnrolled', 'false');

		response.put('rdStationCampaigns', loadCampaigns(user.Contact.Account.Parent.RDStation_Campaigns__c));
		
		response.put('destinations', loadDestinations(user.Contact.Account.ParentId));

		*/

		return response;
	}

    public static String loadYears(Integer yearBase){
        List<Integer> years = new List<Integer>();
        for(Integer i = yearBase - 10; yearBase >= i; yearBase--){
            years.add(yearBase);
        }
        return JSON.serialize(years);
    }
    public static String loadMonths(){
        List<IPClasses.SelectOptions> months = new List<IPClasses.SelectOptions>();
        months.add(new IPClasses.SelectOptions('1', 'January'));
        months.add(new IPClasses.SelectOptions('2', 'February'));
        months.add(new IPClasses.SelectOptions('3', 'March'));
        months.add(new IPClasses.SelectOptions('4', 'April'));
        months.add(new IPClasses.SelectOptions('5', 'May'));
        months.add(new IPClasses.SelectOptions('6', 'June'));
        months.add(new IPClasses.SelectOptions('7', 'July'));
        months.add(new IPClasses.SelectOptions('8', 'August'));
        months.add(new IPClasses.SelectOptions('9', 'September'));
        months.add(new IPClasses.SelectOptions('10', 'October'));
        months.add(new IPClasses.SelectOptions('11', 'November'));
        months.add(new IPClasses.SelectOptions('12', 'December'));
        return JSON.serialize(months);
    }
    public static String loadGroups(String agencyID){
		Set<String> idsAllGroups = new Set<String>();
    	for(SelectOption option : IPFunctions.getAgencyGroupsByPermission(agencyID, Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())){
      		idsAllGroups.add(option.getValue());
		}
    	List<IPClasses.SelectOptions> options = new List<IPClasses.SelectOptions>();
    	for(Account option : [Select Id, Name from Account where id in :idsAllGroups AND Inactive__c = false order by name]){
      		options.add(new IPClasses.SelectOptions(option.ID, option.Name));
    	}
    	return JSON.serialize(options);
	}
    public static String loadAgencies(String agencyID, String groupID, Boolean allAgenciesAcessible){
		List<IPClasses.SelectOptions> options = new List<IPClasses.SelectOptions>();
		Set<String> idsAllAgencies = new Set<String>();
		
		for(SelectOption option : IPFunctions.getAgencyOptions(agencyID, groupID, allAgenciesAcessible)){
			idsAllAgencies.add(option.getValue());
		}

		for(Account option : [Select Id, Name from Account where id in :idsAllAgencies AND Inactive__c = false order by name]){
			options.add(new IPClasses.SelectOptions(option.ID, option.Name));
		}

		return JSON.serialize(options);
  	}
    public static String loadEmployees(String agencyID){
		List<IPClasses.SelectOptions> options = new List<IPClasses.SelectOptions>();
		options.add(new IPClasses.SelectOptions('all','-All Employees-'));
		if(!String.isEmpty(agencyID) && agencyID != 'all'){
			//for(SelectOption option : IPFunctions.getEmployeesByAgency(agencyID)){
			for(User a : [SELECT ID, Contact.Name FROM User where Contact.AccountId= :agencyID and Contact.RecordType.Name='Employee' and IsActive = true order by Name]){
                options.add(new IPClasses.SelectOptions(a.ID, a.Contact.Name));
            }	
			//}
		}
		return JSON.serialize(options);
	}
    @RemoteAction
	public static Map<String, String> refreshEmployees(List<String> agenciesSelected){
		Map<String, String> response = new Map<String, String>();
		response.put('employeeSelected', 'all');
		if(agenciesSelected != null && agenciesSelected.size() == 1){
			response.put('employees', loadEmployees(agenciesSelected.get(0)));
		}else{
			List<IPClasses.SelectOptions> options = new List<IPClasses.SelectOptions>();
			options.add(new IPClasses.SelectOptions('all','-All Employees-'));
			response.put('employees', JSON.serialize(options));
		}
		return response;
	}

    public class ContactReport{
        public String ID{get;set;}
        public String name{get;set;}
        public String email{get;set;}
        public String fromAgency{get;set;}
        public String toAgency{get;set;}
        public String fromUser{get;set;}
        public String toUser{get;set;}
        public String dateChange{get;set;}
        public String action{get;set;}

        public ContactReport(Contact ctt, IPClasses.OwnershipHistory ownership, String dateFormat){
            this.ID = ctt.ID;  
            this.name = ctt.name; 
            this.email = ctt.email; 
            this.fromAgency = ownership.fromAgency; 
            this.toAgency = ownership.toAgency; 
            this.fromUser = ownership.fromUser; 
            this.toUser = ownership.toUser; 
            this.dateChange = ownership.createdDate.format(dateFormat); 
            this.action = ownership.action; 
        }
    }
}