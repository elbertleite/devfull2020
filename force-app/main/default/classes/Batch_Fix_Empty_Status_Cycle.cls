public class Batch_Fix_Empty_Status_Cycle{
	public Batch_Fix_Empty_Status_Cycle(){}
}
/*global with sharing class Batch_Fix_Empty_Status_Cycle implements Database.Batchable<sObject>, Database.Stateful{
    global String agencyGroup;
    global Batch_Fix_Empty_Status_Cycle() {}
    global Batch_Fix_Empty_Status_Cycle(String agencyGroup) {
        this.agencyGroup = agencyGroup;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT ID, Lead_Last_Stage_Cycle__c, Lead_Last_Status_Cycle__c, Agency_Group__c, (SELECT Stage__c, Stage_Item__c, Agency_Group__c, Last_Saved_Date_Time__c  FROM Client_Stage_Follow_Up__r WHERE Agency_Group__c = :agencyGroup ORDER BY Last_Saved_Date_Time__c DESC LIMIT 1) FROM Destination_Tracking__c WHERE Agency_Group__c = :agencyGroup AND Current_Cycle__c = true ORDER BY CreatedDate DESC';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Destination_Tracking__c> cycles) {
        List<Destination_Tracking__c> wrongCycles = new List<Destination_Tracking__c>();
        Client_Stage_Follow_Up__c toCompare;
        for(Destination_Tracking__c cycle : cycles){
            if(cycle.Client_Stage_Follow_Up__r != null && !cycle.Client_Stage_Follow_Up__r.isEmpty()){
		        toCompare = cycle.Client_Stage_Follow_Up__r.get(0);
                if(toCompare.Agency_Group__c == cycle.Agency_Group__c && (toCompare.Stage__c != cycle.Lead_Last_Stage_Cycle__c || cycle.Lead_Last_Status_Cycle__c != toCompare.Stage_Item__c)){
                    cycle.Lead_Last_Stage_Cycle__c = toCompare.Stage__c;
                    cycle.Lead_Last_Status_Cycle__c = toCompare.Stage_Item__c;
                    cycle.Lead_Last_Change_Status_Cycle__c = toCompare.Last_Saved_Date_Time__c;
                    wrongCycles.add(cycle);
                }
            }
        }
        update wrongCycles;
    }

    global void finish(Database.BatchableContext BC) {}
}*/