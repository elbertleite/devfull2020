public with sharing class provider_payment_invoice {
    
    public Invoice__c invoice {get;set;}
    public list<client_product_service__c> products {get;set;}
    
    public provider_payment_invoice() {
        try{
            invoice = [SELECT Provider__r.Provider_Name__c, Provider_Payment_Number__c, External_Reference__c, Paid_On__c, createdDate FROM Invoice__c WHERE Id = :ApexPages.currentPage().getParameters().get('id') AND isProvider_Payment__c = TRUE limit 1];

            products = [SELECT Products_Services__r.Provider__r.Provider_Name__c, Client__r.name, Received_By_Department__r.name, Price_Total__c, Quantity__c, Received_Date__c, Received_By_Agency__r.name, Received_by__r.name, Product_Name__c, Currency__c, Agency_Currency_Value__c, Agency_Currency__c, Agency_Currency_Rate__c, Claim_Commission_Type__c, Total_Provider_Payment__c, Refund_Confirmed_ADM__c, Agency_Commission_Refund__c, Refund_amount__c, Commission_Type__c, Commission_Value__c, Commission__c, Total_Refund__c, Paid_to_Provider_on__c, Refund_Paid_to_ProviderOn__c, (SELECT Product_Name__c, Price_Total__c, Agency_Currency_Value__c, Refund_Confirmed_ADM__c FROM paid_products__r) FROM client_product_service__c WHERE (Provider_Payment__c = :invoice.id OR Provider_Refund_Payment__c = :invoice.Id) AND Paid_with_product__c = NULL order by Product_Name__c, Client__r.name];
        }
        catch(Exception e){

            invoice = new Invoice__c();
            products = new list<client_product_service__c>();

            system.debug('Error===' + e);
			system.debug('Error Line ===' + e.getLineNumber());
        }
    }
}