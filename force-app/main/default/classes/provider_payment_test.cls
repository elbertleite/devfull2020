@isTest
private class provider_payment_test
{
	public static User portalUser {get{
	if (null == portalUser) {
	portalUser = [Select Id, Name, Contact.AccountId from User where email = 'test12345@test.com' limit 1];
	} return portalUser;} set;}

	@testSetup static void setup() { 
		TestFactory tf = new TestFactory();
		tf.setupProdcutsTest();

		Contact client = [Select Id, Accountid From Contact limit 1];
	
		portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
		system.runAs(portalUser){
			
			//Add product to booking
			ApexPages.currentPage().getParameters().put('id', client.Id);
			products_search ps = new products_search();

			for(String av : ps.result.keySet())
				for(String cat : ps.result.get(av).keyset())
					for(products_search.products p : ps.result.get(av).get(cat)){
						p.product.Selected__c = true;

						for(Quotation_List_Products__c f : p.prodFees)
							f.isSelected__c = true;
					}

			ps.saveProduct();

			//Pay Product
			client_product_service__c cliProduct = [Select Id from client_product_service__c WHERE Related_to_Product__c = null limit 1];

			client_course_product_pay payProd = new client_course_product_pay(new ApexPages.StandardController(cliProduct));

			payProd.newPayDetails.Payment_Type__c = 'Creditcard';
			payProd.newPayDetails.Value__c = payProd.totalPay;
			payProd.addPaymentValue();
			payProd.savePayment();
		}
	}

	static testMethod void confirmPayment(){

		system.runAs(portalUser){

			provider_payment p = new provider_payment();

			string selectedAgencyGroup = p.selectedAgencyGroup;
			string selectedAgency  = p.selectedAgency;
			string selectedProvider  = p.selectedProvider;
			string selectedCategory  = p.selectedCategory;
			string selectedProduct  = p.selectedProduct;
			map<String,String> mapAgCurrency  = p.mapAgCurrency;
			List<SelectOption> agencyGroupOptions = p.agencyGroupOptions;
			List<SelectOption> agencyOptions = p.agencyOptions;
			List<SelectOption> providersOpt = p.providersOpt;
			List<SelectOption> categoriesOpt = p.categoriesOpt;
			List<SelectOption> agencyProducts = p.agencyProducts;
			p.changeGroup();
			p.changeAgency();
			p.selectedProvider = p.providersOpt[1].getValue();
			p.changeProvider();
			p.selectedCategory =  p.categoriesOpt[1].getValue();
			p.changeCategory();


			p.searchProducts();


			for(string st: p.listProducts.keySet())
				for(client_product_service__c prod : p.listProducts.get(st))
					prod.isSelected__c = true;

			p.createInvoice();
			
			p.updateCommission();
			Decimal totalInvoice = p.totalInvoice;
			String prodCurrency = p.prodCurrency;

			ApexPages.currentPage().getParameters().put('inv', p.payInvoice.id);
			p.confirmInvoice();

			p.deleteInvoice();
		}

	}

	static testMethod void payRefund(){

		system.runAs(portalUser){

			provider_payment p = new provider_payment();

			client_product_service__c cliProduct = [Select Id from client_product_service__c WHERE Related_to_Product__c = null limit 1];
			cliProduct.Refund_Confirmed_ADM__c = true;
			update cliProduct;

			List<SelectOption> agencyGroupOptions = p.agencyGroupOptions;
			List<SelectOption> agencyOptions = p.agencyOptions;
			List<SelectOption> providersOpt = p.providersOpt;
			List<SelectOption> categoriesOpt = p.categoriesOpt;
			List<SelectOption> agencyProducts = p.agencyProducts;
			p.changeGroup();
			p.changeAgency();
			p.selectedProvider = p.providersOpt[1].getValue();
			p.changeProvider();
			p.selectedCategory =  p.categoriesOpt[1].getValue();
			p.changeCategory();

			p.searchProducts();

			for(string st: p.listProducts.keySet()) 
				for(client_product_service__c prod : p.listProducts.get(st))
					prod.isSelected__c = true;

			p.createInvoice();
			Decimal totalInvoice = p.totalInvoice;
			String prodCurrency = p.prodCurrency;

			p.confirmPay();
			
			ApexPages.currentPage().getParameters().put('inv', p.payInvoice.id);
			p.loadEmail();
			p.emTo = 'test@test.com';
			p.emSubj = 'Hi';
			//p.changeTemplate();
			p.sendEmail();
		}

	}
}