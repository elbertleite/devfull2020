public with sharing class cash_bank_deposits {

	public list<depositDetails> result {get;set;}

	private map<id, Date> dateMap {get;set;}
	
	public cash_bank_deposits() {
		findGroups();
		findDeposits();
	}


	public void findDeposits(){

		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && currentUser.Aditional_Agency_Managment__c == null)
			selectedAgency = currentUser.Contact.AccountId;

		String sql = 'SELECT Amount__c, Bank__r.Bank__c, Bank__r.Account_Name__c, Bank__r.Account_Number__c, Type__c, Deposit_Date__c, Details__c, CreatedBy.Name ';
		
		sql += ' , (SELECT Client__r.Name, client_course_instalment__r.Name, client_product_service__r.Name, Invoice__r.Name, Value__c FROM client_course_instalment_payments__r order by Client__r.Name) ';

		sql += ' FROM Bank_Deposit__c WHERE Deposited_By_Agency__c = \''+ selectedAgency +'\'';
		
		if(selectedPeriod != null && selectedPeriod != 'range')
			sql += ' AND Deposit_Date__c = ' + selectedPeriod;
		else{
			sql+= ' AND Deposit_Date__c >= '+ IPFunctions.FormatSqlDateIni(dates.Commission_Due_Date__c);
			sql+= ' AND Deposit_Date__c <= '+ IPFunctions.FormatSqlDateFin(dates.Commission_Paid_Date__c);
		}

		sql += ' order by Deposit_Date__c ';

		system.debug('sql==>' + sql);

		result = new list<depositDetails>();
		depositDetails dt; 
		for(Bank_Deposit__c bk : Database.query(sql))
			result.add(new depositDetails(bk));
	}



	public String bkId {get;set;}

	public void saveChange(){

		system.debug('bkId==>' + bkId);

		Bank_Deposit__c toUpdate;
		for(depositDetails dt : result){
			if(dt.deposit.id == bkId){

				if(dt.deposit.Deposit_Date__c == null){
					dt.errorMsg = 'Please select a date.';
					dt.showSaved = false;
				}
				else if(dt.deposit.Deposit_Date__c > system.today()){
					dt.errorMsg = 'Date cannot be a future date.';
					dt.showSaved = false;
				}
				else
					toUpdate = dt.deposit;
				
				break;
			}
		}//end for

		if(toUpdate!=null){
			update toUpdate;

			for(depositDetails dt : result){
				if(dt.deposit.id == toUpdate.id){
					dt.errorMsg = null;
					dt.showSaved = true;
					dt.oldDate = toUpdate.Deposit_Date__c;
				}
			}//end for
		}
	}

	public void cancelChange(){

		system.debug('bkId==>' + bkId);

		for(depositDetails dt : result)
			if(dt.deposit.id == bkId){
				dt.deposit.Deposit_Date__c = dt.oldDate;
				dt.showSaved = false;
				break;
			}
	}


	/****************** FILTERS ***********************/

	private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}
	private User currentUser {get{if(currentUser==null) currentUser = ff.currentUser; return currentUser;}set;}


	public class depositDetails{
		public Bank_Deposit__c deposit {get;set;}
		public String errorMsg {get;set;}
		public Boolean showSaved {get;set;}
		private Date oldDate {get;set;}
		public depositDetails(Bank_Deposit__c deposit){
			this.deposit = deposit;
			this.oldDate = deposit.Deposit_Date__c;
		}
	}

	public string selectedAgencyGroup{
    	get{
    		if(selectedAgencyGroup == null)
    			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
    		return selectedAgencyGroup;
    	}  
    	set;
    }

	public list<SelectOption> agencyGroupOptions {get;set;}
	private void findGroups(){

		agencyGroupOptions = new List<SelectOption>();  
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group  
			agencyGroupOptions.add(new SelectOption(currentUser.Contact.Account.ParentId, currentUser.Contact.Account.Parent.Name));
		}
		else{
			for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name])
				agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
		}

		findAgencies();
	}


	public void changeGroup(){
		findAgencies();
	}

	  
	public string selectedAgency {get;set;}

	public list<SelectOption> agencyOptions {get;set;}
	private void findAgencies(){
		if(selectedAgencyGroup != null){
			agencyOptions = new List<SelectOption>();  
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency  
				agencyOptions.add(new SelectOption(currentUser.Contact.AccountId, currentUser.Contact.Account.Name));
				selectedAgency = currentUser.Contact.AccountId;
				if(currentUser.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
						agencyOptions.add(new SelectOption(ac.id, ac.name));
					}
				}
			}
			else{
				for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency' AND ParentId = :selectedAgencyGroup order by Name])
					agencyOptions.add(new SelectOption(ag.id, ag.Name));

				if(agencyOptions != null && agencyOptions.size()>0){
					if(selectedAgencyGroup == currentUser.Contact.Account.ParentId)
						selectedAgency = currentUser.Contact.AccountId;
					else
						selectedAgency = agencyOptions[0].getValue();
				}
			}
		}
	}

	
	public client_course_instalment__c dates{get{
      if(dates==null){

        dates = new client_course_instalment__c();

        dates.Commission_Due_Date__c = system.today();
		dates.Commission_Paid_Date__c = dates.Commission_Due_Date__c.addDays(14);
      }
      return dates;
    }set;}


	public string SelectedPeriod{get {if(SelectedPeriod == null) SelectedPeriod = 'THIS_MONTH'; return SelectedPeriod; } set;}
    private List<SelectOption> periods;
    public List<SelectOption> getPeriods() {
        if(periods == null){
            periods = new List<SelectOption>();
            periods.add(new SelectOption('THIS_WEEK','This Week'));
            periods.add(new SelectOption('THIS_MONTH','This Month'));
            periods.add(new SelectOption('LAST_WEEK','Last Week'));
            periods.add(new SelectOption('LAST_MONTH','Last Month'));
            periods.add(new SelectOption('range','Range'));
        }
        return periods;
    }



}