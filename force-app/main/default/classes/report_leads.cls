public with sharing class report_leads {
	
	public String REPORT_TYPE { get;set; }
	public String REPORT_LEADSOURCE {get{return 'leadSource';}set;}
	public String REPORT_FIRSTCONTACT {get{return 'firstContact';}set;}
	
	public report_leads(){
		
		selectedAgencyGroup = myDetails.Account.Parentid;		
		selectedAgency = mydetails.Accountid;
		
        if(ApexPages.currentPage().getParameters().get('type') == 'source')
			REPORT_TYPE = REPORT_LEADSOURCE;
       	else REPORT_TYPE = REPORT_FIRSTCONTACT;
		
		search();
	}
	
	public PageReference checkUserAccess(){

     	if(!myDetails.Marketing_Report_Permission__c){
      		PageReference retURL = new PageReference('/apex/agency_reports');
      		retURL.setRedirect(true);
      		return retURL;
      	} else return null;      	
      	
 	}
    
    public boolean showEmpty { get{ if(showEmpty == null) showEmpty = true; return showEmpty; } set; }
	
	public Contact myDetails {
		get{
			if(myDetails == null)
				myDetails = UserDetails.getMyContactDetails();
			return myDetails;
		}
		set;
	}
	
	public String selectedAgencyGroup {get;set;}
	private List<SelectOption> agencyGroupOptions;
	public List<SelectOption> getAgencyGroupOptions() {
		if(agencyGroupOptions == null){
			agencyGroupOptions = new List<SelectOption>();
			if(myDetails.Account.Parent.Destination_Group__c || myDetails.Global_Manager__c){
				agencyGroupOptions.add(new SelectOption('all', '-- ALL --'));
				for(Account ag : IPFunctionsGLobal.getAgencyGroups(myDetails.Account.Parent.Global_Link__c))
					agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
			} else {
				for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ])
					agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
			}
		}
		
		return agencyGroupOptions;
	}
	
	public void refreshAgencies(){
		selectedAgency = null;		
		agencyOptions = null;		
		getAgencyOptions();		
		//leadSourceOptions = null;
		//getLeadSourceOptions();
	}

	public void refreshDestinations(){
		selectedDestination = null;		
		destinationOptions = null;		
		getdestinationOptions();		
	}
	
	public String selectedAgency {get;set;}
	private List<SelectOption> agencyOptions;
	public List<SelectOption> getAgencyOptions(){
		if(agencyOptions == null){
			agencyOptions = new List<SelectOption>();
			agencyOptions.add(new SelectOption('all', '-- ALL --'));
			if(selectedAgencyGroup != null)
				for(Account agency : [select id, name from Account where RecordType.Name = 'Agency' and parentid = :selectedAgencyGroup order by Name])
					agencyOptions.add(new SelectOption(agency.id, agency.Name));
			
		}
		return agencyOptions;
	}

	public String selectedDestination {get;set;}
	private List<SelectOption> destinationOptions;
	public List<SelectOption> getdestinationOptions(){
		if(destinationOptions == null){
			destinationOptions = new List<SelectOption>();
			destinationOptions.add(new SelectOption('all', '-- ALL --'));
			if(selectedAgencyGroup != null){
				string sql = 'Select Destination_Country__c dest from contact where recordType.name in (\'client\',\'lead\') and Destination_Country__c != null ';
				if(selectedAgency != 'all')
					sql += ' and Original_Agency__c = :selectedAgency ';
				else if(selectedAgencyGroup != 'all')
					sql += ' and Original_Agency__r.ParentID = :selectedAgencyGroup ';
				sql += ' group by Destination_Country__c';
				for(AggregateResult destination :Database.query(sql))
					destinationOptions.add(new SelectOption(String.valueOf(destination.get('dest')),String.valueOf(destination.get('dest'))));
			}
		}
		return destinationOptions;
	}
	
	/*public void refreshLeadSources(){
		
		leadSourceOptions = null;
		getLeadSourceOptions();
		
	}
	
	public List<String> selectedLeadSource {get;set;}
	private List<SelectOption> leadSourceOptions;
	public List<SelectOption> getLeadSourceOptions(){
		if(leadSourceOptions == null){
			leadSourceOptions = new List<SelectOption>();			
			if(selectedAgencyGroup != null){
				if(selectedAgency  == 'all')
					for(AggregateResult ar : [select LeadSource from Contact where LeadSource != null and RecordType.Name in ('Client', 'Lead') and Current_Agency__r.ParentId = :selectedAgencyGroup group by LeadSource order by LeadSource])
						leadSourceOptions.add(new SelectOption(String.valueOf(ar.get('LeadSource')), String.valueOf(ar.get('LeadSource'))));
				else
					for(AggregateResult ar : [select LeadSource from Contact where LeadSource != null and RecordType.Name in ('Client', 'Lead') and Current_Agency__c = :selectedAgency group by LeadSource order by LeadSource])
						leadSourceOptions.add(new SelectOption(String.valueOf(ar.get('LeadSource')), String.valueOf(ar.get('LeadSource'))));
			}
		}
		return leadSourceOptions;
	}*/
	
	public Contact range {
		get {
			if(range == null){
				range = new Contact();
				range.Arrival_Date__c = system.today().addMonths(-5); 
				range.Expected_Travel_Date__c = system.today();
			}
			return range;
		}
		set;
	}
	
	private void genLeadMap(String leadSource){		
		Date startDate = range.Arrival_Date__c;		
		Integer monthsBetween = range.Arrival_Date__c.monthsBetween(range.Expected_Travel_Date__c);
		
		for(Integer i = 1; i <= monthsBetween+1; i++){
			
			Integer year = startDate.year();
			Integer month = startDate.month();
			
			
			if( leadSourceMap.containsKey( leadSource ) ){				
				if( leadSourceMap.get( leadSource ).containsKey( year ) ){
					if( !leadSourceMap.get( leadSource ).get( year ).containsKey(month) )	
						leadSourceMap.get( leadSource ).get( year ).put( month, new Total(0, 0) );					
				} else 					
					leadSourceMap.get( leadSource ).put( year, new Map<Integer, Total>{ month => new Total(0, 0) } );
			} else
				leadSourceMap.put( leadSource, new Map<Integer, Map<Integer, Total>>{ year => new Map<Integer, Total>{ month => new Total(0, 0) }} );
			
			
			
			startDate = startDate.addMonths(1);
			
		}
		
	}
	
	private void genMonthsMap(){
		Date startDate = range.Arrival_Date__c;		
		Integer monthsBetween = range.Arrival_Date__c.monthsBetween(range.Expected_Travel_Date__c);
		
		for(Integer i = 1; i <= monthsBetween+1; i++){
			
			Integer year = startDate.year();
			Integer month = startDate.month();
			
			if(monthsByYear.containsKey(year))
				monthsByYear.get(year).add(month);
			else monthsByYear.put(year, new List<Integer>{ month });
			
			startDate = startDate.addMonths(1);
			
		}
		
	}
	
	private String getChartSourceKey(String source, String specific){
		if(specific != null)
			return '('+ source +') ' + specific;
		else if(source != null)
			return source;
		else return '(Empty)';
	}
	
	private String getSourceKey(String source, String specific){
		return source + '##' + '('+ source +') ' + specific;
	}
	
	//        <LeadSource, <Year, <Month, Total>>>
	public Map<String, Map<Integer, Map<Integer, Total>>> leadSourceMap {get;set;}	
	public Map<Integer, List<Integer>> monthsByYear {get;set;}
	
	public void search(){
		leadSourceMap = new Map<String, Map<Integer, Map<Integer, Total>>>();
		monthsByYear = new Map<Integer, List<Integer>>();
		chartMap = new Map<String, SourceWrapper>();
		
		genMonthsMap();
		
		if(REPORT_TYPE == REPORT_LEADSOURCE)
			searchLeadSource();
		else
			searchFirstContact();
		
		
		
		Integer totalSources = chartList.size();
		
		totalSources = totalSources * 25 < 250 ? 250 : totalSources * 25;
		chartHeight = String.valueOf(totalSources) + 'px';
		
	}
	
    private void searchFirstContact(){
        
        String query = 'select Lead_First_Contact__c, CALENDAR_YEAR(CreatedDate) year, CALENDAR_MONTH(CreatedDate) month, COUNT(id) total from Contact ';
		String queryWhere = ' where RecordType.Name in (\'Lead\', \'Client\') ';
		
		if(range.Arrival_Date__c != null)
			queryWhere += ' and DAY_ONLY(CreatedDate) >= ' + IPFunctions.FormatSqlDateIni(range.Arrival_Date__c);
		if(range.Expected_Travel_Date__c != null)		
			queryWhere += ' and DAY_ONLY(CreatedDate) <= ' + IPFunctions.FormatSqlDateFin(range.Expected_Travel_Date__c);		
		if(selectedAgency != 'all')
			queryWhere += ' and Original_Agency__c = :selectedAgency ';
		else if(selectedAgencyGroup != 'all')
			queryWhere += ' and Original_Agency__r.ParentID = :selectedAgencyGroup ';

		if(selectedDestination != 'all')
			queryWhere += ' and Destination_Country__c = :selectedDestination ';
        
        if(!showEmpty)
            queryWhere += ' and Lead_First_Contact__c != null';
					
		String queryGroup = ' group by CALENDAR_YEAR(CreatedDate), CALENDAR_MONTH(CreatedDate), Lead_First_Contact__c ';
		String queryOrder = ' order by Lead_First_Contact__c, CALENDAR_YEAR(CreatedDate), CALENDAR_MONTH(CreatedDate) ';
		
		String theQuery = query + queryWhere + queryGroup + queryOrder;
		
		system.debug('theQuery 1: ' + theQuery);
		
		for(AggregateResult ar : Database.query(theQuery)){
			
			String leadFirstContact = (String) ar.get('Lead_First_Contact__c');
			Integer year = Integer.valueOf(ar.get('year'));
			Integer month = Integer.valueOf(ar.get('month'));
			Integer total = Integer.valueOf(ar.get('total'));
			
			String theKey = leadFirstContact;
			
			if(theKey == null)
				theKey = 'z#(Empty)';	
				
			genLeadMap(theKey);
			
			try {
				leadSourceMap.get( theKey ).get( year ).get( month ).totalLeads = total;
				chartMap.put( getChartSourceKey(leadFirstContact, null) , new SourceWrapper( getChartSourceKey(leadFirstContact, null) , 0));
				
			} catch (Exception e) {
				ApexPages.addMessages(e);
				system.debug('Exception: ' + e.getMessage() + ' ' + e.getLineNumber() + ' ' + e.getStackTraceString());
				system.debug('leadFirstContact: ' + leadFirstContact);
				system.debug('year: ' + year);
				system.debug('month: ' + month);
				system.debug('total: ' + total);
			}			
		}
		
		
		
		//Get converted leads
		queryWhere += ' and Lead_Converted_on__c != null ';
		
		theQuery = query + queryWhere + queryGroup + queryOrder;
		system.debug('theQuery 2: ' + theQuery);
		
		for(AggregateResult ar : Database.query(theQuery)){
			
			String leadFirstContact = (String) ar.get('Lead_First_Contact__c');			
			Integer year = Integer.valueOf(ar.get('year'));
			Integer month = Integer.valueOf(ar.get('month'));
			Integer total = Integer.valueOf(ar.get('total'));
			
			String theKey = leadFirstContact;
			
			if(theKey == null)
				theKey = 'z#(Empty)';
			
			try {
				leadSourceMap.get( theKey ).get( year ).get( month ).convertedLeads = total;
				chartMap.get( getChartSourceKey(leadFirstContact, null) ).totalConverted += total;
					
								
			} catch (Exception e) {
				ApexPages.addMessages(e);
				system.debug('Exception: ' + e.getMessage() + ' ' + e.getLineNumber() + ' ' + e.getStackTraceString());
				system.debug('leadFirstContact: ' + leadFirstContact);
				system.debug('year: ' + year);
				system.debug('month: ' + month);
				system.debug('total: ' + total);
			}			
		}
		
		try {
			
			chartList = new List<SourceWrapper>();
			SourceWrapper empty;
			for(Integer i = chartMap.values().size()-1; i >= 0; i--){
				if(chartMap.values().get(i).leadSource.containsIgnoreCase('Empty'))
					empty = chartMap.values().get(i);
				else
					chartList.add(chartMap.values().get(i));
			}
			if(empty != null)
				chartList.add(0, empty);
		} catch (Exception e) {
			ApexPages.addMessages(e);
			system.debug('Exception: ' + e.getMessage() + ' ' + e.getLineNumber() + ' ' + e.getStackTraceString());				
		}
        
        
        
	}
	
	private void searchLeadSource(){
		
		String query = 'select LeadSource, Lead_Source_Specific__c, Lead_Reference_Type__c, CALENDAR_YEAR(CreatedDate) year, CALENDAR_MONTH(CreatedDate) month, COUNT(id) total from Contact ';
		String queryWhere = ' where RecordType.Name in (\'Lead\', \'Client\') ';
		
		if(range.Arrival_Date__c != null)
			queryWhere += ' and DAY_ONLY(CreatedDate) >= ' + IPFunctions.FormatSqlDateIni(range.Arrival_Date__c);
		if(range.Expected_Travel_Date__c != null)		
			queryWhere += ' and DAY_ONLY(CreatedDate) <= ' + IPFunctions.FormatSqlDateFin(range.Expected_Travel_Date__c);		
		if(range.Visa_Expiry_Date__c != null){
			queryWhere += ' and Visa_Expiry_Date__c >= ' + IPFunctions.FormatSqlDateIni(range.Visa_Expiry_Date__c);		
		}
		if(range.Estimated_Date_to_Close__c != null){
			queryWhere += ' and Visa_Expiry_Date__c <= ' + IPFunctions.FormatSqlDateFin(range.Estimated_Date_to_Close__c);		
		}
		if(selectedAgency != 'all')
			queryWhere += ' and Original_Agency__c = :selectedAgency ';
		else if(selectedAgencyGroup != 'all')
			queryWhere += ' and Original_Agency__r.ParentID = :selectedAgencyGroup ';

		if(selectedDestination != 'all')
			queryWhere += ' and Destination_Country__c = :selectedDestination ';

        if(!showEmpty)
            queryWhere += ' and LeadSource != null';
					
		String queryGroup = ' group by CALENDAR_YEAR(CreatedDate), CALENDAR_MONTH(CreatedDate), LeadSource, Lead_Source_Specific__c, Lead_Reference_Type__c ';
		String queryOrder = ' order by LeadSource, CALENDAR_YEAR(CreatedDate), CALENDAR_MONTH(CreatedDate) ';
		
		String theQuery = query + queryWhere + queryGroup + queryOrder;
		
		system.debug('theQuery 1: ' + theQuery);
		
		for(AggregateResult ar : Database.query(theQuery)){
			
			String leadSource = (String) ar.get('LeadSource');
			String leadSourceSpecific = (String) ar.get('Lead_Source_Specific__c');
			String leadReferenceType = (String) ar.get('Lead_Reference_Type__c');
			Integer year = Integer.valueOf(ar.get('year'));
			Integer month = Integer.valueOf(ar.get('month'));
			Integer total = Integer.valueOf(ar.get('total'));
			
			String theKey;
			if(!String.isEmpty(leadSourceSpecific))
				theKey = getSourceKey(leadSource, leadSourceSpecific);
			else if(!String.isEmpty(leadReferenceType)) 
				theKey = getSourceKey(leadSource, leadReferenceType);
			else 
				theKey = leadSource;
			
			if(theKey == null)
				theKey = 'z#(Empty)';	
				
			genLeadMap(theKey);
			
			try {
				leadSourceMap.get( theKey ).get( year ).get( month ).totalLeads = total;
				chartMap.put( getChartSourceKey(leadSource, leadSourceSpecific) , new SourceWrapper( getChartSourceKey(leadSource, leadSourceSpecific) , 0));
				
			} catch (Exception e) {
				ApexPages.addMessages(e);
				system.debug('Exception: ' + e.getMessage() + ' ' + e.getLineNumber() + ' ' + e.getStackTraceString());
				system.debug('leadSource: ' + leadSource);
				system.debug('leadSourceSpecific: ' + leadSourceSpecific);
				system.debug('year: ' + year);
				system.debug('month: ' + month);
				system.debug('total: ' + total);
			}			
		}
		
		
		
		//Get converted leads
		queryWhere += ' and Lead_Converted_on__c != null ';
		
		theQuery = query + queryWhere + queryGroup + queryOrder;
		system.debug('theQuery 2: ' + theQuery);
		
		for(AggregateResult ar : Database.query(theQuery)){
			
			String leadSource = (String) ar.get('LeadSource');
			String leadSourceSpecific = (String) ar.get('Lead_Source_Specific__c');
			Integer year = Integer.valueOf(ar.get('year'));
			Integer month = Integer.valueOf(ar.get('month'));
			Integer total = Integer.valueOf(ar.get('total'));
			
			String theKey;
			if(leadSourceSpecific != null)
				theKey = getSourceKey(leadSource, leadSourceSpecific);
			else 
				theKey = leadSource;
			
			if(theKey == null)
				theKey = 'z#(Empty)';
			
			try {
				leadSourceMap.get( theKey ).get( year ).get( month ).convertedLeads = total;
				chartMap.get( getChartSourceKey(leadSource, leadSourceSpecific) ).totalConverted += total;
					
								
			} catch (Exception e) {
				ApexPages.addMessages(e);
				system.debug('Exception: ' + e.getMessage() + ' ' + e.getLineNumber() + ' ' + e.getStackTraceString());
				system.debug('leadSource: ' + leadSource);
				system.debug('leadSourceSpecific: ' + leadSourceSpecific);
				system.debug('year: ' + year);
				system.debug('month: ' + month);
				system.debug('total: ' + total);
			}			
		}
		
		try {
			
			chartList = new List<SourceWrapper>();
			SourceWrapper empty;
			for(Integer i = chartMap.values().size()-1; i >= 0; i--){
				if(chartMap.values().get(i).leadSource.containsIgnoreCase('Empty'))
					empty = chartMap.values().get(i);
				else
					chartList.add(chartMap.values().get(i));
			}
			if(empty != null)
				chartList.add(0, empty);
		} catch (Exception e) {
			ApexPages.addMessages(e);
			system.debug('Exception: ' + e.getMessage() + ' ' + e.getLineNumber() + ' ' + e.getStackTraceString());				
		}	
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public PageReference generateExcel(){
		PageReference pr = Page.report_leads_excel;
		pr.setRedirect(false);
		return pr;		
	}
	
	
	
	public List<Contact> exportList {get;set;}
	public void excelSearch(){
		
		String sql = 'Select Original_Agency__r.Name, Name, Web_Form_URL__c , RecordType.Name, CreatedDate, CreatedBy.Name, Lead_Stage__c, Status__c, Nationality__c, Email, Client_Classification__c, Lead_Level_of_Interest__c, ';
		sql += ' LeadSource, Lead_Source_Specific__c, Lead_Reference_Type__c, Lead_Reference_Name__c, Lead_First_Contact__c, Lead_First_Contact_Date__c, Lead_Product_Type__c, Lead_Study_Type__c, Lead_Other_Product__c, Lead_Assignment_To__r.Name, ';
		sql += ' Lead_Destinations__c, 	Lead_Other_Destination__c, Lead_Converted_by__r.Name, Lead_Converted_on__c, Conversion_Time_in_Days__c, Destination_Country__c, Destination_City__c, Expected_Travel_Date__c, Arrival_Date__c, Visa_Expiry_Date__c, ';
		sql += ' ( select detail__c from forms_of_contact__r where type__c = \'Mobile\' ) ' ;
		sql += ' from Contact ';
		
		String queryWhere = ' where RecordType.Name in (\'Lead\', \'Client\') ';
		
		if(range.Arrival_Date__c != null)
			queryWhere += ' and DAY_ONLY(CreatedDate) >= ' + IPFunctions.FormatSqlDateIni(range.Arrival_Date__c);
		if(range.Expected_Travel_Date__c != null)		
			queryWhere += ' and DAY_ONLY(CreatedDate) <= ' + IPFunctions.FormatSqlDateIni(range.Expected_Travel_Date__c);	
		if(range.Visa_Expiry_Date__c != null){
			queryWhere += ' and Visa_Expiry_Date__c >= ' + IPFunctions.FormatSqlDateIni(range.Visa_Expiry_Date__c);		
		}
		if(range.Estimated_Date_to_Close__c != null){
			queryWhere += ' and Visa_Expiry_Date__c <= ' + IPFunctions.FormatSqlDateFin(range.Estimated_Date_to_Close__c);		
		}	
		if(selectedAgency != 'all')
			queryWhere += ' and Original_Agency__c = :selectedAgency ';
		else if(selectedAgencyGroup != 'all')
			queryWhere += ' and Original_Agency__r.ParentID = :selectedAgencyGroup ';
		
		if(selectedDestination != 'all')
			queryWhere += ' and Destination_Country__c = :selectedDestination ';
			
		String sqlOrder = ' order by Original_Agency__r.Name, Name';
		
		exportList = Database.query(sql + queryWhere + sqlOrder);
		
	}
	
	public String chartHeight { get; set; }
	
	public List<SourceWrapper> chartList {get;set;}
	private Map<String, SourceWrapper> chartMap;
	
	public class SourceWrapper {
		public SourceWrapper(String leadSource, Integer totalConverted){
			this.leadSource = leadSource;
			this.totalConverted = totalConverted;			
		}
		public String leadSource {get;set;}
		public Integer totalConverted {get;set;}
	}
	
	public class Total {
		public Total(Integer totalLeads, Integer convertedLeads){
			this.totalLeads = totalLeads;
			this.convertedLeads = convertedLeads;
		}		
		public Integer totalLeads {get;set;}
		public Integer convertedLeads {get;set;}
	}
	
	
	private void logException(Exception e){
			
	}
	
	public Map<Integer, String> monthNames {
		get{
			if(monthNames == null){
				monthNames = new Map<Integer, String>();
				monthNames.put(1, 'January');
				monthNames.put(2, 'February');
				monthNames.put(3, 'March');
				monthNames.put(4, 'April');
				monthNames.put(5, 'May');
				monthNames.put(6, 'June');
				monthNames.put(7, 'July');
				monthNames.put(8, 'August');
				monthNames.put(9, 'September');
				monthNames.put(10, 'October');
				monthNames.put(11, 'November');
				monthNames.put(12, 'December');				
			}
			return monthNames;			
		}
		set;		
	}
	
	
}