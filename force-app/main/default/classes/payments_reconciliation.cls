public with sharing class payments_reconciliation {

	public final String CONFIRMED_INSTALMENTS = 'confirmed';
	public final String OPEN_INSTALMENTS = 'open';
	public String checkedPayments {get;set;}
	public String selectedAgency {get;set;}
	public String selectedSchool {get{if(selectedSchool==null) selectedSchool = 'all'; return selectedSchool;}set;}
	public String selectedPaymentStatus {
		get{
			if(selectedPaymentStatus == null)
			selectedPaymentStatus = OPEN_INSTALMENTS;
			return selectedPaymentStatus;
		}
		set;
	}
	public decimal totalDiscount {get;set;}
	public Map<String, Decimal> mapTotals {get;set;}
	public Contact filterDate {
		get{
			if(filterDate == null){
				filterDate = new Contact();
				//filterDate.BirthDate = system.today();
			}
			return filterDate;
		}
		set;
	}
	public List<SelectOption> paymentStatus {
		get{
			if(paymentStatus == null){
				paymentStatus = new List<SelectOption>();
				paymentStatus.add(new SelectOption(OPEN_INSTALMENTS,'Open'));
				paymentStatus.add(new SelectOption(CONFIRMED_INSTALMENTS,'Confirmed'));

			}
			return paymentStatus;
		}
		set;
	}
	public list<selectOption> depositListType{get; set;}
	public String selectedPaymentTypes {get{if(selectedPaymentTypes == null) selectedPaymentTypes = 'all'; return selectedPaymentTypes;}set;}
	public List<SelectOption> paymentTypes {
		get{
			if(paymentTypes == null){
				//getdepositListType();
				paymentTypes = new List<SelectOption>();
				depositListType = new List<SelectOption>();
				paymentTypes.add(new SelectOption('all','All'));
				for(AggregateResult ar : [SELECT Payment_Type__c FROM client_course_instalment_payment__c where Payment_Type__c!= null group by Payment_Type__c]){
					if(String.valueOf(ar.get('Payment_Type__c')).toLowerCase()!='pds' && String.valueOf(ar.get('Payment_Type__c')).toLowerCase()!='pcs')
					paymentTypes.add(new SelectOption((String) ar.get('Payment_Type__c'), (String) ar.get('Payment_Type__c')));

					if(String.valueOf(ar.get('Payment_Type__c')).toLowerCase()!='deposit' && String.valueOf(ar.get('Payment_Type__c')).toLowerCase()!='school credit' && String.valueOf(ar.get('Payment_Type__c')).toLowerCase()!='offshore' && String.valueOf(ar.get('Payment_Type__c')).toLowerCase()!='transferred from deposit'
					&& String.valueOf(ar.get('Payment_Type__c')).toLowerCase()!='pds' && String.valueOf(ar.get('Payment_Type__c')).toLowerCase()!='pcs' && String.valueOf(ar.get('Payment_Type__c')).toLowerCase()!='covered by agency' && String.valueOf(ar.get('Payment_Type__c')).toLowerCase()!='client scholarship')
					depositListType.add(new SelectOption((String) ar.get('Payment_Type__c'), (String) ar.get('Payment_Type__c')));

					if(String.valueOf(ar.get('Payment_Type__c')).toLowerCase() == 'deposit' || String.valueOf(ar.get('Payment_Type__c')).toLowerCase() == 'school credit' || String.valueOf(ar.get('Payment_Type__c')).toLowerCase() == 'offshore' || String.valueOf(ar.get('Payment_Type__c')).toLowerCase()=='transferred from deposit'
					|| String.valueOf(ar.get('Payment_Type__c')).toLowerCase() == 'covered by agency' || String.valueOf(ar.get('Payment_Type__c')).toLowerCase()=='client scholarship')
					depositListType.add(new SelectOption((String) ar.get('Payment_Type__c'), (String) ar.get('Payment_Type__c'), true));
				}
				//paymentTypes.addAll(depositListType);

			}
			return paymentTypes;
		}
		set;
	}
	public map<string, decimal> paymentTypeTotal{get{if(paymentTypeTotal == null) paymentTypeTotal = new map<string, decimal>(); return paymentTypeTotal;} set;}
	public transient list<consolidation> result {get;set;}
	public client_course_instalment_payment__c editPayment {get;set;}
	public boolean showError {get{if(showError == null) showError = false; return showError;}set;}

	// private map<string, client_course_instalment_payment__c> originalValues;
	private IPFunctions.SearchNoSharing searchNS;
	private User currentUser {get;set;}
	private set<string> unchangeableTypes = new set<string>{'covered by agency','school credit','pcs','pds','deposit','offshore','transferred from deposit','client scholarship'};


	// C O N S T R U C T O R
	public payments_reconciliation(){
		currentUser = [Select Id, Name, email, Aditional_Agency_Managment__c, Contact.Account.ParentId, Contact.AccountId, Contact.Department__c from User where id = :UserInfo.getUserId() limit 1];
		selectedAgency = currentUser.Contact.AccountId;

		//selectedPayments = new set<id>();

		searchNS = new IPFunctions.SearchNoSharing();

		searchPayments();
	}


	private class paymentOrder{
		dateTime dt;
		string cry;
		private paymentOrder(dateTime dt, string cry){
			this.dt = dt;
			this.cry = cry;
		}
	}
	// F I N D 		P A Y M E N T S
	public void searchPayments(){

		String  fromDate = IPFunctions.FormatSqlDateIni(dateFilter.Original_Due_Date__c);
		String  toDate = IPFunctions.FormatSqlDateFin(dateFilter.Discounted_On__c);


		list<Invoice__c> lInvoice = findInvoices(fromDate, toDate);
		list<client_product_service__c> lProducts = findProducts(false, fromDate, toDate);
		List<client_course_instalment__c> lInstalments  = findInstalments(fromDate, toDate);

		List<paymentOrder> ldt = new List<paymentOrder>();
		result = new list<consolidation>();
		consolidation ch;

		// originalValues = new map<string, client_course_instalment_payment__c>();

		paymentTypeTotal = new map<string, decimal>();
		/** Invoice **/
		if(lInvoice!= null && lInvoice.size()>0){
			Integer pos;
			for(Invoice__c i : lInvoice){

				if(ldt.size() == 0 )
					pos = 0;
				else pos = addObjectListPosition(i.Received_Date__c,ldt, i.CurrencyIsoCode__c);
				ch = new consolidation();
				ch.invoice = i;

				for(client_course_instalment_payment__c cp :ch.invoice.client_course_instalment_payments__r){
					if(selectedPaymentTypes == cp.Payment_Type__c || selectedPaymentTypes == 'all' || showAll){
						if(!paymentTypeTotal.containsKey(cp.Payment_Type__c))
							paymentTypeTotal.put(cp.Payment_Type__c, cp.Value__c);
						else paymentTypeTotal.put(cp.Payment_Type__c, paymentTypeTotal.get(cp.Payment_Type__c) + cp.Value__c);
					}
					// originalValues.put(cp.id, cp.clone(true));
				}

				if(result.size() == pos){
					result.add(ch);
					ldt.add(new paymentOrder(i.Received_Date__c, i.CurrencyIsoCode__c));
				}else{
					result.add(pos, ch);
					ldt.add(pos, new paymentOrder(i.Received_Date__c, i.CurrencyIsoCode__c));
				}
			}//end for
		}

		/** Instalment **/
		if(lInstalments!=null && lInstalments.size()>0){
			Integer pos;
			for(client_course_instalment__c i : lInstalments){

				if(!i.isInstalment_Amendment__c && (i.isCommission_Amendment__c || i.isOverpayment_Amendment__c)){}
				else{
					if(ldt.size() == 0 )
						pos = 0;
					else pos = addObjectListPosition(i.Received_Date__c,ldt, i.Client_Course__r.CurrencyIsoCode__c);
					ch = new consolidation();
					ch.instalment = i;

					for(client_course_instalment_payment__c cp :ch.instalment.client_course_instalment_payments__r){
						if(selectedPaymentTypes == cp.Payment_Type__c || selectedPaymentTypes == 'all' || showAll){
							if(!paymentTypeTotal.containsKey(cp.Payment_Type__c))
								paymentTypeTotal.put(cp.Payment_Type__c, cp.Value__c);
							else paymentTypeTotal.put(cp.Payment_Type__c, paymentTypeTotal.get(cp.Payment_Type__c) + cp.Value__c);
						}
						// originalValues.put(cp.id, cp.clone(true));
					}

					if(result.size() == pos){
						result.add(ch);
						ldt.add(new paymentOrder(i.Received_Date__c, i.Client_Course__r.CurrencyIsoCode__c));
					}else{
						result.add(pos, ch);
						ldt.add(pos, new paymentOrder(i.Received_Date__c, i.Client_Course__r.CurrencyIsoCode__c));
					}
				}
			}//end for
		}

		/** Procuts **/
		if(lProducts!=null && lProducts.size()>0){
			Integer pos;
			for(client_product_service__c i : lProducts){


				if(ldt.size() == 0 )
					pos = 0;
				else pos = addObjectListPosition(i.Received_Date__c,ldt, i.Currency__c);
				ch = new consolidation();
				ch.product = i;

				for(client_course_instalment_payment__c cp :ch.product.client_course_instalment_payments__r){
					if(selectedPaymentTypes == cp.Payment_Type__c || selectedPaymentTypes == 'all' || showAll){
						if(!paymentTypeTotal.containsKey(cp.Payment_Type__c))
							paymentTypeTotal.put(cp.Payment_Type__c, cp.Value__c);
						else paymentTypeTotal.put(cp.Payment_Type__c, paymentTypeTotal.get(cp.Payment_Type__c) + cp.Value__c);
					}
					// originalValues.put(cp.id, cp.clone(true));
				}

				if(result.size() == pos){
					result.add(ch);
					ldt.add(new paymentOrder(i.Received_Date__c, i.Currency__c));
				}else{
					result.add(pos, ch);
					ldt.add(pos, new paymentOrder(i.Received_Date__c, i.Currency__c));
				}
			}//end for
		}
	}

	// F I N D 		I N V O I C E S
	public list<Invoice__c> findInvoices(String  fromDate, String  toDate){

		list<Invoice__c> lInvoice = new list<Invoice__c>();
		//mapInvoices = new map<Id, Invoice__c>();

		String sql = filters(true, fromDate, toDate);

		set<String> invoices = new set<String>();

		for(client_course_instalment__c cci : searchNS.NSInstalments(sql)){
			invoices.add(cci.Invoice__c);
		}//end for

		for(client_product_service__c cps:findProducts(true,fromDate,toDate)){
			invoices.add(cps.Invoice__c);
		}


		/** Payment Types Filter **/
		if(selectedPaymentTypes != 'all'){

			for(Invoice__c i: [SELECT Id, Name,  Client__c, Client__r.Name, Client__r.Owner__r.Name, Received_By__r.Name, Received_Date__c, Due_Date__c, Total_Value__c, Total_Products__c , Total_Instalments__c, Confirmed_Date__c,
								CurrencyIsoCode__c, Country__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, 
								Confirmed_By__c, Confirmed_By__r.Name, Total_Discount__c,
								( Select client_course__r.Course_Name__c, client_course__r.CurrencyIsoCode__c, Instalment_Value__c, Invoice__c from client_course_instalments__r),
								( Select Category__c, Price_Total__c, Product_Name__c, Currency__c from client_products_services__r),
								(SELECT id, selected__c, Value__c, Date_Paid__c, Payment_Type__c, Confirmed_By__r.Name, Confirmed_By_Agency__r.Name, Confirmed_Date__c, Unconfirmed_By__r.name, Unconfirmed_Date__c, Paid_From_Deposit__c, Paid_From_Deposit__r.Deposit_Reconciliated__c, Booking_Closed_On__c, Bank_Deposit__c FROM client_course_instalment_payments__r)
									FROM Invoice__c WHERE id in :invoices order by CurrencyIsoCode__c]){

				for(client_course_instalment_payment__c ccip : i.client_course_instalment_payments__r){
					if(ccip.Payment_Type__c == selectedPaymentTypes){
						lInvoice.add(i);
						break;
					}
				}//end for

			}//end for
		}
		else {
			lInvoice.addAll([SELECT Id, Name,  Client__c, Client__r.Name, Client__r.Owner__r.Name, Received_By__r.Name, Received_Date__c, Due_Date__c, Total_Value__c, Total_Products__c , Total_Instalments__c, Confirmed_Date__c,
								CurrencyIsoCode__c, Country__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, 
								Confirmed_By__c, Confirmed_By__r.Name, Total_Discount__c,
								( Select client_course__r.Course_Name__c, client_course__r.CurrencyIsoCode__c, Instalment_Value__c, Invoice__c from client_course_instalments__r),
								( Select Category__c, Price_Total__c, Product_Name__c, Currency__c from client_products_services__r),
								(SELECT id, selected__c, Value__c, Date_Paid__c, Payment_Type__c, Confirmed_By__r.Name, Confirmed_By_Agency__r.Name, Confirmed_Date__c, Unconfirmed_By__r.name, Unconfirmed_Date__c, Paid_From_Deposit__c, Paid_From_Deposit__r.Deposit_Reconciliated__c,Booking_Closed_On__c, Bank_Deposit__c FROM client_course_instalment_payments__r)
									FROM Invoice__c WHERE id in :invoices order by CurrencyIsoCode__c]);

		}
		/** END -- Payment Types Filter **/

		//mapInvoices = new map<Id, Invoice__c>(lInvoice);

		return lInvoice;
	}

	// F I N D 		P R O D U C T S
	public list<client_product_service__c> findProducts(boolean showInvoice, String  fromDate, String  toDate){

		list<client_product_service__c> listProducts = new list<client_product_service__c>();

		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && currentUser.Aditional_Agency_Managment__c == null) //set the user to see only his own agency
			selectedAgency = currentUser.Contact.AccountId;

		String filter = 'SELECT id, Name, Confirmed_Date__c, Category__c, Product_Name__c, Products_Services__r.Provider__r.Provider_Name__c, Confirmed_By__c, Price_Total__c, Quantity__c, Unit_Description__c, Received_By__r.Name, Received_Date__c, Confirmed_By__r.Name, Client__c, Client__r.Name, Client__r.Owner__r.Name, Invoice__c, Booking_Closed_On__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, Currency__c, ' +
							  
							'client_course__r.client__c, ' +
							'client_course__r.client__r.Name, ' +
							'client_course__r.Course_Name__c, ' +
							'client_course__r.Campus_Name__c, ' +
							'client_course__r.client__r.Owner__r.Name, ' +

							
							'(SELECT id, selected__c, Value__c, Date_Paid__c, Payment_Type__c, Confirmed_By__r.Name, Confirmed_By_Agency__r.Name, Confirmed_Date__c, Unconfirmed_By__r.name, Unconfirmed_Date__c, 	Paid_From_Deposit__c, 	Paid_From_Deposit__r.Deposit_Reconciliated__c, Booking_Closed_On__c, Bank_Deposit__c FROM client_course_instalment_payments__r), ' +

							'(SELECT Product_Name__c, Start_Date__c, End_Date__c, Price_Unit__c, Quantity__c, Price_Total__c, Unit_Description__c, Received_Date__c FROM paid_products__r order by Product_Name__c)' + 
					   'FROM client_product_service__c ';


		if(showInvoice){
			filter += 'WHERE Received_By_Agency__c = \'' + selectedAgency +'\' and Invoice__c != null and Received_Date__c != null  ';
		}
		else {
			filter += 'WHERE Received_By_Agency__c = \'' + selectedAgency +'\' and Invoice__c = null and 	Received_Date__c != null ';
		}

		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
		 	filter+= ' AND Received_By_Agency__r.ParentId = \'' +selectedAgencyGroup +'\'';

		/*if(selectedPaymentStatus == CONFIRMED_INSTALMENTS)
			filter += ' and Confirmed_Date__c != null ';
		else
			filter += ' and Confirmed_Date__c = null ';*/

		if(dateCriteria == 'rc'){
			if(SelectedPeriod == 'range')
				filter += ' and DAY_ONLY(convertTimezone(Received_Date__c)) >= '+ fromDate + ' and  DAY_ONLY(convertTimezone(Received_Date__c)) <= '+ toDate ;
			else if(SelectedPeriod == 'TODAY')
				filter += ' and Received_Date__c = TODAY ';
			else if(SelectedPeriod == 'THIS_WEEK')
				filter += ' and Received_Date__c = THIS_WEEK ';
			else if(SelectedPeriod == 'THIS_MONTH')
				filter += ' and Received_Date__c = THIS_MONTH ';
			else if(SelectedPeriod == 'unc'){
				//filter += ' and DAY_ONLY(convertTimezone(Received_Date__c)) >= '+ fromDate + ' and  DAY_ONLY(convertTimezone(Received_Date__c)) <= '+ toDate ; //commented
				filter += ' and Confirmed_Date__c = null ';
			}
		}else{
			if(SelectedPeriod == 'range')
				filter += ' and DAY_ONLY(convertTimezone(Confirmed_Date__c)) >= '+fromDate+' and  DAY_ONLY(convertTimezone(Confirmed_Date__c)) <= '+toDate;
			else if(SelectedPeriod == 'TODAY')
				filter += ' and Confirmed_Date__c = TODAY ';
			else if(SelectedPeriod == 'THIS_WEEK')
				filter += ' and Confirmed_Date__c = THIS_WEEK ';
			else if(SelectedPeriod == 'THIS_MONTH')
				filter += ' and Confirmed_Date__c = THIS_MONTH ';
			else if(SelectedPeriod == 'unc'){
				//filter += ' and DAY_ONLY(convertTimezone(Received_Date__c)) >= '+ fromDate + ' and  DAY_ONLY(convertTimezone(Received_Date__c)) <= '+ toDate ; //commented
				filter += ' and Confirmed_Date__c = null ';
			}
		}

		if(selectedSchool != 'all')
			filter += ' and client_course__r.Campus_Course__r.Campus__r.ParentId = \'' + selectedSchool +'\' ';
		
		filter += ' AND Paid_with_product__c = NULL ';

		filter += ' order by  Currency__c, Received_Date__c ';
		system.debug('productfilter==>' + filter);

		//listProducts = new list<client_product_service__c>();
		if(selectedPaymentTypes != 'all'){
			for(client_product_service__c cps : searchNS.NSProducts(filter)){
				for(client_course_instalment_payment__c ccip : cps.client_course_instalment_payments__r){
					if(ccip.Payment_Type__c == selectedPaymentTypes){
						listProducts.add(cps);
						break;
					}
				}
			}
		} else {
			listProducts = searchNS.NSProducts(filter);
		}

		return 	listProducts;
	}

	// F I N D 		I N S T A L M E N T S
	public list<client_course_instalment__c> findInstalments(String  fromDate, String  toDate){
		totalDiscount =	0;
		//lInstalments = new List<client_course_instalment__c>();

		String sql = filters(false, fromDate, toDate);

		system.debug('sql: ' + sql);

		list<client_course_instalment__c> lInstalments = new List<client_course_instalment__c>();

		if(selectedPaymentTypes != 'all'){
			for(client_course_instalment__c cci : searchNS.NSInstalments(sql)){
				if(cci.Discount__c!=null)
					totalDiscount += cci.Discount__c;
				for(client_course_instalment_payment__c ccip : cci.client_course_instalment_payments__r){
					if(ccip.Payment_Type__c == selectedPaymentTypes){
						lInstalments.add(cci);
						break;
					}
				}
			}
		} else {
			lInstalments = searchNS.NSInstalments(sql);
			for(client_course_instalment__c cci : lInstalments)
				if(cci.Discount__c!=null)
					totalDiscount += cci.Discount__c;
		}
		return lInstalments;
	}


	public void loadPayment(){
		editPayment = [SELECT Value__c, Date_Paid__c, Payment_Type__c, Client__r.Name FROM client_course_instalment_payment__c WHERE Id = :ApexPages.currentpage().getparameters().get('payID') limit 1];
		originalValue = editPayment.clone();
		ApexPages.currentpage().getparameters().remove('payID');
	}

	private client_course_instalment_payment__c originalValue {get;set;}
	public void savePaymentChanges(){

		if(unchangeableTypes.contains(editPayment.Payment_Type__c.toLowerCase())){
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'You cannot change the payment type to ' + editPayment.Payment_Type__c + '.'));
			// editPayment.Date_Paid__c.addError('You cannot change the payment type to ' + editPayment.Payment_Type__c + '.');
		}
		else if(editPayment.Date_Paid__c > system.today()){
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Paid Date cannot be greater than today.'));
			// editPayment.Date_Paid__c.addError('Paid Date cannot be greater than today.');
			showError = false;
		}
		else if(editPayment.Payment_Type__c==null || editPayment.Payment_Type__c==''){
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please select a valid Payment Type.'));
			// editPayment.Date_Paid__c.addError('Please select a valid Payment Type.');
			showError = false;
		}
		else{
			update editPayment;
			showError = true;
			editPayment = null;
		}
	}

	public String filters(Boolean showInvoice, String  fromDate, String  toDate){
			String filter = 'SELECT id, Name, Confirmed_Date__c, Confirmed_By__c, Instalment_Value__c, Due_Date__c, Discount__c, Invoice__c, Kepp_Fee__c, Commission_Tax_Value__c, Commission_Value__c, ' +
								  ' Received_By__r.Name, Received_Date__c, Confirmed_By__r.Name, client_course__c, Paid_To_School_By__r.Name, Paid_To_School_On__c, Booking_Closed_On__c, ' +
								  ' Tuition_Value__c, Extra_Fee_Discount_Value__c, Extra_Fee_Value__c,  Split_Number__c, Number__c, Amendment_Instalment_Balance__c,' +

								  'isInstalment_Amendment__c, isOverpayment_Amendment__c, isCommission_Amendment__c,' +


								  'Original_Instalment__r.isAmended_BeforeSchoolPayment__c,' +
									
								  'Agency_Currency__c, ' +
							  	  'Agency_Currency_Rate__c, ' +
							  	  'Agency_Currency_Value__c, ' +
							  	  'Client_Course__r.CurrencyIsoCode__c, '+
							  	  'Client_Course__r.Campus_Country__c, '+
								  
								  'client_course__r.client__c, ' +
								  'client_course__r.client__r.Name, ' +
								  'client_course__r.Course_Name__c, ' +
								  'client_course__r.Campus_Name__c, ' +
								  'client_course__r.Campus_Course__r.Campus__r.ParentId, ' +
								  'client_course__r.client__r.Owner__r.Name, ' +
								  '(SELECT id, selected__c, Value__c, Date_Paid__c, Payment_Type__c, Confirmed_By__r.Name, Confirmed_By_Agency__r.Name, Confirmed_Date__c, Unconfirmed_By__r.name, Unconfirmed_Date__c, Paid_From_Deposit__c, Paid_From_Deposit__r.Deposit_Reconciliated__c, Booking_Closed_On__c, Bank_Deposit__c ' +
								  '	FROM client_course_instalment_payments__r),' +

								  '(SELECT Id, Name from client_course_instalment_amendment__r limit 1)' +


						   'FROM client_course_instalment__c ';

			if(showInvoice){
				filter += 'WHERE Received_By_Agency__c = \'' + selectedAgency + '\' and isMigrated__c=false AND Invoice__r.isClientInvoice__c = true and Received_Date__c!= null ';
			}
			else {
				filter += 'WHERE Received_By_Agency__c  = \'' + selectedAgency  + '\' and isMigrated__c=false AND Invoice__c = null and Received_Date__c!= null ';
			}

			if(dateCriteria == 'rc'){
				if(SelectedPeriod == 'range')
					filter += ' and DAY_ONLY(convertTimezone(Received_Date__c)) >= '+fromDate+' and  DAY_ONLY(convertTimezone(Received_Date__c)) <= '+toDate;
				else if(SelectedPeriod == 'TODAY')
					filter += ' and Received_Date__c = TODAY ';
				else if(SelectedPeriod == 'THIS_WEEK')
					filter += ' and Received_Date__c = THIS_WEEK ';
				else if(SelectedPeriod == 'THIS_MONTH')
					filter += ' and Received_Date__c = THIS_MONTH ';
				else if(SelectedPeriod == 'unc'){
					//filter += ' and DAY_ONLY(convertTimezone(Received_Date__c)) >= '+fromDate+' and  DAY_ONLY(convertTimezone(Received_Date__c)) <= '+toDate;
					filter += ' and Confirmed_Date__c = null ';
				}
			}else{
				if(SelectedPeriod == 'range')
					filter += ' and DAY_ONLY(convertTimezone(Confirmed_Date__c)) >= '+fromDate+' and  DAY_ONLY(convertTimezone(Confirmed_Date__c)) <= '+toDate;
				else if(SelectedPeriod == 'TODAY')
					filter += ' and Confirmed_Date__c = TODAY ';
				else if(SelectedPeriod == 'THIS_WEEK')
					filter += ' and Confirmed_Date__c = THIS_WEEK ';
				else if(SelectedPeriod == 'THIS_MONTH')
					filter += ' and Confirmed_Date__c = THIS_MONTH ';
				else if(SelectedPeriod == 'unc'){
					//filter += ' and DAY_ONLY(convertTimezone(Received_Date__c)) >= '+fromDate+' and  DAY_ONLY(convertTimezone(Received_Date__c)) <= '+toDate;
					filter += ' and Confirmed_Date__c = null ';
				}
			}

			if(selectedSchool != 'all')
				filter += ' and client_course__r.School_Id__c = \'' + selectedSchool  + '\'';

			filter += ' and isPDS__c = false and isPCS__c = false and Paid_split__c = false ';

			if(selectedPaymentTypes.equalsIgnoreCase('offShore'))
				filter += ' and isPaidOffShore__c = true ';
			else
				filter += ' and isPaidOffShore__c = false ';

			filter += ' order by  Client_Course__r.CurrencyIsoCode__c, Received_Date__c ';

			system.debug('filter==>' + filter);

			return filter;
		}




	// C O N F I R M 		P A Y M E N T S
	public void confirmPayments(){

		Savepoint sp;
		string nullType = '';
		try{
			sp = Database.setSavepoint();
			set<Id> idsInst = new set<Id>();
			set<Id> idsInv = new set<Id>();
			set<Id> idsProd = new set<Id>();
			list<client_course_instalment_payment__c> toUpdatePayment = new list<client_course_instalment_payment__c>();
			list<SObject> toUpdate = new list<SObject>();

			for(client_course_instalment_payment__c ccip : [SELECT Id, Payment_Type__c, client_course_instalment__c,	Invoice__c,	client_product_service__c FROM client_course_instalment_payment__c WHERE id in :checkedPayments.split(';')]){

				if(ccip.Payment_Type__c!= null && ccip.Payment_Type__c!=''){
					if(ccip.client_course_instalment__c!=null)
						idsInst.add(ccip.client_course_instalment__c);

					else if(ccip.Invoice__c!=null)
						idsInv.add(ccip.Invoice__c);

					else if(ccip.client_product_service__c!=null)
						idsProd.add(ccip.client_product_service__c);

					toUpdatePayment.add(new client_course_instalment_payment__c (Id = ccip.Id, Confirmed_By__c = currentUser.id, Confirmed_Date__c = system.now(), Confirmed_By_Agency__c = currentUser.Contact.AccountId, Unconfirmed_By__c = null, Unconfirmed_Date__c = null));
				}
			}//end for Update Payments

			update toUpdatePayment;

			boolean allConfirmed;

			if(idsInst.size()>0){
				list<client_course_instalment__c> toUpdateInst = new list<client_course_instalment__c>();

				for(client_course_instalment__c cci : [Select Id, (SELECT Id, Confirmed_Date__c FROM client_course_instalment_payments__r) from client_course_instalment__c where id in :idsInst]){
					allConfirmed = true;

					for(client_course_instalment_payment__c instProd : cci.client_course_instalment_payments__r){
						if(instProd.Confirmed_Date__c == null){
							allConfirmed = false;
							break;
						}
					}//end prod for

					if(allConfirmed){
						cci.Confirmed_By__c = Userinfo.getuserId();
						cci.Confirmed_Date__c = System.now();
						toUpdate.add(cci);
					}

				}//end for instalment
			}

			if(idsInv.size()>0){
				for(Invoice__c inv : [Select Id, (SELECT Id FROM client_course_instalments__r), (SELECT Id FROM client_products_services__r), (SELECT Id, Confirmed_Date__c FROM client_course_instalment_payments__r) from Invoice__c where id in :idsInv]){
					allConfirmed = true;


					for(client_course_instalment_payment__c instProd : inv.client_course_instalment_payments__r){
						if(instProd.Confirmed_Date__c == null){
							allConfirmed = false;
							break;
						}
					}//end prod for

					if(allConfirmed){
						inv.Confirmed_By__c = Userinfo.getuserId();
						inv.Confirmed_Date__c = System.now();

						for(client_course_instalment__c cci:inv.client_course_instalments__r)
							toUpdate.add(new client_course_instalment__c(id = cci.id, Confirmed_By__c = Userinfo.getuserId(), Confirmed_Date__c = System.now()));

						for(client_product_service__c cps:inv.client_products_services__r)
							toUpdate.add(new client_product_service__c(id = cps.id, Confirmed_By__c = Userinfo.getuserId(), Confirmed_Date__c = System.now()));

						toUpdate.add(inv);
					}

				}//end for Invoice
			}

			if(idsProd.size()>0){
				for(client_product_service__c prod : [Select Id, (SELECT Id, Confirmed_Date__c FROM client_course_instalment_payments__r) from client_product_service__c where id in :idsProd]){
					allConfirmed = true;

					for(client_course_instalment_payment__c instProd : prod.client_course_instalment_payments__r){
						if(instProd.Confirmed_Date__c == null){
							allConfirmed = false;
							break;
						}
					}//end prod for

					if(allConfirmed){
						prod.Confirmed_By__c = Userinfo.getuserId();
						prod.Confirmed_Date__c = System.now();
						toUpdate.add(prod);
					}

				}//end for Product
			}

			update toUpdate;

		}
		catch(Exception e ){
			system.debug('Exception thrown: ' + e.getMessage());
			system.debug('Exception Line: ' + e.getLineNumber());
			Database.rollback(sp);
		}
		finally{
			searchPayments();
		}
	}

	public void unconfirmPayment(){

		String paymentid = ApexPages.currentpage().getparameters().get('paymentid');
		String updateObj = ApexPages.currentpage().getparameters().get('objUp');

		Savepoint sp;

		try{
			sp = Database.setSavepoint();

			List<SObject> toUpdate = new list<SObject>();

			toUpdate.add(new client_course_instalment_payment__c(id = id.valueOf(paymentid),Confirmed_By__c = null, Confirmed_Date__c = null, Confirmed_By_Agency__c = null, Unconfirmed_By__c = UserInfo.getUserId(), Unconfirmed_Date__c = system.now()));

			sobject sObj = id.valueOf(updateObj).getSobjectType().newSObject(updateObj) ;
			sObj.put('Confirmed_By__c', null);
			sObj.put('Confirmed_Date__c', null);
			toUpdate.add(sObj);

			//If it is a Client Invoice Unconfirm also its instalments
			if(id.valueOf(updateObj).getSobjectType() == Invoice__c.sObjectType){
				for(client_course_instalment__c cci : [SELECT Id FROM client_course_instalment__c WHERE Invoice__c = :id.valueOf(updateObj)])
					toUpdate.add(new client_course_instalment__c(id = cci.id, Confirmed_By__c = null, Confirmed_Date__c = null));
				for(client_product_service__c ccip : [SELECT Id FROM client_product_service__c WHERE Invoice__c = :id.valueOf(updateObj)])
					toUpdate.add(new client_product_service__c(id = ccip.id, Confirmed_By__c = null, Confirmed_Date__c = null));
			}

			update toUpdate;
		}
		catch(Exception e ){
			system.debug('Exception thrown: ' + e.getMessage());
			system.debug('Exception Line: ' + e.getLineNumber());
			Database.rollback(sp);
		}
		finally{
			searchPayments();
		}
	}

	public class consolidation{
		public Invoice__c invoice {get;set;}
		public client_course_instalment__c instalment {get;set;}
		public client_product_service__c product {get;set;}
		public boolean showSave {get{if(showSave==null) showSave = false; return showSave;}set;}
	}


public string selectedAgencyGroup{
  	get{
  		if(selectedAgencyGroup == null)
  			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
  		return selectedAgencyGroup;
  	}
  	set;
  }

 public List<SelectOption> agencyGroupOptions {
		get{
 			if(agencyGroupOptions == null){

 				agencyGroupOptions = new List<SelectOption>();
 				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group
    			for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:currentUser.Contact.AccountId order by Parent.Name]){
	     			agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
	    		}
 				}else{
 					for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
	     			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
	    		}
 				}
   		}
   		return agencyGroupOptions;
  }
  set;
	}
public list<SelectOption> getAgencies(){
	List<SelectOption> result = new List<SelectOption>();
	if(selectedAgencyGroup != null){
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
			for(Account a: [Select Id, Name from Account where id =:currentUser.Contact.AccountId order by name]){
				if(selectedAgency=='')
					selectedAgency = a.Id;
				result.add(new SelectOption(a.Id, a.Name));
			}
			if(currentUser.Aditional_Agency_Managment__c != null){
				for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
					result.add(new SelectOption(ac.id, ac.name));
				}
			}
		}else{
			for(Account a: [Select Id, Name from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
				result.add(new SelectOption(a.Id, a.Name));
				if(selectedAgency=='')
					selectedAgency = a.Id;
			}
		}
	}

	return result;
}
public void changeGroup(){
	selectedAgency = '';
	getAgencies();
}
public list<SelectOption> schools { get{
	if(schools==null){
		IPFunctions.CampusAvailability result = new IPFunctions.CampusAvailability();
		schools = result.getGroupSchools();
	}
	return schools;}set;}
private static integer addObjectListPosition(dateTime d, List<paymentOrder> ld, string cry){
	integer pos = 0;
	for(paymentOrder dt:ld)
		if(d < dt.dt && dt.cry == cry){
			return pos;
		}
		else pos++;
	return pos;
}
public boolean showAll{get{if(showAll == null) showAll = false; return showAll;} set;}
public string SelectedPeriod{get {if(SelectedPeriod == null) SelectedPeriod = 'TODAY'; return SelectedPeriod; } set;}
  private List<SelectOption> periods;
  public List<SelectOption> getPeriods() {
      if(periods == null){
          periods = new List<SelectOption>();
          periods.add(new SelectOption('TODAY','Today'));
          periods.add(new SelectOption('THIS_WEEK','This Week'));
          periods.add(new SelectOption('THIS_MONTH','This Month'));
          periods.add(new SelectOption('unc','Unconfirmed'));
          periods.add(new SelectOption('range','Range'));
      }
      return periods;
  }

  public string dateCriteria{get {if(dateCriteria == null) dateCriteria = 'rc'; return dateCriteria; } set;}
  private List<SelectOption> dateCriteriaOptions;
  public List<SelectOption> getdateCriteriaOptions() {
      if(dateCriteriaOptions == null){
          dateCriteriaOptions = new List<SelectOption>();
          dateCriteriaOptions.add(new SelectOption('rc','Received from Client'));
          dateCriteriaOptions.add(new SelectOption('pr','Payment Reconciled'));
      }
      return dateCriteriaOptions;
  }

   public client_course_instalment__c dateFilter{
	get{
		if(dateFilter == null){
			dateFilter = new client_course_instalment__c();
			Date myDate = Date.today();
			Date weekStart = myDate.toStartofWeek();
			dateFilter.Original_Due_Date__c = weekStart;
			dateFilter.Discounted_On__c = dateFilter.Original_Due_Date__c.addDays(6);
		}
		return dateFilter;
	}
	set;
}
}