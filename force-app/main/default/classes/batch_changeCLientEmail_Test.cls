global class batch_changeCLientEmail_Test implements Database.Batchable<sObject> {   
    
    
    global batch_changeCLientEmail_Test() {    
        
    }  
    
        
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        String query = 'Select Id, name, Email, MobilePhone, (select detail__c from forms_of_contact__r where type__c = \'Mobile\')  from Contact WHERE RecordType.name IN (\'employee\',\'client\',\'lead\',\'School Campus Contact\') and email != null AND (not email like \'test_%\') ';
                
        return Database.getQueryLocator(query);
    }   
    
    global void execute(Database.BatchableContext BC, List<Contact> scope){   
        List<Forms_of_Contact__c> focs = new List<Forms_of_Contact__c>();
        for(Contact cd:scope){
            cd.Email = 'test_' + cd.email;
            cd.MobilePhone = '000-' + cd.MobilePhone;
            if(cd.MobilePhone.length() > 40)
                cd.MobilePhone = cd.MobilePhone.left(40);
            for(Forms_of_Contact__c foc : cd.forms_of_contact__r){
                foc.detail__c = '000-' + foc.detail__c;
                focs.add(foc);
            }
        }
        
        update scope;
        update focs;

    }   
    
    global void finish(Database.BatchableContext BC){       
        System.debug('Batch Process Complete');   
    } 
    
    
    
/*open up the Debug Log and run this
    batch_changeCLientEmail_Test batch = new batch_changeCLientEmail_Test();
    Id batchId = Database.executeBatch(batch);



    batch_changeCLientEmail_Test batch = new batch_changeCLientEmail_Test();
    Id batchId = Database.executeBatch(batch, 80); //Define batch size
*/

}