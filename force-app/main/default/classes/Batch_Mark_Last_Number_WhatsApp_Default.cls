global class Batch_Mark_Last_Number_WhatsApp_Default implements Database.Batchable<sObject> {
    String idAustraliaGroup;
    String [] contacts;

    global Batch_Mark_Last_Number_WhatsApp_Default() {
        idAustraliaGroup = '0019000001MjqDY';
        contacts = new String [] {'0039000001i2VNgAAM','0039000001bofw5AAA'};
    }

	global Database.QueryLocator start(Database.BatchableContext BC) {
        //String query = 'SELECT ID FROM Contact WHERE Current_Agency__r.Parent.ID = :idAustraliaGroup AND ID IN :contacts';
        String query = 'SELECT ID FROM Contact WHERE Current_Agency__r.Parent.ID = :idAustraliaGroup';
        
		return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Contact> ctts) {
        Set<String> ids = new Set<String>();
        for(Contact ctt : ctts){
            ids.add(ctt.ID);
        }
        List<String> types = new List<string>{'WhatsApp','Mobile'};
        Set<String> contactsIds = new Set<String>();
        List<Forms_Of_Contact__c> toUpdate = new List<Forms_Of_Contact__c>();
        for(Forms_Of_Contact__c fc : [SELECT ID, Contact__c FROM Forms_Of_Contact__c WHERE Type__c IN :types AND Contact__c IN :ids ORDER BY Contact__c, LastModifiedDate DESC]){
            fc.Is_WhatsApp_Number__c = contactsIds.contains(fc.Contact__c) ? false : true;
            contactsIds.add(fc.Contact__c);
            toUpdate.add(fc);
        }
        if(!toUpdate.isEmpty()){
            update toUpdate;
        }
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}

}