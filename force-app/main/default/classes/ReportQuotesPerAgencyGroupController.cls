public with sharing class ReportQuotesPerAgencyGroupController{

	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}

	public ReportQuotesPerAgencyGroupController(Apexpages.StandardController controller){
		selectedAgency = userDetails.Contact.AccountId;
	}
	
	//return the user's agency 
	public User userDetails{
		get{
			if (userDetails == null)
				userDetails = [SELECT Contact.AccountId, Contact.Account.Name, Contact.Account.ParentId, Name, Email, Contact.Export_Report_Permission__c FROM User WHERE id = :UserInfo.getUserId()];
			return userDetails;
		}
		set;
	}
	
	public Contact fromDate {
		get{
			if(fromDate == null){
				fromDate = new Contact();
				fromDate.Expected_Travel_Date__c = system.today();
			}
			return fromDate;}
		set;}
			
	public Contact toDate {
		get{
			if(toDate == null){
				toDate = new Contact();
				toDate.Expected_Travel_Date__c = system.today();
			}
			return toDate;}
			set;}
	
	map<string,string> selectedGroupName; 
	public String selectedAgencyGroup {get{if(selectedAgencyGroup == null) selectedAgencyGroup = userDetails.Contact.Account.ParentID; return selectedAgencyGroup;}set;}
	public List<SelectOption> agencyGroupOptions {
		get{
		if(agencyGroupOptions == null){
			selectedGroupName = new map<string,string>();
			agencyGroupOptions = new List<SelectOption>();
			//agencyGroupOptions.add(new SelectOption('', 'Select Agency Group'));	
			for(Account ag : [select id, name from Account where RecordType.Name='Agency Group' order by Name]){
			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
			selectedGroupName.put(ag.id, ag.Name);
		}
				
			}
		return agencyGroupOptions;
	}
		set;
	}
	
	public String selectedDateOptions {get{if(selectedDateOptions == null) selectedDateOptions = 'co'; return selectedDateOptions;}set;}
	public List<SelectOption> getDateOptions(){
		List<SelectOption> dateOptions = new List<SelectOption>();
		dateOptions.add(new SelectOption('co', 'Quote Created on'));	
		dateOptions.add(new SelectOption('eo', 'Quote Expiry on'));	
		return dateOptions;
	}
	
	public string getGroupName(){
		return 	selectedGroupName.get(selectedAgencyGroup);
	}
	
	public void searchAgencies(){
		agencyOptions = null;
		selectedAgency = null;
		userOptions = null;
		selectedUser = null;
	}
	
	private set<id> agenciesId;
	public String selectedAgency {get{if(selectedAgency == null) selectedAgency = ''; return selectedAgency;}set;}
	public List<SelectOption> agencyOptions {
		get{
		if(agencyOptions == null){
			agencyOptions = new List<SelectOption>();
			agencyOptions.add(new SelectOption('', '--All--'));
			agenciesId = new Set<ID>();
			for(Account ag : [Select Id, Name from Account where ParentId = :selectedAgencyGroup order by Name]){
				agencyOptions.add(new SelectOption(ag.Id, ag.Name));
				agenciesId.add(ag.Id);	
			}	
		}
		return agencyOptions;
	}
		set;
	}
	
	public void searchUsers(){
		userOptions = null;
	}
	List<id> lEmployee = new List<id>();
	public list<String> selectedUser {get{if(selectedUser == null) selectedUser = new list<String>(); return selectedUser;}set;}
	public List<SelectOption> userOptions {
		get{
		if(userOptions == null){
			userOptions = new List<SelectOption>();
			List<User> lUsers = new List<User>();
			if(selectedAgency == null || selectedAgency == '')
				lUsers = [SELECT id, Name FROM User WHERE Contact.AccountId in :agenciesId and IsActive = true and Contact.Chatter_Only__c = false order by name];
			else
				lUsers = [SELECT id, Name FROM User WHERE Contact.AccountId = :selectedAgency and IsActive = true and Contact.Chatter_Only__c = false order by name];
			
			for(User ac : lUsers){
				userOptions.add(new SelectOption(ac.Id, ac.Name));	
				lEmployee.add(ac.Id);	
			}
		}
		return userOptions;
	}
		set;
	}
	
	
	public List<AgencyWrapper> agencyList {
		get{
		if(agencyList == null)
			agencyList = new List<AgencyWrapper>();
		return agencyList;
	}
		set;
	}
	
	
	public class AgencyWrapper {
		public String agencyID {get;set;}
		public String agencyName {get;set;}
		public Integer quoteCount {get{if(quoteCount == null) quoteCount = 0; return quoteCount;}set;}
		public map<string, integer> countQuotesStatus{get{if(countQuotesStatus == null) countQuotesStatus = new map<string, integer>(); return countQuotesStatus;} set;}
		public Integer quoteCountEnroll {get{if(quoteCountEnroll == null) quoteCountEnroll = 0; return quoteCountEnroll;}set;}
		public Integer quotesPerClient {get{if(quotesPerClient == null) quotesPerClient = 0; return quotesPerClient;}set;}
		public List<UserWrapper> userList {
			get{
			if(userList == null)
				userList = new List<UserWrapper>();
			return userList;
		}
			set;}
	}
	
	
	public class UserWrapper {
		public String userID {get;set;}
		public String userName {get;set;}
		public Integer quoteCount {get{if(quoteCount == null) quoteCount = 0; return quoteCount;}set;}
		public map<string, integer> countQuotesStatus{get{if(countQuotesStatus == null) countQuotesStatus = new map<string, integer>(); return countQuotesStatus;} set;}
		public Integer quoteCountEnroll {get{if(quoteCountEnroll == null) quoteCountEnroll = 0; return quoteCountEnroll;}set;}
		public Integer quotesPerClient {get{if(quotesPerClient == null) quotesPerClient = 0; return quotesPerClient;}set;}
		public map<string, List<quotation>> quoteList {
			get{
			if(quoteList == null)
				quoteList = new map<string, List<quotation>>();
			return quoteList;
		}
			set;
		}
	
	}	
	
	
	/*public List<String> unsuccessfulReason {get{if(unsuccessfulReason == null) unsuccessfulReason = new List<String>(); return unsuccessfulReason;}set;}
	public List<SelectOption> getunsuccessful_items() {
		List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult F = Quote__c.fields.Unsuccessful__c.getDescribe();
		List<Schema.PicklistEntry> P = F.getPicklistValues();
		for (Schema.PicklistEntry lst:P){
			options.add(new SelectOption(lst.getLabel(),lst.getValue()));
		}
		return options;
	}*/
	
	
	public Map<String, String> agenciesMap { get;set; }
	Set<String> userIDs = new Set<String>();
	
	
	public class quotation{
		public String name {get;set;}
		public String quoteID {get;set;}
		public String nationality {get;set;}
		public String clientStatus {get;set;}
		public String fullName {get;set;}
		public String createdById {get;set;}
		public DateTime createdDate {get;set;}
		public Date expiryDate {get;set;}
		public String statusId {get;set;}
		public String clientId {get;set;}
		public Integer totQuotes {get;set;}
	}
	
	
	public void search(){		
		clientDetails = null;		
		agencyList = null;
		List<AggregateResult> agencies;
	
		if(selectedAgencyGroup != null && selectedAgencyGroup != ''){
		
			agenciesMap = new Map<string, String>();
			if(selectedAgency == null || selectedAgency == ''){			
				for(Account ag : [SELECT Id, Name FROM Account WHERE ParentId = :selectedAgencyGroup and RecordType.Name = 'Agency' order by Name]){
					agenciesMap.put(ag.Id, ag.Name);				
					AgencyWrapper aw = new AgencyWrapper();
					aw.agencyID = ag.Id;
					aw.agencyName = ag.Name;			
					agencyList.add(aw);
				}
							
			} else {		
				for(Account a : [Select id, Name  from Account A where id = :selectedAgency]){
					agenciesMap.put(a.Id, a.Name);
					AgencyWrapper aw = new AgencyWrapper();
					aw.agencyID = a.id;
					aw.agencyName = a.Name;
					agencyList.add(aw);
				}
			}
		
			List<User> users;		
			if(selectedUser.size() > 0)
				users = [SELECT Contact.AccountId, Name, Id FROM User WHERE Id in :selectedUser and isActive = true  order by name desc];
			else 
				users = [SELECT Contact.AccountId, Name, Id FROM User WHERE Contact.AccountId in :agenciesMap.keySet() and isActive = true order by name desc];
	
			userIDs = new Set<String>();
			for(User a : users){
				userIDs.add(a.Id);
				UserWrapper uw = new UserWrapper();
				uw.userID = a.Id;
				uw.userName = a.Name;
				for(AgencyWrapper aw: agencyList)
					if(aw.agencyID == a.Contact.AccountId)
						aw.userList.add(uw);
				
			}
			
			if(selectedUser.size() == 1){
				String sql = 'Select id, Name, Q.Client__r.Nationality__c,Q.Client__c,  Q.Client__r.Name, Q.Client__r.Status__c, createdById, createdDate, expiry_date__c, status__c   ' +
					' FROM Quotation__c Q where createdById = \'' + selectedUser[0] + '\'';
			
				if(selectQuoteStatus.size() > 0)
					sql += ' and status__c  in :selectQuoteStatus ';
			
				/*if(unsuccessfulReason.size() > 0)
					sql += ' and Unsuccessful__c  in :unsuccessfulReason ';
				*/
				if(fromDate.Expected_Travel_Date__c != null)
					if (selectedDateOptions == 'co')
						sql += ' and createdDate >= ' + FormatSqlDateTimeIni(fromDate.Expected_Travel_Date__c) + ' ';
					else  sql += ' and expiry_date__c >= ' + FormatSqlDate(fromDate.Expected_Travel_Date__c) + ' ';
			
				if(toDate.Expected_Travel_Date__c != null)
					if (selectedDateOptions == 'co')
						sql += ' and createdDate <= ' + FormatSqlDateTimeFin(toDate.Expected_Travel_Date__c) + ' ';
					else sql += ' and expiry_date__c <= ' + FormatSqlDate(toDate.Expected_Travel_Date__c) + ' ';
			
			
				sql += ' order by q.createdDate desc ';
				
				
				List<quotation> quotes = new List<quotation>();
				//List<Quote__c> quotes = Database.query(sql);
				
				/* Quotation result */
				for(Quotation__c result : database.query(sql)){
					quotation qt = new quotation();
					qt.name = result.Name;
					qt.quoteId = result.Id;
					qt.nationality = result.Client__r.Nationality__c;
					qt.fullName = result.Client__r.Name;
					qt.clientStatus = result.Client__r.Status__c;
					qt.createdById = result.CreatedById;
					qt.createdDate = result.CreatedDate;
					qt.expiryDate = result.Expiry_date__c;
					qt.statusId = result.Status__c;
					qt.clientId = result.Client__c;
										
					quotes.add(qt);
				}
				
				/*sql = sql.replace('Quotation__c', 'Quotes_archive__c');
				sql = sql.replace('status__c', 'QuoteStatus__c');
				sql = sql.replace('expiry_date__c', 'EndDate__c');
				sql = sql.replace('createdByid', 'CreatedByUser__c');
				sql = sql.replace('createdDate ', 'CreatedDate__c');
				sql = sql.replace('id ', 'QuoteId__c');
				
				
				 Quotation Archive result 
				for(Quotes_archive__c result : database.query(sql)){
					quotation qt = new quotation();
					qt.name = result.Name;
					qt.nationality = result.Client__r.Nationality__c;
					qt.fullName = result.Client__r.Name;
					qt.createdById = result.CreatedByUser__c;
					qt.createdDate = result.CreatedDate__c;
					qt.expiryDate = result.EndDate__c;
					qt.statusId = result.QuoteStatus__c;
					qt.clientId = result.Client__c;
					qt.quoteId = result.QuoteId__c;
					
					quotes.add(qt);
				}*/
				
				
			
				Set<String> clientIds = new Set<String>();
	
				//system.debug('==> sql: '+sql);
				for(quotation q : quotes)
					for(AgencyWrapper aw: agencyList)
						for(UserWrapper uw : aw.userList)
							if(uw.userID == q.CreatedById){
								clientIds.add(q.clientId);
								if(!uw.quoteList.containsKey(q.clientId)){
									List<quotation> lq = new List<quotation>();								
									lq.add(q);
									uw.quoteList.put(q.clientId, lq);
								} else uw.quoteList.get(q.clientId).add(q);
							}
								//uw.quoteList.add(q);
				getClientDetails(clientIds);
			}
			
			
			getNumberOfQuotesPerUser(userIDs);
		}
	}
		
	
	public string getQuotesperStatus(){

		String InnerSql = 'Select id from Quotation__c Q where createdByid in :userIDs ';
		if(selectQuoteStatus.size() > 0)
			InnerSql += ' and status__c  in :selectQuoteStatus ';
		
		/*if(unsuccessfulReason.size() > 0)
			InnerSql += ' and Unsuccessful__c  in :unsuccessfulReason ';
		*/
		if(fromDate.Expected_Travel_Date__c != null)
			if (selectedDateOptions == 'co')
				InnerSql += ' and createdDate >= ' + FormatSqlDateTimeIni(fromDate.Expected_Travel_Date__c) + ' ';
			else  InnerSql += ' and expiry_date__c >= ' + FormatSqlDate(fromDate.Expected_Travel_Date__c) + ' ';
			
		if(toDate.Expected_Travel_Date__c != null)
			if (selectedDateOptions == 'co')
				InnerSql += ' and createdDate <= ' + FormatSqlDateTimeFin(toDate.Expected_Travel_Date__c) + ' ';
			else InnerSql += ' and expiry_date__c <= ' + FormatSqlDate(toDate.Expected_Travel_Date__c) + ' ';
	
		if(SelectedUser.size() > 0)
			InnerSql += ' and createdById in :selectedUser ';
	
		set<string> lids = new set<string>();
	
		for(Quotation__c a:Database.query(InnerSql))
			lids.add(a.id);
		
		/*InnerSql = InnerSql.replace('Quotation__c', 'Quotes_archive__c');
		InnerSql = InnerSql.replace('status__c', 'QuoteStatus__c');
		InnerSql = InnerSql.replace('expiry_date__c', 'EndDate__c');
		InnerSql = InnerSql.replace('createdByid', 'CreatedByUser__c');
		InnerSql = InnerSql.replace('createdDate', 'CreatedDate__c');
		InnerSql = InnerSql.replace(' id ', ' QuoteId__c ');		
		
		for(Quotes_archive__c a:Database.query(InnerSql))
			lids.add(a.id);	*/
			
		string qpu = '';
		String sql =  'Select status__c, Count(id) tot from Quotation__c where id in :lids ';
				
		sql += ' group by status__c ';
		
		map<string,integer> totMap = new map<string,integer>();
		
		/*put result in the map to sum with the arquives search*/
		for(AggregateResult ar : Database.query(sql)){
			totMap.put(String.valueOf(ar.get('status__c')), Integer.valueOf(ar.get('tot')));
		}
		
		/*Do another search for the arquives
		sql = sql.replace('Quotation__c', 'Quotes_archive__c');
		sql = sql.replace('status__c', 'QuoteStatus__c');
		sql = sql.replace('expiry_date__c', 'EndDate__c');
		sql = sql.replace('createdByid', 'CreatedByUser__c');
		sql = sql.replace('createdDate', 'CreatedDate__c');
		sql = sql.replace('(id)', '(QuoteId__c)');
					
		for(AggregateResult ar : Database.query(sql)){
			if(!totMap.containsKey(String.valueOf(ar.get('QuoteStatus__c'))))
				totMap.put(String.valueOf(ar.get('QuoteStatus__c')), Integer.valueOf(ar.get('tot')));
			else
				totMap.put(String.valueOf(ar.get('QuoteStatus__c')), (totMap.get(String.valueOf(ar.get('QuoteStatus__c'))) + Integer.valueOf(ar.get('tot'))));
		}*/
		
		for (string statusName : totMap.keySet()){
		    qpu += String.valueOf('('+ statusName )+ ' -  ' + totMap.get(statusName) + ' ) &nbsp;  ';
		}
		
		return qpu;
	}
	
	
	public Map<String, Contact> clientDetails {
		get{
		if(clientDetails == null)
			clientDetails = new Map<String, Contact>();
		
		return clientDetails;
	}
		set;
	}
	
	private void getClientDetails(Set<String> clientIds){
		system.debug('clientsIDs---' + clientIds);
		list<Contact> la = [Select Id, (Select Client__c, Checklist_Item__c, Checklist_stage__c, createdDate, CreatedBy.name from Client_Checklist__r order by createdDate desc limit 1),
		( Select Agency__r.Name, Client__c, Stage__c, Stage_Item__c, Stage_Item_Id__c, createdDate, CreatedBy.name from Client_Stage_Follow_Up__r order by Last_Saved_Date_Time__c desc limit 1)
		from Contact where id in :clientIds];
		
		system.debug('la---' + la);
		
			
		for( Contact ac : la )										
			if(!clientDetails.containsKey(ac.id)){
				clientDetails.put(ac.id, ac);
				system.debug('ac---' + ac);
				system.debug('ac---' + ac.Client_Stage_Follow_Up__r);
		
			}
				
		clientDetails.put('',new Contact());
		clientDetails.put(null,new Contact());
			
		//system.debug('@@@@@@@@@@@@@ clientDetails.size=' + clientDetails.size());
		
	}
		
		
	public void searchQuotesByUser(){
	
		String theUserID = ApexPages.currentPage().getParameters().get('userID');
		String showALL = ApexPages.currentPage().getParameters().get('showALL');
	
		String sql = '';
		if(theUserID != null && theUserID != ''){
			List<quotation> quotes = new List<quotation>();
			if(selectQuoteStatus.size() > 0)
				if (selectedDateOptions == 'co'){
					
					//Query fields deleted: Unsuccessful__c, status_comments__c
					/* Quotation result */
					sql = 'Select Id, Name, Q.Client__r.Nationality__c, Q.Client__r.Status__c, Q.Client__c,  Q.Client__r.Name, Q.CreatedById, Q.CreatedDate, Q.Expiry_date__c, Q.Status__c ' +
						   ' FROM Quotation__c Q where CreatedById = :theUserID and CreatedDate >= ' + FormatSqlDateTimeIni(fromDate.Expected_Travel_Date__c) +' and CreatedDate <= ' + FormatSqlDateTimeFin(toDate.Expected_Travel_Date__c) + ' and status__c in :selectQuoteStatus order by q.CreatedDate desc';
					for(Quotation__c result : Database.query(sql)){
						quotation qt = new quotation();
						qt.name = result.Name;
						qt.quoteId = result.id;
						qt.nationality = result.Client__r.Nationality__c;
						qt.fullName = result.Client__r.Name;
						qt.clientStatus = result.Client__r.Status__c;
						qt.createdById = result.CreatedById;
						qt.createdDate = result.CreatedDate;
						qt.expiryDate = result.Expiry_date__c;
						qt.statusId = result.Status__c;
						qt.clientId = result.Client__c;
											
						quotes.add(qt);
					}
					
					/* Quotation Archive result 
					for(Quotes_archive__c result : [Select QuoteId__c, Name, Q.Client__r.Nationality__c, Q.Client__c, Q.Client__r.Name, Q.CreatedByUser__c, Q.CreatedDate__c, Q.EndDate__c, Q.QuoteStatus__c 
											   FROM Quotes_archive__c Q where CreatedByUser__c = :theUserID and CreatedDate__c >= :fromDate.Expected_Travel_Date__c and CreatedDate__c <= :toDate.Expected_Travel_Date__c and QuoteStatus__c in :selectQuoteStatus order by q.CreatedDate__c desc]){
						quotation qt = new quotation();
						qt.name = result.Name;
						qt.quoteId = result.QuoteId__c;
						qt.nationality = result.Client__r.Nationality__c;
						qt.fullName = result.Client__r.Name;
						qt.createdById = result.CreatedByUser__c;
						qt.createdDate = result.CreatedDate__c;
						qt.expiryDate = result.EndDate__c;
						qt.statusId = result.QuoteStatus__c;
						qt.clientId = result.Client__c;
											
						quotes.add(qt);
					}*/
				}
				
				
				
			else{  
				//Query fields deleted: Unsuccessful__c, status_comments__c
				/* Quotation result */
				sql = 'Select Id, Name, Q.Client__r.Nationality__c, Q.Client__r.Status__c,  Q.Client__c,  Q.Client__r.Name, Q.CreatedById, Q.CreatedDate, Q.Expiry_date__c, Q.Status__c  '+
					   ' FROM Quotation__c Q where CreatedById = :theUserID and Expiry_date__c >= ' + FormatSqlDate(fromDate.Expected_Travel_Date__c) +' and Expiry_date__c <= ' +FormatSqlDate(toDate.Expected_Travel_Date__c) +'  and status__c in :selectQuoteStatus order by q.CreatedDate desc';
				for(Quotation__c result : Database.query(sql)){
					quotation qt = new quotation();
					qt.name = result.Name;
					qt.quoteId = result.Id;
					qt.nationality = result.Client__r.Nationality__c;
					qt.fullName = result.Client__r.Name;
					qt.clientStatus = result.Client__r.Status__c;
					qt.createdById = result.CreatedById;
					qt.createdDate = result.CreatedDate;
					qt.expiryDate = result.Expiry_date__c;
					qt.statusId = result.Status__c;
					qt.clientId = result.Client__c;
										
					quotes.add(qt);
				}
				
				/* Quotation Archive result 
				for(Quotes_archive__c result : [Select QuoteId__c , Name, Q.Client__r.Nationality__c, Q.Client__c,  Q.Client__r.Name, Q.CreatedByUser__c, Q.CreatedDate__c, Q.EndDate__c, Q.QuoteStatus__c  
										   FROM Quotes_archive__c Q where CreatedByUser__c = :theUserID and EndDate__c >= :fromDate.Expected_Travel_Date__c and EndDate__c <= :toDate.Expected_Travel_Date__c and QuoteStatus__c in :selectQuoteStatus order by q.CreatedDate__c desc]){
					quotation qt = new quotation();
					qt.name = result.Name;
					qt.quoteId = result.QuoteId__c;
					qt.nationality = result.Client__r.Nationality__c;
					qt.fullName = result.Client__r.Name;
					qt.createdById = result.CreatedByUser__c;
					qt.createdDate = result.CreatedDate__c;
					qt.expiryDate = result.EndDate__c;
					qt.statusId = result.QuoteStatus__c;
					qt.clientId = result.Client__c;
										
					quotes.add(qt);
				}*/
			
			}
			else{
				if (selectedDateOptions == 'co'){
					//Query fields deleted: Unsuccessful__c, status_comments__c
					/* Quotation result */
					sql  = 'Select Id, Name, Q.Client__r.Nationality__c, Q.Client__r.Status__c, Q.Client__c,  Q.Client__r.Name, Q.CreatedById, Q.CreatedDate, Q.Expiry_date__c, Q.Status__c  ' +
							' FROM Quotation__c Q where CreatedById = :theUserID and CreatedDate >= ' + FormatSqlDateTimeIni(fromDate.Expected_Travel_Date__c) +' and CreatedDate <= ' + FormatSqlDateTimeFin(toDate.Expected_Travel_Date__c) + ' order by q.CreatedDate desc';
					for(Quotation__c result : Database.query(sql)){
						quotation qt = new quotation();
						qt.name = result.Name;
						qt.quoteId = result.Id;
						qt.nationality = result.Client__r.Nationality__c;
						qt.fullName = result.Client__r.Name;
						qt.clientStatus = result.Client__r.Status__c;
						qt.createdById = result.CreatedById;
						qt.createdDate = result.CreatedDate;
						qt.expiryDate = result.Expiry_date__c;
						qt.statusId = result.Status__c;
						qt.clientId = result.Client__c;
											
						quotes.add(qt);
					}
					
					/* Quotation Archive result
					for(Quotes_archive__c result : [Select QuoteId__c, Name, Q.Client__r.Nationality__c, Q.Client__c,  Q.Client__r.Name, Q.CreatedByUser__c, Q.CreatedDate__c, Q.EndDate__c, Q.QuoteStatus__c   
													FROM Quotes_archive__c Q where CreatedByUser__c = :theUserID and CreatedDate__c >= :fromDate.Expected_Travel_Date__c and CreatedDate__c <= :toDate.Expected_Travel_Date__c order by q.CreatedDate__c desc]){
						quotation qt = new quotation();
						qt.name = result.Name;
						qt.quoteId = result.QuoteId__c;
						qt.nationality = result.Client__r.Nationality__c;
						qt.fullName = result.Client__r.Name;
						qt.createdById = result.CreatedByUser__c;
						qt.createdDate = result.CreatedDate__c;
						qt.expiryDate = result.EndDate__c;
						qt.statusId = result.QuoteStatus__c;
						qt.clientId = result.Client__c;
											
						quotes.add(qt);
					} */
				}
					
				else{
					//Query fields deleted: Unsuccessful__c, status_comments__c
					/* Quotation result */
					sql = 'Select Id, Name, Q.Client__r.Nationality__c, Q.Client__r.Status__c, Q.Client__c,  Q.Client__r.Name, Q.CreatedById, Q.CreatedDate, Q.Expiry_date__c, Q.Status__c  ' +
							' FROM Quotation__c Q where CreatedById = :theUserID and Expiry_date__c >= ' + FormatSqlDate(fromDate.Expected_Travel_Date__c) + ' and Expiry_date__c <= ' + FormatSqlDate(toDate.Expected_Travel_Date__c) +' order by q.CreatedDate desc';
					for(Quotation__c result : Database.query(sql)){
						quotation qt = new quotation();
						qt.name = result.Name;
						qt.quoteId = result.Id;
						qt.nationality = result.Client__r.Nationality__c;
						qt.fullName = result.Client__r.Name;
						qt.clientStatus = result.Client__r.Status__c;
						qt.createdById = result.CreatedById;
						qt.createdDate = result.CreatedDate;
						qt.expiryDate = result.Expiry_date__c;
						qt.statusId = result.Status__c;
						qt.clientId = result.Client__c;
											
						quotes.add(qt);
					}
					
					/* Quotation Archive result
					for(Quotes_archive__c result : [Select QuoteId__c, Name, Q.Client__r.Nationality__c, Q.Client__c, Q.Client__r.Name, Q.CreatedByUser__c, Q.CreatedDate__c, Q.EndDate__c, Q.QuoteStatus__c  
											  		FROM Quotes_archive__c Q where CreatedByUser__c = :theUserID and EndDate__c >= :fromDate.Expected_Travel_Date__c and EndDate__c <= :toDate.Expected_Travel_Date__c order by q.CreatedDate__c desc]){
						quotation qt = new quotation();
						qt.name = result.Name;
						qt.quoteId = result.QuoteId__c;
						qt.nationality = result.Client__r.Nationality__c;
						qt.fullName = result.Client__r.Name;
						qt.createdById = result.CreatedByUser__c;
						qt.createdDate = result.CreatedDate__c;
						qt.expiryDate = result.EndDate__c;
						qt.statusId = result.QuoteStatus__c;
						qt.clientId = result.Client__c;
											
						quotes.add(qt);
					} */
					
				}

			}
		
			Set<String> clientIds = new Set<String>();
			
			for(quotation q : quotes)
				for(AgencyWrapper aw: agencyList)
					for(UserWrapper uw : aw.userList)
						if(uw.userID == q.CreatedById){
							clientIds.add(q.clientId);
							if(!uw.quoteList.containsKey(q.clientId)){
								List<quotation> lq = new List<quotation>();								
								lq.add(q);
								uw.quoteList.put(q.clientId, lq);
							} else uw.quoteList.get(q.clientId).add(q);
							//uw.quoteList.add(q);
						}
			
			
			getClientDetails(clientIds);
		}
	
	}
	
		class countQuotes{
			integer allQuotes;
			integer enrolled;
			integer perClient;
		}
	
		
		private void getNumberOfQuotesPerUser(Set<String> userIDs){
			//All Quotes
		 		
			String sql = 'Select createdByid, Count(Name) tot from Quotation__c Q where createdByid in :userIDs ';
				
			if(selectQuoteStatus.size() > 0)
				sql += ' and status__c  in :selectQuoteStatus ';
		
			if(fromDate.Expected_Travel_Date__c != null){
				if (selectedDateOptions == 'co')
					sql += ' and createdDate >= ' + FormatSqlDateTimeIni(fromDate.Expected_Travel_Date__c) + ' ';
				else  sql += ' and expiry_date__c >= ' + FormatSqlDate(fromDate.Expected_Travel_Date__c) + ' ';
			}
			if(toDate.Expected_Travel_Date__c != null){
				if (selectedDateOptions == 'co')
					sql += ' and createdDate <= ' + FormatSqlDateTimeFin(toDate.Expected_Travel_Date__c) + ' ';
				else sql += ' and expiry_date__c <= ' + FormatSqlDate(toDate.Expected_Travel_Date__c) + ' ';
			}
			sql += ' group by createdByid ';
		
			List<quotation> lar = new List<quotation>();
			//List<AggregateResult> lar = Database.query(sql);
				
			/* Quotation result */
			for(AggregateResult result : database.query(sql)){
				quotation qt = new quotation();
				qt.createdById = string.valueOf(result.get('createdByid'));
				qt.totQuotes = Integer.valueOf(result.get('tot'));					
				lar.add(qt);
			}
			
			/*sql = sql.replace('Quotation__c', 'Quotes_archive__c');
			sql = sql.replace('status__c', 'QuoteStatus__c');
			sql = sql.replace('expiry_date__c', 'EndDate__c');
			sql = sql.replace('createdByid', 'CreatedByUser__c');
			sql = sql.replace('createdDate ', 'CreatedDate__c');
			
			 Quotation Archive result
			for(AggregateResult result : database.query(sql)){
				quotation qt = new quotation();
				qt.createdById = string.valueOf(result.get('CreatedByUser__c'));
				qt.totQuotes = Integer.valueOf(result.get('tot'));			
				lar.add(qt);
			} */
		
			//Quotes per client			
			String sqlClient = 'Select createdByid, Client__c,  Count(Id) tot from Quotation__c where createdByid in :userIDs ';
				
			if(selectQuoteStatus.size() > 0)
				sqlClient += ' and status__c  in :selectQuoteStatus ';
		
			if(fromDate.Expected_Travel_Date__c != null){
				if (selectedDateOptions == 'co')
					sqlClient += ' and createdDate >= ' + FormatSqlDateTimeIni(fromDate.Expected_Travel_Date__c) + ' ';
				else  sqlClient += ' and expiry_date__c >= ' + FormatSqlDate(fromDate.Expected_Travel_Date__c) + ' ';
			}	
			if(toDate.Expected_Travel_Date__c != null){
				if (selectedDateOptions == 'co')
					sqlClient += ' and createdDate <= ' + FormatSqlDateTimeFin(toDate.Expected_Travel_Date__c) + ' ';
				else sqlClient += ' and expiry_date__c <= ' + FormatSqlDate(toDate.Expected_Travel_Date__c) + ' ';
			}
			sqlClient += ' group by createdByid, Client__c ';
			
			//List<AggregateResult> lpc = Database.query(sqlClient);
			List<quotation> lpc = new List<quotation>();
			/* Quotation result */
			for(AggregateResult result : database.query(sqlClient)){
				quotation qt = new quotation();
				qt.createdById = string.valueOf(result.get('createdByid'));
				qt.totQuotes = Integer.valueOf(result.get('tot'));			
				qt.clientId= string.valueOf(result.get('Client__c'));	
				lpc.add(qt);
			}
			
			/*sqlClient = sqlClient.replace('Quotation__c', 'Quotes_archive__c');
			sqlClient = sqlClient.replace('status__c', 'QuoteStatus__c');
			sqlClient = sqlClient.replace('expiry_date__c', 'EndDate__c');
			sqlClient = sqlClient.replace('createdByid', 'CreatedByUser__c ');
			sqlClient = sqlClient.replace('createdDate ', 'CreatedDate__c ');
			sqlClient = sqlClient.replace('(Id)', '(QuoteId__c)');
			 Quotation Archive result
			
			system.debug('SQL======>' + sqlClient);
			for(AggregateResult result : database.query(sqlClient)){
				quotation qt = new quotation();
				qt.createdById = string.valueOf(result.get('CreatedByUser__c'));
				qt.totQuotes = Integer.valueOf(result.get('tot'));			
				qt.clientId= string.valueOf(result.get('Client__c'));	
				lpc.add(qt);
			} */
					
		
			//Quotes enrolled
			string sqlEnroll = 'Select createdByid, Count(Name) tot from Quotation__c Q where status__c = \'Enrolled\' and createdByid in :userIDs ';

			if(selectQuoteStatus.size() > 0)
				sqlEnroll += ' and status__c  in :selectQuoteStatus ';
		
			if(fromDate.Expected_Travel_Date__c != null){
				if (selectedDateOptions == 'co')
					sqlEnroll += ' and createdDate >= ' + FormatSqlDateTimeIni(fromDate.Expected_Travel_Date__c) + ' ';
				else  sqlEnroll += ' and expiry_date__c >= ' + FormatSqlDate(fromDate.Expected_Travel_Date__c) + ' ';
			}	
			if(toDate.Expected_Travel_Date__c != null){
				if (selectedDateOptions == 'co')
					sqlEnroll += ' and createdDate <= ' + FormatSqlDateTimeFin(toDate.Expected_Travel_Date__c) + ' ';
				else sqlEnroll += ' and expiry_date__c <= ' + FormatSqlDate(toDate.Expected_Travel_Date__c) + ' ';
			}
			sqlEnroll += ' group by createdByid ';
		
		
			//========mapQuotesPerUser=======================
		
			//All Quotes
		 		
			String sqlStatus = 'Select createdByid, status__c, Count(Name) tot from Quotation__c Q where createdByid in :userIDs ';
							
			if(selectQuoteStatus.size() > 0)
				sqlStatus += ' and status__c  in :selectQuoteStatus ';
			/*if(unsuccessfulReason.size() > 0)
				sql += ' and Unsuccessful__c  in :unsuccessfulReason ';
			*/
			if(fromDate.Expected_Travel_Date__c != null){
				if (selectedDateOptions == 'co')
					sqlStatus += ' and createdDate >= ' + FormatSqlDateTimeIni(fromDate.Expected_Travel_Date__c) + ' ';
				else  sqlStatus += ' and expiry_date__c >= ' + FormatSqlDate(fromDate.Expected_Travel_Date__c) + ' ';
			}	
			if(toDate.Expected_Travel_Date__c != null){
				if (selectedDateOptions == 'co')
					sqlStatus += ' and createdDate <= ' + FormatSqlDateTimeFin(toDate.Expected_Travel_Date__c) + ' ';
				else sqlStatus += ' and expiry_date__c <= ' + FormatSqlDate(toDate.Expected_Travel_Date__c) + ' ';
			}
			sqlStatus += ' group by createdByid, status__c ';
		
			//List<AggregateResult> countQuotesPerStatus = Database.query(sqlStatus);
			List<quotation> countQuotesPerStatus = new List<quotation>();
			/* Quotation result */
			for(AggregateResult result : database.query(sqlStatus)){
				quotation qt = new quotation();
				qt.createdById = string.valueOf(result.get('createdByid'));
				qt.totQuotes = Integer.valueOf(result.get('tot'));			
				qt.statusId =   string.valueOf(result.get('status__c'));				
				countQuotesPerStatus.add(qt);
			}
			
			/*sqlStatus = sqlStatus.replace('Quotation__c', 'Quotes_archive__c');
			sqlStatus = sqlStatus.replace('status__c', 'QuoteStatus__c');
			sqlStatus = sqlStatus.replace('expiry_date__c', 'EndDate__c');
			sqlStatus = sqlStatus.replace('createdByid', 'CreatedByUser__c');
			sqlStatus = sqlStatus.replace('createdDate ', 'CreatedDate__c');
			
			//system.debug('SQL Status==>' + sqlStatus);
			 Quotation Archive result 
			for(AggregateResult result : database.query(sqlStatus)){
				quotation qt = new quotation();
				qt.createdById = string.valueOf(result.get('CreatedByUser__c'));
				qt.totQuotes = Integer.valueOf(result.get('tot'));			
				qt.statusId =   string.valueOf(result.get('QuoteStatus__c'));			
				countQuotesPerStatus.add(qt);
			}*/
			
			Map<String, Map<string, integer>> mapQuotesPerUser = new Map<String, Map<string, integer>>();
			Map<string, integer> quoteItem;
			for(quotation ar : countQuotesPerStatus){
				if(!mapQuotesPerUser.containsKey(ar.createdById)){
					quoteItem = new Map<string, integer>();
					quoteItem.put(ar.statusId,ar.totQuotes);
					mapQuotesPerUser.put(ar.createdById,quoteItem);
				}else if(!mapQuotesPerUser.get(ar.createdById).containsKey(ar.statusId)){					
					mapQuotesPerUser.get(ar.createdById).put(ar.statusId,ar.totQuotes);
				}else{
					integer i =	mapQuotesPerUser.get(ar.createdById).get(ar.statusId) + ar.totQuotes;
					mapQuotesPerUser.get(ar.createdById).put(ar.statusId,i);
				}
			}
		
			//List<AggregateResult> larEnroll = Database.query(sqlEnroll);
			List<quotation> larEnroll = new List<quotation>();
			/* Quotation result */
			for(AggregateResult result : database.query(sqlEnroll)){
				quotation qt = new quotation();
				qt.createdById = string.valueOf(result.get('createdByid'));
				qt.totQuotes = Integer.valueOf(result.get('tot'));							
				larEnroll.add(qt);
			}
			
			/*sqlEnroll = sqlEnroll.replace('Quotation__c', 'Quotes_archive__c');
			sqlEnroll = sqlEnroll.replace('status__c', 'QuoteStatus__c');
			sqlEnroll = sqlEnroll.replace('expiry_date__c', 'EndDate__c');
			sqlEnroll = sqlEnroll.replace('createdByid', 'CreatedByUser__c');
			sqlEnroll = sqlEnroll.replace('createdDate ', 'CreatedDate__c');
			
			 Quotation Archive result 
			for(AggregateResult result : database.query(sqlEnroll)){
				quotation qt = new quotation();
				qt.createdById = string.valueOf(result.get('CreatedByUser__c'));
				qt.totQuotes = Integer.valueOf(result.get('tot'));			
				larEnroll.add(qt);
			}*/
		
			Map<String, countQuotes> quotesPerUser = new Map<String, countQuotes>();
		
			for(quotation ar : lar){
				countQuotes cq = new countQuotes();
				cq.allQuotes = ar.totQuotes;
				cq.enrolled = 0;
				cq.perClient = 0;
				for(quotation are : larEnroll){
					if(ar.createdById == are.createdById){
						cq.enrolled = are.totQuotes;
					}
				}
				for(quotation pc : lpc){
					if(ar.createdById == pc.createdById){
						if(pc.totQuotes > 0){
							cq.perClient ++;
						}
					}
				}
				
				if(!quotesPerUser.containsKey(ar.createdById))
					quotesPerUser.put(ar.createdById,cq );
				else{
					countQuotes sumQ = quotesPerUser.get(ar.createdById);
					sumQ.allQuotes += cq.allQuotes;
					sumQ.enrolled += cq.enrolled;
					sumQ.perClient += cq.perClient;
					quotesPerUser.put(ar.createdById, sumQ );
				}
			}
		
			//Quotes per client
			for(String id : quotesPerUser.keySet())			
				for(AgencyWrapper aw: agencyList)
					for(UserWrapper uw : aw.userList)
						if(uw.userID == id){
							uw.quoteCount = quotesPerUser.get(id).allQuotes;
							uw.quoteCountEnroll = quotesPerUser.get(id).enrolled;
							uw.quotesPerClient = quotesPerUser.get(id).perClient;							
							uw.countQuotesStatus.putAll(mapQuotesPerUser.get(id));
															
						}
		
		
			//COUNT PER AGENCY
			for(AgencyWrapper aw: agencyList)
				for(UserWrapper uw : aw.userList){					
					aw.quoteCount += uw.quoteCount;
					aw.quoteCountEnroll += uw.quoteCountEnroll;
					aw.quotesPerClient += uw.quotesPerClient;
					for(string u:uw.countQuotesStatus.keyset())
						if(!aw.countQuotesStatus.containsKey(u))
							aw.countQuotesStatus.put(u,uw.countQuotesStatus.get(u));
						else aw.countQuotesStatus.put(u,aw.countQuotesStatus.get(u) + uw.countQuotesStatus.get(u));
				}
		}
		
		public list<String> selectQuoteStatus {get{if(selectQuoteStatus == null) selectQuoteStatus = new list<String>(); return selectQuoteStatus;}set;}
		public List<SelectOption> getQuoteStatus() {
			List<SelectOption> options = new List<SelectOption>();
			Schema.DescribeFieldResult F = Quotation__c.fields.Status__c.getDescribe();
			List<Schema.PicklistEntry> P = F.getPicklistValues();
			for (Schema.PicklistEntry lst:P){
				options.add(new SelectOption(lst.getLabel(),lst.getValue()));
			}
			return options;
		}
	
		//Graph
		public class quotesPerAgency{
			public string agency { get; set; }
			public Integer total { get; set; }
			public Integer totalEnroll { get; set; }
			public quotesPerAgency( string agency , Integer total , Integer totalEnroll) {
				this.agency = agency;
				this.total = total;
				this.totalEnroll = totalEnroll;
			}
		}
		public list<quotesPerAgency> getquotesPerAgency(){
			list<quotesPerAgency> lqpc = new list<quotesPerAgency>();
			quotesPerAgency qpc;

			if(agencyList.size() > 0 && (selectedAgency != null && selectedAgency != '')){
				for(AgencyWrapper aw:agencyList)
					for(UserWrapper uw:aw.userList){
						if(selectQuoteStatus.size() == 0)
							qpc = new quotesPerAgency(uw.userName,uw.quoteCount, uw.quoteCountEnroll);
						else qpc = new quotesPerAgency(uw.userName,uw.quoteCount, 0);
						lqpc.add(qpc);
					}
			}else if(selectedAgencyGroup != null && selectedAgencyGroup != ''){
				for(AgencyWrapper aw:agencyList){
					if(selectQuoteStatus.size() == 0)
						qpc = new quotesPerAgency(aw.agencyName,aw.quoteCount, aw.quoteCountEnroll);
					else qpc = new quotesPerAgency(aw.agencyName,aw.quoteCount, 0);
					lqpc.add(qpc);
				}
			}
			return lqpc;
		}
	
	private static string FormatSqlDateTimeIni(date d){
		string day = string.valueOf(d.day());
		string month = string.valueOf(d.month());
		if (day.length() == 1)  
			day = '0'+day;
		if (month.length() == 1)    
			month = '0'+month;
		return d.year()+'-'+month+'-'+day+'T'+System.now().formatGmt('00:00:00')+'Z';
	}
	
	private static string FormatSqlDateTimeFin(date d){
		string day = string.valueOf(d.day());
		string month = string.valueOf(d.month());
		if (day.length() == 1)  
			day = '0'+day;
		if (month.length() == 1)    
			month = '0'+month;
		return d.year()+'-'+month+'-'+day+'T'+System.now().formatGmt('23:59:59')+'Z';
	}
	
	private static  string FormatSqlDate(date d){
		string day = string.valueOf(d.day());
		string month = string.valueOf(d.month());
		if (day.length() == 1)  
			day = '0'+day;
		if (month.length() == 1)    
			month = '0'+month;
		return d.year()+'-'+month+'-'+day;
	}
	


	/*public Integer getQueries() { return Limits.getQueries(); }
	public Integer getLimitQueries() { return Limits.getLimitQueries(); }
	public Decimal getQueryPerc() { return (Decimal.valueOf(getQueries()) / Decimal.valueOf(getLimitQueries())) * 100; }
	
	public Integer getQueryRows() { return Limits.getQueryRows(); }
	public Integer getLimitQueryRows() { return Limits.getLimitQueryRows(); }
	public Decimal getQueryRowsPerc() { return (Decimal.valueOf(getQueryRows()) / Decimal.valueOf(getLimitQueryRows())) * 100; }
	
	public Integer getDmlStatements() { return Limits.getDmlStatements(); }
	public Integer getLimitDmlStatements() { return Limits.getLimitDmlStatements(); }
	public Decimal getDmlStatementsPerc() { return (Decimal.valueOf(getDmlStatements()) / Decimal.valueOf(getLimitDmlStatements())) * 100; }
	
	public Integer getDmlRows() { return Limits.getDmlRows(); }
	public Integer getLimitDmlRows() { return Limits.getLimitDmlRows(); }
	public Decimal getDmlRowsPerc() { return (Decimal.valueOf(getDmlRows()) / Decimal.valueOf(getLimitDmlRows())) * 100; }
	
	public Integer getScriptStatements() { return Limits.getScriptStatements(); }
	public Integer getLimitScriptStatements() { return Limits.getLimitScriptStatements(); }
	public Decimal getScriptStatementsPerc() { return (Decimal.valueOf(getScriptStatements()) / Decimal.valueOf(getLimitScriptStatements())) * 100; }*/

}