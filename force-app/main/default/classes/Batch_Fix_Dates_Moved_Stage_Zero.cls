public with sharing class Batch_Fix_Dates_Moved_Stage_Zero{
	public Batch_Fix_Dates_Moved_Stage_Zero(){
		system.debug('');
	}
}
/*global with sharing class Batch_Fix_Dates_Moved_Stage_Zero implements Database.Batchable<sObject>{
    String groupID;

    global Batch_Fix_Dates_Moved_Stage_Zero(String groupID) {
        this.groupID = groupID;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        List<String> ids = new List<String>{'0039000001p5iQ6AAI','0032v00002bqqEWAAY'};
        String query = 'SELECT ID, Name FROM Contact WHERE RecordType.Name IN (\'Lead\',\'Client\') AND Current_Agency__r.Parent.ID = :groupID';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Contact> ctts) {
        List<String> ids = new List<String>();
        for(Contact ctt : ctts){
            ids.add(ctt.ID);
        }

        Map<String, Destination_Tracking__c> cycles = new Map<String, Destination_Tracking__c>([SELECT ID, Lead_Last_Stage_Cycle__c FROM Destination_Tracking__c WHERE Client__c IN :ids]); 
        List<Client_Stage_Follow_up__c> clicks = [SELECT Stage_Item_Id__c, Stage_Item__c, Client__c, Destination_Tracking__c, Last_Saved_Date_Time__c FROM Client_Stage_Follow_Up__c WHERE Destination_Tracking__c IN :cycles.keySet() AND Stage__c != 'Stage 0' ORDER BY Destination_Tracking__c, Last_Saved_Date_Time__c];

        Map<String, Destination_Tracking__c> toUpdate = new Map<String, Destination_Tracking__c>();
        Destination_Tracking__c cycle = null;
        for(Client_Stage_Follow_up__c click : clicks){
            cycle = cycles.get(click.Destination_Tracking__c);

            if(cycle != null && !toUpdate.containsKey(click.Destination_Tracking__c)){
                cycle.Date_Moved_From_Stage_Zero__c = click.Last_Saved_Date_Time__c;
                toUpdate.put(click.Destination_Tracking__c, cycle);
            }
        }  

        if(!toUpdate.isEmpty()){
            update toUpdate.values();
        }
    }

    global void finish(Database.BatchableContext BC) {}
}*/