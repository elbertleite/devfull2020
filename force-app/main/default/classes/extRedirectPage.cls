public with sharing class extRedirectPage {
	
	public boolean edit {get;set;}
    public  extRedirectPage(ApexPages.StandardController controller) {
        this.acco = controller.getRecord(); 
        
        //used to retrieve the campus course and make possible save data from his dependent object course
        if (acco.getSObjectType() == Campus_Course__c.sObjectType)
            cc = (Campus_Course__c)controller.getRecord();  
        else {
            ep = [Select OwnerId, Owner.Name, User__r.id from Account where id = :controller.getRecord().id];
        }
        
        edit = false;
    }
    
    sObject acco;
    Campus_Course__c cc;

    
    public Account ep {
        get;
        set;
    }
    
    public string redirectPage{
        get{
        redirectPage = ApexPages.currentPage().getParameters().get('pg');
        return redirectPage;}
        set;}
    
    public void Save() {
        try{
            upsert acco;
            edit = false;
        }catch(Exception e){
            ApexPages.addMessages(e);
        }
    }
    
    //Used to save details from course and campusCourse on page inc_course_edit
    public PageReference SaveCampusCourse() {
        
        UPDATE cc.course__r;
        UPDATE cc;
        return RedirectToPage();
    }
    
    //Used to save details from employee and user on page inc_employee_edit
    public PageReference SaveEmployeeUser() {
        if (ep.user__c != null)
            UPDATE ep.user__r;
        UPDATE ep;
        return RedirectToPage();
    }
    
    public string ReturnURL = ApexPages.currentPage().getUrl();
        
         
    
    public string rpg {
        get{
        if((ApexPages.currentPage().getParameters().get('rpg')==null || ApexPages.currentPage().getParameters().get('rpg')=='') && ApexPages.currentPage().getParameters().get('retURL')!=null){
            PageReference pref = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
            rpg = pref.getParameters().get('rpg');          
        }else{
            rpg = ApexPages.currentPage().getParameters().get('rpg');}
        return rpg;
    }
        set;
    }
    
    
    
    public PageReference RedirectToPage()
    {
        string url = '/apex/' + redirectPage;
        
        if(ApexPages.currentPage().getParameters().get('mid')!=null && ApexPages.currentPage().getParameters().get('mid')!='')
            url += '?id=' + ApexPages.currentPage().getParameters().get('mid');
        else
            url += '?id=' + acco.Id + '&idCampus=' + ApexPages.currentPage().getParameters().get('idCampus') + '&currentPage=' + ApexPages.currentPage().getParameters().get('currentPage');
        
        PageReference acctPage = new PageReference(url);
        acctPage.setRedirect(true);
        return acctPage;
    }
    

    
     public void Edit() {

        edit = true;
    }

    public void Cancel() {
       edit = false;
    }
    
    public PageReference del() {
        delete  acco;
        PageReference acctPage = new PageReference('/apex/'+redirectPage);
        acctPage.setRedirect(true);
        return acctPage;
    }
    
    public PageReference delCampusCourse() {
        List<Campus_Course__c> lcc = [Select C.Campus__r.Name, C.Campus__r.Parent.ID from Campus_Course__c C where Campus__c = :acco.id];
        if (lcc.size() > 0)
            delete  lcc;
        delete acco;
        PageReference acctPage = new PageReference('/apex/listSchool');
        acctPage.setRedirect(true);
        return acctPage;
    }
    
    public PageReference delCourse() {
        Campus_Course__c cId = [Select C.Course__r.Id, C.Course__r.name, C.Campus__r.ParentId, C.Campus__r.name  from Campus_Course__c C where Id = :acco.id];
        List<Campus_Course__c> lcc = [Select Id, C.Campus__c, C.Course__c, C.Campus__r.ParentId from Campus_Course__c C where Course__r.Id = : cId.Course__r.Id];
        if (lcc.size() == 1)
            delete [select id from course__c where id = :cId.Course__r.Id];
        delete acco;
        PageReference acctPage = new PageReference('/apex/inc_Courses?id='+cId.Campus__r.ParentId+'&cpID='+cId.Campus__c);
        acctPage.setRedirect(true);
        return acctPage;
    }
    
    
    
//    public static String selectedAgency {get{if(selectedAgency == null) selectedAgency = IPFunctionsGlobal.userDetails.id; return selectedAgency;}set;}
    private Set<String> agenciesSet = new Set<String>();
    public List<SelectOption> agencyOptions {
        get{
            /*if(agencyOptions == null){
                agencyOptions = new List<SelectOption>();
                
                for(AgencyGroupToAgency__c ag : [Select A.Agency__c, Agency__r.Name from AgencyGroupToAgency__c A order by Agency__r.Name]){
                    if(!agenciesSet.contains(ag.Agency__r.Name)){
                        agenciesSet.add(ag.Agency__r.Name);
                        agencyOptions.add(new SelectOption(ag.Agency__c, ag.Agency__r.Name));
                    }
                }
            }*/
            return agencyOptions;
        }
        set;
    }
    
    public void searchUsers(){
        userOptions = null;
    }
    public String selectedUser {get{if(selectedUser == null) selectedUser = ''; return selectedUser;}set;}
    public List<SelectOption> userOptions {
        get{
            if(userOptions == null){
                userOptions = new List<SelectOption>();
                userOptions.add(new SelectOption('', '--Select User--'));
            
                id recoTypeEmployee = [Select Id from RecordType where name = 'Employee' and IsActive = true limit 1].id;
                list<id> employees = new List<id>();
                    
                    
                /*if(selectedAgency != null || selectedAgency != ''){
                    List<Account> lUsers = [Select id, user__c, Name from Account where User__r.IsActive = true and id in :employees and Agency__pc = :selectedAgency order by name];
                    
                    for(Account ac : lUsers)
                        userOptions.add(new SelectOption(ac.user__c, ac.Name));             
                        
                }*/
            }
            return userOptions;
        }
        set;
    }
    
    /*public void transferOnwerShip(){
        
        if(selectedAgency != null && selectedAgency != '' && selectedUser != null && selectedUser != ''){
            
            Account originalOwner = [select id, Full_Name__pc, Agency__pr.Name, PersonEmail from Account where User__c = :UserInfo.getUserId()];
            
            ep.OwnerId = selectedUser;
            if(ep.Agency__pc != selectedAgency)
                ep.Agency__pc = selectedAgency;
            
            UPDATE ep;
            
            
            Account transferredTo = [select id, Full_Name__pc, PersonEmail, Agency__pr.Name from Account where User__c = :selectedUser];
            
            emailAlert(transferredTo.PersonEmail, originalOwner.Full_Name__pc, originalOwner.Agency__pr.Name, ep.Full_Name__pc, originalOwner.PersonEmail, true);
            
            ep = [Select OwnerId, Owner.Name, FirstName, LastName, User__r.id, RecordType.Name from Account where id = :ep.id];
            
            
            if(ep.RecordType.Name == 'Lead'){
                ep.Lead_Assignment__c = 'Assigned';
                ep.Lead_Assignment_By__c = originalOwner.Id;
                ep.Lead_Assignment_On__c = System.now();
                ep.Lead_Assignment_To__c = transferredTo.id;    
                update ep;
            }
            
            
            
        }
        
    }
    
    
    
    
    
    public void TakeOnwerShip() {
        ep.OwnerId = UserInfo.getUserId();
        Account userAgency = [select Agency__pc, Owner.Email, Full_Name__pc, PersonEmail from Account where user__c = :ep.OwnerId];
        Account ownerEmail = [select Owner.Email from Account where id = :acco.id];
        Account taking = [Select id, Agency__pr.Name from Account where User__c = :UserInfo.getUserId()];
        if (userAgency != null){
            //emailAlert('elbert.leite@informationplanet.com.au');
            emailAlert(ownerEmail.Owner.Email, UserInfo.getFirstName() +' '+ UserInfo.getLastName(),taking.Agency__pr.Name, userAgency.Full_Name__pc, userAgency.PersonEmail, false);
            System.debug('---> ep.Agency__pc: ' + ep.Agency__pc);
            System.debug('---> userAgency.Agency__pc: ' + userAgency.Agency__pc);
            if (userAgency.Agency__pc != null && ep.Agency__pc != userAgency.Agency__pc)
                ep.Agency__pc = userAgency.Agency__pc;
        }
        
        if(ep.RecordType.Name == 'Lead'){
            ep.Lead_Assignment__c = 'Assigned';
            ep.Lead_Assignment_By__c = taking.id;
            ep.Lead_Assignment_On__c = System.now();
            ep.Lead_Assignment_To__c = taking.id;   
            
        }
        
        
        UPDATE ep;
        //ep = [Select OwnerId, Owner.Name, FirstName, LastName, User__r.id from Account where id = :ep.id];
        
        
        
        
    }
    
    private void emailAlert(string email, string userName, String userAgency, string clientName, string replyTo, boolean isTransfer){
        
        account client = [Select full_name__pc from Account where id  = :ApexPages.currentPage().getParameters().get('id') limit 1];
                
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        // Strings to hold the email addresses to which you are sending the email.  
    
        String[] toAddresses = new String[] {email}; 
        //String[] ccAddresses = new String[] {'smith@gmail.com'};
  

        // Assign the addresses for the To and CC lists to the mail object.  
    
        mail.setToAddresses(toAddresses);
        System.debug('---> toAddresses: ' + toAddresses);

        // Specify the address used when the recipients reply to the email.   
    
        mail.setReplyTo(replyTo);
        System.debug('---> replyTo: ' + replyTo);
        // Specify the name used as the display name.  
    
        mail.setSenderDisplayName('Salesforce Support');

        // Specify the subject line for your email address.  
        if(isTransfer)
            mail.setSubject('Client ownership has been transferred' );
        else
            mail.setSubject('Client ownership has been taken' );

        // Set to True if you want to BCC yourself on the email.  
    
        mail.setBccSender(false);

        // Optionally append the salesforce.com email signature to the email.  
    
        // The email address of the user executing the Apex Code will be used.  
    
        mail.setUseSignature(false);
        
        String signature = '';
        if(userDetails.Signature_URL__c != null && userDetails.Signature_URL__c != '' )
            signature = '<br/><br/> <img src="'+userDetails.Signature_URL__c+'" />';
    
        // Specify the text content of the email.  
        if(isTransfer){
            mail.setPlainTextBody(userName + ' from ' + userAgency + ' has transferred the ownership of <b>' + client.full_name__pc  + '</b> to you.' + signature );
            mail.setHtmlBody(userName + ' from ' + userAgency + ' has transferred the ownership of  <b>' + client.full_name__pc  + ' </b> to you.' + signature);
        } else {
            mail.setPlainTextBody(userName + ' from ' + userAgency + ' has taken the ownership of  <b>' + client.full_name__pc  + '</b> from you.' + signature );
            mail.setHtmlBody(userName + ' from ' + userAgency + ' has taken the ownership of  <b>' + client.full_name__pc  + '</b> from you.' + signature );
        }

        // Send the email you have created.  
    
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });   
        
        
    }
    */
    //return the user's agency 
    public  User userDetails{
        get{
        //if (userDetails == null)
            //userDetails = [select Signature_URL__c from User where id = :UserInfo.getUserId()];
        return userDetails;
    }
        set;
    }
    
    
    
    
    
    
   /* public void disableAvailability(){
        
        try {
            
            String campusID = ApexPages.currentPage().getParameters().get('campusID');
            if(campusID != null && campusID != '')
                delete [Select id from Supplier__c where Supplier__c = :campusID];
            
        } catch (Exception e){
            System.debug('@@@ : ' + e.getMessage());
        }
        
    }*/
    
    
}