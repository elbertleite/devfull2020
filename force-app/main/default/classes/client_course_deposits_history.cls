public with sharing class client_course_deposits_history {
    
    
    public list<depositDetails> allDeposits {get;set;}
    public String contactId {get;set;}
    
    public client_course_deposits_history(ApexPages.StandardController controller){
    	contactId = controller.getId();
    	loadDeposits();
    }
    
	
    public boolean hasCancelReqUndo{get{

    	try{
	    	client_course__c cru = [SELECT Id FROM client_course__c WHERE client__c = :contactId AND isCancelRequest__c = true and isCancelled__c = true order by createdDate limit 1];
    		return true;
    	}
		catch(exception e){
			return false;
		}

    }set;}

    
    private void loadDeposits(){
    	allDeposits = new list<depositDetails>();
    	depositDetails dd;
    	
    	for(client_course__c cc : [SELECT Deposit_Description__c, Deposit_Total_Used__c, Deposit_Total_Value__c,  Deposit_Type__c,  Deposit_Total_Refund__c, createdDate, 
    	
									(SELECT Id, Value__c, Received_By__r.Name, Received_On__c, Received_By_Agency__r.Name, Payment_Type__c, Date_Paid__c, Confirmed_By__c, CurrencyIsoCode__c,isInstalment_Refund__c, isClient_Refund__c, createdBy.Name, createdDate, Description__c, Paid_to_Client_By__r.Name, Refund_Requested_By_Agency__r.Name, Refund_Requested_By_Department__r.Name, Paid_to_Client_On__c, Paid_to_Client_By_Department__r.Name, Paid_to_Client_By_Agency__r.Name, Refund_Cancelled_By__r.Name, Refund_Cancelled_By_Agency__r.Name, Refund_Cancelled_By_Department__r.Name, Refund_Cancelled_On__c, Refund_Cancelled_Description__c, Agency_Currency_Value__c, Agency_Currency__c, Not_Refunded__c FROM client_course_payments_deposit__r)
									
									FROM client_course__c WHERE client__c = :contactId AND isDeposit__c = true and Deposit_Total_Available__c <= 0 order by createdDate ]){
			
			dd = new depositDetails();
			dd.deposit = cc;
			
			for(client_course_instalment_payment__c ccip : cc.client_course_payments_deposit__r){
 			
				if(!ccip.isClient_Refund__c)
					dd.payments.add(ccip);
				else
					dd.refunds.add(ccip);

 			}//end for		
					
			allDeposits.add(dd);		
							
		}//end for
    }
    
    
    
    public class depositDetails{
    	public client_course__c deposit {get;set;}
    	public list<client_course_instalment_payment__c> payments {get{if(payments==null) payments = new list<client_course_instalment_payment__c>(); return payments;}set;}
    	public list<client_course_instalment_payment__c> refunds {get{if(refunds==null) refunds = new list<client_course_instalment_payment__c>(); return refunds;}set;}
    	
    	/**public depositDetails(client_course__c deposit, list<client_course_instalment_payment__c> payments, list<client_course_instalment_payment__c> refunds){
    	
    		this.deposit = deposit;
    		this.payments = payments;
    		this.refunds = refunds;
    		
    	}**/
    	
    }
   
    
}