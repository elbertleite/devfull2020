public without sharing class commissions_pending_pds_confirmations {

	public String cancelReason {get;set;}
	public boolean showError {get{if(showError == null) showError = false; return showError;}set;}
	public list<schoolResult> invResult {get;set;}
	public client_course_instalment__c cancelInst {get{if(cancelInst==null) cancelInst = new client_course_instalment__c(); return cancelInst;}set;}
	public Integer totSendInvoice {get;set;}
	public Integer totToRequest {get;set;}
	public Integer totToConfirm {get;set;}
	public integer totResult {get;set;}
	private string cancelInstalment {get;set;}
	private string invoiceId {get;set;}
	private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}


	private map<Integer, list<ID>> pageIds;
    private map<String, list<String>> bo {get;set;}
	public Integer currentPage {get{if(currentPage==null) currentPage = 1; return currentPage;}set;}
	public list<Integer> totPages {get;set;} 
	public list<SelectOption> backOfficeOptions{get; set;}
	public string selectedBackOffice{get; set;}

	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}

	//Contructor
	public commissions_pending_pds_confirmations() {
		// bo = ff.findBOAgenciesPerService(currentUser.Contact.AccountId, new list<String>{'PDS_PCS_PFS_Chase'});
		backOfficeOptions = FinanceFunctions.retrieveBackOfficeOptions();

		String pBO = ApexPages.currentPage().getParameters().get('bk') != null && ApexPages.currentPage().getParameters().get('bk') != '' ? ApexPages.currentPage().getParameters().get('bk') : currentUser.Contact.AccountId;
		
		for(SelectOption so : backOfficeOptions)
			if(selectedBackOffice == null || so.getValue() == pBO)
				selectedBackOffice = so.getValue();

		findBackoffice();	
      	checkFilters();
		searchPendingRequests();
	}

	private void findBackoffice(){
		bo = ff.findBOAgenciesPerService(selectedBackOffice, new list<String>{'PDS_PCS_PFS_Chase'});
	}

	public void changeBackOffice(){
		findBackoffice();
		allFilters = ff.pdsFlowBOFilters(bo);
		countryOptions = allFilters.get('allCountries');
		if(countryOptions != null && countryOptions.size() > 0){
			selectedCountry = countryOptions[0].getValue();
			changeCountry();
		}
		else{
			schGroupOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
			schoolOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
		}
	} 
	//Find Pending Confirmation
	public void searchPendingRequests(){
		if(currentUser.Finance_Backoffice_User__c &&  (selectedSchoolGP!=null && selectedSchoolGP != 'none') || requestNumber.length()>0){
			invResult = new list<schoolResult>();
			totResult = 0;

			String sql = 'SELECT Id ';

			if(requestNumber.length()>0){
				requestNumber = requestNumber.replace(' ', '');
				list<String> allRequest = new list<String>(requestNumber.split(','));
				sql += ' FROM Invoice__c WHERE Request_Confirmation_Number__c in ( \''+ String.join(allRequest, '\',\'') + '\' ) AND Requested_by_Agency__c = \'' + selectedBackOffice + '\' AND Country__c = :selectedCountry AND isPDS_Confirmation__c = TRUE AND Request_Confirmation_Completed_On__c = NULL ';
			}

			else{
				if(searchName.length()>0){
					sql += ', (SELECT Id FROM client_course_instalments_pds_conf_inv__r WHERE Client_Course__r.Client__r.Name LIKE  \'%'+ searchName.trim() +'%\' AND isCancelled__c = false AND PDS_Confirmed_On__c = NULL )';
				}

				sql+=' FROM Invoice__c WHERE Requested_by_Agency__c = \'' + selectedBackOffice + '\' AND Country__c = :selectedCountry AND isPDS_Confirmation__c = TRUE AND Request_Confirmation_Completed_On__c = NULL ';

				if(selectedSchool!= null && selectedSchool != 'none' && selectedSchool != 'all')
					sql+=' AND ( School_Id__c =  \''+ selectedSchoolGP + '\' OR School_Id__c =  \''+ selectedSchool + '\')'; //Agency

				else if(selectedSchoolGP != 'all'){
					list<String> ids = new list<String>{selectedSchoolGP};
					for(SelectOption s : schoolOptions)
						ids.add(s.getValue());

					sql+=' AND School_Id__c IN ( \''+ String.join(ids, '\',\'') + '\' ) '; //Agency
				}

				if(selectedPayment=='firstPayment')
					sql += ' AND hasFirstPayment__c = TRUE '; //First Payments
				else if(selectedPayment=='repayments')
					sql += ' AND hasFirstPayment__c = FALSE '; //Repayments

				sql+= ' AND ((Due_Date__c >= '+ IPFunctions.FormatSqlDateIni(dates.Commission_Due_Date__c); //Commission Due Date

				sql+= ' AND Due_Date__c <= '+ IPFunctions.FormatSqlDateFin(dates.Commission_Paid_Date__c); //Commission Due Date

				if(showOverdue)
					sql+= ' ) OR Due_Date__c = NULL OR Due_Date__c < ' + System.now().format('yyyy-MM-dd') + ' )';//Overdue
				else sql+= ' ))';

				sql += ' order by  School_Name__c, Due_Date__c NULLS First ' ;
			}
			

			system.debug('sql pending confirmation ==>' + sql);


			list<Invoice__c> preResult = Database.query(sql); //used to double filter is there is a Name search
			list<Invoice__c> result;

			if(searchName.length()>0){
				result = new list<Invoice__c>();
				for(Invoice__c i : preResult)
					if(i.client_course_instalments_pds_conf_inv__r.size()>0)
						result.add(i);
			}else{
				result = preResult;
			}

			totResult = result.size();

			/** C R E A T E 	P A G I N A T I O N **/
			Integer pageSize = 10;
			Integer counter = 1;
			Integer page = 1;
			list<ID> instId = new list<ID>();
			totPages = new list<Integer>();
			pageIds = new map<Integer, list<ID>>();


			for(Invoice__c i : result){
				instId.add(i.id);
				if(counter == pageSize){
					pageIds.put(page, instId);
					instId = new list<ID>();
					totPages.add(page);
					page++;
					counter = 1;
				}else
					counter++;
			}//end for


			//add the remaining ids
			if(counter > 1){
				pageIds.put(page, instId);
				totPages.add(page);
			}

			if(result.size()>0)
				findPaginationItems();
			// else
				// invResult.clear();

			list<String> boAgencies = bo.get(selectedCountry);
			agenciesUnderBackoffice = '';
			list<String> agenciesBO = new list<String>(); 
			
			integer boCount = 0;
			for(String agBo : boAgencies){
				
				list<String> ag = agBo.split(';-;');
				if(boCount>0)
					agenciesUnderBackoffice += ', ' + ag[1];
				else
					agenciesUnderBackoffice += ag[1];
				agenciesBO.add(ag[0]);
				boCount++;
			}//end for

			//totToRequest PARAMS ==> (map<String, list<String>> bo, String country, String school, String clientName, Date iniDate, Date finDate)	
			totToRequest = ff.totPdsToRequest(agenciesBO, selectedCountry, selectedSchoolGP, selectedSchool, searchName, dates.Commission_Due_Date__c, dates.Commission_Paid_Date__c);

			totSendInvoice = ff.totInstSendInv(agenciesBO, selectedCountry, selectedSchoolGP, selectedSchool, searchName, dates.Commission_Due_Date__c, dates.Commission_Paid_Date__c);

			totToConfirm = ff.totConfCommInv(selectedCountry, selectedSchoolGP, selectedSchool, searchName, dates.Commission_Due_Date__c, dates.Commission_Paid_Date__c, selectedBackOffice, schoolOptions);

		}
		else{
			invResult = new list<schoolResult>();
			totSendInvoice = 0;
			totToRequest = 0;
			totToConfirm = 0;
		}
	}

	public string agenciesUnderBackoffice{get{if(agenciesUnderBackoffice == null) agenciesUnderBackoffice = ''; return agenciesUnderBackoffice;} set;} 

	/** G E T 	P A G E 	I T E M S **/
	public void findPaginationItems(){
		invResult = new list<schoolResult>();

		String paramPage = ApexPages.CurrentPage().getParameters().get('page');

		if(paramPage!=null && paramPage!=''){
			currentPage = integer.valueOf(paramPage);
			ApexPages.CurrentPage().getParameters().remove('page');
		}

		if(currentPage> totPages.size())
			currentPage = totPages.size();

		list<Id> allIds = pageIds.get(currentPage);


		String sql = 'SELECT Id, Request_Confirmation_Number__c, Sent_Email_To__c, Sent_On__c, Status__c, Due_Date__c, Total_Emails_Sent__c, Total_Refused__c, Total_Unsure__c, School_Name__c,'+
										'	isSchool_to_Answer__c, 	Request_Completed_by_School_On__c, School_Id__c, Invoice_Activities__c, Total_Instalments_Requested__c, ';

		if(searchName.length()>0){
			sql += ' (SELECT ID, Commission_Due_Date__c, Due_Date__c, Client_Course__r.Client__c, Client_Course__c, Client_Course__r.Client__r.Name, Client_Course__r.Client__r.Owner__r.Name, Number__c, Split_Number__c, Tuition_Value__c, isInstalment_Amendment__c, Client_Course__r.Course_Name__c, Client_Course__r.School_Name__c, School_PDS_Notes__c, School_Request_Confirmation_Status__c, PDS_Confirmed__c, instalment_activities__c, isSelected__c,  Extra_Fee_Value__c, Commission__c, Commission_Value__c, Commission_Tax_Value__c, Commission_Tax_Rate__c, client_course__r.Commission_Tax_Name__c, client_course__r.Commission_Type__c, PDS_Confirmed_By__c, PDS_Confirmed_On__c, isRefused_PDS_Action_Taken__c, client_course__r.CurrencyIsoCode__c FROM client_course_instalments_pds_conf_inv__r WHERE Client_Course__r.Client__r.Name LIKE  \'%'+ searchName +'%\' AND isCancelled__c = false AND PDS_Confirmed_On__c = NULL order by Due_Date__c) ' ;
		}
		else{
			sql += ' (SELECT ID, Commission_Due_Date__c, Due_Date__c, Client_Course__r.Client__c, Client_Course__c, Client_Course__r.Client__r.Name, Client_Course__r.Client__r.Owner__r.Name, Number__c, Split_Number__c, Tuition_Value__c, isInstalment_Amendment__c, Client_Course__r.Course_Name__c, Client_Course__r.School_Name__c, School_PDS_Notes__c, School_Request_Confirmation_Status__c, PDS_Confirmed__c, instalment_activities__c, isSelected__c,  Extra_Fee_Value__c, Commission__c, Commission_Value__c, Commission_Tax_Value__c, Commission_Tax_Rate__c, client_course__r.Commission_Tax_Name__c, client_course__r.Commission_Type__c, PDS_Confirmed_By__c, PDS_Confirmed_On__c,isRefused_PDS_Action_Taken__c, client_course__r.CurrencyIsoCode__c FROM client_course_instalments_pds_conf_inv__r order by Due_Date__c) ' ;
		}


		sql += ' FROM Invoice__c  WHERE ID in  ( \''+ String.join(allIds, '\',\'') + '\' ) order by School_Name__c, Due_Date__c NULLS First';



		schoolResult schR;
		invoice inv;
		map<string, schoolResult> mapResult = new map<string,schoolResult>();
		list<instalmentActivity> invActivities;
		list<SelectOption> opt;
		list<SelectOption> instOptions;
		for(Invoice__c i : Database.query(sql)){
			invActivities = new list<instalmentActivity>();
			opt = new list<SelectOption>();
			opt.add(new SelectOption('none', '-- Select Action --'));

			instOptions = new list<SelectOption>();
			instOptions.add(new SelectOption('none', '-- Select Action --'));

			list<client_course_instalment__c> showInst = new list<client_course_instalment__c>();

			/** Invoice Activities **/
			if(i.Invoice_Activities__c!=null && i.Invoice_Activities__c != ''){

				List<String> allAc = i.Invoice_Activities__c.split('!#!');

				for(Integer ac = (allAc.size() - 1); ac >= 0; ac--){//activity order desc

					instalmentActivity instA = new instalmentActivity();
					List<String> a = allAc[ac].split(';;');

					instA.acType = a[0];
					instA.acTo = a[1];
					instA.acSubject = a[2];
					if(instA.acType=='SMS') // saved date in system mode
						instA.acDate = new Contact(Lead_Accepted_On__c = Datetime.valueOfGmt(a[3]));
					else
						instA.acDate = new Contact(Lead_Accepted_On__c = Datetime.valueOf(a[3]));
					instA.acStatus = a[4];
					instA.acError = a[5];
					instA.acFrom = a[6];

					invActivities.add(instA);
				}
			}

		//Invoice Options
		if(i.isSchool_to_Answer__c && i.Request_Completed_by_School_On__c == null){//School haven't completed the form yet
			opt.add(new SelectOption('sendEmail', 'Send Email'));
			opt.add(new SelectOption('cancelReq', 'Cancel Request'));
			inv = new Invoice(i, i.client_course_instalments_pds_conf_inv__r, opt, new list<SelectOption>(), invActivities);
		}

		else if(i.isSchool_to_Answer__c && i.Request_Completed_by_School_On__c != null){//School Completed the Form

			instOptions.add(new SelectOption('editInst', 'Edit Instalment'));
			instOptions.add(new SelectOption('noCommission', 'No Commission Claim'));
			instOptions.add(new SelectOption('cancelPds', 'PDS Not Received'));
			// instOptions.add(new SelectOption('cancelCourse', 'Cancel Course'));

			//Get refused Instalments
			for(client_course_instalment__c cci : i.client_course_instalments_pds_conf_inv__r)
				if(!cci.PDS_Confirmed__c && !cci.isRefused_PDS_Action_Taken__c)
				showInst.add(cci);

				inv = new Invoice(i, showInst, opt, instOptions, invActivities);
			}
			else if(!i.isSchool_to_Answer__c){//EHF will anwser the form
				opt.add(new SelectOption('sendEmail', 'Send Email'));
				

				instOptions.add(new SelectOption('editInst', 'Edit Instalment'));
				instOptions.add(new SelectOption('noCommission', 'No Comm. Claim'));
				// instOptions.add(new SelectOption('cancelCourse', 'Cancel Course'));
				instOptions.add(new SelectOption('cancelPds', 'PDS Not Received'));
				instOptions.add(new SelectOption('sendToRequest', 'Send Back to PDS Request'));

				boolean allowToCancel = true;

				for(client_course_instalment__c cci : i.client_course_instalments_pds_conf_inv__r)
					if(cci.School_Request_Confirmation_Status__c == NULL)
						showInst.add(cci);
					else
						allowToCancel = false;

				if(allowToCancel)
					opt.add(new SelectOption('cancelReq', 'Cancel Request'));

				inv = new Invoice(i, showInst, opt, instOptions, invActivities);
			}

			/** G R O U P 		P E R 		S C H O O L **/
			if(!mapResult.containsKey(i.School_Id__c))
				mapResult.put(i.School_Id__c, new schoolResult(i.School_Id__c, i.School_Name__c, inv, (i.isSchool_to_Answer__c && i.Request_Completed_by_School_On__c != null)));
			else
				mapResult.get(i.School_Id__c).addInvoice(inv, (i.isSchool_to_Answer__c && i.Request_Completed_by_School_On__c != null));
			//invResult.add(inv);

		}//end for

		for(String sch : mapResult.keySet())
			invResult.add(mapResult.get(sch));

	}

	//PDS Not Received Request
	public void cancelRequest(){
		String toCancel = ApexPages.CurrentPage().getParameters().get('inv');
		String emailHifyId = '' + Datetime.now().getTime();
		Savepoint sp;
		try{
			sp = Database.setSavepoint();

			String activity = 'PDS Request Confirmation Cancelled;;-;;-;;' + system.now().format('yyyy-MM-dd HH:mm:ss') + ';;error;;-;;' + currentUser.Name + '!#!';
			Invoice__c invEmail;
			for(schoolResult schr : invResult)
				for(invoice i : schr.invoices)
					if(i.inv.id == id.valueOf(toCancel)){
						invEmail = i.inv;
						//Remove PDS Status for all Instalments
						for(client_course_instalment__c cci : i.inv.client_course_instalments_pds_conf_inv__r){
							cci.isSelected__c = false;
							cci.School_Request_Confirmation_Status__c = null;
							cci.PDS_Requested_By__c = null;
							cci.PDS_Requested_On__c = null;
							cci.Request_Commission_Invoice__c = null;
							cci.instalment_activities__c += activity;
						}//end for

					update i.inv.client_course_instalments_pds_conf_inv__r;
					deleteObj(i.inv);
				}


			//Send email to School and Invoice "owner"
			mandrillSendEmail mse = new mandrillSendEmail();
			mandrillSendEmail.messageJson email_md = new mandrillSendEmail.messageJson();
			String body = label.Cancel_Request_Confirmation;

			email_md.setMetadata('hify_id', emailHifyId);

			email_md.setFromEmail(currentUser.Contact.Email);
			email_md.setFromName(currentUser.Name);
			String emailService = IpFunctions.getS3EmailService();
			email_md.setSubject(invEmail.Request_Confirmation_Number__c + ' - Request Commission Confirmation CANCELLED');
			email_md.setTo(invEmail.Sent_Email_To__c, '');
			email_md.setTag(invEmail.Sent_Email_To__c);
			email_md.setCc(currentUser.Contact.Email, '');
			email_md.setHtml(body);

			// PageReference pr = Page.payment_school_request_confirmation;
			// pr.getParameters().put('i', string.valueOf(invEmail.id));
			// if(!Test.isRunningTest()){
			// 	blob fileBody = pr.getContentAsPDF();
			// 	email_md.setAttachment('application/pdf', invEmail.Request_Confirmation_Number__c + ' - PDS Confirmation Completed.pdf', fileBody);
			// }
			mse.sendMail(email_md);
			blob pdfFile;
			EmailToS3Controller s3 = new EmailToS3Controller();
			DateTime currentTime = DateTime.now();
			String myDate = currentTime.format('dd-MM-yyyy HH:mm:ss');

			s3.reqConfirmEmailS3(Blob.valueof(body), pdfFile, invEmail.Request_Confirmation_Number__c + ' - PDS Confirmation CANCELLED', invEmail.School_Id__c, myDate, UserInfo.getUserId(), invEmail.Request_Confirmation_Number__c,invEmail.Sent_Email_To__c,emailHifyId);
		}
		catch(Exception e){
			Database.rollback(sp);
			system.debug('Error===>' + e.getMessage());
			system.debug('Error Line===>' + e.getLineNumber());
		}
		finally{
			ApexPages.CurrentPage().getParameters().remove('inv');
		}

		searchPendingRequests();
	}

	//Confirm selected PDS
	public void confirmPds(){
		String invoiceId = ApexPages.CurrentPage().getParameters().get('inv');

		Savepoint sp;
		try{
			sp = Database.setSavepoint();
			for(schoolResult schr : invResult)
				for(invoice i : schr.invoices)
					if(i.inv.id == invoiceId){
						boolean hasInstalmentLeft = false;
						for(client_course_instalment__c cci : i.instalments){

							if(cci.isSelected__c){
								cci.isSelected__c = false;
								cci.PDS_Confirmed__c = true;
								cci.PDS_Confirmed_On__c = system.now();
								cci.PDS_Confirmed_By__c = UserInfo.getUserId();
								cci.School_Request_Confirmation_Status__c ='Confirmed';
								cci.instalment_activities__c += 'Request Confirmation Completed by School ;;-;;' + 'PDS confirmed by the school' +';;' + Datetime.now().format('yyyy-MM-dd HH:mm:ss') + ';;sent;;-;;' + currentUser.Name + '!#!';
							}
							else{
								hasInstalmentLeft = true;
							}
						}//end for

					update i.instalments;

					//If all instalments are confirmed close request
					if(!hasInstalmentLeft){
						closeConfRequest(i.inv);
					}
					break;
				}
			searchPendingRequests();
		}
		catch(Exception e){
			system.debug('Error===>' + e.getMessage());
			system.debug('Error Line ===>' + e.getLineNumber());
			Database.rollback(sp);
		}
		finally{
			ApexPages.CurrentPage().getParameters().remove('inv');
		}
	}

	//Close Confirmation Request
	private void closeConfRequest(Invoice__c inv){
		integer totConfirmed = 0;
		integer totRefused = 0;
		integer totUnsure = 0;

		inv.Request_Confirmation_Completed_On__c = system.now();
		inv.Request_Confirmation_Completed_by__c = UserInfo.getUserId();

		//Check Result of all instalments from invoice
		for(client_course_instalment__c cci : [SELECT Id, School_Request_Confirmation_Status__c
																						FROM client_course_instalment__c WHERE Request_Commission_Invoice__c = :inv.id]){

			if(cci.School_Request_Confirmation_Status__c =='Confirmed')
				totConfirmed++;
			else if (cci.School_Request_Confirmation_Status__c =='Refused')
				totRefused ++;
			else if (cci.School_Request_Confirmation_Status__c =='Unsure')
				totUnsure ++;
		}//end for

		inv.Total_Confirmed__c = totConfirmed;
		inv.Total_Refused__c = totRefused;
		inv.Total_Unsure__c = totUnsure;
		inv.Status__c = 'Completed';

		update inv;
	}

	//No Commission Claim
	public void noCommissionClaim(){
		boolean hasInstalmentLeft = false;
		showError = false;
		Savepoint sp;
		try{
			ff.noCommissionClaim(cancelInstalment, cancelInst.No_Commission_Reason__c, cancelReason, true);

			//Check if the Request has pending instalments
			for(schoolResult schr : invResult)
				for(invoice i : schr.invoices)
					if(i.inv.id == invoiceId){
						for(client_course_instalment__c cci : i.instalments){
							if(cci.id!=cancelInstalment && !cci.PDS_Confirmed__c && !cci.isRefused_PDS_Action_Taken__c){
								hasInstalmentLeft = true;
								break;
							}
						}//end for instalments
						if(!hasInstalmentLeft)
							closeConfRequest(i.inv);
						break;
					}

			searchPendingRequests();
			showError = true;
			cancelReason = '';
			cancelInst.No_Commission_Reason__c = null;
		}
		catch(Exception e){
			system.debug('Error===>' + e.getMessage());
			system.debug('Error Line ===>' + e.getLineNumber());
			Database.rollback(sp);
		}
	}

	//Cancel Payment
	public void setIdCancel(){
		cancelInstalment = ApexPages.currentPage().getParameters().get('cancelId');
		invoiceId = ApexPages.currentPage().getParameters().get('invId');

		ApexPages.currentPage().getParameters().remove('cancelId');
		ApexPages.currentPage().getParameters().remove('invId');
	}

	public void cancelPayment(){
		Savepoint sp;
		boolean hasInstalmentLeft = false;
		showError = false;
		try{
			ff.cancelPayment(new list<String>{cancelInstalment}, cancelInst.Cancel_Payment_Reason__c, cancelReason, true);

			//Check if the Request has pending instalments
			for(schoolResult schr : invResult)
				for(invoice i : schr.invoices)
					if(i.inv.id == invoiceId){
						for(client_course_instalment__c cci : i.instalments){
							if(cci.id!=cancelInstalment && !cci.PDS_Confirmed__c && !cci.isRefused_PDS_Action_Taken__c){
								hasInstalmentLeft = true;
								break;
							}
						}//end for instalments
						if(!hasInstalmentLeft)
							closeConfRequest(i.inv);
						break;
					}

			cancelInst.Cancel_Payment_Reason__c = null;
			cancelReason = '';
			searchPendingRequests();
			showError = true;
		}
		catch(Exception e){
			system.debug('Error===>' + e.getMessage());
			system.debug('Error Line ===>' + e.getLineNumber());
			Database.rollback(sp);
		}
	}


	//Send Back to Request Confirmation
	public void sendBackToRequest(){
		Savepoint sp;
		try{
			sp = Database.setSavepoint();
			String instId = ApexPages.currentPage().getParameters().get('inst');
			String invId = ApexPages.currentPage().getParameters().get('invId');

			for(schoolResult schr : invResult)
				for(invoice i : schr.invoices)
					if(i.inv.id == invId){
						String activity = 'Sent back to Request Confirmation;;-;;' + 'Original Request Confirmation - '+ i.inv.Request_Confirmation_Number__c +';;' + Datetime.now().format('yyyy-MM-dd HH:mm:ss') + ';;sent;;-;;' + currentUser.Name + '!#!';
						boolean hasInstalmentLeft = false;
						for(client_course_instalment__c cci : i.instalments){

							if(cci.id== instId){
								cci.isSelected__c = false;
								cci.instalment_activities__c += activity;
								cci.School_Request_Confirmation_Status__c = null;
								cci.PDS_Confirmation_Invoice__c = null;
								cci.status__c = 'Sent back to request';
								cci.PDS_Requested_By__c = null;
								cci.PDS_Requested_On__c = null;
							}
							else{
								hasInstalmentLeft = true;
							}
						}//end for

					update i.instalments;

					//If all instalments are confirmed close request
					if(!hasInstalmentLeft){
						closeConfRequest(i.inv);
					}
					break;
				}
			searchPendingRequests();
		}
		catch(Exception e){
			system.debug('Error===>' + e.getMessage());
			system.debug('Error Line ===>' + e.getLineNumber());
			Database.rollback(sp);
		}
		finally{
			ApexPages.currentPage().getParameters().remove('inst');
			ApexPages.currentPage().getParameters().remove('invId');
		}

	}

	/********************** Filters **********************/
	public String selectedCountry {get{if(selectedCountry==null) selectedCountry = 'all'; return selectedCountry;}set;}
	public String selectedSchoolGP{get;set;}
	public String selectedSchool{get;set;}
	public String selectedCity{get;set;}
	public String selectedCampus{get;set;}
	public String selectedAgencyGroup {get;set;}
	public String selectedAgency {get;set;}
	public String selectedPayment {get{if(selectedPayment==null) selectedPayment = 'all'; return selectedPayment;}set;}
	public String searchName {get{if(searchName==null) searchName = ApexPages.CurrentPage().getParameters().get('nm'); return searchName;}set;}
	public String requestNumber {get{if(requestNumber==null) requestNumber = ''; return requestNumber;}set;}
	 public string selectedDateFilterBy{
      get{
        if(selectedDateFilterBy == null)
          selectedDateFilterBy = 'commdue';
        return selectedDateFilterBy;
      }
      set;
    }

     public boolean showOverdue{
      get{
        if(showOverdue == null)
          showOverdue = true;
        return showOverdue;
      }
      set;
    }

	public list<SelectOption> countryOptions {get;set;}
	public list<SelectOption> schGroupOptions {get;set;}
	public list<SelectOption> schoolOptions {get;set;}

	public client_course_instalment__c dates{get{
		if(dates==null){

			dates = new client_course_instalment__c();

			String pIni = ApexPages.CurrentPage().getParameters().get('ini');
			String pFin = ApexPages.CurrentPage().getParameters().get('fin');
			
			if(pIni != null && pini!= ''){
				// list<String>dt = pIni.split('-');
				dates.Commission_Due_Date__c = date.valueOf(pini + ' 00:00:00');
			}
			else{
				dates.Commission_Due_Date__c = system.today();
			}

			if(pFin != null && pFin!= ''){
				// list<String>dt = pFin.split('-');
				dates.Commission_Paid_Date__c = date.valueOf(pFin + ' 00:00:00');
			}
			else{
				dates.Commission_Paid_Date__c = dates.Commission_Due_Date__c.addDays(30);
			}
		}
		return dates;
	}set;}

	private map<string,list<SelectOption>> schools {get;set;}

	private map<string,list<SelectOption>> allFilters {get;set;}


	private void checkFilters(){
      
      	allFilters = ff.pdsFlowBOFilters(bo);

      	countryOptions = allFilters.get('allCountries');
      	if(countryOptions != null && countryOptions.size()>0){

			String pCountry = ApexPages.CurrentPage().getParameters().get('cn');
			String pGroup = ApexPages.CurrentPage().getParameters().get('pGroup');

			//Get Country
			if(pCountry != null && pCountry != '')
				selectedCountry = pCountry;
			else
				selectedCountry = countryOptions[0].getValue();

			//Get School Group
			schGroupOptions = allFilters.get(selectedCountry);
			
			selectedSchoolGP = pGroup;
			if(selectedSchoolGP == null || selectedSchoolGP == '')
				selectedSchoolGP = schGroupOptions[0].getValue();

			//Get School 
			if(selectedSchoolGP != 'none' && selectedSchoolGP != 'all')
				schoolOptions = allFilters.get(selectedCountry+'-'+selectedSchoolGP);

			String pSchool = ApexPages.CurrentPage().getParameters().get('sch');

			if( pSchool != null && pSchool != ''){
				selectedSchool = pSchool;
			}
			else if(schoolOptions != null){
				selectedSchool = schoolOptions[0].getValue();
			}
      	}
    }

    // public void changeCountry(){
    //     schoolOptions = allFilters.get(selectedCountry);
    //     selectedSchool = schoolOptions[0].getValue();
    // }

	public void changeCountry(){
        schGroupOptions = allFilters.get(selectedCountry);
        selectedSchoolGP = schGroupOptions[0].getValue();
        schoolOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
    }

	//Change School Group
    public void changeSchoolGP(){
		system.debug('selectedSchoolGP===>' + selectedSchoolGP);

        if(selectedSchoolGP != 'none' && selectedSchoolGP != 'all'){
          schoolOptions = allFilters.get(selectedCountry+'-'+selectedSchoolGP);
        }
        else{
          schoolOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
        }
		
		if(schoolOptions!= null && schoolOptions.size()>0)
        	selectedSchool = schoolOptions[0].getValue();
    }


  /********************** Inner Classes **********************/

	public class schoolResult{
		public String schoolId {get;set;}
		public String schoolName {get;set;}
		public list<invoice> invoices {get{if(invoices==null) invoices= new list<invoice>(); return invoices;}set;}
		public integer totalCompleted {get{if(totalCompleted==null) totalCompleted=0; return totalCompleted;}set;}
		public schoolResult (String schoolId, String schoolName, invoice inv, boolean isCompleted){
				this.schoolId = schoolId;
				this.schoolName = schoolName;
				this.invoices.add(inv);
				if(isCompleted){
					this.totalCompleted ++;
				}
		}

		public void addInvoice(invoice inv, boolean isCompleted){
				this.invoices.add(inv);

				if(isCompleted){
					this.totalCompleted ++;
				}
		}
	}

	//Invoice Details
	public class invoice{
		public Invoice__c inv {get;set;}
		public list<client_course_instalment__c> instalments {get;set;}
		public list<SelectOption> options {get;set;}
		public list<SelectOption> instOptions {get;set;}
		public Date lastEmail {get;set;}
		public list<instalmentActivity> activities {get{if(activities == null) activities = new list<instalmentActivity>(); return activities;} set;}
		public invoice (Invoice__c inv, list<client_course_instalment__c> instalments, list<SelectOption> options, list<SelectOption> instOptions, list<instalmentActivity> activities){
			this.inv = inv;
			this.instalments = instalments;
			this.options = options;
			this.instOptions = instOptions;
			this.lastEmail = date.newinstance(inv.Sent_On__c.year(), inv.Sent_On__c.month(), inv.Sent_On__c.day());
			this.activities = activities;
		}
	}

	//Last Activity
	public class instalmentActivity{
		public string acType {get; set;}
		public Contact acDate {get; set;}
		public string acStatus {get; set;}
		public string acError {get; set;}
		public string acTo {get; set;}
		public string acFrom {get; set;}
		public string acSubject {get; set;}
	}



	public map<Id,Account> findAccountNames(set<String> ids){
		map<Id,Account> accountMap = new map<Id,Account> ([Select Id, Name, BillingCity, ParentId FROM Account WHERE id in :ids]);
		return accountMap;
	}

	public void deleteObj(SObject obj){
		delete obj;
	}

	/** Current User **/
	private Id financeGlobalLink {get{
		if(financeGlobalLink==null){
			if(currentUser.Contact.Finance_Global_Link__c != null)
				financeGlobalLink = currentUser.Contact.Finance_Global_Link__c;
			else financeGlobalLink = currentUser.Contact.Account.Global_Link__c;
		}
		return financeGlobalLink;
	}set;}

	private User currentUser {get{
      if(currentUser==null) {
        currentUser = ff.currentUser;

      }
      return currentUser;
    }set;}

}