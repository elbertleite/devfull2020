public with sharing class payment_plan_bo {
	private user UserDetails;
	public String bucketName {get;set;}
	public String gbLink {get;set;}
	public payment_plan_bo() {
		userDetails = [Select id, contact.account.name, Contact.Account.Global_Link__c from user where id = :userInfo.getUserId() limit 1];
		bucketName = IPFunctions.s3bucket;
		gbLink = userDetails.Contact.Account.Global_Link__c;
		retrievePaymentPlans();
	}
	string ct;
	
	public map<string, integer> listSize{get; set;}
	public map<string,list<Payment_Plan__c>> listPaymentPlans{get; set;}
	public Payment_Plan__c itemUpdate{get{if(itemUpdate == null) itemUpdate = new Payment_Plan__c(); return itemUpdate;} set;}
	public Payment_Plan__c itemUpdateBucket{get{if(itemUpdateBucket == null) itemUpdateBucket = new Payment_Plan__c(); return itemUpdateBucket;} set;}

	public void retrievePaymentPlans(){
		string employee = ApexPages.CurrentPage().getParameters().get('userId');
		string agencyId = ApexPages.CurrentPage().getParameters().get('agencyId');
		string groupId = ApexPages.CurrentPage().getParameters().get('groupId');
		
		listPaymentPlans = new map<string,list<Payment_Plan__c>>();
		listPaymentPlans.put('bucket', new list<Payment_Plan__c>());
		listPaymentPlans.put('instalments', new list<Payment_Plan__c>());

		string sql = 'Select id, Client__r.name, Payment_To_Amend__c, Payment_Confirmed_On__c, Value__c, Instalments__c, preview_link__c, Value_Remaining__c, Total_Paid__c, Value_Paid__c, Currency_Code__c, ';
				sql += ' createdby.Name, createdDate, Description__c, Created_By_Agency__r.name, Loan_Status__c, Loan_Approved_By__c, Loan_Approved_On__c, number_of_instalments__c, ';
				sql += ' Currency_Paid__c, Currency_Rate__c, Value_in_Local_Currency__c, Instalments__r.Description__c, Instalments__r.Value_Remaining__c, Instalments__r.Plan_Type__c, Notes__c, Received_By_Agency__r.name, Received_By__r.name, Received_On__c, ';
				sql += ' Reminder_Date__c from Payment_Plan__c where ';
				if(employee != null && employee.trim() != '' && employee != 'all' )
					sql += ' (client__r.owner__c = :employee) and ';
				else if(agencyId != null && agencyId.trim() != '' && agencyId != 'all' )
					sql += ' (client__r.current_Agency__c = :agencyId) and ';
				else if(groupId != null && groupId.trim() != '' && groupId != 'all' )
					sql += ' client__r.current_Agency__r.ParentId = :groupId and ';
				sql += ' ((Instalments__c != null and Instalments__r.Plan_Type__c = \'bucket\' and Received_On__c != null and Payment_Confirmed_On__c = null and Payment_To_Amend__c = false) ';
				sql += ' or (Plan_Type__c = \'instalments\' and Instalments__c = null and Loan_Approved_On__c = null)) ';
				sql += ' order by Received_On__c, lastModifiedDate ';

		// listPaymentPlans = new list<Payment_Plan__c>();
		mapLinks = new map<string, list<fileLinks>>();
		mapNotes = new map<string, list<noteDetails>>();
		for(Payment_Plan__c pp:database.query(sql)){
			//listPaymentPlans.add(pp);
			if(pp.Instalments__r.Plan_Type__c != null || pp.Instalments__c == null){
				if(pp.Instalments__c == null)
					listPaymentPlans.get('instalments').add(pp);
				else listPaymentPlans.get(pp.Instalments__r.Plan_Type__c).add(pp);
				if(pp.preview_link__c != null){
					for(string pl:pp.preview_link__c.split(';;'))
						if(!mapLinks.containsKey(pp.id))
							mapLinks.put(pp.id, new list<fileLinks>{new fileLinks(pl.split('!#!')[1],pl.split('!#!')[0])});
						else mapLinks.get(pp.id).add(new fileLinks(pl.split('!#!')[1],pl.split('!#!')[0]));
				}		
				else mapLinks.put(pp.id, new list<fileLinks>());
				
				if(pp.notes__c != null){
					if(!mapNotes.containsKey(pp.id))
						mapNotes.put(pp.id, new list<noteDetails>((list<noteDetails>)JSON.deserialize(pp.notes__c,list<noteDetails>.class)));
				}
				else mapNotes.put(pp.id, new list<noteDetails>());
				mapNotes.get(pp.id).sort();
			}
		}
		listSize = new map<string, integer>();
		for(string s:listPaymentPlans.keySet())
			listSize.put(s,listPaymentPlans.get(s).size());

	}

	public map<string, list<fileLinks>> mapLinks{get{if(mapLinks == null) mapLinks = new map<string, list<fileLinks>>(); return mapLinks;} set;}
	public map<string, list<noteDetails>> mapNotes{get{if(mapNotes == null) mapNotes = new map<string, list<noteDetails>>(); return mapNotes;} set;}

	public void confirmPayment(){
		string payId = ApexPages.CurrentPage().getParameters().get('payId');
		Payment_Plan__c payPlan = new Payment_Plan__c(id = payId, Payment_Confirmed_On__c = System.today(), Payment_Confirmed_By__c = userInfo.getUserId());
		update payPlan;
		retrievePaymentPlans();
	}

	public class fileLinks{
		public string fileName{get;set;}
		public string previewLInk{get;set;}
		public fileLinks(string fileName, string previewLInk){
			this.fileName = fileName;
			this.previewLInk = previewLInk;
		}
	}

	public void createUpdate(){
		string itemId = ApexPages.currentPage().getParameters().get('itemId');
		itemUpdate = new Payment_Plan__c(id = itemId, notes__c = '');
	}
	
	public void createUpdateBucket(){
		string itemId = ApexPages.currentPage().getParameters().get('itemId');
		itemUpdateBucket = [Select id, client__c, notes__c from Payment_Plan__c where id = :itemId];
		itemUpdateBucket.notes__c = '';
	}

	public class noteDetails implements Comparable{
		public string noteOn{get;set;}
		public string noteBy{get;set;}
		public string noteAgency{get;set;}
		public string noteDesc{get;set;}
		public noteDetails(string noteOn, string noteBy, string noteAgency, string noteDesc){
			this.noteOn = noteOn;
			this.noteBy = noteBy;
			this.noteAgency = noteAgency;
			this.noteDesc = noteDesc;
		}

		public Integer compareTo(object compareTo) {
			noteDetails noteDetailsToCompare = (noteDetails)compareTo;
			
			if(datetime.parse(noteOn) > datetime.parse(noteDetailsToCompare.noteOn)){
				return -1;
			}else if(datetime.parse(noteOn) < datetime.parse(noteDetailsToCompare.noteOn)){
				return 1;
			}
			return 0;
		}
	}

	public void updateItem(){
		System.debug('==>itemUpdate.notes__c: '+itemUpdateBucket.notes__c);
		if(itemUpdateBucket.notes__c != null && itemUpdateBucket.notes__c.trim() != ''){
			noteDetails nt = new noteDetails( System.now().format(), userInfo.getName(), userDetails.contact.account.name, itemUpdateBucket.notes__c);
			mapNotes.get(itemUpdateBucket.id).add(nt);
			Payment_Plan__c paymentToUpdate = new Payment_Plan__c(id = itemUpdateBucket.id, notes__c = JSON.serialize(mapNotes.get(itemUpdateBucket.id)), Payment_To_Amend__c = true, Payment_Confirmed_On__c = null);
			update paymentToUpdate;
		}
		retrievePaymentPlans();
	}

	public void updateLoanStatus(){
		noteDetails nt = new noteDetails( System.now().format(), userInfo.getName(), userDetails.contact.account.name, itemUpdate.notes__c);
		System.debug('==>mapNotes: '+mapNotes);
		System.debug('==>mapNotes: '+mapNotes);
		System.debug('==>itemUpdate.id: '+itemUpdate.id);
		System.debug('==>itemUpdate.notes__c: '+itemUpdate.notes__c);
		mapNotes.get(itemUpdate.id).add(nt);
		Payment_Plan__c paymentToUpdate = new Payment_Plan__c(id = itemUpdate.id, notes__c = JSON.serialize(mapNotes.get(itemUpdate.id)), Loan_Status__c = 'denied', Loan_Approved_By__c = userInfo.getUserId(), Loan_Approved_On__c = System.now());
		update paymentToUpdate;
		retrievePaymentPlans();
	}

	@RemoteAction
    public static list<Account> changeAccount() {
		// return [SELECT Id, Name FROM Account WHERE recordType.name = 'Agency Group' order by name];
		
		list<Account> agencyGroupOptions = new List<Account>();  
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group  
			user localUser = [Select id, Contact.AccountId from user where id =:userInfo.getUserId() limit 1];		
			for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:localUser.Contact.AccountId order by Parent.Name]){
				if(ag.Parent.Name != null && ag.ParentId != null)
					agencyGroupOptions.add(new Account(id = ag.ParentId, name = ag.Parent.Name));
			}
		}else{
			for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
				agencyGroupOptions.add(new Account(id = ag.id, name = ag.Name));
			}
		}
		return agencyGroupOptions;
    }

    @RemoteAction
    public static list<Account> changeAgency(string groupId) {
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
			user localUser = [Select id, Contact.AccountId from user where id =:userInfo.getUserId() limit 1];
			return [Select Id, Name from Account where id =:localUser.Contact.AccountId order by name];
		}else{
			return [SELECT Id, Name FROM Account WHERE parentId = :groupId order by name];
		}
    }
	
    @RemoteAction
    public static list<User> changeEmployee(string agencyId) {
        return [SELECT Id, Name FROM User WHERE contact.Accountid = :agencyId and isActive = true and Contact.Chatter_Only__c = false and api_user__c = false order by name];
    }

	private string groupID{
		get{
			if(groupID == null){
				try {
					groupID = [select Contact.Account.ParentId from User where id =:UserInfo.getUserId() limit 1].Contact.Account.ParentId;
				} catch (Exception e){
					groupID = '';
				}
			}
			return groupID;
		}
		set;
	}

	public string groupBgColor{
		get{
			
			UserDetails.GroupSettings gs = new UserDetails.GroupSettings();
			groupBgColor = gs.getBackgroundColour(groupID);
			
			return groupBgColor;
		}
		set;
	}
	
	public string groupLabelColor{
		get{
			UserDetails.GroupSettings gs = new UserDetails.GroupSettings();
			groupLabelColor = gs.getLabelColour(groupID);
			
			return groupLabelColor;
		}
		set;
	}

	public void confirmLoan(){
		string itemId = ApexPages.currentPage().getParameters().get('itemId');
		string loanStatus = ApexPages.currentPage().getParameters().get('loanStatus');
		System.debug('==>itemId: '+itemId);
		update new Payment_Plan__c(id = itemId, Loan_Status__c = loanStatus, Loan_Approved_By__c = userInfo.getUserId(), Loan_Approved_On__c = System.now() );
		retrievePaymentPlans();
	}

	
}