public with sharing class Address{
	
	public Account acco {get;Set;}
	public Contact cont {get;Set;}
	public Address__c contactAddress {get;Set;}

	
	public Address(){
		
		String recordid = ApexPages.currentPage().getParameters().get('id');
		
		if(recordid.startsWith('001'))
			acco = [select id, BillingStreet, BillingPostalCode, BillingCity, BillingState, BillingCountry from Account where Id = :recordid];
		else if(recordid.startsWith('003')){
			try{
				cont = [select Id from Contact where Id = :recordid];
				contactAddress = [select Id, Country__c, Street__c, State__c, Post_Code__c, City__c  from Address__c where Contact__c = :cont.Id and Current_Address__c = true limit 1];
			}catch(Exception e){
				
			}
		}
		
	}
	
	

}