public with sharing class agency_details {

	public Account agency {get;set;}
	public Account agencyGroup {get;set;}
	public Bank_Detail__c bankDetail {get;set;}
	public String agencyFromID {get;set;}
	public String agencyID;
	public String agencyGroupID;
	private boolean newAgency = false;

	public String path {
		get {
			return agency.AgencyGroup__c + '/Agencies/' + agency.id + '/Logo/';
		}
	}
	private user currentUser = [Select Id, Aditional_Agency_Managment__c, Contact.AccountId from User where id = :UserInfo.getUserId() limit 1];

	public agency_details(ApexPages.StandardController controller){

		this.agencyFromID = ApexPages.currentPage().getParameters().get('agencyID');
		this.agencyGroupID = ApexPages.currentPage().getParameters().get('agencyGroupID');
		String action = ApexPages.currentPage().getParameters().get('action');

		string groupFromAgencyParam;
		user currentUser = [Select Id, Aditional_Agency_Managment__c, Contact.AccountId, Contact.Account.ParentId from User where id = :UserInfo.getUserId() limit 1];
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){
			if(agencyGroupID != null && agencyGroupID != currentUser.Contact.Account.ParentId)
				agencyGroupID = currentUser.Contact.Account.ParentId;
			if(controller.getRecord().id != null && [Select parentId from Account where id= :controller.getRecord().id].parentId != currentUser.Contact.Account.ParentId)
				groupFromAgencyParam = currentUser.Contact.Account.ParentId;
		}

		if(agencyGroupID != null)
			agencyGroup = getDetails(agencyGroupID);

		if(action != 'new'){

			if(controller.getRecord().id != null){
				this.agencyID = controller.getRecord().id;
				if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() || groupFromAgencyParam != null){ //set the user to see only his own agency

						set<string> idsAgency = new set<string>{currentUser.Contact.AccountId};
					
						if(currentUser.Aditional_Agency_Managment__c != null){
							for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
								idsAgency.add(ac.id);
							}
						}
						if(!idsAgency.contains(this.agencyID))
							this.agencyID = currentUser.Contact.AccountId;
				}

			}
			else {
				xCourseSearchPersonDetails pd = new xCourseSearchPersonDetails();
				this.agencyID = pd.getAgencyDetails().Agency_Id;
			}


			agency = getDetails(this.agencyID);

		} else {
			newAgency = true;
			agency = new Account();
			bankDetail = new Bank_Detail__c();
			edit = true;
		}
	}

	public string accRTName {get;set;}

	private Account getDetails(String id){
		Account acc = [Select Name, RecordType.Name, CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name, Agency_Type__c, Group_Type__c, Phone, Skype__c, Logo__c, AgencyGroup__c, NumberOfEmployees, Parent.ID, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,  ParentId, Parent.Name, Account_Currency_ISO_Code__c, Services__c, ServicesName__c, Hify_Agency__c, Registration_name__c, Year_established__c, BusinessEmail__c, Website, BillingLatitude, BillingLongitude, Inactive__c, Hide_Timetable__c, Email_Quotation_Option__c, Business_Number__c, Type_of_Business_Number__c, Search_Instalment_Plan__c, courseSearch_SchoolType__c, courseSearch_TuitionType__c, courseSearch_InstalOption__c, courseSearch_PriceOrder__c, Website_Agency_Name__c, opening_hours__c, Show_On_Website__c, required_pay_intall_to_agency__c, Clients_Payments_Email__c, School_Payments_Email__c, BackOffice_Email__c,Auto_Check_Previous_Statuses__c, (SELECT Country__c, Back_Office_Name__c, Services__c FROM Back_Offices_Control__r order by Country__c, Back_Office_Name__c) from Account where id = :id limit 1];

		if(acc.RecordType.Name=='campus')
			accRTName = 'School';
		if(acc.RecordType.Name=='Agency'){
			accRTName = 'Agency';
		}

		//for(Bank_Detail__c bd : acc.Bank_Details1__r)
		//	bankDetail = bd;

		if(bankDetail == null){
			bankdetail = new Bank_Detail__c();
		}

		if(String.isEmpty(acc.Auto_Check_Previous_Statuses__c)){
			stagesToAutoClick = new Map<String, List<String>>();
		}else{
			stagesToAutoClick = (Map<String, List<String>>) JSON.deserialize(acc.Auto_Check_Previous_Statuses__c, Map<String, List<String>>.class);
		}

		return acc;
	}

	public pageReference refreshAgency(){
		agency = getDetails(agency.id);
		return null;
	}

	public List<SelectOption> instalments {
		get{
			if(instalments == null){
				instalments = new List<SelectOption>();
				instalments.add(new SelectOption( '' , '-- Select Agency Plan --' ));
				for(Instalment_Plan__c ip : [Select id, Name__c from Instalment_Plan__c WHERE Agency__c = :agencyID order by Name__c])
					instalments.add(new SelectOption( ip.id, ip.Name__c ));
			}

			return instalments;
		}
		set;
	}

	private List<SelectOption> searchOrder;
	public List<SelectOption> getSearchOrder(){
		if(SearchOrder == null){
			xCourseSearchPersonDetails spd = new xCourseSearchPersonDetails();
			SearchOrder = spd.getSearchOrder();
		}
		return SearchOrder;
	}

	private List<SelectOption> SchoolType;
	public List<SelectOption> getSchoolType(){
		if(SchoolType == null){
			xCourseSearchPersonDetails spd = new xCourseSearchPersonDetails();
			SchoolType = spd.getSchoolType();
		}
		return SchoolType;
	}

	private List<SelectOption> schoolPaymentPrice;
	public List<SelectOption> getSchoolPaymentPrice(){
		if(schoolPaymentPrice == null){
			xCourseSearchPersonDetails spd = new xCourseSearchPersonDetails();
			schoolPaymentPrice = spd.getSchoolPaymentPrice();
		}
		return schoolPaymentPrice;
	}

	private List<SelectOption> SchoolTuitionOption;
	public List<SelectOption> getSchoolTuitionOption(){
		if(SchoolTuitionOption == null){
			xCourseSearchPersonDetails spd = new xCourseSearchPersonDetails();
			SchoolTuitionOption = spd.getSchoolTuitionOption();
		}
		return SchoolTuitionOption;
	}


	public String service {get;set;}
	public string serviceKey { get{if(serviceKey == null) serviceKey=''; return serviceKey;} set; }

	 private void generateAgencyServices(String sv){
	  	 if(sv != null) servicesList = sv.split(';');
	  }


	public void addService(){
	    if(!String.isEmpty(service)){
	    	if(!String.isEmpty(agencyServices)){
	    		agencyServices = agencyServices + ';' + service;
	    		generateAgencyServices(agencyServices);
	    	}
	      else servicesList.add(service);

	    	service = '';
	    }
	  }


	public void delAgencyService(){
	    if(!String.isEmpty(serviceKey)){
	      integer index = Integer.valueOf(serviceKey);
	      servicesList.remove(index);
	      agencyServices = '';
	    }
  	}


	public string agencyServices{
		get{
			if(agencyServices == null){
				if(agency.ServicesName__c != null)
					agencyServices = agency.ServicesName__c;
				else if(agency.Services__c != null)
					agencyServices = agency.Services__c;
			}
			return agencyServices;
		}
		Set;
	}

	public list<string> servicesList{
		get{
			if(servicesList == null){
				servicesList = new list<string>();
				if(agency.ServicesName__c != null)
					servicesList = agency.ServicesName__c.split(';');
				else if(agency.Services__c != null)
					servicesList = agency.Services__c.split(';');
			}
			return servicesList;
		}
		Set;
	}

	public Map<String, List<String>> stagesToAutoClick {get;set;}
	public List<String> availableStages{get;set;}
	public String destinationForStages{get{if(destinationForStages == null) destinationForStages=''; return destinationForStages;} set;}

	public void loadStagesFromDestination(){
		destinationForStages = ApexPages.currentPage().getParameters().get('destination');
		String agencyGroup = [SELECT Parent.ID FROM Account WHERE ID = :agencyID].Parent.ID;
		
		availableStages = new List<String>();
		if(destinationForStages == 'All'){
			for(Client_Stage__c stage : [SELECT ID, Stage__c FROM Client_Stage__c WHERE Agency_Group__c = :agencyGroup ORDER BY Stage__c]){
				if(!String.isEmpty(stage.Stage__c) && !availableStages.contains(stage.Stage__c)){
					availableStages.add(stage.Stage__c);
				}
			}	
		}else{
			for(Client_Stage__c stage : [SELECT ID, Stage__c FROM Client_Stage__c WHERE Agency_Group__c = :agencyGroup AND (Destination__c = :destinationForStages OR Stage__c = 'Stage 0') ORDER BY Stage__c]){
				if(!String.isEmpty(stage.Stage__c) && !availableStages.contains(stage.Stage__c)){
					availableStages.add(stage.Stage__c);
				}
			}
		}
		availableStages.sort();
	}

	public List<String> getDestinationsSavedToAutoCheck(){
		List<String> items = new List<String>();
		if(!stagesToAutoClick.isEmpty()){
			for(String dest : stagesToAutoClick.keySet()){
				items.add(dest);
			}
			items.sort();
		}
		return items;
	}

	public void addStagesToMapping(){
		String stagesString = ApexPages.currentPage().getParameters().get('stages');
		List<String> stages = (List<String>) JSON.deserialize(stagesString, List<String>.class);

		if(destinationForStages == 'All'){
			stagesToAutoClick = new Map<String, List<String>>();	
		}
		stagesToAutoClick.put(destinationForStages, stages);

		destinationForStages = '';
		availableStages = null;
	}

	public void deleteMappingDestinationStages(){
		String destination = ApexPages.currentPage().getParameters().get('destination');
		stagesToAutoClick.remove(destination);
	}

	public List<SelectOption> getCountriesFromChecklist(){
		List<SelectOption> destinations = new List<SelectOption>();
		Set<String> dests = new Set<String>();
		destinations.add(new SelectOption('','Select an Option', true));
		destinations.add(new SelectOption('All','All Destinations'));

		String agencyGroup = [SELECT Parent.ID FROM Account WHERE ID = :agencyID].Parent.ID;
		
		for(Client_Stage__c destination : [SELECT ID, Destination__c FROM Client_Stage__c WHERE Agency_Group__c = :agencyGroup ORDER BY Destination__c]){
			if(!String.isEmpty(destination.Destination__c) && !dests.contains(destination.Destination__c)){
				dests.add(destination.Destination__c);
				destinations.add(new SelectOption(destination.Destination__c, destination.Destination__c, stagesToAutoClick.get(destination.Destination__c) != null));
			}
		}

		return destinations;
	}


	public List<String> getDefaultServices() {
		List<String> options = new List<String>();
		Schema.DescribeFieldResult result = Schema.Account.Services__c.getDescribe();
		for (Schema.PicklistEntry ld:result.getPicklistValues())
			options.add(ld.getLabel());

		return options;
	}

	public boolean edit {get{if(edit== null) edit = false; return edit;}set;}

	public void setEdit(){
		edit = true;
	}

	public PageReference cancel(){
		edit = false;
		if(newAgency){
			return new Pagereference('/apex/agency_group_list_agencies?id=' + agency.ParentId);
		}else{
			if(String.isEmpty(agency.Auto_Check_Previous_Statuses__c)){
				stagesToAutoClick = new Map<String, List<String>>();
			}else{
				stagesToAutoClick = (Map<String, List<String>>) JSON.deserialize(agency.Auto_Check_Previous_Statuses__c, Map<String, List<String>>.class);
			}
			return null;
		}
	}

	public void saveAgency(){

		if(!String.isEmpty(agencyServices)){
			system.debug('agencyServices==>' + agencyServices);
			agency.Services__c = agencyServices;
			agency.ServicesName__c = agencyServices;

		}else{
			system.debug('servicesList==>' + servicesList);
			agency.Services__c = String.join(servicesList, ';');
			agency.ServicesName__c =  String.join(servicesList, ';');
		}

		agency.Auto_Check_Previous_Statuses__c = JSON.serialize(stagesToAutoClick);

		if(agency.ParentId == null){
			agency.ParentId = agencyGroupID;
		}

		upsert agency;




		/*if(bankdetail != null){

			if(bankdetail.Account__c == null)
				bankDetail.Account__c = agency.id;

			upsert bankDetail;

		}*/



		edit = false;
		servicesList = null;
		agencyServices = null;

	}

	public List<SelectOption> getCurrencies(){
		return xCourseSearchFunctions.getCurrencies();
	}

	public List<SelectOption> getlistBackOffices(){
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('', '--Select Back Office--'));
		for(Account ac:[Select id, name from account where recordtype.name = 'agency' order by name])
			options.add(new SelectOption(ac.id, ac.Name));
		return options;
	}


	public String Search_Instalment_Plan {
		get{
			return IPFunctions.getSearchConfigLabel(agency.Search_Instalment_Plan__c);
		}
		set;
	}

	public String courseSearch_SchoolType {
		get{
			return IPFunctions.getSearchConfigLabel(agency.courseSearch_SchoolType__c);
		}
		set;
	}

	public String courseSearch_TuitionType {
		get{
			return IPFunctions.getSearchConfigLabel(agency.courseSearch_TuitionType__c);
		}
		set;
	}

	public String courseSearch_InstalOption {
		get{
			return IPFunctions.getSearchConfigLabel(agency.courseSearch_InstalOption__c);
		}
		set;
	}

	public String courseSearch_PriceOrder {
		get{
			return IPFunctions.getSearchConfigLabel(agency.courseSearch_PriceOrder__c);
		}
		set;
	}



}