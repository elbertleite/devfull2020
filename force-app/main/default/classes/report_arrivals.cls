public with sharing class report_arrivals {

	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}

	public report_arrivals(){

		selectedAgencyGroup = myDetails.Account.Parentid;
		selectedAgency = mydetails.Accountid;
	}

	public Contact myDetails {
		get{
			if(myDetails == null)
				myDetails = UserDetails.getMyContactDetails();
			return myDetails;
		}
		set;
	}

	private Map<String, String> groupNames;
	public String selectedAgencyGroup {get;set;}
	private List<SelectOption> agencyGroupOptions;
	public List<SelectOption> getAgencyGroupOptions() {
		if(agencyGroupOptions == null){
			agencyGroupOptions = new List<SelectOption>();
			groupNames = new Map<String,String>();
			if(myDetails.Account.Parent.Destination_Group__c || myDetails.Global_Manager__c){
				agencyGroupOptions.add(new SelectOption('all', '-- ALL --'));
				for(Account ag : IPFunctionsGLobal.getAgencyGroups(myDetails.Account.Parent.Global_Link__c)){
					agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
					groupNames.put(ag.id, ag.Name);
				}
			} else {
				for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
					agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
					groupNames.put(ag.id, ag.Name);
				}

				//agencyGroupOptions.add(new SelectOption(myDetails.Account.ParentId, myDetails.Account.Parent.Name));
				//groupNames.put(myDetails.Account.ParentId, myDetails.Account.Parent.Name);
			}
		}

		return agencyGroupOptions;
	}

	public void refreshAgencies(){
		selectedAgency = null;
		agencyOptions = null;
		getAgencyOptions();
		countryOptions = null;
		getCountryOptions();
		refreshUsers();
	}

	public String selectedAgency {get;set;}
	private List<SelectOption> agencyOptions;
	public List<SelectOption> getAgencyOptions(){
		if(agencyOptions == null){
			agencyOptions = new List<SelectOption>();
			agencyOptions.add(new SelectOption('all', '-- ALL --'));
			if(selectedAgencyGroup != null)
				for(Account agency : [select id, name from Account where RecordType.Name = 'Agency' and parentid = :selectedAgencyGroup order by Name])
					agencyOptions.add(new SelectOption(agency.id, agency.Name));

		}
		return agencyOptions;
	}


	public void refreshUsers(){
		selectedUsers = new List<String>();
		userOptions = null;
		getuserOptions();
	}

	public List<String> selectedUsers { get{ if(selectedUsers == null) selectedUsers = new List<String>(); return selectedUsers; } set;}
	private List<SelectOption> userOptions;
	public List<SelectOption> getuserOptions(){
		if(userOptions == null){
			userOptions = new List<SelectOption>();
			if(selectedAgencyGroup != null && selectedAgencyGroup != 'all')
				if(selectedAgency != null && selectedAgency != 'all')
					for(User u : [select id, name from User where Contact.AccountID = :selectedAgency and isActive = true and Contact.Chatter_Only__c = false order by Name])
						userOptions.add(new SelectOption(u.id, u.Name));
				else
					for(User u : [select id, name from User where Contact.Account.Parentid = :selectedAgencyGroup and isActive = true and Contact.Chatter_Only__c = false order by Name])
						userOptions.add(new SelectOption(u.id, u.Name));

		}
		return userOptions;
	}

	public String selectedDestinationCountry { get;set; }
	public String selectedDestinationCity { get;set; }
	private List<SelectOption> countryOptions;
	public List<SelectOption> getCountryOptions() {
		if(countryOptions == null){
			countryOptions = new List<SelectOption>();
			if(!myDetails.Global_Manager__c && myDetails.Account.Parent.Destination_Group__c && (selectedAgencyGroup == 'all' || selectedAgencyGroup != myDetails.Account.Parentid)){
				countryOptions.add(new SelectOption(myDetails.Account.BillingCountry, myDetails.Account.BillingCountry));
			} else {
				countryOptions = IPFunctions.getAllCountries();
				countryOptions.add(0, new SelectOption('all','-- ALL --'));
			}

		}
		return countryOptions;
	}

	public String selectedNationality { get;set; }
	private List<SelectOption> nationalityOptions;
	public List<SelectOption> getNationalityOptions(){
		if(nationalityOptions == null){
			nationalityOptions = new List<SelectOption>();
			nationalityOptions = IPFunctions.getAllCountries();
			nationalityOptions.add(0, new SelectOption('all','-- ALL --'));
		}
		return nationalityOptions;
	}


	public Contact arrivalDate {
		get {
			if(arrivalDate == null){
				arrivalDate = new Contact();
				arrivalDate.Arrival_Date__c = system.today();
				arrivalDate.Expected_Travel_Date__c = system.today().addDays(7);
			}
			return arrivalDate;
		}
		set;
	}

	public Contact expTravelDate {
		get {
			if(expTravelDate == null){
				expTravelDate = new Contact();
				expTravelDate.Arrival_Date__c = system.today();
				expTravelDate.Expected_Travel_Date__c = system.today().addDays(7);
			}
			return expTravelDate;
		}
		set;
	}

	public Client_Course_Overview__c courseDate  {
		get {
			if(courseDate == null){
				courseDate = new Client_Course_Overview__c();
				courseDate.Course_Start_Date__c = system.today();
				courseDate.Course_End_Date__c = system.today().addDays(7);
			}
			return courseDate;
		}
		set;
	}

	public transient Map<String, Contact> contacts {get;set;}
	public transient List<String> contactKeys {get;set;}

	private boolean excelSearch {get{if(excelSearch==null) excelSearch = false; return excelSearch;}set;}
	public void searchExcel(){
		excelSearch = true;
	 	search();
	}


	public void search(){

		boolean searchCourses = false;
		String sqlWhereCourseDates = ' where createddate != null ';
		if(courseDate.Course_Start_Date__c != null){
			sqlWhereCourseDates += ' and course_start_date__c >= ' + IPFunctions.formatSqlDate(courseDate.Course_Start_Date__c);
			searchCourses = true;
		}
		if(courseDate.Course_End_Date__c != null){
			sqlWhereCourseDates += ' and course_start_date__c <= ' + IPFunctions.formatSqlDate(courseDate.Course_End_Date__c);
			searchCourses = true;
		}


		boolean searchArrival = false;
		String sqlWhereArrivalDate = '';
		if(arrivalDate.Arrival_Date__c != null){
			sqlWhereArrivalDate += ' arrival_date__c >= ' + IPFunctions.formatSqlDate(arrivalDate.Arrival_Date__c);
			searchArrival = true;
		}
		if(arrivalDate.Expected_Travel_Date__c != null){
			if(searchArrival)
				sqlWhereArrivalDate += ' and ';
			sqlWhereArrivalDate += ' arrival_date__c <= ' + IPFunctions.formatSqlDate(arrivalDate.Expected_Travel_Date__c);
			searchArrival = true;
		}

		if(searchArrival)
			sqlWhereArrivalDate = 'and ( (' + sqlWhereArrivalDate  + ')';

		boolean searchExpTravel = false;
		
		if(expTravelDate.Arrival_Date__c != null){
			if(searchArrival)
				sqlWhereArrivalDate += ' OR ( ' ;
			else
				sqlWhereArrivalDate += ' AND ( ' ;
			sqlWhereArrivalDate += ' Expected_Travel_date__c >= ' + IPFunctions.formatSqlDate(expTravelDate.Arrival_Date__c);
			searchExpTravel = true;
		}
		
		if(expTravelDate.Expected_Travel_Date__c != null){
			if(searchArrival && !searchExpTravel){
				sqlWhereArrivalDate += ' OR ( Expected_Travel_date__c <= ' + IPFunctions.formatSqlDate(expTravelDate.Expected_Travel_Date__c) + ' ) ';
			}
			else if(searchExpTravel){
				sqlWhereArrivalDate += ' and Expected_Travel_date__c <= ' + IPFunctions.formatSqlDate(expTravelDate.Expected_Travel_Date__c) + ' ) ';
			}
			else if(!searchArrival){
				sqlWhereArrivalDate += ' and Expected_Travel_date__c <= ' + IPFunctions.formatSqlDate(expTravelDate.Expected_Travel_Date__c) ;
			}
			searchExpTravel = true;
		}

		// if(searchExpTravel)
		// 	sqlWhereArrivalDate += ' ) ';

		if(searchArrival)
			sqlWhereArrivalDate += ')';


		String sqlSelect = 'SELECT id, name, RecordType.Name, CreatedDate, Current_Agency__r.Parent.Name, Current_Agency__r.Name, Owner__r.Name, Lead_Stage__c, Status__c, Nationality__c, Email, MobilePhone, Destination_Country__c,  Destination_City__c, Expected_Travel_Date__c, Arrival_Date__c, Visa_Expiry_Date__c, Current_Agency__r.AgencyGroupName__c, ';

		if(!excelSearch){
			sqlSelect += ' (Select Course_End_Date__c, Course_Start_Date__c,  Course_Name__c, School_Campus_Name__c from Client_Courses_Overview__r ' + sqlWhereCourseDates + ' order by Course_Start_Date__c), ';
		}
		else{
			String datesFilter = '';
			if(courseDate.Course_Start_Date__c != null)
				datesFilter = ' where course_start_date__c >= ' + IPFunctions.formatSqlDate(courseDate.Course_Start_Date__c);

			else if(arrivalDate.Arrival_Date__c != null)
				datesFilter = ' where course_start_date__c >= ' + IPFunctions.formatSqlDate(arrivalDate.Arrival_Date__c);

			sqlSelect += ' (Select Course_End_Date__c, Course_Start_Date__c,  Course_Name__c, School_Campus_Name__c from Client_Courses_Overview__r ' + datesFilter + ' order by Course_Start_Date__c), ';
		}

		sqlSelect += ' (Select Arrival_Date__c from Destinations_Tracking__r where createdDate != null ' + sqlWhereArrivalDate + '), ' +
									' (SELECT Detail__c from forms_of_contact__r where type__c = \'Mobile\' ) ' +
									' from Contact ';
		String sqlWhere = ' where RecordType.Name in (\'Lead\', \'Client\') ';

		if(selectedAgencyGroup != null && selectedAgencyGroup != 'all')
			sqlWhere += ' and Current_Agency__r.ParentID = :selectedAgencyGroup ';
		if(selectedAgency != null && selectedAgency != 'all')
			sqlWhere += ' and Current_Agency__c = :selectedAgency ';
		if(!selectedUsers.isEmpty())
			sqlWhere += ' and Owner__c in :selectedUsers ';
		if(selectedDestinationCountry != null && selectedDestinationCountry != 'all' && selectedDestinationCountry != '')
			sqlWhere += ' and Destination_Country__c = :selectedDestinationCountry ';
		if(selectedDestinationCity != null && selectedDestinationCity != 'all' && selectedDestinationCity != '')
			sqlWhere += ' and Destination_City__c = :selectedDestinationCity ';
			if(selectedNationality != null && selectedNationality != 'all')
			sqlWhere += ' and Nationality__c = :selectedNationality ';

		String filterByCourse = ' and Id IN (Select Client__c from Client_Course_Overview__c ' + sqlWhereCourseDates + ' ) ';

		String filterByArrival = ' and Id IN (Select Client__c from Destination_Tracking__c where createdDate != null ' + sqlWhereArrivalDate + ' ) ';


		String sqlOrderBy = ' order by Name ';

		system.debug('@sql===>' + sqlSelect + sqlWhere + filterByCourse + sqlOrderBy);
		system.debug('@sql===>' + sqlSelect + sqlWhere + filterByArrival + sqlOrderBy);
		Map<String, Contact> resultMap = new Map<String, Contact>();
		if(searchCourses)
			resultMap.putAll(new Map<String, Contact>( (List<Contact>) Database.query(sqlSelect + sqlWhere + filterByCourse + sqlOrderBy)));

		if(searchArrival || searchExpTravel)
			resultMap.putAll(new Map<String, Contact>( (List<Contact>) Database.query(sqlSelect + sqlWhere + filterByArrival + sqlOrderBy)));


		System.debug('resultMap: ' + resultMap.size());

		contacts = new Map<String, Contact>();
		for(String str : resultMap.keySet()){
			contacts.put( resultMap.get(str).Current_Agency__r.AgencyGroupName__c + resultMap.get(str).Current_Agency__r.Name + resultMap.get(str).Owner__r.Name + resultMap.get(str).name + resultMap.get(str).id, resultMap.get(str));
			// system.debug('key to order: ' + resultMap.get(str).Current_Agency__r.AgencyGroupName__c + resultMap.get(str).Current_Agency__r.Name + resultMap.get(str).Owner__r.Name + resultMap.get(str).name + resultMap.get(str).id);
		}

		contactKeys = new List<String>();
		contactKeys.addAll(contacts.keySet());
		contactKeys.sort();

		if(contactKeys.isEmpty())
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Sorry, no results were found.'));

	}

	public PageReference generateExcel(){
		PageReference pr = Page.report_arrivals_excel;
		pr.setRedirect(false);
		return pr;
	}

	public String xlsHeader {
        get {
            String strHeader = '';
            strHeader += '<?xml version="1.0"?>';
            strHeader += '<?mso-application progid="Excel.Sheet"?>';
            return strHeader;
        }
    }

}