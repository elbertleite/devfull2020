public without sharing class client_course_request_loo extends EnrolmentManager {

	Contact currentUser;
	public String keyWords {get;set;}
	public TotalWrapper totals {get;set;}
	//public Map<String, List<File>> files {get;set;}
	public String requestType {get;set;}
	public String requestCourseID {get;set;}
	public boolean isLOO {get{if(isLOO == null) isLOO = false; return isLOO;}set;}
	public boolean isACC {get{if(isACC == null) isACC = false; return isACC;}set;}
	public boolean isINS {get{if(isINS == null) isINS = false; return isINS;}set;}
	private boolean isBOUser {get;set;}
	public Double offset { get{ if(offset == null) offset = EnrolmentManager.getUserTZOffset(); return offset;} set; }
	public String bucket { get{ return EnrolmentManager.bucket; } private set; }

	private map<String, list<String>> boAgenciesMap {get;set;}
	private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}

	public void setParams(){

		currentUser = UserDetails.getMyContactDetails();

		isBOUser = [Select Finance_Backoffice_User__c from User where id = :UserInfo.getUserId()].Finance_Backoffice_User__c;

		if(ApexPages.currentPage().getParameters().get('type') != null && ApexPages.currentPage().getParameters().get('type').trim() != ''){
			requestType = ApexPages.currentPage().getParameters().get('type');
			if(requestType == '' || requestType.equalsIgnoreCase('loo'))
				isLOO = true;
			else if(requestType.equalsIgnoreCase('acc'))
				isACC = true;
			else if(requestType.equalsIgnoreCase('ins'))
				isINS = true;
		} else {
			requestType = 'loo';
			isLOO = true;
		}

		// if(ApexPages.currentPage().getParameters().get('gr') != null && ApexPages.currentPage().getParameters().get('gr') != '')
		// 	selectedAgencyGroup = ApexPages.currentPage().getParameters().get('gr');
		// else selectedAgencyGroup = currentUser.Account.ParentId;

		// if(ApexPages.currentPage().getParameters().get('ag') != null && ApexPages.currentPage().getParameters().get('ag') != '')
		// 	selectedAgency = ApexPages.currentPage().getParameters().get('ag');
		// else selectedAgency = currentUser.AccountId;

		// selectedDepartment = 'all';

		//BackOffice
		boAgenciesMap = ff.findBOAgenciesPerService(currentUser.AccountId, new list<String>{'Admissions'});

		String pCountry = ApexPages.CurrentPage().getParameters().get('ct');

        if(pCountry == null || pCountry == '')
          pCountry = currentUser.Account.BillingCountry;


		//Country Options
		countryOptions = new list<SelectOption>();

		set<Id> allAgencies = new set<Id>();
		for(String ct : boAgenciesMap.keySet()){
			for(String agBo : boAgenciesMap.get(ct)){
				allAgencies.add(agBo.split(';-;')[0]);
			}//end for
		}//end for

		for(AggregateResult ar : [SELECT Course_Country__c ct FROM client_course__c WHERE LOO_Requested_to_BO_By__r.Contact.AccountID IN :allAgencies AND Course_Country__c IN :boAgenciesMap.keySet() AND LOO_Requested_To_BO__c = true AND LOO_Received__c = false group by 	Course_Country__c order by 	Course_Country__c]){
          countryOptions.add(new SelectOption((String)ar.get('ct'), (String)ar.get('ct')));
          if((String)ar.get('ct') == pCountry)
            selectedCountry = (String)ar.get('ct');
        }//end for
		
		if(selectedCountry == null && countryOptions.size()>0)
        	selectedCountry = countryOptions[0].getLabel();

		if(selectedCountry != null)
			changeCountry();
		//end -- country 

		campusMap = new map<String,map<String,String>>();
		campusMap.put('all', new map<string,string>());

		search();
	}

	public List<SelectOption> countryOptions {get;set;} 
  	public string selectedCountry{get;set;}

	public client_course_request_loo() {
		
	}

	//Create Note
	public void createCourseNote(){
		String clientId = ApexPages.currentPage().getParameters().get('clientId');
		String courseid = ApexPages.currentPage().getParameters().get('courseid');
		String noteSubj = ApexPages.currentPage().getParameters().get('noteSubj');
		String noteDesc = ApexPages.currentPage().getParameters().get('noteDesc');
		String firstInst = ApexPages.currentPage().getParameters().get('firstInst');
		
		Savepoint sp = Database.setSavepoint();
		try {
			IPFunctions.createCourseNote(clientId, courseid, noteSubj, noteDesc, firstInst);
			search();
		}catch(Exception e){
			ApexPages.addMessages(e);
			Database.rollback(sp);
		}
		finally{
			ApexPages.currentPage().getParameters().remove('clientId');
			ApexPages.currentPage().getParameters().remove('courseid');
			ApexPages.currentPage().getParameters().remove('noteSubj');
			ApexPages.currentPage().getParameters().remove('noteDesc');
			ApexPages.currentPage().getParameters().remove('firstInst');
		}
		
	}


	public string selectedAgencyGroup{get; set;}
	private list<String> searchAgencies {get;set;}
	public List<SelectOption> agencyGroupOptions {get;set;}
	
	public void changeGroup(){
		searchAgencies = new list<String>();
		Agencies = new list<SelectOption>();

		if(selectedAgencyGroup == 'all' && allFilters != null && allFilters.size()>0 && allFilters.get('allGroups') != null){
			for(SelectOption ag:allFilters.get('allGroups')){
				if(ag.getValue() != 'all'){
					for(SelectOption ct:allFilters.get(ag.getValue())){
						if(ct.getValue() != 'all'){
							Agencies.add(ct);
							searchAgencies.add(ct.getValue());

						}
					}
				}
			}
			Agencies.add(0, new SelectOption('all', '-- All --'));
		}else if(allFilters.get(selectedAgencyGroup) != null){

			Agencies = allFilters.get(selectedAgencyGroup);

			String paramAg = ApexPages.currentPage().getParameters().get('ag');

			for(SelectOption so : agencies){
				if(so.getValue() != 'all')
					searchAgencies.add(so.getValue());
				if(so.getValue() == paramAg)
					selectedAgency = paramAg;
			}//end for

			if(selectedAgency == null && Agencies.size()>0)
				selectedAgency = Agencies[0].getValue();
		}
 	}

	public string selectedAgency {get{if(selectedAgency == null) selectedAgency = 'all'; return selectedAgency;}set;}
	public list<SelectOption> agencies {get;set;}
		
	// public IPFunctions.CampusAvailability ca {get{if(ca==null) ca = new IPFunctions.CampusAvailability(); return ca;}set;}

	public String selectedSchool {get{if(selectedSchool==null) selectedSchool = 'all'; return selectedSchool;}set;}
	public  List<SelectOption> schoolOptions {get;set;}
	// 	get{
	// 		if(schoolOptions == null){
	// 			schoolOptions = new List<SelectOption>();

	// 			schoolOptions = ca.getAvlSchools(selectedAgency);

	// 		}
	// 		return schoolOptions;
	// 	}set;}

	private map<String,map<String,String>> campusMap {get;set;}
	public void changeSchool(){
		system.debug('selectedSchool ==> ' + selectedSchool);
		map<String,String> cpMap = campusMap.get(selectedSchool);
		list<String> cpName = new list<String>(cpMap.keySet());

		system.debug('cpName ==> ' + cpName);
		cpName.sort();

		campusOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
		for(String nm : cpName)
			campusOptions.add(new SelectOption(cpMap.get(nm), nm));


		system.debug('campusOptions ==> ' + campusOptions);

		if(selectedCampus==null)
			selectedCampus = 'all';
	}

	public String selectedCampus {get{if(selectedCampus==null) selectedCampus = 'all'; return selectedCampus;}set;}
	public List<SelectOption> campusOptions {get;set;}
	// 	get{
	// 		if(campusOptions == null && selectedSchool != 'all'){

	// 			campusOptions = new List<SelectOption>();
	// 			campusOptions = ca.getSchoolCampus(selectedSchool);
	// 		}

	// 		if(campusOptions == null && selectedSchool == 'all'){
	// 			campusOptions = new List<SelectOption>();
	// 			campusOptions.add(new SelectOption('all', '-- All --'));
	// 		}

	// 		return campusOptions;
	// 	}
	// 	set;
	// }

	public Contact dateFrom {
		get{
			if(dateFrom == null){

				dateFrom = new Contact();
				String vi = ApexPages.CurrentPage().getParameters().get('vi');
				
				if(vi != null && vi != '--')
					dateFrom.BirthDate = date.valueOf(vi);
			}
			return dateFrom;
		}
		set;
	}

	public Contact dateTo {
		get{
			if(dateTo == null){
				dateTo = new Contact();
				String vf = ApexPages.CurrentPage().getParameters().get('vf');
				
				if(vf != null && vf != '--')
					dateTo.BirthDate = date.valueOf(vf);
			}
			return dateTo;
		}
		set;
	}

	public RequestWrapper rWrapper {get;set;}
	public List<RequestWrapper> pendingRequests {get;set;}
	Map<String, RequestWrapper> packageRequests = new Map<String, RequestWrapper>();

	public void search(){

		pendingRequests = new List<RequestWrapper>();
		System.debug('==>isBOUser: '+isBOUser);
		System.debug('==>searchAgencies: '+searchAgencies);

		if(isBOUser && searchAgencies != null && searchAgencies.size()>0){

			List<Client_Course__c> packages = new List<Client_Course__c>();

			String sqlSelect = 'select Client__r.Name, Client__r.Status__c, Client__r.Email, Client__r.Visa_Expiry_Date__c, isPackage__c, course_package__c, School_Name__c, School_Id__c, Campus_Name__c, Campus_Id__c,Course_Name__c, Course_Length__c,Unit_Type__c, lastModifiedDate, Start_Date__c, End_Date__c, COE_Received_On__c, LOO_Requested_to_BO_By__c, LOO_Requested_to_BO_On__c, Enrolment_Form__r.Preview_Link__c, isOnlineApplication__c, Enrolment_Start_Date__c,  Enrolment_Activities__c, Client__r.Owner__r.Name, Client__r.Owner__r.Contact.Account.Name, Enrolment_Status__c, Booking_Number__c, isUrgentEnrolment__c, isSoftDeleted__c, SoftDeletedBy__c, isCancelled__c, Cancelled_On__c, LOO_Requested_to_BO_By__r.Contact.Account.Name, visa_type__c, ' +
									' (select Id,isFirstPayment__c, Number__c, Split_Number__c from client_course_instalments__r where Number__c = 1 AND (Split_Number__c = Null OR Split_Number__c = 1)), '+ 
									' (select id, CreatedBy.Name, CreatedDate, LastModifiedDate, LastModifiedBy.name, comments__c, subject__c from Custom_Notes_Tasks__r where isNote__c = true and isEnrolment__c = true order by LastModifiedDate desc)';
			String sqlFrom = 'from Client_Course__c where LOO_Requested_To_BO__c = true and LOO_Requested_TO_SC__c = false and  '; //LOO_Requested_to_BO_By__r.Contact.AccountID = :selectedAgency


			if(selectedAgency != 'all')
				sqlFrom += ' LOO_Requested_to_BO_By__r.Contact.AccountID = \'' + selectedAgency  +'\' AND campus_country__c = \'' + selectedCountry  +'\'';
			else
				sqlFrom += ' LOO_Requested_to_BO_By__r.Contact.AccountID in  ( \''+ String.join(searchAgencies, '\',\'') + '\' ) AND campus_country__c = \'' + selectedCountry +'\'';


			// if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
			if(selectedAgencyGroup != 'all')
				sqlFrom += ' AND LOO_Requested_to_BO_By__r.Contact.Account.ParentId = :selectedAgencyGroup ';

			if(selectedCampus!='all')
				sqlFrom += ' AND Campus_Id__c = :selectedCampus '; //Campus
			else if(selectedSchool!='all')
				sqlFrom += ' AND School_Id__c = :selectedSchool '; //School

			// if(selectedDepartment!='all')
			// 	sqlFrom += ' AND LOO_Requested_to_BO_By__r.Contact.Department__c = :selectedDepartment '; //Department

			if(dateFrom.BirthDate!=null)
				sqlFrom += ' AND Client__r.Visa_Expiry_Date__c >= '+ IPFunctions.FormatSqlDateIni(dateFrom.BirthDate); //Visa Expiry From

			if(dateTo.BirthDate!=null)
				sqlFrom += ' AND Client__r.Visa_Expiry_Date__c <= '+ IPFunctions.FormatSqlDateFin(dateTo.BirthDate); //Visa Expiry To

			if(keywords!=null && keywords != '')
				sqlFrom += ' AND Client__r.Name like \'%' +keywords+ '%\'  '; //Client Name

			sqlFrom += ' order by LOO_Requested_to_BO_On__c, course_package__c ';

			map<String,String> schoolMap = new map<String,String>();

			if(isLOO){

				System.debug('!@# ' + sqlSelect + sqlFrom);

				for(Client_Course__c course : Database.query(sqlSelect + sqlFrom)){

					if(course.Course_Package__c == null){
						RequestWrapper rw = new RequestWrapper();
						rw.contact = course.client__r;
						rw.course = course;
						rw.courseNotes = course.Custom_Notes_Tasks__r;
						rw.activities = buildEnrolmentHistory(course.Enrolment_Activities__c);
						packageRequests.put(course.id, rw);
						pendingRequests.add(rw);
						
					} else
						packages.add(course);

					schoolMap.put(course.School_Name__c, course.School_Id__c);

					if(!campusMap.containsKey(course.School_Id__c))
						campusMap.put(course.School_Id__c, new map<string,string>{course.Campus_Name__c => course.Campus_Id__c});
					else
						campusMap.get(course.School_Id__c).put(course.Campus_Name__c, course.Campus_Id__c);

				}

				for(Client_Course__c pkg : packages){
					if(packageRequests.containsKey(pkg.Course_Package__c))
						packageRequests.get(pkg.Course_Package__c).packageCourses.add(pkg);
				}


			} /*else if(isACC) {

				//AGENCY PRODUCTS
				String sql = 'select id, Product_Name__c, Category__c, Document__c, Document__r.Preview_link__c, BO_Requested__c, BO_Requested_By__c, BO_Requested_On__c, Client__r.Name, Sold_By_Agency__r.Name ' +
							' from client_product_service__c where BO_Requested__c = true and Category__c = \'Accommodation\' ';

				rWrapper = new RequestWrapper();
				for(client_product_service__c cps : Database.query(sql))
					rWrapper.products.add(cps);

				//Course FEES
				for(client_course_fees__c ccf : [select isSelected__c, fee_name__c, fee_type__c,client_course__c, client_course__r.Campus_Name__c, Document__c, Document__r.Preview_link__c,
														BO_Requested__c, BO_Requested_By__c, BO_Requested_On__c, client_course__r.Client__r.Name
												from client_course_fees__c where BO_Requested__c = true and fee_type__c = 'Accommodation' order by fee_type__c, fee_name__c])
					rWrapper.courseFees.add(ccf);

			} else if(isINS){

			}*/

			// for(Client_Course__c cc : requestLOO){
			// 	for(String str : cc.Enrolment_Form__r.Preview_link__c.split(';#;')){
			// 		String[] filestr = str.split('->');
			// 		files.get(cc.id).add(new File(filestr[1], filestr[0]));
			// 	}
			// }


			//School and Campus Filters
			schoolOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
			campusOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};

			list<String> allSchools = new list<String>(schoolMap.keySet());
			// list<String> allCampuses = new list<String>(campusMap.keySet());

			allSchools.sort();

			for(String sch : allSchools)
				schoolOptions.add(new SelectOption(schoolMap.get(sch), sch));
			
			changeSchool();

			if(selectedAgency != 'all')
				totals = getBOTotals(selectedCountry, selectedAgencyGroup, new list<String>{selectedAgency});
			else
				totals = getBOTotals(selectedCountry, selectedAgencyGroup, searchAgencies);

		}
	}

	public void onlineApplication(){
		String requestCourseID = ApexPages.currentPage().getParameters().get('courseid');
		String noteCommentsParam = ApexPages.currentPage().getParameters().get('noteComments');
		String firstInst = ApexPages.currentPage().getParameters().get('firstInst');

		if(packageRequests.containsKey(requestCourseID)){
			Client_Course__c cc = packageRequests.get(requestCourseID).course;

			List<Client_Course__c> updatecc = new List<Client_Course__c>();
			cc.LOO_Requested_TO_SC__c = true;
			cc.LOO_Requested_TO_SC_On__c = system.now();
			cc.LOO_Requested_TO_SC_By__c = UserInfo.getUserId();
			cc.isOnlineApplication__c = true;
			cc.Enrolment_Activities__c += EnrolmentManager.createActivity(EnrolmentManager.ACTIVITY_TYPE_SYSTEM, STATUS_LOO_REQUESTED_TO_SCH + ' (Online Application)', system.now(), currentUser.Name + ' ('+ currentUser.Account.Name+')');
			updatecc.add(cc);

			if(!packageRequests.get(requestCourseID).packageCourses.isEmpty()){
				for(Client_Course__c pack : packageRequests.get(requestCourseID).packageCourses){
					pack.LOO_Requested_TO_SC__c = true;
					pack.LOO_Requested_TO_SC_On__c = system.now();
					pack.LOO_Requested_TO_SC_By__c = UserInfo.getUserId();
					pack.isOnlineApplication__c = true;
					updatecc.add(pack);
				}
			}

			//Double check if the courses have been updated during this session
			String checkModifiedCourses = IPFunctionsGlobal.errorCoursesModified(updatecc);
			if(checkModifiedCourses == null){
				update updatecc;

				if(noteCommentsParam != null && noteCommentsParam.trim().length() > 0){
					Custom_Note_Task__c note = new Custom_Note_Task__c();
					note.client_course__c = cc.id;
					note.Related_Contact__c = packageRequests.get(requestCourseID).contact.id;
					note.isEnrolment__c = true;
					note.isNote__c = true;
					note.Comments__c = noteCommentsParam;
					note.subject__c = EnrolmentManager.STATUS_LOO_REQUESTED_TO_SCH;
					note.Instalment__c = firstInst;
					insert note;
				}
			}
			else{
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,checkModifiedCourses));
			}
		}
		search();
	}

	public void missingDocuments(){
		String requestCourseID = ApexPages.currentPage().getParameters().get('courseid');
		String noteCommentsParam = ApexPages.currentPage().getParameters().get('noteComments');
		String firstInst = ApexPages.currentPage().getParameters().get('firstInst');
		boolean isUrgent = ApexPages.currentPage().getParameters().get('isUrgent') != null ? boolean.valueof(ApexPages.currentPage().getParameters().get('isUrgent')) : false;

		system.debug('requestID: ' + requestCourseID);

		Savepoint sp = Database.setSavepoint();
		try{
			doMissingDocuments(requestCourseID, noteCommentsParam, isUrgent,  packageRequests, currentUser, firstInst);
			search();
		}catch(Exception e){
			ApexPages.addMessages(e);
			Database.rollback(sp);
		}

	}



	// FILTERS

	public string agenciesUnderBackoffice{get{if(agenciesUnderBackoffice == null) agenciesUnderBackoffice = ''; return agenciesUnderBackoffice;} set;}
	private list<String> agenciesBO;

	private map<String,list<SelectOption>> allFilters {get;set;}
	
	public void changeCountry(){
		list<String> boAgencies = boAgenciesMap.get(selectedCountry);
		agenciesUnderBackoffice = '';
		agenciesBO = new list<String>(); 
		
		integer counter = 0;
		for(String agBo : boAgencies){
			
			list<String> ag = agBo.split(';-;');
			if(counter>0)
				agenciesUnderBackoffice += ', ' + ag[1];
			else
				agenciesUnderBackoffice += ag[1];
			agenciesBO.add(ag[0]);
			counter++;
		}//end for

		retrieveAgencyGroupOptions();
	}

	public void retrieveAgencyGroupOptions(){
		selectedAgencyGroup = null;
		allFilters = ff.looBOFilters(selectedCountry, agenciesBO);

		if(allFilters.size()>0 && allFilters.get('allGroups') != null){
			agencyGroupOptions = allFilters.get('allGroups');

			if(agencyGroupOptions != null && agencyGroupOptions.size() > 0)
				agencyGroupOptions.add(0, new SelectOption('all', '-- All --'));

			String paramAg = ApexPages.currentPage().getParameters().get('gr');

			for(SelectOption so : agencyGroupOptions)
				if(so.getValue() == paramAg){
					selectedAgencyGroup = paramAg;
					break;
				}

			if(selectedAgencyGroup == null && agencyGroupOptions.size()>0)
				selectedAgencyGroup = agencyGroupOptions[0].getValue();

			changeGroup();
		}
		// schoolOptions = null;
   	}


}