public with sharing class payment_plan_instalments {
	
	public payment_plan_instalments() {
		retrievePaymentPlans();
	}

	public list<Payment_Plan__c> listPaymentPlansInstalments{get; set;}

	private void retrievePaymentPlans(){
		listPaymentPlansInstalments = new list<Payment_Plan__c>([Select id, Client__r.name, Value__c, Instalments__c, Instalments__r.Notes__c, Value_Remaining__c, Total_Paid__c, Client__c, Value_Paid__c, Currency_Code__c, 
											Currency_Paid__c, Currency_Rate__c, Value_in_Local_Currency__c, lastModifiedDate, lastModifiedBy.Name,   
											Reminder_Date__c from Payment_Plan__c where Instalments__c != null and Value_Paid__c = null order by createdDate]); 
	}
}