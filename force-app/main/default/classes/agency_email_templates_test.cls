@isTest
private class agency_email_templates_test {

    @testSetup static void setup() {
		TestFactory tf = new TestFactory();

		Account agencyGroup = tf.createAgencyGroup();
		Account agency = tf.createAgency();
	}

    static testMethod void testEmailTempalte() {

        Account agency = [SELECT Id FROM Account WHERE RecordType.Name = 'agency' limit 1];

		agency_email_templates t = new agency_email_templates(new Apexpages.Standardcontroller(agency));
        t.availableTags();
        t.newTemplate.Category__c = 'Send Student Email';
        t.saveTemplate();
        t.newTemplate.Template_Description__c = 'Test Template';
        t.saveTemplate();
        t.newTemplate.Template__c = 'Test Template';
        t.saveTemplate();

        System.debug('==>t.templates: '+t.templates);
        ApexPages.currentPage().getParameters().put('tpId', t.templates.get('Send Student Email')[0].id);
        t.editTemplate();
        t.cancelEdition();

        ApexPages.currentPage().getParameters().put('tpId', t.templates.get('Send Student Email')[0].id);
        t.deleteTemplate();


    }
}