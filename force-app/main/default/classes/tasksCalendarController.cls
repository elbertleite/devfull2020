public with sharing class tasksCalendarController {
    
    public list<calEvent> events {get;set;} //events showed on the calendar
	String dtFormat = 'yyyy-MM-dd'; //format accepted on the calendar
	
	
	public String actualView {get;set;}//actual view from the calendar
	public String nextDayBlock {get;set;}// next block of days that will be showed when clicked on Next button
	public Date dt {get;set;} //Ini day of the search
	public Date dtFin {get;set;} //Fin day of the search
	
	public String sFilterSubject {get;set;} //filter selected
	public String sFilterStatus {get;set;}  //filter selected
	public String sAssignedAgency {get;set;}  //filter selected
	public String sAssignedTo {get;set;}  //filter selected
	
	public List<SelectOption> subjects {get;set;}
	public List<SelectOption> status {get;set;}
	public List<SelectOption> assignedTo {get;set;}
	public List<SelectOption> assignedAgency {get;set;}
	
	
	public String listView {get;set;} //will define wt user will see when accssing the list tasks page
	
	public tasksCalendarController(){
		listView='list';
	}
	
	
	public class calEvent{
		 public String title {get;set;}
		 public Boolean allDay {get;set;}
		 public String startString {get;set;}
		 public String endString {get;set;}
		 public String description {get;set;}
	  	 public String url {get;set;}
		 public String className {get;set;}
		  public String relatedTo {get;set;}
		}
	
	private User user {get;set;}
	public pagereference pageLoad(){
		user = [Select u.ContactId, u.Contact.Name,Contact.Account.ParentId From User u where User.id = :UserInfo.getUserId() limit 1];
       	listAllSubjects();
		filterTaskStatus();
		filterAssignedAgency();
		filterAssignedTo();
		
		sFilterSubject='all';
		sFilterStatus='InProgress/Overdue';
		sAssignedTo = Userinfo.getUserId();
		
		Integer Year = Date.Today().Year();
		Integer Month = Date.Today().month();
		Date currentDate = Date.newInstance(Year,Month , 01);
		dt = dateIniSearch(currentDate,null);
		
		searchTasks();
		return null;
	}
	
	
	/*
	 * Check day Of Week and define the first day for the Search
	 */
	 public Date dateIniSearch(Date day, String typeView){
	 	Date dayS; 
	 	if(typeView != null && (typeView.equals('agendaDay') || typeView.equals('basicDay')))
	 		dayS = date.valueOf(day);
	 	else{
			dayS = date.valueOf(day);
			date dt1 = Date.newInstance(1985,7,7);
			Integer dayOfWeek = math.mod(dt1.daysBetween(dayS),7);
			
			dayS = dayS.addDays(-dayOfWeek);	
	 	}			
		if(typeView != null){
			if(typeView.equals('month')){
				dtFin = dayS.addDays(41);
			}else if(typeView.equals('basicWeek') || typeView.equals('agendaWeek')){
				dtFin = dayS.addDays(6);
			}else if(typeView.equals('agendaDay') || typeView.equals('basicDay')){
				dtFin = dayS;
			}
		}else{
			dtFin = dayS.addDays(41);
		}
		return dayS;	
	}
	
	
	/*
	 * When user click at the Month Button on the Calendar
	 */	
 	public void monthButton(){
		Integer intMonth = Integer.valueOf(ApexPages.currentPage().getParameters().get('pMonth'));
		Integer intYear = Integer.valueOf(ApexPages.currentPage().getParameters().get('pYear'));
		
		if(Test.isRunningTest()){
			intMonth = 8;
			intYear = 2014;
		}
		
		Date calendarDate = Date.newInstance(intYear,intMonth+1 , 01);
		dt = dateIniSearch(calendarDate,'month');
		
		searchTasks();	
	}
	
	/*
	 * When user click at the Day Button on the Calendar
	 */	
 	public void dayButton(){
		
		nextDayBlock = ApexPages.currentPage().getParameters().get('pNextDayBlock');
		if(Test.isRunningTest()){
			nextDayBlock= '2014-11-01';
		}
		
		dt = date.valueOf(nextDayBlock);
		dtFin = dt.addDays(1);
		
		searchTasks();	
	}	
	
	/*
	 * When user click at the Today Button on the Calendar with month view
	 */	
 	public void todayButton(){
		
		actualView = ApexPages.currentPage().getParameters().get('pMonth');
		if(Test.isRunningTest()){
			actualView= 'month';
		}
		
		//We put the inidate as the beginning of the moth to take the whole month tasks
		Integer Year = Date.Today().Year();
		Integer Month = Date.Today().month();
		
		Date currentDate = Date.newInstance(Year,Month , 01);
		dt = dateIniSearch(currentDate,null);
		
		dt = dateIniSearch(dt, actualView);
		
		searchTasks();	
	}	
	
	
	/*
	 * When user click at the Next Button on the Calendar
	 */	
 	public void nextButton(){
		
		actualView = ApexPages.currentPage().getParameters().get('pActualView');
		nextDayBlock = ApexPages.currentPage().getParameters().get('pNextDayBlock');
		if(Test.isRunningTest()){
			actualView= 'month';
			nextDayBlock = '2014-11-01';
		}
		
		dt = date.valueOf(nextDayBlock);
		dt = dateIniSearch(dt, actualView);
		
		searchTasks();	
	}	
	
	
	/*
	 * When user change the view from a List to a Calendar
	 */	
 	public void changedView(){
		
		if(listView.equals('calendar')){
			/*Integer Year = Date.Today().Year();
			Integer Month = Date.Today().month();
			Date currentDate = Date.newInstance(Year,Month , 01);
			dt = dateIniSearch(currentDate,null);
			*/
			pageLoad();
		}	
	}
	
	/*
	 * When an event is moved to other date
	 */	
 	@RemoteAction
 	public static void moveEvent(String id, String dateMoved){
		
		Date newDate = Date.valueOf(dateMoved);
		Custom_Note_Task__c movedTask = new Custom_Note_Task__c(id=id, Due_Date__c = newDate);
		update movedTask;
	}
		
		
	
	/*
	 * Get all the tasks related to the filters selected
	 */	
	public void searchTasks() {
		
		system.debug('Date ini====' +dt + 'Date Fin ====' +dtFin );
		
	    events = new list<calEvent>();
        list<Custom_Note_Task__c> tasks = new list<Custom_Note_Task__c>();
        
        String sql= 'SELECT Id, Due_Date__c, Subject__c, Related_Contact__r.Name,Related_To_Quote__r.Client__r.Name, Comments__c, Status__c FROM Custom_Note_Task__c WHERE isNote__c=false and Assign_To__c = \'' + sAssignedTo + '\'';
        
        //Check DATE
        if(dt == null){
        	sql+= ' and Due_Date__c = THIS_MONTH'; 
        }else{
        	sql+= ' and Due_Date__c >= :dt and Due_Date__c <= :dtFin '; 
        }
       	
        //Filter Status 
        if(sFilterStatus == 'InProgress/Overdue'){
        	sql += ' and Status__c !=\'Completed\'';
        }        
        else if(sFilterStatus == 'Completed'){
        	sql += ' and Status__c = \'Completed\' ';
        }
        else if(sFilterStatus=='Overdue'){
        	sql += ' and Due_Date__c  < ' +System.now().format('yyyy-MM-dd') + ' and Status__c !=\'Completed\'';
        }    
        //Filter subject
        if(sFilterSubject != 'all'){
        	sql += ' and Subject__c = \''+ sFilterSubject + '\' ';
        }
        
        
       sql+= ' Order by Due_Date__c'; 
        //TODO FAZER O ASSIGNED_TO
        //and Assign_To__c = :UserInfo.getUserId()';
        system.debug('SQL ==>' + sql);
    	tasks= Database.query(sql);
        
        for(Custom_Note_Task__c t : tasks){
            
            DateTime startDate= t.Due_Date__c;            
            DateTime todayDate = DateTime.now().date();
            
            calEvent task = new calEvent();
            
            if(t.Subject__c!=null && t.Subject__c !='') 
            	task.title = String.escapeSingleQuotes(t.Subject__c);
        	else
        		task.title = '';
            task.allDay = true;
            task.startString = startDate.format(dtFormat);
            task.endString = '';
            task.url = t.Id;
            
            if(t.Status__c=='Completed'){
	            task.className = 'task-completed';
            }else if(t.Status__c!='Completed' &&  startDate >= todayDate ){
	            task.className = 'task-inProgress';
            }else if(t.Status__c!='Completed' &&  startDate < todayDate ){
	            task.className = 'task-overdue';
            }			            
           	
            if(t.Related_Contact__r.Name != null){
           		task.relatedTo = t.Related_Contact__r.Name;
            }else if(t.Related_To_Quote__r.Client__r.Name!=null){
        		task.relatedTo = t.Related_To_Quote__r.Client__r.Name;
            }else {
            	task.relatedTo ='';
            }
            
            if(t.Comments__c==null)
            	t.Comments__c = '(no description)';
            task.description = String.escapeSingleQuotes(t.Comments__c);
           
            events.add(task);
        }
		
       
	}
	
	
	/*
	 * SELECT OPTION: Agency
	 */
	public List<SelectOption> filterAssignedAgency(){
		
		assignedAgency = new List<SelectOption>();
        
		User u = [Select Contact.AccountID, Contact.Account.ParentID from User Where id= :UserInfo.getUserId() limit 1];
		if(sAssignedAgency == null)
			sAssignedAgency = u.Contact.AccountID;
				
        /*for(AggregateResult ar : [Select Contact.AccountID, Contact.Account.Name from User where Contact.Account.ParentID = :u.Contact.Account.ParentID and Id != :UserInfo.getUserId() group by Contact.AccountID, Contact.Account.Name order by Contact.Account.Name]){
        	assignedAgency.add(new SelectOption( (String) ar.get('AccountID'), (String) ar.get('Name') ) );
        }*/
        
        IPFunctions.Sharing agencySharing = new IPFunctions.Sharing();
		assignedAgency=agencySharing.getAgenciesWithSharing();
		
        return assignedAgency;
	}
	
	public void filterAssignedByAgency(){
		filterAssignedTo();
		if(assignedTo.size()>0)
			sAssignedTo = assignedTo[0].getValue();
		system.debug('sAssignedTo==='  + sAssignedTo);
		searchTasks();
	}
	
	/*
	 * SELECT OPTION: Assigned To
	 */
	public List<SelectOption> filterAssignedTo(){
		assignedTo = new List<SelectOption>();
        
		/*list<User> users = [Select Id, Name from User where Contact.AccountID = :sAssignedAgency order by Name];
		
        for(User u : users){
          assignedTo.add(new SelectOption(u.Id, u.Name));
        }*/
        
        IPFunctions.agencyNoSharing agencyNoSharing = new IPFunctions.agencyNoSharing();
		
        assignedTo = agencyNoSharing.getEmployeesByAgencyNoSharing(sAssignedAgency);
        return assignedTo;
	}
	
	/*
	 * SELECT OPTION: Task Subjects
	 */
	public void listAllSubjects(){
		
		UserDetails.GroupSettings gs = new UserDetails.GroupSettings();		
        subjects = gs.getTaskSubjects(user.Contact.Account.ParentId);
        subjects.add(new SelectOption('Quote Follow Up', 'Quote Follow Up'));
        subjects.add(new SelectOption('Web Enquiry', 'Web Enquiry'));
		subjects.add(new SelectOption('RDStation Conversion', 'RDStation Conversion'));
        //subjects.add(new SelectOption('COE Received', 'COE Received'));
    	subjects.add(0,new SelectOption('all','--All--'));
        
	}
	
	/*
	 *SELECT OPTION: Task Status
	 */
	public List<SelectOption> filterTaskStatus(){
		status= new List<SelectOption>();
	    status.add(new SelectOption('all','--All--'));
        status.add(new SelectOption('InProgress/Overdue','In Progress + Overdue'));
        status.add(new SelectOption('Overdue','Overdue'));
        status.add(new SelectOption('Completed','Completed'));
        return status;
	}

}