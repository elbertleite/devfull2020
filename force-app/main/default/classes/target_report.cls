public with sharing class target_report {
    private User currentUser {get;set;}
  public string listDefaultColumns{get{if(listDefaultColumns == null) listDefaultColumns = 'none;'; return listDefaultColumns;} set;} //initialized with none because Discrepencies with ISBLANK and CONTAINS functions in Visualforce Page SF - bug

  public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}

  public target_report(){

    currentUser = [Select Id, Name, Aditional_Agency_Managment__c, Contact.Account.ParentId, Contact.AccountId, Contact.Department__c, Contact.Sales_Report_Default__c from User where id = :UserInfo.getUserId() limit 1];
    selectedAgency = currentUser.Contact.AccountId;

    if(currentUser.contact.Sales_Report_Default__c != null){
      salesReportOpt = new list<string>(currentUser.contact.Sales_Report_Default__c.split(';'));
      listDefaultColumns = currentUser.contact.Sales_Report_Default__c;
    }
    else {
      salesReportOpt = new list<string>();
    }

    if(!Schema.sObjectType.User.fields.Finance_Show_Target_Filters__c.isAccessible())
      salesReportOpt = new list<string>{'Received_By_On_Department','Enrolled_By_On','Discount','Commission_After_Discount','Covered_by_Agency','Keep_Fee'};

    searchCourses();
  }

  public list<string> salesReportOpt{get; set;}
  public List<selectOption> defaultSalesReportOpt{
    get{
      if(defaultSalesReportOpt == null){
        defaultSalesReportOpt = new List<selectOption>();
        Schema.DescribeFieldResult result = Schema.Contact.Sales_Report_Default__c.getDescribe();
        for (Schema.PicklistEntry ld:result.getPicklistValues())
          defaultSalesReportOpt.add(new SelectOption( ld.getValue(), ld.getLabel()));
      }
      return defaultSalesReportOpt;
    } 
    set;
  }

  public class sumTotals{

    public integer totItens{get{if(totItens == null) totItens = 0; return totItens;} set;}
    public decimal tuition{get{if(tuition == null) tuition = 0; return tuition;} set;}
    public decimal extraFee{get{if(extraFee == null) extraFee = 0; return extraFee;} set;}
    public decimal discount{get{if(discount == null) discount = 0; return discount;} set;}
    public decimal instalment{get{if(instalment == null) instalment = 0; return instalment;} set;}
    public decimal commission{get{if(commission == null) commission = 0; return commission;} set;}
    public decimal commissionDiscount{get{if(commissionDiscount == null) commissionDiscount = 0; return commissionDiscount;} set;}
    public decimal tax{get{if(tax == null) tax = 0; return tax;} set;}
    public decimal enrollAgency{get{if(enrollAgency == null) enrollAgency = 0; return enrollAgency;} set;}
    public decimal receiveAgency{get{if(receiveAgency == null) receiveAgency = 0; return receiveAgency;} set;}
    public decimal totalReceive{get{if(totalReceive == null) totalReceive = 0; return totalReceive;} set;}

    public decimal Deal{get{if(Deal == null) Deal = 0; return Deal;} set;}
    public decimal Scholarship{get{if(Scholarship == null) Scholarship = 0; return Scholarship;} set;}
    public decimal KeepFee{get{if(KeepFee == null) KeepFee = 0; return KeepFee;} set;}
    public void calculateTotals(decimal tuition, decimal extraFee, decimal discount, decimal instalment, decimal commission, decimal commissionDiscount, decimal tax, decimal enrollAgency, decimal receiveAgency, decimal totalReceive, decimal deal, decimal scholarship, decimal keepFee){
      this.totItens++;
      this.tuition += tuition;
      this.extraFee += extraFee;
      this.discount += discount;
      this.instalment += instalment;
      this.commission += commission;
      this.commissionDiscount += commissionDiscount;
      this.tax += tax;
      this.enrollAgency += enrollAgency;
      this.receiveAgency += receiveAgency;
      this.totalReceive += totalReceive;
      this.Deal += Deal;
      this.Scholarship += Scholarship;
      this.KeepFee += KeepFee;
       
    }
  }

    public map<string, list<client_course_instalment__c>> listCoursesDep{get{if(listCoursesDep == null) listCoursesDep = new map<string, list<client_course_instalment__c>>(); return listCoursesDep;} set;}
    public map<string,sumTotals> listCoursesTotal{get{if(listCoursesTotal == null) listCoursesTotal = new map<string,sumTotals>(); return listCoursesTotal;} set;}

    public map<string,string> departmentName{get{if(departmentName == null) departmentName = new map<string,string>(); return departmentName;} set;}

    public list<client_course_instalment__c> listCoursesExcel{get{if(listCoursesExcel == null) listCoursesExcel = new list<client_course_instalment__c>(); return listCoursesExcel;} set;}

    private map<string,client_course_instalment__c> mapFisrtValues;


    string sqlFilter = '';
    String  fromDate;
  String  toDate;
    public string returnQuery(){
      listCoursesDep = new map<string, list<client_course_instalment__c>>();
      listCoursesTotal = new map<string,sumTotals>();
      departmentName = new map<string,string>();

      String  fromDate = IPFunctions.FormatSqlDateIni(dateFilter.Original_Due_Date__c);
    String  toDate = IPFunctions.FormatSqlDateFin(dateFilter.Discounted_On__c);

    if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && currentUser.Aditional_Agency_Managment__c == null) //set the user to see only his own agency
      selectedAgency = currentUser.Contact.AccountId;

    sqlFilter = '';

      string sql = 'Select Balance__c, client_course__r.Cancelled_On__c, Total_Paid_School_Credit__c,  client_course__r.isCOE_Required__c, client_course__r.course_length_in_weeks__c, client_course__r.Client_Type__c, client_course__r.Agency_Admin_Fee__c, client_course__r.Campus_Name__c, client_course__r.Client__r.name, client_course__r.Commission_Tax_Rate__c, client_course__r.Commission_Tax_Value__c, client_course__r.Commission_Type__c, client_course__r.client__r.Nationality__c, ';
        sql += ' client_course__r.Course_Name__c, Commission__c, Commission_Confirmed_On__c, Commission_Paid_Date__c, Commission_Tax_Rate__c, Commission_Tax_Value__c, Commission_Value__c, Confirmed_Date__c, Discount__c, Discounted_By__c, Discounted_On__c, Due_Date__c, Extra_Fee_Discount_Value__c,  ';
        sql += ' Extra_Fee_Value__c, Instalment_Value__c, Number__c, Split_Number__c, Paid_To_School_By__c, Paid_To_School_On__c, Splited_From_Number__c, Status__c, Total_School_Payment__c, Tuition_Value__c, Received_Date__c, ';
        sql += ' client_course__r.Enroled_by_Agency__r.name, client_course__r.Enroled_by_User__r.name, client_course__r.Enrolment_Date__c, isSharedCommission__c, Received_by__r.name, Received_By_Agency__r.name, ';
        sql += ' Shared_Comm_Agency_Enroll__c, Shared_Comm_Agency_Enroll_Value__c, Shared_Comm_Agency_Receive__c, Shared_Comm_Agency_Receive_Value__c, Received_by_Department__c, Received_By_Department__r.name, ';
        sql += ' Shared_On_Enrolled_Agency__c, Shared_On_Received_Agency__c, Shared_On_Requested_Agency__c, ';
        sql += ' client_course__r.share_commission_request_agency__c, client_course__r.share_commission_request_agency__r.name, Shared_Comm_Agency_Request_Value__c, Shared_Comm_Agency_Request__c, client_course__r.share_commission_decision_status__c, isShareCommissionSplited__c, isReceived_Enrolled_Different_Agency__c, isRequestedShareCommission__c, isEnrolled_Requested_Same_Agency__c, isReceived_Requested_Same_Agency__c, isReceived_Enrolled_Same_Agency__c,';
        sql += ' isFirstPayment__c, isPDS__c, isPCS__c, isPFS__c, client_course__r.Campus_Course__r.Course__r.Course_Type__c, client_course__r.Course_Length__c, client_course__r.course_package__c, client_course__r.Free_Units__c, client_course__r.Free_Weeks__c, client_course__r.Unit_Type__c, ';
        sql += ' client_course__r.Client__r.owner__r.contact.account.name, client_course__r.Client__r.owner__r.name, client_course__r.Client__r.owner__r.contact.department__r.name, ';
        sql += ' Agent_Deal_Value__c, Agent_Deal_Name__c, Scholarship_Taken__c, Kepp_Fee__c, Covered_by_agency__c, ';
        sql += ' Client_Course__r.Campus_Country__c, ';
        sql += ' Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c,  Shared_Commission_Bank_Dep_Received__r.Received_Currency_Rate__c, Shared_Commission_Bank_Dep_Request__r.Received_Currency_Rate__c, Shared_Commission_Bank_Dep_Enrolled__r.Received_Currency_Rate__c, ';
        sql += ' Shared_Commission_Bank_Dep_Received__r.Received_Currency__c, Shared_Commission_Bank_Dep_Request__r.Received_Currency__c, Shared_Commission_Bank_Dep_Enrolled__r.Received_Currency__c, isShare_Commission__c, Shared_Commission_Bank_Dep_Received__r.Share_Commission_Number__c, ';
        sql += ' Pay_to_Agency__c, Shared_Comm_Pay_Agency_Value__c, Shared_Comm_Pay_Agency__c, Shared_On_Pay_Agency__c, Shared_Commission_Bank_Dep_Request__r.Share_Commission_Number__c, Shared_Commission_Bank_Dep_Pay_Agency__r.Share_Commission_Number__c, Shared_Commission_Bank_Dep_Enrolled__r.Share_Commission_Number__c, Pay_to_Agency__r.name, ';
        sql += ' Shared_Commission_Bank_Dep_Pay_Agency__r.Received_Currency_Rate__c, Shared_Commission_Bank_Dep_Pay_Agency__r.Received_Currency__c, ';
        //Amendments
        sql += ' isOverpayment_Amendment__c, isCommission_Amendment__c, isInstalment_Amendment__c,   Commission_Adjustment__c, Commission_Tax_Adjustment__c, School_Payment_Invoice__r.Agency_Currency_Rate__c, Amendment_Adjustment_Confirmed_On__c, original_instalment__r.isCommission_charged_on_amendment__c, original_instalment__r.Tuition_Value__c ';

        sql += ' FROM client_course_instalment__c where '; 
        
        sqlFilter += ' ((client_course__r.Enroled_by_Agency__c = \''+selectedAgency+'\' and Received_By_Agency__c != \''+selectedAgency+'\') or ';
        sqlFilter += ' (client_course__r.share_commission_request_agency__c = \''+selectedAgency+'\' and isRequestedShareCommission__c = true and client_course__r.share_commission_decision_status__c = \'Accepted\' and Received_By_Agency__c != \''+selectedAgency+'\' and client_course__r.Enroled_by_Agency__c != \''+selectedAgency+'\') or ';
        sqlFilter += ' (pay_to_agency__c != null AND pay_to_agency__c = \''+selectedAgency+'\' AND isPayToAgency_Diff_Enroll_Receive_Agency__c = TRUE) or ';// AND Shared_Commission_Bank_Dep_Pay_Agency__c = NULL) or ';
        sqlFilter += ' (Received_By_Agency__c = \''+selectedAgency+'\' and client_course__r.Enroled_by_Agency__c != \''+selectedAgency+'\') or (Received_By_Agency__c = \''+selectedAgency+'\' and client_course__r.Enroled_by_Agency__c = \''+selectedAgency+'\')) ';
      


    if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){

      sqlFilter += ' and ((client_course__r.Enroled_by_Agency__r.ParentId = \''+selectedAgencyGroup+'\' and Received_By_Agency__r.ParentId != \''+selectedAgencyGroup+'\') or ';
      sqlFilter += ' (Received_By_Agency__r.ParentId = \''+selectedAgencyGroup+'\' and client_course__r.Enroled_by_Agency__r.ParentId != \''+selectedAgencyGroup+'\') or (Received_By_Agency__r.ParentId = \''+selectedAgencyGroup+'\' and client_course__r.Enroled_by_Agency__r.ParentId = \''+selectedAgencyGroup+'\')) ';

    }

    if(SelectedPaymentType == 'first')
      sqlFilter += ' and (isFirstPayment__c = true or Required_for_COE__c = true or Splited_From__r.isFirstPayment__c = true or Splited_From__r.Required_for_COE__c = true or (client_course__r.isCOE_Required__c = false and number__c = 1 and client_course__r.course_package__c = null)) ';
    else if(SelectedPaymentType == 'firstPay')
      sqlFilter += ' and (isFirstPayment__c = true) ';
      
    else if(SelectedPaymentType == 'repay')
      sqlFilter += ' and (isFirstPayment__c = false and Required_for_COE__c = false and Splited_From__r.isFirstPayment__c = false and Splited_From__r.Required_for_COE__c = false) ';


    sqlFilter += ' and ((isPfs__c = false and isPds__c = false and isPcs__c = false and ( ';
        if(SelectedPeriod == 'range')
          sqlFilter += ' DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) >= '+ fromDate +' and  DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) <= '+toDate;
        else if(SelectedPeriod == 'THIS_WEEK')
          sqlFilter += ' Commission_Confirmed_On__c = THIS_WEEK ';
        else if(SelectedPeriod == 'LAST_WEEK')
          sqlFilter += ' Commission_Confirmed_On__c = LAST_WEEK ';
        else if(SelectedPeriod == 'NEXT_WEEK')
          sqlFilter += ' Commission_Confirmed_On__c = NEXT_WEEK ';
        else if(SelectedPeriod == 'THIS_MONTH')
          sqlFilter += ' Commission_Confirmed_On__c = THIS_MONTH ';
        else if(SelectedPeriod == 'LAST_MONTH')
          sqlFilter += ' Commission_Confirmed_On__c = LAST_MONTH ';  
        else if(SelectedPeriod == 'NEXT_MONTH')
          sqlFilter += ' Commission_Confirmed_On__c = NEXT_MONTH';
        sqlFilter += ' )) ';  

        // sqlFilter += ' or ((isPds__c = true or isPcs__c = true) and isPfs__c = false and (isFirstPayment__c = false and Required_for_COE__c = false and Splited_From__r.isFirstPayment__c = false and Splited_From__r.Required_for_COE__c = false and (client_course__r.isCOE_Required__c = false and client_course__r.course_package__c != null)) and  ( ';
        sqlFilter += ' or ((isPds__c = true or isPcs__c = true) and isPfs__c = false and (isFirstPayment__c = true or Required_for_COE__c = true or Splited_From__r.isFirstPayment__c = true or Splited_From__r.Required_for_COE__c = true or (client_course__r.isCOE_Required__c = false and number__c = 1 and client_course__r.course_package__c = null)) and  ( ';
        if(SelectedPeriod == 'range')
          sqlFilter += ' Due_Date__c >= '+ fromDate +' and Due_Date__c <= '+toDate;
        else if(SelectedPeriod == 'THIS_WEEK')
          sqlFilter += ' Due_Date__c = THIS_WEEK ';
        else if(SelectedPeriod == 'LAST_WEEK')
          sqlFilter += ' Due_Date__c = LAST_WEEK ';
        else if(SelectedPeriod == 'NEXT_WEEK')
          sqlFilter += ' Due_Date__c = NEXT_WEEK ';
        else if(SelectedPeriod == 'THIS_MONTH')
          sqlFilter += ' Due_Date__c = THIS_MONTH ';
        else if(SelectedPeriod == 'LAST_MONTH')
          sqlFilter += ' Due_Date__c = LAST_MONTH ';
        else if(SelectedPeriod == 'NEXT_MONTH')
          sqlFilter += ' Due_Date__c = NEXT_MONTH ';
        sqlFilter += ' )) ';

        

        //sqlFilter += ' ((isPfs__c = true or isPds__c = true or isPcs__c = true) and (isFirstPayment__c = true or Required_for_COE__c = true or Splited_From__r.isFirstPayment__c = true or Splited_From__r.Required_for_COE__c = true or (client_course__r.isCOE_Required__c = false and number__c = 1 and client_course__r.course_package__c = null)) and  ( ';
        sqlFilter += ' or (isPfs__c = true and (isFirstPayment__c = true or Required_for_COE__c = true or Splited_From__r.isFirstPayment__c = true or Splited_From__r.Required_for_COE__c = true or (client_course__r.isCOE_Required__c = false and number__c = 1 and client_course__r.course_package__c = null)) and  ( ';
        if(SelectedPeriod == 'range')
          sqlFilter += ' DAY_ONLY(convertTimezone(Received_Date__c)) >= '+ fromDate +' and DAY_ONLY(convertTimezone(Received_Date__c)) <= '+toDate;
        else if(SelectedPeriod == 'THIS_WEEK')
          sqlFilter += ' Received_Date__c = THIS_WEEK ';
        else if(SelectedPeriod == 'LAST_WEEK')
          sqlFilter += ' Received_Date__c = LAST_WEEK ';
        else if(SelectedPeriod == 'NEXT_WEEK')
          sqlFilter += ' Received_Date__c = NEXT_WEEK ';
        else if(SelectedPeriod == 'THIS_MONTH')
          sqlFilter += ' Received_Date__c = THIS_MONTH ';
        else if(SelectedPeriod == 'LAST_MONTH')
          sqlFilter += ' Received_Date__c = LAST_MONTH ';
        else if(SelectedPeriod == 'NEXT_MONTH')
          sqlFilter += ' Received_Date__c = NEXT_MONTH ';
        sqlFilter += ' )) ';

        

    sqlFilter += ' ) ';  
       
        
    if(selectedDepartment != 'all')
      sqlFilter+= ' AND Received_by_Department__c = \''+selectedDepartment +'\''; 
    
    // if(SelectedPaymentType == 'first')
    //   sqlFilter+= ' AND isFirstPayment__c = true ';   
    // else if(SelectedPaymentType == 'repay')
    //   sqlFilter+= ' AND isFirstPayment__c = false ';   
    
    if(selectedClientType != 'all')
      sqlFilter+= ' AND Client_Type__c = \''+selectedClientType +'\'';  
    
    sqlFilter += ' and (isMigrated__c = false or (isMigrated__c = true and PFS_Pending__c = true)) ';  


    sql += sqlFilter;
    sql += ' order by Received_Date__c, Commission_Confirmed_On__c ';

    System.debug('==>sql: '+sql);
    return sql;
    }

    public void searchCoursesExcel(){
      IPFunctions.SearchNoSharing ns = new IPFunctions.SearchNoSharing();
      listCoursesExcel = ns.NSInstalments(returnQuery());
      retrieveCoursesSummary(fromDate, toDate, sqlFilter);
    }

  public void searchCourses(){
    listDefaultColumns = 'none;'; //initialized with none because Discrepencies with ISBLANK and CONTAINS functions in Visualforce Page SF - bug
    listDefaultColumns += string.join(salesReportOpt,';');

    IPFunctions.SearchNoSharing ns = new IPFunctions.SearchNoSharing();
    for(client_course_instalment__c lc:ns.NSInstalments(returnQuery())){

      if(lc.Total_Paid_School_Credit__c == null || lc.Total_Paid_School_Credit__c < lc.Instalment_Value__c){

        decimal totReceived = 0;
        if(lc.client_course__r.Enroled_by_Agency__c == lc.Received_By_Agency__c && lc.client_course__r.Enroled_by_Agency__c == SelectedAgency)
          totReceived = lc.Commission_Value__c - lc.Discount__c;
        else if(lc.client_course__r.Enroled_by_Agency__c != lc.Received_By_Agency__c && lc.client_course__r.Enroled_by_Agency__c == SelectedAgency)
          totReceived = lc.Shared_Comm_Agency_Enroll_Value__c;
        else if(lc.client_course__r.Enroled_by_Agency__c != lc.Received_By_Agency__c && lc.Received_By_Agency__c == SelectedAgency)
          totReceived = lc.Shared_Comm_Agency_Receive_Value__c;

        if(!listCoursesDep.containsKey(lc.Received_by_Department__c)){
          departmentName.put(lc.Received_by_Department__c, lc.Received_by_Department__r.name);
          sumTotals st = new sumTotals();
          if(lc.client_course__r.Enroled_by_Agency__c != lc.Received_By_Agency__c && lc.client_course__r.Enroled_by_Agency__c == SelectedAgency)
            st.calculateTotals(lc.Tuition_Value__c, lc.Extra_Fee_Value__c, lc.Discount__c, lc.Instalment_Value__c, lc.Commission_Value__c, lc.Commission_Value__c - lc.Discount__c, lc.Commission_Tax_Value__c, lc.Shared_Comm_Agency_Enroll_Value__c, lc.Shared_Comm_Agency_Receive_Value__c, totReceived, lc.Agent_Deal_Value__c, lc.Scholarship_Taken__c, lc.Kepp_Fee__c);
          else
            st.calculateTotals(lc.Tuition_Value__c, lc.Extra_Fee_Value__c, lc.Discount__c, lc.Instalment_Value__c, lc.Commission_Value__c, lc.Commission_Value__c - lc.Discount__c, lc.Commission_Tax_Value__c, lc.Shared_Comm_Agency_Receive_Value__c, lc.Shared_Comm_Agency_Enroll_Value__c, totReceived, lc.Agent_Deal_Value__c, lc.Scholarship_Taken__c, lc.Kepp_Fee__c);
          listCoursesTotal.put(lc.Received_by_Department__c, st);
          listCoursesDep.put(lc.Received_by_Department__c, new list<client_course_instalment__c>{lc});
        }else{
          listCoursesDep.get(lc.Received_by_Department__c).add(lc);
          sumTotals st = listCoursesTotal.get(lc.Received_by_Department__c);
          if(lc.client_course__r.Enroled_by_Agency__c != lc.Received_By_Agency__c && lc.client_course__r.Enroled_by_Agency__c == SelectedAgency)
            st.calculateTotals(lc.Tuition_Value__c, lc.Extra_Fee_Value__c, lc.Discount__c, lc.Instalment_Value__c, lc.Commission_Value__c, lc.Commission_Value__c - lc.Discount__c, lc.Commission_Tax_Value__c, lc.Shared_Comm_Agency_Enroll_Value__c, lc.Shared_Comm_Agency_Receive_Value__c, totReceived, lc.Agent_Deal_Value__c, lc.Scholarship_Taken__c, lc.Kepp_Fee__c);
          else
            st.calculateTotals(lc.Tuition_Value__c, lc.Extra_Fee_Value__c, lc.Discount__c, lc.Instalment_Value__c, lc.Commission_Value__c, lc.Commission_Value__c - lc.Discount__c, lc.Commission_Tax_Value__c, lc.Shared_Comm_Agency_Receive_Value__c, lc.Shared_Comm_Agency_Enroll_Value__c, totReceived, lc.Agent_Deal_Value__c, lc.Scholarship_Taken__c, lc.Kepp_Fee__c);
          listCoursesTotal.put(lc.Received_by_Department__c,st);
        }
      }
    }

       retrieveCoursesSummary(fromDate, toDate, sqlFilter);
    }

    public AggregateResult summaryValues{get; set;}
    public void retrieveCoursesSummary(string fromDate, string toDate, string sqlFilter){
    string sql = 'Select count(id) totItens, SUM(Tuition_Value__c) totTuition, SUM(Extra_Fee_Value__c) totFee, SUM(Instalment_Value__c) totInstall, SUM(Commission_Value__c) totCommission, SUM(Discount__c) totDiscount, SUM(Commission_Tax_Value__c) TotTax, ';
    sql += ' SUM(Shared_Comm_Agency_Enroll_Value__c) totCommEnroll, SUM(Shared_Comm_Agency_Receive_Value__c) totCommReceive ';
    sql += '  from client_course_instalment__c where ';

    sql += sqlFilter;

    IPFunctions.SearchNoSharing ns = new IPFunctions.SearchNoSharing();
    summaryValues = ns.NSAggregateSingle(sql);
  }



    public client_course_instalment__c dateFilter{
    get{
      if(dateFilter == null){
        dateFilter = new client_course_instalment__c();
        Date myDate = Date.today();
        Date weekStart = myDate.toStartofWeek();
        dateFilter.Original_Due_Date__c = weekStart;
        dateFilter.Discounted_On__c = dateFilter.Original_Due_Date__c.addDays(6);
      }
      return dateFilter;
    }
    set;
  }

    public string SelectedPeriod{get {if(SelectedPeriod == null) SelectedPeriod = 'THIS_WEEK'; return SelectedPeriod; } set;}
    private List<SelectOption> periods;
    public List<SelectOption> getPeriods() {
        if(periods == null){
            periods = new List<SelectOption>();
            periods.add(new SelectOption('THIS_WEEK','This Week'));
            periods.add(new SelectOption('LAST_WEEK','Last Week'));
            periods.add(new SelectOption('THIS_MONTH','This Month'));
            periods.add(new SelectOption('LAST_MONTH','Last Month'));
            periods.add(new SelectOption('range','Range'));
        }
        return periods;
    }

    public string SelectedPaymentType{get {if(SelectedPaymentType == null) SelectedPaymentType = 'first'; return SelectedPaymentType; } set;}
    private List<SelectOption> commissionType;
    public List<SelectOption> getcommissionType() {
        if(commissionType == null){
            commissionType = new List<SelectOption>();
            //commissionType.add(new SelectOption('all','-- All --'));
            commissionType.add(new SelectOption('first','First Instalments'));

      commissionType.add(new SelectOption('firstPay','First Payments'));

      if(Schema.sObjectType.User.fields.Finance_Show_Target_Filters__c.isAccessible())
              commissionType.add(new SelectOption('repay','Repayments'));
        }
        return commissionType;
    }

   
    private string ContactId = [Select ContactId from user where id = :UserInfo.getUserId() limit 1].ContactId;
    private contact userDetails = [Select AccountId, Account.ParentId from Contact where id = :ContactId limit 1];
    public string selectedAgencyGroup{
      get{
        if(selectedAgencyGroup == null)
          selectedAgencyGroup = currentUser.Contact.Account.ParentId;
        return selectedAgencyGroup;
      }
      set;
    }

   public List<SelectOption> agencyGroupOptions {
      get{
         if(agencyGroupOptions == null){

           agencyGroupOptions = new List<SelectOption>();
           if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group
            for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:currentUser.Contact.AccountId order by Parent.Name]){
               agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
            }
           }else{
             for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
               agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
            }
           }
         }
         return agencyGroupOptions;
    }
    set;
   }

  public String selectedAgency {get;set;}
  public List<SelectOption> agencyOptions {
    get{
         if(selectedAgencyGroup != null){
           agencyOptions = new List<SelectOption>();
        if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
          for(Account a: [Select Id, Name from Account where id =:currentUser.Contact.AccountId order by name]){
            if(selectedAgency=='')
              selectedAgency = a.Id;
            agencyOptions.add(new SelectOption(a.Id, a.Name));
          }
          if(currentUser.Aditional_Agency_Managment__c != null){
						for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
							agencyOptions.add(new SelectOption(ac.id, ac.name));
						}
					}
        }else{
          for(Account a: [Select Id, Name from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
            agencyOptions.add(new SelectOption(a.Id, a.Name));
            if(selectedAgency=='')
              selectedAgency = a.Id;
          }
        }
      }
         return agencyOptions;
      }
      set;
  }

  public string selectedDepartment {get{if(selectedDepartment==null) selectedDepartment = 'all'; return selectedDepartment;}set;}
  private set<Id> allDepartments {get;set;}
  public list<SelectOption> getDepartments(){
    List<SelectOption> result = new List<SelectOption>();
    allDepartments = new set<Id>();
    result.add(new SelectOption('all', '-- All --'));
    if(selectedAgency!=null)
      for(Department__c d: [Select Id, Name from Department__c where Agency__c = :selectedAgency and Inactive__c = false]){
        result.add(new SelectOption(d.Id, d.Name));
        allDepartments.add(d.Id);
      }

    return result;
  }

  public string selectedClientType {get{if(selectedClientType==null) selectedClientType = 'all'; return selectedClientType;}set;}
  public list<SelectOption> getClientType(){
    List<SelectOption> result = new List<SelectOption>();
    result.add(new SelectOption('all', '-- All --'));
    Schema.DescribeFieldResult fieldResult = client_course__c.Client_Type__c.getDescribe();
    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
    for( Schema.PicklistEntry f : ple){
      result.add(new SelectOption(f.getLabel(), f.getValue()));
    }
    return result;
  }

  public PageReference generateExcel(){
    PageReference pr = Page.target_report_excel;
    pr.setRedirect(false);
    return pr;
  }

}