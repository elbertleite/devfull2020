@RestResource(urlMapping = '/v1/agencies/*')
global class API_v1_agencies extends API_Manager {
	
	
	 @HttpGet
	global static List<Agency> doGet(){
		
		RestRequest request = RestContext.request;
		
		System.debug('/v1/agencies request: ' + request);
		System.debug('UserInfo.getUserType(): ' + UserInfo.getUserType());
		try {
			String globalLink;
			if(UserInfo.getUserType() != 'Guest')
				globalLink = [select Contact.Account.Global_Link__c from User where id =:UserInfo.getUserId() limit 1].Contact.Account.Global_Link__c;
			else if(api_manager.getRequestParam(request, 'global').equalsIgnoreCase('ip'))
				globalLink = [select id from account where (name like '%IP%' or name like '%Information Planet%') and RecordType.name = 'Global Link'].id;				
				
			system.debug('@globalLink: ' + globalLink);
			if(globalLink != null){
				
				API_v1_agenciesHandler.NoSharing handler = new API_v1_agenciesHandler.NoSharing();
				return handler.getAllAgencies(globalLink);
				
			} else return null;
				
		} catch (Exception e) {			
			api_manager.sendExceptionEmail(api_manager.getExceptionMessage(e));
			RestContext.response.statusCode = api_manager.CODE_INTERNAL_SERVER_ERROR;
			return null;
		}
		
		
	}
    
}