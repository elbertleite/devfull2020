/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class scCloneSchoolCourse_test {

    static testMethod void myUnitTest() {
        
        TestFactory tf = new TestFactory();
	    Account agency = tf.createAgency();

	    Account school = tf.createSchool();
	    Account campus = tf.createCampus(school, agency);
	    Account newCampus = tf.createCampus(school, agency);
	    Course__c course = tf.createCourse();
	    Course__c course2 = tf.createCourse();
	    Course__c lCourse = tf.createLanguageCourse();
	    Course__c lCourse2 = tf.createLanguageCourse();
	    course.School__c = school.id;
	    course2.School__c = school.id;
	    lCourse.School__c = school.id;
	    lCourse2.School__c = school.id;
	    Course__c[] courses = new Course__c[]{course, course2, lCourse, lCourse2};
	    update courses;
	    
	    tf.createCampusCourse(campus, course);
	    tf.createCampusCourse(newCampus, course2);
	    tf.createCampusCourse(campus, lCourse);
	    tf.createCampusCourse(campus, lCourse2);
	    
		Test.startTest();
		
		ApexPages.currentPage().getParameters().put('cpid', newCampus.id);
		ApexPages.currentPage().getParameters().put('id', school.id);
		
		scCloneSchoolCourse sc = new scCloneSchoolCourse();
		sc.setParams();
		sc.getCampuses(); 
		sc.selectedCampus = campus.id;
		sc.searchCourses();
		
		for(String cat : sc.courses.keySet())
			for(String tp : sc.courses.get(cat).keySet())
				for(Campus_Course__c cc : sc.courses.get(cat).get(tp))
					cc.is_selected__c = true;
					
		sc.saveCourses();
		sc.cancel();
		
	    Test.stopTest();
	    
	    
	    
	    
        
    }
}