public without sharing class school_page {

    public Account accSelected{get;set;}

    public List<Account> schoolsFromGroup{get;set;}
    public Map<String, List<Account>> campusFromSchool{get;set;}

    public String idGroup{get;set;}
    public String idSchool{get;set;}
    public String idCampus{get;set;}
    public String type{get;set;}
    public String campusCurrency{get;set;}
    public String urlLogo{get;set;}

    public Boolean updateSchoolNewLogo{get{if(updateSchoolNewLogo == null) updateSchoolNewLogo = false; return updateSchoolNewLogo;} set;}
    
    public list<SelectOption> allCountries {get;set;}
    public list<SelectOption> listSchoolsGroup {get;set;}

    public List<String> savedNearTo{get;set;}
    public List<String> savedWhyChooseThisCampus{get;set;}
    public List<String> savedFeatures{get;set;}
    public List<IPClasses.SavedItemCampus> savedNationalities{get;set;}
    public List<IPClasses.SavedItemCampus> savedSocialNetworks{get;set;}
    public List<IPClasses.SavedItemCampus> savedVideos{get;set;}
    public List<IPClasses.SavedItemCampus> savedPathways{get;set;}

    public String pageSelected{get;set;}
    public String sectionSelected{get;set;}

    public Boolean edit{get{if(edit == null) edit = false; return edit;} set;}

    public Boolean allowAccessToFullMenu{get;set;}

    public String uploadLogoName{get;set;}
    public Integer uploadLogoSize{get;set;}
    public Blob uploadFileLogo{get;set;}

    public Integer totalSchoolCasesOpen{get;set;}

    public school_page() {
        String id = ApexPages.currentPage().getParameters().get('id');
        campusFromSchool = new Map<String, List<Account>>();
        loadDetails(id);
        type = accSelected.RecordType.Name;
        String queryCountSchoolCases;
        if(type == 'School'){
            idGroup = accSelected.parent.ID;
            idSchool = id;
            campusFromSchool.put(accSelected.id, new List<Account>());
        }else if(type == 'Campus'){
            idGroup = accSelected.Parent.Parent.ID;
            idSchool = accSelected.Parent.ID;
            idCampus = id;
            loadCampusCurrency();
            loadExtraItens();
        }else if(type == 'School Group'){
            idGroup = accSelected.ID;
        }

        for(Account acc : [SELECT ID, Name, Parent.ID, BillingCountry, Main_Campus__c FROM Account WHERE RecordType.Name = 'Campus' AND Parent.Parent.ID = :idGroup ORDER BY Name ASC]){
            if(!campusFromSchool.containsKey(acc.Parent.ID)){
                campusFromSchool.put(acc.Parent.ID, new List<Account>());
            }
            campusFromSchool.get(acc.Parent.ID).add(acc);
        }

        schoolsFromGroup = new List<Account>();
        for(Account school : [SELECT ID, Name, BillingCountry FROM Account WHERE RecordType.Name = 'School' AND Parent.ID = :idGroup ORDER BY Name ASC]){
            schoolsFromGroup.add(school);
        }
        
        if(!String.isEmpty(idSchool)){
            totalSchoolCasesOpen = pmc_school_issues.getTotalCasesOpen(idSchool);
        }

        if(Test.isRunningTest()){
            pageSelected = 'school_campus_page';
        }else{
            pageSelected = ApexPages.currentPage().getUrl().substringBetween('apex/', '?');
        }
        sectionSelected = retrieveSectionSelected(pageSelected);

        uploadLogoName = null;
        uploadLogoSize = null;
        uploadFileLogo = null;

        allowAccessToFullMenu = !PMCHelper.hasUserRestrictedAccessPMCFunctionalities(); 
    }

    public void uploadSchoolLogo(){
        try{
            Datetime now = Datetime.now();
            String docBody = EncodingUtil.base64Encode(uploadFileLogo);
            String bucket = IPFunctions.getBucket();
            String accID = accSelected.RecordType.Name == 'School' ? accSelected.ID : accSelected.Parent.ID;
            String filePath = 'schools/' + accID + '/logo/' + uploadLogoName;

            system.debug('FILEPATH '+filePath);

            AWSKeys credentials = new AWSKeys(IPFunctions.awsCredentialName);
            S3.AmazonS3 as3 = new S3.AmazonS3(credentials.key,credentials.secret);

        
        //Store the Canonical User Id for your account

            Boolean putObjResult;
            if(!Test.isRunningTest()){
                S3.ListAllMyBucketsResult allBuckets = as3.ListAllMyBuckets(as3.key,now,as3.signature('ListAllMyBuckets', now));
                String OwnerId = allBuckets.Owner.Id;   
                putObjResult = as3.PutObjectInline_ACL(bucket, filePath , null, docBody, uploadLogoSize, 'public-read', as3.key, now, as3.signature('PutObjectInline', now), as3.secret, OwnerId, null);
            }else{
                putObjResult = true;
            }
            if(putObjResult){
                String previewLink = 'https://s3.amazonaws.com/' + bucket + '/' + filePath;

                Account acc = new Account(id = accID);
                acc.Logo__c = previewLink;
                acc.Campus_Photo_URL__c = null;
                update acc;

                Long sum = 0;
                for(Integer i = 0; i < 1000000l; i++){  //HOLD THE RESPONSE WHILE AMAZON S3 ATTACHES THE FILE AND THE SERVER UPDATES THE SCHOOL ACCOUNT.
                    sum = sum + 1;
                }
                sum = null;
                docBody = null;
                uploadLogoName = null;
                uploadLogoSize = null;
                uploadFileLogo = null;
            }
            
        }catch(Exception ex){
            system.debug('ERROR UPLOADING LOGO '+ex);
        }

    }

    public String retrieveSectionSelected(String pageSelected){
        String section = null;
        if(pageSelected == 'pmccoursesearch'){
            section = 'Courses Search';
        }else{
            if(type == 'Campus'){
                String [] campusInformationPages = new String [] {'school_finance_info','school_campus_page','school_campus_pictures','school_campus_payment_dates','school_dashboard','school_campus_showcase','schools_payment_rules'};
                if(campusInformationPages.contains(pageSelected)){
                    section = 'Campus Information';
                }else{
                    section = 'Courses';
                }
            }else if(type == 'School'){
                section = null;
            }
        }
        return section;
    }

    public void deleteSchoolLogo(){
        try{
            /*AWSKeys credentials = new AWSKeys(IPFunctions.awsCredentialName);
            String bucketName = IPFunctions.isOrgBucket() ? Userinfo.getOrganizationId().toLowerCase() : Userinfo.getOrganizationId();
            String entryFileName = this.accSelected.RecordType.Name == 'School' ? this.accSelected.ID : this.accSelected.Parent.ID;
            String folder = urlLogo.split(bucketName)[1];
            if(!String.isEmpty(folder)){
                folder = folder.substring(0, folder.lastIndexOf('/')+1);
                folder = folder.removeStart('/').removeEnd('/');

            }
            system.debug('THE FOLDER ' + folder);
            system.debug('THE BUCKET ' + bucketName);
            system.debug('THE FILE ' + entryFileName);

            Boolean deleted = AWSHelper.deleteObjectFromS3(credentials.key, credentials.secret, bucketName, folder, entryFileName);
            
            /*if(deleted){
                system.debug('PICTURE SUCCESSFULLY DELETED. ');
                this.client.PhotoClient__c = null;
                update this.client;
            }else{
                system.debug('THE PICTURE WASN\'T DELETED FROM AMAZON. ');
            }*/

            Account acc = new Account(ID = accSelected.RecordType.Name == 'School' ? accSelected.ID : accSelected.Parent.ID);
            acc.Logo__c = null;
            acc.Campus_Photo_URL__c = null;

            update acc;

        } catch (Exception e){          
            system.debug('Error on deleteImage() ===> ' + e.getLineNumber() + ' <=== '+e.getMessage());     
        }
    }

    public void loadCampusCurrency(){
        for(SelectOption op: xCourseSearchFunctions.getCurrencies()){
            if(op.getValue() == accSelected.Account_Currency_Iso_Code__c){
                campusCurrency = op.getLabel();
                break;
            }
        }
    }

    public void loadExtraItens(){
        if(!String.isEmpty(accSelected.Near_to__c)){
            savedNearTo = new List<String>();
            for(String value : accSelected.Near_to__c.split('!#!')){
                if(!String.isEmpty(value)){
                    savedNearTo.add(value);
                }
            }
        }
        if(!String.isEmpty(accSelected.Why_choose_this_campus__c)){
            savedWhyChooseThisCampus = new List<String>();
            for(String value : accSelected.Why_choose_this_campus__c.split('!#!')){
                if(!String.isEmpty(value)){
                    savedWhyChooseThisCampus.add(value);
                }
            }
        }
        if(!String.isEmpty(accSelected.Features__c)){
            savedFeatures = new List<String>();
            for(String value : accSelected.Features__c.split('!#!')){
                if(!String.isEmpty(value)){
                    savedFeatures.add(value);
                }
            }
        }
        if(!String.isEmpty(accSelected.Nationality_Mix__c)){
            savedNationalities = new List<IPClasses.SavedItemCampus>();
            IPClasses.SavedItemCampus item;
            for(String value : accSelected.Nationality_Mix__c.split('!#!')){
                if(!String.isEmpty(value)){
                    item = new IPClasses.SavedItemCampus();
                    item.item = value.split('>')[0];
                    item.value = value.split('>')[1];
                    savedNationalities.add(item);
                }
            }
        }
        if(!String.isEmpty(accSelected.Videos__c)){
            savedVideos = new List<IPClasses.SavedItemCampus>();
            IPClasses.SavedItemCampus item;
            for(String value : accSelected.Videos__c.split(';')){
                if(!String.isEmpty(value)){
                    item = new IPClasses.SavedItemCampus();
                    item.item = value.split('>')[0];
                    item.value = value.split('>')[1];
                    savedVideos.add(item);
                }
            }
        }
        if(!String.isEmpty(accSelected.Social_network_url__c)){
            savedSocialNetworks = new List<IPClasses.SavedItemCampus>();
            IPClasses.SavedItemCampus item;
            for(String value : accSelected.Social_network_url__c.split(';')){
                if(!String.isEmpty(value)){
                    item = new IPClasses.SavedItemCampus();
                    item.item = value.split('>')[0];
                    item.value = value.split('>')[1];
                    savedSocialNetworks.add(item);
                }
            }
        }
        if(!String.isEmpty(accSelected.School_Pathways__c)){
            savedPathways = (List<IPClasses.SavedItemCampus>) JSON.deserialize(accSelected.School_Pathways__c, List<IPClasses.SavedItemCampus>.class);
        }

    }

    public void loadDetails(String id){
        system.debug('THE ID '+id);
        accSelected = Database.query('SELECT ID, Name, Campus_Photo_URL__c, Parent.Campus_Photo_URL__c, Logo__c, Parent.Name, Parent.ID, Parent.Logo__c, Parent.Parent.ID, RecordType.Name, Nickname__c, Registration_name__c, Type_of_Business_Number__c, Business_Number__c, Year_established__c, BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode, ShippingCountry, ShippingState, ShippingCity, ShippingStreet, ShippingPostalCode, Type_of_Institution__c, Course_Types_Offered__c, Medical_Insurance_Company__c, BusinessEmail__c, Pathways_URL__c, School_campus_category__c, Reason_Disabled_Campus__c, Account_Currency_Iso_Code__c, Main_Campus__c, Disabled_Campus__c, ShowCaseOnly__c, Website, Phone, Students_per_class__c, Total_Number_of_Students__c, General_Description__c, Near_to__c, Why_choose_this_campus__c, Features__c, Nationality_Mix__c, Videos__c, Social_network_url__c, School_Pathways__c FROM Account WHERE ID = :id');

        if(accSelected.RecordType.Name != 'School Group'){
            if(accSelected.RecordType.Name == 'School'){
                if(!String.isEmpty(accSelected.Campus_Photo_URL__c)){
                    urlLogo = accSelected.Campus_Photo_URL__c;
                }else{
                    urlLogo = accSelected.Logo__c;
                }
            }else{
                if(!String.isEmpty(accSelected.Parent.Campus_Photo_URL__c)){
                    urlLogo = accSelected.Parent.Campus_Photo_URL__c;
                }else{
                    urlLogo = accSelected.Parent.Logo__c;
                }
            }       
        }
    }

    /*public void updateSchoolNewLogo(){
        String idUpdate = accSelected.RecordType.Name == 'School' ? accSelected.ID : accSelected.Parent.ID;
        Account acc = new Account(ID = idUpdate);
        acc.Logo__c = urlLogo;
        update acc;
    }*/
 
    public void saveDetails(){
        try{
            upsert accSelected;
            //openCloseForEdition();
        }catch(Exception e){
           system.debug('Error on saveDetails() ===> ' + e.getLineNumber() + ' <=== '+e.getMessage());
        }
    }

    public void openCloseForEdition(){
        this.edit = !edit;
        if(this.edit){
            listSchoolsGroup  = new list<SelectOption>(IPFunctions.getSchoolsGroup());
            allCountries = new list<SelectOption>(IPFunctions.getAllCountries());
        }
    }
}