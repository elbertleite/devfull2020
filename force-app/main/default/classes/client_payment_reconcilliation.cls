public with sharing class client_payment_reconcilliation {
	
	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}
	
	public client_payment_reconcilliation() {
		searchPayments();
	}

	//Find All Client Payments that fit the criteria
	public void searchPayments(){

		String fromDate = IPFunctions.FormatSqlDateIni(dateFilter.Original_Due_Date__c);
		String toDate = IPFunctions.FormatSqlDateFin(dateFilter.Discounted_On__c);

		String sql = 'SELECT Id, client_product_service__c, client_course_instalment__c, Invoice__c, payment_plan__c FROM client_course_instalment_payment__c WHERE ';

		//Received Agency
		sql += ' Received_By_Agency__c = \'' + selectedAgency +'\' AND Received_On__c != null ';

		//Received Group
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
			sql+= ' AND Received_By_Agency__r.ParentId = \'' +selectedAgencyGroup +'\'';

		//Payment Date
		if(dateCriteria == 'rc'){
			if(SelectedPeriod == 'range')
				sql += ' AND Date_Paid__c >= '+ fromDate + ' AND  Date_Paid__c <= '+ toDate ;
			else if(SelectedPeriod == 'TODAY')
				sql += ' AND Date_Paid__c = TODAY ';
			else if(SelectedPeriod == 'THIS_WEEK')
				sql += ' AND Date_Paid__c = THIS_WEEK ';
			else if(SelectedPeriod == 'THIS_MONTH')
				sql += ' AND Date_Paid__c = THIS_MONTH ';
			else if(SelectedPeriod == 'unc'){
				sql += ' AND Confirmed_Date__c = null ';
			}
		}
		// Confirmed Date
		else{
			if(SelectedPeriod == 'range')
				sql += ' AND DAY_ONLY(convertTimezone(Confirmed_Date__c)) >= '+fromDate+' AND  DAY_ONLY(convertTimezone(Confirmed_Date__c)) <= '+toDate;
			else if(SelectedPeriod == 'TODAY')
				sql += ' AND Confirmed_Date__c = TODAY ';
			else if(SelectedPeriod == 'THIS_WEEK')
				sql += ' AND Confirmed_Date__c = THIS_WEEK ';
			else if(SelectedPeriod == 'THIS_MONTH')
				sql += ' AND Confirmed_Date__c = THIS_MONTH ';
			else if(SelectedPeriod == 'unc'){
				sql += ' AND Confirmed_Date__c = null ';
			}
		}

		sql += ' AND Payment_Type__c != \'PDS\' ';
        sql += ' AND Payment_Type__c != \'PCS\' ';
        sql += ' AND Payment_Type__c != \'OFFSHORE\' ';
        sql += ' AND Payment_Type__c != \'School Credit\' ';
		
		if(selectedPaymentTypes != 'all')
			sql += ' AND Payment_Type__c = \'' + selectedPaymentTypes + '\'';
			
			
		sql += ' AND (client_course_instalment__c = NULL OR client_course_instalment__r.isMigrated__c = FALSE) ';


		system.debug('payment search==>' +  sql);

		set<Id> inst = new set<Id>();
		set<Id> prod = new set<Id>();
		set<Id> inv = new set<Id>();
		set<Id> plan = new set<Id>();

		//Get all objects related to the payment (Instalment, Product, Invoice)
		for(client_course_instalment_payment__c ccip : Database.query(sql))
			//Payment for instalment
			if(ccip.client_course_instalment__c != null)
				inst.add(ccip.client_course_instalment__c);

			//Payment for product
			else if(ccip.client_product_service__c != null)
				prod.add(ccip.client_product_service__c);

			//Payment for Invoice
			else if(ccip.Invoice__c != null)
				inv.add(ccip.Invoice__c);

			//Payment Plan
			else if(ccip.payment_plan__c != null)
				plan.add(ccip.payment_plan__c);


		list<client_course_instalment__c> lInst = new list<client_course_instalment__c>();
		list<client_product_service__c> lProd = new list<client_product_service__c>();
		list<Invoice__c> lInv = new list<Invoice__c>();
		list<payment_plan__c> lPlan = new list<payment_plan__c>();
		
		//Instalments
		if(inst.size() > 0)
			lInst = findInstalments(new list<Id>(inst));
			
		//Products
		if(prod.size() > 0)
			lProd = findProducts(new list<Id>(prod));

		//Invoices
		if(inv.size() > 0)
			lInv = findInvoices(new list<Id>(inv));

		//Payment Plan
		if(plan.size() > 0)
			lPlan = findPaymentPlan(new list<Id>(plan));

		orderResult(lInst, lProd, lInv, lPlan);
	}

	//Build result variables
	public list<consolidation> result {get;set;}
	public map<string, decimal> paymentTypeTotal{get{if(paymentTypeTotal == null) paymentTypeTotal = new map<string, decimal>(); return paymentTypeTotal;} set;}
	public client_course_instalment_payment__c editPayment {get;set;}
	public boolean showError {get{if(showError == null) showError = false; return showError;}set;}

	//Build result
	private void orderResult(list<client_course_instalment__c> lInstalments, list<client_product_service__c> lProducts, list<Invoice__c> lInvoice, list<payment_plan__c> lPaymentPlan){
		result = new list<consolidation>();
		consolidation ch;

		List<paymentOrder> ldt = new List<paymentOrder>();
		paymentTypeTotal = new map<string, decimal>();

		/** Invoice **/
		if(lInvoice!= null && lInvoice.size()>0){
			Integer pos;
			for(Invoice__c i : lInvoice){

				if(ldt.size() == 0 )
					pos = 0;
				else pos = addObjectListPosition(i.Received_Date__c,ldt, i.CurrencyIsoCode__c);
				ch = new consolidation();
				ch.invoice = i;

				for(client_course_instalment_payment__c cp :ch.invoice.client_course_instalment_payments__r){
					if(selectedPaymentTypes == cp.Payment_Type__c || selectedPaymentTypes == 'all' || showAll){
						if(!paymentTypeTotal.containsKey(cp.Payment_Type__c))
							paymentTypeTotal.put(cp.Payment_Type__c, cp.Value__c);
						else paymentTypeTotal.put(cp.Payment_Type__c, paymentTypeTotal.get(cp.Payment_Type__c) + cp.Value__c);
					}
				}

				if(result.size() == pos){
					result.add(ch);
					ldt.add(new paymentOrder(i.Received_Date__c, i.CurrencyIsoCode__c));
				}else{
					result.add(pos, ch);
					ldt.add(pos, new paymentOrder(i.Received_Date__c, i.CurrencyIsoCode__c));
				}
			}//end for
		}

		/** Instalment **/
		if(lInstalments!=null && lInstalments.size()>0){
			Integer pos;
			for(client_course_instalment__c i : lInstalments){

				if(!i.isInstalment_Amendment__c && (i.isCommission_Amendment__c || i.isOverpayment_Amendment__c)){}
				else{
					if(ldt.size() == 0 )
						pos = 0;
					else pos = addObjectListPosition(i.Received_Date__c,ldt, i.Client_Course__r.CurrencyIsoCode__c);
					ch = new consolidation();
					ch.instalment = i;

					for(client_course_instalment_payment__c cp :ch.instalment.client_course_instalment_payments__r){
						if(selectedPaymentTypes == cp.Payment_Type__c || selectedPaymentTypes == 'all' || showAll){
							if(!paymentTypeTotal.containsKey(cp.Payment_Type__c))
								paymentTypeTotal.put(cp.Payment_Type__c, cp.Value__c);
							else paymentTypeTotal.put(cp.Payment_Type__c, paymentTypeTotal.get(cp.Payment_Type__c) + cp.Value__c);
						}
					}

					if(result.size() == pos){
						result.add(ch);
						ldt.add(new paymentOrder(i.Received_Date__c, i.Client_Course__r.CurrencyIsoCode__c));
					}else{
						result.add(pos, ch);
						ldt.add(pos, new paymentOrder(i.Received_Date__c, i.Client_Course__r.CurrencyIsoCode__c));
					}
				}
			}//end for
		}

		/** Procuts **/
		if(lProducts!=null && lProducts.size()>0){
			Integer pos;
			for(client_product_service__c i : lProducts){

				if(ldt.size() == 0 )
					pos = 0;
				else pos = addObjectListPosition(i.Received_Date__c,ldt, i.Currency__c);
				ch = new consolidation();
				ch.product = i;

				for(client_course_instalment_payment__c cp :ch.product.client_course_instalment_payments__r){
					if(selectedPaymentTypes == cp.Payment_Type__c || selectedPaymentTypes == 'all' || showAll){
						if(!paymentTypeTotal.containsKey(cp.Payment_Type__c))
							paymentTypeTotal.put(cp.Payment_Type__c, cp.Value__c);
						else paymentTypeTotal.put(cp.Payment_Type__c, paymentTypeTotal.get(cp.Payment_Type__c) + cp.Value__c);
					}
					// originalValues.put(cp.id, cp.clone(true));
				}

				if(result.size() == pos){
					result.add(ch);
					ldt.add(new paymentOrder(i.Received_Date__c, i.Currency__c));
				}else{
					result.add(pos, ch);
					ldt.add(pos, new paymentOrder(i.Received_Date__c, i.Currency__c));
				}
			}//end for
		}

		/** Payment Plan **/
		if(lPaymentPlan!=null && lPaymentPlan.size()>0){
			Integer pos;
			for(payment_plan__c i : lPaymentPlan){

				if(ldt.size() == 0 )
					pos = 0;
				else pos = addObjectListPosition(i.Received_Date__c,ldt, i.Currency_Code__c);
				ch = new consolidation();
				ch.payplan = i;

				for(client_course_instalment_payment__c cp :ch.payplan.client_course_instalment_payments__r){
					if(selectedPaymentTypes == cp.Payment_Type__c || selectedPaymentTypes == 'all' || showAll){
						if(!paymentTypeTotal.containsKey(cp.Payment_Type__c))
							paymentTypeTotal.put(cp.Payment_Type__c, cp.Value__c);
						else paymentTypeTotal.put(cp.Payment_Type__c, paymentTypeTotal.get(cp.Payment_Type__c) + cp.Value__c);
					}
					// originalValues.put(cp.id, cp.clone(true));
				}

				if(result.size() == pos){
					result.add(ch);
					ldt.add(new paymentOrder(i.Received_Date__c, i.Currency_Code__c));
				}else{
					result.add(pos, ch);
					ldt.add(pos, new paymentOrder(i.Received_Date__c, i.Currency_Code__c));
				}
			}//end for
		}
	}
	
	//Order result items
	private static integer addObjectListPosition(dateTime d, List<paymentOrder> ld, string cry){
		integer pos = 0;
		for(paymentOrder dt:ld)
			if(d < dt.dt && dt.cry == cry){
				return pos;
			}
			else pos++;
		return pos;
	}
	
	//Get Instalments
	private list<client_course_instalment__c> findInstalments(list<Id> ids){
		String sql = 'SELECT id, Name, Confirmed_Date__c, Confirmed_By__c, Instalment_Value__c, Due_Date__c, Discount__c, Invoice__c, Kepp_Fee__c, Commission_Tax_Value__c, Commission_Value__c, ' +
								  ' Received_By__r.Name, Received_Date__c, Confirmed_By__r.Name, client_course__c, Paid_To_School_By__r.Name, Paid_To_School_On__c, Booking_Closed_On__c, ' +
								  ' Tuition_Value__c, Extra_Fee_Discount_Value__c, Extra_Fee_Value__c,  Split_Number__c, Number__c, Amendment_Instalment_Balance__c, isInstalment_Amendment__c, isOverpayment_Amendment__c, isCommission_Amendment__c, Original_Instalment__r.isAmended_BeforeSchoolPayment__c,' +
									
								  'Agency_Currency__c, ' +
							  	  'Agency_Currency_Rate__c, ' +
							  	  'Agency_Currency_Value__c, ' +
							  	  'Client_Course__r.CurrencyIsoCode__c, '+
							  	  'Client_Course__r.Campus_Country__c, '+
								  
								  'client_course__r.client__c, ' +
								  'client_course__r.client__r.Name, ' +
								  'client_course__r.Course_Name__c, ' +
								  'client_course__r.Campus_Name__c, ' +
								  'client_course__r.Campus_Course__r.Campus__r.ParentId, ' +
								  'client_course__r.client__r.Owner__r.Name, ' +

								  '(SELECT id, selected__c, Value__c, Date_Paid__c, Payment_Type__c, Confirmed_By__r.Name, Confirmed_By_Agency__r.Name, Confirmed_Date__c, Unconfirmed_By__r.name, Unconfirmed_Date__c, Paid_From_Deposit__c, Paid_From_Deposit__r.Deposit_Reconciliated__c, Booking_Closed_On__c, Bank_Deposit__c ' +
								  '	FROM client_course_instalment_payments__r),' +

								  '(SELECT Id, Name from client_course_instalment_amendment__r limit 1)' +


						   'FROM client_course_instalment__c WHERE Id IN :ids order by client_course__r.CurrencyIsoCode__c ';
		
		return Database.query(sql);
	}

	//Get products
	private list<client_product_service__c> findProducts(list<Id> ids){
		String sql = 'SELECT id, Name, Confirmed_Date__c, Category__c, Product_Name__c, Products_Services__r.Provider__r.Provider_Name__c, Confirmed_By__c, Price_Total__c, Quantity__c, Unit_Description__c, Received_By__r.Name, Received_Date__c, Confirmed_By__r.Name, Client__c, Client__r.Name, Client__r.Owner__r.Name, Invoice__c, Booking_Closed_On__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, Currency__c, ' +

							'(SELECT id, selected__c, Value__c, Date_Paid__c, Payment_Type__c, Confirmed_By__r.Name, Confirmed_By_Agency__r.Name, Confirmed_Date__c, Unconfirmed_By__r.name, Unconfirmed_Date__c, 	Paid_From_Deposit__c, Paid_From_Deposit__r.Deposit_Reconciliated__c, Booking_Closed_On__c, Bank_Deposit__c FROM client_course_instalment_payments__r), ' +

							'(SELECT Product_Name__c, Start_Date__c, End_Date__c, Price_Unit__c, Quantity__c, Price_Total__c, Unit_Description__c, Received_Date__c FROM paid_products__r order by Product_Name__c)' + 
					   'FROM client_product_service__c WHERE Id IN :ids order by Currency__c ';
	
		return Database.query(sql);
	}

	//Get Invoices
	private list<Invoice__c> findInvoices(list<Id> ids){
		String sql = 'SELECT Id, Name,  Client__c, Client__r.Name, Client__r.Owner__r.Name, Received_By__r.Name, Received_Date__c, Due_Date__c, Total_Value__c, Total_Products__c , Total_Instalments__c, Confirmed_Date__c, CurrencyIsoCode__c, Country__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c,  Confirmed_By__c, Confirmed_By__r.Name, Total_Discount__c, ' +

					' (SELECT client_course__r.Course_Name__c, client_course__r.CurrencyIsoCode__c, Instalment_Value__c, Invoice__c from client_course_instalments__r), ' +
					
					' (SELECT Category__c, Price_Total__c, Product_Name__c, Currency__c from client_products_services__r), ' +
					
					' (SELECT id, selected__c, Value__c, Date_Paid__c, Payment_Type__c, Confirmed_By__r.Name, Confirmed_By_Agency__r.Name, Confirmed_Date__c, Unconfirmed_By__r.name, Unconfirmed_Date__c, Paid_From_Deposit__c, Paid_From_Deposit__r.Deposit_Reconciliated__c,Booking_Closed_On__c, Bank_Deposit__c FROM client_course_instalment_payments__r)' + 
					
					'FROM Invoice__c WHERE id in :ids order by CurrencyIsoCode__c';

		return Database.query(sql);
			
	}

	//Get Payment Plan
	private list<payment_plan__c> findPaymentPlan(list<Id> ids){
		String sql = 'SELECT Id, Name,  Client__c, Client__r.Name, Client__r.Owner__r.Name, Received_By__r.Name, Payment_Confirmed_On__c, Received_Date__c, Due_Date__c, Total_Paid__c, Value_Paid__c, Value__c, Currency_Code__c, ' +

					' (SELECT id, selected__c, Value__c, Date_Paid__c, Payment_Type__c, Confirmed_By__r.Name, Confirmed_By_Agency__r.Name, Confirmed_Date__c, Unconfirmed_By__r.name, Unconfirmed_Date__c, Paid_From_Deposit__c, Paid_From_Deposit__r.Deposit_Reconciliated__c,Booking_Closed_On__c, Bank_Deposit__c FROM client_course_instalment_payments__r)' + 
					
					'FROM payment_plan__c WHERE id in :ids order by Currency_Code__c';

		return Database.query(sql);
			
	}


	// C O N F I R M 		P A Y M E N T S
	public void confirmPayments(){

		Savepoint sp;
		string nullType = '';
		try{
			sp = Database.setSavepoint();
			set<Id> idsInst = new set<Id>();
			set<Id> idsInv = new set<Id>();
			set<Id> idsProd = new set<Id>();
			set<Id> idsPlan = new set<Id>();
			list<client_course_instalment_payment__c> toUpdatePayment = new list<client_course_instalment_payment__c>();
			list<SObject> toUpdate = new list<SObject>();
			for(client_course_instalment_payment__c ccip : [SELECT Id, Payment_Type__c, client_course_instalment__c, Invoice__c, client_product_service__c, payment_plan__c FROM client_course_instalment_payment__c WHERE id in :checkedPayments.split(';')]){

				if(ccip.Payment_Type__c!= null && ccip.Payment_Type__c!=''){
					if(ccip.client_course_instalment__c!=null)
						idsInst.add(ccip.client_course_instalment__c);

					else if(ccip.Invoice__c!=null)
						idsInv.add(ccip.Invoice__c);

					else if(ccip.client_product_service__c!=null)
						idsProd.add(ccip.client_product_service__c);

					else if(ccip.payment_plan__c!=null)
						idsPlan.add(ccip.payment_plan__c);

					toUpdatePayment.add(new client_course_instalment_payment__c (Id = ccip.Id, Confirmed_By__c = currentUser.id, Confirmed_Date__c = system.now(), Confirmed_By_Agency__c = currentUser.Contact.AccountId, Unconfirmed_By__c = null, Unconfirmed_Date__c = null));
				}
			}//end for Update Payments

			update toUpdatePayment;

			boolean allConfirmed;

			if(idsInst.size()>0){
				list<client_course_instalment__c> toUpdateInst = new list<client_course_instalment__c>();

				for(client_course_instalment__c cci : [Select Id, (SELECT Id, Confirmed_Date__c FROM client_course_instalment_payments__r) from client_course_instalment__c where id in :idsInst]){
					allConfirmed = true;

					for(client_course_instalment_payment__c instProd : cci.client_course_instalment_payments__r){
						if(instProd.Confirmed_Date__c == null){
							allConfirmed = false;
							break;
						}
					}//end prod for

					if(allConfirmed){
						cci.Confirmed_By__c = Userinfo.getuserId();
						cci.Confirmed_Date__c = System.now();
						toUpdate.add(cci);
					}

				}//end for instalment
			}

			if(idsInv.size()>0){
				for(Invoice__c inv : [Select Id, (SELECT Id FROM client_course_instalments__r), (SELECT Id, Claim_Commission_Type__c FROM client_products_services__r), (SELECT Id, Confirmed_Date__c FROM client_course_instalment_payments__r) from Invoice__c where id in :idsInv]){
					allConfirmed = true;


					for(client_course_instalment_payment__c instProd : inv.client_course_instalment_payments__r){
						if(instProd.Confirmed_Date__c == null){
							allConfirmed = false;
							break;
						}
					}//end prod for

					if(allConfirmed){
						inv.Confirmed_By__c = Userinfo.getuserId();
						inv.Confirmed_Date__c = System.now();

						for(client_course_instalment__c cci:inv.client_course_instalments__r)
							toUpdate.add(new client_course_instalment__c(id = cci.id, Confirmed_By__c = Userinfo.getuserId(), Confirmed_Date__c = System.now()));

						for(client_product_service__c cps:inv.client_products_services__r){
							cps.Confirmed_By__c = Userinfo.getuserId();
							cps.Confirmed_Date__c = System.now();

							//Retain Product Commission if NET
							// if(cps.Claim_Commission_Type__c == 'Net commission'){
							// 	cps.Commission_Confirmed_By__c = Userinfo.getuserId();
							// 	cps.Commission_Confirmed_by_Agency__c = currentUser.Contact.AccountId;
							// 	cps.Commission_Confirmed_On__c = system.now();
							// 	cps.Commission_Paid_Date__c = system.today();
							// }

							toUpdate.add(cps);
						}

						toUpdate.add(inv);
					}

				}//end for Invoice
			}

			if(idsProd.size()>0){
				for(client_product_service__c prod : [Select Id, Claim_Commission_Type__c, (SELECT Id FROM paid_products__r), (SELECT Id, Confirmed_Date__c FROM client_course_instalment_payments__r) from client_product_service__c where id in :idsProd]){
					allConfirmed = true;

					for(client_course_instalment_payment__c instProd : prod.client_course_instalment_payments__r){
						if(instProd.Confirmed_Date__c == null){
							allConfirmed = false;
							break;
						}
					}//end prod for

					if(allConfirmed){
						prod.Confirmed_By__c = Userinfo.getuserId();
						prod.Confirmed_Date__c = System.now();

						//Retain Product Commission if NET
						// if(prod.Claim_Commission_Type__c == 'Net commission'){
						// 	prod.Commission_Confirmed_By__c = Userinfo.getuserId();
						// 	prod.Commission_Confirmed_by_Agency__c = currentUser.Contact.AccountId;
						// 	prod.Commission_Confirmed_On__c = system.now();
						// 	prod.Commission_Paid_Date__c = system.today();
						// }
 
						for(client_product_service__c pp : prod.paid_products__r){
							pp.Confirmed_By__c = Userinfo.getuserId();
							pp.Confirmed_Date__c = System.now();
							toUpdate.add(pp);
						}//end for
						toUpdate.add(prod);
					}

				}//end for Product
			}
			System.debug('==>idsPlan: '+idsPlan);
			if(idsPlan.size()>0){
				for(payment_plan__c plan : [Select Id, Payment_Confirmed_By__c, Payment_Confirmed_On__c, (SELECT Id, Confirmed_Date__c FROM client_course_instalment_payments__r) from payment_plan__c where id in :idsPlan]){
					allConfirmed = true;
					System.debug('==>allConfirmed: '+allConfirmed);
					
					for(client_course_instalment_payment__c instPlan : plan.client_course_instalment_payments__r){
						if(instPlan.Confirmed_Date__c == null){
							allConfirmed = false;
							break;
						}
					}//end plan for
					System.debug('==>allConfirmed2: '+allConfirmed);

					if(allConfirmed){
						plan.Payment_Confirmed_By__c = Userinfo.getuserId();
						plan.Payment_Confirmed_On__c = System.today();
						
						toUpdate.add(plan);
					}

				}//end for Product
			}

			update toUpdate;

		}
		catch(Exception e ){
			system.debug('Exception thrown: ' + e.getMessage());
			system.debug('Exception Line: ' + e.getLineNumber());
			Database.rollback(sp);
		}
		finally{
			searchPayments();
		}
	}

	// U N C O N F I R M 		P A Y M E N T S
	public void unconfirmPayment(){

		String paymentid = ApexPages.currentpage().getparameters().get('paymentid');
		String updateObj = ApexPages.currentpage().getparameters().get('objUp');

		Savepoint sp;

		try{
			sp = Database.setSavepoint();

			List<SObject> toUpdate = new list<SObject>();

			toUpdate.add(new client_course_instalment_payment__c(id = id.valueOf(paymentid),Confirmed_By__c = null, Confirmed_Date__c = null, Confirmed_By_Agency__c = null, Unconfirmed_By__c = UserInfo.getUserId(), Unconfirmed_Date__c = system.now()));

			sobject sObj = id.valueOf(updateObj).getSobjectType().newSObject(updateObj) ;
			if(id.valueOf(updateObj).getSobjectType() == payment_plan__c.sObjectType){
				sObj.put('Payment_Confirmed_By__c', null);
				sObj.put('Payment_Confirmed_On__c', null);
			}else{
				sObj.put('Confirmed_By__c', null);
				sObj.put('Confirmed_Date__c', null);
			}
			toUpdate.add(sObj);

			//If it is a Product object also unconfirm it's related paid products
			if(id.valueOf(updateObj).getSobjectType() == client_product_service__c.sObjectType){

				// sObj.put('Commission_Confirmed_By__c', null);
				// sObj.put('Commission_Confirmed_by_Agency__c', null);
				// sObj.put('Commission_Confirmed_On__c', null);
				// sObj.put('Commission_Paid_Date__c', null);

				for(client_product_service__c ccip : [SELECT Id FROM client_product_service__c WHERE Paid_with_product__c = :id.valueOf(updateObj)])
					toUpdate.add(new client_product_service__c(id = ccip.id, Confirmed_By__c = null, Confirmed_Date__c = null));
			}

			//If it is a Client Invoice Unconfirm also its instalments
			if(id.valueOf(updateObj).getSobjectType() == Invoice__c.sObjectType){
				for(client_course_instalment__c cci : [SELECT Id FROM client_course_instalment__c WHERE Invoice__c = :id.valueOf(updateObj)])
					toUpdate.add(new client_course_instalment__c(id = cci.id, Confirmed_By__c = null, Confirmed_Date__c = null));
				for(client_product_service__c ccip : [SELECT Id FROM client_product_service__c WHERE Invoice__c = :id.valueOf(updateObj)])
					toUpdate.add(new client_product_service__c(id = ccip.id, Confirmed_By__c = null, Confirmed_Date__c = null));
			}

			update toUpdate;
		}
		catch(Exception e ){
			system.debug('Exception thrown: ' + e.getMessage());
			system.debug('Exception Line: ' + e.getLineNumber());
			Database.rollback(sp);
		}
		finally{
			searchPayments();
		}
	}


	// ------------------------------ FILTERS

	public class consolidation{
		public Invoice__c invoice {get;set;}
		public client_course_instalment__c instalment {get;set;}
		public client_product_service__c product {get;set;}
		public payment_plan__c payplan {get;set;}
		public boolean showSave {get{if(showSave==null) showSave = false; return showSave;}set;}
	}

	private class paymentOrder{
		dateTime dt;
		string cry;
		private paymentOrder(dateTime dt, string cry){
			this.dt = dt;
			this.cry = cry;
		}
	}


	//Current User
	private User currentUser = [Select Id, Name, email, Aditional_Agency_Managment__c, Contact.Account.ParentId, Contact.AccountId, Contact.Department__c from User where id = :UserInfo.getUserId() limit 1];

	//Selected Payments
	public String checkedPayments {get;set;}

	//Selected Agency Group
	public string selectedAgencyGroup{
		get{
			if(selectedAgencyGroup == null)
				selectedAgencyGroup = currentUser.Contact.Account.ParentId;
			return selectedAgencyGroup;
		}set;}

	//Agency Group Options
	public List<SelectOption> agencyGroupOptions {
		get{
 			if(agencyGroupOptions == null){

 				agencyGroupOptions = new List<SelectOption>();
 				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group
    			for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:currentUser.Contact.AccountId order by Parent.Name]){
	     			agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
	    		}
 				}else{
 					for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
	     			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
	    		}
 				}
   		}
   			return agencyGroupOptions;
		}set;}

	//Change Agency Group
	public void changeGroup(){
		selectedAgency = '';
		getAgencies();
	}

	// Selected Agency
	public String selectedAgency {get{if(selectedAgency == null) selectedAgency = currentUser.Contact.AccountId; return selectedAgency;}set;}

	// Agency Options
	public list<SelectOption> getAgencies(){
		List<SelectOption> result = new List<SelectOption>();
		if(selectedAgencyGroup != null){
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
				for(Account a: [Select Id, Name from Account where id =:currentUser.Contact.AccountId order by name]){
					if(selectedAgency=='')
						selectedAgency = a.Id;
					result.add(new SelectOption(a.Id, a.Name));
				}
				if(currentUser.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
						result.add(new SelectOption(ac.id, ac.name));
					}
				}
			}else{
				for(Account a: [Select Id, Name from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
					result.add(new SelectOption(a.Id, a.Name));
					if(selectedAgency=='')
						selectedAgency = a.Id;
				}
			}
		}
		return result;
	}

	//Selected Payment Type
	public String selectedPaymentTypes {get{if(selectedPaymentTypes == null) selectedPaymentTypes = 'all'; return selectedPaymentTypes;}set;}
	
	// Payment Types Options
	public list<selectOption> depositListType{get; set;}
	public List<SelectOption> paymentTypes {
		get{
			if(paymentTypes == null){
				//getdepositListType();
				paymentTypes = new List<SelectOption>();
				depositListType = new List<SelectOption>();
				paymentTypes.add(new SelectOption('all','All'));
				for(AggregateResult ar : [SELECT Payment_Type__c FROM client_course_instalment_payment__c where Payment_Type__c!= null group by Payment_Type__c]){
					if(String.valueOf(ar.get('Payment_Type__c')).toLowerCase()!='pds' && String.valueOf(ar.get('Payment_Type__c')).toLowerCase()!='pcs')
					paymentTypes.add(new SelectOption((String) ar.get('Payment_Type__c'), (String) ar.get('Payment_Type__c')));

					if(String.valueOf(ar.get('Payment_Type__c')).toLowerCase()!='deposit' && String.valueOf(ar.get('Payment_Type__c')).toLowerCase()!='school credit' && String.valueOf(ar.get('Payment_Type__c')).toLowerCase()!='offshore' && String.valueOf(ar.get('Payment_Type__c')).toLowerCase()!='transferred from deposit'
					&& String.valueOf(ar.get('Payment_Type__c')).toLowerCase()!='pds' && String.valueOf(ar.get('Payment_Type__c')).toLowerCase()!='pcs' && String.valueOf(ar.get('Payment_Type__c')).toLowerCase()!='covered by agency' && String.valueOf(ar.get('Payment_Type__c')).toLowerCase()!='client scholarship')
					depositListType.add(new SelectOption((String) ar.get('Payment_Type__c'), (String) ar.get('Payment_Type__c')));

					if(String.valueOf(ar.get('Payment_Type__c')).toLowerCase() == 'deposit' || String.valueOf(ar.get('Payment_Type__c')).toLowerCase() == 'school credit' || String.valueOf(ar.get('Payment_Type__c')).toLowerCase() == 'offshore' || String.valueOf(ar.get('Payment_Type__c')).toLowerCase()=='transferred from deposit'
					|| String.valueOf(ar.get('Payment_Type__c')).toLowerCase() == 'covered by agency' || String.valueOf(ar.get('Payment_Type__c')).toLowerCase()=='client scholarship')
					depositListType.add(new SelectOption((String) ar.get('Payment_Type__c'), (String) ar.get('Payment_Type__c'), true));
				}
				//paymentTypes.addAll(depositListType);

			}
			return paymentTypes;
		}set;}

	// Selected School
	// public String selectedSchool {get{if(selectedSchool==null) selectedSchool = 'all'; return selectedSchool;}set;}

	//School Options
	// public list<SelectOption> schools { 
	// 	get{
	// 	if(schools==null){
	// 		IPFunctions.CampusAvailability result = new IPFunctions.CampusAvailability();
	// 		schools = result.getGroupSchools();
	// 	}
	// 	return schools;
	// }set;}

	//Selected Periods
	public boolean showAll{get{if(showAll == null) showAll = false; return showAll;} set;}
	public string SelectedPeriod{get {if(SelectedPeriod == null) SelectedPeriod = 'THIS_WEEK'; return SelectedPeriod; } set;}
	
	// Periods Options
	private List<SelectOption> periods;
	public List<SelectOption> getPeriods() {
		if(periods == null){
			periods = new List<SelectOption>();
			periods.add(new SelectOption('TODAY','Today'));
			periods.add(new SelectOption('THIS_WEEK','This Week'));
			periods.add(new SelectOption('THIS_MONTH','This Month'));
			periods.add(new SelectOption('unc','Unconfirmed'));
			periods.add(new SelectOption('range','Range'));
		}
		return periods;
	}

	//Selected Date Criteria
	public string dateCriteria{get {if(dateCriteria == null) dateCriteria = 'rc'; return dateCriteria; } set;}

	//Date Criteria Options
	private List<SelectOption> dateCriteriaOptions;
	public List<SelectOption> getdateCriteriaOptions() {
		if(dateCriteriaOptions == null){
			dateCriteriaOptions = new List<SelectOption>();
			dateCriteriaOptions.add(new SelectOption('rc','Received from Client'));
			dateCriteriaOptions.add(new SelectOption('pr','Payment Reconciled'));
		}
		return dateCriteriaOptions;
	}

	//Date Filter
	public client_course_instalment__c dateFilter{
		get{
			if(dateFilter == null){
				dateFilter = new client_course_instalment__c();
				Date myDate = Date.today();
				Date weekStart = myDate.toStartofWeek();
				dateFilter.Original_Due_Date__c = weekStart;
				dateFilter.Discounted_On__c = dateFilter.Original_Due_Date__c.addDays(6);
			}
			return dateFilter;
		}set;}
}