/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CourseUpdate_test {

    static testMethod void myUnitTest() {
    	
        Map<String,String> recordTypes = new Map<String,String>();
       
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
		
		Account agency = new Account();
		agency.name = 'IP Sydney';
		agency.RecordTypeId = recordTypes.get('Agency');
		agency.BillingCountry = 'Australia';
		insert agency;
		
		Account school = new Account();
		school.recordtypeid = recordTypes.get('School');
		school.name = 'Test School';
		school.BillingCountry = 'Australia';
		school.BillingCity = 'Sydney';		
		insert school;
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;
		insert p3;
		
		
		Nationality_Group__c ng = new Nationality_Group__c();
		ng.Account__c = school.id;
		ng.Country__c = 'Brazil';
		ng.Name = 'Latin America';
		insert ng;
       
		Account campus = new Account();
		campus.RecordTypeId = recordTypes.get('Campus');
		campus.Name = 'Test Campus CBD';
		campus.BillingCountry = 'Australia';
		campus.BillingCity = 'Sydney';
		campus.ParentId = school.id;
		insert campus;
		
		Account campus2 = new Account();
		campus2.RecordTypeId = recordTypes.get('Campus');
		campus2.Name = 'Test Campus Brisbane';
		campus2.BillingCountry = 'Australia';
		campus2.BillingCity = 'Brisbane';
		campus2.ParentId = school.id;
		insert campus2;
		
		     
       
		Course__c course = new Course__c();
		course.Name = 'Certificate III in Business';        
		course.Type__c = 'Business';
		course.Sub_Type__c = 'Business';
		course.Course_Qualification__c = 'Certificate III';
		course.Course_Type__c = 'VET';
		course.Course_Unit_Type__c = 'Week';
		course.Only_Sold_in_Blocks__c = true;
		course.School__c = school.id;
		insert course;
		
		Course__c course2 = new Course__c();
		course2.Name = 'Certificate I in Business';        
		course2.Type__c = 'Business';
		course2.Sub_Type__c = 'Business';
		course2.Course_Qualification__c = 'Certificate III';
		course2.Course_Type__c = 'VET';
		course2.Course_Unit_Type__c = 'Week';
		course2.Only_Sold_in_Blocks__c = false;
		course2.School__c = school.id;
		insert course2;
       
		Campus_Course__c cc = new Campus_Course__c();
		cc.Course__c = course.Id;
		cc.Campus__c = campus.Id;        
		cc.Is_Available__c = true;
		cc.is_Selected__c = true;      
		insert cc;
		
		Campus_Course__c cc2 = new Campus_Course__c();
		cc2.Course__c = course2.Id;
		cc2.Campus__c = campus.Id;        
		cc2.Is_Available__c = true;
		cc2.is_Selected__c = true;      
		insert cc2;
		
		
		Course_Price__c cp = new Course_Price__c();		
		cp.Nationality__c = 'Published Price';
		cp.From__c = 1;
		cp.Price_per_week__c = 150;
		cp.Availability__c = 3.0;
		cp.Campus_Course__c = cc.Id;
		cp.Price_valid_from__c = system.today();
		cp.Price_valid_until__c = system.today().addYears(1);        
		insert cp;
		
		Course_Price__c cp2 = new Course_Price__c();		
		cp2.Nationality__c = 'Latin America';
		cp2.From__c = 5;
		cp2.Price_per_week__c = 150;
		cp2.Availability__c = 3.0;
		cp2.Campus_Course__c = cc2.Id;
		cp2.Price_valid_from__c = system.today();
		cp2.Price_valid_until__c = system.today().addYears(1);       
		insert cp2;
		
		CourseUpdate cu = new CourseUpdate();
		cu.getCityDestination();
		
		cu.getlistDestinations();
		cu.destination = 'Australia';
		cu.getCityDestination();
		cu.destinationCity = 'Sydney';		
		cu.getlistSchools();
		cu.selectedSchool = school.id;		
		cu.getlistCampus();
		cu.selectedCampus = campus.id;
		cu.refreshScools();
		cu.refreshCampus();
		
    }
}