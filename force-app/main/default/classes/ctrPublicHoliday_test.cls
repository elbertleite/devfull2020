/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ctrPublicHoliday_test{

	static testMethod void myUnitTest() {
        
			
		Public_Holiday__c holiday = new Public_Holiday__c();
		holiday.State__c = 'New South Wales';
		holiday.Country__c = 'Australia';
		Date today = system.today();
		holiday.Date__c = today;
		insert holiday;
			
		Address__c address = new Address__c();
		address.State__c = holiday.State__c;
		address.Country__c = holiday.Country__c;

		ctrPublicHoliday ph = new ctrPublicHoliday(new Apexpages.Standardcontroller(holiday));
		date d = ph.FromDate;
		
		ph.newAddress.Country__c = holiday.Country__c;
		ph.newAddress.State__c = holiday.State__c;
		
		ph.getSavedHolidays();
		ph.save();
		ph.saveExtras();
		ph.clearState();

		ph.addYear();
		ph.removeYear();
		ph.getFromMonthIndex();
		ph.getFromYear();
		List<String> selectedDatesStrList = ph.selectedDatesStrList;
		ph.getFromDateOutOfBounds();
		ph.getToDateOutOfBounds();
		d = ph.ToDate;
		
		
	}

	
}