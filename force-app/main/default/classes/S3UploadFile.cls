public with sharing class S3UploadFile {
	
	public S3UploadFile(){
		constructor();
	}
	
	private String AWSCredentialName = IPFunctions.awsCredentialName;
	
	public String theID {get;set;}
	public String bucketPath {get;set;}
	
	public Blob fileBlob {get;set;}
    public Integer fileSize {get;set;}
    public String fileName {get;set;}
    
    public String S3Key {get;set;}
	public S3.AmazonS3 as3 { get; private set; } 
	public String OwnerId {get;set;}
	public PageReference constructor(){
  		try{
  
			AWSKeys credentials = new AWSKeys(AWSCredentialName);
			as3 = new S3.AmazonS3(credentials.key,credentials.secret);  			
  			S3Key= credentials.key;
  			
  			Datetime now = Datetime.now();
  			 //This performs the Web Service call to Amazon S3 and retrieves all the Buckets in your AWS Account. 
    		S3.ListAllMyBucketsResult allBuckets = as3.ListAllMyBuckets(as3.key,now,as3.signature('ListAllMyBuckets',now));    
    
    		//Store the Canonical User Id for your account
    		OwnerId = allBuckets.Owner.Id;
  			
		} catch(AWSKeys.AWSKeysException AWSEx){
			
			System.debug('Caught exception in AWS_S3_ExampleController: ' + AWSEx);
     		ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, AWSEx.getMessage());
   			ApexPages.addMessage(errorMsg);       
		}
   		return null;  
	}
    
    
    
    
	/*
	 This method uploads a file from the filesystem and puts it in S3. 
	 It also supports setting the Access Control policy. 
	*/
	public pageReference syncFilesystemDoc(){
		
		if(fileBlob == null){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please select a file to upload.'));
			return null;
		}
		
    	Datetime now = Datetime.now();
      	String docBody = EncodingUtil.base64Encode(fileBlob);
      
  		//TODO - make sure doc.bodyLength is not greater than 100000 to avoid apex limits
  			
  		System.debug('body length: ' + fileSize);
  		
  		system.debug('theID==>' + theID);

		String bucket = IPFunctions.isOrgBucket() ? Userinfo.getOrganizationId().toLowerCase() : Userinfo.getOrganizationId();
  		
  		fileName = bucketPath + theID;
  		Boolean putObjResult;
  		if(!Test.isRunningTest())	
  			putObjResult = as3.PutObjectInline_ACL(bucket, fileName ,null,docBody,fileSize,'public-read',as3.key,now,as3.signature('PutObjectInline',now),as3.secret, OwnerId,null);
  		else
  			putObjResult = true;
  		
  		
  		system.debug('putObjResult==>' + putObjResult);	
  		if(putObjResult){
  			
  			String previewLink = 'https://s3.amazonaws.com/' + bucket + '/' + fileName; 
      		
      		system.debug('previewLink==>' + previewLink);	
      		
      		if(theID.startsWith('003')){
      			Contact ct = new Contact(id=theID);
      			ct.PhotoClient__c = previewLink;
      			update ct;
      		} else if(theID.startsWith('001')){
      			Account acc = new Account(id = theID);
      			acc.Logo__c = previewLink;
      			update acc;
      		}
      		
      		fileBlob = null;
  			fileName = null;
  			fileSize = null;
      			
      		      			
  		}

		/*system.debug('REQUEST URL '+Url.getCurrentRequestUrl().getPath());
		system.debug('PAGE NAME '+ApexPages.CurrentPage().getUrl());
		system.debug('HEADERS '+JSON.serialize(ApexPages.CurrentPage().getHeaders()));
		system.debug('PARAMETERS '+JSON.serialize(ApexPages.CurrentPage().getParameters()));*/
		//String url = Url.getCurrentRequestUrl().getPath().split('apex/')[1];

		String urlPage;
		if(Test.isRunningTest()){
			urlPage = 'school_campus';
		}else{
			urlPage = Url.getCurrentRequestUrl().getPath().split('apex/')[1];
		}

		if(urlPage.contains('contact_new') || urlPage.contains('school_campus')){
			PageReference pr = new PageReference('/apex/'+urlPage);
			Map<String, String> parameters = ApexPages.CurrentPage().getParameters();
			if(parameters != null && !parameters.isEmpty()){
				for(String key : parameters.keySet()){
					if(!key.contains('j_id') && !key.contains('com.salesforce')){
						pr.getParameters().put(key, parameters.get(key));
					}
				}
			}
			pr.setRedirect(true);
			return pr;
		}else{
			return null;
		}
      
	}

}