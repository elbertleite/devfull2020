public with sharing class AgencyChecklistController {
	
	public Account acco{get; set;}

    public String substatusChecklist{get;set;}
    public String idChecklist{get;set;}
	public Boolean verifyStageZero{get;set;}

    public AgencyChecklistController(ApexPages.StandardController controller){
        if(acco == null){
            acco = [Select id, Name from Account where id = :controller.getRecord().id];
            List<Client_Stage__c> newLead = [SELECT ID FROM Client_Stage__c WHERE Agency_Group__c = :acco.ID AND Stage__c = 'Stage 0' AND Stage_description__c = 'NEW LEAD'];
            this.verifyStageZero = newLead.size() == 0;

            if(!this.verifyStageZero){
                newLead = [SELECT ID FROM Client_Stage__c WHERE Agency_Group__c = :acco.ID AND Stage__c = 'Stage 0' AND Stage_description__c = 'LEAD - Not Interested'];
                this.verifyStageZero = newLead.size() == 0;
            }
            system.debug('VERIFY STAGE ZERO ' + this.verifyStageZero);
        }

    }

   public void verifyStageZero(){
        system.debug('VERIFYING STAGE ZERO');
        Client_Stage__c cStage = null;
        try{
            cStage = [SELECT ID FROM Client_Stage__c WHERE Agency_Group__c = :acco.ID AND Stage__c = 'Stage 0' AND Stage_description__c = 'NEW LEAD'];
        }catch(Exception ex){
            system.debug('NEW LEAD NOT FOUND. ');
            cStage = new Client_Stage__c();
            cStage.Stage__c = 'Stage 0';
            cStage.Destination__c = '';
            cStage.Agency_Group__c = acco.Id;
            cStage.Stage_description__c = 'NEW LEAD';
            cStage.Lead_Stage__c = true;
            insert cStage; 
        }
        cStage = null;
        try{
            cStage = [SELECT ID FROM Client_Stage__c WHERE Agency_Group__c = :acco.ID AND Stage__c = 'Stage 0' AND Stage_description__c = 'LEAD - Not Interested'];
        }catch(Exception ex){
            system.debug('LEAD - Not Interested NOT FOUND. ');
            cStage = new Client_Stage__c();
            cStage.Stage__c = 'Stage 0';
            cStage.Destination__c = '';
            cStage.Agency_Group__c = acco.Id;
            cStage.Stage_description__c = 'LEAD - Not Interested';
            cStage.Lead_Stage__c = true;
            insert cStage; 
        }
   }
    
   public Agency_Checklist__c agency{
        get{
        if(agency == null){
            agency = new Agency_Checklist__c();
            agency.Agency_Group__c = acco.Id;
        }
        return agency;
    }set;}
    
    
   public class checkListperStage{
        public string stage {get; set;}
        public list<Agency_Checklist__c> lcheckList{get{if(lcheckList == null) lcheckList = new list<Agency_Checklist__c>(); return lcheckList;} set;}
    }
    
    private list<Agency_Checklist__c> clist;
    public list<checkListperStage> checkList{
        get{
        if(checkList == null ){
            map<string, string> mp = new map<string, string>();
            checkList = new list<checkListperStage>();
            checkListperStage stage;
            
         
            clist = [Select Agency_Group__c, Checklist_Item__c, Checklist_stage__c, Id,Destination__c,ItemOrder__c from Agency_Checklist__c where Agency_Group__c = :acco.Id and Destination__c = :agency.Destination__c order by Checklist_stage__c, ItemOrder__c nulls last];
         
            
            for(Agency_Checklist__c acl:clist)
                if(!mp.containsKey(acl.Checklist_stage__c)){
                    mp.put(acl.Checklist_stage__c, acl.Checklist_stage__c);
                    stage = new checkListperStage();
                    stage.stage = acl.Checklist_stage__c;
                    stage.lcheckList.add(acl);
                    checkList.add(stage);
                }else{
                    for(integer i = 0; i < checkList.size(); i++)
                        if(checkList[i].stage == acl.Checklist_stage__c){
                            checkList[i].lcheckList.add(acl);
                            break;
                        }
                }
        }
        return checkList;
    }
        set;
    }
    
    public pageReference saveReorderedList(){
        string newListOrder = ApexPages.currentpage().getParameters().get('listtoOrder');
                
        if(newListOrder != null){
            Agency_Checklist__c itemckl;
            list<Agency_Checklist__c> ReorderList = new list<Agency_Checklist__c>();
            list<id> listToDelete = new list<id>();
            
            if(newListOrder.LastIndexOf(',') != -1)
                newListOrder.substring(0,newListOrder.LastIndexOf(',')-1);
            
            List<string> lc = new List<string>(newListOrder.split(','));
                
            integer counter = 0;
            for(string s:lc)
                for(Agency_Checklist__c l:clist)    
                    if(s != null && s != '' && l.id == s){                      
                        
                        itemckl = new Agency_Checklist__c ();
                        itemckl.Destination__c = l.Destination__c;
                        itemckl.Checklist_stage__c = l.Checklist_stage__c;
                        itemckl.Checklist_Item__c = l.Checklist_Item__c;
                        itemckl.Agency_Group__c = l.Agency_Group__c;
                        itemckl.ItemOrder__c = counter;
                        ReorderList.add(itemckl);
                        listToDelete.add(l.id);
                        counter++;
                        break;
                    }
            Savepoint sp;
            try{
                sp = Database.setSavepoint();
                System.debug('==>ReorderList: '+ReorderList);
                delete [Select id from Agency_Checklist__c where id in :listToDelete];
                insert ReorderList;
                checkList = null;
            }catch(Exception e){
                ApexPages.addMessages(e);
                Database.rollback(sp);
            }
        } 
        return null;
    }
    
    public pageReference refreshCheckList(){
        agency.Checklist_stage__c = '';
        checkList = null;
        return null;
    }
    
    public pageReference refreshStageList(){
        cStage.Stage__c = '';
        listStages = null;
        return null;
    }
    
    public pageReference addItemCheckList(){
        if(agency.Checklist_stage__c == null || agency.Checklist_stage__c == '' ){
            agency.Checklist_stage__c.addError('You must select a stage');
            return null;
        }
        if(agency.Checklist_Item__c == null || agency.Checklist_Item__c == '' ){
            agency.Checklist_Item__c.addError('You must enter a value');
            return null;
        }           
        
      
        update acco;
        string st = agency.Checklist_stage__c;  
        string dest = agency.Destination__c;    
        agency.Agency_Group__c = acco.Id;
	    insert agency;
        agency = new Agency_Checklist__c();
        agency.Checklist_stage__c = st;
        agency.Destination__c = dest;
        checkList = null;
        return null;
    }
    
    public pageReference addCheckListToAgency(){
        System.debug('@@@@@ agency.Agency_Group__c : ' + agency.Agency_Group__c);
        acco.Agency_Checklist__c =  agency.Agency_Group__c;
        update acco;
        checkList = null;
		listStages = null;
        return null;
    }
    
    public pageReference deleteCheckListToAgency(){

        acco.Agency_Checklist__c =  null;
        update acco;
        return null;
    }
    
    public pageReference deleteItem(){
        delete  [Select id from Agency_Checklist__c where id = :ApexPages.currentPage().getParameters().get('itemToDelte')];
        checkList = null;
        return null;
    }
    
    
    public User currentUser {get{if(currentUser==null) currentUser = IpFunctions.getUserInformation(UserInfo.getUserId()); return currentUser;}set;}
    public List<SelectOption> getDestinations() {
        List<SelectOption> options = IPFunctions.getAllCountries();
		options.add(0, new SelectOption('','Default'));  
        return options;
    }
    
    
    //=============Client Stage==============================================   
    
    public List<SelectOption> getStages(){
		List<SelectOption> options = new List<SelectOption>();
		if(String.isEmpty(cStage.Destination__c)){
            options.add(new SelectOption( 'Stage 0', 'Stage 0' ));
        }else{
            Schema.DescribeFieldResult fieldResult = Schema.Client_Stage__c.Stage__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry f : ple){
                options.add(new SelectOption( f.getValue(), f.getLabel() ));
            }
        }
		
		return options;
	}

    public Client_Stage__c cStage{
        get{
        if(cStage == null){
            cStage = new Client_Stage__c(); 
            cStage.Agency_Group__c = acco.Id;
        }
        return cStage;
    }set;}
    
    public class clientStage{
        public string stage {get; set;}
        public list<Client_Stage__c> lstage{get{if(lstage == null) lstage = new list<Client_Stage__c>(); return lstage;} set;}
    }
    
    public boolean hasNotInterested {get;set;}
    //public boolean hasNewLead {get;set;}
    public list<clientStage> listStages{
        get{if(listStages == null){
        	hasNotInterested = false;
        	//hasNewLead = false;
            map<string, string> mp = new map<string, string>();
            listStages = new list<clientStage>();
            clientStage stage;
            list<Client_Stage__c> clist = new list<Client_Stage__c>();
            list<Client_Stage__c> stageZero = [Select id, Stage__c, Stage_description__c, Agency_Group__c, Destination__c, Lead_Stage__c, Stage_Sub_Options__c From Client_Stage__c where Agency_Group__c = :acco.Id and Stage__c = 'Stage 0' order by Stage__c, itemOrder__c nulls last];
            
            if(stageZero != null && !stageZero.isEmpty()){
                clist.addAll(stageZero);    
            }

            if(!String.isEmpty(cStage.Destination__c)){
                list<Client_Stage__c> allStages = [Select id, Stage__c, Stage_description__c, Agency_Group__c, Destination__c, Lead_Stage__c, Stage_Sub_Options__c From Client_Stage__c where Agency_Group__c = :acco.Id and Destination__c = :cStage.Destination__c order by Stage__c, itemOrder__c nulls last];
                clist.addAll(allStages);
            }
            

            for(Client_Stage__c acl:clist){
            
                if(acl.Stage_description__c == 'LEAD - Not Interested' && acl.Stage__c == 'Stage 0'){
                	hasNotInterested = true;
                }
                /*if(acl.Stage_description__c == 'NEW LEAD' && acl.Stage__c == 'Stage 0'){
                	hasNewLead = true;
                }*/
                	
                if(!mp.containsKey(acl.Stage__c)){
                    mp.put(acl.Stage__c, acl.Stage__c);
                    stage = new clientStage();
                    stage.stage = acl.Stage__c;
                    stage.lstage.add(acl);
                    listStages.add(stage);
                    	
                }else{
                    for(integer i = 0; i < listStages.size(); i++)
                        if(listStages[i].stage == acl.Stage__c){
                            listStages[i].lstage.add(acl);
                            break;
                        }
                }
                
            //else listStages = new list<clientStage>();
        }
        }
	    return listStages;
    }
    set;
    }
    
    public pageReference saveReorderedClientStageList(){        
        
        string newListOrder = ApexPages.currentpage().getParameters().get('listtoOrder');
            
        if(newListOrder != null){
            Client_Stage__c itemckl;
            list<Client_Stage__c> ReorderList = new list<Client_Stage__c>();
            list<id> listToDelete = new list<id>();
            
            //newListOrder = newListOrder.removeEnd(',');
            
            if(newListOrder.LastIndexOf(',') != -1)
                newListOrder.substring(0,newListOrder.LastIndexOf(',')-1);
                        
            List<string> lc = new List<string>(newListOrder.split(','));

            integer counter = 0;
            for(string s:lc)                
                for(clientStage cs : listStages)    
                    for(Client_Stage__c l : cs.lstage){
                        if(s != null && s != '' && l.id == s){
                            
                            itemckl = new Client_Stage__c ();
                            itemckl.Destination__c = l.Destination__c;
                        	itemckl.Lead_Stage__c = l.Lead_Stage__c;
                            itemckl.Stage__c = l.Stage__c;
                            itemckl.Stage_description__c = l.Stage_description__c;
                            itemckl.Agency_Group__c = l.Agency_Group__c;
                            itemckl.Stage_Sub_Options__c = l.Stage_Sub_Options__c;
                            itemckl.ItemOrder__c = counter;
                            ReorderList.add(itemckl);
                            listToDelete.add(l.id);
                            counter++;
                            break;
                        }
                    }
            Savepoint sp;
            try{
                sp = Database.setSavepoint();
                System.debug('==>ReorderList: '+ReorderList);
                delete [Select id from Client_Stage__c where id in :listToDelete];
                insert ReorderList;
                listStages = null;
            }catch(Exception e){
                
                ApexPages.addMessages(e);
                Database.rollback(sp);
            }
        } 
        return null;
    }
        
    public pageReference addItemClientStage(){
        
        
        
        if(cStage.Stage__c == null || cStage.Stage__c == '' ){
            cStage.Stage__c.addError('You must select a stage');
            return null;
        }
        
        if(cStage.Stage_description__c == null || cStage.Stage_description__c == '' ){
            cStage.Stage_description__c.addError('You must enter a value');
            return null;
        }           
        
        string st = cStage.Stage__c;
        string dest = cStage.Destination__c;
        insert cStage;
        
        
        if(!hasNotInterested && st == 'Stage 0'){
	    	cStage = new Client_Stage__c();
	        cStage.Stage__c = st;
	        cStage.Destination__c = dest;
	        cStage.Agency_Group__c = acco.Id;
	        cStage.Stage_description__c = 'LEAD - Not Interested';
	        cStage.Lead_Stage__c = true;
         	insert cStage;
        }

        /*if(!hasNewLead && st == 'Stage 0'){
	    	cStage = new Client_Stage__c();
	        cStage.Stage__c = st;
	        cStage.Destination__c = dest;
	        cStage.Agency_Group__c = acco.Id;
	        cStage.Stage_description__c = 'NEW LEAD';
	        cStage.Lead_Stage__c = true;
         	insert cStage;
            hasNewLead = true;
        }*/
        
        cStage = new Client_Stage__c();
        cStage.Stage__c = st;
        cStage.Destination__c = dest;
        cStage.Agency_Group__c = acco.Id;
        listStages = null;
        
        return null;
    }

    public void saveStageSubOptions(){
        Client_Stage__c cStage = new Client_Stage__c();
        cStage.ID = idChecklist;
        
        cStage.Stage_Sub_Options__c = String.isEmpty(substatusChecklist) ? '' : JSON.serialize(substatusChecklist.replace(',',';').split(';'));
        
        update cStage;
    }
    
    
    public pageReference deleteStageItem(){
		
        delete  [Select id from Client_Stage__c where id = :ApexPages.currentPage().getParameters().get('itemToDelte')];
        listStages = null;
        return null;
    }
    
    //=============Notes & Tasks=============================================
    
    
    public String Subject {get;set;}
    
    private Account accoSubjects {get;set;}
   
    private set<String> setSubjects {get;set;}
   
    public list<String> listSubjects {get{
    	if(listSubjects == null){
    		listSubjects = new list<String>();
    		setSubjects = new set<String>();
    		accoSubjects = [Select Task_Subjects__c from Account where id = :acco.Id limit 1];
    		if(accoSubjects.Task_Subjects__c!=null){
            	listSubjects.addAll(accoSubjects.Task_Subjects__c.split(';'));
            	setSubjects.addAll(listSubjects);
    		}
        }   
        return listSubjects;
    }set;}

    public void addSubject(){    	
    	try{
    		listSubjects.add(Subject.trim());
    		listSubjects.sort();
    		setSubjects.add(Subject.trim());
    		accoSubjects.Task_Subjects__c = String.join(listSubjects, ';');
    		update accoSubjects;	
    		subject = null;    		
    		
    	}catch(Exception e){
    		system.debug('Error====' + e);
    	}
    }
    
    public void deleteSubject(){
		try{
			setSubjects.remove(ApexPages.currentPage().getParameters().get('subjectToDelte'));
	    	listSubjects.clear();
	    	listSubjects.addAll(setSubjects); 
	    	accoSubjects.Task_Subjects__c = String.join(listSubjects, ';');	    	
	    	
			update accoSubjects;	
		}catch(Exception e){
    		system.debug('Error====' + e);
    	}
    }
    
	
	//=============Lead source =============================================
    
    public Lead_Status_Source__c LeadSource{
        get{
        if(LeadSource == null){
           LeadSource = new Lead_Status_Source__c(); 
           LeadSource.Agency_Group__c = acco.Id;
        }
        return LeadSource;
    } 
        set;
    }
    
	public List<SelectOption> listleadSource {
		get{
			if(listleadSource == null){
				listleadSource = new List<SelectOption>();
				Schema.DescribeFieldResult F = Contact.fields.LeadSource.getDescribe();
				List<Schema.PicklistEntry> P = F.getPicklistValues();
				for (Schema.PicklistEntry lst:P)
					//if(lst.getValue() != 'Referral')
						listleadSource.add(new SelectOption(lst.getValue(),lst.getLabel()));
				
				listleadSource.add(0,new SelectOption('', '--Select Option--' ));
			}
				
			return listleadSource;
		}
		set;
	}
	
    private list<Lead_Status_Source__c> listLeadsSource;
	public list<string> ListSourceItems{
		get{
			if(ListSourceItems == null){
				ListSourceItems = new list<string>();
				Set<string> setls = new Set<string>();					
				for(Lead_Status_Source__c ls:getlistLeadsSource())
					setls.add(ls.Lead_Source__c);
				ListSourceItems.addAll(setls);	
			} 
			ListSourceItems.sort();
			return ListSourceItems;
		}
		Set;
	}
	
	public pageReference deleteSpecificSource(){
		
        delete  [Select id from Lead_Status_Source__c where id = :ApexPages.currentPage().getParameters().get('itemToDelte')];
        listLeadsSource = null;
		ListSourceItems = null;
        return null;
    }
	
	
	
	
	
    public list<Lead_Status_Source__c> getlistLeadsSource(){
        if(listLeadsSource == null){
            listLeadsSource = [Select Id, Agency_Group__c, Subject__c, Field_Type__c, Lead_Source__c from Lead_Status_Source__c where Agency_Group__c = :acco.Id and Field_Type__c = 'source' order by Lead_Source__c, Subject__c];
        }   

        return listLeadsSource;
    }

    public pageReference addLeadSource(){
        /*acco.Id =  agency.Agency_Group__c;
        update acco;*/
		LeadSource.Field_Type__c = 'source';
        LeadSource.Agency_Group__c = acco.Id;
		String ls = LeadSource.Lead_Source__c;
        insert LeadSource;
        LeadSource = new Lead_Status_Source__c();
		LeadSource.Lead_Source__c = ls;
        listLeadsSource = null;
		ListSourceItems = null;
        return null;
    }
    
    public pageReference deleteLeadSource(){
        delete  [Select id from Lead_Status_Source__c where id = :ApexPages.currentPage().getParameters().get('leadStatusToDelete')];
        listLeadsSource = null;
        return null;
    }
	
	public pageReference refreshlistLeadsSource(){
        LeadSource.Subject__c = '';
        listLeadsSource = null;
        return null;
    }


}