public class batch_updateSDrive_previewLink_doc{
	public batch_updateSDrive_previewLink_doc(){}
}

/*global class batch_updateSDrive_previewLink_doc implements Database.Batchable<sObject> {
	
	String query;
	
	global batch_updateSDrive_previewLink_doc(string qr) {
		query = qr;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Account_Document_File__c> scope) {
		List<ID>  parentIDs = new List<ID>();
        List<ID> fileObjects = new List<ID>();
        list<Account_Document_File__c> lf = scope;

        for(Account_Document_File__c file : lf){
            parentIDs.add(file.Parent__c);
            fileObjects.add(file.id);   
                
        }

        if(!parentIDs.isEmpty() && !fileObjects.isEmpty()){
            for(string url :cg.SDriveTools.getAttachmentURLs( parentIDs , fileObjects, (631138519))){
                for(Account_Document_File__c af: lf){
                    if(url.contains(af.id)){
                       // af.URL__c = url;
						af.Preview_Link__c = url;
                        break;
                    }
                }
            }   
        }
        update lf;
	}
	 
	global void finish(Database.BatchableContext BC) { 
		System.debug('Batch Process Complete');   
	}
	
}*/

/*open up the Debug Log and run this
    batch_updateSDrive_previewLink batch = new batch_updateSDrive_previewLink('Select id, Content_Type__c, File_Name__c, URL__c, Parent__c from Account_Picture_File__c where createdBy.name = null');
    Id batchId = Database.executeBatch(batch);



    batch_updateSDrive_previewLink_doc batch = new batch_updateSDrive_previewLink_doc('Select id, File_Name__c, Parent__c, Preview_Link__c, url__c, createdby.name, wip__c from Account_Document_File__c where createdBy.name != null and Content_Type__c != \'Folder\'');
    Id batchId = Database.executeBatch(batch, 80); //Define batch size

	Select id, File_Name__c, URL__c, preview_link__c, Parent__c, createdby.name from Account_document_File__c where createdby.name != null 
*/