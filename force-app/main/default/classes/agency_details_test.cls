/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class agency_details_test {

    static testMethod void myUnitTest() {
        
        TestFactory testFactory = new TestFactory();
		
		Account agency = TestFactory.createAgency();
		
		Bank_Detail__c bd = new Bank_Detail__c();
		bd.Account__c = agency.id;
		bd.Bank__c = 'Commonwealth';
		bd.Account_Name__c = 'My Account';
		bd.Account_Number__c = '0000 000000';
		bd.BSB__c = '1290';
		insert bd;
		
		//Apexpages.currentPage().getParameters().put('agencyID', agency.id);
		Apexpages.currentPage().getParameters().put('agencyGroupID', agency.ParentId);
		
		agency_details ad = new agency_details(new Apexpages.Standardcontroller(agency));
		
		boolean edit = ad.edit;
		ad.setEdit();
		ad.cancel();
		ad.saveAgency();
		
		agency_details newagency = new agency_details(new Apexpages.Standardcontroller(new Account()));
		newagency.cancel();
		newagency.agency.Name = 'EH Melbourne';
		newagency.bankDetail.Bank__c = 'Commonwealth';
		newagency.bankDetail.Account_Name__c = 'My Account';
		newagency.bankDetail.Account_Number__c = '0000 000000';
		newagency.bankDetail.BSB__c = '1290';
		
		newagency.saveAgency();
		
		
		String p = newagency.path;
	
    }
}