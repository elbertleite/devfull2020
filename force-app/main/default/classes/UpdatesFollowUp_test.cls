/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class UpdatesFollowUp_test {

    static testMethod void myUnitTest() {
    	
       	TestFactory tf = new TestFactory();
		
		Account agency = tf.createAgency();
		
		Contact userDetails = tf.createEmployee(agency);
        
		Account school = tf.createSchool();
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;
		insert p3;
		
		
		Account campus = tf.createCampus(school, agency);
		    
		Course__c course = tf.createCourse();
		
		Course__c course2 = tf.createLanguageCourse();
				
		Campus_Course__c cc = tf.createCampusCourse(campus, course);
		
		Campus_Course__c cc2 = tf.createCampusCourse(campus, course2);
       
		Course_Price__c cp = tf.createCoursePrice(cc, 'Latin America');
		
		Course_Price__c cp2 = tf.createCoursePrice(cc2, 'Published Price');
		
		
		Course_Intake_Date__c cid = tf.createCourseIntakeDate(cc);
		
		Course_Intake_Date__c cid2 = tf.createCourseIntakeDate(cc2);
        
		Course_Extra_Fee__c cef = tf.createCourseExtraFee(cc, 'Latin America', p);
		
		Course_Extra_Fee_Dependent__c cefd = tf.createCourseRelatedExtraFee(cef, p2);
		
		tf.createCourseExtraFeeCombined(cc, 'Latin America', p, p2);
        
		
		
		UpdatesFollowUp cu = new UpdatesFollowUp();
		
		string destCountry = cu.destination;
		string destCity = cu.destinationCity;
		string destScholl = cu.selectedSchool;
		string destCampus = cu.selectedCampus;  
		list<String> listCampuses = cu.listCampuses;
		
		cu.getCityDestination();
		
		cu.getlistDestinations();
		cu.destination = 'Australia';
		cu.getCityDestination();
		cu.destinationCity = 'Sydney';		
		cu.getlistSchools();
		cu.selectedSchool = school.id;		
		cu.getlistCampus();
		cu.selectedCampus = campus.id;
		cu.refreshScools();
		cu.refreshCampus();
		
		Update_Follow_Up__c fu = new Update_Follow_Up__c();
		 
		fu.Country__c = 'Australia';
		fu.City__c = 'Sydney';
		fu.School__c = school.id;
		fu.Campus__c = campus.Id;
		fu.Comments__c = 'baba';
		insert fu;
		ApexPages.currentPage().getParameters().put('updateId', fu.Id);
		
		
		
		cu.getlistUpdates();
		cu.addUpdateCourse();
		//cu.saveUpdate();
		cu.addUpdateCourse();
		ApexPages.currentPage().getParameters().put('deleteID', fu.Id);
		cu.deleteFollowUp();
		
		
		
		
    }
}