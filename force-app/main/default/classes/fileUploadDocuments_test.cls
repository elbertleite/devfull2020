/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class fileUploadDocuments_test {

    static testMethod void myUnitTest() {
      	TestFactory factory = new TestFactory();
	  	
	  	UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
		Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
		User portalAccountOwner1 = new User(
			UserRoleId = portalRole.Id,
			ProfileId = profile1.Id,
			Username = System.now().millisecond() + 'fileUploadDocuments_test@test.com',
		   	Alias = 'batman',
			Email='bruce.wayne2@wayneenterprises.com',
			EmailEncodingKey='UTF-8',
			Firstname='Bruce',
			Lastname='Wayne',
			LanguageLocaleKey='en_US',
			LocaleSidKey='en_US',
			TimeZoneSidKey='America/Chicago'
		);
		Database.insert(portalAccountOwner1);
		
		Account agency;
		Contact client;
		
		System.RunAs(new User(id = UserInfo.getUserId())){ 
			//Agency
			agency = factory.createPortalAgency(portalAccountOwner1.id);    	
			//Client	
			client = factory.createClient(agency); //Client		
			//S3 Key
			AWSKey__c testKey = new AWSKey__c(name=IPFunctions.awsCredentialName,key__c='key',secret__c='secret');
			insert testKey;
			
			Client_Document__c cd = new Client_Document__c();
	        cd.Document_Category__c = 'Personal';
	        cd.Document_Type__c = 'Bank Details';
	        cd.Client__c = client.id;
	        insert cd;
			
			Client_Document_File__c cdf= new Client_Document_File__c();
	       	cdf.Client__c = client.id;
	       	cdf.File_Name__c = 'SomeFile.txt';
	       	cdf.ParentId__c = cd.id;
	       	insert cdf;
	        
	       	
	        Apexpages.currentPage().getParameters().put('fileSize', '5151551'); 
			fileUploadDocuments f = new fileUploadDocuments();
	       	Apexpages.currentPage().getParameters().put('clientId', client.id);
	       	
	       	f.clientId = client.id;
	       	f.setParameters();
	       	f.constructor();
	       	Client_Document_File__c dummy = f.clientDocFile;
	       	f.getPageHost();
	       	f.getOrgId();
	       	f.getHexPolicy();
	       	f.getPolicy();
	       	f.getSignedPolicy();
	       	
	       	f.createClientFile();
       
			
				
		
		}
    }
}