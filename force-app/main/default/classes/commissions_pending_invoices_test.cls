@isTest
private class commissions_pending_invoices_test {

	public static Account school {get;set;}
	public static Account agency {get;set;}
	public static Contact emp {get;set;}
	public static Contact client {get;set;}
	public static User portalUser {get{
		if (null == portalUser) {
			portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
		}
		return portalUser;
	}set;}
	public static Account campus {get;set;}
	public static Course__c course {get;set;}
	public static Campus_Course__c campusCourse {get;set;}
	public static client_course__c clientCourseBooking {get;set;}
	public static client_course__c clientCourse {get;set;}
	public static client_course__c clientCourse2 {get;set;}


	@testSetup static void setup() {
		TestFactory tf = new TestFactory();

		Account sg = tf.createSchoolGroup();
		school = tf.createSchool();
		school.ParentId = sg.Id;
		update school;
		agency = tf.createAgency();
		emp = tf.createEmployee(agency);
		portalUser = tf.createPortalUser(emp);
		campus = tf.createCampus(school, agency);
		course = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);

		Back_Office_Control__c bo = new Back_Office_Control__c(Agency__c = agency.id, Country__c = 'Australia', Backoffice__c = agency.id, Services__c = 'PDS_PCS_PFS_Chase');
		insert bo;

		system.runAs(portalUser){
			client = tf.createClient(agency);
			clientCourseBooking = tf.createBooking(client);
			clientCourse = tf.createClientCourse(client, school, campus, course, campusCourse, clientCourseBooking);
			tf.createClientCourseFees(clientCourse, false);
			List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(clientCourse);

			//TO REQUEST PDS
			instalments[0].Received_By_Agency__c = agency.id;
			instalments[0].Paid_to_School_By_Agency__c =  agency.id;
			instalments[0].Received_Date__c =  system.today();
			instalments[0].Paid_To_School_On__c =  system.today();
			instalments[0].isPFS__c = true;
			instalments[0].commission_due_date__c = system.today();
			instalments[0].isSelected__c = true;

			instalments[1].Received_By_Agency__c = agency.id;
			instalments[1].Received_Date__c =  system.today();
			instalments[1].isPCS__c = true;
			instalments[1].commission_due_date__c = system.today();
			instalments[1].isSelected__c = true;

			instalments[2].Received_By_Agency__c = agency.id;
			instalments[2].Received_Date__c =  system.today();
			instalments[2].isPDS__c = true;
			instalments[2].PDS_Confirmed__c = true;
			instalments[2].isSelected__c = true;
			instalments[2].commission_due_date__c = system.today();

			update instalments;

			commissions_send_school_invoice req = new commissions_send_school_invoice();
			req.selectedCountry = 'Australia';
			req.selectedSchoolGP = sg.id;
			req.selectedSchool = school.id;
			req.searchName = '';
			req.dates.Commission_Paid_Date__c = system.today().addYears(1);
			req.findInstalments();

			req.result[0].campuses[0].instalments[0].installment.isSelected__c = true;
			req.result[0].campuses[0].instalments[1].installment.isSelected__c = true;
			req.result[0].campuses[0].instalments[2].installment.isSelected__c = true;
			ApexPages.currentPage().getParameters().put('schId', string.valueOf(req.result[0].schoolId));
			req.dates.Commission_Paid_Date__c = system.today().addYears(1);
			req.setToRequest();

			ApexPages.CurrentPage().getParameters().put('cn', 'Australia');
			ApexPages.currentPage().getParameters().put('type', 'sendInvoice');
			PaymentSchoolEmail invoice = new PaymentSchoolEmail();
			invoice.setupEmail();
			invoice.dueDate.Expected_Travel_Date__c= System.today().addDays(350);
			invoice.toAddress = 'test@educationhify.com';
			invoice.createInvoice();

			list<client_course_instalment__c> requestInstalments = [Select Id from client_course_instalment__c where Request_Commission_Invoice__c != null];
			System.debug('requestInstalments.size===>' + requestInstalments.size());
			System.assertEquals(requestInstalments.size()==3, true);

		}
	}


	static testMethod void confirmCommission(){
		Test.startTest();
		system.runAs(portalUser){
			commissions_pending_invoices toTest = new commissions_pending_invoices();
			toTest.selectedCountry = 'Australia';
			toTest.selectedSchoolGP = 'all';
			toTest.selectedSchool = 'all';
			toTest.searchName = '';
			toTest.dates.Commission_Paid_Date__c = toTest.dates.Commission_Paid_Date__c.addDays(365);
			toTest.searchPendingInvoices();

			String invoiceId = toTest.invResult[0].invoices[0].inv.id;
			String instalmentId = toTest.invResult[0].invoices[0].instalments[0].installment.id;
			ApexPages.currentPage().getParameters().put('invId', invoiceId);
			toTest.confirmCommission(); // Validation for Commission Paid Date null

 			invoiceId = toTest.invResult[0].invoices[0].inv.id;
			instalmentId = toTest.invResult[0].invoices[0].instalments[0].installment.id;
			ApexPages.currentPage().getParameters().put('invId', invoiceId);
			toTest.invResult[0].invoices[0].inv.Commission_Paid_Date__c = system.today();
			toTest.confirmCommission();
			client_course_instalment__c inst = [SELECT Id, Commission_Confirmed_On__c FROM client_course_instalment__c WHERE id = :instalmentId limit 1];
			system.assertNotEquals(inst.Commission_Confirmed_On__c, null);

			

			/** UNCONFIRM COMMISSION **/
			toTest = new commissions_pending_invoices();
			toTest.searchName = '';
			toTest.selectedPaymentStatus = 'confirmed';
			toTest.resetFilters();
			toTest.selectedCountry = 'Australia';
			toTest.selectedSchoolGP = 'all';
			toTest.selectedSchool = 'all';
			toTest.dates.Commission_Paid_Date__c = toTest.dates.Commission_Paid_Date__c.addDays(365);
			toTest.searchPendingInvoices();

			invoiceId = toTest.invResult[0].invoices[0].inv.id;
			instalmentId = toTest.invResult[0].invoices[0].instalments[0].installment.id;
			ApexPages.currentPage().getParameters().put('unId', invoiceId);
			toTest.setIdToUnconfirm();
			toTest.unconfirmCommission();

			inst = [SELECT Id, Commission_Confirmed_On__c FROM client_course_instalment__c WHERE id = :instalmentId limit 1];
			system.assertEquals(inst.Commission_Confirmed_On__c, null);


		}
		Test.stopTest();
	}


	static testMethod void cancelInvoice(){
		Test.startTest();
		system.runAs(portalUser){
			commissions_pending_invoices toTest = new commissions_pending_invoices();
			toTest.selectedCountry = 'Australia';
			toTest.selectedSchoolGP = 'all';
			toTest.selectedSchool = 'all';
			toTest.searchName = '';
			toTest.dates.Commission_Paid_Date__c = toTest.dates.Commission_Paid_Date__c.addDays(365);
			toTest.searchPendingInvoices();

			String invoiceId = toTest.invResult[0].invoices[0].inv.id;
			ApexPages.currentPage().getParameters().put('unId', invoiceId);
			toTest.setIdToUnconfirm();
			toTest.cancelInvoice();

			commissions_cancelled_school_invoices cancelledInvoices = new commissions_cancelled_school_invoices();
			String selectedAgency = cancelledInvoices.selectedAgency;
			cancelledInvoices.findCancelledInvoices();
		}
		Test.stopTest();
	}


	static testMethod void testFilters(){
		Test.startTest();
		system.runAs(portalUser){
			commissions_pending_invoices toTest = new commissions_pending_invoices();

			totest.selectedCountry = 'Australia';
			totest.changeCountry();

			list<SelectOption> pendingOptions  =  toTest.pendingOptions;
			Invoice__c invReason  =  toTest.invReason ;
			boolean showError  =  toTest.showError ;
		}
		Test.stopTest();
	}

}