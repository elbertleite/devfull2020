public with sharing class school_campus_courses {

	public Account accSelected{get;set;}

	public List<IPClasses.PMCCourseContainer> allCourses{get;set;}

	public boolean editAll{get{if(editAll == null) editAll = false; return editAll;} set;}
	public boolean editPackages{get{if(editPackages == null) editPackages = false; return editPackages;} set;}
	public boolean cloneCourses{get{if(cloneCourses == null) cloneCourses = false; return cloneCourses;} set;}
	
	public boolean editCourse{get{if(editCourse == null) editCourse = false; return editCourse;} set;}
	public IPClasses.PMCCourse courseToEdit{get;set;}

	public String idCampus{get;set;}

	public Integer totalCourses{get;set;}

	public List<CampusCourse> campusesToClone{get;set;}

	public List<CoursePerCampus> coursesPerCampus{get;set;}

	//public Map<String, Map<String, List<CoursePerCampus>>> coursesPerCampus{get;set;}

	public school_campus_courses() {
		idCampus = ApexPages.currentPage().getParameters().get('id');
		accSelected = Database.query('SELECT ID, Name, Parent.ID FROM Account WHERE ID = :idCampus');
		loadAllCourses();
	}

	public void loadAllCourses(){

		totalCourses = 0;

		List<Campus_Course__c> courses = [Select Course__r.Course_Category__c, Course__r.Type__c, Course__r.Name, Course__r.Sub_Type__c, Course__r.Course_Type__c, Course__r.Course_Unit_Type__c, Course__r.Course_Unit_Length__c, Course__r.Hours_Week__c, Course__r.Language__c, Course__r.Only_Sold_in_Blocks__c, Course__r.Optional_Hours_Per_Week__c, Course__r.Package__c, Course__r.Id, Course__r.Maximum_length__c, Course__r.Course_Country__c, Course__r.Minimum_length__c, Period__c, Is_Available__c, Unavailable_Reason__c, Package_Courses__c, Available_From__c, Campus_Country__c, Language_Level_Required__c, Length_of_each_Lesson__c, Minimum_age_requirement__c, Course__r.Form_of_Delivery__c, Course__r.Instalment_Option__c, Course__r.Instalment_Term_Option__c, Course__r.Instalment_Term_Interval__c, Course__r.Payment_Rules__c, Course__r.Important_information__c,
		(Select id from client_courses__r limit 1), 
		(Select id from promotions__r limit 1), 
		(Select id from Course_Intake_Dates__r limit 1), 
		(Select id from Quotation_Details__r limit 1), 
		(Select id from Commissions__r limit 1), 
		(Select id from Search_CampusCourse__r limit 1), 
		(Select id from Course_Extra_Fees__r limit 1),
		(Select id from Course_Prices__r limit 1)
		FROM Campus_Course__c WHERE Campus__c = :idCampus ORDER BY Is_Available__c desc, Course__r.Name];

		allCourses = new List<IPClasses.PMCCourseContainer>();
		IPClasses.PMCCourseContainer container;
		IPClasses.PMCCourseCategory category;
		Integer index;
		if(courses != null && !courses.isEmpty()){
			for(Campus_Course__c cc : courses){
				
				container = new IPClasses.PMCCourseContainer(cc.Course__r.Course_Category__c);
				index = allCourses.indexOf(container);
				if(index > -1){
					container = allCourses.get(index);
				}else{
					allCourses.add(container);
				}
				
				category = new IPClasses.PMCCourseCategory(String.isEmpty(cc.Course__r.Type__c) ? 'Other' : cc.Course__r.Type__c);
				index = container.categories.indexOf(category);
				if(index > -1){
					category = container.categories.get(index);
				}else{
					container.categories.add(category);
				}
				//system.debug('THE COURSE '+JSON.serialize(cc));
				//system.debug('THE COURSE '+JSON.serialize(cc.Course__r));
				cc.Course__r.Optional_Hours_Per_Week__c = cc.Course__r.Optional_Hours_Per_Week__c == null ? 0 : cc.Course__r.Optional_Hours_Per_Week__c;
				category.pmcCourses.add(new IPClasses.PMCCourse(cc));
				totalCourses += 1;

			}
		}

		for(IPClasses.PMCCourseContainer courseContainer : allCourses){
			courseContainer.categories.sort();
		}
		allCourses.sort();
	}

	public void saveAllCourses(){
		List<Campus_Course__c> coursesToUpdate = new List<Campus_Course__c>();
		List<Course__c> coursesRToUpdate = new List<Course__c>();
		String period;
		for(IPClasses.PMCCourseContainer courseContainer : allCourses){
			for(IPClasses.PMCCourseCategory category : courseContainer.categories){
				for(IPClasses.PMCCourse pmcCourse : category.pmcCourses){
					period = '';
					for(String periodSave : pmcCourse.periods){
						period = period + ';' + periodSave;
					}
					pmcCourse.course.Period__c = period.removeStart(';');
					coursesToUpdate.add(pmcCourse.course);
					coursesRToUpdate.add(pmcCourse.course.Course__r);
				}
			}
		}
		update coursesToUpdate;
		update coursesRToUpdate;
		this.editAll = false;
		this.editPackages = false;
	}

	public void updateCoursesInCampuses(){
		Map<String, Boolean> statusCampusCourses = new Map<String, Boolean>();
		Map<String, campus_course__c> ccPerCampus = new Map<String, campus_course__c>(); 
		List<Campus_Course__c> campusCoursesToUpdate = new List<Campus_Course__c>();
		List<Campus_Course__c> campusCoursesToSave = new List<Campus_Course__c>();
		Campus_Course__c newCC;

		List<campus_course__c> schoolCampusCourses = [SELECT ID, course__c, course__r.name, campus__c, campus__r.Name, Is_Available__c, Period__c FROM campus_course__c WHERE campus__r.parentId = :accSelected.Parent.ID ORDER BY course__r.name];

		for(campus_course__c cc : schoolCampusCourses){
			statusCampusCourses.put(cc.ID, cc.Is_Available__c);
			ccPerCampus.put(cc.course__c, cc);	
			//if(cc.campus__c == accSelected.ID){
			//}
		}

		for(CoursePerCampus course : coursesPerCampus){
			for(CampusCourse campus : course.campuses){
				if(String.isEmpty(campus.campusCourseID) && campus.isActive){
					newCC = new Campus_Course__c();
					newCC.Is_Available__c = true;
					newCC.Campus__c = campus.campusID;
					newCC.Course__c = course.courseID;
					if(ccPerCampus.containsKey(course.courseID)){
						newCC.Period__c = ccPerCampus.get(course.courseID).Period__c;
					}else{
						newCC.Period__c = 'Morning;Afternoon;Evening;Weekend';
					}
					
					campusCoursesToSave.add(newCC);
				}else if(!String.isEmpty(campus.campusCourseID) && campus.isActive != statusCampusCourses.get(campus.campusCourseID)){
						newCC = new Campus_Course__c(ID = campus.campusCourseID);
						newCC.Is_Available__c = campus.isActive;
						campusCoursesToUpdate.add(newCC);
				}
			}
		}

		if(!campusCoursesToUpdate.isEmpty()){
			update campusCoursesToUpdate;
		}
		if(!campusCoursesToSave.isEmpty()){
			insert campusCoursesToSave;
		}
	}

	public void openCloseCloneCourses(){
		this.cloneCourses = !cloneCourses;
		if(this.cloneCourses){
			CoursePerCampus course;
			CampusCourse campus;
			campusesToClone = new List<CampusCourse>();
			Map<String, CoursePerCampus> mapCoursesPerCampuses = new Map<String, CoursePerCampus>();

			List<Account> campuses = [SELECT ID, Name FROM Account WHERE RecordType.Name = 'Campus' AND Parent.ID = :accSelected.Parent.ID ORDER BY Name];
			
			for(Account c : campuses){
				campus = new CampusCourse();
				campus.campusID = c.ID;
				campus.campusName = c.Name;
				campusesToClone.add(campus);
			}

			List<campus_course__c> schoolCourses = [SELECT ID, course__c, course__r.name, campus__c, campus__r.Name, Is_Available__c FROM campus_course__c WHERE campus__r.parentId = :accSelected.Parent.ID ORDER BY course__r.name];
			
			for(campus_course__c cc : schoolCourses){
				if(!mapCoursesPerCampuses.containsKey(cc.course__c)){
					course = new CoursePerCampus();
					course.courseID = cc.course__c;
					course.courseName = cc.course__r.name;
					for(Account c : campuses){
						campus = new CampusCourse();
						campus.campusID = c.ID;
						campus.campusName = c.Name;
						campus.isActive = false;
						course.campuses.add(campus);
					}
					mapCoursesPerCampuses.put(cc.course__c, course);
				}
				course = mapCoursesPerCampuses.get(cc.course__c);
				for(CampusCourse c : course.campuses){
					if(c.campusID == cc.campus__c){
						c.campusCourseID = cc.ID;
						c.isActive = cc.Is_Available__c;
					}
				}
			}

			coursesPerCampus = mapCoursesPerCampuses.values();
		}else{
			campusesToClone = null;
			coursesPerCampus = null;
		}
	}

	/*public void openCloseCloneCourses(){
		this.cloneCourses = !cloneCourses;
		if(this.cloneCourses){
			coursesPerCampus = new Map<String, Map<String, List<CoursePerCampus>>>(); 
			
			List<campus_course__c> coursesPerSchool = [SELECT course__c, course__r.name, course__r.Is_Unavailable__c, campus__c, campus__r.Name, Is_Available__c FROM campus_course__c WHERE campus__r.parentId = :accSelected.Parent.ID ORDER BY course__r.name];

			campuses = new List<CoursePerCampus>();
			CoursePerCampus cpc;
			List<Account> ccs = [SELECT ID, Name FROM Account WHERE RecordType.Name = 'Campus' AND Parent.ID = :accSelected.Parent.ID ORDER BY Name];
			for(Account campus : ccs){
				cpc = new CoursePerCampus();
				cpc.campusID = campus.ID;
				cpc.campusName = campus.Name;
				cpc.existingOnCampus = false;
				campuses.add(cpc);
			}
			for(campus_course__c cc : coursesPerSchool){
				if(!coursesPerCampus.containsKey(cc.course__c)){
					coursesPerCampus.put(cc.course__c, new Map<String, List<CoursePerCampus>>());
					coursesPerCampus.get(cc.course__c).put(cc.course__r.name, new List<CoursePerCampus>());
					for(Account campus : ccs){
						cpc = new CoursePerCampus();
						cpc.campusID = campus.ID;
						cpc.campusName = campus.Name;
						cpc.existingOnCampus = false;
						if(campus.ID == cc.campus__c){
							cpc.existingOnCampus = cc.Is_Available__c;
						}
						coursesPerCampus.get(cc.course__c).get(cc.course__r.name).add(cpc);
					}
				}else{
					for(CoursePerCampus savedCampuses : coursesPerCampus.get(cc.course__c).get(cc.course__r.name)){
						if(cc.campus__c == savedCampuses.campusID){
							savedCampuses.existingOnCampus = cc.Is_Available__c;
						}
					}
				}
			}
		}else{
			campuses = null;
			coursesPerCampus = null;
		}
	}*/

	/*public void saveCoursesToCampuses(){
		Map<String, Map<String, String>> existingCoursesPerCampus = new Map<String, Map<String, String>>();
		
		List<campus_course__c> campusCourses = [SELECT ID, course__c, course__r.name, course__r.Is_Unavailable__c, campus__c, campus__r.Name FROM campus_course__c WHERE campus__r.parentId = :accSelected.Parent.ID ORDER BY course__r.name];
		for(campus_course__c cc : campusCourses){
			if(!existingCoursesPerCampus.containsKey(cc.course__c)){
				existingCoursesPerCampus.put(cc.course__c, new Map<String, String>());
			}
			existingCoursesPerCampus.get(cc.course__c).put(cc.campus__c, cc.ID);
		}

		Map<String, Set<String>> newCampuseCourses = new Map<String, Set<String>>(); 
		Map<String, Set<String>> campusCoursesToActivate = new Map<String, Set<String>>(); 
		Map<String, Set<String>> campusCoursesToDelete = new Map<String, Set<String>>(); 

		system.debug('existingCoursesPerCampus '+JSON.serialize(existingCoursesPerCampus));

		for(String courseID : coursesPerCampus.keySet()){
			for(String courseName : coursesPerCampus.get(courseID).keySet()){
				for(CoursePerCampus savedCampus : coursesPerCampus.get(courseID).get(courseName)){
					if(savedCampus.existingOnCampus){
						if(existingCoursesPerCampus.containsKey(courseID)){
							if(!existingCoursesPerCampus.get(courseID).containsKey(savedCampus.campusID)){
								if(!newCampuseCourses.containsKey(courseID)){
									newCampuseCourses.put(courseID, new Set<String>());
								}
								newCampuseCourses.get(courseID).add(savedCampus.campusID);
							}else{
								if(!campusCoursesToActivate.containsKey(courseID)){
									campusCoursesToActivate.put(courseID, new Set<String>());
								}
								campusCoursesToActivate.get(courseID).add(savedCampus.campusID);
							}
						}
					}else{
						if(existingCoursesPerCampus.containsKey(courseID)){
							if(existingCoursesPerCampus.get(courseID).containsKey(savedCampus.campusID)){
								if(!campusCoursesToDelete.containsKey(courseID)){
									campusCoursesToDelete.put(courseID, new Set<String>());
								}
								campusCoursesToDelete.get(courseID).add(savedCampus.campusID);
							}
						}
					}
				}
			}
		}
		if(!campusCoursesToActivate.isEmpty()){
			List<campus_course__c> ccToUpdate = new List<campus_course__c>();
			campus_course__c cc;
			for(String courseID : campusCoursesToActivate.keySet()){
				for(String campusID : campusCoursesToActivate.get(courseID)){
					cc = new campus_course__c(ID = existingCoursesPerCampus.get(courseID).get(campusID));
					cc.Is_Available__c = true;
					ccToUpdate.add(cc);
				}
			}
			update ccToUpdate;
		}
		if(!newCampuseCourses.isEmpty()){
			List<campus_course__c> newCCs = new List<campus_course__c>();
			campus_course__c newCC;
			for(String courseID : newCampuseCourses.keySet()){
				for(String campusID : newCampuseCourses.get(courseID)){
					newCC = new campus_course__c();
					newCC.Is_Available__c = true;
					newCC.Course__c = courseID;
					newCC.Campus__c = campusID;
					newCCs.add(newCC);
				}
			}
			insert newCCs;
		}
		if(!campusCoursesToDelete.isEmpty()){
			List<campus_course__c> toDelete = new List<campus_course__c>();
			for(campus_course__c cc : campusCourses){
				if(campusCoursesToDelete.containsKey(cc.course__c) && campusCoursesToDelete.get(cc.course__c).contains(cc.campus__c)){
					cc.Is_Available__c = false;
					toDelete.add(cc);
				}
			}
			//delete toDelete;
			update toDelete;
		}

		//openCloseCloneCourses();
}*/

	public class CoursePerCampus{
		public String courseID{get;set;}
		public String courseName{get;set;}
		public List<CampusCourse> campuses{get;set;}

		public CoursePerCampus(){
			campuses = new List<CampusCourse>();	
		}
	}

	public class CampusCourse{
		public String campusID{get;set;}
		public String campusName{get;set;}
		public String campusCourseID{get;set;}
		public Boolean isActive{get;set;}
	}

	public void openCloseEditCourse(){
		this.editCourse = !editCourse;
		courseToEdit = null;

		if(editCourse){
			String container = ApexPages.currentPage().getParameters().get('container');
			String category = ApexPages.currentPage().getParameters().get('category');
			String idCourse = ApexPages.currentPage().getParameters().get('idCourse');

			for(IPClasses.PMCCourseContainer containerCourses : allCourses){
				if(containerCourses.type == container){
					for(IPClasses.PMCCourseCategory categoryCourses : containerCourses.categories){
						if(categoryCourses.category == category){
							for(IPClasses.PMCCourse pmcCourse : categoryCourses.pmcCourses){
								if(pmcCourse.course.ID == idCourse){
									courseToEdit = pmcCourse;
									break;
								}
							}
						}
					}
				}
			}
		}
	}

	public void openCloseEditAll(){
		this.editAll = !editAll;
	}

	public void openCloseUpdatePackages(){
		this.editPackages = !editPackages;
	}
	
	public List<SelectOption> getPeriods(){
       	List<SelectOption> Periods = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = Campus_Course__c.Period__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple)
			Periods.add(new SelectOption(f.getLabel(), f.getValue()));
        return Periods;
    }

	public List<SelectOption> getCourses(){
        List<SelectOption> courses = new List<SelectOption>();
        for(Campus_Course__c cc : [select id, course__r.Name, course__r.Hours_Week__c, course__r.Optional_Hours_Per_Week__c, course__r.Course_Unit_Type__c, Is_Available__c from Campus_Course__c where campus__c = :accSelected.ID and course__r.Package__c = false order by course__r.Name]){
			String courseName = cc.course__r.Name + getCourseHours(cc.course__r);
			if(!cc.Is_Available__c)
				courseName += ' (unavailable)';
			courses.add(new SelectOption(cc.id, courseName));
		}
        return courses;
    }

	public String getCourseHours(Course__c course){
        String courseHours = '';
        if(course.Hours_Week__c != null && course.Hours_Week__c != 0){
            courseHours = ' (' + Integer.valueOf(course.Hours_Week__c) + 'hrs';

            if(course.Optional_Hours_Per_Week__c != null && course.Optional_Hours_Per_Week__c != 0){
                courseHours += ' + ' + Integer.valueOf(course.Optional_Hours_Per_Week__c) + ' opt./' + course.Course_Unit_Type__c + ')';
            } else {
                courseHours += '/'+ course.Course_Unit_Type__c + ')';
            }

        }
        return courseHours;
    }

	public void deleteCourse(){ 
		string ccId = ApexPages.currentPage().getParameters().get('ccId');
		campus_course__c cc = [Select id, course__c from campus_course__c where id = :ccId limit 1];
		string courseId = cc.course__c;
		//delete cc;
		IPFunctions.SearchNoSharing sns = new IPFunctions.SearchNoSharing();
		sns.deleteNoSharing(cc);
		
		String sqlCount = 'Select count() from campus_course__c where course__c = :courseId ';
		integer total_size = Database.countQuery(sqlCount);
		if(total_size == 0){
			course__c c = [Select id from course__c where id = :courseId limit 1];
			sns.deleteNoSharing(c);
		}
		loadAllCourses();
	}

}