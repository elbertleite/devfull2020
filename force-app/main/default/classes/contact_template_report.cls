public with sharing class contact_template_report {
	
	public Blob csvFileBody{get;set;}
	public string csvAsString{get;set;}
	public map<string,string> listTemplates{get; set;}
	private map<string, Object> mapFields;
	private list<string> fieldsTemplate;

	string sqlWhere;
	string htmlAsString;

	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}

	public contact_template_report(){

		retrieveListTemplate();
		retrieveListStages();
		retrieveAgencyGroupOptions();
		agencyOptions();
		retrieveDepartments();
		

		mapFields = IPFunctions.findTokens(IPFunctions.tokensContact);

		// mapFields.put('Current_Agency',new emailTemplateDynamic.fieldNameType('Current_Agency__r.name','string'));
		// mapFields.put('Original_Agency',new emailTemplateDynamic.fieldNameType('Original_Agency__r.name','string'));
		// mapFields.put('name',new emailTemplateDynamic.fieldNameType('name','string'));
		// mapFields.put('firstName',new emailTemplateDynamic.fieldNameType('firstName','string'));
		// mapFields.put('lastName',new emailTemplateDynamic.fieldNameType('lastName','string'));
		// mapFields.put('Visa_Expiry_Date',new emailTemplateDynamic.fieldNameType('visa_Expiry_Date__c','date'));
		// mapFields.put('Country_of_Birth',new emailTemplateDynamic.fieldNameType('Nationality__c','string'));
		// mapFields.put('Birthdate',new emailTemplateDynamic.fieldNameType('Birthdate','date')); 
		// mapFields.put('Email',new emailTemplateDynamic.fieldNameType('Email','string'));
		// mapFields.put('Arrival_Date',new emailTemplateDynamic.fieldNameType('Arrival_Date__c','date'));
		// mapFields.put('Expected_Travel_Date',new emailTemplateDynamic.fieldNameType('Expected_Travel_Date__c','date'));
		// mapFields.put('Destination_Country',new emailTemplateDynamic.fieldNameType('Destination_Country__c','string'));
		
		// mapFields.put('Client',new emailTemplateDynamic.fieldNameType('Client__r.name', 'string'));
		// mapFields.put('Email',new emailTemplateDynamic.fieldNameType('Client__r.owner__r.name', 'string'));
		// mapFields.put('Course_Name',new emailTemplateDynamic.fieldNameType('Course_Name__c', 'string'));
		// mapFields.put('created',new emailTemplateDynamic.fieldNameType('Client__r.owner__r.createdDate', 'datetime'));
	}

	public list<contact> listResult{get; set;} 



	public pageReference generateTemplate(){

		System.debug('==> selectedDestinationCountry: ' + selectedDestinationCountry); 
		System.debug('==> selectedStage: ' + selectedStage);
		System.debug('==> stagesItems: ' + selectedStageItem );

		selectedDestinationCountry = ApexPages.CurrentPage().getParameters().get('destCountry');
		selectedStage = ApexPages.CurrentPage().getParameters().get('stage');
		selectedStageItem = ApexPages.CurrentPage().getParameters().get('stageItems');

		sqlWhere = ' , isSelected__c from contact where ';
			
		if(selectedClientAgency == 1)
			sqlWhere += ' Current_Agency__r.ParentId = \''+selectedAgencyGroup +'\' and ';
		else
			sqlWhere += ' Original_Agency__r.ParentId = \''+selectedAgencyGroup +'\' and ';

		if(selectedAgency != 'all'){
			if(selectedClientAgency == 1)
				sqlWhere += ' Current_Agency__c = \''+selectedAgency +'\' and ';
			else
				sqlWhere += ' Original_Agency__c = \''+selectedAgency +'\' and ';
		}


		if(selectedDepartment != 'all')
			sqlWhere += ' Owner__r.Contact.Department__c = \''+selectedDepartment +'\' and ';

		if(selectedClientOwnership != 'all')
			sqlWhere += ' Owner__c = \''+selectedClientOwnership +'\' and ';
			

		if(selectedNationality != 'all')
			sqlWhere += ' Nationality__c = \''+selectedNationality +'\' and ';

		if(selectedDestinationCountry == '')
			selectedDestinationCountry = null;
		if(selectedStageItem != null && selectedStageItem != '' && selectedStageItem != 'all'){
			if(selectedDestinationCountry != null)
				sqlWhere += 'Destination_Country__c =  \''+selectedDestinationCountry +'\' and status__c = \''+selectedStageItem +'\' and ';
			else sqlWhere += ' status__c = \''+selectedStageItem +'\' and ';
		}


		if(selectedType == 'visa'){
			sqlWhere += ' visa_Expiry_Date__c >= ' + FormatSqlDate(fromDate.Expected_Travel_Date__c) + ' and  visa_Expiry_Date__c <= ' + FormatSqlDate(todate.Expected_Travel_Date__c) + ' ';
			sqlWhere += ' order by visa_Expiry_Date__c ';
		}else if(selectedType == 'birthdate'){
			sqlWhere += ' CALENDAR_MONTH(Birthdate) = ' + selectedMonth + ' ';
			sqlWhere += ' order by Birthdate ';
		}else if(selectedType == 'arrival'){
			sqlWhere += ' Arrival_Date__c >= ' + FormatSqlDate(fromDate.Expected_Travel_Date__c) + ' and  Arrival_Date__c <= ' + FormatSqlDate(todate.Expected_Travel_Date__c) + ' ';
			sqlWhere += ' order by Arrival_Date__c ';
		}else if(selectedType == 'travel'){
			sqlWhere += ' Expected_Travel_Date__c >= ' + FormatSqlDate(fromDate.Expected_Travel_Date__c) + ' and  Expected_Travel_Date__c <= ' + FormatSqlDate(todate.Expected_Travel_Date__c) + ' ';
			sqlWhere += ' order by Expected_Travel_Date__c ';
		}else if(selectedType == 'created'){
			sqlWhere += ' DAY_ONLY(convertTimezone(createdDate)) >= ' + FormatSqlDate(fromDate.Expected_Travel_Date__c) + ' and  DAY_ONLY(convertTimezone(createdDate)) <= ' + FormatSqlDate(todate.Expected_Travel_Date__c) + ' ';
			sqlWhere += ' order by createdDate ';
		}

		
		try{
			// if(csvFileBody == null){
			// 	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a Template.');
			// 	ApexPages.addMessage(myMsg);
			// 	return null;
			// }
			Email_Template__c tp = [Select Template__c, Email_Subject__c from Email_Template__c where id = :selectedTemplate limit 1];
			htmlAsString = tp.Template__c;
			emailSubject = tp.Email_Subject__c;
			fieldsTemplate = emailTemplateDynamic.extractFieldsFromFile(htmlAsString, mapFields);  
			listResult = emailTemplateDynamic.createListResult(sqlWhere, mapFields, htmlAsString, fieldsTemplate);
			if(listResult.size() > 0)
				listTemplates = emailTemplateDynamic.createTemplate(mapFields, htmlAsString, fieldsTemplate, new list<sObject>{listResult[0]});
			else listTemplates = null;
		}catch(Exception e){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Records Found or Invalid Template.');
			ApexPages.addMessage(myMsg);
			//ApexPages.addMessages(e);
			return null;
		}
		return null;
	}


	/** Send Emails **/
	public Contact userDetails{
	    get{
	      if(userDetails == null){
	      	if(!Test.isRunningTest()){
	      		string contactId = [Select ContactId from user where id = :UserInfo.getUserId() limit 1].ContactId;
	       		userDetails = [select id, Name, Email, Signature__c, AccountId, Account.Name, Account.ParentId, Account.Global_Link__c, Account.Clients_Payments_Email__c from Contact where id = :contactId];
				
	      	} else {
	      		userDetails = new Contact();
	      	}
	      }
	      return userDetails;
	    }
	    set;
	}

	public string selectedTemplate{get; set;}
	public list<SelectOption> listTemplatesAgency{get; set;}
	public void retrieveListTemplate(){
		listTemplatesAgency = new List<SelectOption>();
		listTemplatesAgency.add(new SelectOption('all', '-- Please Select a Template --'));
		for(Email_Template__c ag : [SELECT id, Template_Description__c FROM Email_Template__c where agency__c = :userDetails.AccountId and Category__c = 'Lead/Client Mass Emails' order by Template_Description__c ]){
			listTemplatesAgency.add(new SelectOption(ag.id, ag.Template_Description__c));
		}
	}

	public string emailSubject{
		get{
			if(emailSubject == null)
				emailSubject = '';
			return emailSubject;
		}
		set;
	}

	public pageReference sendEmail(){
		if(emailSubject.trim() == ''){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please fill in the subject.');
			ApexPages.addMessage(myMsg);
			return null;
		}
		generateEmail(emailSubject);
		listResult = new list<contact>();
		emailSubject = '';
		return null;
	}


	private Custom_Note_Task__c createTask(contact ct){
		return new Custom_Note_Task__c(
			Subject__c = 'Visa Follow up',
			Priority__c = '1 - Medium',
			Comments__c = 'Email Sent - Visa Follow up',
			Related_Contact__c = ct.id,
			Due_Date__c = System.today().addDays(7),
			Status__c ='In Progress',
			Assign_To__c = ct.Owner__c,

			AssignedOn__c = DateTime.now(),
			AssignedBy__c = UserInfo.getUserId()
		);
	}

	public void generateEmail(string subject){
		
		list<Custom_Note_Task__c> lTasks = new list<Custom_Note_Task__c>();
		map<string, string> mSubject = new map<string, string>();
		try{

			/** Send email via Mandrill **/
			String emailBody;
			String instalmentInfo;
			

			List<mandrillSendEmail.messageJson> listEmails = new list<mandrillSendEmail.messageJson>();

			list<contact> selectedContacts = new list<contact>();
			for(contact ct : listResult){
				system.debug('ct.isSelected__c ==' + ct.Name + ' - ' +ct.isSelected__c);
				if(ct.isSelected__c){
					selectedContacts.add(ct);
					mSubject.put(ct.id, emailTemplateDynamic.createSingleTemplate(mapFields, subject, fieldsTemplate, ct));
				}
			}

			system.debug('==> htmlAsString: ' + htmlAsString);
			map<string,string> templates = emailTemplateDynamic.createTemplate(mapFields, htmlAsString, fieldsTemplate, selectedContacts);


			system.debug('selectedContacts ==' + selectedContacts);
			for(contact a : selectedContacts){
				//create task for Visa Follow up
				if(selectedType == 'visa')
					lTasks.add(createTask(a));

				mandrillSendEmail mse = new mandrillSendEmail();
				mandrillSendEmail.messageJson email_md = new mandrillSendEmail.messageJson();
				//List<String> tAddress = a.getToAddresses();

				email_md.setTo(a.Email, a.Name);
				// if(agency.Clients_Payments_Email__c!=null)
				// 	email_md.setFromEmail(agency.Clients_Payments_Email__c);
				// else
					email_md.setFromEmail(userDetails.Email);

				email_md.setFromName(userDetails.Name);
				email_md.setSubject(mSubject.get(a.id));

				emailBody = templates.get(a.id);
				system.debug('==>emailBody:' + emailBody);
				//if(useSignature) emailBody += '<br /><div style="margin-top: 20px;" class="sig">'+/*getSignature()+*/'</div>';
				email_md.setHtml(emailBody);

				system.debug('mandrill ==' + email_md);

				listEmails.add(email_md);
				//mse.sendMail(email_md); //send Email


				/** Save to S3 **/
				String emailService = IpFunctions.getS3EmailService();
				EmailToS3Controller s3 = new EmailToS3Controller();
				Datetime myDT = Datetime.now();
				String myDate = myDT.format('dd-MM-yyyy HH:mm:ss');
				Blob bodyblob = Blob.valueof(templates.get(a.id));//Blob body, String subject, String instalment, Contact client, Contact employee, String dateCreated
				s3.generateEmailToS3(a, userDetails, true, mSubject.get(a.id), bodyblob, false, myDate);
				//s3.instalmentEmailS3(bodyblob,subject,a.getWhatId(), mapContactsEmail.get(tAddress[0]),userDetails, myDate, false);
				/** END -- Save to S3 **/

			}//end for

			if(lTasks.size() > 0)
				insert lTasks;

			//Batch to send emails
			if(!Test.isRunningTest()){
				batch_sendMassEmail batchSendMass = new batch_SendMassEmail(listEmails);
				Database.executeBatch(batchSendMass,50);
			}
			/** END -- Send email via Mandrill **/
	
			//theMsg = 'Email sent.';
		   // showMsg = true;

		}catch(Exception e){
			system.debug('Send Emails Error ==='+ e);
			//theMsg = e.getMessage();
		   // showMsg = true;
		}

	}
	/** END -- Send Emails **/


	//======================================== FILTERS ============================================================================

	public String selectedAgencyGroup {get{if(selectedAgencyGroup == null) selectedAgencyGroup = userDetails.Account.ParentId; return selectedAgencyGroup;}set;}
	public List<SelectOption> agencyGroupOptions {get; set;}
	public void retrieveAgencyGroupOptions() {
		agencyGroupOptions = new List<SelectOption>();
		for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
		}
		
	}

	

	
	public String selectedAgency {get{if(selectedAgency == null) selectedAgency = userDetails.AccountId; return selectedAgency;}set;}
	public List<SelectOption> agencyOptions{get; set;}
	public void agencyOptions() {
		agencyOptions = new List<SelectOption>();
		agencyOptions.add(new SelectOption('all', '--All--'));
		for(Account ag : [SELECT Id, Name FROM Account WHERE ParentId = :selectedAgencyGroup and RecordType.Name = 'Agency'   order by Name]){
			agencyOptions.add(new SelectOption(ag.Id, ag.Name));
		}
		if(userDetails.Account.ParentId == selectedAgencyGroup)
			selectedAgency = userDetails.AccountId;
		else selectedAgency = 'all';
		retrieveListStages();
		retrieveClientOwnership();
	}

	public String selectedType {get{if(selectedType == null) selectedType = 'visa'; return selectedType;}set;}
	public List<SelectOption> contactTypeOptions {
		get{
			if(contactTypeOptions == null){
				contactTypeOptions = new List<SelectOption>();
				contactTypeOptions.add(new SelectOption('visa', 'Visa Expiry Date (VED)'));
				contactTypeOptions.add(new SelectOption('birthdate', 'Lead/Client Birthdate (DOB)'));
				contactTypeOptions.add(new SelectOption('arrival', 'Client Arrival Date'));
				contactTypeOptions.add(new SelectOption('travel', 'Expected Travel Date'));
				contactTypeOptions.add(new SelectOption('created', 'Created Date'));
			}
			return contactTypeOptions;
		}
		set;
	}

	public String selectedNationality { get{if(selectedNationality == null) selectedNationality = 'all'; return selectedNationality;} set;}
	public List<SelectOption> destinations {
		get{
			if(destinations == null){
				destinations = new List<SelectOption>();
				destinations = IPFunctions.getAllCountries();
				destinations.add(0, new SelectOption('all','--ALL--'));
			}
			return destinations;
		}
		set;
	}

	public Contact fromDate {
		get{
			if(fromDate == null){
				fromDate = new Contact();
				fromDate.Expected_Travel_Date__c = system.today();
			}
			return fromDate;}
		set;}

	public Contact toDate {
		get{
			if(toDate == null){
				toDate = new Contact();
				toDate.Expected_Travel_Date__c = system.today().addMonths(1);
			}
			return toDate;
		}
		set;
	}

	private  string FormatSqlDate(date d){
		string day = string.valueOf(d.day());
		string month = string.valueOf(d.month());
		if (day.length() == 1)
			day = '0'+day;
		if (month.length() == 1)
			month = '0'+month;
		return d.year()+'-'+month+'-'+day;
	}

	
	public string selectedDepartment {get{if(selectedDepartment==null) selectedDepartment = 'all'; return selectedDepartment;}set;}
	public list<SelectOption> departments {get;set;}
	public void retrieveDepartments(){
		departments = new List<SelectOption>();
		departments.add(new SelectOption('all', '-- All --'));
		if(selectedAgency != 'all')
			for(Department__c d: [Select Id, Name from Department__c where Agency__c = :selectedAgency and Inactive__c = false]){
				departments.add(new SelectOption(d.Id, d.Name));
		}//end for
		retrieveClientOwnership();
	}

	public string selectedClientOwnership {get{if(selectedClientOwnership==null) selectedClientOwnership = 'all'; return selectedClientOwnership;}set;}
	public list<SelectOption> ClientOwnership {get;set;}
	public void retrieveClientOwnership(){
		ClientOwnership = new List<SelectOption>();
		ClientOwnership.add(new SelectOption('all', '-- All --')); 
		if(selectedAgency != 'all')
			for(User u: [Select Id, Name from User where accountId = :selectedAgency and isActive = true order by Name]){
				ClientOwnership.add(new SelectOption(u.Id, u.Name));
		}//end for
		
	}

	public integer selectedMonth {get{if(selectedMonth == null) selectedMonth = System.today().month(); return selectedMonth;}set;}
	public List<SelectOption> listMonths {
		get{
			if(listMonths == null){
				listMonths = new List<SelectOption>();
				listMonths.add(new SelectOption('1', 'January'));
				listMonths.add(new SelectOption('2', 'February'));
				listMonths.add(new SelectOption('3', 'March'));
				listMonths.add(new SelectOption('4', 'April'));
				listMonths.add(new SelectOption('5', 'May'));
				listMonths.add(new SelectOption('6', 'June'));
				listMonths.add(new SelectOption('7', 'July'));
				listMonths.add(new SelectOption('8', 'August'));
				listMonths.add(new SelectOption('9', 'September'));
				listMonths.add(new SelectOption('10','October'));
				listMonths.add(new SelectOption('11','November'));
				listMonths.add(new SelectOption('12','December'));
			}
			return listMonths;
		}
		set;
	}

	public integer selectedClientAgency {get{if(selectedClientAgency == null) selectedClientAgency = 1; return selectedClientAgency;}set;}
	public List<SelectOption> listClientOwnerType {
		get{
			if(listClientOwnerType == null){
				listClientOwnerType = new List<SelectOption>();
				listClientOwnerType.add(new SelectOption('1', 'Current Agency'));
				listClientOwnerType.add(new SelectOption('2', 'Original Agency'));
			}
			return listClientOwnerType;
		}
		set;
	}

	public class listOptions{
		public string optLabel{get; set;}
		public string optValue{get; set;}
		public listOptions(string optValue, string optLabel){
			this.optLabel = optLabel;
			this.optValue = optValue;
		}
	}

	

	public List<listOptions> destinationsCountry{get; set;}
	public List<listOptions> stages{get; set;}
	public List<listOptions> stagesItems{get; set;}

	public string selectedDestinationCountry {get; set;}
	public string selectedStage {get{if(selectedStage==null) selectedStage = 'all'; return selectedStage;}set;}
	public string selectedStageItem {get{if(selectedStageItem==null) selectedStageItem = 'all'; return selectedStageItem;}set;}

	public map<string, list<string>> listStages{get; set;}
	public void retrieveListStages(){
		//selectedDestinationCountry = 'all';
		selectedStage = 'all';
		selectedStageItem = 'all';

		listStages = new map<string, list<string>>();
		stages = new List<listOptions>();
		stagesItems = new List<listOptions>();
		destinationsCountry = new List<listOptions>();
		set<string> dest = new set<string>();

		stages.add(new listOptions('all', '-- Select Stage --'));
		stagesItems.add(new listOptions('all', '-- Select Stage Item --'));
		destinationsCountry.add(new listOptions('all', '-- Select Destination --'));
		destinationsCountry.add(new listOptions('', 'All Destinations'));

		System.debug('==>selectedAgencyGroup: '+selectedAgencyGroup);
		for(Client_Stage__c ce:[Select id, Stage__c, Stage_description__c, Agency_Group__r.name, Destination__c, Lead_Stage__c  From Client_Stage__c where Agency_Group__c = :selectedAgencyGroup order by Destination__c, Stage__c, itemOrder__c]){
			if(ce.Stage_description__c.containsIgnoreCase('xxxxxx'))
				continue;
			if(ce.Destination__c == null)
				ce.Destination__c = '';
			if(!listStages.containsKey(ce.Stage__c+ce.Destination__c)){
				dest.add(ce.Destination__c);
				listStages.put(ce.Stage__c+ce.Destination__c, new list<string>{ce.Stage_description__c});
				stages.add(new listOptions(ce.Destination__c, ce.Stage__c));
				stagesItems.add(new listOptions(ce.Destination__c+ce.Stage__c, ce.Stage_description__c));
			}
			else{
				listStages.get(ce.Stage__c+ce.Destination__c).add(ce.Stage_description__c);

				stagesItems.add(new listOptions(ce.Destination__c+ce.Stage__c, ce.Stage_description__c));
			} 
		}
		//destinationsCountry.add(new listOptions('all', '-- Select Country --'));
		System.debug('==>dest: '+dest);
		for(string st:dest)
			destinationsCountry.add(new listOptions(st, st));
	}

	
}