@isTest
private class client_course_cancel_followup_test {

	public static Account school {get;set;}
	public static Account agency {get;set;}
	public static Contact emp {get;set;}
	public static Contact client {get;set;}
	public static User portalUser {get{
    if (null == portalUser) {
      portalUser = [Select Id, Name, Contact.AccountId, Contact.Account.ParentId, Contact.Department__c from User where email = 'test12345@test.com' limit 1];
    } return portalUser;} set;}
	public static Account campus {get;set;}
	public static Course__c course {get;set;}
	public static Campus_Course__c campusCourse {get;set;}
	public static client_course__c clientCourseBooking {get;set;}
	public static client_course__c clientCourse {get;set;}
    
	@testSetup static void setup() {
		TestFactory tf = new TestFactory();
		Account schGroup = tf.createSchoolGroup();

		school = tf.createSchool();
		school.ParentId = schGroup.Id;
		update school;

		agency = tf.createAgency();
		emp = tf.createEmployee(agency);
		portalUser = tf.createPortalUser(emp);
		campus = tf.createCampus(school, agency);
		course = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);

		Schema.DescribeFieldResult fieldResult = client_course__c.Credit_Reason__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		
		system.runAs(portalUser){
			client = tf.createClient(agency);
			client.Owner__c = portalUser.Id;
			update client;

			clientCourseBooking = tf.createBooking(client);
			clientCourse = tf.createClientCourse(client, school, campus, course, campusCourse, clientCourseBooking);
			
			//------------- Create Cancel Request ------------------//
			client_course__c course = [SELECT Id, (Select Id from client_course_instalments__r Where ((Received_Date__c != null AND Paid_To_School_On__c = null AND isMigrated__c = false AND isPDS__c = false AND isPCS__c = false AND Original_Instalment__c = NULL) OR (isInstalment_Amendment__c = true AND Total_School_Payment__c> 0 AND Paid_To_School_On__c = null AND isPDS__c = false AND isPCS__c = false))) FROM client_course__c WHERE Booking_Number__c != null AND isCourseCredit__c = FALSE and isDeposit__c = FALSE];

			ApexPages.currentPage().getParameters().put('id', course.Id);
			client_course_cancel test = new client_course_cancel();

			client_course_cancel.saveCancel('undefined', client_course_cancel.TYPE_DRAFT, new list<String>{course.Id}, ple[0].getValue(), '0', '');
		}
  	}

	public static String requestId {get{
    if (null == requestId) {
      requestId = [SELECT Id FROM client_course__c WHERE isCancelRequest__c = true limit 1].Id;
    } return requestId;} set;}

	
	static testMethod void contructor(){
		system.runAs(portalUser){
			client_course_cancel_followup test = new client_course_cancel_followup();
		}
	}

	static testMethod void changeFilters(){

		system.runAs(portalUser){
			client_course_cancel_followup.changeGroup(portalUser.Contact.Account.ParentId);
			client_course_cancel_followup.changeAgency(portalUser.Contact.AccountId);
			client_course_cancel_followup.changeDepartment(portalUser.Contact.AccountId, portalUser.Contact.Department__c);
		}
	}

	static testMethod void addNote(){
		system.runAs(portalUser){
			client_course_cancel_followup.addNote(requestId, 'Test', 'pending');
		}
	}

	static testMethod void requestToADM(){
		system.runAs(portalUser){
			client_course_cancel_followup.requestToADM(requestId, 'note', 'reqSchool', portalUser.Contact.Account.ParentId, portalUser.Contact.AccountId, portalUser.Contact.AccountId, 'all', '');
		}
	}

	static testMethod void undoRequest(){
		system.runAs(portalUser){
			client_course_cancel_followup.undoRequest(requestId, 'note', 'reqSchool', portalUser.Contact.Account.ParentId, portalUser.Contact.AccountId, portalUser.Contact.AccountId, 'all', '');
		}
	}

	static testMethod void strings(){

		system.runAs(portalUser){
			client_course_cancel_followup test = new client_course_cancel_followup();
			String requests = test.requests;
			String SEPARATOR_FILE = test.SEPARATOR_FILE;
			String SEPARATOR_URL = test.SEPARATOR_URL;
			String notesSplit = test.notesSplit;
			String noteFld = test.noteFld;
			String typeDraft = test.typeDraft;
			String typeMissDocs = test.typeMissDocs;
			String typeCourseTransferred = test.typeCourseTransferred;
			String typeCourseRefunded = test.typeCourseRefunded;
		}
	}
	static testMethod void findRequests(){

		system.runAs(portalUser){
			client_course_cancel_followup.findRequests('pending', portalUser.Contact.Account.ParentId, portalUser.Contact.AccountId, portalUser.Contact.AccountId, 'all', '');
			client_course_cancel_followup.findRequests('reqSchool', portalUser.Contact.Account.ParentId, portalUser.Contact.AccountId, portalUser.Contact.AccountId, 'all', '');
			client_course_cancel_followup.findRequests('transfer', portalUser.Contact.Account.ParentId, portalUser.Contact.AccountId, portalUser.Contact.AccountId, 'all', '');
			client_course_cancel_followup.findRequests('refund', portalUser.Contact.Account.ParentId, portalUser.Contact.AccountId, portalUser.Contact.AccountId, 'all', '');
		}
	}
	
}