public with sharing class commissions_find_invoice {

    public Invoice__c result {get;set;}
    public client_course_instalment__c resultInstallmentInvoice {get;set;}
    public String invoiceNumber {get{if(invoiceNumber == null) invoiceNumber = ''; return invoiceNumber;}set;}
    public String link {get;set;}
    public Map<String, String> invoiceLinks{get;set;}

    public commissions_find_invoice() {
        result = new Invoice__c();
    }

    // private void setInvoiceLink(){
    //     if(invoiceResult.isCommission_Invoice__c){
    //         this.link = this.replaceLink('commissions_school_commission_invoice?i=' + invoiceResult.id);

    //     }else if(invoiceResult.isShare_Commission_Invoice__c){
    //         this.link = this.replaceLink('share_commission_invoice?id=' + invoiceResult.id);

    //     }else if(invoiceResult.isPDS_Confirmation__c){
    //         this.link = this.replaceLink('payment_school_request_confirmation?i=' + invoiceResult.id);
    //     }
    // }

    // private String replaceLink(String paramURI){
    //     return System.URL.getCurrentRequestUrl().toExternalForm().replace('commissions_find_invoice', paramURI);
    // }

    public list<IPFunctions.s3Docs> payDocs {get;set;}
    public void searchInvoice(){
        invoiceLinks = new Map<String, String>();
        if(invoiceNumber.length()>0){
            resultInstallmentInvoice = null;
            result = null;
            invoiceNumber = invoiceNumber.trim();
            try{
                result = [SELECT Id, createdBy.Name, Due_Date__c, Sent_On__c, Sent_Email_Receipt__c, Requested_Commission_To_Agency__r.Name, Sent_Email_To__c, isShare_Commission_Invoice__c, isCommission_Invoice__c, isProvider_Commission__c, isPDS_Confirmation__c, Requested_by_Agency__r.Name, Received_by__r.Name, Confirmed_By__r.Name, School_Name__c, Paid_On__c, Paid_by_Agency__r.Name, Paid_by__r.Name, Confirmed_Date__c, Total_Value__c, Total_Instalments__c, Total_Instalments_Requested__c, Request_Completed_by_School_On__c, isSchool_to_Answer__c, Request_Confirmation_Completed_On__c, Request_Confirmation_Completed_by__r.Name, isCancelled__c, Cancel_Reason__c, Cancel_Description__c, Provider__r.Provider_Name__c, External_Reference__c, isProvider_Payment__c, isCreditCard_payment__c, Provider_Payment_Number__c, Creditcard_Payment_Number__c, preview_link__c, Paid_Date__c, isSchool_Payment__c, hasPFS__c FROM Invoice__c WHERE ((Provider_Payment_Number__c =:invoiceNumber OR Share_Commission_Number__c =:invoiceNumber OR Request_Confirmation_Number__c =:invoiceNumber OR School_Invoice_Number__c =:invoiceNumber OR Request_Provider_Commission__c =:invoiceNumber OR Creditcard_Payment_Number__c =:invoiceNumber OR External_Reference__c =:invoiceNumber ) OR School_Payment_Number__c =:invoiceNumber )
                AND (isCommission_Invoice__c = TRUE OR isShare_Commission_Invoice__c = TRUE OR isProvider_Commission__c = TRUE OR isPDS_Confirmation__c = TRUE OR isCreditCard_payment__c = TRUE OR isProvider_Payment__c = TRUE OR isSchool_Payment__c = TRUE) limit 1];
                PageReference pg;  
                if(result!= null)

                    if(result.isCommission_Invoice__c){
                        pg = Page.commissions_school_commission_invoice;
                        link =  pg.getUrl() + '?i=' + result.id;
                        invoiceLinks.put(invoiceNumber, link);
                    }
                    
                    else if(result.isShare_Commission_Invoice__c){
                        pg = Page.share_commission_invoice;
                        link =  pg.getUrl() + '?id=' + result.id;
                        invoiceLinks.put(invoiceNumber, link);
                    }

                    else if(result.isProvider_Commission__c){
                        pg = Page.product_commission_invoice;
                        link =  pg.getUrl() + '?id=' + result.id;
                        invoiceLinks.put(invoiceNumber, link);
                    }

                    //PROVIDER PAYMENT
                    else if(result.isProvider_Payment__c){
                        pg = Page.provider_payment_invoice;
                        link =  pg.getUrl() + '?id=' + result.id;
                        invoiceLinks.put(invoiceNumber, link);

                        if(result.preview_link__c != null)
                            payDocs = IPFunctions.loadPreviewLink(result.preview_link__c);
                    }
                    //PROVIDER PAYMENT
                    else if(result.isCreditCard_payment__c){
                        pg = Page.agency_creditcard_payment_invoice;
                        link =  pg.getUrl() + '?id=' + result.id;
                        invoiceLinks.put(invoiceNumber, link);

                        if(result.preview_link__c != null)
                            payDocs = IPFunctions.loadPreviewLink(result.preview_link__c);
                    }

                    else if(result.isPDS_Confirmation__c){
                        pg = Page.payment_school_request_confirmation;
                        link =  pg.getUrl() + '?i=' + result.id;
                        invoiceLinks.put(invoiceNumber, link);
                    }
                    
                    if(result.isSchool_Payment__c){
                        pg = Page.payment_school_grouped_invoice;
                        if(result.hasPFS__c && result.Total_Value__c != result.Total_Instalments__c){
                            link =  pg.getUrl() + '?id=' + result.id+'&type=gSchoolInvoice';
                            invoiceLinks.put(invoiceNumber + ' (School Invoice)', link);

                            link =  pg.getUrl() + '?id=' + result.id+'&type=pfsList';
                            invoiceLinks.put(invoiceNumber + ' (PFS)', link);
                        }else if(result.hasPFS__c && result.Total_Value__c == result.Total_Instalments__c){
                            link =  pg.getUrl() + '?id=' + result.id+'&type=pfsList';
                            invoiceLinks.put(invoiceNumber, link);
                        }else{
                            link =  pg.getUrl() + '?id=' + result.id+'&type=gSchoolInvoice';
                            invoiceLinks.put(invoiceNumber, link);
                        }
                    }
            }
            catch(Exception e){
                result = null;
                findInstalmentInvoice(invoiceNumber);
            }
        }
    }

    public void findInstalmentInvoice(string invoiceNumber){
        invoiceLinks = new Map<String, String>();
        try{
            resultInstallmentInvoice = [SELECT Id, Sent_Email_Receipt_By__r.Name, Due_Date__c, Sent_Email_Receipt__c, Amendment_Receive_From_School__c, Booking_Closed_By__r.name, Booking_Closed_On__c FROM client_course_instalment__c WHERE School_Invoice_Number__c =:invoiceNumber and isCommission_Amendment__c = true limit 1];
            PageReference pg;  
            if(resultInstallmentInvoice!= null){
                pg = Page.payment_school_invoice;
                link =  pg.getUrl() + '?type=payment&id=' + resultInstallmentInvoice.id;
                invoiceLinks.put(invoiceNumber, link); 
            }
        }
        catch(Exception e){
            resultInstallmentInvoice = null;
        }
    }

}