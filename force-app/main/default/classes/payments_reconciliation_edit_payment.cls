public with sharing class payments_reconciliation_edit_payment {

	private client_course_instalment__c instalment {get;set;}
	private Invoice__c invoice {get;set;}
	private client_product_service__c product {get;set;}
	private payment_plan__c payplan {get;set;}


	public client_course__c clientDeposit {get; set;}
	public list<client_course_instalment_payment__c> payments {get;set;}
	public String objCurrency {get;set;}
	public String clientName {get;set;}
	public decimal totalPaid {get;set;}
	public decimal totalPayment {get;set;}
	public boolean showError {get{if(showError==null) showError = false; return showError; }set;}


	private String contactId {get;set;}
	private decimal totalCoveredByAg {get;set;}
	private list<string> valuesToDelete {get{if(valuesToDelete==null) valuesToDelete = new list<string>(); return valuesToDelete;}set;}

	public client_course_instalment_payment__c receivedDetails {get;set;}

	private decimal agCurrencyRate {get;set;}
	//Constructor
	public payments_reconciliation_edit_payment() {
		try{
			payments = new list<client_course_instalment_payment__c>();
			totalPaid = 0;
			totalCoveredByAg = 0;

			String objId = ApexPages.currentpage().getparameters().get('id');

			// I N S T A L M E N T
			if(id.valueOf(objId).getSobjectType() == client_course_instalment__c.sObjectType){
				instalment = [SELECT Id, client_course__r.CurrencyIsoCode__c, client_course__c, client_course__r.Client__c, client_course__r.Client__r.Name, Number__c, Split_Number__c, Instalment_Value__c, Discount__c, Agency_Currency_Rate__c, Received_By_Agency__c, (SELECT Id, Value__c, Date_Paid__c, Payment_Type__c, Confirmed_Date__c, Paid_From_Deposit__c, Received_By__c, Received_On__c,	Received_By_Agency__c, Received_By_Agency__r.ParentId FROM client_course_instalment_payments__r) FROM client_course_instalment__c WHERE id = :objId limit 1];

				clientName = instalment.client_course__r.Client__r.Name;
				objCurrency = instalment.client_course__r.CurrencyIsoCode__c;


				receivedDetails = instalment.client_course_instalment_payments__r[0];
				for(client_course_instalment_payment__c ccip : instalment.client_course_instalment_payments__r){
					payments.add(ccip);
					totalPaid += ccip.Value__c;
					if(ccip.Payment_Type__c.toLowerCase()=='covered by agency')
						totalCoveredByAg += ccip.Value__c;
				}//end for payments

				totalPayment = instalment.Instalment_Value__c;

				contactId = instalment.client_course__r.Client__c;
				retrieveDeposit(contactId);

				agCurrencyRate = instalment.Agency_Currency_Rate__c;
			}

			// I N V O I C E
			else if(id.valueOf(objId).getSobjectType() == Invoice__c.sObjectType){
				invoice = [SELECT Id, Client__c, Client__r.Name, Total_Value__c, Agency_Currency_Rate__c, Received_By_Agency__c, (SELECT Id, Value__c, Date_Paid__c, Payment_Type__c, Confirmed_Date__c, Paid_From_Deposit__c, Received_By__c,	Received_On__c,	Received_By_Agency__c, Received_By_Agency__r.ParentId FROM client_course_instalment_payments__r) FROM Invoice__c WHERE id = :objId limit 1];

				clientName = invoice.Client__r.Name;
				objCurrency = '$';

				receivedDetails = invoice.client_course_instalment_payments__r[0];
				for(client_course_instalment_payment__c ccip : invoice.client_course_instalment_payments__r){
					payments.add(ccip);
					totalPaid += ccip.Value__c;
					// if(ccip.Payment_Type__c.toLowerCase()=='covered by agency')
					// 	totalCoveredByAg += ccip.Value__c;
				}//end for payments

				totalPayment = invoice.Total_Value__c;

				contactId = invoice.Client__c;
				retrieveDeposit(contactId);

				agCurrencyRate = invoice.Agency_Currency_Rate__c;
			}

			// P R O D U C T
			else if(id.valueOf(objId).getSobjectType() == client_product_service__c.sObjectType){
				product = [SELECT Id, Client__c, Client__r.Name, Price_Total__c,Currency__c, Agency_Currency_Rate__c, Received_By_Agency__c, (SELECT Price_Total__c FROM paid_products__r), (SELECT Id, Value__c, Date_Paid__c, Payment_Type__c, Confirmed_Date__c, Paid_From_Deposit__c, Received_By__c,	Received_On__c,	Received_By_Agency__c, Received_By_Agency__r.ParentId FROM client_course_instalment_payments__r) FROM client_product_service__c WHERE id = :objId limit 1];

				clientName = product.Client__r.Name;
				objCurrency = product.Currency__c;
				totalPayment = product.Price_Total__c;

				receivedDetails = product.client_course_instalment_payments__r[0];
				for(client_course_instalment_payment__c ccip : product.client_course_instalment_payments__r){
					payments.add(ccip);
					totalPaid += ccip.Value__c;
					
				}//end for payments

				for(client_product_service__c ccip : product.paid_products__r)
					totalPayment += ccip.Price_Total__c;


				contactId = product.Client__c;
				retrieveDeposit(contactId);

				agCurrencyRate = product.Agency_Currency_Rate__c;
			}
			// P A Y M E N T  P L A N
			else if(id.valueOf(objId).getSobjectType() == payment_plan__c.sObjectType){
				payplan = [SELECT Id, Client__c, Client__r.Name, value__c,Currency_Code__c, Currency_Rate__c, Received_By_Agency__c, (SELECT Id, Value__c, Date_Paid__c, Payment_Type__c, Confirmed_Date__c, Paid_From_Deposit__c, Received_By__c,	Received_On__c,	Received_By_Agency__c, Received_By_Agency__r.ParentId FROM client_course_instalment_payments__r) FROM payment_plan__c WHERE id = :objId limit 1];

				clientName = payplan.Client__r.Name;
				objCurrency = payplan.Currency_Code__c;
				totalPayment = payplan.value__c;

				receivedDetails = payplan.client_course_instalment_payments__r[0];
				for(client_course_instalment_payment__c ccip : payplan.client_course_instalment_payments__r){
					payments.add(ccip);
					totalPaid += ccip.Value__c;
					
				}//end for payments

				contactId = payplan.Client__c;
				retrieveDeposit(contactId);

				agCurrencyRate = payplan.Currency_Rate__c;
			}
		}
		catch(Exception e){
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage() + ' ' + e.getLineNumber()));
		}

		if(clientDeposit != null && clientDeposit.Deposit_Total_Value__c != null && (clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c) > 0)
			addPaymentsType.add(new SelectOption('Deposit','Deposit'));


		paymentsType.sort();
	}


	private void retrieveDeposit(string contactId){
		try{
		 	clientDeposit = [Select Deposit_Description__c, Deposit_Total_Used__c, Deposit_Total_Value__c, Deposit_Total_Available__c, Deposit_Type__c, Deposit_Value_Track__c, Id, client__c, Client__r.name, Deposit_Total_Refund__c,
		 						(SELECT Id, Value__c, Received_By__r.Name, Received_On__c, Received_By_Agency__r.Name, Payment_Type__c, Date_Paid__c FROM client_course_payments_deposit__r)
 							FROM client_course__c where client__c = :contactId and isDeposit__c = true and Deposit_Total_Available__c > 0];
		}catch(Exception e){
			clientDeposit = new client_course__c();
		}
	}

	//S A V E 		C H AN G E S
	public void saveChanges(){
		if(agCurrencyRate==null)
			agCurrencyRate = 1;
		Savepoint sp;
		try{
			sp = Database.setSavepoint();
			showError = false;

			//Double check sum of payments
			decimal dbCheck = 0;
			//decimal paidWDeposit = 0;
			decimal upCoveredByAg = 0;
			//Double check availability of client deposit
			retrieveDeposit(contactId);

			for(client_course_instalment_payment__c ccip : payments){
				if(ccip.Payment_Type__c.toLowerCase()=='deposit' && ccip.Paid_From_Deposit__c == null){
					//paidWDeposit += ccip.Value__c;
					ccip.Paid_From_Deposit__c = clientDeposit.id;
				}
				else if(ccip.Payment_Type__c.toLowerCase()=='covered by agency')
					upCoveredByAg += ccip.Value__c;

				dbCheck += ccip.Value__c;

				ccip.Received_By_Agency__c = selectedAgency;

				ccip.Agency_Currency_Value__c = ccip.Value__c * agCurrencyRate;

			}//end for


			if(dbCheck != totalPayment){
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'The sum of the payments must match the Payment Value.'));
				totalPaid = dbCheck;
			}
			else if(paidWDeposit > clientDeposit.Deposit_Total_Available__c){
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'The amount of Deposit cannot be greater than Deposit Available.'));
				totalPaid = dbCheck;
			}
			else{
				client_course__c updateCourse;
				if(paidWDeposit>0){
					clientDeposit.Deposit_Total_Used__c += paidWDeposit;
					clientDeposit.Deposit_Total_Available__c = clientDeposit.Deposit_Total_Value__c - clientDeposit.Deposit_Total_Used__c;

					if(instalment!=null){
						 	updateCourse = [SELECT Id, Total_Paid_Deposit__c, Total_Covered_by_Agency__c, Total_Balance__c FROM client_course__c WHERE id = :instalment.client_course__c limit 1 ];
					    updateCourse.Total_Paid_Deposit__c += paidWDeposit;
					}

					update clientDeposit;
				}


				if(instalment != null && upCoveredByAg != totalCoveredByAg){ //Update Covered by Agency
					if(updateCourse==null){

						//Double check instalment Balance
						client_course_instalment__c updatedInstalment = [Select Id, Balance__c, Covered_By_Agency__c, Received_By_Agency__c from client_course_instalment__c where id = :instalment.id];

						updatedInstalment.Balance__c += totalCoveredByAg;
						updatedInstalment.Balance__c -= upCoveredByAg;
						updatedInstalment.Covered_By_Agency__c = upCoveredByAg;

						updateCourse = [SELECT Id, Total_Covered_by_Agency__c, Total_Balance__c FROM client_course__c WHERE id = :instalment.client_course__c limit 1 ];

						updateCourse.Total_Covered_by_Agency__c -= totalCoveredByAg;
						updateCourse.Total_Covered_by_Agency__c += upCoveredByAg;

						updateCourse.Total_Balance__c += totalCoveredByAg;
						updateCourse.Total_Balance__c -= upCoveredByAg;

						update updatedInstalment;
					}
				}
				//System.debug('==>invoice '+invoice);
				//System.debug('==>selectedAgency '+selectedAgency);
				//System.debug('==>invoice.Received_By_Agency__c '+invoice.Received_By_Agency__c);
				if(instalment != null && selectedAgency != instalment.Received_By_Agency__c){
					client_course_instalment__c updateReceived = [Select Id, Received_By_Agency__c from client_course_instalment__c where id = :instalment.id];
					updateReceived.Received_By_Agency__c = selectedAgency;
					update updateReceived;
				} else if(invoice != null && selectedAgency != invoice.Received_By_Agency__c){
					Invoice__c updateReceivedInvoice = [SELECT Id,  Received_By_Agency__c, (SELECT Id, Received_By_Agency__c FROM client_products_services__r), (SELECT Id, Received_By_Agency__c FROM client_course_instalments__r), (SELECT Id, Received_By_Agency__c FROM client_course_instalment_payments__r) FROM Invoice__c WHERE id = :invoice.id limit 1];
					updateReceivedInvoice.Received_By_Agency__c = selectedAgency;

					for(client_course_instalment_payment__c iv : updateReceivedInvoice.client_course_instalment_payments__r)
						iv.Received_By_Agency__c = selectedAgency;
					for(client_course_instalment__c inst : updateReceivedInvoice.client_course_instalments__r)
						inst.Received_By_Agency__c = selectedAgency;
					for(client_product_service__c ps : updateReceivedInvoice.client_products_services__r)
						ps.Received_By_Agency__c = selectedAgency;


					update updateReceivedInvoice;
					update updateReceivedInvoice.client_course_instalments__r;
					update updateReceivedInvoice.client_course_instalment_payments__r;
					update updateReceivedInvoice.client_products_services__r;

				}else if(product != null && selectedAgency != product.Received_By_Agency__c){
					client_product_service__c updateReceivedProduct = [SELECT Id,  Received_By_Agency__c, (SELECT Id, Received_By_Agency__c FROM client_course_instalment_payments__r) FROM client_product_service__c WHERE id = :product.id limit 1];
					updateReceivedProduct.Received_By_Agency__c = selectedAgency;
					for(client_course_instalment_payment__c pd : updateReceivedProduct.client_course_instalment_payments__r)
						pd.Received_By_Agency__c = selectedAgency;
					update updateReceivedProduct;
					update updateReceivedProduct.client_course_instalment_payments__r;
				}

				else if(payplan != null && selectedAgency != payplan.Received_By_Agency__c){
					payment_plan__c updateReceivedPayPlan = [SELECT Id,  Received_By_Agency__c, (SELECT Id, Received_By_Agency__c FROM client_course_instalment_payments__r) FROM payment_plan__c WHERE id = :payplan.id limit 1];
					updateReceivedPayPlan.Received_By_Agency__c = selectedAgency;
					for(client_course_instalment_payment__c pd : updateReceivedPayPlan.client_course_instalment_payments__r)
						pd.Received_By_Agency__c = selectedAgency;
					update updateReceivedPayPlan;
					update updateReceivedPayPlan.client_course_instalment_payments__r;
				}

				if(updateCourse!=null)
					update updateCourse;

				upsert payments;

				if(valuesToDelete.size() > 0)
					Database.delete(valuesToDelete, true);

				showError = true;
			}
		}
		catch(Exception e){
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage() + ' ' + e.getLineNumber()));
			system.debug('Error===>' + e.getMessage());
			system.debug('Error Line===>' + e.getLineNumber());
			Database.rollback(sp);
		}
	}


	//A D D 		P A Y M E N T
	private decimal paidWDeposit {get{if(paidWDeposit == null) paidWDeposit = 0; return paidWDeposit;}set;}
	public void addPayment(){
		Savepoint sp;
		try{
			sp = Database.setSavepoint();

			if(newPayment.Date_Paid__c == null){
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select the payment date.'));
			}
			else if(newPayment.Value__c<=0 || newPayment.Value__c == null){
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Payment Value must be greater than 0 (zero).'));
			}
			else if(newPayment.Date_Paid__c> system.today()){
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Payment Date cannot be a future date.'));
			}
			else{
				totalPaid = 0;
				boolean sameType = false;

				for(client_course_instalment_payment__c ccip : payments){
					totalPaid += ccip.Value__c;
					if(newPayment.Payment_Type__c.toLowerCase()!='deposit' && ccip.Payment_Type__c == newPayment.Payment_Type__c && ccip.Date_Paid__c == newPayment.Date_Paid__c)
					 	sameType = true;
					else if(newPayment.Payment_Type__c.toLowerCase()=='deposit' && ccip.Payment_Type__c.toLowerCase() == 'deposit' && ccip.Date_Paid__c == newPayment.Date_Paid__c && ccip.Paid_From_Deposit__c == clientDeposit.id)
					 	sameType = true;
				}//end for

				if(newPayment.Value__c > (totalPayment - totalPaid)){
					ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Total payments cannot be greater than instalment value.'));
				}
				else{

					if(newPayment.Payment_Type__c.toLowerCase()=='deposit')
						paidWDeposit += newPayment.Value__c;


					totalPaid += newPayment.Value__c;

					if(sameType){
						for(client_course_instalment_payment__c ccip : payments){
							if(ccip.Payment_Type__c == newPayment.Payment_Type__c && ccip.Date_Paid__c == newPayment.Date_Paid__c){
								ccip.Value__c += newPayment.Value__c;
								break;
							}
						}//end for
					}
					else{

						newPayment.Received_By__c = receivedDetails.Received_By__c;
						newPayment.Received_On__c = receivedDetails.Received_On__c;
						// newPayment.Received_By_Agency__c = receivedDetails.Received_By_Agency__c;
						newPayment.Received_By_Agency__c = selectedAgency;
						newPayment.Client__c = contactId;

						if(instalment!=null)
							newPayment.client_course_instalment__c = instalment.id;
						else if(invoice!=null)
							newPayment.Invoice__c = invoice.id;
						else if(product!=null)
							newPayment.client_product_service__c = product.id;
						else if(payplan!=null)
							newPayment.payment_plan__c = payplan.id;


						payments.add(newPayment);
					}

					newPayment = new client_course_instalment_payment__c();
				}
			}
		}
		catch(Exception e){
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage() + ' ' + e.getLineNumber()));
			system.debug('Error===>' + e.getMessage());
			system.debug('Error Line===>' + e.getLineNumber());
			Database.rollback(sp);
		}
	}


	// R E M O V E 		P A Y M E N T
	public void deletePayment(){
		Savepoint sp;
		try{
			sp = Database.setSavepoint();

			String payType = ApexPages.currentPage().getParameters().get('type');
			String payDate = ApexPages.currentPage().getParameters().get('pDate');
			system.debug('payDate===>' + payDate);
			decimal payValue = decimal.valueOf(ApexPages.currentPage().getParameters().get('value'));


			for(integer i = 0; i < payments.size(); i++){
				system.debug('string.valueOf(payments[i].Date_Paid__c)===>' + payments[i].Date_Paid__c.format());
				if(payValue== payments[i].Value__c && payType == payments[i].Payment_Type__c && payments[i].Date_Paid__c.format() == payDate){
					if(payments[i].id!=null)
						valuesToDelete.add(payments[i].id);
					totalPaid -= payments[i].Value__c;
					if(payments[i].Payment_Type__c.toLowerCase()=='deposit')
					paidWDeposit -= payments[i].Value__c;
					payments.remove(i);
					break;
				}
			}//end for

		}
		catch(Exception e){
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getMessage() + ' ' + e.getLineNumber()));
			system.debug('Error===>' + e.getMessage());
			system.debug('Error Line===>' + e.getLineNumber());
			Database.rollback(sp);
		}
		finally{
			ApexPages.currentPage().getParameters().remove('type');
			ApexPages.currentPage().getParameters().remove('value');
		}
	}






	public client_course_instalment_payment__c newPayment {get{if
		(newPayment == null){
			newPayment = new client_course_instalment_payment__c();
		}
		return newPayment;}
		set;}

	public list<selectOption> paymentsType{
		get{
			if(paymentsType == null){
				paymentsType = new list<selectOption>();
				Schema.DescribeFieldResult fieldResult = client_course__c.Deposit_Type__c.getDescribe();
				List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
				for( Schema.PicklistEntry f : ple)
					if(f.getValue().toLowerCase()=='client scholarship' || f.getValue().toLowerCase()=='school credit' || f.getValue().toLowerCase()=='offshore')
						continue;
					else
						paymentsType.add(new SelectOption(f.getLabel(), f.getValue(), false));
			}
			return paymentsType;
		}set;}


		public list<selectOption> addPaymentsType{
			get{
				if(addPaymentsType == null){
					addPaymentsType = new list<selectOption>();
					Schema.DescribeFieldResult fieldResult = client_course__c.Deposit_Type__c.getDescribe();
					List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
					for( Schema.PicklistEntry f : ple)
						if(f.getValue().toLowerCase()=='client scholarship' || f.getValue().toLowerCase()=='school credit' || f.getValue().toLowerCase()=='offshore')
							continue;
						else
							addPaymentsType.add(new SelectOption(f.getLabel(), f.getValue(), false));
				}
				return addPaymentsType;
			}set;}

	private IPFunctions.agencyNoSharing agencyNoSharing {get{if(agencyNoSharing == null)agencyNoSharing = new IPFunctions.agencyNoSharing();return agencyNoSharing;}set;}
	public String selectedAgency {get{if(selectedAgency==null) selectedAgency = receivedDetails.Received_By_Agency__c; return selectedAgency;}set;}
	public List<SelectOption> agencyOptions {
		get{
			if(agencyOptions == null){
				system.debug('==>receivedDetails: '+receivedDetails);
				agencyOptions = agencyNoSharing.getAgenciesByParentNoSharing(receivedDetails.Received_By_Agency__r.ParentId);

			}
			return agencyOptions;
		}
		set;
	}

	private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}
	public User currentUser {get{
      if(currentUser==null) {
        currentUser = ff.currentUser;

      }
      return currentUser;
    }set;}


}