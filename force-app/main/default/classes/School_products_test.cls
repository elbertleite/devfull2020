/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class School_products_test {

    static testMethod void myUnitTest() {
        
        Map<String,String> recordTypes = new Map<String,String>();
       
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
        
		Account school = new Account();
		school.recordtypeid = recordTypes.get('School');
		school.name = 'Test School';
		school.BillingCountry = 'Australia';
		school.BillingCity = 'Sydney';		
		insert school;
		
		Account campus = new Account();
		campus.RecordTypeId = recordTypes.get('Campus');
		campus.Name = 'Test Campus CBD';
		campus.BillingCountry = 'Australia';
		campus.BillingCity = 'Sydney';
		campus.ParentId = school.id;
		insert campus;
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'CAE Exam';
		p2.Product_Type__c = 'Other';
		p2.Supplier__c = school.id;
		insert p2;
		
		Course__c course = new Course__c();
		course.Name = 'Certificate III in Business';        
		course.Type__c = 'Business';
		course.Sub_Type__c = 'Business';
		course.Course_Qualification__c = 'Certificate III';
		course.Course_Type__c = 'VET';
		course.Course_Unit_Type__c = 'Week';
		course.Only_Sold_in_Blocks__c = true;
		course.School__c = school.id;
		insert course;
       
		Campus_Course__c cc = new Campus_Course__c();
		cc.Course__c = course.Id;
		cc.Campus__c = campus.Id;        
		cc.Is_Available__c = true;
		cc.is_Selected__c = true;      
		insert cc;
		
		Course_Extra_Fee__c cef = new Course_Extra_Fee__c();
		cef.Campus_Course__c = cc.Id;
		cef.Nationality__c = 'Australia';
		cef.Availability__c = 3;
		cef.From__c = 1;
		cef.Value__c = 100;		
		cef.Product__c = p.id;
		insert cef;
		
		Deal__c d = new Deal__c();
		d.Campus_Course__c = cc.id;
		d.Product__c = p.id;		
		d.Availability__c = 3;
		d.From__c = 1;
		d.Total_Price__c = 100; 
		insert d;
		
		
		
		ApexPages.currentPage().getParameters().put('id', school.id);
		ApexPages.Standardcontroller controller = new Apexpages.Standardcontroller(school);
		School_products sp = new School_products(controller);
		
		List<School_products.Campus_Course> listFees = sp.listFees;
		
       	sp.newProductFee.Name__c = 'Enrolment Fee';
       	sp.newProductFee.Product_Type__c = 'Other';
       	sp.newProductFee.Unit_Type__c = 'Week';
       	sp.newProductFee.Allow_change_units__c = true;
       	sp.newProductFee.Optional__c = true;
       	sp.newProductFee.Description__c = 'enrolment fee description';
       	
       	sp.addProduct();
       	
       	
       	sp.newProductFee.Name__c = 'Homestay - Shared';
       	sp.newProductFee.Product_Type__c = 'Accommodation';
       	sp.newProductFee.Unit_Type__c = 'Week';       	
       	sp.newProductFee.Description__c = 'homestay shared description';
       	
       	sp.addProduct();
       	
        sp.getProductsAccommodation();
        sp.getProductsOther();
        
        Apexpages.currentPage().getParameters().put('selProdId', p.id);
        sp.editProduct();
        sp.cancelEdit();
        
        Apexpages.currentPage().getParameters().put('delProduct', p.id);
        sp.deleteProduct();
        
        Apexpages.currentPage().getParameters().put('delProduct', p2.id);
        sp.deleteProduct();
        
        sp.getAgeList();
        
        Apexpages.currentPage().getParameters().put('prodType', 'Other');
        Apexpages.currentPage().getParameters().put('prodId', p.id);
        Apexpages.currentPage().getParameters().put('schoolId', school.id);
        sp.resetFormParams();
    }
}