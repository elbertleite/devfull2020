public with sharing class client_course_new_install {
	
	private string ccId;
	public client_course__c course{get; set;}
	public client_course_instalment__c newInstallment{get{if(newInstallment == null) newInstallment = new client_course_instalment__c(); return newInstallment;} set;}
	
	
	public client_course_new_install(ApexPages.StandardController ctr){
		ccId = ctr.getId();
		loadCourse();
	}
	
	public void loadCourse(){
		course = [Select Campus_Name__c, Client__c, Client__r.Name, Course_Length__c, Course_Name__c, End_Date__c, Free_Units__c, Start_Date__c, Total_Course__c, Total_Extra_Fees__c, Total_Extra_Fees_Promotion__c, Total_Tuition__c, Unit_Type__c
					,Commission__c, Commission_Type__c, Total_Commissions__c, PFS_School__c, Pfs_First_Instalment_Only__c, Total_Balance__c, Commission_Tax_Rate__c, Number_Instalments__c, Total_School_Payment__c, 
					course_package__c, 	COE_Received__c, PFS_in_Bulk__c				
					, ( Select Due_Date__c, Received_Date__c, Extra_Fee_Discount_Value__c, Extra_Fee_Value__c, Number__c, Tuition_Value__c, Total_School_Payment__c, Instalment_Value__c, isExtraInstallment__c, 
						isCancelled__c, Commission__c, Commission_Value__c, Commission_Tax_Rate__c, Balance__c, Original_Due_Date__c, Original_Extra_Fee_Value__c, Original_Instalment_Value__c, Original_Tuition_Value__c, Split_Number__c, Splited_From_Number__c
						from client_course_instalments__r ORDER BY Number__c, due_date__c) 
					from client_course__c WHERE Id = :ccID];
	}
	
	public pageReference addInstalmentValue(){
		for(client_course_instalment__c cc:course.client_course_instalments__r){
			if(cc.Due_Date__c == newInstallment.Due_Date__c && !cc.isCancelled__c){
				newInstallment.Due_Date__c.addError('Date cannot be the same as instalment ' + cc.Number__c);
				return null;
			}
			
		}
		
		
		client_course_instalment__c ciPrev;
		client_course_instalment__c ciNext;
		dateTime lastReceivedDate;
		for(integer i= 0; i < course.client_course_instalments__r.size(); i++){
			if(i >= 0 && course.client_course_instalments__r[i].Due_Date__c <= newInstallment.Due_Date__c)
				ciPrev = course.client_course_instalments__r[i];
			if(i < course.client_course_instalments__r.size()  && course.client_course_instalments__r[i].Due_Date__c >= newInstallment.Due_Date__c && ciNext == null)
				ciNext = course.client_course_instalments__r[i];
			if(course.client_course_instalments__r[i].Received_Date__c != null)
				lastReceivedDate = course.client_course_instalments__r[i].Due_Date__c;	
		}
		if(ciNext != null && ciPrev != null && ciPrev.Splited_From_Number__c != null && ciNext.Splited_From_Number__c != null){
			newInstallment.Due_Date__c.addError('Date cannot be between split instalments.');
			return null;
		}
		
		if(lastReceivedDate != null && newInstallment.Due_Date__c < lastReceivedDate){
			newInstallment.Due_Date__c.addError('Date cannot be before the date of any paid instalment ');
			return null;
		}
		
		Savepoint sp = Database.setSavepoint();
		try{
			integer positionAdd;
			boolean hasUpdate = false;
			date currentPosition;
			decimal intallmentNumber;
			for(integer i = 0; i < course.client_course_instalments__r.size(); i++){
				if(newInstallment.Due_Date__c < course.client_course_instalments__r[i].Due_Date__c && (currentPosition == null || course.client_course_instalments__r[i].Due_Date__c < currentPosition)){
					positionAdd = i;
					intallmentNumber = course.client_course_instalments__r[i].Number__c;
					currentPosition = course.client_course_instalments__r[i].Due_Date__c;	
				}
			}
			
			if(course.client_course_instalments__r.size() == 0 || positionAdd == null){
				if(ciPrev != null)
					newInstallment.Number__c = ciPrev.number__c + 1;
				else newInstallment.Number__c = course.client_course_instalments__r.size() + 1;
				
				if(!course.COE_Received__c && course.course_package__c == null && newInstallment.Number__c==1)
					newInstallment.isFirstPayment__c = true; //first installment
			}else {
				newInstallment.Number__c = intallmentNumber;
				if(!course.COE_Received__c && course.course_package__c == null && intallmentNumber==1)
					newInstallment.isFirstPayment__c = true;
				
				//splitedInstalments.add(positionAdd, newSplitedInstalment);
				for(integer i = positionAdd; i < course.client_course_instalments__r.size(); i++){
					course.client_course_instalments__r[i].Number__c++;
					hasUpdate  = true;
					course.client_course_instalments__r[i].isFirstPayment__c = false;
				}
				
				if(!course.COE_Received__c)
					for(integer i = positionAdd; i < course.client_course_instalments__r.size(); i++)
						course.client_course_instalments__r[i].isFirstPayment__c = false;
					
			}
			
			
			
			//system.debug('newInst==>' + newInstallment);	
			newInstallment.isExtraInstallment__c = true;
			newInstallment.client_course__c = ccId;
			
			newInstallment.status__c = 'Open';
			if(course.PFS_School__c || (newInstallment.Number__c == 1 && course.Pfs_First_Instalment_Only__c))
				newInstallment.isPFS__c = true;
				
			newInstallment.Instalment_Value__c = newInstallment.Tuition_Value__c;
			
			
			system.debug('course.Commission__c==>' + course.Commission__c);
			system.debug('course.Commission_Tax_Rate__c==>' + course.Commission_Tax_Rate__c);
			newInstallment.Commission_Tax_Rate__c = course.Commission_Tax_Rate__c;
			if(course.Commission__c != null && course.Commission__c != 0){
				
				newInstallment.Commission__c = course.Commission__c;
				if(course.Commission_Type__c == 0)
					newInstallment.Commission_Value__c = newInstallment.Instalment_Value__c * course.Commission__c/100;
				else
					newInstallment.Commission_Value__c = course.Commission__c;
					
				course.Total_Commissions__c += newInstallment.Commission_Value__c;
				
				newInstallment.Commission_Tax_Value__c = newInstallment.Commission_Value__c * (course.Commission_Tax_Rate__c/100);
			}else{
				newInstallment.Commission_Tax_Value__c = 0;
				newInstallment.Commission_Value__c = 0;
				
			}
			
		
			system.debug('newInstallment.Commission_Tax_Value__c==>' + newInstallment.Commission_Tax_Value__c);
			system.debug('newInstallment.Commission_Value__c==>' + newInstallment.Commission_Value__c);
			
			if(course.PFS_School__c || course.Pfs_First_Instalment_Only__c)
				newInstallment.Total_School_Payment__c = newInstallment.Tuition_Value__c;
			else newInstallment.Total_School_Payment__c = newInstallment.Tuition_Value__c - newInstallment.Commission_Tax_Value__c - newInstallment.Commission_Value__c;
			
			newInstallment.Balance__c = newInstallment.Commission_Value__c + newInstallment.Commission_Tax_Value__c;
			
			
			newInstallment.Original_Due_Date__c = newInstallment.Due_Date__c;
			newInstallment.Original_Instalment_Value__c = newInstallment.Instalment_Value__c;
			newInstallment.Original_Tuition_Value__c = newInstallment.Tuition_Value__c;
			newInstallment.isPFS_Bulk__c = course.PFS_in_Bulk__c;
			
			insert newInstallment;
			
			//=============================ADD DOCUMENT===============================================================================
			//Client_Document__c schoolReceipt = new Client_Document__c(Client__c = course.Client__c, Document_Category__c = 'Finance', Document_Type__c = 'School Installment Receipt', client_course_instalment__c = newInstallment.id);
			//insert schoolReceipt;
			
			//newInstallment.Client_Document__c = schoolReceipt.id;
			//update newInstallment;
			
			
			if(hasUpdate)
				update course.client_course_instalments__r;
				
			course.Total_Tuition__c += newInstallment.Tuition_Value__c;
			course.Total_Course__c += newInstallment.Tuition_Value__c;
			
			course.Total_Balance__c += newInstallment.Balance__c;
			
			course.isAmendment__c = true;
			if(course.Number_Instalments__c!=null)
				course.Number_Instalments__c += 1;
			
			else
				course.Number_Instalments__c = 2;
			
			course.Total_School_Payment__c += newInstallment.Total_School_Payment__c;
			 
			update course;
				
			newInstallment = new client_course_instalment__c();
			
			loadCourse();
		}catch(Exception e){
			system.debug('Exception==>' + e);
			system.debug('Line==>' + e.getLineNumber());
			
			
			Database.rollback(sp);
		}
		return null;
	}
	
}