public class xCourseSearchCustomFee{

	private String websearchid;
	
	public xCourseSearchCustomFee(){
		websearchid = ApexPages.currentPage().getParameters().get('wsid');
		webSearch = [Select Id, Combine_Quotation__c, Currency_Rates__c, Total_Products__c,
									(Select Category__c, Currency__c, Description__c, isCustom__c, Name__c, Price__c, Quantity__c, Search_Course__c, Total__c, Unit_Description__c, Quotation_Products_Services__c 
									from Search_Course_Products__r),
									( Select id, Campus_Course__r.Campus__r.account_currency_iso_code__c, Custom_Currency__c, Selected__c, Campus_Course__r.Course__r.Name,  Campus_Course__r.Campus__r.Name, 
											Custom_Course__c, Custom_School__c, isCustomCourse__c, Custom_Fees__c 
									from Search_Courses__r WHERE Course_Deleted__c = false order by Course_Order__c NULLS LAST, CreatedDate)
							from Web_Search__c where Id = :websearchid];
	}
	
	public Web_Search__c webSearch {
		get{
			if(webSearch == null)
				webSearch = new Web_Search__c();
			return webSearch;
		}
		Set;
	}
	
	public List<String> selectedCourses {
		get{
			if(selectedCourses == null)
				selectedCourses = new List<String>();
			return selectedCourses;
		}
		Set;
	}
	
	public List<SelectOption> courses {
		get{
			if(courses == null){
				courses = new List<SelectOption>();
				for(Search_Courses__c sc : webSearch.Search_Courses__r){

					if(sc.isCustomCourse__c)
						courses.add(new SelectOption( sc.id, sc.Custom_Course__c + ' at ' + sc.Custom_School__c ));
					else
						courses.add(new SelectOption( sc.id, sc.Campus_Course__r.Course__r.Name + ' at ' + sc.Campus_Course__r.Campus__r.Name ));
											
				}
			}
			return courses;
		}
		Set;
	}
	
	
	public CustomFee newCustomFee {
		get{
			if(newCustomFee == null)
				newCustomFee = new CustomFee();
			return newCustomFee;
		}
		Set;
	}
	
	
	private Integer count = 0;
	
	public void addCustomFee(){
				
		newCustomFee.Id = count;
		count++;
		
		customFees.add(newCustomFee);
		
		newCustomFee = null;
		
	}
	
	public void deleteCustomFee(){
		Integer cfid = Integer.valueOf(ApexPages.currentPage().getParameters().get('cfid'));
		
		for(integer i = 0; i < customFees.size(); i++){
			CustomFee cf = customFees.get(i);
			if(cf.id == cfid)
				customFees.remove(i);
		}
	}
	
	public Boolean customFeesAdded {get;Set;}
	public void saveCustomFees(){
		
		customFeesAdded = false;
		
		String INNER_FEE_SEPARATOR = ':#';
		String FEE_SEPARATOR = ':&';
		
		if(checkCourseSelected()){
			List<Search_Courses__c> sCourses = [select Custom_Fees__c from Search_Courses__c WHERE Id in :selectedCourses];
			for(Search_Courses__c sc : sCourses){
				String courseCustomFees = sc.Custom_Fees__c != null ? sc.Custom_Fees__c : '';
				String fees ='';
				for(customFee cf : customFees){
					fees += cf.Name;
					fees += INNER_FEE_SEPARATOR + cf.value + INNER_FEE_SEPARATOR + cf.type + INNER_FEE_SEPARATOR + cf.applyTo + FEE_SEPARATOR;
				}
				
				if(courseCustomFees.endsWith(FEE_SEPARATOR))
					courseCustomFees += fees;
				else 
					courseCustomFees = courseCustomFees + FEE_SEPARATOR + fees;
					
				
				if(courseCustomFees.startsWith(FEE_SEPARATOR))
					courseCustomFees = courseCustomFees.replaceFirst(FEE_SEPARATOR, '');
				if(courseCustomFees.endsWith(FEE_SEPARATOR))
					courseCustomFees = courseCustomFees.substring(0, courseCustomFees.length()-2);
				
				sc.Custom_Fees__c = courseCustomFees;
				
			} //for
			
			
			update sCourses;
			customFeesAdded = true;
			
		} else 
			customFeesAdded = false;
		
	}
	
	public List<SelectOption> typeOptions {
		get{
			if(typeOptions == null){
				typeOptions = new List<SelectOption>();
				typeOptions.add(new SelectOption('add','Add'));
				typeOptions.add(new SelectOption('subtract','Subtract'));
			}
			return typeOptions;
		}
		Set;
	}
	
	public List<SelectOption> applyToOptions {
		get{
			if(applyToOptions == null){
				applyToOptions = new List<SelectOption>();
				applyToOptions.add(new SelectOption('extraFee','Extra Fees'));
				applyToOptions.add(new SelectOption('tuition','Tuition'));
			}
			return applyToOptions;
		}
		Set;
	}
	
	
	private boolean checkCourseSelected(){
		Boolean isCourseSelected = false;
		if(selectedCourses != null && !selectedCourses.isEmpty())		
			isCourseSelected = true;
		
		if(!isCourseSelected){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select the courses you want to add products.');
            ApexPages.addMessage(myMsg);
			
		}
		
		return isCourseSelected;
		
	}
	
	public List<customFee> customFees {
		get{
			if(customFees == null)
				customFees = new List<customFee>();
			return customFees;
		}
		Set;
	}
	
	public class customFee {
		public Integer Id {get;Set;}
		public String Name {Get;Set;}
		public String Type {Get;Set;}
		public String applyTo {get;set;}
		public Double value {get;Set;}
		
	}
	

}