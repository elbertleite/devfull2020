public with sharing class payment_plan_receipt {

    public Payment_Plan__c planReceipt{get; set;}

	public string language{get; set;}

	public payment_plan_receipt(ApexPages.StandardController ctr){
		generateReceipt();
	}
    
    public Account agencyDetails {get; set;}

	public void generateReceipt(){
		
		if(ApexPages.CurrentPage().getParameters().get('inst')!=null){
            planReceipt = [Select id, Payment_Type__c, instalment_Number__c, Plan_Type__c, client__r.Name, Client__r.Preferable_Language__c,  name, due_date__c, Value__c,
									(Select id, Received_By__r.Name, Payment_Type__c, Type_of_Payment__c, Received_On__c, Received_By_Agency__c, Value__c, Date_Paid__c, Agency_Currency_Value__c from client_course_instalment_payments__r)
									from Payment_Plan__c where Id = :ApexPages.CurrentPage().getParameters().get('inst') limit 1];
            language = planReceipt.Client__r.Preferable_Language__c != null ? planReceipt.Client__r.Preferable_Language__c : 'en_US';	
            agencyDetails = [Select Id, Name, Logo__c, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode From Account where Id = :planReceipt.client_course_instalment_payments__r[0].Received_By_Agency__c limit 1];
        }
    }
}