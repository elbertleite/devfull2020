public with sharing class Contacts{

	public Contacts(){
		try {
			user userAccount = [Select AccountId, ContactId from User where  id = :UserInfo.getUserId() limit 1];
			
			agency = [Select Id, Name from Account where Id = :userAccount.AccountId];	
		} catch( Exception e ){
			agency = new Account();
		}
		
	}
	
	public Account agency {get;Set;}

}