global with sharing class enrolment_process extends EnrolmentManager{


	public String bucket { get{ return EnrolmentManager.bucket; } private set; }

	public String STATUS_DRAFT { get{ return EnrolmentManager.STATUS_DRAFT; } private set; }
	public String STATUS_LOO_REQUESTED_TO_BO { get{ return EnrolmentManager.STATUS_LOO_REQUESTED_TO_BO; } private set; }
	public String STATUS_LOO_REQUESTED_TO_SCH { get{ return EnrolmentManager.STATUS_LOO_REQUESTED_TO_SCH; } private set; }
	public String STATUS_LOO_RECEIVED { get{ return EnrolmentManager.STATUS_LOO_RECEIVED; } private set; }
	public String STATUS_LOO_SENT_TO_CLI { get{ return EnrolmentManager.STATUS_LOO_SENT_TO_CLI; } private set; }
	public String STATUS_WAITING_CLI_PAYMENT { get{ return EnrolmentManager.STATUS_WAITING_CLI_PAYMENT; } private set; }
	public String STATUS_PAID_BY_CLIENT { get{ return EnrolmentManager.STATUS_PAID_BY_CLIENT; } private set; }
	public String STATUS_COE_REQUESTED_TO_BO { get{ return EnrolmentManager.STATUS_COE_REQUESTED_TO_BO; } private set; }
	public String STATUS_COE_REQUESTED_TO_SCH { get{ return EnrolmentManager.STATUS_COE_REQUESTED_TO_SCH; } private set; }
	public String STATUS_COE_RECEIVED { get{ return EnrolmentManager.STATUS_COE_RECEIVED; } private set; }
	public String STATUS_COE_SENT_TO_CLI { get{ return EnrolmentManager.STATUS_COE_SENT_TO_CLI; } private set; }
	public String STATUS_MISSING_DOCUMENTS { get{ return EnrolmentManager.STATUS_MISSING_DOCUMENTS; } private set; }
	public String STATUS_WAITING_VISA { get{ return EnrolmentManager.STATUS_WAITING_VISA; } private set; }
	public String STATUS_ENROLLED { get{ return EnrolmentManager.STATUS_ENROLLED; } private set; }
	public String STATUS_WAITING_CLI_PAYMENT2 { get{ return EnrolmentManager.STATUS_WAITING_CLI_PAYMENT2; } private set; }

	public String STYLE_MISSING_DOCUMENTS { get{ return EnrolmentManager.STATUS_MISSING_DOCUMENTS.replace(' ', '_'); } private set; }
	public String STYLE_COMPLETED { get{ return EnrolmentManager.STATUS_ENROLLED.replace(' ', '_'); } private set; }

	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}

	public Double offset { get{ if(offset == null) offset = EnrolmentManager.getUserTZOffset(); return offset;} set; }
	public boolean isDeletable { get{ return Schema.sObjectType.client_course__c.isDeletable(); } set; }
	public String updateVisaID {get;set;}

	public Contact contact {get;set;}
	public client_course__c booking {get;set;}
	public Map<String, List<client_course_fees__c>> schoolProducts {get;set;}
	public Map<String, List<File>> files {get;set;}
	public Map<String, Integer> totalFilesByCourse {get;set;}
	public Map<String, Map<String, List<File>>> campusFiles {get;set;}
	public Map<String, Integer> totalFilesByCampus {get;set;}
	private String bookingID;
	private String contactID;
	public String noteComments {get;set;}
	public List<RequestWrapper> pendingRequests {get;set;}
	Map<String, RequestWrapper> packageRequests = new Map<String, RequestWrapper>();

	Contact currentUser;

	public boolean isBookingInProgress {get{if(isBookingInProgress == null) isBookingInProgress = false; return isBookingInProgress;}set;}

    
	@RemoteAction
	global static String saveOnHold(String courseId, String isHold){
		
		Boolean onHold = Boolean.valueOf(isHold);
		list<client_course__c> allCourses = new list<client_course__c>();

		for(client_course__c c : [SELECT Id FROM client_course__c WHERE Id = :courseId Or course_package__c = :courseId ]){
			c.isEnrolment_on_hold__c = onHold;
			allCourses.add(c);
		}//end for
		
		update allCourses;

		if(onHold)
			return 'Enrolment saved as On Hold';
		else
			return 'Enrolment saved as In Progress';
	}

	@RemoteAction
	global static String saveAgency(String courseId, String agencyId){
		
		list<client_course__c> allCourses = new list<client_course__c>();

		for(client_course__c c : [SELECT Id FROM client_course__c WHERE Id = :courseId Or course_package__c = :courseId ]){
			c.Enrolment_Agency__c = agencyId;
			allCourses.add(c);
		}//end for
		
		update allCourses;

		client_course__c upCourse = [SELECT Enrolment_Agency__r.Name FROM client_course__c WHERE Id = :courseId limit 1];

		return 'Saved as enrolment through ' + upCourse.Enrolment_Agency__r.Name;
	}

	public list<SelectOption> agencies {get;set;}
	private void findAgencies(){
		agencies = new list<SelectOption>();
		String agGroup;
		for(Account ac : [SELECT Id, Name, ParentId, Parent.Name FROM Account WHERE RecordType.Name = 'agency' order by Parent.Name, Name]){
			if(ac.Parent.Name != agGroup){
				agencies.add(new SelectOption(ac.ParentId, ac.Parent.Name, true));
				agGroup = ac.Parent.Name;
			}

			agencies.add(new SelectOption(ac.Id, ac.Name));
		}//end for
	}
	
	
	public enrolment_process(){
		findAgencies();

		currentUser = UserDetails.getMyContactDetails();
    	contactID = ApexPages.currentPage().getParameters().get('id');
    	bookingID = ApexPages.currentPage().getParameters().get('bk');
    	if(contactID != null)
    		getContactInfo();
    	if(bookingID != null)
    		getBookingInfo();

    }

   	private void getContactInfo(){
   		contact = [select id, name, RecordType.Name from contact where id = :contactID];
   	}


   	private void getBookingInfo(){
		isBookingInProgress = false;
		files = new Map<String, List<File>>();
		totalFilesByCampus = new Map<String, Integer>();
		totalFilesByCourse = new Map<String, Integer>();
		Set<String> campusids = new Set<String>();
		pendingRequests = new List<RequestWrapper>();
		List<Client_Course__c> packages = new List<Client_Course__c>();


		booking = [select id, Name from client_course__c where id = :bookingID];

		String sql = EnrolmentManager.ENROLMENT_SQL_FIELDS;
		String sqlFrom = ' WHERE Booking_Number__c = \'' + bookingID + '\'';
		sqlFrom += ' AND ' + EnrolmentManager.ENROLMENT_SQLWHERE;

		IPFunctions.SearchNoSharing sns = new IPFunctions.SearchNoSharing();
		List<Id> mainCourses = new list<Id>();
		for(Client_Course__c course : sns.NSClientCourse(sql + sqlFrom)){
			if(course.Course_Package__c == null){//Main course
				mainCourses.add(course.id);
				RequestWrapper rw = new RequestWrapper();
				rw.course = course;
				rw.contact = course.client__r;
				rw.courseNotes = course.Custom_Notes_Tasks__r;
				rw.activities = buildEnrolmentHistory(course.Enrolment_Activities__c);
				packageRequests.put(course.id, rw);
				pendingRequests.add(rw);
			} else	//Belongs to a package
				packages.add(course);


			if(course.Enrolment_Status__c != STATUS_COE_SENT_TO_CLI)
				isBookingInProgress = true;
		}

		for(Client_Course__c pkg : packages){
			if(packageRequests.containsKey(pkg.Course_Package__c))
				packageRequests.get(pkg.Course_Package__c).packageCourses.add(pkg);
		}


		Map<String, Client_Document__c> visas = getClientVisas(mainCourses);

		//COURSE FILES
		for(RequestWrapper rw : pendingRequests){
			if(visas.containsKey(rw.course.id))
				rw.visa = visas.get(rw.course.id);

			files.put(rw.course.id, new List<File>());
			totalFilesByCampus.put(rw.course.Campus_id__c, 0);
			totalFilesByCourse.put(rw.course.id, 0);

			//ADD ENROLMENT FORM DOCS
			if(rw.course.Enrolment_Form__r.Preview_link__c != null && rw.course.Enrolment_Form__r.Preview_link__c.trim() != ''){
				for(String str : rw.course.Enrolment_Form__r.Preview_link__c.split(FileUpload.SEPARATOR_FILE)){
					String[] filestr = str.split(FileUpload.SEPARATOR_URL);
					files.get(rw.course.id).add(new File(filestr[1], filestr[0]));
					totalFilesByCourse.put(rw.course.id, totalFilesByCourse.get(rw.course.id)+1);
				}
			}

			//ADD LOO DOCS
			if(rw.course.LOO__r.Preview_Link__c != null && rw.course.LOO__r.Preview_Link__c.trim() != ''){
				for(String str : rw.course.LOO__r.Preview_link__c.split(FileUpload.SEPARATOR_FILE)){
					String[] filestr = str.split(FileUpload.SEPARATOR_URL);
					files.get(rw.course.id).add(new File(filestr[1], filestr[0]));
					totalFilesByCourse.put(rw.course.id, totalFilesByCourse.get(rw.course.id)+1);
				}
			}
			//ADD COE DOCS
			for(Client_Document__c cd : rw.course.Client_Documents__r){
				if(cd.Preview_Link__c != null && cd.Preview_Link__c.trim() != ''){
					for(String str : cd.Preview_link__c.split(FileUpload.SEPARATOR_FILE)){
						String[] filestr = str.split(FileUpload.SEPARATOR_URL);
						files.get(cd.Client_Course__c).add(new File(filestr[1], filestr[0]));
						totalFilesByCourse.put(cd.Client_Course__c, totalFilesByCourse.get(cd.Client_Course__c)+1);
					}
				}
			}//end for

			for(Client_Course__c cc : rw.packageCourses){
				for(Client_Document__c cd : cc.Client_Documents__r){
					if(cd.Preview_Link__c != null && cd.Preview_Link__c.trim() != ''){
						for(String str : cd.Preview_link__c.split(FileUpload.SEPARATOR_FILE)){
							String[] filestr = str.split(FileUpload.SEPARATOR_URL);
							files.get(cd.Client_Course__r.course_package__c).add(new File(filestr[1], filestr[0]));
							totalFilesByCourse.put(cd.Client_Course__r.course_package__c, totalFilesByCourse.get(cd.Client_Course__r.course_package__c)+1);
						}
					}
				}//end for
			}//end for pkg
		}


		// for(Client_Document__c cd : [Select Id, Client_Course__C, Preview_Link__c FROM Client_Document__c WHERE Document_Type__c = 'COE' and Document_Category__c = 'Enrolment' AND Client_Course__c in :packageRequests.keySet()]){
		// 	if(cd.Preview_Link__c != null && cd.Preview_Link__c.trim() != ''){
		// 		for(String str : cd.Preview_link__c.split(FileUpload.SEPARATOR_FILE)){
		// 			String[] filestr = str.split(FileUpload.SEPARATOR_URL);
		// 			files.get(cd.Client_Course__c).add(new File(filestr[1], filestr[0]));
		// 			totalFilesByCourse.put(cd.Client_Course__c, totalFilesByCourse.get(cd.Client_Course__c)+1);
		// 		}
		// 	}
		// 	if(packageRequests.containsKey(cd.Client_Course__c))
		// 		packageRequests.get(cd.Client_Course__c).CoeDocId = cd.id;
		// }
		// END COURSE FILES

		getCampusDocuments();
		updateVisaID = '';

   	}

	private Map<String, Client_Document__c> getClientVisas(List<Id> coursesId){
		Map<String, Client_Document__c> clientVisas = new Map<String, Client_Document__c>();
		for(Client_Document__c visa : [Select Client__c, Client_Course__c, Visa_Country_applying_for__c, Expiry_Date__c from Client_Document__c where Document_Category__c = 'Visa' and Document_Type__c = 'Visa' and client_course__c in :coursesId]){
			clientVisas.put(visa.Client_Course__c, visa);
		}
		return clientVisas;
	}

	public void refresh(){
		getBookingInfo();
	}

	private void getCampusDocuments(){

		campusFiles = new Map<String, Map<String, List<File>>>();
		Map<String, String> folders = new Map<String, String>();
		EnrolmentManager.WithoutSharing emws = new EnrolmentManager.WithoutSharing();
		List<account_document_file__c> listFiles = emws.getCampusDocuments(totalFilesByCampus.keySet());

		for(account_document_file__c adf : listFiles)
			if(adf.Content_Type__c == 'Folder')
				folders.put(adf.id, adf.File_Name__c);

		for(account_document_file__c adf : listFiles){
			if(adf.Content_Type__c != 'Folder'){
				String category = folders.get(adf.Parent_Folder_Id__c) != null ? folders.get(adf.Parent_Folder_Id__c) : 'Not Classified';
				String filename = adf.Description__c != null ? adf.Description__c : adf.File_Name__c;

				if(campusFiles.containsKey(adf.Parent__c)){
					if(campusFiles.get(adf.Parent__c).containsKey(category)){
						campusFiles.get(adf.Parent__c).get(category).add( new File(filename, adf.Preview_Link__c) );
					} else
						campusFiles.get(adf.Parent__c).put( category, new List<File>{new File(filename, adf.Preview_Link__c)}  );
				} else
					campusFiles.put(adf.Parent__c, new Map<String, List<File>>{ category => new List<File>{new File(filename, adf.Preview_Link__c)} });

				totalFilesByCampus.put(adf.Parent__c, totalFilesByCampus.get(adf.Parent__c)+1);


			}

		}

 		system.debug('@@@ campusFiles : ' + campusFiles);
	}


	public List<SelectOption> countries {
        get {
            if(countries == null){
                countries = IPFunctions.getAllCountries();
                if(countries.size() > 0)
                    countries.add(0, new SelectOption('','--None--'));
            }
            return countries;
        }
        Set;
    }


	public void sendEmail(){
		List<String> selectedFiles = new List<String>();
		system.debug('@ files L: ' + files);
		for(String key : files.keySet())
			for(File f : files.get(key) )
				if(f.selected)
					selectedFiles.add(f.name + '#>#' + f.url);

		for(String key : campusFiles.keySet())
			for(String k2 : campusFiles.get(key).keySet())
				for(File f : campusFiles.get(key).get(k2))
					if(f.selected)
						selectedFiles.add(f.name + '#>#' + f.url);

		system.debug('@#$ selectedFiles: ' + selectedFiles);

		if(!selectedFiles.isEmpty()){
			String docs = String.join(selectedFiles, '#;#');
			system.debug('@#$ docs: ' + docs);

			booking.Email_Documents__c = docs;
	 		update booking;
		} else {
			booking.Email_Documents__c = null;
	 		update booking;
		}
	}

	public PageReference addNewCourse(){

		ApexPages.StandardController controller = new ApexPages.StandardController(contact);
		client_course_new ccn = new client_course_new(controller);
		ccn.setAddNewCourse();

		if(!redirectNewContactPage){
			PageReference pr = Page.client_course_new;
			pr.setRedirect(false);
			pr.getParameters().put('id', contact.id);
			return pr;
		}else{
			PageReference pr = Page.contact_new_bookings;
			pr.setRedirect(false);
			pr.getParameters().put('id', contact.id);
			pr.getParameters().put('page', 'contactNewPage');
			return pr;
		}
	}

	// Create Note
	public void createCourseNote(){
		String clientId = ApexPages.currentPage().getParameters().get('clientId');
		String courseid = ApexPages.currentPage().getParameters().get('courseid');
		String noteSubj = ApexPages.currentPage().getParameters().get('noteSubj');
		String noteDesc = ApexPages.currentPage().getParameters().get('noteDesc');
		String firstInst = ApexPages.currentPage().getParameters().get('firstInst');
		
		Savepoint sp = Database.setSavepoint();
		try {
			IPFunctions.createCourseNote(clientId, courseid, noteSubj, noteDesc, firstInst);
			refresh();
		}catch(Exception e){
			ApexPages.addMessages(e);
			Database.rollback(sp);
		}
		finally{
			ApexPages.currentPage().getParameters().remove('clientId');
			ApexPages.currentPage().getParameters().remove('courseid');
			ApexPages.currentPage().getParameters().remove('noteSubj');
			ApexPages.currentPage().getParameters().remove('noteDesc');
			ApexPages.currentPage().getParameters().remove('firstInst');
		}
		
	}

	public void requestLOO(){

		String requestCourseID = ApexPages.currentPage().getParameters().get('courseid');
		String noteCommentsParam = ApexPages.currentPage().getParameters().get('noteComments');
		String firstInst = ApexPages.currentPage().getParameters().get('firstInst');
		boolean isOnlineApplication = ApexPages.currentPage().getParameters().get('isOnlineApplication') != null ? boolean.valueof(ApexPages.currentPage().getParameters().get('isOnlineApplication')) : false;
		boolean isUrgent = ApexPages.currentPage().getParameters().get('isUrgent') != null ? boolean.valueof(ApexPages.currentPage().getParameters().get('isUrgent')) : false;

		Savepoint sp = Database.setSavepoint();
		try {
			doRequestLOO(requestCourseID, noteCommentsParam, isOnlineApplication, isUrgent, packageRequests, currentUser, firstInst);
			refresh();
		}catch(Exception e){
			ApexPages.addMessages(e);
			Database.rollback(sp);
		}
	}

	public void requestCOE(){

		String courseId = ApexPages.currentPage().getParameters().get('courseid');
		String firstInst = ApexPages.currentPage().getParameters().get('firstInst');
		String noteComments = ApexPages.currentPage().getParameters().get('noteComments');

		Boolean isUrgent = ApexPages.currentPage().getParameters().get('isUrgent') != null ? boolean.valueof(ApexPages.currentPage().getParameters().get('isUrgent')) : false;

		Savepoint sp = Database.setSavepoint();
		try {
			doRequestCOE(courseId, firstInst, noteComments, isUrgent, currentUser);
			refresh();
		}catch(Exception e){
			ApexPages.addMessages(e);
			Database.rollback(sp);
		}
	}

	public void advSignLOO(){
		String requestCourseID = ApexPages.currentPage().getParameters().get('courseid');
		Savepoint sp = Database.setSavepoint();
		try {
			doAdvSignLOO(requestCourseID, packageRequests, currentUser);
			refresh();
		}catch(Exception e){
			ApexPages.addMessages(e);
			Database.rollback(sp);
		}
	}

	public void saveVisa(){
		String requestCourseID = ApexPages.currentPage().getParameters().get('courseid');
		Savepoint sp = Database.setSavepoint();
		try {
			doSaveVisa(requestCourseID, packageRequests);
			refresh();
		}catch(Exception e){
			ApexPages.addMessages(e);
			Database.rollback(sp);
		}
	}

	public PageReference confirmVisaEnrolment(){
		String requestCourseID = ApexPages.currentPage().getParameters().get('courseid');
		Boolean isRedirect = Boolean.valueOf(ApexPages.currentPage().getParameters().get('courseRedirect'));
		Savepoint sp = Database.setSavepoint();
		try {
			list<client_course__c> upCourses = new list<client_course__c>();

			for(client_course__c cc : [SELECT Id FROM client_course__c WHERE Id = :requestCourseID OR course_package__c = :requestCourseID])
				upCourses.add(new client_course__c(Id = cc.Id, Visa_Confirmed__c = true));

			update upCourses;
			if(isRedirect){
				PageReference reference;
				if(!redirectNewContactPage){
					reference = new PageReference('/client_course_new?id='+contact.id+'&ccId='+requestCourseID);
				}else{
					reference = Page.contact_new_bookings;
					reference.getParameters().put('id', contact.id);
					reference.getParameters().put('ccId', requestCourseID);
					reference.getParameters().put('page', 'contactNewPage');
				}
				reference.setRedirect(true);
				return reference;
			}
			else{
				refresh();
				return null;
			} 

		}catch(Exception e){
			ApexPages.addMessages(e);
			Database.rollback(sp);
			return null;
		}
	}

	public PageReference deleteCourse(){

		String courseid = ApexPages.currentPage().getParameters().get('courseid');
		if(isDeletable){
	    	Savepoint sp = Database.setSavepoint();
	    	try{
	    		doDeleteEnrolment(courseid);
				PageReference reference;
				if(!redirectNewContactPage){
					reference = new PageReference('/client_course_new?id='+contact.id);
				}else{
					reference = Page.contact_new_bookings;
					reference.getParameters().put('id', contact.id);
					reference.getParameters().put('page', 'contactNewPage');
				}
				reference.setRedirect(true);
				return reference;
	      	}catch(Exception e){
	        	ApexPages.addMessages(e);
	           	Database.rollback(sp);
	        }
		}
		return null;
    }

	public void moveToWaitingVisa(){
		String requestCourseID = ApexPages.currentPage().getParameters().get('courseid');

		Savepoint sp = Database.setSavepoint();
		try {
			doMoveToWaitingVisa(requestCourseID, packageRequests, currentUser);
			refresh();
		}catch(Exception e){
			ApexPages.addMessages(e);
			Database.rollback(sp);
		}
	}

	public void enrol(){

		String requestCourseID = ApexPages.currentPage().getParameters().get('courseid');

		Savepoint sp = Database.setSavepoint();
		try {
			doEnrol(requestCourseID, packageRequests, currentUser);
			refresh();
		}catch(Exception e){
			ApexPages.addMessages(e);
			Database.rollback(sp);
		}

	}

	public void updateVisa(){
		updateVisaID = ApexPages.currentPage().getParameters().get('updateVisaID');
	}





}