/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class client_course_deposit_test {

    static testMethod void myUnitTest() {
       
       TestFactory tf = new TestFactory();
       
       Account school = tf.createSchool();
       Account agency = tf.createAgency();
       Contact emp = tf.createEmployee(agency);
       User portalUser = tf.createPortalUser(emp);
       Account campus = tf.createCampus(school, agency);
       Course__c course = tf.createCourse();
       Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
       
       
       
       Test.startTest();
       system.runAs(portalUser){
       	   Contact client = tf.createLead(agency, emp);
	       client_course__c booking = tf.createBooking(client);
	       client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
	       
	       client_course_deposit testClass = new client_course_deposit(new ApexPages.StandardController(client));
	       
	       String clientName = testClass.clientName;
	       testClass.getdepositListType();
	       Account agencyDetails = testClass.agencyDetails;
	       
	       testClass.cancelDeposit();
	       
	       testClass.newDepositValue.Value__c=50;
	       testClass.newDepositValue.Date_Paid__c=system.today();
	       testClass.newDepositValue.Payment_Type__c = 'Cash';
	       testClass.addDepositValue();
	       
	       testClass.newDepositValue.Value__c=150;
	       testClass.newDepositValue.Date_Paid__c=system.today();
	       testClass.newDepositValue.Payment_Type__c = 'Money Transfer';
	       testClass.addDepositValue();
	       
	       ApexPages.currentPage().getParameters().put('type', 'Cash');
	       ApexPages.currentPage().getParameters().put('value', '50');
	       ApexPages.currentPage().getParameters().put('by', userInfo.getUserId());
	       testClass.removeDepositValue();
	       
	       testClass.saveDeposit();
	       
	       ApexPages.currentPage().getParameters().remove('type');
	       ApexPages.currentPage().getParameters().remove('value');
	       ApexPages.currentPage().getParameters().remove('by');
	       
	       
	       
	       instalments[0].Received_By_Agency__c = agency.id;
		   instalments[0].Received_Date__c = system.today();
		   instalments[0].Paid_To_School_On__c = system.now();
		   update instalments[0];
		   
	       
	       client_course_instalment_amendment testAmend = new client_course_instalment_amendment(new ApexPages.StandardController(instalments[0]));
	       testAmend.amendment.Tuition_Value__c = 2000;
	       testAmend.saveAmendment();
	       
	       String amendId= testAmend.amendment.id;
	       
	       system.debug('testAmend.amendment===>'+ testAmend.amendment);
	       
	       ApexPages.currentPage().getParameters().put('rf', amendId);
	       testClass = new client_course_deposit(new ApexPages.StandardController(client));
		   testClass.overpaymenToDeposit();
		   	       
	       Test.stopTest();
       }
    }
}