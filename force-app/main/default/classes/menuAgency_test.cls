/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class menuAgency_test {

    static testMethod void myUnitTest() {

    	TestFactory factory = new TestFactory();

		Map<String,String> recordTypes = new Map<String,String>();
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);

	    Account school = factory.createSchool();
  		Account agency = factory.createAgency();

  		Account agency2 = new Account();
  		agency2.Name = 'Second agency 2';
  		agency2.recordtypeid = recordTypes.get('Agency');
  		agency2.ParentId = agency.ParentId;
  		insert agency2;

		Account campus = factory.createCampus(school, agency);
		Contact employee = factory.createEmployee(campus);

		User userEmp = factory.createPortalUser(employee);


    	//Account agency = tf.createAgency();
		System.RunAs(userEmp){
			menuAgency ma = new menuAgency();
			ma.accoID = agency.id;
      String currTab = ma.currTab;
      String agencyID = ma.agencyID;

      agency.Logo__c = 'test';
      ma = new menuAgency();
      ma.accoID = agency.id;


			ma.getfileURL();
		}

    }
}