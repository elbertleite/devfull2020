@isTest
private class ClientCourseInstalmentsController_test {

    static testMethod void myUnitTest() {
        
		TestFactory tf = new TestFactory();
       
       	Account school = tf.createSchool();
       	Account agency = tf.createAgency();
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
       	Account campus = tf.createCampus(school, agency);
       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
        Department__c department = tf.createDepartment(agency);
       
       	Test.startTest();
       	system.runAs(portalUser){
			Contact client = tf.createLead(agency, emp);
			client_course__c booking = tf.createBooking(client);
	       	client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       	client_course__c cc2 =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       	client_product_service__c product = tf.createCourseProduct(booking, agency);
	       	
	       	
	       	
	       	Invoice__c invoice = tf.createInvoice(client, cc, booking, agency);
	       	Invoice__c cancelledInvoice = tf.createInvoiceCancelled(client, cc2, booking, agency);
	       	
	       	
			List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
			
			ApexPages.currentPage().getParameters().put('id', client.id);
			ClientCourseInstalmentsController testClass = new ClientCourseInstalmentsController();
			
			cc.Enrolment_Date__c = System.today();
			update cc;
			
			testClass.selectedAgency = agency.id;
			testClass.setupPending();
			testClass.setupRefund();
			testClass.setupCredit();
			testClass.setupProducts();
			testClass.addItemInvoice();
			testClass.clearListInvoice();
			testClass.clearInvoiceCart();
			testClass.saveDefaultPhoneNumber();
			
			
			testClass.contactInvoices();
			//testClass.searchResult();
			
			//testClass.findProducts();
			
			ApexPages.currentPage().getParameters().put('iId', instalments[0].id);
			testClass.PDSPayment();
			
			/*ApexPages.currentPage().getParameters().remove('id');
			ClientCourseInstalmentsController testClass2 = new ClientCourseInstalmentsController();
			
			
			//testClass2.getDepartments();
			testClass2.getStatus();
			testClass2.getAgencies();
			List<SelectOption> campuses = testClass2.campusFilter;
			List<SelectOption> nationality = testClass2.nationalityFilter;
			testClass2.changeAgency();
			testClass2.changeSchool();
			List<SelectOption> notpaidProd = testClass2.optionsNotPaidProd;
			List<SelectOption> paidProd = testClass2.optionsPaidProd;
			List<SelectOption> instNumber = testClass2.filterInstNumber;
			
			
			testClass2.contactInvoices();*/
			//testClass2.findPendingInstalments();
		
			
		}
       	Test.stopTest();
    }
}