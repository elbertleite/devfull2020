public without sharing class update_package_courses {

    public String selectedCountry {get;set;}
    public String selectedCity {get;set;}
    public String selectedSchool {get;set;}
    public String selectedCampus {get;set;}
	public String campusName {get;set;}

    public Map<String, Map<String, Map<String, Map<String, String>>>> filters {get;set;}
    public Map<String, String> schoolNames {get;set;}

    public List<Campus_Course__c> originalPackages {get;set;}
    public List<Campus_Course__c> packages {get;set;}
    public List<SelectOption> courses {get;set;}

	public List<SelectOption> optCountries {get;set;}
	public List<SelectOption> optCities {get;set;}
	public List<SelectOption> optSchools {get;set;}
	public List<SelectOption> optCampuses {get;set;}


    public void loadOptions(){

        selectedCountry = getParam('country');
        selectedCity = getParam('city');
        selectedSchool = getParam('school');
        selectedCampus = getParam('campus');

        optCountries = new List<SelectOption>();
        optCities = new List<SelectOption>();
        optSchools = new List<SelectOption>();
        optCampuses = new List<SelectOption>();

        filters = new Map<String, Map<String, Map<String, Map<String, String>>>>{ '' => new Map<String, Map<String, Map<String, String>>>{ '' => new Map<String, Map<String, String>>{ '' => new Map<String, String>() } } };
        schoolNames = new Map<String, String>();
        optCountries.add(new SelectOption('', ' - Choose a Country - '));

        for(AggregateResult ar : [select Campus__r.BillingCountry, Campus__r.BillingCity, Campus__r.Parentid schoolid,  Campus__r.Parentid, Campus__r.Parent.Name schoolName, Campus__c campusid, Campus__r.Name campusName
                                    from Campus_Course__c where Course__r.Package__c = true and Campus__r.BillingCountry != null and Campus__r.BillingCity != null
                                group by Campus__r.BillingCountry, Campus__r.BillingCity, Campus__r.Parentid, Campus__r.Parent.Name, Campus__c, Campus__r.Name
                                order by Campus__r.BillingCountry, Campus__r.BillingCity, Campus__r.Parent.Name, Campus__r.Name]){

            String country = (String) ar.get('BillingCountry');
            String city = (String) ar.get('BillingCity');
            String schoolName = (String) ar.get('schoolName');
            String schoolid = (String) ar.get('schoolid');
            String campusid = (String) ar.get('campusid');
            String campusName = (String) ar.get('campusName');

            schoolNames.put(schoolid, schoolName);

            if(filters.containsKey(country)){
                if(filters.get(country).containsKey(city)){
                    if(filters.get(country).get(city).containsKey(schoolid)){
                        filters.get(country).get(city).get(schoolid).put(campusid, campusName);
                    } else {
                        filters.get(country).get(city).put(schoolid, new Map<String, String>{ campusid => campusName } );
                    }
                } else {
                    filters.get(country).put(city, new Map<String, Map<String, String>>{ schoolid => new Map<String, String>{ campusid => campusName } } );
                }
            } else {
                filters.put( country, new Map<String, Map<String, Map<String, String>>>{ city => new Map<String, Map<String, String>>{ schoolid => new Map<String, String>{  campusid => campusName } } } );
                optCountries.add(new SelectOption(country, country));
            }

        }

        if(selectedCountry != null) loadCities();
        if(selectedCity != null) loadSchools();
        if(selectedSchool != null) loadCampuses();
        if(selectedCampus != null) retrieveSchoolCourses();

    }

    public void refreshCities(){
    	selectedCity = null;
		selectedSchool = null;
		selectedCampus = null;
		loadCities();
    }

	private void loadCities(){
		optCities = new List<SelectOption>();
		optCities.add(new SelectOption('', ' - Choose a City - '));
		optSchools = new List<SelectOption>();
		optCampuses = new List<SelectOption>();
		try {
			if(selectedCountry != null)
				for(String city : filters.get(selectedCountry).keySet())
					optCities.add(new SelectOption(city, city));
		} catch (Exception e){
			ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an error creating the city options. ' + e);
            ApexPages.addMessage(Msg);
		}
	}

	public void refreshSchools(){
		selectedSchool = null;
		selectedCampus = null;
		loadSchools();
	}

	private void loadSchools(){
		optSchools = new List<SelectOption>();
		optSchools.add(new SelectOption('', ' - Choose a School - '));
		optCampuses = new List<SelectOption>();
		try {
			if(selectedCountry != null && selectedCity != null)
				for(String schoolid : filters.get(selectedCountry).get(selectedCity).keySet())
					optSchools.add(new SelectOption(schoolid, schoolNames.get(schoolid)));
		} catch (Exception e){
			ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an error creating the school options. ' + e);
            ApexPages.addMessage(Msg);
		}
	}

	public void refreshCampuses(){
		selectedCampus = null;
		loadCampuses();
	}
	private void loadCampuses(){
		optCampuses = new List<SelectOption>();
		optCampuses.add(new SelectOption('', ' - Choose a Campus - '));

		try {
			if(selectedCountry != null && selectedCity != null && selectedSchool != null)
				for(String campusid : filters.get(selectedCountry).get(selectedCity).get(selectedSchool).keySet())
					optCampuses.add(new SelectOption(campusid, filters.get(selectedCountry).get(selectedCity).get(selectedSchool).get(campusid)));
		} catch (Exception e){
			ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'There was an error creating the campus options. ' + e);
            ApexPages.addMessage(Msg);
		}

	}


    public void retrieveSchoolCourses(){
        courses = new List<SelectOption>();
        packages = new List<Campus_Course__c>();
        originalPackages = new List<Campus_Course__c>();

        if(selectedCampus != null && selectedCampus != 'campus'){
            for(Campus_Course__c campusCourse : [select id, campus__r.Name, course__r.name, Course__r.Package__c, Package_Courses__c, Course__r.Hours_Week__c, Course__r.Optional_Hours_Per_Week__c, Course__r.Course_Unit_Type__c, Is_Available__c
                                    			from Campus_Course__c where Campus__c = :selectedCampus order by Course__r.Name]){
				campusName = campusCourse.campus__r.Name;
                if(campusCourse.course__r.package__c){
                    packages.add(campusCourse);
                    originalPackages.add(campusCourse.clone(true));
                } else {
                    String courseName = campusCourse.course__r.Name + getCourseHours(campusCourse.course__r);
                    if(!campusCourse.Is_Available__c)
                        courseName += ' (unavailable)';
                    courses.add(new SelectOption(campusCourse.id, courseName));
                }
            }
        }

    }

    private String getCourseHours(Course__c course){
        String courseHours = '';
        if(course.Hours_Week__c != null && course.Hours_Week__c != 0){
            courseHours = ' (' + Integer.valueOf(course.Hours_Week__c) + 'hrs';

            if(course.Optional_Hours_Per_Week__c != null && course.Optional_Hours_Per_Week__c != 0){
                courseHours += ' + ' + Integer.valueOf(course.Optional_Hours_Per_Week__c) + ' opt./' + course.Course_Unit_Type__c + ')';
            } else {
                courseHours += '/'+ course.Course_Unit_Type__c + ')';
            }

        }
        return courseHours;
    }


    public PageReference savePackages(){
        List<Campus_Course__c> updateCourses = new List<Campus_Course__c>();

        system.debug('@#$ packages: ' + packages);

        for(Campus_Course__c cPack : packages){
            cPack.Package_Courses__c = fixField(cPack.Package_Courses__c);
            for(Campus_Course__c oPack : originalPackages){
                if(cPack.id == oPack.id && cPack.Package_Courses__c != oPack.Package_Courses__c)
                    updateCourses.add(cPack);
            }
        }

        if(!updateCourses.isEmpty()){
            update updateCourses;
            retrieveSchoolCourses();
            ApexPages.Message Msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Courses saved.');
            ApexPages.addMessage(Msg);
        }

		return null;
    }

    public void cancel(){ retrieveSchoolCourses(); }

    private String fixField(String toFix){
        if(toFix != null)
            return toFix.replaceAll('[^a-zA-Z0-9\\,]+', '');
        else return toFix;
    }

    private String getParam(String paramName){
        if(ApexPages.currentPage().getParameters().get(paramName) != null && ApexPages.currentPage().getParameters().get(paramName).trim() != '')
            return ApexPages.currentPage().getParameters().get(paramName).trim();
        else return null;

    }

}