global without sharing class school_external_pages {

    public boolean allowCreateSchoolLoginDetails{get;set;}
    
    global school_external_pages() {
        allowCreateSchoolLoginDetails = [SELECT ID, Allow_Create_School_Login_Details__c FROM User WHERE ID = :UserInfo.getUserId()].Allow_Create_School_Login_Details__c;
    }

    @RemoteAction
    @ReadOnly
    global static Map<String, Object> initPage(List<String> schoolIds, Boolean validateAccess, String idsToValidateString){
        Map<String, Object> response = new Map<String, Object>();

        Set<String> idsToValidate = null;
        if(!String.isEmpty(idsToValidateString)){
            idsToValidate = new Set<String>(idsToValidateString.split(','));    
        }
        Map<String, List<IPClasses.SchoolExternalPage>> externalPages = PMCHelper.retrieveSchoolExternalPages(schoolIds, validateAccess, idsToValidate);
        String [] accountTypes = new String[]{'Agency','Agency Group'};
        
        Map<String, Map<String,String>> accountsAndUsers = new Map<String, Map<String,String>>();
        accountsAndUsers.put('Agency Groups' , new Map<String, String>());
        accountsAndUsers.put('Agencies' , new Map<String, String>());
        accountsAndUsers.put('Users' , new Map<String, String>());
        
        Account globalLink = [SELECT ID, Name FROM Account WHERE Name = 'IP Global Link' LIMIT 1];

        for(Account acc : [SELECT ID, Name, Parent.Name, RecordType.Name FROM Account WHERE RecordType.Name in :accountTypes AND Global_Link__c = :globalLink.ID ORDER BY Parent.Name, Name]){
            if(acc.RecordType.Name == 'Agency'){
                accountsAndUsers.get('Agencies').put(acc.ID, acc.Name + ' - ' + acc.Parent.Name );
            }else{
                accountsAndUsers.get('Agency Groups').put(acc.ID, acc.Name);
            }
        }
        for(User user : [SELECT ID, Contact.Name, Contact.Account.Name FROM User WHERE Contact.Account.Parent.Global_Link__c = :globalLink.ID ORDER BY Name, Contact.Account.Parent.Name, Contact.Account.Name]){
            accountsAndUsers.get('Users').put(user.ID, user.Contact.Name + ' - ' + user.Contact.Account.Name);
        }

        Map<String, String> schoolNames = new Map<String, String>();
        for(Account school : [SELECT ID, Name, school_external_pages__c FROM Account WHERE ID IN :schoolIds]){
            schoolNames.put(school.ID, school.Name);
        } 

        response.put('schoolNames', schoolNames);
        response.put('currentUserName', [SELECT Name FROM User WHERE ID = :UserInfo.getUserId()].Name);
        response.put('accountsAndUsers', accountsAndUsers);
        response.put('externalPages', externalPages);
        return response;
    }

    @RemoteAction
    global static void saveChanges(Map<String, List<IPClasses.SchoolExternalPage>> existingUrls){
        List<Account> schoolsToUpdate = new List<Account>();
        for(String schoolId : existingUrls.keySet()){
            schoolsToUpdate.add(new Account(id = schoolId, school_external_pages__c = JSON.serialize(existingUrls.get(schoolId))));
        }
        update schoolsToUpdate;       
    }
}