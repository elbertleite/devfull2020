public without sharing class pfs_schools {
	
	public string selectedCountry{get; set;}
	public string selectedCity{get; set;}
	public string selectedSchool{get; set;}
	
	//============================return school Destinations=============================================== 
	private List<SelectOption> CountryDestinations;
	public List<SelectOption> getCountryDestinations() {
		if(CountryDestinations == null){
			CountryDestinations = new List<SelectOption>();
			for(AggregateResult ld: [Select BillingCountry from Account where recordType.name = 'Campus' group by  BillingCountry order by BillingCountry]){
				CountryDestinations.add(new SelectOption((String)ld.get('BillingCountry'),(String)ld.get('BillingCountry')));
				if(selectedCountry == null)
					selectedCountry = (String)ld.get('BillingCountry');
			}
		}
		return CountryDestinations;
	}
	
	
	private List<SelectOption> cityDestinations;
	public List<SelectOption> getCityDestinations() {
		if(cityDestinations == null && selectedCountry != null){
			cityDestinations = new List<SelectOption>();
			cityDestinations.add(new SelectOption('all','All'));
			for(AggregateResult ld: [Select BillingCity from Account where recordType.name = 'Campus' and BillingCountry = :selectedCountry group by  BillingCity order by BillingCity]){
				cityDestinations.add(new SelectOption((String)ld.get('BillingCity'),(String)ld.get('BillingCity')));
			}
		}
		return cityDestinations;
	}
	
	
	private List<SelectOption> schools;
	public List<SelectOption> getSchools() {
		if(schools == null && selectedCity != null){
			schools = new List<SelectOption>();
			schools.add(new SelectOption('all','All'));
			for(AggregateResult ld: [Select parentId, Parent.Name school from Account where recordType.name = 'Campus' and BillingCountry = :selectedCountry and BillingCity = :selectedCity and ParentId != null group by parentId, Parent.Name order by Parent.Name]){
				schools.add(new SelectOption((String)ld.get('parentId'),(String)ld.get('school')));
			}
		}
		return schools;
	}
	
	public list<account> listSchools{get; set;}
	
	public void searchSchools(){
		list<string> schoolIds = new list<string>();
		string sqlSchool = 'Select parentid  from Account WHERE RecordType.name = \'campus\' AND BillingCountry = :selectedCountry ';
		if(selectedCity != null && selectedCity != 'all')
			sqlSchool += ' and BillingCity = :selectedCity ';
			
		if(selectedSchool != null && selectedSchool != 'all')
			sqlSchool += ' and parentid = :selectedSchool ';
			
		sqlSchool += ' group by parentid';
		
				System.debug('==>selectedSchool: '+selectedSchool);
				
		for(AggregateResult sc:Database.query(sqlSchool))
			schoolIds.add((String)sc.get('parentId'));
			
		System.debug('==>schoolIds: '+schoolIds); 
		listSchools = [Select id, Name, BillingCountry, BillingCity, PFS_School__c, Pfs_First_Instalment_Only__c, PFS_Details__c, PFS_in_Bulk__c, isTaxDeductible__c, isCOE_Required__c, School_Status__c from Account where id in :schoolIds order by BillingCity, Name];	
		
	}
	
	public void refreshSchool(){
		schools = null;
		selectedSchool = 'all';
	}
	
	public void refreshCity(){
		cityDestinations = null;
		schools = null;
		selectedCity = 'all';
		selectedSchool = 'all';
	}
	
	public PageReference savePFS(){
		update listSchools;
		return null;
	}
	
	public PageReference cancelPFS(){
		searchSchools();
		return null;
	}
	
}