/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class search_school_test {

    static testMethod void myUnitTest() {
        TestFactory factory = new TestFactory();
       
    Map<String,String> recordTypes = new Map<String,String>();       
    for( RecordType r :[select id, name from recordtype where isActive = true] )
      recordTypes.put(r.Name,r.Id);
    
      Account school = factory.createSchool();
      Account agency = factory.createAgency();
      
    Contact employee = factory.createEmployee(agency);
    User userEmp = factory.createPortalUser(employee);
    
  
    list<String> countries = new list<String>{'Australia', 'New_Zealand', 'Canada', 'United_States', 'United_Kingdom', 'Ireland', 'South_Africa', 'Denmark', 'Sweden', 'Malta', 'Germany', 'Spain', 'Italy', 'Netherlands'};
    
    list<Account> la = new list<Account>();
    for(String c : countries){

      Account campus = new Account();
      campus.account_currency_iso_code__c = 'AUD';
      campus.RecordTypeId = recordTypes.get('Campus');
      campus.Name = 'Test Campus';
      campus.BillingCountry = c;
      campus.BillingCity = 'Test City';
      campus.BillingState = 'State';
      campus.ParentId = school.id;    

      la.add(campus);
    }//end for
    
    insert la;

    list<Supplier__c> sp = new list<Supplier__c>();
    for(Account cp : la){

      Supplier__c sp3 = new Supplier__c();
      sp3.Agency__c = agency.id;
      sp3.Supplier__c = cp.id;
      sp3.Record_Type__c = 'campus';
      sp3.Available__c = true;
      sp3.preferable__c = true;
      sp3.web_available__c = true;

      sp.add(sp3);
    }// end for
       
    insert sp;
     
         
     System.RunAs(userEmp){ 
        search_school els = new search_school(new Apexpages.Standardcontroller(school));
      list<SelectOption> typeOptions = els.typeOptions;
      string sShow = els.sShow;
      string name = els.name;
      list<search_school.countriesNames> lcountry = els.lcountry;
      String selectedType = els.selectedType;


      ApexPages.currentPage().getParameters().put('accountID', la[0].id);
      els.redirectTo();


      search_school.findSchools(0, 50, 'Australia', '', '', '', '', '', 'all', 'all', 'all');
      search_school.findSchools(0, 50, 'Australia', '', '', '', '', 'myfavourite', 'all', 'all','IT');
      search_school.changeState('Australia', 'State');
      search_school.retrieveSchoolCourses(school.id);
        
     }
    }
}