public class SchoolMap{

	public List<Account> getCampus(){
		
		return [SELECT Id, BillingLatitude, BillingLongitude, Name, Why_choose_this_campus__c FROM Account where RecordType.Name = 'Campus' and BillingLatitude != null and BillingLongitude != null ];
		
	}
	
	public list<Account> getAgencies(){
		return [SELECT ParentId, BillingLatitude, BillingLongitude, Name, Parent.General_Description__c, Parent.Name  FROM Account where RecordType.Name = 'Agency' and BillingLatitude != null and BillingLongitude != null ];
		
	}
	
	
	public String communityName {
		get{
			if(communityName == null)
				communityName = IPFunctions.getCommunityName();
			return communityName;
		}
		set;
	}
	
}