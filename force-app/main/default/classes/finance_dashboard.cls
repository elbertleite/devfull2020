public with sharing class finance_dashboard {
    
    public User currentUser{get; set;}
    
    
    public finance_dashboard(){
     	currentUser = [Select Id, Name, Aditional_Agency_Managment__c, Contact.Account.ParentId, Contact.AccountId, Contact.Department__c, Contact.Account.Name from User where id = :UserInfo.getUserId() limit 1];
      	selectedAgency = currentUser.Contact.AccountId;
      	updateChart();
    }
    
    public class paymentPerAgency{
    	public integer totItems { get; set; }
		public string period { get; set; }
		public decimal sales { get; set; }
		public decimal commissions { get; set; }
		public paymentPerAgency( integer totItems ,string period , decimal sales, decimal commissions) {
			this.totItems = totItems;
			this.period = period;
			this.sales = sales;
			this.commissions = commissions;
		}
	}
	
	
	 public class accountBalance{
		public decimal fromClient { get; set; }
		public decimal toSchool { get; set; }
		public decimal balance { get; set; }
		public decimal discount { get; set; }
		public string period { get; set; }
		public decimal instalment { get; set; }
		public accountBalance( decimal fromClient, decimal toSchool, decimal balance, decimal discount, string period, decimal instalment) {
			this.fromClient = fromClient;
			this.toSchool = toSchool;
			this.balance = balance;
			this.discount = discount;
			this.period = period;
			this.instalment = instalment;
		}
	}
	
	 public class paymentForecast{
		public integer totItems { get; set; }
		public decimal value { get; set; }
		public decimal commissions { get; set; }
		public string period { get; set; }
		public paymentForecast( integer totItems , decimal value, decimal commissions, string period) {
			this.totItems = totItems;
			this.value = value;
			this.commissions = commissions;
			this.period = period;
		}
	}
	
	public list<paymentPerAgency> PaymentPerAgency{get; set;}
	private list<paymentPerAgency> PaymentPerAgency(){
		PaymentPerAgency = new list<paymentPerAgency>();
		for(AggregateResult qd:[Select count(id) tot, CALENDAR_YEAR(Received_Date__c) year,CALENDAR_MONTH(Received_Date__c) month, SUM(Instalment_Value__c) installment, SUM(Commission_Value__c) commission from client_course_instalment__c 
				WHERE Received_By_Agency__c = :selectedAgency and Received_Date__c = LAST_N_MONTHS:6 
				GROUP BY CALENDAR_YEAR(Received_Date__c), CALENDAR_MONTH(Received_Date__c) order by CALENDAR_YEAR(Received_Date__c), CALENDAR_MONTH(Received_Date__c)]){
				string period = returnPeriod((integer)qd.get('year'), (integer)qd.get('month'));
				PaymentPerAgency.add( new paymentPerAgency((integer)qd.get('tot'), period, (decimal)qd.get('installment'), (decimal)qd.get('commission')) );
			}
		return PaymentPerAgency;
	}
	
	public list<accountBalance> agencyBalance{get; set;}
	private void mapAgencyBalance(){
		agencyBalance = new list<accountBalance>();
		map<string,accountBalance> mapAgencyBalance = new map<string,accountBalance>();
		for(AggregateResult qd:[Select CALENDAR_YEAR(Received_On__c) year, CALENDAR_MONTH(Received_On__c) month, SUM(Value__c) tot from client_course_instalment_payment__c 
								WHERE Received_By_Agency__c = :selectedAgency and Date_Paid__c = LAST_N_MONTHS:6 and Confirmed_Date__c != null
								group by CALENDAR_YEAR(Received_On__c), CALENDAR_MONTH(Received_On__c) order by CALENDAR_YEAR(Received_On__c), CALENDAR_MONTH(Received_On__c)]){
			string period = returnPeriod((integer)qd.get('year'), (integer)qd.get('month'));
			mapAgencyBalance.put(period, new accountBalance((decimal)qd.get('tot'), 0, 0, 0, period, 0) );
		}
		
		for(AggregateResult ar:[Select SUM(Instalment_Value__c) install, SUM(Total_School_Payment__c) paidSchool, SUM(Discount__c) disc, 
								CALENDAR_YEAR(Due_Date__c) year, CALENDAR_MONTH(Due_Date__c) month from client_course_instalment__c 
								WHERE Received_By_Agency__c = :selectedAgency and Paid_To_School_On__c  = LAST_N_MONTHS:6 and isMigrated__c = false
								group by CALENDAR_YEAR(Due_Date__c) , CALENDAR_MONTH(Due_Date__c)  
								order by CALENDAR_YEAR(Due_Date__c) , CALENDAR_MONTH(Due_Date__c) ]){
			string period = returnPeriod((integer)ar.get('year'), (integer)ar.get('month'));
			if(!mapAgencyBalance.containsKey(period)){
				mapAgencyBalance.put(period, new accountBalance(0, (decimal)ar.get('paidSchool'), (decimal)ar.get('paidSchool') - (decimal)ar.get('disc'), (decimal)ar.get('disc'), period, (decimal)ar.get('install')) );
			}else{
				mapAgencyBalance.get(period).fromClient -= (decimal)ar.get('disc');
				mapAgencyBalance.get(period).toSchool = (decimal)ar.get('paidSchool');
				mapAgencyBalance.get(period).discount = (decimal)ar.get('disc');
				mapAgencyBalance.get(period).balance = mapAgencyBalance.get(period).fromClient - (decimal)ar.get('paidSchool') - (decimal)ar.get('disc');
				mapAgencyBalance.get(period).instalment = (decimal)ar.get('install');
			}		
		}
		
		
		agencyBalance = mapAgencyBalance.values();
	}
	
	public list<paymentForecast> PaymentForecast{get; set;}
	private list<paymentForecast> PaymentForecast(){
		PaymentForecast = new list<paymentForecast>();
		for(AggregateResult qd:[Select count(id) tot, SUM(Instalment_Value__c) value, SUM(Commission_Value__c) commission, CALENDAR_YEAR(Due_Date__c) year, CALENDAR_MONTH(Due_Date__c) month from client_course_instalment__c 
					WHERE client_course__r.Enroled_by_Agency__c = :selectedAgency and (Due_Date__c = THIS_MONTH or Due_Date__c = NEXT_N_MONTHS:6) group by CALENDAR_YEAR(Due_Date__c), CALENDAR_MONTH(Due_Date__c) order by CALENDAR_YEAR(Due_Date__c), CALENDAR_MONTH(Due_Date__c)]){
				string period = returnPeriod((integer)qd.get('year'), (integer)qd.get('month'));
				PaymentForecast.add( new paymentForecast((integer)qd.get('tot'), (decimal)qd.get('value'), (decimal)qd.get('commission'), period ));
			}
		return PaymentForecast;
	}
	
	public void updateGroup(){
		selectedAgency='';
		updateChart();
	}
	
	public string selectedAgencyName{get{if(selectedAgencyName == null) selectedAgencyName = ''; return selectedAgencyName;} set;}
	public void updateChart(){
		returnAgencyOptions();
		if(agencyName.containsKey(selectedAgency))
			selectedAgencyName = agencyName.get(selectedAgency);
		System.debug('==>selectedAgency: '+selectedAgency);
		PaymentPerAgency();
		PaymentForecast();
		//mapAgencyBalance();
	}
	
	private string returnPeriod(integer year, integer month){
		list<string> months = new list<string>{'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'};
		System.debug('==>month: '+month);
		return year +'/'+ months[month-1];
	}
	
	
	public string selectedAgencyGroup{
    	get{
    		if(selectedAgencyGroup == null)
    			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
    		return selectedAgencyGroup;
    	}  
    	set;
    }
    
   public List<SelectOption> agencyGroupOptions {
  		get{
   			if(agencyGroupOptions == null){
   				
   				agencyGroupOptions = new List<SelectOption>();  
   					System.debug('==>Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible(): '+Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible());
   				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group  
	    			for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:currentUser.Contact.AccountId order by Parent.Name]){
	    				if(ag.Parent.Name != null && ag.ParentId != null)
		     				agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
		     			System.debug('==>ag: '+ag);
		    		}
   				}else{
   					for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
		     			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
		     			System.debug('==>ag2: '+ag);
		    		}
   				}
	   		}
	   		System.debug('==>agencyGroupOptions: '+agencyGroupOptions);
	   		return agencyGroupOptions;
	  }
	  set;
 	}

	public map<string, string> agencyName{get; set;}
	public String selectedAgency {get;set;}
	public List<SelectOption> agencyOptions{get; set;}
	private List<SelectOption> returnAgencyOptions() {
   		if(selectedAgencyGroup != null){
   			
   			agencyName = new map<string, string>();
   			agencyOptions = new List<SelectOption>();
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
				for(Account a: [Select Id, Name from Account where id =:currentUser.Contact.AccountId order by name]){
					if(selectedAgency=='')
						selectedAgency = a.Id;					
					agencyOptions.add(new SelectOption(a.Id, a.Name));
					agencyName.put(a.Id, a.Name);
					if(currentUser.Aditional_Agency_Managment__c != null){
						for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
							agencyOptions.add(new SelectOption(ac.id, ac.name));
							agencyName.put(ac.Id, ac.Name);
						}
					}
				}
			}else{
				for(Account a: [Select Id, Name from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
					agencyOptions.add(new SelectOption(a.Id, a.Name));
					agencyName.put(a.Id, a.Name);
					if(selectedAgency=='')
						selectedAgency = a.Id;	
				}
			}
			//updateChart();
		}
	   	return agencyOptions;
	}
	
	public list<AggregateResult> mostEnroledCampuses{
		get{
			return [SELECT Campus_Name__c campus, count(id) total FROM client_course__c where 
					Enroled_by_Agency__c= :selectedAgency and Booking_Number__c != null and enrolment_date__c != null  group by Campus_Name__c order by count(id) desc limit 15];
		}
		set;
	}
	
	public list<AggregateResult> mostEnroledCourses{
		get{
			return [SELECT Campus_Name__c campus, Course_Name__c course, count(id) total FROM client_course__c where 
					Enroled_by_Agency__c= :selectedAgency and Booking_Number__c != null and enrolment_date__c != null  group by Campus_Name__c, Course_Name__c order by count(id) desc limit 15];
		}
		set;
	}
	
	
	
}