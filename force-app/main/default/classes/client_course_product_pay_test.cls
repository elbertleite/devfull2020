@isTest
private class client_course_product_pay_test {

   public static User portalUser {get{
	if (null == portalUser) {
	portalUser = [Select Id, Name, Contact.AccountId from User where email = 'test12345@test.com' limit 1];
	} return portalUser;} set;}

	@testSetup static void setup() {
		TestFactory tf = new TestFactory();
		tf.setupClientProdcutsTest();
	}


	static testMethod void classVariables() {
		system.runAs(portalUser){
			//Pay Product
			client_product_service__c cliProduct = [Select Id from client_product_service__c WHERE Related_to_Product__c = null limit 1];

			client_course_product_pay p = new client_course_product_pay(new ApexPages.StandardController(cliProduct));

			Contact receivedOn = p.receivedOn;
			String selectedAgencyGroup = p.selectedAgencyGroup;
			String selectedAgency = p.selectedAgency;
			String selectedUser = p.selectedUser;
			List<SelectOption> agencyGroupOptions = p.agencyGroupOptions;
			List<SelectOption> agencyOptions = p.agencyOptions;
			List<SelectOption> userOptions = p.userOptions;
			p.getdepositListType();
			p.cancelPayment();
			p.changeAgencyGroup();
			p.changeAgency();
			
		}
	}

	static testMethod void payProduct() {
		system.runAs(portalUser){
			//Pay Product
			client_product_service__c cliProduct = [Select Id from client_product_service__c WHERE Related_to_Product__c = null limit 1];

			client_course_product_pay p = new client_course_product_pay(new ApexPages.StandardController(cliProduct));

			p.newPayDetails.Payment_Type__c = 'Creditcard';
			p.newPayDetails.Value__c = 0;
			p.addPaymentValue();
			p.newPayDetails.Value__c = p.totalPay + 10;
			p.addPaymentValue();
			p.newPayDetails.Value__c = p.totalPay/2;
			p.newPayDetails.Date_Paid__c = system.today().addDays(10);
			p.addPaymentValue();
			p.newPayDetails.Date_Paid__c = system.today().addDays(-30);
			p.addPaymentValue();
			p.newPayDetails.Date_Paid__c = system.today();
			p.addPaymentValue();
			p.newPayDetails.Payment_Type__c = 'Creditcard';
			p.newPayDetails.Value__c = p.totalPay/2;
			p.addPaymentValue();

			ApexPages.currentPage().getParameters().put('index', '1');
			p.removePaymentValue();

			p.newPayDetails.Payment_Type__c = 'Cash';
			p.newPayDetails.Value__c = p.totalPay/2;
			p.addPaymentValue();

			p.paidByAgency.Paid_by_Agency__c = true;
			p.savePayment();
		}
	}
}