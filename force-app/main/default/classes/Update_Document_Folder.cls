public with sharing class Update_Document_Folder {
	
	private String campusID;
	
	private List<Account_Document_File__c> folderList;
	
	public Update_Document_Folder(ApexPages.StandardController controller){
		campusID = controller.getid();
		
		folderList = [Select A.Content_Type__c, A.Description__c, A.File_Name__c, A.File_Size__c, A.File_Size_in_Bytes__c, A.Parent__c, A.Parent_Folder_Id__c, A.URL__c, A.WIP__c, preview_link__c from Account_Document_File__c A 
						WHERE A.Parent__c = :campusID and Content_Type__c = 'Folder' order by File_Name__c];
		
		hasFolders = folderList != null && folderList.size() > 0 ? true : false;
	}
	
	public void createFolders(){
		 
    	List<Account_Document_File__c> defaultFolders = new List<Account_Document_File__c>();
		Account_Document_File__c adf;	
			
		
		Schema.DescribeFieldResult fieldResult = Account.Default_Campus_Folders__c.getDescribe();
   		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();        
		for( Schema.PicklistEntry f : ple){
		   	adf = new Account_Document_File__c();
			adf.Content_Type__c = 'Folder';
			adf.File_Name__c = f.getLabel();
			adf.Parent__c = campusID;
			defaultFolders.add(adf);					
		}

		insert defaultFolders;
		
		setFolderList();
		orderedFolders = null;
		files = null;
		folderMap = null;
		folderOptions = null;
		getFiles();		
			
	}
	
	private void setFolderList(){
		
		folderList = [Select A.Content_Type__c, A.Description__c, A.File_Name__c, A.File_Size__c, A.File_Size_in_Bytes__c, A.Parent__c, A.Parent_Folder_Id__c, A.URL__c, A.WIP__c, preview_link__c from Account_Document_File__c A 
						WHERE A.Parent__c = :campusID and Content_Type__c = 'Folder' order by File_Name__c];
		hasFolders = folderList != null && folderList.size() > 0 ? true : false;
	}
	
	public boolean hasFolders {get;set;}
	
	private Map<String, List<Account_Document_File__c>> files;

	public Map<String, List<Account_Document_File__c>> getFiles(){
		
		if(files == null){
			getFolderMap();
			files = new Map<String, List<Account_Document_File__c>>();
			for(String folderID : folderMap.keySet())
				files.put(folderID == null ? '' : folderID, new List<Account_Document_File__c>());
			
			
			List<Account_Document_File__c> listadf = new List<Account_Document_File__c>();
			for(Account_Document_File__c adf : [Select Selected__c, A.Content_Type__c, A.Description__c, A.File_Name__c, A.File_Size__c, A.File_Size_in_Bytes__c, A.Parent__c, A.Parent_Folder_Id__c, A.URL__c, A.WIP__c, preview_link__c
												from Account_Document_File__c A WHERE A.Parent__c = :campusID and Content_Type__c != 'Folder' and WIP__c = false order by File_Name__c]){
				listadf = files.get(adf.Parent_Folder_Id__c == null ? '' : adf.Parent_Folder_Id__c);
				listadf.add(adf);
				files.put(adf.Parent_Folder_Id__c, listadf );
					
					
					
			}
		}
		return files;
		
	}
	
	
	private Map<String, String> folderMap;
	public List<String> orderedFolders {
		get{
			if(orderedFolders == null){
				orderedFolders = new List<String>();
				orderedFolders.add('');
				for(Account_Document_File__c adf : folderList)
					orderedFolders.add(adf.id);
			}
			return orderedFolders;	
		}
		Set;
	}
	public Map<String, String> getFolderMap(){
		
		if(folderMap == null){
			folderMap = new Map<String, String>();
			
			for(Account_Document_File__c adf : folderList)				
				folderMap.put(adf.id, adf.File_Name__c);
			
			
			folderMap.put('', 'Not Classified');
			
		}
		
		return folderMap;
		
	}
	
	
	public String selectedFolder {get;Set;}	
	private List<SelectOption> folderOptions;
	
	public List<SelectOption> getFolderOptions(){

		
		if(folderOptions == null){
			folderOptions = new List<SelectOption>();
			folderOptions.add(new SelectOption('', '--Select--'));
			if(folderList != null)
				for(Account_Document_File__c adf : folderList)
					folderOptions.add(new SelectOption(adf.Id, adf.File_Name__c));
			
		}
		
		
		return folderOptions;
		
	}
	
	public boolean editMode { get {if(editMode == null) editMode = false; return editMode; } Set; }
	
	public void edit(){
		editMode = true;
	}
	
	public void cancelEdit(){		
		clearLists();
	}

	public void save(){
		List<Account_Document_File__c> listADF = new List<Account_Document_File__c>();
		Set<String> ids = new Set<String>();
		for(String folderid : files.keySet())
			for(Account_Document_File__c adf : files.get(folderid))
				if(!ids.contains(adf.id)){
					listADF.add(adf);
					ids.add(adf.id);
				}
			
		
		update listADF;
		
		clearLists();
		
	}
	
	public pagereference viewFile(){
		String fileId =  Apexpages.currentPage().getParameters().get('fileId');
		try {		
			return new pageReference(cg.SDriveTools.getAttachmentURL( campusID , fileId, (100 * 60)));
		} catch (Exception e){
			return null;
		}
	}
	
	private void clearLists(){
		editMode = false;
		folderOptions = null;
		orderedFolders = null;
		folderMap = null;		
		files = null;
	}

}