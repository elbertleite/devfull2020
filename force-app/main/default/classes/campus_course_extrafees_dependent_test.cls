/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class campus_course_extrafees_dependent_test {

    static testMethod void myUnitTest() {
        
    	TestFactory testFactory = new TestFactory();
        
		Account school = TestFactory.createSchool();
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		
		Nationality_Group__c ng = new Nationality_Group__c();
		ng.Account__c = school.id;
		ng.Country__c = 'Australia';
		ng.Name = 'Australia';
		insert ng;
       
       	Account agency = TestFactory.createAgency();
       
		Account campus = TestFactory.createCampus(school, agency);
		
		 
       
		Course__c course = TestFactory.createCourse();
		
		Course__c course2 = TestFactory.createLanguageCourse();
				
		Campus_Course__c cc = TestFactory.createCampusCourse(campus, course);
		
		Campus_Course__c cc2 = TestFactory.createCampusCourse(campus, course2);
       
		Course_Price__c cp = TestFactory.createCoursePrice(cc, 'Latin America');
		
		Course_Price__c cp2 = TestFactory.createCoursePrice(cc2, 'Published Price');
				
		Course_Intake_Date__c cid = TestFactory.createCourseIntakeDate(cc);
		
		Course_Intake_Date__c cid2 = TestFactory.createCourseIntakeDate(cc2);
        
        
		Course_Extra_Fee__c cef = new Course_Extra_Fee__c();
		cef.Campus_Course__c = cc.Id;
		cef.Nationality__c = 'Australia';
		cef.Availability__c = 3;
		cef.From__c = 1;
		cef.Value__c = 100;		
		cef.Product__c = p.id;		
		insert cef;		
		
		
		Course_Extra_Fee_Dependent__c cefd = new Course_Extra_Fee_Dependent__c();
		cefd.Course_Extra_Fee__c = cef.id;
		cefd.Product__c = p.id;
		cefd.Details__c = 'Testing';
		insert cefd;
		
        
		Course_Extra_Fee__c cef2 = new Course_Extra_Fee__c();
		cef2.Campus__c = campus.Id;
		cef2.Nationality__c = 'Australia';
		cef2.Availability__c = 3;
		cef2.From__c = 2;
		cef2.Value__c = 100;		
		cef2.Optional__c = true;
		cef2.Product__c = p.id;
		insert cef2;
		
		
		Apexpages.Standardcontroller ct = new ApexPages.Standardcontroller(cef);
		Apexpages.currentPage().getParameters().put('campusId', campus.id);
				
		campus_course_extrafees_dependent cced = new campus_course_extrafees_dependent(ct);
		
		cced.getschoolFees();
		cced.cancelEdit();		
		
		ApexPages.currentPage().getParameters().put('selProdId', cefd.id);
		cced.editRelatedFees();
		
		cced.newDependentFee.Details__c = 'Editing';
		cced.addrelatedFee();
		
		
		ApexPages.currentPage().getParameters().put('delProduct', cefd.id);
		cced.deleteRelatedFees();
		
		
		for(Course_Extra_Fee_Dependent__c dep : cced.listFees)
			dep.Value__c = 100;
		
		cced.saveRelatedFees();
		
		
		try {
			cced.viewFile();
		} catch (Exception e){}
		
		List<SelectOption> so = cced.files;
        
    }
}