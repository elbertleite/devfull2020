public with sharing class agency_group_list_agencies {
	
	public Account agencyGroup {get;set;}
	public Account agency {get;set;}
	private String agencyGroupID;
	
	public agency_group_list_agencies(ApexPages.StandardController controller){
		
		this.agency = [Select id, ParentID from account where id = :controller.getRecord().id];
		this.agencyGroupID =  agency.ParentID;
		
		agencyGroup = getDetails(this.agencyGroupID);
	}
	
	private Account getDetails(String id){
		return [Select Name, CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name, Nickname__c, Group_Type__c,
									Registration_name__c, Year_established__c, BusinessEmail__c, Website
							 from Account where id = :id];
	}
	
	
	private List<Account> agencies;
	public List<Account> getAgencies(){
		if(agencies == null){
			
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
				user currentUser = [Select Id, Aditional_Agency_Managment__c, Contact.AccountId from User where id = :UserInfo.getUserId() limit 1];

				list<string> idsAgency = new list<string>{currentUser.Contact.AccountId};
			
				if(currentUser.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
						idsAgency.add(ac.id);
					}
				}
				agencies = [Select Name, CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name, Nickname__c, Agency_Type__c, NumberOfEmployees, Inactive__c,
									Registration_name__c, Year_established__c, BusinessEmail__c, Website, BillingCountry, BillingCity
						from Account where id in :idsAgency order by Name];
			}else{
				agencies = [Select Name, CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name, Nickname__c, Agency_Type__c, NumberOfEmployees, Inactive__c,
									Registration_name__c, Year_established__c, BusinessEmail__c, Website, BillingCountry, BillingCity
						from Account where RecordType.Name = 'Agency' order by Name];
			}
			
		}
		
		return agencies;
	}
	

}