public with sharing class products_request_commission {

	private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}

	private User currentUser = [Select Id, Contact.Name, Contact.Email, Contact.Signature__c, Aditional_Agency_Managment__c, Contact.Account.Name, Contact.Account.ParentId, Contact.AccountId, Contact.Department__c, Contact.Account.Global_Link__c, Contact.Account.account_currency_iso_code__c from User where id = :UserInfo.getUserId() limit 1];

	
	//Currencies
	public DateTime currencyLastModifiedDate {get;set;}
	public list<SelectOption> mainCurrencies {get;set;}
	private Map<String, double> agencyCurrencies;
	//

	public String showTab {get;set;}
	public products_request_commission() {
		showTab = 'reqComm';
		mainCurrencies = ff.retrieveMainCurrencies();
		agencyCurrencies = ff.retrieveAgencyCurrencies();
		currencyLastModifiedDate = ff.currencyLastModifiedDate;
		searchProducts();
	}

	public Decimal totRequest {get;set;}	
	public Decimal totConfirm {get;set;}
	public void search(){
		if(showTab == 'reqComm'){
			providersOpt.remove(0);
			if(providersOpt.size()>0)
				providersOpt.add(0, new SelectOption('none', '-- Select Provider --'));
			else
				providersOpt.add(new SelectOption('none', '-- Select Provider --'));
			if(selectedProvider == 'all')
				selectedProvider = 'none';
			searchProducts();
		}
		else if(showTab == 'confComm'){
			searchInvoices();
			providersOpt.remove(0);
			if(providersOpt.size()>0)
				providersOpt.add(0, new SelectOption('all', '-- All --'));
			else
				providersOpt.add(new SelectOption('all', '-- All --'));
		}
	} 

	// S E A R C H 		I N V O I C E S
	public list<Invoice__c> invoices {get;set;}
	public map<String, list<IPFunctions.activityWrapper>> mapActivities{get;set;}
	public void searchInvoices(){
		listProducts = null;

		String sql = 'SELECT Id, Provider__r.Provider_Name__c, Request_Provider_Commission__c, createdBy.Name, createdDate, Due_Date__c, Total_Value__c, CurrencyIsoCode__c, Sent_Email_To__c, Sent_On__c, Total_Emails_Sent__c, Last_Email_Sent__c, Received_Currency__c, Commission_Notes__c, Invoice_Activities__c, ';
		
		sql+= ' (SELECT  Products_Services__r.Provider__c, Products_Services__r.Provider__r.Provider_Name__c, Client__r.name, Received_By_Department__r.name, Price_Total__c, Quantity__c, Received_Date__c, Received_By_Agency__r.name, Received_by__r.name, Product_Name__c, Currency__c, Agency_Currency_Value__c, Agency_Currency__c, Agency_Currency_Rate__c, Claim_Commission_Type__c, Total_Provider_Payment__c, Commission_Tax_Rate__c, Commission_Tax_Value__c, Commission_Type__c, Commission_Value__c, Commission__c FROM requested_products__r order by Client__r.Name) ';
		
		sql+= ' FROM Invoice__c WHERE ';

		sql += ' Requested_by_Agency__c = \'' + selectedAgency + '\' '; //Requested by agency

		if(selectedProvider != 'none' && selectedProvider != 'all') //Provider
			sql += ' AND  Provider__c = \'' + selectedProvider + '\'';

		
		sql += ' AND Commission_Paid_Date__c = NULL '; //Not received


		sql+= ' AND ((Due_Date__c >= '+ IPFunctions.FormatSqlDateIni(dates.Due_Date__c); //Commission Due Date

		sql+= ' AND Due_Date__c <= '+ IPFunctions.FormatSqlDateFin(dates.Commission_Paid_Date__c); //Commission Due Date

		// if(showOverdue)
		sql+= ' ) OR Due_Date__c = NULL OR Due_Date__c < ' + System.now().format('yyyy-MM-dd') + ' )';//Overdue
		// else sql+= ' ))';

		sql += ' AND isProvider_Commission__c = TRUE AND isCancelled__c = FALSE';
		
		sql += ' order by Due_Date__c ';

		system.debug('invoice===>' + sql);

		invoices = new list<Invoice__c>();
		mapActivities = new map<String, list<IPFunctions.activityWrapper>>();

		for(Invoice__c inv :  Database.query(sql)){
			if(inv.Invoice_Activities__c != null){
				list<IPFunctions.activityWrapper> act = (list<IPFunctions.activityWrapper>) JSON.deserialize(inv.Invoice_Activities__c, list<IPFunctions.activityWrapper>.class);
				for(IPFunctions.activityWrapper w : act)
					w.actDate = Datetime.valueOfGMT(w.actDate).format();


				mapActivities.put(inv.Id, act);
			}
			else
				mapActivities.put(inv.Id, new list<IPFunctions.activityWrapper>());
			invoices.add(inv);
		}//end for


		totConfirm = invoices.size();

		if(selectedProvider != 'none' && selectedProvider != 'all') 
			for(AggregateResult ar : [SELECT count(Id) tot FROM client_product_service__c WHERE Paid_to_Provider_by_Agency__c = :selectedAgency AND Products_Services__r.Provider__c = :selectedProvider AND Request_Commission__c = NULL AND Commission_Confirmed_On__c = NULL AND Claim_Commission_Type__c = 'Paid in full'])
				totRequest = Integer.valueOf(ar.get('tot'));
		else
			for(AggregateResult ar : [SELECT count(Id) tot FROM client_product_service__c WHERE Paid_to_Provider_by_Agency__c = :selectedAgency AND Request_Commission__c = NULL AND Commission_Confirmed_On__c = NULL AND Claim_Commission_Type__c = 'Paid in full'])
				totRequest = Integer.valueOf(ar.get('tot'));
	}

	public void changeTab(){
		showTab = ApexPages.currentPage().getParameters().get('show');

		search();
		ApexPages.currentPage().getParameters().remove('show');
	}

	// S E A R C H 		P R O D U C T S
	public list<client_product_service__c> listProducts{get; set;}
	public void searchProducts(){
		showMsg = false;
		invoices = null;
		listProducts = new list<client_product_service__c>();

		if(selectedProvider != null && selectedProvider != 'none'){

			groupComm = selectedAgencyGroup;
			agencyComm = selectedAgency;
			
			String sql = 'SELECT isSelected__c, Products_Services__r.Provider__c, Products_Services__r.Provider__r.Provider_Name__c, Client__r.name, Received_By_Department__r.name, Price_Total__c, Quantity__c, Received_Date__c, Received_By_Agency__r.name, Received_by__r.name, Product_Name__c, Currency__c, Agency_Currency_Value__c, Agency_Currency__c, Agency_Currency_Rate__c, Claim_Commission_Type__c, Total_Provider_Payment__c, Commission_Tax_Rate__c, Commission_Tax_Value__c, Commission_Type__c, Commission_Value__c, Commission__c FROM client_product_service__c WHERE ';
				
			sql += ' Paid_to_Provider_by_Agency__c = \'' + selectedAgency + '\' AND  Products_Services__r.Provider__c = \'' + selectedProvider + '\' AND Request_Commission__c = NULL AND Commission_Confirmed_On__c = NULL AND Claim_Commission_Type__c = \'Paid in full\' ';

			if(selectedCategory != null && selectedCategory != 'none' && selectedCategory != 'all')
				sql += ' AND ( Products_Services__r.Category__c = \'' + selectedCategory + '\' OR Products_Services__r.Category__c = NULL)';
			
			if(selectedProduct != null && selectedProduct != 'none' && selectedProduct != 'all')
				sql += ' and Products_Services__c = \''+selectedProduct +'\'';
			
			sql += ' order by Currency__c, Received_Date__c, Client__r.name, Product_Name__c';

			system.debug('sql===>' + sql);

			listProducts = Database.query(sql);

			totRequest = listProducts.size();

			for(AggregateResult ar : [SELECT count(Id) tot FROM Invoice__c WHERE Requested_by_Agency__c = :selectedAgency AND Provider__c = :selectedProvider AND Commission_Paid_Date__c = NULL AND isProvider_Commission__c = TRUE AND isCancelled__c = FALSE])
				totConfirm = Integer.valueOf(ar.get('tot')) != null ? Integer.valueOf(ar.get('tot')) : 0;
		}
		else{
			for(AggregateResult ar : [SELECT count(Id) tot FROM Invoice__c WHERE Requested_by_Agency__c = :selectedAgency AND Commission_Paid_Date__c = NULL AND isProvider_Commission__c = TRUE AND isCancelled__c = FALSE])
				totConfirm = Integer.valueOf(ar.get('tot')) != null ? Integer.valueOf(ar.get('tot')) : 0;

			for(AggregateResult ar : [SELECT count(Id) tot FROM client_product_service__c WHERE Paid_to_Provider_by_Agency__c = :selectedAgency AND Request_Commission__c = NULL AND Commission_Confirmed_On__c = NULL AND Claim_Commission_Type__c = 'Paid in full'])
				totRequest = Integer.valueOf(ar.get('tot'));
		}
	}

	// S E T U P 	I N V O I C E		T O 	C O N F I R M 
	public Invoice__c confInv {get{if(confInv == null) confInv = new Invoice__c(); return confInv;}set;}
	public void setupToConfirm(){
		confInv = [SELECT ID, Requested_Commission_for_Agency__r.Name, Request_Provider_Commission__c, CurrencyIsoCode__c, Total_Value__c FROM Invoice__c WHERE ID = :ApexPages.currentPage().getParameters().get('inv') limit 1];

		confInv.Received_Currency__c = currentUser.Contact.Account.account_currency_iso_code__c;
		confInv.Transfer_Receive_Fee_Currency__c = confInv.Received_Currency__c;
		confInv.Received_Currency_Rate__c = agencyCurrencies.get(confInv.CurrencyIsoCode__c);
		confInv.Commission_Paid_Date__c = system.today();
		ff.convertCurrency(confInv, confInv.CurrencyIsoCode__c, confInv.Total_Value__c, null, 'Received_Currency__c', 'Received_Currency_Rate__c', 'Received_Currency_Value__c');


		ApexPages.currentPage().getParameters().remove('inv');
	}

	public void confirmInvoice(){
		Savepoint sp = Database.setSavepoint() ;
		try{
			confInv.Confirmed_By__c = UserInfo.getUserId();
			confInv.Confirmed_Date__c = System.now();
			
			if(confInv.Commission_Paid_Date__c == null)
				confInv.Commission_Paid_Date__c = system.today();
			update confInv;
				
				
			list<client_product_service__c> prods = new list<client_product_service__c>();

			for(client_product_service__c p : [SELECT Id FROM client_product_service__c WHERE Request_Commission__c = :confInv.Id]){
				p.Commission_Confirmed_On__c = system.now();
				p.Commission_Confirmed_By__c = UserInfo.getUserId();
				p.Commission_Paid_Date__c = confInv.Commission_Paid_Date__c;
				p.Commission_Confirmed_By_Agency__c = currentUser.Contact.AccountId;
				prods.add(p);
			}

			update prods;

		}
		catch(Exception e){
			Database.rollback(sp);
			system.debug('Error: ' + e.getMessage() + ' line--' + e.getLineNumber());
		}
	}

	// C A L C U L A T E	I N V O I C E
	public Decimal totalInvoice {get;set;}
	public Integer countProds {get{if(countProds == null) countProds = 0; return countProds;}set;}
	public String prodCurrency {get;set;}
	public Invoice__c commInvoice {get;set;}
	private list<client_product_service__c> upProds {get;set;}
	public Provider__c invProv {get;set;}
	public String prodIds {get;set;}
	public Boolean sendEmail {get;set;}
	public String emTo {get;set;}
	public String emCc {get;set;}
	public String emSubj {get;set;}
	public String emCont {get;set;}

	// S E T 		C O M M I S S I O N 		N O T 		C L A I M A B L E
	public void commNotClaimable(){
		list<client_product_service__c> toUpdate = new list<client_product_service__c>();

		for(client_product_service__c p : listProducts)
			if(p.isSelected__c){
				P.Claim_Commission_Type__c = 'Commission not claimable';
				P.Commission_Confirmed_By__c = Userinfo.getuserId();
				P.Commission_Confirmed_by_Agency__c = currentUser.Contact.AccountId;
				P.Commission_Confirmed_On__c = system.now();
				P.Commission_Paid_Date__c = system.today();

				p.isSelected__c = false;
				toUpdate.add(p);
			}

		update toUpdate;

		search();
	}
	

	// C A L C 		I N V O I C E
	public void calcInvoice(){
		sendEmail = false;

		countProds = 0;
		Decimal totalInvoice = 0;
		String prodCurrency;
		String providerId;
		prodIds = '';
		upProds = new list<client_product_service__c>();

		for(client_product_service__c p : listProducts)

			if(p.isSelected__c){
				totalInvoice += p.Commission_Value__c + p.Commission_Tax_Value__c;
				prodCurrency = p.Currency__c;
				providerId = p.Products_Services__r.Provider__c;
				prodIds += p.Id +';';
				countProds++;
				upProds.add(p);
			}

		//Create Invoice
		commInvoice = new Invoice__c (
			isProvider_Commission__c = true,
			Instalments__c = prodIds,
			Provider__c = providerId,
			Total_Value__c = totalInvoice,
			CurrencyIsoCode__c = prodCurrency,
			Due_Date__c = system.today().addDays(14), 
			Agency_Currency__c = mapAgCurrency.get(selectedAgency),
			Requested_by_Agency__c = currentUser.Contact.AccountId,
			Requested_Commission_for_Agency__c = agencyComm,
			Requested_Commission_for_Agency_Group__c = groupComm
		);


		invProv = [SELECT Registration_Name__c, Type_of_Business_Number__c, Business_Number__c, Invoice_Street__c, Invoice_City__c, Invoice_State__c, Invoice_PostalCode__c, Invoice_Country__c, Invoice_to_Email__c FROM Provider__c WHERE ID = :providerId limit 1];

		emTo = invProv.Invoice_to_Email__c;
		emCc = null;
		objTemplate = (sObject) invProv;
		if(emailOptions != null && emailOptions.size()>0){
			selectedTemplate = emailOptions[0].getValue();
			changeTemplate();
			emCont = tempBody;
		}
		else{
			emSubj = '';
			emCont = '';
		}

	}

	// C R E A T E		I N V O I C E
	public String agencyComm {get;set;}
	public String groupComm {get;set;}
	public void createInvoice(){
	
		Savepoint sp = Database.setSavepoint();
		try{
			//Insert Invoice
			commInvoice.Sent_Email_To__c = emTo;
			commInvoice.Sent_On__c = system.now();
			commInvoice.Total_Emails_Sent__c = 1;

			Datetime GMT = system.now();
			String editDate = GMT.format('yyyy-MM-dd HH:mm:ss', 'UTC');
			commInvoice.Invoice_Activities__c = JSON.serialize(new list<IPFunctions.activityWrapper>{new IPFunctions.activityWrapper('1', 'Sent Invoice', emTo, currentUser.Contact.Name, currentUser.Contact.Account.Name, 'Request Commission Invoice', editDate)}); 
			noS.saveInvoice(commInvoice, currentUser);

			//Update Products
			for(client_product_service__c p : upProds){
				p.Request_Commission__c = commInvoice.Id;
				p.isSelected__c = false;
			}

			update upProds;
			sendEmail = true;
		}
		catch(Exception e){
			Database.rollback(sp);
			system.debug('Error: ' + e.getMessage() + ' line--' + e.getLineNumber());
		}
	}


	// R E T R I E V E		I N V O I C E		F O R 		E M A I L
	public void setupResendEmail(){

		commInvoice = [SELECT Request_Provider_Commission__c, Sent_Email_To__c, Total_Emails_Sent__c,Invoice_Activities__c FROM Invoice__c WHERE ID = :ApexPages.CurrentPage().getParameters().get('inv')];

		emTo = commInvoice.Sent_Email_To__c;
		emCc = null;
		// emSubj = commInvoice.Request_Provider_Commission__c + ' - Product Commission Reminder';
		// emCont = '<br/>' + getSignature();

		objTemplate = (sObject) invProv;

		if(emailOptions != null && emailOptions.size()>0){
			selectedTemplate = emailOptions[0].getValue();
			changeTemplate();
			emCont = tempBody;
		}
		else{
			emSubj = '';
			emCont = '';
		}
	}

	// S E N D		E M A I L
	public boolean showMsg {get;set;}
	public void sendEmail(){
		//Send Email
		String emailService = IpFunctions.getS3EmailService();
		blob pdfFile = null;
		mandrillSendEmail mse = new mandrillSendEmail();
		mandrillSendEmail.messageJson email_md = new mandrillSendEmail.messageJson();

		email_md.setFromEmail(currentUser.Contact.Email);
		email_md.setFromName(currentUser.Contact.Name);
		list<String> toAd;
		list<String> toCc;

		if(emTo.contains(';')){
			toAd = emTo.split(';', 0);
			for(String t : toAd)
				if(!String.isBlank(t))
					email_md.setTo(t, '');
		}
		else if(emTo.contains(',')){
			toAd = emTo.split(',', 0);
			for(String t : toAd)
				if(!String.isBlank(t))
					email_md.setTo(t, '');
		}else{
			email_md.setTo(emTo, '');
		}

		if (emCc != null && emCc != ''){
			emCc = emCc.replaceAll(',',';').replaceAll('\n',';');
			toCc = emCc.split(';', 0);
			for(String c_c : toCc) 
				email_md.setCc(c_c, '');
		}

		Invoice__c inv;
		if(commInvoice.Request_Provider_Commission__c == null){
		 	inv = [Select Request_Provider_Commission__c, Total_Emails_Sent__c, Invoice_Activities__c from Invoice__c where Id = :commInvoice.id];
		}
		else
			inv = commInvoice;

		//Re-send email functionality only
		if(sendEmail == null || !sendEmail){
			inv.Sent_Email_To__c = emTo;
			inv.Sent_on__c = system.now();
			inv.Total_Emails_Sent__c ++;

			
			Datetime GMT = system.now();
			String editDate = GMT.format('yyyy-MM-dd HH:mm:ss', 'UTC');
			list<IPFunctions.activityWrapper> lAct = new list<IPFunctions.activityWrapper>();
			
			if(inv.Invoice_Activities__c != null)
			 	lAct = (list<IPFunctions.activityWrapper>) JSON.deserialize(inv.Invoice_Activities__c, list<IPFunctions.activityWrapper>.class);

			lAct.add(new IPFunctions.activityWrapper(string.valueOf(lAct.size() + 1),'Re-sent Email', emTo, currentUser.Contact.Name, currentUser.Contact.Account.Name, emSubj, editDate));	
			inv.Invoice_Activities__c = JSON.serialize(lAct); 
			update inv;
		}


		//Send a copy to the sender
		email_md.setCc(currentUser.Contact.Email, '');

		if(sendEmail)
			email_md.setSubject(inv.Request_Provider_Commission__c + ' - ' + emSubj);
		else
			email_md.setSubject(emSubj);

		email_md.preserve_recipients(true);

		emCont = emCont.replace('<style type="text/css">', '<head><style>').replace('</style>', '</style></head>');
		email_md.setHtml(emCont);

		PageReference pr = Page.product_commission_invoice;
		pr.getParameters().put('id', inv.Id);
		if(!Test.isRunningTest()){
			pdfFile = pr.getContentAsPDF();
			email_md.setAttachment('application/pdf', inv.Request_Provider_Commission__c + ' - Products Invoice.pdf', pdfFile);
		}

		if(!Test.isRunningTest())
			mse.sendMail(email_md);//send email

		//TODO: SAVE ON AMAZON S3
		showMsg = true;
		sendEmail = false;
	}

	// C A N C E L 		I N V O I C E
		// Set Id to Cancel
	public Invoice__c cancelInv {get{if(cancelInv == null) cancelInv = new Invoice__c(); return cancelInv;}set;}
	public void setIdtoCancel(){
		cancelInv = [SELECT ID, Provider__r.Provider_name__c,Request_Provider_Commission__c FROM Invoice__c WHERE Id = :ApexPages.currentPage().getParameters().get('inv') limit 1];
		cancelInv.isCancelled__c = true;
		cancelInv.Cancelled_by__c = UserInfo.getUserId();

		ApexPages.currentPage().getParameters().remove('inv');
	}

		// Cancel Invoice
	public void cancelInvoice(){
		noS.toCancelInvoice(cancelInv, currentUser);
		saveCancelledToS3(cancelInv.Id, cancelInv.Request_Provider_Commission__c, cancelInv.Provider__r.Provider_name__c, currentUser.Contact.Account.Global_Link__c, currentUser.Contact.AccountId);
	}
		// Save on S3

	@Future(callout = true)
	private static void saveCancelledToS3(String invId, String invNumber, String pName,  String gbLink, String agId){

		system.debug('invUpdate==>' + invId);
		system.debug('invUpdate==>' + invNumber);

		PageReference pr = Page.product_commission_invoice;
		pr.getParameters().put('id', invId);
		Blob pdfFile;
		if(!Test.isRunningTest())
			pdfFile = pr.getContentAsPDF();

		String fileName = gbLink + '/'+ agId + '/Commissions/RequestedProvider/Cancelled/';
		fileName += invNumber + ';#;' + pName;

		S3Controller cS3 = new S3Controller();
		cS3.constructor(); //Generate Credentials
		List<String> allbuck= cS3.allBuckets;

		cS3.insertFileS3(pdfFile, Userinfo.getOrganizationId(), fileName, 'application/pdf');

		Database.delete(invId);
	}


	// N O 		S H A R I N G 		M E T H O D S
	private noSharing noS = new noSharing();
	private without sharing class noSharing{

		public void saveInvoice(Invoice__c commInvoice, User currentUser){

			commInvoice.Finance_Global_Link__c = currentUser.Contact.Account.Global_Link__c;
			insert commInvoice;
		}

		public void toCancelInvoice(Invoice__c invCancelled, User currentUser){
			Savepoint sp = Database.setSavepoint() ;
			try{
				Invoice__c invAct = [SELECT Invoice_Activities__c FROM Invoice__c WHERE ID = :invCancelled.Id limit 1];

				Datetime GMT = system.now();
				String editDate = GMT.format('yyyy-MM-dd HH:mm:ss', 'UTC');
				list<IPFunctions.activityWrapper> lAct = new list<IPFunctions.activityWrapper>();
			
				if(invAct.Invoice_Activities__c != null)
					lAct = (list<IPFunctions.activityWrapper>) JSON.deserialize(invAct.Invoice_Activities__c, list<IPFunctions.activityWrapper>.class);

				lAct.add(new IPFunctions.activityWrapper(string.valueOf(lAct.size() + 1),'Invoice Cancelled', '-', currentUser.Contact.Name, currentUser.Contact.Account.Name, invCancelled.Cancel_Reason__c, editDate));	
				invCancelled.Invoice_Activities__c = JSON.serialize(lAct); 

				update invCancelled;

				list<client_product_service__c> prods = new list<client_product_service__c>();

				for(client_product_service__c p : [SELECT Id FROM client_product_service__c WHERE Request_Commission__c = :invCancelled.Id]){
					p.Request_Commission__c = null;
					prods.add(p);
				}

				update prods;
			}
			catch(Exception e){
				Database.rollback(sp);
				system.debug('Error: ' + e.getMessage() + ' line--' + e.getLineNumber());
			}
		}
	}


	/** Signature **/
	public string getSignature(){
	    if(currentUser.Contact.Signature__c != null && currentUser.Contact.Signature__c != '' )
	      return currentUser.Contact.Signature__c;
	    else return textSignature;
	}

	public String textSignature {
	  	get{
	  		if(textSignature == null){
	  			textSignature = '<div style="margin-top: 20px;" class="sig">' + currentUser.Contact.Name;
	            textSignature += '<br />' + currentUser.Contact.Account.Name;
	            textSignature += '<br />' + currentUser.Contact.Email + ' </div>';
	  		}
	  		return textSignature;
	  	}set;}


	// ---------------------------------------------------	FILTERS


	// Agency Group Options
	public string selectedAgencyGroup{
    	get{
    		if(selectedAgencyGroup == null)
    			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
    		return selectedAgencyGroup;
    	}set;}
	public List<SelectOption> agencyGroupOptions {
  		get{
   			if(agencyGroupOptions == null){
   				
   				agencyGroupOptions = new List<SelectOption>();  
   				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group  
	    			for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:currentUser.Contact.AccountId order by Parent.Name]){
		     			agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
		    		}
   				}else{
   					for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
		     			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
		    		}
   				}
	   		}
	   		return agencyGroupOptions;
	  }set;}

	public void changeGroup(){
		agencyOptions = null;
	}

	// Agency Options
	public String selectedAgency {get{if(selectedAgency == null) selectedAgency = currentUser.Contact.AccountId; return selectedAgency;}set;}
	public map<String,String> mapAgCurrency {get;set;}
	public list<SelectOption> agencyOptions {get{
		if(agencyOptions == null){
			mapAgCurrency = new map<String,String>();
			agencyOptions = new List<SelectOption>();
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
				for(Account a: [Select Id, Name, Account_Currency_Iso_Code__c from Account where id =:currentUser.Contact.AccountId order by name]){
					if(selectedAgency == null)
						selectedAgency = a.Id;					
					agencyOptions.add(new SelectOption(a.Id, a.Name));
					mapAgCurrency.put(a.Id, a.Account_Currency_Iso_Code__c);
				}
				if(currentUser.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
						agencyOptions.add(new SelectOption(ac.id, ac.name));
						mapAgCurrency.put(ac.Id, ac.Account_Currency_Iso_Code__c);
					}
				}
			}else{
				for(Account a: [Select Id, Name, Account_Currency_Iso_Code__c from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
					agencyOptions.add(new SelectOption(a.Id, a.Name));
					mapAgCurrency.put(a.Id, a.Account_Currency_Iso_Code__c);
					if(selectedAgency == null)
						selectedAgency = a.Id;	
				}
			}

			findProviders();
		}

		return agencyOptions;
		
	}set;}

	public void changeAgency(){
		findProviders();
	}


	//Providers
	public string selectedProvider{
    	get{
    		if(selectedProvider == null)
    			selectedProvider = 'none';
    		return selectedProvider;
    	}set;}
	public List<SelectOption> providersOpt {get;set;}
	public void findProviders(){
		providersOpt = new List<SelectOption>();
		providersOpt.add(new SelectOption('none','-- Select Provider --'));

		for(AggregateResult ag : [SELECT Products_Services__r.Provider__c pId, Products_Services__r.Provider__r.Provider_Name__c pName FROM client_product_service__c WHERE Paid_to_Provider_by_Agency__c = :selectedAgency AND Commission_Confirmed_On__c = NULL AND Claim_Commission_Type__c = 'Paid in full' group by Products_Services__r.Provider__c, Products_Services__r.Provider__r.Provider_Name__c]){
			if((String) ag.get('pId') != null)
				providersOpt.add(new SelectOption((String) ag.get('pId'), (String) ag.get('pName')));
		}//end for

		selectedProvider = 'none';
		findCategories();
	}

	public void changeProvider(){
		findCategories();
	}


	//Providers Category
	public string selectedCategory{get;set;}

	public List<SelectOption> categoriesOpt {get;set;}
	public void findCategories(){
		categoriesOpt = new List<SelectOption>();

		if(selectedProvider != 'none' && selectedProvider != 'all'){
			categoriesOpt.add(new SelectOption('all','-- All --'));
			for(AggregateResult ag : [SELECT Products_Services__r.Category__c cat FROM client_product_service__c WHERE Paid_to_Provider_by_Agency__c = :selectedAgency AND Commission_Confirmed_On__c = NULL AND Claim_Commission_Type__c = 'Paid in full' group by Products_Services__r.Category__c]){
				categoriesOpt.add(new SelectOption((String) ag.get('cat'), (String) ag.get('cat')));
			}//end for

		}
		else{
			categoriesOpt.add(new SelectOption('none','-- Select Provider --'));
		}

		selectedCategory = 'all';

		if(selectedProvider != 'none')
			findCatProducts();
		else{
			agencyProducts = new List<SelectOption>();
			agencyProducts.add(new SelectOption('none','-- Select Provider --'));
		}
	}

	public void changeCategory(){
		findCatProducts();
	}


	// Products
	public string selectedProduct{get;set;}
	public List<SelectOption> agencyProducts {get;set;}

	public void findCatProducts(){
		agencyProducts = new List<SelectOption>();
		agencyProducts.add(new SelectOption('all','-- All --'));

		String sql = 'SELECT Products_Services__c pId, Product_Name__c nm FROM client_product_service__c WHERE Paid_to_Provider_by_Agency__c = \'' + selectedAgency  + '\' ';

		if(selectedProvider != 'none' && selectedProvider != 'all')
			sql += ' AND Products_Services__r.Provider__c = \'' + selectedProvider + '\' ';

		if(selectedCategory != 'none' && selectedCategory != 'all')
			sql += ' AND ( Products_Services__r.Category__c = \'' + selectedCategory + '\' OR Products_Services__r.Category__c = NULL)';

		sql += '  AND Commission_Confirmed_On__c = NULL AND Claim_Commission_Type__c = \'Paid in full\'  group by Products_Services__c, Product_Name__c ';

		system.debug('products sql ==> ' + sql);

		for(AggregateResult ag : Database.query(sql)){
			agencyProducts.add(new SelectOption((String) ag.get('pId'), (String) ag.get('nm')));
		}//end for

		selectedProduct = 'all';
	}

	//Invoice Due Date
	public Invoice__c dates {get{if(dates == null) dates = new Invoice__c(Due_Date__c = system.today(), Commission_Paid_Date__c = system.today().addDays(14)); return dates;}set;}


	//Email Templates
	public String selectedTemplate {get;set;}
	public list<SelectOption> emailOptions {get{if(emailOptions == null) findEmailTemplates(); return emailOptions;}set;}
	public map<Id, Email_Template__c> mEmailTemplate {get;set;}

	public String tempBody {get;set;}
	private String paymentRef;
	private sObject objTemplate;
	private Back_Office_Control__c userBo;
	private list<string> fieldsTemplate;
	private map<string, Object> mapFields;
	private map<Id,String> mapSubject;

	private void findEmailTemplates(){

		mapFields = new map<string, Object>();
		fieldsTemplate = new list<String>(mapFields.keySet());

		emailOptions = new list<SelectOption>();
		mEmailTemplate = new map<Id, Email_Template__c>();
		mapSubject = new map<Id, String>();
		String mSubject;

		for(Email_Template__c et : [SELECT Email_Subject__c, Template__c, Template_Description__c FROM Email_Template__c WHERE Agency__c = :currentUser.Contact.AccountId and Category__c = 'Products Request Commission' order by Email_Subject__c]){
			mSubject = emailTemplateDynamic.createSingleTemplate(mapFields, et.Email_Subject__c, fieldsTemplate, objTemplate);
			mapSubject.put(et.id, mSubject);
			emailOptions.add(new SelectOption(et.Id, et.Template_Description__c));
			mEmailTemplate.put(et.id, et);
		}//end for
	}

	public void changeTemplate(){

		emSubj =  mapSubject.get(selectedTemplate);
		tempBody = mEmailTemplate.get(selectedTemplate).Template__c;
		tempBody = emailTemplateDynamic.createSingleTemplate(mapFields, tempBody, fieldsTemplate, objTemplate);
		tempBody += getSignature();

	}
}