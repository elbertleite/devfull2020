/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class report_leads_test {
	
	static testMethod void myUnitTest() {
		
		TestFactory tf = new TestFactory();
		Account agency = tf.createAgency();		
		Contact employee = tf.createEmployee(agency);
		User portalUser = tf.createPortalUser(employee);
		employee.Global_Manager__c = true;
		update employee;
		String recordTypeClient = [Select id from RecordType where Name = 'Client' and isActive = true LIMIT 1].id;
		
		Test.startTest();
		system.runAs(portalUser){
			
			Contact lead = tf.createLead(agency, employee);
			Date dToday = System.Today().addDays(2);
			Datetime dt = datetime.newInstance(dToday.year(), dToday.month(),dToday.day());
			lead.Lead_Converted_On__c = dt;
			lead.Lead_Converted_By__c = employee.id;
			lead.RecordTypeId = recordTypeClient;
			lead.Lead_Assignment_To__c = employee.id;
			lead.Lead_First_Contact__c = 'Phone';
			lead.Lead_Source_Specific__c = 'ABC Seminar';			
			update lead;			



			ApexPages.currentPage().getParameters().put('type', 'source');
			report_leads rl = new report_leads();
			rl.checkUserAccess();
			rl.getAgencyGroupOptions();
			rl.getAgencyOptions();
			
			rl.range.arrival_date__c = system.today().addMonths(-6);
			rl.range.expected_travel_date__c = system.today().addMonths(1);
			
			rl.search();
			
			rl.generateExcel();
			rl.excelSearch();
			
			Map<Integer, String> monthNames = rl.monthNames;
			rl.refreshAgencies();
			
			ApexPages.currentPage().getParameters().put('type', 'firstContact');
			rl = new report_leads();
			rl.range.arrival_date__c = system.today().addMonths(-6);
			rl.range.expected_travel_date__c = system.today().addMonths(1);
			rl.search();
		}
		Test.stopTest();
		
	}
    
}