public with sharing class pds_pfs_track {
    
    
    
    public pds_pfs_track(){
    	selectedAgency = currentUser.Contact.AccountId;
    	
    	//listPDS = new list<client_course_instalment__c>();
    	//listPFS = new list<client_course_instalment__c>();
    	
    	searchInstalments();
    }
    
    
    public transient list<client_course_instalment__c> listPDS {get;set;}
    public transient list<client_course_instalment__c> listPFS {get;set;}
    public transient list<client_course_instalment__c> listPCS {get;set;}
	public sumTotals listPDSTotal{get{if(listPDSTotal == null) listPDSTotal = new sumTotals(); return listPDSTotal;} set;}    
	public sumTotals listPFSTotal{get{if(listPFSTotal == null) listPFSTotal = new sumTotals(); return listPFSTotal;} set;}
	public sumTotals listPCSTotal{get{if(listPCSTotal == null) listPCSTotal = new sumTotals(); return listPCSTotal;} set;}
	
	
	
	
    public void searchInstalments(){
    	
    	listPDS = new list<client_course_instalment__c>();
    	listPFS = new list<client_course_instalment__c>();
    	listPCS = new list<client_course_instalment__c>();
    	
    	listPDSTotal = new sumTotals();
    	listPFSTotal = new sumTotals();
    	listPCSTotal = new sumTotals();
    	
    	String  fromDate = IPFunctions.FormatSqlDateIni(dateFilter.Original_Due_Date__c); 
		String  toDate = IPFunctions.FormatSqlDateFin(dateFilter.Discounted_On__c);
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && currentUser.Aditional_Agency_Managment__c == null){ //set the user to see only his own agency
			selectedAgency = currentUser.Contact.AccountId;
			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
		}
		
		String sql = 'SELECT Balance__c, Commission__c, Commission_Confirmed_On__c, Commission_Tax_Rate__c, Commission_Tax_Value__c, Commission_Value__c, Confirmed_Date__c, Discount__c, isFirstPayment__c, isPDS__c, isPCS__c, isPFS__c, ' +
					' Discounted_By__c, Discounted_On__c, Due_Date__c, Extra_Fee_Discount_Value__c, Extra_Fee_Value__c, Instalment_Value__c, Kepp_Fee__c, Number__c, Paid_To_School_By__c, Paid_To_School_On__c, ' +
					' Scholarship_Taken__c, Splited_From_Number__c, Status__c, Total_School_Payment__c, Tuition_Value__c, Received_Date__c, isSharedCommission__c, Received_By_Agency__r.name,  Received_by__r.name, ' +
					' Shared_Comm_Agency_Enroll__c, Shared_Comm_Agency_Enroll_Value__c, Shared_Comm_Agency_Receive__c, Shared_Comm_Agency_Receive_Value__c, Received_by_Department__c, Received_By_Department__r.name, ' +
					' PDS_Requested_On__c, PDS_Requested_By__r.Name, PDS_Requested_By__r.contact.account.name, PDS_Requested_By__r.contact.department__r.name, PDS_Note__c, ' +
					' Sent_Email_Receipt_On__c, Sent_Email_Receipt_By__r.Name, Sent_Email_Receipt_By__r.contact.account.name, Sent_Email_Receipt_By__r.contact.department__r.name, Commission_Due_Date__c, ' +
					' Commission_Paid_Date__c, Commission_Confirmed_By__r.Name, Commission_Confirmed_By_Agency__r.Name,  Commission_Confirmed_By__r.contact.department__r.name, '+					
					
					' Invoice__r.Sent_On__c, Invoice__r.Sent_By__r.Name, Invoice__r.Sent_By__r.contact.account.name, Invoice__r.Sent_By__r.contact.department__r.name, Invoice__r.Due_Date__c, ' + 
					
					' client_course__r.Client_Type__c, ' + 
					' client_course__r.Agency_Admin_Fee__c, ' +
					' client_course__r.Campus_Name__c, ' +
					' client_course__r.Client__r.name, ' +
					' client_course__r.Commission_Tax_Rate__c, ' +
					' client_course__r.Commission_Tax_Value__c, ' +
					' client_course__r.Commission_Type__c, ' +
					' client_course__r.Course_Name__c, ' +
					' client_course__r.Enroled_by_Agency__r.name,  ' +
					' client_course__r.Enroled_by_User__r.name,  ' +
					' client_course__r.Enrolment_Date__c,  ' +
					' client_course__r.Course_Length__c,  ' +
					' client_course__r.course_package__c,  ' +
					' client_course__r.Free_Units__c, ' +
					' client_course__r.Free_Weeks__c,  ' +
					' client_course__r.Unit_Type__c, ' +
					' client_course__r.Campus_Course__r.Course__r.Course_Type__c,  ' +
					' client_course__r.Client__r.owner__r.contact.account.name,  ' +
					' client_course__r.Client__r.owner__r.name,  ' +
					' client_course__r.Client__r.owner__r.contact.department__r.name ';
		
		sql+= ' FROM client_course_instalment__c  ';
		
		String sqlWhere = ' WHERE (isPDS__c = true OR isPFS__c = true OR isPCS__c = true) AND ((client_course__r.Enroled_by_Agency__c = :selectedAgency AND Received_By_Agency__c != :selectedAgency) OR (Received_By_Agency__c = :selectedAgency AND client_course__r.Enroled_by_Agency__c != :selectedAgency) '+
					' OR (Received_By_Agency__c = :selectedAgency AND client_course__r.Enroled_by_Agency__c = :selectedAgency))  ';
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
		 	sqlWhere+= ' AND Received_By_Agency__r.ParentId = :selectedAgencyGroup ';
		 	
	 	if(SelectedPeriod == 'range')
			sqlWhere += ' AND DAY_ONLY(convertTimezone(Received_Date__c)) >= '+ fromDate +' and  DAY_ONLY(convertTimezone(Received_Date__c)) <= '+toDate;
		else if(SelectedPeriod == 'THIS_WEEK')
			sqlWhere += ' AND Received_Date__c = THIS_WEEK ';
		else if(SelectedPeriod == 'LAST_WEEK')
			sqlWhere += ' AND Received_Date__c = LAST_WEEK ';
		else if(SelectedPeriod == 'NEXT_WEEK')
			sqlWhere += ' AND Received_Date__c = NEXT_WEEK ';
		else if(SelectedPeriod == 'THIS_MONTH')
			sqlWhere += ' AND Received_Date__c = THIS_MONTH ';
		else if(SelectedPeriod == 'LAST_MONTH')
			sqlWhere += ' AND Received_Date__c = LAST_MONTH ';
		else if(SelectedPeriod == 'NEXT_MONTH')
			sqlWhere += ' AND Received_Date__c = NEXT_MONTH ';
		else if(SelectedPeriod == 'unconfirmed')
			sqlWhere += ' AND Received_Date__c != NULL AND Commission_Confirmed_On__c = NULL ';
			
			
		if(selectedDepartment != 'all')
			sqlWhere+= ' AND Received_by_Department__c = :selectedDepartment '; 
		
		if(SelectedPaymentType == 'first')
			sqlWhere+= ' AND isFirstPayment__c = true '; 	
		else if(SelectedPaymentType == 'repay')
			sqlWhere+= ' AND isFirstPayment__c = false '; 	
		
		sqlWhere += ' AND isMigrated__c = false ';	
			
		sqlWhere += ' ORDER BY Received_Date__c ';
			
		sql += sqlWhere;
		
		system.debug('sql===>' + sql);
		
		for(client_course_instalment__c lc : Database.Query(sql)){
			if(lc.isPDS__c){
				listPDSTotal.calculateTotals(lc.Tuition_Value__c, lc.Commission_Paid_Date__c!=null ? 0 : 1,  lc.Commission_Paid_Date__c!=null ? 1 : 0, lc.Extra_Fee_Value__c, lc.Discount__c, lc.Instalment_Value__c, lc.Commission_Value__c, lc.Commission_Value__c - lc.Discount__c, lc.Commission_Tax_Value__c, lc.Shared_Comm_Agency_Enroll_Value__c, lc.Shared_Comm_Agency_Receive_Value__c);
				listPDS.add(lc);
			}
			else if(lc.isPFS__c){
				listPFSTotal.calculateTotals(lc.Tuition_Value__c, lc.Commission_Paid_Date__c!=null ? 0 : 1,  lc.Commission_Paid_Date__c!=null ? 1 : 0, lc.Extra_Fee_Value__c, lc.Discount__c, lc.Instalment_Value__c, lc.Commission_Value__c, lc.Commission_Value__c - lc.Discount__c, lc.Commission_Tax_Value__c, lc.Shared_Comm_Agency_Enroll_Value__c, lc.Shared_Comm_Agency_Receive_Value__c);
				listPFS.add(lc);
			}else if(lc.isPCS__c){
				listPCSTotal.calculateTotals(lc.Tuition_Value__c, lc.Commission_Paid_Date__c!=null ? 0 : 1,  lc.Commission_Paid_Date__c!=null ? 1 : 0, lc.Extra_Fee_Value__c, lc.Discount__c, lc.Instalment_Value__c, lc.Commission_Value__c, lc.Commission_Value__c - lc.Discount__c, lc.Commission_Tax_Value__c, lc.Shared_Comm_Agency_Enroll_Value__c, lc.Shared_Comm_Agency_Receive_Value__c);
				listPCS.add(lc);
			}
			
		}//end for
		
    }
    
    
    public void searchExcel(){
		searchInstalments();
    }
	
	
	public PageReference generateExcel(){
		PageReference pr = Page.pds_pfs_track_excel;
		pr.setRedirect(false);
		return pr;		
	}
	
	public String xlsHeader {
        get {
            String strHeader = '';
            strHeader += '<?xml version="1.0"?>';
            strHeader += '<?mso-application progid="Excel.Sheet"?>';
            return strHeader;
        }
    }
    
    
    
	public class sumTotals{
		
		public integer totItens{get{if(totItens == null) totItens = 0; return totItens;} set;}
		public decimal totPending{get{if(totPending == null) totPending = 0; return totPending;} set;}
		public decimal totConfirmed{get{if(totConfirmed == null) totConfirmed = 0; return totConfirmed;} set;}
		public decimal tuition{get{if(tuition == null) tuition = 0; return tuition;} set;}
		public decimal extraFee{get{if(extraFee == null) extraFee = 0; return extraFee;} set;}
		public decimal discount{get{if(discount == null) discount = 0; return discount;} set;}
		public decimal instalment{get{if(instalment == null) instalment = 0; return instalment;} set;}
		public decimal commission{get{if(commission == null) commission = 0; return commission;} set;}
		public decimal commissionDiscount{get{if(commissionDiscount == null) commissionDiscount = 0; return commissionDiscount;} set;}
		public decimal tax{get{if(tax == null) tax = 0; return tax;} set;}
		public decimal enrollAgency{get{if(enrollAgency == null) enrollAgency = 0; return enrollAgency;} set;}
		public decimal receiveAgency{get{if(receiveAgency == null) receiveAgency = 0; return receiveAgency;} set;}
		public void calculateTotals(decimal tuition,decimal totPending,decimal totConfirmed, decimal extraFee, decimal discount, decimal instalment, decimal commission, decimal commissionDiscount, decimal tax, decimal enrollAgency, decimal receiveAgency){
			this.totItens++;
			this.tuition += tuition;
			this.totPending += totPending;
			this.totConfirmed += totConfirmed;
			this.extraFee += extraFee;
			this.discount += discount;
			this.instalment += instalment;
			this.commission += commission;
			this.commissionDiscount += commissionDiscount;
			this.tax += tax;
			this.enrollAgency += enrollAgency;
			this.receiveAgency += receiveAgency;
		}
	}
    
    
    
    
    private User currentUser {get{
    	if(currentUser==null){
    		currentUser = [Select Id, Name, Aditional_Agency_Managment__c, Contact.Account.ParentId, Contact.AccountId, Contact.Department__c from User where id = :UserInfo.getUserId() limit 1];
    	}
    	return currentUser;
    }set;}
    
    
    public string selectedAgencyGroup{
    	get{
    		if(selectedAgencyGroup == null)
    			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
    		return selectedAgencyGroup;
    	}  
    	set;
    }
    
   public List<SelectOption> agencyGroupOptions {
  		get{
   			if(agencyGroupOptions == null){
   				
   				agencyGroupOptions = new List<SelectOption>();  
   				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group  
	    			for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:currentUser.Contact.AccountId order by Parent.Name]){
		     			agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
		    		}
   				}else{
   					for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
		     			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
		    		}
   				}
	   		}
	   		return agencyGroupOptions;
	  }
	  set;
 	}
 	
 	
 	public String selectedAgency {get;set;}
	public List<SelectOption> agencyOptions {
		get{
	   		if(selectedAgencyGroup != null){
	   			agencyOptions = new List<SelectOption>();
				if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
					for(Account a: [Select Id, Name from Account where id =:currentUser.Contact.AccountId order by name]){
						if(selectedAgency=='')
							selectedAgency = a.Id;					
						agencyOptions.add(new SelectOption(a.Id, a.Name));
					}
					if(currentUser.Aditional_Agency_Managment__c != null){
						for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
							agencyOptions.add(new SelectOption(ac.id, ac.name));
						}
					}
				}else{
					for(Account a: [Select Id, Name from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
						agencyOptions.add(new SelectOption(a.Id, a.Name));
						if(selectedAgency=='')
							selectedAgency = a.Id;	
					}
				}
			}
		   	return agencyOptions;
	  	}
	  	set;
	}
    
    
    public string selectedDepartment {get{if(selectedDepartment==null) selectedDepartment = 'all'; return selectedDepartment;}set;}
	private set<Id> allDepartments {get;set;}
	public list<SelectOption> getDepartments(){
		List<SelectOption> result = new List<SelectOption>();
		allDepartments = new set<Id>();
		result.add(new SelectOption('all', '-- All --'));
		if(selectedAgency!=null)
			for(Department__c d: [Select Id, Name from Department__c where Agency__c = :selectedAgency and Inactive__c = false]){
				result.add(new SelectOption(d.Id, d.Name));
				allDepartments.add(d.Id);
			}
		
		return result;
	}
	
	
	public string SelectedPaymentType{get {if(SelectedPaymentType == null) SelectedPaymentType = 'all'; return SelectedPaymentType; } set;}
    private List<SelectOption> commissionType;
    public List<SelectOption> getcommissionType() {
        if(commissionType == null){
            commissionType = new List<SelectOption>();
            commissionType.add(new SelectOption('all','-- All --'));
            commissionType.add(new SelectOption('first','First Payment'));
            commissionType.add(new SelectOption('repay','Repayments'));
        }
        return commissionType;
    }
    
    public string SelectedPeriod{get {if(SelectedPeriod == null) SelectedPeriod = 'unconfirmed'; return SelectedPeriod; } set;}
    private List<SelectOption> periods;
    public List<SelectOption> getPeriods() {
        if(periods == null){
            periods = new List<SelectOption>();
             periods.add(new SelectOption('unconfirmed','Not confirmed'));
            periods.add(new SelectOption('THIS_WEEK','This Week'));
            periods.add(new SelectOption('LAST_WEEK','Last Week'));
            periods.add(new SelectOption('THIS_MONTH','This Month'));
            periods.add(new SelectOption('LAST_MONTH','Last Month'));
            periods.add(new SelectOption('range','Range'));
        }
        return periods;
    }
	
	public client_course_instalment__c dateFilter{
		get{
			if(dateFilter == null){
				dateFilter = new client_course_instalment__c(); 
				Date myDate = Date.today();
				Date weekStart = myDate.toStartofWeek();
				dateFilter.Original_Due_Date__c = weekStart;
				dateFilter.Discounted_On__c = dateFilter.Original_Due_Date__c.addDays(6);
			}
			return dateFilter;
		} 
		set;
	}
	
	
    
}