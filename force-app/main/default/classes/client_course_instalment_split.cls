public with sharing class client_course_instalment_split {

	public client_course_instalment__c selectedInstallment{get; set;}
	public client_course_instalment__c nextInstallment{get; set;}
	public client_course_instalment__c previousInstallment{get; set;}

	list<client_course_instalment__c> listInstalments;

	list<client_course_instalment__c> newInstalments = new  list<client_course_instalment__c>();

	public date maxLimitInstallment{get; set;}

	public date minLimitInstallment{get; set;}
	private string ccID;

	private boolean isPaid;

	public client_course_instalment_split(ApexPages.StandardController ctr){
		ccId = ctr.getId();
		listInstalments = [SELECT id, client_course__c, client_course__r.client__c, client_course__r.Course_Length__c, client_course__r.End_Date__c, client_course__r.Start_Date__c, client_course__r.Unit_Type__c, client_course__r.Client__r.name, Original_Due_Date__c, Splited_From_Number__c, Split_Number__c, client_course__r.Course_Name__c, Due_Date__c, Extra_Fee_Value__c, Instalment_Value__c, Number__c, Received_Date__c, Related_Fees__c, Tuition_Value__c, isRequestedShareCommission__c, Original_Extra_Fee_Value__c, Original_Instalment_Value__c, Original_Tuition_Value__c, Commission__c, Commission_Tax_Rate__c, Commission_Tax_Value__c, isPFS_Bulk__c, isPFS__c, isPDS__c, isPCS__c, Received_By_Agency__c, Received_by__c, Received_by_Department__c, pay_to_agency__c, Paid_To_School_By__c, Paid_to_School_By_Agency__c, Paid_to_School_Date__c, Paid_To_School_On__c, isMigrated__c, isPaidOffShore__c  FROM client_course_instalment__c
									WHERE client_course__c = :ccId AND isCancelled__c = false order by Number__c, Split_Number__c];
		System.debug('==>listInstalments: '+listInstalments);
		System.debug('==>itid: '+ApexPages.currentPage().getParameters().get('itid'));
		for(integer i = 0; i < listInstalments.size(); i++){
			if(listInstalments[i].id == ApexPages.currentPage().getParameters().get('itid')){
				selectedInstallment = listInstalments[i];
				openFeesDetails(listInstalments[i].Related_Fees__c);
				if(i > 0)
					previousInstallment = listInstalments[i-1];
			}else if(nextInstallment == null && selectedInstallment != null)
				 nextInstallment = listInstalments[i];
		}

		if(nextInstallment != null)
			maxLimitInstallment = nextInstallment.Due_Date__c-1;
		else
			maxLimitInstallment = selectedInstallment.client_course__r.End_Date__c;

		System.debug('==>previousInstallment: '+previousInstallment);
		if(previousInstallment != null)
			minLimitInstallment = previousInstallment.Due_Date__c+1;
		//else minLimitInstallment = System.today();

		if(selectedInstallment.Received_Date__c != null)
			isPaid = true;
		else
			isPaid = false;
	}


	public class feeInstallmentDetail{
		public string feeName{get; set;}
		public decimal feeValue{get; set;}
		public feeInstallmentDetail(string feeName, decimal feeValue){
			this.feeName = feeName;
			this.feeValue = feeValue;
		}
	}

	public list<feeInstallmentDetail> feesInstallment{get; set;}

	private void openFeesDetails(string relatedFees){
		if(feesInstallment == null && relatedFees != null && relatedFees != ''){
			feesInstallment = new list<feeInstallmentDetail>();
			for(string lf:relatedFees.split('!#'))
				feesInstallment.add(new feeInstallmentDetail(lf.split(';;')[1], decimal.valueOf(lf.split(';;')[2])));
		}
	}

	public list<instalmentValue> splitedInstalments{
		get{
			if(splitedInstalments == null){
				splitedInstalments = new list<instalmentValue>();
			}
			return splitedInstalments;
		}
		set;
	}


	public class instalmentValue{
		public integer instIndex{get; set;}
		public client_course_instalment__c ccInstall{get{if(ccInstall == null) ccInstall = new client_course_instalment__c(); return ccInstall;} set;}
		public instalmentValue(){}
		public instalmentValue(integer instIndex, client_course_instalment__c installment){
			this.instIndex = instIndex;
			this.ccInstall = installment;
		}
	}

	public instalmentValue newSplitedInstalment{
		get{
			if(newSplitedInstalment == null){
				newSplitedInstalment = new instalmentValue();
			}
			return newSplitedInstalment;
		}
		set;
	}

	public decimal totalSplited{
		get{
			if(totalSplited == null)
				totalSplited = 0;
			return totalSplited;
		}
		set;
	}

	private integer countNumberInstall = 0;

	private list<client_course_instalment__c> instalment_SplitUpdate = new list<client_course_instalment__c>();

	public pageReference addInstalmentValue(){
		System.debug('==>minLimitInstallment: '+minLimitInstallment);

		if(newSplitedInstalment.ccInstall.Tuition_Value__c < 0){
			newSplitedInstalment.ccInstall.Tuition_Value__c.addError('Value cannot be negative');
			return null;
		}

		if(newSplitedInstalment.ccInstall.Tuition_Value__c == 0 && selectedInstallment.Extra_Fee_Value__c == 0){
			newSplitedInstalment.ccInstall.Tuition_Value__c.addError('Tuition cannot be 0');
			return null;
		}

		if((newSplitedInstalment.ccInstall.Tuition_Value__c + totalSplited) > selectedInstallment.Tuition_Value__c){
			newSplitedInstalment.ccInstall.Tuition_Value__c.addError('Value cannot be bigger than: '+ (selectedInstallment.Tuition_Value__c - totalSplited));
			return null;
		}

		if(newSplitedInstalment.ccInstall.Due_Date__c > maxLimitInstallment){
			newSplitedInstalment.ccInstall.Due_Date__c.addError('Date cannot be after: '+ maxLimitInstallment.format());
			return null;
		}

		/*if(newSplitedInstalment.ccInstall.Due_Date__c < System.today()){
			newSplitedInstalment.ccInstall.Due_Date__c.addError('Date cannot be before today');
			return null;
		}*/

		if(minLimitInstallment != null && newSplitedInstalment.ccInstall.Due_Date__c < minLimitInstallment){
			newSplitedInstalment.ccInstall.Due_Date__c.addError('Date cannot be before: '+ minLimitInstallment.format());
			return null;
		}

		if(newSplitedInstalment.ccInstall.Tuition_Value__c == 0){
			boolean hasZero = false;
			for(instalmentValue ist:splitedInstalments){
				if(ist.ccInstall.Tuition_Value__c == 0)
					hasZero = true;
			}
			if(hasZero){
				newSplitedInstalment.ccInstall.Tuition_Value__c.addError('You cannot have more than 1 installment with 0');
				return null;
			}

		}

		integer positionAdd;
		boolean isSameDate = false;
		date currentPosition;
		//decimal intallmentNumber;
		for(integer i = 0; i < splitedInstalments.size(); i++){
			if(newSplitedInstalment.ccInstall.Due_Date__c < splitedInstalments[i].ccInstall.Due_Date__c && (currentPosition == null || splitedInstalments[i].ccInstall.Due_Date__c < currentPosition)){
				positionAdd = i;
				//intallmentNumber = splitedInstalments[i].ccInstall.Number__c;
				currentPosition = splitedInstalments[i].ccInstall.Due_Date__c;
			}
			if(splitedInstalments[i].ccInstall.Due_Date__c == newSplitedInstalment.ccInstall.Due_Date__c){
				splitedInstalments[i].ccInstall.Instalment_Value__c += newSplitedInstalment.ccInstall.Tuition_Value__c;
				splitedInstalments[i].ccInstall.Tuition_Value__c += newSplitedInstalment.ccInstall.Tuition_Value__c;
				/*if(positionAdd != 0 && selectedInstallment.Number__c != splitedInstalments[i].ccInstall.Number__c){
					splitedInstalments[i].ccInstall.Original_Instalment_Value__c += newSplitedInstalment.ccInstall.Tuition_Value__c;
					splitedInstalments[i].ccInstall.Original_Tuition_Value__c += newSplitedInstalment.ccInstall.Tuition_Value__c;
				}		*/
				isSameDate = true;
			}
		}

		if(!isSameDate){
			newSplitedInstalment.instIndex = countNumberInstall++;
			newSplitedInstalment.ccInstall.client_course__c = selectedInstallment.client_course__c;
			newSplitedInstalment.ccInstall.Tuition_Value__c = newSplitedInstalment.ccInstall.Tuition_Value__c;
			newSplitedInstalment.ccInstall.Splited_From_Number__c = selectedInstallment.Number__c;
			newSplitedInstalment.ccInstall.Splited_On__c = System.today();
			newSplitedInstalment.ccInstall.Splited_By__c = Userinfo.getUserId();
			newSplitedInstalment.ccInstall.Instalment_Value__c = newSplitedInstalment.ccInstall.Tuition_Value__c;

			newSplitedInstalment.ccInstall.Commission__c = selectedInstallment.Commission__c;
			newSplitedInstalment.ccInstall.Commission_Tax_Rate__c = selectedInstallment.Commission_Tax_Rate__c;

			newSplitedInstalment.ccInstall.Commission_Tax_Value__c = selectedInstallment.Commission__c * (selectedInstallment.Commission_Tax_Rate__c/100);

			newSplitedInstalment.ccInstall.isPFS_Bulk__c = selectedInstallment.isPFS_Bulk__c;
			newSplitedInstalment.ccInstall.isPFS__c = selectedInstallment.isPFS__c;
			newSplitedInstalment.ccInstall.isPDS__c = selectedInstallment.isPDS__c;
			newSplitedInstalment.ccInstall.isPCS__c = selectedInstallment.isPCS__c;
			newSplitedInstalment.ccInstall.isRequestedShareCommission__c = selectedInstallment.isRequestedShareCommission__c;

			newSplitedInstalment.ccInstall.Received_By_Agency__c = selectedInstallment.Received_By_Agency__c;
			newSplitedInstalment.ccInstall.Received_by__c = selectedInstallment.Received_by__c;
			newSplitedInstalment.ccInstall.Received_by_Department__c = selectedInstallment.Received_by_Department__c;
			newSplitedInstalment.ccInstall.Received_Date__c = selectedInstallment.Received_Date__c;
			newSplitedInstalment.ccInstall.pay_to_agency__c = selectedInstallment.pay_to_agency__c;

			newSplitedInstalment.ccInstall.Commission_Due_Date__c = newSplitedInstalment.ccInstall.Due_Date__c.addDays(7);

			newSplitedInstalment.ccInstall.Paid_To_School_By__c = selectedInstallment.Paid_To_School_By__c;
			newSplitedInstalment.ccInstall.Paid_to_School_By_Agency__c = selectedInstallment.Paid_to_School_By_Agency__c;
			newSplitedInstalment.ccInstall.Paid_to_School_Date__c = selectedInstallment.Paid_to_School_Date__c;
			newSplitedInstalment.ccInstall.Paid_To_School_On__c = selectedInstallment.Paid_To_School_On__c;
			newSplitedInstalment.ccInstall.isMigrated__c = selectedInstallment.isMigrated__c;
			newSplitedInstalment.ccInstall.isPaidOffShore__c = selectedInstallment.isPaidOffShore__c;


			//if(selectedInstallment.Number__c == newSplitedInstalment.ccInstall.Number__c)
			//	newSplitedInstalment.ccInstall.Original_Due_Date__c = selectedInstallment.Due_Date__c;

			if(splitedInstalments.size() == 0){
				newSplitedInstalment.ccInstall.Extra_Fee_Value__c = selectedInstallment.Extra_Fee_Value__c;
				newSplitedInstalment.ccInstall.Original_Extra_Fee_Value__c = selectedInstallment.Original_Extra_Fee_Value__c;
				newSplitedInstalment.ccInstall.Original_Instalment_Value__c = selectedInstallment.Original_Instalment_Value__c;
				newSplitedInstalment.ccInstall.Original_Tuition_Value__c = selectedInstallment.Original_Tuition_Value__c;
				newSplitedInstalment.ccInstall.Original_Due_Date__c = selectedInstallment.Original_Due_Date__c;
				newSplitedInstalment.ccInstall.Related_Fees__c = selectedInstallment.Related_Fees__c;
				newSplitedInstalment.ccInstall.Number__c = selectedInstallment.Number__c;
				if(selectedInstallment.Extra_Fee_Value__c != null)
					newSplitedInstalment.ccInstall.Instalment_Value__c += selectedInstallment.Extra_Fee_Value__c;

			}else{
				newSplitedInstalment.ccInstall.Number__c = selectedInstallment.Number__c;

				newSplitedInstalment.ccInstall.Original_Extra_Fee_Value__c = 0;
				newSplitedInstalment.ccInstall.Original_Instalment_Value__c = newSplitedInstalment.ccInstall.Tuition_Value__c;
				newSplitedInstalment.ccInstall.Original_Tuition_Value__c = newSplitedInstalment.ccInstall.Tuition_Value__c;
				newSplitedInstalment.ccInstall.Original_Due_Date__c = newSplitedInstalment.ccInstall.Due_Date__c;
			}

			System.debug('==>positionAdd: '+positionAdd);
			System.debug('==>splitedInstalments: '+splitedInstalments);
			System.debug('==>newSplitedInstalment: '+newSplitedInstalment);

			if(splitedInstalments.size() == 0 || positionAdd == null){
				newSplitedInstalment.ccInstall.Original_Due_Date__c = selectedInstallment.Due_Date__c;
				splitedInstalments.add(newSplitedInstalment);
			}else {
				if(positionAdd == 0 && splitedInstalments.size() > 0){
					newSplitedInstalment.ccInstall.Extra_Fee_Value__c = splitedInstalments[positionAdd].ccInstall.Extra_Fee_Value__c;

					newSplitedInstalment.ccInstall.Original_Extra_Fee_Value__c = splitedInstalments[positionAdd].ccInstall.Original_Extra_Fee_Value__c;
					newSplitedInstalment.ccInstall.Original_Instalment_Value__c = splitedInstalments[positionAdd].ccInstall.Original_Instalment_Value__c;
					newSplitedInstalment.ccInstall.Original_Tuition_Value__c = splitedInstalments[positionAdd].ccInstall.Original_Tuition_Value__c;
					newSplitedInstalment.ccInstall.Original_Due_Date__c = splitedInstalments[positionAdd].ccInstall.Original_Due_Date__c;
					newSplitedInstalment.ccInstall.Commission__c = selectedInstallment.Commission__c;
					newSplitedInstalment.ccInstall.Commission_Tax_Rate__c = selectedInstallment.Commission_Tax_Rate__c;

					newSplitedInstalment.ccInstall.Commission_Tax_Value__c = selectedInstallment.Commission__c * (selectedInstallment.Commission_Tax_Rate__c/100);

					newSplitedInstalment.ccInstall.Related_Fees__c = splitedInstalments[positionAdd].ccInstall.Related_Fees__c;

					if(splitedInstalments[positionAdd].ccInstall.Extra_Fee_Value__c != null){
						newSplitedInstalment.ccInstall.Instalment_Value__c += splitedInstalments[positionAdd].ccInstall.Extra_Fee_Value__c;
					}

					splitedInstalments[positionAdd].ccInstall.Extra_Fee_Value__c = 0;
					splitedInstalments[positionAdd].ccInstall.Original_Extra_Fee_Value__c = 0;
					splitedInstalments[positionAdd].ccInstall.Original_Instalment_Value__c = splitedInstalments[positionAdd].ccInstall.Tuition_Value__c;
					splitedInstalments[positionAdd].ccInstall.Original_Tuition_Value__c = splitedInstalments[positionAdd].ccInstall.Tuition_Value__c;
					splitedInstalments[positionAdd].ccInstall.Original_Due_Date__c = splitedInstalments[positionAdd].ccInstall.Due_Date__c;
					splitedInstalments[positionAdd].ccInstall.Related_Fees__c = null;
					splitedInstalments[positionAdd].ccInstall.Instalment_Value__c = splitedInstalments[positionAdd].ccInstall.Tuition_Value__c;
				}

				//newSplitedInstalment.ccInstall.Number__c = intallmentNumber-1;
				newSplitedInstalment.ccInstall.Number__c = selectedInstallment.Number__c;
				splitedInstalments.add(positionAdd, newSplitedInstalment);
				for(integer i = positionAdd; i < splitedInstalments.size(); i++){
					//splitedInstalments[i].ccInstall.Number__c++;
					countNumberInstall++;
				}
			}

		}

		//delete instalment with value and fee == 0
		for(integer i = 0; i < splitedInstalments.size(); i++){
			if(splitedInstalments[i].ccInstall.Instalment_Value__c == 0 && (splitedInstalments[i].ccInstall.Extra_Fee_Value__c == 0 || splitedInstalments[i].ccInstall.Extra_Fee_Value__c == null)){
				splitedInstalments.remove(i);
				break;
			}
		}

		integer countSplitNumber = 1;
		if(selectedInstallment.Split_Number__c != null)
			countSplitNumber = integer.valueOf(selectedInstallment.Split_Number__c);

		for(integer i = 0; i < splitedInstalments.size(); i++){
			splitedInstalments[i].ccInstall.Split_Number__c = countSplitNumber++;
		}



		totalSplited += newSplitedInstalment.ccInstall.Tuition_Value__c;

		newSplitedInstalment = new instalmentValue();


		return null;
	}

	public pageReference removeInstalmentValue(){
		integer index = integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
		boolean foundInstall = false;
		decimal feeValue;
		string relatedFees;
		for(integer i = 0; i < splitedInstalments.size(); i++){
			if(index == splitedInstalments[i].instIndex){
				system.debug('splitedInstalments===> ' + splitedInstalments);
				system.debug('splitedInstalments.size()===> ' + splitedInstalments.size());
				if(i == 0 && splitedInstalments[i].ccInstall.Extra_Fee_Value__c != null && splitedInstalments.size() > 1){
					splitedInstalments[i+1].ccInstall.Extra_Fee_Value__c = splitedInstalments[i].ccInstall.Extra_Fee_Value__c;

					splitedInstalments[i+1].ccInstall.Original_Extra_Fee_Value__c = splitedInstalments[i].ccInstall.Original_Extra_Fee_Value__c;
					splitedInstalments[i+1].ccInstall.Original_Instalment_Value__c = splitedInstalments[i].ccInstall.Original_Instalment_Value__c;
					splitedInstalments[i+1].ccInstall.Original_Tuition_Value__c = splitedInstalments[i].ccInstall.Original_Tuition_Value__c;
					splitedInstalments[i+1].ccInstall.Original_Due_Date__c = splitedInstalments[i].ccInstall.Original_Due_Date__c;

					splitedInstalments[i+1].ccInstall.Commission__c = selectedInstallment.Commission__c;
					splitedInstalments[i+1].ccInstall.Commission_Tax_Rate__c = selectedInstallment.Commission_Tax_Rate__c;
					splitedInstalments[i+1].ccInstall.Commission_Tax_Value__c = selectedInstallment.Commission__c * (selectedInstallment.Commission_Tax_Rate__c/100);

					splitedInstalments[i+1].ccInstall.Related_Fees__c = splitedInstalments[i].ccInstall.Related_Fees__c;

					if(splitedInstalments[i].ccInstall.Extra_Fee_Value__c != null)
						splitedInstalments[i+1].ccInstall.Instalment_Value__c += splitedInstalments[i].ccInstall.Extra_Fee_Value__c;
				}
				totalSplited -= splitedInstalments[i].ccInstall.Tuition_Value__c;
				splitedInstalments.remove(i);
				foundInstall = true;
			}
			/*if(foundInstall && splitedInstalments.size() > 0)
				splitedInstalments[i].ccInstall.Number__c--;*/
		}
		integer countSplitNumber = 1;
		for(integer i = 0; i < splitedInstalments.size(); i++){
			splitedInstalments[i].ccInstall.Split_Number__c = countSplitNumber++;
		}
		return null;
	}

	public pageReference saveNewInstallments(){
		Savepoint sp = Database.setSavepoint();
		try{

			list<client_course_instalment__c> newInstalments = new list<client_course_instalment__c>();
			//list<client_course_instalment__c> instalmentsNumberUpdate = new list<client_course_instalment__c>();



			/*for(client_course_instalment__c iu:[Select id, number__c from client_course_instalment__c where client_course__c = : selectedInstallment.client_course__c and Number__c > :selectedInstallment.Number__c]){
				iu.Number__c = iu.Number__c + splitedInstalments.size()-1;
				instalmentsNumberUpdate.add(iu);
			}*/

			//delete [Select id from Client_Document__c WHERE client_course_instalment__c = :selectedInstallment.id];
			//delete [Select id from client_course_instalment__c where id = :selectedInstallment.id];


			for(instalmentValue itv:splitedInstalments){
				if(itv.ccInstall.Number__c == selectedInstallment.Number__c && (itv.ccInstall.Split_Number__c == 1 || itv.ccInstall.Split_Number__c == selectedInstallment.Split_Number__c))
					itv.ccInstall.id = selectedInstallment.id;
				newInstalments.add(itv.ccInstall);
			}

			upsert newInstalments;




			System.debug('==> newInstalments: '+newInstalments);

			/**list<string> installmentIds = new list<string>();
			list<Client_Document__c> schoolReceipt = new list<Client_Document__c>();
			for(client_course_instalment__c cci:newInstalments){
				if(!(cci.Number__c == selectedInstallment.Number__c && cci.Split_Number__c == 1)){
					schoolReceipt.add(new Client_Document__c(Client__c = selectedInstallment.client_course__r.client__c, Document_Category__c = 'Finance', Document_Type__c = 'School Installment Receipt', client_course_instalment__c = cci.id));
					installmentIds.add(cci.id);
				}
			}

		 	insert schoolReceipt;

		 	map<string, string> docReceipt = new map<string, string>();
		 	for(Client_Document__c cd:[Select id, client_course_instalment__c from Client_Document__c where client_course_instalment__c in :installmentIds])
	 			docReceipt.put(cd.client_course_instalment__c, cd.id);

	 		for(client_course_instalment__c cci:newInstalments){
	 			if(docReceipt.containsKey(cci.id))
	 				cci.Client_Document__c = docReceipt.get(cci.id);
	 		}

	 		update newInstalments;
	 		**/

	 		client_course__c updateNumberInstall = new client_course__c(id = ccId, Number_Instalments__c = listInstalments.size() + newInstalments.size()-1);
		 	update updateNumberInstall;

			/*if(instalmentsNumberUpdate.size() > 0)
				update instalmentsNumberUpdate;*/

			instalment_SplitUpdate = new list<client_course_instalment__c>();
			integer countSplitNumber = 1;
			for(client_course_instalment__c inst:[Select id, Split_Number__c, Number__c, isRequestedShareCommission__c, client_course__r.share_commission_decision_status__c, Shared_Commission_Bank_Dep_Enrolled__c, Shared_Commission_Bank_Dep_Received__c, Shared_Commission_Bank_Dep_Request__c from client_course_instalment__c WHERE client_course__c = :ccId and Number__c = :selectedInstallment.Number__c order by Due_Date__c]){
				inst.Split_Number__c = countSplitNumber++;

				instalment_SplitUpdate.add(inst);
			}
			update instalment_SplitUpdate;

			string firstSplitId;
			string schoolInvoice;
			list<client_course_instalment__c> instParents = [Select id, Splited_From__c, Discount__c, Tuition_Value__c, client_course__c, Split_Number__c, isFirstPayment__c, Required_for_COE__c, School_Payment_Invoice__c from client_course_instalment__c where client_course__c = :ccId and number__c = :selectedInstallment.number__c order by Number__c, Split_Number__c]; 
			for(client_course_instalment__c ci:instParents){
				if(ci.id == ApexPages.currentPage().getParameters().get('itid')){
					schoolInvoice = ci.School_Payment_Invoice__c;
				}

				if(ci.Split_Number__c == 1){
					firstSplitId = ci.id;
					ci.Tuition_Value__c += ci.Discount__c;
				}
				else{
					ci.Splited_From__c = firstSplitId;
					if(isPaid){
						ci.School_Payment_Invoice__c = schoolInvoice;
						ci.Paid_split__c = true;
					} 
				}										

			}
			update instParents;


		}catch(Exception e){
			Database.rollback(sp);
		}

		return null;
	}

	public String getInfoMessage(){
		string minMsg = minLimitInstallment == null?'Any Date':minLimitInstallment.format();
		String msg = 'Input Date between <b>' + minMsg + '</b> and <b>' + maxLimitInstallment.format() + '</b> and Tuition Value to Create a New Installment';

		return msg;
	}

}