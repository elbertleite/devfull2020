@isTest
private class enrolment_process_test {
	public static Account school {get;set;}
	public static Account agency {get;set;}
	public static Contact emp {get;set;}
	public static Contact client {get{
		if (null == client) {
			client = [Select Id, Name from Contact limit 1];
		}
		return client;
	}set;}
	public static User portalUser {get{
		if (null == portalUser) {
			portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
		}
		return portalUser;
	}set;}
	public static Account campus {get;set;}
	public static Course__c course {get;set;}
	public static Course__c course2 {get;set;}
	public static Course__c course3 {get;set;}
	public static Campus_Course__c campusCourse {get;set;}
	public static Campus_Course__c cc2 {get;set;}
	public static Campus_Course__c cc3 {get;set;}
	public static client_course__c clientCourseBooking {
		get {
			if (null == clientCourseBooking)
				clientCourseBooking = [Select Id, Name from client_course__c where isBooking__c = true limit 1];
			return clientCourseBooking;
		}
		set;
	}
	public static client_course__c clientCourse {get;set;}
	public static client_course__c clientCourse2 {get;set;}
	public static client_course__c clientCourse3 {get;set;}


	@testSetup static void setup() {
		TestFactory tf = new TestFactory();

		school = tf.createSchool();
		agency = tf.createAgency();
		emp = tf.createEmployee(agency);
		portalUser = tf.createPortalUser(emp);
		campus = tf.createCampus(school, agency);
		course = tf.createCourse();
		course2 = tf.createCourse();
		course3 = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);
		cc2 =  tf.createCampusCourse(campus, course2);
		cc3 =  tf.createCampusCourse(campus, course3);
		system.runAs(portalUser){
			client = tf.createLead(agency, emp);
			client.Owner__c = portalUser.id;
			update client;
			clientCourseBooking = tf.createBooking(client);
			clientCourse = tf.createClientCourse(client, school, campus, course, campusCourse, clientCourseBooking);
			clientCourse2 = tf.createClientCourse(client, school, campus, course2, cc2, clientCourseBooking);
			clientCourse3 = tf.createClientCourse(client, school, campus, course3, cc3, clientCourseBooking);


			Client_Document__c cd = new Client_Document__c();
			cd.Document_Category__c = 'Enrolment';
			cd.Document_Type__c = 'COE';
			cd.Preview_Link__c = 'https://s3.amazonaws.com/ehfdev/cloudpondconnector%2FClient_Document__c%2Fa0uN00000018DHp+-+a0uN00000018DHp%2F%5Bu%3APatricia+Greco%3B%5DLOO.txt?AWSAccessKeyId=AKIAIF5ZUYUWN65E5MTA&Expires=2118116380&Signature=T%2F7%2FbUdpuNN93IQ3u17%2FjEywWf8%3D!#!LOO.txt;;https://s3.amazonaws.com/ehfdev/cloudpondconnector%2FClient_Document__c%2Fa0uN00000018DHp+-+a0uN00000018DHp%2F%5Bu%3APatricia+Greco%3B%5DSIGNED+LOO.txt?AWSAccessKeyId=AKIAIF5ZUYUWN65E5MTA&Expires=2118116380&Signature=06TrkGtQ2zHkPv7Q4H1D9umTjuA%3D!#!SIGNED LOO.txt';
			cd.Client_Course__c = clientCourse.id;
			insert cd;

			clientCourse.Enrolment_Form__c = cd.id;
			clientCourse.LOO__c = cd.id;
			update clientCourse;

			ApexPages.currentPage().getParameters().put('id', client.id);
			ApexPages.currentPage().getParameters().put('bk', clientCourseBooking.id);
			enrolment_process ep = new enrolment_process();
			system.assertEquals(ep.pendingRequests.size() > 0, true);

		}
	}

	static testMethod void testEmail() {
		Test.startTest();
		system.runAs(portalUser){
			ApexPages.currentPage().getParameters().put('id', client.id);
			ApexPages.currentPage().getParameters().put('bk', clientCourseBooking.id);
			enrolment_process ep = new enrolment_process();

            for(String key : ep.files.keySet())
				for(EnrolmentManager.File f : ep.files.get(key) )
               		f.selected = true;
           	for(String key : ep.campusFiles.keySet())
				for(String k2 : ep.campusFiles.get(key).keySet())
					for(EnrolmentManager.File f : ep.campusFiles.get(key).get(k2))
						f.selected = true;

			ep.sendEmail();
		}
		Test.stopTest();
	}

	static testMethod void testAddNewCourse() {
		Test.startTest();
		system.runAs(portalUser){
			ApexPages.currentPage().getParameters().put('id', client.id);
			ApexPages.currentPage().getParameters().put('bk', clientCourseBooking.id);
			enrolment_process ep = new enrolment_process();
			ep.addNewCourse();
		}
		Test.stopTest();
	}

	static testMethod void testRequestLOO() {
		Test.startTest();
		system.runAs(portalUser){
			ApexPages.currentPage().getParameters().put('id', client.id);
			ApexPages.currentPage().getParameters().put('bk', clientCourseBooking.id);
			enrolment_process ep = new enrolment_process();
			ApexPages.currentPage().getParameters().put('courseid', ep.pendingRequests[0].course.id);
            ep.requestLOO();
			System.assertEquals(ep.pendingRequests[0].course.LOO_Requested_to_BO__c, true);
			System.assertEquals(ep.pendingRequests[0].course.Enrolment_Status__c, EnrolmentManager.STATUS_LOO_REQUESTED_TO_BO);
		}
		Test.stopTest();
	}

	static testMethod void testSaveVisa() {
		Test.startTest();
		system.runAs(portalUser){
			ApexPages.currentPage().getParameters().put('id', client.id);
			ApexPages.currentPage().getParameters().put('bk', clientCourseBooking.id);


			enrolment_process ep = new enrolment_process();


			ep.pendingRequests[0].visa.Visa_Country_applying_for__c = 'Australia';
			ep.pendingRequests[0].visa.Expiry_Date__c = system.today();


			ApexPages.currentPage().getParameters().put('courseid', ep.pendingRequests[0].course.id);
            ep.saveVisa();

		}
		Test.stopTest();
	}

	static testMethod void test_method_one() {

		Test.startTest();

		system.runAs(portalUser){
			ApexPages.currentPage().getParameters().put('id', client.id);
			ApexPages.currentPage().getParameters().put('bk', clientCourseBooking.id);
			enrolment_process ep = new enrolment_process();
            ep.enrol();
            ep.deleteCourse();
            ep.updateVisa();

			String types = ep.STATUS_DRAFT;
			types = ep.STATUS_LOO_REQUESTED_TO_BO;
			types = ep.STATUS_LOO_REQUESTED_TO_SCH;
			types = ep.STATUS_MISSING_DOCUMENTS;
			types = ep.STATUS_LOO_RECEIVED;
			types = ep.STATUS_LOO_SENT_TO_CLI;
			types = ep.STATUS_WAITING_CLI_PAYMENT;
			types = ep.STATUS_PAID_BY_CLIENT;
			types = ep.STATUS_COE_REQUESTED_TO_BO;
			types = ep.STATUS_COE_REQUESTED_TO_SCH;
			types = ep.STATUS_COE_RECEIVED;
			types = ep.STATUS_COE_SENT_TO_CLI;
			types = ep.STATUS_ENROLLED;
			types = ep.STATUS_MISSING_DOCUMENTS;
			types = ep.STYLE_MISSING_DOCUMENTS;
			types = ep.STYLE_COMPLETED;
			double d = ep.offset;
			boolean bol = ep.isDeletable;
			types = ep.bucket;
			List<SelectOption> dummy = ep.countries;

		}


		Test.stopTest();
	}



}