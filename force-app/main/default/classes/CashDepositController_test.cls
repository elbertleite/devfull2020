/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CashDepositController_test {

    static testMethod void myUnitTest() {
         
       TestFactory tf = new TestFactory();
       
       Account school = tf.createSchool();
       Account agency = tf.createAgency();
       Contact emp = tf.createEmployee(agency);
       User portalUser = tf.createPortalUser(emp);
       Account campus = tf.createCampus(school, agency);
       Course__c course = tf.createCourse();
       Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
       
       Test.startTest();
       system.runAs(portalUser){
       	   Contact client = tf.createLead(agency, emp);
	       client_course__c booking = tf.createBooking(client);
	       client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
	       
	       CashDepositController testClass = new CashDepositController();
	       
	       client_course_instalment_pay instPay = new client_course_instalment_pay(new ApexPages.StandardController(instalments[0]));
	       instPay.newPayDetails.Value__c = 2230;
		   instPay.selectedUser = client.id;
		   instPay.selectedAgency = agency.id;
		   instPay.newPayDetails.Date_Paid__c = system.today();
		   instPay.newPayDetails.Payment_Type__c = 'Cash';
		   instPay.addPaymentValue();
		   instPay.savePayment();
		   
		   Contact toDate = testClass.toDate;
		   Contact fromDate = testClass.fromDate;
		   
		   List<SelectOption> agencyGroupOptions = testClass.agencyGroupOptions;
		   testClass.changeGroup();
		   testClass.selectedAgency = agency.id;
		   
		   
		   List<client_course_instalment_payment__c> updatePayments = new List<client_course_instalment_payment__c>();
		   for(client_course_instalment_payment__c ccip : [Select Id, selected__c, Confirmed_Date__c from client_course_instalment_payment__c]){
		   		ccip.Confirmed_Date__c = system.today();
		   		ccip.selected__c = true;
		   		updatePayments.add(ccip);
		   }//end for
		   
		   update updatePayments;
		   			   
		   testClass.fromDate.Expected_Travel_Date__c = system.today();
		   testClass.toDate.Expected_Travel_Date__c = system.today().addDays(3);
		   testClass.findCashToDeposit();
		   
		   system.debug('testClass.lCash ===>' + testClass.lCash);
		   testClass.confirmDeposit();
		   testClass.selectedBank = testClass.banks[1].getValue();
		   testClass.confirmDeposit();
		   testClass.bankDeposit.Deposit_Date__c = system.today();
		   testClass.confirmDeposit();
	       
	       
       }
       Test.stopTest();
        
    }
}