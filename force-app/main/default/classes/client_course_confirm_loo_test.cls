@isTest
private class client_course_confirm_loo_test {

	@isTest static void test_method_one() {

		TestFactory tf = new TestFactory();
		Account agencyGroup = tf.createAgencyGroup();
		Account agency = tf.createSimpleAgency(agencyGroup);
		Contact employee = tf.createEmployee(agency);
		User portalUser = tf.createPortalUser(employee);
		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);
		Course__c course = tf.createCourse();
		Campus_Course__c cc = tf.createCampusCourse(campus, course);
		Course__c course2 = tf.createCourse();
		Campus_Course__c cc2 = tf.createCampusCourse(campus, course);
		Commission__c commission = tf.createCommission(school);

		Back_Office_Control__c bo = new Back_Office_Control__c(Agency__c = agency.id, Country__c = 'Australia', Backoffice__c = agency.id, Services__c = 'Admissions');
		insert bo;

		Test.startTest();

		system.runAs(portalUser){

			Contact client = tf.createLead(agency, employee);
			client.Owner__c = portalUser.id;
			update client;
			client_course__c clientCourseBooking = tf.createBooking(client);
			client_course__c clientCourse = tf.createClientCourse(client, school, campus, course, cc, clientCourseBooking);
			client_course__c clientCourse2 = tf.createClientCourse(client, school, campus, course2, cc2, clientCourseBooking);
			client_course__c clientCourse3 = tf.createClientCourse(client, school, campus, course2, cc2, clientCourseBooking);
			clientCourse.LOO_Requested_To_Bo__c = true;
			clientCourse.LOO_Requested_to_Bo_By__c = portalUser.id;
			clientCourse.LOO_Requested_to_Bo_On__c = system.now();
			clientCourse.LOO_Requested_To_SC__c = true;
			clientCourse.LOO_Requested_to_SC_By__c = portalUser.id;
			clientCourse.LOO_Requested_to_SC_On__c = system.now();
			clientCourse.isPackage__c =true;
			update clientCourse;

			clientCourse2.Course_Package__c = clientCourse.id;
			clientCourse2.LOO_Requested_To_BO__c = true;
			clientCourse2.LOO_Requested_to_BO_By__c = portalUser.id;
			clientCourse2.LOO_Requested_to_BO_On__c = system.now();
			clientCourse2.LOO_Requested_To_SC__c = true;
			clientCourse2.LOO_Requested_to_SC_By__c = portalUser.id;
			clientCourse2.LOO_Requested_to_SC_On__c = system.now();
			update clientCourse2;


			client_course_confirm_loo confirm = new client_course_confirm_loo();
			confirm.setParams();
			List<SelectOption> dummy = confirm.agencyGroupOptions;
			confirm.changeGroup();
			dummy = confirm.schoolOptions;
			dummy = confirm.campusOptions;
			Contact dateFrom = confirm.dateFrom;
			Contact dateTo = confirm.dateTo;
			confirm.search();

			client_course_confirm_loo.isFileUploaded(clientCourse.id);

			ApexPages.currentPage().getParameters().put('courseid', clientCourse.id);
			ApexPages.currentPage().getParameters().put('noteComments', '123');
			confirm.confirmLOO();
			confirm.reverseProcess();
			confirm.setMDRequest();
			confirm.missingDocuments();

			ApexPages.currentPage().getParameters().put('bookingid', clientCourseBooking.id);
			confirm.confirmDelete();




			ApexPages.currentPage().getParameters().put('type', 'acc');
			confirm.setParams();

			ApexPages.currentPage().getParameters().put('type', 'ins');
			confirm.setParams();

			ApexPages.currentPage().getParameters().put('type', 'loo');
			confirm.setParams();

			confirm.changeSchool();

			double d = confirm.offSet;
			String st = confirm.bucket;
			st = confirm.requestCourseID;
			st = confirm.STRING_SEPARATOR;

		}

		Test.stopTest();



	}


}