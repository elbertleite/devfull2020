public with sharing class deposit_reconciliation {


	public list<consolidation> result {get;set;}

	private set<id> selectedPayments;
	private map<id, client_course__c> mapDeposit;
	private User currentUser {get;set;}

	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}


	public deposit_reconciliation(){
		currentUser = [Select Id, Name, Aditional_Agency_Managment__c, Contact.AccountId, Contact.Department__c, Contact.Account.ParentId from User where id = :UserInfo.getUserId() limit 1];
		selectedPayments = new set<id>();
		result = new list<consolidation>();
		mapDeposit = new map<Id, client_course__c>();

		findDeposits();

	}


	/** Find All Pending Deposits to Reconciliate **/
	private list<client_course__c> lDeposit {get{if(lDeposit==null) lDeposit = new list<client_course__c>(); return lDeposit;}set;}
	public void findDeposits(){

		String  fromDate = IPFunctions.FormatSqlDateIni(dateFilter.Original_Due_Date__c);
		String  toDate = IPFunctions.FormatSqlDateFin(dateFilter.Discounted_On__c);

		result.clear();
		lDeposit.clear();
		mapDeposit.clear();

		set<string> clietCourseIds = new set<string>();

		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && currentUser.Aditional_Agency_Managment__c == null) //set the user to see only his own agency
			selectedAgency = currentUser.Contact.AccountId;

		string sqlAgency = 'SELECT Deposit__c FROM client_course_instalment_payment__c WHERE Received_By_Agency__c = :selectedAgency and Deposit__c != null ';

		if(SelectedPeriod == 'range')
			sqlAgency += ' and Date_Paid__c >= '+fromDate+' and  Date_Paid__c <= '+toDate;
		else if(SelectedPeriod == 'TODAY')
			sqlAgency += ' and Date_Paid__c = TODAY ';
		else if(SelectedPeriod == 'THIS_WEEK')
			sqlAgency += ' and Date_Paid__c = THIS_WEEK ';
		else if(SelectedPeriod == 'THIS_MONTH')
			sqlAgency += ' and Date_Paid__c = THIS_MONTH ';
		// else if(SelectedPeriod == 'unc'){
		// 	sqlAgency += ' and Confirmed_Date__c = null ';
		// }

		if(selectedPaymentStatus == 'open' || SelectedPeriod == 'unc')
			sqlAgency += ' AND Confirmed_Date__c = null ';
		else
			sqlAgency += ' AND Confirmed_Date__c != null ';

		for(client_course_instalment_payment__c cid:Database.query(sqlAgency))
			clietCourseIds.add(cid.Deposit__c);


		String sql =' SELECT Id, Deposit_Reconciliated__c, Deposit_Total_Value__c, Client__r.Name, Client__r.Owner__r.Name  ,(SELECT Id, Value__c, Received_By__r.Name, Received_On__c, Payment_Type__c, Date_Paid__c, Confirmed_By__r.Name, Confirmed_By_Agency__r.Name, Confirmed_Date__c, Unconfirmed_By__r.Name, Unconfirmed_Date__c, selected__c, Transfered_from_Deposit__c, isClient_Refund__c,Bank_Deposit__c FROM client_course_payments_deposit__r WHERE isClient_Refund__c = FALSE ) FROM client_course__c where id in :clietCourseIds AND Deposit_Total_Value__c >0 ';
		// if(selectedPaymentStatus == 'open')
		// 	sql += ' AND Deposit_Reconciliated__c = false ';
		// else
		// 	sql += ' AND Deposit_Reconciliated__c = true ';



		sql+=' order by lastModifiedDate ';

		system.debug('sql==' + sql);

		system.debug('selectedPaymentTypes==' + selectedPaymentTypes);

		if(selectedPaymentTypes!='all'){
			for(client_course__c cc :  Database.query(sql)){

				for(client_course_instalment_payment__c ccip : cc.client_course_payments_deposit__r)
					if(ccip.Payment_Type__c == selectedPaymentTypes){
						lDeposit.add(cc);
						break;
					}

			}//end for


		}else{
			lDeposit = Database.query(sql);
		}



		consolidation ch;
		for(client_course__c i : lDeposit){
			ch = new consolidation();
			ch.deposit = i;

			if(selectedPayments.size()>0){
				for(client_course_instalment_payment__c cp :ch.deposit.client_course_payments_deposit__r)
					if(selectedPayments.contains(cp.id)){
						cp.selected__c = true;
						ch.showSave = true;
					}
			}

			result.add(ch);

		}//end for

		mapDeposit = new map<Id, client_course__c>(lDeposit);
	}
	/**/



	/** Confirm Payments **/
	public void confirmPayments(){

		checkSelectedPayments();

		String objId = ApexPages.currentpage().getparameters().get('id');

		SavePoint sp;
		//try {
			sp = Database.setSavepoint();
			List<client_course_instalment_payment__c> listPayments = new List<client_course_instalment_payment__c>();
			client_course__c deposit;

			/** Update Payment **/
			boolean allPaymentsConfirmed = true;
			for(client_course_instalment_payment__c ccip : mapDeposit.get(objId).client_course_payments_deposit__r){
				if(ccip.selected__c){
					ccip.selected__c = false;
					ccip.Confirmed_By__c = currentUser.id;
					ccip.Confirmed_Date__c = system.now();
					ccip.Confirmed_By_Agency__c = currentUser.Contact.AccountId;

					ccip.Unconfirmed_By__c = null;
					ccip.Unconfirmed_Date__c = null;

					selectedPayments.remove(ccip.Id);
				}
					listPayments.add(ccip);

				if(ccip.Confirmed_By__c == null)
					allPaymentsConfirmed = false;
			}

			/** Update Deposit IF all Payments Confirmed **/
			if(allPaymentsConfirmed){
				mapDeposit.get(objId).Deposit_Reconciliated__c = true;
				deposit = mapDeposit.get(objId);
			}

			update listPayments;

			if(deposit != null){
				update deposit;
			}

			findDeposits();

		//}catch (Exception e){
		//	system.debug('Exception thrown: ' + e.getMessage());
		//	Database.rollback(sp);
		//}

	}




	/** Unconfirm Payment **/
	public void unconfirmPayment(){
		checkSelectedPayments();

		String paymentid = ApexPages.currentpage().getparameters().get('paymentid');
		system.debug('paymentid===' + paymentid);

		client_course_instalment_payment__c unconfirmedPayment;
		client_course__c unconfirmedInstalment;
		for(client_course__c cci : lDeposit){
			for(client_course_instalment_payment__c ccip : cci.client_course_payments_deposit__r){
				if(ccip.id == paymentid){
					ccip.Confirmed_By__c = null;
					ccip.Confirmed_Date__c = null;
					ccip.Confirmed_By_Agency__c = null;
					ccip.Unconfirmed_By__c = UserInfo.getUserId();
					ccip.Unconfirmed_Date__c = system.now();
					unconfirmedPayment = ccip;

					cci.Deposit_Reconciliated__c = false;
					unconfirmedInstalment = cci;

					break;
				}
			}

			if(unconfirmedPayment != null) break;
		}

		update unconfirmedInstalment;
		update unconfirmedPayment;


		findDeposits();
	}
	/**/



	/** Check Selected Payments **/
	public void checkSelectedPayments(){

		selectedPayments.clear();

		for(consolidation con: result){

			/** Deposit **/
			if(con.deposit!=null){
				for(client_course_instalment_payment__c ccip : mapDeposit.get(con.deposit.id).client_course_payments_deposit__r)
					if(ccip.selected__c)
						selectedPayments.add(ccip.Id);
			}

		}//end for

	}
	/** END -- Check Selected Payments **/






	/** Inner Class for Result **/
	public class consolidation{
		public client_course__c deposit {get;set;}
		public boolean showSave {get{if(showSave==null) showSave = false; return showSave;}set;}
	}
	/**/

	public boolean showAll{get{if(showAll == null) showAll = false; return showAll;} set;}




	/********************** Filters **********************/

	public string selectedAgencyGroup{
    	get{
    		if(selectedAgencyGroup == null)
    			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
    		return selectedAgencyGroup;
    	}
    	set;
    }

	 public List<SelectOption> agencyGroupOptions {
  		get{
   			if(agencyGroupOptions == null){

   				agencyGroupOptions = new List<SelectOption>();
   				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group
	    			for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:currentUser.Contact.AccountId order by Parent.Name]){
		     			agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
		    		}
   				}else{
   					for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
		     			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
		    		}
   				}
	   		}
	   		return agencyGroupOptions;
	  }
	  set;
 	}

	public void changeGroup(){
		selectedAgency = '';
		getAgencies();
	}


	public string selectedAgency {get{if(selectedAgency==null) selectedAgency = currentUser.Contact.AccountId; return selectedAgency;}set;}
	public list<SelectOption> getAgencies(){
		List<SelectOption> result = new List<SelectOption>();
		if(selectedAgencyGroup != null){
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
				for(Account a: [Select Id, Name from Account where id =:currentUser.Contact.AccountId order by name]){
					if(selectedAgency=='')
						selectedAgency = a.Id;
					result.add(new SelectOption(a.Id, a.Name));
				}
				if(currentUser.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
						result.add(new SelectOption(ac.id, ac.name));
					}
				}
			}else{
				for(Account a: [Select Id, Name from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
					result.add(new SelectOption(a.Id, a.Name));
					if(selectedAgency=='')
						selectedAgency = a.Id;
				}
			}
		}

		return result;
	}



	public String selectedPaymentStatus {get{ if(selectedPaymentStatus == null) selectedPaymentStatus = 'open'; return selectedPaymentStatus;} set; }
	public List<SelectOption> paymentStatus {
		get{
			if(paymentStatus == null){
				paymentStatus = new List<SelectOption>();
				paymentStatus.add(new SelectOption('open','Open'));
				paymentStatus.add(new SelectOption('confirmed','Confirmed'));

			}
			return paymentStatus;
		}
		set;
	}

	public list<selectOption> depositListType{get; set;}
	public String selectedPaymentTypes {get{if(selectedPaymentTypes == null) selectedPaymentTypes = 'all'; return selectedPaymentTypes;}set;}
	public List<SelectOption> paymentTypes {
		get{
			if(paymentTypes == null){
				paymentTypes = new List<SelectOption>();
				depositListType = new List<SelectOption>();
				paymentTypes.add(new SelectOption('all','All'));
				Schema.DescribeFieldResult fieldResult = client_course__c.Deposit_Type__c.getDescribe();
				List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
				for( Schema.PicklistEntry f : ple){
					if(f.getLabel()!='offshore'){
						depositListType.add(new SelectOption(f.getLabel(), f.getValue()));
						paymentTypes.add(new SelectOption(f.getLabel(), f.getValue()));
					}
				}//end for
				depositListType.add(new SelectOption('Transfered from Deposit', 'Transfered from Deposit', true));
			}
			return paymentTypes;
		}
		set;
	}


	public client_course_instalment__c dateFilter{
	get{
		if(dateFilter == null){
			dateFilter = new client_course_instalment__c();
			Date myDate = Date.today();
			Date weekStart = myDate.toStartOfMonth();
			dateFilter.Original_Due_Date__c = weekStart;
			dateFilter.Discounted_On__c = myDate;
		}
		return dateFilter;
	}set;}


	public string SelectedPeriod{get {if(SelectedPeriod == null) SelectedPeriod = 'unc'; return SelectedPeriod; } set;}
	private List<SelectOption> periods;
	public List<SelectOption> getPeriods() {
		if(periods == null){
			periods = new List<SelectOption>();
			periods.add(new SelectOption('TODAY','Today'));
			periods.add(new SelectOption('THIS_WEEK','This Week'));
			periods.add(new SelectOption('THIS_MONTH','This Month'));
			periods.add(new SelectOption('unc','Unconfirmed'));
			periods.add(new SelectOption('range','Range'));
		}
		return periods;
	}
}