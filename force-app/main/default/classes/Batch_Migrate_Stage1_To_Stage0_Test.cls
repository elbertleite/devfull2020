@isTest
global class Batch_Migrate_Stage1_To_Stage0_Test {
    static testMethod void myUnitTest() {

    TestFactory tf = new TestFactory();
    
    Account agency = tf.createAgency();

    Account agencyGroup = tf.createAgencyGroup();

    Contact emp = tf.createEmployee(agency);
    
    Contact lead = tf.createLead(agency, emp);
    Contact client = tf.createClient(agency);

    User portalUser = tf.createPortalUser(emp);
    
    Destination_Tracking__c dt = new Destination_tracking__c();
    dt.Client__c = lead.id; 
    dt.Current_Cycle__c = true;
    dt.Arrival_Date__c = system.today().addMonths(3);
    dt.Destination_Country__c = 'Australia';
    dt.Destination_City__c = 'Sydney';
    dt.Expected_Travel_Date__c = system.today().addMonths(3);
    dt.lead_Cycle__c = false;
    insert dt;

    Client_Checklist__c checklist = new Client_Checklist__c();
    checklist.Agency__c = agency.id;
    checklist.Agency_Group__c = agency.Parentid;
    checklist.Destination__c = lead.Destination_Country__c;
    checklist.Checklist_Item__c = 'aaa';
    checklist.Client__c = lead.id;
    checklist.Destination_Tracking__c = dt.id;
    insert checklist;

        Client_Stage_Follow_up__c cs = new Client_Stage_Follow_up__c();
        cs.Agency__c = agency.id;
        cs.Agency_Group__c = agency.Parentid;
        cs.Client__c = lead.id;
        cs.Destination__c = 'Australia';
        cs.Stage_Item__c = 'aaaa';
        cs.Stage_Item_Id__c = 'aaaa';
        cs.Stage__c = 'Stage 1';
        cs.Destination_Tracking__c = dt.id;
        insert cs;

    Test.startTest();
       system.runAs(portalUser){
            /*String [] ids = new String [] {lead.id, client.id};
            //Batch_Migrate_Stage1_To_Stage0 batch = new Batch_Migrate_Stage1_To_Stage0(agency.ParentID);
            Batch_Migrate_Stage1_To_Stage0 batch = new Batch_Migrate_Stage1_To_Stage0();
            Database.executeBatch(batch);*/
    }

  }
}