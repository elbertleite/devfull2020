/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class contact_quotationEmail_test {

    static testMethod void myUnitTest() {
        
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();
        Contact employee = tf.createEmployee(agency);
        Contact client = tf.createClient(agency);
        Account school = tf.createSchool();
  		Account campus = tf.createCampus(school, agency);
  		Course__c course = tf.createCourse();
		Campus_Course__c cc = tf.createCampusCourse(campus, course);	
        Quotation__c quotation = tf.createQuotation(client, cc);
        
        Apexpages.currentPage().getParameters().put('quoteId', quotation.id);
        contact_quotationEmail cqe = new contact_quotationEmail(new Apexpages.Standardcontroller(quotation));
        
        string email = cqe.email;
        cqe.getSignature();
        
        cqe.selectFile();
        cqe.document.name = 'asd';
        cqe.attach();		
        
        cqe.redirectQuotations();
        cqe.getLanguageCode();
        cqe.getAttachments();
        List<SelectOption> categories = cqe.categories;
        List<SelectOption> types  = cqe.types;
        cqe.reloadTypes();
        cqe.reloadFiles();
        cqe.getOrgId();
        cqe.reloadEmailAttachments();
        cqe.sentEmailOwner = true;
        try {
        	cqe.sendEmailQuote();
        } catch (Exception e){}
        
        
    }
}