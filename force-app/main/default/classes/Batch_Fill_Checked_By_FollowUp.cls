global class Batch_Fill_Checked_By_FollowUp implements Database.Batchable<sObject>, Database.Stateful{

    global Batch_Fill_Checked_By_FollowUp() {}

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('SELECT ID, CreatedBy.ID, Checked_By__c FROM Client_Stage_Follow_Up__c WHERE Checked_By__c = NULL ORDER BY Last_Saved_Date_Time__c DESC');
    }
    
    global void execute(Database.BatchableContext BC, List<Client_Stage_Follow_Up__c> followups) {
        for(Client_Stage_Follow_Up__c followup : followups){
            followup.Checked_By__c = followup.CreatedBy.ID;
        }
        update followups;
        
    }
    
    global void finish(Database.BatchableContext BC) {}
}