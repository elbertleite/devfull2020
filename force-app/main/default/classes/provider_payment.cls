public with sharing class provider_payment {
	private User currentUser = [Select Id, Name, Aditional_Agency_Managment__c, Contact.Account.ParentId, Contact.AccountId, Contact.Department__c, Contact.Account.Global_Link__c, Contact.Signature__c, Contact.Name, Contact.Account.Name, Contact.Email from User where id = :UserInfo.getUserId() limit 1];
	public String s3Bucket {get;set;}
	// public String s3Folder {get;set;}

	public provider_payment() {
		retrieveMainCurrencies();
		s3Bucket = IPFunctions.s3bucket;
		list<SelectOption> aop = agencyOptions; //Just called here to populate mapAgCurrency
		searchProducts();
		invoicesToPay();
	}

	// U P D A T E C O M M I S S I ON
	private map<string, client_product_service__c> mapCommission;
	public void updateCommission(){
		list<client_product_service__c> toUpdate = new list<client_product_service__c>();
		for(string provider:listProducts.keySet()){
			for(client_product_service__c ps:listProducts.get(provider)){
				System.debug('==>ps.Commission_Value__c: '+ps.Commission_Value__c);
				System.debug('==>mapCommission.get(ps.id).Commission_Value__c: '+mapCommission.get(ps.id).Commission_Value__c);
				if(ps.Commission_Value__c != mapCommission.get(ps.id).Commission_Value__c)
					toUpdate.add(ps);
			}
		}
		if(toUpdate.size() > 0)
			update toUpdate;
		searchProducts();
	}

	// S E A R C H 		P R O D U C T S
	public map<string, string> provideName{get; set;}
	public integer productsNum{get{if(productsNum == null) productsNum = 0; return productsNum;} set;}
	public map<string, list<client_product_service__c>> listProducts{get; set;}
	public void searchProducts(){
		provideName = new map<string, string>();
		listProducts = new map<string, list<client_product_service__c>>();

			
		String sql = 'SELECT isSelected__c, Products_Services__r.Provider__c, Products_Services__r.Provider__r.Provider_Name__c, Client__r.name, Received_By_Department__r.name, Price_Total__c, Quantity__c, ';
		sql += ' Received_Date__c, Received_By_Agency__r.name, Received_by__r.name, Product_Name__c, Currency__c, Agency_Currency_Value__c, Agency_Currency__c, Agency_Currency_Rate__c, Claim_Commission_Type__c, ';
		sql += ' Total_Provider_Payment__c, Provider_Refund_Payment__c, Refund_Confirmed_ADM__c, Agency_Commission_Refund__c, Refund_amount__c, Commission_Type__c, Commission_Value__c, Commission__c, Total_Refund__c, Covered_by_Agency__c, ';
		sql += ' (SELECT Product_Name__c, Price_Total__c, Agency_Currency_Value__c, Refund_Confirmed_ADM__c FROM paid_products__r) ';
		sql += ' FROM client_product_service__c WHERE ';
		sql += ' ( (Received_By_Agency__c = :selectedAgency AND Provider_Payment__c = NULL AND Paid_to_Provider_on__c = NULL and Provider_Refund_Payment__c = null) OR  (Paid_to_Provider_by_Agency__c = :selectedAgency  AND Refund_Confirmed_ADM__c = TRUE AND Refund_Paid_to_ProviderOn__c = NULL AND Agency_Refund_Client__c = \'No\') )';
	
		if(selectedProvider != null && selectedProvider != 'none')
			sql += ' and Products_Services__r.Provider__c = \'' + selectedProvider + '\'';
		else 	sql += ' and Products_Services__r.Provider__c != null ';

		if(selectedCategory != null && selectedCategory != 'all')
			sql += ' AND ( Products_Services__r.Category__c = \'' + selectedCategory + '\' OR Products_Services__r.Category__c = NULL)';
		
		if(selectedProduct != null && selectedProduct != 'all')
			sql += ' AND Products_Services__c = \''+selectedProduct +'\'';

		if(receivedDate == 'dr')
			sql += ' AND Received_Date__c >= ' + IPFunctions.FormatSqlDateTimeIni(fDates.Visa_Expiry_Date__c) + ' AND Received_Date__c <= ' + IPFunctions.FormatSqlDateTimeFin(fDates.Expected_Travel_Date__c);
		
		sql += ' AND Paid_with_product__c = NULL ';

		sql += ' order by Currency__c, Received_Date__c, Client__r.name, Product_Name__c';

		system.debug('sql===>' + sql);

		mapCommission = new map<string, client_product_service__c>();
		for(client_product_service__c ps:Database.query(sql)){
			if(!listProducts.containsKey(ps.Products_Services__r.Provider__c))
				listProducts.put(ps.Products_Services__r.Provider__c, new list<client_product_service__c>{ps});
			else listProducts.get(ps.Products_Services__r.Provider__c).add(ps);
			mapCommission.put(ps.id, ps.clone());
			provideName.put(ps.Products_Services__r.Provider__c, ps.Products_Services__r.Provider__r.Provider_Name__c);
		}
		productsNum = listProducts.size();
		invoicesToPay();
	}

	// C A L C U L A T E 		I N V O I C E

	//================CURRENCY=============================
	private financeFunctions fin = new financeFunctions();
	public List<SelectOption> MainCurrencies{get; set;}
	private Map<String, double> agencyCurrencies;
	public Datetime currencyLastModifiedDate {get;set;}
	public void retrieveMainCurrencies(){

		MainCurrencies = fin.retrieveMainCurrencies();
		currencyLastModifiedDate = fin.currencyLastModifiedDate;
	}

	public Decimal totalInvoice {get;set;}
	public Integer countProds {get{if(countProds == null) countProds = 0; return countProds;}set;}
	public String prodCurrency {get;set;}
	public Invoice__c payInvoice {get;set;}
	public Contact fDates {get{if(fDates == NULL) fDates = new Contact(Visa_Expiry_Date__c = Date.today().toStartOfMonth(), Expected_Travel_Date__c = Date.today().toStartOfMonth().addMonths(1).addDays(-1)); return fDates;}set;} // Dates to filter
	private list<client_product_service__c> upProds {get;set;}
	


	public void calcInvoice(){

		// s3Folder = '';

		countProds = 0;
		Decimal totalInvoice = 0;
		String prodCurrency;
		String providerId;
		String prodIds = '';
		upProds = new list<client_product_service__c>();
		for(string provider:listProducts.keySet()){
			for(client_product_service__c p : listProducts.get(provider)){
				if(p.isSelected__c){
					prodIds += p.Id +';';

					//Provider Payment
					if(!p.Refund_Confirmed_ADM__c){
						totalInvoice += p.Total_Provider_Payment__c;

						for(client_product_service__c f : p.paid_products__r){
							totalInvoice += f.Price_Total__c;
							prodIds += f.Id +';';
							upProds.add(f);
							countProds++;
						}//end for
					}
					// Product Refund
					else
						totalInvoice += p.Total_Refund__c;

					prodCurrency = p.Currency__c;
					providerId = p.Products_Services__r.Provider__c;
					countProds++;
					upProds.add(p);
				}
			}
		}


		payInvoice = new Invoice__c (
			isProvider_Payment__c = true,
			Instalments__c = prodIds,
			Provider__c = providerId,
			Total_Value__c = totalInvoice,
			CurrencyIsoCode__c = prodCurrency,
			Paid_Date__c = system.today(), 
			Paid_for_Agency__c = selectedAgency,
			Agency_Currency__c = mapAgCurrency.get(selectedAgency)
		);


		recalculateRate();
	}

	public void recalculateRate(){
		fin.convertCurrency(payInvoice, payInvoice.CurrencyIsoCode__c, payInvoice.Total_Value__c, null, 'Agency_Currency__c', 'Agency_Currency_Rate__c', 'Agency_Currency_Value__c');
	}

	public void createInvoice(){
		calcInvoice();
		noS.saveInvoice(payInvoice, currentUser);
		for(client_product_service__c p : upProds){
			if(!p.Refund_Confirmed_ADM__c){
					p.Provider_Payment__c = payInvoice.Id;
				}
				else{
					p.Provider_Refund_Payment__c = payInvoice.Id;
				}
				p.isSelected__c = false;
		}
		update upProds;
		invoicesToPay();
		searchProducts();
	}

	public map<string, Invoice__c> listInvoices{get; set;}
	public integer numInvoices{get{if(numInvoices == null) numInvoices = 0; return numInvoices;} set;}
	public map<string, list<client_product_service__c>> productsInvoice{get; set;}
	public void invoicesToPay(){
		set<string> productIds = new set<string>();
		listInvoices = new map<string, Invoice__c>([Select provider__r.provider_name__c, provider__r.Require_File_to_Send_Email__c, Paid_On__c, Sent_Email_Receipt__c, Sent_Email_Receipt_By__c, Sent_Email_Receipt_On__c, Sent_Email_To__c, Instalments__c, External_Reference__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, Provider__c, Total_Value__c, Provider_Payment_Number__c, CurrencyIsoCode__c, Paid_Date__c, Agency_Currency__c, Paid_by_Agency__c, Finance_Global_Link__c from Invoice__c where Paid_for_Agency__c = :selectedAgency and isProvider_Payment__c = true and Sent_Email_Receipt__c = false]);
		numInvoices = listInvoices.size();
		for(Invoice__c linv:listInvoices.values())
			productIds.addAll(linv.Instalments__c.split(';'));
		productsInvoice = new map<string, list<client_product_service__c>>();
		for(client_product_service__c ps:[Select id, Provider_Payment__c, Provider_Refund_Payment__c, Products_Services__r.Provider__r.Provider_Name__c, Client__r.name, Received_By_Department__r.name, Quantity__c, Received_Date__c, Received_By_Agency__r.name, Received_by__r.name, 
										Product_Name__c, Currency__c, Agency_Currency_Value__c, Price_Total__c, Refund_Confirmed_ADM__c, Refund_amount__c, Total_Provider_Payment__c, (SELECT Product_Name__c, Price_Total__c, Agency_Currency_Value__c, Refund_Confirmed_ADM__c FROM paid_products__r)  from client_product_service__c where id in:productIds AND Paid_with_product__c = NULL]){
			if(ps.Provider_Payment__c != null){
				if(!productsInvoice.containsKey(ps.Provider_Payment__c))
					productsInvoice.put(ps.Provider_Payment__c, new list<client_product_service__c>{ps});
				else productsInvoice.get(ps.Provider_Payment__c).add(ps);

			}else if(ps.Provider_Refund_Payment__c != null){
				if(!productsInvoice.containsKey(ps.Provider_Refund_Payment__c))
					productsInvoice.put(ps.Provider_Refund_Payment__c, new list<client_product_service__c>{ps});
				else productsInvoice.get(ps.Provider_Refund_Payment__c).add(ps);
			}
		}
		

	}

	public void confirmInvoice(){
		System.debug('==> payInvoice1: '+payInvoice);
		payInvoice = listInvoices.get(ApexPages.currentPage().getParameters().get('inv'));
		System.debug('==> payInvoice2: '+payInvoice);
		confirmPay();
	}

	// P A Y 		I N V O I C E
	private noSharing noS = new noSharing();
	public String emTo {get;set;}
	public String emCc {get;set;}
	public String emSubj {get;set;}
	public String emCont {get;set;}

	public void confirmPay(){

		Savepoint sp = Database.setSavepoint();
		try{

			payInvoice.Paid_by__c = UserInfo.getUserId();
			payInvoice.Paid_On__c = system.now();
			if(payInvoice.Paid_Date__c == null)
				payInvoice.Paid_Date__c = system.today(); 
			//payInvoice.Paid_by_Agency__c = currentUser.Contact.AccountId;
			payInvoice.Paid_by_Agency__c = selectedAgency;

			noS.saveInvoice(payInvoice, currentUser);
			System.debug('==> payInvoice3: '+payInvoice);
			
			upProds = [Select id, Refund_Confirmed_ADM__c, Provider_Payment__c, Paid_to_Provider_by__c, Paid_to_Provider_by_Agency__c, Paid_to_Provider_on__c, Provider_Refund_Payment__c, Paid_Refund_to_Provider_By__c, Refund_Paid_to_ProviderOn__c, isSelected__c, Claim_Commission_Type__c,Commission_Confirmed_By__c,Commission_Confirmed_by_Agency__c,Commission_Confirmed_On__c,Commission_Paid_Date__c from client_product_service__c where id in :new list<string>(payInvoice.Instalments__c.split(';'))];
			for(client_product_service__c p : upProds){

				if(!p.Refund_Confirmed_ADM__c){
					p.Provider_Payment__c = payInvoice.Id;
					p.Paid_to_Provider_by__c = UserInfo.getUserId();
					// p.Paid_to_Provider_by_Agency__c = currentUser.Contact.AccountId;
					p.Paid_to_Provider_by_Agency__c = selectedAgency;
					p.Paid_to_Provider_on__c = system.now();
					//Retain Product Commission if NET
					if(p.Claim_Commission_Type__c == 'Net commission'){
						p.Commission_Confirmed_By__c = Userinfo.getuserId();
						p.Commission_Confirmed_by_Agency__c = currentUser.Contact.AccountId;
						p.Commission_Confirmed_On__c = system.now();
						p.Commission_Paid_Date__c = system.today();
					}
				}
				else{
					p.Provider_Refund_Payment__c = payInvoice.Id;
					p.Paid_Refund_to_Provider_By__c = UserInfo.getUserId();
					//p.Paid_to_Provider_by_Agency__c = currentUser.Contact.AccountId;
					p.Paid_to_Provider_by_Agency__c = selectedAgency;
					p.Refund_Paid_to_ProviderOn__c = system.now();
				}
				p.isSelected__c = false;
			}

			update upProds;

			//s3Folder = payInvoice.Finance_Global_Link__c + '/Providers/' + payInvoice.Provider__c + '/payments/' + payInvoice.Id;
			invoicesToPay();
			searchProducts();
		}
		catch(Exception e){
			Database.rollback(sp);
			system.debug('Error: ' + e.getMessage() + ' line--' + e.getLineNumber());
		}
	}

	public void deleteInvoice(){
			string invoiceId = ApexPages.currentPage().getParameters().get('inv');
			Invoice__c invoiceDelete = [Select id, Instalments__c from Invoice__c where id = :invoiceId];
			list<client_product_service__c> toUpdate =  [Select id, Provider_Payment__c, Paid_to_Provider_by__c, Paid_to_Provider_on__c, Provider_Refund_Payment__c, Paid_Refund_to_Provider_By__c, Paid_to_Provider_by_Agency__c, Refund_Paid_to_ProviderOn__c from client_product_service__c where id in :new list<string>(invoiceDelete.Instalments__c.split(';'))];
			for(client_product_service__c p :toUpdate ){
					p.Provider_Payment__c = null;
					p.Paid_to_Provider_by__c = null;
					p.Paid_to_Provider_on__c = null;
					p.Provider_Refund_Payment__c = null;
					p.Paid_Refund_to_Provider_By__c = null;
					p.Paid_to_Provider_by_Agency__c = null;
					p.Refund_Paid_to_ProviderOn__c = null;
			}
			update toUpdate;
			delete invoiceDelete;
			invoicesToPay();
			searchProducts();
	}

	public void loadEmail(){
		string invoiceId = ApexPages.currentPage().getParameters().get('inv');
		system.debug('invoiceId==> '+invoiceId);
		payInvoice = listInvoices.get(invoiceId);
		Invoice__c inv = [SELECT Preview_Link__c, Provider__r.Invoice_to_Email__c FROM Invoice__c WHERE id = :invoiceId limit 1];

		findEmailTemplates();
		emTo = inv.Provider__r.Invoice_to_Email__c;
		
		if(inv.preview_link__c != null)
			payDocs = IPFunctions.loadPreviewLink(inv.preview_link__c);
		
		if(emailOptions != null && emailOptions.size()>0){
			selectedTemplate = emailOptions[0].getValue();
			changeTemplate();
			emCont = tempBody;
		}
		else{
			emSubj = '';
			emCont = '' + getSignature();
		}
	}

	public list<IPFunctions.s3Docs> payDocs {get;set;}

	public void sendEmail(){
		String emailService = IpFunctions.getS3EmailService();
		mandrillSendEmail mse = new mandrillSendEmail();
		mandrillSendEmail.messageJson email_md = new mandrillSendEmail.messageJson();
		string invoiceId = ApexPages.currentPage().getParameters().get('inv');
		payInvoice = listInvoices.get(invoiceId);
		blob pdfFile = null;
		system.debug('payInvoice2==> '+payInvoice);
		PageReference pr = Page.provider_payment_invoice;
		pr.getParameters().put('id', payInvoice.id);

		if(!Test.isRunningTest()){
			pdfFile = pr.getContentAsPDF();
			email_md.setAttachment('application/pdf', 'Invoice Confirmation.pdf', pdfFile);
		}

		email_md.setFromEmail(currentUser.Contact.Email);
		email_md.setFromName(currentUser.Contact.Name);
		list<String> toAd;
		list<String> toCc;

		if(emTo.contains(';')){
			toAd = emTo.split(';', 0);
			for(String t : toAd)
				if(!String.isBlank(t))
					email_md.setTo(t, '');
		}
		else if(emTo.contains(',')){
			toAd = emTo.split(',', 0);
			for(String t : toAd)
				if(!String.isBlank(t))
					email_md.setTo(t, '');
		}else{
			email_md.setTo(emTo, '');
		}

		if (emCc != null && emCc != ''){
			emCc = emCc.replaceAll(',',';').replaceAll('\n',';');
			toCc = emCc.split(';', 0);
			for(String c_c : toCc) 
				email_md.setCc(c_c, '');
		}

		//Send a copy to the sender
		email_md.setCc(currentUser.Contact.Email, '');
		email_md.setSubject(emSubj);
		email_md.preserve_recipients(true);

		emCont = emCont.replace('<style type="text/css">', '<head><style>').replace('</style>', '</style></head>');
		email_md.setHtml(emCont);

	
		if(!Test.isRunningTest())
			mse.sendMailNoFuture(email_md);

		payInvoice.Sent_Email_Receipt__c = true;
		payInvoice.Sent_Email_Receipt_By__c = UserInfo.getUserId();
		payInvoice.Sent_Email_Receipt_On__c = DateTime.now();
		payInvoice.Sent_Email_To__c = emTo.replace(';','; ').replace(',', ', ');
		update payInvoice;
		invoicesToPay();

		 // Save to S3
		 EmailToS3Controller s3 = new EmailToS3Controller();
			DateTime currentTime = DateTime.now();
			String myDate = currentTime.format('dd-MM-yyyy HH:mm:ss');
		if(!Test.isRunningTest()){
			Blob bodyblob = Blob.valueof(emCont);//Blob body, String subject, String instalment, Contact client, Contact employee, String dateCreated
			string sendEmailTo = emTo;
			if(toAd != null && toAd.size() > 0)
				sendEmailTo = toAd[0];
			system.debug('==> bodyblob: '+bodyblob);
			system.debug('==> payInvoice.provider__c: '+payInvoice.provider__c);
			system.debug('==> payInvoice.id: '+payInvoice.id);
			system.debug('==> emSubj: '+emSubj);
			system.debug('==> myDate: '+myDate);
			system.debug('==> userInfo.getUserId(): '+userInfo.getUserId());
			system.debug('==> sendEmailTo: '+sendEmailTo);
			s3.saveProviderPaymentEmailS3(bodyblob, payInvoice.provider__c, payInvoice.id, emSubj, myDate, userInfo.getUserId(), sendEmailTo);
		}
	}

	//Email Templates
	public String selectedTemplate {get;set;}
	public list<SelectOption> emailOptions {get; set;}
	public map<Id, Email_Template__c> mEmailTemplate {get;set;}

	public String tempBody {get;set;}
	private String paymentRef;
	private sObject objTemplate;
	private Back_Office_Control__c userBo;
	private list<string> fieldsTemplate;
	private map<string, Object> mapFields;
	private map<Id,String> mapSubject;

	private void findEmailTemplates(){

		mapFields = new map<string, Object>();


		mapFields = IPFunctions.findTokens(IPFunctions.tokensInvoice);
		system.debug('==>mapFields: '+mapFields);
		system.debug('==>payInvoice.id: '+payInvoice.id);
		list<sObject> listResult = emailTemplateDynamic.createListResult(' FROM invoice__c WHERE id = \''+payInvoice.id +'\'', mapFields, null, new list<String>(mapFields.keySet()));	
		objTemplate = listResult[0];

		fieldsTemplate = new list<String>(mapFields.keySet());

		emailOptions = new list<SelectOption>();
		mEmailTemplate = new map<Id, Email_Template__c>();
		mapSubject = new map<Id, String>();
		String mSubject;

		for(Email_Template__c et : [SELECT Email_Subject__c, Template__c, Template_Description__c FROM Email_Template__c WHERE Agency__c = :currentUser.Contact.AccountId and Category__c = 'Provider Payment' order by Email_Subject__c]){
			mSubject = emailTemplateDynamic.createSingleTemplate(mapFields, et.Email_Subject__c, fieldsTemplate, objTemplate);
			mapSubject.put(et.id, mSubject);
			emailOptions.add(new SelectOption(et.Id, et.Template_Description__c));
			mEmailTemplate.put(et.id, et);
		}//end for
	}

	public void changeTemplate(){
		system.debug('==>passou');
		emSubj =  mapSubject.get(selectedTemplate);
		tempBody = mEmailTemplate.get(selectedTemplate).Template__c;
		system.debug('==>tempBody: '+tempBody);
		system.debug('==>objTemplate: '+objTemplate);
		tempBody = emailTemplateDynamic.createSingleTemplate(mapFields, tempBody, fieldsTemplate, objTemplate);
		// tempBody += '<p>' + payInvoice.id + ' - '+ payInvoice.Agency_Currency_Value__c + ' - '+ payInvoice.Paid_Date__c + '</p>';
		system.debug('==>tempBody2: '+tempBody);
		tempBody += getSignature();

	}

	/** Signature **/
	public string getSignature(){
		String sig = '';

		if(payDocs != null && payDocs.size()>0){
			sig += ' <div class="attachments"><b>Attachments</b>';	
			sig += ' <br/> ';	

			for(IPFunctions.s3Docs d : payDocs){
				sig += ' <a href="'+ d.docUrl+'" target="_blank">'+d.docName + '</a> ';	
				sig += ' <br/> ';	
			}//end for


			sig += ' </div>';	
		}

	    if(currentUser.Contact.Signature__c != null && currentUser.Contact.Signature__c != '' )
	      sig += currentUser.Contact.Signature__c;
	    else 
			sig += textSignature;

		return sig;
	}

	public String textSignature {
	  	get{
	  		if(textSignature == null){
	  			textSignature = '<div style="margin-top: 20px;" class="sig">' + currentUser.Contact.Name;
	            textSignature += '<br />' + currentUser.Contact.Account.Name;
	            textSignature += '<br />' + currentUser.Contact.Email + ' </div>';
	  		}
	  		return textSignature;
	  	}set;}



	private without sharing class noSharing{

		public void saveInvoice(Invoice__c payInvoice, User currentUser){

			payInvoice.Finance_Global_Link__c = currentUser.Contact.Account.Global_Link__c;
			upsert payInvoice;
		}

	}

	// ---------------------------------------------------	FILTERS

	public string receivedDate{get;set;}

	// Agency Group Options
	public string selectedAgencyGroup{
    	get{
    		if(selectedAgencyGroup == null)
    			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
    		return selectedAgencyGroup;
    	}set;}
	public List<SelectOption> agencyGroupOptions {
  		get{
   			if(agencyGroupOptions == null){
   				
   				agencyGroupOptions = new List<SelectOption>();  
   				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group  
	    			for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:currentUser.Contact.AccountId order by Parent.Name]){
		     			agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
		    		}
   				}else{
   					for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
		     			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
		    		}
   				}
	   		}
	   		return agencyGroupOptions;
	  }set;}

	public void changeGroup(){
		agencyOptions = null;
	}

	// Agency Options
	public String selectedAgency {
		get{
			if(selectedAgency == null)
				selectedAgency = currentUser.Contact.AccountId;
			return selectedAgency;
		}
		set;
	}
	public map<String,String> mapAgCurrency {get;set;}
	public list<SelectOption> agencyOptions {get{
		if(agencyOptions == null){
			mapAgCurrency = new map<String,String>();
			agencyOptions = new List<SelectOption>();
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
				for(Account a: [Select Id, Name, Account_Currency_Iso_Code__c from Account where id =:currentUser.Contact.AccountId order by name]){
					if(selectedAgency == null)
						selectedAgency = a.Id;					
					agencyOptions.add(new SelectOption(a.Id, a.Name));
					mapAgCurrency.put(a.Id, a.Account_Currency_Iso_Code__c);
				}
				if(currentUser.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
						agencyOptions.add(new SelectOption(ac.id, ac.name));
						mapAgCurrency.put(ac.Id, ac.Account_Currency_Iso_Code__c);
					}
				}
			}else{
				for(Account a: [Select Id, Name, Account_Currency_Iso_Code__c from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
					agencyOptions.add(new SelectOption(a.Id, a.Name));
					mapAgCurrency.put(a.Id, a.Account_Currency_Iso_Code__c);
					if(selectedAgency == null)
						selectedAgency = a.Id;	
				}
			}

			findProviders();

		}

		return agencyOptions;
		
	}set;}

	public void changeAgency(){
		findProviders();
	}


	//Providers
	public string selectedProvider{
    	get{
    		if(selectedProvider == null)
    			selectedProvider = 'none';
    		return selectedProvider;
    	}set;}
	public List<SelectOption> providersOpt {get;set;}
	public void findProviders(){
		providersOpt = new List<SelectOption>();
		providersOpt.add(new SelectOption('none','-- Select Provider --'));

		for(AggregateResult ag : [SELECT Products_Services__r.Provider__c pId, Products_Services__r.Provider__r.Provider_Name__c pName FROM client_product_service__c WHERE ( (Received_By_Agency__c = :selectedAgency AND Provider_Payment__c = NULL AND Paid_to_Provider_on__c = NULL) OR (Paid_to_Provider_by_Agency__c = :selectedAgency AND Refund_Confirmed_ADM__c = TRUE AND Refund_Paid_to_ProviderOn__c = NULL AND Agency_Refund_Client__c = 'No' ) ) group by Products_Services__r.Provider__c, Products_Services__r.Provider__r.Provider_Name__c]){
			if((String) ag.get('pId') != null)
				providersOpt.add(new SelectOption((String) ag.get('pId'), (String) ag.get('pName')));
		}//end for

		selectedProvider = 'none';
		findCategories();
	}

	public void changeProvider(){
		findCategories();
	}


	//Providers Category
	public string selectedCategory{get;set;}

	public List<SelectOption> categoriesOpt {get;set;}
	public void findCategories(){
		categoriesOpt = new List<SelectOption>();
		string sql = 'SELECT Products_Services__r.Category__c cat FROM client_product_service__c WHERE (Received_By_Agency__c = :selectedAgency OR Paid_to_Provider_by_Agency__c = :selectedAgency) ';
		if(selectedProvider != 'none')
			sql += ' AND Products_Services__r.Provider__c = :selectedProvider ';
		else sql += ' AND Products_Services__r.Provider__c != null ';
		sql += ' and Products_Services__r.Category__c != null ';
		sql += ' AND ((Provider_Payment__c = NULL AND Paid_to_Provider_on__c = NULL) OR (Refund_Confirmed_ADM__c = TRUE AND Refund_Paid_to_ProviderOn__c = NULL AND Agency_Refund_Client__c = \'No\')) group by Products_Services__r.Category__c ';
		system.debug('==>sql: '+sql);
		categoriesOpt.add(new SelectOption('all','-- All --'));
		for(AggregateResult ag : Database.query(sql)){
			categoriesOpt.add(new SelectOption((String) ag.get('cat'), (String) ag.get('cat')));
		}//end for
		
		// else{
		// 	categoriesOpt.add(new SelectOption('none','-- Select Provider --'));
		// }

		selectedCategory = 'all';

		//if(selectedProvider != 'none')
			findCatProducts();
		// else{
		// 	agencyProducts = new List<SelectOption>();
		// 	agencyProducts.add(new SelectOption('none','-- Select Provider --'));
		// }
	}

	public void changeCategory(){
		findCatProducts();
	}


	// Products
	public string selectedProduct{get;set;}
	public List<SelectOption> agencyProducts {get;set;}

	public void findCatProducts(){
		agencyProducts = new List<SelectOption>();
		agencyProducts.add(new SelectOption('all','-- All --'));

		String sql = 'SELECT Products_Services__c pId, Product_Name__c nm FROM client_product_service__c WHERE (Received_By_Agency__c = \'' + selectedAgency  + '\' OR  Paid_to_Provider_by_Agency__c = \'' + selectedAgency  + '\' ) ';
		
		if(selectedProvider != 'none')
			sql += ' AND Products_Services__r.Provider__c = \'' + selectedProvider + '\'';
		else sql += ' AND Products_Services__r.Provider__c != null ';

		sql += ' and Products_Services__c != null ';
		if(selectedCategory != null && selectedCategory != 'all')
			sql += ' AND ( Products_Services__r.Category__c = \'' + selectedCategory + '\' OR Products_Services__r.Category__c = NULL)';

		sql += ' AND ((Provider_Payment__c = NULL AND Paid_to_Provider_on__c = NULL) OR (Refund_Confirmed_ADM__c = TRUE AND Refund_Paid_to_ProviderOn__c = NULL AND Agency_Refund_Client__c = \'No\')) group by Products_Services__c, Product_Name__c ';

		system.debug('products sql ==> ' + sql);

		for(AggregateResult ag : Database.query(sql)){
			agencyProducts.add(new SelectOption((String) ag.get('pId'), (String) ag.get('nm')));
		}//end for

		selectedProduct = 'all';
	}
	
}