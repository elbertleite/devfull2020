public with sharing class Batch_Migrate_Stage1_To_Stage0{
	public Batch_Migrate_Stage1_To_Stage0(){
		system.debug('');
	}
}
/*global class Batch_Migrate_Stage1_To_Stage0 implements Database.Batchable<sObject> {
  
  global Map<String, Client_Stage__c> itensStageOne;
	global Map<String, Client_Stage__c> itensStageZero;
  global Map<String, String> fromStageOneToZero;
  global String ipMexico;
  global String query;
  
  //BATCH EXECUTED FOR IP MEXICO//
  global Batch_Migrate_Stage1_To_Stage0() {
    this.ipMexico = '0019000001NaqlE';
    
    itensStageOne = new Map<String, Client_Stage__c>();
    itensStageZero = new Map<String, Client_Stage__c>();
    
    fromStageOneToZero = new Map<String, String>();

    fromStageOneToZero.put('PRIMER CONTACTO', 'CONTACT'); 
		fromStageOneToZero.put('NEW LEAD', 'NEW LEAD'); 
		fromStageOneToZero.put('PROSPECT', 'PROSPECT'); 
		fromStageOneToZero.put('CONTACT', 'CONTACT'); 
		fromStageOneToZero.put('CLOSING', 'CLOSING'); 
		fromStageOneToZero.put('CLIENT', 'CLIENT'); 
		fromStageOneToZero.put('LEAD - NOT INTERESTED', 'LEAD - NOT INTERESTED'); 
		fromStageOneToZero.put('2ND CONTACT', 'CONTACT'); 
		fromStageOneToZero.put('SUSPECT', 'PROSPECT');
		fromStageOneToZero.put('QUOTE SENT', 'QUOTE SENT');
		fromStageOneToZero.put('NEGOTIATION', 'NEGOTIATION');
		fromStageOneToZero.put('DEPOSIT CHARGED', 'CLOSING');
		fromStageOneToZero.put('DEPOSIT RECEIVED', 'CLOSING');
		fromStageOneToZero.put('COTIZACIÓN', 'QUOTE SENT');
		fromStageOneToZero.put('NEGOCIACIÓN', 'NEGOTIATION');
		fromStageOneToZero.put('CIERRE DE INSCRIPCIÓN', 'CLOSING');
		fromStageOneToZero.put('PAGO DE INSCRIPCIÓN', 'CLOSING');
		fromStageOneToZero.put('1ST CONTACT', 'CONTACT');

    String [] stages = new String [] {'Stage 0','Stage 1'};
    for(Client_Stage__c stage : [SELECT ID, Stage_description__c, Stage__c, itemOrder__c FROM Client_Stage__c WHERE Stage__c IN :stages AND Agency_Group__c = :ipMexico ORDER BY itemOrder__c DESC]){
			if(stage.Stage__c == 'Stage 0'){
				itensStageZero.put(stage.Stage_description__c.toUpperCase().trim(), stage);
			}else{
				itensStageOne.put(stage.Stage_description__c.toUpperCase().trim(), stage);
			}
		}
  }
  
  global Database.QueryLocator start(Database.BatchableContext BC) {
    //String [] ids = new String [] {'0039000002R0C9aAAF','0032v00002a7BSIAA2','00390000027R2VeAAK','0039000002OcqzJAAR','00390000024KC3AAAW','00390000020qCNpAAM','0039000002OMTvLAAX','0039000002BRgeJAAT','0039000002Ph4FiAAJ','0039000002PDNORAA5','0032v00002a7GPgAAM','0032v00002a6Xi1AAE','0032v00002a7LIEAA2'};
    String query = 'SELECT ID FROM Contact WHERE Current_Agency__r.Parent.ID = :ipMexico AND RecordType.Name IN (\'Lead\',\'Client\')';
    return Database.getQueryLocator(query);
  }

  global void execute(Database.BatchableContext BC, List<Contact> contacts) {

    	Map<String, List<Client_Stage_Follow_Up__c>> itensStageOneToDelete = new Map<String, List<Client_Stage_Follow_Up__c>>(); 
      Map<String, List<Client_Stage_Follow_Up__c>> newFollowUpsStageZero = new Map<String, List<Client_Stage_Follow_Up__c>>(); 
      Map<String, Set<String>> stageZeroItensContactItens = new Map<String, Set<String>>();
      Map<String, Contact> contactsToUpdate = new Map<String, Contact>();
      
      Client_Stage_Follow_Up__c newFollowUpStageZero;

      Contact contactToUpdate;

      String correspondentStageZeroItem;
      String stageOneToZeroItem;

      Set<String> ids = new Set<String>(); 
      for(Contact ctt : contacts){
        ids.add(ctt.ID);
      }

      for(Client_Stage_Follow_Up__c checkedItensStageZero : [SELECT ID, Stage_Item__c, Client__c FROM Client_Stage_Follow_up__c WHERE Destination_Tracking__r.Current_Cycle__c = true AND Destination_Tracking__r.Agency_Group__c = :ipMexico AND Client__c IN :ids AND Stage__c = 'Stage 0']){
        if(!stageZeroItensContactItens.containsKey(checkedItensStageZero.Client__c)){
          stageZeroItensContactItens.put(checkedItensStageZero.Client__c, new Set<String>());
        }
        stageZeroItensContactItens.get(checkedItensStageZero.Client__c).add(checkedItensStageZero.Stage_Item__c.toUpperCase().trim());
      }

      for(Client_Stage_Follow_Up__c followUp : [SELECT ID, Stage_Item__c, Checked_By__c, Client__c, Client__r.ID, Client__r.Lead_Stage__c, client__r.Status__c, Agency_Group__c, agency__c, Destination_Tracking__c, Destination__c, Last_Saved_Date_Time__c FROM Client_Stage_Follow_up__c WHERE Destination_Tracking__r.Current_Cycle__c = true AND Destination_Tracking__r.Agency_Group__c = :ipMexico AND Client__c IN :ids AND Stage__c = 'Stage 1' ORDER BY Client__c, Last_Saved_Date_Time__c]){
			
        if(!newFollowUpsStageZero.containsKey(followUp.Client__r.ID)){
          newFollowUpsStageZero.put(followUp.Client__r.ID, new List<Client_Stage_Follow_Up__c>());
          itensStageOneToDelete.put(followUp.Client__r.ID, new List<Client_Stage_Follow_Up__c>());   
        }

        if(!stageZeroItensContactItens.containsKey(followUp.Client__r.ID)){
          stageZeroItensContactItens.put(followUp.Client__r.ID, new Set<String>());
        }
        
        correspondentStageZeroItem = fromStageOneToZero.get(followUp.Stage_Item__c.toUpperCase().trim());

        if(!String.isEmpty(correspondentStageZeroItem)){
          if(!stageZeroItensContactItens.get(followUp.Client__r.ID).contains(correspondentStageZeroItem)){
            
            newFollowUpStageZero = new Client_Stage_Follow_Up__c();
            newFollowUpStageZero.Checked_By__c = followUp.Checked_By__c;
            newFollowUpStageZero.Client__c = followUp.Client__c;
            newFollowUpStageZero.Agency_Group__c = followUp.Agency_Group__c;
            newFollowUpStageZero.agency__c = followUp.agency__c;
            newFollowUpStageZero.Stage_Item__c = itensStageZero.get(correspondentStageZeroItem).Stage_description__c;
            newFollowUpStageZero.Stage_Item_Id__c = itensStageZero.get(correspondentStageZeroItem).ID;
            newFollowUpStageZero.Stage__c = itensStageZero.get(correspondentStageZeroItem).Stage__c;
            newFollowUpStageZero.Destination__c = followUp.Destination__c;
            newFollowUpStageZero.Destination_Tracking__c = followUp.Destination_Tracking__c;
            newFollowUpStageZero.Last_Saved_Date_Time__c = followUp.Last_Saved_Date_Time__c;
            
            newFollowUpsStageZero.get(followUp.Client__r.ID).add(newFollowUpStageZero);

            stageZeroItensContactItens.get(followUp.Client__r.ID).add(correspondentStageZeroItem);
          }	
          itensStageOneToDelete.get(followUp.Client__r.ID).add(followUp);
        }

        if(followUp.Client__r.Lead_Stage__c == 'Stage 1' && !String.isEmpty(followUp.Client__r.Status__c) && fromStageOneToZero.containsKey(followUp.Client__r.Status__c.toUpperCase().trim())){
          if(!contactsToUpdate.containsKey(followUp.Client__r.ID)){
            contactToUpdate = new Contact(ID = followUp.Client__r.ID);
            contactToUpdate.Lead_Stage__c = 'Stage 0';
            stageOneToZeroItem = fromStageOneToZero.get(followUp.Client__r.Status__c.toUpperCase().trim());
            if(itensStageZero.get(stageOneToZeroItem) != null){
              contactToUpdate.Status__c = itensStageZero.get(stageOneToZeroItem).Stage_description__c;
            }else{
              contactToUpdate.Status__c = stageOneToZeroItem;
            }
            contactsToUpdate.put(followUp.Client__r.ID, contactToUpdate);
          }
        }

      }

      List<Client_Stage_Follow_Up__c> toDelete = new List<Client_Stage_Follow_Up__c>();
      List<Client_Stage_Follow_Up__c> toInsert = new List<Client_Stage_Follow_Up__c>();

      for(String ctt : itensStageOneToDelete.keySet()){
        for(Client_Stage_Follow_up__c check : itensStageOneToDelete.get(ctt)){
            toDelete.add(check);
        }
      }

      for(String ctt : newFollowUpsStageZero.keySet()){
        for(Client_Stage_Follow_up__c check : newFollowUpsStageZero.get(ctt)){
            toInsert.add(check);
        }
      }

      //delete toDelete;
      insert toInsert;
      update contactsToUpdate.values();
      
  }  
  global void finish(Database.BatchableContext BC) {}
  
}*/


  //END OF BATCH EXECUTED FOR IP MEXICO//

  //BATCH EXECUTED FOR IP AUSTRALIA//
  /*global void execute(Database.BatchableContext BC, List<Contact> ctts) {
    List<String> ids = new List<String>();
    for(Contact ctt : ctts){
      ids.add(ctt.ID);
    }

    Map<String, String> stageZeroIDs = new Map<String, String>();
    for(Client_Stage__c stage : [SELECT ID, Stage_description__c FROM Client_Stage__c WHERE Stage__c = 'Stage 0' AND Agency_Group__c = :agencyGroup]){
      stageZeroIDs.put(stage.Stage_description__c, stage.ID);
    }
    Set<String> stagesToSearch = new Set<String>();
	  stagesToSearch.add('Stage 2');
	  stagesToSearch.add('Stage 1');

    List<Client_Stage_Follow_up__c> checklist = [SELECT ID, Client__c, Agency_Group__c, Agency__c, Destination__c, Destination_Tracking__c, Stage_Item__c, Stage_Item_Id__c, Stage__c, Last_Saved_Date_Time__c, CreatedBy.Contact.Account.Parent.ID FROM Client_Stage_Follow_up__c WHERE Agency_Group__c = :agencyGroup and Client__c IN :ids AND Stage__c IN :stagesToSearch AND Destination_Tracking__r.Current_Cycle__c  = true];


    List<StatusesToCreate> statusesToCreate = new List<StatusesToCreate>();
    Map<String, Map<String, Client_Stage_Follow_up__c>> checkListPerContact = new Map<String, Map<String, Client_Stage_Follow_up__c>>();

    if(checklist != null && !checklist.isEmpty()){
      for(Client_Stage_Follow_up__c followUp : checklist){
        if(!checkListPerContact.containsKey(followUp.Client__c)){
          checkListPerContact.put(followUp.Client__c, new Map<String, Client_Stage_Follow_up__c>());
        }
        checkListPerContact.get(followUp.Client__c).put(followUp.Stage_Item__c, followUp);
      }
      if(!checkListPerContact.isEmpty()){
        for(String contactID : checkListPerContact.keySet()){
          statusesToCreate.add(new StatusesToCreate(contactID, checkListPerContact.get(contactID), stageZeroIDs));
        }
      }

      if(!statusesToCreate.isEmpty()){
        Client_Stage_Follow_Up__c followUp;
        List<Client_Stage_Follow_Up__c> itensToInsert = new List<Client_Stage_Follow_Up__c>();
        for(StatusesToCreate newStatuses : statusesToCreate){
          if(!newStatuses.listStatusesToCreate.isEmpty()){
            itensToInsert.addAll(newStatuses.listStatusesToCreate);
          }
        }
        if(!itensToInsert.isEmpty()){
          insert itensToInsert;
        }
      }
    }
  }*/

  /*public class StatusesToCreate{
      public String contact{get;set;}

      List<Client_Stage_Follow_up__c> listStatusesToCreate{get;set;}

      public StatusesToCreate(String contact, Map<String, Client_Stage_Follow_up__c> contactChecklist, Map<String, String> stageZeroIDs){
        listStatusesToCreate = new List<Client_Stage_Follow_up__c>();
        Client_Stage_Follow_up__c newCheck;
        Client_Stage_Follow_up__c oldCheck;
        Boolean isNewLead = false;

        Set<String> checklist = contactChecklist.keySet();
        String [] itensChecklist;
        if(Test.isRunningTest()){
          checklist = new Set<String>();
          checklist.add('Potential Client - Walk in');
          checklist.add('PRE-SALE - 1st email');
          checklist.add('PRE-SALE - 2nd email');
          checklist.add('PRE-SALE - 3rd email');
          checklist.add('QUOTATION - Sent');
          checklist.add('QUOTATION - Follow up');
          isNewLead = true;
        }
        if(checklist.contains('Potential Client - Walk in') || checklist.contains('Potential Client - Membership') || checklist.contains('Potential Client - New Lead') || checklist.contains('Potential Client - Online')){
          isNewLead = true;
          itensChecklist = new String [] {'Potential Client - Walk in', 'Potential Client - Membership', 'Potential Client - New Lead', 'Potential Client - Online'};
          for(String item : itensChecklist){
            if(contactChecklist.containsKey(item)){
              oldCheck = contactChecklist.get(item);
              break;
            }
          }
          if(Test.isRunningTest()){
            oldCheck = new Client_Stage_Follow_up__c();  
          }
          newCheck = new Client_Stage_Follow_up__c();
          newCheck.Checked_By__c = UserInfo.getUserId();
          newCheck.Client__c = contact;
          newCheck.Stage_Item__c = 'NEW LEAD';
          newCheck.Stage_Item_Id__c = stageZeroIDs.get('NEW LEAD');
          newCheck.Stage__c = 'Stage 0';
          newCheck.Agency_Group__c = oldCheck.Agency_Group__c;
          newCheck.agency__c = oldCheck.Agency__c;
          newCheck.Destination__c = oldCheck.Destination__c;
          newCheck.Destination_Tracking__c = oldCheck.Destination_Tracking__c;
          newCheck.Last_Saved_Date_Time__c = oldCheck.Last_Saved_Date_Time__c;

          listStatusesToCreate.add(newCheck);
        }

        if(isNewLead){

          if(checklist.contains('PRE-SALE - 1st email') || checklist.contains('PRE-SALE - 1st call') || checklist.contains('PRE-SALE - 1st sms')){
            itensChecklist = new String [] {'PRE-SALE - 1st email', 'PRE-SALE - 1st call', 'PRE-SALE - 1st sms'};
            for(String item : itensChecklist){
              if(contactChecklist.containsKey(item)){
                oldCheck = contactChecklist.get(item);
                break;
              }
            }
            if(Test.isRunningTest()){
              oldCheck = new Client_Stage_Follow_up__c();  
            }
            newCheck = new Client_Stage_Follow_up__c();
            newCheck.Checked_By__c = UserInfo.getUserId();
            newCheck.Client__c = contact;
            newCheck.Agency_Group__c = oldCheck.Agency_Group__c;
            newCheck.agency__c = oldCheck.Agency__c;
            newCheck.Stage_Item__c = '1st Contact';
            newCheck.Stage_Item_Id__c = stageZeroIDs.get('1st Contact');
            newCheck.Stage__c = 'Stage 0';
            newCheck.Destination__c = oldCheck.Destination__c;
            newCheck.Destination_Tracking__c = oldCheck.Destination_Tracking__c;
            newCheck.Last_Saved_Date_Time__c = oldCheck.Last_Saved_Date_Time__c;

            listStatusesToCreate.add(newCheck);
          }

          if(checklist.contains('PRE-SALE - 2nd email') || checklist.contains('PRE-SALE - 2nd call') || checklist.contains('PRE-SALE - 2nd sms')){
            itensChecklist = new String [] {'PRE-SALE - 2nd email', 'PRE-SALE - 2nd call', 'PRE-SALE - 2nd sms'};
            for(String item : itensChecklist){
              if(contactChecklist.containsKey(item)){
                oldCheck = contactChecklist.get(item);
                break;
              }
            }
            if(Test.isRunningTest()){
              oldCheck = new Client_Stage_Follow_up__c();  
            }
            newCheck = new Client_Stage_Follow_up__c();
            newCheck.Checked_By__c = UserInfo.getUserId();
            newCheck.Client__c = contact;
            newCheck.Agency_Group__c = oldCheck.Agency_Group__c;
            newCheck.agency__c = oldCheck.Agency__c;
            newCheck.Stage_Item__c = '2nd Contact';
            newCheck.Stage_Item_Id__c = stageZeroIDs.get('2nd Contact');
            newCheck.Stage__c = 'Stage 0';
            newCheck.Destination__c = oldCheck.Destination__c;
            newCheck.Destination_Tracking__c = oldCheck.Destination_Tracking__c;
            newCheck.Last_Saved_Date_Time__c = oldCheck.Last_Saved_Date_Time__c;

            listStatusesToCreate.add(newCheck);
          }
          
          if(checklist.contains('PRE-SALE - 3rd email') || checklist.contains('PRE-SALE - 3rd call') || checklist.contains('PRE-SALE - 3rd sms')){
            itensChecklist = new String [] {'PRE-SALE - 3rd email', 'PRE-SALE - 3rd call', 'PRE-SALE - 3rd sms'};
            for(String item : itensChecklist){
              if(contactChecklist.containsKey(item)){
                oldCheck = contactChecklist.get(item);
                break;
              }
            }
            if(Test.isRunningTest()){
              oldCheck = new Client_Stage_Follow_up__c();  
            }
            newCheck = new Client_Stage_Follow_up__c();
            newCheck.Checked_By__c = UserInfo.getUserId();
            newCheck.Client__c = contact;
            newCheck.Agency_Group__c = oldCheck.Agency_Group__c;
            newCheck.agency__c = oldCheck.Agency__c;
            newCheck.Stage_Item__c = '3rd Contact';
            newCheck.Stage_Item_Id__c = stageZeroIDs.get('3rd Contact');
            newCheck.Stage__c = 'Stage 0';
            newCheck.Destination__c = oldCheck.Destination__c;
            newCheck.Destination_Tracking__c = oldCheck.Destination_Tracking__c;
            newCheck.Last_Saved_Date_Time__c = oldCheck.Last_Saved_Date_Time__c;

            listStatusesToCreate.add(newCheck);
          }

          if(checklist.contains('QUOTATION - Sent')){
            oldCheck = contactChecklist.get('QUOTATION - Sent'); 
            if(Test.isRunningTest()){
              oldCheck = new Client_Stage_Follow_up__c();  
            }
            newCheck = new Client_Stage_Follow_up__c();
            newCheck.Checked_By__c = UserInfo.getUserId();
            newCheck.Client__c = contact;
            newCheck.Agency_Group__c = oldCheck.Agency_Group__c;
            newCheck.agency__c = oldCheck.Agency__c;
            newCheck.Stage_Item__c = 'LEAD - Quote Sent';
            newCheck.Stage_Item_Id__c = stageZeroIDs.get('LEAD - Quote Sent');
            newCheck.Stage__c = 'Stage 0';
            newCheck.Destination__c = oldCheck.Destination__c;
            newCheck.Destination_Tracking__c = oldCheck.Destination_Tracking__c;
            newCheck.Last_Saved_Date_Time__c = oldCheck.Last_Saved_Date_Time__c;

            listStatusesToCreate.add(newCheck);
          }
          if(checklist.contains('QUOTATION - Follow up')){
            oldCheck = contactChecklist.get('QUOTATION - Follow up'); 
            if(Test.isRunningTest()){
              oldCheck = new Client_Stage_Follow_up__c();  
            }
            newCheck = new Client_Stage_Follow_up__c();
            newCheck.Checked_By__c = UserInfo.getUserId();
            newCheck.Client__c = contact;
            newCheck.Agency_Group__c = oldCheck.Agency_Group__c;
            newCheck.agency__c = oldCheck.Agency__c;
            newCheck.Stage_Item__c = 'LEAD - Quote Follow Up';
            newCheck.Stage_Item_Id__c = stageZeroIDs.get('LEAD - Quote Follow Up');
            newCheck.Stage__c = 'Stage 0';
            newCheck.Destination__c = oldCheck.Destination__c;
            newCheck.Destination_Tracking__c = oldCheck.Destination_Tracking__c;
            newCheck.Last_Saved_Date_Time__c = oldCheck.Last_Saved_Date_Time__c;

            listStatusesToCreate.add(newCheck);
          }
        }

      }
  }*/
  //END OF BATCH EXECUTED FOR IP AUSTRALIA//