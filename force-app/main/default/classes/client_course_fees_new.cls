public class client_course_fees_new {
	
	public client_course__c clientCourse {get;set;}
	
	public client_course_fees_new(ApexPages.standardController controller){
		
	}
	
	private map<string, string> relatedPromotions = new map<string, string>();
	private map<string, client_course_fees__c> mapFees = new map<string, client_course_fees__c>();
	public void loadFees(){
		clientCourse = [Select id, campus_course__c, campus_course__r.campus__r.ParentID, Total_Extra_Fees__c, Total_Tuition__c, Total_Course__c, Original_Total_Course__c, Original_Total_Extra_Fees__c, Original_Total_Extra_Fees_Promotion__c, Total_Extra_Fees_Promotion__c, Commission_Type__c, Commission__c, Credit_Available__c, Credit_Related_To__c, Credit_Valid_For_School__c, Credit_Value__c, Client__r.name, Course_Name__c, Course_Length__c, Unit_Type__c, Start_Date__c, End_Date__c, School_Name__c, Campus_Name__c, CurrencyIsoCode__c, (SELECT Fee_Name__c, Number_Units__c, Total_Value__c, Unit_Type__c, Value__c, Original_Total_Value__c, Original_Value__c, Commission_Value__c, isPromotion__c, relatedPromotionFee__c, Extra_Fee_Discount_Value__c FROM client_courses_fees__r order by Fee_Name__c) FROM client_course__c  where id = :ApexPages.currentPage().getParameters().get('id')]; //,( Select id from client_course_instalments__r) 

		for(client_course_fees__c rp:clientCourse.client_courses_fees__r){
			if(rp.isPromotion__c && rp.relatedPromotionFee__c != null)
				relatedPromotions.put(rp.relatedPromotionFee__c, rp.id);
			if(!rp.isPromotion__c)
				mapFees.put(rp.Fee_Name__c, rp);
		}//end for
	}
	
	public client_course_fees__c newFee { 
		get {
			if(newFee == null)
				newFee = new client_course_fees__c(Client_Course__c = clientCourse.id, Extra_Fee_Discount_Value__c = 0);
			return newFee; 
		}
		set;
	}
	
	
	public client_course_fees__c newDiscount { 
		get {
			if(newDiscount == null)
				newDiscount = new client_course_fees__c(Client_Course__c = clientCourse.id);
			return newDiscount; 
		}
		set;
	}
	
	private List<String> extraFeeNames;
	public List<String> getExtraFeeNames(){
		
		if(extraFeeNames == null){
			extraFeeNames = new List<String>();
			
			for(Product__c prd : [SELECT Name__c, Product_Type__c FROM Product__c where Supplier__c = :clientCourse.campus_course__r.campus__r.Parentid order by Name])
				extraFeeNames.add(prd.Name__c);
			
		}
		return extraFeeNames;	
		
	}
	
	private List<SelectOption> numberOfUnits;
	public List<SelectOption> getNumberOfUnits() {
		if(numberOfUnits == null){
	        numberOfUnits = new List<SelectOption>();
			for(integer i = 1; i <= 52; i++)
	            numberOfUnits.add(new SelectOption(string.valueOf(i),string.valueOf(i)));
		}
        return numberOfUnits;
    }
    
    
   // public list<client_course_fees__c> listFeesToAdd{get{if(listFeesToAdd == null) listFeesToAdd = new list<client_course_fees__c>(); return listFeesToAdd;} set;}
    
    public string feeNameError{get; set;}
    
    public pageReference addNewFee(){
    	boolean checkFields = false;
    	feeNameError = null;
    	if(newFee.Fee_Name__c == null || newFee.Fee_Name__c.trim() == ''){
    		feeNameError = '<span style="font-weight:bold;">Error:</span> Name required';
    		checkFields = true;
    	}
    	if(newFee.Value__c == null || newFee.Value__c == 0){
    		newFee.Value__c.addError('Price required');
    		checkFields = true;
    	}
    	if(newFee.Total_Value__c == null || newFee.Total_Value__c == 0){
    		newFee.Total_Value__c.addError('Total required');
    		checkFields = true;
    	}
    	if(checkFields)
    		return null;
    	
    	if(mapFees.containsKey(newFee.Fee_Name__c)){
    		newFee.id = mapFees.get(newFee.Fee_Name__c).id;
    	}else newFee.Client_Course__c = clientCourse.id;
    	
    	if(newFee.Unit_Type__c == '0')
    		newFee.Unit_Type__c = null;
    		
    	newFee.Original_Value__c = newFee.Value__c;
    	newFee.Original_Total_Value__c = newFee.Total_Value__c;
    	
    	newFee.Original_Fee_Type__c = newFee.Fee_Type__c;
		newFee.Original_Number_Units__c = newFee.Number_Units__c;
		newFee.Original_Fee_Name__c = newFee.Fee_Name__c;
			
    	upsert newFee;

		String sNewFee = newFee.id + ';;' + newFee.Fee_Name__c + ';;' + (newFee.Total_Value__c - newFee.Extra_Fee_Discount_Value__c) + ';;0' + ';;' + (newFee.Total_Value__c - newFee.Extra_Fee_Discount_Value__c) + ';;0';


		//Update Course Totals
		clientCourse.Total_Extra_Fees__c += newFee.Total_Value__c;
		clientCourse.Total_Extra_Fees_Promotion__c += newFee.Extra_Fee_Discount_Value__c;
		clientCourse.Total_Course__c += (newFee.Total_Value__c - newFee.Extra_Fee_Discount_Value__c);
		clientCourse.Total_Extra_Fees_Unallocated__c = 0;
		clientCourse.Original_Total_Course__c += (newFee.Total_Value__c - newFee.Extra_Fee_Discount_Value__c);
		clientCourse.Original_Total_Extra_Fees__c += newFee.Total_Value__c;
		clientCourse.Original_Total_Extra_Fees_Promotion__c += newFee.Extra_Fee_Discount_Value__c;
		update clientCourse;

		//Add Fee to the first installment not paid
		String relatedFees;
		client_course_instalment__c upinst;
		for(client_course_instalment__c cci : [SELECT Id, Received_Date__c, Related_Fees__c, Instalment_Value__c, Extra_Fee_Value__c FROM client_course_instalment__c WHERE client_course__c = :clientCourse.Id order by Number__c, Due_Date__c]){
			if(cci.Received_Date__c == null){

				//Add related fee
				if(cci.Related_Fees__c != null)
					cci.Related_Fees__c += '!#' + sNewFee;
				else
					cci.Related_Fees__c = sNewFee;

				//Update totals
				cci.Instalment_Value__c += newFee.Total_Value__c;
				cci.Extra_Fee_Value__c += newFee.Total_Value__c;

				upinst = cci;
				break;
			}
		}//end for
		update upinst;
		//
		// resetInstallmets();	
		quoteFees = null;	
		newFee = null;
		loadFees();	
    	//listFeesToAdd.add(newFee);
    	//newFee = null;
    	return null;
    }
    
    /*
    public pageReference saveFees(){
    	if(listFeesToAdd.size() > 0){
    		
    		insert listFeesToAdd;
			resetInstallmets();	
    			
    		listFeesToAdd = new list<client_course_fees__c>();
    		quoteFees = null;	
    		loadFees();	
    	}
    		
    	return null;
    }
    */
    // private void resetInstallmets(){
    // 	double commValue = 0;
    // 	list<string> relatedFees = new list<string>();
	// 	string addRelatedFees = '';
		
	// 	decimal totalFees = 0;
	// 	decimal totalPromotions = 0;
	// 	for(client_course_fees__c f:[Select id, Total_Value__c, isPromotion__c, Fee_Name__c, Extra_Fee_Discount_Value__c from client_course_fees__c where client_course__c = :clientCourse.id]){
	// 		if(!f.isPromotion__c){
	// 			totalFees += f.Total_Value__c;
	// 			relatedFees.add(f.id + ';;' + f.Fee_Name__c + ';;' + (f.Total_Value__c - f.Extra_Fee_Discount_Value__c) + ';;0' + ';;' + (f.Total_Value__c - f.Extra_Fee_Discount_Value__c) + ';;0');
	// 		}else {
	// 			totalPromotions += f.Total_Value__c;
	// 		}
	// 	}
	// 	update new client_course__c(id = clientCourse.id, Total_Extra_Fees__c = totalFees, Total_Extra_Fees_Promotion__c = totalPromotions, Total_Course__c = clientCourse.Total_Tuition__c + totalFees - totalPromotions, total_Extra_Fees_Unallocated__c = 0, Original_Total_Course__c = clientCourse.Total_Tuition__c + totalFees - totalPromotions, Original_Total_Extra_Fees__c = totalFees, Original_Total_Extra_Fees_Promotion__c = totalPromotions, Original_Total_Tuition__c = clientCourse.Total_Tuition__c);
		  		
    		
	// 	if(clientCourse.Commission_Type__c == 1)
	// 		commValue = clientCourse.Commission__c;
	// 	else 
	// 		commValue = clientCourse.Total_Tuition__c * clientCourse.Commission__c/100;
		
	// 	//============================Reset Installments

	// 	delete clientCourse.client_course_instalments__r;
			
	// 	addRelatedFees = '';
	// 	if(relatedFees.size() > 0)
	// 		addRelatedFees = string.join(relatedFees,'!#');
	// 	client_course_instalment__c cci;
		
	// 	 if(isMigration)
    //     	cci = new client_course_instalment__c(client_course__c = clientCourse.id, Number__c = 1, Due_Date__c = clientCourse.Start_Date__c, Tuition_Value__c = clientCourse.Total_Tuition__c, Extra_Fee_Value__c = totalFees - totalPromotions,
	// 																			Instalment_Value__c = clientCourse.Total_Tuition__c + totalFees - totalPromotions, Commission_Value__c = commValue, Commission__c = clientCourse.Commission__c, Related_Fees__c = addRelatedFees);
    //     else
    //     	cci = new client_course_instalment__c(client_course__c = clientCourse.id, Number__c = 1, Due_Date__c = System.today(), Tuition_Value__c = clientCourse.Total_Tuition__c, Extra_Fee_Value__c = totalFees - totalPromotions,
	// 																			Instalment_Value__c = clientCourse.Total_Tuition__c + totalFees - totalPromotions, Commission_Value__c = commValue, Commission__c = clientCourse.Commission__c, Related_Fees__c = addRelatedFees);
    //     insert cci;
		
    // }
    
    public pageReference saveDiscount(){
    	boolean checkFields = false;
    	if(mapFees.get(newDiscount.Fee_Name__c).Total_Value__c < newDiscount.Value__c){
    		newDiscount.Value__c.addError('Value bigger than Fee');
    		checkFields = true;
    	}
    	if(newDiscount.Promotion_Name__c == null || newDiscount.Promotion_Name__c.trim() == ''){
    		newDiscount.Promotion_Name__c.addError('Description Required');
    		checkFields = true;
    	}
    	if(newDiscount.Value__c == null || newDiscount.Value__c == 0){
    		newDiscount.Value__c.addError('Value Required');
    		checkFields = true;
    	}

		//If there is any installment paid with the related fee, check if it is still possible to add the discount
		for(client_course_instalment__c cci : [SELECT Id, Received_Date__c, Related_Fees__c, Instalment_Value__c,  Extra_Fee_Value__c FROM client_course_instalment__c WHERE client_course__c = :clientCourse.Id AND Received_Date__c != NULL order by Number__c, Due_Date__c]){
			if(cci.Related_Fees__c != null){
				for(String rf : cci.Related_Fees__c.split('!#')){
					list<String> fee = rf.split(';;');
					
					if(fee[1] == newDiscount.Fee_Name__c){
						if((mapFees.get(newDiscount.Fee_Name__c).Total_Value__c - decimal.valueOf(fee[2])) < newDiscount.Value__c){
							newDiscount.Promotion_Name__c.addError('An amount of $' +fee[2] + ' was already paid for this fee, the discount is greater than the remaining value.');
							checkFields = true;
							break;
						}
					}
				}//end for
			}
		}//end for
    	
    	if(checkFields)
    		return null;
    	
    	string feename = newDiscount.Fee_Name__c;

    	newDiscount.Client_Course__c = clientCourse.id;
    	newDiscount.isPromotion__c = true;
    	newDiscount.Total_Value__c = newDiscount.Value__c;
    	newDiscount.relatedPromotionFee__c = mapFees.get(feename).id;
    	newDiscount.Fee_Name__c += ' ' + newDiscount.Promotion_Name__c;
    	
    	newDiscount.Original_Total_Value__c = newDiscount.Value__c;
    	newDiscount.Original_Value__c = newDiscount.Value__c;
    	 
    	insert newDiscount;
    	
    	mapFees.get(feename).Extra_Fee_Discount_Value__c = newDiscount.Value__c;
    	update mapFees.get(feename);
    	
   		update new client_course__c(id = clientCourse.id, Total_Extra_Fees_Promotion__c = clientCourse.Total_Extra_Fees_Promotion__c + newDiscount.Total_Value__c, Total_Course__c = clientCourse.Total_Course__c - newDiscount.Total_Value__c, Total_Instalments__c = clientCourse.Total_Course__c - newDiscount.Total_Value__c); 	

		//Update Discount Fee in Open Installments
		Decimal remainingValue = newDiscount.Value__c;
		list<client_course_instalment__c> upInst = new list<client_course_instalment__c>();
		for(client_course_instalment__c cci : [SELECT Id, Related_Fees__c, Instalment_Value__c, Tuition_Value__c, Extra_Fee_Value__c FROM client_course_instalment__c WHERE client_course__c = :clientCourse.Id AND Received_Date__c = NULL order by Number__c, Due_Date__c]){
			if(remainingValue == 0 || remainingValue < 0) break;
			if(cci.Related_Fees__c != null){
				list<String> upFees = new list<String>();
				for(String rf : cci.Related_Fees__c.split('!#')){
					list<String> fee = rf.split(';;');
					
					if(fee[1] == feename){

						decimal newVal = decimal.valueOf(fee[2]) - remainingValue;
						if(newVal < 0){
							remainingValue = Math.abs(newVal);
							newVal = 0;

							cci.Extra_Fee_Value__c -= decimal.valueOf(fee[2]);
							cci.Instalment_Value__c -= decimal.valueOf(fee[2]);
							upFees.add(fee[0] + ';;' + fee[1] + ';;' + newVal + ';;0' + ';;' + newVal + ';;0');
							
						}
						else{
							cci.Extra_Fee_Value__c = newVal;
							cci.Instalment_Value__c = cci.Tuition_Value__c + newVal;
							upFees.add(fee[0] + ';;' + fee[1] + ';;' + newVal + ';;0' + ';;' + newVal + ';;0');
							remainingValue = 0;
						}
					}
					else
						upFees.add(rf);
				}//end for
				
				cci.Related_Fees__c = String.join(upFees, '!#');
				upInst.add(cci);
			}
		}//end for
    	
		update upInst;
    	// resetInstallmets();	TODO:
    	
    	newDiscount = null;
    	loadFees();   	
    	quoteFees = null;
    		
    	return null;
    }
	
	private List<SelectOption> unitTypes;
	public List<SelectOption> getUnitTypes(){
		if(unitTypes == null){
			unitTypes = new List<SelectOption>();
			unitTypes.add(new SelectOption('0', 'None'));
			for(String uType : IPFunctions.getUnitTypes())
				unitTypes.add(new SelectOption(uType, uType));
		}
		unitTypes.sort();
		return unitTypes;
		
	}
	
	private List<SelectOption> quoteFees;
	public List<SelectOption> getQuoteFees(){
		if(quoteFees == null){
			quoteFees = new List<SelectOption>();
			for(client_course_fees__c cf : clientCourse.client_courses_fees__r)
				if(!cf.isPromotion__c && !relatedPromotions.containsKey(cf.id))
					quoteFees.add(new SelectOption(cf.Fee_Name__c, cf.Fee_Name__c));
		}
		return quoteFees;
		
	}
	
	
	
	
	/***************************** MIGRATION METHODS *******************************/
   	public boolean isMigration {get
	{
		if(isMigration==null){
			if(ApexPages.currentPage().getParameters().get('mi')=='true')
				isMigration = true;
			else
				isMigration = false;
		}
		return isMigration;
	}set;}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}