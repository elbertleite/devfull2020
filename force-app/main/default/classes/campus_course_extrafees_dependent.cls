public class campus_course_extrafees_dependent {

	public Course_Extra_Fee__c courseFee{get; Set;}
	private String supplierId;
	private String campusID;
	public campus_course_extrafees_dependent(ApexPages.StandardController control){
		courseFee = [Select id, optional__c, Product__r.Name__c from Course_Extra_Fee__c where id = :control.getId() limit 1];
		supplierId = [Select parentId from Account where id =:ApexPages.currentPage().getParameters().get('campusId')].parentId;
		campusID = ApexPages.currentPage().getParameters().get('campusId');
		
	}
	
	
	private List<SelectOption> schoolFees;
	public List<SelectOption> getschoolFees() {
		if(schoolFees == null){
			schoolFees = new List<SelectOption>();
			schoolFees.add(new SelectOption('','Select Extra Fee'));
			for (Product__c pd:[Select Name__c, Allow_change_units__c, Description__c, Optional__c, Product_Type__c, Id, Supplier__c, Unit_Type__c from Product__c P WHERE P.Supplier__c = :supplierId order by Name__c]){
				schoolFees.add(new SelectOption(pd.id,pd.Name__c));
			}
		}
		return schoolFees;
	}
	
	public Course_Extra_Fee_Dependent__c newDependentFee { 
		get{
		if(newDependentFee==null){
			newDependentFee = new Course_Extra_Fee_Dependent__c();
		}
		return newDependentFee;
	} 
		set;
	} 
	
	public Boolean editMode{get{if(editMode == null) editMode = false; return editMode;} Set;}
	
	public pageReference editRelatedFees(){		
		editMode = true;
		return null;
	}
	
	public pageReference deleteRelatedFees(){	
		List<Course_Extra_Fee_Dependent__c> deleting = new List<Course_Extra_Fee_Dependent__c>();
		
		for(Course_Extra_Fee_Dependent__c cefd : listFees)
			if(cefd.selected__c)
				deleting.add(cefd);
		
		delete deleting;
		return null;
	}
	
	private Map<String, Course_Extra_Fee_Dependent__c> oldCourseRelatedFees = new Map<String, Course_Extra_Fee_Dependent__c>();
	public set<string> errorUpdateRelated{get{if(errorUpdateRelated == null) errorUpdateRelated = new set<string>(); return errorUpdateRelated;} set;}
	
	public void saveRelatedFees(){
		List<Course_Extra_Fee_Dependent__c> lrelated = new List<Course_Extra_Fee_Dependent__c>();
		Map<String, List<Course_Extra_Fee_Dependent__c>> mapRelatedFees = new Map<String, List<Course_Extra_Fee_Dependent__c>>();
		Course_Extra_Fee_Dependent__c oldRelated;
		
		errorUpdateRelated = new Set<String>();
		
		
		for(Course_Extra_Fee_Dependent__c related : listFees){
						
			oldRelated = oldCourseRelatedFees.get(related.id);
			
			if( related.Product__c != oldRelated.Product__c || related.Date_Paid_From__c != oldRelated.Date_Paid_From__c || related.Date_Paid_To__c != oldRelated.Date_Paid_To__c ||
					related.Date_Start_From__c != oldRelated.Date_Start_From__c || related.Date_Start_To__c != oldRelated.Date_Start_To__c || related.From__c != oldRelated.From__c ||
					related.Value__c != oldRelated.Value__c || related.Optional__c != oldRelated.Optional__c || related.Allow_Change_Units__c != oldRelated.Allow_Change_Units__c || 
					related.Details__c != oldRelated.Details__c || related.Keep_Fee__c != oldRelated.Keep_Fee__c || related.Extra_Fee_Interval__c != oldRelated.Extra_Fee_Interval__c ||
					related.Account_Document_File__c != oldRelated.Account_Document_File__c ){
				related.selected__c = false;
				lrelated.add(related);

			}

			if(!mapRelatedFees.containsKey(related.Course_Extra_Fee__c))
				mapRelatedFees.put(related.Course_Extra_Fee__c, new list<Course_Extra_Fee_Dependent__c>{related});
			else mapRelatedFees.get(related.Course_Extra_Fee__c).add(related);
		}
		
	
		if(!validateFields(lrelated)) return;
		
		String innerErrorMsg = '';
		boolean isValid = true;
				
		list<Course_Extra_Fee_Dependent__c> validRelatedExtraFees = new list<Course_Extra_Fee_Dependent__c>();
		for(Course_Extra_Fee_Dependent__c relatedFee : lrelated){
			isValid = true;
			for(Course_Extra_Fee_Dependent__c mapRelated : mapRelatedFees.get(relatedFee.Course_Extra_Fee__c)){
				
				
	        	if(relatedFee.Id != mapRelated.id && relatedFee.Product__c == mapRelated.Product__c && relatedFee.From__c == mapRelated.From__c) {
					
					if( !( (relatedFee.date_paid_from__c < mapRelated.date_paid_from__c && relatedFee.date_paid_to__c < mapRelated.date_paid_from__c) || (relatedFee.date_paid_from__c > mapRelated.date_paid_to__c && relatedFee.date_paid_to__c > mapRelated.date_paid_to__c) ) ) {
						isValid = false;
						if(!errorUpdateRelated.contains(relatedFee.id)){
							//innerErrorMsg += '<b>'+relatedFee.Nationality__c + '</b><br />';
							innerErrorMsg += '<span style="color:maroon;">'+relatedFee.Product__r.Name__c +'</span> - <span style="color:grey">From:</span> '+relatedFee.From__c+'<span style="color:grey"> From Date:</span> '+ relatedFee.date_paid_from__c.format() +' <span style="color:grey">To Date:</span> '+ relatedFee.date_paid_to__c.format() + '<br />';
							errorUpdateRelated.add(relatedFee.id);
						}
					}
				}
			} 
			if(isValid)
				validRelatedExtraFees.add(relatedFee);
		}
		
		if(validRelatedExtraFees.size() > 0)
			update validRelatedExtraFees;
		
		if(errorUpdateRelated.size() > 0){
			String errorMsg = '<span style="color:red;">'+errorUpdateRelated.size() + ' out of ' + lrelated.size() + ' related fees showed below couldn\'t be updated as they have duplicated values.</span> <br /> ';
			errorMsg += innerErrorMsg;
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, errorMsg);
			ApexPages.addMessage(msg);
		}
		
		listFees = null;
		editMode = false;
	}
	
	public Boolean validateFields(List<Course_Extra_Fee_Dependent__c> relatedFees){
		
		Boolean fieldsValidated = true;
		for(Course_Extra_Fee_Dependent__c dep : relatedFees){
			
			if(dep.Allow_Change_Units__c && (dep.Extra_Fee_Interval__c == null || dep.Extra_Fee_Interval__c == '') ){
				dep.Extra_Fee_Interval__c.addError('Field is required when Allow Change Units is checked');
				fieldsValidated = false;
			}
			
			if(dep.date_paid_from__c > dep.date_paid_to__c){
				dep.date_paid_from__c.addError('From date cannot be greater than To date.');
				fieldsValidated = false;
			}
			
			
		}
		return fieldsValidated;
	}
	
	public pageReference cancelEdit(){	
		editMode = false;
		listFees = null;
		return null;
	}
	
	public pageReference addrelatedFee(){
		
		if(!validateFields(new List<Course_Extra_Fee_Dependent__c>{newDependentFee})) return null;
			
		
		
		errorUpdateRelated = new Set<String>();
		String innerErrorMsg = '';
		
		if(newDependentFee.Course_Extra_Fee__c == null)
			newDependentFee.Course_Extra_Fee__c = courseFee.id;
		
		boolean isValid = true;
		
		for(Course_Extra_Fee_Dependent__c mapRelated : listFees){
			if(newDependentFee.Product__c == mapRelated.Product__c && newDependentFee.From__c == mapRelated.From__c){
				if( !( (newDependentFee.date_paid_from__c < mapRelated.date_paid_from__c && newDependentFee.date_paid_to__c < mapRelated.date_paid_from__c) ||
						(newDependentFee.date_paid_from__c > mapRelated.date_paid_to__c && newDependentFee.date_paid_to__c > mapRelated.date_paid_to__c) ) ) {
					isValid = false;
					//innerErrorMsg += '<b>'+relatedFee.Nationality__c + '</b><br />';
					innerErrorMsg += '<span style="color:maroon;">'+ mapRelated.Product__r.Name__c +'</span> - <span style="color:grey">From:</span> '+mapRelated.From__c+'<span style="color:grey"> From Date:</span> '+ mapRelated.date_paid_from__c.format() +' <span style="color:grey">To Date:</span> '+ mapRelated.date_paid_to__c.format() + '<br />';
					errorUpdateRelated.add(mapRelated.id);
				}
			}
		}
		
		if(isValid){			
			upsert newDependentFee;
			newDependentFee = new Course_Extra_Fee_Dependent__c();
			listFees = null;
			editMode = false;		
		} else {
			if(errorUpdateRelated.size() > 0){
				String errorMsg = 'The related fee couldn\'t be created because it has duplicated values. <br /> ';
				errorMsg += innerErrorMsg;
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, errorMsg);
				ApexPages.addMessage(msg);
			}
		}
		
		return null;	
	}
	
	public List<Course_Extra_Fee_Dependent__c> listFees {
		get{
			if(listFees == null)
				listFees = getListFees();
			return listFees;
		}
		Set;
	}
	
	private List<Course_Extra_Fee_Dependent__c> getlistFees(){
		List<Course_Extra_Fee_Dependent__c> thelist = [Select Course_Extra_Fee__c, Account_Document_File__c, Account_Document_File__r.Description__c, Selected__c, Product__r.Name__c, Allow_Change_Units__c, Date_Paid_From__c, Date_Paid_To__c, 
																Date_Start_From__c, Account_Document_File__r.File_Name__c, 
																Date_Start_To__c, Details__c, From__c, Optional__c, Value__c, Keep_Fee__c, Extra_Fee_Interval__c,
																( Select Account_Document_File__r.Description__c, Account_Document_File__c, Account_Document_File__r.File_Name__c, From_Date__c, Selected__c, To_Date__c, Value__c, Comments__c 
																	from Start_Date_Ranges__r order by From_Date__c, Value__c)
														from Course_Extra_Fee_Dependent__c where Course_Extra_Fee__c = :courseFee.id order by Product__r.Name__c,Date_Paid_From__c, Date_Paid_To__c, From__c ];
		
		oldCourseRelatedFees = new Map<String, Course_Extra_Fee_Dependent__c>(thelist.deepClone(true));
		
		return thelist;
				
	}
	
	public pagereference viewFile(){
		String fileId =  Apexpages.currentPage().getParameters().get('fileId');
		return new pageReference(cg.SDriveTools.getAttachmentURL(campusID, fileId, (100 * 60)));	
	}
	
	public class sDriveFiles{
		public String URL {get;set;}
		public Account_Document_File__c File {get;set;}
	} 
	
	private Map<String, sDriveFiles> mapFiles = new Map<String, sDriveFiles>();
	
	public List<SelectOption> Files {
		get{
			if(Files== null){
				Files = new List<SelectOption>();
				List<ID> parentIDs = new List<ID>();
				List<ID> fileObjects = new List<ID>();
				files.add(new SelectOption('','-- Select a File --'));	
				
				Map<String, List<Account_Document_File__c>> campusFiles = new Map<String, List<Account_Document_File__c>>();
				Map<String, String> folderNames = new Map<String, String>();
				folderNames.put('','Not Classified');
				campusFiles.put( '' , new List<Account_Document_File__c>() );
				for(Account_Document_File__c folder : [SELECT id, File_Name__c FROM Account_Document_File__c WHERE Parent__c = :campusID and Content_Type__c = 'Folder' and WIP__c = false order by Content_Type__c]){
					campusFiles.put( folder.id, new List<Account_Document_File__c>() );
					folderNames.put( Folder.id, Folder.File_Name__c );
				}
				
				
				for(Account_Document_File__c folder : [SELECT id, Parent_Folder_Id__c, Description__c, CreatedDate, File_Name__c,File_Size__c FROM Account_Document_File__c WHERE Parent__c = :campusID and Content_Type__c != 'Folder' order by Content_Type__c]){
					List<Account_Document_File__c> ladf = campusFiles.get(Folder.Parent_Folder_Id__c == null ? '' : Folder.Parent_Folder_Id__c);
					ladf.add(folder);
					campusFiles.put(Folder.Parent_Folder_Id__c == null ? '' : Folder.Parent_Folder_Id__c, ladf);
				}
				
				for(String folderid : campusFiles.keyset()){					
					if(!campusFiles.get(folderid).isEmpty()){
						
						files.add(new SelectOption('Folder',folderNames.get(folderid)));
						
						for(Account_Document_File__c file : campusFiles.get(folderid)){							
							parentIDs.add(campusID);
							fileObjects.add(file.id);
							sDriveFiles sdf = new sDriveFiles();
							sdf.File = file;
							mapFiles.put(file.id,sdf);
							files.add(new SelectOption(file.id, file.Description__c + ' [*' + file.File_Name__c + ' *]')  );
							
						}
						
						files.add(new SelectOption('disabled', ''));	
					}
				}
				
				try {
					if(files.size() > 1)
						files.remove(files.size()-1);
				} catch (Exception e){}
				
				
				if(parentIDs.size() > 0 && fileObjects.size() > 0){
					try {	
						List<String> urls = cg.SDriveTools.getAttachmentURLs( parentIDs , fileObjects, (100 * 60));
							
						for(string sd : mapFiles.keySet())
							for(String url : urls )
								if(url.contains(mapFiles.get(sd).file.id)){
									mapFiles.get(sd).url = url;
									break;
								}
					} catch (Exception e){}
				}
			}
			return Files;		
		}
		set;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}