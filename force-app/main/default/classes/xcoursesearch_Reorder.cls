public class xcoursesearch_Reorder{

	private list<Search_Courses__c> listCourses;
	public xcoursesearch_Reorder(ApexPages.StandardController controller){
		listCourses = [Select id, Campus_Course__r.Campus__r.BillingCountry, Campus_Course__r.Campus__r.BillingCity, Campus_Course__r.Campus__r.Name, Campus_Course__r.Course__r.Name, Number_of_Units__c,
                    Unit_Type__c, Course_Order__c,  Custom_Campus__c, Custom_City__c, Custom_Country__c, Custom_Course__c, isCustomCourse__c from Search_Courses__c WHERE Web_Search__c = :controller.getId() AND Course_Deleted__c = false  ORDER BY Course_Order__c NULLS LAST, CreatedDate];
	}
	
	private map<String, Search_Courses__c> courses;
	
	
	
	public map<String, Search_Courses__c> getCourses(){
		if(courses == null){
			courses = new map<String, Search_Courses__c>(listCourses);
		}
		return courses;
	}
	
	public list<Search_Courses__c> getcoursesOrdered(){
		return listCourses;
	}
	
	 public pageReference saveReorderedCourses(){        
        
        string newListOrder = ApexPages.currentpage().getParameters().get('listtoOrder');
         getCourses();  
        if(newListOrder != null){
            
            if(newListOrder.LastIndexOf(',') != -1)
                newListOrder.substring(0,newListOrder.LastIndexOf(',')-1);
            
            integer counter = 0;
            for(string s:newListOrder.split(',')){             
				courses.get(s).Course_Order__c = counter;
            	counter++;
            }
            Savepoint sp;
            try{
                sp = Database.setSavepoint();
                update courses.values();
            }catch(Exception e){
                ApexPages.addMessages(e);
                Database.rollback(sp);
            }
        } 
        return null;
    }

}