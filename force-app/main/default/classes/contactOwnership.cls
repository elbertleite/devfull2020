public with sharing class contactOwnership {
	
	public String ctid {get;set;}
	public xCourseSearchPersonDetails.userDetails userDetails{get; set;}
	
	
	
	public contactOwnership(){
		xCourseSearchPersonDetails personDetails = new xCourseSearchPersonDetails();
		userDetails = personDetails.getAgencyDetails();
		getContact();
		allAgencies = new agenytNoSharing(this);
	}
	
	private Contact contact;
	public Contact getContact(){		
		if(ctid != null)
			contact = [Select id, Name, Owner__c, Owner__r.Name, Owner__r.ID, Owner__r.Email, Ownership_History_Field__c, RecordType.Name, Lead_Accepted_By__c, Lead_Assignment_By__c, Lead_State__c from Contact where id = :ctid];		
		return contact;			
	}
	
	public PageReference takeOwnership(){
		User userDetails = [Select Id, ContactId, Contact.AccountId From User where id = :Userinfo.getUserId() limit 1];
		system.debug('@ contact: ' + contact);
		
		try {
			
			allAgencies.setNewOwner(Userinfo.getUserId());
			allAgencies.setCurrentOwner(contact.Owner__c);
			
			String subject = contact.RecordType.Name + ' ownership has been taken';
			String body = newOwner.Name + ' from ' + newOwner.Account.Name + ' has taken the ownership of ' + contact.Name + ' from you.';
			
			body += '<br/> ';
			
			if(newOwner.Signature__c != null)
				body += newOwner.Signature__c;
			
			contact.Owner__c = Userinfo.getUserId();
			contact.Current_Agency__c = userDetails.Contact.AccountId;
			
			if(contact.RecordType.Name == 'Lead'){
				contact.Lead_Assignment__c = 'Assigned';
				contact.Lead_Assignment_By__c = newOwner.id;
				contact.Lead_Assignment_On__c = System.now();
				contact.Lead_Assignment_To__c = newOwner.id;
			}

			User userNewOwner = [SELECT Contact.Account.Name, Contact.AccountId, ID, name, Contact.Account.ParentID FROM User WHERE ID = :Userinfo.getUserId()];
			User userCurrentOwner = null;
			if(currentOwner != null){
				userCurrentOwner = [SELECT Contact.Account.Name, Contact.AccountId, ID, name, Contact.Account.ParentID FROM User WHERE ID = :contact.Owner__r.ID];
			}
			
			contact.Ownership_History_Field__c = JSON.serialize(new IPFunctions.ContactNoSharing().transferOwnerShip(contact, userNewOwner, userCurrentOwner, userNewOwner, 'Taken'));

			update contact;	
			
			if(currentOwner != null){
				IPFunctions.sendEmail(newOwner.Name, newOwner.Email, currentOwner.Name, currentOwner.Email, subject, body);
			}

			String url = '/Contact_Overview?id=' + contact.id;

			User cOwner = null;
			try{
				cOwner = [SELECT ID, Contact.Account.ParentId, Contact.Account.Parent.RDStation_Salesforce_mapping__c, Contact.Account.Parent.RDStation_Client_ID__c, Contact.Account.Parent.Synchronize_RDStation_Change_Ownership__c From User WHERE ID = :contact.Owner__r.ID];
			}catch(QueryException qe){
				system.debug('CURRENT CONTACT DOESN\'T HAVE AN OWNER. ');
			}
			
			User nOwner = [SELECT ID, Contact.Account.ParentId, Contact.Account.Parent.RDStation_Salesforce_mapping__c, Contact.Account.Parent.RDStation_Client_ID__c, Contact.Account.Parent.Synchronize_RDStation_Change_Ownership__c From User WHERE ID = :Userinfo.getUserId()];

			boolean synchronizeRDStation = nOwner.Contact.Account.Parent.Synchronize_RDStation_Change_Ownership__c && (cOwner == null || cOwner.Contact.Account.Parent.Synchronize_RDStation_Change_Ownership__c);

			if(synchronizeRDStation){
				Set<String> ids = new Set<string>();
				ids.add(contact.ID);
				RDStationSaveLeadWebhook.updateBulkWithRDStation(ids, nOwner.Contact.Account.ParentId);
			}

			/*if(!String.isEmpty(nOwner.Contact.Account.Parent.RDStation_Client_ID__c) && !String.isEmpty(nOwner.Contact.Account.Parent.RDStation_Salesforce_mapping__c) && (cOwner == null || cOwner.Contact.Account.ParentId == nOwner.Contact.Account.ParentId)){
				url = url + '&callRD=synchronize';
			}*/
				
			//getContact();
			PageReference pageRef = new PageReference(url);
			pageRef.setRedirect(true);
			return pageRef;
				
		} catch (Exception e){			
			Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));	
			return null;		
		}
		
				
	}
	
	
	
	
	public String selectedUser {get;set;}
	
	public agenytNoSharing allAgencies{get;set;}
	
	private Contact currentOwner;
	private Contact newOwner;
	
	
	
	
	
	public without sharing class agenytNoSharing{
		
		
		contactOwnership outerClass;
	    public agenytNoSharing (contactOwnership outerClass) {
	       this.outerClass = outerClass;
	       System.debug(' ===> outerClass.userDetails: ' + outerClass.userDetails);
	    }
	    
	    
	  
			
		public boolean shareVisibility {get{if(shareVisibility == null) shareVisibility= false; return shareVisibility;}set;}
		
		public pageReference transferOwnership(){
			//User userDetailsLocal = [Select Contact.AccountId From User where id = :selectedUser limit 1];
			system.debug('@ contact: ' + outerClass.contact);
			
			if(outerClass.selectedUser != null){
				
				try {
					setNewOwner(outerClass.selectedUser);
					setCurrentOwner(outerClass.contact.Owner__c);
					String subject;
					String body;
					String action;
					if(!shareVisibility){
						action = 'Given';
						subject = outerClass.contact.RecordType.Name + ' ownership has been transferred';
						body = outerClass.currentOwner.Name + ' from ' + outerClass.currentOwner.Account.Name + ' has transferred the ownership of ' + outerClass.contact.Name + ' to you.';
					}
					else{
						action = 'Shared Visibility';
						subject = outerClass.contact.RecordType.Name + ' is now visible';
						body = outerClass.currentOwner.Name + ' from ' + outerClass.currentOwner.Account.Name + ' has shared visibility of ' + outerClass.contact.Name + ' with you.';
					}
					body += '<br/> ';

					User createdBy = [SELECT Contact.Account.Name, Contact.AccountId, ID, name FROM User WHERE ID = :Userinfo.getUserId()];
					User userNewOwner = [SELECT Contact.Account.Name, Contact.AccountId, ID, name, Contact.Account.ParentID FROM User WHERE ID = :outerClass.selectedUser];
					User userCurrentOwner = [SELECT Contact.Account.Name, Contact.AccountId, ID, name, Contact.Account.ParentID FROM User WHERE ID = :outerClass.contact.Owner__r.ID];

					outerClass.contact.Ownership_History_Field__c = JSON.serialize(new IPFunctions.ContactNoSharing().transferOwnerShip(outerClass.contact, userNewOwner, userCurrentOwner, createdBy, action));

					if(outerClass.currentOwner.Signature__c != null)
						body += outerClass.currentOwner.Signature__c;
						
					if(!shareVisibility){//Don't change owner and current agency if user is sharing visibility
						outerClass.contact.Owner__c = outerClass.selectedUser;
						outerClass.contact.Current_Agency__c = selectedAgency; //userDetailsLocal.Contact.AccountId;						
					}
					
					if(outerClass.userDetails.AgencyGroup_Id != selectedAgencyGroup && !agencyGroupType.get(selectedAgencyGroup))
						outerClass.contact.AccountId = selectedAgency;
						
					
					if(outerClass.contact.RecordType.Name == 'Lead'){
						outerClass.contact.Lead_Assignment__c = 'Assigned';
						outerClass.contact.Lead_Assignment_By__c = outerClass.newOwner.id;
						outerClass.contact.Lead_Assignment_On__c = System.now();
						outerClass.contact.Lead_Assignment_To__c = outerClass.newOwner.id;
					}
					update outerClass.contact;
					
					IPFunctions.sendEmail(outerClass.currentOwner.Name, outerClass.currentOwner.Email, outerClass.newOwner.Name, outerClass.newOwner.Email, subject, body);
					
					if(outerClass.userDetails.AgencyGroup_Id != selectedAgencyGroup && !shareVisibility){
						PageReference pageRef = new PageReference('/Contacts');
						pageRef.setRedirect(true);
						return pageRef;
					}else {
						//outerClass.getContact();

						String url = '/Contact_Overview?id=' + outerClass.ctid;

						Contact cOwner = [SELECT ID, Account.ParentId, Account.Parent.RDStation_Salesforce_mapping__c, Account.Parent.RDStation_Client_ID__c, Account.Parent.Synchronize_RDStation_Change_Ownership__c From Contact WHERE ID = :outerClass.currentOwner.ID];
						Contact nOwner = [SELECT ID, Account.ParentId, Account.Parent.RDStation_Salesforce_mapping__c, Account.Parent.RDStation_Client_ID__c, Account.Parent.Synchronize_RDStation_Change_Ownership__c From Contact WHERE ID = :outerClass.newOwner.id];

						/*if(!String.isEmpty(nOwner.Account.Parent.RDStation_Client_ID__c) && !String.isEmpty(nOwner.Account.Parent.RDStation_Salesforce_mapping__c) && cOwner.Account.ParentId == nOwner.Account.ParentId){
							url = url + '&callRD=synchronize';
						}*/

						if(cOwner.Account.Parent.Synchronize_RDStation_Change_Ownership__c && nOwner.Account.Parent.Synchronize_RDStation_Change_Ownership__c){
							Set<String> ids = new Set<string>();
							ids.add(outerClass.contact.ID);
							RDStationSaveLeadWebhook.updateBulkWithRDStation(ids, nOwner.Account.ParentId);
						}

						PageReference pageRef = new PageReference(url);
						pageRef.setRedirect(true);
						return pageRef;
					}
				} catch (Exception e){			
					Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));		
				}
				
			}
			return null;
		}
	
	    
	    
		private void setCurrentOwner(String userid){
			if(userid != null){
				system.debug('userid========' + userid);
				User u = [Select ContactID from user where id = :userid LIMIT 1];
				system.debug('u.========' + u);
				system.debug('u.ContactID========' + u.ContactID);
				IpFunctions.ContactNoSharing cns = new IpFunctions.ContactNoSharing(); 
				outerClass.currentOwner = cns.getCurrentUser(u.ContactID);
			}
			
		}
		
		
		private void setNewOwner(String userid){
			User u = [Select ContactID from user where id = :userid LIMIT 1];
			outerClass.newOwner = [Select id, Name, Signature__c, Email, Accountid, Account.Name from Contact where id = :u.ContactID];
			
		}
		
		public void clearUsers(){
			selectedAgency = '';
			//outerClass.selectedUser = null;
			userOptions.clear();
		}
		
		public map<string, boolean> agencyGroupType{get; set;}
		
		public String selectedAgencyGroup { get{if(selectedAgencyGroup == null) selectedAgencyGroup = outerClass.userDetails.AgencyGroup_Id; return selectedAgencyGroup;} set; }
		public List<SelectOption> agencyGroupOptions {
			
			get {
				agencyGroupType = new  map<string, boolean>();
				agencyGroupOptions = new List<SelectOption>();
				for(Account agency : [select id, name, Destination_Group__c from Account where RecordType.name = 'Agency Group' and Global_Link__c = :outerClass.userDetails.Global_Link_Id order by Name]){
					agencyGroupOptions.add(new SelectOption(agency.id, agency.Name));								
					agencyGroupType.put(agency.id, agency.Destination_Group__c);
				}
				return agencyGroupOptions;
			}
			set;
			
		}
		
		public String selectedAgency { get{if(selectedAgency == null) selectedAgency = outerClass.userDetails.agency_id; return selectedAgency;} set; }
		public List<SelectOption> agencyOptions {
			
			get {
				if(selectedAgencyGroup != null){
					agencyOptions = new List<SelectOption>();
					for(Account agency : [select id, name from Account where RecordType.name = 'Agency' and parentId = :selectedAgencyGroup order by Name]){
						agencyOptions.add(new SelectOption(agency.id, agency.Name));					
						if(selectedAgency == '')
							selectedAgency = agency.id;
					}		
					userOptions = userOptions;	
				}
				return agencyOptions;
			}
			set;
			
		}
		
		
		public List<SelectOption> userOptions {
			get{
				if(selectedAgency != null){
					userOptions = new List<SelectOption>();
					for(User u : [select id, Name from User where AccountID = :selectedAgency and id != :Userinfo.getUserId() and isActive = true and Contact.Chatter_Only__c = false order by Name])
						userOptions.add(new SelectOption(u.id, u.Name));
				}
				return userOptions;
			}
			set;
		}
		
	}
	
	
	public PageReference acceptLead(){
		User userDetails = [Select Id, ContactId, Contact.AccountId From User where id = :Userinfo.getUserId() limit 1];
		contact.Lead_Assignment__c = 'Assigned';
		contact.Lead_Assignment_By__c = userDetails.ContactId;
		contact.Lead_Assignment_To__c = userDetails.ContactId;	
		contact.Lead_State__c = 'Accepted';
		contact.Lead_Accepted_By__c = userDetails.ContactId;
		contact.Lead_Accepted_On__c = System.now();
		contact.Original_Agency__c = userDetails.Contact.AccountId;
		contact.Current_Agency__c = userDetails.Contact.AccountId;
		contact.Owner__c = userDetails.Id;
		contact.Last_Enquiry__c = System.now();
			
		update contact;
			
		//getContact();
		PageReference pageRef = new PageReference('/Contact_Overview?id=' + contact.id);
		pageRef.setRedirect(true);
		return pageRef;	
	}
	
	

}