/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class extClientQuotationEmail_test {

    static testMethod void myUnitTest() {
        Contact c = new Contact();
		c.FirstName = 'Joao';
		c.LastName = 'Doe';
		c.Nationality__c = 'Brazil';
		c.MobilePhone = '2875278636';
		c.email = 'testest@testest.com';
		insert c;
		
		Quotation__c q = new Quotation__c();
		q.client__c = c.id;
		q.Status__c = 'In Progress';
		q.Expiry_Date__c = system.today().addDays(7);
		insert q;
		
		extClientQuotationEmail ct = new extClientQuotationEmail(new Apexpages.Standardcontroller(q));
		Quotation__c qt = ct.quote;
    }
}