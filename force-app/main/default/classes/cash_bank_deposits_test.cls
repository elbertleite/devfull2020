@isTest
private class cash_bank_deposits_test {
	
	static testMethod void myUnitTest() {

		TestFactory tf = new TestFactory();

		Account school = tf.createSchool();
		Account agency = tf.createAgency();
		Contact emp = tf.createEmployee(agency);
		User portalUser = tf.createPortalUser(emp);
		Account campus = tf.createCampus(school, agency);
		Course__c course = tf.createCourse();
		Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);

		Test.startTest();
		system.runAs(portalUser){
			Contact client = tf.createLead(agency, emp);
			client_course__c booking = tf.createBooking(client);
			client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);

			Bank_Detail__c bank = new Bank_Detail__c(Account__c = agency.ParentId, Account_Name__c = 'Test Account');
			insert bank;

			List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);

			//Instalment
			client_course_instalment_pay instPay = new client_course_instalment_pay(new ApexPages.StandardController(instalments[0]));
			instPay.newPayDetails.Value__c = 2230;
			instPay.newPayDetails.Date_Paid__c = system.today();
			instPay.newPayDetails.Payment_Type__c = 'Cash';
			instPay.addPaymentValue();
			instPay.savePayment();

			//Product
			client_product_service__c product =  tf.createCourseProduct(booking, agency);
			client_product_service__c product2 =  tf.createCourseProduct(booking, agency);

			client_course_product_pay prodPay = new client_course_product_pay(new ApexPages.StandardController(product));
			prodPay.newPayDetails.Value__c = 50;
			prodPay.newPayDetails.Payment_Type__c = 'Cash';
			prodPay.addPaymentValue();
			prodPay.savePayment();

			//Invoice
			ApexPages.currentPage().getParameters().put('cs', instalments[3].id);
			ApexPages.currentPage().getParameters().put('pd', product2.id);
			ApexPages.currentPage().getParameters().put('ct', client.id);

			client_course_create_invoice invoicePay = new client_course_create_invoice();
			invoicePay.createInvoice();
			invoicePay.newPayDetails.Value__c = 1600;
			invoicePay.newPayDetails.Payment_Type__c = 'Cash';
			invoicePay.addPaymentValue();
			invoicePay.savePayment();


			CashDepositController toDeposit = new CashDepositController();
			toDeposit.fromDate.Expected_Travel_Date__c = system.today();
			toDeposit.toDate.Expected_Travel_Date__c = system.today();

			List<SelectOption> agencyGroupOptions = toDeposit.agencyGroupOptions;
			toDeposit.changeGroup();

			toDeposit.confirmDeposit();
			toDeposit.selectedBank = toDeposit.banks[1].getValue();
			toDeposit.confirmDeposit();
			toDeposit.bankDeposit.Deposit_Date__c = system.today();
			toDeposit.confirmDeposit();

			cash_bank_deposits allDeposits = new cash_bank_deposits();

			List<SelectOption> periods = allDeposits.getPeriods();
			client_course_instalment__c dates = allDeposits.dates;
			allDeposits.changeGroup();

			allDeposits.bkId = allDeposits.result[0].deposit.id;
			allDeposits.cancelChange();
			allDeposits.saveChange();

       }
       Test.stopTest();
    }
}