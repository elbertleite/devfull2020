/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CreateAccountPicturePreviewLink_test {

    static testMethod void myUnitTest() {
    	
    	TestFactory tf = new TestFactory();
        
        Account agencyGroup = tf.createAgencyGroup();
        
        Account_Picture_File__c apf = new Account_Picture_File__c();
		apf.File_Name__c = 'myfile';
		apf.URL__c = 'mylink';
		apf.Description__c = 'mydescription';
		apf.Parent__c = agencyGroup.id;
		apf.Content_Type__c = 'image/jpeg';
		apf.WIP__c = false;
		insert apf;
        
        update apf;
        
        
        
        
    }
}