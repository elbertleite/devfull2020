/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class contact_documents_list_test {

    static testMethod void myUnitTest() {
        
        Map<String,String> recordTypes = new Map<String,String>();
  		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
        
		TestFactory tf = new TestFactory();
		Account agency = tf.createAgency();
       	Contact client1 = tf.createClient(agency);
        
        
        Client_Document__c cd = new Client_Document__c();
        cd.Document_Category__c = 'Passport';
        cd.Document_Type__c = 'Passport';
        cd.Client__c = client1.id;
        insert cd;
        
        Client_Document_File__c cdf = new Client_Document_File__c();
        cdf.Client__c = client1.id;
        cdf.File_Name__c = 'SomeFile.txt';
        cdf.ParentId__c = cd.id;
        cdf.File_Size_in_Bytes__c = 12510550;
        insert cdf;
        
        Client_Document__c cd2 = new Client_Document__c();
        cd2.Document_Category__c = 'Travel';
        cd2.Document_Type__c = 'Flight Ticket';
        cd2.Client__c = client1.id;
        insert cd2;
        
        Client_Flight_Details__c cfd = new Client_Flight_Details__c();
        cfd.Client_Document__c = cd2.id;
        insert cfd;
        
        Client_Document_File__c cdf2 = new Client_Document_File__c();
        cdf2.Client__c = client1.id;
        cdf2.File_Name__c = 'SomeFile.txt';
        cdf2.ParentId__c = cd2.id;
        cdf2.File_Size_in_Bytes__c = 12510550;
        insert cdf2;
        
        Client_Document_File__c cdf3 = new Client_Document_File__c();
        cdf3.Client__c = client1.id;
        cdf3.File_Name__c = 'SomeFile.txt';
        cdf3.ParentId__c = cd2.id;
        cdf3.File_Size_in_Bytes__c = 12510550;
        insert cdf3;
                
        Client_Document__c cd3 = new Client_Document__c();
        cd3.Document_Category__c = 'Personal';
        cd3.Document_Type__c = 'Bank Details';
        cd3.Client__c = client1.id;
        insert cd3;
        
        Client_Document__c cd4 = new Client_Document__c();
        cd4.Document_Category__c = 'Personal';
        cd4.Document_Type__c = 'Drivers License';
        cd4.Client__c = client1.id;
        insert cd4;
        
       
       	Client_Document__c cd5 = new Client_Document__c();
        cd5.Document_Category__c = 'Visa';
        cd5.Document_Type__c = 'Visa Test';
        cd5.Client__c = client1.id;
        insert cd5;
        
        Client_Document_File__c cdfVisa = new Client_Document_File__c();
        cdfVisa.Client__c = client1.id;
        cdfVisa.File_Name__c = 'SomeFile.txt';
        cdfVisa.ParentId__c = cd5.id;
        cdfVisa.File_Size_in_Bytes__c = 12510550;
        insert cdfVisa;
        
        Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(client1);
        contact_documents_list cdl = new contact_documents_list(controller);
        cdl.getDocs();
        
        List<String> order = cdl.categoriesOrder;
        
    
    }
}