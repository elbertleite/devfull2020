/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class client_course_amendment_pay_test {

    static testMethod void myUnitTest() {
    		
		TestFactory tf = new TestFactory();
   
	   	Account school = tf.createSchool();
	  	Account agency = tf.createAgency();
	   	Contact emp = tf.createEmployee(agency);
	   	User portalUser = tf.createPortalUser(emp);
	   	Account campus = tf.createCampus(school, agency);
	   	Course__c course = tf.createCourse();
	   	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
		       
		system.runAs(portalUser){
    	   	Contact client = tf.createLead(agency, emp);
	       	client_course__c booking = tf.createBooking(client);
	       	client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       	client_Course_fees__c ccf = tf.createClientCourseFees(cc, false);
	       	
	       	List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
	       	
	       	instalments[1].Paid_To_School_On__c = System.today();
		   	instalments[1].Extra_Fee_Value__c = 0;
	   		instalments[1].Extra_Fee_Discount_Value__c = 0;
	   		instalments[1].Instalment_Value__c = 500;
	   		instalments[1].Tuition_Value__c = 500;
		   	update instalments[1];
	       	
		   	//Create Underpayment Amendment
		   	client_course_instalment_pay toPay = new client_course_instalment_pay(new ApexPages.StandardController(instalments[1]));
	   		toPay.newPayDetails.Payment_Type__c = 'Creditcard';
		   	toPay.newPayDetails.Value__c = instalments[1].Instalment_Value__c;
		   	toPay.newPayDetails.Date_Paid__c = Date.today();
		   	toPay.addPaymentValue();
		   	toPay.savePayment();
		   	
		   	
		   	
		   	//Create Deposit
		   	client_course_deposit deposit = new client_course_deposit(new ApexPages.StandardController(client));
		   	deposit.newDepositValue.Value__c=200;
	        deposit.newDepositValue.Date_Paid__c=system.today();
	        deposit.newDepositValue.Payment_Type__c = 'Money Transfer';
	        deposit.addDepositValue();
	        deposit.saveDeposit();
		   	
		   	
		   	
		   	
		   	client_course_instalment_amendment instAmend = new client_course_instalment_amendment(new ApexPages.StandardController(instalments[1]));
		   	
		   	instAmend.newTuition = 600; //Change tuition > original tuition
	   		instAmend.saveAmendment();
	   		
	   		system.debug('Overpayment?==>' + instAmend.amendment.isOverpayment_Amendment__c);
	   		system.debug('Commission?==>' + instAmend.amendment.isCommission_Amendment__c);
	   		system.debug('Tuition/Fee?==>' + instAmend.amendment.isInstalment_Amendment__c);
	   		system.debug('amendment.==>' + instAmend.amendment);
	   		
	   		
	   		//Start test
	   		client_course_amendment_pay testClass = new client_course_amendment_pay(new ApexPages.StandardController(instAmend.amendment));
	   		
	   		list<selectOption> depositListType = testClass.depositListType;
	   		
	   		
	   		testClass.newPayDetails.Value__c = 100;
	   		testClass.newPayDetails.Payment_Type__c = 'cash';
	   		testClass.newPayDetails.Date_Paid__c = Date.today();
	   		
	   		
	   		system.debug('totalPay==>' + testClass.totalPay);
	   		system.debug('totalpaid==>' + testClass.totalpaid);
	   		
	   		testClass.addPaymentValue();
	   		
	   		ApexPages.CurrentPage().getParameters().put('index', '0');
	   		testClass.removePaymentValue();
	   		
	   		testClass.newPayDetails.Value__c = 100;
	   		testClass.newPayDetails.Payment_Type__c = 'Covered by Agency';
	   		testClass.newPayDetails.Date_Paid__c = Date.today();
	   		testClass.addPaymentValue();
	   		testClass.savePayment();
		
       	}
    }
}