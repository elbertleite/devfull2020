/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class UserDetails_test {

    static testMethod void myUnitTest() {
    	    	
    	TestFactory tf = new TestFactory();
		Account agency = tf.createAgency();
		Contact employee = tf.createEmployee(agency);
		User portalUser = tf.createPortalUser(employee);	
		tf.createChecklists(agency.Parentid);
    	
    	System.runAs(portalUser) {
	        UserDetails.getMyAgencyGroupID();
	        UserDetails.getMyContactDetails();
	        
	        UserDetails.GroupSettings gs = new UserDetails.GroupSettings();
	        gs.getLeadStatus(agency.Parentid, 'Australia');
	        gs.getClientStatus(agency.Parentid,'Australia');
	        gs.getLeadSourceSpecifics(agency.parentid, 'Fair');
	        gs.getTaskSubjects(agency.parentid);
	        gs.getBackgroundColour(agency.parentid);
			gs.getLabelColour(agency.parentid);
			gs.getAgencyGroupChecklist(agency.parentid);
			Set<ID> groupsWorkingOnClient = new Set<ID>();
			groupsWorkingOnClient.add(portalUser.Contact.Account.Parent.ID);
			Set<String> destinationsCycle = new Set<String>();
			destinationsCycle.add('Australia');

			gs.getClientStageList(employee, groupsWorkingOnClient, portalUser, true);
			gs.getAgencyChecklist(employee, groupsWorkingOnClient, portalUser, 'Australia');
			gs.getClientStageZeroMultipleDestinations(employee, groupsWorkingOnClient, portalUser, true);
			gs.getClientStageListMultipleDestinations(employee, groupsWorkingOnClient, portalUser, true, destinationsCycle);

			gs.getClientStageList(employee, groupsWorkingOnClient, portalUser, false);
			gs.getClientStageZeroMultipleDestinations(employee, groupsWorkingOnClient, portalUser, false);
			gs.getClientStageListMultipleDestinations(employee, groupsWorkingOnClient, portalUser, false, destinationsCycle);
    	}
        
    }
}