public with sharing class report_statusPipeline {
    
    public report_statusPipeline(){
		
		selectedAgencyGroup = myDetails.Account.Parentid;		
		selectedAgency = mydetails.Accountid;
		
       
		search();
	}
    
    
    public Contact myDetails {
		get{
			if(myDetails == null)
				myDetails = UserDetails.getMyContactDetails();
			return myDetails;
		}
		set;
	}
	
	public PageReference checkUserAccess(){

     	if(!myDetails.Management_Report_Permission__c){
      		PageReference retURL = new PageReference('/apex/agency_reports');
      		retURL.setRedirect(true);
      		return retURL;
      	} else return null;      	
      	
 	}
    
	
	public String selectedAgencyGroup {get;set;}
	private List<SelectOption> agencyGroupOptions;
	public List<SelectOption> getAgencyGroupOptions() {
		if(agencyGroupOptions == null){
			agencyGroupOptions = new List<SelectOption>();
			if(myDetails.Account.Parent.Destination_Group__c || myDetails.Global_Manager__c){
				agencyGroupOptions.add(new SelectOption('all', '-- ALL --'));
				for(Account ag : IPFunctionsGLobal.getAgencyGroups(myDetails.Account.Parent.Global_Link__c))
					agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
			} else {
				for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ])
					agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
			}
		}
		
		return agencyGroupOptions;
	}
	
	public void refreshAgencies(){
		selectedAgency = null;		
		agencyOptions = null;		
		getAgencyOptions();		
		//leadSourceOptions = null;
		//getLeadSourceOptions();
	}
	
	public String selectedAgency {get;set;}
	private List<SelectOption> agencyOptions;
	public List<SelectOption> getAgencyOptions(){
		if(agencyOptions == null){
			agencyOptions = new List<SelectOption>();
			agencyOptions.add(new SelectOption('all', '-- ALL --'));
			if(selectedAgencyGroup != null)
				for(Account agency : [select id, name from Account where RecordType.Name = 'Agency' and parentid = :selectedAgencyGroup order by Name])
					agencyOptions.add(new SelectOption(agency.id, agency.Name));
			
		}
		return agencyOptions;
	}
	
	public Contact range {
		get {
			if(range == null){
				range = new Contact();
				range.Arrival_Date__c = system.today().addMonths(-5); 
				range.Expected_Travel_Date__c = system.today();
			}
			return range;
		}
		set;
	}
	
	
	public class StageWrapper {
		public Integer total {get;set;}
		public Map<String, Integer> statusMap {get {if(statusMap == null) statusMap = new Map<String, Integer>(); return statusMap;} set;}
		public String sliceColour {get;set;}
		public Integer statusThreshold {get;set;}
	}
	
	public Map<String, StageWrapper> stageMap { get;set; }
    public Integer threshold {get;set;}
  	public Integer totalLeads {get;set;}
  	
	public void search(){
		
		stageMap = new Map<String, StageWrapper>();
		
		String query = 'SELECT Lead_Stage__c stage, Status__c status, Count(ID) total from Contact ';
		String queryWhere = getQueryFilters();
		String queryGroup = ' group by Lead_Stage__c, Status__c ';
		String queryOrder = ' order by Lead_Stage__c, Status__c ';
		
		String theQuery = query + queryWhere + queryGroup + queryOrder;
		system.debug('theQuery 0: ' + theQuery);
		
		Map<String, Integer> totalStage = new Map<String, Integer>();
		totalLeads = 0;
		
		for(AggregateResult ar: Database.query(theQuery)){
			String stage = (String) ar.get('stage');
			String status = (String) ar.get('status');
			Integer total = Integer.valueOf(ar.get('total'));
			
			totalLeads += total;
			
			//if LEAD - Not Interested, add to Stage 5.
			if(status.equalsIgnoreCase('LEAD - Not Interested'))
				stage = 'Stage 5';
			
						
			if(stageMap.containsKey(stage)){
				stageMap.get(stage).statusMap.put(status, total);				
				totalStage.put(stage, totalStage.get(stage) + total);
			} else {
				StageWrapper sw = new StageWrapper();
				sw.sliceColour = getStageColour(stage);
				sw.statusMap = new Map<String, Integer>{status => total};
				stageMap.put(stage, sw);
				totalStage.put(stage, total);
			}
			
		}
		
		for(String key : stageMap.keySet()){
			stageMap.get(key).total = totalStage.get(key);
			stageMap.get(key).statusThreshold = Integer.valueOf((stageMap.get(key).total / stageMap.get(key).statusMap.keySet().size()) * 0.25);
		}
		
        
        if(totalLeads > 0)
        	threshold = Integer.valueOf((totalLeads / stageMap.keySet().size()) * 0.5);
        
	}
	
	private String getQueryFilters(){
		String queryWhere = ' where RecordType.Name in (\'Lead\', \'Client\') ';
		
        if(range.Arrival_Date__c != null)
            queryWhere += ' and DAY_ONLY(CreatedDate) >= ' + IPFunctions.FormatSqlDateIni(range.Arrival_Date__c);
        if(range.Expected_Travel_Date__c != null)		
            queryWhere += ' and DAY_ONLY(CreatedDate) <= ' + IPFunctions.FormatSqlDateIni(range.Expected_Travel_Date__c);
    
					
		if(selectedAgency != 'all')
			queryWhere += ' and Current_Agency__c = :selectedAgency ';
		else if(selectedAgencyGroup != 'all')
			queryWhere += ' and Current_Agency__r.Parentid = :selectedAgencyGroup ';
		
       	queryWhere += ' and Lead_Stage__c != null and Status__c != null ';
        
		return queryWhere;
	}
	
	private String getStageColour(String stage){
		
		if(stage == 'Stage 1')
			return '#67B7DC';
		else if(stage == 'Stage 2')
			return '#FDD400';
		else if(stage == 'Stage 3')
			return '#84B761';
		else if(stage == 'Stage 4')
			return '#CC4748';
		else if(stage == 'Stage 5')
			return '#CD82AD';
		else
			return '#2563D6';
		
	}
	
	
	
	
	
	
	
	
    
}