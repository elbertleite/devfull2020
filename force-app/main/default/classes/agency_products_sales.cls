public without sharing class agency_products_sales {

    public agency_products_sales(){
    	searchProducts();
    }

	//TODO: REMOVE METHOD BELOW
	public list<SelectOption> getCategories(){return null;}
	public list<SelectOption> getcommType(){return null;}
	public list<SelectOption> getAgencies(){return null;}
	//end todo

    public map<string, list<client_product_service__c>> listProducts{get; set;}

    private map<string,client_product_service__c> mapFisrtValues;

	public Decimal sumCommission {get;set;}

    public void searchProducts(){
    	String  fromDate = IPFunctions.FormatSqlDateIni(dateFilter.Original_Due_Date__c);
		String  toDate = IPFunctions.FormatSqlDateFin(dateFilter.Discounted_On__c);
		string sqlFilter = '';
		string sqlOrder = '';

		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && currentUser.Aditional_Agency_Managment__c == null) //set the user to see only his own agency
			selectedAgency = currentUser.Contact.AccountId;

    	string sql = 'Select Booking_Number__c, Commission_Paid_Date__c, Commission_Confirmed_By__r.Name, Commission_Confirmed_By_Agency__r.Name, Related_to_Product__c, Related_to_Product__r.Currency__c, Products_Services__r.Provider__r.Provider_Name__c, CreatedBy.name, Received_by__r.name, Sold_by_Agency__r.name, Category__c, Client__c, Client__r.Name, client_course__c, Commission__c, Commission_Tax_Rate__c, Commission_Tax_Value__c, Commission_Type__c, Commission_Value__c, Confirmed_By__c, Confirmed_Date__c, CreatedDate, Currency__c, Description__c, Invoice__c, isCustom__c, isSelected__c, Price_Total__c, Price_Unit__c, Product_Name__c, Products_Services__c, Quantity__c, Received_By_Agency__r.name, 	Covered_by_Agency__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, Received_by__c, Received_Date__c, Id, Unit_Description__c, isRefund_Requested__c, Refund_Start_Date__c, Agency_to_Refund__c, Agency_Commission_Refund__c, Agency_Refund_Paid_Date__c, Refund_amount__c, Agency_Refund_Client__c, Refund_Confirmed_ADM__c, Total_Refund__c, Refund_Paid_to_ProviderOn__c FROM client_product_service__c where Sold_By_Agency__r.ParentId = :selectedAgencyGroup and ';

     	if(selectedAgency != 'all')
     		sqlFilter += ' Sold_By_Agency__c = :selectedAgency AND ';

     	if(selectedCategory != 'all')
     		sqlFilter	+= ' Category__c = :selectedCategory AND ';
     	
		if(selectedProvider != 'all')
     		sqlFilter	+= ' Products_Services__r.Provider__c = :selectedProvider AND ';
     	
		if(selectedProduct != 'all')
     		sqlFilter	+= ' Products_Services__c = :selectedProduct AND ';


     	if(dateCriteria == 'cc'){ //COMMISSION CONFIRMED
			if(SelectedPeriod == 'range')
				sqlFilter += ' DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) >= '+fromDate+' AND  DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) <= '+toDate;
			else if(SelectedPeriod == 'THIS_WEEK')
				sqlFilter += ' Commission_Confirmed_On__c = THIS_WEEK ';
			else if(SelectedPeriod == 'LAST_WEEK')
				sqlFilter += ' Commission_Confirmed_On__c = LAST_WEEK ';
			else if(SelectedPeriod == 'THIS_MONTH')
				sqlFilter += ' Commission_Confirmed_On__c = THIS_MONTH ';
			else if(SelectedPeriod == 'LAST_MONTH')
				sqlFilter += ' Commission_Confirmed_On__c = LAST_MONTH ';

			sqlOrder += ' order by Commission_Confirmed_On__c';

		}else if(dateCriteria == 'pc'){ //Pending Commission
			if(SelectedPeriod == 'range')
				sqlFilter += ' DAY_ONLY(convertTimezone(Received_Date__c)) >= '+fromDate+' AND DAY_ONLY(convertTimezone(Received_Date__c)) <= '+toDate;
			else if(SelectedPeriod == 'THIS_WEEK')
				sqlFilter += ' 	Received_Date__c = THIS_WEEK ';
			else if(SelectedPeriod == 'LAST_WEEK')
				sqlFilter += ' 	Received_Date__c = LAST_WEEK ';
			else if(SelectedPeriod == 'THIS_MONTH')
				sqlFilter += ' 	Received_Date__c = THIS_MONTH ';
			else if(SelectedPeriod == 'LAST_MONTH')
				sqlFilter += ' 	Received_Date__c = LAST_MONTH ';
			

			sqlOrder += ' AND Commission_Confirmed_On__c = NULL AND Claim_Commission_Type__c != \'Commission not claimable\' order by CreatedDate';

		}else if(dateCriteria == 'cnp'){ //CLIENT NOT PAID
			sqlFilter += ' Received_Date__c = null ';
			sqlOrder += ' order by Currency__c, Received_Date__c';
		}

		else if(dateCriteria == 'rd'){ 
			if(SelectedPeriod == 'range')
				sqlFilter += ' DAY_ONLY(convertTimezone(Received_Date__c)) >= '+fromDate+' AND  DAY_ONLY(convertTimezone(Received_Date__c)) <= '+toDate;
			else if(SelectedPeriod == 'THIS_WEEK')
				sqlFilter += ' Received_Date__c = THIS_WEEK ';
			else if(SelectedPeriod == 'LAST_WEEK')
				sqlFilter += ' Received_Date__c = LAST_WEEK ';
			else if(SelectedPeriod == 'THIS_MONTH')
				sqlFilter += ' Received_Date__c = THIS_MONTH ';
			else if(SelectedPeriod == 'LAST_MONTH')
				sqlFilter += ' Received_Date__c = LAST_MONTH ';

			sqlOrder += ' order by Received_Date__c';
		}

		sql += sqlFilter + sqlOrder;

		System.debug('==>selectedAgencyGroup: '+selectedAgencyGroup);
		System.debug('==>selectedAgency: '+selectedAgency);
		System.debug('==>sql: '+sql);
		mapFisrtValues = new map<string,client_product_service__c>();
		listProducts = new map<string, list<client_product_service__c>>();

		sumCommission = 0;

     	for(client_product_service__c ps:Database.Query(sql)){
     		mapFisrtValues.put(ps.id, ps.clone(true));

			sumCommission += ps.Commission_Value__c;
			if(ps.isRefund_Requested__c && ((ps.Agency_Refund_Client__c == 'No' && ps.Refund_Paid_to_ProviderOn__c != NULL) || (ps.Agency_Refund_Client__c == 'Yes' && ps.Agency_to_Refund__c == selectedAgency && ps.Refund_Confirmed_ADM__c == true))){
				ps.isSelected__c = true;
				sumCommission += ps.Agency_Commission_Refund__c;
			}

     		if(!listProducts.containsKey(ps.currency__c)){
     			listProducts.put(ps.currency__c, new list<client_product_service__c>{ps});
     		}else{
     			listProducts.get(ps.currency__c).add(ps);
     		}
     	}

     	//mapFisrtValues = new map<string,client_product_service__c>(listProducts.deepclone(true));

     	//retrieveProductSummary(fromDate, toDate, sqlFilter);
    }

	// TODO: DELETE summaryProductValues DEPLOY THIS METHOD
   public AggregateResult summaryProductValues{get; set;}
   
    // public void retrieveProductSummary(string fromDate, string toDate, string sqlFilter){
	// 	string sql = 'Select count(id) totItens, SUM(Price_Total__c) totPrice, SUM(Commission_Value__c) totCommission ';
	// 	sql += '  from client_product_service__c where Sold_By_Agency__r.ParentId = :selectedAgencyGroup and ';
	// 	sql += sqlFilter;

	// 	summaryProductValues = Database.query(sql);
	// }

  public PageReference generateExcel(){
		PageReference pr = Page.agency_products_sales_excel;
		pr.setRedirect(false);
		return pr;
	}

  public void searchExcel(){
    searchProducts();
  }

  public pageReference saveUpdates(){
  	list<client_product_service__c> listUpdate = new list<client_product_service__c>();
  	client_product_service__c ps;
  	for(string lps:listProducts.keySet()){
	  	for(client_product_service__c cps:listProducts.get(lps)){
	  		ps = mapFisrtValues.get(cps.id);
	  		System.debug('==>ps: '+ps);
	  		System.debug('==>cps: '+cps);
	  		if(cps.Commission__c != ps.Commission__c || cps.Commission_Type__c != ps.Commission_Type__c){
	  			if(cps.Commission_Type__c == 0){
	  				cps.Commission_Value__c = cps.Price_Total__c * cps.Commission__c/100;
	  				listUpdate.add(cps);
	  			}else {
	  				cps.Commission_Value__c = cps.Commission__c;
	  				listUpdate.add(cps);
	  			}
	  		}
	  	}
  	}
  	if(listUpdate.size() > 0){
  		update listUpdate;
  		searchProducts();
  	}
  	return null;
  }

   public pageReference cancelUpdates(){
   	searchProducts();
   	return null;
   }

    /********************** Filters **********************/
	private User currentUser {get{if(currentUser==null) currentUser = [Select Id, Name, Aditional_Agency_Managment__c, Contact.Account.ParentId, Contact.AccountId, Contact.Department__c from User where id = :UserInfo.getUserId() limit 1]; return currentUser;}set;}

	public string selectedAgencyGroup{
    	get{
    		if(selectedAgencyGroup == null)
    			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
    		return selectedAgencyGroup;
    	}
    	set;
    }

	 public List<SelectOption> agencyGroupOptions {
  		get{
   			if(agencyGroupOptions == null){

   				agencyGroupOptions = new List<SelectOption>();
   				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group
	    			for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:currentUser.Contact.AccountId order by Parent.Name]){
		     			agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
		    		}
   				}else{
   					for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
		     			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
		    		}
   				}
	   		}
	   		return agencyGroupOptions;
	  }
	  set;
 	}
	 
	public void changeGroup(){
		agencyOptions = null;
	}

	// Agency Options
	public String selectedAgency {get{if(selectedAgency == null) selectedAgency = currentUser.Contact.AccountId; return selectedAgency;}set;}
	public list<SelectOption> agencyOptions {get{
		if(agencyOptions == null){
			agencyOptions = new List<SelectOption>();
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
				for(Account a: [Select Id, Name, Account_Currency_Iso_Code__c from Account where id =:currentUser.Contact.AccountId order by name]){
					if(selectedAgency == null)
						selectedAgency = a.Id;					
					agencyOptions.add(new SelectOption(a.Id, a.Name));
				}
				if(currentUser.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
						agencyOptions.add(new SelectOption(ac.id, ac.name));
					}
				}
			}else{
				for(Account a: [Select Id, Name, Account_Currency_Iso_Code__c from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
					agencyOptions.add(new SelectOption(a.Id, a.Name));
					if(selectedAgency == null)
						selectedAgency = a.Id;	
				}
			}

			findCategories();
		}

		return agencyOptions;
		
	}set;}

	
	public void changeAgency(){
		findCategories();
	}

	//Category
    public string selectedCategory{get {if(selectedCategory == null) selectedCategory = 'all'; return selectedCategory; } set;}
	public List<SelectOption> categoriesOpt {get;set;}
	public void findCategories(){
		categoriesOpt = new List<SelectOption>();

		categoriesOpt.add(new SelectOption('all','-- All --'));
		for(AggregateResult ag : [SELECT Products_Services__r.Category__c cat FROM client_product_service__c WHERE Sold_By_Agency__c = :selectedAgency group by Products_Services__r.Category__c]){ // AND Products_Services__r.Provider__c = :selectedProvider 
			if((String) ag.get('cat') != NULL)
				categoriesOpt.add(new SelectOption((String) ag.get('cat'), (String) ag.get('cat')));
		}//end for


		selectedCategory = 'all';
		changeCategory();

		// if(selectedProvider != 'all')
		// 	findCatProducts();
		// else{
		// 	agencyProducts = new List<SelectOption>();
		// 	agencyProducts.add(new SelectOption('all','-- All --'));
		// }
	}

	public void changeCategory(){
		findProviders();
	}


	//Providers
	public string selectedProvider{
    	get{
    		if(selectedProvider == null)
    			selectedProvider = 'all';
    		return selectedProvider;
    	}set;}
	public List<SelectOption> providersOpt {get;set;}
	public void findProviders(){
		providersOpt = new List<SelectOption>();
		providersOpt.add(new SelectOption('all','-- All --'));

		String sql = 'SELECT Products_Services__r.Provider__c pId, Products_Services__r.Provider__r.Provider_Name__c pName FROM client_product_service__c WHERE Sold_By_Agency__c = :selectedAgency ';

		if(selectedCategory != 'all')
			sql+= ' AND Products_Services__r.Category__c = :selectedCategory ';

		sql+= ' group by Products_Services__r.Provider__c, Products_Services__r.Provider__r.Provider_Name__c order by Products_Services__r.Provider__r.Provider_Name__c ';

		for(AggregateResult ag : Database.query(sql))
			if((String) ag.get('pId') != null)
				providersOpt.add(new SelectOption((String) ag.get('pId'), (String) ag.get('pName')));

		selectedProvider = 'all';
		findCatProducts();
	}

	public void changeProvider(){
		findCatProducts();
	}
	

	// Products
	public string selectedProduct{get;set;}
	public List<SelectOption> agencyProducts {get;set;}

	public void findCatProducts(){
		agencyProducts = new List<SelectOption>();
		agencyProducts.add(new SelectOption('all','-- All --'));

		String sql = 'SELECT Products_Services__c pId, Product_Name__c nm FROM client_product_service__c WHERE Sold_By_Agency__c = \'' + selectedAgency  + '\' ';

		if(selectedProvider != 'none' && selectedProvider != 'all')
			sql += ' AND Products_Services__r.Provider__c = \'' + selectedProvider + '\' ';

		if(selectedCategory != 'none' && selectedCategory != 'all')
			sql += ' AND ( Products_Services__r.Category__c = \'' + selectedCategory + '\' OR Products_Services__r.Category__c = NULL)';

		sql += ' group by Products_Services__c, Product_Name__c order by Product_Name__c';

		system.debug('products sql ==> ' + sql);

		for(AggregateResult ag : Database.query(sql))
			if((String) ag.get('pId') != null)
				agencyProducts.add(new SelectOption((String) ag.get('pId'), (String) ag.get('nm')));

		selectedProduct = 'all';
	}

    public client_course_instalment__c dateFilter{
		get{
			if(dateFilter == null){
				dateFilter = new client_course_instalment__c();
				Date myDate = Date.today();
				Date weekStart = myDate.toStartofWeek();
				dateFilter.Original_Due_Date__c = weekStart;
				dateFilter.Discounted_On__c = dateFilter.Original_Due_Date__c.addDays(6);
			}
			return dateFilter;
		}
		set;
	}

    public string SelectedPeriod{get {if(SelectedPeriod == null) SelectedPeriod = 'THIS_MONTH'; return SelectedPeriod; } set;}
    private List<SelectOption> periods;
    public List<SelectOption> getPeriods() {
        if(periods == null){
            periods = new List<SelectOption>();
            periods.add(new SelectOption('THIS_WEEK','This Week'));
            periods.add(new SelectOption('LAST_WEEK','Last Week'));
            periods.add(new SelectOption('THIS_MONTH','This Month'));
            periods.add(new SelectOption('LAST_MONTH','Last Month'));
            periods.add(new SelectOption('range','Range'));
        }
        return periods;
    }

    public string dateCriteria{get {if(dateCriteria == null) dateCriteria = 'cc'; return dateCriteria; } set;}
    private List<SelectOption> dateCriteriaOptions;
    public List<SelectOption> getdateCriteriaOptions() {
        if(dateCriteriaOptions == null){
            dateCriteriaOptions = new List<SelectOption>();
            dateCriteriaOptions.add(new SelectOption('cc','Commission Confirmed'));
            dateCriteriaOptions.add(new SelectOption('pc','Pending Commission'));
            dateCriteriaOptions.add(new SelectOption('cnp','Client not Paid'));
            dateCriteriaOptions.add(new SelectOption('rd','Received Date'));
        }
        return dateCriteriaOptions;
    }

}