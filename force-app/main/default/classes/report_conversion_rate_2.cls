public with sharing class report_conversion_rate_2 {
    
  public User user{get;set;}
  public List<SelectOption> agencies{get;set;}
  public List<SelectOption> campaigns{get;set;}
  public List<SelectOption> employees{get;set;}

  public String employeeSelected{get;set;}
  public List<String> campaignSelected{get;set;}
  public String agencySelected{get;set;}
  public String groupSelected{get;set;}
  public String destinationSelected{get;set;}
  /*public String reasonSelected{get;set;}*/
  public String endDate{get;set;}
  public String beginDate{get;set;}

  public String graph{get;set;}

  public Double conversionRate{get;set;}

  public String typeOfSearch{get;set;}

  public boolean showClientsEnroled{get{if(showClientsEnroled == null) showClientsEnroled = false; return showClientsEnroled;} set;}

  //public List<AverageTimeReport> averageTime{get;set;}

  public List<ContactsForExcel> resultExcel{get;set;}

  public List<ReportResult> reportResult{get;set;}

  public Set<String> idAgencies{get;set;}

  public boolean startingPage{get;set;}
  public boolean canExportToExcel{get;set;}

  String firstStatusStageZero{get;set;}
  
  public report_conversion_rate_2() {
    try{

      user = [Select Id, Name, Report_Convertion_Rate_Filters__c, Contact.Export_Report_Permission__c, Contact.AccountId, Contact.Name, Contact.Account.Id, Contact.Account.Name, Contact.Account.Parent.RDStation_Campaigns__c, Contact.Account.Parent.Name, Contact.Account.Parent.Id, Aditional_Agency_Managment__c, Contact.Group_View_Permission__c, Contact.Agency_View_Permission__c, Contact.Account.Global_Link__c from User where Id = :UserInfo.getUserId()];
      
      canExportToExcel = user.Contact.Export_Report_Permission__c;
      agencySelected = user.Contact.Account.Id;
      groupSelected = user.Contact.Account.ParentId;

      resetFirstClick();

      //system.debug('REPORT RESULT OBJECT '+JSON.serialize(reportResult));

      if(!String.isEmpty(ApexPages.currentPage().getParameters().get('export')) && Boolean.valueOf(ApexPages.currentPage().getParameters().get('export'))){
        Map<String, String> filters = (Map<String, String>) JSON.deserialize(user.Report_Convertion_Rate_Filters__c, Map<String, String>.class); 
        
        beginDate = filters.get('beginDate');
        endDate = filters.get('endDate');
        groupSelected = filters.get('groupSelected');
        agencySelected = filters.get('agencySelected');
        destinationSelected = filters.get('destinationSelected');
        campaignSelected = (List<String>) JSON.deserialize(filters.get('campaignSelected'), List<String>.class);
        typeOfSearch = filters.get('typeOfSearch');

        if(agencySelected == 'all'){
          this.agencies = loadAgencies(null, groupSelected);
        }

        resetFirstClick();
        generateExcel();
      }else{
        startingPage = true;
        typeOfSearch = 'firstClick';
        endDate = Date.today().format();
        beginDate = Date.today().addMonths(-1).format();
        destinationSelected = '';
        campaignSelected = new List<String>();
        employeeSelected = user.Contact.ID;
        this.agencies = loadAgencies(user.Contact.AccountId, user.Contact.Account.Parent.Id);
        this.campaigns = loadCampaigns();
        this.employees = loadEmployees(user.Contact.AccountId);
      
      }
    }catch(Exception e){
      system.debug('Error on report_not_assigned_new_leads() ===> ' + e.getLineNumber() + ' '+e.getMessage());  
    }
  }

  public void resetFirstClick(){
    firstStatusStageZero = null;
    reportResult = new List<ReportResult>();
    ReportResult result;
    for(Client_Stage__c stage : [SELECT ID, Stage_description__c, itemOrder__c FROM Client_Stage__c WHERE Agency_Group__c = :groupSelected AND Stage__c = 'Stage 0' AND Stage_description__c != 'LEAD - Not Interested' ORDER BY itemOrder__c]){
      if(!stage.Stage_description__c.contains('xxx')){
        if(String.isEmpty(firstStatusStageZero)){
          firstStatusStageZero = stage.id;
        }
        result = new ReportResult(); 
        result.statusID = stage.ID;
        result.status = stage.Stage_description__c.trim();
        result.itemOrder = stage.itemOrder__c != null ? stage.itemOrder__c.intValue() : 0;
        reportResult.add(result);
      }
    }
    //SYSTEM.DEBUG('FIRST ITEM ID '+firstStatusStageZero);
    ReportResult converted = new ReportResult('SUMMARY');
    reportResult.add(converted);
  }

  public void generateExcel(){
    try{
      
      Map<String, String> contactsCycle = getDestinationTrackingsForContacts();
      
      List<String> dTrackings = contactsCycle.values(); 
      
      String query = generateQueryForExcel();
      List<Destination_Tracking__c> result = new MethodsWithoutSharing().queryExcel(query, beginDate, endDate, groupSelected, idAgencies, firstStatusStageZero, agencySelected, destinationSelected, employeeSelected, campaignSelected, dTrackings);

      //system.debug('RESULTS FOUND FOR EXCEL '+JSON.serialize(result));

      resultExcel = new List<ContactsForExcel>();
      ContactsForExcel ctt;
      for(Destination_Tracking__c cycle : result){
        ctt = new ContactsForExcel();
        ctt.contactID = cycle.Client__r.ID;
        ctt.contactEmail = cycle.Client__r.Email;
        ctt.contactName = cycle.Client__r.Name;
        ctt.rdStationCampaign = cycle.RDStation_Campaigns__c;
        
        if(cycle.Client__r.RecordType.Name == 'Client'){
          ctt.contactStatus = 'Client';
        }else if(cycle.Client__r.RecordType.Name == 'Lead'){
          ctt.contactStatus = 'Lead';
        }
        if(cycle.Client__r.Status__c == 'LEAD - Not Interested'){
          ctt.contactStatus = cycle.Client__r.Lead_Stage__c == 'Stage 0' ? 'Lead Lost' : 'Client Cancelled'; 
        }
        ctt.status = cycle.Lead_Last_Status_Cycle__c == 'LEAD - Not Interested' ? cycle.Client__r.Last_Stage_Before_Not_Interested__c : cycle.Lead_Last_Status_Cycle__c;
        ctt.stageStatus = cycle.Lead_Last_Status_Cycle__c == 'LEAD - Not Interested' ? cycle.Client__r.Lead_Stage__c : cycle.Lead_Last_Stage_Cycle__c;
        ctt.destination = cycle.Destination_Country__c;
        ctt.checkedDate = cycle.Lead_Last_Change_Status_Cycle__c;
        ctt.createdDate = cycle.Client__r.CreatedDate;
        ctt.conversionDate = cycle.Lead_Last_Status_Cycle__c == 'LEAD - Not Interested' ? cycle.Client__r.Lead_Status_Date_Time__c : cycle.Client__r.Lead_Converted_On__c;
        ctt.checkedBy = cycle.Client__r.Owner__r.Name;
        ctt.currentAgency = cycle.Client__r.Current_Agency__r.Name;
        ctt.enrolledCourse = cycle.Client__r.Client_Enrolled_In_Course__c ? 'YES' : 'NO';

        resultExcel.add(ctt);
      }

    }catch(Exception e){
      system.debug('Error on generateExcel() ===> ' + e.getLineNumber() + ' '+e.getMessage());  
    }  
  }

  public void generateReport(){
    Map<String, String> filters = new Map<String, String>();
    startingPage = false;
    graph = '';
    try{
      if(destinationSelected == null){
        destinationSelected = '';
      }
      if(campaignSelected == null){
        campaignSelected = new List<String>();
      }
      if(employeeSelected == null){
        employeeSelected = '';
      }

      Map<String, Integer> itemsOrder = new Map<String, Integer>();
      for(ReportResult itemResult : reportResult){
        itemResult.eraseValues();
        itemsOrder.put(itemResult.status, itemResult.itemOrder);
      }

      Map<String, String> contactsCycle = getDestinationTrackingsForContacts();

      if(!contactsCycle.isEmpty()){
        List<String> dTrackings = contactsCycle.values(); 
        system.debug('THE DTRACKINGS FOUND '+JSON.serialize(dTrackings));
        system.debug('THE TOTAL CLIENTS '+contactsCycle.keySet().size());

        String query = generateQueryForReport();
        //system.debug('THE QUERY TO GET THE CHECKLIST '+query);
        List<Client_Stage_Follow_Up__c> result = new MethodsWithoutSharing().queryFollowUp(query, beginDate, endDate, groupSelected, idAgencies, firstStatusStageZero, agencySelected, destinationSelected, employeeSelected, campaignSelected, dTrackings);
        system.debug('RESULT FOUND '+JSON.serialize(result));

        Map<String, String> clientsToRemoveFromStagnated = new Map<String, String>(); //Map necessary because contacts converted must be remove from the stagnated bar, otherwise the numbers won't match. 
        Map<String, String> clientsWithCourses = new Map<String, String>(); //Map necessary because contacts converted must be remove from the stagnated bar, otherwise the numbers won't match. 

        //system.debug('THE REPORT RESULT '+JSON.serialize(reportResult));

        if(result != null && !result.isEmpty()){
          ReportResult item;
          Integer index;
          Integer totalLost = 0;
          Set<String> totalClients = new Set<String>(); 

          Map<String, List<AverageTimePerContact>> averageTimePerClient = new Map<String, List<AverageTimePerContact>>();
          Map<String, Datetime> dateClientLostOrWon = new Map<String, Datetime>();

          List<AverageTimePerContact> timeAverage;
          List<Client_Stage_Follow_Up__c> strange = new List<Client_Stage_Follow_Up__c>();
          for(Client_Stage_Follow_Up__c stage : result){
            if(averageTimePerClient.get(stage.Client__c) == null){
              timeAverage = new List<AverageTimePerContact>();
              timeAverage.add(new AverageTimePerContact(stage.Stage_Item__c , stage.Last_Saved_Date_Time__c, itemsOrder.get(stage.Stage_Item__c)));
              averageTimePerClient.put(stage.Client__c, timeAverage);
            }else{
              timeAverage = averageTimePerClient.get(stage.Client__c);
              timeAverage.add(new AverageTimePerContact(stage.Stage_Item__c , stage.Last_Saved_Date_Time__c, itemsOrder.get(stage.Stage_Item__c)));
            }

            if(stage.Client__r.RecordType.Name == 'Client' && stage.Client__r.Status__c != 'LEAD - Not Interested'){
              if(stage.Client__r.Client_Enrolled_In_Course__c){
                clientsWithCourses.put(stage.Client__c, stage.Stage_Item__c);
              }else{
                clientsToRemoveFromStagnated.put(stage.Client__c, stage.Stage_Item__c);
              }
              
              totalClients.add(stage.Client__c);
              dateClientLostOrWon.put(stage.Client__c, stage.Client__r.Lead_Converted_On__c);
            }
            
            if(stage.Stage_Item__c == 'LEAD - Not Interested'){
              for(ReportResult itemResult : reportResult){
                if(itemResult.status == stage.Client__r.Last_Stage_Before_Not_Interested__c){
                  totalLost += 1;
                  itemResult.lost += 1;
                }
              }
              dateClientLostOrWon.put(stage.Client__c, stage.Last_Saved_Date_Time__c);
            }else{
              item = new ReportResult(stage.Stage_Item__c.trim()); 
              index = reportResult.indexOf(item);
              if(index > -1){
                item = reportResult.get(index);
                if(!item.contacts.contains(stage.Client__c)){
                  ////system.debug('WATCHING CONTACT '+stage.Client__c+' CURRENT TOTAL '+item.total);
                  item.total += 1;
                  item.contacts.add(stage.Client__c);
                }else{
                  //system.debug('ENTROU NESSE ELSE');
                }
              }else{
                //system.debug('ENTROU NESSE ELSE 2');
              }
            }
          }

          //system.debug('INITIAL AVERAGE TIME PER CLIENT '+JSON.serialize(averageTimePerClient));
          //system.debug('THE REPORT RESULT '+JSON.serialize(reportResult));
          //system.debug('THE CLIENTS STAGNATED '+JSON.serialize(clientsToRemoveFromStagnated));
          //system.debug('THE STRANGE CLICKS '+JSON.serialize(strange));
          //system.debug('THE STRANGE CLICKS SIZE'+strange.size());

          for(String contact : averageTimePerClient.keySet()){
            timeAverage = averageTimePerClient.get(contact);
            timeAverage.sort();
            for(Integer i = 0; i < timeAverage.size(); i++){
              if(i + 1 < timeAverage.size()){
                //timeAverage.get(i).daysInStatus = timeAverage.get(i + 1).dateSaved.date().daysBetween(timeAverage.get(i).dateSaved.date());
                if(!Test.isRunningTest()){
                  timeAverage.get(i).daysInStatus = timeAverage.get(i).dateSaved.date().daysBetween(timeAverage.get(i+1).dateSaved.date());
                }
              }else{
                if(dateClientLostOrWon.get(contact) != null){
                  //timeAverage.get(i).daysInStatus = dateClientLostOrWon.get(contact).date().daysBetween(timeAverage.get(i).dateSaved.date());
                  if(dateClientLostOrWon.get(contact).date() >= timeAverage.get(i).dateSaved.date()){
                    timeAverage.get(i).daysInStatus = timeAverage.get(i).dateSaved.date().daysBetween(dateClientLostOrWon.get(contact).date());
                  }else{
                    timeAverage.get(i).daysInStatus = 0;
                  }
                }else{
                  //timeAverage.get(i).daysInStatus = Date.today().daysBetween(timeAverage.get(i).dateSaved.date());
                  timeAverage.get(i).daysInStatus = timeAverage.get(i).dateSaved.date().daysBetween(Date.today());
                }
              }
            }
          }
          //system.debug('CALCULATED AVERAGE TIME PER CLIENT '+JSON.serialize(averageTimePerClient));

          AverageTimePerContact timeAverageItem;
          for(Integer i = 0; i < reportResult.size(); i++){
            item = reportResult.get(i);
            item.stagnated = Math.abs(item.total - item.lost);
            if(i + 1 < reportResult.size() && reportResult.get(i + 1).status != 'SUMMARY'){
              item.stagnated = Math.abs(item.stagnated - reportResult.get(i+1).total);  
            }
            item.current = Math.abs(item.total - item.lost - item.stagnated);
            for(String contact : averageTimePerClient.keySet()){
              timeAverage = averageTimePerClient.get(contact);
              index = timeAverage.indexOf(new AverageTimePerContact(item.status, null, null));
              if(index > -1){
                timeAverageItem = timeAverage.get(index);
                item.totalDaysInStatus += timeAverageItem.daysInStatus;
              }
            }   
          }

          if(!clientsToRemoveFromStagnated.isEmpty()){
            for(String statusClientStagnated : clientsToRemoveFromStagnated.values()){
              item = new ReportResult(statusClientStagnated); 
              index = reportResult.indexOf(item);
              if(index > -1){
                item = reportResult.get(index);
                item.won += 1;
                item.stagnated -= 1;
              }
            }
          }
          if(!clientsWithCourses.isEmpty()){
            for(String statusClientStagnated : clientsWithCourses.values()){
              item = new ReportResult(statusClientStagnated); 
              index = reportResult.indexOf(item);
              if(index > -1){
                item = reportResult.get(index);
                if(showClientsEnroled){
                  item.wonWithCourses += 1;
                }else{
                  item.won += 1;
                }
                item.stagnated -= 1;
              }
            }
          }

          Double resultDifference;
          totalLost = 0;
          for(ReportResult itemResult : reportResult){
            resultDifference = itemResult.total > 0 ? itemResult.totalDaysInStatus / itemResult.total : itemResult.totalDaysInStatus;
            itemResult.averageDays = resultDifference.round();
            totalLost += itemResult.lost;
          }

          //Integer totalWon = totalClients.size();
          Integer clientsWon;
          Integer totalWon;
          Integer totalWonWithCourses;
          if(!showClientsEnroled){
            totalWon = clientsToRemoveFromStagnated.size() + clientsWithCourses.size();  
            totalWonWithCourses = 0;
          }else{
            totalWon = clientsToRemoveFromStagnated.size();  
            totalWonWithCourses = clientsWithCourses.size();
          }
          clientsWon = totalWon + totalWonWithCourses;
      
          reportResult.get(reportResult.size() -1).won = totalWon;
          reportResult.get(reportResult.size() -1).wonWithCourses = totalWonWithCourses;
          reportResult.get(reportResult.size() -1).lost = totalLost; //-- CHECK IF THIS LOGIC SHOULD BE APPLIED (IS THE CORRECT ONE - SHOWING THE NUMBER OF NOT INTERESTED)

          reportResult.get(reportResult.size() -1).stagnated = reportResult.get(0).total - (clientsWon + totalLost);
          
          //reportResult.get(reportResult.size() -1).lost = reportResult.get(0).total - totalWon;  //-- CHECK IF THIS LOGIC SHOULD BE APPLIED (IS THE CORRECT ONE - SHOWING THE NUMBER OF NOT INTERESTED)
          
          if(!showClientsEnroled){
            conversionRate = ((clientsWon * 100.00) / (reportResult.get(0).total)).setScale(2);
          }else{
            conversionRate = ((totalWonWithCourses * 100.00) / (reportResult.get(0).total)).setScale(2);
          }

          /*if(totalLost == 0 && clientsWon > 0){
            conversionRate = 100;
          }else{
            if(!showClientsEnroled){
              conversionRate = ((clientsWon * 100.00) / (reportResult.get(0).total)).setScale(2);
            }else{
              conversionRate = ((totalWonWithCourses * 100.00) / (reportResult.get(0).total)).setScale(2);
            }
            
            //conversionRate = ((totalWon * 100.00) / (totalWon+totalLost)).setScale(2);
          }*/
          //reportResult.get(reportResult.size() - 1 ).lost = reportResult.get(0).total - totalClients.size();

          //system.debug('AVERAGE TIME PER CLIENT FOUND '+JSON.serialize(averageTimePerClient));
          
          //system.debug('FINAL REPORT RESULT '+JSON.serialize(reportResult));

          graph = JSON.serialize(reportResult);
        }
      }
      filters.put('beginDate',beginDate);
      filters.put('endDate',endDate);
      filters.put('groupSelected',groupSelected);
      filters.put('agencySelected',agencySelected);
      filters.put('destinationSelected',destinationSelected);
      filters.put('campaignSelected',JSON.serialize(campaignSelected));
      filters.put('typeOfSearch',typeOfSearch);

      user.Report_Convertion_Rate_Filters__c = JSON.serialize(filters);
      update user; 
    }catch(Exception e){
      system.debug('Error on generateReport() ===> ' + e.getLineNumber() + ' '+e.getMessage());  
    }
  }

  public Datetime checkedDate {get;set;}
  public String checkedBy {get;set;}

  public String generateQueryForExcel(){
    return 'SELECT Client__r.Current_Agency__r.Name, Client__r.Client_Enrolled_In_Course__c, Client__r.CreatedDate, Client__r.ID, Client__r.Name, Client__r.Owner__r.Name, Client__r.Lead_Status_Date_Time__c, Client__r.Last_Stage_Before_Not_Interested__c, Client__r.Lead_Converted_On__c ,Client__r.Status__c, Client__r.Email, Client__r.Lead_Stage__c, Client__r.RecordType.Name, Lead_Last_Change_Status_Cycle__c, Lead_Last_Stage_Cycle__c, RDStation_Campaigns__c, Lead_Last_Status_Cycle__c, Destination_Country__c, LastModifiedBy.Name FROM Destination_Tracking__c WHERE ID IN :dTrackings ';
  }
  public String generateQueryForReport(){
    String query = 'SELECT Stage_Item_Id__c, Stage_Item__c, Destination_Tracking__r.ID, Client__c, Client__r.RecordType.Name, Client__r.Status__c, Client__r.Client_Enrolled_In_Course__c ,Client__r.Last_Stage_Before_Not_Interested__c, Client__r.Lead_Converted_On__c, Last_Saved_Date_Time__c FROM Client_Stage_Follow_Up__c WHERE Agency_Group__c = :groupSelected '+ getQueryAnd(false) + ' AND Destination_Tracking__r.ID IN :dTrackings ORDER BY Client__c, Last_Saved_Date_Time__c';

    if(Test.isRunningTest()){
      query = 'SELECT Stage_Item_Id__c, Stage_Item__c, Destination_Tracking__r.ID, Client__c, Client__r.RecordType.Name, Client__r.Status__c, Client__r.Client_Enrolled_In_Course__c ,Client__r.Last_Stage_Before_Not_Interested__c, Client__r.Lead_Converted_On__c, Last_Saved_Date_Time__c FROM Client_Stage_Follow_Up__c';
    }
    return query;

  }

  public Map<String, String> getDestinationTrackingsForContacts(){
    String query = 'SELECT Client__c, Destination_Tracking__c FROM Client_Stage_Follow_Up__c WHERE Agency_Group__c = :groupSelected AND Destination_Tracking__r.Current_Cycle__c = true ';
    
    query = query + getQueryAnd(true);

    query = query + ' ORDER BY Destination_Tracking__r.CreatedDate, Client__c ';

    system.debug('query ' +query);
    system.debug('beginDate ' +beginDate);
    system.debug('endDate ' +endDate);
    system.debug('groupSelected ' +groupSelected);
    system.debug('idAgencies ' +idAgencies);
    system.debug('firstStatusStageZero ' +firstStatusStageZero);
    system.debug('agencySelected ' +agencySelected);
    system.debug('destinationSelected ' +destinationSelected);
    system.debug('employeeSelected ' +employeeSelected);
    system.debug('campaignSelected ' +campaignSelected);

    if(Test.isRunningTest()){
      query = 'SELECT Client__c, Destination_Tracking__c FROM Client_Stage_Follow_Up__c';
    }

    return new MethodsWithoutSharing().getDestinationTrackingsForContacts(query, beginDate, endDate, groupSelected, idAgencies, firstStatusStageZero, agencySelected, destinationSelected, employeeSelected, campaignSelected);
  }

  public without sharing class MethodsWithoutSharing{
    public Map<String, String> getDestinationTrackingsForContacts(String query, String beginDate, String endDate, String groupSelected, Set<String> idAgencies, String firstStatusStageZero, String agencySelected, String destinationSelected, String employeeSelected, List<String> campaignSelected){
      
      Map<String, String> destinationTrackings = new Map<String, String>();

      Date fromDate = String.isEmpty(beginDate) ? null : Date.parse(beginDate);
      Date toDate = String.isEmpty(endDate) ? null : Date.parse(endDate);

      Datetime moment = Datetime.now();
      
      List<Client_Stage_Follow_Up__c> contacts = Database.query(query);      

      for(Client_Stage_Follow_Up__c contact : contacts){
        destinationTrackings.put(contact.Client__c, contact.Destination_Tracking__c);
      }

      if(Test.isRunningTest()){
        destinationTrackings.put('12345', '12345');
      }

      return destinationTrackings;
    }
    public List<Client_Stage_Follow_Up__c> queryFollowUp(String query, String beginDate, String endDate, String groupSelected, Set<String> idAgencies, String firstStatusStageZero, String agencySelected, String destinationSelected, String employeeSelected, List<String> campaignSelected, List<String> dTrackings){
      Date fromDate = String.isEmpty(beginDate) ? null : Date.parse(beginDate);
      Date toDate = String.isEmpty(endDate) ? null : Date.parse(endDate);
      return Database.query(query);
    }
    public List<Destination_Tracking__c> queryExcel(String query, String beginDate, String endDate, String groupSelected, Set<String> idAgencies, String firstStatusStageZero, String agencySelected, String destinationSelected, String employeeSelected, List<String> campaignSelected, List<String> dTrackings){
      Date fromDate = String.isEmpty(beginDate) ? null : Date.parse(beginDate);
      Date toDate = String.isEmpty(endDate) ? null : Date.parse(endDate);
      return Database.query(query);
    }
  }

  public String getQueryAnd(Boolean considerClicksOrCampaigns){
    String query = ' AND Stage__c = \'Stage 0\' ';
	
	if(!String.isEmpty(destinationSelected)){ 
      query = query + ' AND Destination__c = :destinationSelected ';
    }
	if(!String.isEmpty(agencySelected)){
      if(agencySelected != 'all'){
        query = query + ' AND Client__r.Current_Agency__c = :agencySelected ';
      }else{
        query = query + ' AND Client__r.Current_Agency__c IN :idAgencies ';
      }
    }    
	if(!String.isEmpty(employeeSelected)){ 
      query = query + ' AND Client__r.Owner__r.Contact.ID = :employeeSelected ';
    }
	if(considerClicksOrCampaigns){
      if(campaignSelected != null && !campaignSelected.isEmpty()){
        query = query + ' AND Destination_Tracking__r.RDStation_Campaigns__c IN :campaignSelected ';
      }else{
        if(typeOfSearch == 'firstClick'){
          query = query + ' AND Stage_Item_Id__c = :firstStatusStageZero ';
        }else{
          query = query + ' AND Stage_Item__c != \'LEAD - Not Interested\' ';
        }
        if(!String.isEmpty(beginDate)){
          query = query + ' AND DAY_ONLY(convertTimezone(Last_Saved_Date_Time__c)) >= :fromDate '; 
        }
        if(!String.isEmpty(endDate)){
          query = query + ' AND DAY_ONLY(convertTimezone(Last_Saved_Date_Time__c)) <= :toDate ';
        }
      }
    }
    

    return query;
  }

  public Long getTimeInSeconds(Datetime moment){
    Long currentMoment = Datetime.now().getTime();
    return (currentMoment - moment.getTime()) / 1000;
  }

  public void updateAgencies(){
    String idGroup = ApexPages.currentPage().getParameters().get('idGroup');
    this.groupSelected = idGroup;
    this.agencies = loadAgencies(user.Contact.AccountId, idGroup);
    agencySelected = 'all';
    employeeSelected = '';
    if(campaignSelected == null){
      campaignSelected = new List<String>();
    }
    if(destinationSelected == null){
      destinationSelected = '';
    }
    this.employees = loadEmployees(null);    
    resetFirstClick();
  }

  public void updateEmployees(){
    employeeSelected = '';
    if(campaignSelected == null){
      campaignSelected = new List<String>();
    }
    if(destinationSelected == null){
      destinationSelected = '';
    }
    String idAgency = ApexPages.currentPage().getParameters().get('idAgency');
    this.employees = loadEmployees(idAgency);    
  }

  /*public void updateSelectedCampaign(){
    String idCampaign = ApexPages.currentPage().getParameters().get('idCampaign');
    this.campaignSelected = idCampaign;
    if(destinationSelected == null){
      destinationSelected = '';
    }
    if(employeeSelected == null){
      employeeSelected = '';
    }
  }*/

  public List<SelectOption> loadEmployees(String agencyID){
    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('','-All Employees-'));
    if(!String.isEmpty(agencyID) && agencyID != 'all'){
      for(SelectOption option : IPFunctions.getEmployeesByAgency(agencyID)){
        options.add(new SelectOption(option.getValue(), option.getLabel()));
      }
    }
    return options;
  }

  public List<SelectOption> loadCampaigns(){
    List<SelectOption> options = new List<SelectOption>();
    //options.add(new SelectOption('','-- No Filter By Campaign --'));
    //options.add(new SelectOption('filter-walk-in-hify','Only saved in Hify'));
    if(!String.isEmpty(user.Contact.Account.Parent.RDStation_Campaigns__c)){
      List<String> campaigns = new List<String>((Set<String>) JSON.deserialize(user.Contact.Account.Parent.RDStation_Campaigns__c, Set<String>.class));
      campaigns.sort();
      for(String campaign : campaigns){
        options.add(new SelectOption(campaign, campaign));
      }
    }
    return options;
  }

  public List<SelectOption> getGroups(){
    Set<String> idsAllGroups = new Set<String>();
    for(SelectOption option : IPFunctions.getAgencyGroupsByPermission(user.Contact.AccountId, Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())){
      idsAllGroups.add(option.getValue());
      //
    }
    List<SelectOption> options = new List<SelectOption>();
    for(Account option : [Select Id, Name from Account where id in :idsAllGroups AND Inactive__c = false order by name]){
      options.add(new SelectOption(option.ID, option.Name));
    }
    
    return options;
  }

  public List<SelectOption> getDestinations(){
    List<String> destinations = new List<String>();
    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('','-- All Destinations --'));
    List<Client_Stage__c> clientStages = [SELECT Destination__c, Stage__c FROM Client_Stage__c where Agency_Group__c = :groupSelected];
    for(Client_Stage__c ct : clientStages){
      if(!String.isEmpty(ct.Destination__c) && !destinations.contains(ct.Destination__c)){
        destinations.add(ct.Destination__c);
        //options.add(new SelectOption(ct.Destination__c, ct.Destination__c));
      }
    }
    destinations.sort();
    for(String destination : destinations){
      options.add(new SelectOption(destination, destination));
    }
    //List<SelectOption> options = IPFunctions.getAllCountries();
    return options;
  }
  public List<SelectOption> getReasons(){
    List<SelectOption> options = new List<SelectOption>();
    Schema.DescribeFieldResult fieldResult = Schema.Contact.Lead_Status_Reason__c.getDescribe();
    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
    options.add(new SelectOption('', ' - All Reasons - '));
    for( Schema.PicklistEntry f : ple){
      options.add(new SelectOption( f.getValue(), f.getLabel() ));
    }
    List<Client_Stage__c> leadNotInterested = [SELECT ID, Stage_Sub_Options__c FROM Client_Stage__c WHERE Stage_description__c = 'LEAD - Not Interested' AND Agency_Group__r.ID = :groupSelected];
    for(Client_Stage__c cs : leadNotInterested){
      if(!String.isEmpty(cs.Stage_Sub_Options__c)){
        List<String> subOptions = (List<String>) JSON.deserialize(cs.Stage_Sub_Options__c, List<String>.class);
        for(String option : subOptions){
          options.add(new SelectOption( option, option ));
        }
      }

    }
    return options;
  }

  public List<SelectOption> loadAgencies(String agencyID, String groupID){
    /*if(Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){
      options.add(new SelectOption('', ' - All Agencies Available - '));
    }*/
    Set<String> idsAllAgencies = new Set<String>();
    for(SelectOption option : IPFunctions.getAgencyOptions(agencyID, groupID, Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible())){
      idsAllAgencies.add(option.getValue());
      //
    }
    List<SelectOption> options = new List<SelectOption>();
    for(Account option : [Select Id, Name from Account where id in :idsAllAgencies AND Inactive__c = false order by name]){
      options.add(new SelectOption(option.ID, option.Name));
    }

    if(options.size() > 1){
      idAgencies = new Set<String>();
      for(SelectOption option : options){
        idAgencies.add(option.getValue());
      }
      options.add(0, new SelectOption('all', 'All Available Agencies'));
    }
    return options;
  }

  public class ContactsForExcel{
    public String contactID {get;set;}
    public String contactEmail {get;set;}
    public String contactName {get;set;}
    public String contactStatus {get;set;}
    public String rdStationCampaign {get;set;}
    public String enrolledCourse {get;set;}
    public String status {get;set;}
    public String stageStatus {get;set;}
    public String destination {get;set;}
    public Datetime checkedDate {get;set;}
    public Datetime createdDate {get;set;}
    public Datetime conversionDate {get;set;}
    public String checkedBy {get;set;}
    public String currentAgency {get;set;}
  }

  public class AverageTimePerContact implements Comparable{
    public String item{get;set;}
    public Datetime dateSaved{get;set;}
    public Integer itemOrder{get;set;}
    public Double daysInStatus;

    public AverageTimePerContact(){}  
    public AverageTimePerContact(String item, Datetime dateSaved, Integer itemOrder){
      this.item = item;
      this.dateSaved = dateSaved;
      this.itemOrder = itemOrder;
      this.daysInStatus = 0;
    }

    public Boolean equals(Object obj) {
      if (obj instanceof AverageTimePerContact) {
        AverageTimePerContact cs = (AverageTimePerContact)obj;
        if(item != null){
          return item.equals(cs.item);
        }
        return false;
      }
      return false;
    }
    public Integer hashCode() {
      return item.hashCode();
    }

    public Integer compareTo(Object compareTo) {
      AverageTimePerContact lst = (AverageTimePerContact)compareTo;
      if(this.itemOrder < lst.itemOrder){
        return -1;
      }else if(this.itemOrder > lst.itemOrder){
        return 1;
      }
      return 0;
    }  
  }

  public class ReportResult implements Comparable{
    public String statusID{get;set;}
    public String status{get;set;}
    public Integer total {get{if(total == null) total = 0; return total;} set;}
    public Integer current {get{if(current == null) current = 0; return current;} set;}
    public Integer lost {get{if(lost == null) lost = 0; return lost;} set;}
    public Integer stagnated {get{if(stagnated == null) stagnated = 0; return stagnated;} set;} 
    public Integer itemOrder {get{if(itemOrder == null) itemOrder = 0; return itemOrder;} set;} 
    public Integer won {get{if(won == null) won = 0; return won;} set;} 
    public Integer wonWithCourses {get{if(wonWithCourses == null) wonWithCourses = 0; return wonWithCourses;} set;} 

    public Double totalDaysInStatus {get{if(totalDaysInStatus == null) totalDaysInStatus = 0; return totalDaysInStatus;} set;}
    public Long averageDays {get{if(averageDays == null) averageDays = 0; return averageDays;} set;}

    public String colorWon {get{if(colorWon == null) colorWon = '#92d465'; return colorWon;} set;}
    public String colorWonCourses {get{if(colorWonCourses == null) colorWonCourses = '#4f7733'; return colorWonCourses;} set;}
    public String colorCurrent {get{if(colorCurrent == null) colorCurrent = '#67b7dc'; return colorCurrent;} set;}
    public String colorStagnated {get{if(colorStagnated == null) colorStagnated = '#fdd400'; return colorStagnated;} set;}
    public String colorLost {get{if(colorLost == null) colorLost = '#da3c3d'; return colorLost;} set;}

    public Set<String> contacts{get;set;}

    public ReportResult(){
      contacts = new Set<String>();
    }

    public ReportResult(String status){
      this.status = status;
    }

    public void eraseValues(){
      this.total = 0;
      this.current = 0;
      this.lost = 0;
      this.stagnated = 0;
      this.won = 0;
      this.wonWithCourses = 0;
      this.totalDaysInStatus = 0;
      this.averageDays = 0;
      contacts = new Set<String>();
    }

    /*public ReportResult(){
      this.total = 0;
      this.current = 0;
      this.lost = 0;
      this.stagnated = 0;
      this.won = 0;
    }

    public ReportResult(String status){
      this.status = status;
      this.total = 0;
      this.current = 0;
      this.lost = 0;
      this.stagnated = 0;
      this.won = 0;
    }

    public ReportResult(String status, Integer order){
      this.status = status;
      this.itemOrder = order;
      this.total = 0;
      this.current = 0;
      this.lost = 0;
      this.stagnated = 0;
    }*/

    public Boolean equals(Object obj) {
      if (obj instanceof ReportResult) {
        ReportResult cs = (ReportResult)obj;
        if(status != null){
          return status.equals(cs.status);
        }
        return false;
      }
      return false;
    }
    public Integer hashCode() {
      return status.hashCode();
    }

    public Integer compareTo(Object compareTo) {
      ReportResult lst = (ReportResult)compareTo;
      if(this.itemOrder < lst.itemOrder){
        return -1;
      }else if(this.itemOrder > lst.itemOrder){
        return 1;
      }
      return 0;
    }

  }
}