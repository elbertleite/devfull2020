global class Batch_Force_Database_Caching{
    global Batch_Force_Database_Caching(){}
}

/*global class Batch_Force_Database_Caching implements Database.Batchable<Integer>, Database.AllowsCallouts, Database.Stateful{
	
    public static Boolean SUCCESS = false;
    public static final String HIFY_EMAIL = 'develop@educationhify.com';
	public static final String EMAIL_FROM = 'Education Hify';

    Datetime startExecution;
    String groupID;
    String startDate; 
    String endDate;
    List<Integer> totalLoops;

	global Batch_Force_Database_Caching(List<Integer> totalLoops, String groupID, String startDate, String endDate) {
        this.totalLoops = totalLoops;
        this.groupID = groupID;
        this.startDate = startDate;
        this.endDate = endDate;
    }
	
	global Iterable<Integer> start(Database.BatchableContext BC) {
		return totalLoops;
	}

   	global void execute(Database.BatchableContext BC, List<Integer> loops) {
		//for(Integer lp : loops){
        //}
        if(!SUCCESS){
            startExecution = Datetime.now();
            Map<String, String> result = report_conversion_rate.generateReport(groupID, null, 'all', '-- All Destinations --', 'firstClick', null, endDate, startDate, false, null);   
            SUCCESS = true;
        }
    }
	
	global void finish(Database.BatchableContext BC) {
        Long duration = IPFunctions.getTimeInSeconds(startExecution);
        String mailSubject;
        String mailBody;
        if(SUCCESS){
            mailSubject = 'Service ForceDatabaseCaching finished successfully. Executed in '+IPFunctions.getCurrentEnvironment()+'.';
            mailBody = 'The scheduled service ForceDatabaseCaching finished successfully. Service executed on '+endDate+'. Time took in seconds: '+duration;
        }else{
            mailSubject = 'Service ForceDatabaseCaching failed. Executed in '+IPFunctions.getCurrentEnvironment()+'.';
            mailBody = 'The scheduled service ForceDatabaseCaching failed.  Service executed on '+endDate+'. Time took in seconds: '+duration;
        }
        SUCCESS = false;
        IPFunctions.sendEmailNoFuture(EMAIL_FROM, HIFY_EMAIL, EMAIL_FROM, HIFY_EMAIL, null, mailSubject, mailBody);
    }
}*/