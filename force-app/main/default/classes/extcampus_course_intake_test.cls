/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class extcampus_course_intake_test {

    static testMethod void myUnitTest() {
        
        TestFactory tf = new TestFactory();
        
        Account agency = tf.createAgency();
        Account school = tf.createSchool();
        Account campus = tf.createCampus(school, agency);
        Course__c course = tf.createCourse();
        Campus_Course__c cc = tf.createCampusCourse(campus, course);
        Course_Intake_Date__c cid = tf.createCourseIntakeDate(cc);
        
        /*Course_Intake_Date__c intake = [select Campus_Course__r.id, Campus_Course__r.Campus__r.id from Course_Intake_Date__c limit 1];
        
        Account acc = [Select id from Account where id = :intake.Campus_Course__r.Campus__c];*/

        Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(campus);
        //Apexpages.currentPage().getParameters().put('cId',cc.id);
        extcampus_course_intake cci = new extcampus_course_intake(controller);
        
        cci.campusCourseID = cc.id;
        cci.selectCourse();
        
      
        cci.selectCourse();
        cci.getCourseList();
        
        string SavedDatesStringList = cci.SavedDatesStringList;
        cci.save();
        
        list<Campus_Course__c> lcc = cci.lcc;
        
        // HERE.
        cci.SelectedDatesList = '';
        cci.save();
        cci.saveExtras();
        /*
        cci.course = '--None--';
        cci.selectCourse();*/
        cci.copyDatesToNextYear();
        List<SelectOption> getCourseListClone = cci.getCourseListClone();
        cci.selectCourseClone();
        cci.itemsToClone();
        cci.cloneDates();
        cci.saveClones();
        
        cci.cancel();
        
    }
}