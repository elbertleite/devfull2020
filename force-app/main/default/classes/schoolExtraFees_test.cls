@isTest
global without sharing class schoolExtraFees_test {
    static testMethod void myUnitTest() {
        TestFactory tf = new TestFactory();
        
        Account agency = tf.createAgency();
        Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
        
        Map<String,String> recordTypes = new Map<String,String>();
        for( RecordType r :[select id, name from recordtype where isActive = true] ){
            recordTypes.put(r.Name,r.Id);
        }

        Contact client = tf.createClient(agency);

        Account schoolGroup = tf.createSchoolGroup();
        
        Account school = tf.createSchool();
        school.parentId = schoolGroup.id;
        update school;

        Account campus = tf.createCampus(school, agency);

        Course__c course = tf.createCourse();
        Campus_Course__c cc = tf.createCampusCourse(campus, course);

        Course_Price__c price = tf.createCoursePrice(cc, 'Published Price');

        Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;

        Course_Extra_Fee__c cef = tf.createCourseExtraFee(cc, 'Published Price', p);
        cef.To__c = Decimal.valueOf(20);

        update cef;

        Start_Date_Range__c sdr = new Start_Date_Range__c();
		sdr.Course_Extra_Fee__c = cef.id;
		sdr.Campus__c = cc.Campus__c;
		sdr.From_Date__c = system.today();
		sdr.To_Date__c = system.today().addDays(30);
		sdr.Value__c = 300;
		insert sdr;

        Course_Extra_Fee_Dependent__c dep = tf.createCourseRelatedExtraFee(cef, p);

        dep.From__c = Decimal.valueOf(20);

        update dep;

        Start_Date_Range__c sdr2 = new Start_Date_Range__c();
		sdr2.Course_Extra_Fee_Dependent__c = dep.id;
		sdr2.Campus__c = cef.Campus_Parent__c;
		sdr2.From_Date__c = system.today();
		sdr2.To_Date__c = system.today().addDays(30);
		sdr2.Value__c = 125;
		insert sdr2;

        Start_Date_Range__c sd = tf.createProdDateRange(agency, new Quotation_List_Products__c());

        Quotation__c quote = tf.createQuotation(client, cc);

        new schoolExtraFees();

        schoolExtraFees.init(true, null, null, null, null);
        schoolExtraFees.retrieveFromCountry('Australia', true);
        schoolExtraFees.retrieveFromSchool(school.ID, 'Australia', true);
        schoolExtraFees.retrieveFromFeeCategory(school.ID, null);
        schoolExtraFees.retrieveCourseTypes(school.ID, null, true);
        schoolExtraFees.retrieveInfoToAddFee(school.ID, null);
        schoolExtraFees.search(true, 'Australia', school.ID, new List<String>{campus.ID}, new List<String>(), new List<String>(), new List<String>(), new List<String>(), new List<String>(), new List<String>(), null, null);

        Map<String, List<Start_Date_Range__c>> startRangesByDependentFees = new Map<String, List<Start_Date_Range__c>>();
        startRangesByDependentFees.put(dep.ID, new List<Start_Date_Range__c>{sdr2});

        schoolExtraFees.Fee fee = new schoolExtraFees.Fee(cef, new List<Start_Date_Range__c>{sdr}, new List<Course_Extra_Fee_Dependent__c>{dep}, startRangesByDependentFees);
        schoolExtraFees.RelatedFee relFee = new schoolExtraFees.RelatedFee(dep, cef.ID, new List<Start_Date_Range__c>{sdr2}, campus.ID);
        schoolExtraFees.StartDate std = new schoolExtraFees.StartDate(sdr, campus.ID, fee.ID, relFee.ID); 

        fee.nationalityGroups = new List<String>{'Published Price'};
        fee.campuses = new List<String>{campus.ID};  
        fee.courses = new List<String>{course.ID};  

        schoolExtraFees.saveNewExtraFee(school.ID, 'campus', fee);     
        schoolExtraFees.saveNewExtraFee(school.ID, 'course', fee);     

        schoolExtraFees.saveNewStartDate(new List<String>{cef.ID}, new List<String>{dep.ID}, std);
        schoolExtraFees.saveNewRelatedFee(new List<String>{cef.ID}, relFee);
        schoolExtraFees.cloneFees(school.ID, fee, new List<String>{cef.ID}, new List<String>(), new List<String>());

        schoolExtraFees.updateExtraFeePrices(new List<schoolExtraFees.Fee>{fee}, new List<schoolExtraFees.RelatedFee>{relFee}, new List<schoolExtraFees.StartDate>{std});

        //schoolExtraFees.updateExtraFees(new List<schoolExtraFees.Fee>{fee});
        //schoolExtraFees.updateStartDates(List<schoolExtraFees.StartDate>{sdr.ID, sdr2.ID});
        //schoolExtraFees.deleteExtraFees(new List<String>{cef.ID}, new List<String>{dep.ID}, new List<String>{sdr.ID, sdr2.ID});
    }
}