/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ReportUsersTasksController_test {

    static testMethod void myUnitTest() {
        
        TestFactory tf = new TestFactory();
        
		Account agency = tf.createAgency();				
		Contact employee = tf.createEmployee(agency);
		User portalUser = tf.createPortalUser(employee);
		
		Custom_Note_Task__c t = new Custom_Note_Task__c();
		t.Comments__c='bla bla bla';
		t.Subject__c = 'Follow Up';
		t.Due_Date__c = system.today().addDays(15);
		t.Assign_To__c =  portalUser.id;
		t.AssignedOn__c = system.today();
		t.AssignedBy__c = portalUser.id;
		t.Status__c = 'In Progress';
		t.Selected__c = true;
		insert t;
		
		t = new Custom_Note_Task__c();
		t.Comments__c='bla bla bla';
		t.Subject__c = 'Follow Up';
		t.Due_Date__c = system.today().addDays(-30);
		t.Assign_To__c =  portalUser.id;
		t.AssignedOn__c = system.today();
		t.AssignedBy__c = portalUser.id;
		t.Status__c = 'In Progress';
		t.Selected__c = true;
		insert t;
		
		t = new Custom_Note_Task__c();
		t.Comments__c='bla bla bla';
		t.Subject__c = 'Test';
		t.Due_Date__c = system.today().addDays(3);
		t.Assign_To__c =  portalUser.id;
		t.AssignedOn__c = system.today();
		t.AssignedBy__c = portalUser.id;
		t.Status__c = 'Completed';
		t.Selected__c = true;
		insert t;
        
        Test.startTest();
        
        system.runAs(portalUser){
        	
        	Contact client = tf.createClient(agency);
			Contact lead = tf.createLead(agency, employee);
			
        	
			ReportUsersTasksController report = new ReportUsersTasksController();
			List<SelectOption> agencyGroupOptions = report.agencyGroupOptions;
			Contact dates = report.fromdate;
			dates = report.toDate;
			
			report.fromDate.Expected_Travel_Date__c = system.today().addDays(-90);
			report.toDate.Expected_Travel_Date__c = system.today().addDays(90);
			
			
        	report.searchAgencies();
        	report.selectedAgencyGroup = agency.ParentID;
        	List<SelectOption> agencyOptions = report.agencyOptions;
        	report.selectedAgency = agency.id;
        	report.clearUser();
        	List<SelectOption> userOptions = report.userOptions;
        	report.getTaskType();
        	report.getTaskSubject();
        	report.getTaskStatus();
        	
        	report.search();
        	
        	ApexPages.currentPage().getParameters().put('userID', portalUser.id);
        	report.searchTasksByUser();
        	
        	
        	report.gettasksPerAgency();
        }
        
        Test.stopTest();
        
    }
}