public with sharing class students_coupons {

	public transient list<Contact> coupons {get;set;}

	//CONSTRUCTOR
	public students_coupons() {

	}


	public PageReference generateCouponsPDF(){

		if(selectedPeriod=='range' && (dateFilter.Original_Due_Date__c == null || dateFilter.Discounted_On__c == null)){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill date filters before you search.'));
			return null;
		}

		try{
			//G E T 	A L L 	P A Y M E N T S
			String DatefromDate;
			String DatetoDate;

			String sql = 'SELECT Client__c ct FROM client_course_instalment_payment__c WHERE Received_By_Agency__c = :selectedAgency ';

			if(selectedPeriod != 'range')
				sql += ' AND Date_Paid__c = ' + selectedPeriod + ' ';

			else{
				DatefromDate = IPFunctions.FormatSqlDateIni(dateFilter.Original_Due_Date__c);
				DatetoDate = IPFunctions.FormatSqlDateFin(dateFilter.Discounted_On__c);
				sql += ' AND Date_Paid__c >= ' + DatefromDate + ' AND Date_Paid__c <= ' + DatetoDate + ' ';
			}

			sql+= 'AND (client_course_instalment__c != null OR Invoice__c != null) AND Payment_type__c NOT IN (\'Covered by Agency\', \'Offshore\', \'School Credit\',  \'Client Scholarship\') AND client_course_instalment__r.isPCS__c = false AND client_course_instalment__r.isPDS__c = false group by Client__c';


			system.debug('sql@:' + sql);

			set<String> allContacts = new set<String>();

			for(AggregateResult ar : Database.query(sql))
			allContacts.add(string.valueOf(ar.get('ct')));


			//G E T 		C O N T A C T 		D E T A I L S
			coupons = [SELECT Name, Nationality__c, Email, owner__r.Contact.Name, owner__r.Contact.Department_Name__c, (Select detail__c from forms_of_contact__r where Type__c = 'Mobile' and Country__c = :currentUser.Contact.Account.BillingCountry limit 1) FROM Contact WHERE Id in :allContacts order by Department_Name__c, Name];

			PageReference pr = Page.students_coupons_pdf;
			pr.setRedirect(false);
			return pr;
		}
		catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			return null;
		}
	}





	/********************** Filters **********************/
	private IPFunctions.SearchNoSharing sns {get{if(sns == null) sns = new IPFunctions.SearchNoSharing(); return sns;}set;}
	public String agencyLogo {get;set;}
	private User currentUser {get{if(currentUser==null) currentUser = [Select Id, Name, Aditional_Agency_Managment__c, Contact.Account.ParentId, Contact.AccountId, Contact.Account.BillingCountry, Contact.Account.Logo__c from User where id = :UserInfo.getUserId() limit 1]; agencyLogo = currentUser.Contact.Account.Logo__c; return currentUser;}set;}

	public string selectedAgencyGroup{
  	get{
  		if(selectedAgencyGroup == null)
  			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
  		return selectedAgencyGroup;
  	}
  	set;
  }
	public string selectedAgency {get{
		if(selectedAgency==null)
			selectedAgency = currentUser.Contact.AccountId;
		return selectedAgency;
	}set;}


	public List<SelectOption> agencyGroupOptions {
 		get{
 			if(agencyGroupOptions == null){

 				agencyGroupOptions = new List<SelectOption>();
 				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group
 					for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:currentUser.Contact.AccountId order by Parent.Name]){
 						if(ag.Parent.Name != null && ag.ParentId != null)
 							agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
 					}
 				}else{
 					for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
 						agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
 					}
 				}
 			}
 			return agencyGroupOptions;
 	}
 	set;
 }


	public list<SelectOption> getAgencies(){
		List<SelectOption> result = new List<SelectOption>();
		if(selectedAgencyGroup != null){
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
				for(Account a: [Select Id, Name, Global_Link__c from Account where id = :currentUser.Contact.AccountId order by name]){
					if(selectedAgency=='')
						selectedAgency = a.Id;
					result.add(new SelectOption(a.Id, a.Name));
				}
				if(currentUser.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
						result.add(new SelectOption(ac.id, ac.name));
					}
				}
			}else{
				for(Account a: [Select Id, Name, Global_Link__c from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
					result.add(new SelectOption(a.Id, a.Name));
					if(selectedAgency=='')
						selectedAgency = a.Id;
				}
			}
		}
		return result;
	}

	public void changeGroup(){
		getAgencies();
		selectedAgency = '';
		changeAgency();
	}

	public void changeAgency(){
		getDepartments();
		selectedDepartment = 'all';
	}

	public string selectedDepartment {get{if(selectedDepartment==null) selectedDepartment = 'all'; return selectedDepartment;}set;}
	public list<SelectOption> getDepartments(){
		List<SelectOption> result = new List<SelectOption>();
		result.add(new SelectOption('all', '-- All --'));
		if(selectedAgency!=null)
			for(Department__c d: [Select Id, Name, Services__c, Show_Visits__c from Department__c where Agency__c = :selectedAgency and Inactive__c = false])
				result.add(new SelectOption(d.Id, d.Name));

		return result;
	}


	public string selectedPeriod{get {if(selectedPeriod == null) selectedPeriod = 'THIS_MONTH'; return selectedPeriod; } set;}
	private List<SelectOption> periods;
  public List<SelectOption> getPeriods() {
      if(periods == null){
          periods = new List<SelectOption>();
          periods.add(new SelectOption('THIS_MONTH','This Month'));
          periods.add(new SelectOption('range','Range'));
      }
      return periods;
  }



	public client_course_instalment__c dateFilter{
	get{
		if(dateFilter == null){
			dateFilter = new client_course_instalment__c();
			Date myDate = Date.today();
			Date weekStart = myDate.toStartofWeek();
			dateFilter.Original_Due_Date__c = weekStart;
			dateFilter.Discounted_On__c = dateFilter.Original_Due_Date__c.addDays(6);
		}
		return dateFilter;
	}
	set;
}
}