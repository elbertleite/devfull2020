@RestResource(urlMapping = '/v1/courses/search/*')
global class API_v1_courseSearch extends api_manager {

	static RestRequest request;

    @HttpGet
	global static List<Course> doGet(){

		request = RestContext.request;

		if(request != null){

			System.debug('/v1/courses/search request: ' + request);

			String schoolid = api_manager.getRequestParam(request, 'school');
			String campusIDs;
			String unitsSearch = api_manager.getRequestParam(request, 'units');
			String courseType = api_manager.getRequestParam(request, 'type');
			String datePay = api_manager.getRequestParam(request, 'datePay');
			String nationality = api_manager.getRequestParam(request, 'nationality');
			String country = api_manager.getRequestParam(request, 'country');
			String city = api_manager.getRequestParam(request, 'city');
			String category = api_manager.getRequestParam(request, 'category');
			String fields = api_manager.getRequestParam(request, 'fields');
			String timetable = api_manager.getRequestParam(request, 'timetable');
			String ranking = api_manager.getRequestParam(request, 'ranking');
			String language = api_manager.getRequestParam(request, 'language');
			String studentRanking = api_manager.getRequestParam(request, 'studentRanking');


			//DEBUG
			String HTMLbreak1 = ' ** ';
			String msg1 = '';

			msg1 += 'schoolid: ' + schoolid + HTMLbreak1;
			msg1 += 'unitsSearch: ' + unitsSearch + HTMLbreak1;
			msg1 += 'courseType: ' + courseType + HTMLbreak1;
			msg1 += 'datePay: ' + datePay + HTMLbreak1;
			msg1 += 'nationality: ' + nationality + HTMLbreak1;
			msg1 += 'country: ' + country + HTMLbreak1;
			msg1 += 'city: ' + city + HTMLbreak1;
			msg1 += 'category: ' + category + HTMLbreak1;
			msg1 += 'fields: ' + fields + HTMLbreak1;
			msg1 += 'timetable: ' + timetable + HTMLbreak1;
			msg1 += 'ranking: ' + ranking + HTMLbreak1;
			msg1 += 'language: ' + language + HTMLbreak1;
			system.debug(LoggingLevel.Info, '@@@ PARAMS: ' + msg1);
			//DEBUG




			//RestResponse response = restContext.response;
			//if(response != null)
				//response.addHeader('Access-Control-Allow-Origin','*');

			try {

				if(api_manager.isBadRequest(new List<String>{nationality, country, city, category, courseType, unitsSearch})){
					RestContext.response.statusCode = api_manager.CODE_BAD_REQUEST;
					return null;
				}

				API_v1_courseSearchHandler handler = new API_v1_courseSearchHandler();
				List<Course> courses = handler.search(schoolId, campusIDs, unitsSearch, courseType, datePay, nationality, country, city, category, fields, timetable, ranking, language, studentRanking);
				System.debug('***> courses.size(): '+courses.size());

				//Log the Call
				API_Manager.logCall(UserInfo.getUserID(), System.now().dategmt(), country, city, category);

				return courses;

			} catch (Exception e){


				String HTMLbreak = '<br/>';
				String msg = api_manager.getExceptionMessage(e);
				msg += HTMLbreak + HTMLbreak;

				msg += 'schoolid: ' + schoolid + HTMLbreak;
				msg += 'unitsSearch: ' + unitsSearch + HTMLbreak;
				msg += 'courseType: ' + courseType + HTMLbreak;
				msg += 'datePay: ' + datePay + HTMLbreak;
				msg += 'nationality: ' + nationality + HTMLbreak;
				msg += 'country: ' + country + HTMLbreak;
				msg += 'city: ' + city + HTMLbreak;
				msg += 'category: ' + category + HTMLbreak;
				msg += 'fields: ' + fields + HTMLbreak;
				msg += 'timetable: ' + timetable + HTMLbreak;
				msg += 'ranking: ' + ranking + HTMLbreak;
				msg += 'language: ' + language + HTMLbreak;


				api_manager.sendExceptionEmail(msg);

				RestContext.response.statusCode = api_manager.CODE_INTERNAL_SERVER_ERROR;

				return null;

			}


		} else return null;



	}


}