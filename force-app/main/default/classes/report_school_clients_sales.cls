public without sharing class report_school_clients_sales {
 
 	public report_school_clients_sales(){
		

		//Redirect from Report Year Comparison
		String schNm = ApexPages.currentPage().getParameters().get('sch');
		String display = ApexPages.currentPage().getParameters().get('dis');
		String year = ApexPages.currentPage().getParameters().get('year');

		//School Group and Destination
		if(schNm != null){
			Account sch  = [SELECT Id, ParentId, Parent.BillingCountry FROM Account WHERE Id = :schNm order by createdDate desc limit 1];
			selectedCountry = sch.Parent.BillingCountry;
			selectedSchoolGroup = sch.ParentId;
			selectedSchool = sch.Id;
		}

		selectedReportType = 'agencyschool';
		//Destination
		if(ApexPages.currentPage().getParameters().containsKey('bc')){
			selectedCountry = ApexPages.currentPage().getParameters().get('bc');
			selectedReportType = 'schoolagency';
		}

		//Agency Group
		if(ApexPages.currentPage().getParameters().containsKey('agp')){
			selectedGroup = ApexPages.currentPage().getParameters().get('agp');
		}

		//School Campus
		if(ApexPages.currentPage().getParameters().containsKey('cp')){
			selectedCampus = ApexPages.currentPage().getParameters().get('cp');
		}

		//Course Type
		if(ApexPages.currentPage().getParameters().containsKey('ct')){
			selectedCourseType = ApexPages.currentPage().getParameters().get('ct');
			
			if(selectedCourseType != 'all')
				try{
					if(schNm!= null)
						selectedCourseCategory = [SELECT course__r.Course_Category__c FROM campus_course__c WHERE campus__r.Parent.ParentId = :selectedSchoolGroup AND course__r.Course_Type__c =: selectedCourseType limit 1].course__r.Course_Category__c;
					else
						selectedCourseCategory = [SELECT course__r.Course_Category__c FROM campus_course__c WHERE campus__r.Parent.BillingCountry = :selectedCountry AND course__r.Course_Type__c =: selectedCourseType limit 1].course__r.Course_Category__c;
				}
				catch(exception e){
					selectedCourseCategory = 'all';
				}
		}

		//Date Range
		if(display != null){
			String key = ApexPages.currentPage().getParameters().get('key');
			SelectedPeriod = 'range';

			if(display=='year'){
				dateFilter.Original_Due_Date__c =  Date.newInstance(integer.valueOf(year), 1, 1); //FROM
				dateFilter.Discounted_On__c = Date.newInstance(integer.valueOf(year), 12, 31); //TO
			}
			else if(display=='semester'){
				if(key== '1'){
					dateFilter.Original_Due_Date__c = Date.newInstance(integer.valueOf(year), 1, 1); //FROM
					dateFilter.Discounted_On__c = Date.newInstance(integer.valueOf(year), 6, 30); //TO
				}
				else if(key== '2'){
					dateFilter.Original_Due_Date__c = Date.newInstance(integer.valueOf(year), 7, 1); //FROM
					dateFilter.Discounted_On__c = Date.newInstance(integer.valueOf(year), 12, 31); //TO
				}
			}
			else if(display=='quarter'){
				if(key== '1'){
					dateFilter.Original_Due_Date__c = Date.newInstance(integer.valueOf(year), 1, 1); //FROM
					dateFilter.Discounted_On__c = Date.newInstance(integer.valueOf(year), 3, 31); //TO
				}
				else if(key== '2'){
					dateFilter.Original_Due_Date__c = Date.newInstance(integer.valueOf(year), 4, 1); //FROM
					dateFilter.Discounted_On__c = Date.newInstance(integer.valueOf(year), 6, 30); //TO
				}
				else if(key== '3'){
					dateFilter.Original_Due_Date__c = Date.newInstance(integer.valueOf(year), 7, 1); //FROM
					dateFilter.Discounted_On__c = Date.newInstance(integer.valueOf(year), 9, 30); //TO
				}
				else if(key== '4'){
					dateFilter.Original_Due_Date__c = Date.newInstance(integer.valueOf(year), 10, 1); //FROM
					dateFilter.Discounted_On__c = Date.newInstance(integer.valueOf(year), 12, 31); //TO
				}
			}
		}
		
		//Search by
		if(ApexPages.currentPage().getParameters().containsKey('st')){
			selectSearchBy = ApexPages.currentPage().getParameters().get('st');
		}
		
		//End -- Redirect from Report Year Comparison
		
		if(display != null)
			sales();
 	}
 	
 	public transient list<AggregateResult> reportSales{get; set;}
 	
 	
 	private user userDetails = IPFunctions.getUserInformation(userInfo.getUserId());
 	
 	public void sales(){
 		periodReport = null;
 		string fromDate = IPFunctions.FormatSqlDateIni(dateFilter.Original_Due_Date__c); 
		string toDate = IPFunctions.FormatSqlDateFin(dateFilter.Discounted_On__c);
		
		//============== SQL SUM/COUNT============================
		string sqlTotals = '';
		sqlTotals += ' , COUNT_DISTINCT(client_course__r.client__c) clients, COUNT_DISTINCT(client_course__c) enrollments, count(id) ninstall, sum(Average_Weeks_Per_Instalment__c) weeks, sum(Tuition_Value__c) tuition, sum(Commission_Value__c) comm, sum(Commission_Adjustment__c) commAdj ';
 		if(freeweeks)
 			sqlTotals += ' , sum(client_course__r.Free_Weeks__c) freeweeks ';
 		if(paidschool)
 			sqlTotals += ' , sum(Net_Paid_To_School__c) totpaidschool ';
 		if(extrafee || install) 	
 			sqlTotals += ' , sum(Extra_Fee_Value__c) extrafee '; 
 		if(tax)
 			sqlTotals += ' , sum(Commission_Tax_Value__c) tax '; 
 		if(clientscholarship || finalcomm)
 			sqlTotals += ' , sum(Client_Scholarship__c) clientscholarship '; 
 		if(paidcredit)
 			sqlTotals += ' , sum(Total_Paid_School_Credit__c) paidcredit ';
 		if(scholarship || finalcomm)
 			sqlTotals += ' , sum(Scholarship_Taken__c) scholarship '; 
 		if(deal || finalcomm)
 			sqlTotals += ' , sum(Agent_Deal_Value__c) agDeal  ';
 		if(keepFee)
 			sqlTotals += ' , sum(Kepp_Fee__c) keepFee '; 
		
		//============== INNER SQL ============================
 		string innerSql = '';
 		if(selectedSchoolGroup != null && selectedSchoolGroup != 'all')
			innerSql += ' client_course__r.campus_course__r.campus__r.parent.ParentId = \''+selectedSchoolGroup+ '\' and ';
 		if(selectedSchool != null && selectedSchool != 'all')
			innerSql += ' client_course__r.campus_course__r.campus__r.ParentId = \''+selectedSchool+ '\' and ';
		else if(selectedCountry !=null && selectedCountry != ''){
			innerSql += ' client_course__r.Course_Country__c = \''+selectedCountry+ '\' and ';
		}
		if(selectedCampus != null && selectedCampus != 'all')
			innerSql += ' client_course__r.Campus_Id__c = \''+selectedCampus+ '\' and ';
			
			
		if(selectedGroup != null && selectedGroup != 'noAgency'){
			if(selectSearchBy == 'enrolment' || selectSearchBy == 'startdate'){
				innerSql += ' client_course__r.Enroled_by_Agency__r.ParentId = \''+selectedGroup+ '\' and ';
			}else if(selectSearchBy == 'payment')
				innerSql += ' Received_By_Agency__r.ParentId = \''+selectedGroup+ '\' and ';
		}else{
			if(selectSearchBy == 'enrolment' || selectSearchBy == 'startdate'){
				innerSql += ' client_course__r.Enroled_by_Agency__r.Parent.Global_Link__c = \''+userDetails.Contact.Account.Global_Link__c+ '\' and ';
			}else if(selectSearchBy == 'payment')
				innerSql += ' Received_By_Agency__r.Parent.Global_Link__c = \''+userDetails.Contact.Account.Global_Link__c+ '\' and ';
		}
		
		if(selectedAgency != null && selectedAgency != 'noAgency'){
			if(selectSearchBy == 'enrolment' || selectSearchBy == 'startdate')
				innerSql += ' client_course__r.Enroled_by_Agency__c = \''+selectedAgency+ '\' and ';
			else if(selectSearchBy == 'payment')
				innerSql += ' Received_By_Agency__c = \''+selectedAgency+ '\' and ';
		}
		
		if(selectedCourseCategory != null && selectedCourseCategory !='all')
			innerSql += ' client_course__r.campus_course__r.course__r.Course_Category__c = \''+selectedCourseCategory+ '\' and ';
		if(selectedCourseType != null && selectedCourseType !='all')
			innerSql += ' client_course__r.campus_course__r.course__r.Course_Type__c = \''+selectedCourseType+ '\' and ';
			
		if(selectSearchBy == 'enrolment'){
			if(SelectedPeriod == 'range')
				innerSql += ' client_course__r.Enrolment_Date__c >= '+ fromDate +' and client_course__r.Enrolment_Date__c <= '+toDate;
			else if(SelectedPeriod == 'THIS_WEEK')
				innerSql += ' client_course__r.Enrolment_Date__c = THIS_WEEK ';
			else if(SelectedPeriod == 'LAST_WEEK')
				innerSql += ' client_course__r.Enrolment_Date__c = LAST_WEEK ';
			else if(SelectedPeriod == 'NEXT_WEEK')
				innerSql += ' client_course__r.Enrolment_Date__c = NEXT_WEEK ';
			else if(SelectedPeriod == 'THIS_MONTH')
				innerSql += ' client_course__r.Enrolment_Date__c = THIS_MONTH ';
			else if(SelectedPeriod == 'LAST_MONTH')
				innerSql += ' client_course__r.Enrolment_Date__c = LAST_MONTH ';
			else if(SelectedPeriod == 'NEXT_MONTH')
				innerSql += ' client_course__r.Enrolment_Date__c = NEXT_MONTH ';
		}else if(selectSearchBy == 'startdate'){
			if(SelectedPeriod == 'range')
				innerSql += ' client_course__r.Start_Date__c >= '+ fromDate +' and client_course__r.Start_Date__c <= '+toDate;
			else if(SelectedPeriod == 'THIS_WEEK')
				innerSql += ' client_course__r.Start_Date__c = THIS_WEEK ';
			else if(SelectedPeriod == 'LAST_WEEK')
				innerSql += ' client_course__r.Start_Date__c = LAST_WEEK ';
			else if(SelectedPeriod == 'NEXT_WEEK')
				innerSql += ' client_course__r.Start_Date__c = NEXT_WEEK ';
			else if(SelectedPeriod == 'THIS_MONTH')
				innerSql += ' client_course__r.Start_Date__c = THIS_MONTH ';
			else if(SelectedPeriod == 'LAST_MONTH')
				innerSql += ' client_course__r.Start_Date__c = LAST_MONTH ';
			else if(SelectedPeriod == 'NEXT_MONTH')
				innerSql += ' client_course__r.Start_Date__c = NEXT_MONTH ';
		}else if(selectSearchBy == 'payment'){
			
			if(SelectedPeriod == 'range')
				innerSql += ' DAY_ONLY(convertTimezone(Received_Date__c)) >= '+ fromDate +' and DAY_ONLY(convertTimezone(Received_Date__c)) <= '+toDate;
			else if(SelectedPeriod == 'THIS_WEEK')
				innerSql += ' DAY_ONLY(convertTimezone(Received_Date__c)) = THIS_WEEK ';
			else if(SelectedPeriod == 'LAST_WEEK')
				innerSql += ' DAY_ONLY(convertTimezone(Received_Date__c)) = LAST_WEEK ';
			else if(SelectedPeriod == 'NEXT_WEEK')
				innerSql += ' DAY_ONLY(convertTimezone(Received_Date__c)) = NEXT_WEEK ';
			else if(SelectedPeriod == 'THIS_MONTH')
				innerSql += ' DAY_ONLY(convertTimezone(Received_Date__c)) = THIS_MONTH ';
			else if(SelectedPeriod == 'LAST_MONTH')
				innerSql += ' DAY_ONLY(convertTimezone(Received_Date__c)) = LAST_MONTH ';
			else if(SelectedPeriod == 'NEXT_MONTH')
				innerSql += ' DAY_ONLY(convertTimezone(Received_Date__c)) = NEXT_MONTH ';
		}
		
		innerSql += ' and client_course__r.Enrolment_Date__c != null and Received_Date__c != null and isCancelled__c = false  ';
		innerSql += ' and isPaymentConfirmed__c = true ';
		//innerSql += ' and ((isPDS__c = false and isPCS__c = false) or (isPDS__c = true and Commission_Confirmed_On__c != null) or (isPCS__c = true and Commission_Confirmed_On__c != null)) ';
		//===============================END INNER SQL====================================================================
		
 		if(selectedReportType == 'schoolagency'){
 			string sql = 'SELECT client_course__r.Campus_Name__c group1, client_course__r.Course_Name__c group2, ';
 			if(selectedGroupedBy == 'name')
 				sql += ' client_course__r.client__r.name group3 ';
 			else sql += ' client_course__r.client__r.Nationality__c group3 ';
 				/*if(selectSearchBy == 'enrolment' || selectSearchBy == 'startdate')
					sql += ' client_course__r.Enroled_by_Agency__r.name agency ';
				else if(selectSearchBy == 'payment')
 					sql += ' Received_By_Agency__r.name agency ';
 				*/
 				sql += sqlTotals;
 				
		 		sql += ' FROM client_course_instalment__c ';
				sql += ' where ';
				
				sql += innerSql;
					
				sql += ' group by ROLLUP(client_course__r.Campus_Name__c, client_course__r.Course_Name__c, ';
				if(selectedGroupedBy == 'name')
	 				sql += ' client_course__r.client__r.name) ';
	 			else sql += ' client_course__r.client__r.Nationality__c) ';

				/*if(selectSearchBy == 'enrolment' || selectSearchBy == 'startdate')
					sql += ' client_course__r.Enroled_by_Agency__r.name) ';
				else if(selectSearchBy == 'payment')
 					sql += ' Received_By_Agency__r.name) ';
 				*/	
				sql += ' order by client_course__r.Campus_Name__c nulls last, client_course__r.Course_Name__c nulls last, ';
				if(selectedGroupedBy == 'name')
					sql += ' client_course__r.client__r.name nulls last ';
				else sql += ' client_course__r.client__r.Nationality__c nulls last ';
				
				/*if(selectSearchBy == 'enrolment' || selectSearchBy == 'startdate')
					sql += ' client_course__r.Enroled_by_Agency__r.name  nulls last ';
				else if(selectSearchBy == 'payment')
 					sql += ' Received_By_Agency__r.name nulls last ';
				*/
				
				reportSales = new list<AggregateResult>();
				System.debug('==>sql:' +sql);
				reportSales = Database.query(sql);
 			
 		}
 		else if(selectedReportType == 'schoolgroup'){
 				string sql = ' SELECT ';
 					if(selectSearchBy == 'enrolment' || selectSearchBy == 'startdate')
						sql += ' client_course__r.Enroled_by_Agency__r.Parent.name group1, ';
					else if(selectSearchBy == 'payment')
	 					sql += ' Received_By_Agency__r.Parent.name group1, ';
					sql += ' client_course__r.Course_Name__c group2, ';
					if(selectedGroupedBy == 'name')
		 				sql += ' client_course__r.client__r.name group3 ';
		 			else sql += ' client_course__r.client__r.Nationality__c group3 ';
			 		sql += sqlTotals;
			 		sql += ' FROM client_course_instalment__c ';
					sql += ' where ';
					
					sql += innerSql;
					
					if(selectSearchBy == 'enrolment' || selectSearchBy == 'startdate')
						sql += ' group by ROLLUP(client_course__r.Enroled_by_Agency__r.Parent.name, ';
					else if(selectSearchBy == 'payment')
	 					sql += ' group by ROLLUP(Received_By_Agency__r.Parent.name, ';
					sql += ' client_course__r.Course_Name__c, ';
					if(selectedGroupedBy == 'name')
		 				sql += ' client_course__r.client__r.name) ';
		 			else sql += ' client_course__r.client__r.Nationality__c) ';
	 			
					sql += ' order by ';
					if(selectSearchBy == 'enrolment' || selectSearchBy == 'startdate')
						sql += ' client_course__r.Enroled_by_Agency__r.Parent.Name nulls last, ';
					else if(selectSearchBy == 'payment')
	 					sql += ' Received_By_Agency__r.Parent.name nulls last,';
					sql += ' client_course__r.Course_Name__c nulls last, ';
					if(selectedGroupedBy == 'name')
						sql += ' client_course__r.client__r.name nulls last ';
					else sql += ' client_course__r.client__r.Nationality__c nulls last ';
					
					reportSales = new list<AggregateResult>();
					System.debug('==>sql:' +sql);
					reportSales = Database.query(sql);
 			
 		}
 		else if(selectedReportType == 'agencyschool'){
 				string sql = ' SELECT ';
 					if(selectSearchBy == 'enrolment' || selectSearchBy == 'startdate')
						sql += ' client_course__r.Enroled_by_Agency__r.name group1, ';
					else if(selectSearchBy == 'payment')
	 					sql += ' Received_By_Agency__r.name group1, ';
					sql += ' client_course__r.Course_Name__c group2, ';
					if(selectedGroupedBy == 'name')
		 				sql += ' client_course__r.client__r.name group3 ';
		 			else sql += ' client_course__r.client__r.Nationality__c group3 ';
			 		sql += sqlTotals;
			 		sql += ' FROM client_course_instalment__c ';
					sql += ' where ';
					
					sql += innerSql;
					
					if(selectSearchBy == 'enrolment' || selectSearchBy == 'startdate')
						sql += ' group by ROLLUP(client_course__r.Enroled_by_Agency__r.name, ';
					else if(selectSearchBy == 'payment')
	 					sql += ' group by ROLLUP(Received_By_Agency__r.name, ';
					sql += ' client_course__r.Course_Name__c, ';
					if(selectedGroupedBy == 'name')
		 				sql += ' client_course__r.client__r.name) ';
		 			else sql += ' client_course__r.client__r.Nationality__c) ';
	 			
					sql += ' order by ';
					if(selectSearchBy == 'enrolment' || selectSearchBy == 'startdate')
						sql += ' client_course__r.Enroled_by_Agency__r.Name nulls last, ';
					else if(selectSearchBy == 'payment')
	 					sql += ' Received_By_Agency__r.name nulls last,';
					sql += ' client_course__r.Course_Name__c nulls last, ';
					if(selectedGroupedBy == 'name')
						sql += ' client_course__r.client__r.name nulls last ';
					else sql += ' client_course__r.client__r.Nationality__c nulls last ';
					
					reportSales = new list<AggregateResult>();
					System.debug('==>sql:' +sql);
					reportSales = Database.query(sql);
 			
 		}
 		else{
 			reportSales  = new list<AggregateResult>();
 		}
 	}
 	
 	//===============================FILTER OPTIONS=========================================
 	
 	public string selectedReportType{get {if(selectedReportType == null) selectedReportType = ''; return selectedReportType; } set;}
    private List<SelectOption> reportType;
    public List<SelectOption> getreportType() {
        if(reportType == null){
            reportType = new List<SelectOption>();
            reportType.add(new SelectOption('','--Select Report Type--'));
            reportType.add(new SelectOption('schoolagency','Campus / Course / Client'));
            if((selectedSchool != null && selectedSchool != 'all') || (selectedSchoolGroup != null && selectedSchoolGroup != 'all')){
	            reportType.add(new SelectOption('schoolgroup','Agency Group / Course / Client'));
	            reportType.add(new SelectOption('agencyschool','Agency / Course / Client'));
	            
            }
        }
        return reportType;
    }
    
    public string selectSearchBy{get {if(selectSearchBy == null) selectSearchBy = 'enrolment'; return selectSearchBy; } set;}
    private List<SelectOption> searchBy;
    public List<SelectOption> getsearchBy() {
        if(searchBy == null){
            searchBy = new List<SelectOption>();
            searchBy.add(new SelectOption('enrolment','--Enrolment Date--'));
            searchBy.add(new SelectOption('startdate','Course Start Date'));
            searchBy.add(new SelectOption('payment','Payment Date'));
        }
        return searchBy;
    }
    
    IPFunctions.CampusAvailability result = new IPFunctions.CampusAvailability();
    IPFunctions.agencyNoSharing agencyAndGroup = new IPFunctions.agencyNoSharing();
    
    public string selectedSchool{get {if(selectedSchool == null) selectedSchool = 'all'; return selectedSchool; } set;}
    private List<SelectOption> schools;
    public List<SelectOption> getSchools() {
        if(schools == null && selectedCountry != null){
		if(selectedSchoolGroup != null && selectedSchoolGroup != 'all')
		   schools = result.getSchoolByCountryGroup(selectedCountry, selectedSchoolGroup);
		else schools = result.getSchoolByCountry(selectedCountry);
        }
        return schools;
    }
    
    public string getSelectedSchoolName(){
    	for(SelectOption so:schools)
    		if(so.getValue() == selectedSchool){
    			return so.getLabel();
    			break;
    		}
    	return '';		
    }

    public string selectedSchoolGroup{get {if(selectedSchoolGroup == null) selectedSchoolGroup = 'all'; return selectedSchoolGroup; } set;}
    private List<SelectOption> schoolGroup;
    public List<SelectOption> getschoolGroup() {
        if(schoolGroup == null && selectedCountry != null){
		   schoolGroup = result.getSchoolGroupByCountry(selectedCountry);
        }
        return schoolGroup;
    }
    
   
    public string selectedCountry{get {if(selectedCountry == null) selectedCountry = ''; return selectedCountry; } set;}
    private List<SelectOption> countries;
    public List<SelectOption> getCountries() {
        if(countries == null){
		   countries = IPFunctions.CountryDestinations();
		   if(selectedCountry == null)
		   		selectedCountry = countries[0].getValue();
        }
        return countries;
    }
    
    public void refreshCampus(){
    	campuses = null;
    	reportType = null;
    	refreshCourseCategory();
    }
    
    public void refreshAgencies(){
    	agencies = null;
    }
    
    public void refreshCourseCategory(){
    	courseCategory = null;
    }
    
    public void refreshCourseType(){
    	courseType = null;
    }
    
    public void refreshSchoolGroup(){
		schoolGroup = null;
    	refreshSchool(); 
    }

    public void refreshSchool(){
    	schools = null;
    	refreshCampus();
    	refreshCourseType();
    }
    
    public string selectedCampus{get {if(selectedCampus == null) selectedCampus = 'all'; return selectedCampus; } set;}
    private List<SelectOption> campuses;
    public List<SelectOption> getCampuses() {
        if(campuses == null && selectedSchool != null && selectedSchool != 'all'){
		   campuses = result.getCampusBySchoolNA(selectedSchool);
        }
        return campuses;
    }
    
    public string selectedGroup{get {if(selectedGroup == null) selectedGroup = 'noAgency'; return selectedGroup; } set;}
    private List<SelectOption> agencyGroups;
    public List<SelectOption> getAgencyGroups() {
        if(agencyGroups == null){
		   agencyGroups = agencyAndGroup.getAllGroups();
        }
        return agencyGroups;
    }
 	
 	
 	public string selectedAgency{get {if(selectedAgency == null) selectedAgency = 'noAgency'; return selectedAgency; } set;}
    private List<SelectOption> agencies;
    public List<SelectOption> getAgencies() {
        if(agencies == null && selectedGroup != null && selectedGroup != 'noAgency'){
		   agencies = agencyAndGroup.getAgenciesByParentNoSharing(selectedGroup);
        }
        return agencies;
    }
 	   
 	public client_course_instalment__c dateFilter{
		get{
			if(dateFilter == null){
				dateFilter = new client_course_instalment__c(); 
				Date myDate = Date.today();
				Date weekStart = myDate.toStartofWeek();
				dateFilter.Original_Due_Date__c = weekStart;
				dateFilter.Discounted_On__c = dateFilter.Original_Due_Date__c.addDays(6);
			}
			return dateFilter;
		} 
		set;
	}
	
	public string SelectedPeriod{get {if(SelectedPeriod == null) SelectedPeriod = 'THIS_WEEK'; return SelectedPeriod; } set;}
    private List<SelectOption> periods;
    public List<SelectOption> getPeriods() {
        if(periods == null){
            periods = new List<SelectOption>();
            periods.add(new SelectOption('THIS_WEEK','This Week'));
            periods.add(new SelectOption('LAST_WEEK','Last Week'));
            periods.add(new SelectOption('THIS_MONTH','This Month'));
            periods.add(new SelectOption('LAST_MONTH','Last Month'));
            periods.add(new SelectOption('range','Range'));
        }
        return periods;
    }
    
    public string selectedGroupedBy{get {if(selectedGroupedBy == null) selectedGroupedBy = 'name'; return selectedGroupedBy; } set;}
    private List<SelectOption> groupedBy;
    public List<SelectOption> getgroupedBy() {
        if(groupedBy == null){
            groupedBy = new List<SelectOption>();
            groupedBy.add(new SelectOption('name','Name'));
            groupedBy.add(new SelectOption('nationality','Nationality'));
        }
        return groupedBy;
    }
    
    public string selectedCourseCategory{get {if(selectedCourseCategory == null) selectedCourseCategory = 'all'; return selectedCourseCategory; } set;}
    private List<SelectOption> courseCategory;
    public List<SelectOption> getcourseCategory() {
        if(courseCategory == null && selectedCountry != null){
           string sql = 'Select course__r.Course_Category__c cat from campus_course__c ';
           sql += ' where campus__r.billingCountry = \''+selectedCountry+'\' ';	
           if(selectedSchool != null && selectedSchool != 'all')
           		sql += ' and campus__r.parentId = \''+selectedSchool+ '\' and course__r.Course_Type__c != null ';
           	sql += ' group by course__r.Course_Category__c ';
		   courseCategory = new List<SelectOption>();
		   courseCategory.add(new selectOption('all','All'));
		   for(AggregateResult cc:Database.query(sql))
		   	 courseCategory.add(new selectOption((string)cc.get('cat'),(string)cc.get('cat')));
        }
        return courseCategory;
    }
    
    public string selectedCourseType{get {if(selectedCourseType == null) selectedCourseType = 'all'; return selectedCourseType; } set;}
    private List<SelectOption> courseType;
    public List<SelectOption> getcourseType() {
        if(courseType == null && selectedCourseCategory != null && selectedCourseCategory != 'all'){
           string sql = 'Select course__r.Course_Type__c typ from campus_course__c ';
           sql += ' where campus__r.billingCountry = \''+selectedCountry+'\' ';	
           if(selectedSchool != null && selectedSchool != 'all')
           		sql += ' and campus__r.parentId = \''+selectedSchool+ '\' ';
           sql += ' and course__r.Course_Category__c = \''+selectedCourseCategory+ '\' and course__r.Course_Type__c != null ';
           sql += ' group by course__r.Course_Type__c';
		   courseType = new List<SelectOption>();
		   courseType.add(new selectOption('all','All'));
		   for(AggregateResult cc:Database.query(sql))
		   	 courseType.add(new selectOption((string)cc.get('typ'),(string)cc.get('typ')));
        }
        return courseType;
    }
    
    
    
    public string periodReport{
    	get{
    		IF(periodReport==null){
    			if(SelectedPeriod == 'range')
					periodReport =  IPFunctions.FormatSqlDateIni(dateFilter.Original_Due_Date__c) +' to '+IPFunctions.FormatSqlDateFin(dateFilter.Discounted_On__c);
				else if(SelectedPeriod == 'THIS_WEEK')
					periodReport = 'This Week';
				else if(SelectedPeriod == 'LAST_WEEK')
					periodReport = 'Last Week';
				else if(SelectedPeriod == 'NEXT_WEEK')
					periodReport = 'Next Week';
				else if(SelectedPeriod == 'THIS_MONTH')
					periodReport = 'This Month';
				else if(SelectedPeriod == 'LAST_MONTH')
					periodReport = 'Last Month';
    		}
    		return periodReport;
    	} 
    	set;
    }
    
    public PageReference generatePDF(){
		periodReport = null;
		sales();
		PageReference pr = Page.report_school_clients_sales_pdf;
		pr.setRedirect(false);
		return pr;		
	}
	
	public PageReference generateExcel(){
		sales();
		PageReference pr = Page.report_school_clients_sales_excel;
		pr.setRedirect(false);
		return pr;		
	}
	
	public String date_time { 
	 	get{
	 		return System.now().format(); 
	 	}
	 	set; 
	 } 
	 
	public boolean freeweeks { get{if(freeweeks == null) freeweeks = false; return freeweeks;} set; } 
	public boolean install { get{if(install == null) install = false; return install;} set; } 
	public boolean extrafee { get{if(extrafee == null) extrafee = false; return extrafee;} set; } 
	public boolean tax { get{if(tax == null) tax = false; return tax;} set; } 
	public boolean avgcomm { get{if(avgcomm == null) avgcomm = false; return avgcomm;} set; } 
	public boolean deal { get{if(deal == null) deal = false; return deal;} set; } 
	public boolean keepfee { get{if(keepfee == null) keepfee = false; return keepfee;} set; } 
	public boolean paidcredit { get{if(paidcredit == null) paidcredit = false; return paidcredit;} set; } 
	public boolean scholarship { get{if(scholarship == null) scholarship = false; return scholarship;} set; } 
	public boolean clientscholarship { get{if(clientscholarship == null) clientscholarship = false; return clientscholarship;} set; } 
	public boolean ticket { get{if(ticket == null) ticket = false; return ticket;} set; } 
	public boolean paidschool { get{if(paidschool == null) paidschool = false; return paidschool;} set; } 
	public boolean finalcomm { get{if(finalcomm == null) finalcomm = false; return finalcomm;} set; } 
	public boolean commission { get{if(commission == null) commission = false; return commission;} set; } 
}