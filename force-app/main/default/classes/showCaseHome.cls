public with sharing class showCaseHome{

	public PageReference getRedir() {
		try {
			
			string accoId = [Select AccountId from User where  id = :UserInfo.getUserId() limit 1].AccountId;
			account userContact = [Select recordType.name, id, parentId from account where id = :accoId limit 1];
	 
			//Account acc = [Select id From Account Where parentId = :userContact.AccountId order by Name limit 1];//and Main_Campus__c = true limit 1];
	
			PageReference newPage;
		
			if(userContact.recordType.name == 'campus')
				newPage = new PageReference('/ShowCaseCampus?id=' + userContact.id);
			else if(userContact.recordType.name == 'agency')
				newPage = new PageReference('/ShowCaseGroupAgency?id=' + userContact.parentId);
				
			System.debug('==>newPage: '+newPage);
			
			return newPage.setRedirect(true);
		} catch (Exception e){
			
		}
		return null;

	}
}