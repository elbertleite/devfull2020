public class ClientWebEnquiries{

	public list<webFormInquiry__c> webEnquiry{get; set;}
	public ClientWebEnquiries(ApexPages.StandardController controller){
		webEnquiry = [Select Quote_URL__c, Form_URL__C, w.Account__r.name, Account__r.Email, w.City__c, w.Country__c, w.CreatedDate, w.Destination__c, w.How_did_you_hear_about_us__c, w.How_long_are_you_going_for__c, w.Message__c, w.Other_Destination__c, w.Other_Product_Type__c, w.State__c, w.Study_Area__c, w.What_are_you_looking_for__c, w.When_are_you_planning_to_travel__c from webFormInquiry__c w where Account__c = :controller.getRecord().id];
	}


}