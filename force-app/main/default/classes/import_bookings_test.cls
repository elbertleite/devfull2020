@isTest
private class import_bookings_test {

    static testMethod void myUnitTest() {
        TestFactory TestFactory = new TestFactory();
		
		Account agency = TestFactory.createAgency();
		
		Contact employee = TestFactory.createEmployee(agency);
		
		Contact client = TestFactory.createClient(agency);
		client.email = 'b.provatti@gmail.com';
        update client;
        
		Account school = testFactory.createSchool();
		Account campus = testFactory.createCampus(school, agency);
		Course__c course = testFactory.createCourse();
		Campus_Course__c cc = testFactory.createCampusCourse(campus, course);
		Quotation__c q = TestFactory.createQuotation(client, cc);

        string csfFile = 'Franquia,Estudante,Escola,Tipo Curso,Semanas Vend,Moeda,Tuition Moeda,Inicio Curs,Termino Curs,Pais,E-mail,Cambio,CM Tuition Moeda,Vendedor\n';
                csfFile += 'IP TATUAPE,ERISANDRA CELIA SANCHES VICTORIANO A,NSW Goverment Education School,Junior High School,24,AUD,4500,1/07/2019,31/01/2020,AUSTRALIA,erisandra.celia@gmail.com,2.82,0,ADRI TIEPPO\n';
                csfFile += 'IP TATUAPE,BEATRIZ GONZAGA PROVATTI,EC Brighton,General English - Intensive,4,GBP,1110,15/07/2019,9/08/2019,United Kingdom,b.provatti@gmail.com,4.8,355.2,CAMILA RODRIGUES\n';
                csfFile += 'IP TATUAPE,BEATRIZ GONZAGA PROVATTI,EC Brighton,General English - Intensive,4,GBP,1110,15/07/2019,9/08/2019,United Kingdom,b.provatti@gmail.com,4.8,355.2,CAMILA RODRIGUES\n';
                csfFile += 'IP TATUAPE,BEATRIZ GONZAGA PROVATTI,EC Brighton,General English - Intensive,4,GBP,1110,15/07/2019,9/08/2019,United Kingdom,b.provatti@gmail.com,4.8,355.2,CAMILA RODRIGUES\n';
                csfFile += 'IP TATUAPE,MARIA CATARINA CORREA GESTINARI,KAPLAN Bournemouth,General English - Part-time,4,GBP,700,1/07/2019,26/07/2019,UNITED KINGDOM,ninacorreag1@hotmail.com,5.03,210,CAMILA RODRIGUES\n';
                csfFile += 'IP TATUAPE,VALTER DE ASSIS MIROTA FILH,EC St. Julians,English in the City 30+ - Intensive - Morning,24,EUR,4128,8/07/2019,20/12/2019,Malta,valtermirota@gmail.com,4.29,1342.43,CAMILA RODRIGUES\n';
                csfFile += 'IP TATUAPE,RENATO CESAR DE LIMA SENA,ACA English Sydney,General English - Morning,14,AUD,2990,15/07/2019,18/10/2019,AUSTRALIA,contatorenatosena@gmail.com,2.82,1097.03,CARLOS EDUARDO\n';
                csfFile += 'IP TATUAPE,VICTOR CAMPOS NOVELLO,ACA English Sydney,General English - Morning,0,AUD,3450,1/07/2019,18/10/2019,AUSTRALIA,victor_rodriges10@hotmail.com,2.84,1035,CARLOS EDUARDO';

        import_bookings isch = new import_bookings();
        isch.csvFileBody = Blob.valueOf(csfFile);
        ApexPages.currentPage().getParameters().put('quoteId',q.id);
        ApexPages.currentPage().getParameters().put('courseId',course.id);
        isch.openQuoteEnrol();
        isch.importCSVFile();
    }
}