public with sharing class report_commissions {
	
	private User currentUser {get;set;}
	
	public report_commissions (){
		
		currentUser = [Select Id, Name, Aditional_Agency_Managment__c, Contact.Account.ParentId, Contact.AccountId, Contact.Department__c from User where id = :UserInfo.getUserId() limit 1];
		selectedAgency = new List<String>{currentUser.Contact.AccountId};
		
		searchCommissions();
	}
	
	public transient list<client_course_instalment__c> listCommissions{get; set;}
	public transient list<client_product_service__c> listCommissionsProducts{get; set;}
	
	public AggregateResult summaryValues{get; set;}
	public AggregateResult summaryProductValues{get; set;}
	
	public client_course_instalment__c dateFilter{
		get{
			if(dateFilter == null){
				dateFilter = new client_course_instalment__c(); 
				Date myDate = Date.today();
				Date weekStart = myDate.toStartofWeek();
				dateFilter.Original_Due_Date__c = weekStart;
				dateFilter.Discounted_On__c = dateFilter.Original_Due_Date__c.addDays(6); 
			}
			return dateFilter;
		} 
		set;
	}
	
	public void searchCommissions(){
		//DateTime  fromDate = DateTime.newInstance(dateFilter.Original_Due_Date__c.year(), dateFilter.Original_Due_Date__c.month(), dateFilter.Original_Due_Date__c.day(), 0, 0, 0); 
		//DateTime  toDate = DateTime.newInstance(dateFilter.Discounted_On__c.year(), dateFilter.Discounted_On__c.month(), dateFilter.Discounted_On__c.day(), 0, 0, 0);
		
		String  fromDate = IPFunctions.FormatSqlDateIni(dateFilter.Original_Due_Date__c); 
		String  toDate = IPFunctions.FormatSqlDateFin(dateFilter.Discounted_On__c);
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && currentUser.Aditional_Agency_Managment__c == null) //set the user to see only his own agency
			selectedAgency = new list<string>{currentUser.Contact.AccountId};
			
		string decision_status = 'Accepted';
		string sqlFilter = '';
		string sql = 'Select Agent_Deal_Name__c, Agent_Deal_Value__c, Balance__c, client_course__r.Agency_Admin_Fee__c, client_course__r.Campus_Name__c, client_course__r.Commission_Type__c, client_course__r.Client__r.name, client_course__r.Commission_Tax_Rate__c, client_course__r.Commission_Tax_Value__c, ';
				sql += ' client_course__r.Course_Name__c, Commission__c, Commission_Tax_Rate__c, Commission_Tax_Value__c, Commission_Value__c, Commission_Confirmed_On__c, Discount__c, Discounted_By__c, Discounted_On__c, Due_Date__c, Extra_Fee_Discount_Value__c,  isShare_Commission__c, client_course__r.client__r.Nationality__c, ';
				sql += ' Extra_Fee_Value__c, Instalment_Value__c, Kepp_Fee__c, Number__c, Paid_To_School_By__c, Paid_To_School_On__c, Scholarship_Taken__c, Splited_From_Number__c, Status__c, Total_School_Payment__c, Tuition_Value__c, Received_Date__c, Received_by__r.name, Received_By_Agency__r.name, ';
				sql += ' Commission_Confirmed_By_Agency__r.name, Commission_Paid_Date__c, Commission_Confirmed_By__r.name, Paid_to_School_By_Agency__r.name, Paid_To_School_By__r.name, isPDS__c, isPFS__c, isPCS__c, '; 
				sql += ' Confirmed_Date__c, Confirmed_By__r.name, Split_Number__c, client_course__r.Client__c, client_course__c ';
				sql += ' from client_course_instalment__c where ';

				if(SelectedClientSource == 'current_agency')
					sql += ' client_course__r.Client__r.Current_Agency__c in :selectedAgency ';
				else sql += ' (client_course__r.Enroled_by_Agency__c in :selectedAgency or (client_course__r.share_commission_request_agency__c in :selectedAgency and client_course__r.share_commission_decision_status__c = \'' +decision_status +'\')) ';

				sql += ' and client_course__r.Enrolment_Date__c !=null and isCancelled__c = false and ';
				
				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible() && SelectedClientSource == 'current_agency')
		 			sql += ' client_course__r.Client__r.Current_Agency__r.ParentId = \'' +selectedAgencyGroup +'\' AND ';
		 		
				if(dateCriteria == 'rc'){
					if(SelectedPeriod == 'range')
						sqlFilter += ' DAY_ONLY(convertTimezone(Received_Date__c)) >= '+ fromDate + ' and  DAY_ONLY(convertTimezone(Received_Date__c)) <= '+ toDate;
					else if(SelectedPeriod == 'THIS_WEEK')
						sqlFilter += ' Received_Date__c = THIS_WEEK ';
					else if(SelectedPeriod == 'LAST_WEEK')
						sqlFilter += ' Received_Date__c = LAST_WEEK ';
					else if(SelectedPeriod == 'NEXT_WEEK')
						sqlFilter += ' Received_Date__c = NEXT_WEEK ';
					else if(SelectedPeriod == 'THIS_MONTH')
						sqlFilter += ' Received_Date__c = THIS_MONTH ';
					else if(SelectedPeriod == 'LAST_MONTH')
						sqlFilter += ' Received_Date__c = LAST_MONTH ';
					else if(SelectedPeriod == 'NEXT_MONTH')
						sqlFilter += ' Received_Date__c = NEXT_MONTH ';
				}if(dateCriteria == 'rs'){
					if(SelectedPeriod == 'range')
						sqlFilter += ' Commission_Paid_Date__c >= '+ fromDate + ' and  Commission_Paid_Date__c <= '+ toDate;
					else if(SelectedPeriod == 'THIS_WEEK')
						sqlFilter += ' Commission_Paid_Date__c = THIS_WEEK ';
					else if(SelectedPeriod == 'LAST_WEEK')
						sqlFilter += ' Commission_Paid_Date__c = LAST_WEEK ';
					else if(SelectedPeriod == 'NEXT_WEEK')
						sqlFilter += ' Commission_Paid_Date__c = NEXT_WEEK ';
					else if(SelectedPeriod == 'THIS_MONTH')
						sqlFilter += ' Commission_Paid_Date__c = THIS_MONTH ';
					else if(SelectedPeriod == 'LAST_MONTH')
						sqlFilter += ' Commission_Paid_Date__c = LAST_MONTH ';
					else if(SelectedPeriod == 'NEXT_MONTH')
						sqlFilter += ' Commission_Paid_Date__c = NEXT_MONTH ';
					sqlFilter += ' and isNotNetCommission__c = true ';
				}if(dateCriteria == 'cs'){
					if(SelectedPeriod == 'range')
						sqlFilter += ' DAY_ONLY(convertTimezone(Received_Date__c)) >= '+ fromDate + ' and  DAY_ONLY(convertTimezone(Received_Date__c)) <= '+ toDate;
					else if(SelectedPeriod == 'THIS_WEEK')
						sqlFilter += ' Received_Date__c = THIS_WEEK ';
					else if(SelectedPeriod == 'LAST_WEEK')
						sqlFilter += ' Received_Date__c = LAST_WEEK ';
					else if(SelectedPeriod == 'NEXT_WEEK')
						sqlFilter += ' Received_Date__c = NEXT_WEEK ';
					else if(SelectedPeriod == 'THIS_MONTH')
						sqlFilter += ' Received_Date__c = THIS_MONTH ';
					else if(SelectedPeriod == 'LAST_MONTH')
						sqlFilter += ' Received_Date__c = LAST_MONTH ';
					else if(SelectedPeriod == 'NEXT_MONTH')
						sqlFilter += ' Received_Date__c = NEXT_MONTH ';
					sqlFilter += ' and Confirmed_Date__c != null ';
				}if(dateCriteria == 'cpnr'){
					if(SelectedPeriod == 'range')
						sqlFilter += ' DAY_ONLY(convertTimezone(Received_Date__c)) >= '+ fromDate + ' and  DAY_ONLY(convertTimezone(Received_Date__c)) <= '+ toDate;
					else if(SelectedPeriod == 'THIS_WEEK')
						sqlFilter += ' Received_Date__c = THIS_WEEK ';
					else if(SelectedPeriod == 'LAST_WEEK')
						sqlFilter += ' Received_Date__c = LAST_WEEK ';
					else if(SelectedPeriod == 'NEXT_WEEK')
						sqlFilter += ' Received_Date__c = NEXT_WEEK ';
					else if(SelectedPeriod == 'THIS_MONTH')
						sqlFilter += ' Received_Date__c = THIS_MONTH ';
					else if(SelectedPeriod == 'LAST_MONTH')
						sqlFilter += ' Received_Date__c = LAST_MONTH ';
					else if(SelectedPeriod == 'NEXT_MONTH')
						sqlFilter += ' Received_Date__c = NEXT_MONTH ';
					sqlFilter += ' and Confirmed_Date__c = null and Commission_Paid_Date__c = null ';
				}if(dateCriteria == 'rscs'){
					if(SelectedPeriod == 'range')
						sqlFilter += ' ((DAY_ONLY(convertTimezone(Confirmed_Date__c)) >= '+ fromDate + ' and DAY_ONLY(convertTimezone(Confirmed_Date__c)) <= '+toDate+') or (Commission_Paid_Date__c >= '+ fromDate + ' and Commission_Paid_Date__c <= '+toDate+'))';
					else if(SelectedPeriod == 'THIS_WEEK')
						sqlFilter += ' (Confirmed_Date__c = THIS_WEEK or Commission_Paid_Date__c = THIS_WEEK) ';
					else if(SelectedPeriod == 'LAST_WEEK')
						sqlFilter += ' (Confirmed_Date__c = LAST_WEEK or Commission_Paid_Date__c = LAST_WEEK)';
					else if(SelectedPeriod == 'NEXT_WEEK')
						sqlFilter += ' (Confirmed_Date__c = NEXT_WEEK or Commission_Paid_Date__c = NEXT_WEEK)';
					else if(SelectedPeriod == 'THIS_MONTH')
						sqlFilter += ' (Confirmed_Date__c = THIS_MONTH or Commission_Paid_Date__c = THIS_MONTH)';
					else if(SelectedPeriod == 'LAST_MONTH')
						sqlFilter += ' (Confirmed_Date__c = LAST_MONTH or Commission_Paid_Date__c = LAST_MONTH)';
					else if(SelectedPeriod == 'NEXT_MONTH')
						sqlFilter += ' (Confirmed_Date__c = NEXT_MONTH or Commission_Paid_Date__c = NEXT_MONTH)';
				}else if(dateCriteria == 'ps'){
					if(SelectedPeriod == 'range')
						sqlFilter += ' DAY_ONLY(convertTimezone(Paid_To_School_On__c)) >= '+ fromDate + ' and  DAY_ONLY(convertTimezone(Paid_To_School_On__c)) <= '+ toDate;
					else if(SelectedPeriod == 'THIS_WEEK')
						sqlFilter += ' Paid_To_School_On__c = THIS_WEEK ';
					else if(SelectedPeriod == 'LAST_WEEK')
						sqlFilter += ' Paid_To_School_On__c = LAST_WEEK ';
					else if(SelectedPeriod == 'NEXT_WEEK')
						sqlFilter += ' Paid_To_School_On__c = NEXT_WEEK ';
					else if(SelectedPeriod == 'THIS_MONTH')
						sqlFilter += ' Paid_To_School_On__c = THIS_MONTH ';
					else if(SelectedPeriod == 'LAST_MONTH')
						sqlFilter += ' Paid_To_School_On__c = LAST_MONTH ';
					else if(SelectedPeriod == 'NEXT_MONTH')
						sqlFilter += ' Paid_To_School_On__c = NEXT_MONTH ';
				}if(dateCriteria == 'rcns'){
					if(SelectedPeriod == 'range')
						sqlFilter += ' DAY_ONLY(convertTimezone(Received_Date__c)) >= '+ fromDate + ' and  DAY_ONLY(convertTimezone(Received_Date__c)) <= '+ toDate;
					else if(SelectedPeriod == 'THIS_WEEK')
						sqlFilter += ' Received_Date__c = THIS_WEEK ';
					else if(SelectedPeriod == 'LAST_WEEK')
						sqlFilter += ' Received_Date__c = LAST_WEEK ';
					else if(SelectedPeriod == 'NEXT_WEEK')
						sqlFilter += ' Received_Date__c = NEXT_WEEK ';
					else if(SelectedPeriod == 'THIS_MONTH')
						sqlFilter += ' Received_Date__c = THIS_MONTH ';
					else if(SelectedPeriod == 'LAST_MONTH')
						sqlFilter += ' Received_Date__c = LAST_MONTH ';
					else if(SelectedPeriod == 'NEXT_MONTH')
						sqlFilter += ' Received_Date__c = NEXT_MONTH ';
					sqlFilter += ' and Paid_To_School_On__c = null ';
				}else if(dateCriteria == 'np'){
					if(SelectedPeriod == 'range')
						sqlFilter += ' Due_Date__c >= '+ fromDate + ' and  Due_Date__c <= '+ toDate;
					else if(SelectedPeriod == 'THIS_WEEK')
						sqlFilter += ' Due_Date__c = THIS_WEEK ';
					else if(SelectedPeriod == 'LAST_WEEK')
						sqlFilter += ' Due_Date__c = LAST_WEEK ';
					else if(SelectedPeriod == 'NEXT_WEEK')
						sqlFilter += ' Due_Date__c = NEXT_WEEK ';
					else if(SelectedPeriod == 'THIS_MONTH')
						sqlFilter += ' Due_Date__c = THIS_MONTH ';
					else if(SelectedPeriod == 'LAST_MONTH')
						sqlFilter += ' Due_Date__c = LAST_MONTH ';
					else if(SelectedPeriod == 'NEXT_MONTH')
						sqlFilter += ' Due_Date__c = NEXT_MONTH ';
					sqlFilter += ' and Received_Date__c = null ';
				}else if(dateCriteria == 'dd'){
					if(SelectedPeriod == 'range')
						sqlFilter += ' Due_Date__c >= '+ fromDate + ' and  Due_Date__c <= '+ toDate;
					else if(SelectedPeriod == 'THIS_WEEK')
						sqlFilter += ' Due_Date__c = THIS_WEEK ';
					else if(SelectedPeriod == 'LAST_WEEK')
						sqlFilter += ' Due_Date__c = LAST_WEEK ';
					else if(SelectedPeriod == 'NEXT_WEEK')
						sqlFilter += ' Due_Date__c = NEXT_WEEK ';
					else if(SelectedPeriod == 'THIS_MONTH')
						sqlFilter += ' Due_Date__c = THIS_MONTH ';
					else if(SelectedPeriod == 'LAST_MONTH')
						sqlFilter += ' Due_Date__c = LAST_MONTH ';
					else if(SelectedPeriod == 'NEXT_MONTH')
						sqlFilter += ' Due_Date__c = NEXT_MONTH ';
				}
				
				sqlFilter += ' and isMigrated__c = false ';
				
				
				sql += sqlFilter;
				
				sql += ' order by Received_Date__c';
				system.debug('==> sql: '+sql);
		listCommissions = Database.query(sql);
		retrieveSummary(fromDate, toDate, sqlFilter);
		if(dateCriteria != 'ps' && dateCriteria != 'rs' && dateCriteria != 'rscs' && dateCriteria != 'rs' && dateCriteria != 'rcns' && dateCriteria != 'dd')
			retrieveProducts(fromDate, toDate);
		else {
			listCommissionsProducts = new list<client_product_service__c>();
		}
	}
	
	public void retrieveSummary(string fromDate, string toDate, string sqlFilter){
		string decision_status = 'Accepted';
		string sql = 'Select count(id) totItens, SUM(Instalment_Value__c) totInstall, SUM(Total_School_Payment__c) totPaySchool, SUM(Commission_Value__c) totCommission,  ';
		sql += ' SUM(Balance__c) totBalance, SUM(Tuition_Value__c) totTuition, SUM(Extra_Fee_Value__c) totFee, SUM(Commission_Tax_Value__c) totTax, Sum(Agent_Deal_Value__c) AgencyDeal,';
		sql += ' SUM(Discount__c) totDiscount, SUM(Kepp_Fee__c) totKeepFee, SUM(Scholarship_Taken__c) totScholarShip from client_course_instalment__c where ';
		
		if(SelectedClientSource == 'current_agency')
			sql += ' client_course__r.Client__r.Current_Agency__c in :selectedAgency ';
		else sql += ' (client_course__r.Enroled_by_Agency__c in :selectedAgency or (client_course__r.share_commission_request_agency__c in :selectedAgency and client_course__r.share_commission_decision_status__c = \'' +decision_status +'\')) ';

		sql += ' and client_course__r.Enrolment_Date__c !=null and isCancelled__c = false and ';
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible() && SelectedClientSource == 'current_agency')
			sql += ' client_course__r.Client__r.Current_Agency__r.ParentId = \'' +selectedAgencyGroup +'\' AND ';
		 		
		
		sql += sqlFilter;
			
		summaryValues = Database.query(sql);
	}
	
	
	public void retrieveProducts(string fromDate, string toDate){
		string sqlFilter = '';
		string sql = 'Select Category__c, Client__r.Name, Confirmed_By__c, Confirmed_Date__c, Invoice__c, Price_Total__c, Price_Unit__c, Product_Name__c, Quantity__c, Received_by__c, Received_Date__c, Unit_Description__c, Commission_Tax_Value__c, Commission_Type__c, Commission_Value__c, Commission__c  ';
		sql += ' from client_product_service__c where Booking_Number__r.Client__r.Current_Agency__c in :selectedAgency and ';
		
		if(dateCriteria == 'rc'){
			if(SelectedPeriod == 'range')
				sqlFilter += ' DAY_ONLY(convertTimezone(Received_Date__c)) >= '+ fromDate + ' and  DAY_ONLY(convertTimezone(Received_Date__c)) <= '+ toDate;
			else if(SelectedPeriod == 'THIS_WEEK')
				sqlFilter += ' Received_Date__c = THIS_WEEK ';
			else if(SelectedPeriod == 'LAST_WEEK')
				sqlFilter += ' Received_Date__c = LAST_WEEK ';
			else if(SelectedPeriod == 'NEXT_WEEK')
				sqlFilter += ' Received_Date__c = NEXT_WEEK ';
			else if(SelectedPeriod == 'THIS_MONTH')
				sqlFilter += ' Received_Date__c = THIS_MONTH ';
			else if(SelectedPeriod == 'LAST_MONTH')
				sqlFilter += ' Received_Date__c = LAST_MONTH ';
			else if(SelectedPeriod == 'NEXT_MONTH')
				sqlFilter += ' Received_Date__c = NEXT_MONTH ';
		}if(dateCriteria == 'cs'){
			if(SelectedPeriod == 'range')
				sqlFilter += ' DAY_ONLY(convertTimezone(Received_Date__c)) >= '+ fromDate + ' and  DAY_ONLY(convertTimezone(Received_Date__c)) <= '+ toDate;
			else if(SelectedPeriod == 'THIS_WEEK')
				sqlFilter += ' Received_Date__c = THIS_WEEK ';
			else if(SelectedPeriod == 'LAST_WEEK')
				sqlFilter += ' Received_Date__c = LAST_WEEK ';
			else if(SelectedPeriod == 'NEXT_WEEK')
				sqlFilter += ' Received_Date__c = NEXT_WEEK ';
			else if(SelectedPeriod == 'THIS_MONTH')
				sqlFilter += ' Received_Date__c = THIS_MONTH ';
			else if(SelectedPeriod == 'LAST_MONTH')
				sqlFilter += ' Received_Date__c = LAST_MONTH ';
			else if(SelectedPeriod == 'NEXT_MONTH')
				sqlFilter += ' Received_Date__c = NEXT_MONTH ';
			sqlFilter += ' and Confirmed_Date__c != null ';
		}if(dateCriteria == 'cpnr'){
			if(SelectedPeriod == 'range')
				sqlFilter += ' DAY_ONLY(convertTimezone(Received_Date__c)) >= '+ fromDate + ' and  DAY_ONLY(convertTimezone(Received_Date__c)) <= '+ toDate;
			else if(SelectedPeriod == 'THIS_WEEK')
				sqlFilter += ' Received_Date__c = THIS_WEEK ';
			else if(SelectedPeriod == 'LAST_WEEK')
				sqlFilter += ' Received_Date__c = LAST_WEEK ';
			else if(SelectedPeriod == 'NEXT_WEEK')
				sqlFilter += ' Received_Date__c = NEXT_WEEK ';
			else if(SelectedPeriod == 'THIS_MONTH')
				sqlFilter += ' Received_Date__c = THIS_MONTH ';
			else if(SelectedPeriod == 'LAST_MONTH')
				sqlFilter += ' Received_Date__c = LAST_MONTH ';
			else if(SelectedPeriod == 'NEXT_MONTH')
				sqlFilter += ' Received_Date__c = NEXT_MONTH ';
			sqlFilter += ' and Confirmed_Date__c = null ';
		}else if(dateCriteria == 'np'){
			sqlFilter += ' Received_Date__c = null  ';
			
		}
		
		sql += sqlFilter;
		system.debug('selectedAgency===>'+selectedAgency);
		system.debug('prodSQL===>'+sql);
		
		listCommissionsProducts = Database.query(sql);
		
		retrieveProductSummary(fromDate, toDate, sqlFilter);
	}
	
	public void retrieveProductSummary(string fromDate, string toDate, string sqlFilter){
		string sql = 'Select count(id) totItens, SUM(Price_Total__c) totPrice, SUM(Commission_Value__c) totCommission, SUM(Price_Total__c) priceTotal ';
		sql += '  from client_product_service__c where Booking_Number__r.Client__r.Current_Agency__c in :selectedAgency and ';
		
		sql += sqlFilter;
			
		summaryProductValues = Database.query(sql);
	}
	
	public string SelectedPeriod{get {if(SelectedPeriod == null) SelectedPeriod = 'THIS_WEEK'; return SelectedPeriod; } set;}
    private List<SelectOption> periods;
    public List<SelectOption> getPeriods() {
        if(periods == null){
            periods = new List<SelectOption>();
            periods.add(new SelectOption('THIS_WEEK','This Week'));
            periods.add(new SelectOption('LAST_WEEK','Last Week'));
            periods.add(new SelectOption('NEXT_WEEK','Next Week'));
            periods.add(new SelectOption('THIS_MONTH','This Month'));
            periods.add(new SelectOption('LAST_MONTH','Last Month'));
            periods.add(new SelectOption('NEXT_MONTH','Next Month'));
            periods.add(new SelectOption('range','Range'));
        }
        return periods;
	}
	
	public string SelectedClientSource{get {if(SelectedClientSource == null) SelectedClientSource = 'current_agency'; return SelectedClientSource; } set;}
    public List<SelectOption> getClientSource() {
		List<SelectOption> clientSource;
        if(clientSource == null){
            clientSource = new List<SelectOption>();
            clientSource.add(new SelectOption('current_agency','Current Agency'));
            clientSource.add(new SelectOption('enroll_agency','Enroll Agency'));
        }
        return clientSource;
    }
    
    public string dateCriteria{get {if(dateCriteria == null) dateCriteria = 'rc'; return dateCriteria; } set;}
    private List<SelectOption> dateCriteriaOptions;
    public List<SelectOption> getdateCriteriaOptions() {
        if(dateCriteriaOptions == null){
            dateCriteriaOptions = new List<SelectOption>();
            dateCriteriaOptions.add(new SelectOption('rc','Client Payments Received'));
            dateCriteriaOptions.add(new SelectOption('rs','Received Commission from School (PDS/PFS/PCS)'));
            dateCriteriaOptions.add(new SelectOption('cs','Client Payments - Reconciled'));
            dateCriteriaOptions.add(new SelectOption('cpnr','Client Payments – Not Reconciled'));
            dateCriteriaOptions.add(new SelectOption('rscs','Client Payments – Reconciled/Commission Confirmed'));
            dateCriteriaOptions.add(new SelectOption('rcns','Paid by Client/Not Paid to School'));
            dateCriteriaOptions.add(new SelectOption('ps','Paid to School'));
            dateCriteriaOptions.add(new SelectOption('np','Client Payments Due/Not Paid'));
            dateCriteriaOptions.add(new SelectOption('dd','Client Payments Forecast (Paid/Not Paid)'));
        }
        return dateCriteriaOptions;
    }
    
    private string ContactId = [Select ContactId from user where id = :UserInfo.getUserId() limit 1].ContactId;
    private contact userDetails = [Select AccountId, Account.ParentId from Contact where id = :ContactId limit 1];
    public string selectedAgencyGroup{
    	get{
    		if(selectedAgencyGroup == null)
    			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
    		return selectedAgencyGroup;
    	}  
    	set;
    }
    
 	 public List<SelectOption> agencyGroupOptions {
  		get{
   			if(agencyGroupOptions == null){
   				
   				agencyGroupOptions = new List<SelectOption>();  
   				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group  
	    			for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:currentUser.Contact.AccountId order by Parent.Name]){
		     			agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
		    		}
   				}else{
   					for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
		     			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
		    		}
   				}
	   		}
	   		return agencyGroupOptions;
	  }
	  set;
 	}
	
	public List<String> selectedAgency {get;set;}
	public List<SelectOption> agencyOptions {
		get{
	   		if(selectedAgencyGroup != null){
	   			agencyOptions = new List<SelectOption>();
				if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
					for(Account a: [Select Id, Name from Account where id =:currentUser.Contact.AccountId order by name]){
						if(selectedAgency == null){
							selectedAgency = new List<String>{a.Id};
						}
						agencyOptions.add(new SelectOption(a.Id, a.Name));
					}
					if(currentUser.Aditional_Agency_Managment__c != null){
						for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
							agencyOptions.add(new SelectOption(ac.id, ac.name));
						}
					}
				}else{
					for(Account a: [Select Id, Name from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
						agencyOptions.add(new SelectOption(a.Id, a.Name));
						if(selectedAgency == null)
							selectedAgency = new List<String>{a.Id};	
					}
				}
			}
		   	return agencyOptions;
	  	}
	  	set;
	}

	public PageReference generateExcel(){
		PageReference pr = Page.report_commissions_excel;
		pr.setRedirect(false);
		return pr;		
	}
	
	 public void searchCommissionsExcel(){
    	searchCommissions();
    }
    
    public String xlsHeader {
        get {
            String strHeader = '';
            strHeader += '<?xml version="1.0"?>';
            strHeader += '<?mso-application progid="Excel.Sheet"?>';
            return strHeader;
        }
    }
	    
}