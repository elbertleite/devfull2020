public with sharing class contact_visits {
	
	public String ctid {get;set;}
	public Integer limitVisits {get;set;}
	
	public List<Visit__c> clientVisits {
		get{
			if(clientVisits == null){
				//ApexPages.currentPage().getUrl().substringAfter('/apex/').contains('contact_new_visits')
				if(String.isEmpty(ctid) && !String.isEmpty(ApexPages.currentPage().getParameters().get('id'))){
					ctid = ApexPages.currentPage().getParameters().get('id');
				}
				if(limitVisits != null && limitVisits > 0)
					clientVisits = [Select createdDate, Service__c, Department__r.Name, Status__c, Closed_By__r.Name, Close_Date__c, Comments__c, Staff__c from Visit__c where Contact__c = :ctid and status__c in ('Completed', 'Cancelled') ORDER BY CreatedDate DESC LIMIT :limitVisits];
				else
					clientVisits = [Select createdDate, Service__c, Department__r.Name, Status__c, Closed_By__r.Name, Close_Date__c, Comments__c, Staff__c from Visit__c where Contact__c = :ctid and status__c in ('Completed', 'Cancelled') ORDER BY CreatedDate DESC]; 
			}
			return clientVisits;
		}
		set;
	}
	
	

}