/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class products_paid_by_agency_test {

    static testMethod void myUnitTest() {
        TestFactory tf = new TestFactory();
       
       	Account agency = tf.createAgency();
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
		Contact client = tf.createLead(agency, emp);
		client_course__c booking = tf.createBooking(client);
       	client_product_service__c product = tf.createCourseProduct(booking, agency);
       	
       	product.Sold_By_Agency__c = agency.id;
       	product.Received_Date__c = System.today();
       	product.Commission_Type__c = 0;
       	product.Commission__c = 10;
       	update product;
       
       	Test.startTest();
       	system.runAs(portalUser){
	       	
			products_paid_by_agency testClass = new products_paid_by_agency();
			
			List<SelectOption> Periods = testClass.getPeriods();
			List<SelectOption> agencyGroupOptions = testClass.agencyGroupOptions;
			
			testClass.SelectedPeriod = 'THIS_MONTH';
			testClass.searchProducts();

			testClass.changeGroup();
			testClass.generateExcel();
			testClass.searchProductsExcel();
       	}
    }
}