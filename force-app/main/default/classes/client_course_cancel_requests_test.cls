@isTest
private class client_course_cancel_requests_test {
	
	
	public static Account school {get;set;}
	public static Account agency {get;set;}
	public static Contact emp {get;set;}
	public static Contact client {get;set;}
	public static User portalUser {get{
    if (null == portalUser) {
      portalUser = [Select Id, Name, Contact.AccountId, Contact.Account.ParentId, Contact.Department__c from User where email = 'test12345@test.com' limit 1];
    } return portalUser;} set;}
	public static Account campus {get;set;}
	public static Course__c course {get;set;}
	public static Campus_Course__c campusCourse {get;set;}
	public static client_course__c clientCourseBooking {get;set;}
	public static client_course__c clientCourse {get;set;}
    
	@testSetup static void setup() {
		TestFactory tf = new TestFactory();
		Account schGroup = tf.createSchoolGroup();

		school = tf.createSchool();
		school.ParentId = schGroup.Id;
		update school;

		agency = tf.createAgency();
		emp = tf.createEmployee(agency);
		portalUser = tf.createPortalUser(emp);
		campus = tf.createCampus(school, agency);
		course = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);

		Schema.DescribeFieldResult fieldResult = client_course__c.Credit_Reason__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		
		system.runAs(portalUser){
			client = tf.createClient(agency);
			client.Owner__c = portalUser.Id;
			update client;

			clientCourseBooking = tf.createBooking(client);
			clientCourse = tf.createClientCourse(client, school, campus, course, campusCourse, clientCourseBooking);
			
			//------------- Create Cancel Request ------------------//
			client_course__c course = [SELECT Id, (Select Id from client_course_instalments__r Where ((Received_Date__c != null AND Paid_To_School_On__c = null AND isMigrated__c = false AND isPDS__c = false AND isPCS__c = false AND Original_Instalment__c = NULL) OR (isInstalment_Amendment__c = true AND Total_School_Payment__c> 0 AND Paid_To_School_On__c = null AND isPDS__c = false AND isPCS__c = false))) FROM client_course__c WHERE Booking_Number__c != null AND isCourseCredit__c = FALSE and isDeposit__c = FALSE];

			ApexPages.currentPage().getParameters().put('id', course.Id);
			client_course_cancel test = new client_course_cancel();

			client_course_cancel.saveCancel('undefined', client_course_cancel.TYPE_REQUESTED_ADM, new list<String>{course.Id}, ple[0].getValue(), '0', '');
		}
  	}

	public static String requestId {get{
    if (null == requestId) {
      requestId = [SELECT Id FROM client_course__c WHERE isCancelRequest__c = true limit 1].Id;
    } return requestId;} set;}


	static testMethod void contructor(){
		system.runAs(portalUser){
			client_course_cancel_requests test = new client_course_cancel_requests();
		}
	}

	static testMethod void changeFilters(){

		system.runAs(portalUser){
			client_course_cancel_requests.changeGroup(portalUser.Contact.Account.ParentId);
			client_course_cancel_requests.changeAgency(portalUser.Contact.AccountId);
		}
	}

	static testMethod void findRequests(){
		system.runAs(portalUser){
			client_course_cancel_requests.findRequests('pending', portalUser.Contact.Account.ParentId, new list<String>{portalUser.Contact.AccountId}, portalUser.Contact.AccountId, 'all', '');
			client_course_cancel_requests.findRequests('reqSchool', portalUser.Contact.Account.ParentId, new list<String>{portalUser.Contact.AccountId}, portalUser.Contact.AccountId, 'all', '');
			client_course_cancel_requests.findRequests('transfer', portalUser.Contact.Account.ParentId, new list<String>{portalUser.Contact.AccountId}, portalUser.Contact.AccountId, 'all', '');
			client_course_cancel_requests.findRequests('refund', portalUser.Contact.Account.ParentId, new list<String>{portalUser.Contact.AccountId}, portalUser.Contact.AccountId, 'all', '');
		}
	}

	static testMethod void addMissingDocs(){
		system.runAs(portalUser){
			client_course_cancel_requests.addMissingDocs(requestId, 'note', 'pending', portalUser.Contact.Account.ParentId, new list<String>{portalUser.Contact.AccountId}, portalUser.Contact.AccountId, 'all', '');
		}
	}

	static testMethod void requestCredit(){
		system.runAs(portalUser){
			client_course_cancel_requests.requestCredit(requestId, '1000', 'note', 'pending', portalUser.Contact.Account.ParentId, new list<String>{portalUser.Contact.AccountId}, portalUser.Contact.AccountId, 'all', '');
		}
	}

	static testMethod void requestRefund(){
		system.runAs(portalUser){
			client_course_cancel_requests.requestRefund(requestId, '1000', 'note', 'pending', portalUser.Contact.Account.ParentId, new list<String>{portalUser.Contact.AccountId}, portalUser.Contact.AccountId, 'all', '', 'request');
		}
	}

	static testMethod void undoRequest(){
		system.runAs(portalUser){

			client_course__c req = [Select Client__c From client_course__c where Id = :requestId limit 1];
			client_course_cancel_requests.undoRequest(requestId, 'note', 'pending', portalUser.Contact.Account.ParentId, new list<String>{portalUser.Contact.AccountId}, portalUser.Contact.AccountId, 'all', '');

			ApexPages.currentPage().getParameters().put('id', req.Client__c);
			client_course_cancel_history hist = new client_course_cancel_history();
		}
	}

	static testMethod void cancelCourse(){
		system.runAs(portalUser){
			client_course_cancel_requests.cancelCourse(requestId, 'note', 'pending', portalUser.Contact.Account.ParentId, new list<String>{portalUser.Contact.AccountId}, portalUser.Contact.AccountId, 'all', '');
		}
	}

	static testMethod void schoolRefund(){
		system.runAs(portalUser){
			client_course_cancel_requests.schoolRefund(requestId, '1000', '2018-05-18', 'string:'+ string.valueOf(portalUser.Contact.AccountId), 'note', 'pending', portalUser.Contact.Account.ParentId,  new list<String>{portalUser.Contact.AccountId}, portalUser.Contact.AccountId, 'all', '');
		}
	}
	// static testMethod void agencyRefund(){
	// 	system.runAs(portalUser){
	// 		client_course__c course = [SELECT Id FROM client_course__c WHERE Booking_Number__c != null AND isCourseCredit__c = FALSE and isDeposit__c = FALSE];

	// 		client_course_cancel_requests.agencyRefund(requestId, 'string:'+ string.valueOf(portalUser.Contact.AccountId), '1000', '2018-05-18', new list<String>{course.Id + ';;1000'}, '0', '1000', 'note', 'pending', portalUser.Contact.Account.ParentId,  portalUser.Contact.AccountId, portalUser.Contact.AccountId, 'all', '');
	// 	}
	// }

	static testMethod void confirmCourseTransfer(){
		system.runAs(portalUser){
			client_course_cancel_requests.confirmCourseTransfer(requestId, '1000', 'note', 'pending', portalUser.Contact.Account.ParentId,  new list<String>{portalUser.Contact.AccountId}, portalUser.Contact.AccountId, 'all', '');
		}
	}

	static testMethod void addNote(){
		system.runAs(portalUser){
			client_course_cancel_requests.addNote(requestId, 'note', 'pending');
		}
	}

	static testMethod void strings(){

		system.runAs(portalUser){
			client_course_cancel_requests test = new client_course_cancel_requests();
			String requests = test.requests;
			String SEPARATOR_FILE = test.SEPARATOR_FILE;
			String SEPARATOR_URL = test.SEPARATOR_URL;
			String notesSplit = test.notesSplit;
			String noteFld = test.noteFld;
			String typeMissDocs = test.typeMissDocs;
			String typeCourseTransferred = test.typeCourseTransferred;
			String typeCourseRefunded = test.typeCourseRefunded;
		}
	}
	
}