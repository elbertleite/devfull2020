public without sharing class PMCHelper {
    public PMCHelper() {}

    public static boolean hasUserRestrictedAccessPMCFunctionalities(){
        return [SELECT Restrict_Access_PMC_Functionalities__c FROM User WHERE ID = :UserInfo.getUserId()].Restrict_Access_PMC_Functionalities__c;
        //return true;
    }

    public static Map<String, IPClasses.PMCPriceResponse> validateAndSaveSchoolPrices(List<Course_Price__c> existingPrices, List<Course_Price__c> newPrices){
        system.debug('EXISTING PRICES. '+JSON.serialize(existingPrices));
        system.debug('NEW PRICES. '+JSON.serialize(newPrices));
        if(existingPrices == null || existingPrices.isEmpty()){
            system.debug('NO EXISTING PRICES FOUND. SAVING ALL NEW PRICES. ');
            insert newPrices;
            return null;
        }else{
            system.debug('EXISTING PRICES FOUND. VALIDATING NEW PRICES. ');
            Map<String, IPClasses.PMCPriceResponse> pricesResponse = new Map<String, IPClasses.PMCPriceResponse>();
            Map<String, Map<String, List<Course_Price__c>>> existingPricesPerNationality = new Map<String, Map<String, List<Course_Price__c>>>();
            
            pricesResponse.put('pricesOk', new IPClasses.PMCPriceResponse());
            pricesResponse.put('pricesError', new IPClasses.PMCPriceResponse());

            for(Course_Price__c existingPrice : existingPrices){
                if(!existingPricesPerNationality.containsKey(existingPrice.Nationality__c)){
                    existingPricesPerNationality.put(existingPrice.Nationality__c, new Map<String, List<Course_Price__c>>());
                }
                if(!existingPricesPerNationality.get(existingPrice.Nationality__c).containsKey(existingPrice.Campus_Course__c)){
                    existingPricesPerNationality.get(existingPrice.Nationality__c).put(existingPrice.Campus_Course__c, new List<Course_Price__c>());
                }
                existingPricesPerNationality.get(existingPrice.Nationality__c).get(existingPrice.Campus_Course__c).add(existingPrice);
            }

            Set<String> campusCoursesIds = new Set<String>(); 
            for(Course_Price__c newPrice : newPrices){
                campusCoursesIds.add(newPrice.Campus_Course__c);
            }

            Map<String, Campus_Course__c> campusCoursesSaved = new Map<String, Campus_Course__c>([SELECT ID, Course__r.Name, Campus__r.Name, Is_Available__c FROM Campus_Course__c WHERE ID in :campusCoursesIds]);

            List<Course_Price__c> pricesToSave = new List<Course_Price__c>();
            Campus_Course__c campusCourseFromPrice;
            List<Course_Price__c> similarPrices;
            Boolean priceValid;
            for(Course_Price__c newPrice : newPrices){
                campusCourseFromPrice = campusCoursesSaved.get(newPrice.Campus_Course__c);
                if(campusCourseFromPrice.Is_Available__c){
                    similarPrices = getSimilarPrices(newPrice, existingPricesPerNationality);
                    if(similarPrices == null || similarPrices.isEmpty()){
                        priceValid = true;
                    }else{
                        priceValid = true;
                        for(Course_Price__c existingPrice : similarPrices){
                            if(existingPrice.Availability__c == newPrice.Availability__c && existingPrice.From__c == newPrice.From__c){ 

                                //priceValid = (newPrice.Price_valid_from__c < existingPrice.Price_valid_from__c && newPrice.Price_valid_until__c < existingPrice.Price_valid_until__c && newPrice.Price_valid_until__c < existingPrice.Price_valid_from__c && newPrice.Price_valid_from__c < existingPrice.Price_valid_until__c) || (newPrice.Price_valid_from__c > existingPrice.Price_valid_from__c && newPrice.Price_valid_until__c > existingPrice.Price_valid_until__c && newPrice.Price_valid_until__c > existingPrice.Price_valid_from__c && newPrice.Price_valid_from__c > existingPrice.Price_valid_until__c);
                                if(priceValid){
                                    priceValid = isPMCNewPriceValid(newPrice.Price_valid_from__c, newPrice.Price_valid_until__c, existingPrice.Price_valid_from__c, existingPrice.Price_valid_until__c);
                                }
                            }
                        }
                    }
                    if(priceValid){
                        pricesToSave.add(newPrice);
                    }
                }else{
                    priceValid = false;
                }
                
                addPriceInfoToResponse(priceValid ? pricesResponse.get('pricesOk') : pricesResponse.get('pricesError'), newPrice.Nationality__c, campusCourseFromPrice.Campus__r.Name, campusCourseFromPrice.Course__r.Name);
            }           
            if(!Test.isRunningTest() && !pricesToSave.isEmpty()){
                insert pricesToSave;
            }
            return pricesResponse;
        }
    }

    public static boolean isPMCNewPriceValid(Date newPriceFrom, Date newPriceTo, Date existingPriceFrom, Date existingPriceTo){
        return newPriceFrom < newPriceTo && (newPriceTo < existingPriceFrom || newPriceFrom > existingPriceTo);
    }

    public static List<SelectOption> getSchoolFiles(String schoolId){
        String query = 'SELECT id, Parent_Folder_Id__c, Description__c, CreatedDate, Content_Type__c, File_Name__c,File_Size__c FROM Account_Document_File__c WHERE (Parent__c = :schoolID OR Parent__r.Parent.ID = :schoolID) ';

        query = query + ' and WIP__c = false order by File_Name__c';


        Map<String, String> folders = new Map<String, String>();
        Map<String, List<Account_Document_File__c>> filesPerFolder = new Map<String, List<Account_Document_File__c>>();
        
        List<Account_Document_File__c> documents = Database.query(query);

        for(Account_Document_File__c document : documents){
            if(document.Content_Type__c == 'Folder'){
                folders.put(document.ID, document.File_Name__c);
            }
        }
    
        String folderName;
        for(Account_Document_File__c document : documents){
            if(document.Content_Type__c != 'Folder'){
                folderName = folders.get(document.Parent_Folder_Id__c);
                if(!filesPerFolder.containsKey(folderName)){
                    filesPerFolder.put(folderName, new List<Account_Document_File__c>());
                }
                filesPerFolder.get(folderName).add(document);
            }
        }

        List<SelectOption> files = new List<SelectOption>();

        files.add(new SelectOption('','- SELECT A FILE -'));
        for(String folder : filesPerFolder.keySet()){
            if(!String.isEmpty(folder)){
                files.add(new SelectOption('SEPARATOR','SEPARATOR',true));
                files.add(new SelectOption(folder,folder,true));
                for(Account_Document_File__c file : filesPerFolder.get(folder)){
                    files.add(new SelectOption(file.ID, file.Description__c + ' (' +file.File_Name__c+ ')'));
                }
            }
        }
        return files;
    }

    public static List<Campus_Course__c> retrieveCampusCourses(String schoolId, List<String> courseId, List<String> courseType, List<String> courseCategory, Boolean searchDisabledCourses){
        String query = 'SELECT course__c, course__r.Course_Unit_Length__c, course__r.name, course__r.Only_Sold_in_Blocks__c, Course__r.Course_Unit_Type__c, Campus__c, Campus__r.Name, Id, Is_Available__c FROM campus_course__c WHERE campus__r.parentId = :schoolId ';
    
        if(!searchDisabledCourses){
            query = query + 'AND course__r.Is_Unavailable__c = false ';
        }
        if(courseId != null && !courseId.isEmpty()){
            query += ' AND course__c in :courseId ';
        }
        if(courseType != null && !courseType.isEmpty()){
            query += ' AND course__r.Type__c IN :courseType ';  
        }
        if(courseCategory != null && !courseCategory.isEmpty()){
            query += ' AND course__r.Course_Category__c IN :courseCategory ';  
        }

        query += ' ORDER BY course__r.name ';

        return Database.query(query);
    }

    public static List<Course_Price__c> retrieveSchoolPrices(String schoolId, List<string> campusId, List<String> courseId, List<String> nationality, Date pricesValidFrom, Date pricesValidUntil, List<String> courseType, String pricesValidFor, List<String> courseCategory, Boolean searchDisabledCourses){
                
        // String query ='Select From__c, isSelected__c, Campus_Course__r.course__c, Campus_Course__r.campus__c, Availability__c, LastModifiedDate, LastModifiedBy.Name, Price_valid_from__c, Price_valid_until__c, Price_valid_until_expired__c, Nationality__c, To__c, Unavailable__c, unit_description__c, Price_per_week__c, Account_Document_File__c, Account_Document_File__r.Preview_Link__c, Account_Document_File__r.File_Name__c, Account_Document_File__r.Description__c, Comments__c, Campus_Course__r.course__r.name, (Select From_Date__c, To_Date__c, Value__c, Comments__c, CreatedBy.Name, CreatedDate, LastModifiedBy.Name, LastModifiedDate, Account_Document_File__c, Account_Document_File__r.Preview_Link__c, Account_Document_File__r.File_Name__c, Account_Document_File__r.Description__c FROM Start_Date_Ranges__r order by From_Date__c, Value__c) from Course_Price__c where Campus_Course__r.campus__r.parentId = :schoolId and campus_Course__r.campus__r.Disabled_Campus__c = false ';
        String query ='Select From__c, isSelected__c, Campus_Course__r.course__c, Campus_Course__r.campus__c, Availability__c, LastModifiedDate, LastModifiedBy.Name, Price_valid_from__c, Price_valid_until__c, Price_valid_until_expired__c, Nationality__c, To__c, Unavailable__c, unit_description__c, Price_per_week__c, Account_Document_File__c, Account_Document_File__r.Preview_Link__c, Account_Document_File__r.File_Name__c, Account_Document_File__r.Description__c, Comments__c, Campus_Course__r.course__r.name, (Select From_Date__c, To_Date__c, Value__c, Comments__c, CreatedBy.Name, CreatedDate, LastModifiedBy.Name, LastModifiedDate, Account_Document_File__c, Account_Document_File__r.Preview_Link__c, Account_Document_File__r.File_Name__c, Account_Document_File__r.Description__c FROM Start_Date_Ranges__r order by From_Date__c, Value__c) from Course_Price__c where Campus_Course__r.campus__r.parentId = :schoolId ';
        
        if(!searchDisabledCourses){
            query = query + 'AND Campus_Course__r.course__r.Is_Unavailable__c = false ';
        }
        if(campusId != null && !campusId.isEmpty()){
            query += ' and Campus_Course__r.campus__c in :campusId ';
        }
        if(courseId != null && !courseId.isEmpty()){
            query += ' and Campus_Course__r.course__c in :courseId ';
        }
        if(nationality != null && !nationality.isEmpty()){
            query += ' and Nationality__c in :nationality ';
        }
        if(pricesValidFrom != null){
            query += ' and Price_valid_From__c >= :pricesValidFrom ';
        }
        if(pricesValidUntil != null){
            query += ' and Price_valid_until__c <= :pricesValidUntil ';
        }
        if(courseType != null && !courseType.isEmpty()){
            query += ' AND Campus_Course__r.Course__r.Type__c in :courseType ';  
        }
        if(courseCategory != null && !courseCategory.isEmpty()){
            query += ' AND Campus_Course__r.Course__r.Course_Category__c in :courseCategory ';  
        }
        Decimal priceValid;
        if(pricesValidFor != '0'){
            priceValid = Decimal.valueOf(pricesValidFor);
            query += ' AND Availability__c = :priceValid ';  
        }

        query += ' order by Nationality__c, Campus_Course__r.campus__r.name, Campus_Course__r.course__r.name, Availability__c, Price_valid_from__c, Price_valid_until__c, From__c ';
        
        return Database.query(query);
    }

    public static List<Account> retrieveCampusesBySchool(String schoolId, List<string> campusesID, Boolean retrieveDisabledCampuses){
        String query = 'SELECT Id, Name, Disabled_Campus__c, ShowCaseOnly__c FROM Account WHERE parentId = :schoolId';
        if(campusesID != null && !campusesID.isEmpty()){
            query += ' AND ID in :campusesID ';
        }
        if(!retrieveDisabledCampuses){
            query = query + ' AND Disabled_Campus__c = false ';
        }
        query = query + ' ORDER BY name ';

        return Database.query(query);
    }

    public static List<String> retrieveCourseTypesFromSchool(String school, List<String> courseCategory, Boolean searchDisabledCourses){
        String query = 'SELECT Course__r.Type__c FROM Campus_Course__c WHERE Campus__r.Parent.ID = :school ';
        if(courseCategory != null && !courseCategory.isEmpty()){
            query = query + ' AND Course__r.Course_Category__c IN :courseCategory ';
        }
        if(!searchDisabledCourses){
            query = query + 'AND course__r.Is_Unavailable__c = false ';
        }
        query = query + ' ORDER BY Course__r.Type__c';

        List<String> types = new List<String>();
        for(Campus_Course__c cc : Database.query(query)){
            if(!types.contains(cc.Course__r.Type__c)){
                types.add(cc.Course__r.Type__c);
            }
        }
        return types;
    }

    public static double generatePMCPriceByOperation(Decimal currentPrice, Double priceClone, String operation, String typeOperation){
        if(priceClone == null){
            return currentPrice;
        }else{
            if(operation == 'full'){
                return priceClone;
            }else{
                Double newPriceToApply;
                newPriceToApply = typeOperation == 'percentage' ? (currentPrice * priceClone) / 100 : priceClone; 
                newPriceToApply = operation == 'add' ? newPriceToApply + currentPrice : currentPrice - newPriceToApply; 
                return newPriceToApply;
            }
        }
    }

    public static List<IPClasses.PMCCourse> retrieveCoursesFromSchool(string schoolId, List<String> courseType,  List<String> courseCategory, Boolean searchDisabledCourses){
        String query = 'SELECT course__c, course__r.name, course__r.Is_Unavailable__c, course__r.Only_Sold_in_Blocks__c FROM campus_course__c WHERE campus__r.parentId = :schoolId ';

        if(courseType != null && !courseType.isEmpty()){
            query = query + ' AND course__r.Type__c IN :courseType ';
        }
        if(courseCategory != null && !courseCategory.isEmpty()){
            query = query + ' AND course__r.Course_Category__c IN :courseCategory ';
        }
        if(!searchDisabledCourses){
            query = query + ' AND course__r.Is_Unavailable__c = false ';
        }

        query = query + ' ORDER BY course__r.name';

        Set<String> idCourses = new Set<String>(); 
        list<IPClasses.PMCCourse> response = new list<IPClasses.PMCCourse>();
        for(campus_course__c cc : Database.query(query)){
            if(!idCourses.contains(cc.course__c)){
                response.add(new IPClasses.PMCCourse(String.valueOf(cc.course__c), cc.course__r.name, cc.course__r.Is_Unavailable__c, cc.course__r.Only_Sold_in_Blocks__c));
            }
            idCourses.add(cc.course__c);
        }

        return response;
    }

    public static Map<String, List<IPClasses.SchoolExternalPage>> retrieveSchoolExternalPages(List<String> schoolIds, Boolean validateUserAccess, Set<String> idsToValidate){
        Map<String, List<IPClasses.SchoolExternalPage>> schoolsPages = new Map<String, List<IPClasses.SchoolExternalPage>>();
        Set<String> userIds = new Set<String>();
        if(validateUserAccess){
            if(idsToValidate == null){
                idsToValidate = new Set<String>();
            }
            User userLogged = [SELECT ID, Contact.Account.ID, Contact.Account.Parent.ID FROM User WHERE ID = :UserInfo.getUserId()];
            idsToValidate.add(userLogged.ID);
            if(!String.isEmpty(userLogged.Contact.Account.ID)){
                idsToValidate.add(userLogged.Contact.Account.ID);
            }
            if(!String.isEmpty(userLogged.Contact.Account.Parent.ID)){
                idsToValidate.add(userLogged.Contact.Account.Parent.ID);
            }
        }
        List<IPClasses.SchoolExternalPage> externalPages;
        for(Account school : [SELECT ID, Name, school_external_pages__c FROM Account WHERE ID IN :schoolIds ORDER BY Name]){
            schoolsPages.put(school.ID, new List<IPClasses.SchoolExternalPage>());     
            boolean hideAccessInfo;
            if(!String.isEmpty(school.school_external_pages__c)){
                externalPages = (List<IPClasses.SchoolExternalPage>) JSON.deserialize(school.school_external_pages__c, List<IPClasses.SchoolExternalPage>.class);
                for(IPClasses.SchoolExternalPage ep : externalPages){
                    hideAccessInfo = false;
                    if(validateUserAccess){
                        hideAccessInfo = true;
                        if(ep.idsAllowedToAccess.isEmpty()){
                            hideAccessInfo = false;
                        }else{
                            for(String id : ep.idsAllowedToAccess){
                                //if(userIds.contains(id)){
                                if(idsToValidate.contains(id)){
                                    hideAccessInfo = false;
                                    break;
                                }
                            }
                        }
                        if(hideAccessInfo){
                            ep.user = '*********';
                            ep.password = '*********';
                            ep.url = 'https://www.***********************.com';
                        }
                    }
                    ep.hideAccessInfo = hideAccessInfo;
                    schoolsPages.get(school.ID).add(ep);
                }
            }
        }
        return schoolsPages;
    }

    public static List<String> retrieveCourseCategoriesFromSchool(String school, Boolean searchDisabledCourses){
        List<String> categories = new List<String>();
        String category;
        String query = 'SELECT ID, Course__r.Course_Category__c FROM Campus_Course__c WHERE Campus__r.Parent.ID = :school ';
        if(!searchDisabledCourses){
            query = query + 'AND course__r.Is_Unavailable__c = false ';
        }
        query = query + ' ORDER BY Course__r.Course_Category__c';
        for(Campus_Course__c cc : Database.query(query)){
            category = String.isEmpty(cc.Course__r.Course_Category__c) ? 'Other' : cc.Course__r.Course_Category__c;
            if(!categories.contains(category)){
                categories.add(category);
            }
        }
        return categories;
    
    }
    public static Set<String> retrieveNationalityGroups(String schoolId, List<String> nationality){
        Set<String> response = new Set<String>();
        String query = 'SELECT name FROM nationality_group__c ';

        if(!String.isEmpty(schoolId)){
            query = query + ' WHERE account__c = :schoolId ';
        }
        
        if(nationality == null || nationality.isEmpty() || nationality.contains('Published Price')){
            response.add('Published Price');
        }
        
        if(nationality != null && !nationality.isEmpty() && (nationality.size() >= 1 || !nationality.contains('Published Price'))){
            if(!String.isEmpty(schoolId)){
                query += ' AND name in :nationality ';
            }else{
                query += ' WHERE name in :nationality ';
            } 
        }

        query = query + ' ORDER BY name';
        for(nationality_group__c nation : Database.query(query)){
            if(!response.contains(nation.name)){
                response.add(nation.name);
            }
        }
        return response;
    }

    public static List<IPClasses.PMCSchool> retrieveSchoolsByCountry(String country){
        Set<String> idSchools = new Set<String>();
        List<IPClasses.PMCSchool> response = new List<IPClasses.PMCSchool>();
       // String query = 'SELECT campus__r.parentId, campus__r.parent.Name FROM Campus_course__c WHERE campus__r.BillingCountry = :country and campus__r.Disabled_Campus__c = false order by campus__r.parent.Name';
        String query = 'SELECT campus__r.parentId, campus__r.parent.Name FROM Campus_course__c WHERE campus__r.BillingCountry = :country order by campus__r.parent.Name';
        
        for(Campus_course__c cc : Database.query(query)){
            if(!idSchools.contains(cc.campus__r.parentId)){
                response.add(new IPClasses.PMCSchool(cc.campus__r.parentId, cc.campus__r.parent.Name));
            }
            idSchools.add(cc.campus__r.parentId);
        }
        return response;
    }

    private static void addPriceInfoToResponse(IPClasses.PMCPriceResponse priceResponseField, String nationality, String campus, String course){
        priceResponseField.totalItens += 1;

        if(!priceResponseField.responsePerNationality.containsKey(nationality)){
            priceResponseField.responsePerNationality.put(nationality, new Map<String, List<String>>());    
        }
        if(!priceResponseField.responsePerNationality.get(nationality).containsKey(campus)){
            priceResponseField.responsePerNationality.get(nationality).put(campus, new List<String>());
        }
        priceResponseField.responsePerNationality.get(nationality).get(campus).add(course);
    } 

    private static List<Course_Price__c> getSimilarPrices(Course_Price__c newPrice, Map<String, Map<String, List<Course_Price__c>>> existingPricesPerNationality){
        if(existingPricesPerNationality.containsKey(newPrice.Nationality__c) && existingPricesPerNationality.get(newPrice.Nationality__c).containsKey(newPrice.Campus_Course__c)){
            return existingPricesPerNationality.get(newPrice.Nationality__c).get(newPrice.Campus_Course__c);
        }
        return null;
    }


    /*private static void addFileToSchoolFolder(String schoolID, String folderName, IPClasses.PMCFile file, List<IPClasses.PMCSchoolDocuments> response){
        IPClasses.PMCSchoolDocuments school;
        IPClasses.PMCDocumentsFolder folder;

        Integer index = response.indexOf(new IPClasses.PMCSchoolDocuments(schoolID));
        if(index > -1){
            school = response.get(index);
            index = school.folders.indexOf(new IPClasses.PMCDocumentsFolder(folderName));
            if(index > -1){
                folder = school.folders.get(index);
                folder.totalDocuments += 1;
                folder.files.add(file);
            }else{
                folder = new IPClasses.PMCDocumentsFolder(folderName);
                folder.totalDocuments += 1;
                folder.files.add(file);
                school.folders.add(folder);
            }
            school.totalDocuments += 1;
        }
    }*/

    /*
        This method returns all the new and old documents from one or more schools, separating them on a map by schools, folders and documents.
    */
    public static List<IPClasses.PMCSchoolDocuments> retrieveSchoolDocumentsTreeFormat(List<String> idSchoolsToSearch, Boolean orderFiles){
        
        List<IPClasses.PMCSchoolDocuments> response = null;
        if(idSchoolsToSearch != null && !idSchoolsToSearch.isEmpty()){
            Map<String, Map<String, IPClasses.PMCDocumentsFolder>> foldersPerSchool = new Map<String, Map<String, IPClasses.PMCDocumentsFolder>>();
            Map<String, String> foldersPerId = new Map<String, String>();  
            IPClasses.PMCFile schoolFile;
            IPClasses.PMCDocumentsFolder filesFolder;
            String folderName;
            String schoolId;


            for(String id : idSchoolsToSearch){
                foldersPerSchool.put(id, new Map<String, IPClasses.PMCDocumentsFolder>());
			}

            List<Account_Document_File__c> allFolders = [SELECT ID, File_Name__c, Parent__r.RecordType.Name, Parent__r.Parent.ID, Parent__c, Links_To_External_Files__c FROM Account_Document_File__c WHERE (Parent__r.Parent.ID IN :idSchoolsToSearch OR Parent__c IN :idSchoolsToSearch) AND Content_Type__c = 'Folder' ORDER BY File_Name__c];

            if(allFolders != null && !allFolders.isEmpty()){
                List<IPClasses.PMCFile> links;
                for(Account_Document_File__c folder : allFolders){
                    schoolId = folder.Parent__r.RecordType.Name == 'Campus' ? folder.Parent__r.Parent.ID : folder.Parent__c;
                    folderName = normalizeFolderName(folder.File_Name__c);
                    
                    foldersPerId.put(folder.ID, folderName);

                    if(!String.isEmpty(folder.Links_To_External_Files__c)){
                        links = (List<IPClasses.PMCFile>) JSON.deserialize(folder.Links_To_External_Files__c, List<IPClasses.PMCFile>.class);
                    }

                    if(!foldersPerSchool.get(schoolId).containsKey(folderName)){
                        foldersPerSchool.get(schoolId).put(folderName, new IPClasses.PMCDocumentsFolder(folderName));
                    }

                    if(links != null && !links.isEmpty()){
                        foldersPerSchool.get(schoolId).get(folderName).files.addAll(links);
                        foldersPerSchool.get(schoolId).get(folderName).totalDocuments = foldersPerSchool.get(schoolId).get(folderName).files.size(); 
                    }
                    links = null;
                }
            }


            for(String school : foldersPerSchool.keySet()){
                if(!foldersPerSchool.get(school).containsKey('Other')){
                    foldersPerSchool.get(school).put('Other', new IPClasses.PMCDocumentsFolder('Other'));
                }
            }

            List<Account_Document_File__c> allFiles = [SELECT ID, CreatedDate, CreatedBy.Name, File_Name__c, Preview_Link__c, Parent_Folder_id__c, Parent__r.Parent.ID, Description__c, LastModifiedDate, LastModifiedBy.Name, Parent__c, Parent__r.RecordType.Name, Is_Folder_New_School_Page__c, Parent__r.Name, Content_Type__c, File_Expiry_Date__c, S3_File_Name__c FROM Account_Document_File__c WHERE (Parent__r.Parent.ID IN :idSchoolsToSearch OR Parent__c IN :idSchoolsToSearch) AND Content_Type__c != 'Folder' ORDER BY File_Name__c];

            if(allFiles != null && !allFiles.isEmpty()){
                
                
                List<Account_Document_File__c> newFiles = new List<Account_Document_File__c>();
                String [] previewLinkData;
                Date today = Date.today();

                for(Account_Document_File__c file : allFiles){

                    schoolId = file.Parent__r.RecordType.Name == 'Campus' ? file.Parent__r.Parent.ID : file.Parent__c;
                    
                    folderName = null;
                    if(!String.isEmpty(file.Parent_Folder_id__c)){
                        folderName = foldersPerId.get(file.Parent_Folder_id__c);
                    }
                    folderName = normalizeFolderName(folderName); 

                    filesFolder = foldersPerSchool.get(schoolId).get(folderName);
                    if(filesFolder == null){
                        filesFolder = foldersPerSchool.get(schoolId).get('Other');
                    }

                    schoolFile = new IPClasses.PMCFile();
                    schoolFile.ID = file.ID;
                    schoolFile.description = String.isEmpty(file.Description__c) ? '' : file.Description__c;
                    schoolFile.name = file.File_Name__c;
                    schoolFile.lastModifiedBy = file.lastModifiedBy.Name;
                    schoolFile.lastModifiedDate = file.lastModifiedDate;
                    schoolFile.createdDate = file.CreatedDate;
                    schoolFile.createdBy = file.CreatedBy.Name;
                    
                    if(file.Is_Folder_New_School_Page__c){
                        previewLinkData = file.Preview_Link__c.split('!#!');
					
                        schoolFile.expiryDate = IPFunctions.getStringFromDate(file.File_Expiry_Date__c);
                        schoolFile.isExpired = file.File_Expiry_Date__c < today;
                        schoolFile.type = 'File';
                        schoolFile.url = previewLinkData[0];
                        if(previewLinkData.size() > 1){
                            //schoolFile.downloadUrl = previewLinkData[1];
                            schoolFile.downloadUrl = previewLinkData[2];
                        }
                    }else{
                        schoolFile.type = 'Old File';
                        schoolFile.url = file.Preview_Link__c;
                        schoolFile.downloadUrl = file.Preview_Link__c;
                        //file.parentID = oldFile.Parent_Folder_id__c;
                        //file.folder = folderName;
                    }

                    

                    if(!String.isEmpty(schoolFile.url) || !String.isEmpty(schoolFile.downloadUrl)){
                        filesFolder.files.add(schoolFile);
                        filesFolder.totalDocuments += 1;
                    }
                    
                }
                //system.debug('THE FILES FOUND '+JSON.serialize(allFiles));
            }

            response = new List<IPClasses.PMCSchoolDocuments>();
            IPClasses.PMCSchoolDocuments schoolDocuments;
            Integer schoolTotalDocs;
            for(String school : foldersPerSchool.keySet()){
                schoolTotalDocs = 0;
                schoolDocuments = new IPClasses.PMCSchoolDocuments(school);
                for(String folder : foldersPerSchool.get(school).keySet()){
                    filesFolder = foldersPerSchool.get(school).get(folder);
                    if(!Test.isRunningTest() && orderFiles && !filesFolder.files.isEmpty()){
                        filesFolder.files.sort();
                    }
                    schoolDocuments.folders.add(filesFolder);   
                    schoolTotalDocs += filesFolder.totalDocuments;
                }  
                schoolDocuments.totalDocuments = schoolTotalDocs;
                response.add(schoolDocuments);         
            }            

        }
        //system.debug('THE RESPONSE '+JSON.serialize(response));
        return response;
    }

    private static String normalizeFolderName(String folderName){
        if(!String.isEmpty(folderName)){
            if(folderName == 'Home'){
                return 'Other';
            }
            return folderName.toLowerCase().capitalize();
        }else{
            return 'Other';
        }
    }

    public static Boolean isFileExpired(String expireDate){
        if(!String.isEmpty(expireDate)){
            Date fileExpireDate = IPFunctions.getDateFromString(expireDate);
            return fileExpireDate < Date.today();
        }
        return false;
    } 

    /*public static List<IPClasses.PMCSchoolDocuments> retrieveSchoolDocumentsTreeFormat(List<String> idSchools, Boolean orderFiles){
        
        List<IPClasses.PMCSchoolDocuments> response = null;
        String folderName;
        if(idSchools != null && !idSchools.isEmpty()){ 
            Map<String, Map<String, String>> oldFoldersStructure = new Map<String, Map<String, String>>();
            Map<String, List<String>> foldersPerSchool = new Map<String, List<String>>();
            
            for(String id : idSchools){
                foldersPerSchool.put(id, new List<String>());
			}
      
            List<Account_Document_File__c> foldersNewPage = [SELECT ID, File_Name__c, Preview_Link__c, Parent__c, S3_File_Description__c FROM Account_Document_File__c WHERE Parent__c IN :idSchools AND Is_Folder_New_School_Page__c = true AND Content_Type__c = 'Folder' ORDER BY Parent__c, File_Name__c, CreatedDate];
            
            List<Account_Document_File__c> foldersOldPage = [SELECT ID, File_Name__c, Parent__r.RecordType.Name, Parent__r.Parent.ID, Parent__c FROM Account_Document_File__c WHERE (Parent__r.Parent.ID IN :idSchools OR Parent__c IN :idSchools) AND Is_Folder_New_School_Page__c = false AND Content_Type__c = 'Folder' ORDER BY Parent__r.Name];

            String parentID;
            for(Account_Document_File__c folder : foldersNewPage){
                parentID = folder.Parent__c;
                folderName = normalizeFolderName(folder.File_Name__c);
                if(!foldersPerSchool.get(parentID).contains(folderName)){
                    foldersPerSchool.get(parentID).add(folderName);
                }
            }
            system.debug('THE CURRENT FOLDERS PER SCHOOL '+JSON.serialize(foldersPerSchool));
            for(Account_Document_File__c folder : foldersOldPage){
                if(folder.Parent__r.RecordType.Name == 'Campus'){
                    parentID = folder.Parent__r.Parent.ID;
                }else{
                    parentID = folder.Parent__c;
                }
                if(!String.isEmpty(parentID)){
                    folderName = normalizeFolderName(folder.File_Name__c);				
                    if(!foldersPerSchool.get(parentID).contains(folderName)){
                        foldersPerSchool.get(parentID).add(folderName);
                    }
                    if(!oldFoldersStructure.containsKey(parentID)){
                        oldFoldersStructure.put(parentID, new Map<String, String>());
                    }
                    oldFoldersStructure.get(parentID).put(folder.ID, normalizeFolderName(folder.File_Name__c));
                }
            }

            response = new List<IPClasses.PMCSchoolDocuments>();
            IPClasses.PMCSchoolDocuments schoolDocs;

            List<String> folders;
            for(String idSchool : foldersPerSchool.keySet()){
                schoolDocs = new IPClasses.PMCSchoolDocuments(idSchool);
                folders = foldersPerSchool.get(idSchool);
                if(folders != null && !folders.isEmpty()){
                    folders.sort();
                    for(String folder : folders){
                        schoolDocs.folders.add(new IPClasses.PMCDocumentsFolder(folder));
                    }
                }
                response.add(schoolDocs);
            }

            String [] link;
            IPClasses.PMCFile file;
            List<String> previewLinks;
            List<IPClasses.PMCFile> s3FilesDescription;
            for(Account_Document_File__c folder : foldersNewPage){
                folderName = normalizeFolderName(folder.File_Name__c);
                s3FilesDescription = null;

                if(!String.isEmpty(folder.Preview_Link__c)){
                    
                    if(!String.isEmpty(folder.S3_File_Description__c)){
                        s3FilesDescription = (List<IPClasses.PMCFile>) JSON.deserialize(folder.S3_File_Description__c, List<IPClasses.PMCFile>.class);
                    }
                    previewLinks = folder.Preview_Link__c.split(';;');
                    if(previewLinks != null && !previewLinks.isEmpty()){
                        for(String previewLink : previewLinks){
                            link = previewLink.split('!#!');
                            file = new IPClasses.PMCFile();
                            file.ID = link[1];
                            file.name = link[1];
                            file.url = link[0];
                            file.downloadUrl = link[2];
                            file.type = 'File';
                            file.parentID = folder.ID;
                            file.folder = folderName;
                            if(s3FilesDescription != null && !s3FilesDescription.isEmpty()){
                                for(IPClasses.PMCFile savedFile : s3FilesDescription){
                                    if(savedFile.ID == file.ID){
                                        file.description = savedFile.description;
                                        file.expiryDate = savedFile.expiryDate;
                                        file.isExpired = isFileExpired(savedFile.expiryDate);
                                        file.lastModifiedBy = savedFile.lastModifiedBy;
                                        file.lastModifiedDate = savedFile.lastModifiedDate;
                                    }
                                }
                            }
                            if(String.isEmpty(file.description)){
                                file.description = file.name;
                            }

                            addFileToSchoolFolder(folder.Parent__c, folderName, file, response);    
                        }
                    }
                }
            }

            for(Account_Document_File__c oldFile : [SELECT ID, CreatedDate, CreatedBy.Name, File_Name__c, Preview_Link__c, Parent_Folder_id__c, Parent__r.Parent.ID, Description__c, LastModifiedDate, LastModifiedBy.Name, Parent__c, Parent__r.RecordType.Name FROM Account_Document_File__c WHERE (Parent__r.Parent.ID IN :idSchools OR Parent__c IN :idSchools) AND Is_Folder_New_School_Page__c = false AND Content_Type__c != 'Folder' ORDER BY Parent__r.Name]){
                
                if(oldFile.Parent__r.RecordType.Name == 'Campus'){
                    parentID = oldFile.Parent__r.Parent.ID;
                }else{
                    parentID = oldFile.Parent__c;
                }
                folderName = normalizeFolderName(oldFoldersStructure.get(parentID).get(oldFile.Parent_Folder_id__c));
                file = new IPClasses.PMCFile();
                file.ID = oldFile.ID;
                file.description = oldFile.Description__c;
                file.name = oldFile.File_Name__c;
                file.url = oldFile.Preview_Link__c;
                file.downloadUrl = oldFile.Preview_Link__c;
                file.lastModifiedBy = oldFile.LastModifiedBy.Name;
                file.lastModifiedDate = oldFile.lastModifiedDate;
                file.createdDate = oldFile.CreatedDate;
                file.createdBy = oldFile.CreatedBy.Name;
                file.parentID = oldFile.Parent_Folder_id__c;
                file.folder = folderName;
                file.type = 'Old File';

                addFileToSchoolFolder(parentID, folderName, file, response); 
            }

            if(orderFiles){
                for(IPClasses.PMCSchoolDocuments school : response){
                    for(IPClasses.PMCDocumentsFolder folder : school.folders){
                        if(folder.totalDocuments > 0){
                            if(!Test.isRunningTest()){
                                folder.files.sort();
                            }
                        }
                    }
                }
            }
        }

        system.debug('THE RESPONSE '+JSON.serialize(response));

        return response;
    }*/
}