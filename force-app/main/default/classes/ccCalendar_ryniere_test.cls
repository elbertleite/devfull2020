/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ccCalendar_ryniere_test {

    static testMethod void myUnitTest() {
    	
    	
    	String id = [Select Id from RecordType where Name = 'Campus' and isActive = true LIMIT 1].id;
    	Account campus = new Account();
    	campus.RecordTypeId = id;
    	campus.name='Campus CBD';
    	insert campus;
    	
    	
        ccCalendar_ryniere cal = new ccCalendar_ryniere();
        cal.AccountID =campus.id;
        cal.FromDate = System.today();
        cal.ToDate = System.today();
        cal.inWeekday = '1';
        cal.WeekFrequency = '2';
        Course_Price__c auxDate = cal.auxDate;
        cal.getNumberOfMonths();
        cal.getFromMonthIndex();
        cal.getFromYear();
        cal.getFromDateOutOfBounds();
        cal.getToDateOutOfBounds();
        
        List<String> selectedDatesStrList = cal.selectedDatesStrList;
        cal.convertedToList();
        cal.getWeekdayNamesSelectOption();
        cal.getWeekFrequencySelectOption();
        cal.getDaysAndRecur();
        //cal.getXdays(1,1,cal.FromDate,cal.ToDate);
        cal.getFindFirstXdayFrom(1,cal.FromDate);
        
    }
}