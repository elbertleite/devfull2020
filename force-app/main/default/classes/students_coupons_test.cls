@isTest
private class students_coupons_test {

	public static Account school {get;set;}
	public static Account agency {get;set;}
	public static Contact emp {get;set;}
	public static Contact client {get;set;}
	public static User portalUser {get{
    if (null == portalUser) {
      portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
    }
    return portalUser;
  }set;}
	public static Account campus {get;set;}
	public static Course__c course {get;set;}
	public static Campus_Course__c campusCourse {get;set;}
	public static client_course__c clientCourseBooking {get;set;}
	public static client_course__c clientCourse {get;set;}
	public static client_course__c clientCourse2 {get;set;}


	@testSetup static void setup() {
		TestFactory tf = new TestFactory();

		school = tf.createSchool();
		agency = tf.createAgency();
		emp = tf.createEmployee(agency);
		portalUser = tf.createPortalUser(emp);
		campus = tf.createCampus(school, agency);
		course = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);
		system.runAs(portalUser){
			client = tf.createClient(agency);
			clientCourseBooking = tf.createBooking(client);
			clientCourse = tf.createClientCourse(client, school, campus, course, campusCourse, clientCourseBooking);
			tf.createClientCourseFees(clientCourse, false);
			List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(clientCourse);


			//Pay Instalment
			client_course_instalment_pay toPay = new client_course_instalment_pay(new ApexPages.StandardController(instalments[0]));
			toPay.newPayDetails.Payment_Type__c = 'Creditcard';
			toPay.newPayDetails.Value__c = instalments[0].Instalment_Value__c;
			toPay.newPayDetails.Date_Paid__c = Date.today();
			toPay.addPaymentValue();
			toPay.savePayment();
	  }
	}

	static testMethod void retrieveFilters(){
		Test.startTest();
		system.runAs(portalUser){
			students_coupons toTest = new students_coupons();
			List<SelectOption> agencyGroupOptions = toTest.agencyGroupOptions;
			client_course_instalment__c dateFilter = toTest.dateFilter;

			toTest.getAgencies();
			toTest.getDepartments();
			toTest.getPeriods();
			toTest.changeGroup();

		}
		Test.stopTest();
	}

	static testMethod void generateCouponsPDF(){
		Test.startTest();
		system.runAs(portalUser){
			students_coupons toTest = new students_coupons();
			toTest.generateCouponsPDF();
		}
		Test.stopTest();
	}


}