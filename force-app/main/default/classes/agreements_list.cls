public without sharing class agreements_list {
	
	public String allSchools {get;set;}
	public String allAgencies {get;set;}
	public String allCountries {get;set;}
	public String allStatus {get;set;}
	public String allCourseTypes {get;set;}
	public String allInstType {get;set;}

	//Constructor
	public agreements_list(){
		findCountries();
		findAllSchools();
		agreementStatus();
		courseTypes();
		institutionType();
	}

	@remoteAction
	public static UserProfile getUserProfile(){
		String userid = 'U' + userInfo.getUserId();  //included a U in front of the Id 
													//to prevent a javascript error
		String sflocale = userInfo.getLocale();      //get user’s locale
		String anlocale = sflocale.toLowerCase();          
		anlocale = anlocale.replace('_','-');        // convert from locale format 
													//of en_US to en-us
		String money = userInfo.getDefaultCurrency();  // get user’s currency
		String language = userInfo.getLanguage();      // get user’s language
		TimeZone tz = userInfo.getTimezone();          // get the timezone
		String timezone = tz.toString();               // convert to user’s timezone

		UserProfile userp = new UserProfile(userid, sflocale, anlocale, 
											money, language, timezone);
		return userp;     
	}

	//helper class to return an object
	public class UserProfile {                  
		public String userid{get;set;}
		public String sflocale{get;set;}
		public String anlocale{get;set;}
		public String money{get;set;}
		public String language{get;set;}
		public String timezone{get;set;}

		public UserProfile (String userid, String sflocale, String anlocale, String money, 
							String language, String timezone) {
			this.userid = userid;
			this.sflocale = sflocale;
			this.anlocale = anlocale;
			this.money = money;
			this.language = language;   
			this.timezone = timezone;
		}
	}

	// @RemoteAction
	// public static String findAgreementByAgency(String selStatus, String selGroup, String selAgency){
	// 	system.debug('selStatus ==> ' + selStatus);
	// 	system.debug('selGroup ==> ' + selGroup);
	// 	system.debug('selAgency ==> ' + selAgency);

	// 	list<Id> allAgree = new list<Id>();
	// 	list<agWrapper> result = new list<agWrapper>();

	// 	String sql = 'SELECT Agreement__c FROM Agreement_Accounts__c WHERE ';

	// 	//School Id
	// 	if(selAgency != 'all' && selAgency != null){
	// 		sql += ' Account__c = :selAgency ';
	// 	}
	// 	else{
	// 		//All Schools
	// 		sql += ' Account__r.RecordType.Name = \'Agency\' ';

	// 		//Country
	// 		if(selGroup != 'all' && selGroup != null)
	// 			sql += ' and Account__r.ParentId = :selGroup';
	// 	}

	// 	//Agreement Status
	// 	sql += ' and Agreement__r.Status__c = :selStatus' ;

	// 	system.debug('sql===>' + sql);

	// 	for(Agreement_Accounts__c aa : Database.query(sql))
	// 		allAgree.add(aa.Agreement__c);

	// 	return findAgreements(allAgree, false, null);
	// }

	@RemoteAction
	public static String findAgreementBySchool(Integer sqlOffset, Integer pageLimit, String selStatus, String selCountry, String schId, String expireIn, String courseType, String instType, String validFrom){
		system.debug('selStatus ==> ' + selStatus);
		system.debug('selCountry ==> ' + selCountry);
		system.debug('schId ==> ' + schId);
		system.debug('expireIn ==> ' + expireIn);

		set<Id> allAgree = new set<Id>();
		list<agWrapper> result = new list<agWrapper>();

		String sql = 'SELECT Agreement__c ';

		if(schId != 'all' && schId != null){
			sql += ' , (SELECT School_Control__c, Campus_Control__c, Campus_Name__c, Course_Type_Control__c, Country__c, School_Name__c, Campus_Id__r.name, Campus_Commission__c, Campus_Commission_Type__c, Campus_Course__c, Campus_Course__r.campus__c,CampusCourseTypeId__c, Campus_Id__c, Comments__c, Name, Course_Commission__c, Course_Commission_Type__c, Course_Name__c, Course_Type_Commission__c, Course_Type_Commission_Type__c, courseType__c, Id, School_Commission__c, School_Commission_Type__c, School_Id__c FROM Commissions__r order by Country__c, School_Name__c, Campus_Name__c, Course_Type_Control__c, Course_Name__c) ';
		}

		sql += ' FROM Agreement_Accounts__c WHERE ';

		//School Id
		if(schId != 'all' && schId != null){
			sql += ' Account__c = :schId ';
		}
		//All Schools
		else{
			sql += ' Account__r.RecordType.Name = \'School\' ';

			//Country
			if(selCountry != 'all' && selCountry != null)
				sql += ' AND Account__r.BillingCountry = :selCountry';

			if(courseType != 'all')
				sql += ' and Account__r.Course_Types_Offered__c Includes ( \''+ courseType + '\' ) ';

			if(instType != 'all')
				sql += ' and Account__r.Type_of_Institution__c = :instType ';
		}

		if(expireIn != 'none')
			sql += ' AND Agreement__r.End_Date__c <= ' + expireIn + ' AND Agreement__r.isCancelled__c = false AND  Agreement__r.End_Date__c > TODAY' ;

		//Agreement Status
		if(selStatus == 'Valid')
			sql += ' AND Agreement__r.Start_Date__c <= TODAY AND (Agreement__r.End_Date__c >= TODAY OR Agreement__r.Until_Terminated__c = true ) AND Agreement__r.isAvailable__c = true ' ;
		else if(selStatus == 'Draft')
			sql += ' AND Agreement__r.isAvailable__c = false ' ;
		else if(selStatus == 'Expired')
			sql += ' AND Agreement__r.isAvailable__c = true AND Agreement__r.End_Date__c < TODAY AND Agreement__r.Until_Terminated__c = false ' ;
		else if (selStatus == 'Cancelled')
			sql += ' AND Agreement__r.isCancelled__c = true';


		if(validFrom != null && validFrom != '')
			sql += ' AND Agreement__r.Start_Date__c  >= '+ validFrom; //Valid From'
			
		if(selStatus!= 'Cancelled' && selStatus!= 'all')
			sql += ' AND Agreement__r.isCancelled__c = false';

		sql += ' order by Agreement__r.Agreement_Name__c ';

		system.debug('sql===>' + sql);

		Boolean showComm = false;

		map<String,commissionDetails> aSchoolComm = new map<String, commissionDetails>();
		map<String,list<commissionDetails>> aOtherComm = new map<String, list<commissionDetails>>();

		if(schId != 'all' && schId != null){ // Show School Commissions
			for(Agreement_Accounts__c aa : Database.query(sql)){
				allAgree.add(aa.Agreement__c);
				list<commissionDetails> otherC = new list<commissionDetails>();
				aSchoolComm.put(aa.Agreement__c, new commissionDetails());

				for(Commission__c c : aa.Commissions__r){
					if(c.School_Id__c != null)
						aSchoolComm.put(aa.Agreement__c, new commissionDetails(c.country__c, c.School_Name__c, c.Campus_Name__c, '', '', c.School_Commission__c, c.School_Commission_Type__c, c.Comments__c));
					else if(c.Campus_Id__c != null)
						otherC.add(new commissionDetails(c.country__c, c.School_Name__c, c.Campus_Name__c, '', '', c.Campus_Commission__c, c.Campus_Commission_Type__c, c.Comments__c));
					else if(c.CampusCourseTypeId__c != null)
						otherC.add(new commissionDetails(c.country__c, c.School_Name__c, c.Campus_Name__c, c.Course_Type_Control__c, '', c.Course_Type_Commission__c, c.Course_Type_Commission_Type__c, c.Comments__c));
					else if(c.Campus_Course__c != null)
						otherC.add(new commissionDetails(c.country__c, c.School_Name__c, c.Campus_Name__c, c.Course_Type_Control__c, c.Course_Name__c, c.Course_Commission__c, c.Course_Commission_Type__c, c.Comments__c));
				}//end for commissions

				aOtherComm.put(aa.Agreement__c, otherC);
				showComm = true;
			}//end for
		
		}
		else{
			for(Agreement_Accounts__c aa : Database.query(sql))
				allAgree.add(aa.Agreement__c);
		}

		
		return findAgreements(sqlOffset, pageLimit, allAgree, showComm, aSchoolComm, aOtherComm);
	}


	private static String findAgreements(Integer sqlOffset, Integer pageLimit, set<Id> allAgree, Boolean showComm, map<String,commissionDetails> aSchoolComm, map<String,list<commissionDetails>> aOtherComm){
		String sql;

		// Decimal pageCount = decimal.valueOf((allAgree.size()/20)).round(system.RoundingMode.CEILING); // 20 items per page
		Decimal pageCount = allAgree.size(); 
		//Find Agreements
		sql = 'SELECT Id, Signed_by_Agency__r.Name, Signed_by_User__c, Start_Date__c, Agreement_Type__c, Agency_Regions__c, Agency_Exc_Countries__c, End_Date__c, Status__c, Provider_Type__c, Until_Terminated__c, isAvailable__c, isCancelled__c, Expire_in_days__c, Preview_Link__c, Agreement_Name__c, Status_Formula__c, Signed_Date__c, School_not_Sign__c, Renew_in_progress__c, Renew_upon_request__c, Expired_Notes__c ';
		
		sql += ', (SELECT Account__r.Name, Account__r.RecordType.Name, Account__r.Parent.Name FROM Agreement_Accounts__r order by Account__r.Name) ';
		sql += ' FROM Agreement__c WHERE Id in :allAgree order by Agreement_Name__c limit '+ pageLimit +' offset ' + sqlOffset;

		system.debug('sql agreement ===>' + sql);
		
		// String agStatus;
		list<agWrapper> result = new list<agWrapper>();
		list<s3Docs> aDocs;
		for(Agreement__c a : Database.query(sql)){

			list<Agreement_Accounts__c> agencies = new list<Agreement_Accounts__c>();
			list<Agreement_Accounts__c> schools = new list<Agreement_Accounts__c>();

			//Agreements Accounts
			for(Agreement_Accounts__c aa : a.Agreement_Accounts__r)
				if(aa.Account__r.RecordType.Name == 'Agency')
					agencies.add(aa);
				else
					schools.add(aa);
			//

			aDocs = new list<s3Docs>();	
			//Agreement Documents
			if(a.Preview_Link__c!=null){
				for(String doc : a.Preview_Link__c.split(FileUpload.SEPARATOR_FILE))
					aDocs.add(new S3Docs(doc.split(FileUpload.SEPARATOR_URL)));
			}

			if(showComm) // Show School Commissions
				result.add(new agWrapper(pageCount, a, schools, agencies, aSchoolComm.get(a.Id), aOtherComm.get(a.Id), aDocs));
			else
				result.add(new agWrapper(pageCount, a, schools, agencies, null, null, aDocs));
		}//end for

		return JSON.serialize(result);
	}


	/********************** INNER CLASSES **********************/
	
	private class agWrapper{
		private Decimal totItems {get;set;}
		private Agreement__c agreement {get;set;}
		private list<Agreement_Accounts__c> schools {get;set;}
		private list<Agreement_Accounts__c> agencies {get;set;}
		// private String status {get;set;}
		private commissionDetails schoolComm {get;set;}
		private List<commissionDetails> otherComm {get;set;}
		private list<s3Docs> docs {get;set;}
		
		private agWrapper(Decimal totItems, Agreement__c agreement, list<Agreement_Accounts__c> schools, list<Agreement_Accounts__c> agencies, commissionDetails schoolComm, List<commissionDetails> otherComm, list<s3Docs> docs){
			this.totItems = totItems;	
			this.agreement = agreement;	
			this.schools = schools;
			this.agencies = agencies;	
			// this.status = status;	
			this.schoolComm = schoolComm;	
			this.otherComm = otherComm;	
			this.docs = docs;	
		}
	}

	public class commissionDetails{
		public string country{get; set;}
		public string school{get; set;}
		public string campus{get; set;}
		public string courseType{get; set;}
		public string course{get; set;}
		public decimal cValue{get; set;}
		// public decimal hifyCommission{get; set;}
		public decimal cType{get; set;}
		public string cComments{get; set;}

		public commissionDetails(){
			cValue = 0;
			cType = 0;
		}

		public commissionDetails(string country, string school, string campus, string courseType, string course, decimal cValue, decimal cType, string cComments){ //, decimal agCommission
			this.country = country;
			this.school = school;
			this.campus = campus;
			this.courseType = courseType;
			this.course = course;
			this.cValue = cValue;
			this.cType = cType;
			this.cComments = cComments;
			// this.hifyCommission = (value*agCommission)/100;
		}
	}

	/********************** FILTERS **********************/

	private void findCountries(){

		List<String> result = new list<String>{'All'};
		for(AggregateResult ar : [SELECT Account__r.BillingCountry ct FROM Agreement_Accounts__c WHERE Account__r.RecordType.Name = 'School' group by Account__r.BillingCountry order by Account__r.BillingCountry])
			result.add((String) ar.get('ct'));

		allCountries = JSON.serialize(result);
	}

	private void findAllSchools(){
		list<opWrapper> result = new list<opWrapper>();
		for(AggregateResult ar : [SELECT Account__c aId, Account__r.Name aName FROM Agreement_Accounts__c WHERE Account__r.RecordType.Name = 'School' group by Account__c, Account__r.Name  order by Account__r.Name])
			result.add(new opWrapper((String)ar.get('aId'), (String)ar.get('aName')));

		if(result.size()>0)
			result.add(0,new opWrapper('all', 'All Schools'));

		allSchools = JSON.serialize(result);
	}

	@RemoteAction
	public static String searchSchools(String country, String courseType, String instType){
		String sql = 'SELECT Account__c aId, Account__r.Name aName FROM Agreement_Accounts__c WHERE Account__r.RecordType.Name = \'School\' ';

		if(country != 'all')
			sql += ' and Account__r.BillingCountry = \''+ country +'\'';

		if(courseType != 'all')
			sql += ' and Account__r.Course_Types_Offered__c Includes ( \''+ courseType + '\' ) ';

		if(instType != 'all')
			sql += ' and Account__r.Type_of_Institution__c = :instType ';

		sql += ' group by Account__c, Account__r.Name order by Account__r.Name';

		list<opWrapper> result = new list<opWrapper>();
		for(AggregateResult ar : Database.query(sql)){
			result.add(new opWrapper((String)ar.get('aId'), (String)ar.get('aName')));
		}//end for	

		if(result.size()>0)
			result.add(0,new opWrapper('all', 'All Schools'));
		

		return JSON.serialize(result);
	}

	@RemoteAction
	public static String searchGroups(){
		String sql = 'SELECT Account__r.ParentId apId, Account__r.Parent.Name apName FROM Agreement_Accounts__c WHERE Account__r.RecordType.Name = \'Agency\' group by Account__r.ParentId, Account__r.Parent.Name order by Account__r.Parent.Name';

		list<opWrapper> result = new list<opWrapper>();
		for(AggregateResult ar : Database.query(sql))
			result.add(new opWrapper((string) ar.get('apId'), (String) ar.get('apName')));

		if(result.size()>0)
			result.add(0,new opWrapper('all', 'All Groups'));

		return JSON.serialize(result);
	}

	@RemoteAction
	public static String searchAgencies(String parentId){
		String sql = 'SELECT Account__c aId, Account__r.Name aName FROM Agreement_Accounts__c WHERE Account__r.ParentId = :parentId AND Account__r.RecordType.Name = \'Agency\' group by Account__c, Account__r.Name order by Account__r.Name';

		list<opWrapper> result = new list<opWrapper>();
		for(AggregateResult ar : Database.query(sql))
			result.add(new opWrapper((string) ar.get('aId'), (String) ar.get('aName')));

		if(result.size()>0)
			result.add(0,new opWrapper('all', 'All Agencies'));

		return JSON.serialize(result);
	}

	private void agreementStatus(){

		Schema.DescribeFieldResult fieldResult = Agreement__c.Status__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

		list<opWrapper> result = new list<opWrapper>();

		for( Schema.PicklistEntry f : ple)
			result.add(new opWrapper(f.getValue(), f.getLabel()));

		result.add(0, new opWrapper('all', 'All'));

		allStatus = JSON.serialize(result);

	}

	private void courseTypes(){

		Schema.DescribeFieldResult fieldResult = Account.Course_Types_Offered__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

		list<opWrapper> result = new list<opWrapper>();

		for( Schema.PicklistEntry f : ple)
			result.add(new opWrapper(f.getValue(), f.getLabel()));

		result.add(0, new opWrapper('all', 'All'));

		allCourseTypes = JSON.serialize(result);

	}

	private void institutionType(){

		Schema.DescribeFieldResult fieldResult = Account.Type_of_Institution__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

		list<opWrapper> result = new list<opWrapper>();

		for( Schema.PicklistEntry f : ple)
			result.add(new opWrapper(f.getValue(), f.getLabel()));

		result.add(0, new opWrapper('all', 'All'));

		allInstType = JSON.serialize(result);

	}

	private class opWrapper{
		public String opValue {get;set;}
		public String opLabel {get;set;}

		public opWrapper(String opValue, String opLabel){
			this.opValue = opValue;
			this.opLabel = opLabel;
		}
	}

	private class s3Docs{
		private String docName {get;set;}
		private String docUrl {get;set;}

		private s3Docs (List<String> docParts){
			this.docUrl = docParts[0];
			this.docName = docParts[1];
		}
	}


}