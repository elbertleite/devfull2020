global class batch_createPDFandSendEmail implements Database.batchable<Invoice__c>, Database.AllowsCallouts {
		
	List<Invoice__c> invoices;
	list<client_course_instalment__c> invInstalments;

	map<Id, Invoice__c> mInvoices;
	map<Id, mandrillSendEmail.messageJson> mInvoiceEmails;
	map<Id, String> mActivities;
	
	global batch_createPDFandSendEmail(List<Invoice__c> pinvoices){
		invoices = pinvoices;
	}

	global Iterable<Invoice__c> start(Database.BatchableContext info){ 
       return invoices; 
   	}    

	global void execute(Database.BatchableContext BC, List<Invoice__c> invoices){

		mandrillSendEmail mse = new mandrillSendEmail();

		mInvoices = new map<Id, Invoice__c>();
		mInvoiceEmails = new map<Id, mandrillSendEmail.messageJson>();
		mActivities = new map<Id, String>();
		invInstalments = new list<client_course_instalment__c>();

		for(Invoice__c invoice : invoices){

			mandrillSendEmail.messageJson email_md = new mandrillSendEmail.messageJson();

			String[] toAd;

			if(invoice.Sent_Email_To__c.contains(';')){
				toAd = invoice.Sent_Email_To__c.split(';', 0);
				for(String t : toAd)
					if(!String.isBlank(t))
						email_md.setTo(t, '');
			}
			else if(invoice.Sent_Email_To__c.contains(',')){
				toAd = invoice.Sent_Email_To__c.split(',', 0);
				for(String t : toAd)
					if(!String.isBlank(t))
						email_md.setTo(t, '');
			}else{
				email_md.setTo(invoice.Sent_Email_To__c, '');
			}

			email_md.setCc(invoice.createdBy.Email, '');
			email_md.setFromEmail(invoice.createdBy.Email);
			email_md.setFromName(invoice.createdBy.Name);

			String subject = 'Request Commission Confirmation Reminder - ' + invoice.School_Invoice_Number__c;
			email_md.setSubject(subject);
			email_md.setHtml(Label.Commission_Invoice); 

			//Maps
			mInvoiceEmails.put(invoice.Id, email_md);
			mInvoices.put(invoice.Id, invoice);
			mActivities.put(invoice.Id, 'Email - Commission Invoice Reminder;;' + invoice.Sent_Email_To__c+ ' ;;' + subject +';;' + DateTime.now().format('yyyy-MM-dd HH:mm:ss') + ';;sent;;-;;scheduled action!#!');

			//Update invoice details
			invoice.Invoice_Activities__c += mActivities.get(invoice.Id);
			invoice.Total_Emails_Sent__c = invoice.Total_Emails_Sent__c + 1;
			invoice.Sent_On__c = system.now();


			//Attach invoice
			PageReference pr = Page.commissions_school_commission_invoice;
			pr.getParameters().put('i', invoice.Id);
			blob pdfFile;
			if(Test.isRunningTest()) 
	  			pdfFile = blob.valueOf('Unit.Test');
			else {
				pdfFile = pr.getContentAsPDF();
				email_md.setAttachment('application/pdf', invoice.School_Invoice_Number__c + ' - Commission Invoice.pdf', pdfFile);

				HttpRequest req = new HttpRequest();
				req.setEndpoint('https://mandrillapp.com/api/1.0/messages/send.json');
				req.setMethod('POST');
				mandrillSendEmail.jsonStringDetails js = new mandrillSendEmail.jsonStringDetails();

				js.message = email_md;
				req.setBody(JSON.Serialize(js));
				Http h = new Http();
				HttpResponse res = h.send(req);

				//check response
				String status = res.getStatus();
				system.debug('email status ==' + status);
			}
		}
		
		if(mInvoices.size()>0){
			//Update Invoice instalments
			invInstalments = [SELECT Id, instalment_activities__c, Request_Commission_Invoice__c FROM client_course_instalment__c WHERE Request_Commission_Invoice__c IN :mInvoices.keySet()];

			//Update Instalments
			for(client_course_instalment__c cci : invInstalments)
				cci.instalment_activities__c += mActivities.get(cci.Request_Commission_Invoice__c);
		}
    	
		HttpRequest req = new HttpRequest();
		req.setEndpoint('https://mandrillapp.com/api/1.0/messages/send.json');
		req.setMethod('POST');
		
		mandrillSendEmail.jsonStringDetails js = new mandrillSendEmail.jsonStringDetails();
		list<Invoice__c> upInvoices = new list<Invoice__c>();

		
		update invoices;
		update invInstalments;
		
	}//end execute
	
	
	 global void finish(Database.BatchableContext BC)
     {

     }
}