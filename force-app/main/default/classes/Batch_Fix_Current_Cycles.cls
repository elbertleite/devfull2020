public with sharing class Batch_Fix_Current_Cycles{
	public Batch_Fix_Current_Cycles(){
		system.debug('');
	}
}
/*global with sharing class Batch_Fix_Current_Cycles implements Database.Batchable<sObject>, Database.Stateful{
    global Batch_Fix_Current_Cycles() {}

    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT ID FROM Contact WHERE RecordType.Name IN (\'Lead\',\'Client\')';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Contact> ctts) {
        Set<String> ids = new Set<String>();
        for(Contact ctt : ctts){
            ids.add(ctt.ID);
        }

        Map<String, Map<String, List<Destination_Tracking__c>>> cyclesPerContact = new Map<String, Map<String, List<Destination_Tracking__c>>>();

        for(Destination_Tracking__c cycle : [SELECT Client__c, Client__r.Name, Client__r.Original_Agency__r.Parent.ID, Client__r.Current_Agency__r.Parent.ID, Client__r.Account.Parent.ID, Name, Agency_Group__c, CreatedBy.Contact.Account.Parent.ID, Current_Cycle__c, CreatedDate FROM Destination_Tracking__c WHERE Client__c IN :ids ORDER BY Client__c, CreatedDate]){
			if(String.isEmpty(cycle.Agency_Group__c)){
				if(!String.isEmpty(cycle.CreatedBy.Contact.Account.Parent.ID)){
					cycle.Agency_Group__c = cycle.CreatedBy.Contact.Account.Parent.ID;
				}else{
                    if(!String.isEmpty(cycle.Client__r.Original_Agency__r.Parent.ID)){
                        cycle.Agency_Group__c = cycle.Client__r.Original_Agency__r.Parent.ID;
                    }else if(!String.isEmpty(cycle.Client__r.Account.Parent.ID)){
					    cycle.Agency_Group__c = cycle.Client__r.Account.Parent.ID;
                    }else{
					    cycle.Agency_Group__c = cycle.Client__r.Current_Agency__r.Parent.ID;
                    }
				}
			}

			if(!cyclesPerContact.containsKey(cycle.Client__c)){
				cyclesPerContact.put(cycle.Client__c, new Map<String, List<Destination_Tracking__c>>());
			}

			if(!cyclesPerContact.get(cycle.Client__c).containsKey(cycle.Agency_Group__c)){
				cyclesPerContact.get(cycle.Client__c).put(cycle.Agency_Group__c, new List<Destination_Tracking__c>());
			}

			cyclesPerContact.get(cycle.Client__c).get(cycle.Agency_Group__c).add(cycle);
		} 

		List<Destination_Tracking__c> toUpdate = new List<Destination_Tracking__c>(); 
		Destination_Tracking__c cycle;
		Integer listSize;
		for(String contact : cyclesPerContact.keySet()){
			for(String idGroup : cyclesPerContact.get(contact).keySet()){
				listSize = cyclesPerContact.get(contact).get(idGroup).size();
				for(Integer i = 0; i < listSize; i++){
					cycle = cyclesPerContact.get(contact).get(idGroup).get(i);
					cycle.Current_Cycle__c = false;
					if(i + 1 == listSize){
						cycle.Current_Cycle__c = true;
					}		
					toUpdate.add(cycle);
				}
			}
		}
	
        update toUpdate;
    }

    global void finish(Database.BatchableContext BC) {}
}*/