public with sharing class extClientQuotationEmail{
	
	public extClientQuotationEmail(ApexPages.StandardController std){
        if(std.getRecord().id != null)
            quote = [Select Client__r.Email, Client__r.Name, Expiry_date__c from Quotation__c where id = :std.getRecord().id];    
        
    }
	
	public Quotation__c quote{get;set;}

}