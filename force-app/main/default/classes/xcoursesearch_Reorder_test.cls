/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class xcoursesearch_Reorder_test {

    static testMethod void myUnitTest() {
    	
    	TestFactory testFactory = new TestFactory();
    	
    	Account agency = TestFactory.createAgency();
		
		Contact employee = testFactory.createEmployee(agency);
        
		Account school = testFactory.createSchool();
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;
		insert p3;
		
		
		Nationality_Group__c ng = new Nationality_Group__c();
		ng.Account__c = school.id;
		ng.Country__c = 'Brazil';
		ng.Name = 'Latin America';
		insert ng;
       
		Account campus = testFactory.createCampus(school, agency);
		
		Course__c course = new Course__c();
		course.Name = 'Certificate III in Business';        
		course.Type__c = 'Business';
		course.Sub_Type__c = 'Business';
		course.Course_Qualification__c = 'Certificate III';
		course.Course_Type__c = 'English/ELICOS';
		course.Course_Unit_Type__c = 'Week';
		course.Only_Sold_in_Blocks__c = true;
		course.Period__c = 'Morning';
		course.Minimum_length__c = 1;
		course.Maximum_length__c = 52;
		course.School__c = school.id;
		insert course;
		
		Course__c course2 = new Course__c();
		course2.Name = 'Certificate I in Business';        
		course2.Type__c = 'Business';
		course2.Sub_Type__c = 'Business';
		course2.Course_Qualification__c = 'Certificate III';
		course2.Course_Type__c = 'English/ELICOS';
		course2.Course_Unit_Type__c = 'Week';
		course2.Only_Sold_in_Blocks__c = false;
		course2.Period__c = 'Evening';
		course2.Minimum_length__c = 1;
		course2.Maximum_length__c = 52;
		course2.School__c = school.id;
		insert course2;
       
		Campus_Course__c cc = new Campus_Course__c();
		cc.Course__c = course.Id;
		cc.Campus__c = campus.Id;        
		cc.Is_Available__c = true;
		cc.is_Selected__c = true;      
		insert cc;
		
		Campus_Course__c cc2 = new Campus_Course__c();
		cc2.Course__c = course2.Id;
		cc2.Campus__c = campus.Id;        
		cc2.Is_Available__c = true;
		cc2.is_Selected__c = true;      
		insert cc2;	
        
        Web_Search__c ws = new Web_Search__c();
		ws.Email__c = 'iptest@ip.com';
		ws.Country__c = 'Australia';
		insert ws;
		
		Search_Courses__c sc = new Search_Courses__c();
		sc.Campus_Course__c = cc.id;
		sc.Web_Search__c = ws.id;
		sc.Number_of_Units__c = 10;
		insert sc;
		
		Search_Courses__c sc2 = new Search_Courses__c();
		sc2.Campus_Course__c = cc2.id;
		sc2.Web_Search__c = ws.id;
		sc2.Number_of_Units__c = 10;
		insert sc2;
		
		ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(ws);
		xcoursesearch_Reorder reorder = new xcoursesearch_Reorder(controller);
		
        ApexPages.currentPage().getParameters().put('listtoOrder', sc.id + ',' + sc2.id);
        reorder.saveReorderedCourses();
        reorder.getcoursesOrdered();
        
        
    }
}