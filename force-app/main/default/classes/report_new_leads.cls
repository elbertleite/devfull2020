public with sharing class report_new_leads {

    public List<String> agencySelected{get;set;}
	public String groupSelected{get;set;}
	//public String agencySelected{get;set;}
	public String endDate{get;set;}
	public String beginDate{get;set;}

    public List<ReportResult> reportResult{get;set;}

    public List<SelectOption> agencyOptions{get;set;}
    public boolean startingPage{get;set;}

    public Map<String, String> groupAgencies{get;set;}

    public User user{get;set;}
	public boolean canExportToExcel{get;set;}

    public String graph{get;set;}

    public List<List<ContactReport>> listOfListContacts{get;set;}

    public String invalidDates{get;set;}

    public report_new_leads() {
        try{

			user = [Select Id, Name, Contact.Export_Report_Permission__c, Contact.AccountId, Contact.Name, Contact.Account.Id, Contact.Account.Name, Contact.Account.Parent.RDStation_Campaigns__c, Contact.Account.Parent.Name, Contact.Account.Parent.Id, Aditional_Agency_Managment__c, Contact.Group_View_Permission__c, Contact.Agency_View_Permission__c, Contact.Account.Global_Link__c from User where Id = :UserInfo.getUserId()];
			
			canExportToExcel = user.Contact.Export_Report_Permission__c;

			if(!String.isEmpty(ApexPages.currentPage().getParameters().get('export')) && Boolean.valueOf(ApexPages.currentPage().getParameters().get('export'))){
				beginDate = ApexPages.currentPage().getParameters().get('beginDate'); 
				endDate = ApexPages.currentPage().getParameters().get('endDate'); 
				groupSelected = ApexPages.currentPage().getParameters().get('groupSelected'); 
				//agencySelected = ApexPages.currentPage().getParameters().get('agencySelected'); 
				agencySelected = (List<String>) JSON.deserialize(ApexPages.currentPage().getParameters().get('agencySelected'), List<String>.class); 
				this.agencyOptions = loadAgencies(user.Contact.AccountId, groupSelected);
                generateExcel();
			}else{
				startingPage = true;
				endDate = Date.today().format();
				beginDate = Date.today().addMonths(-1).format();
				agencySelected = new String [] {user.Contact.Account.Id};
				//agencySelected = user.Contact.Account.Id;
				groupSelected = user.Contact.Account.ParentId;
				this.agencyOptions = loadAgencies(user.Contact.AccountId, user.Contact.Account.Parent.Id);
				generateReport();
			
			}
		}catch(Exception e){
			system.debug('Error on report_not_assigned_new_leads() ===> ' + e.getLineNumber() + ' '+e.getMessage());	
		}
    }

    public void generateExcel(){
        //system.debug('THE GROUP THAT CAME FROM THE URL '+groupSelected);
        Set<String> idAgencies = groupAgencies.keySet();
        //Date fromDate = String.isEmpty(beginDate) ? null : Date.parse(beginDate);
		//Date toDate = String.isEmpty(endDate) ? null : Date.parse(endDate);
        Date fromDate = IPFunctions.getDateFromString(beginDate);
        Date toDate = IPFunctions.getDateFromString(endDate);
        Set<String> idContacts = new Set<String>();
        List<Contact> contacts = new MethodsWithoutSharing().queryContact(groupSelected, agencySelected, fromDate, toDate, idAgencies);
        //system.debug('THE CONTACTS RETURNED '+JSON.serialize(contacts));
        ContactReport reportCtt;
        List<ContactReport> ctts = new List<ContactReport>();
        if(Test.isRunningTest()){
            Contact contact = new Contact();
            contact.firstName = 'test';
            contact.lastName = 'test';
            contact.email = 'test@reportnewleads4654s.com';
            insert contact;
            contacts.add(contact);
        }
        for(Contact ctt : contacts){
            idContacts.add(ctt.ID);
            reportCtt = new ContactReport();
            reportCtt.id = ctt.ID; 
            reportCtt.name = ctt.Name;
            reportCtt.email = ctt.email;
            reportCtt.newCycle = ctt.CreatedBy.UserType != 'CspLitePortal' ? 'New Lead Online' : 'New Lead Offline';
            reportCtt.createdByName = ctt.CreatedBy.UserType != 'CspLitePortal' ? 'Web Lead' : ctt.CreatedBy.Name; 
            //reportCtt.createdByName = ctt.CreatedBy.Name; 
            if(!Test.isRunningTest()){
                reportCtt.createdDate = ctt.CreatedDate.format('dd/MM/yyyy');
                reportCtt.originalAgency = ctt.Original_Agency__r.Name;
                reportCtt.currentAgency = ctt.Current_Agency__r.Name;
                reportCtt.owner = ctt.Owner__r.Name;
            }
            if(ctt.Contact_Origin__c == 'RDStation'){
                reportCtt.contactOrigin = ctt.Contact_Origin__c;

                if(!String.isEmpty(ctt.RDStation_Current_Campaign__c)){
                    reportCtt.rdStationCampaign = ctt.RDStation_Current_Campaign__c;
                }else if(!String.isEmpty(ctt.RDStation_Last_Conversion__c)){
                    reportCtt.rdStationCampaign = ctt.RDStation_Last_Conversion__c;
                }
            }
            ctts.add(reportCtt); 
        }
       
        List<Destination_Tracking__c> cycles = new MethodsWithoutSharing().queryCycles(groupSelected, agencySelected, fromDate, toDate, idAgencies, idContacts);
        //system.debug('THE CYCLES RETURNED '+JSON.serialize(cycles));
        if(Test.isRunningTest()){
            Contact contact = new Contact();
            contact.firstName = 'test';
            contact.lastName = 'test';
            contact.email = 'test@reportnewleads16523.com';
            insert contact;

            Destination_Tracking__c dt = new Destination_tracking__c();
            dt.Client__c = contact.id;
            dt.Arrival_Date__c = system.today().addMonths(3);
            dt.Destination_Country__c = 'Australia';
            dt.Destination_City__c = 'Sydney';
            dt.Expected_Travel_Date__c = system.today().addMonths(3);
            dt.lead_Cycle__c = false;
            insert dt;

            cycles.add(dt);
        }

        Set<String> contactsIdsPerCycle = new Set<String>();
        Integer totalCycles = 0;
        for(Destination_Tracking__c cycle : cycles){
            if(!idContacts.contains(cycle.Client__c)){
                if(!contactsIdsPerCycle.contains(cycle.Client__c)){
                    reportCtt = new ContactReport();
                    reportCtt.id = cycle.Client__c; 
                    reportCtt.name = cycle.Client__r.Name;
                    reportCtt.email = cycle.Client__r.email;
                    reportCtt.newCycle = 'New Cycle';
                    reportCtt.createdByName = cycle.Client__r.CreatedBy.UserType != 'CspLitePortal' ? 'Web Lead' : cycle.Client__r.CreatedBy.Name; 
                    reportCtt.originalAgency = cycle.Client__r.Original_Agency__r.Name;
                    reportCtt.currentAgency = cycle.Client__r.Current_Agency__r.Name;
                    reportCtt.owner = cycle.Client__r.Owner__r.Name;
                    if(!Test.isRunningTest()){
                        reportCtt.createdDate = cycle.Client__r.CreatedDate.format('dd/MM/yyyy');
                    }
                    
                    if(cycle.Client__r.Contact_Origin__c == 'RDStation'){
                        reportCtt.contactOrigin = cycle.Client__r.Contact_Origin__c;

                        if(!String.isEmpty(cycle.RDStation_Campaigns__c)){
                            reportCtt.rdStationCampaign = cycle.RDStation_Campaigns__c;
                        }else if(!String.isEmpty(cycle.Client__r.RDStation_Current_Campaign__c)){
                            reportCtt.rdStationCampaign = cycle.Client__r.RDStation_Current_Campaign__c;
                        }else if(!String.isEmpty(cycle.RDStation_First_Conversion__c)){
                            reportCtt.rdStationCampaign = cycle.RDStation_First_Conversion__c;
                        }else if(!String.isEmpty(cycle.Client__r.RDStation_Last_Conversion__c)){
                            reportCtt.rdStationCampaign = cycle.Client__r.RDStation_Last_Conversion__c;
                        }
                    }                
                    ctts.add(reportCtt);
                    totalCycles += 1;
                    //idContacts.add(cycle.Client__c); 
                    contactsIdsPerCycle.add(cycle.Client__c);    
                }
           }
        }
        //system.debug('TOTAL CYCLES '+totalCycles);
        Map<String, Destination_Tracking__c> cyclesPerContact = new Map<String, Destination_Tracking__c>();
        for(Destination_Tracking__c cycle : [SELECT CreatedDate, Client__c, Created_By_Agency__r.Name, Lead_Last_Stage_Cycle__c, Lead_Last_Status_Cycle__c FROM Destination_Tracking__c WHERE Agency_Group__c = :groupSelected AND Current_Cycle__c = true ORDER BY Client__c, CreatedDate]){
            cyclesPerContact.put(cycle.Client__c, cycle);
        } 

        Destination_Tracking__c cycle;
        if(!Test.isRunningTest()){
            for(ContactReport cr : ctts){
                cycle = cyclesPerContact.get(cr.ID);
                if(cycle != null){
                    cr.cycleCreatedDate = cycle.CreatedDate.format('dd/MM/yyyy');
                    cr.cycleCreatedBy = cycle.Created_By_Agency__r.Name;
                    cr.cycleCurrentStage = cycle.Lead_Last_Stage_Cycle__c;
                    cr.cycleCurrentStatus = cycle.Lead_Last_Status_Cycle__c;
                }
            }
        }

        Integer maxItens = 10000;
        listOfListContacts = new List<List<ContactReport>>();
        List<ContactReport> contactsResponse;
        Long totalItens = ctts.size();
        Long numberOfLists;
        //system.debug('THE TOTAL ITENS '+totalItens);
        if(totalItens <= maxItens){
            numberOfLists = 1;
        }else{
            numberOfLists = totalItens / maxItens;
            if(math.mod(totalItens, maxItens) > 1){
				numberOfLists += 1;
			}
        }
        Integer currentIndex = 0;
        for(Integer i = 0; i < numberOfLists; i++){
            contactsResponse = new List<ContactReport>();
            for(Integer x = 0; x < maxItens; x++){
                if(currentIndex < totalItens){
                    contactsResponse.add(ctts.get(currentIndex));
                    currentIndex += 1;
                }else{
                    break;
                }
            }
            listOfListContacts.add(contactsResponse);
        }
        ctts = null;
        //system.debug('THE RESPONSE '+JSON.serialize(listOfListContacts));
    }

    public void generateReport(){
        invalidDates = '';
        Date fromDate = null;
        Date toDate = null;
        reportResult = null;
        fromDate = IPFunctions.getDateFromString(beginDate);
        toDate = IPFunctions.getDateFromString(endDate);
        if(fromDate != null && toDate != null){
            graph = '';
            if(fromDate <= toDate){
                Map<String, ReportResult> mapReportResult = generateMonthsYears(fromDate, toDate);
                Set<String> idAgencies = groupAgencies.keySet();
                List<Contact> contacts = new MethodsWithoutSharing().queryContact(groupSelected, agencySelected, fromDate, toDate, idAgencies);
                if(Test.isRunningTest()){
                    Contact contact = new Contact();
                    contact.firstName = 'test';
                    contact.lastName = 'test';
                    contact.email = 'test@reportnewleads2.com';
                    insert contact;
                    contacts.add(contact);
                }
                ReportResult result;
                Set<String> idContacts = new Set<String>();
                Date createDate;
                //Integer index;
                String monthYear;
                if(contacts != null && !contacts.isEmpty()){
                    
                    for(Contact ctt : contacts){
                        idContacts.add(ctt.ID);
                        if(Test.isRunningTest()){
                            createDate = Date.today();
                        }else{
                            createDate = ctt.CreatedDate.date();
                        }
                        monthYear = IPFunctions.getMonthName(createDate.month()) + '/' + createDate.year();
                        result = mapReportResult.get(monthYear);
                        if(result != null){
                            if(ctt.CreatedBy.UserType != 'CspLitePortal'){
                                result.newLeadWeb += 1;
                            }else{
                                result.newLeadOffline += 1;
                            }
                            result.total += 1;
                        }
                    }
                }
                Set<String> contactsIdsPerCycle = new Set<String>();
                Set<String> cyclesSaved = new Set<String>();
                List<Destination_Tracking__c> cycles = new MethodsWithoutSharing().queryCycles(groupSelected, agencySelected, fromDate, toDate, idAgencies, idContacts);
                
                if(Test.isRunningTest()){
                    Contact contact = new Contact();
                    contact.firstName = 'test';
                    contact.lastName = 'test';
                    contact.email = 'test@reportnewleads123.com';
                    insert contact;

                    Destination_Tracking__c dt = new Destination_tracking__c();
                    dt.Client__c = contact.id;
                    dt.Arrival_Date__c = system.today().addMonths(3);
                    dt.Destination_Country__c = 'Australia';
                    dt.Destination_City__c = 'Sydney';
                    dt.Expected_Travel_Date__c = system.today().addMonths(3);
                    dt.lead_Cycle__c = false;
                    insert dt;

                    cycles.add(dt);
                }
                for(Destination_Tracking__c cycle : cycles){
                    if(!idContacts.contains(cycle.Client__c)){
                        if(!contactsIdsPerCycle.contains(cycle.Client__c)){
                            cyclesSaved.ADD(cycle.ID);
                            createDate = cycle.CreatedDate.date(); 
                            
                            monthYear = IPFunctions.getMonthName(createDate.month()) + '/' + createDate.year();
                            result = mapReportResult.get(monthYear);
                            if(result != null){
                                result.oldLeadNewCycle += 1;
                                result.total += 1;
                            }
                        }
                        contactsIdsPerCycle.add(cycle.Client__c);
                    }
                }
                //system.debug('THE CYCLES RETURNED '+JSON.serialize(cyclesSaved));
                reportResult = mapReportResult.values();
                graph = JSON.serialize(reportResult);
                system.debug('THE RESULT '+graph);
            }else{
                invalidDates = 'The From Date must be before or equal the To Date.';
            } 
        }else{
            invalidDates = 'Both dates are required to generate the graph.';
        } 
    }

    public String getAgencySelectedString(){
        return JSON.serialize(agencySelected);
    }

    public Map<String, ReportResult> generateMonthsYears(Date fromDate, Date toDate){
        system.debug('THE FROM DATE '+fromDate);
        system.debug('THE TO DATE '+toDate);
        Date fromDateToGenerate = fromDate;
        Date toDateToGenerate = toDate;
        Integer initialMonth = fromDateToGenerate.month(); 
        Integer initialYear = fromDateToGenerate.year();
        Integer finalMonth = toDateToGenerate.month(); 
        Integer finalYear = toDateToGenerate.year();
    
        Map<String, ReportResult> reportResult = new Map<String, ReportResult>();
        ReportResult result;
        boolean finishLoop = false;

        if(fromDateToGenerate.month() == toDateToGenerate.month() && fromDateToGenerate.year() == toDateToGenerate.year()){
            result = new ReportResult();
            result.monthYear = IPFunctions.getMonthName(toDateToGenerate.month()) + '/' + toDateToGenerate.year();
            reportResult.put(result.monthYear, result);
        }else{
            while(!finishLoop){
                result = new ReportResult();
                result.monthYear = IPFunctions.getMonthName(fromDateToGenerate.month()) + '/' + fromDateToGenerate.year();
                fromDateToGenerate = fromDateToGenerate.addMonths(1);
                
                reportResult.put(result.monthYear, result);

                finishLoop = fromDateToGenerate.month() == toDateToGenerate.month() && fromDateToGenerate.year() == toDateToGenerate.year();

                if(finishLoop){
                    result = new ReportResult();
                    result.monthYear = IPFunctions.getMonthName(toDateToGenerate.month()) + '/' + toDateToGenerate.year();
                    reportResult.put(result.monthYear, result);
                }
            }
        }
        system.debug('THE MONTHS '+JSON.serialize(reportResult));
        return reportResult;
    }

	public List<SelectOption> loadAgencies(String agencyID, String groupID){
		Set<String> idsAllAgencies = new Set<String>();
		for(SelectOption option : IPFunctions.getAgencyOptions(agencyID, groupID, Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible())){
			idsAllAgencies.add(option.getValue());
			//
		}
		List<SelectOption> options = new List<SelectOption>();
        groupAgencies = new Map<String, String>();
		for(Account option : [Select Id, Name from Account where id in :idsAllAgencies AND Inactive__c = false order by name]){
			options.add(new SelectOption(option.ID, option.Name));
			groupAgencies.put(option.ID, option.Name);
		}
		return options;
	}

    public List<SelectOption> getGroups(){
		Set<String> idsAllGroups = new Set<String>();
		for(SelectOption option : IPFunctions.getAgencyGroupsByPermission(user.Contact.AccountId, Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())){
			idsAllGroups.add(option.getValue());
			//
		}
		List<SelectOption> options = new List<SelectOption>();
		for(Account option : [Select Id, Name from Account where id in :idsAllGroups AND Inactive__c = false order by name]){
			options.add(new SelectOption(option.ID, option.Name));
		}
		
		return options;
	}

    public void updateAgencies(){
		this.groupSelected = ApexPages.currentPage().getParameters().get('idGroup');
		this.agencyOptions = loadAgencies(user.Contact.AccountId, groupSelected);
		agencySelected = new List<String>();	
	}

    public class ReportResult{
        public String monthYear{get;set;}

        public Integer newLeadOffline{get{if(newLeadOffline == null) newLeadOffline = 0; return newLeadOffline;} set;}
        public Integer newLeadWeb{get{if(newLeadWeb == null) newLeadWeb = 0; return newLeadWeb;} set;}
        public Integer oldLeadNewCycle{get{if(oldLeadNewCycle == null) oldLeadNewCycle = 0; return oldLeadNewCycle;} set;}
        public Integer total{get{if(total == null) total = 0; return total;} set;}

        public Boolean equals(Object obj) {
			if (obj instanceof ReportResult) {
				ReportResult cs = (ReportResult)obj;
				if(monthYear != null){
					return monthYear.equals(cs.monthYear);
				}
				return false;
			}
			return false;
		}
		public Integer hashCode() {
			return monthYear.hashCode();
		}
    }

    public class ContactReport{
        public String id{get;set;}
        public String name{get;set;}
        public String email{get;set;}
        public String newCycle{get;set;}
        public String createdByName{get;set;}
        public String createdDate{get;set;}
        public String cycleCreatedDate{get;set;}
        public String cycleCreatedBy{get;set;}
        public String cycleCurrentStage{get;set;}
        public String cycleCurrentStatus{get;set;}
        public String cycleCurren{get;set;}
        public String originalAgency{get;set;}
        public String currentAgency{get;set;}
        public String contactOrigin{get;set;}
        public String rdStationCampaign{get;set;}
        public String owner{get;set;}
    }

    public without sharing class MethodsWithoutSharing{
        public List<Contact> queryContact(String groupSelected, List<String> agencySelected, Date fromDate, Date toDate, Set<String> idAgencies){
            String queryContact = 'SELECT ID, NAME, CreatedBy.Name, CreatedBy.UserType, Email, Original_Agency__c, Original_Agency__r.Name, Current_Agency__r.Name, CreatedDate, Contact_Origin__c, RDStation_Current_Campaign__c, RDStation_Last_Conversion__c, Owner__r.Name FROM Contact ';
            String queryAndContact = generateQueryAndContact(groupSelected, agencySelected, fromDate, toDate, idAgencies);
            system.debug('THE QUERY TO GET CONTACTS::: '+queryContact + queryAndContact + ' ORDER BY CreatedDate DESC ');
            return Database.query(queryContact + queryAndContact + ' ORDER BY CreatedDate DESC ');
        }
        public List<Destination_Tracking__c> queryCycles(String groupSelected, List<String> agencySelected, Date fromDate, Date toDate, Set<String> idAgencies, Set<String> idContacts){
            String queryCycle = 'SELECT ID, Client__c, Client__r.CreatedBy.UserType, Client__r.Original_Agency__c, CreatedBy.Contact.Account.ID, Client__r.Email, Client__r.ID, Client__r.NAME, Client__r.CreatedBy.Name, Client__r.Original_Agency__r.Name, Client__r.CreatedDate, Client__r.Current_Agency__r.Name, CreatedDate, Client__r.Contact_Origin__c, Client__r.RDStation_Current_Campaign__c, Client__r.RDStation_Last_Conversion__c, RDStation_Campaigns__c, RDStation_First_Conversion__c, Client__r.Owner__r.Name FROM Destination_Tracking__c ';
            String queryAndContact = generateQueryAndCycle(groupSelected, agencySelected, fromDate, toDate, idAgencies, idContacts);
            system.debug('THE QUERY TO GET CYCLES::: '+queryCycle + queryAndContact + ' ORDER BY Client__r.CreatedDate DESC ');
            return Database.query(queryCycle + queryAndContact + ' ORDER BY Client__r.CreatedDate DESC ');
        }

        public String generateQueryAndContact(String groupSelected, List<String> agencySelected, Date fromDate, Date toDate, Set<String> idAgencies){
            String query = ' WHERE Original_Agency__r.Parent.ID = :groupSelected ';
            /*if(agencySelected != null && !agencySelected.isEmpty()){
                query = query + ' AND Original_Agency__c IN :agencySelected ';
            }else{
                query = query + ' AND Original_Agency__c IN :idAgencies ';
            }*/
            if(agencySelected != null && !agencySelected.isEmpty()){
                query = query + ' AND Original_Agency__c IN :agencySelected ';
            }else{
                query = query + ' AND Original_Agency__c IN :idAgencies ';
            }
            if(fromDate != null){
                query = query + ' AND DAY_ONLY(convertTimezone(CreatedDate)) >= :fromDate '; 
            }
            if(toDate != null){
                query = query + ' AND DAY_ONLY(convertTimezone(CreatedDate)) <= :toDate ';
            }
            return query;
        }

        public String generateQueryAndCycle(String groupSelected, List<String> agencySelected, Date fromDate, Date toDate, Set<String> idAgencies, Set<String> idContacts){
            String query = ' WHERE Agency_Group__c = :groupSelected ';
            if(idContacts != null && !idContacts.isEmpty()){
                query = query + ' AND Client__c NOT IN :idContacts ';
            }
            if(agencySelected != null && !agencySelected.isEmpty()){
                //query = query + ' AND Client__R.Original_Agency__c IN :agencySelected ';
                query = query + ' AND Created_By_Agency__c IN :agencySelected ';
            }else{
                //query = query + ' AND Client__R.Original_Agency__c IN :idAgencies ';
                query = query + ' AND Created_By_Agency__c IN :idAgencies ';
            }
            //query = query + ' AND Client__R.Original_Agency__c IN :agencySelected ';
            if(fromDate != null){
                query = query + ' AND DAY_ONLY(convertTimezone(CreatedDate)) >= :fromDate '; 
            }
            if(toDate != null){
                query = query + ' AND DAY_ONLY(convertTimezone(CreatedDate)) <= :toDate ';
            }
            return query;
        }
    }
}