/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class client_course_instalment_split_test {

    static testMethod void myUnitTest() {
       
       TestFactory tf = new TestFactory();
       
       Account school = tf.createSchool();
       Account agency = tf.createAgency();
       Contact emp = tf.createEmployee(agency);
       User portalUser = tf.createPortalUser(emp);
       Account campus = tf.createCampus(school, agency);
       Course__c course = tf.createCourse();
       Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
       
   	   Contact client = tf.createLead(agency, emp);
       client_course__c booking = tf.createBooking(client);
       client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
       
       List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
       
       ApexPages.currentPage().getParameters().put('itid', instalments[0].id);
       client_course_instalment_split testClass = new client_course_instalment_split(new ApexPages.StandardController(cc));
       
       testClass.newSplitedInstalment.ccInstall.Due_Date__c = system.today().addDays(-30);
       
       testClass.newSplitedInstalment.ccInstall.Tuition_Value__c =0;
       testClass.newSplitedInstalment.ccInstall.Extra_Fee_Value__c = 0;
       testClass.addInstalmentValue();
       
       
       testClass.newSplitedInstalment.ccInstall.Extra_Fee_Value__c = 680;
       testClass.newSplitedInstalment.ccInstall.Tuition_Value__c =-1;
       testClass.addInstalmentValue();
       
       testClass.newSplitedInstalment.ccInstall.Tuition_Value__c =0;
       testClass.addInstalmentValue();
       
       
       
       testClass.newSplitedInstalment.ccInstall.Tuition_Value__c =10000;
       testClass.addInstalmentValue();
       
       testClass.newSplitedInstalment.ccInstall.Due_Date__c = system.today().addDays(5);
       testClass.newSplitedInstalment.ccInstall.Tuition_Value__c =6000;
       testClass.addInstalmentValue();
       
        testClass.newSplitedInstalment.ccInstall.Due_Date__c = system.today().addDays(10);
       testClass.newSplitedInstalment.ccInstall.Tuition_Value__c =1000;
       testClass.addInstalmentValue();
       
       testClass.newSplitedInstalment.ccInstall.Due_Date__c = system.today().addDays(5);
       testClass.newSplitedInstalment.ccInstall.Tuition_Value__c =550;
       testClass.addInstalmentValue();
       
       ApexPages.currentPage().getParameters().put('index', '1');
       testClass.removeInstalmentValue();
       
       testClass.saveNewInstallments();
       
       testClass.getInfoMessage();
       
    }
}