public with sharing class instalment_payment_history {
	
	private string pId;
	public instalment_payment_history(ApexPAges.StandardController ctr){
		pId = ctr.getId();
	}
	
	
	public list<client_course_instalment_payment__History> getHistory(){
		return [Select Field, CreatedBy.Name, CreatedDate, NewValue, OldValue from client_course_instalment_payment__History where ParentId = :pId order by createddate];
	}
}