@isTest(seealldata=true)
private class batch_createPDFandSendEmail_test {
    
    static testMethod void myUnitTest() {
        
        TestFactory tf = new TestFactory();
       
        Account school = tf.createSchool();
		Account agency = tf.createAgency();		
		Contact employee = tf.createEmployee(agency);
		User portalUser = tf.createPortalUser(employee);
        Account campus = tf.createCampus(school, agency);
       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
        Department__c department = tf.createDepartment(agency);
        Contact client = tf.createLead(agency, employee);
        client_course__c booking = tf.createBooking(client);
        client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);

        Invoice__c inv = new Invoice__c(Sent_Email_To__c = 'ze@test.com;mary@test.com', Total_Emails_Sent__c = 0);
        Invoice__c inv2 = new Invoice__c(Sent_Email_To__c = 'ze@test.com,mary@test.com', Total_Emails_Sent__c = 0);
        list<Invoice__c> li = new list<Invoice__c>();
        li.add(inv);
        li.add(inv2);

        insert li;

        client_course_instalment__c cci = new client_course_instalment__c(client_course__c = cc.id, Request_Commission_Invoice__c = inv.id);
        insert cci;

		Test.startTest();
            batch_createPDFandSendEmail b = new batch_createPDFandSendEmail(li);
            Database.executeBatch(b);
	    Test.stopTest();
		 
    }
}