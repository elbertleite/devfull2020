public with sharing class InstalmentActivity {
	
	public boolean showError {get;set;}
	private client_course_instalment__c instalment {get;set;}
	private Invoice__c invoice {get;set;}
	
	private String objType {get;set;}
	public InstalmentActivity(ApexPages.StandardController controller){
		
		if(ApexPages.currentPage().getParameters().get('instalmentId')!=null){
			instalment = [Select instalment_activities__c, Client_Course__r.Client__r.Name  from client_course_instalment__c where id = : ApexPages.currentPage().getParameters().get('instalmentId') limit 1];
			objType = 'instalment';
		}	
		else if(ApexPages.currentPage().getParameters().get('invoiceId') != null){
			invoice = [Select Invoice_Activities__c,  Client__r.Name  from Invoice__c where id = : ApexPages.currentPage().getParameters().get('invoiceId') limit 1];
			objType = 'invoice';
		}
		showError = false;
	}
	
	
	/** Create Activity **/	
	public String activityType {get;set;}
	public String activitySubject {get;set;}
	
	
	public void createActivity(){
		System.debug('Data==' + instalment);
		DateTime currentTime = DateTime.now();
		User currentUser = [Select Id, Name from User where id = :UserInfo.getUserId() limit 1];
		
		if(objType == 'instalment'){
			String activity = activityType + ';;' + instalment.Client_Course__r.Client__r.Name + ';;' + activitySubject.replace(';', ' ') +';;' + currentTime.format('yyyy-MM-dd HH:mm:ss') + ';;-;;-;;'+ currentUser.Name + '!#!' ;
			
			if(instalment.instalment_activities__c == null || instalment.instalment_activities__c == '')
				instalment.instalment_activities__c = activity;
			else
				instalment.instalment_activities__c += activity;
				
			update instalment;
			showError = true;
		}
		else if(objType == 'invoice'){
			String activity = activityType + ';;' + invoice.Client__r.Name + ';;' + activitySubject.replace(';', ' ') +';;' + currentTime.format('yyyy-MM-dd HH:mm:ss') + ';;-;;-;;'+ currentUser.Name + '!#!' ;
			
			if(invoice.Invoice_Activities__c == null || invoice.Invoice_Activities__c == '')
				invoice.Invoice_Activities__c = activity;
			else
				invoice.Invoice_Activities__c += activity;
				
			update invoice;
			showError = true;
		}
		
	}
	
	/** END -- Create Activity **/	

}