@isTest
private class agreements_new_test {

	public static Account school {get;set;}
	public static Account school2 {get;set;}
	public static Account agency {get;set;}
	public static Account agency2 {get;set;}
	public static Account campus {get;set;}
	public static Course__c course {get;set;}
	public static Campus_Course__c campusCourse {get;set;}

	
	@testSetup static void setup() {
		TestFactory tf = new TestFactory();

		agency = tf.createAgency();
		agency2 = tf.createAgency();
		school = tf.createSchool();
		school2 = tf.createSchool();
		campus = tf.createCampus(school, agency);
		course = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);

	}

	static testMethod void testContructor(){
		agreements_new test = new agreements_new();
	}

	static testMethod void createAgreement(){

		List<Account> schools = [Select Id From Account Where RecordType.Name = 'School'];
		List<Account> agencies = [Select Id From Account Where RecordType.Name = 'Agency'];

		//Agreement Parties
		agreements_new.CallResult result = agreements_new.saveAgreement(null, 'Agreement Test', agencies[0].Id, 'Test User', '2018-02-12', 'Direct', 'School', schools[0].Id + ';' + agencies[0].id, 'All Regions', null, null, 'true', 'false');

		result = agreements_new.saveAgreement(null, 'Agreement Test', agencies[0].Id, 'Test User', '2018-02-12', 'Direct', 'School', schools[0].Id + ';' + agencies[0].id, 'All Regions', null, null, 'true', 'true');

		//Agreement Details 
		result = agreements_new.saveGeneralInfo(result.agreementId, '2018-02-12', null, true, 'School Contact Details', 'Nationality Exc.', 'Agree Duration', 'Enroll Conditions', 'Course Payment', 'Refund Cancel', 'Extra Info', false, false, false, false, '');

		//Edit Constructor
		ApexPages.CurrentPage().getParameters().put('id', result.agreementId);
		agreements_new test = new agreements_new();
		result = agreements_new.saveAgreement(result.agreementId, 'Agreement Test', agencies[0].Id, 'Test User', '2018-02-12', 'Direct', 'School', schools[1].Id + ';' + agencies[0].id + ';' + agencies[1].id, 'All Regions', null, null, 'true', 'false');

		//Redirect to Commission
		test.redirectToCommission();

	}

	static testMethod void findSchools(){
		String result = agreements_new.findSchools('Test');
	}
}