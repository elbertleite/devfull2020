public class coursetranslations {

	private boolean isInternalUser = false;
    private Contact myContact;

    public boolean foundTranslations { get { if(foundTranslations == null) foundTranslations = false; return foundTranslations;} set;}
    public Integer NOT_STARTED {get { return 0; } set;}
    public Integer IN_PROGRESS {get { return 1; } set;}
    public Integer COMPLETED {get { return 2; } set;}
    public Integer UPDATE_REQUIRED {get { return 3; } set;}

    public Map<String, Map<String, Integer>> courseTranslations { get;set; }

    public void coursetranslations(){
			loadUserInfo();
    }

	public void loadUserInfo(){
		isInternalUser = IPFunctions.isInternalUser(UserInfo.getUserID());

		if(!isInternalUser){
			myContact = UserDetails.getMyContactDetails();
    		getSuppliers();
		}
				
	}

	private Set<String> suppliers;
	private void getSuppliers(){
		suppliers = new Set<String>();
		for(Supplier__c s : [select Supplier__c from Supplier__c where agency__c = :myContact.AccountID])
			suppliers.add(s.Supplier__c);
	}


	public String selectedCountry { get;set; }
    private List<SelectOption> countries;
    public List<SelectOption> getCountries(){
    	try {
	    	if(countries == null){
	    		countries = new List<SelectOption>();

	    		if(isInternalUser)
	    			for(AggregateResult ar : [select BillingCountry from account where recordtype.name = 'Campus'  group by BillingCountry order by BillingCountry])
		    			countries.add(new SelectOption( (String) ar.get('BillingCountry') , (String) ar.get('BillingCountry')));			    
		    	else
			    	for(AggregateResult ar : [select BillingCountry from account where recordtype.name = 'Campus' and id in :suppliers group by BillingCountry order by BillingCountry])
			    		countries.add(new SelectOption( (String) ar.get('BillingCountry') , (String) ar.get('BillingCountry')));
		    	
		    	selectedCountry = countries.get(0).getValue();
		    	
		    }
	    } catch (Exception e){
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.Error, e.getMessage() + '<br/>' + e.getStackTraceString()));
    	}

    	return countries;
    }

    public void refreshCities(){
    	cities = null;
    	selectedCity = null;
    	refreshSchools();
    }

    public String selectedCity {get;set;}
    private List<SelectOption> cities;
    public List<SelectOption> getCities(){
    	try {
	    	if(cities == null && selectedCountry != null){
	    		cities = new List<SelectOption>();
	    		cities.add(new SelectOption('all', '-- ALL --'));
	    		if(isInternalUser)
	    			for(AggregateResult ar : [select BillingCity from account where recordtype.name = 'Campus' and BillingCountry = :selectedCountry group by BillingCity order by BillingCity])
	    				cities.add(new SelectOption((String) ar.get('BillingCity'), (String) ar.get('BillingCity')));
	    		else
	    			for(AggregateResult ar : [select BillingCity from account where recordtype.name = 'Campus' and BillingCountry = :selectedCountry and id in :suppliers group by BillingCity order by BillingCity])
	    				cities.add(new SelectOption((String) ar.get('BillingCity'), (String) ar.get('BillingCity')));
	    	}
    	} catch (Exception e){
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.Error, e.getMessage() + '<br/>' + e.getStackTraceString()));
    	}
    	return cities;
    }

    public void refreshSchools(){
    	schools = null;
    	selectedSchool = null;
    }

    public String selectedSchool {get;set;}
    private List<SelectOption> schools;
    public List<SelectOption> getSchools(){
    	try {
	    	if(schools == null && selectedCountry != null){
	    		schools = new List<SelectOption>();
	    		schools.add(new SelectOption('all', '-- ALL --'));
	    		String sql = 'select parentid, Parent.name from account where recordtype.name = \'Campus\' and parentid != null and BillingCountry = :selectedCountry ';
	    		if(selectedCity != null)
	    			sql += ' and BillingCity = :selectedCity ';
					if(!isInternalUser)
						sql += ' and id in :suppliers ';

	    		sql += ' group by parentid, Parent.name order by Parent.name ';

	    		for(AggregateResult ar : Database.query(sql))
	    			schools.add(new SelectOption((String) ar.get('parentid'), (String) ar.get('name')));
	    	}
	    } catch (Exception e){
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.Error, e.getMessage() + '<br/>' + e.getStackTraceString()));
    	}

    	return schools;
    }


    public Map<String, String> languageMap {get;set;}
    public String selectedLanguage {get;set;}
	private List<SelectOption> languages;
	public List<SelectOption> getLanguages(){

		if(languages == null){
			languages = new List<SelectOption>();
			languageMap = new Map<String, String>();
			//languages.add(new SelectOption('', '-- Select Language --'));
			Schema.DescribeFieldResult fieldResult = Contact.Preferable_Language__c.getDescribe();
   			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
   			for(Schema.PicklistEntry f : ple)
   				if(f.getLabel() != 'en_US'){
      				languages.add(new SelectOption(f.getLabel(), IPFunctions.getLanguage(f.getLabel())));
      				languageMap.put(f.getLabel(), IPFunctions.getLanguage(f.getLabel()));
   				}
		}
		languages.sort();
		return languages;
	}

	public String selectedStatus {get;set;}
	private List<SelectOption> statuses;
	public List<SelectOption> getStatuses(){
		if(statuses == null){
			statuses = new List<SelectOption>();
			Schema.DescribeFieldResult fieldResult = Translation__c.Status__c.getDescribe();
   			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
   			for(Schema.PicklistEntry f : ple)
   				if(f.getLabel() != 'Not Started')
      				statuses.add(new SelectOption(f.getLabel(), f.getLabel()));
		}
		return statuses;
	}

	private List<String> languageResults;
	public List<String> getLanguageResults(){

		if(languageResults == null){
			languageResults = new List<String>();

			if(selectedLanguage != null && selectedLanguage != ''){
				languageResults.add(selectedLanguage);
			} else {
				Schema.DescribeFieldResult fieldResult = Contact.Preferable_Language__c.getDescribe();
	   			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
	   			for(Schema.PicklistEntry f : ple)
	   				if(f.getLabel() != 'en_US')
	      				languageResults.add(f.getLabel());
			}
		}
		languageResults.sort();
		return languageResults;
	}




   public Map<String, String> schoolNames {get;set;}
   public void search(){
   		languageResults = null;
    	schoolNames = new Map<String, String>();
    	//school, set<courseid>
     	Map<String, Set<ID>> schoolCourses = new Map<String, Set<ID>>();
     	//courseid, language, translation
     	Map<String, Map<String, Translation__c>> courseLanguages = new Map<String, Map<String, Translation__c>>();
     	//school, language, Status
     	courseTranslations = new Map<String, Map<String, Integer>>();

			String sql = 'select campus__r.parentid, campus__r.Parent.Name, Course__c, (Select id, Language__c, Status__c from translations__r) from campus_Course__c where campus__c != null and Is_Available__c = true ';
			if(selectedCountry != null && selectedCountry != 'all')
				sql += ' and campus__r.BillingCountry = :selectedCountry ';
			if(selectedCity != null && selectedCity != 'all')
				sql += ' and campus__r.BillingCity = :selectedCity ';
			if(selectedSchool != null && selectedSchool != 'all')
				sql += ' and campus__r.ParentId = :selectedSchool ';
			else if(!isInternalUser)
				sql += ' and Campus__c in :suppliers ';

			sql += ' order by Campus__r.Parent.Name ';
		
		system.debug('@sql: ' + sql);
		system.debug('@selectedCountry: ' + selectedCountry);
		system.debug('@selectedCity: ' + selectedCity);
		system.debug('@selectedSchool: ' + selectedSchool);
		system.debug('@suppliers: ' + suppliers);
		
     	for(Campus_Course__c cc : Database.query(sql)){

     		schoolNames.put(cc.campus__r.parent.name, cc.campus__r.parentid);

     		if(schoolCourses.containsKey(cc.campus__r.parent.name))
     			schoolCourses.get(cc.campus__r.parent.name).add(cc.course__c);
     		else
     			schoolCourses.put(cc.campus__r.parent.name, new Set<ID>{cc.course__c});

     		for(Translation__c tr : cc.translations__r){
     			if(courseLanguages.containsKey(cc.course__c))
     				courseLanguages.get(cc.course__c).put(tr.language__c, tr);
     			else
     				courseLanguages.put(cc.course__c, new Map<String, Translation__c> { tr.language__c => tr});
     		}

     	}


     	for(String schoolName : schoolCourses.keySet()){
     		integer totalCourses = schoolCourses.get(schoolName).size();

     		for(String language : getLanguageResults()){
     			integer translationsFound = 0;
     			boolean updateRequired = false;
     			for(String courseid : schoolCourses.get(schoolName)){
						if(courseLanguages.containsKey(courseid) && courseLanguages.get(courseid).containsKey(language))
							if(courseLanguages.get(courseid).get(language).status__c == 'Completed')
								translationsFound++;
							else if(courseLanguages.get(courseid).get(language).status__c == 'Update Required')
								updateRequired = true;
     			}

					Integer status = getSchoolTranslationStatus(totalCourses, translationsFound, updateRequired);
					if(selectedStatus == null || selectedStatus == ''){
		     			if(courseTranslations.containsKey(schoolName))
		     				courseTranslations.get(schoolName).put(language, status);
		     			else
		     				courseTranslations.put(schoolName, new Map<String, Integer>{ language => status });
					} else {
						if( (selectedStatus == 'Completed' && status == COMPLETED) || (selectedStatus == 'Update Required' && status == UPDATE_REQUIRED) ||
								(selectedStatus == 'Not Started' && status == NOT_STARTED) || (selectedStatus == 'In Progress' && status == IN_PROGRESS) )
								if(courseTranslations.containsKey(schoolName))
			     				courseTranslations.get(schoolName).put(language, status);
			     			else
			     				courseTranslations.put(schoolName, new Map<String, Integer>{ language => status });
					}
     		}
     	}

     	if(!courseTranslations.isEmpty())
     		foundTranslations = true;

     }





     private Integer getSchoolTranslationStatus(Integer totalCourses, Integer translatedCourses, boolean updateRequired){
     	Integer trStatus;

     	if(updateRequired)
     		trStatus = UPDATE_REQUIRED;
     	else if(translatedCourses == 0)
     		trStatus = NOT_STARTED;
     	else if(translatedCourses > 0 && translatedCourses < totalCourses)
     		trStatus = IN_PROGRESS;
     	else if(translatedCourses == totalCourses)
     		trStatus = COMPLETED;

     	return trStatus;

     }

}