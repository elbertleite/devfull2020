public with sharing class contact_signature {
	
	public Contact employee {get;set;}
	public boolean edit {get;set;}
	
	public contact_signature(ApexPages.StandardController controller){
		edit = false;
		employee = [select id, Signature__c from Contact where id = :controller.getID()];
		
	}
		

	public void save(){
		String sig = ApexPages.currentPage().getParameters().get('signature');
		employee.Signature__c = sig;
		system.debug('@#@$ sig: ' + sig);
		update employee;
		edit = false;
	}
	
	public void setEdit(){
		edit = true;
	}
	
	public void cancel(){
		edit = false;
	}

}