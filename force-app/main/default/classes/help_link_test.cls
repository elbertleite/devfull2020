@isTest
private class help_link_test {

	@isTest static void test_method_one() {

		Folder f = [select id, name from Folder where Type='Document' LIMIT 1];

		Document d = new Document();
		d.name = 'DocTest';
		d.FolderID = f.id;
		insert d;

		help_link hl = new help_link();
		ApexPages.currentPage().getParameters().put('fl', d.name);
		hl.FileHelp();
		hl.openHelp();
	}


}