public with sharing class client_course_cancel_history {
	
	public String requests {get;set;}
	public list<client_course_cancel.courseInfo> result{get;set;}

	public client_course_cancel_history() {
		User user = [Select Id, Contact.AccountId, Contact.Account.Global_Link__c, Contact.Department__c FROM User where Id = :UserInfo.getUserId()];
		
		result = new list<client_course_cancel.courseInfo>();

		String sql ='SELECT Cancel_Course_Status__c, Course_Cancellation_Service__c, Cancel_Notes__c, Credit_Reason__c, previewLink__c, Enrolment_Activities__c, Credit_Description__c, Enroled_by_Agency__r.Name, Enroled_by_User__r.Name, Client__c, Client__r.Name, Client__r.Status__c, Client__r.Visa_Expiry_Date__c, Client__r.Owner__r.Name, Credit_Courses__c FROM client_course__c WHERE ';

		sql += ' client__c = \'' + ApexPages.currentPage().getParameters().get('id') + '\' AND isCancelRequest__c = true AND isCancelled__c = true'; 

		system.debug('sql==>' + sql); 

		for(client_course__c c : Database.query(sql)){
			result.add(new client_course_cancel.courseInfo(c, IPFunctions.loadNotes(c.Cancel_Notes__c), IPFunctions.loadActivities(c.Enrolment_Activities__c), user.Contact.Account.Global_Link__c, IPFunctions.loadPreviewLink(c.Credit_Courses__c)));

			system.debug('c.Credit_Courses__c==>'+c.Credit_Courses__c);
			system.debug('c.Credit_Courses__c==>'+IPFunctions.loadPreviewLink(c.Credit_Courses__c));
		}//end for

		requests = JSON.serialize(result);
	}



}