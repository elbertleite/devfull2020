public with sharing class Batch_Fix_RDStation_Conversions{
	public Batch_Fix_RDStation_Conversions(){
		system.debug('');
	}
}
/*global with sharing class Batch_Fix_RDStation_Conversions implements Database.Batchable<sObject>{
    String groupID;
    
    global Batch_Fix_RDStation_Conversions(String groupID) {
         this.groupID = groupID;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        List<String> ids = new List<String>{'0032v00002bs7uE'};
        String query = 'SELECT ID, Name, RDStation_Fields__c, RDStation_Current_Campaign__c, RDStation_Last_Conversion__c FROM Contact WHERE Contact_Origin__c = \'RDStation\' AND Current_Agency__r.Parent.ID = :groupID';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Contact> ctts) {
        List<Contact> cttsToCheck = new List<Contact>();
        List<String> ids = new List<String>();
        for(Contact ctt : ctts){
            if(!String.isEmpty(ctt.RDStation_Fields__c)){
                ids.add(ctt.ID);
                cttsToCheck.add(ctt);
            }
        }

        List<Destination_Tracking__c> cycles = [SELECT ID, RDStation_Campaigns__c, RDStation_First_Conversion__c, Client__c FROM Destination_Tracking__c WHERE Client__c IN :ids AND Current_Cycle__c = true AND Agency_Group__c = :groupID];

        Map<String, Destination_Tracking__c> cyclesPerContact = new Map<String, Destination_Tracking__c>();
        for(Destination_Tracking__c cycle : cycles){
            cyclesPerContact.put(cycle.Client__c, cycle);
        }

        Map<String, IPClasses.RDStationHifyData> rdStationFields = null;
        IPClasses.RDStationHifyData rdStationData;
        
        cycles = new List<Destination_Tracking__c>();
        Destination_Tracking__c contactCycle = null;

        Datetime dateLastConversion;
        Datetime conversionDate;
        String firstConversion; 
        String lastConversion; 
        String dateStr = null;
        for(Contact ctt : cttsToCheck){
            dateLastConversion = null;
            firstConversion = null;
            lastConversion = null;
            rdStationFields = (Map<String, IPClasses.RDStationHifyData>) JSON.deserialize(ctt.RDStation_Fields__c, Map<String, IPClasses.RDStationHifyData>.class);
            rdStationData = rdStationFields.get(groupID);
            if(rdStationData != null){
                if(rdStationData.conversions != null && !rdStationData.conversions.isEmpty()){
                    for(String conversion : rdStationData.conversions.keySet()){
                        if(conversion.contains('first_conversion')){
                            if(String.isEmpty(firstConversion)){
                                if(!String.isEmpty(rdStationData.conversions.get(conversion).get('identificador'))){
                                    firstConversion = rdStationData.conversions.get(conversion).get('identificador');
                                }else{
                                    firstConversion = conversion.substring(conversion.lastIndexOf('!#!')).replace('!#!','');
                                }
                            }
                        }else{
                            dateStr = conversion.split('!#!')[1];
                            dateStr = dateStr.replace('***',' ');
                            conversionDate = Datetime.valueOf(dateStr);
                            if(dateLastConversion == null || conversionDate < dateLastConversion){
                                dateLastConversion = conversionDate;
                                if(!String.isEmpty(rdStationData.conversions.get(conversion).get('identificador'))){
                                    lastConversion = rdStationData.conversions.get(conversion).get('identificador');
                                }else{
                                    lastConversion = conversion.split('!#!')[0];
                                }
                            }   
                        }
                    }
                    if(String.isEmpty(lastConversion)){
                        lastConversion = firstConversion;
                    }
                    ctt.RDStation_Current_Campaign__c = firstConversion;
                    ctt.RDStation_Last_Conversion__c = lastConversion;

                    contactCycle = cyclesPerContact.get(ctt.ID);
                    if(contactCycle != null){
                        contactCycle.RDStation_First_Conversion__c = firstConversion;
                        contactCycle.RDStation_Campaigns__c = lastConversion;
                        cycles.add(contactCycle);
                    }
                }
            }
        }

        update cttsToCheck;
        if(!cycles.isEmpty()){
            update cycles;
        }
    }

    global void finish(Database.BatchableContext BC) {}
}*/