/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class contact_controller_test {
	
	static testMethod void myUnitTest() {
		
		TestFactory tf = new TestFactory();
		Account agency = tf.createAgency();
		tf.createChecklists(agency.parentID);
		Contact employee = tf.createEmployee(agency);		
		
		User portalUser = tf.createPortalUser(employee);
		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);
  		Course__c course = tf.createCourse();
		Campus_Course__c campusCourse = tf.createCampusCourse(campus, course);	
		
		 
		
		Test.startTest();
		system.runAs(portalUser){

			ApexPages.currentPage().getParameters().put('id', employee.id);

			Contact client = tf.createLead(agency, employee);
			
			tf.createQuotation(client, campusCourse);
		
			contact_controller cc = new contact_controller();		
			cc.ctid = client.id;		
			cc.getContact();
			String str = cc.path;
			cc.limitQuotations = 2;
			list<contact_controller.listQuotes> quotations = cc.quotations;	
			List<Selectoption> opts = cc.schoolCountries;
			
			cc.setEdit();
			cc.cancel();
			cc.addContactNumber();
			
			List<contact_controller.PhoneNumber> newPhoneNumbers = cc.newPhoneNumbers;
			
			ApexPages.currentPage().getParameters().put('contactID', '0');
			ApexPages.currentPage().getParameters().put('type', 'phoneNumber');
			cc.deleteNewContact();
			
			contact_controller.PhoneNumber pn = new contact_controller.PhoneNumber();
			pn.contact = new Forms_of_Contact__c();
			pn.contact.Country__c = 'Australia';
			pn.contact.Detail__c = '837465876';
			pn.contact.Type__c = 'Mobile';
			pn.contact.contact__c = client.id;
			cc.newPhoneNumbers.add(pn);
			
			cc.addSocialNetworkItem();
			cc.addSocialNetworkItem();
			
			ApexPages.currentPage().getParameters().put('socialID', '0');
			cc.deleteNewSocialNetwork();
			
			cc.addAddressItem();
			ApexPages.currentPage().getParameters().put('addressID', '0');
			cc.deleteNewAddress();
			
			cc.addNewEmail();
			cc.getCountries();
			
			List<SelectOption> quoteLanguages = cc.quoteLanguages;
			
			for(Address__c ad : cc.contact.addresses__r){
				ad.Country__c = 'Australia';
				ad.Street__c = '300 george street';
			}
				
				
			
			cc.save();
			
			
			
			cc.getDestinationsCountryMultiple();
			cc.getLeadProductType();
			cc.getLeadStudyType();
			cc.refreshContactDetails();
			
			
			cc.addNewVisa();
			for(contact_controller.VisaWrapper vw : cc.visas){
				vw.visa.Visa_Country_Applying_for__c = 'Australia';
				vw.visa.Visa_type__c = 'Tourist';
				vw.visa.Visa_subclass__c = 'Tourist test';
				vw.visa.Expiry_Date__c = system.today().addDays(90);			
			}
			cc.saveNewVisa();
			cc.cancelVisa();
			
			String contactPreferableLanguage = cc.contactPreferableLanguage;
			cc.limitQuotations = 0;
			cc.quotations = null;
			
			quotations = cc.quotations;
			
			cc.Beginning();
			cc.Previous();
			cc.Next();
			cc.End();
			cc.getDisablePrevious();
			cc.getDisableNext();
			cc.getTotal_size();
			cc.getPageNumber();
			cc.getTotalPages();
			
			
			
			
			
		}
		
		
		Test.stopTest();
		
		
		
	}

}