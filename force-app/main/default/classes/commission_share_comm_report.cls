public with sharing class commission_share_comm_report {

	public String currencies {get;set;}
	public String invCurrency {get;set;}
	public transient decimal totalAmount {get;set;}
	public transient decimal totalResult {get;set;}
	public transient list<Invoice__c> result {get;set;} 
	public transient map<string,list<s3Docs>> invDocs {get;set;}

	private transient set<String> sCurrencies;
	private transient set<String> sInvCurrency;

	public commission_share_comm_report() {

	}

	public void findInvoices(){
		totalAmount = 0;
		totalResult = 0;

		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()) //set the user to see only his own agency group
			selectedAgencyGroup = currentUser.Contact.Account.ParentId;

		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && currentUser.Aditional_Agency_Managment__c == null) //set the user to see only his own agency
			selectedAgency = currentUser.Contact.AccountId;
		
		String fromDate = IPFunctions.FormatSqlDateIni(dateFilter.Original_Due_Date__c); 
		String toDate = IPFunctions.FormatSqlDateFin(dateFilter.Discounted_On__c);

		String sql = 'SELECT Invoice__c, Share_Commission_Number__c, Summary__c, Requested_Commission_for_Agency__r.Name, Requested_Commission_for_Agency_Group__r.Name, Payment_Receipt__r.Preview_Link__c, Paid_Date__c, Paid_By__r.Name, Paid_By_Agency__r.Name, createdBy.Name, Confirmed_By__r.Name, Commission_Paid_Date__c, Requested_Commission_To_Agency__r.Name, Requested_Commission_to_Agency_Group__r.Name, CurrencyIsoCode__c, Total_Instalments_Requested__c, Total_Value__c, Received_Currency__c, Received_Currency_Rate__c,Received_Currency_Value__c, Transfer_Receive_Fee__c, Transfer_Receive_Fee_Currency__c, Transfer_Send_Fee__c, Transfer_Send_Fee_Currency__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c FROM Invoice__c ';
		
		sql += 'WHERE ';

		String typeDate;

		

		if(selectedTypeCommission == 'paid'){
			typeDate = ' Paid_Date__c ';	

			if(selectedAgency!= 'all')
				sql += ' Paid_By_Agency__c = \'' + selectedAgency + '\' ';
			else
				sql += ' Paid_By_Agency__c in ( \''+ String.join(allAgencies, '\',\'') + '\' ) ';
			
		}
		else{
			typeDate = ' Commission_Paid_Date__c ';	

			if(selectedAgency!= 'all')
				sql += ' Requested_Commission_for_Agency__c = \'' + selectedAgency + '\' ';
			else
				sql += ' Requested_Commission_for_Agency__c in ( \''+ String.join(allAgencies, '\',\'') + '\' ) AND Requested_Commission_for_Agency_Group__c = \'' + selectedAgencyGroup + '\' ';
		}
			
		sql += ' AND isShare_Commission_Invoice__c = TRUE AND Country__c =  \'' + selectedCountry + '\'';

		if(invoiceNumber.length()>0){
			invoiceNumber = invoiceNumber.replace(' ', '');
			list<String> allRequest = new list<String>(invoiceNumber.split(','));
			sql += ' AND Share_Commission_Number__c in ( \''+ String.join(allRequest, '\',\'') + '\' ) ';
		}
		else{

			if(selectedTypeFilter == 'range'){
				sql += ' AND ' + typeDate + ' >= ' + fromDate;
				sql += ' AND ' + typeDate + ' <= ' + toDate;
			}
			else if(selectedTypeFilter == 'THIS_MONTH')
				sql += ' AND ' + typeDate + ' = THIS_MONTH ';
			else if(selectedTypeFilter == 'LAST_MONTH')
				sql += ' AND ' + typeDate + ' = LAST_MONTH ';
		}


		system.debug('sql===>' + sql);

		invDocs = new map<String, list<s3Docs>>();
		sCurrencies = new set<String>();
		sInvCurrency = new set<String>();
		
		result = searchNoSharing.NSLInvoice(sql);

		if(selectedTypeCommission == 'paid')
			for(Invoice__c inv : result){
				if(inv.Transfer_Send_Fee__c == null) inv.Transfer_Send_Fee__c = 0;

				totalAmount += inv.Total_Value__c;
				totalResult += inv.Agency_Currency_Value__c + inv.Transfer_Send_Fee__c;

				sCurrencies.add(inv.Agency_Currency__c);
				sInvCurrency.add(inv.CurrencyIsoCode__c);

				generateDocs(inv);
			}//end for

		else
			for(Invoice__c inv : result){
				if(inv.Transfer_Receive_Fee__c == null) inv.Transfer_Receive_Fee__c = 0;

				totalAmount += inv.Total_Value__c;
				totalResult += inv.Received_Currency_Value__c + inv.Transfer_Receive_Fee__c;

				generateDocs(inv);

				sCurrencies.add(inv.Received_Currency__c);
				sInvCurrency.add(inv.CurrencyIsoCode__c);
			}//end for

		currencies = String.join(new List<String>(sCurrencies), ',');
		invCurrency = String.join(new List<String>(sInvCurrency), ',');

	}

	private void generateDocs(Invoice__c invoice){
		invDocs.put(invoice.id, new list<S3Docs>());
		//Payment Documents
		if(invoice.Payment_Receipt__r.preview_link__c!=null){
			for(String doc : invoice.Payment_Receipt__r.preview_link__c.split(FileUpload.SEPARATOR_FILE))
				invDocs.get(invoice.id).add(new S3Docs(doc.split(FileUpload.SEPARATOR_URL)));
		}
	}



	private IPFunctions.SearchNoSharing searchNoSharing {get{if(searchNoSharing==null) searchNoSharing = new IPFunctions.SearchNoSharing(); return searchNoSharing;}set;}
	private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}
	private User currentUser {get{if(currentUser==null) currentUser = ff.currentUser; return currentUser;}set;}

	public string selectedCountry{get;set;}
	public List<SelectOption> destinationCountries{ 
		get{
			if(destinationCountries == null){
				destinationCountries = new List<SelectOption>();
				// destinationCountries.addAll(FinanceFunctions.CountryDestinations());
				// destinationCountries.remove(0);
				// System.debug('==>:destinationCountries: '+destinationCountries);
				// // if(selectedCountry == null)
				// // 	selectedCountry = destinationCountries[0].getValue();
				// selectedCountry = 'Australia';

				for(AggregateResult ar : [SELECT Country__c ct FROM Invoice__c WHERE (Paid_By_Agency__r.ParentId IN :allGroups OR Requested_Commission_for_Agency__r.ParentId IN :allGroups) AND isShare_Commission_Invoice__c = TRUE group by Country__c order by Country__c])
					destinationCountries.add(new SelectOption((String) ar.get('ct'), (String) ar.get('ct')));

				if(destinationCountries.size()>0)
					selectedCountry = destinationCountries[0].getLabel();
				
			}
			return destinationCountries;
		}
		set;
	}

	public string selectedAgencyGroup{
    	get{
    		if(selectedAgencyGroup == null)
    			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
    		return selectedAgencyGroup;
    	}set;}

	private set<Id> allGroups {get{if(allGroups == null){agencyGroupOptions = null;}return allGroups;}set;}
	public List<SelectOption> agencyGroupOptions {
  		get{
   			if(agencyGroupOptions == null){
				allGroups = new set<id>();
   				agencyGroupOptions = new List<SelectOption>();
   				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group
					agencyGroupOptions.add(new SelectOption(currentUser.Contact.Account.ParentId, currentUser.Contact.Account.Parent.Name));
					selectedAgencyGroup = currentUser.Contact.Account.ParentId;
					allGroups.add(currentUser.Contact.Account.ParentId);
   				}
				   else{
   					for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
		     			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
						allGroups.add(ag.Id);
		    		}//end for
   				}
	   		}
	   		return agencyGroupOptions;
	  }set;}

	public void changeGroup(){
		selectedAgency = '';
		getAgencies();
	}

	public String selectedAgency {get;set;}
	public list<Id> allAgencies {get;set;}
	public list<SelectOption> getAgencies(){
		List<SelectOption> result = new List<SelectOption>();
		allAgencies = new list<Id>();
		if(selectedAgencyGroup != null){
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
				selectedAgency = currentUser.Contact.AccountId;
				result.add(new SelectOption(currentUser.Contact.AccountId, currentUser.Contact.Account.Name));
				if(currentUser.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
						result.add(new SelectOption(ac.id, ac.name));
					}
				}
			}else{
				result.add(new SelectOption('all', ' -- All -- '));
				selectedAgency = 'all';
				for(Account a: [Select Id, Name from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
					result.add(new SelectOption(a.Id, a.Name));
					allAgencies.add(a.Id);
				}
			}
		}return result;}

	public String selectedTypeCommission {get{if(selectedTypeCommission == null) selectedTypeCommission = 'received'; return selectedTypeCommission;}set;}
	public list<SelectOption> typeCommission {get{
		if(typeCommission==null){
			typeCommission = new list<SelectOption>();
			typeCommission.add(new SelectOption('paid', 'Invoices Paid'));
			typeCommission.add(new SelectOption('received', 'Invoices Received'));
		}
		return typeCommission;
	}set;}

	public String selectedTypeFilter {get{if(selectedTypeFilter == null) selectedTypeFilter = 'range'; return selectedTypeFilter;}set;}
	public list<SelectOption> typeFilter {get{
		if(typeFilter==null){
			typeFilter = new list<SelectOption>();
			typeFilter.add(new SelectOption('range', 'Range'));
			typeFilter.add(new SelectOption('THIS_MONTH','This Month'));
			typeFilter.add(new SelectOption('LAST_MONTH','Last Month'));
		}
		return typeFilter;
	}set;}

	public client_course_instalment__c dateFilter{
		get{
			if(dateFilter == null){
				dateFilter = new client_course_instalment__c();
				Date myDate = Date.today();
				dateFilter.Discounted_On__c = system.today();
				dateFilter.Original_Due_Date__c = dateFilter.Discounted_On__c.addDays(-30);
			}
			return dateFilter;
		}set;}

	public String invoiceNumber {get;set;}

	public class s3Docs{
		public String docName {get;set;}
		public String docUrl {get;set;}

		public s3Docs (List<String> docParts){
			this.docUrl = docParts[0];
			this.docName = docParts[1];
		}
    }
}