public with sharing class products_pending_refund {

	private User currentUser = [Select Id, Contact.Name, Contact.Email, Contact.Signature__c, Aditional_Agency_Managment__c, Contact.Account.Name, Contact.Account.ParentId, Contact.AccountId, Contact.Department__c, Contact.Account.Global_Link__c, Contact.Account.account_currency_iso_code__c from User where id = :UserInfo.getUserId() limit 1];
	
	
	public products_pending_refund() {
		showTab = 'reqProvider';
		search();
	}

	public void search(){
		if(showTab == 'reqProvider'){
			searchRequest();
		}
		else if(showTab == 'confRefund'){
			searchConfirm();
		}
	}

	// S E A R C H 		T O 	R E Q U E S T
	public list<client_product_service__c> listProducts{get; set;}
	public Integer totRequest {get;set;}
	private void searchRequest(){
		String sql = 'SELECT isSelected__c, Refund_Notes__c, Products_Services__r.Provider__c, Products_Services__r.Provider__r.Provider_Name__c, Client__r.name,  Client__r.Age__c, Price_Total__c, Quantity__c, Received_Date__c, Provider_Payment__r.Paid_Date__c, Paid_to_Provider_by_Agency__r.Name, Received_by__r.name, Product_Name__c, Currency__c, Agency_Currency_Value__c, Agency_Currency__c, Agency_Currency_Rate__c, Claim_Commission_Type__c, Total_Provider_Payment__c, Commission_Tax_Rate__c, Commission_Tax_Value__c, Commission_Type__c, Commission_Value__c, Commission__c, Refund_Start_Date__c, 	Commission_Confirmed_On__c FROM client_product_service__c WHERE ';

		String swhere = '';
				
		sql += ' Paid_to_Provider_by_Agency__c = \'' + selectedAgency + '\' AND isRefund_Requested__c = TRUE AND Refund_Requested_to_Provider__c = FALSE ';

		if(selectedProvider != null && selectedProvider != 'none' && selectedProvider != 'all')
			swhere += ' AND Products_Services__r.Provider__c = \'' + selectedProvider + '\' ';
		
		
		if(selectedCategory != null && selectedCategory != 'none' && selectedCategory != 'all')
			swhere += ' AND ( Products_Services__r.Category__c = \'' + selectedCategory + '\' OR Products_Services__r.Category__c = NULL)';
		
		if(selectedProduct != null && selectedProduct != 'none' && selectedProduct != 'all')
			swhere += ' and Products_Services__c = \''+selectedProduct +'\'';
		
		sql += swhere;

		sql += ' order by Currency__c, Client__r.name, Product_Name__c';

		system.debug('sql===>' + sql);

		listProducts = Database.query(sql);

		totRequest = listProducts.size();

		// Total to Confirm
		sql = 'SELECT count(id) tot FROM client_product_service__c WHERE Paid_to_Provider_by_Agency__c = \'' + selectedAgency + '\' AND isRefund_Requested__c = TRUE AND Refund_Requested_to_Provider__c = TRUE AND Refund_Confirmed_ADM__c = FALSE ';
		
		sql += swhere ;

		for(AggregateResult ar : Database.query(sql))
			totConfirm = Integer.valueOf(ar.get('tot')) != null ? Integer.valueOf(ar.get('tot')) : 0;

		listRefund = new list<client_product_service__c>();

		
	}

	// S E A R C H 		T O 	C O N F I R M
	public list<client_product_service__c> listRefund{get; set;}
	public map<String, list<IPFunctions.activityWrapper>> mapActivities{get;set;}
	public Integer totConfirm {get;set;}
	private void searchConfirm(){

		listProducts = new list<client_product_service__c>();
		listRefund = new list<client_product_service__c>();

		String sql = 'SELECT isSelected__c, Refund_Notes__c, Products_Services__r.Provider__c, Products_Services__r.Provider__r.Provider_Name__c, Client__r.name, Client__r.Age__c, Price_Total__c, Quantity__c, Received_Date__c, Provider_Payment__r.Paid_Date__c, Paid_to_Provider_by_Agency__r.Name, Received_by__r.name, Product_Name__c, Currency__c, Agency_Currency_Value__c, Agency_Currency__c, Agency_Currency_Rate__c, Claim_Commission_Type__c, Total_Provider_Payment__c, Commission_Tax_Rate__c, Commission_Tax_Value__c, Commission_Type__c, Commission_Value__c, Commission__c, Refund_Start_Date__c, Agency_to_Refund__c, Agency_Commission_Refund__c, Agency_Refund_Paid_Date__c, Refund_amount__c, Agency_Refund_Client__c, Refund_Activities__c, Commission_Confirmed_On__c FROM client_product_service__c WHERE '; 
		String swhere = '';
						
		sql += ' Paid_to_Provider_by_Agency__c = \'' + selectedAgency + '\' AND isRefund_Requested__c = TRUE AND Refund_Requested_to_Provider__c = TRUE AND Refund_Confirmed_ADM__c = FALSE ';

		if(selectedProvider != null && selectedProvider != 'none' && selectedProvider != 'all')
			swhere += ' AND Products_Services__r.Provider__c = \'' + selectedProvider + '\' ';
		
		
		if(selectedCategory != null && selectedCategory != 'none' && selectedCategory != 'all')
			swhere += ' AND ( Products_Services__r.Category__c = \'' + selectedCategory + '\' OR Products_Services__r.Category__c = NULL)';
		
		if(selectedProduct != null && selectedProduct != 'none' && selectedProduct != 'all')
			swhere += ' and Products_Services__c = \''+selectedProduct +'\'';
		sql += swhere;
		
		sql += ' order by Currency__c, Client__r.name, Product_Name__c';

		system.debug('sql===>' + sql);

		// listRefund = Database.query(sql);

		mapActivities = new map<String, list<IPFunctions.activityWrapper>>();

		for(client_product_service__c prod :  Database.query(sql)){
			if(prod.refund_activities__c != null){
				
				list<IPFunctions.activityWrapper> act = (list<IPFunctions.activityWrapper>) JSON.deserialize(prod.refund_activities__c, list<IPFunctions.activityWrapper>.class);

				// for(IPFunctions.activityWrapper w : act)
				// 	w.actDate = Datetime.valueOfGMT(w.actDate).format();

				mapActivities.put(prod.Id, new list<IPFunctions.activityWrapper>());

				for(Integer i = act.size() - 1; i >= 0; i--){
					act[i].actDate = Datetime.valueOfGMT(act[i].actDate).format();
					mapActivities.get(prod.Id).add(act[i]);
				}//end for
				
			}
			else
				mapActivities.put(prod.Id, new list<IPFunctions.activityWrapper>());
			listRefund.add(prod);
		}//end for

		totConfirm = listRefund.size();

		// Total to Request
		sql = 'SELECT count(id) tot FROM client_product_service__c WHERE Paid_to_Provider_by_Agency__c = \'' + selectedAgency + '\' AND isRefund_Requested__c = TRUE AND Refund_Requested_to_Provider__c = FALSE ';
		
		sql += swhere;

		for(AggregateResult ar : Database.query(sql))
			totRequest = Integer.valueOf(ar.get('tot')) != null ? Integer.valueOf(ar.get('tot')) : 0;
	}

	// C H A G E 	T A B
	public String showTab {get;set;}
	public void changeTab(){
		showTab = ApexPages.currentPage().getParameters().get('show');
		search();
		ApexPages.currentPage().getParameters().remove('show');
	}


	// S E T U P 	T O 	C O N F I R M
	public client_product_service__c toConfirm {get;set;}
	public void setupToConfirm(){
		toConfirm = null; 

		String prodId = ApexPages.currentPage().getParameters().get('prodId');
		for(client_product_service__c p : listRefund)
			if(p.Id == prodId){
				toConfirm = p;
				break;
			}

		ApexPages.currentPage().getParameters().remove('prodId');

	}

	// C O N F I R M 		R E F U N D
	public void confirmRefund(){

		if(toConfirm.Agency_Refund_Client__c == 'No'){
			toConfirm.Agency_to_Refund__c = null;
			toConfirm.Agency_Refund_Paid_Date__c = null;
		}

		toConfirm.Refund_Confirmed_ADM__c = true;

		toConfirm.Agency_Commission_Refund__c = math.abs(toConfirm.Agency_Commission_Refund__c) * -1 ;
		toConfirm.Refund_amount__c = math.abs(toConfirm.Refund_amount__c) * -1 ;

		update toConfirm;
	}

	// S E T U P	E M A I L
	public String emTo {get;set;}
	public String emCc {get;set;}
	public String emSubj {get;set;}
	public String emCont {get;set;}
	private client_product_service__c upProd;
	public void setupEmail(){
		upProd  = [SELECT Products_Services__r.Provider__c, Refund_Activities__c FROM client_product_service__c WHERE Id = :ApexPages.CurrentPage().getParameters().get('prodId')];

		Provider__c provider = [SELECT Invoice_to_Email__c FROM Provider__c WHERE ID = :upProd.Products_Services__r.Provider__c];

		emTo = provider.Invoice_to_Email__c;
	}

		
	
	// S E N D		E M A I L
	public void sendEmail(){

		//Send Email
		String emailService = IpFunctions.getS3EmailService();
		blob pdfFile = null;
		mandrillSendEmail mse = new mandrillSendEmail();
		mandrillSendEmail.messageJson email_md = new mandrillSendEmail.messageJson();

		email_md.setFromEmail(currentUser.Contact.Email);
		email_md.setFromName(currentUser.Contact.Name);
		list<String> toAd;
		list<String> toCc;

		if(emTo.contains(';')){
			toAd = emTo.split(';', 0);
			for(String t : toAd)
				if(!String.isBlank(t))
					email_md.setTo(t, '');
		}
		else if(emTo.contains(',')){
			toAd = emTo.split(',', 0);
			for(String t : toAd)
				if(!String.isBlank(t))
					email_md.setTo(t, '');
		}else{
			email_md.setTo(emTo, '');
		}

		if (emCc != null && emCc != ''){
			emCc = emCc.replaceAll(',',';').replaceAll('\n',';');
			toCc = emCc.split(';', 0);
			for(String c_c : toCc) 
				email_md.setCc(c_c, '');
		}

		//Send a copy to the sender
		email_md.setCc(currentUser.Contact.Email, '');
		email_md.setSubject(emSubj);
		email_md.preserve_recipients(true);

		emCont = emCont.replace('<style type="text/css">', '<head><style>').replace('</style>', '</style></head>');
		email_md.setHtml(emCont);

		if(!Test.isRunningTest())
			mse.sendMail(email_md);//send email

		Datetime GMT = system.now();
		String editDate = GMT.format('yyyy-MM-dd HH:mm:ss', 'UTC');

		upProd.Refund_Requested_to_Provider__c = true;


		//Refund activities
		list<IPFunctions.activityWrapper> lAct = new list<IPFunctions.activityWrapper>();

		if(upProd.Refund_Activities__c != null){
			lAct = (list<IPFunctions.activityWrapper>) JSON.deserialize(upProd.Refund_Activities__c, list<IPFunctions.activityWrapper>.class);
			lAct.add(new IPFunctions.activityWrapper(string.valueOf(lAct.size() + 1),'Sent Email', emTo, currentUser.Contact.Name, currentUser.Contact.Account.Name, emSubj, editDate));	
		}
		else{
			lAct.add(new IPFunctions.activityWrapper('1', 'Request Refund to Provider', emTo, currentUser.Contact.Name, currentUser.Contact.Account.Name, emSubj, editDate));
		}

		upProd.Refund_Activities__c = JSON.serialize(lAct); 
		update upProd;

		emTo = null;
		emCc = null;
		emSubj = null;
		emCont = null;
	}

	// ---------------------------------------------------	FILTERS


	// Agency Group Options
	public string selectedAgencyGroup{
    	get{
    		if(selectedAgencyGroup == null)
    			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
    		return selectedAgencyGroup;
    	}set;}
	public List<SelectOption> agencyGroupOptions {
  		get{
   			if(agencyGroupOptions == null){
   				
   				agencyGroupOptions = new List<SelectOption>();  
   				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group  
	    			for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:currentUser.Contact.AccountId order by Parent.Name]){
		     			agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
		    		}
   				}else{
   					for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
		     			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
		    		}
   				}
	   		}
	   		return agencyGroupOptions;
	  }set;}

	public void changeGroup(){
		agencyOptions = null;
	}

	// Agency Options
	public String selectedAgency {get{if(selectedAgency == null) selectedAgency = currentUser.Contact.AccountId; return selectedAgency;}set;}
	public map<String,String> mapAgCurrency {get;set;}
	public list<SelectOption> agencyOptions {get{
		if(agencyOptions == null){
			mapAgCurrency = new map<String,String>();
			agencyOptions = new List<SelectOption>();
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
				for(Account a: [Select Id, Name, Account_Currency_Iso_Code__c from Account where id =:currentUser.Contact.AccountId order by name]){
					if(selectedAgency == null)
						selectedAgency = a.Id;					
					agencyOptions.add(new SelectOption(a.Id, a.Name));
					mapAgCurrency.put(a.Id, a.Account_Currency_Iso_Code__c);
				}
				if(currentUser.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
						agencyOptions.add(new SelectOption(ac.id, ac.name));
						mapAgCurrency.put(ac.Id, ac.Account_Currency_Iso_Code__c);
					}
				}
			}else{
				for(Account a: [Select Id, Name, Account_Currency_Iso_Code__c from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
					agencyOptions.add(new SelectOption(a.Id, a.Name));
					mapAgCurrency.put(a.Id, a.Account_Currency_Iso_Code__c);
					if(selectedAgency == null)
						selectedAgency = a.Id;	
				}
			}

			findProviders();
		}

		return agencyOptions;
		
	}set;}

	public void changeAgency(){
		findProviders();
	}


	//Providers
	public string selectedProvider{
    	get{
    		if(selectedProvider == null)
    			selectedProvider = 'none';
    		return selectedProvider;
    	}set;}
	public List<SelectOption> providersOpt {get;set;}
	public void findProviders(){
		providersOpt = new List<SelectOption>();
		providersOpt.add(new SelectOption('all','-- All --'));

		for(AggregateResult ag : [SELECT Products_Services__r.Provider__c pId, Products_Services__r.Provider__r.Provider_Name__c pName FROM client_product_service__c WHERE Paid_to_Provider_by_Agency__c = :selectedAgency AND isRefund_Requested__c = TRUE AND Refund_Confirmed_ADM__c = FALSE group by Products_Services__r.Provider__c, Products_Services__r.Provider__r.Provider_Name__c]){
			if((String) ag.get('pId') != null)
				providersOpt.add(new SelectOption((String) ag.get('pId'), (String) ag.get('pName')));
		}//end for

		selectedProvider = 'none';
		findCategories();
	}

	public void changeProvider(){
		findCategories();
	}


	//Providers Category
	public string selectedCategory{get;set;}

	public List<SelectOption> categoriesOpt {get;set;}
	public void findCategories(){
		categoriesOpt = new List<SelectOption>();

		if(selectedProvider != 'none' && selectedProvider != 'all'){
			categoriesOpt.add(new SelectOption('all','-- All --'));
			for(AggregateResult ag : [SELECT Products_Services__r.Category__c cat FROM client_product_service__c WHERE Paid_to_Provider_by_Agency__c = :selectedAgency AND isRefund_Requested__c = TRUE AND Refund_Confirmed_ADM__c = FALSE  group by Products_Services__r.Category__c]){
				categoriesOpt.add(new SelectOption((String) ag.get('cat'), (String) ag.get('cat')));
			}//end for

		}
		else{
			categoriesOpt.add(new SelectOption('none','-- Select Provider --'));
		}

		selectedCategory = 'all';

		if(selectedProvider != 'none')
			findCatProducts();
		else{
			agencyProducts = new List<SelectOption>();
			agencyProducts.add(new SelectOption('none','-- Select Provider --'));
		}
	}

	public void changeCategory(){
		findCatProducts();
	}


	// Products
	public string selectedProduct{get;set;}
	public List<SelectOption> agencyProducts {get;set;}

	public void findCatProducts(){
		agencyProducts = new List<SelectOption>();
		agencyProducts.add(new SelectOption('all','-- All --'));

		String sql = 'SELECT Products_Services__c pId, Product_Name__c nm FROM client_product_service__c WHERE Paid_to_Provider_by_Agency__c = \'' + selectedAgency  + '\' ';

		if(selectedProvider != 'none' && selectedProvider != 'all')
			sql += ' AND Products_Services__r.Provider__c = \'' + selectedProvider + '\' ';

		if(selectedCategory != 'none' && selectedCategory != 'all')
			sql += ' AND ( Products_Services__r.Category__c = \'' + selectedCategory + '\' OR Products_Services__r.Category__c = NULL)';

		sql += '  AND isRefund_Requested__c = TRUE AND Refund_Confirmed_ADM__c = FALSE  group by Products_Services__c, Product_Name__c ';

		system.debug('products sql ==> ' + sql);

		for(AggregateResult ag : Database.query(sql)){
			agencyProducts.add(new SelectOption((String) ag.get('pId'), (String) ag.get('nm')));
		}//end for

		selectedProduct = 'all';
	}
}