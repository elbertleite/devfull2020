public without sharing class AssignLeadsController{
	
	private List<String> leadIDs;
	
	public AssignLeadsController(){
		
		String ids = ApexPages.currentPage().getParameters().get('ids');
		ids = ids.replace('[','').replace(']','').replace(' ','');		
		leadIDs = ids.split(',');
		
		selectedAgency = userDetails.Contact.AccountId;
	}
	
	public List<Contact> leadsToAssign {
		get {
			if(leadsToAssign == null){
				leadsToAssign = new List<Contact>();
				for(Contact acc : [Select Name, CreatedDate, Ownership_History_Field__c, Agency_Manager__c, LeadSource, Lead_Destinations__c, Travel_Duration__c, Lead_Message__c, Lead_Product_Type__c, Original_Agency__c, RDStation_Fields__c,
											Lead_Study_Type__c, When_are_you_planning_to_travel__c, Client_Classification__c , Expected_Travel_Date__c, Lead_Level_of_Interest__c, Owner__c, Owner__r.ID, RecordType.Name,
											Lead_Assignment__c,	Lead_Assignment_By__r.Name, Lead_Assignment_To__r.ID, Lead_Assignment_To__r.Name, Email, MobilePhone, isSelected__c from Contact where id = :leadIDs]){
					acc.isSelected__c = true;	
					leadsToAssign.add(acc);
				}
			}
			return leadsToAssign;
		}
		Set;
	}
	
	
	public map<string, boolean> agencyGroupType{get; set;}
	
	public map<string,string> agencyGroupMap {get;Set;}
	public String selectedAgencyGroup {get{if(selectedAgencyGroup == null) selectedAgencyGroup = userDetails.Contact.Account.ParentId; return selectedAgencyGroup;}set;}
	public List<SelectOption> agencyGroupOptions {
		get{
			if(agencyGroupOptions == null){
				agencyGroupType = new  map<string, boolean>();
				agencyGroupMap = new Map<String, String>();
				agencyGroupOptions = new List<SelectOption>();				
				for(Account ag : [select id, name, Destination_Group__c from Account where RecordType.name = 'Agency Group' and Global_Link__c = :userDetails.Contact.Account.Parent.Global_Link__c order by Name ]){
					agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
					agencyGroupMap.put(ag.id, ag.Name);
					agencyGroupType.put(ag.id, ag.Destination_Group__c);
				}
			}
			return agencyGroupOptions;
		}
		set;
	}
	
	public void searchAgencies(){
		selectedAgency = '';
		listUsers.clear();
	}
	
	private set<id> agenciesId;
	public String selectedAgency {get{if(selectedAgency == null) selectedAgency = userDetails.Contact.AccountId; return selectedAgency;}set;}
	public List<SelectOption> agencyOptions {
		get{
			if(selectedAgencyGroup != null){
				agencyOptions = new List<SelectOption>();				
				agenciesId = new Set<ID>();
				for(Account ag : [Select Id, Name from Account where ParentId = :selectedAgencyGroup  and RecordType.Name = 'Agency']){
					agencyOptions.add(new SelectOption(ag.Id, ag.Name));
					agenciesId.add(ag.Id);
					if(selectedAgency == '')
						selectedAgency = ag.id;
				}
				
				listUsers = listUsers;
			}
			return agencyOptions;
		}
		set;
	}
	
	public void searchUsers(){
		listUsers = null;
	}
	
	public String selectedUser {get; set;}
	private map <string,User> userXContact {get;set;}
	public List<SelectOption> listUsers {
		get{
			if(selectedAgency != null){
				listUsers = new List<SelectOption>();				
				userXContact = new map<string,User>();
				
				for(User acc : [Select id, Name, ContactId, Contact.AccountId from User where AccountId = :selectedAgency and IsActive = true and Contact.Chatter_Only__c = false order by name]){
					listUsers.add(new SelectOption(acc.id, acc.Name));
					userXContact.put(acc.id, acc);
				}
			}
			return listUsers;
		}
		set;
	}
	
	public List<Contact> emptyList {
		get {
			return new List<Contact>();
		}
	}
	
	
	public Boolean saved {
		get{
			if(saved == null)
				saved = false;
			return saved;
		}
		Set;
	}
	
	public Set<String> idContactsToRDStation{get;set;}

	public void assign(){
		idContactsToRDStation = new Set<String>();
		List<Contact> toUpdate = new List<Contact>();	

		for(Contact l : leadsToAssign)
			if(l.isSelected__c)
				toUpdate.add(l);
		
		if(toUpdate.isEmpty()){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select the leads to be assign.');
    		ApexPages.addMessage(myMsg);			
			return;
		}
		if(selectedUser == null || selectedUser == ''){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select one user.');
    		ApexPages.addMessage(myMsg);			
			return;
		}
		
		IPFunctions.ContactNoSharing cns = new IPFunctions.ContactNoSharing();
		User transferToContact = [SELECT AccountID, contactID, Contact.Account.Name, Contact.AccountId, ID, name, Contact.Account.Parent.Synchronize_RDStation_Change_Ownership__c, Contact.Account.ParentID, Contact.Account.Parent.RDStation_Synchronize_Incoming_Leads__c, Contact.Account.Parent.Create_New_Cycle_Incoming_Leads__c, Contact.Account.Parent.Check_New_Lead_On_New_Cycle__c FROM User WHERE ID = :selectedUser];
		User transferedBy = [SELECT contactID, Contact.Account.ParentID, Contact.Account.Name, Contact.AccountId, ID, name, Contact.Account.Parent.Synchronize_RDStation_Change_Ownership__c FROM User WHERE ID = :UserInfo.getUserId()];
		User transferedFrom = null;
		String idUser;
		Set<String> idUsers = new Set<String>();
		Set<String> idUsersContacts = new Set<String>();
		for(Contact ac : toUpdate){
			if(ac.RecordType.Name == 'Lead'){
				if(!String.isEmpty(ac.Lead_Assignment_To__r.ID)){
					idUsersContacts.add(ac.Lead_Assignment_To__r.ID);
				}
			}else if(ac.RecordType.Name == 'Client'){
				if(!String.isEmpty(ac.Owner__r.ID)){
					idUsers.add(ac.Owner__r.ID);
				}
			}
		}
		system.debug('ID USERS FOUND '+idUsers);
		Map<String, User> users = new Map<String, User>();
		for(User usr : [SELECT contactID, Contact.Account.Name, Contact.AccountId, ID, name, Contact.Account.ParentID FROM User WHERE ID in :idUsers]){
			users.put(usr.ID, usr);
		}
		for(User usr : [SELECT contactID, Contact.Account.Name, Contact.AccountId, ID, name, Contact.Account.ParentID FROM User WHERE contactID in :idUsersContacts]){
			users.put(usr.contactID, usr);
		}
		system.debug('ALL USERS FOUND '+users);
		String action = null;
		Map<String, IPClasses.RDStationHifyData> rdStationFields = null;
		
		Boolean syncWithRd;
		Boolean verifyAndCreateNewCycles = false;
		String idGroupToSync = transferToContact.Contact.Account.ParentID;
		if(transferedBy.Contact.Account.ParentID == transferToContact.Contact.Account.ParentID){
			syncWithRd = transferToContact.Contact.Account.Parent.Synchronize_RDStation_Change_Ownership__c;
		}else{
			verifyAndCreateNewCycles = transferToContact.Contact.Account.Parent.Create_New_Cycle_Incoming_Leads__c;
			syncWithRd = transferToContact.Contact.Account.Parent.RDStation_Synchronize_Incoming_Leads__c;
		}

		Map<String, Contact> contactsById = new Map<String, Contact>();
		for(Contact ac : toUpdate){
			contactsById.put(ac.ID, ac);
			User emp = userXContact.get(selectedUser);
			ac.Lead_Assignment__c = 'Assigned';
			ac.Lead_Assignment_By__c = userDetails.ContactId;
			ac.Lead_Assignment_On__c = System.now();
			ac.Lead_Assignment_To__c = emp.ContactId;
			ac.isSelected__c = false;
			ac.Current_Agency__c = emp.Contact.AccountId;	
			
			if(ac.Original_Agency__c == null){ 
				//ac.Original_Agency__c = userDetails.Contact.AccountID;
				//ac.AccountId = userDetails.Contact.AccountID;
				ac.AccountId = selectedAgency;
			}else if(userDetails.Contact.Account.ParentId != selectedAgencyGroup && !agencyGroupType.get(selectedAgencyGroup))
					ac.AccountId = selectedAgency;

			transferedFrom = null;
			
			if(ac.RecordType.Name == 'Lead'){
				if(!String.isEmpty(ac.Lead_Assignment_To__r.ID)){
					transferedFrom = users.get(ac.Lead_Assignment_To__r.ID);
				}
			}else if(ac.RecordType.Name == 'Client'){
				if(!String.isEmpty(ac.Owner__r.ID)){
					transferedFrom = users.get(ac.Owner__r.ID);
				}
			}

			action = 'Assigned';		
			if(transferedFrom != null && transferedFrom.ID == transferedBy.ID){
				action = 'Given';
			}
			if(transferToContact.ID == transferedBy.ID){
				action = 'Taken';
			}

			ac.Ownership_History_Field__c = JSON.serialize(cns.transferOwnerShip(ac, transferToContact, transferedFrom, transferedBy, action));
					
			ac.Owner__c = selectedUser;

			if(syncWithRd){
				idContactsToRDStation.add(ac.ID); 
			}	
		}
		
		update toUpdate;

		List<Destination_Tracking__c> cycles = [SELECT ID, Name, Current_Agency__c, Client__c FROM Destination_Tracking__c WHERE Client__c IN :contactsById.keySet() AND Current_Cycle__c = true AND Agency_Group__c = :transferToContact.Contact.Account.ParentID];
		
		Set<String> contactsWithCycle = new Set<String>();

		if(cycles != null && !cycles.isEmpty()){
			for(Destination_Tracking__c cycle : cycles){
				contactsWithCycle.add(cycle.Client__c);
				cycle.Current_Agency__c = transferToContact.Contact.AccountId;
			}
			update cycles;
		}

		if(verifyAndCreateNewCycles){
			List<Contact> contactsToCreateCycle = new List<Contact>();
			for(String idContact : contactsById.keySet()){
				if(!contactsWithCycle.contains(idContact)){
					contactsToCreateCycle.add(contactsById.get(idContact));
				}
			}
			if(!contactsToCreateCycle.isEmpty()){
				cycles = new List<Destination_Tracking__c>();
				Destination_Tracking__c newCycle;
				for(Contact ctt : contactsToCreateCycle){
					
					newCycle = new Destination_Tracking__c();
		
					newCycle.Client__c = ctt.ID;
					newCycle.Current_Cycle__c = true;
					newCycle.Current_Agency__c = transferToContact.Contact.AccountID;
					newCycle.Created_By_Agency__c = transferToContact.Contact.AccountID;
					newCycle.Agency_Group__c = transferToContact.Contact.Account.ParentID;
					//newCycle.Destination_Country__c = IPFunctions.NO_DESTINATION_DEFINED;
					newCycle.Lead_Last_Change_Status_Cycle__c = Datetime.now();
					newCycle.Lead_Last_Stage_Cycle__c = 'Stage 0';
					newCycle.Lead_Last_Status_Cycle__c = 'NEW LEAD';

					cycles.add(newCycle);
				}
				insert cycles;
				if(transferToContact.Contact.Account.Parent.Check_New_Lead_On_New_Cycle__c){
					try{
						Client_Stage__c newLeadItem = [SELECT ID, Stage__c, Stage_description__c FROM Client_Stage__c WHERE Agency_Group__c = :transferToContact.Contact.Account.ParentID AND Stage_description__c = 'NEW LEAD' AND Stage__c = 'Stage 0' LIMIT 1];

						List<Client_Stage_Follow_Up__c> followUps = new List<Client_Stage_Follow_Up__c>(); 
						Client_Stage_Follow_Up__c followUp = null;
						for(Contact ctt : contactsToCreateCycle){
							
							followUp = new Client_Stage_Follow_Up__c();

							followUp.Client__c = ctt.id;
							followUp.Checked_By__c = transferToContact.ID;
							followUp.agency__c = transferToContact.Contact.AccountId;
							followUp.Agency_Group__c = transferToContact.Contact.Account.ParentID;
							
							followUp.Stage_Item__c = newLeadItem.Stage_description__c;
							followUp.Stage_Item_Id__c = newLeadItem.ID;
							followUp.Stage__c = newLeadItem.Stage__c;
							followUp.Last_Saved_Date_Time__c = Datetime.now();

							for(Destination_Tracking__c cycle : cycles){
								if(ctt.ID == cycle.Client__c){
									followUp.Destination__c = cycle.Destination_Country__c;
									followUp.Destination_Tracking__c = cycle.ID;
								}
							}
							followUps.add(followUp);
						}
						insert followUps;
					}catch(Exception ex){
						system.debug('ERROR RETRIEVING NEW LEAD CHECK. '+ex);
					}
				}
			}
		}

		saved = true;
		if(!idContactsToRDStation.isEmpty()){
			system.debug('SENDING THE LEAD TO THE GROUP '+idGroupToSync);
			RDStationSaveLeadWebhook.updateBulkWithRDStation(idContactsToRDStation, idGroupToSync);
		}

	}
	
	
	public User userDetails{
		get{
			if (userDetails == null)
				userDetails = [select Contact.AccountId, Contact.Account.Name, Contact.Account.ParentId, Contact.Account.Parent.Global_Link__c, Contact.Account.Parent.Destination_Group__c, Name, Email, Contact.Agency_Manager__c, Contact.Account.BillingCountry from User where id = :UserInfo.getUserId()];
			
			return userDetails;
		}
		set;
	}	

}