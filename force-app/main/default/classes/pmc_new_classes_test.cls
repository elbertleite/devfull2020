@isTest
private class pmc_new_classes_test {
    static testMethod void myUnitTest() {
		TestFactory tf = new TestFactory();
        
        Account agency = tf.createAgency();
        Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
        
        Map<String,String> recordTypes = new Map<String,String>();
        for( RecordType r :[select id, name from recordtype where isActive = true] ){
            recordTypes.put(r.Name,r.Id);
        }

        Contact client = tf.createClient(agency);

        Account schoolGroup = tf.createSchoolGroup();
        
        Account school = tf.createSchool();
        school.parentId = schoolGroup.id;
        update school;

        Account campus = tf.createCampus(school, agency);

        Course__c course = tf.createCourse();
        Campus_Course__c cc = tf.createCampusCourse(campus, course);

        Course_Price__c price = tf.createCoursePrice(cc, 'Published Price');

        Start_Date_Range__c sd = tf.createProdDateRange(agency, new Quotation_List_Products__c());

        Quotation__c quote = tf.createQuotation(client, cc);

        testSchoolPageHeader(campus, school);
        testSchoolDashboard(campus, school);
        testSchoolContacts(campus, school, client);
        testSchoolCourses(campus, course);
	}
    public static void testSchoolCourses(Account campus, Course__c course){
        Apexpages.currentPage().getParameters().put('id', campus.id);
        school_campus_courses controller = new school_campus_courses();
        controller.openCloseEditCourse();
        controller.openCloseUpdatePackages();
        controller.openCloseEditAll();
        controller.getPeriods();
        controller.getCourses();
        controller.getCourseHours(course);
        controller.saveAllCourses();
        //controller.updateCoursesInCampuses();
        controller.openCloseCloneCourses();
    } 
    public static void testSchoolContacts(Account campus, Account school, Contact ctt){
        Apexpages.currentPage().getParameters().put('id', school.id);
        school_campus_contact controller = new school_campus_contact();
        controller.openCloseModalNewContact();
        Apexpages.currentPage().getParameters().put('contactID', ctt.id);
        Apexpages.currentPage().getParameters().put('campusID', campus.id);
        controller.saveContact();
        controller.openEditContact();
        controller.addNewOtherForm();
        controller.getListTypesOtherContact();
        controller.updateTypeFormContact();
        controller.deleteContact();
        controller.deleteOtherForm();
        controller.saveContact();
        List<SelectOption> allContactTypes = controller.allContactTypes;
    }
    public static void testSchoolPageHeader(Account campus, Account school){
        Apexpages.currentPage().getParameters().put('id', campus.id);
        school_page controller = new school_page();

        controller.saveDetails();
        controller.openCloseForEdition();
        controller.deleteSchoolLogo();

        Apexpages.currentPage().getParameters().put('id', school.id);
        controller = new school_page();
        controller.updateSchoolNewLogo = true;
        controller.savedPathways = new List<IPClasses.SavedItemCampus>();
    }
    public static void testSchoolDashboard(Account campus, Account school){
        school_dashboard controller = new school_dashboard();
        school_dashboard.initPage(school.id);
        school_dashboard.retrieveCourses(school);
        school_dashboard.generateDashboard(school.id, 'year', 2014, 2015, null);
        school_dashboard.generateDashboard(campus.id, 'group', 2014, 2015, null);
        school_dashboard.retrieveAgencyGroupsFromQuotations(school, 2014, 2015);

        new school_dashboard.ChartResponse('', '', 10);

        school_dashboard.Enrolment enrol = new school_dashboard.Enrolment();
        enrol.id = '';
        enrol.agency = '';
        enrol.course = '';
        enrol.dateEnrolment = Datetime.now();

        school_dashboard.CourseResponse cr = new school_dashboard.CourseResponse();
        cr.name = 'null';
        cr.available = null;
        cr.show = null;
        cr.quotations = null;
        cr.enrolments = null;

        cr.equals(cr);
        cr.hashCode();
    }
}