public without sharing class commissions_pending_invoices {

	public list<schoolResult> invResult {get;set;}
	public boolean showError {get{if(showError == null) showError = false; return showError;}set;}
	public Integer totSendInvoice {get;set;}
	public Integer totPendingConf {get;set;}
	public Integer totToRequest {get;set;}
	private String unconfirmId {get;set;}
	public integer totResult {get;set;}
	public Invoice__c invReason {get{if(invReason==null) invReason = new Invoice__c(); return invReason;}set;}

	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}
	
	//Currencies
	public DateTime currencyLastModifiedDate {get;set;}
	public list<SelectOption> mainCurrencies {get;set;}
	private Map<String, double> agencyCurrencies;
	//

	public list<SelectOption> pendingOptions {get{
		if(pendingOptions == null){
			pendingOptions = new list<SelectOption>();
			pendingOptions.add(new SelectOption('none','-- Select Option --'));
			pendingOptions.add(new SelectOption('confirm','Payment Received'));
			pendingOptions.add(new SelectOption('sendEmail','Send Email'));
			pendingOptions.add(new SelectOption('cancelInvoice','Cancel Invoice'));
		}
		return pendingOptions;
	}set;}

	private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}

	private map<Integer, list<ID>> pageIds;
	private map<String, list<String>> bo {get;set;}
	public Integer currentPage {get{if(currentPage==null) currentPage = 1; return currentPage;}set;}
	public list<Integer> totPages {get;set;}

	public list<SelectOption> backOfficeOptions{get; set;}
	public string selectedBackOffice{get; set;}

	//Constructor
	public commissions_pending_invoices() {
		// bo = ff.findBOAgenciesPerService(currentUser.Contact.AccountId, new list<String>{'PDS_PCS_PFS_Chase'});

		backOfficeOptions = FinanceFunctions.retrieveBackOfficeOptions();

		String pBO = ApexPages.currentPage().getParameters().get('bk') != null && ApexPages.currentPage().getParameters().get('bk') != '' ? ApexPages.currentPage().getParameters().get('bk') : currentUser.Contact.AccountId;
		
		for(SelectOption so : backOfficeOptions)
			if(selectedBackOffice == null || so.getValue() == pBO)
				selectedBackOffice = so.getValue();

		findBackoffice();	
		checkFilters();

		mainCurrencies = ff.retrieveMainCurrencies();
        agencyCurrencies = ff.retrieveAgencyCurrencies();
		system.debug('agencyCurrencies==>' + agencyCurrencies);
		currencyLastModifiedDate = ff.currencyLastModifiedDate;

		searchPendingInvoices();
	}

	private void findBackoffice(){
		bo = ff.findBOAgenciesPerService(selectedBackOffice, new list<String>{'PDS_PCS_PFS_Chase'});
	}

	public void changeBackOffice(){
		findBackoffice();
		allFilters = ff.pdsFlowBOFilters(bo);
		countryOptions = allFilters.get('allCountries');
		if(countryOptions != null && countryOptions.size() > 0){
			selectedCountry = countryOptions[0].getValue();
			changeCountry();
		}
		else{
			schGroupOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
			schoolOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
		}
	} 

	

	//Find Pending Commission Invoices
	public void searchPendingInvoices(){

		if(currentUser.Finance_Backoffice_User__c &&  (selectedSchoolGP!=null && selectedSchoolGP != 'none') || invoiceNumber.length()>0){

			invResult = new list<schoolResult>();
			totResult = 0;

			String sql = 'SELECT Id ';

			if(invoiceNumber.length()>0){
				invoiceNumber = invoiceNumber.replace(' ', '');
				list<String> allRequest = new list<String>(invoiceNumber.split(','));
				sql += ' FROM Invoice__c WHERE School_Invoice_Number__c in ( \''+ String.join(allRequest, '\',\'') + '\' ) AND Requested_by_Agency__c = \'' + selectedBackOffice + '\' ';

				if(selectedPaymentStatus=='unconfirmed')
					sql += ' AND Confirmed_Date__c = NULL '; //Commission Not Confirmed
				else if(selectedPaymentStatus=='confirmed')
					sql += ' AND Confirmed_Date__c != NULL '; //Commission Confirmed
			}

			else{
				
				if(searchName.length()>0){
					sql += ', (SELECT Id FROM client_course_instalments_req_comm_inv__r WHERE Client_Course__r.Client__r.Name LIKE  \'%'+ searchName.trim() +'%\' )';
				}

				sql+=' FROM Invoice__c WHERE Requested_by_Agency__c = \'' + selectedBackOffice + '\' AND Country__c = :selectedCountry AND isCommission_Invoice__c = TRUE ';
				

				if(selectedPaymentStatus=='unconfirmed'){
					sql+= ' AND ((Due_Date__c >= '+ IPFunctions.FormatSqlDateIni(dates.Commission_Due_Date__c); //Commission Due Date

					sql+= ' AND Due_Date__c <= '+ IPFunctions.FormatSqlDateFin(dates.Commission_Paid_Date__c); //Commission Due Date

					if(showOverdue)
						sql+= ' ) OR Due_Date__c = NULL OR Due_Date__c < ' + System.now().format('yyyy-MM-dd') + ' )';//Overdue
					else sql+= ' ))';

				}
				else if(selectedPaymentStatus=='confirmed'){
					sql+= ' AND (Due_Date__c >= '+ IPFunctions.FormatSqlDateIni(dates.Commission_Due_Date__c); //Commission Due Date

					sql+= ' AND Due_Date__c <= '+ IPFunctions.FormatSqlDateFin(dates.Commission_Paid_Date__c) + ' )'; //Commission Due Date
				}

				if(selectedPaymentStatus=='unconfirmed')
					sql += ' AND Confirmed_Date__c = NULL '; //Commission Not Confirmed
				else if(selectedPaymentStatus=='confirmed')
					sql += ' AND Confirmed_Date__c != NULL '; //Commission Confirmed

				if(selectedSchool!= null && selectedSchool != 'none' && selectedSchool != 'all')
					sql+=' AND ( School_Id__c =  \''+ selectedSchoolGP + '\' OR School_Id__c =  \''+ selectedSchool + '\')'; //Agency

				else if(selectedSchoolGP != 'all'){
					list<String> ids = new list<String>{selectedSchoolGP};
					for(SelectOption s : schoolOptions)
						ids.add(s.getValue());

					sql+=' AND School_Id__c IN ( \''+ String.join(ids, '\',\'') + '\' ) '; //Agency
				}

					

				sql += ' order by  School_Name__c, Due_Date__c NULLS First ' ;
			}

			system.debug('sql==>' + sql);

			list<Invoice__c> preResult = Database.query(sql); //used to double filter is there is a Name search
			list<Invoice__c> result;

			if(searchName.length()>0){
				result = new list<Invoice__c>();
				for(Invoice__c i : preResult)
					if(i.client_course_instalments_req_comm_inv__r.size()>0)
						result.add(i);
			}else{
				result = preResult;
			}

			totResult = result.size();

			/** C R E A T E 	P A G I N A T I O N **/
			Integer pageSize = 10;
			Integer counter = 1;
			Integer page = 1;
			list<ID> instId = new list<ID>();
			totPages = new list<Integer>();
			pageIds = new map<Integer, list<ID>>();

			for(Invoice__c i : result){
				instId.add(i.id);
				if(counter == pageSize){
					pageIds.put(page, instId);
					instId = new list<ID>();
					totPages.add(page);
					page++;
					counter = 1;
				}else
					counter++;
			}//end for


			//add the remaining ids
			if(counter > 1){
				pageIds.put(page, instId);
				totPages.add(page);
			}

			if(result.size()>0)
				findPaginationItems();
			// else
				// invResult.clear();

			list<String> boAgencies = bo.get(selectedCountry);
			agenciesUnderBackoffice = '';
			list<String> agenciesBO = new list<String>(); 
			
			integer boCount = 0;
			for(String agBo : boAgencies){
				
				list<String> ag = agBo.split(';-;');
				if(boCount>0)
					agenciesUnderBackoffice += ', ' + ag[1];
				else
					agenciesUnderBackoffice += ag[1];
				agenciesBO.add(ag[0]);
				boCount++;
			}//end for	
				
			totToRequest = ff.totPdsToRequest(agenciesBO, selectedCountry, selectedSchoolGP, selectedSchool, searchName, dates.Commission_Due_Date__c, dates.Commission_Paid_Date__c);

			totPendingConf = ff.totReqConfInv(selectedCountry, selectedSchoolGP, selectedSchool, searchName, dates.Commission_Due_Date__c, dates.Commission_Paid_Date__c, selectedBackOffice, schoolOptions);

			totSendInvoice = ff.totInstSendInv(agenciesBO, selectedCountry, selectedSchoolGP, selectedSchool, searchName, dates.Commission_Due_Date__c, dates.Commission_Paid_Date__c);
		}
		else{
			invResult = new list<schoolResult>();
			totToRequest = 0;
			totPendingConf = 0;
			totSendInvoice = 0;
		}
	}

	public string agenciesUnderBackoffice{get{if(agenciesUnderBackoffice == null) agenciesUnderBackoffice = ''; return agenciesUnderBackoffice;} set;} 

		/** G E T 	P A G E 	I T E M S **/

	public void findPaginationItems(){
		invResult = new list<schoolResult>();

		String paramPage = ApexPages.CurrentPage().getParameters().get('page');

		if(paramPage!=null && paramPage!=''){
			currentPage = integer.valueOf(paramPage);
			ApexPages.CurrentPage().getParameters().remove('page');
		}

		if(currentPage> totPages.size())
			currentPage = totPages.size();

		list<Id> allIds = pageIds.get(currentPage);
		String sql = 'SELECT Id, School_Invoice_Number__c, Sent_Email_To__c, Sent_On__c, Due_Date__c, Total_Emails_Sent__c, School_Name__c, Total_Value__c,	CurrencyIsoCode__c, Commission_Paid_Date__c, Invoice_Activities__c, createdDate, School_Id__c, Received_Currency__c, Received_Currency_Value__c, Transfer_Receive_Fee_Currency__c, Transfer_Receive_Fee__c, Received_Currency_Rate__c, Commission_Notes__c, '+

		'  (SELECT ID, Due_Date__c, Commission_Due_Date__c, Client_Course__c, Client_Course__r.Client__r.Name, Client_Course__r.School_Name__c, Client_Course__r.Client__r.Owner__r.Name, Number__c, Split_Number__c, Instalment_Value__c,  Client_Course__r.Course_Name__c, Extra_Fee_Value__c, Commission__c, Commission_Value__c, Commission_Tax_Value__c, Commission_Tax_Rate__c, isPDS__c, isPFS__c, isPCS__c, instalment_activities__c, Kepp_Fee__c, Discount__c, Scholarship_Taken__c, Tuition_Value__c, client_course__r.Commission_Tax_Name__c, client_course__r.Commission_Type__c, PDS_Confirmed_By__c, PDS_Confirmed_On__c, isInstalment_Amendment__c, isCommission_Amendment__c, Amendment_Related_Fees__c, Balance__c, Amendment_PFS_Adjustment__c, Commission_Adjustment__c, Commission_Tax_Adjustment__c, Original_Instalment__r.Commission_Value__c, Original_Instalment__r.Commission_Tax_Value__c, Original_Instalment__r.Balance__c, Original_Instalment__r.isCommission_charged_on_amendment__c, Original_Instalment__r.Tuition_Value__c, Original_Instalment__r.Extra_Fee_Value__c, Original_Instalment__r.Kepp_Fee__c, Original_Instalment__r.Instalment_Value__c, isPaidOffShore__c, Received_By_Country__c, client_course__r.Campus_Country__c FROM client_course_instalments_req_comm_inv__r order by Received_Date__c) '+

		'   FROM Invoice__c WHERE ID in  ( \''+ String.join(allIds, '\',\'') + '\' ) order by School_Name__c, Due_Date__c NULLS First';

		schoolResult schR;
		invoice id;
		map<string, schoolResult> mapResult = new map<string,schoolResult>();
		list<SelectOption> opt;
		list<SelectOption> instOptions;
		for(Invoice__c i : Database.query(sql)){
			id = new invoice();
			
			//C U R R E N C Y
			if(i.Commission_Paid_Date__c == NULL){
				i.Received_Currency__c = currentUser.Contact.Account.account_currency_iso_code__c;
				i.Transfer_Receive_Fee_Currency__c = i.Received_Currency__c;
				i.Received_Currency_Rate__c = agencyCurrencies.get(i.CurrencyIsoCode__c);
				ff.convertCurrency(i, i.CurrencyIsoCode__c, i.Total_Value__c, null, 'Received_Currency__c', 'Received_Currency_Rate__c', 'Received_Currency_Value__c');
			}

			id.inv = i;


			if(i.Sent_On__c!=null)
				id.lastEmail = date.newinstance(i.Sent_On__c.year(), i.Sent_On__c.month(), i.Sent_On__c.day());
			for(client_course_instalment__c cci : i.client_course_instalments_req_comm_inv__r)
				id.instalments.add(new installmentDetails(cci));
			/** Invoice Activities **/
			if(i.Invoice_Activities__c!=null && i.Invoice_Activities__c != ''){

				List<String> allAc = i.Invoice_Activities__c.split('!#!');

				for(Integer ac = (allAc.size() - 1); ac >= 0; ac--){//activity order desc

					instalmentActivity instA = new instalmentActivity();
					List<String> a = allAc[ac].split(';;');

					instA.acType = a[0];
					instA.acTo = a[1];
					instA.acSubject = a[2];
					if(instA.acType=='SMS') // saved date in system mode
						instA.acDate = new Contact(Lead_Accepted_On__c = Datetime.valueOfGmt(a[3]));
					else
						instA.acDate = new Contact(Lead_Accepted_On__c = Datetime.valueOf(a[3]));
					instA.acStatus = a[4];
					instA.acError = a[5];
					instA.acFrom = a[6];

					system.debug('single activity==' + instA);

					id.activities.add(instA);
				}
			}

			/** G R O U P 		P E R 		S C H O O L **/
			if(!mapResult.containsKey(i.School_Id__c))
				mapResult.put(i.School_Id__c, new schoolResult(i.School_Id__c, i.School_Name__c, id));
			else
				mapResult.get(i.School_Id__c).addInvoice(id);
			//invResult.add(inv);

		}//end for

		for(String sch : mapResult.keySet())
			invResult.add(mapResult.get(sch));

	}

	//Confirm Commission Invoice
	public void confirmCommission(){
		String invoice = ApexPages.currentPage().getParameters().get('invId');
		Savepoint sp;
		try{
			sp = Database.setSavepoint();
			for(schoolResult schr : invResult)
				for(invoice id : schr.invoices)
					if(id.inv.id == invoice){
						if(id.inv.Commission_Paid_Date__c == null){
							ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill the Commission Paid Date for the invoice ' +id.inv.School_Invoice_Number__c+ ' before Confirm.'));
							if(selectedPaymentStatus == 'confirmed'){
								selectedCountry = oldCountry;
								selectedSchoolGP = oldGroup;
								selectedSchool = oldSchool;
								selectedPaymentStatus = 'unconfirmed';
							}
							break;
						}
						else if(id.inv.Commission_Paid_Date__c > system.today()){
							ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'The Commission Received Date for the invoice ' +id.inv.School_Invoice_Number__c+ ' cannot be a future date.'));
							if(selectedPaymentStatus == 'confirmed'){
								selectedCountry = oldCountry;
								selectedSchoolGP = oldGroup;
								selectedSchool = oldSchool;
								selectedPaymentStatus = 'unconfirmed';
							}
							break;
						}
						else{

							Datetime datePayment = datetime.newInstance(id.inv.Commission_Paid_Date__c.year(), id.inv.Commission_Paid_Date__c.month(),id.inv.Commission_Paid_Date__c.day());
							String activity = 'Commission Received;;-;;Commission Received From School;;' + datePayment.format('yyyy-MM-dd HH:mm:ss') + ';;sent;;-;;' + currentUser.Name + '!#!';
							String activityPds = 'Commission Received;;-;;PDS Commission Received From School;;' + datePayment.format('yyyy-MM-dd HH:mm:ss') + ';;sent;;-;;' + currentUser.Name + '!#!';
							String activityPfs = 'Commission Received;;-;;PFS Commission Received From School;;' + datePayment.format('yyyy-MM-dd HH:mm:ss') + ';;sent;;-;;' + currentUser.Name + '!#!';
							String activityPcs = 'Commission Received;;-;;PCS Commission Received From School;;' + datePayment.format('yyyy-MM-dd HH:mm:ss') + ';;sent;;-;;' + currentUser.Name + '!#!';

							//U P D A T E 		I N V O I C E
							id.inv.Confirmed_By__c = UserInfo.getUserId();
							id.inv.Confirmed_Date__c = System.now();
							id.inv.Invoice_Activities__c += activity;
							update id.inv;

							//U P D A T E 		I N S T A L M E N T S
							map<string, list<client_course_instalment__c>> courseInstalments = new map<string, list<client_course_instalment__c>>();

							for(client_course_instalment__c cci : id.inv.client_course_instalments_req_comm_inv__r){
								cci.Commission_Confirmed_On__c = system.now();
								cci.Commission_Confirmed_By__c = UserInfo.getUserId();
								cci.Commission_Paid_Date__c = id.inv.Commission_Paid_Date__c;
								cci.Commission_Confirmed_By_Agency__c = currentUser.Contact.AccountId;
								cci.Status__c = 'Commission Confirmed';

								if(cci.isPDS__c)
									cci.instalment_activities__c += activityPds;
								else if(cci.isPFS__c)
									cci.instalment_activities__c += activityPfs;
								else if (cci.isPCS__c)
									cci.instalment_activities__c += activityPcs;

								if(cci.Received_By_Country__c != cci.client_course__r.Campus_Country__c)
									cci.isPaidOffShore__c = true;
								//Put on map to update all the client course with their instalments
								if(!courseInstalments.containsKey(cci.client_course__c))
									courseInstalments.put(cci.client_course__c, new list<client_course_instalment__c>{cci});
								else
									courseInstalments.get(cci.client_course__c).add(cci);

							}//end for instalments
							update id.inv.client_course_instalments_req_comm_inv__r;

							//U P D A T E 		C O U R S E S
							list<client_course__c> toUpdateCourse = new list<client_course__c>();
							for(client_course__c cc : [SELECT Id, Total_Paid__c, Total_Keep_Fee_Received__c, Total_Commission_Received__c, Total_Discount_Given__c, Total_GST_Paid__c,
																					Total_PDS_Commission_Received__c, Total_PFS_Commission_Received__c
																						FROM client_course__c WHERE id in :courseInstalments.keySet()]){

								decimal totalPaid = 0;
								decimal totalCommission = 0;
								decimal totalGST = 0;
								decimal totalKeepFee = 0;
								decimal totalDiscount = 0;
								decimal totalScholarship = 0;
								decimal totalPDSCommReceived = 0;
								decimal totalPFSCommReceived = 0;

								for(client_course_instalment__c i : courseInstalments.get(cc.Id)){
									totalPaid += i.Instalment_Value__c;
									totalKeepFee += i.Kepp_Fee__c;
									totalCommission += i.Commission_Value__c;
									totalDiscount += i.Discount__c;
									totalGST += i.Commission_Tax_Value__c;
									totalScholarship += i.Scholarship_Taken__c;
									if(i.isPDS__c || i.isPCS__c)
										totalPDSCommReceived += i.Commission_Value__c;
									else if(i.isPFS__c)
										totalPFSCommReceived += i.Commission_Value__c;

								}//end for

								if(cc.Total_PDS_Commission_Received__c==null)
									cc.Total_PDS_Commission_Received__c = totalPDSCommReceived;
								else
									cc.Total_PDS_Commission_Received__c += totalPDSCommReceived;

								if(cc.Total_PFS_Commission_Received__c==null)
									cc.Total_PFS_Commission_Received__c = totalPFSCommReceived;
								else
									cc.Total_PFS_Commission_Received__c += totalPFSCommReceived;

								toUpdateCourse.add(cc);
							}//end for courses

							update toUpdateCourse;
							searchPendingInvoices();
							break;
						}
					}
		}
		catch(Exception e){
			system.debug('Exception thrown: ' + e.getMessage());
			system.debug('Exception Line: ' + e.getLineNumber());
			Database.rollback(sp);
		}
		finally{
			ApexPages.currentPage().getParameters().remove('invId');
		}
	}

	//Set id to Unconfirm
	public void setIdToUnconfirm(){
		unconfirmId = ApexPages.currentPage().getParameters().get('unId');
		ApexPages.currentPage().getParameters().remove('unId');
	}
	//Unconfirm Commission Invoice
	public void unconfirmCommission(){
		SavePoint sp;
		try {
			sp = Database.setSavepoint();
			showError = false;
			map<string, list<client_course_instalment__c>> courseInstalments = new map<string, list<client_course_instalment__c>>();

			for(schoolResult schr : invResult)
				for(invoice i : schr.invoices)
					if(i.inv.id == id.valueOf(unconfirmId)){
						String activity = 'Revoked Commission Received;;-;;'+invReason.unconfirm_commission_reason__c+';;' + system.now().format('yyyy-MM-dd HH:mm:ss') + ';;error;;-;;' + currentUser.Name + '!#!';
						String activityPds = 'Revoked PDS Commission Received;;-;;'+invReason.unconfirm_commission_reason__c+';;' + system.now().format('yyyy-MM-dd HH:mm:ss') + ';;error;;-;;' + currentUser.Name + '!#!';
						String activityPfs = 'Revoked PFS Commission Received;;-;;'+invReason.unconfirm_commission_reason__c+';;' + system.now().format('yyyy-MM-dd HH:mm:ss') + ';;error;;-;;' + currentUser.Name + '!#!';
						String activityPcs = 'Revoked PCS Commission Received;;-;;'+invReason.unconfirm_commission_reason__c+';;' + system.now().format('yyyy-MM-dd HH:mm:ss') + ';;error;;-;;' + currentUser.Name + '!#!';

						i.inv.Confirmed_By__c =null;
						i.inv.Confirmed_Date__c = null;
						i.inv.Commission_Paid_Date__c = null;
						i.inv.Received_Currency__c = null;
						i.inv.Received_Currency_Value__c = null;
						i.inv.Transfer_Receive_Fee_Currency__c = null;
						i.inv.Transfer_Receive_Fee__c = null;
						i.inv.Received_Currency_Rate__c = null;
						i.inv.Invoice_Activities__c += activity;
						update i.inv;

						for(client_course_instalment__c cci : i.inv.client_course_instalments_req_comm_inv__r){
							cci.Commission_Confirmed_On__c = null;
							cci.Commission_Confirmed_By__c = null;
							cci.Commission_Confirmed_By_Agency__c = null;
							cci.Commission_Paid_Date__c = null;
							cci.Status__c = 'Unconfirmed Commission';

							if(cci.isPDS__c)
								cci.instalment_activities__c += activityPds;
							else if(cci.isPFS__c)
								cci.instalment_activities__c += activityPfs;
							else if (cci.isPCS__c)
								cci.instalment_activities__c += activityPcs;

							//Put on map to update all the client course with their instalments
							if(!courseInstalments.containsKey(cci.client_course__c))
								courseInstalments.put(cci.client_course__c, new list<client_course_instalment__c>{cci});
							else
								courseInstalments.get(cci.client_course__c).add(cci);
						}//end for instalments

						update i.inv.client_course_instalments_req_comm_inv__r;

						list<client_course__c> toUpdateCourse = new list<client_course__c>();

						for(client_course__c cc : [SELECT Id, Total_Paid__c, Total_Keep_Fee_Received__c, Total_Commission_Received__c, Total_Discount_Given__c, Total_GST_Paid__c, Total_PDS_Commission_Received__c
														FROM client_course__c WHERE id in :courseInstalments.keySet()]){

						decimal totalPaid = 0;
						decimal totalCommission = 0;
						decimal totalGST = 0;
						decimal totalKeepFee = 0;
						decimal totalDiscount = 0;
						decimal totalScholarship = 0;
						decimal totalPDSCommReceived = 0;

						for(client_course_instalment__c cci : courseInstalments.get(cc.Id)){
							totalPaid += cci.Instalment_Value__c;
							totalKeepFee += cci.Kepp_Fee__c;
							totalCommission += cci.Commission_Value__c;
							totalDiscount += cci.Discount__c;
							totalGST += cci.Commission_Tax_Value__c;
							totalScholarship += cci.Scholarship_Taken__c;
							totalPDSCommReceived += cci.Commission_Value__c;

						}//end for

						cc.Total_Paid__c -= totalPaid;
						cc.Total_PDS_Commission_Received__c -= totalPDSCommReceived;
						cc.Total_Keep_Fee_Received__c -= totalKeepFee;
						cc.Total_Commission_Received__c -= totalCommission;
						cc.Total_GST_Paid__c -= totalGST;

						toUpdateCourse.add(cc);
						}//end for

						invReason.unconfirm_commission_reason__c = null;
						update toUpdateCourse;
						showError = true;
						// searchPendingInvoices();
						break;
					}

		}
		catch(Exception e){
			Database.rollBack(sp);
			system.debug('Error===' + e);
			system.debug('Error Line ===' + e.getLineNumber());
		}
	}

	//Cancel Commission Invoice
	public void cancelInvoice(){
		showError = false;
		Savepoint sp;
		try{
			sp = Database.setSavepoint();
			String activity = 'Commission Invoice Cancelled;;-;;-;;' + system.now().format('yyyy-MM-dd HH:mm:ss') + ';;error;;-;;' + currentUser.Name + '!#!';
			map<id, client_course_instalment__c> instalments = new map<id, client_course_instalment__c>();
			Invoice__c invUpdate;
			for(schoolResult schr : invResult)
				for(invoice id : schr.invoices)
					if(id.inv.id == unconfirmId){
						invUpdate = id.inv;
						for(installmentDetails cci : id.instalments){
							cci.installment.instalment_activities__c += activity;
							if(cci.installment.isPDS__c){
								cci.installment.PDS_Confirmed__c = true;
							}
							instalments.put(cci.installment.id, cci.installment);
						}//end for instalment
						break;
					}

			//Check if any of the instalments has an Amendment that is not requested/received commission yet
			for(client_course_instalment__c cci : [SELECT ID, Original_Instalment__c, isInstalment_Amendment__c, isCommission_Amendment__c, isOverpayment_Amendment__c FROM client_course_instalment__c WHERE Original_Instalment__c in :instalments.keySet() AND Request_Commission_Invoice__c = null])
				if(!cci.isInstalment_Amendment__c && (cci.isCommission_Amendment__c || cci.isOverpayment_Amendment__c)) continue;
				else
					instalments.get(cci.Original_Instalment__c).isCommission_charged_on_amendment__c = true;

			update instalments.values();
			// deleteId(unconfirmId);

			// invUpdate.isCancelled__c = true;
			invUpdate.Cancel_Reason__c = invReason.Cancel_Reason__c;
			invUpdate.Cancel_Description__c = invReason.Cancel_Description__c;
			// update invUpdate;

			saveCancelledToS3(invUpdate);

			// searchPendingInvoices();

			showError = true;
			invReason = new Invoice__c();
		}
		catch(Exception e){
			system.debug('Error===>' + e.getMessage() + ' line==>' + e.getLineNumber());
			Database.rollback(sp);
		}
	}


	private void saveCancelledToS3(Invoice__c invoice){

		system.debug('invUpdate==>' + invoice);
		String cancelDetails = currentUser.Name + ';#;' +  system.now().format() + ';#;' + invoice.Cancel_Reason__c + ';#;' + invoice.Cancel_Description__c;

		PageReference pr = Page.commissions_school_commission_invoice;
		pr.getParameters().put('i', invoice.id);
		pr.getParameters().put('cancelDt', cancelDetails);
		Blob pdfFile = pr.getContentAsPDF();

		String fileName = currentUser.Contact.Account.Global_Link__c + '/'+ currentUser.Contact.AccountId + '/Commissions/RequestedSchool/Cancelled/';
		fileName += invoice.School_Invoice_Number__c + ';#;' + invoice.School_Name__c;

		EmailToS3Controller.saveEmailtoS3(pdfFile, Userinfo.getOrganizationId(), fileName, 'application/pdf');

		Database.delete(invoice.id);
	}


	//R E C A L C U L A T E       R A T E
    public String paramSchool {get;set;}
    public String paramInvoice {get;set;}
    public void recalculateRate(){

      system.debug('@paramCountry==>'+ paramSchool);
      system.debug('@paramInvoice==>'+ paramInvoice);

      for(schoolResult schr : invResult)
        if(schr.schoolId == paramSchool)
          for(invoice cinv : schr.invoices)
			if(cinv.inv.id == paramInvoice){

				ff.convertCurrency(cinv.inv, cinv.inv.CurrencyIsoCode__c, cinv.inv.Total_Value__c, null, 'Received_Currency__c', 'Received_Currency_Rate__c', 'Received_Currency_Value__c');

				paramSchool = null;
				paramInvoice = null;
				break;
			}
    }


	/********************** Inner Classes **********************/

	public class schoolResult{
		public String schoolId {get;set;}
		public String schoolName {get;set;}
		public list<invoice> invoices {get{if(invoices==null) invoices= new list<invoice>(); return invoices;}set;}
		public decimal totalAmount {get{if(totalAmount==null) totalAmount= 0; return totalAmount;}set;}

		public schoolResult (String schoolId, String schoolName, invoice i){
				this.schoolId = schoolId;
				this.schoolName = schoolName;
				this.invoices.add(i);
				this.totalAmount = i.inv.Total_Value__c;
		}

		public void addInvoice(invoice i){
				this.invoices.add(i);
				this.totalAmount += i.inv.Total_Value__c;
		}
	}


	public class invoice{
		public Invoice__c inv {get;set;}
		public Date lastEmail {get;set;}
		public list<instalmentActivity> activities {get{if(activities == null) activities = new list<instalmentActivity>(); return activities;} set;}
		public list<installmentDetails> instalments {get{if(instalments == null) instalments = new list<installmentDetails>(); return instalments;} set;}
	}

	public class installmentDetails{
		public client_course_instalment__c installment{get;set;}
		public decimal tuitionValue {get;set;}
		public decimal extraFeeValue {get;set;}
		public decimal keepFeeValue {get;set;}
		public decimal instalmentValue {get;set;}
		public decimal commissionValue {get;set;}
		public decimal taxValue {get;set;}
		public decimal totalEarned {get;set;}

		public installmentDetails(client_course_instalment__c installment){

			if(installment.Commission_Adjustment__c==null)installment.Commission_Adjustment__c=0;
			if(installment.Commission_Tax_Adjustment__c==null)installment.Commission_Tax_Adjustment__c=0;

			this.installment = installment;
			this.tuitionValue = installment.Tuition_Value__c;
			this.extraFeeValue = installment.Extra_Fee_Value__c;
			this.keepFeeValue = installment.Kepp_Fee__c;
			this.instalmentValue = installment.Instalment_Value__c;
			this.commissionValue = installment.Commission_Value__c + installment.Commission_Adjustment__c;
			this.taxValue = installment.Commission_Tax_Value__c + installment.Commission_Tax_Adjustment__c ;
			this.totalEarned = installment.Balance__c;

			if((installment.Original_Instalment__c !=null && installment.Original_Instalment__r.isCommission_charged_on_amendment__c) || installment.Amendment_PFS_Adjustment__c>0){
				this.tuitionValue +=  installment.Original_Instalment__r.Tuition_Value__c;
				this.extraFeeValue += installment.Original_Instalment__r.Extra_Fee_Value__c;
				this.keepFeeValue += installment.Original_Instalment__r.Kepp_Fee__c;
				this.instalmentValue += installment.Original_Instalment__r.Instalment_Value__c;
				this.commissionValue += installment.Original_Instalment__r.Commission_Value__c;
				this.taxValue +=  installment.Original_Instalment__r.Commission_Tax_Value__c;
				this.totalEarned += installment.Original_Instalment__r.Balance__c;
			}

		}
	}




	//Last Activity
	public class instalmentActivity{
		public string acType {get; set;}
		public Contact acDate {get; set;}
		public string acStatus {get; set;}
		public string acError {get; set;}
		public string acTo {get; set;}
		public string acFrom {get; set;}
		public string acSubject {get; set;}
	}

	/********************** Filters **********************/
	public String selectedCountry{get;set;}
	public String selectedSchoolGP{get;set;}
	public String selectedSchool{get;set;}
	public String selectedCity{get;set;}
	public String selectedCampus{get;set;}
	public String selectedAgencyGroup {get;set;}
	public String selectedAgency {get;set;}
	public String selectedPayment {get{if(selectedPayment==null) selectedPayment = 'all'; return selectedPayment;}set;}
	public String selectedPaymentStatus {get{if(selectedPaymentStatus==null) selectedPaymentStatus = 'unconfirmed'; return selectedPaymentStatus;}set;}
	public String searchName {get{if(searchName==null) searchName = ApexPages.CurrentPage().getParameters().get('nm'); return searchName;}set;}
	public String invoiceNumber {get{if(invoiceNumber==null) invoiceNumber = ''; return invoiceNumber;}set;}
	public string selectedDateFilterBy{
      get{
        if(selectedDateFilterBy == null)
          selectedDateFilterBy = 'commdue';
        return selectedDateFilterBy;
      }
      set;
    }

     public boolean showOverdue{
      get{
        if(showOverdue == null)
          showOverdue = true;
        return showOverdue;
      }
      set;
    }

	public list<SelectOption> countryOptions {get;set;}
	public list<SelectOption> schGroupOptions {get;set;}
	public list<SelectOption> schoolOptions {get;set;}
	public list<SelectOption> paymentStatus {get{
		if(paymentStatus == null){
			paymentStatus = new list<SelectOption>();
			paymentStatus.add(new SelectOption('unconfirmed','Unconfirmed'));
			paymentStatus.add(new SelectOption('confirmed','Confirmed'));
		}
		return paymentStatus;
	}set;}

	public client_course_instalment__c dates{get{
      if(dates==null){

        dates = new client_course_instalment__c();

        String pIni = ApexPages.CurrentPage().getParameters().get('ini');
        String pFin = ApexPages.CurrentPage().getParameters().get('fin');
        
        if(pIni != null && pini!= ''){
          // list<String>dt = pIni.split('-');
          dates.Commission_Due_Date__c = date.valueOf(pini + ' 00:00:00');
        }
        else{
          dates.Commission_Due_Date__c = system.today();
        }

        if(pFin != null && pFin!= ''){
          // list<String>dt = pFin.split('-');
          dates.Commission_Paid_Date__c = date.valueOf(pFin + ' 00:00:00');
        }
        else{
          dates.Commission_Paid_Date__c = dates.Commission_Due_Date__c.addDays(14);
        }
      }
      return dates;
    }set;}

	private map<string,list<SelectOption>> schools {get;set;}

	 private map<string,list<SelectOption>> allFilters {get;set;}

	 private void checkFilters(){
		if(selectedPaymentStatus == 'unconfirmed')
      		allFilters = ff.pdsFlowBOFilters(bo);
		else if(selectedPaymentStatus == 'confirmed')
      		allFilters = ff.confirmCommissionFilters();

		countryOptions = allFilters.get('allCountries');
		if(countryOptions!= null && countryOptions.size()>0){
			String pCountry = ApexPages.CurrentPage().getParameters().get('cn');
			String pGroup = ApexPages.CurrentPage().getParameters().get('pGroup');

			//Get Country
			if(pCountry != null && pCountry != '')
				selectedCountry = pCountry;
			else
				selectedCountry = countryOptions[0].getValue();

			//Get School Group
			schGroupOptions = allFilters.get(selectedCountry);
			
			selectedSchoolGP = pGroup;
			if(selectedSchoolGP == null || selectedSchoolGP == '')
				selectedSchoolGP = schGroupOptions[0].getValue();

			//Get School 
			if(selectedSchoolGP != 'none' && selectedSchoolGP != 'all')
				schoolOptions = allFilters.get(selectedCountry+'-'+selectedSchoolGP);

			String pSchool = ApexPages.CurrentPage().getParameters().get('sch');

			if( pSchool != null && pSchool != ''){
				selectedSchool = pSchool;
			}
			else if(schoolOptions != null){
				selectedSchool = schoolOptions[0].getValue();
			}
		}
    }

	private String oldCountry;
	private String oldGroup;
	private String oldSchool;

	public void resetFilters(){
		oldCountry = selectedCountry;
		oldGroup = selectedSchoolGP;
		oldSchool = selectedSchool;
		schGroupOptions = null;
		schoolOptions = null;
		selectedSchoolGP = 'none';
		selectedCountry = 'none';
		selectedSchool = 'none';
		checkFilters();
	}

    // public void changeCountry(){
    //     schoolOptions = allFilters.get(selectedCountry);
    //     selectedSchool = schoolOptions[0].getValue();
    // }
	
	public void changeCountry(){
        schGroupOptions = allFilters.get(selectedCountry);
        selectedSchoolGP = schGroupOptions[0].getValue();
        schoolOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
    }
	//Change School Group
    public void changeSchoolGP(){
		system.debug('selectedSchoolGP===>' + selectedSchoolGP);

        if(selectedSchoolGP != 'none' && selectedSchoolGP != 'all'){
          schoolOptions = allFilters.get(selectedCountry+'-'+selectedSchoolGP);
        }
        else{
          schoolOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
        }
		
		if(schoolOptions!= null && schoolOptions.size()>0)
        	selectedSchool = schoolOptions[0].getValue();
    }

    /** Current User **/
  	private Id financeGlobalLink {get{
  		if(financeGlobalLink==null){
  			if(currentUser.Contact.Finance_Global_Link__c != null)
  				financeGlobalLink = currentUser.Contact.Finance_Global_Link__c;
  			else financeGlobalLink = currentUser.Contact.Account.Global_Link__c;
  		}
  		return financeGlobalLink;
  	}set;}

    private User currentUser {get{
      if(currentUser==null) {
        currentUser = ff.currentUser;

      }
      return currentUser;
    }set;}

	public map<Id,Account> findAccountNames(set<String> ids){
		map<Id,Account> accountMap = new map<Id,Account> ([Select Id, Name, BillingCity, ParentId FROM Account WHERE id in :ids]);
		return accountMap;
	}

	public void deleteId(String objId){
		Database.delete(objId);
	}
	
}