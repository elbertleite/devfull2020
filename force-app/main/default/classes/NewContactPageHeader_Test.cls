@isTest
private class NewContactPageHeader_Test{
	static testMethod void myUnitTest() {

		TestFactory tf = new TestFactory();
		
		Account agency = tf.createAgency();

		Account agencyGroup = tf.createAgencyGroup();

		tf.createChecklists(agency.Parentid);

		Contact emp = tf.createEmployee(agency);
		
		Contact lead = tf.createLead(agency, emp);
		lead.Ownership_History_Field__c = '[{"toUserID":"005O0000004bBaSIAU","toUser":"Patricia Greco","toAgencyID":"0019000001MjqIIAAZ","toAgency":"IP Australia Sydney (HO)","fromUserID":null,"fromUser":null,"fromAgencyID":null,"fromAgency":null,"createdDate":"2018-09-18T04:17:12.024Z","createdByUserID":"005O0000004bBaSIAU","createdByUser":"Patricia Greco","action":"Taken"}]';
		update lead;

		Contact client = tf.createClient(agency);

		User portalUser = tf.createPortalUser(emp);

		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);

       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);

		Quotation__c q1 = tf.createQuotation(lead, campusCourse);
		Quotation__c q2 = tf.createQuotation(lead, campusCourse);

		Destination_Tracking__c dt = new Destination_tracking__c();
		dt.Client__c = lead.id;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
		dt.Expected_Travel_Date__c = system.today().addMonths(3);
		dt.lead_Cycle__c = false;
		insert dt;

		Client_Checklist__c checklist = new Client_Checklist__c();
		checklist.Agency__c = agency.id;
		checklist.Agency_Group__c = agency.Parentid;
		checklist.Destination__c = lead.Destination_Country__c;
		checklist.Checklist_Item__c = 'aaa';
		checklist.Client__c = lead.id;
		checklist.Destination_Tracking__c = dt.id;
		insert checklist;

		Test.startTest();
     	system.runAs(portalUser){

			List<Client_Stage_Follow_up__c> followUps = new List<Client_Stage_Follow_up__c>();
			for(Client_Stage__c stage : [SELECT ID, Stage_Description__c FROM Client_Stage__c WHERE Agency_Group__c = : agency.Parentid]){
				Client_Stage_Follow_up__c cs = new Client_Stage_Follow_up__c();
				cs.Agency__c = agency.id;
				cs.Agency_Group__c = agency.Parentid;
				cs.Client__c = lead.id;
				cs.Destination__c = 'Australia';
				cs.Stage_Item__c = stage.Stage_Description__c;
				cs.Stage_Item_Id__c = stage.id;
				cs.Stage__c = 'Stage 1';
				cs.Destination_Tracking__c = dt.id;
				followUps.add(cs);
			}

			insert followUps;
		
			emp.Account = agency;
			portalUser.contact = emp;
			update portalUser;

			 Apexpages.currentPage().getParameters().put('id', lead.id);

			Client_Stage__c csc = new Client_Stage__c();
			csc.Stage_description__c = 'LEAD - Not Interested';
			csc.Agency_Group__c = portalUser.Contact.Account.ParentId;
			csc.Stage_Sub_Options__c = '["Option Not Interested 1","Option Not Interested 2","Option Not Interested 3"]';
			insert csc;

			NewContactPageHeader controller = new NewContactPageHeader();
			controller.checkContactRDStation();
			controller.convertToClientRDStation();
			controller.openCloseFormchangePossibleSchool();
			controller.openCloseModalTransferOwnership();
			controller.markLeadAsLost();
			controller.convertLeadToClient();
			controller.savePossibleSchool();
			controller.takeOwnership();
			controller.syncRDStation();
			controller.showHideProfilePhotoUpload();
			controller.updateAgenciesAndEmployees();
			NewContactPageHeader.checkActionIsValid('19/09/2018 9:25 AM', lead.id);
			controller.checkCurrentPage('contact_new_activity_log');
			controller.checkCurrentPage('contact_new_documents');
			controller.checkCurrentPage('contact_new_email_history');
			controller.checkCurrentPage('apex/test');
			controller.chcekStatusFromTimeline();
			
			Apexpages.currentPage().getParameters().put('agencyIdTransferOwnership', agency.id);
			controller.updateEmployees();
			controller.updateEmployee();
			controller.deleteImage();
			controller.getUserLogoPath();
			controller.transferOwnership();

			ClientPageController a1 = new ClientPageController();
			Batch_Client_Ownership a2 = new Batch_Client_Ownership();
			Batch_Fill_Empty_Status a3 = new Batch_Fill_Empty_Status();
			Batch_Fill_Gaps_Checklist a4 = new Batch_Fill_Gaps_Checklist();
			scPageHeaderController a5 = new scPageHeaderController();
			a5.accoID = agency.id;
			Account acco = a5.acco;
			Batch_Fix_Checked_Dates a6 = new Batch_Fix_Checked_Dates();
			Batch_Fix_Current_Cycles a7 = new Batch_Fix_Current_Cycles();
			Batch_Fix_Dates_Moved_Stage_Zero a8 = new Batch_Fix_Dates_Moved_Stage_Zero();
			Batch_Fix_RDStation_Conversions a9 = new Batch_Fix_RDStation_Conversions();
			Batch_Migrate_NotInterested_Stage0 a10 = new Batch_Migrate_NotInterested_Stage0();
			Batch_Migrate_Stage1_To_Stage0 a11 = new Batch_Migrate_Stage1_To_Stage0();
			Batch_Fill_Agency_On_Cycles a12 = new Batch_Fill_Agency_On_Cycles();
			Batch_Create_New_Cycles a13 = new Batch_Create_New_Cycles();		
			Batch_IP_Cloud_Permissions a14 = new Batch_IP_Cloud_Permissions();		
			new Batch_Social_Network();	
			new batch_AccountDocuments_IP_Urls();	
			new batch_AccountLogo_IP_Urls();	
			new batch_AccountPictureFiles_IP_Urls();	
			new batch_updateSDrive_previewLink();	
			new batch_updateSDrive_previewLink_doc();	

			batch_translations_scheduler batch = new batch_translations_scheduler();
            batch.execute(null);	
		}
	}
}