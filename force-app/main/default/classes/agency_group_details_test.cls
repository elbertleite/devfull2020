/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class agency_group_details_test {

    static testMethod void myUnitTest() {
        
        TestFactory tf = new TestFactory();
        
		Account agencyGroup = tf.createAgencyGroup();
		Account agency = tf.createSimpleAgency(agencyGroup);
		
		
		agency_group_details agd = new agency_group_details(new ApexPages.Standardcontroller(agency));
		
		agd.setEdit();
		agd.cancel();
		
		list<SelectOption> studentsEnrolledPL = agd.studentsEnrolledPL;
		list<SelectOption> socialNetworkPL = agd.socialNetworkPL;
		list<SelectOption> videoPL = agd.videoPL;
		list<SelectOption> destinations = agd.destinations;
		list<SelectOption> mainCustomersTypes = agd.mainCustomersTypes;
		list<SelectOption> recruitingStudents = agd.recruitingStudents;
		list<string> industryCertificates = agd.industryCertificates;
		
		
		agd.program = 'abc';
		agd.numStudents = '25';
		agd.addNumEnrolled();
		agd.programKey = 'abc';
		agd.delNumEnrolled();
		agd.networkName = 'mynetwork';
		agd.addSocialNetwork();
		agd.networkKey = 'mynetwork';
		agd.delSocialNetwork();
		agd.videoHostName = 'myvideo';
		agd.addVideo();
		agd.videoListKey = 'myvideo';
		agd.delVideo();
		
		//agd.vimeoThumbnail('123123');
		
		agd.agency.Name = 'EH Sydney 2';
		agd.saveGroup();
		System.assertEquals(agd.agency.Name, 'EH Sydney 2');
		
		
        
    }
}