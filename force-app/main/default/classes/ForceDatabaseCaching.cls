global class ForceDatabaseCaching{
    global ForceDatabaseCaching(){}
}
/*global class ForceDatabaseCaching implements Schedulable {
    
    public static Integer ATTEMPT_NUMBER = 1;
    public static final String HIFY_EMAIL = 'develop@educationhify.com';
    public static final String EMAIL_FROM = 'Education Hify';

    @ReadOnly
    global void execute(SchedulableContext sc) {
        String mailSubject;
        String mailBody;
        Datetime now = Datetime.now();
        Date today = now.Date();
        try{
            String ipBrazil = '0019000001MjqDX';
            Date initYear = Date.newInstance(today.year(), 1, 1);
            String query = 'SELECT Client__c, Destination_Tracking__c FROM Client_Stage_Follow_Up__c WHERE Stage__c = \'Stage 0\' AND DAY_ONLY(convertTimezone(Last_Saved_Date_Time__c)) >= :initYear AND DAY_ONLY(convertTimezone(Last_Saved_Date_Time__c)) <= :today ';

            List<Client_Stage_Follow_Up__c> result = Database.query(query);

            mailSubject = 'Service ForceDatabaseCaching finished successfully. Attempt number ' +ATTEMPT_NUMBER+ ' executed in '+IPFunctions.getCurrentEnvironment()+'.';
            mailBody = 'The scheduled service ForceDatabaseCaching finished successfully. Attempt number '+ATTEMPT_NUMBER+'. Service executed on '+today.format()+'. Rows returned: '+result.size()+'. Time took in seconds: '+IPFunctions.getTimeInSeconds(now);
            IPFunctions.sendEmail(EMAIL_FROM, HIFY_EMAIL, EMAIL_FROM, HIFY_EMAIL, null, mailSubject, mailBody);

            ATTEMPT_NUMBER = 1;
            result = null;
        }catch(Exception ex){
            mailSubject = 'Service ForceDatabaseCaching failed. Attempt number ' +ATTEMPT_NUMBER+ ' executed in '+IPFunctions.getCurrentEnvironment()+'.';
            mailBody = 'The scheduled service ForceDatabaseCaching failed.  Attempt number '+ATTEMPT_NUMBER+'. Service executed on '+today.format()+'. Time took in seconds: '+IPFunctions.getTimeInSeconds(now);
            IPFunctions.sendEmail(EMAIL_FROM, HIFY_EMAIL, EMAIL_FROM, HIFY_EMAIL, null, mailSubject, mailBody);
            
            ATTEMPT_NUMBER += 1;

            execute(sc);
        }

    }
}*/