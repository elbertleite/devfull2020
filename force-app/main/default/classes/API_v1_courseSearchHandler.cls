global class API_v1_courseSearchHandler extends api_manager {
    
    private Map<String, Decimal> campusRanking = new Map<String, Decimal>();
    private Map<String, Decimal> campusStudentRanking = new Map<String, Decimal>();
    private Map<String, boolean> campusPreferable = new Map<String, boolean>();
    
    global List<course> search(String schoolId, String listCampuses, String unitsSearch, String courseType, String datePay, String nation, String country, String city, String category, String courseField, String timetable, String ranking, String language, String studentRanking){
        
        List<course> courses = new List<course>();
        
        xCourseSearch courseSearch = new xCourseSearch();       
        courseSearch.isAPIRequest = true;
        courseSearch.eventIdSet = false;
        courseSearch.Nationalities = nation;
        courseSearch.selectedSearchOrder = 'campus';
        courseSearch.selectedSchoolTuitionOption = 'tuition';
        courseSearch.selectedSchoolType = 'all';
        courseSearch.listCourseType = new List<String>();
        
        courseSearch.destination = country;
        courseSearch.destinationCity = city;
        
        
        
        if(ranking != null){
            courseSearch.filterRanking = new List<Decimal>();
            for(String rank : ranking.split(','))
                if(rank != null && rank != '')
                    courseSearch.filterRanking.add(Decimal.valueOf(rank));
        }
        
        if(studentRanking != null){
            courseSearch.filterStudentRanking = new List<Decimal>();
            for(String rank : studentRanking.split(','))
                if(rank != null && rank != '')
                    courseSearch.filterStudentRanking.add(Decimal.valueOf(rank));
        }   
            
        
        courseSearch.selectedCourseCategory = category;
        if(courseSearch.selectedCourseCategory == 'Other'){
            
            courseSearch.selectedSchoolPaymentPrice = 'instal';
            
            if(unitsSearch.contains(',')){
                courseSearch.lenghtFrom = Integer.valueOf(unitsSearch.split(',')[0]);
                courseSearch.lenghtTo = Integer.valueOf(unitsSearch.split(',')[1]);
            } else {
            
                courseSearch.lenghtFrom = 0;
                courseSearch.lenghtTo = Integer.valueOf(unitsSearch);
            
            }
        } else {
            
            courseSearch.numberStudyUnitsSearch = Integer.valueOF(unitsSearch);
            
        }
        
        
        if(courseType != null && courseType != ''){
            courseSearch.selectedCourseArea = courseType;
            courseSearch.listCourseType.add(courseType);
        }
        
        if(courseField != null && courseField != '')
            courseSearch.selectedCourseType = new LIST<string>(courseField.split(','));
        
        
        if(unitsSearch == null || unitsSearch == '')
            unitsSearch = '1';
        
        
            
        if(timetable != null && timetable != '')
            courseSearch.selectedPeriods = new LIST<string>(timetable.split(','));
            
        if(schoolId != null)
            courseSearch.selectedSchool = new LIST<String>{schoolId};
        else {
            courseSearch.getlistSchools();
        }
        
        if(datePay != null && datePay != '')
             courseSearch.dateSearchValue.Expected_Travel_date__c = Date.parse(datePay);
        else 
            courseSearch.dateSearchValue.Expected_Travel_date__c = system.today();
        
        
        campusRanking = courseSearch.campusRanking;
        campusStudentRanking = courseSearch.campusStudentRanking;
        campusPreferable = courseSearch.campusPreferable;
        
        //CALL THE SEARCH!
        List<xCourseSearch.courseDetails> lCourseDetails = courseSearch.getCampusCourses();
        
        
        
        Map<String, Translation__c> translations = new Map<String, Translation__c>();
        Translation__c labelTranslation = new Translation__c();
        
        if(language != null && language != '' && !language.contains('en_US')){
            
            if(courseSearch.searchCampusCourseIDs != null && !courseSearch.searchCampusCourseIDs.isEmpty()){
                for(Translation__c tr : [select id, campus_course__c, course_category__c, course_form_of_delivery__c, course_language_level_required__c, course_name__c, course_period__c, course_type__c, course_field__c, 
                                                type__c, lb_unit_day__c, lb_unit_month__c, lb_unit_per__c, lb_unit_quarter__c, lb_unit_semester__c, lb_unit_subjects__c, lb_unit_week__c, lb_unit_year__c, Extra_Fees__c 
                                            from Translation__c where ( (Campus_Course__c in :courseSearch.searchCampusCourseIDs and type__c = 'course') or type__c = 'label') and Language__c = :language])
                    if(tr.type__c == 'course')
                        translations.put(tr.campus_course__c, tr);
                    else
                        labelTranslation = tr;
            }
            
            
            if(labelTranslation.Extra_Fees__c != null){
                for(String fee : labelTranslation.Extra_Fees__c.split('!#!')){
                    String[] f = fee.split('>');
                    translations.put(f[0], new Translation__c(Extra_Fees__c = f[1]));
                }
            }
        }
        
        
        
        
        if(lCourseDetails != null && !lCourseDetails.isEmpty())
            for(xCourseSearch.courseDetails xc : lCourseDetails)
                courses.add(populateCourse(xc, translations, labelTranslation));
        
        
        return courses;
    }
    
    
    
    private course populateCourse(xCourseSearch.courseDetails cd, Map<String, Translation__c> translations, Translation__c labelTranslation){
        
        CourseDetails courseDetails = new CourseDetails();
        School sc = new School();
        Campus ca = new Campus();
        
        sc.id = cd.schoolID;
        sc.name = cd.schoolName;
        sc.ranking = campusRanking != null && campusRanking.containsKey(cd.campusID) ? campusRanking.get(cd.campusID) : 0; 
        sc.studentRanking = campusStudentRanking != null && campusStudentRanking.containsKey(cd.campusID) ? campusStudentRanking.get(cd.campusID) : 0;
        //campus info 
        ca.id = cd.campusID;
        ca.name = cd.schoolCampusName;
        ca.country = cd.campusCountry;
        ca.city = cd.campusCity;
        ca.favourite = campusPreferable != null && campusPreferable.containsKey(cd.campusID) ? campusPreferable.get(cd.campusID) : false;
        
        //course info
        courseDetails.campusCourseId = cd.campusCourseId;
        
        if( translations.containsKey(cd.campusCourseId) ){
            Translation__c tr = translations.get(cd.campusCourseId);
            
            if(tr.course_name__c != null)
                courseDetails.name = tr.course_name__c;
            else 
                courseDetails.name = cd.courseNameNoHours;  
                
            if(tr.course_category__c != null)
                courseDetails.category = tr.course_category__c;
            else 
                courseDetails.category = cd.courseCategory;
            
            if(tr.course_type__c != null)
                courseDetails.type = tr.course_type__c;
            else 
                courseDetails.type = cd.courseType;
                
            if(tr.course_field__c != null)
                courseDetails.field = tr.course_field__c;
            else 
                courseDetails.field = cd.courseField;
                
            if(tr.course_period__c != null)
                courseDetails.period = tr.course_period__c;
            else 
                courseDetails.period = cd.coursePeriod; 
                
            if(tr.course_language_level_required__c != null)
                courseDetails.languageLevelRequired = tr.course_language_level_required__c;
            else 
                courseDetails.languageLevelRequired = cd.languageLevelRequired; 
            
            if(tr.course_form_of_delivery__c != null)
                courseDetails.formOfStudy = tr.course_form_of_delivery__c;
            else 
                courseDetails.formOfStudy = cd.formOfDelivery;
                
        } else {
            
            courseDetails.name = cd.courseNameNoHours;
            courseDetails.category = cd.courseCategory;
            courseDetails.type = cd.courseType;
            courseDetails.field = cd.courseField;
            courseDetails.period = cd.coursePeriod;
            courseDetails.languageLevelRequired = cd.languageLevelRequired;
            courseDetails.formOfStudy = cd.formOfDelivery;
            
        }
        
        
        
        //courseDetails.unitType = getUnitType(cd.courseUnitType, labelTranslation);
        courseDetails.unitType = cd.courseUnitType;
        courseDetails.courseCurrency = cd.campusCurrency;
        
        courseDetails.courseLengthInMonths = cd.numberCourseLenghtInMonths;
        courseDetails.courseLengthRemainingWeeks = cd.numberCourseLenghtRemainingWeeks;
        
        courseDetails.hoursPerWeek = cd.hoursPerWeek;
        courseDetails.optionalHoursPerWeek = cd.optionalHoursPerWeek;
        courseDetails.pricePerUnit = Decimal.valueOf(cd.totalTuition / cd.numberUnitsStudy).setScale(2);
        courseDetails.totalPrice = cd.totalValueCourse;
        courseDetails.totalExtraFees = cd.totalExtraFees;
        courseDetails.totalExtraFeePromotions = cd.totalExtraFeePromotions;
        
        courseDetails.totalUnits = cd.numberUnitsStudy;
        courseDetails.extraFreeWeek = cd.courseExtraFreeWeek ;
        courseDetails.freeUnits = cd.bestpromotionDeal.quantity;
        courseDetails.totalTuition = cd.totalTuition;
        courseDetails.onlySoldInBlocks = cd.onlySoldInBlocks;
        
        if(courseDetails.freeUnits > 0)
            courseDetails.units = courseDetails.totalUnits -  courseDetails.freeUnits;
        else 
            courseDetails.units = courseDetails.totalUnits;
        
        if(cd.onlySoldInBlocks){    
            if(courseDetails.freeUnits > 0)
                courseDetails.fullPrice = ( (cd.unitBasedCoursePrice / courseDetails.units) * courseDetails.totalUnits) + courseDetails.totalExtraFees;
            else
                courseDetails.fullPrice = cd.unitBasedCoursePrice + courseDetails.totalExtraFees;
        } else
            courseDetails.fullPrice = (courseDetails.totalUnits * cd.unitBasedCoursePrice) + courseDetails.totalExtraFees;  
            
            
        courseDetails.totalSavings = courseDetails.fullPrice - courseDetails.totalPrice;
            
            
        //instalment values for "Other" courses
        courseDetails.hasInstalments = cd.numberRemainingInstalments > 0;
        if(courseDetails.hasInstalments){           
            courseDetails.firstInstalmentValue = cd.firstInstalmentValue;
            courseDetails.numberRemainingInstalments = cd.numberRemainingInstalments;
            
            Decimal d = Decimal.valueOf(cd.instalmentsValue).setScale(2);
            
            courseDetails.instalmentsValue = Double.valueOf(d);
        }
        
        //promotions
        courseDetails.promotions = new List<ExtraFeePromotion>();
        for(String key : cd.promotionExtraFeeDiscount.keySet()){
            xCourseSearch.promotionDetails xcPromo = cd.promotionExtraFeeDiscount.get(key);
            
            ExtraFeePromotion p = new ExtraFeePromotion();
            
            p.name = xcPromo.Name;
            p.type = xcPromo.promotype;
            
            p.feeName = xcPromo.feeName;
            //p.feeDiscountValue = xcPromo.feeDiscountValue;
            
            p.quantity = xcPromo.quantity;
            //p.fromUnits = xcPromo.fromUnits;
            p.value = xcPromo.value;
            
            courseDetails.promotions.add(p);
        }
        
        
        //extra fees
        courseDetails.extraFees = new List<ExtraFee>();
        
        ExtraFee extraSchoolFee = new ExtraFee();
        extraSchoolFee.name = 'Extra School Fees';
        extraSchoolFee.type = 'Extra School Fees';
        extraSchoolFee.allowChangeUnits = false;
        extraSchoolFee.units = 1;
        extraSchoolFee.totalPrice = 0;
        
        for(string ks : cd.validExtraFees.keySet()){
            xCourseSearch.retrievedExtraFees ref = cd.validExtraFees.get(ks);
            
            if(ref.extraFee.Product__r.Name__c == null && ref.extraFee.Value__c == null) continue; //SOMEHOW this map contains empty extra fees(??). They shall not pass.
            
            if(ref.extraFee.Product__r.Website_Fee_Type__c != 'Enrolment Fee' && ref.extraFee.Product__r.Website_Fee_Type__c != 'Material Fee') {   
                            
                extraSchoolFee.totalPrice = (ref.extraFee.Value__c * ref.extraFee.From__c) + extraSchoolFee.totalPrice;
                
            } else {
            
                ExtraFee f = new ExtraFee();
                f.name = translations.containsKey(ref.extraFee.Product__r.Name__c) ? translations.get(ref.extraFee.Product__r.Name__c).extra_fees__c : ref.extraFee.Product__r.Name__c ;
                f.pricePerunit = ref.extraFee.Value__c;
                f.allowChangeUnits = ref.extraFee.Allow_change_units__c;
                f.units = ref.extraFee.From__c;
                if(f.allowChangeUnits)
                    f.totalPrice = f.pricePerUnit * f.units; 
                else
                    f.totalPrice = f.pricePerUnit;
                f.unitType = ref.extraFee.ExtraFeeInterval__c;
                f.type = ref.extraFee.Product__r.Website_Fee_Type__c;
                /*List<ExtraFee> relatedFees;
                
                f.extraFee = xc.validExtraFees.get(ks).extraFee;
                f.relatedExtraFee = xc.validExtraFees.get(ks).relatedExtraFee;*/
                
                courseDetails.extraFees.add(f);
            }           
        }
        
        if(extraSchoolFee.totalPrice > 0)
            courseDetails.extraFees.add(extraSchoolFee);
        
        
        Course course = new Course();
        course.schoolDetails = sc;
        course.campusDetails = ca;
        course.courseDetails = courseDetails;
        
        return course;
        
    }
    
    private String getUnitType(String courseType, Translation__c labelTranslation){
        String ut;
        
        if(courseType == 'Week')
            ut = isEmpty(labelTranslation.lb_unit_week__c) ? courseType : labelTranslation.lb_unit_week__c;
        else if(courseType == 'Day')
            ut = isEmpty(labelTranslation.lb_unit_day__c) ? courseType : labelTranslation.lb_unit_day__c;
        else if(courseType == 'Month')
            ut = isEmpty(labelTranslation.lb_unit_month__c) ? courseType : labelTranslation.lb_unit_month__c;
        else if(courseType == 'Quarter')
            ut = isEmpty(labelTranslation.lb_unit_quarter__c) ? courseType : labelTranslation.lb_unit_quarter__c;
        else if(courseType == 'Semester')
            ut = isEmpty(labelTranslation.lb_unit_semester__c) ? courseType : labelTranslation.lb_unit_semester__c;
        else if(courseType == 'Subjects')
            ut = isEmpty(labelTranslation.lb_unit_subjects__c) ? courseType : labelTranslation.lb_unit_subjects__c;
        else if(courseType == 'Year')
            ut = isEmpty(labelTranslation.lb_unit_year__c) ? courseType : labelTranslation.lb_unit_year__c;
        
        return ut;
    }
    
    
}