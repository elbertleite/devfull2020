/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SchoolCampusOverview_test {

    static testMethod void myUnitTest() {
        
    	School_Campus_Overview__c sco = new School_Campus_Overview__c();
    	sco.Country__c = 'Australia';
    	sco.City__c = 'Sydney';
    	sco.School_Campus_Name__c = 'BROWNS Sydney';
    	insert sco;
    	
    	Course_Overview__c co = new Course_Overview__c();
    	co.Course_Name__c = 'General English';
    	co.School_Campus_Overview__c = sco.id;
    	insert co;
    	
    	
    	ApexPages.currentPage().getParameters().put('country','Australia');
    	ApexPages.currentPage().getParameters().put('city','Sydney');
    	
        Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(sco);
        SchoolCampusOverview sc = new SchoolCampusOverview(controller);
        
        sc.country = 'Australia';
        sc.city = 'Sydney';
        
        List<SelectOption> dummy = sc.countryOptions;
 		dummy = sc.cityOptions;
 		
 		sc.newSchool.School_Campus_Name__c = 'APC Sydney';
 		sc.newCourse.Course_Name__c = 'IELTS';
 		sc.selectedSchoolOverview = 'new';
 		sc.save();
 		
 		sc.getCities();
 		
 		sc.newSchool.School_Campus_Name__c = 'BROWNS Sydney';
 		sc.save();
        
    }
}