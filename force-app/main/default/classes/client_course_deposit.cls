public class client_course_deposit {

	public client_course__c clientCourse{get; set;}


	private String clientId {get;set;}
	private String instRefund {get;set;}

	private contact ct;

	public client_course_instalment__c refundDetails {get;set;}

	//Currencies
	public DateTime currencyLastModifiedDate {get;set;}
	public list<SelectOption> mainCurrencies {get;set;}
	private Map<String, double> agencyCurrencies;
	//

	private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}
	private String depositId;
	public client_course_deposit(ApexPages.standardController controller){
		clientId = controller.getId();

		instRefund = ApexPages.currentPage().getParameters().get('rf');
		depositId = ApexPages.currentPage().getParameters().get('dp');

		if(instRefund!= null && instRefund != ''){
			refundDetails = [SELECT Id, Amendment_Receive_From_School__c, Commission_Value__c, Commission_Tax_Value__c, Kepp_Fee__c, Amendment_Total_Charge_Client__c, Commission_Confirmed_By__c, Commission_Confirmed_By__r.Name, Commission_Confirmed_By_Agency__r.Name, Commission_Paid_Date__c, Paid_to_Client_On__c, Paid_to_Client_By__c,
						Paid_to_Client_By_Agency__c, isOverpayment_Refunded__c, isInstalment_Amendment__c, client_course__r.CurrencyIsoCode__c, client_course__r.Client__r.Owner__r.AccountId, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, Transfer_Receive_Fee_Currency__c, Transfer_Receive_Fee__c FROM client_course_instalment__c WHERE id = :instRefund limit 1];

			mainCurrencies = ff.retrieveMainCurrencies();
			agencyCurrencies = ff.retrieveAgencyCurrencies();
			currencyLastModifiedDate = ff.currencyLastModifiedDate;

			//C U R R E N C Y
			refundDetails.Agency_Currency__c = currentUser.Contact.Account.account_currency_iso_code__c;
			refundDetails.Transfer_Receive_Fee_Currency__c = refundDetails.Agency_Currency__c;
			refundDetails.Agency_Currency_Rate__c = agencyCurrencies.get(refundDetails.client_course__r.CurrencyIsoCode__c);
			ff.convertCurrency(refundDetails, refundDetails.client_course__r.CurrencyIsoCode__c, refundDetails.Amendment_Receive_From_School__c, null, 'Agency_Currency__c', 'Agency_Currency_Rate__c', 'Agency_Currency_Value__c');
		}

		ct = [Select id, Owner__r.AccountId from contact where id = :clientId];
		clientCourse = loadClientCourse(clientId);

		if(refundDetails!=null)
			newDepositValue.CurrencyIsoCode__c = refundDetails.client_course__r.CurrencyIsoCode__c;
	}

	private client_course__c loadClientCourse(string paramId){

		try{
			client_course__c cCourse;

			String sql = 'Select Deposit_Description__c, Deposit_Total_Used__c, Deposit_Total_Value__c, Deposit_Total_Available__c, Deposit_Type__c, Deposit_Value_Track__c, Id, client__c, Client__r.name, Deposit_Reconciliated__c, CurrencyIsoCode__c, Deposit_Total_Refund__c, Client__r.Owner__r.AccountId from client_course__c ';

			if(refundDetails!=null){ //If it is an instalment refund, find a deposit w/ same currency as the course
				sql += ' WHERE client__c = :paramId and isDeposit__c = true and Deposit_Total_Available__c > 0 and CurrencyIsoCode__c = \''+ refundDetails.client_course__r.CurrencyIsoCode__c+'\' LIMIT 1';
				cCourse = Database.query(sql);
			}
			else if(depositId!=null && depositId != ''){
				sql += ' WHERE Id = :depositId  LIMIT 1';
				cCourse = Database.query(sql);
			}
			else{
				sql += ' WHERE client__c = :paramId and isDeposit__c = true and Deposit_Total_Available__c > 0  LIMIT 1';
				cCourse = Database.query(sql);
			}

			system.debug('cCourse==>' + sql);

		 	return cCourse;
		}
		catch(Exception e){
			String depCurrency = (refundDetails!=null) ? refundDetails.client_course__r.CurrencyIsoCode__c : currentUser.Contact.Account.Account_Currency_Iso_Code__c;
			return new client_course__c(isDeposit__c = true, client__c = paramId, CurrencyIsoCode__c = depCurrency);
		}
	}



	public PageReference overpaymenToDeposit(){
		Savepoint sp = Database.setSavePoint();
		try{

			//Validade Refund Received On

			// if(refundDetails.Commission_Paid_Date__c==null){
			if(refundDetails.Paid_to_Client_On__c==null){
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please set a date on the field <b>Received On</b>.'));
				return null;
			}

			//else if(refundDetails.Commission_Paid_Date__c> system.today()){
			else if(refundDetails.Paid_to_Client_On__c> system.today()){
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'<b>Received On</b> cannot be a future date.'));
				return null;
			}

			boolean isGeneralUser = false;
			if(!refundDetails.isInstalment_Amendment__c){
				isGeneralUser = FinanceFunctions.isGeneralUser(currentUser.General_User_Agencies__c, refundDetails.client_course__r.Client__r.Owner__r.AccountId);
                                    
				// if(!isGeneralUser){
				// 	refundDetails.Commission_Confirmed_By_Agency__c = currentUser.Contact.AccountId;
				// }else{
				// 	refundDetails.Commission_Confirmed_By_Agency__c = refundDetails.client_course__r.Client__r.Owner__r.AccountId;
				// }
				if(!isGeneralUser){
					refundDetails.Paid_to_Client_By_Agency__c = currentUser.Contact.AccountId;
				}else{
					refundDetails.Paid_to_Client_By_Agency__c = refundDetails.client_course__r.Client__r.Owner__r.AccountId;
				}

				// refundDetails.Commission_Confirmed_By__c = currentUser.Id;
				// refundDetails.Commission_Confirmed_On__c = system.now();
				refundDetails.Paid_to_Client_By__c = currentUser.Id;
				
				refundDetails.Booking_Closed_By__c = UserInfo.getUserId();
				refundDetails.Booking_Closed_On__c = system.now();
			}
			refundDetails.isOverpayment_Refunded__c = true;
			update refundDetails;


			/** Add Value on Deposit **/
			newDepositValue.Value__c = math.abs(refundDetails.Amendment_Total_Charge_Client__c);
			newDepositValue.isInstalment_Refund__c = true;
			if(!refundDetails.isInstalment_Amendment__c)
				// newDepositValue.Date_Paid__c = refundDetails.Commission_Paid_Date__c;
				newDepositValue.Date_Paid__c = refundDetails.Paid_to_Client_On__c;
			else
				newDepositValue.Date_Paid__c = system.today();

			addDepositValue();
			saveDeposit();

			// Show updated values
			refundDetails = [SELECT Id, Amendment_Receive_From_School__c, Commission_Value__c, Commission_Tax_Value__c, Kepp_Fee__c, Amendment_Total_Charge_Client__c, Commission_Confirmed_By__c, Paid_to_Client_On__c, Paid_to_Client_By__c,
						Paid_to_Client_By_Agency__c, Commission_Confirmed_By__r.Name, Commission_Confirmed_By_Agency__r.Name, Commission_Paid_Date__c, isOverpayment_Refunded__c, isInstalment_Amendment__c, client_course__r.CurrencyIsoCode__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, Transfer_Receive_Fee_Currency__c, Transfer_Receive_Fee__c FROM client_course_instalment__c WHERE id = :instRefund limit 1];

			return null;
		}catch(Exception e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
			system.debug('Error===>' + e.getMessage());
			system.debug('Error Line===>' + e.getLineNumber());

			Database.rollback(sp);
			return null;
		}

	}



	/** Deposit Payments **/
	public list<client_course_instalment_payment__c> listDepositValues {get{
		if(listDepositValues == null && clientCourse.id != null){
			listDepositValues = [Select Id, Value__c, Received_By__r.Name, Received_On__c, Received_By_Agency__r.Name, Payment_Type__c, Date_Paid__c, Confirmed_By__c, Confirmed_Date__c, CurrencyIsoCode__c,isInstalment_Refund__c FROM client_course_instalment_payment__c where Deposit__c = :clientCourse.id and isClient_Refund__c = false order by Date_Paid__c];

			if(listDepositValues==null)
				listDepositValues = new list<client_course_instalment_payment__c>();
		}else if(listDepositValues == null && clientCourse.id == null)
			listDepositValues = new list<client_course_instalment_payment__c>();
		return listDepositValues;
	}set;}
	/**/

	
	/** Add Deposit Value **/
	public pageReference addDepositValue(){

		newDepositValue.Received_On__c = system.Now();
		newDepositValue.Agency_Currency__c = newDepositValue.CurrencyIsoCode__c; 
		newDepositValue.Agency_Currency_Rate__c = 1;
		newDepositValue.Agency_Currency_Value__c = newDepositValue.Value__c;
		System.debug('==>newDepositValue.Payment_Type__c: '+newDepositValue.Payment_Type__c);
		System.debug('==>newDepositValue: '+newDepositValue);
		

		if(clientCourse.Deposit_Total_Value__c != null){
			clientCourse.Deposit_Total_Value__c += newDepositValue.Value__c;
			if(clientCourse.Deposit_Total_Refund__c != null){
				clientCourse.Deposit_Total_Value__c -= clientCourse.Deposit_Total_Refund__c;
			}
		}else{
			clientCourse.Deposit_Total_Value__c = newDepositValue.Value__c;
		}	

		boolean isGeneralUser = FinanceFunctions.isGeneralUser(currentUser.General_User_Agencies__c, clientCourse.Client__r.Owner__r.AccountId);
		string agencyClienttoSave;
		if(!isGeneralUser){
			agencyClienttoSave = currentUser.Contact.AccountId;
		}else{
			agencyClienttoSave = clientCourse.Client__r.Owner__r.AccountId;
		}
		//newDepositValue = new client_course_instalment_payment__c(Received_By__c = currentUser.Id, Received_By_Agency__c= agencyClienttoSave, Client__c = clientCourse.Client__c, CurrencyIsoCode__c = currentUser.Contact.Account.Account_Currency_Iso_Code__c);

		if(instRefund!= null && instRefund != ''){
			newDepositValue.Confirmed_By__c = currentUser.Id;
			newDepositValue.Confirmed_Date__c = system.now();
			if(!isGeneralUser)
				newDepositValue.Confirmed_By_Agency__c = currentUser.Contact.AccountId;
			else 
				newDepositValue.Confirmed_By_Agency__c = ct.Owner__r.AccountId;
		}
		depositSaved = false;

		listDepositValues.add(newDepositValue);
		
		newDepositValue = new client_course_instalment_payment__c(Received_By__c = currentUser.Id, Received_By_Agency__c = agencyClienttoSave, Client__c = clientCourse.Client__c, CurrencyIsoCode__c = currentUser.Contact.Account.Account_Currency_Iso_Code__c);

		return null;
	}
	/**/

	private list<string> valuesToDelete = new list<string>();
	/** Remove Deposit Value **/
	public pageReference removeDepositValue(){
		String payType = ApexPages.currentPage().getParameters().get('type');
		decimal value = decimal.valueOf(ApexPages.currentPage().getParameters().get('value'));
		String byUser = ApexPages.currentPage().getParameters().get('by');

		for(integer i = 0; i < listDepositValues.size(); i++)
			if(value== listDepositValues[i].Value__c && payType == listDepositValues[i].Payment_Type__c && byUser == listDepositValues[i].Received_By__c){
				clientCourse.Deposit_Total_Value__c -= listDepositValues[i].Value__c;
				valuesToDelete.add(listDepositValues[i].id);
				listDepositValues.remove(i);
				break;
			}
		depositSaved = false;

		return null;
	}
	/**/


	/** Save Deposit **/
	public void saveDeposit(){
		Savepoint sp;

		try{
			sp = Database.setSavepoint();

			//Insert deposit object
			clientCourse.isDeposit__c = true;

			if(clientCourse.Deposit_Total_Used__c==null) clientCourse.Deposit_Total_Used__c = 0;
			clientCourse.Deposit_Total_Available__c = clientCourse.Deposit_Total_Value__c - clientCourse.Deposit_Total_Used__c;

			// if(clientCourse.id==null)
			// 	clientCourse.CurrencyIsoCode__c = currentUser.Contact.Account.Account_Currency_Iso_Code__c;


			if(valuesToDelete.size() > 0)
				delete [Select id from client_course_instalment_payment__c where id in :valuesToDelete];

			valuesToDelete = new list<string>();

			//Insert deposit payment
			boolean reconciliated = true;

			for(client_course_instalment_payment__c pay : listDepositValues)
				if(pay.Confirmed_Date__c==null){
					reconciliated = false;
					break;
				}

			clientCourse.Deposit_Reconciliated__c = reconciliated;
			upsert clientCourse;

			for(client_course_instalment_payment__c pay : listDepositValues)
				pay.Deposit__c = clientCourse.id;

			upsert listDepositValues;
			listDepositValues = null;
			depositSaved = true;
			listDepositValues = null;

		}catch(Exception e){
			system.debug('Error===' + e);
			system.debug('Error===' + e.getLineNumber());
			Database.rollback(sp);
		}

	}
	/**/



	/** Cancel Deposit **/
	public pageReference cancelDeposit(){
		listDepositValues = null;
		depositSaved = true;
		clientCourse = loadClientCourse(clientId);
		return null;
	}
	//





	public Boolean depositSaved{get{if(depositSaved == null) depositSaved = true; return depositSaved;} Set;}

	private list<selectOption> depositListType;
	public list<selectOption> getdepositListType(){
		if(depositListType == null){
			depositListType = new list<selectOption>();
			Schema.DescribeFieldResult fieldResult = client_course__c.Deposit_Type__c.getDescribe();
			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
			for( Schema.PicklistEntry f : ple)
				if(f.getLabel().toLowerCase() != 'Client Scholarship' && f.getLabel().toLowerCase() != 'Covered by Agency')
					depositListType.add(new SelectOption(f.getLabel(), f.getValue()));
		}
		return depositListType;
	}


	public String clientName {get{
		if(clientName == null)
			clientName = [Select Name from Contact where id =:clientId limit 1].Name;
		return clientName;
	} set;}


	public Account agencyDetails {get{
		if(agencyDetails==null){

			agencyDetails = [SELECT Id, Name, Logo__c, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode, Registration_name__c, Trading_Name__c, Business_Number__c, Type_of_Business_Number__c From Account where Id = :currentUser.Contact.AccountId limit 1];
		}
		return agencyDetails;}set;}

	private User currentUser {get{
		if(currentUser==null)
			currentUser = ff.currentUser;

		return currentUser;}set;}


	public client_course_instalment_payment__c newDepositValue{
		get{
			if(newDepositValue == null && clientCourse != null){
				boolean isGeneralUser = FinanceFunctions.isGeneralUser(currentUser.General_User_Agencies__c, ct.Owner__r.AccountId);
				string agencyClienttoSave;
				if(!isGeneralUser){
					agencyClienttoSave = currentUser.Contact.AccountId;
				}else{
					agencyClienttoSave = ct.Owner__r.AccountId;
				}
				newDepositValue = new client_course_instalment_payment__c(Received_By__c = currentUser.Id, Received_By_Agency__c = agencyClienttoSave, Client__c = clientCourse.Client__c, CurrencyIsoCode__c = currentUser.Contact.Account.Account_Currency_Iso_Code__c);
				if(instRefund!= null && instRefund != ''){
					newDepositValue.Confirmed_By__c = currentUser.Id;
					newDepositValue.Confirmed_Date__c = system.now();
					if(!isGeneralUser)
						newDepositValue.Confirmed_By_Agency__c = currentUser.Contact.AccountId;
					else 
						newDepositValue.Confirmed_By_Agency__c = ct.Owner__r.AccountId;
				}
			}
			return newDepositValue;
		}
		set;
	}

}