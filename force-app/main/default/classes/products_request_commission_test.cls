@isTest
private class products_request_commission_test
{
	public static User portalUser {get{
	if (null == portalUser) {
	portalUser = [Select Id, Name, Contact.AccountId from User where email = 'test12345@test.com' limit 1];
	} return portalUser;} set;}

	@testSetup static void setup() {
		TestFactory tf = new TestFactory();
		tf.setupProdcutsTest();

		Contact client = [Select Id, Accountid From Contact limit 1];
	
		portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
		system.runAs(portalUser){
			
			//Add product to booking
			ApexPages.currentPage().getParameters().put('id', client.Id);
			products_search ps = new products_search();

			for(String av : ps.result.keySet())
				for(String cat : ps.result.get(av).keyset())
					for(products_search.products p : ps.result.get(av).get(cat)){
						p.product.Selected__c = true;

						for(Quotation_List_Products__c f : p.prodFees)
							f.isSelected__c = true;
					}

			ps.saveProduct();

			//Pay Product
			client_product_service__c cliProduct = [Select Id from client_product_service__c WHERE Related_to_Product__c = null limit 1];

			client_course_product_pay payProd = new client_course_product_pay(new ApexPages.StandardController(cliProduct));

			payProd.newPayDetails.Payment_Type__c = 'Creditcard';
			payProd.newPayDetails.Value__c = payProd.totalPay;
			payProd.addPaymentValue();
			payProd.paidByAgency.Paid_by_Agency__c = true;

			//Data manipulation
			for(client_product_service__c p : payProd.product){
				p.Paid_to_Provider_by_Agency__c = client.AccountId;
				p.Paid_to_Provider_on__c = system.today();
				p.isRefund_Requested__c = TRUE;
			}

			payProd.savePayment();
		}
	}

	static testMethod void createAndConfirm(){

		system.runAs(portalUser){
			products_request_commission p = new products_request_commission();

			string selectedAgencyGroup = p.selectedAgencyGroup;
			List<SelectOption> agencyGroupOptions = p.agencyGroupOptions;
			map<String,String> mapAgCurrency = p.mapAgCurrency;
			list<SelectOption> agencyOptions = p.agencyOptions;

			p.changeGroup();
			p.changeAgency();
			p.selectedProvider = p.providersOpt[1].getValue();
			p.changeProvider();
			p.changeCategory();
			string selectedProduct = p.selectedProduct;
			Invoice__c dates = p.dates;

			p.search();

			Decimal totalInvoice = p.totalInvoice;
			Integer countProds = p.countProds;
			String prodCurrency = p.prodCurrency;
			Invoice__c commInvoice = p.commInvoice;
			Provider__c invProv = p.invProv;
			String prodIds = p.prodIds;
			Boolean sendEmail = p.sendEmail;
			String emTo = p.emTo;
			String emCc = p.emCc;
			String emSubj = p.emSubj;
			String emCont = p.emCont;

			for(client_product_service__c prod : p.listProducts)
				prod.isSelected__c = true;

			ApexPages.currentPage().getParameters().put('ids', p.listProducts[0].Id);
			ApexPages.currentPage().getParameters().put('ag', portalUser.Contact.AccountId);
			product_commission_invoice inv = new product_commission_invoice();
			ApexPages.currentPage().getParameters().remove('ids');


			p.calcInvoice();

			p.createInvoice();
			p.emTo = 'test@test.com,tes2@test.com';
			p.emCc = 'test@test.com,tes2@test.com';
			p.sendEmail();


			ApexPages.currentPage().getParameters().put('show', 'confComm');
			p.changeTab();

			//View Invoice
			ApexPages.currentPage().getParameters().put('id', p.invoices[0].Id);
			inv = new product_commission_invoice();


			//Resend Email
			ApexPages.CurrentPage().getParameters().put('inv', p.invoices[0].Id);
			p.setupResendEmail();
			p.emTo = 'test@test.com;tes2@test.com';
			p.sendEmail();

			ApexPages.CurrentPage().getParameters().put('inv', p.invoices[0].Id);
			p.setupToConfirm();
			p.confirmInvoice();
			
			// string selectedCategory = p.selectedCategory;
			// p.selectedProvider = ;

		}
	}

	static testMethod void createAndCancel(){

		system.runAs(portalUser){
			products_request_commission p = new products_request_commission();
			string selectedAgencyGroup = p.selectedAgencyGroup;
			List<SelectOption> agencyGroupOptions = p.agencyGroupOptions;
			map<String,String> mapAgCurrency = p.mapAgCurrency;
			list<SelectOption> agencyOptions = p.agencyOptions;

			p.changeGroup();
			p.changeAgency();
			p.selectedProvider = p.providersOpt[1].getValue();
			p.changeProvider();
			p.changeCategory();

			p.search();

			for(client_product_service__c prod : p.listProducts)
				prod.isSelected__c = true;

			p.calcInvoice();

			p.createInvoice();
			p.sendEmail();


			ApexPages.currentPage().getParameters().put('show', 'confComm');
			p.changeTab();

			//Resend Email
			ApexPages.CurrentPage().getParameters().put('inv', p.invoices[0].Id);
			p.setupResendEmail();
			p.emTo = 'test@test.com;tes2@test.com';
			p.sendEmail();

			ApexPages.CurrentPage().getParameters().put('inv', p.invoices[0].Id);
			p.setIdtoCancel();
			p.cancelInvoice();
			
			// string selectedCategory = p.selectedCategory;
			// p.selectedProvider = ;

		}
	}
}