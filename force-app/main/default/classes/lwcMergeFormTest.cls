@isTest
private class lwcMergeFormTest {
        
    @isTest static void updateFromMergeAllDisabledExceptionTest(){
        string c = lwcMergeForm.updateFromMergeAllDisabled(null);
        System.assertNotEquals(c, 'success');
    }

    @isTest static void getContactValuestoMergeExceptionTest(){
                
        lwcMergeForm.MergeWrapper c = lwcMergeForm.getContactValuestoMerge('55AA');
        System.assertNotEquals(c, null);      	
    }

    @isTest static void updateMergeQueueExceptionTest(){
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();
        Contact cli = tf.createClient(agency);
        cli.Form_AgencyVisit__c = agency.id;
        cli.Form_FilledUp__c = Date.today();
        cli.Birthdate = system.today().addMonths(-253);
        cli.Form_AgreementImg__c = true;       
      	update cli;

        string lstContact = '{"Agreement Image:true","Email:g@com.br"}';
        string stForm ='';
        string lstDocument = '';
        string r = lwcMergeForm.updateMergeQueue(cli.id,lstContact,stForm,lstDocument);
        System.assertNotEquals(r, 'success');  
    }    


    @isTest static void getWrapperFormsOfContactTest(){
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency(); 
        Contact client = tf.createClient(agency);

        Forms_of_Contact__c foc = new Forms_of_Contact__c();
		foc.Contact__c = client.id;
		foc.Type__c = 'WhatsApp';
		foc.Detail__c = 'WhatsappTest';
		insert foc;
        
        Forms_of_Contact__c foc1 = new Forms_of_Contact__c();
		foc1.Contact__c = client.id;
		foc1.Type__c = 'Facebook';
		foc1.Detail__c = 'FacebookTest';
		insert foc1;

        Forms_of_Contact__c foc2 = new Forms_of_Contact__c();
		foc2.Contact__c = client.id;
		foc2.Type__c = 'Instagram';
		foc2.Detail__c = 'InstagramTest';
		insert foc2;
        
        Forms_of_Contact__c foc3 = new Forms_of_Contact__c();
		foc3.Contact__c = client.id;
		foc3.Type__c = 'Mobile';
		foc3.Detail__c = '+6134344556';
		insert foc3;

        Forms_of_Contact__c foc4 = new Forms_of_Contact__c();
		foc4.Contact__c = client.id;
		foc4.Type__c = 'Secondary e-mail';
		foc4.Detail__c = 'g@g.com';
		insert foc4;

        lwcMergeForm.FormOfContactsWrapper f = lwcMergeForm.getWrapperFormsOfContact(client.id);
        System.assertNotEquals(f, null);
    }
        
    @isTest static void updateFromMergeAllDisabledTest(){
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency(); 
        Contact client = tf.createClient(agency);
        string c = lwcMergeForm.updateFromMergeAllDisabled(client.id);
        System.assertEquals(c, 'success');
    }
    
    @isTest static void getPosFrontDeskQueueTest(){
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();
        Contact cli = tf.createClient(agency);
        cli.Form_AgencyVisit__c = agency.id;
        cli.Form_FilledUp__c = Date.today();
        cli.Form_UpdatedContact__c = '{"apiName":"Contact","fieldsCon":{"FirstName":"Maria","LastName":"Silva","Nationality__c":"Brazil","Form_School__c":"NewSchool","Form_Course__c":"NewCourse", "LeadSource":"Referral","Lead_Source_Specific__c":"Friends","Birthdate":"2019-05-16","Form_ContactPreference__c":"Evening","Form_ContactBy__c":"Email","Form_AgreementImg__c":true,"Email":"test_eduarda.gomes.silva2000@hotmail.com"}};{"apiName":"Client_Document__c","fieldsCli":{"Visa_type__c":"Student","Expiry_Date__c":"2019-05-07"}};[{"Type__c":"WhatsApp","Detail__c":"WhatsappTest"},{"Type__c":"Facebook","Detail__c":"FacebookTest"},{"Type__c":"Instagram","Detail__c":"InstagramTest"},{"Type__c":"Mobile","Detail__c":"+61819889699"},{"Type__c":"Secondary e-mail","Detail__c":"hqm@g.com"}]';
      	update cli;

        Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
        Test.startTest();
     	System.runAs(portalUser){
            List<lwcMergeForm.PosFrontDeskWrapper> c = lwcMergeForm.getPosFrontDeskQueue(agency.id);
            System.assertNotEquals(c, null);
        }        
    }
        
    @isTest static void getContactValuestoMergeTest(){
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();
        Contact cli = tf.createClient(agency);
        cli.Form_AgencyVisit__c = agency.id;
        cli.Form_FilledUp__c = Date.today();
        cli.Birthdate = system.today().addMonths(-253);
        cli.Form_AgreementImg__c = true;
        cli.Form_UpdatedContact__c = '{"apiName":"Contact","fieldsCon":{"FirstName":"Maria","LastName":"Silva","Nationality__c":"Brazil","Form_School__c":"NewSchool","Form_Course__c":"NewCourse", "LeadSource":"Referral","Lead_Source_Specific__c":"Friends","Birthdate":"2019-05-16","Form_ContactPreference__c":"Evening","Form_ContactBy__c":"Email","Form_AgreementImg__c":true,"Email":"g@g.com"}};{"apiName":"Client_Document__c","fieldsCli":{"Visa_type__c":"Student","Expiry_Date__c":"2019-05-07"}};[{"Type__c":"WhatsApp","Detail__c":"WhatsappTest"},{"Type__c":"Facebook","Detail__c":"FacebookTest"},{"Type__c":"Instagram","Detail__c":"InstagramTest"},{"Type__c":"Mobile","Detail__c":"+61819889699"},{"Type__c":"Secondary e-mail","Detail__c":"g@g.com"}]';
      	update cli;

        Lead_Status_Source__c listSource = new Lead_Status_Source__c();
        listSource.Agency_Group__c = agency.ParentId;
        listSource.Field_Type__c = 'LeadSource';
        listSource.Lead_Source__c = 'Referral'; 
        listSource.Subject__c = 'Friends';
        insert listSource;

       Forms_of_Contact__c foc = new Forms_of_Contact__c();
		foc.Contact__c = cli.id;
		foc.Type__c = 'WhatsApp';
		foc.Detail__c = 'WhatsappTest';
		insert foc;
        
        Forms_of_Contact__c foc1 = new Forms_of_Contact__c();
		foc1.Contact__c = cli.id;
		foc1.Type__c = 'Facebook';
		foc1.Detail__c = 'FacebookTest';
		insert foc1;

        Forms_of_Contact__c foc2 = new Forms_of_Contact__c();
		foc2.Contact__c = cli.id;
		foc2.Type__c = 'Instagram';
		foc2.Detail__c = 'InstagramTest';
		insert foc2;
        
        Forms_of_Contact__c foc3 = new Forms_of_Contact__c();
		foc3.Contact__c = cli.id;
		foc3.Type__c = 'Mobile';
		foc3.Detail__c = '+6134344556';
		insert foc3;

        Forms_of_Contact__c foc4 = new Forms_of_Contact__c();
		foc4.Contact__c = cli.id;
		foc4.Type__c = 'Secondary e-mail';
		foc4.Detail__c = 'g@g.com';
		insert foc4;

        Client_Document__c doc = new Client_Document__c();        
        doc.Visa_type__c = 'Student';
        doc.Expiry_Date__c = system.today().addMonths(5);
        doc.Client__c = cli.id;
        insert doc;

        lwcMergeForm.MergeWrapper c = lwcMergeForm.getContactValuestoMerge(cli.id);
        System.assertNotEquals(c, null);
    }

    @isTest static void getContactValuestoMerge1Test(){
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();
        Contact cli = tf.createClient(agency);
        cli.Form_AgencyVisit__c = agency.id;
        cli.Form_FilledUp__c = Date.today();
        cli.Birthdate = system.today().addMonths(-253);
        cli.Form_AgreementImg__c = true;
        cli.Email='g@g.com';
        cli.Form_UpdatedContact__c = '{"apiName":"Contact","fieldsCon":{"FirstName":"Maria","LastName":"Silva","Nationality__c":"Brazil","Form_School__c":"NewSchool","Form_Course__c":"NewCourse", "LeadSource":"Referral","Lead_Source_Specific__c":"Friends","Birthdate":"2019-05-16","Form_ContactPreference__c":"Evening","Form_ContactBy__c":"Email","Form_AgreementImg__c":true,"Email":"g@g.com"}};{"apiName":"Client_Document__c","fieldsCli":{"Visa_type__c":"Student","Expiry_Date__c":"2019-05-07"}};[{"Type__c":"WhatsApp","Detail__c":"WhatsappTest"},{"Type__c":"Facebook","Detail__c":"FacebookTest"},{"Type__c":"Instagram","Detail__c":"InstagramTest"},{"Type__c":"Mobile","Detail__c":"+61819889699"},{"Type__c":"Secondary e-mail","Detail__c":"g@g.com"}]';
      	update cli;

        Lead_Status_Source__c listSource = new Lead_Status_Source__c();
        listSource.Agency_Group__c = agency.ParentId;
        listSource.Field_Type__c = 'LeadSource';
        listSource.Lead_Source__c = 'Referral'; 
        listSource.Subject__c = 'Friends';
        insert listSource;
        
        Forms_of_Contact__c foc3 = new Forms_of_Contact__c();
		foc3.Contact__c = cli.id;
		foc3.Type__c = 'Mobile';
		foc3.Detail__c = '+6134344556';
		insert foc3;

        Forms_of_Contact__c foc4 = new Forms_of_Contact__c();
		foc4.Contact__c = cli.id;
		foc4.Type__c = 'Secondary e-mail';
		foc4.Detail__c = 'g@g.com';
		insert foc4;

        Client_Document__c doc = new Client_Document__c();        
        doc.Visa_type__c = 'Student';
        doc.Expiry_Date__c = system.today().addMonths(5);
        doc.Client__c = cli.id;
        insert doc;

        lwcMergeForm.MergeWrapper c = lwcMergeForm.getContactValuestoMerge(cli.id);
        System.assertNotEquals(c, null);
    }


    @isTest static void getContactValuestoMerge3Test(){
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();
        Contact cli = tf.createClient(agency);
        cli.Form_AgencyVisit__c = agency.id;
        cli.Form_FilledUp__c = Date.today();
        cli.Birthdate = system.today().addMonths(-253);
        cli.Form_AgreementImg__c = true;
        
        cli.Form_UpdatedContact__c = '{"apiName":"Contact","fieldsCon":{"FirstName":"Maria","LastName":"Silva","Nationality__c":"Brazil","Form_School__c":"NewSchool","Form_Course__c":"NewCourse", "LeadSource":"Referral","Lead_Source_Specific__c":"Friends","Birthdate":"2019-05-16","Form_ContactPreference__c":"Evening","Form_ContactBy__c":"Email","Form_AgreementImg__c":true,"Email":"g@g.com"}};{"apiName":"Client_Document__c","fieldsCli":{"Visa_type__c":"Student","Expiry_Date__c":"2019-05-07"}}';
      	update cli;      

        lwcMergeForm.MergeWrapper c = lwcMergeForm.getContactValuestoMerge(cli.id);
        System.assertNotEquals(c, null);
    }    
        
    @isTest static void updateMergeQueueTest(){
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();
        Contact cli = tf.createClient(agency);
        cli.Form_AgencyVisit__c = agency.id;
        cli.Form_FilledUp__c = Date.today();
        cli.Birthdate = system.today().addMonths(-253);
        cli.MobilePhone = '+6195959090';
        cli.Form_AgreementImg__c = true;        
      	update cli;

        Lead_Status_Source__c listSource = new Lead_Status_Source__c();
        listSource.Agency_Group__c = agency.ParentId;
        listSource.Field_Type__c = 'LeadSource';
        listSource.Lead_Source__c = 'Referral'; 
        listSource.Subject__c = 'Friends';
        insert listSource;

        Forms_of_Contact__c foc = new Forms_of_Contact__c();
		foc.Contact__c = cli.id;
		foc.Type__c = 'WhatsApp';
		foc.Detail__c = 'WhatsappTest';
		insert foc;
        
        Forms_of_Contact__c foc1 = new Forms_of_Contact__c();
		foc1.Contact__c = cli.id;
		foc1.Type__c = 'Facebook';
		foc1.Detail__c = 'FacebookTest';
		insert foc1;

        Forms_of_Contact__c foc2 = new Forms_of_Contact__c();
		foc2.Contact__c = cli.id;
		foc2.Type__c = 'Instagram';
		foc2.Detail__c = 'InstagramTest';
		insert foc2;

        Client_Document__c doc = new Client_Document__c();        
        doc.Visa_type__c = 'Student';
        doc.Expiry_Date__c = system.today().addMonths(5);
        doc.Client__c = cli.id;
        insert doc;

        Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
        Test.startTest();
     	System.runAs(portalUser){

            string lstContact ='["First Name:teste cls","Last Name:classe","Nationality:Brazil","LeadSource:Referral","Lead_Source_Specific__c:Friends","School:New_School_ToTrairas","Mobile:+61334344555","Course:Course_Trairas_Master","Date of birth:1998-07-09","Contact Preference:Evening","Contact By:Email","Agreement Image:true","Email:g@com.br"]';
            string stForm = '["Instagram:sdsdsd","Facebook:sdsdsdsd","WhatsApp:555198654232","Mobile:+61652356565","Mobile:+61565986565","Secondary e-mail:ddg@com.br"]';
            string lstDocument = '["Visa Type:Resident","Visa Expire Date:2020-07-04"]';
            string r = lwcMergeForm.updateMergeQueue(cli.id,lstContact,stForm,lstDocument);
            System.assertEquals(r, 'success');            
        }        
    }

    @isTest static void updateMergeQueue1Test(){
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();
        Contact cli = tf.createClient(agency);
        cli.Form_AgencyVisit__c = agency.id;
        cli.Form_FilledUp__c = Date.today();
        cli.Birthdate = system.today().addMonths(-253);
        cli.MobilePhone = '+6195959090';
        cli.Form_AgreementImg__c = true;        
      	update cli;

        Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
        Test.startTest();
     	System.runAs(portalUser){

            string lstContact ='["First Name:teste cls","Last Name:classe","Nationality:Brazil","LeadSource:Referral","Lead_Source_Specific__c:Friends","School:New_School_ToTrairas","Mobile:+61334344555","Course:Course_Trairas_Master","Date of birth:1998-07-09","Contact Preference:Evening","Contact By:Email","Agreement Image:true","Email:g@com.br"]';
            string stForm = '["Instagram:sdsdsd","Facebook:sdsdsdsd","WhatsApp:555198654232","Mobile:+61652356565","Mobile:+61565986565","Secondary e-mail:ddg@com.br"]';
            string lstDocument = '["Visa Type:Resident","Visa Expire Date:2020-07-04"]';
            string r = lwcMergeForm.updateMergeQueue(cli.id,lstContact,stForm,lstDocument);
            System.assertEquals(r, 'success');            
        }        
    }
}