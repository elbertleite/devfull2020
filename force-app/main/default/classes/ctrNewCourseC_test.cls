/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ctrNewCourseC_test {

    static testMethod void myUnitTest() {
    	
    	Account school = new Account();
    	school.name = 'Sydney School';
    	school.recordtypeid = [select id from recordtype where name ='School' and isActive = true LIMIT 1].id;
    	insert school;
    	
    	Account campus = new Account();
    	campus.recordtypeid = [select id from recordtype where name ='Campus' and isActive = true LIMIT 1].id;
    	campus.name = 'Sydney CBD';
    	campus.ParentId = school.id;
    	insert campus;
    	
    	ApexPages.currentPage().getParameters().put('cpid',campus.id);
        ctrNewCourseC nc = new ctrNewCourseC();
        List<SelectOption> getPriceAvailabilityList = nc.getPriceAvailabilityList();
        nc.cancel();
        
        List<ctrNewCourseC.listCampusItem> listCampus = nc.listCampus;
        nc.newCourseDetails.Name = 'English';
        nc.newCoursePriceDetails.Price_per_week__c = 305;
        nc.addCourse();
        nc.cancelInsert();
        
    }
}