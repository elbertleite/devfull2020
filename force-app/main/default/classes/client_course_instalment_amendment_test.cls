/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class client_course_instalment_amendment_test {

    static testMethod void myUnitTest() {

		TestFactory tf = new TestFactory();

       	Account school = tf.createSchool();
      	Account agency = tf.createAgency();
       	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
       	Account campus = tf.createCampus(school, agency);
       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);


       	Test.startTest();
       	system.runAs(portalUser){

    	   	Contact client = tf.createLead(agency, emp);
	       	client_course__c booking = tf.createBooking(client);
	       	client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       	client_Course_fees__c ccf = tf.createClientCourseFees(cc, false);


	       	List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);


	       	/* B E F O R E     P A Y    T O    S C H O O L **/
	       	client_course_instalment_pay toPay = new client_course_instalment_pay(new ApexPages.StandardController(instalments[2]));

	       	instalments[2].Related_Fees__c = 'a1IN00000030XPjMAM;;2016 Enrolment Fee;;220.00;;0;;220.00;;0';
	       	instalments[2].Instalment_Value__c += 220;
	       	update instalments[2];


	       	toPay.newPayDetails.Payment_Type__c = 'Creditcard';
		   	toPay.newPayDetails.Value__c = instalments[2].Instalment_Value__c;
		   	toPay.newPayDetails.Date_Paid__c = Date.today();
		   	toPay.addPaymentValue();
		   	toPay.savePayment();


		   	client_course_instalment_amendment testClass = new client_course_instalment_amendment(new ApexPages.StandardController(instalments[2]));

		   	testClass.amendment.Tuition_value__c = 3000; //Change tuition > original tuition

		   	testClass.feeInstall[0].feeAmendment = 250; //Change Fee Value > original fee value
		   	testClass.feeInstall[0].keepFeeAmendment = 10; //Change Keep Fee > original keep fee

        // Add new fee
        // stClass.newFee.Fee_Name__c = 'COE';
        // stClass.newFee.Value__c = 50;
        // stClass.newFee.Number_Units__c = 1;
        // stClass.newFee.Unit_Type__c = 'Unit';
        // stClass.newFee.Total_Value__c = 50;
        // stClass.addNewFee();

		   	testClass.saveAmendment();

		   	testClass.amendment.Tuition_value__c = 2800; //Change tuition > original tuition


        // exPages.CurrentPage().getParameters().put('idFee', testClass.listFeesToAdd[0].feeid);
        // stClass.removeFee();


		   	testClass.saveAmendment();



		   	/* A F T E R     P A Y    T O    S C H O O L **/
		   	toPay = new client_course_instalment_pay(new ApexPages.StandardController(instalments[1]));
	   		toPay.newPayDetails.Payment_Type__c = 'Creditcard';
		   	toPay.newPayDetails.Value__c = instalments[1].Instalment_Value__c;
		   	toPay.newPayDetails.Date_Paid__c = Date.today();
		   	toPay.addPaymentValue();
		   	toPay.savePayment();


		   	instalments[1].Paid_To_School_On__c = System.today();
		   	instalments[1].Related_Fees__c = 'a1IN00000030XPjMAM;;2016 Enrolment Fee;;100.00;;0;;100.00;;0';
		   	update instalments[1];


		   	testClass = new client_course_instalment_amendment(new ApexPages.StandardController(instalments[1]));

		   	testClass.amendment.Tuition_value__c = 1000; //Change tuition < original tuition

	   		testClass.feeInstall[0].feeAmendment = 250; //Change Fee Value > original fee value
		   	testClass.feeInstall[0].keepFeeAmendment = 10; //Change Keep Fee > original keep fee

        // Add new fee
        // stClass.newFee.Fee_Name__c = 'COE';
        // stClass.newFee.Value__c = 50;
        // stClass.newFee.Number_Units__c = 1;
        // stClass.newFee.Unit_Type__c = 'Unit';
        // stClass.newFee.Total_Value__c = 50;
        // stClass.addNewFee();

	   		testClass.saveAmendment();

	   		testClass.amendment.isPFS__c = true;
	   		testClass.saveAmendment();


	   		testClass.cancelChanges();
	   		testClass.getUnitTypes();
	   		testClass.getNumberOfUnits();

       	}
    }
}