@isTest
private class report_school_year_to_year_test {
	
	public static Account school {get;set;}
	public static Account agency {get;set;}
	public static Contact emp {get;set;}
	public static Contact client {get;set;}
	public static User portalUser {get{
		if (null == portalUser) {
			portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
		}
		return portalUser;
	}set;}
	public static Account campus {get;set;}
	public static Course__c course {get;set;}
	public static Campus_Course__c campusCourse {get;set;}
	public static client_course__c clientCourseBooking {get;set;}
	public static client_course__c clientCourse {get;set;}
	public static client_course__c clientCourse2 {get;set;}


	@testSetup static void setup() {
		TestFactory tf = new TestFactory();

		Account schGroup = tf.createSchoolGroup(); 
		school = tf.createSchool();
		school.ParentId = schGroup.Id;
		update school;
		agency = tf.createAgency();
		emp = tf.createEmployee(agency);
		portalUser = tf.createPortalUser(emp);
		campus = tf.createCampus(school, agency);
		course = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);

		system.runAs(portalUser){
			client = tf.createClient(agency);
			clientCourseBooking = tf.createBooking(client);
			clientCourse = tf.createClientCourse(client, school, campus, course, campusCourse, clientCourseBooking);
			List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(clientCourse);

			
			instalments[0].Received_By_Agency__c = agency.id;
			instalments[0].Received_Date__c =  system.today();
			instalments[0].Paid_To_School_On__c =  system.today();
			instalments[0].Paid_to_School_By_Agency__c =  agency.id;
			instalments[0].isPFS__c = true;

			instalments[1].Received_By_Agency__c = agency.id;
			instalments[1].Received_Date__c =  system.today();
			instalments[1].isPCS__c = true;

			instalments[2].Received_By_Agency__c = agency.id;
			instalments[2].Received_Date__c =  system.today();
			instalments[2].isPDS__c = true;
			instalments[2].PDS_Confirmed__c = true;

			update instalments;
		}
	}

	static testMethod void runReport() {
		Client_Course__c course = [Select Id from Client_Course__c where End_Date__c != null];
		Account agency = [Select id from Account where RecordType.name = 'Agency'];
		course.Enrolment_Date__c = system.today();
		course.Enroled_by_Agency__c = agency.id;
		update course;
			
		//Run Report
		system.runAs(portalUser){
			report_school_year_to_year test = new report_school_year_to_year();
			String result = report_school_year_to_year.runReport('weeksSold', 'destination', 'all', 'agencyGroup', 2016, 2018, 'year');
			result = report_school_year_to_year.runReport('courses', 'destination', 'all', 'courseType', 2016, 2018, 'year');
		}
	}

	
}