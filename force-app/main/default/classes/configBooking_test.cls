@isTest
private class configBooking_test {
    
    static testMethod void testBookingConfig() {
        TestFactory tf = new TestFactory();
        account agency = tf.createAgency();
        contact employee = tf.createEmployee(agency);
        Department__c dep = tf.createDepartment(agency);
        employee.Department__c = dep.id;
        update employee;
        User u = tf.createPortalUser(employee);

        dep.Allow_Booking__c = true;
        update dep;
        Client_Booking__c cb = new Client_Booking__c(Date_Booking__c = System.today(), Time_Booking__c = Time.newInstance(13,0,0,0));
        Client_Booking__c cb2 = new Client_Booking__c(Date_Booking__c = System.today(), Time_Booking__c = Time.newInstance(13,0,0,0), isBlocked__c = true);
        insert cb;
        insert cb2;

            Test.startTest();
            ApexPages.currentPage().getParameters().put('ag',agency.id);
            configBooking book = new configBooking();
            book.userBooking.Work_From_Time__c = '00:00';
            book.userBooking.Work_To_Time__c = '12:00';
            book.userBooking.Break_From__c = '00:00';
            book.userBooking.Break_To__c = '00:00';
            book.timeSelected = '00:00';

            book.setEditConfig();
            book.saveBookingConfig();
            book.cancelConfig();
            book.setEditBlock();
            book.saveBookingBlock();
            book.cancelBlock();

            List<SelectOption> listHour = book.listHour;
            List<SelectOption> listMinutes = book.listMinutes;

            book.deleteItems();
    }
}