public with sharing class CtrPageHeader {

	public String accoID {get;set;}
	public Account acco {
		get{
			System.debug('==>accoId: '+accoId);
			if(accoID != null)
				acco = [Select Name, CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name, Nickname__c, Group_Type__c, ParentID, Parent.Name, AgencyGroup__c, AgencyGroupName__c,
						Registration_name__c, Year_established__c, BusinessEmail__c, Website
				from Account where id = :accoID];

			return acco;
		}
		set;
	}

	public integer leadClientCount { get; set; }
	public String ctID {get;set;}
	public Contact contact {
		get{
			if(ctID != null){
				contact = [Select id, Name, RecordType.Name, Email, Nationality__c, CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name, Lead_Stage__c, Status__c, Destination_Country__c, Destination_City__c, Owner__r.name, Owner__r.Contact.Account.Name from Contact where id = :ctID];
				leadClientCount = [Select count() from contact where email = :contact.email and recordType.name in ('lead','client')];
			}
			return contact;
		}
		set;
	}

	public ctrPageHeader(){

	}

	public void refreshHeader(){
		contact = null;
	}
	

    
    

}