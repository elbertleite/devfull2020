public with sharing class menuContact {
	
	public String ctid{get; Set;}
  	public String currTab{get;set;}  	
  
  	
	public Contact contact {
		get{
			if(contact == null)
				if(ctid == null) ctid= currentUser.ContactId;
				contact = [Select id, name, RecordType.Name from Contact where id = :ctid];
			return contact;
		}
		set;
	}
  
  
	private String fileURL;	
	public String getfileURL(){
		
		String accountPicture;
		
	    try{
	    	
	    	if(contact.RecordType.Name == 'Employee')
	    		accountPicture = [Select FullPhotoUrl from User where ContactID = :contact.id LIMIT 1].FullPhotoUrl;
	    	else
	    		accountPicture = [Select PhotoClient__c from Contact where id = :ctid LIMIT 1].PhotoClient__c;
	    		
	    } catch(Exception e){}
	      
	    // URL for pictures
	    if(accountPicture != null && accountPicture != '')
	    	fileURL = accountPicture;
	    else
	        fileURL = 'NoPhoto';
	        
	        
	    return fileURL;
	    
	}
	
	public User userObj { 
		get{
			if(contact != null && userObj == null){
				try {
					userObj = [select SmallPhotoUrl, FullPhotoUrl from User where contactid = : contact.id ];
				} catch (Exception e){}
			}
			return userObj;
		}
		set;
	}


	private User currentUser {get{
		if(currentUser== null)
			currentUser = [Select Id, ContactId from User where id = :userInfo.getUserId()];
		return currentUser;
	}set;}
}