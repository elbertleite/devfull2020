public with sharing class agency_agreements {
	
	public String allGroups {get;set;}
	public agency_agreements() {
		
		list<opWrapper> groups = new list<opWrapper>();
		for(Account a : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name])
			groups.add(new opWrapper(a.Id, a.Name));

		if(groups.size()>0)
			groups.add(0,new opWrapper('none', '-- Select Group --'));

		allGroups = JSON.serialize(groups);
	}

	@RemoteAction
	public static void updateAgreements(String agencyId, List<String> toAdd, list<String> toDelete){
		system.debug('agencyId==>' + agencyId);
		system.debug('toAdd==>' + toAdd);
		system.debug('toDelete==>' + toDelete);

		set<String> setAdd = new set<String>(toAdd);
		set<String> setRemove = new set<String>(toDelete);

		list<String> allIds = new list<String>(toAdd);
		allIds.addAll(toDelete);

		list<Agreement_Accounts__c> lAddAgree = new list<Agreement_Accounts__c>();
		list<Agreement_Accounts__c> lRemoveAgree = new list<Agreement_Accounts__c>();

		for(Agreement__c a : [SELECT Id, (SELECT Id FROM Agreement_Accounts__r WHERE Account__c = :agencyId ) FROM Agreement__c WHERE Id in :allIds]){

			// Must add agency on agreement
			if(setAdd.contains(a.Id) && (a.Agreement_Accounts__r == null || a.Agreement_Accounts__r.size() < 1 )){
				lAddAgree.add(new Agreement_Accounts__c(Account__c = agencyId, Agreement__c = a.Id));
			}

			// Must Remove agency from Agreement
			else if(setRemove.contains(a.Id) && (a.Agreement_Accounts__r != null)){
				lRemoveAgree.addAll(a.Agreement_Accounts__r);
			}

		}//end for

		if(lAddAgree.size()>0)
			insert lAddAgree;

		if(lRemoveAgree.size()>0)
			delete lRemoveAgree;
	}


	@RemoteAction
	public static String findAgreements(String agencyId){

		//Get agency Details
		Account agency = [SELECT Name, Globe_Region__c, BillingCountry FROM Account WHERE Id = :agencyId];

		//Check agreements agency is in
		list<Id> selAgreements = new list<Id>();
		for(Agreement_Accounts__c aa : [Select Agreement__c from Agreement_Accounts__c where Account__c = :agencyId])
			selAgreements.add(aa.Agreement__c);

		//Get all agreement that the agency could fit in -> Except Cancelled
		list<Agreement__c> agreements = new list<Agreement__c>([SELECT Id, Agreement_Name__c, Status_formula__c FROM Agreement__c WHERE ((Agency_Regions__c = 'All Regions' AND (Agency_Exc_Countries__c = null OR Agency_Exc_Countries__c EXCLUDES ( : agency.BillingCountry ))) OR (Agency_Regions__c Includes ( :agency.Globe_Region__c ) AND (Agency_Exc_Countries__c = null OR Agency_Exc_Countries__c EXCLUDES ( :agency.BillingCountry ))) OR Include_Countries__c Includes ( :agency.BillingCountry )) AND isCancelled__c = false order by Agreement_Name__c]);

		system.debug('agreements===>' + agreements);

		return JSON.serialize(new agreeWrapper(agency, selAgreements, agreements));
	}

	@RemoteAction
	public static String searchAgencies(String parentId){
		String sql = 'SELECT Id, Name, Globe_Region__c, BillingCountry FROM Account WHERE ParentId = :parentId AND RecordType.Name = \'Agency\' and Inactive__c = false order by Name';

		list<opWrapper> result = new list<opWrapper>();
		for(Account a : Database.query(sql))
			result.add(new opWrapper(a.Id, a.Name));

		if(result.size()>0)
			result.add(0,new opWrapper('none', '-- Select Agency --'));

		return JSON.serialize(result);
	}

	@TestVisible
	private class agreeWrapper{
		private Account agency {get;set;}
		private list<Id> selAgree {get;set;}
		@TestVisible
		private list<Agreement__c> agreements {get;set;}

		private agreeWrapper(Account agency, list<Id> selAgree, list<Agreement__c> agreements ){
			this.agency = agency;
			this.selAgree = selAgree;
			this.agreements = agreements;
		}
	}

	private class opWrapper{
		public String opValue {get;set;}
		public String opLabel {get;set;}
		public String opRegion {get;set;}
		public String opCountry {get;set;}

		public opWrapper(String opValue, String opLabel){
			this.opValue = opValue;
			this.opLabel = opLabel;
		}
	}
}