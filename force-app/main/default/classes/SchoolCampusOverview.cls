public class SchoolCampusOverview{
	
	public String country {get;set;}
	public String city {get;set;}
    
	public SchoolCampusOverview(ApexPages.StandardController controller){
		
		country = ApexPages.currentPage().getParameters().get('country');
		city = ApexPages.currentPage().getParameters().get('city');
		if(country != null)
			country = country;
		if(city != null)
			city = city;
		
		if(city != null && country != null)
			getSchools();
	}
	
	public School_Campus_Overview__c newSchool {
		get {
		if(newSchool == null)
			newSchool = new School_Campus_Overview__c();
		return newSchool;
	}
		set;
	}

	public Course_Overview__c newCourse {
		get {
		if(newCourse == null)
			newCourse = new Course_Overview__c();
		return newCourse;
	}
		set;
	}
	
	
	
	public List<SelectOption> countryOptions {
		get {
		if(countryOptions == null)
			countryOptions = IPFunctionsGlobal.CountryDestinations();
		return countryOptions;
	}
		set;
	}
	
	
	public List<SelectOption> cityOptions {
		get {
		if(country != null)
			cityOptions = new List<SelectOption>();
			cityOptions.add(new SelectOption ('', 'Test'));
		return cityOptions;
	}
		set;
	}
    
	public void getCities(){
		schoolOverviewOptions = new List<SelectOption>();
		courses = new List<Course_Overview__c>();
		foundSchools = false;
		foundCourses = false;
		selectedSchoolOverview = '';
	}
	
	
	public String selectedSchoolOverview { get;set; }
	
	public List<SelectOption> schoolOverviewOptions {get;set;}
	
	public boolean foundSchools { get { if(foundSchools == null) foundSchools = false; return foundSchools; } set; }
	
	private List<School_Campus_Overview__c> schoolList = new List<School_Campus_Overview__c>();
	
	public void getSchools(){
		
		selectedSchoolOverview = '';
		schoolOverviewOptions = new List<SelectOption>();
		courses = new List<Course_Overview__c>();
		foundSchools = false;
		foundCourses = false;
		
		if(city != ''){			
			schoolOverviewOptions.add(new SelectOption('','--Select School--'));
			schoolOverviewOptions.add(new SelectOption('new','--Add New School--'));
					
			List<School_Campus_Overview__c> lar = [Select School_Campus_Name__c from School_Campus_Overview__c where Country__c = :country and City__c = :city order by School_Campus_Name__c];
			schoolList = lar;
			if(lar.size() > 0)
				foundSchools = true;
			for(School_Campus_Overview__c ar : lar)
				schoolOverviewOptions.add(new SelectOption(ar.id, ar.School_Campus_Name__c));
		}
		
		
	}
	
	public String selectedCourseOverview { get;set; }
	
	public List<Course_Overview__c> courses {get;set;}
	
	public boolean foundCourses { get { if(foundCourses == null) foundCourses = false; return foundCourses; } set; }
	
	public void getCourses(){
		
		courses = new List<Course_Overview__c>();
		foundCourses = false;
		if(selectedSchoolOverview != '' && selectedSchoolOverview != 'new'){
								
			courses = [Select id,Course_Name__c, School_Campus_Overview__c from Course_Overview__c where School_Campus_Overview__c = :selectedSchoolOverview order by Course_Name__c ];
			if(courses.size() > 0)
				foundCourses = true;
			
		}
		
		
	}
	
	
	
	public void save(){
		
				
		if(selectedSchoolOverview == 'new' || !foundSchools){
			
			
			for(School_Campus_Overview__c sco : schoolList)
				if(newSchool.School_Campus_Name__c.trim().equals(sco.School_Campus_Name__c.trim()))
					return;
			
			newSchool.Country__c = country;
			newSchool.City__c = city;
		
			insert newSchool;
			
			selectedSchoolOverview = newSchool.id;
			
			
			
			
		}
		
		newSchool = new School_Campus_Overview__c();
		
		newCourse.School_Campus_Overview__c = selectedSchoolOverview;
		
		insert newCourse;
		
		newCourse = new Course_Overview__c();
		String schoolid = selectedSchoolOverview;
		getSchools();
		selectedSchoolOverview = schoolid;
		getCourses();
		
	}
	
	
	
	

}