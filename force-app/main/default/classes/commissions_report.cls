public without sharing class commissions_report {
	
	public commissions_report(){
		try{
			user u = [Select contact.AccountId, contact.Account.Hify_Agency_Commission__c, contact.Account.Hify_Agency__c from user where id = :UserInfo.getUserId() limit 1];
			if(u.contact.Account.Hify_Agency_Commission__c != null)
				agencyCommission = u.contact.Account.Hify_Agency_Commission__c;
			else 
				agencyCommission = 0;
			isHifyAgency = u.contact.Account.Hify_Agency__c;
		}catch(Exception e){
			agencyCommission = 0;
		}
		//retrieveCommissions();
	}
	private decimal agencyCommission;
	public boolean isHifyAgency{get; set;}

	public class commissionDetails{
		public string country{get; set;}
		public string school{get; set;}
		public string campus{get; set;}
		public string courseType{get; set;}
		public string course{get; set;} 
		
		public decimal Value{get; set;}
		public decimal hifyCommission{get; set;}

		public decimal Type{get; set;}
		public string Comment{get; set;}
		public commissionDetails(string country, string school, string campus, string courseType, string course, decimal Value, decimal Type, string Comment, decimal agCommission){
			this.country = country;
			this.school = school;
			this.campus = campus;
			this.courseType = courseType;
			this.course = course;
			this.Value = Value;
			this.Type = Type;
			this.Comment = Comment;
			this.hifyCommission = (value*agCommission)/100;
		}
	}
	
	public class CourseType{
		public commissionDetails CourseTypeCommission{get; set;}
		public list<Commission__c> listCoursesCommission{get; set;}
	}
	
	public class campus{
		public commissionDetails CampusCommission{get; set;}
		public list<CourseType> listCourseTypeCommission{get; set;}
	}
	
	public class commissions{
		public commissionDetails SchoolCommission{get; set;}
		public list<campus> listCampusCommission{get; set;}
	}
	

	public list<commissionDetails> listCommissions{get; set;} 
	public pageReference retrieveCommissions(){
		listCommissions = new list<commissionDetails>();
		string sql = 'Select School_Control__r.name, Campus_Control__r.name, Campus_Name__c, Course_Type_Control__c, Country__c, School_Name__c, Campus_Id__r.name, Campus_Commission__c, Campus_Commission_Type__c, Campus_Course__c, Campus_Course__r.campus__c,CampusCourseTypeId__c, Campus_Id__c, Comments__c, Name, Course_Commission__c, Course_Commission_Type__c, Course_Name__c, Course_Type_Commission__c, Course_Type_Commission_Type__c, courseType__c, Id, School_Commission__c, School_Commission_Type__c, School_Id__c from Commission__c where id != null ';
		if(selectedCountry != 'none')
			sql += ' and country__c = \''+selectedCountry +'\'';
		if(selectedSchool != 'none')
			sql += ' and School_Control__r.name = \''+selectedSchool +'\'';
		sql += '  and School_Control__c != null and Agreement_Account__c = null ';
		sql += ' order by Country__c, School_Control__r.name, Campus_Control__r.name, Course_Type_Control__c, Course_Name__c';
		for(Commission__c c:Database.query(sql)){
			if(c.School_Id__c != null)
				listCommissions.add(new commissionDetails(c.country__c, c.School_Control__r.name, c.Campus_Control__r.name, '', '', c.School_Commission__c, c.School_Commission_Type__c, c.Comments__c, agencyCommission));
			else if(c.Campus_Id__c != null)
				listCommissions.add(new commissionDetails(c.country__c, c.School_Control__r.name, c.Campus_Control__r.name, '', '', c.Campus_Commission__c, c.Course_Type_Commission_Type__c, c.Comments__c, agencyCommission));
			else if(c.CampusCourseTypeId__c != null)
				listCommissions.add(new commissionDetails(c.country__c, c.School_Control__r.name, c.Campus_Control__r.name, c.Course_Type_Control__c, '', c.Course_Type_Commission__c, c.Campus_Commission_Type__c, c.Comments__c, agencyCommission));
			else if(c.Campus_Course__c != null)
				listCommissions.add(new commissionDetails(c.country__c, c.School_Control__r.name, c.Campus_Control__r.name, c.Course_Type_Control__c, c.Course_Name__c, c.Course_Commission__c, c.Course_Commission_Type__c, c.Comments__c, agencyCommission));
		}
		return null;
	}
	
	public string selectedCountry{get{if(selectedCountry == null) selectedCountry = 'none'; return selectedCountry;} set;}
	public string selectedSchool{get{if(selectedSchool == null) selectedSchool = 'none'; return selectedSchool;} set;}
	private List<SelectOption> listCountries;
	public List<SelectOption> listSchools{get; set;}
	
	public List<SelectOption> getlistCountries(){
		if(listCountries == null){
			listCountries = new List<SelectOption>();
        	listCountries.add(new SelectOption('none','--Select Country--'));
	        for(AggregateResult ar:[Select country__c from Commission__c  GROUP BY country__c ORDER BY country__c]){
        		if((string)ar.get('country__c')!=null && (string)ar.get('country__c') != '')
		        	listCountries.add(new SelectOption((string)ar.get('country__c'),(string)ar.get('country__c')));
	        }
        }
        return listCountries;
	}
	
	public void retrievelistSchools() {
        if(selectedCountry != null && selectedCountry != 'none'){
			listSchools = new List<SelectOption>();
	        listSchools.add(new SelectOption('none','--Select School--'));
	        for(AggregateResult ar:[Select School_Control__r.name school from Commission__c WHERE country__c = :selectedCountry and School_Control__c != null GROUP BY School_Control__r.name ORDER BY School_Control__r.name]){
			    listSchools.add(new SelectOption((string)ar.get('school'),(string)ar.get('school')));
	        }
	        selectedSchool = 'none';
	        //retrieveCommissions();
        } else{
        	listSchools.clear();
        }
    }
    
}