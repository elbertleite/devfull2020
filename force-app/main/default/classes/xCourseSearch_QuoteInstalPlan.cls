public class xCourseSearch_QuoteInstalPlan{
	
	string wsId;
	String AgencyId;
	public xCourseSearch_QuoteInstalPlan(ApexPages.StandardController controller){
		wsId = controller.getId();
		getUserDetails();
		AgencyId = UserDetails.Agency_Id;
		//String AgencyId =  [Select Agency__pc from Account WHERE User__c = :UserInfo.getUserId() limit 1].Agency__pc;
		openSelectedAgencyPlan();
	}
	
	private xCourseSearchPersonDetails.UserDetails UserDetails;
	public xCourseSearchPersonDetails.UserDetails getUserDetails(){
		if(UserDetails == null){
			xCourseSearchPersonDetails spd = new xCourseSearchPersonDetails();
			UserDetails = spd.getAgencyDetails();
		}
		return UserDetails;
	}

	public PageReference openSelectedAgencyPlan(){
		websearch = [Select id, Combine_Quotation__c, Currency_Rates__c, Total_Products__c, Total_Products_Other__c, (Select id, Campus_Course__r.Campus__r.name, Campus_Course__r.Course__r.name, Total_Course__c, Total_Extra_Fees__c, Number_Instalments__c, Total_Products__c, Total_Products_Other__c, Value_First_Instalment__c, Total_Tuition__c from Search_Courses__r WHERE Course_Deleted__c = false order by course_order__c NULLS LAST, CreatedDate) from Web_Search__c where Id = :wsId];
		listAgencyPlans = [Select id, Agency__c, Deposit__c, Description__c, Group_Instalments__c, Instalments_Interest__c, Interest__c, Interest_Type__c, Name__c, Number_of_Instalments__c, Surcharge_Value__c, isSelected__c from Instalment_Plan__c where Agency__c = :AgencyId  order by Group_Instalments__c nulls first, Number_of_Instalments__c desc];
		listPlans = null;
		quoteSelectedPlans = null;
		getListPlans();
		return null;
	}
	
	private Web_Search__c websearch;
	public Boolean isCombinedQuote{get{if(isCombinedQuote == null) isCombinedQuote = false; return isCombinedQuote;} Set;}
	private list<Instalment_Plan__c> listAgencyPlans;
	
	public class agencyPlanOption{
		public String Name{get; Set;}
		public Boolean isSelected{get; Set;}
		private Instalment_Plan__c planDetails{get; Set;}
		public agencyPlanOption(String n, Boolean sel, Instalment_Plan__c ip){
			Name = n;
			isSelected = sel;
			planDetails = ip;
		}
	}
	
	public list<agencyPlanOption> listInstalmentPlanDetails;
	public list<agencyPlanOption> getlistInstalmentPlanDetails(){
		if(listInstalmentPlanDetails == null){
			listInstalmentPlanDetails = new list<agencyPlanOption> ();
			for(Instalment_Plan__c ip:listAgencyPlans){
				listInstalmentPlanDetails.add(new agencyPlanOption(ip.name__c, getquoteSelectedPlans().contains(ip.id), ip));
			}
		}
		
		return listInstalmentPlanDetails;
	}
	
	
	
	
	public class groupInstalment{
		public Integer instalmentNumber{get; Set;}
		public decimal instalmentValue{get; Set;}
		public groupInstalment(Integer numberInstal, Decimal valInstal){
			instalmentNumber = numberInstal;
			instalmentValue = valInstal;
		}
	}
	
	public class agencyInstalmentPlan{
		public Decimal totalCourse{get; Set;}
		public Decimal Deposit{get; Set;}
		public Decimal remainingValue{get; Set;}
		public integer numberInstalments{get; Set;}
		public list<groupInstalment> instalmentDetails{get{if(instalmentDetails == null) instalmentDetails = new list<groupInstalment>(); return instalmentDetails;} Set;}
		public string planDescription {get;Set;}
		public string name {get;Set;}
		public Decimal surchargeValue{get; Set;}
	}
	
	public class coursePlanOptions{
		public string Campus {get;Set;}
		public string Course {get;Set;}
		public list<agencyInstalmentPlan> listPlansAgency{get{if(listPlansAgency == null) listPlansAgency = new list<agencyInstalmentPlan>(); return listPlansAgency;} Set;}
	}
	
	public  map<string,list<agencyInstalmentPlan>> mapCoursePlan{
		get{
			if(mapCoursePlan == null){
				openSelectedAgencyPlan();
			}
			return mapCoursePlan;
		} 
		Set;
	}
	private list<coursePlanOptions> listPlans;
	
	private Search_Courses__c createSearchCourse() {
		Double totalCourse = 0;
		Double totalProducts = 0;
		Double totalProductsOther = 0;
		isCombinedQuote = true;
		for(Search_Courses__c course : websearch.search_courses__r){
			if(course.Number_Instalments__c > 1)
				totalCourse += course.Value_First_Instalment__c != null ? course.Value_First_Instalment__c : 0;
			else	
				totalCourse += course.Total_Course__c != null ? course.Total_Course__c : 0;
			
			totalProducts += course.Total_Products__c != null ? course.Total_Products__c : 0;
			
			totalProductsOther += course.Total_Products_Other__c != null ? course.Total_Products_Other__c : 0;
		}
		return new Search_Courses__c(Value_First_Instalment__c = totalCourse, Total_Products__c = totalProducts, Total_Products_Other__c = totalProductsOther, Total_Course__c = totalCourse);
	}
	
	public boolean foundAgencyPlan{Get;set;}
	
	public list<coursePlanOptions> getListPlans(){
		if(listPlans == null){
			foundAgencyPlan = false;
			agencyInstalmentPlan agencyPlan;
			coursePlanOptions coursePlan;
			double coefficient;
			Decimal totalCourseQuote = 0;
			list<Search_Courses__c> sc = new list<Search_Courses__c>();
			if(websearch.Combine_Quotation__c){
				sc.add(createSearchCourse());
			}else sc.addAll(websearch.Search_Courses__r);
			
			listPlans = new list<coursePlanOptions>();
			mapCoursePlan = new map<string,list<agencyInstalmentPlan>>();
			
			groupInstalment intalDetails;
			for(Search_Courses__c qd:sc){
				coursePlan = new coursePlanOptions();
				if(websearch.Combine_Quotation__c){
					coursePlan.Course = 'Combined Quotation';
				}else{
					coursePlan.Course = qd.Campus_Course__r.Course__r.name;
					coursePlan.Campus = qd.Campus_Course__r.Campus__r.name;
				}
				
				if(qd.Total_Products__c == null)
					qd.Total_Products__c = 0;
				if(qd.Total_Products_Other__c == null)
					qd.Total_Products_Other__c = 0;
				if(qd.Total_Extra_Fees__c == null)
					qd.Total_Extra_Fees__c = 0;
				if(qd.Total_Tuition__c == null)
					qd.Total_Tuition__c = 0;
				
				//totalCourseQuote = qd.Total_Tuition__c + qd.Total_Extra_Fees__c + qd.Total_Products__c;
				System.debug('===>: '+qd.Value_First_Instalment__c + ' + ' +qd.Total_Products__c +' - '+ qd.Total_Products_Other__c);
				if(websearch.Combine_Quotation__c){
					if(qd.Number_Instalments__c > 1)
						totalCourseQuote = qd.Value_First_Instalment__c + websearch.Total_Products__c - websearch.Total_Products_Other__c;
					else totalCourseQuote = qd.Total_Course__c + websearch.Total_Products__c - websearch.Total_Products_Other__c;
				}else{
					if(qd.Number_Instalments__c > 1)
						totalCourseQuote = qd.Value_First_Instalment__c + qd.Total_Products__c - qd.Total_Products_Other__c;
					else totalCourseQuote = qd.Total_Course__c + qd.Total_Products__c - qd.Total_Products_Other__c;
				}
					
				
				for(Instalment_Plan__c ip:listAgencyPlans){
					agencyPlan = new agencyInstalmentPlan();
					agencyPlan.name = ip.Name__c;
					
					agencyPlan.totalCourse = totalCourseQuote;
					if(ip.Surcharge_Value__c != null)
						agencyPlan.surchargeValue = totalCourseQuote * (ip.Surcharge_Value__c/100);
					else agencyPlan.surchargeValue = 0;
					if(ip.Description__c != null)
						agencyPlan.planDescription = ip.Description__c.replace('\n','<br />');
					else agencyPlan.planDescription =  '';
					//agencyPlan.planDescription = ip.Description__c;
					agencyPlan.numberInstalments = Integer.valueOf(ip.Number_of_Instalments__c);
					if(ip.Number_of_Instalments__c == 0){
						agencyPlan.instalmentDetails.add(new groupInstalment(0, agencyPlan.totalCourse + agencyPlan.surchargeValue));
						
					}else{
						agencyPlan.Deposit = totalCourseQuote * (ip.Deposit__c/100) + agencyPlan.surchargeValue;
						agencyPlan.remainingValue = totalCourseQuote - agencyPlan.Deposit + agencyPlan.surchargeValue;
						if(ip.Group_Instalments__c){
							if(ip.Interest_Type__c == 'Simple'){
								agencyPlan.instalmentDetails.add(new groupInstalment(agencyPlan.numberInstalments, (agencyPlan.remainingValue) * (1 + ip.Interest__c/100) / agencyPlan.numberInstalments));
							}else if(ip.Interest_Type__c == 'Compound'){
								agencyPlan.instalmentDetails.add(new groupInstalment(agencyPlan.numberInstalments, (agencyPlan.remainingValue * math.pow(  Double.valueOf(1 + ip.Interest__c/100) , Double.valueOf(agencyPlan.numberInstalments)))/agencyPlan.numberInstalments ));
							}else{
								if(ip.Interest__c == 0)
									agencyPlan.instalmentDetails.add(new groupInstalment(agencyPlan.numberInstalments, agencyPlan.remainingValue / Double.valueOf(agencyPlan.numberInstalments) ));
								else{
									Double interest = (ip.Interest__c/100);
									integer instalment = integer.valueOf(agencyPlan.numberInstalments);
									coefficient = interest / ( 1 - ( 1 / math.pow( ( 1 + interest), instalment )) );
									agencyPlan.instalmentDetails.add(new groupInstalment(instalment, agencyPlan.remainingValue * coefficient ));
									//agencyPlan.instalmentDetails.add(new groupInstalment(agencyPlan.numberInstalments, agencyPlan.remainingValue * (ip.Interest__c/100) / (1 - (math.pow( Double.valueOf(1 + (ip.Interest__c/100)), - Double.valueOf(agencyPlan.numberInstalments))) ) ));
								} 
									
							
							}
						}else if(ip.Instalments_Interest__c != null){ 
							
							for(String s:ip.Instalments_Interest__c.split(';')){
								Double instalment =  Double.valueOf(s.split(':')[0]);
								Double interest =  Double.valueOf(s.split(':')[1])/100;
								if(ip.Interest_Type__c == 'Simple'){
									agencyPlan.instalmentDetails.add(new groupInstalment(integer.valueOf(instalment), agencyPlan.remainingValue * (1 + interest) / integer.valueOf(instalment)) );
								}else if(ip.Interest_Type__c == 'Compound'){
									agencyPlan.instalmentDetails.add(new groupInstalment(integer.valueOf(instalment), (agencyPlan.remainingValue * math.pow(Double.valueOf(1 + interest) , instalment) )/instalment ));
								}
								else{
									if(interest == 0)
										agencyPlan.instalmentDetails.add(new groupInstalment(integer.valueOf(instalment), agencyPlan.remainingValue / instalment ));
									else {
										coefficient = interest / ( 1 - ( 1 / math.pow( ( 1 + interest), instalment )) );
										agencyPlan.instalmentDetails.add(new groupInstalment(integer.valueOf(instalment), agencyPlan.remainingValue * coefficient ));
										//agencyPlan.instalmentDetails.add(new groupInstalment(integer.valueOf(instalment), agencyPlan.remainingValue * interest / (1 - (math.pow(1 + interest, -instalment)) ) ));
									}
								}
								
							}
						}
					}
					coursePlan.listPlansAgency.add(agencyPlan);
					foundAgencyPlan = true;
					if(websearch.Combine_Quotation__c){
						if(getquoteSelectedPlans().contains(ip.id)){
							if(!mapCoursePlan.containsKey('combinedquote'))
								mapCoursePlan.put('combinedquote', new list<agencyInstalmentPlan>{agencyPlan});
							else mapCoursePlan.get('combinedquote').add(agencyPlan);
						}
					}else{
						if(getquoteSelectedPlans().contains(ip.id)){
							if(!mapCoursePlan.containsKey(qd.id))
								mapCoursePlan.put(qd.id, new list<agencyInstalmentPlan>{agencyPlan});
							else mapCoursePlan.get(qd.id).add(agencyPlan);
						}
					}
				}
				listPlans.add(coursePlan);
			}
			if(mapCoursePlan.size() == 0)
				mapCoursePlan = null;
		}
		return listPlans;
	}
	
	
	public PageReference addAgencyPlan(){
		list<Search_Course_Agency_Pay_Plan__c> listPayPlan = new list<Search_Course_Agency_Pay_Plan__c>();
		Search_Course_Agency_Pay_Plan__c payPlan;
		System.debug('==> listInstalmentPlanDetails: '+listInstalmentPlanDetails);
		delete [Select Id from Search_Course_Agency_Pay_Plan__c where Web_Search__c = :wsId];
		LIST<string> courseIds = new LIST<string>();
		for(agencyPlanOption lp: listInstalmentPlanDetails)
			if(lp.isSelected){
				payPlan = new Search_Course_Agency_Pay_Plan__c();
				payPlan.Web_Search__c = wsId;
				payPlan.Deposit__c = lp.planDetails.Deposit__c;
				payPlan.Description__c = lp.planDetails.Description__c;
				payPlan.Group_Instalments__c = lp.planDetails.Group_Instalments__c;
				payPlan.Instalments_Interest__c = lp.planDetails.Instalments_Interest__c;
				payPlan.Interest__c = lp.planDetails.Interest__c;
				payPlan.Interest_Type__c = lp.planDetails.Interest_Type__c;
				payPlan.Name__c = lp.planDetails.Name__c;
				payPlan.Number_of_Instalments__c = lp.planDetails.Number_of_Instalments__c;
				payPlan.Surcharge_Value__c = lp.planDetails.Surcharge_Value__c;
				payPlan.Instalment_Plan__c = lp.planDetails.Id;
				listPayPlan.add(payPlan);
			}
		
		if(listPayPlan.size() > 0){
			insert 	listPayPlan;
		}
		
		return null;
	}
	
	
	private set<string> quoteSelectedPlans;
	private set<string> getquoteSelectedPlans(){
		if(quoteSelectedPlans == null){
			quoteSelectedPlans = new set<string>();
			Web_Search__c agencyPlans = [Select id, ( Select Instalment_Plan__c from Search_Course_Agency_Pay_Plan__r) from Web_Search__c WHERE Id = :wsId limit 1];
			for(Search_Course_Agency_Pay_Plan__c ip:agencyPlans.Search_Course_Agency_Pay_Plan__r)
				quoteSelectedPlans.add(ip.Instalment_Plan__c);
		}
		return quoteSelectedPlans;
	}
	
	
	/*public map<string,list<agencyInstalmentPlan>> getMapCoursePlan(){
			if(mapCoursePlan == null){
				agencyInstalmentPlan agencyPlan;
				coursePlanOptions coursePlan;
				mapCoursePlan = new map<string,list<agencyInstalmentPlan>>();
				Decimal totalCourseQuote = 0;
				groupInstalment intalDetails;
				
				Web_Search__c agencyPlans = [Select id, (Select  Campus_Course__r.Course__r.name, Campus_Course__r.Campus__r.name, Total_Course__c, Total_Extra_Fees__c, Total_Products__c, Total_Tuition__c from Search_Courses__r where Course_Deleted__c = false order by course_order__c NULLS LAST, CreatedDate), ( Select Deposit__c, Description__c, Group_Instalments__c, Instalments_Interest__c, Interest__c, Interest_Type__c, Name__c, Number_of_Instalments__c, Surcharge_Value__c, Web_Search__c from Search_Course_Agency_Pay_Plan__r) from Web_Search__c WHERE Id = :wsId limit 1];
				for(Search_Courses__c qd:agencyPlans.Search_Courses__r){
					coursePlan = new coursePlanOptions();
					coursePlan.Course = qd.Campus_Course__r.Course__r.name;
					coursePlan.Campus = qd.Campus_Course__r.Campus__r.name;
					if(qd.Total_Products__c == null)
						qd.Total_Products__c = 0;
					if(qd.Total_Extra_Fees__c == null)
						qd.Total_Extra_Fees__c = 0;
					if(qd.Total_Tuition__c == null)
						qd.Total_Tuition__c = 0;
					totalCourseQuote = qd.Total_Tuition__c + qd.Total_Extra_Fees__c + qd.Total_Products__c;
					
					for(Search_Course_Agency_Pay_Plan__c ip:agencyPlans.Search_Course_Agency_Pay_Plan__r){
						
						agencyPlan = new agencyInstalmentPlan();
						agencyPlan.name = ip.Name__c;
						
						agencyPlan.totalCourse = totalCourseQuote;
						if(ip.Surcharge_Value__c != null)
							agencyPlan.surchargeValue = totalCourseQuote * (ip.Surcharge_Value__c/100);
						else agencyPlan.surchargeValue = 0;
						if(ip.Description__c != null)
							agencyPlan.planDescription = ip.Description__c.replace('\n','<br />');
						else agencyPlan.planDescription =  '';
						//agencyPlan.planDescription = ip.Description__c;
						agencyPlan.numberInstalments = Integer.valueOf(ip.Number_of_Instalments__c);
						if(ip.Number_of_Instalments__c == 0){
							agencyPlan.instalmentDetails.add(new groupInstalment(0, agencyPlan.totalCourse + agencyPlan.surchargeValue));
						
						}else{
							agencyPlan.Deposit = totalCourseQuote * (ip.Deposit__c/100) + agencyPlan.surchargeValue;
							agencyPlan.remainingValue = totalCourseQuote - agencyPlan.Deposit;
							if(ip.Group_Instalments__c){
								if(ip.Interest_Type__c == 'Simple'){
									agencyPlan.instalmentDetails.add(new groupInstalment(agencyPlan.numberInstalments, (agencyPlan.remainingValue) * (1 + ip.Interest__c/100) / agencyPlan.numberInstalments));
								}else if(ip.Interest_Type__c == 'Compound'){
									agencyPlan.instalmentDetails.add(new groupInstalment(agencyPlan.numberInstalments, (agencyPlan.remainingValue * math.pow(  Double.valueOf(1 + ip.Interest__c/100) , Double.valueOf(agencyPlan.numberInstalments)))/agencyPlan.numberInstalments ));
								}else{
									agencyPlan.instalmentDetails.add(new groupInstalment(agencyPlan.numberInstalments, agencyPlan.remainingValue * (ip.Interest__c/100) / (1 - (math.pow( Double.valueOf(1 + (ip.Interest__c/100)), - Double.valueOf(agencyPlan.numberInstalments))) ) ));
								}
								}else{ 
								for(String s:ip.Instalments_Interest__c.split(';')){
									Double instalment =  Double.valueOf(s.split(':')[0]);
									Double interest =  Double.valueOf(s.split(':')[1])/100;
									if(ip.Interest_Type__c == 'Simple'){
										agencyPlan.instalmentDetails.add(new groupInstalment(integer.valueOf(instalment), agencyPlan.remainingValue * (1 + interest) / integer.valueOf(instalment)) );
									}else if(ip.Interest_Type__c == 'Compound'){
										agencyPlan.instalmentDetails.add(new groupInstalment(integer.valueOf(instalment), (agencyPlan.remainingValue * math.pow(Double.valueOf(1 + interest) , instalment) )/instalment ));
									}
									else{
										agencyPlan.instalmentDetails.add(new groupInstalment(integer.valueOf(instalment), agencyPlan.remainingValue * interest / (1 - (math.pow(1 + interest, -instalment)) ) ));
									}
									
								}
							}
						}
						if(!mapCoursePlan.containsKey(courseId))
						mapCoursePlan.put(courseId, new list<agencyInstalmentPlan>{agencyPlan});
						else mapCoursePlan.get(courseId).add(agencyPlan);
						
					}
				}
				
			}
			return mapCoursePlan;
	} */
}