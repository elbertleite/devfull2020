/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class xCourseSearchCustomFee_test {

    static testMethod void myUnitTest() {
    	
    	TestFactory TestFactory = new TestFactory();
    	
		Account agency = TestFactory.createAgency();
		
		Contact employee = TestFactory.createEmployee(agency);
		
		Account school = TestFactory.createSchool();
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Cozy Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;
		insert p3;
		
		Account campus = TestFactory.createCampus(school, agency);
				
		Course__c course = TestFactory.createCourse();
		
		Campus_Course__c cc = TestFactory.createCampusCourse(campus, course);
		
		Course_Price__c cp = TestFactory.createCoursePrice(cc, 'Latin America');
		
		Course_Intake_Date__c cid = TestFactory.createCourseIntakeDate(cc);
		
		Course_Extra_Fee__c cef = TestFactory.createCourseExtraFee(cc, 'Latin America', p);
		
		Course_Extra_Fee_Dependent__c cefd = TestFactory.createCourseRelatedExtraFee(cef, p2);
		
		Deal__c extraFeeDeal = TestFactory.createCourseExtraFeePromotion(cc, 'Latin America', p);
		
		Deal__c freeUnits = TestFactory.createCourseFreeUnitsPromotion(cc, 'Latin America');
		
		Web_Search__c ws = TestFactory.createWebSearch(3, cc, 'Brazil', false);
		
		Search_Courses__c sc = TestFactory.createSearchCourse(ws, cc);
		
		Search_Courses__c custom = TestFactory.createCustomSearchCourse(ws);
		
		Search_Course_Product__c product = TestFactory.createSearchCourseProduct(ws, sc);
		
		ws.Currency_Rates__c = 'AUD:1.0;BRL:2.5;COP:150.54';
		
		update ws;
		
		
		
		Test.startTest();
		
		ApexPages.currentPage().getParameters().put('wsid', ws.id);
		xCourseSearchCustomFee cf = new xCourseSearchCustomFee();
		
		List<SelectOption> courses = cf.courses;
		List<SelectOption> typeOptions = cf.typeOptions;
		
		cf.newCustomFee.Name = 'Custom fee add';
		cf.newCustomFee.Value = 250;
		cf.newCustomFee.type = 'Add';
		cf.addCustomFee();
		
		cf.newCustomFee.Name = 'Custom fee subtract';
		cf.newCustomFee.Value = 99;
		cf.newCustomFee.type = 'Subtract';
		cf.addCustomFee();
		
		cf.newCustomFee.Name = 'Custom fee to delete';
		cf.newCustomFee.Value = 11;
		cf.newCustomFee.type = 'Add';
		cf.addCustomFee();
		
		ApexPages.currentPage().getParameters().put('cfid', '2');
		cf.deleteCustomFee();
		
		
		cf.saveCustomFees();
		
		cf.selectedCourses = new List<STring>{sc.id};
		
		cf.saveCustomFees();
		
		Test.stopTest();
    	
    }
}