/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class FileDeleteClass_test {

    static testMethod void myUnitTest() {
    	
    	TestFactory tf = new TestFactory();
    	Account agency = tf.createAgency();
    	Contact client1 = tf.createClient(agency);
		
		Client_Document__c cd = new Client_Document__c();
        cd.Document_Category__c = 'Personal';
        cd.Document_Type__c = 'Bank Details';
        cd.Client__c = client1.id;
        insert cd;
		
		Client_Document_File__c cdf= new Client_Document_File__c();
        cdf.Client__c = client1.id;
        cdf.File_Name__c = 'SomeFile.txt';
        cdf.ParentId__c = cd.id;
        insert cdf;
        
		
		
		ApexPages.currentPage().getParameters().put('page', '/apex/home.jsp');
		ApexPages.Standardcontroller controller = new APexpages.Standardcontroller(cd);		
		fileDeleteClass fdc = new fileDeleteClass(controller);
		fdc.deleteDocument();
		
		
		
		
		
		
		        
    }
}