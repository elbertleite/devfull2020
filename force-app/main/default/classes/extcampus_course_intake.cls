public with sharing class extcampus_course_intake{
    public Account acco{get;set;}

    public extcampus_course_intake(ApexPages.StandardController controller) {
        acco = [select id, parentId, Expected_Travel_date__c from account where id = :controller.getId() limit 1];

        if(getParam('cId') != null)
            campuscourseId = getParam('cId');

        if(getParam('year') != null)
            year = Integer.valueOf( getParam('year') );

        system.debug('==> campuscourseId0: '+campuscourseId);
        system.debug('==> acco.ParentId: '+acco.ParentId);

        selectedYear = year;
        selectCourse();

        system.debug('==> campuscourseId: '+campuscourseId);
    }

    public integer selectedYear {get;set;}
    public string SelectedDatesList
    {
        get
    {
        if(loadSelectedDatesList)
        {
            SelectedDatesList = SavedDatesStringList;
        }
        return SelectedDatesList;
    }
        set;
    }

    private boolean loadSelectedDatesList{get;set;}
    public integer year
    {
        get
    {
        if(year == null)
            year = System.today().year();
        return year;
    }
        set;
    }

    public String course{get; set;}

    public List<SelectOption> getCourseList() {
        List<SelectOption> options = new List<SelectOption>();
        //options.add(new SelectOption('--None--','--None--'));
        for (Campus_Course__c lst:[Select Is_Available__c,C.Course__r.Name from Campus_Course__c C WHERE C.Campus__r.ID = :acco.ID order by Course__r.Name])
            options.add(new SelectOption(lst.ID,lst.Is_Available__c?lst.Course__r.Name:lst.Course__r.Name+' (unavailable)'));
        return options;
    }




    public ID campusCourseID {
        get{
        try{
            if (campusCourseID == null && ApexPages.currentPage().getParameters().get('cId') == null){
                campusCourseID = [Select ID from Campus_Course__c C WHERE C.Campus__r.ID = :acco.ID  order by Course__r.Name limit 1].id;

                course = campusCourseID;

            }
            return campusCourseID;
        }catch(Exception e){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'There are currently no time table registered to this course.');
            ApexPages.addMessage(msg);
            return null;
        }
    }
        set;
    }

    public List<date> datesSaved
    {
        get{
        return datesSaved;
    }
        set;
    }

    public list<Campus_Course__c> lcc
    {
        get
    {
        if(lcc == null || selectedYear != year){
            lcc = retrieveIntakeDates();
            selectedYear = year;
        }
        return lcc;
    }
        set;
    }


    public void selectCourse()
    {
        lcc = null;
        SelectedDatesList = '';
        SavedDatesStringList = '';
        loadSelectedDatesList = true;
        campusCourseID = course;
        datesSaved = new List<date>();

        if(lcc != null && lcc.size() > 0){
            for(Course_Intake_Date__c cc : lcc[0].Course_Intake_Dates__r)
            {
                system.debug('===================================================================> cc.Instalment_Payment_Date__c:' + cc.Intake_Date__c);
                Date d = date.valueOf(cc.Intake_Date__c + '00:00:00');
                system.debug('===================================================================> d:' + d);
                datesSaved.add(d);
                system.debug('===================================================================> datesSaved.size:' + datesSaved.size());
                //SelectedDatesList = SavedDatesStringList;

                /*SelectedDatesList = '2011/01/' + Math.ceil(Math.random()*30);
                SavedDatesStringList = '2011/01/' + Math.ceil(Math.random()*30);*/
            }
        }
    }





    public string SavedDatesStringList {
        get{
        SavedDatesStringList = '';
        list<date> dl = new list<date>();

        if(lcc!=null && lcc.size()>0){
            datesSaved = new List<date>();
            for (Course_Intake_Date__c cc : lcc[0].Course_Intake_Dates__r){
                Date d = date.valueOf(cc.Intake_Date__c + '00:00:00');
                datesSaved.add(d);
                if(SavedDatesStringList.length()>0)
                    SavedDatesStringList+=',';
                SavedDatesStringList += d.year() + '/' + d.month() + '/' + d.day();
            }
        }

        return SavedDatesStringList;
    }
        set;
    }

    private List<Date> filterByYear(List<Date> dl)
    {
        List<Date> filteredDateList = new List<Date>();
        for(Date d:dl)
        {
            if(d.year() == year)
                filteredDateList.add(d);
        }
        return filteredDateList;
    }


    public PageReference save(){

        system.debug('===============> entrou ');
        loadSelectedDatesList = false;
        List<date> dl = filterByYear( StringListToDateList(SelectedDatesList) );
        loadSelectedDatesList = true;
        List<Course_Intake_Date__c> lOriginal =  new List<Course_Intake_Date__c>();
        List<Course_Intake_Date__c> lEdited = new List<Course_Intake_Date__c>();
        List<Course_Intake_Date__c> lDifference;

        //Fill lOriginal list
        if(lcc[0].Course_Intake_Dates__r != null && lcc[0].Course_Intake_Dates__r.size()>0)
            lOriginal = lcc[0].Course_Intake_Dates__r;

        //Fill  lEdited list
        for(Date d:dl){
            Course_Intake_Date__c cid = new Course_Intake_Date__c();
            cid.Intake_Date__c = d;
            cid.Campus_Course__c = campusCourseID;
            lEdited.add(cid);
        }


        system.debug('===================================================================> lOriginal.size:' + lOriginal.size());
        system.debug('===================================================================> lEdited.size:' + lEdited.size());


        //diference between lOriginal - lEdited, to find the deleted dates
        lDifference = new List<Course_Intake_Date__c>();
        for(Course_Intake_Date__c lo : lOriginal){
            boolean found = false;
            for(Course_Intake_Date__c le : lEdited){
                if(lo.Intake_Date__c == le.Intake_Date__c)
                {found=true; break;}

            }
            if(!found)
                lDifference.add(lo);
        }

        system.debug('===================================================================> to deleted lDifference.size:' + lDifference.size());

        DELETE lDifference;


        //diference between lEdited - lOriginal, to find the added dates
        lDifference = new List<Course_Intake_Date__c>();
        for(Course_Intake_Date__c le : lEdited){
            boolean found = false;
            for(Course_Intake_Date__c lo : lOriginal){
                if(le.Intake_Date__c == lo.Intake_Date__c)
                    found=true;
            }
            if(!found){
                le.Campus_Course__c = campusCourseID;
                lDifference.add(le);
            }
        }

        system.debug('===================================================================> to add lDifference.size:' + lDifference.size());

        INSERT lDifference;

        SelectedDatesList = '';
        SavedDatesStringList = '';
        lcc = null;
        selectCourse();

        return null;
    }
    public String apexMSG {get;set;}

    public PageReference saveExtras()
    {
        try {
            if(lcc != null && lcc.size()>0){
                upsert lcc[0].Course_Intake_Dates__r;
            }
        } catch (Exception e){
            apexMSG = e.getMessage();
        }
        return null;
    }

    private List<date> StringListToDateList(string sl){
        List<date> dl = new List<date>();

        if(sl.Length()>0){
            List<String> strDates = sl.split(',');
            for(String s : strDates)
            {
                s = s.replace('/','-');
                Date d = date.valueOf(s + ' 00:00:00');
                dl.add(d);
            }}

        return dl;
    }


    private List<Campus_Course__c> retrieveIntakeDates(){

        Date beginOfYear = date.newInstance(year, 1, 1);
        Date endOfYear = date.newInstance(year, 12, 31);

        list<Campus_Course__c>  tlcc = [Select C.Campus__c,C.Campus__r.name, C.Name, C.Id, C.Course__r.Name,
        ( Select Intake_Date__c,course_full__c, tbc__c, beginning_of_the_cycle__c, isToCopy__c,Campus_Course__c from Course_Intake_Dates__r where Intake_Date__c>=:beginOfYear and Intake_Date__c<=:endOfYear ORDER BY Intake_Date__c)
        from Campus_Course__c C where ID = :campusCourseID];

        return tlcc;
    }


    public String getParam(String name) {
        return ApexPages.currentPage().getParameters().get(name);
    }


    // CLONE INTAKE DATES //// CLONE INTAKE DATES //// CLONE INTAKE DATES //


    public List<Course_Intake_Date__c> intakeDates {
        get{
        if(intakeDates == null){
            if(campusCourseID == null)
                campusCourseID = getParam('cId');
            List<Campus_Course__c> listcc = retrieveIntakeDates();
            campus_course__c cc = listcc[0];
            intakeDates = cc.course_intake_dates__r;
        }
        return intakeDates;
    }
        set;
    }

    public List<Course_Intake_Date__c> intakeDatesClone {
        get{
        if(intakeDatesClone == null)
            intakeDatesClone = new List<Course_Intake_Date__c>();
        return intakeDatesClone;
    }
        set;
    }

    public PageReference cloneDates(){
        List<Campus_Course__c> listcc = retrieveIntakeDates();
        campus_course__c cc = listcc[0];
        intakeDates = cc.course_intake_dates__r;

        String pageSelected;
        if(!Test.isRunningTest()){
            pageSelected = ApexPages.currentPage().getUrl().substringBetween('apex/', '?');
        }else{
            pageSelected = 'Test';
        }

        PageReference p;
        if(pageSelected == 'school_campus_intake_dates'){
            p = Page.clone_intake_dates;
            p.getParameters().put('embedded','true');
        }else{
            p = Page.scCloneCoursesIntake;
        }
        p.setRedirect(true);
        p.getParameters().put('id',ApexPages.currentPage().getParameters().get('id'));
        p.getParameters().put('currentPage',ApexPages.currentPage().getParameters().get('currentPage'));
        p.getParameters().put('cId',campusCourseID);
        p.getParameters().put('year',String.valueOf(year));
        return p;
        //return null;
    }

    public Campus_Course__c campusCourse {
        get {
        if(campusCourse == null){
            campusCourse = [select Campus__r.Id, Course__r.Name from Campus_course__c where id= :campusCourseID];
        }
        return campusCourse;
    }
        set;
    }

    public void copyDatesToNextYear(){

        List<Course_Intake_Date__c> nextYearList = new List<Course_Intake_Date__c>();
        for(Course_Intake_Date__c cid : lcc[0].Course_Intake_Dates__r){

            Course_Intake_Date__c newDate = new Course_Intake_Date__c();
            newDate.Intake_Date__c = cid.Intake_Date__c.addYears(1);
            newDate.tbc__c = cid.tbc__c;
            newDate.course_full__c = cid.course_full__c;
            newDate.Campus_Course__c = cid.Campus_course__c;
            newDate.Beginning_of_the_cycle__c = cid.Beginning_of_the_cycle__c;
            nextYearList.add(newDate);
        }

        try{
            insert nextYearList;

        } catch(Exception e){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(msg);
        }


    }


    public String courseClone {get;set;}
    public list<String> listCourseClone {get{if(listCourseClone == null) listCourseClone = new list<String>(); return listCourseClone;}set;}
    public List<SelectOption> getCourseListClone() {
        List<SelectOption> options = new List<SelectOption>();
        //options.add(new SelectOption('','--None--'));

        for (Campus_Course__c lst : [Select Course__r.Name, Is_Available__c from Campus_Course__c WHERE Campus__r.ID = :selectedCampus order by Course__r.Name]){
            if(selectedCampus == acco.id && lst.Course__r.Name == campusCourse.Course__r.Name)
                continue;
            options.add(new SelectOption(lst.ID,lst.Is_Available__c?lst.Course__r.Name:lst.Course__r.Name+' (unavailable)'));
        }
        return options;
    }

    public void selectCourseClone(){
        intakeDatesClone = retrieveIntakeDatesClone();
    }


    private List<Course_Intake_Date__c> retrieveIntakeDatesClone(){

        Date beginOfYear = date.newInstance(year, 1, 1);
        Date endOfYear = date.newInstance(year, 12, 31);
        list<Course_Intake_Date__c>  tlcc = [Select Intake_Date__c,course_full__c, tbc__c, beginning_of_the_cycle__c, isToCopy__c
        from Course_Intake_Date__c
        where Intake_Date__c>=:beginOfYear
            and Intake_Date__c<=:endOfYear
            and Campus_course__c = :courseClone];
        return tlcc;
    }

    private List<Course_Intake_Date__c> cloneDatesToDelete;

    public void itemsToClone(){
        cloneDatesToDelete = intakeDatesClone;
        intakeDatesClone = new List<Course_Intake_Date__c>();

        for(Course_Intake_Date__c cid : intakeDates){
            if(cid.IsToCopy__c){
                Course_Intake_Date__c clone = new Course_Intake_Date__c();
                clone.Intake_Date__c = cid.intake_date__c;
                clone.Course_full__c = cid.course_full__c;
                clone.tbc__c = cid.tbc__c;
                clone.Beginning_of_the_cycle__c = cid.Beginning_of_the_cycle__c;
                clone.Campus_Course__c = courseClone;
                intakeDatesClone.add(clone);
                needSave = true;
            }
        }
    }

    public boolean needSave {get;set;}

    public pageReference saveClones(){
        Savepoint sp;
        try{
            sp = Database.setSavepoint();
            delete cloneDatesToDelete;
            upsert intakeDatesClone;
            apexMSG = 'Saved.';

        } catch (Exception e) {
            Database.rollback(sp);
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(msg);

        }
        selectCourseClone();
        return null;
    }

    public pageReference cancel(){
        selectCourseClone();
        return null;
    }

    // ================= CLONE UPDATES ================================================

    public boolean tbc{get{if(tbc == null) tbc = false; return tbc;} set;}
    public boolean full{get{if(full == null) full = false; return full;} set;}
    public boolean cycle{get{if(cycle == null) cycle = false; return cycle;} set;}

    public string selectedCampus{get{if(selectedCampus == null) selectedCampus = acco.id; return selectedCampus;} set;}
    private List<SelectOption> listCampuses;
    public List<SelectOption> getlistCampuses() {
        if(listCampuses == null){
            listCampuses = new List<SelectOption>();
            for (Account lst:[Select id, Name from Account WHERE parentId = :acco.ParentId and recordType.name = 'campus' order by Name])
                listCampuses.add(new SelectOption(lst.ID,lst.Name));
        }
        return listCampuses;
    }

    public pageReference cloneSelectedDays(){
        list<Course_Intake_Date__c> clonnedIntakes = new list<Course_Intake_Date__c>();
        Course_Intake_Date__c clone;
        for(Course_Intake_Date__c cid : intakeDates){
            if(cid.IsToCopy__c){
                for(string lc:listCourseClone){
                    clone = new Course_Intake_Date__c();
                    clone.Intake_Date__c = cid.intake_date__c;
                    clone.Campus_Course__c = lc;
                    if(full)
                        clone.Course_full__c = cid.course_full__c;
                    if(tbc)
                        clone.tbc__c = cid.tbc__c;
                    if(cycle)
                        clone.Beginning_of_the_cycle__c = cid.Beginning_of_the_cycle__c;
                    clonnedIntakes.add(clone);
                }
            }
        }
        delete [Select id from Course_Intake_Date__c where Campus_Course__c in :listCourseClone and CALENDAR_YEAR(Intake_Date__c) = :selectedYear];
        insert clonnedIntakes;
        return null;
    }

}