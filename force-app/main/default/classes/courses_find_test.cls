@isTest
private class courses_find_test {

    static testMethod void testSearch() {
    	
    	TestFactory tf = new TestFactory();
       
		Account agency = tf.createAgency();
		
		Contact employee = tf.createEmployee(agency);

        User portalUser = tf.createPortalUser(employee);

        Account school = tf.createSchool();
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;
		insert p3;
		
		
		Account campus = tf.createCampus(school, agency);
		    
		Course__c course = tf.createCourse();
		
		Course__c course2 = tf.createLanguageCourse();
				
		Campus_Course__c cc = tf.createCampusCourse(campus, course);
		
		Campus_Course__c cc2 = tf.createCampusCourse(campus, course2);

        Course_Price__c cp = tf.createCoursePrice(cc, 'Latin America');
		
		Course_Price__c cp2 = tf.createCoursePrice(cc2, 'Published Price');

        Course_Intake_Date__c cid = tf.createCourseIntakeDate(cc);
		
		Course_Intake_Date__c cid2 = tf.createCourseIntakeDate(cc2);
        
		Course_Extra_Fee__c cef = tf.createCourseExtraFee(cc, 'Latin America', p);

        Course_Extra_Fee_Dependent__c cefd = tf.createCourseRelatedExtraFee(cef, p2);
		
		tf.createCourseExtraFeeCombined(cc, 'Latin America', p, p2);
        
		Course_Extra_Fee__c cef2 = new Course_Extra_Fee__c();
		cef2.Campus__c = campus.Id;
		cef2.Nationality__c = 'Published Price';
		cef2.Availability__c = 3;
		cef2.From__c = 2;
		cef2.Value__c = 100;		
		cef2.Optional__c = false;
		cef2.Product__c = p.id;
		cef2.date_paid_from__c = system.today();
		cef2.date_paid_to__c = system.today().addDays(+31);
		insert cef2;
		
		
		
		
		
		
		Deal__c d = new Deal__c();
		d.Availability__c = 3;
		d.Campus_Account__c = campus.id;
		d.From__c = 1;
		d.Extra_Fee__c = cef2.id;
		d.Promotion_Type__c = 3;
		d.From_Date__c = system.today();
		d.To_Date__c = system.today().addDays(60);
		d.Extra_Fee_Type__c = 'Cash';
		d.Extra_Fee_Value__c = 60;
		d.Product__c = p.id;
		d.Nationality_Group__c = 'Publish Price';
		insert d;
		
		
		Deal__c d2 = new Deal__c();
		d2.Availability__c = 3;
		d2.Campus_Course__c = cc2.id;
		d2.From__c = 1;		
		d2.Promotion_Type__c = 2;
		d2.From_Date__c = system.today();
		d2.To_Date__c = system.today().addDays(60);
		d2.Promotion_Weeks__c = 1;
		d2.Number_of_Free_Weeks__c = 1;
		d2.Nationality_Group__c = 'Latin America';
		d2.Promotion_Name__c = '1+1';
		d2.Promo_Price__c = 50;		
		insert d2;
		
		Start_Date_Range__c sdr = new Start_Date_Range__c();
		sdr.Promotion__c = d2.id;
		sdr.Campus__c = cc2.campus__c;
		sdr.From_Date__c = system.today();
		sdr.To_Date__c = system.today().addDays(15);
		sdr.Number_of_Free_Weeks__c = 2;
		insert sdr;		
		
		
		
		Deal__c d3 = new Deal__c();
		d3.Availability__c = 3;
		d3.Campus_Course__c = cc.id;
		d3.From__c = 1;
		d3.Extra_Fee__c = cef.id;
		d3.Promotion_Type__c = 3;
		d3.From_Date__c = system.today();
		d3.To_Date__c = system.today().addDays(60);
		d3.Extra_Fee_Type__c = 'Cash';
		d3.Extra_Fee_Value__c = 50;
		d3.Nationality_Group__c = 'Latin America';
		insert d3;
		
		Deal__c d4 = new Deal__c();
		d4.Availability__c = 3;
		d4.Campus_Course__c = cc.id;
		d4.From__c = 1;		
		d4.Promotion_Type__c = 2;
		d4.Promotion_Name__c = '5+2';
		d4.Promo_Price__c = 50;		
		d4.From_Date__c = system.today();
		d4.To_Date__c = system.today().addDays(61);
		d4.Promotion_Weeks__c = 5;
		d4.Number_of_Free_Weeks__c = 2;
		d4.Nationality_Group__c = 'Published Price';
		insert d4;
		
		
		
		Deal__c d5 = new Deal__c();
		d5.Availability__c = 3;
		d5.Campus_Course__c = cc.id;
		d5.Extra_Fee_Dependent__c = cefd.id;
		d5.From__c = 1;		
		d5.Promotion_Type__c = 3;
		d5.From_Date__c = system.today();
		d5.To_Date__c = system.today().addDays(62);
		d3.Extra_Fee_Type__c = 'Cash';
		d3.Extra_Fee_Value__c = 50;
		d5.Nationality_Group__c = 'Latin America';
		d5.Product__c = p3.id;

        Test.startTest();
        system.runAs(portalUser){
            courses_find testClass = new courses_find();
            string supplierIds_json = testClass.supplierIds_json;
            list<string> supplierIds = new list<string>(courses_find.supplierIds_json_angular().keySet());
            map<string,boolean> supplierIdsMap = new map<string,boolean>(courses_find.supplierIds_json_angular());
            courses_find.changeNationalityGroup();
            
            list<string> country = new list<string>();
            for(AggregateResult ar:courses_find.changeDestinationCountry(supplierIds)){
                system.System.debug('==> ar: '+ar);
                country.add((string) ar.get('BillingCountry'));
            }
           
            list<string> lstDestinations = new list<string>();
            for(AggregateResult ar:courses_find.changeDestinationCity(supplierIds, country))
                lstDestinations.add((string) ar.get('BillingCity'));

            list<string> listCourseCategory = new list<string>();
            for(AggregateResult ar:courses_find.changeCourseCategories(supplierIds, country, lstDestinations))
                listCourseCategory.add((string) ar.get('Course_Category__c'));

            courses_find.changeCourseArea(supplierIds, country, lstDestinations, listCourseCategory);

            list<string> listCourseType = new list<string>();
            courses_find.changeCourseQualification(supplierIds, country, lstDestinations, listCourseCategory, listCourseType);

            list<string> listCourseQualification = new list<string>();
            courses_find.changeCourseType(supplierIds, country, lstDestinations, listCourseCategory, listCourseType, listCourseQualification);

            list<string> listType = new list<string>();
            courses_find.changeCourseSpecialization(supplierIds, country, lstDestinations, listCourseCategory, listCourseType, listCourseQualification, listType); 

            list<string> listSpecialization = new list<string>();
            courses_find.changeSchool(supplierIds, country, lstDestinations, listCourseCategory, listCourseType, listCourseQualification, listType, listSpecialization);

            list<string> listPeriods = new list<string>();
            list<string> listSchool = new list<string>();
            list<string> listSchools = new list<string>();
            courses_find.searchCountryFilters(supplierIds, country, lstDestinations, listCourseCategory, listCourseType, listCourseQualification, listType, listSpecialization, listPeriods, listSchools);

            list<string>  cities = new list<string>();
            DateTime ds = System.Today().addDays(5) ;
            String dt =  ds.format('yyyy-MM-dd') ;
            courses_find.searchCourses(supplierIdsMap, supplierIds, 'Brazil', 'Australia',  cities,  listCourseCategory,  listCourseType, listCourseQualification, 
									listType, listSpecialization, listSchool, listPeriods, dt, 12, 1, 50, '3', 1, 50, '0', 5,  listSchools);

            courses_find.changePeriods();
            courses_find.changeClientNationality();
            courses_find.maxCourseLength();
            courses_find.retrieveAgencyCurrencies();

            Web_Search__c cart = courses_find.retrieveCart('','Latin America');
           
            courses_find.retrieveUserDetails();

            courses_find.retrieveCoursesCart(cart.id);

            ApexPages.currentPage().getParameters().put('campusCourse', cc.id);
            ApexPages.currentPage().getParameters().put('campus', campus.id);
            ApexPages.currentPage().getParameters().put('numUnits', '12');

            ApexPages.currentPage().getParameters().put('clientNationality','Latin America');
		    ApexPages.currentPage().getParameters().put('cartId', cart.id);
		    ApexPages.currentPage().getParameters().put('location', '3');
            testClass.addCourseToCart();


       
            searchcoursecountry testClass2 = new searchcoursecountry();
            searchcoursecountry.changeNationalityGroup();
            testClass2.getPriceStartDateRanges();
            testClass2.calculateEndDate(System.today(), 10, 'Day');
            testClass2.calculateEndDate(System.today(), 10, 'Month');
            list<string> campusType;
            testClass2.Nationalities = 'Brazil';
            testClass2.selectedSchool = new list<string>{school.id};
            list<searchcoursecountry.courseDetails> lc = testClass2.createListCourses(new list<string>{cc.id}, 12, campusType, null, null, null, false, null, '', false);
        }
        Test.stopTest();

    }
}