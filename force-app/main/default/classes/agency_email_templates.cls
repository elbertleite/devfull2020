public with sharing class agency_email_templates {

	private string agencyId;
	public agency_email_templates(ApexPages.StandardController ctrl) {
		agencyId = ctrl.getId();
		retrieveCategoryTokes();
		retrieveTemplates();
	}

	public map<string,list<Email_Template__c>> templates{get{if(templates==null) templates = new map<string,list<Email_Template__c>>(); return templates;} set;}

	public void retrieveTemplates(){
		templates = new map<string,list<Email_Template__c>>();
		for(Email_Template__c et:[Select id, Template__c, Template_Description__c, Description__c, Email_Subject__c, Category__c, lastModifiedBy.name, lastModifiedDate from Email_Template__c where Agency__c = :agencyId order by Category__c, Template_Description__c])
			if(!templates.containsKey(et.Category__c))
				templates.put(et.Category__c, new list<Email_Template__c>{et});
			else templates.get(et.Category__c).add(et);
	}

	public Email_Template__c newTemplate{get{if(newTemplate==null) newTemplate = new Email_Template__c(agency__c = agencyId); return newTemplate;} set;}
	public pageReference saveTemplate(){
		if(newTemplate.Template_Description__c == null || newTemplate.Template_Description__c.trim() == ''){
			newTemplate.Template_Description__c.addError('Email Template Name is Required');
			return null;
		}
		if(newTemplate.Template__c == null || newTemplate.Template__c.trim() == ''){
			newTemplate.Template__c.addError('Email Content is Required');
			return null;
		}
		upsert newTemplate;
		newTemplate = new Email_Template__c(agency__c = agencyId);
		retrieveTemplates();
		return null;
	}

    private map<string, string> categoryTokens;
	private void  retrieveCategoryTokes(){
		categoryTokens = new map<string, string>();
		categoryTokens.put('Send LOO Student','tokensCourse');
		categoryTokens.put('Send COE Student','tokensCourse');
		categoryTokens.put('Lead/Client Mass Emails','tokensContact');
		categoryTokens.put('Request COE','tokensInstalment');
		categoryTokens.put('Request LOO','tokensCourse');
		categoryTokens.put('School Payments','tokensInvoice');
		categoryTokens.put('Clients Payments Email','tokensInstalment');
		categoryTokens.put('Client Payments SMS','tokensInstalment');
		categoryTokens.put('Send Student Email','tokensContact');
		categoryTokens.put('Course Refund','tokensPayment');
		categoryTokens.put('Provider Payment','tokensInvoice');
		categoryTokens.put('Products Request Commission','tokensInvoice');
		categoryTokens.put('Quotation','tokensQuotation');
		//categoryTokens.put('Cancel Course','tokensCourse');
		categoryTokens.put('Cancel Course','tokensCancel');
		categoryTokens.put('WhatsApp','tokensWhatsapp');

		// categoryTokens.put('Provider Payment','tokensContact');
		
		// categoryTokens.put('refund','tokensInstalment');
		// categoryTokens.put(,'tokensCourse');
		// categoryTokens.put(,'tokensInvoice');
		
	}
	public list<string> listTags{get; set;}
	public map<string, Object> mapFields{get; set;}
	public void availableTags(){
		listTags = new list<string>();
		mapFields = new map<string, Object>();
		System.debug('==>newTemplate.Category__c: '+newTemplate.Category__c);
		mapFields = IPFunctions.findTokens(categoryTokens.get(newTemplate.Category__c));
		System.debug('==>mapFields: '+mapFields);
		listTags.addAll(mapFields.keySet());
		listTags.sort();
	}

	public pageReference editTemplate(){
		string tpId = ApexPages.CurrentPage().getParameters().get('tpId');
		newTemplate = [Select id, Template__c, Template_Description__c, Description__c, Email_Subject__c, Category__c, lastModifiedBy.name, lastModifiedDate from Email_Template__c where id = :tpId];
		return null;
	}

	public pageReference deleteTemplate(){
		string tpId = ApexPages.CurrentPage().getParameters().get('tpId');
		delete [Select id from Email_Template__c where id = :tpId];
		newTemplate = new Email_Template__c(agency__c = agencyId);
		retrieveTemplates();
		return null;
	}

	public pageReference cancelEdition(){
		newTemplate = new Email_Template__c(agency__c = agencyId);
		retrieveTemplates();
		return null;
	}
	
}