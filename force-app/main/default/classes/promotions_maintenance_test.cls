/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class promotions_maintenance_test {
	
	static testMethod void myTestMethod() {
        Map<String,String> recordTypes = new Map<String,String>();
  		for(RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
		
		
		Account school = new Account();
		school.recordtypeid = recordTypes.get('School');
		school.name = 'Test School';
		school.BillingCountry = 'Australia';
		school.BillingCity = 'Sydney';		
		insert school;
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		Nationality_Group__c ng = new Nationality_Group__c();
		ng.Account__c = school.id;
		ng.Country__c = 'Brazil';
		ng.Name = 'Latin America';
		insert ng;
       
		Account campus = new Account();
		campus.RecordTypeId = recordTypes.get('Campus');
		campus.Name = 'Test Campus CBD';
		campus.BillingCountry = 'Australia';
		campus.BillingCity = 'Sydney';
		campus.ParentId = school.id;
		insert campus;
        
		       
		Course__c course = new Course__c();
		course.Name = 'Certificate III in Business';        
		course.Type__c = 'Business';
		course.Sub_Type__c = 'Business';
		course.Course_Qualification__c = 'Certificate III';
		course.Course_Type__c = 'VET';
		course.Course_Unit_Type__c = 'Week';
		course.Only_Sold_in_Blocks__c = true;
		course.Period__c = 'Morning';
		course.School__c = school.id;
		insert course;
       
		Campus_Course__c cc = new Campus_Course__c();
		cc.Course__c = course.Id;
		cc.Campus__c = campus.Id;        
		cc.Is_Available__c = true;        
		insert cc;
		
		
		Account_Document_File__c folder = new Account_Document_File__c();
		folder.File_Name__c = 'The Folder';
		folder.Parent__c = campus.id;
		folder.Content_Type__c = 'Folder';
		insert folder;
		
		Account_Document_File__c adf = new Account_Document_File__c();
		adf.File_Name__c = 'Campus File';
		adf.Parent__c = campus.id;
		adf.Parent_Folder_Id__c = folder.id;
		insert adf;
		
		
		Course_Extra_Fee__c cef = new Course_Extra_Fee__c();
		cef.Campus_Course__c = cc.Id;
		cef.Nationality__c = 'Latin America';
		cef.From__c = 1;
		cef.Account_Document_File__c = adf.id;
		cef.Value__c = 100;
		cef.Extra_Fee__c = 'Placement Fee';
		cef.Product__c = p.id;
		cef.Campus_Parent__c = campus.id;
		insert cef;
		
		Course_Extra_Fee_Dependent__c cefd = new Course_Extra_Fee_Dependent__c();
		cefd.Course_Extra_Fee__c = cef.id;
		cefd.Product__c = p.id;
		cefd.Details__c = 'Testing';
		insert cefd;
		
		cef = new Course_Extra_Fee__c();
		cef.Campus__c = campus.Id;
		cef.Nationality__c = 'Latin America';
		cef.Value__c = 200;
		cef.From__c = 2;
		cef.Product__c = p.id;
		cef.Extra_Fee__c = 'OSHC';
		cef.Optional__c = true;
		cef.Campus_Parent__c = campus.id;
		insert cef;
        
		Deal__c promotion = new Deal__c();
		promotion.Campus_Course__c = cc.id;		      
		promotion.Promotion_Type__c = 3;     
		promotion.From__c = 1;
		promotion.Extra_Fee__c = cef.id;
		promotion.Product__c = p.id;
		promotion.Extra_Fee_Name__c = 'OSHC Discount';
		promotion.Extra_Fee_Value__c = 100;
		promotion.Nationality_Group__c = 'Latin America';
		promotion.To_Date__c = system.today();
		promotion.From_Date__c = system.today().addDays(60);
		promotion.Campus_Parent__c = campus.id;
		insert promotion;
		
		
		Deal__c promo = new Deal__c();
		promo.Account_Document_File__c = adf.id;
		promo.Nationality_Group__c = 'Latin America';
		promo.Availability__c = 3;
		promo.Campus_Course__c = cc.id;
		promo.Promotion_Type__c = 2;
		promo.Promotion_Weeks__c = 10;
		promo.From_Date__c = system.today().addDays(-60);
		promo.To_Date__c = system.today().addDays(60);
		promo.Number_of_Free_Weeks__c = 2;
		promo.Campus_Parent__c = campus.id;
		insert promo;
		
		
		Deal__c promotion2 = new Deal__c();
		promotion2.Campus_Account__c = campus.id;		      
		promotion2.Promotion_Type__c = 3;     
		promotion2.From__c = 1;
		promotion2.Extra_Fee__c = cef.id;
		promotion2.Product__c = p.id;
		promotion2.Extra_Fee_Name__c = 'OSHC Discount';
		promotion2.Extra_Fee_Value__c = 100;
		promotion2.Nationality_Group__c = 'Latin America';
		promotion2.To_Date__c = system.today();
		promotion2.From_Date__c = system.today().addDays(60);
		promotion2.Campus_Parent__c = campus.id;
		insert promotion2;
		
		TestFactory testFactory = new TestFactory();
		
		Deal__c d2 = testFactory.createCourseExtraFeePromotion(cc, 'Published Price', p);
		
		d2 = testFactory.createCourseFreeUnitsPromotion(cc, 'Published Price');
		
		Test.startTest();
		
		Apexpages.Standardcontroller controller = new ApexPages.Standardcontroller(campus);
		promotions_maintenance pm = new promotions_maintenance(controller);
		
		pm.promoAction = 'new';
		
		pm.getCloneOptions();
		String str = pm.cloneOption;
		
		pm.getpromoActionOptions();
		str = pm.promoAction;
		
		pm.getlistpromoFeesOption();
		str = pm.promoFeesOption;
		
		pm.getselectedCourseTypeOptions();
		List<String> lStr = pm.selectedCourseType;
		
		pm.getNationalitiesEdit();
		lStr = pm.selectedNationalities;
		pm.selectedNationalities = new List<String>{'Australia'};
		
		pm.selectFeesOption();
		
		
		pm.getCourseAvailableList();
		boolean courseAv = pm.CourseAvailable;
		
		pm.getPeriods();
		List<String> listString = pm.period;
		
		pm.getCourseListClone();
		pm.courseClone = new List<String>{cc.id};
		
		pm.newPromotionDetails.Promotion_Name__c = 'Promotion 1';
		pm.newPromotionDetails.Promotion_Weeks__c = 5;
		pm.newPromotionDetails.Number_of_Free_Weeks__c = 1;
		pm.newPromotionDetails.Availability__c = 3.0;
		pm.newPromotionDetails.From_Date__c = system.today().addDays(-7);
		pm.newPromotionDetails.To_Date__c = system.today().addDays(365);
		pm.newPromotionDetails.Comments__c = 'testing!';
		
		pm.addMultipleFeesDiscount();
		
		pm.addMultipleFeesDiscount(); //force error
		
		
		
		pm.promoAction = 'edit';
		
		
		
		
		
		pm.selectPromotions(); //SEARCH
		
		pm.getPriceAvailabilityList();
		List<String> avail = pm.filterAvailability;
		
		str = pm.listFeesView;
		pm.getlistFeesViewOptions();
		
		pm.refreshFees();
		
		pm.getExtraFeeName();
		lstr = pm.selectedExtraFeeName;
		
		List<promotions_maintenance.promotionDetails> promotions = pm.lstPromotions;
		
		List<SelectOption> files = pm.Files;
		
		pm.dateFilter = '15/05/2015';
		pm.filterDates();
		
		pm.setEditMode();
		
		for(promotions_maintenance.promotionDetails pd : pm.lstPromotions)
			for(Deal__c d : pd.promotion)
				d.Comments__c = 'testing';
			
		
		
		pm.savePromotionsUpdate();
		
		
		pm.showGrouped = true;
		pm.selectPromotions(); //SEARCH
		
		
		pm.getgroupedCoursePromotion();
		pm.setEditMode();
		
		for(String ccKey : pm.getgroupedCoursePromotion().keyset())
			for(String key : pm.getgroupedCoursePromotion().get(ccKey).keyset())				
				for(Deal__c d : pm.getgroupedCoursePromotion().get(ccKey).get(key).coursePromotion)
					d.Comments__c = 'testing';
				
		pm.savePromotionsUpdate();
		
		for(String ccKey : pm.getgroupedCoursePromotion().keyset())
			for(String key : pm.getgroupedCoursePromotion().get(ccKey).keyset())				
				for(Deal__c d : pm.getgroupedCoursePromotion().get(ccKey).get(key).coursePromotion)
					d.isSelected__c = true;
		
		pm.deletePromotions();
		
		pm.showGrouped = false;
		for(promotions_maintenance.promotionDetails pd : pm.lstPromotions)
			for(Deal__c d : pd.promotion)
				d.isSelected__c = true;
		pm.deletePromotions();
		
		
		pm.promoAction = 'new';
		pm.cloneOption = '3.0';
		
		
		pm.getselectedCourseRelatedPromotionOptions();
		pm.getselectedCourseRelatedFeePromotionOptions();
		
		pm.selectedCourseRelatedFeePromotion = cef.Product__c;
		pm.selectedCourseRelatedPromotion = cefd.Product__c;
		pm.selectPromotions();
		pm.getCourseListClone();
		pm.promoFeesOption = '1';
		pm.promoAction = 'edit';
		pm.selectFeesOption();
		
		pm.getselectedCourseRelatedPromotionOptions();
		pm.getselectedCourseRelatedFeePromotionOptions();
		pm.getCoursesDeals();		
		pm.refreshFees();
		
		
		pm.selectPromotions();		
		pm.refreshRelatedFees();
		pm.cleanListClone();
		pm.getCourseListClone();
		pm.getCoursesExtraFeePromotions();
		pm.selectCourseAvailable();
		
		pm.selectFeeOption();
		pm.selectActionOption();
		
		pm.selectedCourseRelatedPromotion = cefd.Product__c;
		pm.getCourseListClone();
		
		pm.getgroupedCampusPromotion();
		
		
		pm.getPriceOption();
		
		for(Course_Extra_Fee__c cef2 : pm.getCoursesExtraFeePromotions())
			cef2.selectedFee__c = true;
		
		pm.newPromotionDetails.Promotion_Name__c = 'Promotion Fee';
		pm.newPromotionDetails.From__c = 1;
		pm.newPromotionDetails.Extra_Fee_Type__c = 'Cash';
		pm.newPromotionDetails.Extra_Fee_Value__c = 50;		
		pm.newPromotionDetails.Availability__c = 3.0;
		pm.newPromotionDetails.From_Date__c = system.today().addDays(-7);
		pm.newPromotionDetails.To_Date__c = system.today().addDays(365);
		pm.newPromotionDetails.Comments__c = 'testing!';
		
		pm.addMultipleFeesDiscount();
		
		
		
			
		pm.getCategoryOptions();
		
		pm.cancel();
		
		pm.changePeriod();
		
		Test.stopTest();
	
	}
	
	
	static testMethod void myTestMethod2() {
		
		
        Map<String,String> recordTypes = new Map<String,String>();
  		for(RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
		
		
		Account school = new Account();
		school.recordtypeid = recordTypes.get('School');
		school.name = 'Test School';
		school.BillingCountry = 'Australia';
		school.BillingCity = 'Sydney';		
		insert school;
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		Nationality_Group__c ng = new Nationality_Group__c();
		ng.Account__c = school.id;
		ng.Country__c = 'Brazil';
		ng.Name = 'Latin America';
		insert ng;
       
		Account campus = new Account();
		campus.RecordTypeId = recordTypes.get('Campus');
		campus.Name = 'Test Campus CBD';
		campus.BillingCountry = 'Australia';
		campus.BillingCity = 'Sydney';
		campus.ParentId = school.id;
		insert campus;
        
		
		Course__c course = new Course__c();
		course.Name = 'Certificate III in Business';        
		course.Type__c = 'Business';
		course.Sub_Type__c = 'Business';
		course.Course_Qualification__c = 'Certificate III';
		course.Course_Type__c = 'VET';
		course.Course_Unit_Type__c = 'Week';
		course.Only_Sold_in_Blocks__c = true;
		course.Period__c = 'Morning';
		course.School__c = school.id;
		insert course;
       
		Campus_Course__c cc = new Campus_Course__c();
		cc.Course__c = course.Id;
		cc.Campus__c = campus.Id;        
		cc.Is_Available__c = true;        
		insert cc;
		
		
		Account_Document_File__c folder = new Account_Document_File__c();
		folder.File_Name__c = 'The Folder';
		folder.Parent__c = campus.id;
		folder.Content_Type__c = 'Folder';
		insert folder;
		
		Account_Document_File__c adf = new Account_Document_File__c();
		adf.File_Name__c = 'Campus File';
		adf.Parent__c = campus.id;
		adf.Parent_Folder_Id__c = folder.id;
		insert adf;
		
		
		Course_Extra_Fee__c cef = new Course_Extra_Fee__c();
		cef.Campus_Course__c = cc.Id;
		cef.Nationality__c = 'Latin America';
		cef.From__c = 1;
		cef.Account_Document_File__c = adf.id;
		cef.Value__c = 100;
		cef.Extra_Fee__c = 'Placement Fee';
		cef.Product__c = p.id;
		insert cef;
		
		Course_Extra_Fee_Dependent__c cefd = new Course_Extra_Fee_Dependent__c();
		cefd.Course_Extra_Fee__c = cef.id;
		cefd.Product__c = p.id;
		cefd.Details__c = 'Testing';
		insert cefd;
		
		cef = new Course_Extra_Fee__c();
		cef.Campus__c = campus.Id;
		cef.Nationality__c = 'Latin America';
		cef.Value__c = 200;
		cef.From__c = 2;
		cef.Product__c = p.id;
		cef.Extra_Fee__c = 'OSHC';
		cef.Optional__c = true;
		insert cef;
        
		Deal__c promotion = new Deal__c();
		promotion.Campus_Course__c = cc.id;		      
		promotion.Promotion_Type__c = 3;     
		promotion.From__c = 1;
		promotion.Extra_Fee__c = cef.id;
		promotion.Product__c = p.id;
		promotion.Extra_Fee_Name__c = 'OSHC Discount';
		promotion.Extra_Fee_Value__c = 100;
		promotion.Nationality_Group__c = 'Latin America';
		promotion.To_Date__c = system.today();
		promotion.From_Date__c = system.today().addDays(60);
		insert promotion;
		
		Start_Date_Range__c sdr = new Start_Date_Range__c();
		sdr.Promotion__c = promotion.id;
		sdr.Campus__c = cc.Campus__c;
		sdr.From_Date__c = system.today();
		sdr.To_Date__c = system.today().addDays(30);
		sdr.Value__c = 200;
		insert sdr;
		
		
		Deal__c promo = new Deal__c();
		promo.Account_Document_File__c = adf.id;
		promo.Nationality_Group__c = 'Latin America';
		promo.Availability__c = 3;
		promo.Campus_Course__c = cc.id;
		promo.Promotion_Type__c = 2;
		promo.Promotion_Weeks__c = 10;
		promo.From_Date__c = system.today().addDays(-60);
		promo.To_Date__c = system.today().addDays(60);
		promo.Number_of_Free_Weeks__c = 2;
		insert promo;
		
		sdr = new Start_Date_Range__c();
		sdr.Promotion__c = promo.id;
		sdr.Campus__c = cc.Campus__c;
		sdr.From_Date__c = system.today();
		sdr.To_Date__c = system.today().addDays(30);
		sdr.Number_of_Free_Weeks__c = 3;
		insert sdr;
		
		
		Deal__c promotion2 = new Deal__c();
		promotion2.Campus_Account__c = campus.id;		      
		promotion2.Promotion_Type__c = 3;     
		promotion2.From__c = 1;
		promotion2.Extra_Fee__c = cef.id;
		promotion2.Product__c = p.id;
		promotion2.Extra_Fee_Name__c = 'OSHC Discount';
		promotion2.Extra_Fee_Value__c = 100;
		promotion2.Nationality_Group__c = 'Latin America';
		promotion2.To_Date__c = system.today();
		promotion2.From_Date__c = system.today().addDays(60);
		insert promotion2;
		
		sdr = new Start_Date_Range__c();
		sdr.Promotion__c = promotion2.id;
		sdr.Campus__c = campus.id;
		sdr.From_Date__c = system.today();
		sdr.To_Date__c = system.today().addDays(30);
		sdr.Value__c = 300;
		insert sdr;
		
		TestFactory testFactory = new TestFactory();
		
		Deal__c d2 = TestFactory.createCourseExtraFeePromotion(cc, 'Published Price', p);
		
		d2 = TestFactory.createCourseFreeUnitsPromotion(cc, 'Published Price');
		
		Apexpages.Standardcontroller controller = new ApexPages.Standardcontroller(campus);
		promotions_maintenance pm = new promotions_maintenance(controller);
		
		pm.cloneOption = '3.0';
		pm.selectedNationalities = new List<String>{'Latin America', 'Published Price'};
		
		
		List<Deal__c> listDeal = pm.listPromotions;
		
		
		
		Start_Date_Range__c newExtraFeeRange = pm.newExtraFeeRange;
		pm.addStartDateRange();
		
		for(Deal__c theDeal : pm.listPromotions){
			theDeal.isSelected__c = true;
		}
		
		
		pm.newExtraFeeRange.From_Date__c = null;
		pm.addStartDateRange();
		
		pm.newExtraFeeRange.From_Date__c = System.today();
		pm.newExtraFeeRange.To_Date__c = null;
		pm.addStartDateRange();
		
		pm.newExtraFeeRange.To_Date__c = system.today().addDays(-5);
		pm.cloneOption = '2.0';
		pm.newExtraFeeRange.Number_of_Free_Weeks__c = null;
		pm.addStartDateRange();
	
		pm.newExtraFeeRange.Number_of_Free_Weeks__c = 5;
		pm.addStartDateRange();
		
	
		pm.cloneOption = '3.0';
		pm.newExtraFeeRange.Value__c = null;
		pm.addStartDateRange();
	
	
		pm.cloneOption = '2.0';
		pm.newExtraFeeRange.Number_of_Free_Weeks__c = 5;
		pm.newExtraFeeRange.To_Date__c = system.today().addDays(30);
		pm.addStartDateRange();
		
		
		
		pm.cloneOption = '3.0';
		pm.getCoursesDeals();
		
		pm.refreshFees();
		
		
		
		pm.savePromotionsUpdate();
		
		
		pm.promoFeesOption = '2';
		pm.showGrouped = true;
		pm.getgroupedCoursePromotion();
		
		pm.savePromotionsUpdate();
		
		pm.promoFeesOption = '1';
		pm.showGrouped = false;
		pm.savePromotionsUpdate();
		
		
		
		pm.showGrouped = true;
		pm.getgroupedCampusPromotion();
		
		pm.savePromotionsUpdate();
		
		
		
		
		pm.cloneOption = '3.0';
		pm.selectedNationalities = new List<String>{'Latin America', 'Published Price'};
		pm.promoFeesOption = '2';
		pm.selectedCourseRelatedPromotion = cefd.Product__c;
		
		
		
		
		
		
		
		
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}