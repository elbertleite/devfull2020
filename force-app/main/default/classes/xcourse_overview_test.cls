/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class xcourse_overview_test {

    static testMethod void myUnitTest() {
        
        TestFactory testFactory = new TestFactory();
        
        Account agency = TestFactory.createAgency();
        
        Currency_rate__c cr = new Currency_rate__c();
        cr.Agency__c = agency.id;
        cr.CurrencyCode__c = 'CAD';
        cr.Value__c = 1.5; 
        cr.High_Value__c = 1.8;
        insert cr;
        
        Contact employee = TestFactory.createEmployee(agency);
        
        Account school = TestFactory.createSchool();
        
        Product__c p = new Product__c();
        p.Name__c = 'Enrolment Fee';
        p.Product_Type__c = 'Other';
        p.Supplier__c = school.id;
        insert p;
        
        
        Product__c p2 = new Product__c();
        p2.Name__c = 'Elberts Homestay';
        p2.Product_Type__c = 'Accommodation';
        p2.Supplier__c = school.id;
        insert p2;
        
        Product__c p3 = new Product__c();
        p3.Name__c = 'Accommodation Placement Fee';
        p3.Product_Type__c = 'Accommodation';
        p3.Supplier__c = school.id;
        insert p3;
        
        
        Account campus = TestFactory.createCampus(school, agency);
            
        Course__c course = TestFactory.createCourse();
        
        Course__c course2 = TestFactory.createLanguageCourse();
                
        Campus_Course__c cc = TestFactory.createCampusCourse(campus, course);
        
        Campus_Course__c cc2 = TestFactory.createCampusCourse(campus, course2);
       
        Course_Price__c cp = TestFactory.createCoursePrice(cc, 'Latin America');
        
        Course_Price__c cp2 = TestFactory.createCoursePrice(cc2, 'Published Price');        
        
        Course_Intake_Date__c cid = TestFactory.createCourseIntakeDate(cc);
        
        Course_Intake_Date__c cid2 = TestFactory.createCourseIntakeDate(cc2);
        
        Course_Extra_Fee__c cef = TestFactory.createCourseExtraFee(cc, 'Latin America', p);
        
        Course_Extra_Fee__c cef2 = TestFactory.createCourseExtraFee(cc2, 'Published Price', p);
        
        Course_Extra_Fee_Dependent__c cefd = TestFactory.createCourseRelatedExtraFee(cef, p2);
        
        Course_Extra_Fee_Dependent__c cefd2 = TestFactory.createCourseRelatedExtraFee(cef2, p2);
        
        Course_Extra_Fee__c combinedfee = TestFactory.createCourseExtraFeeCombined(cc, 'Latin America', p, p2);
        
        Course_Extra_Fee__c opt1 = new Course_Extra_Fee__c();
        opt1.Campus_Course__c = cc.id;
        opt1.Availability__c = 3.0;
        opt1.From__c = 1;
        opt1.Value__c = 150;        
        opt1.Optional__c = true;
        opt1.Product__c = p2.id;
        opt1.date_paid_from__c = system.today();
        opt1.date_paid_to__c = system.today().addDays(+31);
        opt1.Nationality__c = 'Latin America';      
        insert opt1;
        
        Start_Date_Range__c sdrOpt1 = new Start_Date_Range__c();
        sdrOpt1.Course_Extra_Fee__c = opt1.id;
        sdrOpt1.From_Date__c = system.today();
        sdrOpt1.To_Date__c =  system.today().addDays(+31);
        sdrOpt1.Value__c = 300;
        sdrOpt1.Campus__c = cc.Campus__c;
        insert sdrOpt1;
        
        Course_Extra_Fee_Dependent__c dep = new Course_Extra_Fee_Dependent__c();
        dep.Course_Extra_Fee__c = opt1.id;
        dep.Date_Paid_From__c = system.today();
        dep.Date_Paid_To__c = system.today().addDays(+31);
        dep.Value__c = 200;
        dep.Product__c = p3.id;
        dep.Optional__c = true;
        insert dep;
        
        Start_Date_Range__c sdrDep = new Start_Date_Range__c();
        sdrDep.Course_Extra_Fee_Dependent__c = dep.id;
        sdrDep.Campus__c = cc.Campus__c;
        sdrDep.From_Date__c = system.today();
        sdrDep.To_Date__c =  system.today().addDays(+31);
        sdrDep.Value__c = 300;
        insert sdrDep;
        
        
        
        Course_Extra_Fee__c cef3 = new Course_Extra_Fee__c();
        cef3.Campus_Course__c = cc.Id;
        cef3.Nationality__c = 'Published Price';
        cef3.Availability__c = 3;
        cef3.From__c = 1;
        cef3.Value__c = 100;        
        cef3.Optional__c = false;
        cef3.Product__c = p.id;
        cef3.Allow_change_units__c = true;
        cef3.ExtraFeeInterval__c = 'Week';
        cef3.date_paid_from__c = system.today();
        cef3.date_paid_to__c = system.today().addDays(+31);
        insert cef3;
        
        
        
        Deal__c d = new Deal__c();
        d.Availability__c = 3;
        d.Campus_Account__c = campus.id;
        d.From__c = 1;
        d.Extra_Fee__c = cef2.id;
        d.Promotion_Type__c = 3;
        d.From_Date__c = system.today();
        d.To_Date__c = system.today().addDays(60);
        d.Extra_Fee_Type__c = 'Cash';
        d.Extra_Fee_Value__c = 60;
        d.Product__c = p.id;
        d.Nationality_Group__c = 'Publish Price';
        insert d;
        
        
        Deal__c d2 = new Deal__c();
        d2.Availability__c = 3;
        d2.Campus_Course__c = cc2.id;
        d2.From__c = 1;     
        d2.Promotion_Type__c = 2;
        d2.From_Date__c = system.today();
        d2.To_Date__c = system.today().addDays(60);
        d2.Promotion_Weeks__c = 1;
        d2.Number_of_Free_Weeks__c = 1;
        d2.Nationality_Group__c = 'Latin America';
        d2.Promotion_Name__c = '1+1';
        d2.Promo_Price__c = 50;     
        insert d2;
        
        Start_Date_Range__c sdr = new Start_Date_Range__c();
        sdr.Promotion__c = d2.id;
        sdr.From_Date__c = system.today();
        sdr.To_Date__c = system.today().addDays(15);
        sdr.Number_of_Free_Weeks__c = 2;
        sdr.Campus__c = cc2.Campus__c;
        insert sdr;     
        
        
        
        Deal__c d3 = new Deal__c();
        d3.Availability__c = 3;
        d3.Campus_Course__c = cc.id;
        d3.From__c = 1;
        d3.Extra_Fee__c = cef.id;
        d3.Promotion_Type__c = 3;
        d3.From_Date__c = system.today();
        d3.To_Date__c = system.today().addDays(60);
        d3.Extra_Fee_Type__c = 'Cash';
        d3.Extra_Fee_Value__c = 50;
        d3.Nationality_Group__c = 'Latin America';
        insert d3;
        
        Deal__c d4 = new Deal__c();
        d4.Availability__c = 3;
        d4.Campus_Course__c = cc.id;
        d4.From__c = 1;     
        d4.Promotion_Type__c = 2;
        d4.Promotion_Name__c = '5+2';
        d4.Promo_Price__c = 50;     
        d4.From_Date__c = system.today();
        d4.To_Date__c = system.today().addDays(61);
        d4.Promotion_Weeks__c = 5;
        d4.Number_of_Free_Weeks__c = 2;
        d4.Nationality_Group__c = 'Published Price';
        insert d4;
        
        Web_Search__c ws = new Web_Search__c();
        insert ws;
        
        Test.startTest();
        
        ApexPAges.currentPage().getParameters().put('id', ws.id);
        ApexPAges.currentPage().getParameters().put('ccid', cc.id);
        ApexPAges.currentPage().getParameters().put('startAfter', system.today().format());
        ApexPAges.currentPage().getParameters().put('nation', 'Brazil');
        ApexPAges.currentPage().getParameters().put('avail','3.0');
        ApexPAges.currentPage().getParameters().put('numUnits','10');
        ApexPAges.currentPage().getParameters().put('selSchool', campus.id);
        ApexPAges.currentPage().getParameters().put('countryDest', 'Australia');
        
        ApexPages.Standardcontroller controller = new Apexpages.Standardcontroller(cc);
        xcourse_overview xc = new xcourse_overview(controller);
        
        Campus_Course__c campusDetails = xc.campusDetails;
        
        xc.getCampusCourse();
        xc.getCoursePrices();
        xc.getGroupedRequiredExtraFees();
        xc.getStartDates();
        xc.getGroupedOptionalExtraFees();
        xc.getRelatedFees();
        xc.getCoursePromotions();
        xc.getpromotionStartDates();
        Boolean isGroupedFreeWeeksPromotionsEmpty = xc.isGroupedFreeWeeksPromotionsEmpty;
        isGroupedFreeWeeksPromotionsEmpty = xc.isGroupedFreeWeeksPromotionsEmpty;
        
        Boolean isGroupedExtraFeePromotionsEmpty = xc.isGroupedExtraFeePromotionsEmpty;
        isGroupedExtraFeePromotionsEmpty = xc.isGroupedExtraFeePromotionsEmpty;
        
        List<String> importantInfoAsList = xc.importantInfoAsList;
        List<String> academicReqAsList = xc.academicReqAsList;
        List<String> extraReqAsList = xc.extraReqAsList;
        List<String> whyAsList = xc.whyAsList;
        List<String> tipsAsList = xc.tipsAsList;
        //List<String> formPaymentAsList = xc.formPaymentAsList;
        string SavedDatesStringList = xc.SavedDatesStringList;
        
        
       
        
        
       
        
        Test.stopTest();
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }
}