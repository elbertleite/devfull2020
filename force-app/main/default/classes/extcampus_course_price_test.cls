/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData = true)
private class extcampus_course_price_test {

    static testMethod void myUnitTest() {
    	
    	Map<String,String> recordTypes = new Map<String,String>();
       
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
        
		TestFactory tf = new TestFactory();
		
		Account agency = tf.createAgency();
		
		Contact userDetails = tf.createEmployee(agency);
        
		Account school = tf.createSchool();
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;
		insert p3;
		
		
		Nationality_Group__c ng = new Nationality_Group__c();
		ng.Account__c = school.id;
		ng.Country__c = 'Brazil';
		ng.Name = 'Latin America';
		insert ng;
       
		Account campus = tf.createCampus(school, agency);
		
		Account_Document_File__c folder = new Account_Document_File__c();
		folder.File_Name__c = 'The Folder';
		folder.Parent__c = campus.id;
		folder.Content_Type__c = 'Folder';
		insert folder;
		
		Account_Document_File__c adf = new Account_Document_File__c();
		adf.File_Name__c = 'Campus File';
		adf.Parent__c = campus.id;
		adf.Parent_Folder_Id__c = folder.id;
		insert adf;
		
		
		
		Account campus2 = new Account();
		campus2.RecordTypeId = recordTypes.get('Campus');
		campus2.Name = 'Test Campus Brisbane';
		campus2.BillingCountry = 'Australia';
		campus2.BillingCity = 'Brisbane';
		campus2.ParentId = school.id;
		insert campus2;
		
		Course__c course = tf.createCourse();
		
		Course__c course2 = new Course__c();
		course2.Name = 'Certificate I in Business';        
		course2.Type__c = 'Business';
		course2.Sub_Type__c = 'Business';
		course2.Course_Qualification__c = 'Certificate III';
		course2.Course_Type__c = 'VET';
		course2.Course_Unit_Type__c = 'Week';
		course2.Only_Sold_in_Blocks__c = false;
		course2.Period__c = 'Morning';
		course2.School__c = school.id;
		insert course2;
       
		Campus_Course__c cc = tf.createCampusCourse(campus, course);
		
		Campus_Course__c cc2 = new Campus_Course__c();
		cc2.Course__c = course2.Id;
		cc2.Campus__c = campus.Id;        
		cc2.Is_Available__c = true;
		cc2.is_Selected__c = true;      
		insert cc2;
		
		
		Course_Price__c cp = tf.createCoursePrice(cc, 'Published Price');
		
		Course_Price__c cp2 = new Course_Price__c();
		cp2.Nationality__c = 'Latin America';
		cp2.From__c = 5;
		cp2.Price_per_week__c = 150;
		cp2.Availability__c = 3.0;
		cp2.Campus_Course__c = cc2.Id;
		cp2.Price_valid_from__c = system.today();
		cp2.Price_valid_until__c = system.today().addYears(1);       
		insert cp2;
		
		Course_Price__c cp3 = tf.createCoursePrice(cc, 'Latin America');
		
		Course_Price__c cp4 = tf.createCoursePrice(cc2, 'Published Price');		
		
		
		Test.startTest();
		
		Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(campus);
		extcampus_course_price eccp = new extcampus_course_price(controller);
		eccp.setCountry('ALL');
		
		ApexPages.currentPage().getParameters().put('paramDt', system.today().addDays(-365).format());
		ApexPages.currentPage().getParameters().put('paramDtf', system.today().addDays(365).format());
		ApexPages.currentPage().getParameters().put('cId', course.id);
		ID courseid = eccp.courseID;
		
		//eccp.courseIDClone = course2.Id;
		eccp.courseClone = new List<String>{course.Id};
		eccp.selectCourse();
		ID courseClone = eccp.courseIDClone;
		
		eccp.selectCourse();
		//eccp.selectCourseClone();
		//eccp.itemsToClone();
		eccp.cloneCourse();
		eccp.cloneCourse();//force exception
		eccp.cancel();
		String country = eccp.getCountry();
		eccp.setCourse(course.Name);
		String str = eccp.getCourse();
		
		List<SelectOption> getNationalityGroup = eccp.getNationalityGroup();
		List<SelectOption> getCourseList = eccp.getCourseList();
		
		eccp.cloneOption = 'clone';
		eccp.selectedCloneCampus = campus.Id;
		List<SelectOption> getCourseListClone = eccp.getCourseListClone();
		List<SelectOption> getPriceAvailabilityList = eccp.getPriceAvailabilityList();
		
		List<SelectOption> files = eccp.Files;
		//Apexpages.currentPage().getParameters().put('fileID', adf.id);
		//eccp.viewFile();
		
		List<SelectOption> getCoursesCampus = eccp.getCoursesCampus();
		eccp.setCourse(course.id);
		eccp.getCourse();
		
		
		for(Campus_Course__c thecc : eccp.lst)
			for (Course_Price__c l : thecc.Course_Prices__r)	
				l.isToCopy__c = true;
		eccp.itemsToClone();
		
		eccp.newCoursePrice.Campus_Course__c = cc.Id;
		eccp.newCoursePrice.Price_per_week__c = 150;
		eccp.newCoursePrice.From__c = 100;
		eccp.newCoursePrice.Price_valid_from__c = null;
		eccp.newCoursePrice.Price_valid_until__c = null;
		eccp.newCoursePrice.Nationality__c = 'Brazil';
		eccp.addCoursePrice();
		eccp.newCoursePrice.Campus_Course__c = cc.Id;
		eccp.newCoursePrice.Price_per_week__c = 150;
		eccp.newCoursePrice.From__c = 100;
		eccp.newCoursePrice.Nationality__c = 'Brazil';
		eccp.addCoursePrice();
		

		eccp.saveCourseTips();
		eccp.getlstNRDates();
		eccp.savelstNRDates();
		eccp.cancellstNRDates();
		
		eccp.retrieveCampusCoursesByMonth();
		eccp.retrieveCampusCoursesByYear();
		eccp.getNationalitiesInserted();
		eccp.delidPrice = cp.id;
		eccp.delPrice();
		eccp.setEditMode();
		eccp.UpdateRecords();
		eccp.listCountry();
		Course__c dummy = eccp.courseTIPS;
		User userDetailz = eccp.userDetails;
		
		
		
		
		eccp.courseClone = new List<String>{course.id, course2.id};
		eccp.cloneOption = '';
		eccp.selectCourseAvailable();
		eccp.selectMaintenanceOption();
		eccp.getnationalityGroupHint();
		eccp.getPeriods();
		eccp.getListNewTimeTable();
		eccp.filterDates();
		eccp.getgroupedCoursesPrice();
		
		for(Campus_Course__c thecc : eccp.lst)
			for (Course_Price__c l : thecc.Course_Prices__r)	
				l.isToCopy__c = true;
		
			
		//eccp.getgroupedCoursesPrice();
		eccp.itemsToClone();	
		eccp.cloneOption = 'timetable';
		eccp.saveCourseInfo();
		eccp.cloneOption = '';
		eccp.saveCourseInfo();
		
		eccp.saveCoursePrice();
		
		eccp.deleteCoursePrice();
		
		eccp.getCloneOptions();
		
		eccp.getErrorMessage();
		eccp.selectedNationalities = new List<String>{'Brazil'};
		//eccp.getgroupedCoursesPrice();
		eccp.addMultipleCoursePrice();
		
		eccp.getNationalitiesEdit();
		
		eccp.getselectedCourseTypeOptions();
		eccp.getCourseAvailableList();
		eccp.listCourses();
		eccp.getlstNRDates();
		eccp.applyDates();
		
		
		eccp.filterDates();
		eccp.refreshPrices();
		
    }
    
    
    
    
    
    
    
    
    
     static testMethod void myUnitTest2() {
        Map<String,String> recordTypes = new Map<String,String>();
       
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
		
		TestFactory tf = new TestFactory();
		
		Account agency = tf.createAgency();
		
		Contact userDetails = tf.createEmployee(agency);
        
		Account school = tf.createSchool();
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;
		insert p3;
		
		
		Nationality_Group__c ng = new Nationality_Group__c();
		ng.Account__c = school.id;
		ng.Country__c = 'Brazil';
		ng.Name = 'Latin America';
		insert ng;
       
		Account campus = new Account();
		campus.RecordTypeId = recordTypes.get('Campus');
		campus.Name = 'Test Campus CBD';
		campus.BillingCountry = 'Australia';
		campus.BillingCity = 'Sydney';
		campus.ParentId = school.id;
		insert campus;
		
		Account_Document_File__c folder = new Account_Document_File__c();
		folder.File_Name__c = 'The Folder';
		folder.Parent__c = campus.id;
		folder.Content_Type__c = 'Folder';
		insert folder;
		
		Account_Document_File__c adf = new Account_Document_File__c();
		adf.File_Name__c = 'Campus File';
		adf.Parent__c = campus.id;
		adf.Parent_Folder_Id__c = folder.id;
		insert adf;
		
		
		
		Account campus2 = new Account();
		campus2.RecordTypeId = recordTypes.get('Campus');
		campus2.Name = 'Test Campus Brisbane';
		campus2.BillingCountry = 'Australia';
		campus2.BillingCity = 'Brisbane';
		campus2.ParentId = school.id;
		insert campus2;
		
		
		Course__c course = new Course__c();
		course.Name = 'Certificate III in Business';        
		course.Type__c = 'Business';
		course.Sub_Type__c = 'Business';
		course.Course_Qualification__c = 'Certificate III';
		course.Course_Type__c = 'VET';
		course.Course_Unit_Type__c = 'Week';
		course.Only_Sold_in_Blocks__c = true;
		course.Period__c = 'Morning';
		insert course;
		
		Course__c course2 = new Course__c();
		course2.Name = 'Certificate I in Business';        
		course2.Type__c = 'Business';
		course2.Sub_Type__c = 'Business';
		course2.Course_Qualification__c = 'Certificate III';
		course2.Course_Type__c = 'VET';
		course2.Course_Unit_Type__c = 'Week';
		course2.Only_Sold_in_Blocks__c = false;
		course2.Period__c = 'Morning';
		insert course2;
       
		Campus_Course__c cc = new Campus_Course__c();
		cc.Course__c = course.Id;
		cc.Campus__c = campus.Id;        
		cc.Is_Available__c = true;
		cc.is_Selected__c = true;      
		insert cc;
		
		Campus_Course__c cc2 = new Campus_Course__c();
		cc2.Course__c = course2.Id;
		cc2.Campus__c = campus.Id;        
		cc2.Is_Available__c = true;
		cc2.is_Selected__c = true;      
		insert cc2;
		
		
		Course_Price__c cp = new Course_Price__c();
		
		cp.Nationality__c = 'Published Price';
		cp.From__c = 1;
		cp.Price_per_week__c = 150;
		cp.Availability__c = 3.0;
		cp.Campus_Course__c = cc.Id;
		cp.Price_valid_from__c = system.today();
		cp.Price_valid_until__c = system.today().addYears(1);        
		insert cp;
		
		Course_Price__c cp2 = new Course_Price__c();
		
		cp2.Nationality__c = 'Latin America';
		cp2.From__c = 5;
		cp2.Price_per_week__c = 150;
		cp2.Availability__c = 3.0;
		cp2.Campus_Course__c = cc2.Id;
		cp2.Price_valid_from__c = system.today();
		cp2.Price_valid_until__c = system.today().addYears(1);
		insert cp2;
		
		Course_Price__c cp3 = tf.createCoursePrice(cc, 'Latin America');
		
		Course_Price__c cp4 = tf.createCoursePrice(cc2, 'Published Price');
		
		Test.startTest();
		
		Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(campus);
		extcampus_course_price eccp = new extcampus_course_price(controller);
		
		String cloneOption = eccp.cloneOption;
		eccp.getCloneOptions();
		
		
		List<String> lstr = eccp.selectedCourseType;
		for(Selectoption so : eccp.getselectedCourseTypeOptions())
			eccp.selectedCourseType.add(so.getValue());	
		
		eccp.selectCourseClone();
		
		lstr = eccp.selectedNationalities;
		for(Selectoption so : eccp.getNationalitiesEdit())
			eccp.selectedNationalities.add(so.getvalue());
			
		
		eccp.listCountry();
		
		eccp.filterDates();
		eccp.getgroupedCoursesPrice();
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		eccp.cloneOption = 'new';
		eccp.selectMaintenanceOption();
		
		eccp.selectCourseClone();
		
		
		
		eccp.getCourseAvailableList();
		eccp.CourseAvailable = true;
		eccp.selectCourseAvailable();
		
		eccp.getPeriods();
		eccp.period = new List<String>{'Morning'};	
		
		/*for(Selectoption so : eccp.getCourseListClone())
			eccp.courseClone.add(so.getValue());
		eccp.selectCourseClone();*/
		
		eccp.getCoursePricesRange();
		eccp.cloneOption = 'newRange';
		//eccp.selectCourseClone();
		
		eccp.getPriceAvailabilityList();
		eccp.newCoursePrice.Availability__c = 3;
		eccp.newCoursePrice.Price_valid_from__c = system.today().addDays(-30);
		eccp.newCoursePrice.Price_valid_until__c = system.today().addDays(360);
		eccp.newCoursePrice.From__c = 1;
		eccp.newCoursePrice.Price_per_week__c = 200;
		eccp.newCoursePrice.unit_description__c = 'week';
		eccp.newCoursePrice.Account_Document_File__c = adf.id;
		eccp.addMultipleCoursePrice();
		
		eccp.showGrouped = true;
		
		List<extcampus_course_price.priceDetails> pdet =  eccp.lstClone;
		
		eccp.cloneOption = 'edit';
		
		eccp.setEditMode();
		eccp.saveCourseInfo();
		//eccp.cancel();
		
		
		
		
		
		
		
		Test.stopTest();
		
     }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}