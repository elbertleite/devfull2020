global class API_v1_schoolProductsHandler extends api_manager  {
	
    global static List<SchoolProduct> retrieveProducts(Set<String> campusCourses, String nationality, Date paymentDate, String language){
		
		double location = 3; // waiting definition if prices dynamic or always offshore
		
		ProductService ps = new ProductService();
		
		return ps.getWebsiteAccommodationFees(campusCourses, nationality, paymentDate, location, language);
	}
}