/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class xSearchCourseQuotation_test {

    static testMethod void myUnitTest() {
        
        TestFactory TestFactory = new TestFactory();
		
		Account agency = TestFactory.createAgency();
		
		Contact employee = TestFactory.createEmployee(agency);
		
		Contact client = TestFactory.createClient(agency);
		
		Account school = testFactory.createSchool();
		Account campus = testFactory.createCampus(school, agency);
		Course__c course = testFactory.createCourse();
		Campus_Course__c cc = testFactory.createCampusCourse(campus, course);
		Quotation__c q = TestFactory.createQuotation(client, cc);
		
		ApexPages.currentPage().getParameters().put('Id', q.id);
		XSearchCourseQuotation quote = new XSearchCourseQuotation();
		quote.getQuote();
		quote.getquoteDetails();
		
		quote.getIsCombinedQuote();
		
		
		
		// xCourseSearchPersonDetails.userDetails acc = quote.userDetails;
		// String str = quote.agencyPhone;
		// str = quote.mobile;
		// str = quote.agencyCurrency;
		// str = quote.optionalCurrency;
		
		
		
		
		XSearchCourseQuotation.Agency ag = new XSearchCourseQuotation.Agency(employee.Name, agency.Name, '3453543', employee.Email);
		
		
		
		
		
		
        
    }
}