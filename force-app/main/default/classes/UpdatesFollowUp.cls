public class UpdatesFollowUp{

	public String destination{
		get{
			if(destination == null)
				if(ApexPages.currentPage().getParameters().get('country') != null && ApexPages.currentPage().getParameters().get('country') != '')
					destination = ApexPages.currentPage().getParameters().get('country');
				else destination = 'selected'; 
			return destination;
		} 
		Set;
	}
	
	public String destinationCity{
		get{
			if(destinationCity == null)
				if(ApexPages.currentPage().getParameters().get('city') != null && ApexPages.currentPage().getParameters().get('city') != '' )
					destinationCity = ApexPages.currentPage().getParameters().get('city');
				else destinationCity = 'selected'; 
			return destinationCity;
		} 
		Set;
	}
	
	public String selectedSchool{
		get{
			if(selectedSchool == null)
				if(ApexPages.currentPage().getParameters().get('school') != null)
					selectedSchool = ApexPages.currentPage().getParameters().get('school');
				else selectedSchool = 'selected'; 
			return selectedSchool;
		} 
		Set;
	}
	
	public String selectedCampus{
		get{
			if(selectedCampus == null)
				if(ApexPages.currentPage().getParameters().get('campus') != null)
					selectedCampus = ApexPages.currentPage().getParameters().get('campus');
				else selectedCampus = 'selected'; 
			return selectedCampus;
		} 
		Set;
	}
	
	public list<String> listCampuses{
		get{
			if(listCampuses == null){
				if(ApexPages.currentPage().getParameters().get('listcampus') != null){
					listCampuses = new list<String>();
					String listId = ApexPages.currentPage().getParameters().get('listcampus');
					if(listId != null && listId.trim() != '')
						listCampuses.addAll(listId.trim().split(','));
					System.debug('==>listCampuses: '+listCampuses);
				}
				
			}
			return listCampuses;
		} 
		Set;
	}
	
	//public String destinationCity{get{if(destinationCity == null) destinationCity = ''; return destinationCity;} Set;}
	public map<String,String> mapSelectedSchool{get{if(mapSelectedSchool == null) mapSelectedSchool = new map<String,String>(); return mapSelectedSchool;} Set;}
	
	private List<SelectOption> listDestinations;
	private List<SelectOption> listDestinationsCity;
	
	
	public List<SelectOption> getlistDestinations(){
		if(listDestinations == null){
			listDestinations = new List<SelectOption>();
			listDestinationsCity = new List<SelectOption>();
			CityDestination = new map<String,list<SelectOption>>();
			Set<string> countryDestination = new Set<string>();
			listDestinations.add(new SelectOption('selected','--Select a Country--'));
			for(AggregateResult acco:[Select BillingCountry, BillingCity from Account where recordType.name = 'Campus' and BillingCountry!=null and BillingCity!=null  group by BillingCountry, BillingCity order by BillingCountry, BillingCity])
				if(!countryDestination.contains((String)acco.get('BillingCountry'))){
					if(destination == 'selected')
						destination = (String)acco.get('BillingCountry');
					listDestinations.add(new SelectOption((String)acco.get('BillingCountry'),(String)acco.get('BillingCountry')));
					CityDestination.put((String)acco.get('BillingCountry'),new List<SelectOption>{new SelectOption((String)acco.get('BillingCity'),(String)acco.get('BillingCity'))});
					CityDestination.get((String)acco.get('BillingCountry')).add(0,new SelectOption('selected','--Select a City--'));
					countryDestination.add((String)acco.get('BillingCountry'));
				} else	CityDestination.get((String)acco.get('BillingCountry')).add(new SelectOption((String)acco.get('BillingCity'),(String)acco.get('BillingCity')));
		}
		return listDestinations;
	}	
	
	private map<String,list<SelectOption>> CityDestination;
	public map<String,list<SelectOption>> getCityDestination(){
		if(CityDestination == null){
			CityDestination = new map<String,list<SelectOption>>();
			getlistDestinations();
		}
		return CityDestination;
	}
	
	
	//public String selectedSchool{get{if(selectedSchool == null) selectedSchool = ''; return selectedSchool;} Set;}
	private List<SelectOption> listSchools;
	public List<SelectOption> getlistSchools(){
		if(listSchools == null && destinationCity != null && destinationCity != 'selected'){
			listSchools = new List<SelectOption>();
			listSchools.add(new SelectOption('selected','--Select a School--'));
			for(AggregateResult school:[Select ParentId schoolId, Parent.Name school from Account where recordType.name = 'Campus' and ParentId!=null  and BillingCountry = :destination and BillingCity = :destinationCity group by ParentId, Parent.Name order by Parent.Name]){
				listSchools.add(new SelectOption((String)school.get('schoolId'),(String)school.get('school')));
			}
		}
		return listSchools;
	}
	
	//public String selectedCampus{get{if(selectedCampus == null) selectedCampus = ''; return selectedCampus;} Set;}
	private List<SelectOption> listCampus;
	public List<SelectOption> getlistCampus(){
		if(listCampus == null && selectedSchool != null && selectedSchool != 'selected'){
			listCampus = new List<SelectOption>();
			listCampus.add(new SelectOption('selected','--Select a Campus--'));
			for(Account campus:[Select id, Name from Account where parentid = :selectedSchool and billingCountry = :destination and billingCity = :destinationCity and recordType.Name = 'Campus' order by Name]){
				listCampus.add(new SelectOption(campus.id, campus.name));
				mapSelectedSchool.put(campus.id, campus.name);
			}
		}
		return listCampus;
	}
	
	public void refreshScools(){
		listSchools = null;
		listCampus = null;
	}
	
	public void refreshCampus(){
		listCampus = null;
	}
	
	public Update_Follow_Up__c newUpdate{get{if(newUpdate == null) newUpdate = new Update_Follow_Up__c(); return newUpdate;} Set;}
	
	public pageReference addUpdateCourse(){
		if(destination == 'selected' || destinationCity == 'selected' || selectedSchool == 'selected' || selectedCampus == 'selected' || newUpdate.Comments__c == null || newUpdate.Comments__c.trim().equals('') ){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'All fields are required when adding one update.');
			ApexPages.addMessage(myMsg);
			return null;
		}
		newUpdate.Country__c = destination;
		newUpdate.city__c = destinationCity;
		newUpdate.school__c = selectedSchool;
		newUpdate.campus__c = selectedCampus;
		insert newUpdate;
		newUpdate = new Update_Follow_Up__c();
		search();
		return null;
	}
	
	public void search(){
		listUpdates = null;
	}
	
	public Integer limitBy {
		get {
			return 200;
		}
		private set;
	}
	
	private list<Update_Follow_Up__c> listUpdates;
	public list<Update_Follow_Up__c> getlistUpdates(){
		
		if(listUpdates == null){
			String sql = 'Select U.Campus__r.Name, U.Comments__c, U.City__c, U.Country__c, U.School__r.Name, LastModifiedDate, LastModifiedBy.Name, CreatedById from Update_Follow_Up__c U where ';
			if(listCampuses != null && listCampuses.size() > 0){
				sql += ' Campus__c in :listCampuses ';
			}else{
				if(destination != '' && destination != 'selected')
					sql += ' Country__c = \''+ String.escapeSingleQuotes(destination) +'\'';
				else return null;
				if(destinationCity != '' && destinationCity != 'selected')
					sql += ' and City__c = \''+ String.escapeSingleQuotes(destinationCity) +'\'';
				if(selectedSchool != '' && selectedSchool != 'selected')
					sql += ' and School__c = \''+ String.escapeSingleQuotes(selectedSchool) +'\'';
				if(selectedCampus != '' && selectedCampus != 'selected')
					sql += ' and Campus__c = \''+ String.escapeSingleQuotes(selectedCampus) +'\'';
			}
			sql += ' order by LastModifiedDate desc LIMIT :limitBy';
			System.debug('==>sql: '+sql);
			listUpdates = Database.query(sql);
			listCampuses = new list<String>();
		}
		
		return listUpdates;
	}
	
	public string updateId{get{if(updateId == null) updateId = ''; return updateId;} Set;}
	public pageReference updateCourse(){
		updateId = ApexPages.currentPage().getParameters().get('updateId');
		return null;
	}
	
	public pagereference deleteFollowUp(){
		String deleteID = ApexPages.currentPage().getParameters().get('deleteID');
		if(deleteID != null)
			delete [Select id from Update_Follow_Up__c where id = :deleteID];
			
		search();
		return null;
	}
	
	public pageReference saveUpdate(){
		for(Update_Follow_Up__c lfu:listUpdates)
			if(updateId == lfu.id){
				update lfu;
				updateId = '';
				break;
			}
		return null;
	}
	
}