/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class client_course_fees_install_test {

    static testMethod void myUnitTest() {
       
       TestFactory tf = new TestFactory();
       
       Account school = tf.createSchool();
       Account agency = tf.createAgency();
       Contact emp = tf.createEmployee(agency);
       User portalUser = tf.createPortalUser(emp);
       Account campus = tf.createCampus(school, agency);
       Course__c course = tf.createCourse();
       Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
       
   	   Contact client = tf.createLead(agency, emp);
       client_course__c booking = tf.createBooking(client);
       client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
       
       List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
       
       client_course_fees_install testClass = new client_course_fees_install(new ApexPages.StandardController(instalments[0]));
       
       testClass.getNumberOfUnits();
       testClass.getUnitTypes();
       client_course_fees__c newFee = testClass.newFee;
       
       client_Course_fees__c ccf = new client_Course_fees__c();
	   ccf.client_course__c = cc.id;
	   ccf.Commission_Value__c = 10;
	   ccf.isPromotion__c = false;
	   ccf.total_value__c = 150;
	   ccf.value__c = 15;
	   ccf.Extra_Fee_Discount_Value__c = 0;
	   ccf.fee_type__c = 'Other';
	   ccf.number_units__c = 10;
	   ccf.fee_name__c = '2016 Enrolment Fee2';
	   ccf.Original_Value__c = 10;
	   ccf.Original_Number_Units__c = 1;
	   ccf.Original_Total_Value__c = 140;
	   
	   insert ccf;
       
       testClass.listFeesToAdd = new list<client_course_fees__c>{ccf};
       
       testClass.saveFees();
       testClass.addNewFee();
       testClass.getExtraFeeNames();
    }
}