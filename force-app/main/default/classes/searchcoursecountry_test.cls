@isTest
private class searchcoursecountry_test {

    static testMethod void myUnitTest() {
    	
    	TestFactory tf = new TestFactory();
       
		Account agency = tf.createAgency();
		
		Contact employee = tf.createEmployee(agency);
		
		Account school = tf.createSchool();
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;
		insert p3;
		
		
		Account campus = tf.createCampus(school, agency);
		    
		Course__c course = tf.createCourse();
		
		Course__c course2 = tf.createLanguageCourse();
				
		Campus_Course__c cc = tf.createCampusCourse(campus, course);
		
		Campus_Course__c cc2 = tf.createCampusCourse(campus, course2);
       
		Course_Price__c cp = tf.createCoursePrice(cc, 'Latin America');
		
		Course_Price__c cp2 = tf.createCoursePrice(cc2, 'Published Price');
		
		
		Course_Intake_Date__c cid = tf.createCourseIntakeDate(cc);
		
		Course_Intake_Date__c cid2 = tf.createCourseIntakeDate(cc2);
        
		Course_Extra_Fee__c cef = tf.createCourseExtraFee(cc, 'Latin America', p);
		
		Course_Extra_Fee_Dependent__c cefd = tf.createCourseRelatedExtraFee(cef, p2);
		
		tf.createCourseExtraFeeCombined(cc, 'Latin America', p, p2);
        
		Course_Extra_Fee__c cef2 = new Course_Extra_Fee__c();
		cef2.Campus__c = campus.Id;
		cef2.Nationality__c = 'Published Price';
		cef2.Availability__c = 3;
		cef2.From__c = 2;
		cef2.Value__c = 100;		
		cef2.Optional__c = false;
		cef2.Product__c = p.id;
		cef2.date_paid_from__c = system.today();
		cef2.date_paid_to__c = system.today().addDays(+31);
		insert cef2;
		
		
		
		
		
		
		Deal__c d = new Deal__c();
		d.Availability__c = 3;
		d.Campus_Account__c = campus.id;
		d.From__c = 1;
		d.Extra_Fee__c = cef2.id;
		d.Promotion_Type__c = 3;
		d.From_Date__c = system.today();
		d.To_Date__c = system.today().addDays(60);
		d.Extra_Fee_Type__c = 'Cash';
		d.Extra_Fee_Value__c = 60;
		d.Product__c = p.id;
		d.Nationality_Group__c = 'Publish Price';
		insert d;
		
		
		Deal__c d2 = new Deal__c();
		d2.Availability__c = 3;
		d2.Campus_Course__c = cc2.id;
		d2.From__c = 1;		
		d2.Promotion_Type__c = 2;
		d2.From_Date__c = system.today();
		d2.To_Date__c = system.today().addDays(60);
		d2.Promotion_Weeks__c = 1;
		d2.Number_of_Free_Weeks__c = 1;
		d2.Nationality_Group__c = 'Latin America';
		d2.Promotion_Name__c = '1+1';
		d2.Promo_Price__c = 50;		
		insert d2;
		
		Start_Date_Range__c sdr = new Start_Date_Range__c();
		sdr.Promotion__c = d2.id;
		sdr.Campus__c = cc2.campus__c;
		sdr.From_Date__c = system.today();
		sdr.To_Date__c = system.today().addDays(15);
		sdr.Number_of_Free_Weeks__c = 2;
		insert sdr;		
		
		
		
		Deal__c d3 = new Deal__c();
		d3.Availability__c = 3;
		d3.Campus_Course__c = cc.id;
		d3.From__c = 1;
		d3.Extra_Fee__c = cef.id;
		d3.Promotion_Type__c = 3;
		d3.From_Date__c = system.today();
		d3.To_Date__c = system.today().addDays(60);
		d3.Extra_Fee_Type__c = 'Cash';
		d3.Extra_Fee_Value__c = 50;
		d3.Nationality_Group__c = 'Latin America';
		insert d3;
		
		Deal__c d4 = new Deal__c();
		d4.Availability__c = 3;
		d4.Campus_Course__c = cc.id;
		d4.From__c = 1;		
		d4.Promotion_Type__c = 2;
		d4.Promotion_Name__c = '5+2';
		d4.Promo_Price__c = 50;		
		d4.From_Date__c = system.today();
		d4.To_Date__c = system.today().addDays(61);
		d4.Promotion_Weeks__c = 5;
		d4.Number_of_Free_Weeks__c = 2;
		d4.Nationality_Group__c = 'Published Price';
		insert d4;
		
		
		
		Deal__c d5 = new Deal__c();
		d5.Availability__c = 3;
		d5.Campus_Course__c = cc.id;
		d5.Extra_Fee_Dependent__c = cefd.id;
		d5.From__c = 1;		
		d5.Promotion_Type__c = 3;
		d5.From_Date__c = system.today();
		d5.To_Date__c = system.today().addDays(62);
		d3.Extra_Fee_Type__c = 'Cash';
		d3.Extra_Fee_Value__c = 50;
		d5.Nationality_Group__c = 'Latin America';
		d5.Product__c = p3.id;
		//insert d5;

		Contact client = tf.createClient(agency);

		Web_Search__c ws = new Web_Search__c();
		ws.Location__c = 3.0;
		ws.Nationality__c = 'Brazil';
		ws.Client__c = client.id;
		insert ws;

		Search_Courses__c sc1 = new Search_Courses__c();
		sc1.Campus_Course__c = cc.id;
		sc1.Web_Search__c = ws.id;
		sc1.Number_of_Units__c = 5;
		sc1.Nationality__c = 'Brazil';
		sc1.Custom_Fees__c = 'Fee 1:#123.0:#add:&Discount:#12.0:#subtract:&Fee 2:#1230.0:#add:&Discount2:#112.0:#subtract';
		sc1.Required_Fee_Changeable__c = cef2.id+':5';
		// sc1.Optional_Fee_Ids__c = opt1.id + ':5';
		// sc1.Optional_Fee_Related_Ids__c = opt1.id + ':' + dep.id + ':1';
		// sc1.Selected_Combined_Fees__c = combinedfee.product__r.Name__c + ':' + combinedfee.product__c;
		sc1.Start_Date__c = system.today();
		sc1.Total_Course__c = 1000;
		sc1.Total_Extra_Fees__c = 500;
		sc1.Total_Products__c = 500;
		sc1.Total_Products_Other__c = 0;
		sc1.Total_Tuition__c = 500;
		sc1.Unit_Type__c = 'Week';
		sc1.Value_First_Instalment__c = 500;
		sc1.Payment_Date__c = system.today();
		insert sc1;
		  
        Test.startTest();
        
             
	    searchcoursecountry cs = new searchcoursecountry();
        list<AggregateResult> changeNationalityGroup = searchcoursecountry.changeNationalityGroup();
        cs.searchCoursesJson(new map<string,boolean>{campus.id => true}, new list<string>{campus.id}, 'Brazil', null, new list<string>(), new list<string>(), new list<string>(), new list<string>(), new list<string>(), new list<string>(), new list<string>(), new list<string>{'Morning'}, System.today().format(), 10, null, null, null, null, null, '', 500, new list<string>(), new list<string>(), false, null);


		// cs.selectedSchoolPaymentPrice = 'instal';
        // cs.searchCoursesJson(new map<string,boolean>{campus.id => true}, new list<string>{campus.id}, 'Brazil', null, new list<string>(), new list<string>(), new list<string>(), new list<string>(), new list<string>(), new list<string>(), new list<string>(), new list<string>{'Morning'}, System.today().format(), 10, null, null, null, null, null, '', 500, new list<string>(), new list<string>(), false, null);
		cs.eventIdSet = false;
		cs.Nationalities = 'Published Price';
		cs.createListCourses(new list<string>{cc.id}, 10, new list<string>{campus.id}, new map<String,list<Search_Courses__c>>{cc.id => new list<Search_Courses__c>{sc1}}, null, null, false, null, ws.id, false);
		cs.refreshSearch();
		List<SelectOption> listDestinationsCountry = cs.listDestinationsCountry;
		integer maxCourseLenght = cs.maxCourseLenght;

		map<string, double> coursesOrder = new map<string, double>{cc.id => 1};
		map<String,list<Search_Courses__c>> mapSearchCourses = new map<String,list<Search_Courses__c>>{cc.id => new list<Search_Courses__c>{sc1}};
		list<string> campusCourseId = new list<string>{cc.id};
		list<string> campusId = new list<string>{campus.id};
		List<SelectOption> getlocationOptions = cs.getlocationOptions();
		List<SelectOption> getNationalityGroup = cs.getNationalityGroup();

		cs.addItemCart(campusCourseId ,10,campusId ,coursesOrder , mapSearchCourses, ws.id, 'Brazil', '1', system.today());
		// cs.searchCoursesJson(new map<string,boolean>{campus.id => true}, new list<string>{campus.id}, 'Brazil', null, new list<string>(), new list<string>(), new list<string>(), new list<string>(), new list<string>(), new list<string>(), new list<string>(), new list<string>{'Morning'}, System.today().format(), 10, null, null, null, null, null, '', 500, new list<string>{school.id}, new list<string>{cc.id, cc2.id}, false, null);
		Test.stopTest();
        
    }
    
    
 
    
    
}