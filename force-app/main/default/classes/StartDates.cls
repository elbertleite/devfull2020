public class StartDates {
	
	public String EXTRA_FEE { get {return 'extraFee';} }
	public String RELATED_FEE { get {return 'relatedFee';} }
	public String COURSE_PRICE { get {return 'coursePrice';} }
	public String PROMOTION { get {return 'promotion';} }
	public Decimal PROMOTION_EXTRA_FEE_DISCOUNT { get {return 3;} }
	public Decimal PROMOTION_FREE_UNITS { get {return 2;} }
	public String Type {Get;Set;}
	private String parentID;
	
	
	public Course_Extra_Fee__c extraFee {get;Set;}
	
	public Course_Price__c coursePrice {get; Set;}
	
	public Deal__c deal {get;Set;}
	
	private String campusID;
	
	public StartDates(){
		Type = ApexPages.currentPage().getParameters().get('type');
		parentID = ApexPages.currentPage().getParameters().get('Id');
		
		if(Type != null){
			
			if(Type.equals(EXTRA_FEE)){
			
				extraFee = [Select id, Campus__c, Campus_Course__r.Campus__c, Product__r.Name__c, Nationality__c, From__c, date_paid_from__c, date_paid_to__c, Value__c from Course_Extra_Fee__c where id = :parentID limit 1];
				campusID = extraFee.Campus__c != null ? extraFee.Campus__c : extraFee.Campus_Course__r.Campus__c;
				
			} else if(Type.equals(RELATED_FEE)){
				
				extraFee = [Select id, Campus__c, Campus_Course__r.Campus__c, Product__r.Name__c, From__c, date_paid_from__c, date_paid_to__c, Value__c, Nationality__c, (Select Date_Paid_From__c, Date_Paid_To__c, From__c, Product__c,Product__r.name__c, Selected__c, Value__c from Course_Extra_Fees_Dependent__r order by Product__r.Name__c, Date_Paid_From__c, From__c ) from Course_Extra_Fee__c where id = :parentID limit 1];
				campusID = extraFee.Campus__c != null ? extraFee.Campus__c : extraFee.Campus_Course__r.Campus__c;
				
			} else if(Type.equals(COURSE_PRICE)){
				
				coursePrice = [Select Id, Nationality__c, From__c, Price_per_week__c, Price_valid_from__c, Price_valid_until__c, Campus_Course__r.Campus__c, Campus_Course__r.Course__r.Course_Unit_Type__c from Course_Price__c where id = :parentID limit 1];
				campusID = coursePrice.Campus_Course__r.Campus__c;
				
			} else if(Type.equals(PROMOTION)){
				
				deal = [Select Id, Campus_Course__r.Campus__c, Campus_Account__c, From__c, Promotion_Type__c, Promotion_Name__c, Nationality_Group__c, Promotion_Weeks__c, Number_of_Free_Weeks__c, Product__r.Name__c, From_Date__c, To_Date__c, Extra_Fee_Value__c,
							Extra_Fee_Type__c  
						from Deal__c where Id = :parentID limit 1];
				
				campusID = deal.Campus_Account__c != null ? deal.Campus_Account__c : deal.Campus_Course__r.Campus__c;
				
			}
		}
		
	}
	
	public Start_Date_Range__c newStartDate {
		get {
			if(newStartDate == null){
				newStartDate = new Start_Date_Range__c();
				if(Type.equals(PROMOTION)) 
					newStartDate.Promotion_Name__c = deal.Promotion_Name__c;
			}
				
			return newStartDate;
		}
		set;
	}
	
	public pagereference viewFile(){
		String fileId =  Apexpages.currentPage().getParameters().get('fileId');
		return new pageReference(cg.SDriveTools.getAttachmentURL( campusID, fileId, (100 * 60)));	
	}
	
	
	public pageReference addStartDateRange(){
		
		if(validateFields(new List<Start_Date_Range__c>{newStartDate})){
			
			if(Type.equals(RELATED_FEE))
				return addStartDatesToRelated();
			
			List<Start_Date_Range__c> lrange;
			String sql  = 'select from_date__c, to_date__c, Course_Extra_Fee__c, Course_Price__c, Promotion__c  from Start_Date_Range__c';
			
			if(Type.equals(EXTRA_FEE))
				sql += ' where Course_Extra_Fee__c = \'' +  extraFee.id + '\'';
			else if(Type.equals(COURSE_PRICE))
				sql += ' where Course_Price__c = \'' +  coursePrice.id + '\'';
			else if(Type.equals(PROMOTION))
				sql += ' where Promotion__c = \'' +  deal.id + '\'';
						
			List<Start_Date_Range__c> existingStartDates = Database.query(sql);
						
			String innerErrorMsg = '';
			boolean isValid = true;
			errorUpdateRange = new set<string>();
			
			isValid = true;
			
			for(Start_Date_Range__c mr : existingStartDates){
				if( !( (newStartDate.From_Date__c < mr.To_Date__c && newStartDate.To_Date__c < mr.From_Date__c) || (newStartDate.From_Date__c > mr.To_Date__c && newStartDate.To_Date__c > mr.To_Date__c) ) ) {
					isValid = false;
					innerErrorMsg += '<span style="color:grey"> From Date:</span> '+ mr.From_Date__c.format() +' <span style="color:grey">To Date:</span> '+ mr.To_Date__c.format() + '<br />';
					errorUpdateRange.add(mr.id);
				}
			}
			
			if(isValid){
				if(Type.equals(EXTRA_FEE))
					newStartDate.Course_Extra_Fee__c = extraFee.id;				
				else if(Type.equals(COURSE_PRICE))
					newStartDate.Course_Price__c = coursePrice.id;
				else if(Type.equals(PROMOTION))
					newStartDate.Promotion__c = deal.id;
				
				newStartDate.Campus__c = campusID;				
				insert newStartDate;
				newStartDate = new Start_Date_Range__c();
				listStartDates = null;
			}
			
			if(!errorUpdateRange.isEmpty()){
				String errorMsg = '<span style="color:red;"> The start dates showed below couldn\'t be updated as they have duplicated values.</span> <br /> ';
				errorMsg += innerErrorMsg;
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, errorMsg);
				ApexPages.addMessage(msg);
			}
			
			
		}
		return null;
	}
	
	public set<string> errorUpdateRange{get{if(errorUpdateRange == null) errorUpdateRange = new set<string>(); return errorUpdateRange;} set;}
		
	public pageReference addStartDatesToRelated(){
		
		Boolean relatedFeesSelected = false;
		for(Course_Extra_Fee_Dependent__c dep : extraFee.Course_Extra_Fees_Dependent__r)
			if(dep.Selected__c){ relatedFeesSelected = true; break; }
			
		if (!relatedFeesSelected){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Select at least one extra fee to add the range.');
        	ApexPages.addMessage(myMsg);
			return null;
		} else {
			
			
			Set<Id> relatedFeeIDs = new Set<Id>();
			List<Start_Date_Range__c> newRangeList = new List<Start_Date_Range__c>();
			Start_Date_Range__c range;
			for(Course_Extra_Fee_Dependent__c dep : extraFee.Course_Extra_Fees_Dependent__r){
				if(dep.Selected__c){
					range = new Start_Date_Range__c();
					range.Course_Extra_Fee_Dependent__c = dep.id;
					range.From_Date__c = newStartDate.From_Date__c;
					range.To_Date__c = newStartDate.To_Date__c;
					range.Value__c = newStartDate.Value__c;
					range.Comments__c = newStartDate.Comments__c;
					range.Account_Document_File__c = newStartDate.Account_Document_File__c;
					newRangeList.add(range);
					relatedFeeIDs.add(dep.id);
				}
			}
			
			
			Map<String, List<Start_Date_Range__c>> mapRange = new Map<String, List<Start_Date_Range__c>>();
			for(Start_Date_Range__c rpop : [select from_date__c, to_date__c, Course_Extra_Fee_Dependent__c from Start_Date_Range__c where Course_Extra_Fee_Dependent__c in :relatedFeeIDs]){
				if(!mapRange.containsKey(rpop.Course_Extra_Fee_Dependent__c))
					mapRange.put(rpop.Course_Extra_Fee_Dependent__c, new list<Start_Date_Range__c>{rpop});
				else mapRange.get(rpop.Course_Extra_Fee_Dependent__c).add(rpop);
			}
			
			
			
			list<Start_Date_Range__c> validRanges = new list<Start_Date_Range__c>();
			String innerErrorMsg = '';
			boolean isValid = true;
			errorUpdateRange = new set<string>();
			for(Start_Date_Range__c newRange : newRangeList){
				isValid = true;
				if(mapRange.containsKey(newRange.Course_Extra_Fee_Dependent__c)){
					for(Start_Date_Range__c mr : mapRange.get(newRange.Course_Extra_Fee_Dependent__c)){
						if( !( (newRange.From_Date__c < mr.To_Date__c && newRange.To_Date__c < mr.From_Date__c) || (newRange.From_Date__c > mr.To_Date__c && newRange.To_Date__c > mr.To_Date__c) ) ) {
							isValid = false;
							innerErrorMsg += '<span style="color:grey"> From Date:</span> '+ newRange.From_Date__c.format() +' <span style="color:grey">To Date:</span> '+ newRange.To_Date__c.format() + '<br />';
							errorUpdateRange.add(mr.id);
						}
					}
				}
				newRange.Campus__c = campusID;
				
				if(isValid)
					validRanges.add(newRange);
				
			}
			
			if(!validRanges.isEmpty()){
				insert validRanges;
			}
			
			if(!errorUpdateRange.isEmpty()){
				String errorMsg = '<span style="color:red;">'+errorUpdateRange.size() + ' out of ' + newRangeList.size() + ' extra fee range(s) showed below couldn\'t be updated as they have duplicated values.</span> <br /> ';
				errorMsg += innerErrorMsg;
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, errorMsg);
				ApexPages.addMessage(msg);
			}
			
			
			
		}
		relatedFees = null;
		return null;
		
	}
	
	
	public Boolean validateFields(List<Start_Date_Range__c> startDateRanges){
		
		Boolean fieldsValidated = true;
		for(Start_Date_Range__c sdr: startDateRanges){
			if(sdr.From_Date__c == null){
				sdr.From_Date__c.addError('Field is required.');
				fieldsValidated = false;			
			}
			if(sdr.To_Date__c == null){
				sdr.To_Date__c.addError('Field is required.');
				fieldsValidated = false;
			}
			if( !Type.equals(PROMOTION) && sdr.Value__c == null){
				sdr.Value__c.addError('Field is required.');
				fieldsValidated = false;
			}
			
			/*if( ( !Type.equals(PROMOTION) || (Type.equals(PROMOTION) && deal.Promotion_Type__c == PROMOTION_EXTRA_FEE_DISCOUNT) ) && sdr.Value__c <= 0){
				sdr.Value__c.addError('From must be positive.');
				fieldsValidated = false;
			}*/
			
			if( Type.equals(PROMOTION) && deal.Promotion_Type__c == PROMOTION_FREE_UNITS && sdr.Number_of_Free_Weeks__c == null){
				sdr.Number_of_Free_Weeks__c.addError('Field is required.');
				fieldsValidated = false;
			}
			
			/*if( Type.equals(PROMOTION) && deal.Promotion_Type__c == PROMOTION_FREE_UNITS && sdr.Number_of_Free_Weeks__c <= 0){
				sdr.Number_of_Free_Weeks__c.addError('Get Free must be positive.');
				fieldsValidated = false;
			}*/
			
			if(sdr.From_Date__c > sdr.To_Date__c){
				sdr.From_Date__c.addError('From date cannot be greater than To date.');
				fieldsValidated = false;
			}
		}
		return fieldsValidated;
	}
	
	
	private Map<String, Start_Date_Range__c> oldCourseFeeRange = new Map<String, Start_Date_Range__c>();
	
	public PageReference saveStartDates(){
		
		errorUpdateRange = new Set<String>();
		
		if(Type.equals(RELATED_FEE))
			saveRelatedFeeStartDates();
		else
			saveStartDateRange();
		
		listStartDates = null;
		editMode = false;
		return null;
	}
	
	
	private void saveStartDateRange(){
		
		if(!validateFields(listStartDates)) return;
		
		String innerErrorMsg = '';
		boolean isValid = true;
		
		list<Start_Date_Range__c> validRange = new list<Start_Date_Range__c>();
		
		for(Start_Date_Range__c range1 : listStartDates){
			for(Start_Date_Range__c range2 : listStartDates){
				
				if( range1.id != range2.id && !( (range1.From_Date__c < range2.From_Date__c && range1.To_Date__c < range2.From_Date__c) || (range1.From_Date__c > range2.To_Date__c && range1.To_Date__c > range2.To_Date__c) ) ) {
					isValid = false;
					if(!errorUpdateRange.contains(range1.id)){
						//innerErrorMsg += '<b>'+rangeFee.Nationality__c + '</b><br />';
						innerErrorMsg += '<span style="color:grey"> From Date:</span> '+ range1.From_Date__c.format() +' <span style="color:grey">To Date:</span> '+ range1.To_Date__c.format() + '<br />';
						errorUpdateRange.add(range1.id);
					}
				}
				
			}
			if(isValid)
				validRange.add(range1);
		}
		
		if(validRange.size() > 0)
			update validRange;
		
		if(errorUpdateRange.size() > 0){
			String errorMsg = '<span style="color:red;">'+errorUpdateRange.size() + ' out of ' + listStartDates.size() + ' related fees showed below couldn\'t be updated as they have duplicated values.</span> <br /> ';
			errorMsg += innerErrorMsg;
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, errorMsg);
			ApexPages.addMessage(msg);
		}
		
		
		
	}
	
	
	public void saveRelatedFeeStartDates(){
		List<Start_Date_Range__c> lrange = new List<Start_Date_Range__c>();
		Map<String, List<Start_Date_Range__c>> mapFeeRanges = new Map<String, List<Start_Date_Range__c>>();
		Start_Date_Range__c oldRange;
		errorUpdateRange = new Set<String>();
		
		
		for(Course_Extra_Fee_Dependent__c cefd : relatedFees){
			for(Start_Date_Range__c cesdr : cefd.Start_Date_Ranges__r){						
				oldRange = oldCourseFeeRange.get(cesdr.id);
				
				if( cesdr.From_Date__c != oldRange.From_Date__c || cesdr.To_Date__c != oldRange.To_Date__c || cesdr.Value__c != oldRange.Value__c || cesdr.Comments__c != oldRange.Comments__c || cesdr.Account_Document_File__c != oldRange.Account_Document_File__c){
					cesdr.selected__c = false;
					lrange.add(cesdr);
				}
				
				if(!mapFeeRanges.containsKey(cefd.id))
					mapFeeRanges.put(cefd.id, new list<Start_Date_Range__c>{cesdr});
				else mapFeeRanges.get(cefd.id).add(cesdr);
				
			}
		}
		
		if(!validateFields(lrange)) return;
		
		String innerErrorMsg = '';
		boolean isValid = true;
		
		list<Start_Date_Range__c> validRange = new list<Start_Date_Range__c>();
		for(Start_Date_Range__c rangeFee : lrange){
			isValid = true;
			for(Start_Date_Range__c mapRange : mapFeeRanges.get(rangeFee.Course_Extra_Fee_Dependent__c)){
	        	if(rangeFee.Id != mapRange.id) 
					if( !( (rangeFee.From_Date__c < mapRange.From_Date__c && rangeFee.To_Date__c < mapRange.From_Date__c) || (rangeFee.From_Date__c > mapRange.To_Date__c && rangeFee.To_Date__c > mapRange.To_Date__c) ) ) {
						isValid = false;
						if(!errorUpdateRange.contains(rangeFee.id)){
							//innerErrorMsg += '<b>'+rangeFee.Nationality__c + '</b><br />';
							innerErrorMsg += '<span style="color:grey"> From Date:</span> '+ rangeFee.From_Date__c.format() +' <span style="color:grey">To Date:</span> '+ rangeFee.To_Date__c.format() + '<br />';
							errorUpdateRange.add(rangeFee.id);
						}
					}
			} 
			if(isValid)
				validRange.add(rangeFee);
		}
		
		if(validRange.size() > 0)
			update validRange;
		
		if(errorUpdateRange.size() > 0){
			String errorMsg = '<span style="color:red;">'+errorUpdateRange.size() + ' out of ' + lrange.size() + ' related fees showed below couldn\'t be updated as they have duplicated values.</span> <br /> ';
			errorMsg += innerErrorMsg;
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, errorMsg);
			ApexPages.addMessage(msg);
		}
		
		relatedFees = null;
	}
	
	
	
	
	
	public Boolean editMode{get{if(editMode == null) editMode = false; return editMode;} Set;}
	
	public pageReference editStartDates(){		
		editMode = true;
		return null;
	}
	
	public pageReference deleteStartDates(){	
		
		List<Start_Date_Range__c> deleteDates = new List<Start_Date_Range__c>();
		
		if(Type.equals(RELATED_FEE)){
			for(Course_Extra_Fee_Dependent__c cefd : relatedFees)
				for(Start_Date_Range__c sdr : cefd.Start_Date_Ranges__r)
					if(sdr.Selected__c)
						deleteDates.add(sdr);
		} else {
			for(Start_Date_Range__c sdr : listStartDates)
				if(sdr.Selected__c)
					deleteDates.add(sdr);
		}
			
		
		if(!deleteDates.isEmpty())
			delete deleteDates;
		
		if(Type.equals(RELATED_FEE))
			relatedFees = null;
		else 
			listStartDates = null;
		
		return null;	
	}
	
	public pageReference cancelEdit(){
		editMode = false;
		relatedFees = null;
		listStartDates = null;
		return null;
	}
	
	public List<Start_Date_Range__c> listStartDates{
		get{
			if(listStartDates == null)
				listStartDates = getListStartDates();
			return listStartDates;
		}
		Set;
	}
	
	private List<Start_Date_Range__c> getListStartDates(){
		
		List<Start_Date_Range__c> ranges = new List<Start_Date_Range__c>();
		
		if(Type.equals(EXTRA_FEE))
			ranges = [select Id, Account_Document_File__r.Description__c, Account_Document_File__c, Account_Document_File__r.File_Name__c, From_Date__c, TO_Date__c, Value__c, Comments__c, Selected__c from Start_Date_Range__c where Course_Extra_Fee__c = :parentID order by From_Date__c, Value__c];		
		else if(Type.equals(COURSE_PRICE))
			ranges = [select Id, Account_Document_File__r.Description__c, Account_Document_File__c, Account_Document_File__r.File_Name__c, From_Date__c, TO_Date__c, Value__c, Comments__c, Selected__c from Start_Date_Range__c where Course_Price__c = :parentID order by From_Date__c, Value__c];		
		else if(Type.equals(PROMOTION))
			ranges = [select Id, Account_Document_File__r.Description__c, Account_Document_File__c, Account_Document_File__r.File_Name__c, From_Date__c, TO_Date__c, Value__c, Comments__c, Selected__c, Promotion_Name__c, Number_of_Free_Weeks__c from Start_Date_Range__c where Promotion__c = :parentID order by From_Date__c, Value__c];		
		
		oldCourseFeeRange = new Map<String, Start_Date_Range__c>(ranges);
		
		return ranges;
	}
	
	public List<Course_Extra_Fee_Dependent__c> relatedFees {
		get{
			if( Type.equals(RELATED_FEE) && relatedFees == null){
				relatedFees = getRelatedFees();
				
				List<Start_Date_Range__c> startDateRanges = new List<Start_Date_Range__c>();
				for(Course_Extra_Fee_Dependent__c cefd : relatedFees)
					startDateRanges.addAll(cefd.Start_Date_Ranges__r.deepClone(true));
				
				oldCourseFeeRange = new Map<String, Start_Date_Range__c>(startDateRanges);
			}
			return relatedFees;
		}
		Set;
	}
	private List<Course_Extra_Fee_Dependent__c> getRelatedFees(){
		List<Course_Extra_Fee_Dependent__c> related = new List<Course_Extra_Fee_Dependent__c>();
		
		for(Course_Extra_Fee_Dependent__c dep : [Select C.Date_Paid_From__c, C.Date_Paid_To__c, C.From__c, C.Product__r.Name__c, C.Value__c, ( Select Account_Document_File__r.Description__c, Account_Document_File__c, Account_Document_File__r.File_Name__c, From_Date__c, Selected__c, To_Date__c, Value__c, Comments__c from Start_Date_Ranges__r order by From_Date__c, Value__c) from Course_Extra_Fee_Dependent__c C WHERE C.Course_Extra_Fee__c = :extraFee.id order by Product__r.Name__c, Date_Paid_From__c, From__c])
			if(dep.Start_Date_Ranges__r.size() > 0)
				related.add(dep);
		
		return related;
	}
	
	
	
	public class sDriveFiles{
		public String URL {get;set;}
		public Account_Document_File__c File {get;set;}
	} 
	
	private Map<String, sDriveFiles> mapFiles = new Map<String, sDriveFiles>();
	
	public List<SelectOption> Files {
		get{
			if(Files== null){
				Files = new List<SelectOption>();
				List<ID> parentIDs = new List<ID>();
				List<ID> fileObjects = new List<ID>();
				files.add(new SelectOption('','-- Select a File --'));	
				
				Map<String, List<Account_Document_File__c>> campusFiles = new Map<String, List<Account_Document_File__c>>();
				Map<String, String> folderNames = new Map<String, String>();
				folderNames.put('','Not Classified');
				campusFiles.put( '' , new List<Account_Document_File__c>() );
				for(Account_Document_File__c folder : [SELECT id, File_Name__c FROM Account_Document_File__c WHERE Parent__c = :campusID and Content_Type__c = 'Folder' and WIP__c = false order by Content_Type__c]){
					campusFiles.put( folder.id, new List<Account_Document_File__c>() );
					folderNames.put( Folder.id, Folder.File_Name__c );
				}
				
				System.debug('@@@@@ campusID: ' + campusID);
				
				for(Account_Document_File__c folder : [SELECT id, Parent_Folder_Id__c, Description__c, CreatedDate, File_Name__c,File_Size__c FROM Account_Document_File__c WHERE Parent__c = :campusID and Content_Type__c != 'Folder' order by Content_Type__c]){
					List<Account_Document_File__c> ladf = campusFiles.get(Folder.Parent_Folder_Id__c == null ? '' : Folder.Parent_Folder_Id__c);
					if(ladf != null){
						ladf.add(folder);
						campusFiles.put(Folder.Parent_Folder_Id__c == null ? '' : Folder.Parent_Folder_Id__c, ladf);
					}
				}
				
				for(String folderid : campusFiles.keyset()){					
					if(!campusFiles.get(folderid).isEmpty()){
						
						files.add(new SelectOption('Folder',folderNames.get(folderid)));
						
						for(Account_Document_File__c file : campusFiles.get(folderid)){							
							parentIDs.add(campusID);
							fileObjects.add(file.id);
							sDriveFiles sdf = new sDriveFiles();
							sdf.File = file;
							mapFiles.put(file.id,sdf);
							files.add(new SelectOption(file.id, file.Description__c + ' [*' + file.File_Name__c + ' *]')  );
							
						}
						
						files.add(new SelectOption('disabled', ''));	
					}
				}
				
				try {
					if(files.size() > 1)
						files.remove(files.size()-1);
				} catch (Exception e){}
				
				
				if(parentIDs.size() > 0 && fileObjects.size() > 0){
					try {	
						List<String> urls = cg.SDriveTools.getAttachmentURLs( parentIDs , fileObjects, (100 * 60));
							
						for(string sd : mapFiles.keySet())
							for(String url : urls )
								if(url.contains(mapFiles.get(sd).file.id)){
									mapFiles.get(sd).url = url;
									break;
								}
					} catch (Exception e){}
				}
			}
			return Files;		
		}
		set;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}