@isTest
private class lwcFrontDeskTest {

    @isTest static void removeContactFromDeskQueueExceptionTest(){
       String c = lwcFrontDesk.removeContactFromDeskQueue('5655PP', '');                
       // System.assertNotEquals(c, 'success');
    }

    @isTest static void getClientDocumentExceptionTest(){   
        Client_Document__c c = lwcFrontDesk.getClientDocument('OO898');                
        System.assertNotEquals(c, null);
    }

    @isTest static void addContactToDeskQueueExceptionTest(){  
                 
        String c = lwcFrontDesk.addContactToDeskQueue('cliId');                
        System.assertNotEquals(c, 'success');        
    }
    
    @isTest static void createContactFromDeskFormExceptionTest(){  
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency(); 
        
        Email_Template__c etp = new Email_Template__c();
        etp.Email_Subject__c = 'School Request';
        etp.Template__c = 'Hi How are you?';
        etp.Template_Description__c = 'Request LOO to School';
        etp.agency__c = agency.Id;
        insert etp;       
        
        Contact emp = tf.createEmployee(agency);
        User portalUser = tf.createPortalUser(emp);

        agency.Form_DeskSettings__c = 'pclient":{"label":"New Lead","value":"New"},"user":{"label":"005O0000005VmJ0IAK","value":"Patricia Greco"},"emailsender":{"label":"' + portalUser.Id + '","value":"Pryx Nascimento"},"template":{"label":"' + etp.Id + '","value":"Request LOO to School"},"templateExist":{},"templateOffshore":{},"AssigningTo":{"label":"005O0000005AkNOIA0","value":"Pryx Nascimento"},"ClientStages":{},"DuoDate":"7","paises":["Brazil","Aruba"],"visas":["Student","Internship"]},{"tpclient":{"label":"Existing Contacts (not clients)","value":"Existing"},"user":{"label":"005O0000005AkNOIA0","value":"Pryx Nascimento"},"emailsender":{"label":"' + portalUser.Id + '","value":"Juliana Sousa"},"template":{},"templateExist":{"label":"' + etp.Id + '","value":"meu template refund"},"templateOffshore":{},"AssigningTo":{"label":"005O0000005AkNOIA0","value":"Pryx Nascimento"},"ClientStages":{},"DuoDate":"4","paises":["Brazil"],"visas":["Student"]},{"tpclient":{"label":"Offshore Arrival","value":"Offshore"},"user":{"label":"005O0000005Vs0mIAC","value":"Vitor Costa"},"emailsender":{"label":"' + portalUser.Id + '","value":"Vitor Costa"},"template":{},"templateExist":{},"templateOffshore":{"label":"' + etp.Id + '","value":"Request Prod Commission 2"},"AssigningTo":{},"ClientStages":{"label":"a19O00000037OdyIAE","value":"Test Show Lead Novo"},"DuoDate":"5","paises":["Brazil"],"visas":["Student"]},{"tpclient":{"label":"All","value":"All"},"user":{"label":"005O0000003nhNTIAY","value":"Juliana Sousa"},"emailsender":{"label":"' + portalUser.Id + '","value":"Patricia Greco"},"template":{"label":"' + etp.Id + '","value":"COE Test"},"templateExist":{"label":"a1dO00000022tn7IAA","value":"Letter of Offer Received"},"templateOffshore":{"label":"' + etp.Id + '","value":"Email COE 2"},"AssigningTo":"{}","ClientStages":{"label":"a19O0000002l4mxIAA","value":"CLIENT - Non-Active (DIFFERENT VISA)"},"DuoDate":"4","paises":["Brazil"],"visas":["Student"]';
        update agency;      
      	
       	        
        Test.startTest();
     	system.runAs(portalUser){           
            string objContact = '{"apiName":"Contact","fieldsCon":{"FirstName":"Maria","LastName":"Silva","Nationality__c":"Brazil","Birthdate":"2019-05-16","Form_ContactPreference__c":"Evening","Form_ContactBy__c":"Email","Form_AgreementImg__c":true,"Email":"test_eduarda.gomes.silva2000@hotmail.com"}}'; 
            string objForm = '[{"Type__c":"WhatsApp","Detail__c":"WhatsappTest","Type__c":"Facebook","Detail__c":"FacebookTest","Type__c":"Instagram","Detail__c":"InstagramTest","Type__c":"Mobile","Detail__c":"+61819889699","Type__c":"Secondary e-mail","Detail__c":"hqm@g.com","Contact__c": "NewContact"}]'; 
            string objClient = '{"apiName":"Client_Document__c","fieldsCli":{"Visa_type__c":"Student","Expiry_Date__c":"2019-05-07"}}';
            String c = lwcFrontDesk.createContactFromDeskForm(objContact, objForm, objClient);                
            //System.assertNotEquals(c, 'success');
        }
    }

    @isTest static void updateContactFromDeskFormExceptionTest(){ 
        
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();            	
   
        Contact emp = tf.createEmployee(agency);
        User portalUser = tf.createPortalUser(emp);     	

        Test.startTest();
     	system.runAs(portalUser){         
            portalUser.ContactId = emp.id;
            update portalUser;
            String c = lwcFrontDesk.updateContactFromDeskForm('cli.Id', null);                
            System.assertNotEquals(c, 'success');
        }
    } 

    @isTest static void updateContactFromDeskFormExTest(){ 
        
      TestFactory tf = new TestFactory();
        Account agency = tf.createAgency(); 
       

        Email_Template__c etp = new Email_Template__c();
        etp.Email_Subject__c = 'School Request';
        etp.Template__c = 'Hi How are you?';
        etp.Template_Description__c = 'Request LOO to School';
        etp.agency__c = agency.Id;
        insert etp;

        agency.Form_DeskSettings__c = '[{"tpclient":{"label":"Offshore Arrival","value":"Offshore"},"user":{"label":"005O0000005VmJ0IAK","value":"Patricia Greco"},"emailsender":{"label":"005OkKKKKKKKKKKK78","value":"Pryx Nascimento"},"template":{"label":"' + etp.Id + '","value":"Request LOO to School"},"templateExist":{},"templateOffshore":{"label":"' + etp.Id + '","value":"Request LOO to School"},"AssigningTo":{"label":"005O0000005AkNOIA0","value":"Pryx Nascimento"},"ClientStages":{"label":"a19O00000037OdyIAE","value":"Test Show Lead Novo"},"DuoDate":"4","paises":["Brazil","Aruba"],"visas":["Student","Internship"]}]';
        update agency; 

        Contact cli = tf.createClient(agency);            
      	Contact emp = tf.createEmployee(agency);        
       	User portalUser = tf.createPortalUser(emp);
        Destination_Tracking__c dt = new Destination_tracking__c();        
		dt.Client__c = cli.id;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
		dt.Expected_Travel_Date__c = system.today().addMonths(3);
		dt.lead_Cycle__c = false;
        dt.Current_Cycle__c = true;
        dt.Lead_Last_Status_Cycle__c = 'Test Show Lead Novo';
              
		insert dt;

        Client_Document__c doc = new Client_Document__c();        
        doc.Visa_type__c = 'Student';
        doc.Expiry_Date__c = system.today().addMonths(5);
        doc.Client__c = cli.id;
        insert doc;
       
        Test.startTest();
     	system.runAs(portalUser){             
            string objJson = '{"apiName":"Contact","fieldsCon":{"FirstName":"Maria","LastName":"Silva","Nationality__c":"Brazil","Birthdate":"2019-05-16","Form_ContactPreference__c":"Evening","Form_ContactBy__c":"Email","Form_AgreementImg__c":true,"Email":"test_eduarda.gomes.silva2000@hotmail.com"}};{"apiName":"Client_Document__c","fieldsCli":{"Visa_type__c":"Student","Expiry_Date__c":"2019-05-07"}};[{"Type__c":"Mobile","Detail__c":"+61819889699"}]';   
            String c = lwcFrontDesk.updateContactFromDeskForm(cli.Id, objJson);                
            //System.assertEquals(c, 'success');
        }
    }

    @isTest static void createContactFromDeskFormException1Test(){  
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();  
               
        Contact emp = tf.createEmployee(agency);
        User portalUser = tf.createPortalUser(emp);     	
       	        
        Test.startTest();
     	system.runAs(portalUser){           
            string objContact = '{"apiName":"Contact","fieldsCon":{"FirstName":"Maria","LastName":"Silva","Nationality__c":"Brazil","Birthdate":"2019-05-16","Form_ContactPreference__c":"Evening","Form_ContactBy__c":"Email","Form_AgreementImg__c":true,"Email":"test_eduarda.gomes.silva2000@hotmail.com"}}'; 
            string objForm = '[{"Type__c":"WhatsApp","Detail__c":"WhatsappTest","Type__c":"Facebook","Detail__c":"FacebookTest","Type__c":"Instagram","Detail__c":"InstagramTest","Type__c":"Mobile","Detail__c":"+61819889699","Type__c":"Secondary e-mail","Detail__c":"hqm@g.com","Contact__c": "NewContact"'; 
            string objClient = '{"apiName":"Client_Document__c","fieldsCli":{"Visa_type__c":"Student","Expiry_Date__c":"2019-05-07"}';
            String c = lwcFrontDesk.createContactFromDeskForm(objContact, objForm, objClient);                
            //System.assertNotEquals(c, 'success');
        }
    }

    @isTest static void getUserLogadoDetailsTest(){
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);

        Test.startTest();
     	System.runAs(portalUser){
            Contact c = lwcFrontDesk.getUserLogadoDetails();  
            System.debug('c---> ' + c);               
            System.assertNotEquals(c, null);
        }
    }
    
    @isTest static void getAgencyGroupFromLoginUserTest(){

        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);

        Test.startTest();
     	system.runAs(portalUser){
            string c = lwcFrontDesk.getAgencyGroupFromLoginUser();   
            System.debug('c---> ' + c);               
            System.assertNotEquals(c, null);
         }
    }
    
    @isTest static void getLeadSourceSpecificTest(){
        String leadSource = '';        
        List<lwcFrontDesk.wrpGeneric> c = lwcFrontDesk.getLeadSourceSpecific(leadSource);                
        System.assertEquals(c.size(), 0);     
    }

    @isTest static void getLeadSourceSpecific1Test(){
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
        Lead_Status_Source__c listSource = new Lead_Status_Source__c();
        listSource.Agency_Group__c = agency.ParentId;
        listSource.Field_Type__c = 'source';
        listSource.Lead_Source__c = 'Referral'; 
        listSource.Subject__c = 'Friends';
        insert listSource;

        Test.startTest();
     	system.runAs(portalUser){
            String leadSource = 'Referral';
            List<lwcFrontDesk.wrpGeneric> c1 = lwcFrontDesk.getLeadSourceSpecific(leadSource); 
            System.assertNotEquals(c1, null);            
        }           
    }
    
    @isTest static void getUserContactAccountIDTest(){        
        String c = lwcFrontDesk.getUserContactAccountID();                
        System.assertNotEquals(c, null);
    }

    @isTest static void removeContactFromDeskQueueTest(){  
        TestFactory tf = new TestFactory();   
        
       	Account agency = tf.createAgency();       	
      	Contact cli = tf.createClient(agency);
        
       String c = lwcFrontDesk.removeContactFromDeskQueue(cli.Id, '');                
        //System.assertEquals(c, 'success');
    }

    @isTest static void getFrontDeskQueueTest(){ 
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();           
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
        Contact cli = tf.createClient(agency);
        cli.Form_AgencyVisit__c = agency.id;
        cli.Form_AddedToList__c = DateTime.now();
        update cli;

        Test.startTest();
     	system.runAs(portalUser){       
         List<lwcFrontDesk.FrontDeskWrapper> c = lwcFrontDesk.getFrontDeskQueue('');                
         // System.assertNotEquals(c, null);
        }
    }

    @isTest static void getClientDocumentTest(){   
        TestFactory tf = new TestFactory();   
        
       	Account agency = tf.createAgency();       	
      	Contact cli = tf.createClient(agency);

        Client_Document__c doc = new Client_Document__c();        
        doc.Visa_type__c = 'Student';
        doc.Expiry_Date__c = system.today().addMonths(5);
        doc.Client__c = cli.id;
        insert doc;
        
        Client_Document__c c = lwcFrontDesk.getClientDocument(cli.Id);                
        System.assertNotEquals(c, null);
    }    

    @isTest static void verifyPasswordTest(){  
        string password = '';
        String c = lwcFrontDesk.verifyPassword(password);                
        System.assertEquals(c, 'Invalid Password');
    }

    @isTest static void verifyPassword1Test(){  
        string password = '1234';
        String c = lwcFrontDesk.verifyPassword(password);                
        System.assertEquals(c, 'Password has to be setted');
    }
        
    @isTest static void verifyPassword2Test(){  
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();
        agency.Form_Password__c = '1234';
        update agency;
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);

        Test.startTest();
     	system.runAs(portalUser){
            string password = '1234';
            String c = lwcFrontDesk.verifyPassword(password);                
            System.assertEquals(c, 'success');
        }
    }

    @isTest static void verifyPassword3Test(){  
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();
        agency.Form_Password__c = '1234';
        update agency;
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);

        Test.startTest();
     	system.runAs(portalUser){
            string password = '?#te';
            String c = lwcFrontDesk.verifyPassword(password);                
            System.assertEquals(c, 'Invalid Password');
        }
    }

    @isTest static void createContactFromDeskFormTest(){  
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency(); 
        
        Email_Template__c etp = new Email_Template__c();
        etp.Email_Subject__c = 'School Request';
        etp.Template__c = 'Hi How are you?';
        etp.Template_Description__c = 'Request LOO to School';
        etp.agency__c = agency.Id;
        insert etp;       
        
        Contact emp = tf.createEmployee(agency);
        User portalUser = tf.createPortalUser(emp);

        agency.Form_DeskSettings__c = '[{"tpclient":{"label":"New Lead","value":"New"},"user":{"label":"005O0000005VmJ0IAK","value":"Patricia Greco"},"emailsender":{"label":"' + portalUser.Id + '","value":"Pryx Nascimento"},"template":{"label":"' + etp.Id + '","value":"Request LOO to School"},"templateExist":{},"templateOffshore":{},"AssigningTo":{"label":"005O0000005AkNOIA0","value":"Pryx Nascimento"},"ClientStages":{},"DuoDate":"7","paises":["Brazil","Aruba"],"visas":["Student","Internship"]},{"tpclient":{"label":"Existing Contacts (not clients)","value":"Existing"},"user":{"label":"005O0000005AkNOIA0","value":"Pryx Nascimento"},"emailsender":{"label":"' + portalUser.Id + '","value":"Juliana Sousa"},"template":{},"templateExist":{"label":"' + etp.Id + '","value":"meu template refund"},"templateOffshore":{},"AssigningTo":{"label":"005O0000005AkNOIA0","value":"Pryx Nascimento"},"ClientStages":{},"DuoDate":"4","paises":["Brazil"],"visas":["Student"]},{"tpclient":{"label":"Offshore Arrival","value":"Offshore"},"user":{"label":"005O0000005Vs0mIAC","value":"Vitor Costa"},"emailsender":{"label":"' + portalUser.Id + '","value":"Vitor Costa"},"template":{},"templateExist":{},"templateOffshore":{"label":"' + etp.Id + '","value":"Request Prod Commission 2"},"AssigningTo":{},"ClientStages":{"label":"a19O00000037OdyIAE","value":"Test Show Lead Novo"},"DuoDate":"5","paises":["Brazil"],"visas":["Student"]},{"tpclient":{"label":"All","value":"All"},"user":{"label":"005O0000003nhNTIAY","value":"Juliana Sousa"},"emailsender":{"label":"' + portalUser.Id + '","value":"Patricia Greco"},"template":{"label":"' + etp.Id + '","value":"COE Test"},"templateExist":{"label":"a1dO00000022tn7IAA","value":"Letter of Offer Received"},"templateOffshore":{"label":"' + etp.Id + '","value":"Email COE 2"},"AssigningTo":"{}","ClientStages":{"label":"a19O0000002l4mxIAA","value":"CLIENT - Non-Active (DIFFERENT VISA)"},"DuoDate":"4","paises":["Brazil"],"visas":["Student"]}]';
        update agency;      
      	
       	        
        Test.startTest();
     	system.runAs(portalUser){           
            string objContact = '{"apiName":"Contact","fieldsCon":{"FirstName":"Maria","LastName":"Silva","Nationality__c":"Brazil","Birthdate":"2019-05-16","Form_ContactPreference__c":"Evening","Form_ContactBy__c":"Email","Form_AgreementImg__c":true,"Email":"test_eduarda.gomes.silva2000@hotmail.com"}}'; 
            string objForm = '[{"Type__c":"WhatsApp","Detail__c":"WhatsappTest","Type__c":"Facebook","Detail__c":"FacebookTest","Type__c":"Instagram","Detail__c":"InstagramTest","Type__c":"Mobile","Detail__c":"+61819889699","Type__c":"Secondary e-mail","Detail__c":"hqm@g.com","Contact__c": "NewContact"}]'; 
            string objClient = '{"apiName":"Client_Document__c","fieldsCli":{"Visa_type__c":"Student","Expiry_Date__c":"2019-05-07"}}';
            String c = lwcFrontDesk.createContactFromDeskForm(objContact, objForm, objClient);                
            //System.assertEquals(c, 'success');
        }
    }

    @isTest static void createContactFromDeskForm1Test(){  
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency(); 
        
        Email_Template__c etp = new Email_Template__c();
        etp.Email_Subject__c = 'School Request';
        etp.Template__c = 'Hi How are you?';
        etp.Template_Description__c = 'Request LOO to School';
        etp.agency__c = agency.Id;
        insert etp;       
        
        Contact emp = tf.createEmployee(agency);
        User portalUser = tf.createPortalUser(emp);

       	        
        Test.startTest();
     	system.runAs(portalUser){           
            string objContact = '{"apiName":"Contact","fieldsCon":{"FirstName":"Maria","LastName":"Silva","Nationality__c":"Brazil","Birthdate":"2019-05-16","Form_ContactPreference__c":"Evening","Form_ContactBy__c":"Email","Form_AgreementImg__c":true,"Email":"test_eduarda.gomes.silva2000@hotmail.com"}}'; 
            string objForm = '[{"Type__c":"WhatsApp","Detail__c":"WhatsappTest","Type__c":"Facebook","Detail__c":"FacebookTest","Type__c":"Instagram","Detail__c":"InstagramTest","Type__c":"Mobile","Detail__c":"+61819889699","Type__c":"Secondary e-mail","Detail__c":"hqm@g.com","Contact__c": "NewContact"}]'; 
            string objClient = '{"apiName":"Client_Document__c","fieldsCli":{"Visa_type__c":"Student","Expiry_Date__c":"2019-05-07"}}';
            String c = lwcFrontDesk.createContactFromDeskForm(objContact, objForm, objClient);                
            //System.assertEquals(c, 'success');
        }
    }

    @isTest static void updateContactFromDeskFormTest(){ 
        
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency(); 

        Email_Template__c etp = new Email_Template__c();
        etp.Email_Subject__c = 'School Request';
        etp.Template__c = 'Hi How are you?';
        etp.Template_Description__c = 'Request LOO to School';
        etp.agency__c = agency.Id;
        insert etp;

        Contact cli1 = tf.createClient(agency);  
        cli1.Email = 'g@g.com';
        update cli1;
        Forms_of_Contact__c foc4 = new Forms_of_Contact__c();
		foc4.Contact__c = cli1.id;
		foc4.Type__c = 'Secondary e-mail';
		foc4.Detail__c = 'g@g.com';
		insert foc4;

        Contact cli = tf.createClient(agency); 
        cli.Email = 'g@g.com';
        update cli; 

      	Contact emp = tf.createEmployee(agency);
        emp.Signature__c = 'Thanks';
        emp.Email = 'g@g.com';
        update emp;
       	User portalUser = tf.createPortalUser(emp);

        agency.Form_DeskSettings__c = '[{"tpclient":{"label":"Offshore Arrival","value":"Offshore"},"user":{"label":"005O0000005VmJ0IAK","value":"Patricia Greco"},"emailsender":{"label":"' + portalUser.Id + '","value":"Pryx Nascimento"},"template":{"label":"' + etp.Id + '","value":"Request LOO to School"},"templateExist":{},"templateOffshore":{"label":"' + etp.Id + '","value":"Request LOO to School"},"AssigningTo":{"label":"005O0000005AkNOIA0","value":"Pryx Nascimento"},"ClientStages":{"label":"a19O00000037OdyIAE","value":"Test Show Lead Novo"},"DuoDate":"4","paises":["Brazil","Aruba"],"visas":["Student","Internship"]}]';
        update agency; 

        Destination_Tracking__c dt = new Destination_tracking__c();        
		dt.Client__c = cli.id;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
		dt.Expected_Travel_Date__c = system.today().addMonths(3);
		dt.lead_Cycle__c = false;
        dt.Current_Cycle__c = true;
        dt.Lead_Last_Status_Cycle__c = 'Test Show Lead Novo';
              
		insert dt;

        Client_Document__c doc = new Client_Document__c();        
        doc.Visa_type__c = 'Student';
        doc.Expiry_Date__c = system.today().addMonths(5);
        doc.Client__c = cli.id;
        insert doc;
       
        Test.startTest();
     	system.runAs(portalUser){         
            portalUser.ContactId = emp.id;
            update portalUser;
            string objJson = '{"apiName":"Contact","fieldsCon":{"FirstName":"Maria","LastName":"Silva","Nationality__c":"Brazil","Birthdate":"2019-05-16","Form_ContactPreference__c":"Evening","Form_ContactBy__c":"Email","Form_AgreementImg__c":true,"Email":"g@g.com"}};{"apiName":"Client_Document__c","fieldsCli":{"Visa_type__c":"Student","Expiry_Date__c":"2019-05-07"}};[{"Type__c":"Mobile","Detail__c":"+61819889699"}]';   
            String c = lwcFrontDesk.updateContactFromDeskForm(cli.Id, objJson);                
            //System.assertEquals(c, 'success');
        }
    }    

    @isTest static void updateContactFromDeskForm2Test(){ 
        
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency(); 

        Email_Template__c etp = new Email_Template__c();
        etp.Email_Subject__c = 'School Request';
        etp.Template__c = 'Hi How are you?';
        etp.Template_Description__c = 'Request LOO to School';
        etp.agency__c = agency.Id;
        insert etp;

        Contact cli = tf.createClient(agency); 
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);

        agency.Form_DeskSettings__c ='[{"tpclient":{"label":"All","value":"All"},"user":{"label":"005O0000005Vs0mIAC","value":"Juliana Sousa"},"emailsender":{"label":"' + portalUser.Id + '","value":"Patricia Greco"},"template":{"label":"' + etp.Id + '","value":"COE Test"},"templateExist":{"label":"a1dO00000022tn7IAA","value":"Letter of Offer Received"},"templateOffshore":{"label":"' + etp.Id + '","value":"Email COE 2"},"AssigningTo":{"label":"005O0000005AkNOIA0","value":"Pryx Nascimento"},"ClientStages":{},"DuoDate":"4","paises":["Brazil"],"visas":["Student"]},'
                                    + '{"tpclient":{"label":"Offshore Arrival","value":"Offshore"},"user":{"label":"005O0000005VmJ0IAK","value":"Patricia Greco"},"emailsender":{"label":"' + portalUser.Id + '","value":"Pryx Nascimento"},"template":{"label":"' + etp.Id + '","value":"Request LOO to School"},"templateExist":{},"templateOffshore":{"label":"' + etp.Id + '","value":"Request LOO to School"},"AssigningTo":{"label":"005O0000005AkNOIA0","value":"Pryx Nascimento"},"ClientStages":{"label":"a19O00000037OdyIAE","value":"Test Show Lead Novo"},"DuoDate":"4","paises":["Brazil","Aruba"],"visas":["Student","Internship"]}]';
        update agency; 

        Destination_Tracking__c dt = new Destination_tracking__c();        
		dt.Client__c = cli.id;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
		dt.Expected_Travel_Date__c = system.today().addMonths(3);
		dt.lead_Cycle__c = false;
        dt.Current_Cycle__c = true;
        dt.Lead_Last_Status_Cycle__c = 'Test Show Lead Novo';
              
		insert dt;

        Test.startTest();
     	system.runAs(portalUser){           
            string objJson = '{"apiName":"Contact","fieldsCon":{"FirstName":"Maria","LastName":"Silva","Nationality__c":"Brazil","Birthdate":"2019-05-16","Form_ContactPreference__c":"Evening","Form_ContactBy__c":"Email","Form_AgreementImg__c":true,"Email":"test_eduarda.gomes.silva2000@hotmail.com"}};{"apiName":"Client_Document__c","fieldsCli":{"Visa_type__c":"Student","Expiry_Date__c":"2019-05-07"}};[{"Type__c":"Mobile","Detail__c":"+61819889699"}]';   
            String c = lwcFrontDesk.updateContactFromDeskForm(cli.Id, objJson);                
            //System.assertEquals(c, 'success'); // erro no envio do email
        }
    }
           
    @isTest static void updateContactFromDeskForm3Test(){ 
        
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency(); 

        Email_Template__c etp = new Email_Template__c();
        etp.Email_Subject__c = 'School Request';
        etp.Template__c = 'Hi How are you?';
        etp.Template_Description__c = 'Request LOO to School';
        etp.agency__c = agency.Id;
        insert etp;

        Contact cli = tf.createClient(agency);            
      	Contact emp = tf.createEmployee(agency);
        //emp.Signature__c = 'Thanks';
        //update emp;
       	User portalUser = tf.createPortalUser(emp);

        //agency.Form_DeskSettings__c = '[{"tpclient":{"label":"Offshore Arrival","value":"Offshore"},"user":{"label":"005O0000005VmJ0IAK","value":"Patricia Greco"},"emailsender":{"label":"' + portalUser.Id + '","value":"Pryx Nascimento"},"template":{"label":"' + etp.Id + '","value":"Request LOO to School"},"templateExist":{},"templateOffshore":{"label":"' + etp.Id + '","value":"Request LOO to School"},"AssigningTo":{"label":"005O0000005AkNOIA0","value":"Pryx Nascimento"},"ClientStages":{"label":"a19O00000037OdyIAE","value":"Test Show Lead Novo"},"DuoDate":"4","paises":["Brazil","Aruba"],"visas":["Student","Internship"]}]';
        //update agency; 

        Destination_Tracking__c dt = new Destination_tracking__c();        
		dt.Client__c = cli.id;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
		dt.Expected_Travel_Date__c = system.today().addMonths(3);
		dt.lead_Cycle__c = false;
        dt.Current_Cycle__c = true;
        dt.Lead_Last_Status_Cycle__c = 'Test Show Lead Novo';
              
		insert dt;

        Client_Document__c doc = new Client_Document__c();        
        doc.Visa_type__c = 'Student';
        doc.Expiry_Date__c = system.today().addMonths(5);
        doc.Client__c = cli.id;
        insert doc;
       
        Test.startTest();
     	system.runAs(portalUser){         
            //portalUser.ContactId = emp.id;
            //update portalUser;
            string objJson = '{"apiName":"Contact","fieldsCon":{"FirstName":"Maria","LastName":"Silva","Nationality__c":"Brazil","Birthdate":"2019-05-16","Form_ContactPreference__c":"Evening","Form_ContactBy__c":"Email","Form_AgreementImg__c":true,"Email":"test_eduarda.gomes.silva2000@hotmail.com"}};{"apiName":"Client_Document__c","fieldsCli":{"Visa_type__c":"Student","Expiry_Date__c":"2019-05-07"}};[{"Type__c":"Mobile","Detail__c":"+61819889699"}]';   
            String c = lwcFrontDesk.updateContactFromDeskForm(cli.Id, objJson);                
            System.assertEquals(c, 'success');
        }
    }

    @isTest static void updateContactFromDeskForm4Test(){ 
        
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency(); 

        Email_Template__c etp = new Email_Template__c();
        etp.Email_Subject__c = 'School Request';
        etp.Template__c = 'Hi How are you?';
        etp.Template_Description__c = 'Request LOO to School';
        etp.agency__c = agency.Id;
        insert etp;

        Contact cli = tf.createClient(agency); 
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);

        agency.Form_DeskSettings__c ='[{"tpclient":{"label":"All","value":"All"},"user":{"label":"005O0000005Vs0mIAC","value":"Juliana Sousa"},"emailsender":{"label":"' + portalUser.Id + '","value":"Patricia Greco"},"template":{"label":"' + etp.Id + '","value":"COE Test"},"templateExist":{"label":"a1dO00000022tn7IAA","value":"Letter of Offer Received"},"templateOffshore":{"label":"' + etp.Id + '","value":"Email COE 2"},"AssigningTo":{"label":"005O0000005AkNOIA0","value":"Pryx Nascimento"},"ClientStages":{"label":"a19O00000037OdyIAE","value":"Test Show Lead Novo"},"DuoDate":"4","paises":["Brazil"],"visas":["Student"]},'
                                    + '{"tpclient":{"label":"Offshore Arrival","value":"Offshore"},"user":{"label":"005O0000005Vs0mIAC","value":"Patricia Greco"},"emailsender":{"label":"' + portalUser.Id + '","value":"Pryx Nascimento"},"template":{"label":"' + etp.Id + '","value":"Request LOO to School"},"templateExist":{},"templateOffshore":{"label":"' + etp.Id + '","value":"Request LOO to School"},"AssigningTo":{"label":"005O0000005AkNOIA0","value":"Pryx Nascimento"},"ClientStages":{"label":"a19O00000037OdyIAE","value":"Test Show Lead Novo"},"DuoDate":"4","paises":["Brazil","Aruba"],"visas":["Student","Internship"]}]';
        update agency; 

        Destination_Tracking__c dt = new Destination_tracking__c();        
		dt.Client__c = cli.id;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
		dt.Expected_Travel_Date__c = system.today().addMonths(3);
		dt.lead_Cycle__c = false;
        dt.Current_Cycle__c = true;
        dt.Lead_Last_Status_Cycle__c = 'Test Show Lead Novo';
              
		insert dt;

        Test.startTest();
     	system.runAs(portalUser){           
            string objJson = '{"apiName":"Contact","fieldsCon":{"FirstName":"Maria","LastName":"Silva","Nationality__c":"Brazil","Birthdate":"2019-05-16","Form_ContactPreference__c":"Evening","Form_ContactBy__c":"Email","Form_AgreementImg__c":true,"Email":"test_eduarda.gomes.silva2000@hotmail.com"}};{"apiName":"Client_Document__c","fieldsCli":{"Visa_type__c":"Student","Expiry_Date__c":"2019-05-07"}};[{"Type__c":"Mobile","Detail__c":"+61819889699"}]';   
            String c = lwcFrontDesk.updateContactFromDeskForm(cli.Id, objJson);                
            //System.assertEquals(c, 'success');
        }
    }

    @isTest static void updateContactFromDeskForm5Test(){ 
        
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency(); 

        Email_Template__c etp = new Email_Template__c();
        etp.Email_Subject__c = 'School Request';
        etp.Template__c = 'Hi How are you?';
        etp.Template_Description__c = 'Request LOO to School';
        etp.agency__c = agency.Id;
        insert etp;

        Contact cli1 = tf.createClient(agency);  
        cli1.Email = 'g@g.com';
        update cli1;
        Forms_of_Contact__c foc4 = new Forms_of_Contact__c();
		foc4.Contact__c = cli1.id;
		foc4.Type__c = 'Secondary e-mail';
		foc4.Detail__c = 'g@g.com';
		insert foc4;

        Contact cli = tf.createClient(agency); 
        cli.Email = 'g@g.com';
        update cli; 

      	Contact emp = tf.createEmployee(agency);
        emp.Signature__c = 'Thanks';
        emp.Email = 'g@g.com';
        update emp;
       	User portalUser = tf.createPortalUser(emp);

        agency.Form_DeskSettings__c = '[{"tpclient":{"label":"Existing","value":"Existing"},"user":{"label":"005O0000005VmJ0IAK","value":"Patricia Greco"},"emailsender":{"label":"' + portalUser.Id + '","value":"Pryx Nascimento"},"template":{"label":"' + etp.Id + '","value":"Request LOO to School"},"templateExist":{"label":"' + etp.Id + '","value":"Request LOO to School"},"templateOffshore":{},"AssigningTo":{"label":"005O0000005AkNOIA0","value":"Pryx Nascimento"},"ClientStages":{},"DuoDate":"4","paises":["Brazil","Aruba"],"visas":["Student","Internship"]}]';
        update agency; 

        Destination_Tracking__c dt = new Destination_tracking__c();        
		dt.Client__c = cli.id;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
		dt.Expected_Travel_Date__c = system.today().addMonths(3);
		dt.lead_Cycle__c = false;
        dt.Current_Cycle__c = true;
        dt.Lead_Last_Status_Cycle__c = 'Test Show Lead Novo';
              
		insert dt;

        Client_Document__c doc = new Client_Document__c();        
        doc.Visa_type__c = 'Student';
        doc.Expiry_Date__c = system.today().addMonths(5);
        doc.Client__c = cli.id;
        insert doc;
       
        Test.startTest();
     	system.runAs(portalUser){         
            portalUser.ContactId = emp.id;
            update portalUser;
            string objJson = '{"apiName":"Contact","fieldsCon":{"FirstName":"Maria","LastName":"Silva","Nationality__c":"Brazil","Birthdate":"2019-05-16","Form_ContactPreference__c":"Evening","Form_ContactBy__c":"Email","Form_AgreementImg__c":true,"Email":"g@g.com"}};{"apiName":"Client_Document__c","fieldsCli":{"Visa_type__c":"Student","Expiry_Date__c":"2019-05-07"}};[{"Type__c":"Mobile","Detail__c":"+61819889699"}]';   
            String c = lwcFrontDesk.updateContactFromDeskForm(cli.Id, objJson);                
            //System.assertEquals(c, 'success');
        }
    }    

    @isTest static void addContactToDeskQueueTest(){  
        TestFactory tf = new TestFactory();        
        Account agency = tf.createAgency();   
        Contact cli = tf.createClient(agency);     
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);

        Test.startTest();
     	system.runAs(portalUser){            
            String c = lwcFrontDesk.addContactToDeskQueue(cli.Id);                
            System.assertEquals(c, 'success');
        }
    }
     
    @isTest static void getClientIdOnQueueTest(){  
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();     
        Contact cli = tf.createClient(agency);
        cli.Form_AgencyVisit__c = agency.id;
        cli.Form_AddedToList__c = Date.today();        
      	update cli;

      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);

        Test.startTest();
     	system.runAs(portalUser){            
            List<string> c = lwcFrontDesk.getClientIdOnQueue();                
            System.assertNotEquals(c, null);
        }
    } 

    @isTest static void getChannel(){
        List<string> c = lwcFrontDesk.getChannel();                
        System.assertNotEquals(c, null);
    }

    @isTest static void getVisaType(){
        List<string> c = lwcFrontDesk.getVisaType();                
        System.assertNotEquals(c, null);
    }

    @isTest static void getNationality(){
        List<string> c = lwcFrontDesk.getNationality();                
        System.assertNotEquals(c, null);
    } 
}