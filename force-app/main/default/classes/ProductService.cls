global class ProductService extends api_manager {
    
    static final String PUBLISHED_PRICE = 'Published Price';
    
    global List<SchoolProduct> getWebsiteAccommodationFees(Set<String> lcc, String nationality, Date paymentDate, double location, String language){
    	
    	Map<String, list<String>> campuses = new Map<String, list<String>>();
    	for(Campus_Course__c cc : [Select id, Campus__c from Campus_Course__c where id in :lcc])
    		if(!campuses.containsKey(cc.campus__c))
    			campuses.put(cc.campus__c, new list<String>{cc.id});
    		else campuses.get(cc.campus__c).add(cc.id);
    	
    	Map<String, Course_Extra_Fee__c> products = new Map<String, Course_Extra_Fee__c>();
    	
    	Map<String, String> nationalities = api_manager.getNationalityGroups(nationality);
    	
    	//Campus Fees
    	products = processFees( products, getWebsiteExtraFees('Accommodation', true, campuses.keySet(), paymentDate, location), nationalities );
    	system.debug('@ products: ' + products);
    	
    	//Course Fees
    	products = processFees( products, getWebsiteExtraFees('Accommodation', false, lcc, paymentDate, location), nationalities );
    	system.debug('@ products2: ' + products);
    	
    	return createSchoolProductList(products, campuses, language);
    	
    }
    
    private List<SchoolProduct> createSchoolProductList(Map<String, Course_Extra_Fee__c> products, Map<String, list<String>> campuses, String language){
    	
    	Map<String, SchoolProduct> msp = new Map<String, SchoolProduct>();
    	Map<String, String> translations = new Map<String, String>();
    	try {
	    	Translation__c tr = [select id, campus_course__c, course_category__c, course_form_of_delivery__c, course_language_level_required__c, course_name__c, course_period__c, course_type__c, course_field__c, 
												type__c, lb_unit_day__c, lb_unit_month__c, lb_unit_per__c, lb_unit_quarter__c, lb_unit_semester__c, lb_unit_subjects__c, lb_unit_week__c, lb_unit_year__c, Extra_Fees__c 
											from Translation__c where type__c = 'label' and Language__c = :language LIMIT 1];
											
			if(tr.Extra_Fees__c != null){
				for(String fee : tr.Extra_Fees__c.split('!#!')){
					String[] f = fee.split('>');
					translations.put(f[0], f[1]);
				}
			}								
    	} catch (Exception e) { 
    		// no translations found, carry on
    	}								
										
    	for(Course_Extra_Fee__c cef : products.values()){
    		list<string> listCampusCourse = new list<string>();
    		if(cef.campus_course__c != null)
    			listCampusCourse.add(cef.campus_course__c);
    		else listCampusCourse.addAll(campuses.get(cef.campus__c));
    		
    		for(string cc:listCampusCourse){
	    		if(msp.containsKey(cc)){
	    			msp.get(cc).extraFees.add(createExtraFee(cef, translations));
	    		} else {
		    		SchoolProduct sp = new SchoolProduct();
		    		sp.campusCourseId = cc;
		    		sp.extraFees = new List<ExtraFee>{createExtraFee(cef, translations)};
		    		msp.put(cc, sp);
	    		}    	
    		}	
    	}
    	return msp.values();
    }
    
    private ExtraFee createExtraFee(Course_Extra_Fee__c cef, Map<String, String> translations){
    	
    	ExtraFee ef = new ExtraFee();
    	ef.name = translations.containsKey(cef.product__r.Website_Fee_Type__c) ? translations.get(cef.product__r.Website_Fee_Type__c) : cef.product__r.Website_Fee_Type__c;
    	ef.units = cef.from__c;
    	ef.pricePerUnit = cef.value__c;
    	ef.allowChangeUnits =  cef.Allow_Change_Units__c;
    	ef.totalPrice = ef.allowChangeUnits ? ef.units * ef.pricePerUnit : ef.pricePerUnit;
    	ef.unitType = cef.ExtraFeeInterval__c;
    	ef.type = cef.product__r.Website_Fee_Type__c;
    	
    	ef.relatedFees = new List<ExtraFee>();
    	    	
    	for(Course_Extra_Fee_Dependent__c related : cef.Course_Extra_Fees_Dependent__r)
    		ef.relatedFees.add(createRelatedExtraFee(related));
    	
    	
    	return ef;
    		
    }
    
    private ExtraFee createRelatedExtraFee(Course_Extra_Fee_Dependent__c relatedFee) {
    	
    	ExtraFee ef = new ExtraFee();
    	ef.name = relatedFee.product__r.Name__c;
    	ef.units = relatedFee.from__c;
    	ef.pricePerUnit = relatedFee.value__c;
    	ef.allowChangeUnits =  relatedFee.Allow_Change_Units__c;
    	ef.totalPrice = ef.allowChangeUnits ? ef.units * ef.pricePerUnit : ef.pricePerUnit;
    	ef.unitType = relatedFee.Extra_Fee_Interval__c;
    	return ef;
    	
    }
    
    private Map<String, Course_Extra_Fee__c> processFees(Map<String, Course_Extra_Fee__c> products, List<Course_Extra_Fee__c> fees, Map<String, String> nationalities){
    	
    	List<Course_Extra_Fee__c> nationalityFees = new List<Course_Extra_Fee__c>();
    	
    	for(Course_Extra_fee__c cef : fees){
    		String schoolid = cef.campus_course__c != null ? cef.campus_course__r.campus__r.parentid : cef.campus__r.parentid;
    		if(cef.Nationality__c == nationalities.get(schoolid)){    		
    			nationalityFees.add(cef);
    		} else {
    			if(products.containsKey(cef.Product__c)){
    				Course_Extra_Fee__c cefMap = products.get(cef.Product__c);
    				if(cef.Availability__c < cefMap.availability__c || cef.From__c < cefMap.From__c)
    					products.put(cef.Product__c, cef);
    			} else
    				products.put(cef.Product__c, cef);
    		}

    	}
    	    	
    	for(Course_Extra_fee__c cef : nationalityFees)
    		products.put(cef.Product__c, cef);
    		
    	return products;
    		
    }
    
    
    private List<Course_Extra_Fee__c> getWebsiteExtraFees(String feeType, boolean isCampusFees, Set<String> filterids, Date paymentDate, double location){
    	
    	double onshore_offshore = api_manager.LOCATION_ONSHORE_OFFSHORE;
    	
    	String relatedFeesSql = '(Select Product__c, Product__r.name__c, Value__c, Allow_Change_Units__c, From__c, Extra_Fee_Interval__c ' +
    						  	' from Course_Extra_Fees_Dependent__r where isDeleted = false ';
    						  
    	relatedFeesSql += ' and Date_Paid_From__c <= :paymentDate AND Date_Paid_To__c >= :paymentDate ';
    	relatedFeesSql += ' and Optional__c = false ';
		relatedFeesSql += ' order by Product__r.Name, from__c ) ';
			
			
    	String sql = 'Select Availability__c, Allow_change_units__c, Campus__c, Campus_Course__c, campus_course__r.campus__r.parentid,  date_paid_from__c, date_paid_to__c, From__c, Nationality__c, Product__c, Value__c,';
    	sql += ' ExtraFeeInterval__c, product__r.Website_Fee_Type__c, Product__r.Name__c, campus__r.parentid, ';
    	sql += relatedFeesSql; 
		sql += ' from Course_Extra_Fee__c WHERE Product__r.Product_Type__c = \'' + feeType + '\' AND Product__r.Website_Fee_Type__c != null ';    	
    	sql += ' and date_paid_from__c <= :paymentDate and date_paid_to__c >= :paymentDate ';
    	sql += ' and (Availability__c = :onshore_offshore or Availability__c = :location) ';
    	
    	if(isCampusFees)
    		sql += ' and Campus__c in :filterids ';
    	else
    		sql += ' and Campus_Course__c in :filterids ';
			
		system.debug('@ filterids: ' + filterids);
		
    	return Database.query(sql);
    	
    }
    
}