public with sharing class client_course_cancel_followup {

	public String requests {get;set;}
	public String SEPARATOR_FILE {get{return Fileupload.SEPARATOR_FILE;}set;} 
	public String SEPARATOR_URL {get{return Fileupload.SEPARATOR_URL;}set;} 
	public String notesSplit {get{return client_course_cancel.notesSplit;}set;} 
	public String noteFld {get{return client_course_cancel.noteFld;}set;} 
	public String typeDraft {get{return client_course_cancel.TYPE_DRAFT;}set;} 
	public String typeMissDocs {get{return client_course_cancel.TYPE_MISS_DOCS;}set;}
	public String typeCourseTransferred {get{return client_course_cancel.TYPE_COURSE_TRANSFERRED;}set;}
	public String typeCourseRefunded {get{return client_course_cancel.TYPE_COURSE_REFUNDED;}set;}
	public String typeCourseRelease {get{return client_course_cancel.TYPE_COURSE_REFUND_CONFIRMED;}set;}

	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}
	
	public client_course_cancel_followup() {
		findGroups();
		findAgencies();
		findDepartments();
		findSchools();
		if(selGroup!=null)
			requests = findRequests('pending', selGroup, selAgency, seldept, 'all', '');
		else{
			requests = JSON.serialize(new client_course_cancel.requestsWrapper());
		}
	}

	private static User user {get;set;}
	public static String fAgencies {get;set;}
	public static String fGroup {get;set;}
	public static String fdept {get;set;}
	public static String fSchools {get;set;}
	public static String selGroup {get;set;}
	public static String selAgency {get;set;}
	public static String seldept {get;set;}
	private static void findGroups(){
		user = [Select Id, Aditional_Agency_Managment__c, Contact.Department__c, Contact.Account.ParentId, Contact.Account.Global_Link__c, Contact.AccountId FROM User where Id = :UserInfo.getUserId()];

		list<IPFunctions.opWrapper> lgroup = new list<IPFunctions.opWrapper>();
		String sql = 'SELECT Client__r.Owner__r.Contact.Account.ParentId gpId, Client__r.Owner__r.Contact.Account.Parent.Name gpName FROM client_course__c WHERE isCancelRequest__c = true and Cancel_Request_Confirmed_to_Client__c = false';

		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
		 	sql+= ' AND Client__r.Owner__r.Contact.Account.ParentId = \'' + user.Contact.Account.ParentId + '\'';
		else
			 sql+= ' AND Client__r.Owner__r.Contact.Account.Global_Link__c = \'' + user.Contact.Account.Global_Link__c + '\'';

		sql+= ' AND isCancelled__c = false ';

		sql += 'group by Client__r.Owner__r.Contact.Account.ParentId, Client__r.Owner__r.Contact.Account.Parent.Name order by Client__r.Owner__r.Contact.Account.Parent.Name';

		for(AggregateResult ar : Database.query(sql)){
			lgroup.add(new IPFunctions.opWrapper((String) ar.get('gpId'), (String) ar.get('gpName')));
			if((String) ar.get('gpId') == user.Contact.Account.ParentId)
				selGroup = (String) ar.get('gpId');
		}//end for

		if(selGroup == null && lgroup.size()>0)
			selGroup = lgroup[0].opId;
		fGroup = JSON.serialize(lgroup);
	}
	private static void findAgencies(){
		if(user == null) 
			user = [Select Id, Aditional_Agency_Managment__c, Contact.Department__c, Contact.Account.ParentId, Contact.Account.Global_Link__c, Contact.AccountId FROM User where Id = :UserInfo.getUserId()];

		list<IPFunctions.opWrapper> lagency = new list<IPFunctions.opWrapper>();

		if(selGroup != null){
			String sql = 'SELECT Client__r.Owner__r.Contact.AccountId gpId, Client__r.Owner__r.Contact.Account.Name gpName FROM client_course__c WHERE isCancelRequest__c = true and Cancel_Request_Confirmed_to_Client__c = false';

			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){
				list<string> agManagment = new list<string>{user.Contact.AccountId};
				 if(user.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(user.Aditional_Agency_Managment__c)){
						agManagment.add(ac.id);
					}
				}
				sql+= ' AND Client__r.Owner__r.Contact.AccountId  in :agManagment ';
				//sql+= ' AND Client__r.Owner__r.Contact.AccountId  = \'' + user.Contact.AccountId + '\'';
			}
			else
				sql+= ' AND Client__r.Owner__r.Contact.Account.ParentId = \'' + selGroup + '\'';
		
		
			sql+= ' AND isCancelled__c = false ';

		
			sql += 'group by Client__r.Owner__r.Contact.AccountId, Client__r.Owner__r.Contact.Account.Name order by Client__r.Owner__r.Contact.Account.Name';

			system.debug('sql agency==> ' + sql);	
	
			for(AggregateResult ar : Database.query(sql)){
				lagency.add(new IPFunctions.opWrapper((String) ar.get('gpId'), (String) ar.get('gpName')));
				if((String) ar.get('gpId') == user.Contact.AccountId)
					selAgency = (String) ar.get('gpId');
			}//end for

			if(selAgency == null && lagency.size()>0)
				selAgency = lagency[0].opId;
			fAgencies = JSON.serialize(lagency);
		}
		else{
			fAgencies = JSON.serialize(lagency);
		}
	}
	private static void findDepartments(){
		if(user == null) 
			user = [Select Id, Aditional_Agency_Managment__c, Contact.Department__c, Contact.Account.ParentId, Contact.Account.Global_Link__c, Contact.AccountId FROM User where Id = :UserInfo.getUserId()];

		list<IPFunctions.opWrapper> lagency = new list<IPFunctions.opWrapper>();

		if(selAgency != null){
			lagency.add(new IPFunctions.opWrapper('all', '-- All --'));
			String sql = 'SELECT Client__r.Owner__r.Contact.Department__c gpId, Client__r.Owner__r.Contact.Department__r.Name gpName FROM client_course__c WHERE isCancelRequest__c = true and Cancel_Request_Confirmed_to_Client__c = false';

			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){
				list<string> agManagment = new list<string>{user.Contact.AccountId};
				 if(user.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(user.Aditional_Agency_Managment__c)){
						agManagment.add(ac.id);
					}
				}
				sql+= ' AND Client__r.Owner__r.Contact.AccountId  in :agManagment ';
				//sql+= ' AND Client__r.Owner__r.Contact.AccountId = \'' + user.Contact.AccountId + '\'';
			}
			else
				sql+= ' AND Client__r.Owner__r.Contact.AccountId = \'' + selAgency + '\'';

			sql+= ' AND isCancelled__c = false ';

			sql += 'group by Client__r.Owner__r.Contact.Department__c, Client__r.Owner__r.Contact.Department__r.Name';
				

			for(AggregateResult ar : Database.query(sql)){
				lagency.add(new IPFunctions.opWrapper((String) ar.get('gpId'), (String) ar.get('gpName')));
				if((String) ar.get('gpId') == user.Contact.Department__c)
					seldept = (String) ar.get('gpId');
			}//end for
			
			if(seldept == null && lagency.size()>0)
				seldept = lagency[0].opId;
			fdept = JSON.serialize(lagency);
		}
		else{
			fdept = JSON.serialize(lagency);
		}
	}
	private static void findSchools(){
		if(user == null) 
			user = [Select Id, Aditional_Agency_Managment__c, Contact.Department__c, Contact.Account.ParentId, Contact.Account.Global_Link__c, Contact.AccountId FROM User where Id = :UserInfo.getUserId()];

		list<IPFunctions.opWrapper> lagency = new list<IPFunctions.opWrapper>();
		if(selAgency != null){
			String sql = 'SELECT School_Id__c sch FROM client_course__c WHERE isCancelRequest__c = true and Cancel_Request_Confirmed_to_Client__c = false';

			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){
				list<string> agManagment = new list<string>{user.Contact.AccountId};
				 if(user.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(user.Aditional_Agency_Managment__c)){
						agManagment.add(ac.id);
					}
				}
				sql+= ' AND Client__r.Owner__r.Contact.AccountId  in :agManagment ';
				//sql+= ' AND Client__r.Owner__r.Contact.AccountId = \'' + user.Contact.AccountId + '\'';
			}
			else
				sql+= ' AND Client__r.Owner__r.Contact.AccountId = \'' + selAgency + '\'';

			sql+= ' AND isCancelled__c = false ';

			if(seldept != null && seldept != 'all')
				sql+= ' AND Client__r.Owner__r.Contact.Department__c = \'' + seldept + '\'';

			sql += 'group by School_Id__c';
				
			list<String> ids = new list<String>();
			for(AggregateResult ar : Database.query(sql)){
				ids.add((String) ar.get('sch'));
			}//end for

			nSharing ns = new nSharing();
			for(Account ac : ns.findSchools(ids))
				lagency.add(new IPFunctions.opWrapper(ac.Id, ac.Name));
			
			if(lagency.size()>0)
				lagency.add(0,new IPFunctions.opWrapper('all', '-- All --'));

			fSchools = JSON.serialize(lagency);
		}
		else{
			fSchools = JSON.serialize(lagency);
		}
	}

	@RemoteAction
	public static String finishRequest(String courseId, String tab, String groupId, String agencyId, String deptId, String schId, String clientNm){
		try{
			update new client_course__c(Id = courseId, isCancel_Finalised__c = true);
			return findRequests(tab, groupId, agencyId, deptId, schId, clientNm);
		}catch(Exception e){
			system.debug('error line==' + e.getLineNumber());
			return 'failed';
		}
	}

	@RemoteAction	
	public static String changeGroup(String groupId){
		selGroup = groupId;
		findAgencies();

		return fAgencies;
	}
	@RemoteAction
	public static String changeAgency(String agencyId){
		selAgency = agencyId;
		findDepartments();

		return fdept;
	}
	@RemoteAction
	public static String changeDepartment(String agencyId, String deptId){
		selAgency = agencyId;
		seldept = deptId;
		findSchools();

		return fSchools;
	}

	@RemoteAction
	public static String requestToADM(String cId, String note, String tab, String groupId, String agencyId, String deptId, String schId, String clientNm){
		String result = client_course_cancel.requestToADM(cId, note); 
		system.debug('===>>' + result);
		if(result == 'success'){
			return findRequests(tab, groupId, agencyId, deptId, schId, clientNm);
		}
		else
			return result;
	}

	@RemoteAction
	public static String findRequests(String tab, String groupId, String agencyId, String deptId, String schId, String clientNm){

		Boolean allowUpload = true;

		if(user == null) 
			user = [Select Id, Aditional_Agency_Managment__c, Contact.Account.Global_Link__c, Contact.AccountId, Contact.Account.ParentId FROM User where Id = :UserInfo.getUserId()];
		
		list<client_course_cancel.courseInfo> result = new list<client_course_cancel.courseInfo>();

		String sql ='SELECT Cancel_Course_Status__c, Client__r.Owner__r.Contact.Account.ParentId, Client__r.Owner__r.Contact.Account.Name, Course_Cancellation_Service__c, Cancel_Notes__c, Credit_Reason__c, Credit_Courses__c, previewLink__c, Enrolment_Activities__c, Credit_Description__c, Client__c, Client__r.Name, Client__r.Age__c,  Client__r.Status__c, Client__r.Visa_Expiry_Date__c, Client__r.Owner__r.Name, Enroled_by_User__r.Name, Enroled_by_Agency__r.Name, CreatedDate, Release_Letter_Grant__c, (Select Id, isPackage__c, course_package__c, School_Name__c, Campus_Name__c, Course_Name__c, Start_Date__c, End_Date__c From client_courses__r order by Start_Date__c) FROM client_course__c WHERE ';

		String sWhere;
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
			sWhere = 'Client__r.Owner__r.Contact.Account.ParentId = \'' + user.Contact.Account.ParentId + '\'';
		else
			sWhere = 'Client__r.Owner__r.Contact.Account.ParentId = \'' + groupId + '\'';

		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){
			list<string> agManagment = new list<string>{user.Contact.AccountId};
			if(user.Aditional_Agency_Managment__c != null){
				for(Account ac:ipFunctions.listAgencyManagment(user.Aditional_Agency_Managment__c)){
					agManagment.add(ac.id);
				}
			}
			sWhere += ' AND Client__r.Owner__r.Contact.AccountId  in :agManagment ';
			//sWhere += ' AND Client__r.Owner__r.Contact.AccountId = \'' + user.Contact.AccountId + '\'';
		}else
			sWhere += ' AND Client__r.Owner__r.Contact.AccountId = \'' + agencyId + '\'';

		if(deptId != 'all')
			sWhere += ' AND Client__r.Owner__r.Contact.Department__c = \'' + deptId + '\'';
		
		if(schId != 'all' && schId != '')
			sWhere += ' AND School_Id__c = \'' + schId + '\'';
		
		if(clientNm.trim().length()>0)
			sWhere += ' AND Client__r.Name LIKE \'%' + clientNm.trim() + '%\'';

		sWhere += ' AND isCancelRequest__c = true';

		sql += sWhere;

		String pend = ' AND (Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_DRAFT + '\' OR Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_REQUESTED_ADM + '\' OR Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_MISS_DOCS + '\')'; 

		String sch = ' AND ( Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_REQUESTED_SCH + '\' OR Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_RELEASE_LETTER_REQUESTED + '\' )';
		
		// String transfer = ' AND (Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_COURSE_TRANSFER_REQUESTED + '\' OR (Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_COURSE_TRANSFERRED + '\' AND Cancel_Request_Confirmed_to_Client__c = false))';
		String transfer = ' AND (Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_COURSE_TRANSFER_REQUESTED + '\' )';
		
		// String refund = ' AND (Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_COURSE_REFUND_REQUESTED + '\' OR (Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_COURSE_REFUNDED + '\' AND Cancel_Request_Confirmed_to_Client__c = false))';
		String refund = ' AND (Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_COURSE_REFUND_REQUESTED + '\' OR Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_COURSE_REFUND_CONFIRMED + '\' )';

		String outcome = ' AND (Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_COURSE_TRANSFERRED + '\' OR Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_COURSE_REFUNDED + '\' OR Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_COURSE_CANCELLED + '\'  OR Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_UNDO + '\' OR Cancel_Course_Status__c = \'' + client_course_cancel.TYPE_COURSE_REFUND_CONFIRMED + '\') AND isCancel_Finalised__c = false';
		
		if(tab == 'pending'){
			// allowUpload = true;
			sql += pend;
		}
		else if(tab == 'reqSchool')
			sql += sch;
		else if(tab == 'transfer')
			sql += transfer;
		else if(tab == 'refund')
			sql += refund;
		else if(tab == 'outcome')
			sql += outcome;


		sql +=' ORDER BY createdDate ';

		system.debug('sql==>' + sql); 

		for(client_course__c c : Database.query(sql)){
			client_course_cancel.courseInfo cinfo = new client_course_cancel.courseInfo(c, IPFunctions.loadNotes(c.Cancel_Notes__c), IPFunctions.loadActivities(c.Enrolment_Activities__c), user.Contact.Account.Global_Link__c, allowUpload);
			cinfo.relatedCourses = IPFunctions.loadPreviewLink(c.Credit_Courses__c);
			result.add(cinfo);
		}//end for

		Integer totPending;
		Integer totSchool;
		Integer totTransfer;
		Integer totRefund;
		Integer totOutcome;
		
		//Tot Pending
		system.debug('tot==>' + 'SELECT count(Id) total FROM client_course__c WHERE ' + sWhere + ' ' + pend);
		for(AggregateResult ar : Database.query('SELECT count(Id) total FROM client_course__c WHERE ' + sWhere + ' ' + pend)){
			totPending = Integer.valueOf(ar.get('total'));
		}
		
		//Tot School
		system.debug('tot==>' + 'SELECT count(Id) total FROM client_course__c WHERE ' + sWhere + ' ' + sch);
		for(AggregateResult ar : Database.query('SELECT count(Id) total FROM client_course__c WHERE ' + sWhere + ' ' + sch)){
			totSchool = Integer.valueOf(ar.get('total'));
		}

		//Tot Transfer
		system.debug('tot==>' + 'SELECT count(Id) total FROM client_course__c WHERE ' + sWhere + ' ' + transfer);
		for(AggregateResult ar : Database.query('SELECT count(Id) total FROM client_course__c WHERE ' + sWhere + ' ' + transfer)){
			totTransfer = Integer.valueOf(ar.get('total'));
		}
		
		//Tot Refund
		system.debug('tot==>' + 'SELECT count(Id) total FROM client_course__c WHERE ' + sWhere + ' ' + refund);
		for(AggregateResult ar : Database.query('SELECT count(Id) total FROM client_course__c WHERE ' + sWhere + ' ' + refund)){
			totRefund = Integer.valueOf(ar.get('total'));
		}

		//Tot Release Letter
		system.debug('tot==>' + 'SELECT count(Id) total FROM client_course__c WHERE ' + sWhere + ' ' + outcome);
		for(AggregateResult ar : Database.query('SELECT count(Id) total FROM client_course__c WHERE ' + sWhere + ' ' + outcome)){
			totOutcome = Integer.valueOf(ar.get('total'));
		}

		return JSON.serialize(new client_course_cancel.requestsWrapper(result, totPending, totSchool, totTransfer, totRefund, totOutcome));
	}
	
	@RemoteAction
	public static String addNote(String cId, String note, String tab){
		return client_course_cancel.addNote(cId, note);
	}

	@RemoteAction
	public static String undoRequest(String cId, String note, String tab, String groupId, String agencyId, String deptId, String schId, String clientNm){
		String result = client_course_cancel.undoRequest(cId, note); 
		system.debug('===>>' + result);
		if(result == 'success'){
			return findRequests(tab, groupId, agencyId, deptId, schId, clientNm);
		}
		else
			return result;
	}

	public without sharing class nSharing{

		public list<Account> findSchools(list<String> ids){
			return [SELECT Id, Name FROM Account WHERE Id in :ids order by Name];
		}
	}
}