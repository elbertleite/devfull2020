@isTest
private class report_tasks_visa_application_test {
	
	@isTest static void test_tasks_report() {
		TestFactory tf = new TestFactory();
        
		Account agencyGroup = tf.createAgencyGroup();				
		Account agency = tf.createSimpleAgency(agencyGroup);				
		Contact employee = tf.createEmployee(agency);
		User portalUser = tf.createPortalUser(employee);
		Contact client = tf.createClient(agency);
		Contact client2 = tf.createClient(agency);
		Contact client3 = tf.createClient(agency);
		Contact client4 = tf.createClient(agency);
		Contact client5 = tf.createClient(agency);
		Contact client6 = tf.createClient(agency);
		Contact client7 = tf.createClient(agency);
		Contact client8 = tf.createClient(agency);

		client.current_Agency__c = agency.id;
		client2.current_Agency__c = agency.id;
		client3.current_Agency__c = agency.id;
		client4.current_Agency__c = agency.id;
		client5.current_Agency__c = agency.id;
		client6.current_Agency__c = agency.id;
		client7.current_Agency__c = agency.id;
		client8.current_Agency__c = agency.id;


		update client;
		update client2;
		update client3;
		update client4;
		update client5;
		update client6;
		update client7;
		update client8;


		Destination_Tracking__c dt = new Destination_tracking__c();
		dt.Client__c = client.id;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
		dt.Expected_Travel_Date__c = system.today().addMonths(3);
		dt.lead_Cycle__c = false;
		insert dt;

		Client_Course_Overview__c co = new Client_Course_Overview__c();
		co.Course_Start_Date__c = system.today();
		co.Course_End_Date__c = system.today().addDays(7);
		co.Client__c = client.id;
		co.Destination_Tracking__c = dt.id;
		insert co;

		co = new Client_Course_Overview__c();
		co.Course_Start_Date__c = system.today();
		co.Course_End_Date__c = system.today().addDays(7);
		co.Client__c = client2.id;
		co.Destination_Tracking__c = dt.id;
		insert co;

		co = new Client_Course_Overview__c();
		co.Course_Start_Date__c = system.today();
		co.Course_End_Date__c = system.today().addDays(7);
		co.Client__c = client3.id;
		co.Destination_Tracking__c = dt.id;
		insert co;

		co = new Client_Course_Overview__c();
		co.Course_Start_Date__c = system.today();
		co.Course_End_Date__c = system.today().addDays(7);
		co.Client__c = client4.id;
		co.Destination_Tracking__c = dt.id;
		insert co;

		co = new Client_Course_Overview__c(); 
		co.Course_Start_Date__c = system.today();
		co.Course_End_Date__c = system.today().addDays(7);
		co.Client__c = client5.id;
		co.Destination_Tracking__c = dt.id;
		insert co;

		co = new Client_Course_Overview__c();
		co.Course_Start_Date__c = system.today();
		co.Course_End_Date__c = system.today().addDays(7);
		co.Client__c = client6.id;
		co.Destination_Tracking__c = dt.id;
		insert co;

		co = new Client_Course_Overview__c();
		co.Course_Start_Date__c = system.today();
		co.Course_End_Date__c = system.today().addDays(7);
		co.Client__c = client7.id;
		co.Destination_Tracking__c = dt.id;
		insert co;

		co = new Client_Course_Overview__c();
		co.Course_Start_Date__c = system.today();
		co.Course_End_Date__c = system.today().addDays(7);
		co.Client__c = client8.id;
		co.Destination_Tracking__c = dt.id;
		insert co;

		
		Custom_Note_Task__c t = new Custom_Note_Task__c();
		t.Comments__c='bla bla bla';
		t.Subject__c = 'BSB Profile Analyze';
		t.Due_Date__c = system.today();
		t.Assign_To__c =  portalUser.id;
		t.AssignedOn__c = system.today();
		t.AssignedBy__c = portalUser.id;
		t.Status__c = 'In Progress';
		t.Selected__c = true;
		t.related_Contact__c = client.id;
		insert t;
		
		t = new Custom_Note_Task__c();
		t.Comments__c='bla bla bla';
		t.Subject__c = 'BSB Profile Risk';
		t.Due_Date__c = system.today().addDays(3);
		t.Assign_To__c =  portalUser.id;
		t.AssignedOn__c = system.today();
		t.AssignedBy__c = portalUser.id;
		t.Status__c = 'Completed';
		t.Selected__c = true;
		t.related_Contact__c = client.id;
		insert t;

		t = new Custom_Note_Task__c();
		t.Comments__c='bla bla bla';
		t.Subject__c = 'BSB Profile Risk';
		t.Due_Date__c = system.today().addDays(3);
		t.Assign_To__c =  portalUser.id;
		t.AssignedOn__c = system.today();
		t.AssignedBy__c = portalUser.id;
		t.Status__c = 'Completed';
		t.Selected__c = true;
		t.related_Contact__c = client4.id;
		insert t;

		t = new Custom_Note_Task__c();
		t.Comments__c='bla bla bla';
		t.Subject__c = 'BSB Documents Analyze';
		t.Due_Date__c = system.today();
		t.Assign_To__c =  portalUser.id;
		t.AssignedOn__c = system.today();
		t.AssignedBy__c = portalUser.id;
		t.Status__c = 'In Progress';
		t.Selected__c = true;
		t.related_Contact__c = client.id;
		insert t;
		
		t = new Custom_Note_Task__c();
		t.Comments__c='bla bla bla';
		t.Subject__c = 'BSB Letter Review';
		t.Due_Date__c = system.today().addDays(3);
		t.Assign_To__c =  portalUser.id;
		t.AssignedOn__c = system.today();
		t.AssignedBy__c = portalUser.id;
		t.Status__c = 'Completed';
		t.Selected__c = true;
		t.related_Contact__c = client.id;
		insert t;

		t = new Custom_Note_Task__c();
		t.Comments__c='bla bla bla';
		t.Subject__c = 'BSB Letter Review';
		t.Due_Date__c = system.today().addDays(3);
		t.Assign_To__c =  portalUser.id;
		t.AssignedOn__c = system.today();
		t.AssignedBy__c = portalUser.id;
		t.Status__c = 'Completed';
		t.Selected__c = true;
		t.related_Contact__c = client5.id;
		insert t;

		t = new Custom_Note_Task__c();
		t.Comments__c='bla bla bla';
		t.Subject__c = 'Visa Application';
		t.Due_Date__c = system.today();
		t.Assign_To__c =  portalUser.id;
		t.AssignedOn__c = system.today();
		t.AssignedBy__c = portalUser.id;
		t.Status__c = 'In Progress';
		t.Selected__c = true;
		t.related_Contact__c = client.id;
		insert t;
		
		t = new Custom_Note_Task__c();
		t.Comments__c='bla bla bla';
		t.Subject__c = 'Visa Confirmation';
		t.Due_Date__c = system.today().addDays(3);
		t.Assign_To__c =  portalUser.id;
		t.AssignedOn__c = system.today();
		t.AssignedBy__c = portalUser.id;
		t.Status__c = 'Completed';
		t.Selected__c = true;
		t.related_Contact__c = client.id;
		insert t;

		t = new Custom_Note_Task__c();
		t.Comments__c='bla bla bla';
		t.Subject__c = 'Visa Confirmation';
		t.Due_Date__c = system.today().addDays(3);
		t.Assign_To__c =  portalUser.id;
		t.AssignedOn__c = system.today();
		t.AssignedBy__c = portalUser.id;
		t.Status__c = 'Completed';
		t.Selected__c = true;
		t.related_Contact__c = client7.id;
		insert t;


		
		t = new Custom_Note_Task__c();
		t.Comments__c='bla bla bla';
		t.Subject__c = 'Visa Application';
		t.Due_Date__c = system.today();
		t.Assign_To__c =  portalUser.id;
		t.AssignedOn__c = system.today();
		t.AssignedBy__c = portalUser.id;
		t.Status__c = 'In Progress';
		t.Selected__c = true;
		t.related_Contact__c = client2.id;
		insert t;
		
		t = new Custom_Note_Task__c();
		t.Comments__c='bla bla bla';
		t.Subject__c = 'Visa Confirmation';
		t.Due_Date__c = system.today().addDays(3);
		t.Assign_To__c =  portalUser.id;
		t.AssignedOn__c = system.today();
		t.AssignedBy__c = portalUser.id;
		t.Status__c = 'Completed';
		t.Selected__c = true;
		t.related_Contact__c = client2.id;
		insert t;

		t = new Custom_Note_Task__c();
		t.Comments__c='bla bla bla';
		t.Subject__c = 'BSB Documents Analyze';
		t.Due_Date__c = system.today();
		t.Assign_To__c =  portalUser.id;
		t.AssignedOn__c = system.today();
		t.AssignedBy__c = portalUser.id;
		t.Status__c = 'In Progress';
		t.Selected__c = true;
		t.related_Contact__c = client3.id;
		insert t;
		
		t = new Custom_Note_Task__c();
		t.Comments__c='bla bla bla';
		t.Subject__c = 'BSB Letter Review';
		t.Due_Date__c = system.today().addDays(3);
		t.Assign_To__c =  portalUser.id;
		t.AssignedOn__c = system.today();
		t.AssignedBy__c = portalUser.id;
		t.Status__c = 'Completed';
		t.Selected__c = true;
		t.related_Contact__c = client3.id;
		insert t;
		

		t = new Custom_Note_Task__c();
		t.Comments__c='bla bla bla';
		t.Subject__c = 'BSB Full Service';
		t.Due_Date__c = system.today().addDays(3);
		t.Assign_To__c =  portalUser.id;
		t.AssignedOn__c = system.today();
		t.AssignedBy__c = portalUser.id;
		t.Status__c = 'Completed';
		t.Selected__c = true;
		t.related_Contact__c = client8.id;
		insert t;
		
		t = new Custom_Note_Task__c();
		t.Comments__c='bla bla bla';
		t.Subject__c = 'BSB Full Service';
		t.Due_Date__c = system.today().addDays(3);
		t.Assign_To__c =  portalUser.id;
		t.AssignedOn__c = system.today();
		t.AssignedBy__c = portalUser.id;
		t.Status__c = 'Completed';
		t.Selected__c = true;
		t.related_Contact__c = client8.id;
		insert t;
		
        
        Test.startTest();
        
        system.runAs(portalUser){
        	
        
		
		report_tasks_visa_application report = new report_tasks_visa_application();
		string fromDate = system.today().addDays(-30).format();
		string toDate = system.today().addDays(30).format();

		//ApexPages.CurrentPage().getParameters().put('agencyId',agency.id);
		ApexPages.CurrentPage().getParameters().put('groupId',agencyGroup.id);
		ApexPages.CurrentPage().getParameters().put('dateFrom',fromDate);
		ApexPages.CurrentPage().getParameters().put('dateTo',toDate);
		ApexPages.CurrentPage().getParameters().put('destination','all');
		report.retrieveReportVisas();

		report_tasks_visa_application.changeDestination();
		report_tasks_visa_application.changeAccount();
		report_tasks_visa_application.changeAgency(agency.id);
		report_tasks_visa_application.retrieveClientTasks(client.Id, fromDate, toDate, agencyGroup.id);
        	
        
        }
        
        Test.stopTest();
        
    
	}
	
	
	
}