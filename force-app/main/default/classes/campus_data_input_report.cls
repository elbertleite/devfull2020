public with sharing class campus_data_input_report {
	account campus;
	public campus_data_input_report() {
		campus = [select id, parentId from account where id = :ApexPages.currentPage().getParameters().get('id')];
		retrieveNationalityGroups();
		retrieveCourses();
		retrieveExtraFees();
		retrieveExtraFeesCampus();
		retrievePromotions();
		retrievePromotionsFeeCourses();
		retrievePromotionsFeeCcampus();
	}

	public class priceDetails{
		public sObject itemValue{get; set;}
		public list<sObject> dateRange{get{if(dateRange == null) dateRange = new list<sObject>(); return dateRange;} set;}
		public list<priceDetails> relatedFee{get{if(relatedFee == null) relatedFee = new list<priceDetails>(); return relatedFee;} set;}
	}

	public class campusDetails{
		public string courseName{get; set;}
		public string courseId{get; set;}
		public map<string,boolean> nationGroup{get{if(nationGroup == null) nationGroup = new map<string,boolean>(); return nationGroup;} set;}
		public map<string,list<sObject>> priceNation{get{if(priceNation == null) priceNation = new map<string,list<sObject>>(); return priceNation;} set;}
		public map<string,list<priceDetails>> valueNation{get{if(valueNation == null) valueNation = new map<string,list<priceDetails>>(); return valueNation;} set;}
	}

	public static map<string, list<campusDetails>> courses{get{if(courses == null) courses = new map<string, list<campusDetails>>(); return courses;} set;}
	public static map<string, list<campusDetails>> fees{get{if(fees == null) fees = new map<string, list<campusDetails>>(); return fees;} set;}
	public static map<string, list<campusDetails>> feesCampus{get{if(feesCampus == null) feesCampus = new map<string, list<campusDetails>>(); return feesCampus;} set;}
	public static map<string, list<campusDetails>> promo{get{if(promo == null) promo = new map<string, list<campusDetails>>(); return promo;} set;}
	public static map<string, list<campusDetails>> promoFeeCourse{get{if(promoFeeCourse == null) promoFeeCourse = new map<string, list<campusDetails>>(); return promoFeeCourse;} set;}
	public static map<string, list<campusDetails>> promoFeeCampus{get{if(promoFeeCampus == null) promoFeeCampus = new map<string, list<campusDetails>>(); return promoFeeCampus;} set;}

	public map<string,boolean> nationalityGroups{get; set;}
	private void retrieveNationalityGroups(){
		nationalityGroups = new map<string,boolean>();
		nationalityGroups.put('Published Price', false); 
		for(AggregateResult ar:[Select name nt from Nationality_Group__c where account__c = :campus.ParentId group by name order by name])
			nationalityGroups.put((string)ar.get('nt'), false);
	}

	public Integer getNationalityGroupsSize(){
		return nationalityGroups.size();
	}
	
	
	// public void retrieveCourses(){
	// 	campusDetails cd;
	// 	for(campus_course__c cc:[Select course__c, course__r.name, Course__r.Course_Category__c, Course__r.Type__c, (Select id, Availability__c, Price_valid_from__c, Price_valid_until__c, From__c, Price_per_week__c, unit_description__c, Comments__c, Nationality__c from Course_Prices__r order by Nationality__c, Availability__c, Price_valid_from__c, From__c)  from campus_course__c where campus__c = :campus.Id and Is_Available__c = true order by Course__r.Course_Category__c, Course__r.Course_Type__c, Course__r.Type__c, Course__r.Name]){
	// 		cd = new campusDetails();
	// 		cd.courseName = cc.course__r.name;
	// 		cd.courseId = cc.course__c;
	// 		cd.nationGroup.putAll(nationalityGroups);
	// 		for(Course_Price__c cp:cc.Course_Prices__r){
	// 			cd.nationGroup.put(cp.Nationality__c, true);
	// 			if(!cd.priceNation.containsKey(cp.Nationality__c))
	// 				cd.priceNation.put(cp.Nationality__c, new list<Course_Price__c>{cp});
	// 			else cd.priceNation.get(cp.Nationality__c).add(cp);
	// 		}
	// 		if(!courses.containsKey(cc.Course__r.Type__c))
	// 			courses.put(cc.Course__r.Type__c, new list<campusDetails>{cd});
	// 		else courses.get(cc.Course__r.Type__c).add(cd);
	// 	}

	// }

	public void retrieveCourses(){
		campusDetails cd;
		priceDetails pd;
		priceDetails pdDependent;
		list<priceDetails> listpdDependent;
		map<string,campus_course__c> mpCampusCourse = new map<string,campus_course__c>([Select id from campus_course__c where campus__c = :campus.Id  and Is_Available__c = true]);
		map<string,Course_Price__c> listPrices = new map<string,Course_Price__c>([Select id, 
			(Select From_Date__c, To_Date__c, Value__c, Comments__c, CreatedBy.Name, CreatedDate, LastModifiedBy.Name, LastModifiedDate from Start_Date_Ranges__r order by From_Date__c, Value__c)
        	from Course_Price__c where 	Campus_Course__c in :mpCampusCourse.KeySet() order by Nationality__c, Availability__c, Price_valid_from__c, From__c]);

		for(campus_course__c cc:[Select course__c, course__r.name, Course__r.Course_Category__c, Course__r.Type__c, (Select id, Availability__c, Price_valid_from__c, Price_valid_until__c, From__c, Price_per_week__c, unit_description__c, Comments__c, Nationality__c from Course_Prices__r order by Nationality__c, Availability__c, Price_valid_from__c, From__c)  from campus_course__c where campus__c = :campus.Id  and Is_Available__c = true order by Course__r.Course_Category__c, Course__r.Course_Type__c, Course__r.Type__c, Course__r.Name]){
			cd = new campusDetails();
			cd.courseName = cc.course__r.name; 
			cd.courseId = cc.course__c;
			cd.nationGroup.putAll(nationalityGroups);
			for(Course_Price__c cp:cc.Course_Prices__r){
				cd.nationGroup.put(cp.Nationality__c, true);
				if(!cd.valueNation.containsKey(cp.Nationality__c)){
					pd = new priceDetails();
					pd.itemValue = cp;
					if(listPrices.containsKey(cp.id)){
						pd.dateRange = listPrices.get(cp.id).Start_Date_Ranges__r;
						
						pd.relatedFee = listpdDependent;
					}
					cd.valueNation.put(cp.Nationality__c, new list<priceDetails>{pd});
				}
				else {
					pd = new priceDetails();
					pd.itemValue = cp;
					if(listPrices.containsKey(cp.id)){
						pd.dateRange = listPrices.get(cp.id).Start_Date_Ranges__r;
						listpdDependent = new list<priceDetails>();
						
					}
					cd.valueNation.get(cp.Nationality__c).add(pd);
				}
			}
			if(!courses.containsKey(cc.Course__r.Type__c))
				courses.put(cc.Course__r.Type__c, new list<campusDetails>{cd});
			else courses.get(cc.Course__r.Type__c).add(cd);
		}

	}

	public void retrieveExtraFees(){
		campusDetails cd;
		priceDetails pd;
		priceDetails pdDependent;
		list<priceDetails> listpdDependent;
		map<string,campus_course__c> mpCampusCourse = new map<string,campus_course__c>([Select id from campus_course__c where campus__c = :campus.Id  and Is_Available__c = true]);
		map<string,Course_Extra_Fee__c> listFees = new map<string,Course_Extra_Fee__c>([Select id, 
			(Select Course_Extra_Fee__c, Product__r.Name__c, Allow_Change_Units__c, Date_Paid_From__c, Date_Paid_To__c, Date_Start_From__c, Date_Start_To__c, Details__c, From__c, Optional__c, Value__c, Keep_Fee__c, Extra_Fee_Interval__c from Course_Extra_Fees_Dependent__r), 
			(Select From_Date__c, To_Date__c, Value__c, Comments__c, CreatedBy.Name, CreatedDate, LastModifiedBy.Name, LastModifiedDate from Start_Date_Ranges__r order by From_Date__c, Value__c)
        	from Course_Extra_Fee__c where 	Campus_Course__c in :mpCampusCourse.KeySet() order by 	product__r.Product_Type__c, Extra_Fee_Name__c, Nationality__c, Availability__c, date_paid_from__c, From__c]);

        //Retrieve extra Fees Dependent Start Dates
		Set<String> relatedIDs = new Set<String>();
		for(Course_Extra_Fee__c st:listFees.values())
			for(Course_Extra_Fee_Dependent__c rf :st.Course_Extra_Fees_Dependent__r)
				relatedIDs.add(rf.id);
		Map<String, Course_Extra_Fee_Dependent__c> relatedStartDates;
        relatedStartDates = new Map<String, Course_Extra_Fee_Dependent__c>([Select id, (Select From_Date__c, To_Date__c, Value__c, Comments__c from Start_Date_Ranges__r order by From_Date__c, Value__c) from Course_Extra_Fee_Dependent__c where id in :relatedIDs]);
        

		for(campus_course__c cc:[Select course__c, course__r.name, Course__r.Course_Category__c, Course__r.Type__c, (Select id, product__c, product__r.Product_Type__c, Extra_Fee_Name__c, Optional__c, Campus__c, Availability__c, date_paid_from__c, date_paid_to__c, From__c, Value__c, Extra_info__c, Nationality__c from Course_Extra_Fees__r)  from campus_course__c where campus__c = :campus.Id  and Is_Available__c = true order by Course__r.Course_Category__c, Course__r.Course_Type__c, Course__r.Type__c, Course__r.Name]){
			cd = new campusDetails();
			cd.courseName = cc.course__r.name; 
			cd.courseId = cc.course__c;
			cd.nationGroup.putAll(nationalityGroups);
			for(Course_Extra_Fee__c cp:cc.Course_Extra_Fees__r){
				cd.nationGroup.put(cp.Nationality__c, true);
				if(!cd.valueNation.containsKey(cp.Nationality__c)){
					pd = new priceDetails();
					pd.itemValue = cp;
					if(listFees.containsKey(cp.id)){
						pd.dateRange = listFees.get(cp.id).Start_Date_Ranges__r;
						listpdDependent = new list<priceDetails>();
						for(Course_Extra_Fee_Dependent__c ced:listFees.get(cp.id).Course_Extra_Fees_Dependent__r){
							pdDependent = new priceDetails();
							pdDependent.itemValue = ced;
							if(relatedStartDates.containsKey(ced.id))
								pdDependent.dateRange = relatedStartDates.get(ced.id).Start_Date_Ranges__r;
							listpdDependent.add(pdDependent);
						}
						pd.relatedFee = listpdDependent;
					}
					cd.valueNation.put(cp.Nationality__c, new list<priceDetails>{pd});
				}
				else {
					pd = new priceDetails();
					pd.itemValue = cp;
					if(listFees.containsKey(cp.id)){
						pd.dateRange = listFees.get(cp.id).Start_Date_Ranges__r;
						listpdDependent = new list<priceDetails>();
						for(Course_Extra_Fee_Dependent__c ced:listFees.get(cp.id).Course_Extra_Fees_Dependent__r){
							pdDependent = new priceDetails();
							pdDependent.itemValue = ced;
							if(relatedStartDates.containsKey(ced.id))
								pdDependent.dateRange = relatedStartDates.get(ced.id).Start_Date_Ranges__r;
							listpdDependent.add(pdDependent);
						}
						pd.relatedFee = listpdDependent;
					}
					cd.valueNation.get(cp.Nationality__c).add(pd);
				}
			}
			if(!fees.containsKey(cc.Course__r.Type__c))
				fees.put(cc.Course__r.Type__c, new list<campusDetails>{cd});
			else fees.get(cc.Course__r.Type__c).add(cd);
		}

	}

	public void retrieveExtraFeesCampus(){
		campusDetails cd;
		priceDetails pd;
		set<string> feeType = new set<string>();
		string currentFeeType;
		integer count = 0;
		list<Course_Extra_Fee__c> listFees;
		string Product_Type;
		priceDetails pdDependent;
		list<priceDetails> listpdDependent;
		try{
		 	listFees = [Select id, product__c, product__r.Product_Type__c, Extra_Fee_Name__c, Campus__c, Availability__c, date_paid_from__c, date_paid_to__c, From__c, Value__c, Extra_info__c, Nationality__c, Optional__c,
			(Select Course_Extra_Fee__c, Product__r.Name__c, Allow_Change_Units__c, Date_Paid_From__c, Date_Paid_To__c, Date_Start_From__c, Date_Start_To__c, Details__c, From__c, Optional__c, Value__c, Keep_Fee__c, Extra_Fee_Interval__c from Course_Extra_Fees_Dependent__r), 
			(Select From_Date__c, To_Date__c, Value__c, Comments__c, CreatedBy.Name, CreatedDate, LastModifiedBy.Name, LastModifiedDate from Start_Date_Ranges__r order by From_Date__c, Value__c)
			from Course_Extra_Fee__c where campus__c = :campus.Id order by 	product__r.Product_Type__c, Extra_Fee_Name__c, Nationality__c, Availability__c, date_paid_from__c, From__c];
		}catch(Exception e){
			listFees = new list<Course_Extra_Fee__c>();
		}

		//Retrieve extra Fees Dependent Start Dates
		Set<String> relatedIDs = new Set<String>();
		for(Course_Extra_Fee__c st:listFees)
			for(Course_Extra_Fee_Dependent__c rf :st.Course_Extra_Fees_Dependent__r)
				relatedIDs.add(rf.id);
		Map<String, Course_Extra_Fee_Dependent__c> relatedStartDates;
        relatedStartDates = new Map<String, Course_Extra_Fee_Dependent__c>([Select id, (Select From_Date__c, To_Date__c, Value__c, Comments__c from Start_Date_Ranges__r order by From_Date__c, Value__c) from Course_Extra_Fee_Dependent__c where id in :relatedIDs]);
 

		for(Course_Extra_Fee__c cc:listFees){
			if(currentFeeType != null && currentFeeType != cc.Extra_Fee_Name__c){
				if(!feesCampus.containsKey(Product_Type))
					feesCampus.put(Product_Type, new list<campusDetails>{cd});
				else feesCampus.get(Product_Type).add(cd);
			}

			if(!feeType.contains(cc.Extra_Fee_Name__c)){
				cd = new campusDetails();
				cd.courseName = cc.Extra_Fee_Name__c;
				cd.courseId = cc.product__c;
				cd.nationGroup.putAll(nationalityGroups);
				feeType.add(cc.Extra_Fee_Name__c);
			}
			currentFeeType = cc.Extra_Fee_Name__c;
			Product_Type = cc.product__r.Product_Type__c;
			cd.nationGroup.put(cc.Nationality__c, true);
			if(!cd.valueNation.containsKey(cc.Nationality__c)){
				pd = new priceDetails();
				pd.itemValue = cc;
				pd.dateRange = cc.Start_Date_Ranges__r;
				listpdDependent = new list<priceDetails>();
				for(Course_Extra_Fee_Dependent__c ced:cc.Course_Extra_Fees_Dependent__r){
					pdDependent = new priceDetails();
					pdDependent.itemValue = ced;
					if(relatedStartDates.containsKey(ced.id))
						pdDependent.dateRange = relatedStartDates.get(ced.id).Start_Date_Ranges__r;
					listpdDependent.add(pdDependent);
				}
				pd.relatedFee = listpdDependent;
				cd.valueNation.put(cc.Nationality__c, new list<priceDetails>{pd});
			}
			else {
				pd = new priceDetails();
				pd.itemValue = cc;
				pd.dateRange = cc.Start_Date_Ranges__r;
				listpdDependent = new list<priceDetails>();
				for(Course_Extra_Fee_Dependent__c ced:cc.Course_Extra_Fees_Dependent__r){
					pdDependent = new priceDetails();
					pdDependent.itemValue = ced;
					if(relatedStartDates.containsKey(ced.id))
						pdDependent.dateRange = relatedStartDates.get(ced.id).Start_Date_Ranges__r;
					listpdDependent.add(pdDependent);
				}
				pd.relatedFee = listpdDependent;
				cd.valueNation.get(cc.Nationality__c).add(pd);
			}
			count++;
			if(count == listFees.size()){
				if(!feesCampus.containsKey(Product_Type))
					feesCampus.put(Product_Type, new list<campusDetails>{cd});
				else feesCampus.get(Product_Type).add(cd);
			}
		}

	}

	public void retrievePromotions(){
		campusDetails cd;
		priceDetails pd;
		list<priceDetails> listpdDependent;
		map<string,campus_course__c> mpCampusCourse = new map<string,campus_course__c>([Select id from campus_course__c where campus__c = :campus.Id  and Is_Available__c = true]);
		map<string,Deal__c> listPromo = new map<string,Deal__c>([Select id, 
			(Select From_Date__c, To_Date__c, Value__c, Comments__c, CreatedBy.Name, CreatedDate, LastModifiedBy.Name, LastModifiedDate from Start_Date_Ranges__r order by From_Date__c, Value__c)
        	from Deal__c where 	Campus_Course__c in :mpCampusCourse.KeySet() and Promotion_Type__c = 2 order by Nationality_Group__c, Availability__c, From_Date__c, From__c]);
		for(campus_course__c cc:[Select course__c, course__r.name, Course__r.Course_Category__c, Course__r.Type__c, (Select id, Promotion_Weeks__c, Number_of_Free_Weeks__c, Promotion_Name__c, Availability__c, From_Date__c, To_Date__c, From__c, Total_Price__c, Comments__c, Nationality_Group__c from promotions__r where Promotion_Type__c = 2 order by Nationality_Group__c, Availability__c, From_Date__c, From__c)  from campus_course__c where campus__c = :campus.Id and Is_Available__c = true order by Course__r.Course_Category__c, Course__r.Course_Type__c, Course__r.Type__c, Course__r.Name]){
			cd = new campusDetails();
			cd.courseName = cc.course__r.name;
			cd.courseId = cc.course__c;
			cd.nationGroup.putAll(nationalityGroups);
			for(Deal__c cp:cc.promotions__r){
				cd.nationGroup.put(cp.Nationality_Group__c, true);
				if(!cd.valueNation.containsKey(cp.Nationality_Group__c)){
					pd = new priceDetails();
					pd.itemValue = cp;
					if(listPromo.containsKey(cp.id)){
						pd.dateRange = listPromo.get(cp.id).Start_Date_Ranges__r;
						
						pd.relatedFee = listpdDependent;
					}
					cd.valueNation.put(cp.Nationality_Group__c, new list<priceDetails>{pd});
				}
				else {
					pd = new priceDetails();
					pd.itemValue = cp;
					if(listPromo.containsKey(cp.id)){
						pd.dateRange = listPromo.get(cp.id).Start_Date_Ranges__r;
						listpdDependent = new list<priceDetails>();
						
					}
					cd.valueNation.get(cp.Nationality_Group__c).add(pd);
				}
				
			}
			if(!promo.containsKey(cc.Course__r.Type__c))
				promo.put(cc.Course__r.Type__c, new list<campusDetails>{cd});
			else promo.get(cc.Course__r.Type__c).add(cd);
		}

	}

	public void retrievePromotionsFeeCourses(){
		campusDetails cd;
		priceDetails pd;
		set<string> PromoType = new set<string>();
		string currentPromoType;
		integer count = 0;
		list<deal__c> listPromotions;
		string Product_Type;
		
		try{
			listPromotions = [Select id, campus_course__r.course__c, campus_course__r.course__r.name, product__c, product__r.name__c, Extra_Fee_Value__c, Extra_Fee_Type__c, To__c, Promotion_Weeks__c, Number_of_Free_Weeks__c, Promotion_Name__c, Availability__c, From_Date__c, To_Date__c, From__c, Total_Price__c, Comments__c, Nationality_Group__c, Extra_Fee_Dependent__r.Course_Extra_Fee__r.product__r.Name__c,
			(Select Promotion_Name__c, From_Date__c, To_Date__c, Value__c, Comments__c, CreatedBy.Name, CreatedDate, LastModifiedBy.Name, LastModifiedDate from Start_Date_Ranges__r order by From_Date__c, Value__c)
			from deal__c where 	Campus_Course__r.campus__c = :campus.Id and Campus_Course__r.Is_Available__c = true and Promotion_Type__c = 3 order by product__r.name__c, campus_course__r.course__r.name, Nationality_Group__c, Availability__c, From_Date__c, From__c];
		}catch(Exception e){
			listPromotions = new list<deal__c>();
		}

		for(deal__c cc:listPromotions){
			if(currentPromoType != null && currentPromoType != cc.product__r.name__c+'-'+cc.campus_course__r.course__c){
				if(!promoFeeCourse.containsKey(Product_Type))
					promoFeeCourse.put(Product_Type, new list<campusDetails>{cd});
				else promoFeeCourse.get(Product_Type).add(cd);
			}

			if(!PromoType.contains(cc.product__r.name__c+'-'+cc.campus_course__r.course__c)){
				cd = new campusDetails();
				cd.courseName = cc.campus_course__r.course__r.name;
				cd.courseId = cc.product__c+'-'+cc.campus_course__r.course__c;
				cd.nationGroup.putAll(nationalityGroups);
				PromoType.add(cc.product__r.name__c+'-'+cc.campus_course__r.course__c);
			}
			currentPromoType = cc.product__r.name__c+'-'+cc.campus_course__r.course__c;
			Product_Type = cc.product__r.name__c;
			cd.nationGroup.put(cc.Nationality_Group__c, true);
			if(!cd.valueNation.containsKey(cc.Nationality_Group__c)){
				pd = new priceDetails();
				pd.itemValue = cc;
				pd.dateRange = cc.Start_Date_Ranges__r;
				
				cd.valueNation.put(cc.Nationality_Group__c, new list<priceDetails>{pd});
			}
			else {
				pd = new priceDetails();
				pd.itemValue = cc;
				pd.dateRange = cc.Start_Date_Ranges__r;
				
				cd.valueNation.get(cc.Nationality_Group__c).add(pd);
			}
			count++;
			if(count == listPromotions.size()){
				if(!promoFeeCourse.containsKey(Product_Type))
					promoFeeCourse.put(Product_Type, new list<campusDetails>{cd});
				else promoFeeCourse.get(Product_Type).add(cd);
			}
		}
	}

	public void retrievePromotionsFeeCcampus(){
		campusDetails cd;
		priceDetails pd;
		set<string> PromoType = new set<string>();
		string currentPromoType;
		integer count = 0;
		list<deal__c> listPromotions;
		string Product_Type;
		
		try{
			listPromotions = [Select id, product__r.Product_Type__c, product__c, product__r.name__c, Extra_Fee_Type__c, To__c, Extra_Fee_Value__c, Promotion_Weeks__c, Number_of_Free_Weeks__c, Promotion_Name__c, Availability__c, From_Date__c, To_Date__c, From__c, Total_Price__c, Comments__c, Nationality_Group__c, Extra_Fee_Dependent__r.Course_Extra_Fee__r.product__r.Name__c,
			(Select Promotion_Name__c, From_Date__c, To_Date__c, Value__c, Comments__c, CreatedBy.Name, CreatedDate, LastModifiedBy.Name, LastModifiedDate from Start_Date_Ranges__r order by From_Date__c, Value__c)
			from deal__c where 	Campus_Account__c = :campus.Id and Promotion_Type__c = 3 order by product__r.name__c, campus_course__r.course__r.name, Nationality_Group__c, Availability__c, From_Date__c, From__c];
		}catch(Exception e){
			listPromotions = new list<deal__c>();
		}

		for(deal__c cc:listPromotions){
			if(currentPromoType != null && currentPromoType != cc.product__c){
				if(!promoFeeCampus.containsKey(Product_Type))
					promoFeeCampus.put(Product_Type, new list<campusDetails>{cd});
				else promoFeeCampus.get(Product_Type).add(cd);
			}

			if(!PromoType.contains(cc.product__c)){
				cd = new campusDetails();
				cd.courseName = cc.product__r.name__c;
				cd.courseId = cc.product__c;
				cd.nationGroup.putAll(nationalityGroups);
				PromoType.add(cc.product__c);
			}
			currentPromoType = cc.product__c;
			Product_Type = cc.product__r.Product_Type__c;
			cd.nationGroup.put(cc.Nationality_Group__c, true);
			if(!cd.valueNation.containsKey(cc.Nationality_Group__c)){
				pd = new priceDetails();
				pd.itemValue = cc;
				pd.dateRange = cc.Start_Date_Ranges__r;
				
				cd.valueNation.put(cc.Nationality_Group__c, new list<priceDetails>{pd});
			}
			else {
				pd = new priceDetails();
				pd.itemValue = cc;
				pd.dateRange = cc.Start_Date_Ranges__r;
				
				cd.valueNation.get(cc.Nationality_Group__c).add(pd);
			}
			count++;
			if(count == listPromotions.size()){
				if(!promoFeeCampus.containsKey(Product_Type))
					promoFeeCampus.put(Product_Type, new list<campusDetails>{cd});
				else promoFeeCampus.get(Product_Type).add(cd);
			}
		}
	}
}