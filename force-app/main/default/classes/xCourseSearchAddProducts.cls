public class xCourseSearchAddProducts{

	private String websearchid;
	
	public xCourseSearchAddProducts(){
		websearchid = ApexPages.currentPage().getParameters().get('wsid');
		getUserDetails();
	}
	
	public Web_Search__c webSearch {
		get{
			if(webSearch == null)
				webSearch = [Select Id, Combine_Quotation__c, Currency_Rates__c, Total_Products__c, Total_Products_Other__c, 
									(Select Category__c, Currency__c, Description__c, isCustom__c, Name__c, Price__c, Quantity__c, Search_Course__c, Total__c, Unit_Description__c, Quotation_Products_Services__c, Start_Date__c, End_Date__c, Related_to_Product__r.Name__c
									from Search_Course_Products__r),
									( Select Total_Products__c, Total_Products_Other__c from Search_Courses__r WHERE Course_Deleted__c = false)
							from Web_Search__c where Id = :websearchid];
			
				for(Search_Courses__c sc : webSearch.search_Courses__r)
					searchCourses.put(sc.id, sc);
			
			return webSearch;
		}
		Set;
	}
	
	public List<String> selectedCourses {
		get{
			if(selectedCourses == null)
				selectedCourses = new List<String>();
			return selectedCourses;
		}
		Set;
	}
	
	public String combinedCurrency { get;Set; }
	
	public List<SelectOption> courses {
		get{
			if(courses == null){
				courses = new List<SelectOption>();
				campusCurrency = new Map<String, String>();
				for(Search_Courses__c sc : [Select id, Campus_Course__r.Campus__r.account_currency_iso_code__c, Custom_Currency__c, Selected__c, Campus_Course__r.Course__r.Name,  Campus_Course__r.Campus__r.Name, Custom_Course__c, Custom_School__c, isCustomCourse__c 
											from Search_Courses__c WHERE Web_Search__c = :websearchid AND Course_Deleted__c = false order by Course_Order__c NULLS LAST, CreatedDate]){

					if(sc.isCustomCourse__c){
						courses.add(new SelectOption( sc.id, sc.Custom_Course__c + ' at ' + sc.Custom_School__c ));
						campusCurrency.put(sc.id, sc.Custom_Currency__c);
						combinedCurrency = sc.Custom_Currency__c;
					} else {
						courses.add(new SelectOption( sc.id, sc.Campus_Course__r.Course__r.Name + ' at ' + sc.Campus_Course__r.Campus__r.Name ));
						campusCurrency.put(sc.id, sc.Campus_Course__r.Campus__r.account_currency_iso_code__c);
						combinedCurrency = sc.Campus_Course__r.Campus__r.account_currency_iso_code__c;
					}
													
											
				}
			}
			return courses;
		}
		Set;
	}
	
	private Map<String, String> campusCurrency;
		
	
	public void getValue(){
		
		String productID = ApexPages.currentPage().getParameters().get('productID');		
		String category = ApexPages.currentPage().getParameters().get('category');
		
        for(Quotation_Products_Services__c qps : products.get(category))
            if(qps.id == productID)
                for(Quotation_list_products__c qlp : qps.Quotation_list_products__r)
                    if(qps.Quantity__c == qlp.Quantity__c)
                        qps.Price__c = qlp.Value__c;
    }
	
	public String selectedCategory {get;Set;}
	public List<SelectOption> productCategories {
		get{
			if(productCategories == null){
				productCategories = new List<SelectOption>();
				productCategories.add(new SelectOption('','-- All --'));
				
				for(String cat : products.keySet())
					productCategories.add(new SelectOption(cat,cat));
			}
			productCategories.sort();
			return productCategories;
		}
		Set;
	}
	
	public void filterProducts(){
		
		products = null;
		
	}
	
	public Map<String, List<Quotation_Products_Services__c>> products {
		get{
			if(products == null){
				products = new Map<String, List<Quotation_Products_Services__c>>();
				
				String sql = 'select Selected__c,Agency__c,Product_Name__c,Quantity__c,Price__c,Description__c, Use_list_of_products__c,Unit_description__c ,Category__c, Currency__c, Group_Other__c, ' +
            				 ' ( Select Quantity__c, Quotation_Products_Services__c, Value__c from Quotation_list_products__r order by Quantity__c) ' +
            				 ' from Quotation_Products_Services__c where (Agency__c = \'' + userDetails.Agency_Id + '\' or agency__c = \'' + userDetails.AgencyGroup_Id + '\') AND Provider__c = NULL ';
				
				if(selectedCategory != null && selectedCategory.trim() != '')
					sql += ' and Category__c = \'' + selectedCategory + '\' ';
					
				sql += ' order by Category__c, Product_Name__c ';
				
				for(Quotation_Products_Services__c qps : Database.query(sql)){
		
					if(qps.Use_list_of_products__c && qps.Quotation_list_products__r != null && qps.Quotation_list_products__r.size() > 0){
                		qps.Quantity__c = qps.Quotation_list_products__r[0].Quantity__c.round(System.RoundingMode.UNNECESSARY);
						qps.Price__c = qps.Quotation_list_products__r[0].Value__c;
						List<SelectOption> options = new List<SelectOption>();
                
	                    for(Quotation_list_products__c qlp : qps.Quotation_list_products__r)
	                        options.add(new SelectOption(''+qlp.Quantity__c+'.0',''+qlp.Quantity__c.round(System.RoundingMode.UNNECESSARY)));
	                    
	                    prodQtdList.put(qps.id, options);
					}
					
					if(products.containsKey(qps.Category__c))
						products.get(qps.Category__c).add(qps);						
					else
						products.put(qps.Category__c, new List<Quotation_Products_Services__c>{qps});
					
				}

			}
			
			return products;
		}
		Set;
	}
	
	public class CustomProductWrapper {
	
		public Integer Id {Get;Set;}
		public Search_Course_Product__c product {get;Set;}
		
	}
	
	public List<CustomProductWrapper> customProductList {
		get{
			if(customProductList == null)
				customProductList = new List<CustomProductWrapper>();
			return customProductList;
		}
		Set;
	}

	public CustomProductWrapper customProduct {
		get{
			if(customProduct == null){
				customProduct = new CustomProductWrapper();
				customProduct.product = new Search_Course_Product__c(isCustom__c = true);
			}
			return customProduct;
		}
		Set;
	}
	
	
	private boolean checkCourseSelected(){
		Boolean isCourseSelected = false;
		if(selectedCourses != null && !selectedCourses.isEmpty())		
			isCourseSelected = true;
		
		if(!isCourseSelected){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select the courses you want to add products.');
            ApexPages.addMessage(myMsg);
			
		}
		
		return isCourseSelected;
		
	}
	
	public Map<String, List<SelectOption>> prodQtdList {
        get{
	        if(prodQtdList == null)
	            prodQtdList = new Map<String, List<SelectOption>>();
	        return prodQtdList;
	    }
        set;
    }
	
	private Map<String, Search_Courses__c> searchCourses {
		get{
			if(searchCourses == null)
				searchCourses = new Map<String, Search_Courses__c>();							
			return searchCourses;
		}
		Set;
	}
	
	
	
	public Map<String, Double> currencyRates {
		get{
			if(currencyRates == null){
				currencyRates = new Map<String, Double>();
				
				currencyRates.put(userDetails.Agency_account_currency_iso_code, 1.0);
				currencyRates.put(combinedCurrency, 1.0);
				
				for(String str : campusCurrency.values())
					if(!currencyRates.containsKey(str))
						currencyRates.put(str, 1);
				
				for(Currency_rate__c cr : [Select CurrencyCode__c, Value__c, LastModifiedDate from Currency_rate__c where Agency__c = :userDetails.Agency_Id AND CurrencyCode__c != null order by Description__c])					
					currencyRates.put(cr.CurrencyCode__c, cr.Value__c);
				
				if(webSearch.Currency_Rates__c != null){
					for(String curr : webSearch.Currency_Rates__c.split(';')){
						String[] str = curr.split(':');
						currencyRates.put(str[0], Double.valueOf(str[1]));
					}
				}
			}
			return currencyRates;
		}
		Set;
	}
	
	public Boolean productsAdded {get;Set;}
	public void addProducts(){
		
		productsAdded = false;
		
		if( !webSearch.Combine_Quotation__c && !checkCourseSelected() ) return;
		
		List<Search_Course_Product__c> listProducts = new List<Search_Course_Product__c>();
		Search_Course_Product__c scp = new Search_Course_Product__c();
		
		Boolean allValidated = true;


		// PRODUCTS
		for(String key : products.keySet()){
			for(Quotation_Products_Services__c qps : products.get(key)){
				if(qps.Selected__c){
					if(!validate(qps)){
						allValidated = false;
						continue;
					}
					if(!webSearch.Combine_Quotation__c){
						
						for(String searchcourseid : selectedCourses){
							
							scp = new Search_Course_Product__c();
							scp.Search_Course__c = searchcourseid;
							scp.Web_Search__c = webSearch.Id;
							scp.Quotation_Products_Services__c = qps.Id;
							scp.Category__c = qps.Category__c;
							scp.Currency__c = qps.Currency__c;
							scp.Description__c = qps.Description__c;
							scp.Name__c = qps.Product_Name__c;
							scp.Quantity__c = qps.Quantity__c;
							scp.Price__c = qps.Price__c;
							scp.Unit_Description__c = qps.Unit_description__c;
							if(qps.Use_list_of_products__c)
								scp.total__c = scp.Price__c;
							else
								scp.total__c = scp.Price__c * scp.Quantity__c;
							
							Double totalProducts = searchCourses.get(searchcourseid).Total_Products__c != null ? searchCourses.get(searchcourseid).Total_Products__c : 0;
							double totalProductsOther = searchCourses.get(searchcourseid).Total_Products_Other__c != null ? searchCourses.get(searchcourseid).Total_Products_Other__c : 0;
							Decimal totalProductConverted = 0;
							
							
							
							if(scp.Currency__c == campusCurrency.get(searchCourseid))
								totalProductConverted = scp.Total__c;
							else if(scp.Currency__c == userDetails.Agency_account_currency_iso_code)
								totalProductConverted = scp.Total__c / currencyRates.get( campusCurrency.get(searchCourseid) );
							else
								totalProductConverted = (scp.Total__c * currencyRates.get(scp.Currency__c)) / currencyRates.get(campusCurrency.get(searchCourseid));
							
							
							searchCourses.get(searchcourseid).Total_Products__c = totalProducts + totalProductConverted.setScale(2);							
							
							if(qps.Group_Other__c){							
								totalProductsOther += totalProductConverted.setScale(2);
								searchCourses.get(searchcourseid).Total_Products_Other__c = totalProductsOther;
							}
							
							
							
							System.debug('@@@@ totalProductConverted: ' + totalProductConverted.setScale(2));
							System.debug('@@@@ searchCourses.get(searchcourseid).Total_Products__c: ' + searchCourses.get(searchcourseid).Total_Products__c );
							listProducts.add(scp);
						}
						
						
						
					} else {
						//COMBINE COURSES
						scp = new Search_Course_Product__c();
						scp.Web_Search__c = webSearch.Id;
						scp.Quotation_Products_Services__c = qps.Id;
						scp.Category__c = qps.Category__c;
						scp.Currency__c = qps.Currency__c;
						scp.Description__c = qps.Description__c;
						scp.Name__c = qps.Product_Name__c;
						scp.Quantity__c = qps.Quantity__c;
						scp.Price__c = qps.Price__c;
						scp.Unit_Description__c = qps.Unit_description__c;
						if(qps.Use_list_of_products__c)
							scp.total__c = scp.Price__c;
						else
							scp.total__c = scp.Price__c * scp.Quantity__c;
						
						listProducts.add(scp);
						
						Double totalProducts = webSearch.Total_Products__c != null ? webSearch.Total_Products__c : 0;
						double totalProductsOther = webSearch.Total_Products_Other__c != null ? webSearch.Total_Products_Other__c : 0;
						Decimal totalProductConverted = 0;
						
						if(scp.Currency__c == combinedCurrency)
							totalProductConverted = scp.Total__c;						
						else if(scp.Currency__c == userDetails.Agency_account_currency_iso_code)
							totalProductConverted = scp.Total__c / currencyRates.get( combinedCurrency );
						else
							totalProductConverted = (scp.Total__c * currencyRates.get(scp.Currency__c)) / currencyRates.get(combinedCurrency);
						
						webSearch.Total_Products__c = totalProducts + totalProductConverted.setScale(2);
						
						if(qps.Group_Other__c){							
							totalProductsOther += totalProductConverted.setScale(2);
							webSearch.Total_Products_Other__c = totalProductsOther;
						}
						
					}
					
					
				}
			}
		}
			
			
		//CUSTOM PRODUCTS
		for(CustomProductWrapper custom : customProductList){
			if(!validateProduct(custom.product)){
				allValidated = false;
				continue;
			}
			
			
			
			if(webSearch.Combine_Quotation__c){
				custom.product.Web_Search__c = webSearch.Id;
				
				
				Double totalProducts = webSearch.Total_Products__c != null ? webSearch.Total_Products__c : 0;
							
				Decimal totalProductConverted = 0;
				
				if(custom.Product.Currency__c == combinedCurrency)
					totalProductConverted = custom.Product.Total__c;
				else if(custom.Product.Currency__c == userDetails.Agency_account_currency_iso_code)
					totalProductConverted = custom.Product.Total__c * currencyRates.get(custom.Product.Currency__c);
				else
					totalProductConverted = (custom.Product.Total__c * currencyRates.get(custom.Product.Currency__c)) / currencyRates.get(combinedCurrency);
				
				webSearch.Total_Products__c = totalProducts + totalProductConverted.setScale(2);
				
				
				
				listProducts.add(custom.product);
				
			} else {
				
				for(String searchcourseid : selectedCourses){
					scp = new Search_Course_Product__c();
					scp.isCustom__c = true;
					scp.Search_Course__c = searchcourseid;
					scp.Web_Search__c = webSearch.Id;
					scp.Currency__c = custom.product.Currency__c;
					scp.Description__c = custom.product.Description__c;
					scp.Name__c = custom.product.Name__c;
					scp.Quantity__c = custom.product.Quantity__c;
					scp.Price__c = custom.product.Price__c;
					scp.total__c = custom.product.Price__c * custom.product.Quantity__c;
					
					
					Double totalProducts = searchCourses.get(searchcourseid).Total_Products__c != null ? searchCourses.get(searchcourseid).Total_Products__c : 0;
							
					Decimal totalProductConverted = 0;
					
					if(scp.Currency__c == campusCurrency.get(searchCourseid))
						totalProductConverted = scp.Total__c;
					else if(scp.Currency__c == userDetails.Agency_account_currency_iso_code)
						totalProductConverted = scp.Total__c / currencyRates.get( campusCurrency.get(searchCourseid) );
					else
						totalProductConverted = (scp.Total__c * currencyRates.get(scp.Currency__c)) / currencyRates.get(campusCurrency.get(searchCourseid));
					
					searchCourses.get(searchcourseid).Total_Products__c = totalProducts + totalProductConverted.setScale(2);
					
					
					listProducts.add(scp);
				}
				
			}	
				
		}
			
		
		if(allValidated){
			insert listProducts;
			update webSearch;
			update webSearch.search_Courses__r;
			productsAdded = true;
		} else
			productsAdded = false;
			
		
		
		
		
		
		
	}
	
	private Integer count = 0;
	
	public void addCustomProduct(){
		
		if(validateProduct(customProduct.product)){
			customProduct.product.Total__c = customProduct.product.Price__c * customProduct.product.Quantity__c;
			customProduct.id = count;
			customProductList.add(customProduct);		
			customProduct = null;
			count++;
		}
		
	}
	
	
	private Boolean validate(Quotation_Products_Services__c prod){
		Boolean validated = true;
		if(prod.Quantity__c == null || prod.Quantity__c == 0){
			prod.Quantity__c.addError('Required Field');
			validated = false;
		}
		if(prod.Price__c == null || prod.Price__c == 0){
			prod.Price__c.addError('Required Field');
			validated = false;
		}
		if(prod.Currency__c == null || prod.Currency__c == ''){
			prod.Currency__c.addError('Required Field');
			validated = false;
		}
		return validated;
	}
	
	public void deleteCustomProduct(){
		Integer cpid = Integer.valueOf(ApexPages.currentPage().getParameters().get('cpid'));
		
		for(integer i = 0; i < customProductList.size(); i++){
			CustomProductWrapper cpw = customProductList.get(i);
			if(cpw.id == cpid)
				customProductList.remove(i);
		}
	}
	
	
	private boolean validateProduct(Search_Course_Product__c prod){
		Boolean validated = true;
		if(prod.Name__c == null || prod.Name__c.trim() == ''){
			prod.Name__c.addError('Required Field');
			validated = false;
		}
		if(prod.Quantity__c == null || prod.Quantity__c < 0){
			prod.Quantity__c.addError('Required Field');
			validated = false;
		}
		if(prod.Price__c == null){
			prod.Price__c.addError('Required Field');
			validated = false;
		}
		if(prod.Currency__c == null || prod.Currency__c == ''){
			prod.Currency__c.addError('Required Field');
			validated = false;
		}
		return validated;
	}
	
	
   /* public Account userDetails{
        get{
	        if (userDetails == null)
	            userDetails = [select Agency__pc, Agency__pr.Name, Agency__pr.BillingCountry, Preferable_nationality__c,Preferable_School_Country__c,Preferable_School_City__c,
	       								 Agency_account_currency_iso_code, Agency__pr.Optional_Currency__c, Full_Name__pc, PersonEmail, Agency__pr.Phone from Account where user__c = :UserInfo.getUserId()];
	        return userDetails;
	    }
        set;
    }*/
	
	private xCourseSearchPersonDetails.UserDetails UserDetails;
	public xCourseSearchPersonDetails.UserDetails getUserDetails(){
		if(UserDetails == null){
			xCourseSearchPersonDetails spd = new xCourseSearchPersonDetails();
			UserDetails = spd.getAgencyDetails();
		}
		return UserDetails;
	}
	
	public List<SelectOption> getUnitsRange(){    	
        return xCourseSearchFunctions.getUnitsRange();
    }
	
	public List<SelectOption> getCurrencies() {
		return xCourseSearchFunctions.getAgencyCurrencies(userDetails.Agency_Id, null);
	}
	
}