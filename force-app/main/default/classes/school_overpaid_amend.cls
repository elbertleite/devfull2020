public with sharing class school_overpaid_amend {
    
    public list<client_course_instalment__c> instalments {get;set;}
    public list<client_course_instalment__c> pfsInstalments {get;set;}

	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}
    
    public school_overpaid_amend(){
    	instalments = new list<client_course_instalment__c>();
    	
    	if(ApexPages.currentPage().getParameters().get('gr')!=null)
			selectedAgencyGroup = ApexPages.currentPage().getParameters().get('gr');
			
		if(ApexPages.currentPage().getParameters().get('ag')!=null){
			selectedAgency = ApexPages.currentPage().getParameters().get('ag');
			selectedAgencies = new List<String>{selectedAgency};
		}
		
		if(ApexPages.currentPage().getParameters().get('sc')!=null)
			selectedSchool = ApexPages.currentPage().getParameters().get('sc');
		
		if(ApexPages.currentPage().getParameters().get('ca')!=null)
			selectedCampus = ApexPages.currentPage().getParameters().get('ca');
    }
    
    
    /** Find Amended Commissions to Request **/
    public void findRequestRefund(){
    	
    	instalments.clear(); 
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && currentUser.Aditional_Agency_Managment__c == null){ //set the user to see only his own agency
			selectedAgency = currentUser.Contact.AccountId;
			selectedAgencies = new List<String>{selectedAgency};
		}

		List<String> agenciesToSearch;
		if(selectedAgencies.size() == 1 && selectedAgencies.get(0) == 'all'){
			agenciesToSearch = new List<String>();
			for(SelectOption opt : getAgencies()){
				if(opt.getValue() != 'all'){
					agenciesToSearch.add(opt.getValue());		
				}
			}
		}else{
			agenciesToSearch = selectedAgencies;
		}
		
		String sql = ' SELECT Id, Name, Due_Date__c, Number__c, Discount__c, isPFS__c, Commission_Value__c, Commission_Tax_Value__c, Kepp_Fee__c, Amendment_Receive_From_School__c, ' +
						' Commission_Adjustment__c, Commission_Tax_Adjustment__c, Extra_Fee_Value__c, Tuition_Value__c, Instalment_Value__c, Amendment_PFS_Adjustment__c, Received_By_Agency__r.Name, ' + 
					 
					 ' Original_Instalment__r.Tuition_Value__c, ' +
					 ' Original_Instalment__r.Extra_Fee_Value__c, ' +
					 ' Original_Instalment__r.Kepp_Fee__c, ' +
					 ' Original_Instalment__r.Instalment_Value__c, ' +
					 ' Original_Instalment__r.Paid_To_School_By__r.Name, ' +
					 ' Original_Instalment__r.Paid_To_School_On__c, ' +
					 ' Original_Instalment__r.Commission_Value__c, ' +
					 ' Original_Instalment__r.Commission_Tax_Value__c, ' +
					 ' Original_Instalment__r.School_Invoice_Number__c, ' +
					 
					 ' Client_Course__c, ' +
					 ' client_course__r.Client__r.Owner__c, ' +
					 ' client_course__r.Client__r.Owner__r.AccountId, ' +
					 ' Client_Course__r.School_Name__c, ' +
					 ' Client_Course__r.Campus_Name__c, ' +
					 ' Client_Course__r.Course_Name__c, ' +
					 ' Client_Course__r.Course_Length__c, ' +
					 ' Client_Course__r.Unit_Type__c,  ' +
					 ' Client_Course__r.Start_Date__c, ' +
					 ' Client_Course__r.End_Date__c, ' +
					 ' Client_Course__r.Client__c,  ' +
					 ' Client_Course__r.Client__r.Status__c, ' +
					 ' Client_Course__r.Client__r.Owner__r.Name, ' +
					 ' client_course__r.Client__r.Email, ' +
					 ' Client_Course__r.Client__r.Name, ' +
					 ' Client_Course__r.Client__r.Visa_Expiry_Date__c  ' +
					
					//  ' FROM Client_Course_Instalment__c WHERE Received_By_Agency__c = :selectedAgency '; //Agency Received_By_Agency__c = :selectedAgency
					 ' FROM Client_Course_Instalment__c WHERE Received_By_Agency__c IN :agenciesToSearch '; //Agency Received_By_Agency__c = :selectedAgency
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
		 	sql+= ' AND Received_By_Agency__r.ParentId = :selectedAgencyGroup ';
		
		if(selectedCampus!='all')
			sql+= ' AND client_course__r.Campus_Id__c = :selectedCampus '; //Campus
		else if(selectedSchool!='all')
			sql+= ' AND client_course__r.School_Id__c = :selectedSchool '; //School 	
		
		sql+= ' AND isOverpayment_Amendment__c = TRUE AND Sent_Email_Receipt__c = NULL'; 
		
		system.debug('sql===' + sql);
		
		instalments = Database.query(sql);
    }
    
    
     public void findConfirmRefund(){
    	
    	instalments.clear();
		pfsInstalments = new list<client_course_instalment__c>();
		
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && currentUser.Aditional_Agency_Managment__c == null){ //set the user to see only his own agency
			selectedAgency = currentUser.Contact.AccountId;
			selectedAgencies = new List<String>{selectedAgency};
		}
		List<String> agenciesToSearch;
		if(selectedAgencies.size() == 1 && selectedAgencies.get(0) == 'all'){
			agenciesToSearch = new List<String>();
			for(SelectOption opt : getAgencies()){
				if(opt.getValue() != 'all'){
					agenciesToSearch.add(opt.getValue());		
				}
			}
		}else{
			agenciesToSearch = selectedAgencies;
		}
		
		String sql = ' SELECT Id, Name, Due_Date__c, Number__c, Discount__c, isPFS__c, Commission_Value__c, Commission_Tax_Value__c, Kepp_Fee__c, Amendment_Receive_From_School__c, ' +
						' Commission_Adjustment__c, Commission_Tax_Adjustment__c, Extra_Fee_Value__c, Tuition_Value__c, Instalment_Value__c, Amendment_PFS_Adjustment__c, Amendment_Total_Charge_Client__c, Received_By_Agency__r.Name, ' + 
					 
					 ' Original_Instalment__r.Tuition_Value__c, ' +
					 ' Original_Instalment__r.Extra_Fee_Value__c, ' +
					 ' Original_Instalment__r.Kepp_Fee__c, ' +
					 ' Original_Instalment__r.Instalment_Value__c, ' +
					 ' Original_Instalment__r.Paid_To_School_By__r.Name, ' +
					 ' Original_Instalment__r.Paid_To_School_On__c, ' +
					 ' Original_Instalment__r.Commission_Value__c, ' +
					 ' Original_Instalment__r.Commission_Tax_Value__c, ' +
					 ' Original_Instalment__r.School_Invoice_Number__c, ' +
					 ' Original_Instalment__r.Commission_Paid_Date__c, '+
					 ' Original_Instalment__r.Commission_Confirmed_On__c, '+
					 ' Original_Instalment__r.Commission_Confirmed_By__c, '+
					 ' Original_Instalment__r.Commission_Confirmed_By_Agency__c, '+
					 
					 ' Client_Course__c, ' +
					 ' client_course__r.Client__r.Owner__c, ' +
					 ' client_course__r.Client__r.Owner__r.AccountId, ' +
					 ' Client_Course__r.School_Name__c, ' +
					 ' Client_Course__r.Campus_Name__c, ' +
					 ' Client_Course__r.Course_Name__c, ' +
					 ' Client_Course__r.Course_Length__c, ' +
					 ' Client_Course__r.Unit_Type__c,  ' +
					 ' Client_Course__r.Start_Date__c, ' +
					 ' Client_Course__r.End_Date__c, ' +
					 ' Client_Course__r.Client__c,  ' +
					 ' Client_Course__r.Client__r.Status__c, ' +
					 ' Client_Course__r.Client__r.Owner__r.Name, ' +
					 ' client_course__r.Client__r.Email, ' +
					 ' Client_Course__r.Client__r.Name, ' +
					 ' Client_Course__r.Client__r.Visa_Expiry_Date__c, ' +
					 ' Client_Course__r.School_Id__c ' +
					
					 //' FROM Client_Course_Instalment__c WHERE Received_By_Agency__c = :selectedAgency '; //Agency Received_By_Agency__c = :selectedAgency
					 ' FROM Client_Course_Instalment__c WHERE Received_By_Agency__c IN :agenciesToSearch '; //Agency Received_By_Agency__c = :selectedAgency
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
		 	sql+= ' AND Received_By_Agency__r.ParentId = :selectedAgencyGroup ';
		
		if(selectedCampus!='all')
			sql+= ' AND client_course__r.Campus_Id__c = :selectedCampus '; //Campus
		else if(selectedSchool!='all')
			sql+= ' AND client_course__r.School_Id__c = :selectedSchool '; //School 	 	
		
		//sql+= ' AND isInstalment_Amendment__c = FALSE AND  isOverpayment_Amendment__c = TRUE AND Sent_Email_Receipt_On__c != NULL AND Commission_Paid_Date__c = NULL '; 
		sql+= ' AND  isOverpayment_Amendment__c = TRUE AND Sent_Email_Receipt_On__c != NULL  and Paid_to_Client_On__c = NULL '; 
		
		system.debug('sql===' + sql);
		
		instalments = Database.query(sql);
		
		
		String sqlPFS = ' SELECT Id, Name, Due_Date__c, Number__c, Discount__c, isPFS__c, Commission_Value__c, Commission_Tax_Value__c, Kepp_Fee__c, Amendment_Receive_From_School__c, ' +
						' Commission_Adjustment__c, Commission_Tax_Adjustment__c, Extra_Fee_Value__c, Tuition_Value__c, Instalment_Value__c, Amendment_PFS_Adjustment__c, Amendment_Total_Charge_Client__c, Received_By_Agency__r.Name, ' + 
					 
					 ' Original_Instalment__r.Tuition_Value__c, ' +
					 ' Original_Instalment__r.Extra_Fee_Value__c, ' +
					 ' Original_Instalment__r.Kepp_Fee__c, ' +
					 ' Original_Instalment__r.Instalment_Value__c, ' +
					 ' Original_Instalment__r.Paid_To_School_By__r.Name, ' +
					 ' Original_Instalment__r.Paid_To_School_On__c, ' +
					 ' Original_Instalment__r.Commission_Value__c, ' +
					 ' Original_Instalment__r.Commission_Tax_Value__c, ' +
					 ' Original_Instalment__r.School_Invoice_Number__c, ' +
					 ' Original_Instalment__r.Commission_Paid_Date__c, '+
					 ' Original_Instalment__r.Commission_Confirmed_On__c, '+
					 ' Original_Instalment__r.Commission_Confirmed_By__c, '+
					 ' Original_Instalment__r.Commission_Confirmed_By_Agency__c, '+
					 
					 ' Client_Course__c, ' +
					 ' client_course__r.Client__r.Owner__c, ' +
					 ' client_course__r.Client__r.Owner__r.AccountId, ' +
					 ' Client_Course__r.School_Name__c, ' +
					 ' Client_Course__r.Campus_Name__c, ' +
					 ' Client_Course__r.Course_Name__c, ' +
					 ' Client_Course__r.Course_Length__c, ' +
					 ' Client_Course__r.Unit_Type__c,  ' +
					 ' Client_Course__r.Start_Date__c, ' +
					 ' Client_Course__r.End_Date__c, ' +
					 ' Client_Course__r.Client__c,  ' +
					 ' Client_Course__r.Client__r.Status__c, ' +
					 ' Client_Course__r.Client__r.Owner__r.Name, ' +
					 ' client_course__r.Client__r.Email, ' +
					 ' Client_Course__r.Client__r.Name, ' +
					 ' Client_Course__r.Client__r.Visa_Expiry_Date__c, ' +
					 ' Client_Course__r.School_Id__c ' +
					
					 //' FROM Client_Course_Instalment__c WHERE Received_By_Agency__c = :selectedAgency '; //Agency Received_By_Agency__c = :selectedAgency
					 ' FROM Client_Course_Instalment__c WHERE Received_By_Agency__c IN :agenciesToSearch '; //Agency Received_By_Agency__c = :selectedAgency
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
		 	sqlPFS+= ' AND Received_By_Agency__r.ParentId = :selectedAgencyGroup ';
		
		if(selectedCampus!='all')
			sqlPFS+= ' AND client_course__r.Campus_Id__c = :selectedCampus '; //Campus
		else if(selectedSchool!='all')
			sqlPFS+= ' AND client_course__r.School_Id__c = :selectedSchool '; //School 	 	
		
		sqlPFS+= ' AND isInstalment_Amendment__c = TRUE AND isPFS__c = TRUE AND  isOverpayment_Amendment__c = TRUE AND Commission_Paid_Date__c != NULL AND isOverpayment_Refunded__c = FALSE '; 
		
		system.debug('sqlPFS===' + sqlPFS);
		
		pfsInstalments = Database.query(sqlPFS);
		
		
		
    }
    
    
    /** C O N V E R T 	A M O U N T 	T O 	S C H O O L 	C R E D I T **/
    public void convertToCredit(){
    
    
    	string toUpdate = ApexPages.currentPage().getParameters().get('amendId');
    
    	for(client_course_instalment__c cci : instalments){
    	
    		if(cci.id == toUpdate){
				
				IPFunctions.SearchNoSharing sns = new IPFunctions.SearchNoSharing();
				Account school = sns.NSAccount('SELECT ParentId, Parent.Name FROM Account WHERE id = \'' + cci.client_course__r.School_Id__c + '\'');
    			
    			findSchoolCredit(cci.client_course__r.Client__c, school);
    			
				boolean isGeneralUser = FinanceFunctions.isGeneralUser(currentUser.General_User_Agencies__c, cci.client_course__r.Client__r.Owner__r.AccountId);
                                    
				if(!isGeneralUser){
					cci.Commission_Confirmed_By_Agency__c = currentUser.Contact.AccountId;
				}else{
					cci.Commission_Confirmed_By_Agency__c = cci.client_course__r.Client__r.Owner__r.AccountId;
				}

				cci.Commission_Confirmed_By__c = currentUser.Id;
					
				// cci.Commission_Confirmed_On__c = DateTime.now();
    			// cci.Commission_Paid_Date__c = system.today();
    			cci.Paid_to_Client_On__c = system.today();
    			
    			if(schoolCredit.Credit_Available__c==null) 
					schoolCredit.Credit_Available__c = 0;
					
				if(schoolCredit.Credit_Value__c==null) 
					schoolCredit.Credit_Value__c = 0;
				
				if(schoolCredit.Credit_Courses__c== null || schoolCredit.Credit_Courses__c == '')
					schoolCredit.Credit_Courses__c = cci.Client_Course__r.Course_Name__c + ' - $'+math.ABS(cci.Amendment_Total_Charge_Client__c);
				else
					schoolCredit.Credit_Courses__c += ', ' + cci.Client_Course__r.Course_Name__c +  '- $' +math.ABS(cci.Amendment_Total_Charge_Client__c);

				schoolCredit.Credit_Available__c += math.ABS(cci.Amendment_Total_Charge_Client__c);
				schoolCredit.Credit_Value__c += math.ABS(cci.Amendment_Total_Charge_Client__c);
				
				
				/** Check if the Original instalment is PFS and it 's still pending to receive commission **/
				// if(cci.Original_Instalment__r.Commission_Paid_Date__c==null){
					
				// 	cci.Original_Instalment__r.Commission_Paid_Date__c = system.today();
				// 	cci.Original_Instalment__r.Commission_Confirmed_On__c = DateTime.now();
				// 	cci.Original_Instalment__r.Commission_Confirmed_By__c = UserInfo.getUserId();
				// 	if(!isGeneralUser){
				// 		cci.Original_Instalment__r.Commission_Confirmed_By_Agency__c = currentUser.Contact.AccountId;
				// 	}else{
				// 		cci.Original_Instalment__r.Commission_Confirmed_By_Agency__c =  cci.client_course__r.Client__r.Owner__r.AccountId;
				// 	}
					
					
				// 	update cci.Original_Instalment__r;
				// }
				
				
				
    			update cci;
				upsert schoolCredit;
    			
    			break;
    		}
    	
    	}//end for
    
    }
    
    
    private client_course__c schoolCredit {get;set;}
	
	private void findSchoolCredit(String clientId, Account school){
		
		// system.debug('Client===>' + cci.Client_Course__r.client__c);
		// system.debug('School====>' + cci.client_course__r.School_Id__c);
		try{
			// schoolCredit = [SELECT Id, Credit_Available__c, Credit_Value__c, Credit_Courses__c FROM client_course__c WHERE Client__c =:cci.Client_Course__r.client__c and Credit_Valid_For_School__c = :cci.client_course__r.School_Id__c and isCourseCredit__c = true];
			schoolCredit = [SELECT Id, Credit_Available__c, Credit_Value__c, Credit_Courses__c FROM client_course__c WHERE Client__c = :clientId and Credit_Valid_For_School__c = :school.ParentId and isCourseCredit__c = true];
		}catch(exception e){
			// schoolCredit = new client_course__c(Client__c = cci.Client_Course__r.client__c, isCourseCredit__c = true, Credit_Valid_For_School__c = cci.client_course__r.School_Id__c, School_Name__c = cci.Client_Course__r.School_Name__c);
			schoolCredit = new client_course__c(Client__c = clientId, isCourseCredit__c = true, Credit_Valid_For_School__c = school.ParentId, School_Name__c = school.Parent.Name);
		}
	} 
	
    
    
    
    
    public integer getTotalRequest(){
    	integer result;
    	
    	String sql = 'Select count(id) total From Client_Course_Instalment__c WHERE Received_By_Agency__c =:selectedAgency '; 
    	
    	
    	if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
		 	sql+= ' AND Received_By_Agency__r.ParentId = :selectedAgencyGroup ';
		 	
	 	if(selectedCampus!='all')
			sql+= ' AND client_course__r.Campus_Id__c = :selectedCampus '; //Campus
		else if(selectedSchool!='all')
			sql+= ' AND client_course__r.School_Id__c = :selectedSchool '; //School	
			
		sql+= 'AND  isOverpayment_Amendment__c = TRUE AND Sent_Email_Receipt__c = NULL  ';
		
		
		for(AggregateResult ar : Database.query(sql))
			result = integer.valueOf(ar.get('total'));	
    	
		return result;
    }
    
    
    public integer getTotalConfirm(){
    	integer result = 0;
    	
    	
    	/** N O  T 		P F S 	**/
    	String sql = 'Select count(id) total From Client_Course_Instalment__c WHERE Received_By_Agency__c =:selectedAgency '; 
    	
    	
    	if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
		 	sql+= ' AND Received_By_Agency__r.ParentId = :selectedAgencyGroup ';
		 	
	 	if(selectedCampus!='all')
			sql+= ' AND client_course__r.Campus_Id__c = :selectedCampus '; //Campus
		else if(selectedSchool!='all')
			sql+= ' AND client_course__r.School_Id__c = :selectedSchool '; //School	
			
		// sql+= ' AND isInstalment_Amendment__c = FALSE AND  isOverpayment_Amendment__c = TRUE AND Sent_Email_Receipt_On__c != NULL AND Commission_Paid_Date__c = NULL  ';
		sql+= ' AND  isOverpayment_Amendment__c = TRUE AND Sent_Email_Receipt_On__c != NULL  and Paid_to_Client_On__c = NULL  ';
		
		
		for(AggregateResult ar : Database.query(sql))
			result = integer.valueOf(ar.get('total'));	
			
		
		
		
		/** PFS 	C O N F I R M E D **/
		String sqlPFS = 'Select count(id) total From Client_Course_Instalment__c WHERE Received_By_Agency__c =:selectedAgency '; 
    	
    	
    	if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
		 	sqlPFS+= ' AND Received_By_Agency__r.ParentId = :selectedAgencyGroup ';
		 	
	 	if(selectedCampus!='all')
			sqlPFS+= ' AND client_course__r.Campus_Id__c = :selectedCampus '; //Campus
		else if(selectedSchool!='all')
			sqlPFS+= ' AND client_course__r.School_Id__c = :selectedSchool '; //School	
			
		sqlPFS+= ' AND isInstalment_Amendment__c = TRUE AND isPFS__c = TRUE AND  isOverpayment_Amendment__c = TRUE AND Commission_Paid_Date__c != NULL AND isOverpayment_Refunded__c = FALSE ';	
		
		
		for(AggregateResult ar : Database.query(sqlPFS))
			result += integer.valueOf(ar.get('total'));	
			
			
		return result;
    }
    
    
    public String selectedAction {get;set;}
	public List<SelectOption> actionOptions {
		get{
			if(actionOptions == null){
				actionOptions = new List<SelectOption>();	
				actionOptions.add(new SelectOption('none', '-- Select Option -- '));
				actionOptions.add(new SelectOption('clientCredit', 'Convert to School Credit'));
				actionOptions.add(new SelectOption('refundClient', 'Convert to Client Deposit'));
				actionOptions.add(new SelectOption('sendEmail', 'Send Email'));
			}
			
			return actionOptions;
		}
		set;
	}    
	
	public String selectedPFSAction {get;set;}
	public List<SelectOption> pfsActionOptions {
		get{
			if(pfsActionOptions == null){
				pfsActionOptions = new List<SelectOption>();	
				pfsActionOptions.add(new SelectOption('none', '-- Select Option -- '));
				pfsActionOptions.add(new SelectOption('refundClient', 'Convert to Client Deposit'));
				//pfsActionOptions.add(new SelectOption('sendEmail', 'Send Email'));
			}
			
			return pfsActionOptions;
		}
		set;
	} 
	
	/********************** Filters **********************/	
	private User currentUser {get{if(currentUser==null) currentUser = [Select Id, Name, Aditional_Agency_Managment__c, General_User_Agencies__c, Contact.Account.ParentId, Contact.AccountId, Contact.Department__c from User where id = :UserInfo.getUserId() limit 1]; return currentUser;}set;}
	
	public string selectedAgencyGroup{
    	get{
    		if(selectedAgencyGroup == null)
    			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
    		return selectedAgencyGroup;
    	}  
    	set;
    }
    
	 public List<SelectOption> agencyGroupOptions {
  		get{
   			if(agencyGroupOptions == null){
   				
   				agencyGroupOptions = new List<SelectOption>();  
   				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group  
	    			for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:currentUser.Contact.AccountId order by Parent.Name]){
		     			agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
		    		}
   				}else{
   					for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
		     			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
		    		}
   				}
	   		}
	   		return agencyGroupOptions;
	  }
	  set;
 	}
	
	public string selectedAgency {get{if(selectedAgency==null) selectedAgency = currentUser.Contact.AccountId; return selectedAgency;}set;}
	public List<String> selectedAgencies {get{if(selectedAgencies==null) selectedAgencies = new List<String>{currentUser.Contact.AccountId}; return selectedAgencies;}set;}
	public Set<string> selectedAgenciesReplica {get{if(selectedAgenciesReplica==null) selectedAgenciesReplica = new Set<String>{currentUser.Contact.AccountId}; return selectedAgenciesReplica;}set;}
		
	public list<SelectOption> getAgencies(){
		List<SelectOption> result = new List<SelectOption>();
		result.add(new SelectOption('all', '-- All Agencies --'));
		if(selectedAgencyGroup != null){
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
				for(Account a: [Select Id, Name from Account where id =:currentUser.Contact.AccountId order by name]){
					if(selectedAgency=='')
						selectedAgency = a.Id;					
					result.add(new SelectOption(a.Id, a.Name));
				}
				if(currentUser.Aditional_Agency_Managment__c != null){
					for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
						result.add(new SelectOption(ac.id, ac.name));
					}
				}
			}else{
				for(Account a: [Select Id, Name from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
					result.add(new SelectOption(a.Id, a.Name));
					if(selectedAgency=='')
						selectedAgency = a.Id;	
				}
			}
		}
		
		return result;
	}
	
	public void changeGroup(){
		selectedAgency = '';
		getAgencies();
		
		changeAgency();
	}
		
	public void changeAgency(){
		/*if(selectedAgencies.contains('all')){
			selectedAgencies = new List<String>();
			for(SelectOption agencyOption : getAgencies()){
				if(agencyOption.getValue() != 'all'){
					selectedAgencies.add(agencyOption.getValue());
				}
			}
		}*/
		if(selectedAgencies.size() > selectedAgenciesReplica.size()){
			String itemAdded;
			for(String agency : selectedAgencies){
				if(!selectedAgenciesReplica.contains(agency)){
					itemAdded = agency;
				}
			}
			if(itemAdded == 'all' || selectedAgencies.indexOf('all') != -1){
				selectedAgencies = new List<String>{itemAdded};
			}
		}
		selectedAgenciesReplica = new Set<String>(selectedAgencies);

		selectedSchool ='all';
		schoolOptions = null;
		
		campusOptions = null;
		selectedCampus = 'all';
	}
	
	public IPFunctions.CampusAvailability ca {get{if(ca==null) ca = new IPFunctions.CampusAvailability(); return ca;}set;}
	
	public String selectedSchool {get{if(selectedSchool==null) selectedSchool = 'all'; return selectedSchool;}set;}
	public  List<SelectOption> schoolOptions {
		get{
			if(schoolOptions == null){
				schoolOptions = new List<SelectOption>();
				//schoolOptions = ca.getAvlSchools(selectedAgency);
				if(selectedAgencies.size() == 1 && selectedAgencies.get(0) != 'all'){
					schoolOptions = ca.getAvlSchools(selectedAgencies.get(0));
				}else{
					schoolOptions = new List<SelectOption>();
					schoolOptions.add(new SelectOption('all','-- All --'));
				}
				
			}
			return schoolOptions;
		}set;}
		
	public void changeSchool(){
		campusOptions = null;
		selectedCampus = 'all';
	}		
			
	public String selectedCampus {get{if(selectedCampus==null) selectedCampus = 'all'; return selectedCampus;}set;}
	public List<SelectOption> campusOptions {
		get{
			if(campusOptions == null && selectedSchool != 'all'){
				
				campusOptions = new List<SelectOption>();				
				campusOptions = ca.getSchoolCampus(selectedSchool);				
			}
			
			if(campusOptions == null && selectedSchool == 'all'){
				campusOptions = new List<SelectOption>();	
				campusOptions.add(new SelectOption('all', '-- All --'));
			}
			
			return campusOptions;
		}
		set;
	}
}