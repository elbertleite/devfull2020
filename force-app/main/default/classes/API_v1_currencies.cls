@RestResource(urlMapping = '/v1/currencies/*')
global class API_v1_currencies extends API_Manager {
	
    @HttpGet
	global static AgencyCurrency doGet(){
		
		RestRequest request = RestContext.request;
		
		System.debug('/v1/agencies request: ' + request);
		
		try {
			
			String agencyid = [select Contact.AccountID from User where id =:UserInfo.getUserId() limit 1].Contact.AccountID;
			String isocode = [select Account_Currency_Iso_Code__c from Account where id = :agencyid].Account_Currency_Iso_Code__c;
			
			system.debug('@agencyid: ' + agencyid);
			system.debug('@isocode: ' + isocode);
			
			API_v1_currenciesHandler ch = new API_v1_currenciesHandler();
			return ch.getAgencyCurrencies(agencyid, isocode);
			
		} catch (Exception e) {
			
			api_manager.sendExceptionEmail(api_manager.getExceptionMessage(e));
			RestContext.response.statusCode = api_manager.CODE_INTERNAL_SERVER_ERROR;
			return null;
		}
		
		
	}
}