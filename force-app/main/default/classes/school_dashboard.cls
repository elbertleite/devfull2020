public with sharing class school_dashboard {
    public string schoolId;
    public static final String IP_GLOBAL_LINK = '0019000001O9Y3XAAV'; //REMOVE WHEN OTHER GROUPS COME 
    
    public school_dashboard() {}

    public static List<Account> retrieveAgencyGroups(List<String> ids){
        String query = 'SELECT ID, Name FROM Account WHERE RecordType.Name = \'Agency Group\' AND Global_Link__c = :IP_GLOBAL_LINK AND Inactive__c = false';
        if(ids != null && !ids.isEmpty() && !ids.contains('all')){
            query = query + ' AND ID in :ids ';
        }
        query = query + ' ORDER BY Name';
        List<Account> response = Database.query(query);
        if(Test.isRunningTest()){
            Account agencyGroup = new Account();
            agencyGroup.Name = 'EHF Group';
            response.add(agencyGroup);
        }
        return response;
    }

    @RemoteAction
    public static Map<String, Object> initPage(String idAccount){
        Account acc = [SELECT ID, Name, RecordType.Name FROM Account WHERE ID = :idAccount];
        Map<String, Object> response = new Map<String, Object>();
        List<IPClasses.SelectOptions> groups = new List<IPClasses.SelectOptions>();
        groups.add(new IPClasses.SelectOptions('all', 'All Groups'));
        for(Account accGroup : retrieveAgencyGroups(new List<String>{'all'})){
            groups.add(new IPClasses.SelectOptions(accGroup.ID, accGroup.Name));
        } 
        response.put('accountType', acc.RecordType.Name);
        response.put('groups', groups);
        return response;
    }

    @ReadOnly
    @RemoteAction
    public static Map<String, Object> generateDashboard(String idAccount, String groupResultBy, Integer minYear, Integer maxYear, List<String> groupIDS){
        Account acc = [SELECT ID, Name, RecordType.Name FROM Account WHERE ID = :idAccount];
        
        Set<String> yearsSearch = new Set<String>();
        Set<String> groups = new Set<String>();
        Map<String, Object> response = new Map<String, Object>();
        
        Map<String, Map<String, Integer>> totalQuotes = new Map<String, Map<String, Integer>>();
        Map<String, Map<String, Integer>> totalEnrolments = new Map<String, Map<String, Integer>>();
        Map<String, Integer> defaultValues = new Map<String, Integer>();

        List<Enrolment> clientEnrolments = retrieveEnrolments(acc, minYear, maxYear, groupIDS);
        List<Quotation_Detail__c> quotations = retrieveQuotationDetails(acc, minYear, maxYear, groupIDS);
        List<Campus_Course__c> courses = retrieveCourses(acc);
                
        String year;

        List<Account> agencyGroups = retrieveAgencyGroups(groupIDS);
        
        if(groupResultBy == 'group'){
            for(Account agGroup : agencyGroups){
                groups.add(agGroup.Name);
                totalQuotes.put(agGroup.Name, new Map<String, Integer>());
                totalEnrolments.put(agGroup.Name, new Map<String, Integer>());
                for(Integer i = minYear; i <= maxYear; i++){
                    year = String.valueOf(i);
                    yearsSearch.add(year);
                    totalQuotes.get(agGroup.Name).put(year, 0);
                    totalEnrolments.get(agGroup.Name).put(year, 0);
                    if(!defaultValues.containsKey(year)){
                        defaultValues.put(year, 0);
                    }
                }
            }
        }else{
            String key = 'year';
            totalQuotes.put(key, new Map<String, Integer>());
            totalEnrolments.put(key, new Map<String, Integer>());
            for(Integer i = minYear; i <= maxYear; i++){
                year = String.valueOf(i);
                yearsSearch.add(year);
                totalQuotes.get(key).put(year, 0);
                totalEnrolments.get(key).put(year, 0);
                defaultValues.put(year, 0);
            }
        }

        Map<String, CourseResponse> courseResponse = new Map<String, CourseResponse>();
        CourseResponse cResponse;
        Integer totalCourses = 0;
        for(Campus_Course__c cc : courses){
            cResponse = new CourseResponse();
            cResponse.name = cc.course__r.name;
            cResponse.available = cc.Is_Available__c;
            cResponse.show = totalCourses < 10;
            cResponse.quotations = new Map<String, Map<String, Integer>>();
            cResponse.enrolments = new Map<String, Map<String, Integer>>();
            if(groupResultBy == 'year'){
                cResponse.quotations.put('year', defaultValues.clone());
                cResponse.enrolments.put('year', defaultValues.clone());
            }else{
                for(String groupName : groups){
                    cResponse.quotations.put(groupName, defaultValues.clone());    
                    cResponse.enrolments.put(groupName, defaultValues.clone());
                }
            }
            courseResponse.put(cResponse.name, cResponse);
            totalCourses += 1;         
        }

        String keyGrouping;
        Integer value;
        for(Quotation_Detail__c qd : quotations){
            year = String.valueOf(qd.createdDate.year());
            keyGrouping = groupResultBy == 'year' ? 'year' : qd.Quotation__r.Agency__r.Parent.Name;
            
            value = totalQuotes.get(keyGrouping).get(year);
            totalQuotes.get(keyGrouping).put(year, value + 1);
            if(courseResponse.containsKey(qd.Campus_Course__r.Course__r.Name)){
                value = courseResponse.get(qd.Campus_Course__r.Course__r.Name).quotations.get(keyGrouping).get(year);
                courseResponse.get(qd.Campus_Course__r.Course__r.Name).quotations.get(keyGrouping).put(year, value + 1);
            }                
        } 
        for(Enrolment enrol : clientEnrolments){
            year = String.valueOf(enrol.dateEnrolment.year());
            keyGrouping = groupResultBy == 'year' ? 'year' : enrol.agency;

            value = totalEnrolments.get(keyGrouping).get(year); 
            totalEnrolments.get(keyGrouping).put(year, value + 1);              
            if(courseResponse.containsKey(enrol.course)){
                value = courseResponse.get(enrol.course).enrolments.get(keyGrouping).get(year);
                courseResponse.get(enrol.course).enrolments.get(keyGrouping).put(year, value + 1);
            }
        }

        /*List<Map<String, Object>> quotesGraph = new List<Map<String, Object>>();
        Map<String, Object> mapResponse = null;
        for(String keyMap : totalQuotes.keySet()){
            mapResponse = new Map<String, Object>();
            mapResponse.put('key', groupResultBy == 'year' ? 'Quotes' : keyMap.capitalize());
            for(String innerKey : totalQuotes.get(keyMap).keySet()){
                mapResponse.put(innerKey, totalQuotes.get(keyMap).get(innerKey));
            }
            quotesGraph.add(mapResponse);
        }

        List<Map<String, Object>> enrolmentsGraph = new List<Map<String, Object>>();
        for(String keyMap : totalEnrolments.keySet()){
            mapResponse = new Map<String, Object>();
            mapResponse.put('key', groupResultBy == 'year' ? 'Enrolments' : keyMap.capitalize());
            for(String innerKey : totalEnrolments.get(keyMap).keySet()){
                mapResponse.put(innerKey, totalEnrolments.get(keyMap).get(innerKey));
            }
            enrolmentsGraph.add(mapResponse);
        }*/

        List<Map<String, Object>> responseGraph = new List<Map<String, Object>>();
        Map<String, Object> mapResponse = null;
        Set<String> responseGraphModel = null;
        String key;
        
        /*for(String keyMap : totalQuotes.keySet()){
            mapResponse = new Map<String, Object>(); 
            if(groupResultBy == 'year'){
                key = yearsSearch.size() == 1 ? 'Enrolments x Quotations: '+minYear : 'Enrolments x Quotations: '+minYear+' - '+maxYear;
            }else{
                key = keyMap.capitalize();
            }
            mapResponse.put('key', key);
            for(String innerKey : totalQuotes.get(keyMap).keySet()){
                mapResponse.put(innerKey+'--quotations', totalQuotes.get(keyMap).get(innerKey));
            }
            for(String innerKey : totalEnrolments.get(keyMap).keySet()){
                mapResponse.put(innerKey+'--enrolments', totalEnrolments.get(keyMap).get(innerKey));
            }
            responseGraph.add(mapResponse);   
        }*/
        if(groupResultBy == 'year'){
            for(String yearSearch : yearsSearch){
                mapResponse = new Map<String, Object>();
                mapResponse.put('key', yearSearch);        
                mapResponse.put('quotations', totalQuotes.get('year').get(yearSearch));        
                mapResponse.put('enrolments', totalEnrolments.get('year').get(yearSearch));        
                
                responseGraph.add(mapResponse);
                
                if(responseGraphModel == null){
                    responseGraphModel = mapResponse.keySet();
                }
            }
        }else{
            String lastYear = String.valueOf(maxYear);
            for(Account agGroup : agencyGroups){
                mapResponse = new Map<String, Object>();
                mapResponse.put('key', agGroup.Name);
                for(String yearSearch : yearsSearch){
                    mapResponse.put(yearSearch+'--quotations', totalQuotes.get(agGroup.Name).get(yearSearch));    
                    mapResponse.put(yearSearch+'--enrolments', totalEnrolments.get(agGroup.Name).get(yearSearch));    
                    if(yearsSearch.size() > 1 && yearSearch != lastYear){
                        mapResponse.put(yearSearch+'--xGapField', 0);    
                    }
                }
                responseGraph.add(mapResponse);

                if(responseGraphModel == null){
                    responseGraphModel = mapResponse.keySet();
                }
            }
        }

        response.put('yearsSearch', yearsSearch);
        response.put('agencyGroupsSearch', groups);
        response.put('responseGraph', responseGraph);
        response.put('responseGraphModel', responseGraphModel);
        response.put('quotesPerCourse', courseResponse.values());
        response.put('totalCourses', courseResponse.size());
        return response;
    }

    public static Map<String, Integer> retrieveAgencyGroupsFromQuotations(Account acc, Integer minYear, Integer maxYear){
        String id = acc.ID;
        Map<String, Integer> groups = new Map<String, Integer>();
        String query = 'SELECT Quotation__r.Agency__r.Parent.Name agencyGroup, count(ID) FROM Quotation_Detail__c WHERE CALENDAR_YEAR(createdDate) >= :minYear and CALENDAR_YEAR(createdDate) <= :maxYear ';
        if(acc.RecordType.Name == 'Campus'){
            query = query + ' AND campus__c = :ID ';
        }else{
            query = query + ' AND campus__r.parent.id = :ID ';
        }
        query = query + ' AND Quotation__r.Agency__r.Parent.Inactive__c = false GROUP BY Quotation__r.Agency__r.Parent.Name ORDER BY Quotation__r.Agency__r.Parent.Name';
        for(AggregateResult ar : Database.query(query)){
            groups.put((String)ar.get('agencyGroup'), 0);
        }
        return groups;
    }

    public static List<Quotation_Detail__c> retrieveQuotationDetails(Account acc, Integer minYear, Integer maxYear, List<String> groupIDS){
        String id = acc.ID;
        String query = 'SELECT Quotation__r.Agency__r.Parent.Name, Course_Name__c, createdDate, Campus_Course__r.Course__r.Name FROM Quotation_Detail__c WHERE CALENDAR_YEAR(createdDate) >= :minYear and CALENDAR_YEAR(createdDate) <= :maxYear ';
        if(acc.RecordType.Name == 'Campus'){
            query = query + ' AND campus__c = :ID ';
        }else{
            query = query + ' AND campus__r.parent.id = :ID ';
        }
        if(groupIDS != null && !groupIDS.isEmpty() && !groupIDS.contains('all')){
            query = query + ' AND Quotation__r.Agency__r.Parent.ID IN :groupIDS ';
        }
        query = query + 'AND Quotation__r.Agency__r.Parent.Global_Link__c = :IP_GLOBAL_LINK ORDER BY Course_Name__c';
        return Database.query(query);
    }

    public static List<Enrolment> retrieveEnrolments(Account acc, Integer minYear, Integer maxYear, List<String> groupIDS){
        String id = acc.ID;
        String query = 'SELECT Client_Course__r.Campus_Course__r.Course__r.Name, Client_Course__c, Client_Course__r.Enroled_by_Agency__r.Parent.Name, Client_Course__r.Enrolment_Date__c FROM client_course_instalment__c WHERE Client_Course__r.Enrolment_Date__c != null AND CALENDAR_YEAR(Client_Course__r.Enrolment_Date__c) >= :minYear and CALENDAR_YEAR(Client_Course__r.Enrolment_Date__c) <= :maxYear and Received_Date__c != null and isCancelled__c = false and isPaymentConfirmed__c = true ';
        if(acc.RecordType.Name == 'Campus'){
            query = query + ' AND Client_Course__r.campus_course__r.campus__c = :ID ';
        }else{
            query = query + ' AND Client_Course__r.campus_course__r.campus__r.parent.id = :ID ';
        }
        if(groupIDS != null && !groupIDS.isEmpty() && !groupIDS.contains('all')){
            query = query + ' AND Client_Course__r.Enroled_by_Agency__r.Parent.ID IN :groupIDS ';
        }
        query = query + 'AND Client_Course__r.Enroled_By_Agency__r.Parent.Global_Link__c = :IP_GLOBAL_LINK ORDER BY Client_Course__r.Enrolment_Date__c ';

        Set<String> ids = new Set<String>();
        List<Enrolment> response = new List<Enrolment>(); 
        Enrolment enrolment;
        for(client_course_instalment__c instalment : Database.query(query)){
            if(!ids.contains(instalment.Client_Course__c)){
                enrolment = new Enrolment();
                enrolment.id = instalment.Client_Course__c;
                enrolment.agency = instalment.Client_Course__r.Enroled_by_Agency__r.Parent.Name;
                enrolment.course = instalment.Client_Course__r.Campus_Course__r.Course__r.Name;
                enrolment.dateEnrolment = instalment.Client_Course__r.Enrolment_Date__c;

                response.add(enrolment);
            }
            ids.add(instalment.Client_Course__c);
        }
        return response;
    }

    public static List<Campus_Course__c> retrieveCourses(Account acc){
        String query;
        String id = acc.ID;
        List<Campus_Course__c> courses = new List<Campus_Course__c>();
        Set<String> idCourses = new Set<String>();
        if(acc.RecordType.Name == 'Campus'){
            query = 'SELECT course__c, course__r.name, Is_Available__c FROM campus_course__c WHERE campus__c = :id ORDER BY course__r.name';
        }else{
            query = 'SELECT course__c, course__r.name, Is_Available__c FROM campus_course__c WHERE campus__r.parent.id = :id ORDER BY course__r.name';
        }
        for(Campus_Course__c cc : Database.query(query)){
            if(!String.isEmpty(cc.course__r.name) && !idCourses.contains(cc.course__c)){
                courses.add(cc);
                idCourses.add(cc.course__c);
            }
        }
        return courses;
    }

    public class ChartResponse{
        public String key{get;set;}
        public String label{get;set;}
        public Integer value{get;set;}

        public ChartResponse(String key, String label, Integer value){
            this.label = label;
            this.key = key;
            this.value = value;
        }
    }

    public class Enrolment{
        public String id{get;set;}
        public String agency{get;set;}
        public String course{get;set;}
        public Datetime dateEnrolment{get;set;}
    }

    public class CourseResponse{
        public String name{get;set;}
        public Boolean available{get;set;}
        public Boolean show{get;set;}
        public Map<String, Map<String, Integer>> quotations{get;set;}
        public Map<String, Map<String, Integer>> enrolments{get;set;}

        public CourseResponse(){}

        /*public CourseResponse(String name, Map<String, Map<String, Integer>> values, Boolean available, Boolean show){
            this.name = name;
            this.values = values;
            this.available = available;
            this.show = show;
        }*/

        public Boolean equals(Object obj) {
        if (obj instanceof CourseResponse) {
            CourseResponse n = (CourseResponse) obj;
				if(!String.isEmpty(this.name)){
                	return this.name == n.name;
				}
				return false;
            }
            return false;
        }

        public Integer hashCode() {
            return name.hashCode();
        }
    }
}