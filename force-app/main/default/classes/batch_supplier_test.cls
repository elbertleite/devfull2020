/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class batch_supplier_test {

    static testMethod void myUnitTest() {
        
        TestFactory tf = new TestFactory();
		Account agencyGroup = tf.createAgencyGroup();
		Account agency1 = tf.createSimpleAgency(agencyGroup);
		Account agency2 = tf.createSimpleAgency(agencyGroup);
		
		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency1);
		Supplier__c s = new Supplier__c();
		s.agency__c = agency2.id;
		s.supplier__c = campus.id;
		s.record_type__c = 'campus';
		insert s;
		
		Set<String> agencies = new Set<String>{ agency1.id, agency2.id };
		
		Map<String, Decimal> rankingMap = new Map<String, Decimal>{ campus.id => 4.5 };
		
		batch_supplier b = new batch_supplier(agencies, 'Rank__c', rankingMap);		
		Database.executeBatch(b);
        
        
    }
}