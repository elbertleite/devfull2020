public class agreement_commissions {

	
	public map<string,list<ruleDetails>> mapRules{get{if(mapRules == null) mapRules = new map<string,list<ruleDetails>>(); return mapRules;} set;}

	public class ruleDetails implements Comparable {
		public integer key{get{if(key == null) key = 0; return key;} set;}
		public integer fromUnit{get{if(fromUnit == null) fromUnit = 0; return fromUnit;} set;}
		public integer control{get{if(control == null) control = 0; return control;} set;}
		public integer action{get{if(action == null) action = 0; return action;} set;}
		public decimal value{get{if(value == null) value = 0; return value;} set;}
		public string details{get{if(details == null) details = ''; return details;} set;}
		public ruleDetails(integer key, integer fromUnit, integer control, integer action, decimal value, string details){
			this.key = key;
			this.fromUnit = fromUnit;
			this.control = control;
			this.action = action;
			this.value = value;
			this.details = details;
		}

		public Integer compareTo(Object compareTo){
			ruleDetails rd = (ruleDetails) compareTo;

			if(this.fromUnit == rd.fromUnit) return 0;
			if(this.fromUnit > rd.fromUnit) return 1;
			return -1;

		}
	}

	public Boolean isAvailable {get;set;}
	private map<String,String> pfsSchool{get{
		if(pfsSchool == null){
			pfsSchool = new map<String,String>();
			pfsSchool.put('0', 'Net Commission');
			pfsSchool.put('1', 'PFS Only');
			pfsSchool.put('2', 'PFS First Instalment');
		}
	return pfsSchool;}set;}

	//CONSTRUCTOR
	public map<String, schSetup> mapSetup {get;set;}
	private String agreeId {get;set;}
	public agreement_commissions(){
		mapSchoolAgreement = new map<Id,Id>();
		mapSetup = new map<String, schSetup>();

		agreeId = ApexPages.currentPage().getParameters().get('id');
		agreeSchools = new list<Id>();
		for(Agreement_Accounts__c ac : [SELECT Account__c, Account__r.Name, School_Payment_Type__c, Commission_Includes_Tax__c, Tax_Claimable__c, Agreement__r.isAvailable__c FROM Agreement_Accounts__c WHERE Agreement__c = :agreeId and Account__r.RecordType.Name = 'School']){
			mapSchoolAgreement.put(ac.Account__c, ac.Id);
			agreeSchools.add(ac.Id);
			mapSetup.put(ac.Account__c, new schSetup(ac.Account__r.Name, ac.School_Payment_Type__c, ac.Commission_Includes_Tax__c, ac.Tax_Claimable__c));
			isAvailable = ac.Agreement__r.isAvailable__c;
		}//end for

		findSchoolCommissions();
	}

	public PageReference redirectWizard(){
		PageReference pageref = new PageReference('/apex/agreements_new');
		pageref.getParameters().put('id', agreeId);
		pageref.getParameters().put('st', ApexPages.currentPage().getParameters().get('stepNum'));
		pageref.setRedirect(true);

		return pageref;
	}
	
	public class schSetup{
		public String schoolName {get;set;}
		public String paymentType {get;set;}
		public Boolean includesTax {get;set;}
		public Boolean claimTax {get;set;}

		public schSetup(String schoolName, String paymentType, Boolean includesTax, Boolean claimTax){
			this.schoolName = schoolName;
			this.paymentType = paymentType;
			this.includesTax = includesTax;
			this.claimTax = claimTax;
		}
	}
	private list<IPFunctions.LogHistory> logs {get;set;}
	public void saveCommission(){
    	list<commission__c> commissionsUpdate = new list<commission__c>();
    	list<commission__c> commissionsDelete = new list<commission__c>();
    	list<Agreement_Accounts__c> upAgreeAccounts = new list<Agreement_Accounts__c>();

		//Logs History Constructor
		logs = new list<IPFunctions.LogHistory>(); 

		User currentUser = [SELECT Name, Contact.Account.Name FROM User WHERE id = :userInfo.getUserId()];
		Datetime GMT = system.now();
		String editDate = GMT.format('yyyy-MM-dd HH:mm:ss', 'UTC');

		//
		Agreement__c upAgreement = new Agreement__c(Id = agreeId, isAvailable__c = Boolean.valueOf(ApexPages.currentPage().getParameters().get('isAvailable')));
		//Log: is Available
		if(isAvailable != upAgreement.isAvailable__c)
			logs.add(new IPFunctions.LogHistory('isAvailable__c','Is Available', string.valueOf(isAvailable), string.valueOf(upAgreement.isAvailable__c), currentUser.Name, currentUser.Contact.Account.Name, editDate));
		//
		update upAgreement;

		map<Decimal,String> commType = new map<Decimal,String>{0 => '%', 1 => '$'};
    	
		for(commissionSchoolDetails school:coursesCommission){

			List<String> setup = school.schSetup.split(';;');	
			system.debug('setup--->' + setup);
			system.debug('school.schoolId--->' + mapSchoolAgreement.get(school.schoolId));

			upAgreeAccounts.add(
				new Agreement_Accounts__c(
					Id = mapSchoolAgreement.get(school.schoolId),
					School_Payment_Type__c = pfsSchool.get(setup[0]),
					Commission_Includes_Tax__c = Boolean.valueOf(setup[1]),
					Tax_Claimable__c = Boolean.valueOf(setup[2])
				)
			);

			//Logs for School Setup
				String schName = mapSetup.get(school.schoolId).schoolName;

				if(mapSetup.get(school.schoolId).paymentType != pfsSchool.get(setup[0]))
					logs.add(new IPFunctions.LogHistory('PFS School',schName, string.valueOf(mapSetup.get(school.schoolId).paymentType), pfsSchool.get(setup[0]), currentUser.Name, currentUser.Contact.Account.Name, editDate));

				if(mapSetup.get(school.schoolId).includesTax != Boolean.valueOf(setup[1]))
					logs.add(new IPFunctions.LogHistory('Comm. Includes Tax',schName, string.valueOf(mapSetup.get(school.schoolId).includesTax), setup[1], currentUser.Name, currentUser.Contact.Account.Name, editDate));

				if(mapSetup.get(school.schoolId).claimTax != Boolean.valueOf(setup[2]))
					logs.add(new IPFunctions.LogHistory('Tax Claimable',schName, string.valueOf(mapSetup.get(school.schoolId).claimTax), setup[2], currentUser.Name, currentUser.Contact.Account.Name, editDate));
			// END -- Logs for School Setup

			//Logs: School Commission OR School Commission Type
			if(school.SchoolCommissionObj.School_Commission__c != school.oldSchoolCommissionObj.School_Commission__c || school.SchoolCommissionObj.School_Commission_Type__c != school.oldSchoolCommissionObj.School_Commission_Type__c)
				logs.add(new IPFunctions.LogHistory('School Commission', schName + ' - School Commission ' , school.oldSchoolCommissionObj.School_Commission__c + ' ' + commType.get(school.oldSchoolCommissionObj.School_Commission_Type__c), school.SchoolCommissionObj.School_Commission__c + ' ' + commType.get(school.SchoolCommissionObj.School_Commission_Type__c), currentUser.Name, currentUser.Contact.Account.Name, editDate));
			//

    		if(school.SchoolCommissionObj.School_Commission__c != null && school.SchoolCommissionObj.School_Commission__c != 0){
    			school.SchoolCommissionObj.Agreement_Account__c = mapSchoolAgreement.get(school.schoolId);
    			school.SchoolCommissionObj.School_Control__c = school.schoolId;
    			commissionsUpdate.add(school.SchoolCommissionObj);
    		} else if (school.SchoolCommissionObj.School_Commission__c == 0 && mpc.containsKey(school.SchoolCommissionObj.school_Id__c)){
    			commissionsDelete.add(school.SchoolCommissionObj);
    		}
    		for(commissionCampusDetails campus:school.listCampus){
				//Logs: Campus Commission OR Campus Commission Type
					if(campus.campusCommissionObj.Campus_Commission__c != campus.oldCampusCommissionObj.Campus_Commission__c || campus.campusCommissionObj.Campus_Commission_Type__c != campus.oldCampusCommissionObj.Campus_Commission_Type__c)
						logs.add(new IPFunctions.LogHistory('Campus Commission', campus.CampusName + ' - Commission ' , campus.oldCampusCommissionObj.Campus_Commission__c + ' ' + commType.get(campus.oldCampusCommissionObj.Campus_Commission_Type__c), campus.campusCommissionObj.Campus_Commission__c + ' ' + commType.get(campus.campusCommissionObj.Campus_Commission_Type__c), currentUser.Name, currentUser.Contact.Account.Name, editDate));
				//
    			if(campus.campusCommissionObj.Campus_Commission__c != null && campus.campusCommissionObj.Campus_Commission__c != 0){
					campus.campusCommissionObj.Agreement_Account__c = mapSchoolAgreement.get(school.schoolId);
	    			campus.campusCommissionObj.School_Control__c = school.schoolId;
	    			campus.campusCommissionObj.Campus_Control__c = campus.campusCommissionObj.campus_Id__c;
    				commissionsUpdate.add(campus.campusCommissionObj);
    			}else if(campus.campusCommissionObj.Campus_Commission__c == 0 && mpc.containsKey(campus.campusCommissionObj.campus_Id__c)){
    				commissionsDelete.add(campus.campusCommissionObj);
    			}
    			for(commissioncourseTypeDetails courseType:campus.listCourseType){
					//Logs: Campus Commission OR Campus Commission Type
						if(courseType.courseTypeCommissionObj.Course_Type_Commission__c != courseType.oldCourseTypeCommissionObj.Course_Type_Commission__c || courseType.courseTypeCommissionObj.Course_Type_Commission_Type__c != courseType.oldCourseTypeCommissionObj.Course_Type_Commission_Type__c)
							logs.add(new IPFunctions.LogHistory('Course Type Commission', campus.CampusName + ' - ' + courseType.courseType + ' - Commission ' , courseType.oldCourseTypeCommissionObj.Course_Type_Commission__c + ' ' + commType.get(courseType.oldCourseTypeCommissionObj.Course_Type_Commission_Type__c), courseType.courseTypeCommissionObj.Course_Type_Commission__c + ' ' + commType.get(courseType.courseTypeCommissionObj.Course_Type_Commission_Type__c), currentUser.Name, currentUser.Contact.Account.Name, editDate));
					//
    				if(courseType.courseTypeCommissionObj.Course_Type_Commission__c != null && courseType.courseTypeCommissionObj.Course_Type_Commission__c != 0){
		    			courseType.courseTypeCommissionObj.Agreement_Account__c = mapSchoolAgreement.get(school.schoolId);
		    			courseType.courseTypeCommissionObj.School_Control__c = school.schoolId;
    					commissionsUpdate.add(courseType.courseTypeCommissionObj);
    				}else if(courseType.courseTypeCommissionObj.Course_Type_Commission__c == 0 && mpc.containsKey(courseType.courseTypeCommissionObj.CampusCourseTypeId__c)){
    					commissionsDelete.add(courseType.courseTypeCommissionObj);
    				}
					
    				for(Commission__c course:courseType.listCourseCommission){
						//Logs: Course Commission OR Course Commission Type
						if(course.Course_Commission__c != courseType.oldCourseCommission.get(course.Id).Course_Commission__c || course.Course_Commission_Type__c != courseType.oldCourseCommission.get(course.Id).Course_Commission_Type__c)
							logs.add(new IPFunctions.LogHistory('Course Commission', campus.CampusName + ' - ' + courseType.courseType + ' - '  + course.Course_Name__c + ' - Commission', courseType.oldCourseCommission.get(course.Id).Course_Commission__c + ' ' + commType.get(courseType.oldCourseCommission.get(course.Id).Course_Commission_Type__c), course.Course_Commission__c + ' ' + commType.get(course.Course_Commission_Type__c), currentUser.Name, currentUser.Contact.Account.Name, editDate));
						//
    					if(course.Course_Commission__c != null && course.Course_Commission__c != 0){
			    			System.debug('==>course: '+course);
			    			course.Agreement_Account__c = mapSchoolAgreement.get(school.schoolId);
			    			course.School_Control__c = school.schoolId;
    						commissionsUpdate.add(course);
    					}
    					else if(course.Course_Commission__c == 0 && mpc.containsKey(course.Campus_Course__c)){
    						commissionsDelete.add(course);
    					}
    				}//end for
    			}
    		}
    	}//end for

		update upAgreeAccounts;

    	if(commissionsDelete.size() > 0)
    		delete commissionsDelete;

    	if(commissionsUpdate.size() > 0)
    		upsert commissionsUpdate;

    }

	public PageReference saveLogs(){
		system.debug('saveLogs==>' + logs);

		String previusLog = agreements_new.findAgreementLog(agreeId);
		system.debug('previusLog===>' + previusLog);

		if(previusLog != null){
			list<IPFunctions.LogHistory> logsList = (list<IPFunctions.LogHistory>) JSON.deserialize(previusLog, list<IPFunctions.LogHistory>.class);
			logsList.addAll(logs);
			agreements_new.saveLogAmazonS3(JSON.serialize(logsList), agreeId);
		}
		else{
			agreements_new.saveLogAmazonS3(JSON.serialize(logs), agreeId);
		}
		
		PageReference pr = new PageReference('/apex/agreements_list');
		pr.setRedirect(true);

		return pr;
	}


	public Commission__c commission{get; set;}

	private map<string, Commission__c> mpc;
	private list<Id> schoolIds {get;set;}
	private map<string, Commission__c> mapCommission(){
		mpc = new map<string, commission__c>();
		for(Commission__c cm:[SELECT Rules__c, Campus_Course__c, school_Id__c, campus_Id__c, CampusCourseTypeId__c, courseType__c, Comments__c, Campus_Course__r.campus__r.name, Campus_Course__r.course__r.name, Campus_Commission__c, Course_Commission__c, Course_Type_Commission__c, School_Commission__c, School_Commission_Type__c, Campus_Commission_Type__c, Course_Commission_Type__c, Course_Type_Commission_Type__c, School_Control__c FROM Commission__c WHERE Agreement_Account__c IN :agreeSchools]){

			if(cm.Campus_Course__c != null)
				mpc.put(cm.Campus_Course__c, cm);
			if(cm.CampusCourseTypeId__c != null)
				mpc.put(cm.CampusCourseTypeId__c, cm);
			if(cm.campus_Id__c != null)
				mpc.put(cm.campus_Id__c, cm);
			if(cm.school_Id__c != null)
				mpc.put(cm.school_Id__c, cm);
		}
		return mpc;
	}

	public class commissionSchoolDetails{
		public integer counter {get;set;}
		public string schSetup {get;set;}
		public string schoolId {get;set;}
		public string schoolName {get;set;}
		public decimal schoolCommission {get;set;}
		public Commission__c SchoolCommissionObj{get;set;}
		private Commission__c oldSchoolCommissionObj{get;set;}
		public list<commissionCampusDetails> listCampus{get{if(listCampus == null) listCampus = new list<commissionCampusDetails>(); return listCampus;} set;}
	}

	public class commissionCampusDetails{
		public integer counter {get;set;}
		public string CampusName {get;set;}
		public decimal campusCommission {get;set;}
		public Commission__c campusCommissionObj {get;set;}
		private Commission__c oldCampusCommissionObj {get;set;}
		public list<commissioncourseTypeDetails> listCourseType{get{if(listCourseType == null) listCourseType = new list<commissioncourseTypeDetails>(); return listCourseType;} set;}
	}

	public class commissioncourseTypeDetails{
		public string courseType {get;set;}
		public decimal courseTypeCommission {get;set;}
		public Commission__c courseTypeCommissionObj {get;set;}
		private Commission__c oldCourseTypeCommissionObj {get;set;}
		public list<Commission__c> listCourseCommission{get{if(listCourseCommission == null) listCourseCommission = new list<Commission__c>(); return listCourseCommission;} set;}
		private map<String, Commission__c> oldCourseCommission {get;set;}
	}

	public String rulesKey {get;set;}
	public PageReference setRulesKey(){

		system.debug('++ruleskey++ ' + ApexPages.currentPage().getParameters().get('ruleskey'));
		rulesKey = ApexPages.currentPage().getParameters().get('ruleskey');

		return null;

	}

	public void refreshCommissions(){
		coursesCommission = null;
	}

	public map<string, string> schoolName{get{if(schoolName == null) schoolName = new map<string, string>(); return schoolName;} set;}
	public map<string, string> campusName{get{if(campusName == null) campusName = new map<string, string>(); return campusName;} set;}

	public list<Id> agreeSchools {get;set;}
	private map<Id, Id> mapSchoolAgreement {get;set;}

	public list<commissionSchoolDetails> coursesCommission {get;set;}
	private map<String, commissionSchoolDetails> oldCoursesCommission {get;set;}
	public list<commissionSchoolDetails> findSchoolCommissions(){
		system.debug('agreeSchools--->' + agreeSchools);
		mpc = null;
		mapCommission();
		coursesCommission = new list<commissionSchoolDetails>();
		oldCoursesCommission = new map<String, commissionSchoolDetails>();
		map<string,map<string,map<string,list<Commission__c>>>> mapCommission = new map<string,map<string,map<string,list<Commission__c>>>>();
		Commission__c cd;
		for(Campus_Course__c cc:[Select Campus__r.parentId, Campus__c, Campus__r.parent.name, Campus__r.name, course__r.name, Course__r.Course_Type__c, Id from Campus_Course__c WHERE Campus__r.ParentId IN :mapSchoolAgreement.keySet() order by Campus__r.parent.name, Campus__r.name, Course__r.Course_Type__c, course__r.name]){

			if(!mapCommission.containsKey(cc.Campus__r.parentId)){//school
				mapRules.put(cc.Campus__r.parentId, new list<ruleDetails>());
				mapRules.put(cc.Campus__c, new list<ruleDetails>());
				mapRules.put(cc.Campus__c+cc.Course__r.Course_Type__c, new list<ruleDetails>());
				mapRules.put(cc.id, new list<ruleDetails>());
				schoolName.put(cc.Campus__r.parentId, cc.Campus__r.parent.name);
				campusName.put(cc.Campus__c, cc.Campus__r.name);
				cd = returnCommission(cc.id, cc.course__r.name);
				cd.Campus_Name__c = cc.Campus__r.name;
				cd.School_Name__c = cc.Campus__r.parent.name;
				cd.Campus_Course__c = cc.id;
				cd.Campus_Control__c = cc.campus__c;
				cd.Course_Type_Control__c = cc.Course__r.Course_Type__c;
				mapCommission.put(cc.Campus__r.parentId, new map<string,map<string,list<Commission__c>>>{cc.Campus__c => new map<string,list<Commission__c>>{cc.Course__r.Course_Type__c => new list<Commission__c>{cd}}});
			}

			else if(!mapCommission.get(cc.Campus__r.parentId).containsKey(cc.Campus__c)){//campus
				campusName.put(cc.Campus__c, cc.Campus__r.name);
				cd = returnCommission(cc.id, cc.course__r.name);
				cd.Campus_Name__c = cc.Campus__r.name;
				cd.School_Name__c = cc.Campus__r.parent.name;
				cd.Campus_Course__c = cc.id;
				cd.Campus_Control__c = cc.campus__c;
				cd.Course_Type_Control__c = cc.Course__r.Course_Type__c;
				mapCommission.get(cc.Campus__r.parentId).put(cc.Campus__c, new map<string,list<Commission__c>>{cc.Course__r.Course_Type__c => new list<Commission__c>{cd}});
				
			}
			else if(!mapCommission.get(cc.Campus__r.parentId).get(cc.Campus__c).containsKey(cc.Course__r.Course_Type__c)){//course type
				
				cd = returnCommission(cc.id, cc.course__r.name);
				cd.Campus_Name__c = cc.Campus__r.name;
				cd.School_Name__c = cc.Campus__r.parent.name;
				cd.Campus_Course__c = cc.id;
				cd.Campus_Control__c = cc.campus__c;
				cd.Course_Type_Control__c = cc.Course__r.Course_Type__c;
				System.debug('==>Course_Type__c: '+cc.Campus__c+cc.Course__r.Course_Type__c);
				mapCommission.get(cc.Campus__r.parentId).get(cc.Campus__c).put(cc.Course__r.Course_Type__c, new list<Commission__c>{cd}); //course
				
			}
			else {//course
				cd = returnCommission(cc.id, cc.course__r.name);
				cd.Campus_Name__c = cc.Campus__r.name;
				cd.School_Name__c = cc.Campus__r.parent.name;
				cd.Campus_Course__c = cc.id;
				cd.Campus_Control__c = cc.campus__c;
				cd.Course_Type_Control__c = cc.Course__r.Course_Type__c;
				mapCommission.get(cc.Campus__r.parentId).get(cc.Campus__c).get(cc.Course__r.Course_Type__c).add(cd);
				
			}

		}

		//System.debug('==>mapRules: '+mapRules);

		list<commissionSchoolDetails> listSchool;
		list<commissionCampusDetails> listCampuses;
		list<commissioncourseTypeDetails> listCourseType;

		listSchool = new list<commissionSchoolDetails>();
		integer counter = 0;
		for(string school:mapCommission.keySet()){
			counter ++;	
			commissionSchoolDetails csd = new commissionSchoolDetails();
			csd.counter = counter ;
			csd.schoolId = school;
			csd.schoolName = schoolName.get(school);
			csd.SchoolCommissionObj = returnCommission(school, null);
			csd.oldSchoolCommissionObj = csd.SchoolCommissionObj.clone(false,true); //old school commission
			csd.SchoolCommissionObj.school_Id__c = school;
			csd.SchoolCommissionObj.school_name__c = csd.schoolName;
			listCampuses = new list<commissionCampusDetails>();
			integer cCounter = 0;
			for(string campus:mapCommission.get(school).keySet()){
				cCounter++;

				commissionCampusDetails ccd = new commissionCampusDetails();
				ccd.counter = cCounter;
				ccd.CampusName = campusName.get(campus);
				ccd.campusCommissionObj = returnCommission(campus, null);
				ccd.oldCampusCommissionObj = ccd.campusCommissionObj.clone(false,true); //old campus commission
				ccd.campusCommissionObj.school_name__c = csd.schoolName;
				ccd.campusCommissionObj.campus_name__c = ccd.CampusName;
				ccd.campusCommissionObj.campus_Id__c = campus;
				listCourseType = new list<commissioncourseTypeDetails>();
				for(string courseType:mapCommission.get(school).get(campus).keySet()){
					commissioncourseTypeDetails cct = new commissioncourseTypeDetails();
					cct.courseType = courseType;
					cct.courseTypeCommissionObj = returnCommission(campus+courseType, null);
					cct.oldCourseTypeCommissionObj = cct.courseTypeCommissionObj.clone(false,true);  //old course type commission
					cct.courseTypeCommissionObj.CampusCourseTypeId__c = campus+courseType;
					cct.courseTypeCommissionObj.Campus_Control__c = campus;
					cct.courseTypeCommissionObj.school_name__c = csd.schoolName;
					cct.courseTypeCommissionObj.campus_name__c = ccd.CampusName;
					cct.courseTypeCommissionObj.Course_Type_Control__c = courseType;
					cct.listCourseCommission.addAll(mapCommission.get(school).get(campus).get(courseType));

					cct.oldCourseCommission = new map<String, Commission__c>();
					for(Commission__c course : cct.listCourseCommission)
						cct.oldCourseCommission.put(course.id, course.clone(false, true)); //old course  commission

					listCourseType.add(cct);
				}
				ccd.listCourseType.addAll(listCourseType);
				listCampuses.add(ccd);
			}
			csd.listCampus.addAll(listCampuses);
			coursesCommission.add(csd);
		}
		return coursesCommission;
	}


	private Commission__c returnCommission(string cc, string courseName){
		Commission__c commission;
 		if(mpc.containsKey(cc)){
 			commission = mpc.get(cc);
 			commission.Course_Name__c = courseName;
 		}else commission = new Commission__c(Course_Name__c = courseName, Campus_Commission__c = 0, Course_Commission__c = 0, School_Commission__c = 0, Course_Type_Commission__c = 0,
 											School_Commission_Type__c = 0, Campus_Commission_Type__c = 0, Course_Commission_Type__c = 0, Course_Type_Commission_Type__c = 0);
 		return commission;
	}

	
	public integer fromUnit{get; set;}
	public decimal value{get; set;}
	public string details{get; set;}

	public string itemControl{get; set;}
	private List<SelectOption> optionsControl;
	public List<SelectOption> getItemsControl() {
        if(optionsControl == null){
			optionsControl = new List<SelectOption>();
	        optionsControl.add(new SelectOption('0','Weeks'));
	        optionsControl.add(new SelectOption('1','Enrollment'));
        }
        return optionsControl;
    }

    public string itemAction{get; set;}
    private List<SelectOption> optionsAction;
    public List<SelectOption> getItemsAction() {
    	if(optionsAction == null){
	        optionsAction = new List<SelectOption>();
	        optionsAction.add(new SelectOption('0','Increase Commission'));
	        optionsAction.add(new SelectOption('1','Agent Bonus'));
    	}
        return optionsAction;
    }

    public string commissionType{get; set;}
    private List<SelectOption> optionscommissionType;
    public List<SelectOption> getItemCommissionType() {
    	if(optionscommissionType == null){
	        optionscommissionType = new List<SelectOption>();
	        optionscommissionType.add(new SelectOption('0','%'));
	        optionscommissionType.add(new SelectOption('1','Cash'));
    	}
        return optionscommissionType;
    }

    private map<string,string> mapAction;
    public map<string,string> getmapAction(){
    	if(mapAction == null){
    		mapAction = new  map<string,string>();
    		mapAction.put('0','Increase Commission');
	    	mapAction.put('1','Agent Bonus');
    	}
	    return mapAction;
    }

    private map<string,string> mapControl;
    public map<string,string> getmapControl(){
    	if(mapControl == null){
    		mapControl = new  map<string,string>();
    		mapControl.put('0','Weeks');
	    	mapControl.put('1','Enrollment');
    	}
	    return mapControl;
    }
}