/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class EmailToS3Controller_test {

    static testMethod void myUnitTest() {
       	TestFactory factory = new TestFactory();		
		Account agency = factory.createAgency();
		Contact employee = factory.createEmployee(agency);
		User userEmp = factory.createPortalUser(employee);
		Contact client;
		Quotation__c quote;
		Account school = factory.createSchool();
		Account campus = factory.createCampus(school, agency);		    
		Course__c course = factory.createCourse();
		Campus_Course__c cc = factory.createCampusCourse(campus, course);
		
       system.runAs(userEmp){
       	
       	client = factory.createClient(agency); //Client			
		quote = factory.createQuotation(client, cc); //Quotation	
       	
       	//Email
		
		// create a new email and envelope object
		Messaging.InboundEmail emailFClient = new Messaging.InboundEmail() ;
		Messaging.InboundEmail emailFEmployee = new Messaging.InboundEmail() ;
		Messaging.InboundEmail emailFGoogle = new Messaging.InboundEmail() ;
		
	  	Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

		//Email from Client
		emailFClient.subject = ' Email / > < from client';
	  	emailFClient.fromname = 'Email from client';
	  	emailFClient.fromAddress = client.Email;
	  	emailFClient.toAddresses = new String[]{userEmp.Email};
	  	emailFClient.HtmlBody = 'Oi email from client yeii HtmlBody';
	  	emailFClient.plainTextBody = 'Oi email from client yeii plainTextBody';
	  	
	  	//Email From Employee
	  	emailFEmployee.subject = '';
	  	emailFEmployee.fromname = 'Email from employee';
	  	emailFEmployee.fromAddress = userEmp.Email;
	  	emailFEmployee.toAddresses = new String[]{client.Email};
	  	emailFEmployee.HtmlBody = 'Hey email from employee yeii HtmlBody';
	  	emailFEmployee.plainTextBody = 'Hey email from employee yeii plainTextBody';
	  	
	  	// add an attachment to employee email
		Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
	  	attachment.body = blob.valueOf('my attachment text');
	  	attachment.fileName = 'textfile.txt';
	  	attachment.mimeTypeSubType = 'text/plain';
	 
	  	emailFEmployee.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
	  	
	  	EmailToS3Controller emailProcess = new EmailToS3Controller();
	  	emailProcess.handleInboundEmail(emailFClient, env);
	  	emailProcess.handleInboundEmail(emailFEmployee, env);
	  	//emailProcess.handleInboundEmail(emailFGoogle, env);
	  	
	  	
	  	//Test the method GenerateEmailsToS3
	  	Blob body = blob.valueOf('tessssssssst');	  	
	  	emailProcess.generateEmailToS3(client, employee, true,'subject', body, true, '01-11-2014 13:00:00');
	  	emailProcess.generateEmailToS3(client, employee, false,'subject', body, false, '01-11-2014 13:00:00');	  
       }
    }
}