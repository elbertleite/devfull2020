public without sharing class pmc_school_issues {
    
    public String schoolId{get;set;}
    
    public pmc_school_issues() {}

    @RemoteAction
    public static Integer closeCases(List<String> casesIds){
        User user = [SELECT ID, Contact.ID FROM User WHERE ID = :UserInfo.getUserId()];
        Set<String> idsToSearch = new Set<String>();
        idsToSearch.add(user.ID);
        if(user.Contact.ID != null){
            idsToSearch.add(user.Contact.ID);
        }
        List<Case> cases = [SELECT ID, Status, Owner.ID FROM Case WHERE ID = :casesIds AND OWner.ID IN :idsToSearch AND Status != 'Closed'];
        if(cases != null && !cases.isEmpty()){
            for(Case cs : cases){
                cs.status = 'Closed';
            }
            update cases;
            return cases.size();
        }
        return 0;
    }
    @RemoteAction
    public static void takeOwnership(List<String> casesIds){
        List<Case> toUpdate = new List<Case>(); 
        for(String id : casesIds){
            toUpdate.add(new Case(ID = id, OwnerId= UserInfo.getUserId(), status = 'Assigned'));
        }
        update toUpdate;
    }

    @RemoteAction
    public static List<CaseComment> saveAndRetrieveComments(String caseId, String comment){
        CaseComment cc = new CaseComment(ParentId = caseId, CommentBody = comment);
        insert cc;
        return [SELECT ID, ParentId, CommentBody, CreatedDate, CreatedBy.Name FROM CaseComment WHERE ParentId = :caseId ORDER BY CreatedDate DESC];
    }

    @RemoteAction
    public static void saveNewComment(List<String> casesIds, String comment){
        List<CaseComment> comments = new List<CaseComment>(); 
        for(String id : casesIds){
            comments.add(new CaseComment(ParentId = id, CommentBody = comment));
        }
        insert comments;
    }

    @RemoteAction
    public static Map<String, Object> init(String id){
        Map<String, Object> response = new Map<String, Object>();
        Map<String, Map<String, Account>> allCampuses = new Map<String, Map<String, Account>>();
        if(!String.isEmpty(id)){
            Account school = [SELECT ID, Name FROM Account WHERE ID = :id];
            response.put('school', school);
            allCampuses.put(school.ID, new Map<String, Account>([SELECT ID, Name FROM Account WHERE Parent.ID = :id AND RecordType.Name = 'Campus']));
        }else{
            Map<String, Account> schools = new Map<String, Account>([SELECT ID, Name FROM Account WHERE RecordType.Name = 'School' ORDER BY Name]);
            for(Account campus : [SELECT ID, Name, Parent.ID FROM Account WHERE Parent.ID in :schools.keySet() AND RecordType.Name = 'Campus' ORDER BY Parent.ID, Name]){
                if(!allCampuses.containsKey(campus.Parent.ID)){
                    allCampuses.put(campus.Parent.ID, new Map<String, Account>());
                }
                allCampuses.get(campus.Parent.ID).put(campus.ID, campus);
            }
            response.put('schools', schools);
        }
        User user = [SELECT ID, Contact.ID FROM User WHERE ID = :UserInfo.getUserId()];
        response.put('allCampuses', allCampuses);
        response.put('priorities', getCasePriorities());
        response.put('statuses', getCaseStatuses());
        response.put('issues', getCaseIssues());
        response.put('userLogged', user.ID);
        response.put('caseOwners', getCasesOwners(user.ID));
        response.put('ids', getIdsToSearchCases());
        return response;
    }

    public static Map<String, String> getCasesOwners(String userId){   
        Map<String, String> owners = new Map<String, String>();
        owners.put('all','All Cases');
        Set<String> ids = getIdsToSearchCases();
        for(AggregateResult ag : [SELECT Owner.Name na, Owner.ID oid FROM Case WHERE Owner.ID IN :ids GROUP BY Owner.Name, Owner.ID ORDER BY Owner.Name]){
            owners.put((String) ag.get('oid'), userId == (String) ag.get('oid') ? 'My Cases' : (String) ag.get('na'));
        }
        owners.put(userId, 'My Cases');
        return owners;
    }

    public static Map<String, Set<String>> getOwnerAndGroupsIds(){
        Map<String, Set<String>> response = new Map<String, Set<String>>();
        response.put('owner', new Set<String>());
        response.put('groups', new Set<String>());

        Set<String> allIds = new Set<String>();

        User user = [SELECT ID, Contact.ID FROM User WHERE ID = :UserInfo.getUserId()];

        response.get('owner').add(user.ID);
        response.get('groups').add(user.ID);
        allIds.add(user.ID);
        if(user.Contact.ID != null){
            allIds.add(user.Contact.ID);
            response.get('groups').add(user.Contact.ID);
            response.get('owner').add(user.Contact.ID);
        }

        Set<String> groupIds = new Set<String>();
        for(GroupMember gm : [SELECT GroupId, UserOrGroupId FROM GroupMember WHERE UserOrGroupId IN :allIds]){
            groupIds.add(gm.GroupId);
        }

        for(Group gp : [SELECT ID FROM Group WHERE Type = 'Queue' AND ID IN :groupIds]){
            response.get('groups').add(gp.ID);
            allIds.add(gp.ID);
        }
        
        for(CaseTeamTemplateMember ctm : [SELECT MemberId, TeamRoleId, TeamTemplateId FROM CaseTeamTemplateMember WHERE MemberId IN :allIds]){
            response.get('groups').add(ctm.TeamTemplateId);
        }

        return response;
    }

    public static Set<String> getIdsToSearchCases(){
        User user = [SELECT ID, Contact.ID FROM User WHERE ID = :UserInfo.getUserId()];
        Set<String> idsToSearch = new Set<String>();
        Set<String> idUser = new Set<String>();
       // idsToSearch.add.Contact.ID);
        idsToSearch.add(user.ID);
        idUser.add(user.ID);
        if(user.Contact.ID != null){
            idsToSearch.add(user.Contact.ID);
            idUser.add(user.Contact.ID);
        }

        Set<String> groupIds = new Set<String>();
        for(GroupMember gm : [SELECT GroupId, UserOrGroupId FROM GroupMember WHERE UserOrGroupId IN :idsToSearch]){
            groupIds.add(gm.GroupId);
        }

        for(Group gp : [SELECT ID FROM Group WHERE Type = 'Queue' AND ID IN :groupIds]){
            idsToSearch.add(gp.ID);
        }
        
        for(CaseTeamTemplateMember ctm : [SELECT MemberId, TeamRoleId, TeamTemplateId FROM CaseTeamTemplateMember WHERE MemberId IN :idsToSearch]){
            idsToSearch.add(ctm.TeamTemplateId);
        }

        return idsToSearch;
    }

    public static Integer getTotalCasesOpen(String id){
        Set<String> ids = getIdsToSearchCases();

        AggregateResult ar = [SELECT Count(ID) tt FROM Case WHERE AccountId = :id AND Status != 'Closed' AND Owner.ID IN :ids];
        //AggregateResult ar = [SELECT Count(ID) tt FROM Case WHERE AccountId = :id AND Status != 'Closed'];
        return (Integer) ar.get('tt');
    }

    @RemoteAction
    public static Map<String, Object> retrieveReportedIssues(String id, String owner, Boolean retrieveClosedCases){
        String [] statusOrder = new String [] {'Escalated','New','Assigned','On Hold','Closed'};
        String [] priorityOrder = new String [] {'High','Medium','Low'};
        Map<String, Map<String, List<IPClasses.SchoolIssue>>> issuesPerStatus = new Map<String, Map<String, List<IPClasses.SchoolIssue>>>();
        
        for(String status : statusOrder){
            issuesPerStatus.put(status, new Map<String, List<IPClasses.SchoolIssue>>());
            for(String priority : priorityOrder){
                issuesPerStatus.get(status).put(priority, new List<IPClasses.SchoolIssue>());
            }
        }
        
        List<IPClasses.SchoolIssue> issues = new List<IPClasses.SchoolIssue>();

        Map<String, Set<String>> ids = getOwnerAndGroupsIds(); 
        Set<String> userId = ids.get('owner');
        
        String query = 'SELECT ID, CaseNumber, status, Account.ID, Account.Name, description, priority, subject, School_campuses__c, School_Issues__c, CreatedBy.Name, CreatedDate, Owner.ID, Owner.Name, LastModifiedDate, LastModifiedBy.Name, Preview_Link__c FROM Case WHERE Account.RecordType.Name = \'School\' ';
        if(!String.isEmpty(id)){
            query += ' AND Account.ID = :id';
        }
        Set<String> visibleOwners;
        if(owner != 'all'){
            query += ' AND (Owner.ID = :owner ';
        }else{
            visibleOwners = getIdsToSearchCases();
            query += ' AND (Owner.ID IN :visibleOwners ';
        }
        query += 'OR CreatedBy.Id IN :userId) ';
        if(!retrieveClosedCases){
            query += ' AND Status != \'Closed\'';
        }

        query += ' ORDER BY School__r.Name, CreatedDate DESC';
        
        
        List<Case> cases = Database.query(query);
        Map<String, Object> response = new Map<String, Object>();
        if(!cases.isEmpty()){
            Set<String> casesIds = new Set<String>();
            for(Case cs : cases){
                casesIds.add(cs.ID);
            }
            Map<String, List<CaseComment>> comments = new Map<String, List<CaseComment>>();
            
            for(CaseComment comment : [SELECT ID, ParentId, CommentBody, CreatedDate, CreatedBy.Name FROM CaseComment WHERE ParentId IN :casesIds ORDER BY CreatedDate DESC]){
                if(!comments.containsKey(comment.ParentId)){
                    comments.put(comment.ParentId, new List<CaseComment>());
                }
                comments.get(comment.ParentId).add(comment);
            }
            Map<String, List<CaseHistory>> history = new Map<String, List<CaseHistory>>();
            for(CaseHistory ch : [SELECT ID, CaseId, Field, NewValue, OldValue, CreatedDate, CreatedBy.Name FROM CaseHistory WHERE CaseId IN :casesIds ORDER BY CreatedDate]){
                if(!history.containsKey(ch.CaseId)){
                    history.put(ch.CaseId, new List<CaseHistory>());
                }
                history.get(ch.CaseId).add(ch);
            } 
            IPClasses.SchoolIssue sc;
            Map<String, String> caseSchools = new Map<String, String>();
            for(Case cs : cases){
                sc = new IPClasses.SchoolIssue(cs, comments.get(cs.ID), history.get(cs.ID), ids.get('owner').contains(cs.Owner.ID), ids.get('groups').contains(cs.Owner.ID));
          
                if(!issuesPerStatus.containsKey(sc.status)){
                    issuesPerStatus.put(sc.status, new Map<String, List<IPClasses.SchoolIssue>>());
                }
                if(!issuesPerStatus.get(sc.status).containsKey(sc.priority)){
                    issuesPerStatus.get(sc.status).put(sc.priority, new List<IPClasses.SchoolIssue>());
                }
                issuesPerStatus.get(sc.status).get(sc.priority).add(sc);
                
                caseSchools.put(sc.school, sc.schoolName);
            }
            
            response.put('caseSchools', caseSchools);
            
            for(String status : issuesPerStatus.keySet()){
                for(String priority : issuesPerStatus.get(status).keySet()){
                    if(!issuesPerStatus.get(status).get(priority).isEmpty()){
                        issues.addAll(issuesPerStatus.get(status).get(priority));
                    }
                }
            }
        }
        response.put('issues', issues);
        response.put('query', cases.size());
        return response;
    }

    @RemoteAction
    public static void saveNewIssue(IPClasses.SchoolIssue issue){
        Case schoolCase = new Case();
        //schoolCase.School__c = issue.school;
        schoolCase.AccountID = issue.school;
        schoolCase.description = issue.description;
        schoolCase.priority = issue.priority;
        schoolCase.subject = issue.subject;

        if(issue.campuses != null && issue.campuses.size() > 0){
            schoolCase.School_campuses__c = '';
            for(String campus : issue.campuses){
                schoolCase.School_campuses__c += campus + ';';
            }
            schoolCase.School_campuses__c = schoolCase.School_campuses__c.removeEnd(';');
        }
        if(issue.issues != null && issue.issues.size() > 0){
            schoolCase.School_Issues__c = '';
            for(String i : issue.issues){
                schoolCase.School_Issues__c += i + ';';
            }
            schoolCase.School_Issues__c = schoolCase.School_Issues__c.removeEnd(';');
        } 
        if(String.isEmpty(issue.id)){
            schoolCase.status = 'New';
            schoolCase.Origin = 'Web';

            Database.DMLOptions dmlOpts = new Database.DMLOptions();
            dmlOpts.assignmentRuleHeader.useDefaultRule = true;
    
            schoolCase.setOptions(dmlOpts);
            insert schoolCase;
        }else{
            schoolCase.ID = issue.id;
            update schoolCase;
        }
    }

    public static List<String> getCaseIssues(){
        List<String> response = new List<String>();
        Schema.DescribeFieldResult F = Case.fields.School_Issues__c.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        for(Schema.PicklistEntry i : P ){
            response.add(i.getValue());
        }
        return response;
    }

    public static List<String> getCaseStatuses(){
        List<String> response = new List<String>();
        Schema.DescribeFieldResult F = Case.fields.Status.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        for(Integer i = P.size() - 1; i >= 0; i-- ){
            response.add(P.get(i).getValue());
        }
        return response;
    }

    public static List<String> getCasePriorities(){
        List<String> response = new List<String>();
        Schema.DescribeFieldResult F = Case.fields.Priority.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        for(Integer i = P.size() - 1; i >= 0; i-- ){
            response.add(P.get(i).getValue());
        }
        return response;
    }
}