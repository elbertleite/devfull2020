/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class VideoUploadController_test {

    static testMethod void myUnitTest() {
        
        Video_Upload__c vu = new Video_Upload__c(Category__c = 'Training', Video_Key__c = 'oqh3r8bKJOQ2873qwe46', Description__c = 'a training video');
        insert vu;

		VideoUploadController vuc = new VideoUploadController();
		map<string, list<VideoUploadController.vimeoVideo>> listVideos = vuc.listVideos;
		
		ApexPages.currentPage().getParameters().put('mapKey', vu.Category__c);
		vuc.videosThumbnails();
		
		vuc.newVideo();
		vuc.save();
		
		ApexPages.currentPage().getParameters().put('idVideo', vu.id);
		vuc.editVideo();
		
		vuc.deleteVideo();
		
		vuc.cancel();
		



    }
}