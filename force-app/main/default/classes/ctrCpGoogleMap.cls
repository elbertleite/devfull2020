public with sharing class ctrCpGoogleMap{

	public String IncList {get;set;}
	
	
	public String StrAddress {get;set;}
	
	public Address__c ObjAddress {get;set;}
	
	public String inAddress {
		get	{
		if(inAddress==null){
			if(StrAddress!=null){
				inAddress = StrAddress;
			}else if(ObjAddress!=null){
				inAddress = ObjAddress.Street__c + '+' + ObjAddress.Suburb__c + '+' +  ObjAddress.City__c + '+' +  ObjAddress.State__c + '+' +  ObjAddress.Country__c;
				inAddress = inAddress.replace('+null','');
				inAddress = inAddress.replace(' ','+');
			}else{
				inAddress='';
			}
		}
		return inAddress;
	}
		
		set;}
}