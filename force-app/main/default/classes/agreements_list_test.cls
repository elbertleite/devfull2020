@isTest
private class agreements_list_test {
	
	public static Account school {get;set;}
	public static Account agency {get;set;}
	public static Account campus {get;set;}
	public static Course__c course {get;set;}
	public static Campus_Course__c campusCourse {get;set;}

	@testSetup static void setup() {
		TestFactory tf = new TestFactory();

		agency = tf.createAgency();
		school = tf.createSchool();
		campus = tf.createCampus(school, agency);
		course = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);

		//Agreement Parties
		agreements_new.CallResult result = agreements_new.saveAgreement(null, 'Agreement Test', agency.Id, 'Test User', '2018-02-12', 'Direct', 'School', school.Id + ';' + agency.id, 'All Regions', null, null, 'true', 'false');

		//Agreement Details
		result = agreements_new.saveGeneralInfo(result.agreementId, '2018-02-12', null, true, 'School Contact Details', 'Nationality Exc.', 'Agree Duration', 'Enroll Conditions', 'Course Payment', 'Refund Cancel', 'Extra Info', false, false, false, false, '');

		//Commission
		ApexPages.currentPage().getParameters().put('id', result.agreementId);
		agreement_commissions comm = new agreement_commissions();

		comm.coursesCommission[0].schSetup = '3;;true;;false';

		comm.coursesCommission[0].SchoolCommissionObj.School_Commission__c = 10;
		comm.coursesCommission[0].SchoolCommissionObj.School_Commission_Type__c = 0;

		comm.coursesCommission[0].listCampus[0].campusCommissionObj.Campus_Commission__c = 20;
		comm.coursesCommission[0].listCampus[0].campusCommissionObj.Campus_Commission_Type__c = 0;

		comm.coursesCommission[0].listCampus[0].listCourseType[0].courseTypeCommissionObj.Course_Type_Commission__c = 30;
		comm.coursesCommission[0].listCampus[0].listCourseType[0].courseTypeCommissionObj.Course_Type_Commission_Type__c = 0;

		comm.coursesCommission[0].listCampus[0].listCourseType[0].listCourseCommission[0].Course_Commission__c = 200;
		comm.coursesCommission[0].listCampus[0].listCourseType[0].listCourseCommission[0].Course_Commission_Type__c = 1;

		ApexPages.currentPage().getParameters().put('isAvailable', 'true');
		comm.saveCommission();

	}

	static testMethod void listContructor(){
		agreements_list test = new agreements_list();
	}

	static testMethod void filters(){
		String groups = agreements_list.searchGroups();
		String schools = agreements_list.searchSchools('Australia', 'Vocational Courses', 'Private');

		String agencyGroup = [Select Id from Account where RecordType.Name = 'Agency Group' limit 1].Id;
		String agencies = agreements_list.searchAgencies(agencyGroup);
	}

	static testMethod void listAgreements(){
		String result = agreements_list.findAgreementBySchool(1, 20, 'all', 'Australia', 'all', 'none', 'all', 'all',  '2016-02-12');

		Account sch = [Select Id from Account where RecordType.Name = 'School'];
		result = agreements_list.findAgreementBySchool(1, 20, 'all', 'Australia', (String)sch.Id, 'none', 'all', 'all',  '2016-02-12');
	}

	static testMethod void userProfile(){
		agreements_list.UserProfile uProfile = agreements_list.getUserProfile();
	}
	
}