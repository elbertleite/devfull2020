@RestResource(urlMapping = '/v1/school/products')
global class API_v1_schoolProducts extends api_manager {
    
    static RestRequest request;
    
    @HttpGet
	global static List<SchoolProduct> doGet(){
		
		request = RestContext.request;		
		System.debug('/v1/school/products ' + request);
				
		String nationality = api_manager.getRequestParam(request, 'nationality');
		String ccids = api_manager.getRequestParam(request, 'campusCourses');
		String paymentDate = api_manager.getRequestParam(request, 'paymentDate');
		String language = api_manager.getRequestParam(request, 'language');
		
		Set<String> campusCourses = new Set<String>();
		if(ccids != null)
			campusCourses.addAll(ccids.split(','));
		
		if(paymentDate == null)
			paymentDate = String.valueOf(System.today());
		
		RestResponse response = restContext.response;
        
        API_Manager.logProductCall(UserInfo.getUserID(), System.now().dategmt(), campusCourses, nationality);
		
		return API_v1_schoolProductsHandler.retrieveProducts(campusCourses, nationality, Date.valueOf(paymentDate), language);
		
		
	}
	
}