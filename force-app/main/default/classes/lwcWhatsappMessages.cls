global without sharing class lwcWhatsappMessages{}
/*@RestResource(urlMapping = '/whatsapp/*')
global without sharing class lwcWhatsappMessages {

    global static final Integer TOTAL_MESSAGES_TO_LOAD = 25;

    global lwcWhatsappMessages() {}

    @AuraEnabled(cacheable=true)
    global static Map<String, Object> init(String id){
        Map<String, Object> response = new Map<String, Object>();
        response.put('totalChatMessagesToload', TOTAL_MESSAGES_TO_LOAD);
        response.put('templates', retrieveTemplates());
        return response;
    }

    @AuraEnabled
    global static String retrieveMessageFromTemplate(String clientID, WhatsappTemplate template){
        map<string, Object> mapFields = IPFunctions.findTokens(IPFunctions.tokensWhatsapp);
		list<sObject> listResult = emailTemplateDynamic.createListResult(' FROM CONTACT WHERE id = \''+clientID+'\'', mapFields, null, new list<String>(mapFields.keySet()));
        sObject objTemplate = listResult[0];

        list<string> fieldsTemplate = emailTemplateDynamic.extractFieldsFromFile(null, mapFields); 
		String message = emailTemplateDynamic.createSingleTemplate(mapFields, template.template, fieldsTemplate, objTemplate);

        return message;
    }

    @AuraEnabled
    global static String retrieveMessageFromDocument(List<clientDocumentsCategory> documents, String parentIndex, String index){
        clientDocumentsCategory category;
        clientDocumentsResponse document;
        try{
            String EHFShortnerHost = IPFunctions.EHFShortnerHost;
            
            category = documents.get(Integer.valueOf(parentIndex));
            document = category.docs.get(Integer.valueOf(index));
            
            HttpRequest req = new HttpRequest();
            req.setEndpoint('https://' + EHFShortnerHost + '/prod/');
            req.setMethod('POST'); 
            req.setHeader('Content-Type','application/json; charset=utf-8');
            
            Map<String, String> body = new Map<String, String>();
            body.put('url_long', document.url);
            body.put('cdn_prefix', EHFShortnerHost);
            
            req.setBody(JSON.serialize(body));
            
            Http http = new Http(); 
            HTTPResponse res = http.send(req);
            
            Map<String, Object> response = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
            if(response.containsKey('url_short') && !String.isEmpty((String)response.get('url_short'))){
                return ' ' + category.name + ' -> ' + response.get('url_short');
            }
            return ' ' + category.name + ' -> ' + document.url;
        }catch(Exception e){
            system.debug(e);
            return '';
        }
    } 

    @AuraEnabled(cacheable=true)
    global static Map<String, Object> openChat(String id){
        system.debug('THE ID SENT '+id);
        Map<String, Object> response = new Map<String, Object>();
        Contact ctt = [SELECT ID, Name, Whatsapp_Number__c, firstName, lastName, PhotoClient__c, Gender__c FROM Contact WHERE ID = :id];
        //Map<String, String> contactDetails = getContactDetails(ctt);
          
        response.putAll(retrieveMessages(ctt.ID, 1)); 
        ContactDetails details = new ContactDetails(ctt);
        /*String [] whatsappTypes = new String [] {'Whatsapp', 'whatsapp', 'WhatsaApp'};
        for(Social_Network__c info : [SELECT id, Type__c, Detail__c, Object_Type__c, RDStation_Custom_Field_Type__c from Social_Network__c WHERE Contact__r.id = :id AND Type__c IN :whatsappTypes]){
            details.phone = info.Detail__c;
        }
        response.put('contact', details);
        response.put('documents', retrieveDocuments(ctt.ID));

        return response;
    }

    @AuraEnabled(cacheable=true)
    global static List<WhatsappTemplate> retrieveTemplates(){
        User user = [SELECT ID, Contact.Name, Contact.Account.ID , Contact.Account.Name FROM User WHERE ID = :UserInfo.getUserId()];
        List<WhatsappTemplate> templates = new List<WhatsappTemplate>();
        for(Email_Template__c et:[Select id, Template__c, Template_Description__c, Description__c, Email_Subject__c, Category__c, lastModifiedBy.name, lastModifiedDate from Email_Template__c where Agency__c = :user.Contact.Account.ID AND Category__c = 'Whatsapp' order by Category__c, Template_Description__c]){
            templates.add(new WhatsappTemplate(et));
        }
        return templates;
    }

    global static List<clientDocumentsCategory> retrieveDocuments(String clientId){
        User userLogged = [Select ContactId, Contact.Account.Id from user where id = :UserInfo.getUserId() limit 1];

        Integer virtualId = 0;
        Map<String, List<clientDocumentsResponse>> documents = new Map<String, List<clientDocumentsResponse>>();
        List<IPFunctions.ContactDocument> docs;
        for(Client_Document__c cd : [Select Document_Category__c, C.Document_Type__c, lastModifiedDate, isSelected__c, Name, Client_Course__c, client_course__r.course_package__c, preview_link__c FROM Client_Document__c C WHERE Client__c = :clientId AND (Agency_Access_Restriction__c = null OR Agency_Access_Restriction__c INCLUDES (:userLogged.Contact.Account.Id)) ORDER BY Document_Category__c, Document_Type__c, CreatedDate desc]){
            if(!documents.containsKey(cd.Document_Category__c)){
                documents.put(cd.Document_Category__c, new List<clientDocumentsResponse>());
            }
            if(cd.preview_link__c!= null){
                for(IPFunctions.ContactDocument doc : IPFunctions.getFilesFromPreviewLink(cd.preview_link__c, virtualId)){
                    documents.get(cd.Document_Category__c).add(new clientDocumentsResponse(doc.fileName, doc.url));
                }
            }
            virtualId += documents.get(cd.Document_Category__c).size();
        }
        
        documents.put('Quotation', new List<clientDocumentsResponse>());
        clientDocumentsResponse quote;
        for(Quotation__c quotation : [SELECT ID, Name, (SELECT ID, School_Name__c, Campus_City__c, Campus_Country__c, Campus_Name__c, Course_Name__c FROM Quotation_Courses__r) FROM Quotation__c where Client__c = :clientID AND CreatedDate != null ORDER BY CreatedDate desc]){
            quote = new clientDocumentsResponse(quotation.Name, IPFunctions.getContactURLByEnvironment(quotation.ID, 'quotation'));
            quote.hasQuotation = true;
            quote.quotationCourses = new List<QuotationCourseResponse>();
            for(Quotation_Detail__c course : quotation.Quotation_Courses__r){
                quote.quotationCourses.add(new QuotationCourseResponse(course.ID, course.Campus_Country__c, course.Campus_City__c, course.Campus_Name__c, course.Course_Name__c));
            }
            documents.get('Quotation').add(quote);
        }

        List<clientDocumentsCategory> documentsResponse = new List<clientDocumentsCategory>();
        if(!documents.isEmpty()){
            clientDocumentsCategory category;
            for(String key : documents.keySet()){
                if(!documents.get(key).isEmpty()){
                    category = new clientDocumentsCategory(key);
                    category.docs = documents.get(key);
                    documentsResponse.add(category);
                }
            }
        }

        return documentsResponse;
    }

    @AuraEnabled
    global static Map<String, Object> retrieveMessages(String id, Integer index){
        List<Message> messages = new List<Message>();
        Integer messagesLimit = TOTAL_MESSAGES_TO_LOAD * (index == null ? 1 : index);
        List<Whatsapp_Message__c> messagesDB = [SELECT Contact__r.Name, Message__c, Message_Direction__c, Message_Status__c, Message_Type__c, SID__c, CreatedById, CreatedBy.Contact.Name, CreatedBy.Contact.Account.Name, CreatedBy.Contact.Account.Parent.Name, CreatedDate, Attachment_Url__c, Location_Latitude__c, Location_Longitude__c FROM Whatsapp_Message__c WHERE Contact__c = :id ORDER BY CreatedDate DESC LIMIT :messagesLimit];

        Integer totalClientMessages = 0;
        if(messagesDB != null && !messagesDB.isEmpty()){
            for(Integer i = messagesDB.size()-1; i >= 0; i--){
                messages.add(new Message(messagesDB.get(i)));
            }
            /*for(Whatsapp_Message__c msg : messagesDB){
                messages.add(new Message(msg));
            }
            if(messages.size() > 1){
                for(Integer i = 0; i < messages.size(); i++){
                    if(i > 0){
                        messages.get(i-1).showCreatedBy = messages.get(i).createdBy != messages.get(i-1).createdBy;
                    }
                }
            }
            AggregateResult ar = [select COUNT(ID) ct FROM Whatsapp_Message__c WHERE Contact__c = :id];
			totalClientMessages = Integer.valueOf(ar.get('ct'));
        }
        Map<String, Object> response = new Map<String, Object>();
        String lastMessage = null;
        if(!messages.isEmpty()){
            for(Message msg : messages){
                if(msg.direction == 'Received'){
                    lastMessage = msg.createdDate;
                }
            }
        }
        response.put('totalClientMessages', totalClientMessages);
        response.put('lastMessage', lastMessage);
        response.put('hasLastMessage', !String.isEmpty(lastMessage));
        response.put('messages', messages);

        return response;
    }

    global static HttpRequest getTwilioRestRequest(Map<String, String> configs){
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://api.twilio.com/2010-04-01/Accounts/'+configs.get('twillioAccountId')+'/Messages.json');
        req.setMethod('POST'); 
        //req.setHeader('Content-Type','application/json');
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
    
        Blob headerValue = Blob.valueOf(configs.get('twillioAccountId') + ':' + configs.get('twillioAccountToken'));
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        return req;
    }

    global static Map<String, String> retrieveWhatsappNumberForMessage(){
        User user = [SELECT ID, Contact.Name, Contact.Account.ID , Contact.Account.Whatsapp_Messages_Config__c FROM User WHERE ID = :UserInfo.getUserId()];
        Map<String, String> configs = (Map<String, String>) JSON.deserialize(user.Contact.Account.Whatsapp_Messages_Config__c, Map<String, String>.class);
        
        Account acc = new Account(ID = user.Contact.Account.ID);
        acc.Whatsapp_Numbers__c = '+14155238886';
        update acc;
        return configs;
        
    }

    @AuraEnabled
    global static List<ContactDetails> retrieveLatestMessages(){
        User user = [SELECT ID, Contact.Name, Contact.Account.Whatsapp_Latest_Messages__c FROM User WHERE ID = :UserInfo.getUserId()];
        Map<String, Map<String, Boolean>> latestMessages;
        Map<String, Boolean> lastMessage;
        if(String.isEmpty(user.Contact.Account.Whatsapp_Latest_Messages__c)){
            latestMessages = new Map<String, Map<String, Boolean>>(); 
        }else{
            latestMessages = (Map<String, Map<String, Boolean>>) JSON.deserialize(user.Contact.Account.Whatsapp_Latest_Messages__c, Map<String, Map<String, Boolean>>.class);
        }
        
        List<ContactDetails> latestMessagesResponse = new List<ContactDetails>();
        if(latestMessages.size() > 0){
            Set<String> latestMessagesIds = new Set<String>();
            for(String ctt : latestMessages.keySet()){
                for(String messageId : latestMessages.get(ctt).keySet()){
                    if(!latestMessages.get(ctt).get(messageId)){
                        latestMessagesIds.add(messageId);
                    }
                }
            }
            if(latestMessagesIds.size() > 0){
                Map<String, Whatsapp_Message__c> messagesPerContact = new Map<String, Whatsapp_Message__c>();
    
                for(Whatsapp_Message__c msg : [SELECT Contact__c, Contact__r.Name, Message__c, Message_Direction__c, Message_Status__c, Message_Type__c, SID__c, CreatedById, CreatedBy.Contact.Name, CreatedBy.Contact.Account.Name, CreatedBy.Contact.Account.Parent.Name, CreatedDate, Attachment_Url__c, Location_Latitude__c, Location_Longitude__c FROM Whatsapp_Message__c WHERE ID IN :latestMessagesIds ORDER BY CreatedDate]){
                    messagesPerContact.put(msg.Contact__c, msg);
                }
    
                ContactDetails details;
                for(Contact ctt : [SELECT ID, Name, Whatsapp_Number__c, firstName, lastName, PhotoClient__c, Gender__c FROM Contact WHERE ID IN :latestMessages.keySet()]){
                    details = new ContactDetails(ctt);
                    if(messagesPerContact.containsKey(ctt.ID)){
                        details.lastMessage = new Message(messagesPerContact.get(ctt.ID));
                        latestMessagesResponse.add(details);
                    }
                }
            }
        }
        return latestMessagesResponse;
    }

    @AuraEnabled
    global static void sendMessage(String message, ContactDetails contact, Boolean latestMessagesOpen){
        Map<String, String> configs = retrieveWhatsappNumberForMessage();
        HttpRequest req = getTwilioRestRequest(configs);


        string jsonString='From='+EncodingUtil.urlEncode('whatsapp:'+configs.get('numberFrom'), 'UTF-8')+'&Body='+EncodingUtil.urlEncode(message.trim(), 'UTF-8')+'&To='+EncodingUtil.urlEncode('whatsapp:'+contact.phone+'', 'UTF-8')+'';
        req.setBody(jsonString);

        try{
            system.debug('THE MESSAGE '+message);
            system.debug('THE MESSAGE JSON '+jsonString);
            Http http = new Http();
            HTTPResponse res = http.send(req);

            if(res.getStatusCode()==201){
                Map<String, Object> twillioResponse = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
                system.debug('Twilio Success');
                insert new Whatsapp_Message__c(Contact__c = contact.id, Message__c = message, Message_Direction__c = 'Sent', Message_Status__c = 'Sent', Message_Type__c = 'Text', SID__c = (String) twillioResponse.get('sid'));
                //return res.getBody();
            }else{
                system.debug('Twilio failed '+res.getBody());
            }
            User user = [SELECT ID, Contact.Name, Contact.Account.ID, Contact.Account.Whatsapp_Latest_Messages__c FROM User WHERE ID = :UserInfo.getUserId()];
            if(!String.isEmpty(user.Contact.Account.Whatsapp_Latest_Messages__c)){
                Map<String, Map<String, Boolean>> latestMessages = (Map<String, Map<String, Boolean>>) JSON.deserialize(user.Contact.Account.Whatsapp_Latest_Messages__c, Map<String, Map<String, Boolean>>.class);
                if(latestMessages.containsKey(contact.id)){
                    for(String messageID : latestMessages.get(contact.id).keySet()){
                        latestMessages.get(contact.id).put(messageID, true);
                    }
                    Account acc = new Account(ID = user.Contact.Account.ID); 
                    acc.Whatsapp_Latest_Messages__c = JSON.serialize(latestMessages);
                    update acc;
                }
            }
        }
        catch(Exception e){
            system.debug('Error :'+e);
        }
        
        //return retrieveMessages(contact.id);
    }

    @HttpPost
	global static String doPost(){
        if(RestContext.request.requestURI.endsWith('changeMessageStatus')){
            updateMessageStatus();
        }else{
            receiveWhatsappMessage();
        }
        return '200';
    }
    
    global static void receiveWhatsappMessage(){
        Map<String, String> params = RestContext.request.params;
        String contactPhone = params.get('From').split('whatsapp:')[1];
        Contact ctt = [SELECT ID, Whatsapp_Number__c FROM Contact WHERE Whatsapp_Number__c = :contactPhone LIMIT 1];
        if(ctt != null){
            system.debug('THE MESSAGE RECEIVED '+JSON.serialize(params));
            String messageType;
            Whatsapp_Message__c newMessage = new Whatsapp_Message__c(Contact__c = ctt.ID, Message_Direction__c = 'Received', Message_Status__c = 'Received', SID__c = (String) params.get('MessageSid'));
            Boolean saveMessage = true;
            if((String) params.get('NumMedia') == '0'){
                if(String.isEmpty(params.get('Body'))){
                    if(params.containsKey('Latitude')){
                        newMessage.Message_Type__c = 'Location';
                        newMessage.Location_Latitude__c = params.get('Latitude');
                        newMessage.Location_Longitude__c = params.get('Longitude'); 
                    }else{
                        saveMessage = false;
                        //CONTACTS AND STICKERS, COMING EMPTY FROM TWILIO
                    }
                }else{
                    newMessage.Message_Type__c = 'Text';
                    newMessage.Message__c = params.get('Body');
                }
            }else{
                if(params.get('MediaContentType0').contains('image')){
                    newMessage.Message_Type__c = 'Image';
                }else if(params.get('MediaContentType0').contains('audio')){
                    newMessage.Message_Type__c = 'Audio';
                }else if(params.get('MediaContentType0').contains('video')){
                    newMessage.Message_Type__c = 'Video';
                }else{
                    newMessage.Message_Type__c = 'File';
                    if(!String.isEmpty(params.get('Body'))){
                        newMessage.Message__c = params.get('Body');
                    }
                }
                newMessage.Attachment_Url__c = params.get('MediaUrl0'); 
            }
            insert newMessage;

            String accountPhone = params.get('To').split('whatsapp:')[1];
            Account acc = [SELECT ID, Name, Whatsapp_Latest_Messages__c FROM Account WHERE Whatsapp_Numbers__c INCLUDES (:accountPhone)];
            Map<String, Map<String, Boolean>> latestMessages;
            Map<String, Boolean> lastMessage;
            if(String.isEmpty(acc.Whatsapp_Latest_Messages__c)){
                latestMessages = new Map<String, Map<String, Boolean>>(); 
            }else{
                latestMessages = (Map<String, Map<String, Boolean>>) JSON.deserialize(acc.Whatsapp_Latest_Messages__c, Map<String, Map<String, Boolean>>.class);
            }
            lastMessage = new Map<String, Boolean>();
            lastMessage.put(newMessage.ID, false);
            latestMessages.put(ctt.ID, lastMessage);

            acc.Whatsapp_Latest_Messages__c = JSON.serialize(latestMessages);

            update acc;
        }
    }
    
    global static void updateMessageStatus(){
        String messageSid = RestContext.request.params.get('MessageSid');
        Whatsapp_Message__c message = [SELECT ID, Message_Status__c FROM Whatsapp_Message__c WHERE SID__c = :messageSid];
        if(message.Message_Status__c != 'Read'){
            message.Message_Status__c = retrieveMessageNewStatus(RestContext.request.params.get('MessageStatus'));
            update message;
        }
    }

    global static String retrieveMessageNewStatus(String twilioStatus){
        String messageStatus = 'Sent';
        if(twilioStatus == 'failed' || twilioStatus == 'undelivered'){
            messageStatus = 'Failed';
        }else if(twilioStatus == 'delivered'){
            messageStatus = 'Delivered';
        }else if(twilioStatus == 'received'){
            messageStatus = 'Received';
        }else if(twilioStatus == 'read'){
            messageStatus = 'Read';
        }
        return messageStatus;
    }

    global class ContactDetails{
        @AuraEnabled public String id {get;set;}
        @AuraEnabled public String name {get;set;}
        @AuraEnabled public String phone {get;set;}
        @AuraEnabled public String firstName {get;set;}
        @AuraEnabled public String lastName {get;set;}
        @AuraEnabled public String initials {get;set;}
        @AuraEnabled public String photo {get;set;}
        @AuraEnabled public Message lastMessage {get;set;}

        public ContactDetails(){}
        public ContactDetails(Contact ctt){
            this.id = ctt.ID;
            this.name = ctt.Name;
            this.phone = ctt.Whatsapp_Number__c;
            this.firstName = ctt.firstName;
            this.lastName = ctt.lastName;
            this.initials = ctt.FirstName.substring(0, 1)+ctt.LastName.substring(0, 1);
            if(ctt.PhotoClient__c != null){
                this.photo = ctt.PhotoClient__c;
            }else{
                if(ctt.Gender__c == null || ctt.Gender__c == 'Male'){
                    this.photo = '/apexpages/slds/latest/assets/images/avatar1.jpg';
                }else{
                    this.photo = '/apexpages/slds/latest/assets/images/avatar2.jpg';
                }
            }
        }
    }

    global class QuotationCourseResponse{
        @AuraEnabled public String id{get;set;}
        @AuraEnabled public String country{get;set;}
        @AuraEnabled public String city{get;set;}
        @AuraEnabled public String campus{get;set;}
        @AuraEnabled public String course{get;set;}

        public QuotationCourseResponse(){}
        public QuotationCourseResponse(String id, String country, String city, String campus, String course){
            this.id = id;
            this.country = country;
            this.city = city;
            this.campus = campus;
            this.course = course;
        }
    }

    global class Message{
        @AuraEnabled public String id {get; set;}
        @AuraEnabled public String sid {get; set;}
        @AuraEnabled public String message {get; set;}
        @AuraEnabled public String status {get; set;}
        @AuraEnabled public String type {get; set;}
        @AuraEnabled public String direction {get; set;}
        @AuraEnabled public String createdBy {get; set;}
        @AuraEnabled public String createdByAgency {get; set;}
        @AuraEnabled public String createdDate {get; set;}
        @AuraEnabled public String mediaUrl {get; set;}
        @AuraEnabled public String locationLatitude {get; set;}
        @AuraEnabled public String locationLongitude {get; set;}

        @AuraEnabled public Boolean showCreatedBy {get; set;}
        @AuraEnabled public Boolean showAvatar {get; set;}
        @AuraEnabled public Boolean messageFailed {get; set;}

        @AuraEnabled public Boolean isText{get;set;}
        @AuraEnabled public Boolean isAudio{get;set;}
        @AuraEnabled public Boolean isImage{get;set;}
        @AuraEnabled public Boolean isFile{get;set;}
        @AuraEnabled public Boolean isLocation{get;set;}
        @AuraEnabled public Boolean isVideo{get;set;}
        

        @AuraEnabled public String liClass {get; set;}
        @AuraEnabled public String messageBodyClass {get; set;}
        @AuraEnabled public String metaClass {get; set;}

        public Message(){}
        public Message(Whatsapp_Message__c msg){

            this.isText = false;
            this.isAudio = false;
            this.isImage = false;
            this.isFile = false;
            this.isLocation = false;

            this.id = msg.Id;
            this.sid = msg.SID__c;
            this.direction = msg.Message_Direction__c;
            this.liClass = this.direction == 'Received' ? 'slds-chat-listitem slds-chat-listitem_inbound' : 'slds-chat-listitem slds-chat-listitem_outbound';
            this.status = msg.Message_Status__c;
            this.createdDate = msg.CreatedDate.format('dd/MM/yy HH:mm');
            this.showCreatedBy = true;
            this.showAvatar = this.direction == 'Received';
            
            this.type = msg.Message_Type__c;
            
            if(this.direction == 'Received'){
                messageBodyClass = 'slds-chat-message__text slds-chat-message__text_inbound';
                this.createdBy = msg.Contact__r.Name;
                this.createdByAgency = '';

                this.message = msg.Message__c;
                this.mediaUrl = msg.Attachment_Url__c;
                this.locationLatitude = msg.Location_Latitude__c;
                this.locationLongitude = msg.Location_Longitude__c;

                this.isText = this.type == 'Text';
                this.isAudio = this.type == 'Audio';
                this.isImage = this.type == 'Image';
                this.isFile = this.type == 'File';
                this.isLocation = this.type == 'Location';
                this.isVideo = this.type == 'Video';

                if(this.isLocation){
                    this.mediaUrl = 'https://maps.google.com/maps?q=loc:'+this.locationLatitude+','+this.locationLongitude+'&output=embed';
                }
            }else{
                this.isText = true;
                this.message = msg.Message__c;
                this.createdBy = msg.CreatedBy.Contact.Name;
                this.createdByAgency = msg.CreatedBy.Contact.Account.Parent.Name;
                if(this.status == 'Read'){
                    messageBodyClass = 'slds-chat-message__text slds-chat-message__text_outbound';
                }else{
                    messageBodyClass = 'slds-chat-message__text slds-chat-message__text_outbound-agent';
                }
            }
            
            if(this.status == 'Failed'){
                messageBodyClass = 'slds-chat-message__text slds-chat-message__text_delivery-failure';
                this.messageFailed = true;
            }else{
                this.messageFailed = false;
            }
            
            this.metaClass = this.direction == 'Received' ? 'slds-chat-message__meta message-received-time' : 'slds-chat-message__meta message-sent-time';
            

        }
    }

    global class WhatsappTemplate{
        @AuraEnabled public String id{get;set;}
        @AuraEnabled public String name{get;set;}
        @AuraEnabled public String description{get;set;}
        @AuraEnabled public String template{get;set;}
        @AuraEnabled public boolean open{get;set;}

        public WhatsappTemplate(){}
        public WhatsappTemplate(Email_Template__c et){
            this.id = et.ID;
            this.name = et.Template_Description__c;
            this.description = et.Description__c;
            this.template = et.Template__c;
            open = false;
        }
    }

    global class clientDocumentsCategory{
        @AuraEnabled public String name{get;set;}
        @AuraEnabled public Boolean show{get;set;}
        @AuraEnabled public List<clientDocumentsResponse> docs{get;set;}
    
        public clientDocumentsCategory(){}
        public clientDocumentsCategory(String name){
            this.name = name;
            this.show = false;
        }
    }

    global class clientDocumentsResponse{
        @AuraEnabled public String name{get;set;}
        @AuraEnabled public String url{get;set;}
        @AuraEnabled public Boolean hasQuotation{get;set;}
        @AuraEnabled public List<QuotationCourseResponse> quotationCourses{get;set;}

        public clientDocumentsResponse(){}
        public clientDocumentsResponse(String name, String url){this.name = name; this.url = url;this.hasQuotation = false;}
    }
}*/