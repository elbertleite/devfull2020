/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class newLanguageFrame_test {

    static testMethod void myUnitTest() {
        
        
        Map<String,String> recordTypes = new Map<String,String>();
        
        for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
			
		
		
		Account agency = new Account();
		agency.RecordTypeId = recordTypes.get('Agency');
		agency.name = 'IP Sydney';
		agency.Account_Currency_Iso_Code__c = 'AUD';     
		insert agency;
		
	
		
		Quotation_Disclaimer__c footer = new Quotation_Disclaimer__c();
		footer.Agency__c = agency.id;
		footer.Default_English_Disclaimer__c = 'asidaiudjaisdiaudiajidjaijsd';
		footer.Destination__c = 'Australia';
		footer.Language__c = 'en_US';
		insert footer;
		
		Quotation_Disclaimer_Other_Nationality__c translation = new Quotation_Disclaimer_Other_Nationality__c();
		translation.Disclaimer__c  = 'bla bla bla bla bla bla';
		translation.Nationalities_Applicable__c = 'Brazil,Argentina,Chile';
		translation.Quotation_Disclaimer__c = footer.id;
		translation.Language__c = 'pt_BR';
		insert translation;
		
		newLanguageFrame nlf = new newLanguageFrame(new ApexPages.StandardController(footer));
		
		ApexPages.currentPage().getParameters().put('langId', translation.id);
		Quotation_Disclaimer_Other_Nationality__c trans = nlf.languageDisclaimer;
        boolean checkData = nlf.checkData;
        nlf.saveNewLanguage();
        
        map<string,string> languages = nlf.languages;
        String notSelectedLanguages = nlf.notSelectedLanguages;
        List<SelectOption> notSelectedLanguagesOptions = nlf.notSelectedLanguagesOptions;
        
        
        nlf.selected = new List<String>();
        nlf.unselected = new List<String>();
        
        nlf.selected.add('Australia');
        nlf.unselected.add('Brazil');
        
        List<String> selected = nlf.selected;
        List<String> unselected = nlf.unselected;
        
        List<SelectOption> selectedOptions = nlf.selectedOptions;
        List<SelectOption> unSelectedOptions = nlf.unSelectedOptions;
        
        nlf.doSelect();
        nlf.doUnSelect();
        
        
        
    }

}