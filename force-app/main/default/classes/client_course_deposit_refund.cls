public with sharing class client_course_deposit_refund {
    
    public client_course__c clientDeposit {get; set;}
	public list<client_course_instalment_payment__c> depositPayments {get{if(depositPayments==null) depositPayments = new list<client_course_instalment_payment__c>(); return depositPayments;}set;} 
	public list<client_course_instalment_payment__c> requestedRefunds {get{if(requestedRefunds==null) requestedRefunds = new list<client_course_instalment_payment__c>(); return requestedRefunds;}set;} 
	public client_course_instalment_payment__c refund {get{if(refund==null) refund = new client_course_instalment_payment__c(); return refund;}set;} 
	
	private string clientId {get;set;}
	
	
	/** C O N S T R U C T O R **/
	public client_course_deposit_refund(ApexPages.standardController controller){
		
		clientId = controller.getId();
		
		loadDeposit();
		
	}
	
	/** G E T 		D E P O S I T **/
	public void loadDeposit(){
		depositPayments.clear();
		requestedRefunds.clear();
		
		try{
			clientDeposit = [SELECT Deposit_Description__c, Deposit_Total_Used__c, Deposit_Total_Value__c, Deposit_Total_Available__c, Deposit_Type__c, Deposit_Value_Track__c, Id, client__c, Client__r.name, Deposit_Reconciliated__c, CurrencyIsoCode__c, Deposit_Total_Refund__c, client__r.Owner__r.AccountId,
 								(SELECT Id, Value__c, Received_By__r.Name, Received_On__c, Received_By_Agency__r.Name, Payment_Type__c, Date_Paid__c, Confirmed_By__c, CurrencyIsoCode__c,isInstalment_Refund__c,isClient_Refund__c, createdBy.Name, createdDate, Description__c, Paid_to_Client_By__r.Name, Refund_Requested_By_Agency__r.Name, Refund_Requested_By_Department__r.Name, Paid_to_Client_On__c, Paid_to_Client_By_Department__r.Name, Paid_to_Client_By_Agency__r.Name, Refund_Cancelled_By__r.Name, Refund_Cancelled_By_Agency__r.Name, Refund_Cancelled_By_Department__r.Name, Refund_Cancelled_On__c, Refund_Cancelled_Description__c, Not_Refunded__c FROM client_course_payments_deposit__r)

							FROM client_course__c WHERE client__c = :clientId AND isDeposit__c = true AND Deposit_Total_Available__c > 0];
 			
 			/** Deposit Payments and Requested Refunds **/
 			for(client_course_instalment_payment__c ccip : clientDeposit.client_course_payments_deposit__r){
 			
				if(!ccip.isClient_Refund__c)
					depositPayments.add(ccip);
				else
					requestedRefunds.add(ccip);

 			}//end for						
 		
		}
		catch(exception e){
			clientDeposit = new client_course__c();	
		}
	}
	
	/** R E Q U E S T 		R E F U N D **/
	public PageReference requestRefund(){
		
		
		/** Double check if deposit has been used while pop up is opened **/
		client_course__c updatedDeposit = [SELECT ID, Deposit_Total_Available__c FROM client_course__c WHERE Id = :clientDeposit.Id];
		
		if(updatedDeposit.Deposit_Total_Available__c!= clientDeposit.Deposit_Total_Available__c)
			clientDeposit.Deposit_Total_Available__c = updatedDeposit.Deposit_Total_Available__c;
			
			
		if(refund.Value__c>updatedDeposit.Deposit_Total_Available__c){
			refund.Value__c.addError('Refund Amount cannot be greater than Deposit Total Available of (' +   clientDeposit.CurrencyIsoCode__c  + ')' + updatedDeposit.Deposit_Total_Available__c);
			return null;		
		}
		
		/** Refund Details **/
		refund.Client__c = clientDeposit.Client__c;
		refund.Deposit__c = clientDeposit.id;
		refund.isClient_Refund__c = true;
		refund.CurrencyIsoCode__c = clientDeposit.CurrencyIsoCode__c;
		// refund.Refund_Requested_By_Agency__c = currentUser.Contact.AccountId;
		// refund.Refund_Requested_By_Department__c = currentUser.Contact.Department__c;

		boolean isGeneralUser = FinanceFunctions.isGeneralUser(currentUser.General_User_Agencies__c, clientDeposit.client__r.Owner__r.AccountId);
		
		if(!isGeneralUser)
			refund.Refund_Requested_By_Agency__c = currentUser.Contact.AccountId;
		else 
			refund.Refund_Requested_By_Agency__c = clientDeposit.client__r.Owner__r.AccountId;
		
		
		Client_Document__c refundDoc =  new Client_Document__c (Client__c = refund.Client__c, Document_Category__c = 'Finance', Document_Type__c = 'Deposit Refund', Client_course__c = refund.Deposit__c);
		insert refundDoc;
		
		refund.Payment_Receipt__c = refundDoc.id;
		insert refund;
		
		
		/** Update Deposit **/
		clientDeposit.Deposit_Total_Available__c -= refund.Value__c;
		
		if(clientDeposit.Deposit_Total_Refund__c==null)
			clientDeposit.Deposit_Total_Refund__c = refund.Value__c;
		else
			clientDeposit.Deposit_Total_Refund__c += refund.Value__c;
			
		update clientDeposit;
		
		loadDeposit();
		refund = new client_course_instalment_payment__c();
		return null;
		
	}
    

	/** R E F U N D 	R E C E I P T **/
	public void refundReceipt(){
		
		string refundId = ApexPages.CurrentPage().getParameters().get('rfid');
		
		refund = [SELECT Id, Client__r.Name, Value__c, Payment_Type__c, Date_Paid__c, CurrencyIsoCode__c, Paid_to_Client_By__r.Name, Paid_to_Client_On__c, Paid_to_Client_By_Department__r.Name, Paid_to_Client_By_Agency__r.Name, Agency_Currency__c, Agency_Currency_Value__c FROM client_course_instalment_payment__c WHERE id = :refundId];
		
	}

	

    private User currentUser {get{if(currentUser==null) currentUser = [Select Id, General_User_Agencies__c, Contact.AccountId, Contact.Department__c from User where id = :UserInfo.getUserId() limit 1]; return currentUser;}set;}
    
    public Account agencyDetails {get{	
		if(agencyDetails==null){
			agencyDetails = [SELECT Id, Name, Logo__c, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode, Registration_name__c, Trading_Name__c, Business_Number__c, Type_of_Business_Number__c From Account where Id = :currentUser.Contact.AccountId limit 1];
		}
		return agencyDetails;
	} set;}
}