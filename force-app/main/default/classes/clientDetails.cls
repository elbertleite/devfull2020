public with sharing class clientDetails{
	
	public Contact Contact {get;Set;}

	public clientDetails(ApexPages.StandardController controller){
		if(ApexPages.currentPage().getParameters().get('quoteID') != null )
			currTab = 'quoteEmail';
		else
			currTab = 'Overview';
		
		Contact = (Contact) controller.getRecord();
		edit = false;
	}
	
	public String currTab{get;set;}
	
	public Boolean edit {get;Set;}
	
	public void toggleEdit(){
		edit = !edit;
	}
	
	public PageReference save(){
		update Contact;
		return refresh();
	}
	
	public PageReference refresh(){
		PageReference acctPage = new PageReference('/apex/inc_ClientDetails?Id='+Contact.id);
        acctPage.setRedirect(true);
        return acctPage;
	}
	
	
	
	public PageReference del() {
        delete  Contact;
        PageReference acctPage = new PageReference('/apex/Contacts');
        acctPage.setRedirect(true);
        return acctPage;
    }
	
	private global_class gc = new global_class();
	public List<SelectOption> getLanguages() {
		List<SelectOption> options = new List<SelectOption>();
		options.addAll(gc.getLanguages());
		return options;
	}
	
	public map<String, string> getlangCodeTranslation(){
		return gc.langCodeTranslation();
	}


}