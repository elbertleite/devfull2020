@isTest(seeAllData=false)
private class school_payments_test {

	public static Account school {get;set;}
	public static Account agency {get;set;}
	public static Contact emp {get;set;}
	public static Contact client {get;set;}
	public static User portalUser {get{
    if (null == portalUser) {
      portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
    }
    return portalUser;
  }set;}
	public static Account campus {get;set;}
	public static Course__c course {get;set;}
	public static Campus_Course__c campusCourse {get;set;}
	public static client_course__c clientCourseBooking {get;set;}
	public static client_course__c clientCourse {get;set;}
	public static client_course__c clientCourse2 {get;set;}


	@testSetup static void setup() {
		TestFactory tf = new TestFactory();

		school = tf.createSchool();
		agency = tf.createAgency();
		emp = tf.createEmployee(agency);
		portalUser = tf.createPortalUser(emp);
		campus = tf.createCampus(school, agency);
		course = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);

		Back_Office_Control__c bo = new Back_Office_Control__c(Agency__c = agency.id, Country__c = 'Australia', Backoffice__c = agency.id, Services__c = 'Pay_School');
		insert bo;
		system.runAs(portalUser){
			client = tf.createClient(agency);
			clientCourseBooking = tf.createBooking(client);
			clientCourse = tf.createClientCourse(client, school, campus, course, campusCourse, clientCourseBooking);
			tf.createClientCourseFees(clientCourse, false);
			List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(clientCourse);

			clientCourse2 = tf.createClientCourse(client, school, campus, course, campusCourse, clientCourseBooking);
			tf.createClientCourseFees(clientCourse2, false);
			List<client_course_instalment__c> instalments2 =  tf.createClientCourseInstalments(clientCourse2);

			instalments2[0].isPFS__c = true;
			update instalments2;

			clientCourse.isCoe_Required__c = true;
			update clientCourse;
			clientCourse2.isCoe_Required__c = true;
			update clientCourse2;

			//Pay Instalment
			client_course_instalment_pay toPay = new client_course_instalment_pay(new ApexPages.StandardController(instalments[0]));
			toPay.newPayDetails.Payment_Type__c = 'Creditcard';
			toPay.newPayDetails.Value__c = instalments[0].Instalment_Value__c;
			toPay.newPayDetails.Date_Paid__c = Date.today();
			toPay.addPaymentValue();
			toPay.savePayment();

			toPay = new client_course_instalment_pay(new ApexPages.StandardController(instalments[1]));
			toPay.newPayDetails.Payment_Type__c = 'Creditcard';
			toPay.newPayDetails.Value__c = instalments[1].Instalment_Value__c;
			toPay.newPayDetails.Date_Paid__c = Date.today();
			toPay.addPaymentValue();
			toPay.savePayment();

			Test.startTest();

			//Pay Instalment
			toPay = new client_course_instalment_pay(new ApexPages.StandardController(instalments2[0]));
			toPay.newPayDetails.Payment_Type__c = 'Creditcard';
			toPay.newPayDetails.Value__c = instalments2[0].Instalment_Value__c;
			toPay.newPayDetails.Date_Paid__c = Date.today();
			toPay.addPaymentValue();
			toPay.savePayment();

			toPay = new client_course_instalment_pay(new ApexPages.StandardController(instalments2[1]));
			toPay.newPayDetails.Payment_Type__c = 'Creditcard';
			toPay.newPayDetails.Value__c = instalments2[1].Instalment_Value__c;
			toPay.newPayDetails.Date_Paid__c = Date.today();
			toPay.addPaymentValue();
			toPay.savePayment();
		Test.stopTest();
			
		}
  }

	static testMethod void retrieveFilters(){
		Test.startTest();
		system.runAs(portalUser){
			school_payments toTest = new school_payments();
			List<SelectOption> agencyGroupOptions = toTest.agencyGroupOptions;
			toTest.getDepartments();
			List<SelectOption> schoolOptions = toTest.schoolOptions;
			List<SelectOption> campusOptions = toTest.campusOptions;
			toTest.changeGroup();
		}
		Test.stopTest();
	}

	// static testMethod void getTotalsTabs(){
	// 	Test.startTest();
	// 	system.runAs(portalUser){
	// 		school_payments toTest = new school_payments();
	// 		toTest.getTotalFirst();
	// 		toTest.getTotalRepayment();
	// 		toTest.getTotalGroupPayment();
	// 	}
	// 	Test.stopTest();
	// }

	static testMethod void saveAndCancelChanges() {
		Test.startTest();
		system.runAs(portalUser){
			school_payments toTest = new school_payments();
			toTest.setupFirstPayments();

			Decimal originalScholarship = toTest.instalments[0].installment.Scholarship_Taken__c;
			String originalAgentDealName = toTest.instalments[0].installment.Agent_Deal_Name__c;
			Decimal originalAgentDeal = toTest.instalments[0].installment.Agent_Deal_Value__c;
			Decimal originalCommission = toTest.instalments[0].installment.Commission_Value__c;
			Decimal originalTax = toTest.instalments[0].installment.Commission_Tax_Value__c;

			ApexPages.CurrentPage().getParameters().put('id', string.valueOf(toTest.instalments[0].installment.id));

			toTest.instalments[0].installment.Scholarship_Taken__c = toTest.instalments[0].installment.Total_School_Payment__c + 1;
			toTest.instalments[0].installment.Hify_Agency_Commission__c = 10;
			toTest.instalments[0].installment.client_course__r.Hify_Agency_Total_Instalment_Collected__c = 10;
			toTest.instalments[0].installment.Hify_Agency_Instalment_Collected__c = 10;
			toTest.saveChanges();

			ApexPages.CurrentPage().getParameters().put('id', string.valueOf(toTest.instalments[0].installment.id));
			toTest.cancelChanges();
			// System.assertEquals(toTest.instalments[0].installment.Scholarship_Taken__c, originalScholarship);
			// System.assertEquals(toTest.instalments[0].installment.Agent_Deal_Name__c, originalAgentDealName);
			// System.assertEquals(toTest.instalments[0].installment.Agent_Deal_Value__c, originalAgentDeal);
			// System.assertEquals(toTest.instalments[0].installment.Commission_Value__c, originalCommission);
			// System.assertEquals(toTest.instalments[0].installment.Commission_Tax_Value__c, originalTax);

			toTest.instalments[0].installment.Scholarship_Taken__c = toTest.instalments[0].installment.Total_School_Payment__c - 1;
			toTest.saveChanges();

			// toTest.saveChangesAndRefresh();
			//TODO:KEEP FEE
		}
		Test.stopTest();
	}

	// static testMethod void saveChangesAndRefresh(){
	// 	system.runAs(portalUser){
	// 		school_payments toTest = new school_payments();
	// 		toTest.setupFirstPayments();
	// 		ApexPages.CurrentPage().getParameters().put('id', string.valueOf(toTest.instalments[0].installment.id));
	// 		// toTest.saveChangesAndRefresh();
	// 	}
	// }

	static testMethod void confirmSinglePayment(){
		Test.startTest();
		system.runAs(portalUser){
			/******* First Payments *******/
			school_payments toTest = new school_payments();
			toTest.setupFirstPayments();

			ApexPages.currentPage().getParameters().put('inst', string.valueOf(toTest.instalments[0].installment.id));
			ApexPages.currentPage().getParameters().put('ac','upSingle');

			// Validation if Document is not inserted
			toTest.confirmPayment();
			System.assertEquals(toTest.instalments[0].installment.Paid_To_School_On__c, null);


			//Add Fake Document
			String instId = toTest.instalments[0].installment.id;
			ApexPages.currentPage().getParameters().put('inst', string.valueOf(toTest.instalments[0].installment.id));
			ApexPages.currentPage().getParameters().put('ac','upSingle');
			client_course_instalment__c addDoc = [SELECT Id, Client_Document__r.preview_link__c FROM client_course_instalment__c WHERE id = :instId];
			addDoc.Client_Document__r.preview_link__c = 'testing...';
			update addDoc.Client_Document__r;


			toTest.confirmPayment();
			client_course_instalment__c updatedInstalment = [SELECT Id, Paid_To_School_On__c FROM client_course_instalment__c WHERE id = :instId];
			System.assertNotEquals(updatedInstalment.Paid_To_School_On__c, null);


			//Confirm in Bulk
			instId = toTest.instalments[0].installment.id; //There is one instalment left
			addDoc = [SELECT Id, Client_Document__r.preview_link__c FROM client_course_instalment__c WHERE id = :instId];
			addDoc.Client_Document__r.preview_link__c = 'testing...';
			update addDoc.Client_Document__r;
			ApexPages.currentPage().getParameters().put('ac','upAll');
			toTest.confirmPayment();
			System.assertNotEquals(updatedInstalment.Paid_To_School_On__c, null);



		}
		Test.stopTest();
	}

	static testMethod void groupedInvoicesFunctions(){
		Test.startTest();
		system.runAs(portalUser){
			school_payments toTest = new school_payments();
			toTest.setupRepayments();

			toTest.createInvoice(); //Create Catch Exception for not having any campus selected

			String instalmentId = toTest.allCampuses[0].cpI[0].installment.id;
			toTest.allCampuses[0].cpI[0].installment.isSelected__c = true;
			ApexPages.currentPage().getParameters().put('cpID',string.valueOf(toTest.allCampuses[0].id));
			ApexPages.currentPage().getParameters().put('typeGroup','0');
			toTest.createInvoice();

			client_course_instalment__c updatedInstalment = [Select Id, School_Payment_Invoice__c from client_course_instalment__c where id = :instalmentId];
			System.assertNotEquals(updatedInstalment.School_Payment_Invoice__c, null);

			//Create Second Invoice
			toTest.allCampuses[0].cpI[0].installment.isSelected__c = true;
			ApexPages.currentPage().getParameters().put('cpID',string.valueOf(toTest.allCampuses[0].id));
			ApexPages.currentPage().getParameters().put('typeGroup','0');
			
			toTest.createInvoice();
			client_course_instalment__c updatedInstalment2 = [Select Id, School_Payment_Invoice__c from client_course_instalment__c where id = :instalmentId];


			/*** Confirm Pay to School **/
			toTest.setupGroupPayment();
			System.assertEquals(toTest.invoices.size()>0, true);

			String instId = toTest.instalments[0].installment.id;
			ApexPages.currentPage().getParameters().put('inv', string.valueOf(updatedInstalment.School_Payment_Invoice__c));
			ApexPages.currentPage().getParameters().put('ac','upSingle');
			toTest.confirmPayment(); //Will cause an error for not having attached document

			//Add Fake Document to Invoice
			Invoice__c invDoc = [SELECT Id, Payment_Receipt__r.preview_link__c FROM Invoice__c WHERE id = :updatedInstalment.School_Payment_Invoice__c];
			invDoc.Payment_Receipt__r.preview_link__c = 'testing...';
			update invDoc.Payment_Receipt__r;

			//Update Single
			ApexPages.currentPage().getParameters().put('inv', string.valueOf(updatedInstalment.School_Payment_Invoice__c));
			ApexPages.currentPage().getParameters().put('ac','upSingle');
			toTest.confirmPayment();
			updatedInstalment = [SELECT Id, Paid_To_School_On__c, School_Payment_Invoice__c FROM client_course_instalment__c WHERE id = :updatedInstalment.id];
			System.assertNotEquals(updatedInstalment.Paid_To_School_On__c, null);


			//Update All
			invDoc = [SELECT Id, Payment_Receipt__r.preview_link__c FROM Invoice__c WHERE id = :updatedInstalment2.School_Payment_Invoice__c];
			invDoc.Payment_Receipt__r.preview_link__c = 'testing...';
			update invDoc.Payment_Receipt__r;

			ApexPages.currentPage().getParameters().put('inv', string.valueOf(updatedInstalment2.School_Payment_Invoice__c));
			ApexPages.currentPage().getParameters().put('ac','upAll');
			toTest.confirmPayment();
			updatedInstalment2 = [SELECT Id, Paid_To_School_On__c, School_Payment_Invoice__c FROM client_course_instalment__c WHERE id = :updatedInstalment2.id];
			System.assertNotEquals(updatedInstalment2.Paid_To_School_On__c, null);


			ApexPages.currentPage().getParameters().put('id',updatedInstalment2.School_Payment_Invoice__c);
			toTest.unconfirmPayment();
			updatedInstalment2 = [SELECT Id, Paid_To_School_On__c, School_Payment_Invoice__c FROM client_course_instalment__c WHERE id = :updatedInstalment2.id];
			System.assertEquals(updatedInstalment2.Paid_To_School_On__c, null);

			//Cancel Invoice
			ApexPages.currentPage().getParameters().put('id',string.valueOf(updatedInstalment.School_Payment_Invoice__c));
			toTest.cancelInvoice();

			updatedInstalment = [Select Id, School_Payment_Invoice__c from client_course_instalment__c where id = :instalmentId];
			System.assertEquals(updatedInstalment.School_Payment_Invoice__c, null);
		}
		Test.stopTest();
	}

	// static testMethod void changeSchool(){
	// 	school_payments toTest = new school_payments();
	// 	toTest.changeSchool();
	// }

	static testMethod void changePage(){
		Test.startTest();
		system.runAs(portalUser){
			school_payments toTest = new school_payments();
			toTest.setupRepayments();
			ApexPages.currentPage().getParameters().put('page', '1');

			//-----------> Clicking on "Next Button"
			toTest.Next();

			//-----------> Method: Find Pagination Items
			toTest.findPaginationItems();
			System.assertEquals(toTest.currentPage, 1);

			//-----------> Testing with page + than result pages
			ApexPages.currentPage().getParameters().put('page', string.valueOf(toTest.totPages.size()+1));
			System.assertEquals(toTest.currentPage, toTest.totPages.size());
		}
		Test.stopTest();
	}



}