public class scDetails {
	
	public boolean edit {get;set;}
	public boolean addingScGroup {get;set;}
	public Account acco {get;set;}
	public Account campus {get;set;}
	public Account schoolGroup {get;set;}
	String action;
	public list<SelectOption> allCountries {get;set;}
	public list<SelectOption> listSchoolsGroup {get;set;}
	public scDetails(){
		String sid = ApexPages.currentPage().getParameters().get('id');
		action = ApexPages.currentPage().getParameters().get('ac');
		addingScGroup = ApexPages.currentPage().getParameters().get('scgp') != null;
		
		edit =  false;
		
		if(sid != null)
			acco = [select id, ParentId , name, Nickname__c, Registration_name__c, Commission__c, Website, Total_Number_of_Students__c, Year_established__c, Partnership_started_on__c, Medical_Insurance_Company__c, Country_Pathways__c,Pathways_offered__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, About__c, Selling_Points__c, Type_of_Business_Number__c, Business_Number__c, Course_Types_Offered__c, Type_of_Institution__c from Account where id = :sid];
		else if(action != null && action == 'new'){
			map<string, string> recordTypes = new map<string, string>();
			for(RecordType rt:[Select id, name from RecordType where name in ('School','Campus','School Group') and isActive = true])
				recordTypes.put(rt.name, rt.id);
			if(!addingScGroup)
				acco = new Account(recordtypeid = recordTypes.get('School'));
			else acco = new Account(recordtypeid = recordTypes.get('School Group'));

			campus = new Account(recordtypeid = recordTypes.get('Campus'));
			edit = true;
		}		

		allCountries = new list<SelectOption>(IPFunctions.getAllCountries());

		listSchoolsGroup  = new list<SelectOption>(IPFunctions.getSchoolsGroup());
	}
	
	public PageReference save() {
        try{
            upsert acco;
            
            
            if(!addingScGroup){ 
				if(action != 'new')
					edit = false;
				else {
					campus.ParentId = acco.id;
					insert campus;
				}
			}else{
				PageReference redirect = new PageReference('/apex/scDetails');
				redirect.getParameters().put('currentPage', 'schooldetails');
				redirect.getParameters().put('ac', 'new');
				redirect.setRedirect(true);
				return redirect;
			}
        }catch(Exception e){
            ApexPages.addMessages(e);
        }
        
        return null;
    }
    
    public void edit(){
        edit = true;
    }

    public PageReference cancel(){
       	edit = false;
		if(addingScGroup){ 
			PageReference redirect = new PageReference('/apex/scDetails');
			redirect.getParameters().put('currentPage', 'schooldetails');
			redirect.getParameters().put('ac', 'new');
			redirect.setRedirect(true);
			return redirect;
		}
		else return null;
    }
    
    
     public List<SelectOption> getCurrencies() {
	    List<SelectOption> options = new List<SelectOption>();
	    for(SelectOption op: xCourseSearchFunctions.getCurrencies())
	      options.add(new SelectOption(op.getValue(),op.getLabel()));
	        
	    return options;
	 }
	 
	 
	 /* ADDRESS */
	 public string numberAddress {get;set;}
	 public string streetAddress {get;set;}

}