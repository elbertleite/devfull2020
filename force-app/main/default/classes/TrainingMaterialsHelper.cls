public virtual class TrainingMaterialsHelper {
	
	public User currentUser{get; set;}
	public String globalLink{get;set;}
	private Integer userPermissionLevel;
	public static String urlBase{get;set;}

	public boolean isSearchResult{get;set;}

	public Integer maxVisibilitySaveEditFolder{get;set;}
	
	public TrainingMaterialsHelper() {
		urlBase = retrieveURLBase();
		currentUser = [Select Id, Name, Contact.Account.BillingCountry, PermissionLevelAccessFolders__c, PermissionCreateFoldersPackages__c, Contact.Account.ParentId, Contact.AccountId, Contact.Account.Global_Link__c from User where id = :UserInfo.getUserId() limit 1];		
		globalLink = currentUser.Contact.Account.Global_Link__c;
		userPermissionLevel = Integer.valueOf(currentUser.PermissionLevelAccessFolders__c);
	}

	public video_upload__c retrieveObject(String id){
		video_upload__c vd = null;
		try{
			vd = [SELECT id, name, objectType__c FROM video_upload__c WHERE id = :id AND permissionLevelRequired__c <= :userPermissionLevel];
		}catch(QueryException ex){
			system.debug('Material not available for your permission level ===> ' + ex);
		}
		return vd;
	}

	public String retrieveURLBase(){
		URL url = Url.getCurrentRequestUrl();
		String queryString = '?sfdc.tabName=';
		String tabId = ApexPages.currentPage().getParameters().get('sfdc.tabName');
		if(IPFunctions.isValidObject(tabId)){
			queryString = queryString + tabId;
		}
		return 'https://' + url.getHost() + url.getPath() + queryString;
	}

	public Folder retrieveRootFolder(String id){
		Account ag;
		Folder f;
		try{
			ag = new MethodsWithoutSharing().retrieveRootFolder(id);
			f = new Folder(ag.id, ag.name, ag.ParentId, ag.RecordType.Name, 1, getLabelPermissionAccess(1), false, false, false);
		}catch(QueryException ex){
			system.debug('No element to be retrieved ===> ' + ex);
		}
		return f;
	}

	public Folder retrieveSubFolder(String id, Boolean checkPermission){
		video_upload__c vd;
		Folder f;
		try{
			if(checkPermission){
				vd = [SELECT id, name, parentFolderID__c, permissionLevelRequired__c, Is_Training_Subfolder__c, Available_Only_To_Group__c FROM video_upload__c WHERE id = :id AND objectType__c = 'Materials Folder' AND permissionLevelRequired__c <= :userPermissionLevel];
			}else{
				vd = [SELECT id, name, parentFolderID__c, permissionLevelRequired__c, Is_Training_Subfolder__c, Available_Only_To_Group__c FROM video_upload__c WHERE id = :id AND objectType__c = 'Materials Folder'];
			}
			f = new Folder(vd.id, vd.name, vd.parentFolderID__c, null, vd.permissionLevelRequired__c, getLabelPermissionAccess(vd.permissionLevelRequired__c), checkFolderCanBeModified(vd.id), vd.Is_Training_Subfolder__c, vd.Available_Only_To_Group__c);
		}catch(QueryException ex){
			system.debug('Folder not available for your permission level ===> ' + ex);
			return null;
		}
		return f;
	}

	public boolean checkFolderCanBeModified(String idFolder){
		return Database.countQuery('SELECT COUNT() FROM video_upload__c WHERE parentFolderID__c = :idFolder') == 0;
	}

	public String getLabelPermissionAccess(Decimal permissionLevel){
		Schema.DescribeFieldResult fieldResult = Schema.User.PermissionLevelAccessFolders__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry f : ple){
			if(Integer.valueOf(f.getValue()) == Integer.valueOf(permissionLevel)){
				return f.getLabel();
			}
		}
		return null;
	}

	public List<Folder> retrieveRootSubfolders(String id){
		List<Folder> rootFolders = new List<Folder>();
		/*for(Account ag : [SELECT id, name, ParentId FROM Account WHERE ParentId = :id AND Hify_Agency__c = false AND Inactive__c  = false AND (RecordType.Name = 'Agency Group' OR RecordType.Name = 'Agency') order by Name]){
			rootFolders.add(new Folder(ag.id, ag.name, ag.ParentId, null));		
		}*/
		return rootFolders;
	}

	public List<Folder> retrieveAllSubfolders(){
		List<Folder> subFolders = new List<Folder>();
		Map<Id, video_upload__c> videoSubFolders =  new Map<Id, video_upload__c>([SELECT id, name, parentFolderID__c, permissionLevelRequired__c, Is_Training_Subfolder__c, Available_Only_To_Group__c FROM video_upload__c WHERE objectType__c = 'Materials Folder' AND permissionLevelRequired__c <= :userPermissionLevel order by createdDate]);
		
		Map<String, Integer> subFoldersPerParent = new Map<String, Integer>();
		for(AggregateResult agg : [SELECT parentFolderID__c idItem, COUNT(ID) cnt FROM video_upload__c WHERE parentFolderID__c IN :videoSubFolders.keySet() GROUP BY parentFolderID__c]){
			subFoldersPerParent.put((string) agg.get('idItem'), (Integer) agg.get('cnt'));
		}
		
		Map<Id, Integer> subFolderPerParent = new Map<Id, Integer>();
		for(video_upload__c subfolder : videoSubFolders.values()){
			subFolders.add(new Folder(subfolder.id, subfolder.name, subfolder.parentFolderID__c, null, subfolder.permissionLevelRequired__c, getLabelPermissionAccess(subfolder.permissionLevelRequired__c), !subFoldersPerParent.containsKey(subfolder.id) || subFoldersPerParent.get(subfolder.id) == 0, subFolder.Is_Training_Subfolder__c, subFolder.Available_Only_To_Group__c));
		}
		return subFolders;
	}

	public List<Folder> retrieveSubfolders(String id){
		system.debug('ENTROU NO SUBFOLDERS ');
		List<Folder> subFolders = new List<Folder>();

		Map<Id, video_upload__c> videoSubFolders =  new Map<Id, video_upload__c>([SELECT id, name, parentFolderID__c, permissionLevelRequired__c, Is_Training_Subfolder__c, Available_Only_To_Group__c FROM video_upload__c WHERE parentFolderID__c = :id AND objectType__c = 'Materials Folder' AND permissionLevelRequired__c <= :userPermissionLevel ORDER BY Is_Training_Subfolder__c desc, Name]);

		Map<String, Integer> subFoldersPerParent = new Map<String, Integer>();
		for(AggregateResult agg : [SELECT parentFolderID__c idItem, COUNT(ID) cnt FROM video_upload__c WHERE parentFolderID__c IN :videoSubFolders.keySet() GROUP BY parentFolderID__c]){
			subFoldersPerParent.put((string) agg.get('idItem'), (Integer) agg.get('cnt'));
		}

		for(video_upload__c subfolder : videoSubFolders.values()){
			subFolders.add(new Folder(subfolder.id, subfolder.name, subfolder.parentFolderID__c, null, subfolder.permissionLevelRequired__c, getLabelPermissionAccess(subfolder.permissionLevelRequired__c), !subFoldersPerParent.containsKey(subfolder.id) || subFoldersPerParent.get(subfolder.id) == 0, subfolder.Is_Training_Subfolder__c, subFolder.Available_Only_To_Group__c));
		}
		return subFolders;
	}

	public List<Folder> retrieveRootParentFolders(){
		List<Folder> rootFolders = new List<Folder>();
		List<Account> accounts = new MethodsWithoutSharing().retrieveRootParentFolders(globalLink);
		for(Account ag : accounts){
			rootFolders.add(new Folder(ag.id, ag.name, ag.ParentId, null, 1, getLabelPermissionAccess(1), false, false, false));		
		}
		return rootFolders;
	}

	public static TrainingMaterial loadPackageForEdition(String id){
		TrainingMaterial material = new TrainingMaterial();
		video_upload__c video = [SELECT ID, Name, Description__c, parentFolderID__c, materialLinks__c, trainingEstimatedHours__c, trainingEstimatedMinutes__c, parentFolderName__c, trainingLanguages__c, trainingMaterialsRelated__c, materialAgencyGroup__r.Name, CreatedBy.Name, permissionLevelRequired__c FROM video_upload__c WHERE ID = :id];	
		material.id = video.Id; 
		material.name = video.Name;  
		material.description = video.Description__c; 
		material.parentFolderID = video.parentFolderID__c; 
		material.parentFolderName = video.parentFolderName__c; 
		material.materialAgencyGroupName = video.materialAgencyGroup__r.Name; 
		material.estimatedHours = video.trainingEstimatedHours__c;
		material.estimatedMinutes = video.trainingEstimatedMinutes__c;
		material.permissionLevelRequired = Integer.valueOf(video.permissionLevelRequired__c);
		String [] idsMaterials = video.trainingMaterialsRelated__c.split(';;');
		material.materialModules = new List<MaterialModule>();
		for(String idMaterial : idsMaterials){
			if(idMaterial != null && !''.equals(idMaterial)){
				video_upload__c module = [SELECT ID, Name, materialLanguage__c from video_upload__c WHERE ID = :idMaterial];
				material.materialModules.add(new MaterialModule(module.ID, module.Name, module.materialLanguage__c));
			}
		}
		return material;
	}

	public static Material loadFullMaterial(String idMaterial){
		User usr = [Select Id, PermissionLevelAccessFolders__c from User where id = :UserInfo.getUserId()];
		video_upload__c video = [SELECT ID, Name, Description__c, LastModifiedBy.Name, previewLink__c, materialLanguage__c, parentFolderID__c, LastModifiedDate, materialLinks__c, materialComments__c, trainingEstimatedHours__c, trainingEstimatedMinutes__c, parentFolderName__c, objectType__c, trainingMaterialsRelated__c, materialAgencyGroup__r.Name, CreatedBy.Name FROM video_upload__c WHERE ID = :idMaterial AND permissionLevelRequired__c <= :Integer.valueOf(usr.PermissionLevelAccessFolders__c)];
		if(video != null){
			if(video.objectType__c == 'Material'){
				MaterialModule material = new MaterialModule();
				material.id = video.Id; 
				material.name = video.Name; 
				material.type = video.objectType__c; 
				material.description = video.Description__c; 
				material.parentFolderID = video.parentFolderID__c; 
				material.parentFolderName = video.parentFolderName__c; 
				material.materialAgencyGroupName = video.materialAgencyGroup__r.Name; 
				material.lastModifiedDate = video.LastModifiedDate;
				material.createdBy = video.LastModifiedBy.Name;
				material.materialUrl = generateMaterialURL(material.Id);
				material.language = video.materialLanguage__c; 
	
				if(IPFunctions.isValidObject(video.previewLink__c)){
					List<MaterialFile> files = new List<MaterialFile>();
					MaterialFile mf;
					String [] file;
					for(String filePath : video.previewLink__c.split(';;')){
						file = filePath.split('!#!');
						mf = new MaterialFile();
						mf.name = file[1];
						mf.link = file[0];
						files.add(mf);
					}
					material.files = files;
				}
				if(IPFunctions.isValidObject(video.materialLinks__c)){
					List<MaterialLink> links = new List<MaterialLink>();
					MaterialLink li;
					String [] link;
					String linkName;
					for(String linkAttributes : video.materialLinks__c.split('!#!')){
						linkName = '';
						link = linkAttributes.split(';;');
						li = new MaterialLink();
						li.type = link[0];
						if(li.type == 'youtubeVideo'){
							linkName = 'Youtube - ';
						}else if(li.type == 'vimeoVideo'){
							linkName = 'Vimeo - ';
						}else if(li.type == 'prezi'){
							linkName = 'Prezi - ';
						}else if(li.type == 'website'){
							linkName = 'Website - ';
						}
						li.name = linkName + link[1];
						li.link = link[2];
						links.add(li);
					}
					material.links = links;
				}
				if(IPFunctions.isValidObject(video.materialComments__c)){
					List<IPFunctions.noteWrapper> comments = new List<IPFunctions.noteWrapper>();
					IPFunctions.noteWrapper comment;
					for(String jsonComment : video.materialComments__c.split(';!;')){
						comment = (IPFunctions.noteWrapper) JSON.deserialize(jsonComment, IPFunctions.noteWrapper.class);
						comments.add(comment);
					}
					material.comments = comments;
				}
				return material;
			}else if(video.objectType__c == 'Training'){
				TrainingMaterial material = new TrainingMaterial();
				material.id = video.Id; 
				material.name = video.Name; 
				material.type = video.objectType__c; 
				material.description = video.Description__c; 
				material.parentFolderID = video.parentFolderID__c; 
				material.parentFolderName = video.parentFolderName__c; 
				material.materialAgencyGroupName = video.materialAgencyGroup__r.Name; 
				material.lastModifiedDate = video.LastModifiedDate;
				material.createdBy = video.LastModifiedBy.Name;
				material.materialUrl = generateMaterialURL(material.Id);
				material.materialModules = new List<MaterialModule>();
				String [] idsMaterials = video.trainingMaterialsRelated__c.split(';;');
				for(String id : idsMaterials){
					if(id != null && !''.equals(id)){
						try{
							video_upload__c module = [SELECT ID, Name from video_upload__c WHERE ID = :id AND permissionLevelRequired__c <= :Integer.valueOf(usr.PermissionLevelAccessFolders__c)];
							if(module != null){
								material.materialModules.add(new MaterialModule(module.ID, module.Name));
							}
						}catch(QueryException ex){
							system.debug('Material not available for your permission level ===> ' + ex);
						}
					}
				}
		
				return material;
			}
		}
		return null;
	}

	public static String generateMaterialURL(String materialID){
		URL url = Url.getCurrentRequestUrl();
		String queryString = '?sfdc.tabName=';
		String tabId = ApexPages.currentPage().getParameters().get('sfdc.tabName');
		if(IPFunctions.isValidObject(tabId)){
			queryString = queryString + tabId;
		}
		return 'https://' + url.getHost() + url.getPath() + queryString + '&id='+materialID;
	}

	public List<Material> retrieveMaterials(String id){
		List<Material> materials = new List<Material>();
		List<video_upload__c> result = null;
		if(id != null){
			result = [SELECT ID, Name, Description__c, LastModifiedBy.Name,  previewLink__c, materialLanguage__c, LastModifiedDate, materialLinks__c , trainingEstimatedHours__c, 
			                 parentFolderID__c, trainingEstimatedMinutes__c, parentFolderName__c, materialAgencyGroup__r.Name, objectType__c, CreatedBy.Name 
							          FROM video_upload__c WHERE (objectType__c = 'Material' OR objectType__c = 'Training') 
									  AND parentFolderID__c = :id AND permissionLevelRequired__c <= :userPermissionLevel
									         ORDER BY objectType__c DESC, Name];
		}else{
			//@TODO - Choose materials to load when land in Home page
		}
		if(result != null && !result.isEmpty()){
			for(video_upload__c obj : result){
				materials.add(fromVideoObjToMaterial(obj));
			}
		}
		return materials;
	}

	public Material fromVideoObjToMaterial(video_upload__c obj){
		Material material = new Material();
		material.id = obj.Id; 
		material.name = obj.Name; 
		material.type = obj.objectType__c; 
		material.description = obj.Description__c; 
		material.parentFolderID = obj.parentFolderID__c; 
		material.parentFolderName = obj.parentFolderName__c; 
		material.materialAgencyGroupName = obj.materialAgencyGroup__r.Name; 
		material.lastModifiedDate = obj.LastModifiedDate;
		material.createdBy = obj.LastModifiedBy.Name;
		if(obj.objectType__c == 'Training'){
			material.estimatedHours = obj.trainingEstimatedHours__c;
			material.estimatedMinutes = obj.trainingEstimatedMinutes__c;
		}else{					
			material.language = obj.materialLanguage__c;
			material.previewLink = obj.previewLink__c;
			material.materialLinks = obj.materialLinks__c;	
		}
		return material;
	}

	public List<Material> searchMaterials(String nameDescriptionSearch, String languageSearch){
		List<Material> materials = new List<Material>();
		if(IPFunctions.isValidObject(nameDescriptionSearch)){	
			String queryLanguage = null;
			if(IPFunctions.isValidObject(languageSearch)){
				queryLanguage = 'materialLanguage__c WHERE materialLanguage__c = \''+languageSearch/*IPFunctions.getLanguage(languageSearch)*/+'\' AND (objectType__c = \'Material\' OR objectType__c = \'Training\') '; 
			}else{
				queryLanguage = 'materialLanguage__c WHERE objectType__c = \'Material\'';
			}

			String searchQuery = 'FIND {'+nameDescriptionSearch+'} RETURNING video_upload__c(ID, Name, previewLink__c, LastModifiedBy.Name, materialLinks__c, parentFolderID__c, trainingEstimatedHours__c, trainingEstimatedMinutes__c, Description__c, objectType__c, parentFolderName__c, materialAgencyGroup__r.Name, LastModifiedDate,  CreatedBy.Name, '+queryLanguage+' AND permissionLevelRequired__c <= '+userPermissionLevel+' )';
			List<List<sObject>> result = search.query(searchQuery);
			video_upload__c[] objs = (video_upload__c[])result[0];
			for(video_upload__c obj : objs){
				materials.add(fromVideoObjToMaterial(obj));
			}

		}else{
			if(IPFunctions.isValidObject(languageSearch)){
				//languageSearch = IPFunctions.getLanguage(languageSearch);
				for(video_upload__c obj : [SELECT ID, Name, Description__c, LastModifiedBy.Name, previewLink__c, materialLanguage__c, LastModifiedDate, materialLinks__c , parentFolderID__c, trainingEstimatedHours__c, trainingEstimatedMinutes__c, parentFolderName__c, materialAgencyGroup__r.Name, CreatedBy.Name, objectType__c FROM video_upload__c WHERE (objectType__c = 'Material' OR  objectType__c = 'Training') AND materialLanguage__c = :languageSearch AND permissionLevelRequired__c <= :userPermissionLevel ORDER BY objectType__c DESC, LastModifiedDate]){
					materials.add(fromVideoObjToMaterial(obj));
				}
			}
		}
		this.isSearchResult = true;
		return materials;
	}
	
	public List<Link> getLinkTypes(){
		List<Link> optionList = new List<Link>();
		optionList.add(new Link('youtubeVideo', 'Youtube Video'));
		optionList.add(new Link('vimeoVideo', 'Vimeo Video'));
		optionList.add(new Link('prezi', 'Prezi - Presentation'));
		optionList.add(new Link('website', 'Website'));
		optionList.add(new Link('other', 'Other'));
		return optionList;
	}

	public List<SelectOption> getVisibilityLevels(){
		List<SelectOption> options = new List<SelectOption>();
		Schema.DescribeFieldResult fieldResult = User.PermissionLevelAccessFolders__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

		for( Schema.PicklistEntry f : ple){
			if(Integer.valueOf(f.getValue()) <= userPermissionLevel){
				if(maxVisibilitySaveEditFolder == null){
					options.add(new SelectOption(f.getValue(), f.getLabel()));
				}else{
					if(Integer.valueOf(f.getValue()) >= maxVisibilitySaveEditFolder){
						options.add(new SelectOption(f.getValue(), f.getLabel()));
					}
				}
			}	
		}
		return options;
	}

	public void updateSelectVisibilitiesModalNewFolder(){
		String parentID = ApexPages.currentPage().getParameters().get('folderID');
		if(parentID.startsWith('001')){
			this.maxVisibilitySaveEditFolder = null;
		}else{
			video_upload__c folder = [SELECT id, name, parentFolderID__c, permissionLevelRequired__c FROM video_upload__c WHERE ID = :parentID];
			this.maxVisibilitySaveEditFolder = Integer.valueOf(folder.permissionLevelRequired__c);
		}
	}

	public List<SelectOption> getAllAgencyGroups(){
		return new IPFunctions.agencyNoSharing().getAllGroups();
	}

	public List<SelectOption> getLanguages() {
		List<SelectOption> options = new List<SelectOption>();
		List<SelectOption> languages = new global_class().getLanguages();
		for(SelectOption language : languages){
			options.add(new SelectOption(language.getLabel(), language.getLabel()));
		}
		return options;
	}

	public video_upload__c save(video_upload__c material){
        try{
			upsert material;
			return material;
		}catch(Exception e){
			system.debug('Error at saving ===> ' + e);	
		}
		return null;
    }

	public void deleteFolder(String id){
		new MethodsWithoutSharing().deleteObject(id);
	}

	public void deleteIPCloudMaterial(String id){
		new MethodsWithoutSharing().deleteObject(id);
	}

	public without sharing class MethodsWithoutSharing{
		public void deleteObject(String id){
			try{
				video_upload__c folder = new video_upload__c();
				folder.id = id;
				delete folder;
			}catch(Exception e){
				system.debug('Error at deleting ===> ' + e);	
			}
		}

		public List<Account> retrieveRootParentFolders(String globalLink){
			return [SELECT id, name, ParentId FROM Account WHERE RecordType.Name = 'Agency Group' AND Inactive__c = false AND Global_Link__c = :globalLink order by Name];
		}

		public Account retrieveRootFolder(String id){
			return [SELECT id, name, ParentId, RecordType.Name FROM Account WHERE id = :id AND (RecordType.Name = 'Agency Group' OR RecordType.Name = 'Agency')];
		}
	}

	public class Folder{
		public String id{get;set;}
		public String parentId{get;set;}
		public String recordType{get;set;}
		public String name{get;set;}
		public Integer permission{get;set;}
		public String permissionLabel{get;set;}
		public Boolean canBeModified{get;set;}
		public Boolean trainingFolder{get;set;}
		public List<Folder> subFolders{get;set;}
		public Boolean availableOnlyToGroup{get;set;}

		public Folder(){}

		public Folder(String id){
			this.id = id;
		}
		
		public Folder(String id, String name, String parentId, String recordType, Decimal permission, String permissionLabel, Boolean canBeModified, Boolean trainingFolder, Boolean availableOnlyToGroup){
			this.id = id;
			this.name = name;
			this.parentId = parentId;
			this.recordType = recordType;
			if(permission != null){
				this.permission = Integer.valueOf(permission);
			}
			this.permissionLabel = permissionLabel;
			this.canBeModified = canBeModified;
			this.trainingFolder = trainingFolder;
			if(availableOnlyToGroup != null){
				this.availableOnlyToGroup = availableOnlyToGroup;
			}else{
				this.availableOnlyToGroup = false;
			}
		}

		public Boolean equals(Object obj) {
        if (obj instanceof Folder) {
            Folder fd = (Folder)obj;
				if(id != null){
                	return id.equals(fd.id);
				}
				return false;
            }
            return false;
        }
        public Integer hashCode() {
            return id.hashCode();
        }
	}

	public class LinkBreadCrumb{
		public String name{get;set;}
		public String id{get;set;}

		public LinkBreadCrumb(){}

		public LinkBreadCrumb(String name, String id){
			this.name = name;
			this.id = id;
		}

		public Boolean equals(Object obj) {
        if (obj instanceof LinkBreadCrumb) {
            LinkBreadCrumb lk = (LinkBreadCrumb)obj;
				if(id != null){
                	return id.equals(lk.id);
				}
				return false;
            }
            return false;
        }
        public Integer hashCode() {
            return id.hashCode();
        }
	}
	
	public class Link{
		public String type{get;set;}
		public String label{get;set;}
		public String name{get;set;}
		public String url{get;set;}

		public Link(){}

		public Link(String type, String label){
			this.type = type;
			this.label = label;
		}

		public Link(String type, String name, String url){
			this.type = type;
			this.name = name;
			this.url = url;
		}
	}

	public class FolderWrapper implements Comparable{
		public Folder folder{get;set;}

		public FolderWrapper(Folder m){
			this.folder = m;
		}

		public Integer compareTo(Object compareTo){
			FolderWrapper mw2 = (FolderWrapper) compareTo;
			if(!String.isEmpty(Folder.name)){
				return Folder.name.compareTo(mw2.Folder.name);
			}
			return 0;
		}
	}

	public virtual class Material{
		public String id {get;set;}
		public String name{get;set;}
		public String type{get;set;}
		public String description{get;set;}
		public String createdBy{get;set;}
		public String parentFolderID{get;set;}
		public String parentFolderName{get;set;}
		public String materialAgencyGroupName{get;set;}
		public Datetime lastModifiedDate{get;set;}
		public Integer permissionLevelRequired{get;set;}
		public String language{get;set;}
		public String previewLink{get;set;}
		public String materialLinks{get;set;}
		public String materialUrl{get;set;}
		public Boolean trainingFolder{get;set;}
		public Decimal estimatedHours{get;set;}
		public Decimal estimatedMinutes{get;set;}

	}

	public class MaterialModule extends Material{

		public List<MaterialFile> files {get;set;}
		public List<MaterialLink> links {get;set;}
		public List<IPFunctions.noteWrapper> comments{get;set;}

		public MaterialModule(){}

		public MaterialModule(String id){
			this.id = id;
		}

		public MaterialModule(String id, String name){
			this.id = id;
			this.name = name;
		}

		public MaterialModule(String id, String name, String language){
			this.id = id;
			this.name = name;
			this.language = language;
		}

		public Boolean equals(Object obj) {
        if (obj instanceof MaterialModule) {
            MaterialModule m = (MaterialModule)obj;
				if(id != null){
                	return id.equals(m.id);
				}
				return false;
            }
            return false;
        }
        public Integer hashCode() {
            return id.hashCode();
        }

	}

	public class TrainingMaterial extends Material{
		public MaterialModule selected{get;set;}

		public List<MaterialModule> materialModules{get;set;}

		public boolean hasNext {get;set;}
		public boolean hasPrevious {get;set;}

		public void init(){
			hasNext = false;
			hasPrevious = false;
			if(materialModules.size() > 0){
				selected = (MaterialModule) loadFullMaterial(materialModules.get(0).id);
				if(materialModules.size() > 1){
					hasNext = true;
				}
			}
		}

		public void openNext(){
			Integer index = materialModules.indexOf(selected);
			String id = materialModules.get(index+1).id;
			selected = (MaterialModule) TrainingMaterialsHelper.loadFullMaterial(id);
			updatePreviousNext();
		}

		public void openPrevious(){
			Integer index = materialModules.indexOf(selected);
			String id = materialModules.get(index-1).id;
			selected = (MaterialModule) TrainingMaterialsHelper.loadFullMaterial(id);
			updatePreviousNext();
		}

		public void openModule(String idModule){
			selected = (MaterialModule) TrainingMaterialsHelper.loadFullMaterial(idModule);
			updatePreviousNext();
		}

		private void updatePreviousNext(){
			Integer indexSelected  = materialModules.indexOf(selected);
			if(indexSelected == 0){
				hasPrevious = false;
			}else{
				hasPrevious = true;
			}

			if(indexSelected == materialModules.size() - 1){
				hasNext = false;
			}else{
				hasNext = true;
			}
		}
	}

	public class MaterialFile{
		public String name{get;set;}
		public String link{get;set;}
	}

	public class MaterialLink{
		public String type{get;set;}
		public String name{get;set;}
		public String link{get;set;}
	}

}