public without sharing class commissions_request_pds {

		public list<groupResult> result {get;set;}
		public list<Integer> totPages {get;set;}
		public list<SelectOption> instOptions {get{
			if(instOptions == null){
				instOptions = new list<SelectOption>();
				instOptions.add(new SelectOption('none','-- Select Option --'));
				instOptions.add(new SelectOption('editInst','Edit Instalment'));
				instOptions.add(new SelectOption('noCommission','No Comm. Claim'));
			}
			return instOptions;
		}set;}
		public Integer totalResult {get;set;}
		public Integer totSendInvoice {get;set;}
		public Integer totPendingConf {get;set;}
		public Integer totToConfirm {get;set;}

		public Integer currentPage {get{if(currentPage==null) currentPage = 1; return currentPage;}set;}
		public client_course_instalment__c cancelInst {get{if(cancelInst==null) cancelInst = new client_course_instalment__c(); return cancelInst;}set;}
		public boolean showError {get{if(showError == null) showError = false; return showError;}set;}
		public String cancelReason {get;set;}
		public list<SelectOption> backOfficeOptions{get; set;}

		private string cancelInstalment {get;set;}
		private String sqlInstFields {get;set;}
		private String pageType {get;set;}

		private map<Integer, list<ID>> pageIds {get;set;}
		private map<String, list<String>> bo {get;set;}
		private list<installmentDetails> instalments {get;set;}
		private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}
		public string agenciesUnderBackoffice{get{if(agenciesUnderBackoffice == null) agenciesUnderBackoffice = ''; return agenciesUnderBackoffice;} set;} 

		public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}

		//Main Constructor
		public commissions_request_pds(){

			//bo = ff.findBOAgenciesPerService(currentUser.Contact.AccountId, new list<String>{'PDS_PCS_PFS_Chase'});
			backOfficeOptions = FinanceFunctions.retrieveBackOfficeOptions();

			String pBO = ApexPages.currentPage().getParameters().get('bk') != null && ApexPages.currentPage().getParameters().get('bk') != '' ? ApexPages.currentPage().getParameters().get('bk') : currentUser.Contact.AccountId;
			
			for(SelectOption so : backOfficeOptions)
				if(selectedBackOffice == null || so.getValue() == pBO)
					selectedBackOffice = so.getValue();


			findBackoffice();
			instalmentsFields();
			checkFilters();
			searchToRequest();
		}

		private void findBackoffice(){
			bo = ff.findBOAgenciesPerService(selectedBackOffice, new list<String>{'PDS_PCS_PFS_Chase'});
		}

		public void changeBackOffice(){
			findBackoffice();
			allFilters = ff.pdsFlowBOFilters(bo);
			countryOptions = allFilters.get('allCountries');
			if(countryOptions != null && countryOptions.size() > 0){
				selectedCountry = countryOptions[0].getValue();
				changeCountry();
			}
			else{
				schGroupOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
				schoolOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
				campusOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
			}
		} 

		public string selectedBackOffice{get; set;}

		//Field for Intalments Query
		private void instalmentsFields(){
			 sqlInstFields ='SELECT id, Name, Confirmed_Date__c, Confirmed_By__c, Instalment_Value__c, Due_Date__c, Commission__c, Client_not_Contacted__c, ' +
							'Commission_Value__c,Extra_Fee_Value__c, Split_Number__c, Status__c, Discount__c, ' +
							'Total_School_Payment__c, Number__c, Commission_Tax_Value__c,Commission_Tax_Rate__c, Balance__c, ' +
							'Paid_To_School_On__c, Tuition_Value__c, isPDS__c, instalment_activities__c, Kepp_Fee__c, Agent_Deal_Name__c, Agent_Deal_Value__c, ' +
							'Received_By_Agency__r.Name, Received_By_Agency__r.Parent.Name,  Received_By_Agency__r.Parent.Answer_PDS_Confirmation__c, Received_by_Department__r.Name, Commission_Due_Date__c,' +
							' Scholarship_Taken__c, Covered_By_Agency__c, Total_Paid_School_Credit__c, isSelected__c, PDS_Note__c, School_PDS_Notes__c, '+

							'isInstalment_Amendment__c, '+

							'client_course__r.client__c, ' +
							'client_course__r.client__r.Name, ' +
							'client_course__r.Start_Date__c, ' +
							'client_course__r.Course_Name__c, ' +
							'client_course__r.Campus_Name__c, ' +
							'client_course__r.client__r.Owner__r.Name, ' +
							'client_course__r.Campus_Id__c, ' +
							'client_course__r.School_Id__c, ' +
							'client_course__r.Commission_Tax_Rate__c, ' +
							'client_course__r.Commission_Type__c,' +
							'client_course__r.Commission_Comments__c,' +
							'client_course__r.Commission_Tax_Name__c,' +
							'client_course__r.isPackage__c,' +
							'client_course__r.course_package__c,' +
							'client_course__r.School_Name__c,' +
							'client_course__r.CurrencyIsoCode__c ,' + 
							'client_course__r.campus_course__r.campus__r.Parent.ParentId,'+
							'client_course__r.campus_course__r.campus__r.Parent.Parent.Name,'+
							'client_course__r.campus_course__r.campus__r.Parent.Parent.Registration_Name__c ,' + 


							//Tasks
							'(SELECT Id,instalment__c, isNote__c,Priority__c, Subject__c, Related_Contact__r.Name, Related_Contact__c, Due_Date__c, Comments__c, Assign_To__r.Name, AssignedBy__r.Name, Status__c, AssignedOn__c, LastModifiedBy.Name, LastModifiedDate, CreatedDate, CreatedBy.Name FROM Custom_Notes_Tasks__r where isNote__c = true) ' +

					'FROM client_course_instalment__c ';
		}

		//Find PDS To Request Commission
		private String sqlWhere;

		public void searchToRequest(){

			if(currentUser.Finance_Backoffice_User__c && bo.get(selectedCountry) != null && selectedSchoolGP!=null && selectedSchoolGP != 'none'){

				list<String> boAgencies = bo.get(selectedCountry);
				agenciesUnderBackoffice = '';
				list<String> agenciesBO = new list<String>(); 
				integer counter = 0;
				for(String agBo : boAgencies){
						
					list<String> ag = agBo.split(';-;');
					if(counter>0)
						agenciesUnderBackoffice += ', ' + ag[1];
					else
						agenciesUnderBackoffice += ag[1];
					agenciesBO.add(ag[0]);
					counter++;
				}//end for

				sqlWhere = '';

				String sql = 'SELECT ID, Received_By_Agency__r.ParentId, Received_By_Agency__r.Parent.Name, Received_By_Agency__c, Received_By_Agency__r.Name FROM client_course_instalment__c WHERE ';

				if(selectedAgencyGroup == null || selectedAgencyGroup == '' || selectedAgencyGroup == 'all'){
					sqlWhere += ' Received_By_Agency__c in ( \''+ String.join(agenciesBO, '\',\'') + '\' ) and Client_Course__r.Course_Country__c = \'' + selectedCountry + '\' ';
				}
				else if(selectedAgency != 'all'){
					sqlWhere += ' Received_By_Agency__c = \''+selectedAgency+'\' and Client_Course__r.Course_Country__c = \'' + selectedCountry + '\' ';
				}else if(selectedAgencyGroup != 'all'){
					sqlWhere += ' Received_By_Agency__r.ParentID = \''+selectedAgencyGroup+'\' and Client_Course__r.Course_Country__c = \'' + selectedCountry + '\' ';
				}

				if(selectedSchoolGP != null && selectedSchoolGP != 'none' && selectedSchoolGP != 'all')
					sqlWhere += ' AND  client_course__r.campus_course__r.campus__r.Parent.ParentId = \''+selectedSchoolGP+'\' ';

				if(selectedSchool != null && selectedSchool != '' && selectedSchool != 'all')
					sqlWhere += ' AND  Client_Course__r.School_Id__c = \''+selectedSchool+'\' ';
				
				if(selectedCampus != null && selectedCampus != '' &&  selectedCampus!='all')
					sqlWhere += ' AND  client_course__r.Campus_Id__c = \''+selectedCampus+'\' ';
				

				if(selectedPayment=='firstPayment')
					sqlWhere += ' AND (isFirstPayment__c = TRUE OR Required_for_COE__c= TRUE) '; //First Payments
				else if(selectedPayment=='repayments')
					sqlWhere += ' AND isFirstPayment__c = FALSE AND Required_for_COE__c= FALSE '; //Repayments

				
				if(searchName != null && searchName.length()>0){
					sqlWhere += ' AND Client_Course__r.Client__r.Name LIKE  \'%'+ searchName +'%\' ';
				}

				sqlWhere += ' AND ((isPDS__c = TRUE AND isMigrated__c = FALSE) OR (isPDS__c = TRUE AND PFS_Pending__c = TRUE)) '; //IS PDS

				sqlWhere += ' AND PDS_Requested_On__c = null AND Commission_Confirmed_On__c = null '; //PDS Requested

				if(selectedDateFilterBy == 'commdue'){
					sqlWhere+= ' AND ((Commission_Due_Date__c >= '+ IPFunctions.FormatSqlDateIni(dates.Commission_Due_Date__c); //Commission Due Date

					sqlWhere+= ' AND Commission_Due_Date__c <= '+ IPFunctions.FormatSqlDateFin(dates.Commission_Paid_Date__c); //Commission Due Date
					if(showOverdue)
						sqlWhere+= ' ) OR Commission_Due_Date__c = NULL OR Commission_Due_Date__c < ' + System.now().format('yyyy-MM-dd') + ' )';//Overdue
					else sqlWhere+= + ' ))';
				}else{
					 sqlWhere+= ' AND ((Due_Date__c >= '+ IPFunctions.FormatSqlDateIni(dates.Commission_Due_Date__c); //Commission Due Date

					sqlWhere+= ' AND Due_Date__c <= '+ IPFunctions.FormatSqlDateFin(dates.Commission_Paid_Date__c); //Commission Due Date
					if(showOverdue)
						sqlWhere+= ' ) OR Due_Date__c = NULL OR Due_Date__c < ' + System.now().format('yyyy-MM-dd') + ' )';//Overdue
					else sqlWhere+= + ' ))';
				}

			 // sqlWhere+= ' AND Received_By_Agency__r.Global_Link__c = :financeGlobalLink '; 

				sql += sqlWhere;

				if(selectedDateFilterBy == 'commdue')
					sql += ' order by Client_Course__r.Campus_Name__c, Client_Course__r.Course_Name__c, Commission_Due_Date__c NULLS FIRST' ;
				else
					sql += ' order by Client_Course__r.Campus_Name__c, Client_Course__r.Course_Name__c, Due_Date__c NULLS FIRST' ;

				system.debug('to request sql===>' + sql);

				createPagination(200,Database.query(sql)); 

				totPendingConf = ff.totReqConfInv(selectedCountry, selectedSchoolGP, selectedSchool, searchName, dates.Commission_Due_Date__c, dates.Commission_Paid_Date__c, selectedBackOffice, schoolOptions);

				totSendInvoice = ff.totInstSendInv(agenciesBO, selectedCountry, selectedSchoolGP, selectedSchool, searchName, dates.Commission_Due_Date__c, dates.Commission_Paid_Date__c);
				
				totToConfirm = ff.totConfCommInv(selectedCountry, selectedSchoolGP, selectedSchool, searchName, dates.Commission_Due_Date__c, dates.Commission_Paid_Date__c, selectedBackOffice, schoolOptions);
				
			}else{
				// if(selectedSchoolGP!=null && selectedSchoolGP!= ''){
				// 	totSendInvoice = 0;
				// 	totPendingConf = ff.totReqConfInv(selectedCountry, selectedSchool, searchName, dates.Commission_Due_Date__c, dates.Commission_Paid_Date__c, selectedBackOffice);
				// 	totToConfirm = ff.totConfCommInv(selectedCountry, selectedSchool, searchName, dates.Commission_Due_Date__c, dates.Commission_Paid_Date__c, selectedBackOffice);
				// }
				result = new list<groupResult>();
				totalResult = 0;
				totSendInvoice = 0;
				totPendingConf = 0;
				totToConfirm = 0;
			}
		}

		//No Commission Claim
		public void noCommissionClaim(){
			showError = false;
			Savepoint sp;
			try{
				ff.noCommissionClaim(cancelInstalment, cancelInst.No_Commission_Reason__c, cancelReason, true);

				searchToRequest();
				showError = true;
				cancelReason = '';
				cancelInst.No_Commission_Reason__c = null;
			}
			catch(Exception e){
				system.debug('Error===>' + e.getMessage());
				system.debug('Error Line ===>' + e.getLineNumber());
				Database.rollback(sp);
			}
		}

		public void setIdCancel(){
			cancelInstalment = ApexPages.currentPage().getParameters().get('cancelId');
			ApexPages.currentPage().getParameters().remove('cancelId');
		}


		public void saveContact(Contact userContact){
			update userContact;
		}

		//Create pagination
		private void createPagination(integer pmPageSize, list<client_course_instalment__c> instalments){

				system.debug('instalments===>' + instalments);

				Integer pageSize = pmPageSize;
				Integer counter = 1;
				Integer page = 1;
				list<ID> instId = new list<ID>();
				totPages = new list<Integer>();
				pageIds = new map<Integer, list<ID>>();

				if(instalments!=null && instalments.size()>0)
					totalResult = instalments.size();
				else
					result = new list<groupResult>();


				map<string, map<string,string>> namesMap = new map<string, map<string, string>>();
				namesMap.put('allGroups', new map<String,String>());
				for(client_course_instalment__c cci : instalments){

					instId.add(cci.id);

					if(counter == pageSize){
						pageIds.put(page, instId);
						instId = new list<ID>();
						totPages.add(page);
						page++;
						counter = 1;
					}else
						counter++;

					//Find Group and Agency Filters
					if(!namesMap.containsKey(cci.Received_by_Agency__r.ParentId)){
							namesMap.get('allGroups').put(cci.Received_by_Agency__r.Parent.Name, cci.Received_by_Agency__r.ParentId);
							namesMap.put(cci.Received_by_Agency__r.ParentId, new map<string,string>{cci.Received_by_Agency__r.Name => cci.Received_by_Agency__c});
					}else{
							namesMap.get(cci.Received_by_Agency__r.ParentId).put(cci.Received_by_Agency__r.Name,cci.Received_by_Agency__c);
					}
				}//end for


				

				//add the remaining ids
				if(counter > 1){
					pageIds.put(page, instId);
					totPages.add(page);
				}

				if(instalments.size()>0){

					//Create Group and Agency filters
					list<String> orderFilter = new list<String>(namesMap.get('allGroups').keySet());
					orderFilter.sort();

					agGroupOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
					agencyOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
					String gpId;

					agencieMap = new map<string, list<SelectOption>>();

					for(String nm : orderFilter){
						gpId = namesMap.get('allGroups').get(nm);
						agGroupOptions.add(new SelectOption(gpId, nm));

						list<String> orderAg = new list<String>(namesMap.get(gpId).keySet());
						orderAg.sort();

						agencieMap.put(gpId, new list<SelectOption>{new SelectOption('all', '-- All --')});

						for(String ag : orderAg)
							agencieMap.get(gpId).add(new SelectOption(namesMap.get(gpId).get(ag), ag));

					}//end for

					if(selectedAgencyGroup != 'all' && agencieMap.containsKey(selectedAgencyGroup)){
						changeagGroup();           
					}
					else{
						selectedAgencyGroup = 'all';
					}

					findPaginationItems();
				}
				else{
					selectedAgencyGroup = null;
					selectedAgency = null;
					agGroupOptions = null;
					result.clear();
					totalResult = 0;
				}
		}
		

		//Find Page Items
		public void findPaginationItems(){
			String paramPage = ApexPages.CurrentPage().getParameters().get('page');

			if(paramPage!=null && paramPage!=''){
				currentPage = integer.valueOf(paramPage);
				ApexPages.CurrentPage().getParameters().remove('page');
			}

			if(currentPage> totPages.size())
				currentPage = totPages.size();

			list<Id> allIds = pageIds.get(currentPage);

			String sql = sqlInstFields + ' WHERE ID in  ( \''+ String.join(allIds, '\',\'') + '\' ) order by Client_Course__r.School_Name__c, Client_Course__r.Campus_Name__c, Client_Course__r.Course_Name__c, Received_Date__c';

			buildResult(sql);
		}

		//Build Result List
		public Boolean allowInvAll {get;set;}
		public void buildResult(String sql){
			result = new list<groupResult>();
			instalments = new list<installmentDetails>();
			system.debug('@buildResultSql===>' + sql);
			list<client_course_instalment__c> instalmentsResult = Database.query(sql);
			installmentDetails inst;
			

			for(client_course_instalment__c cci : instalmentsResult){
				allowInvAll = cci.client_course__r.campus_course__r.campus__r.Parent.Parent.Registration_Name__c != null ? true : false;

				inst = new installmentDetails();
				if(inst.listActivities == null) 
					inst.listActivities = new list<instalmentActivity>(); 
				if(inst.listNotes == null) 
					inst.listNotes = new list<Custom_Note_Task__c>(); 

					inst.installment = cci;
				
				/** Notes **/
				for(Custom_Note_Task__c n : cci.Custom_Notes_Tasks__r)
					inst.listNotes.add(n);

				/** Instalment Activities **/
				if(cci.instalment_activities__c!=null && cci.instalment_activities__c != ''){

					List<String> allAc = cci.instalment_activities__c.split('!#!');

					for(Integer i = (allAc.size() - 1); i >= 0; i--){//activity order desc

						instalmentActivity instA = new instalmentActivity();
						List<String> a = allAc[i].split(';;');

						instA.acType = a[0];
						instA.acTo = a[1];
						instA.acSubject = a[2];
						if(instA.acType=='SMS') // saved date in system mode
							instA.acDate = new Contact(Lead_Accepted_On__c = Datetime.valueOfGmt(a[3]));
						else
							instA.acDate = new Contact(Lead_Accepted_On__c = Datetime.valueOf(a[3]));
						instA.acStatus = a[4];
						instA.acError = a[5];
						instA.acFrom = a[6];

						system.debug('single activity==' + instA);

						inst.listActivities.add(instA);
					}
				}
				/** END -- Instalment Activities **/

				instalments.add(inst);
			}//end for


			/** Group Instalment per Campus **/
			campusDetails cp;
			map<id, campusDetails> mapCp = new map<id,campusDetails>();
			// map<string, string> schName = new map<string,string>();
			map<string, schoolDetails> schName = new map<string,schoolDetails>();
			map<string, string> schCur = new map<string,string>();


			for(installmentDetails id : instalments){
				system.debug('campus id ===' + id.installment.client_course__r.Campus_Id__c);
				if(!mapCp.containsKey(id.installment.client_course__r.Campus_Id__c)){
					system.debug('new campus ===' + id.installment.client_course__r.Campus_Name__c);
					cp = new campusDetails(id.installment.client_course__r.Campus_Id__c,id.installment.client_course__r.Campus_Name__c, id.installment.client_course__r.School_Id__c, id);
					mapCp.put(id.installment.client_course__r.Campus_Id__c,cp);

					// schName.put(id.installment.client_course__r.School_Id__c, id.installment.client_course__r.School_Name__c);
					schName.put(id.installment.client_course__r.School_Id__c, new schoolDetails(id.installment.client_course__r.campus_course__r.campus__r.Parent.ParentId, id.installment.client_course__r.campus_course__r.campus__r.Parent.Parent.Name, id.installment.client_course__r.campus_course__r.campus__r.Parent.Parent.Registration_Name__c, id.installment.client_course__r.School_Name__c));
					
					schCur.put(id.installment.client_course__r.School_Id__c, id.installment.client_course__r.CurrencyIsoCode__c);

				}
				else{
					mapCp.get(id.installment.client_course__r.Campus_Id__c).addInstalment(id);
				}
			}//end for

			/** Group per School **/
			string schId;
			groupResult sch;
			set<String> schoolIds = new set<String>();
			map<string, groupResult> mapSchools = new map<string,groupResult>();

			for(String mi : mapCp.keySet()){
				schId = mapCp.get(mi).schoolId;

				if(!schoolIds.contains(schId)){
						sch = new groupResult(schName.get(schId).groupId, schName.get(schId).groupName, schName.get(schId).groupNumber, schId, schName.get(schId).schoolName, schCur.get(schId), mapCp.get(mi));
					mapSchools.put(schId, sch);
					schoolIds.add(schId);
				}
				else{
						mapSchools.get(schId).addCampus(mapCp.get(mi));
				}
			}//end for

			/** Result List **/
			for(String mi : mapSchools.keySet()){
					result.add(mapSchools.get(mi));
			}
		}

		private list<String> toRequest {get;set;}
		private set<String> campusIds {get;set;}
		private set<String> campusNames {get;set;}
		private String schoolId;
		private String schGroup;
		private boolean invAll = false;

		//Request All Instalments
		public void invoiceAll(){

			toRequest = new list<String>();
			campusIds = new set<String>();
			campusNames = new set<String>();

			invAll = true;

			String sql = 'SELECT Id, client_course__r.School_Id__c, client_course__r.Campus_Id__c, client_course__r.Campus_Name__c, client_course__r.campus_course__r.campus__r.Parent.ParentId FROM client_course_instalment__c WHERE ';
			sql += sqlWhere + ' order by Client_Course__r.School_Name__c, Client_Course__r.Campus_Name__c, Client_Course__r.Course_Name__c, Commission_Due_Date__c NULLS FIRST';

			system.debug('invoice all sql===' + sql);

			for(client_course_instalment__c cci : Database.query(sql)){
				schGroup = cci.client_course__r.campus_course__r.campus__r.Parent.ParentId;
				schoolId = cci.client_course__r.School_Id__c;
				campusIds.add(cci.client_course__r.Campus_Id__c);
				campusNames.add(cci.client_course__r.Campus_Name__c);
				toRequest.add(cci.id);
			}//end for

			system.debug('schGroup===' + schGroup);

			String isGroup = ApexPages.CurrentPage().getParameters().get('isGroup');

			if(isGroup == NULL || isGroup == '')
				schGroup = null;

			system.debug('schGroup===' + schGroup);
			system.debug('toRequest===' + toRequest);

 			setToRequest();
		}

		//Set flag to request commission for selected pds
		public boolean sendEmail {get;set;}
		public void setToRequest(){
			try{

				if(!invAll){
					schGroup = ApexPages.CurrentPage().getParameters().get('gpId');
					schoolId = ApexPages.CurrentPage().getParameters().get('schId');

					toRequest = new list<String>();
					campusIds = new set<String>();
					campusNames = new set<String>();

					if(schGroup == null || schGroup == ''){
						for(groupResult gr : result)
							if(gr.schoolId == schoolId){
								for(campusDetails cd : gr.campuses)
									for(installmentDetails id : cd.instalments)
										if(id.installment.isSelected__c){
											campusIds.add(cd.campusId);
											campusNames.add(cd.campusName);
											toRequest.add(id.installment.id);
											id.installment.isSelected__c = false;
										}
								
								break;
							}
					}
					else{
						for(groupResult gr : result)
							if(gr.groupId == schGroup){
								for(campusDetails cd : gr.campuses)
									for(installmentDetails id : cd.instalments)
										if(id.installment.isSelected__c){
											campusIds.add(cd.campusId);
											campusNames.add(cd.campusName);
											toRequest.add(id.installment.id);
											id.installment.isSelected__c = false;
										}
							}
						}
				}

				Integer count= database.countQuery('SELECT count() FROM client_course_instalment__c WHERE Id in :toRequest AND PDS_Confirmation_Invoice__c!=NULL');

				if(count>0){
					ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Some of the selected Instalments were Requested for Confirmation during this session, please refresh your browser.'));
					sendEmail = false;
				}else{
					sendEmail = true;
					Contact userContact = new contact (id = currentUser.ContactId,
					PDS_Campus_Name_to_Request__c = String.join(new list<String>(campusNames), ';'),
					PDS_Campus_Requested__c = String.join(new list<String>(campusIds), ';'),
					PDS_to_Request__c = String.join(toRequest, ';'),
					PDS_School_to_Request__c = schGroup == null ? id.valueOf(schoolId) : id.valueOf(schGroup), PDS_School_Group__c = schGroup);

					saveContact(userContact);
				}
			}
			catch(Exception e){
				system.debug('toRequestError==>' + e.getMessage());
			}
			finally{
				invAll = false;
				ApexPages.CurrentPage().getParameters().remove('schId');
				// ApexPages.CurrentPage().getParameters().remove('isGroup');
				ApexPages.CurrentPage().getParameters().remove('gpId');
			}
		}

		/********************** Inner Classes **********************/

		//School Details
		private class schoolDetails{
			public String groupId {get;set;}
			public String groupName {get;set;}
			public String groupNumber {get;set;}
			public String schoolName {get;set;}

			public schoolDetails(String groupId, String groupName, String groupNumber, String schoolName){
				this.groupId = groupId;
				this.groupName = groupName;
				this.groupNumber = groupNumber;
				this.schoolName = schoolName;
			}
		}

		//Grouped Result by School
		public class groupResult{
			public String groupId {get;set;}
			public String groupName {get;set;}
			public String groupNumber {get;set;}
			public String schoolId {get;set;}
			public String schoolName {get;set;}
			public String schoolCurrency {get;set;}
			public list<campusDetails> campuses {get{if(campuses==null) campuses= new list<campusDetails>(); return campuses;}set;}

			public groupResult (String groupId, String groupName, String groupNumber, String schoolId, String schoolName, String schoolCurrency, campusDetails cp){
				this.groupId = groupId;
				this.groupName = groupName;
				this.groupNumber = groupNumber;
				this.schoolId = schoolId;
				this.schoolName = schoolName;
				this.schoolCurrency = schoolCurrency;
				this.campuses.add(cp); 
			}

			public void addCampus(campusDetails cp){
					this.campuses.add(cp);
			}
		}

		//Campus Details
		public class campusDetails{
			public String campusId {get;set;}
			public String campusName {get;set;}
			public String schoolId {get;set;}
			public list<installmentDetails> instalments {get{if(instalments == null) instalments = new list<installmentDetails>(); return instalments;}set;}

			public campusDetails (String campusId, String campusName, String schoolId, installmentDetails inst){
					this.campusId = campusId;
					this.campusName = campusName;
					this.schoolId = schoolId;
					this.instalments.add(inst);
			}

			public void addInstalment(installmentDetails inst){
					this.instalments.add(inst);
			}

		}

		//Instalment Details
		public class installmentDetails{
			public client_course_instalment__c installment{get; set;}
			// public list<feeInstallmentDetail> feeInstall{get{if(feeInstall == null) feeInstall = new list<feeInstallmentDetail>(); return feeInstall;} set;}
			// public list<feeInstallmentDetail> amendmentFees{get{if(amendmentFees == null) amendmentFees = new list<feeInstallmentDetail>(); return amendmentFees;} set;}
			public transient list<instalmentActivity> listActivities{get; set;}
			public transient list<Custom_Note_Task__c> listNotes{get; set;}
			public String lastActivityType {get;set;}
			public Contact lastActivityDate {get;set;}
			public String lastActivityStatus {get;set;}
			public list<SelectOption> instOptions {get;set;}
		}

		//Fee Details
		// public class feeInstallmentDetail{
		//   public string feeId{get; set;}
		//   public string feeName{get; set;}
		//   public decimal feeValue{get; set;}
		//   public string feeType{get; set;}
		//   public decimal originalValue{get; set;}
		//   public decimal keepFee{get; set;}
		//   public decimal feeAmendment{get; set;}
		//   public decimal keepFeeAmendment{get; set;}
		// 	public decimal originalKeepFee {get;set;}

		//   public feeInstallmentDetail(string feeId, string feeName, decimal feeValue, string feeType, decimal originalValue, decimal keepFee, decimal feeAmendment, decimal keepFeeAmendment){
		//       this.feeId = feeId;
		//       this.feeName = feeName;
		//       this.feeValue = feeAmendment  + feeValue;
		//       this.feeType = feeType;
		//       this.originalValue = originalValue;
		//       this.keepFee = keepFee;
		//       this.feeAmendment = feeAmendment;
		//       this.keepFeeAmendment = keepFeeAmendment;
		//       this.originalValue = feeValue;
		//       this.originalKeepFee = keepFee;
		//   }

		//   public feeInstallmentDetail(string feeId, string feeName, decimal feeValue, string feeType, decimal originalValue, decimal keepFee){
		//       this.feeId = feeId;
		//       this.feeName = feeName;
		//       this.feeValue = feeValue;
		//       this.feeType = feeType;
		//       this.originalValue = originalValue;
		//       this.keepFee = keepFee;
		//   }
		// }

		//Last Activity
		public class instalmentActivity{
			public string acType {get; set;}
			public Contact acDate {get; set;}
			public string acStatus {get; set;}
			public string acError {get; set;}
			public string acTo {get; set;}
			public string acFrom {get; set;}
			public string acSubject {get; set;}

		}

		/********************** Filters **********************/
		public String selectedCountry{get;set;}
		public String selectedSchoolGP{get;set;}
		public String selectedSchool{get;set;}
		public String selectedCity{get;set;}
		public String selectedCampus{get;set;}
		public String selectedAgencyGroup {get;set;}
		public String selectedAgency {get;set;}
		public String selectedPayment {get{if(selectedPayment==null) selectedPayment = 'all'; return selectedPayment;}set;}
		public String searchName {get{if(searchName==null) searchName = ApexPages.CurrentPage().getParameters().get('nm'); return searchName;}set;}
		public client_course_instalment__c dates{get{
			if(dates==null){

				dates = new client_course_instalment__c();

				String pIni = ApexPages.CurrentPage().getParameters().get('ini');
				String pFin = ApexPages.CurrentPage().getParameters().get('fin');
				
				if(pIni != null && pini!= ''){
					// list<String>dt = pIni.split('-');
					dates.Commission_Due_Date__c = date.valueOf(pini + ' 00:00:00');
				}
				else{
					dates.Commission_Due_Date__c = system.today();
				}

				if(pFin != null && pFin!= ''){
					// list<String>dt = pFin.split('-');
					dates.Commission_Paid_Date__c = date.valueOf(pFin + ' 00:00:00');
				}
				else{
					dates.Commission_Paid_Date__c = dates.Commission_Due_Date__c.addDays(14);
				}
			}
			return dates;
		}set;}

		public string selectedDateFilterBy{
			get{
				if(selectedDateFilterBy == null)
					selectedDateFilterBy = 'instaldue';
				return selectedDateFilterBy;
			}
			set;
		}

		 public boolean showOverdue{
			get{
				if(showOverdue == null)
					showOverdue = true;
				return showOverdue;
			}
			set;
		}


		public list<SelectOption> dateFilterBy {get{
			if(dateFilterBy==null){
				dateFilterBy = new list<SelectOption>();
				dateFilterBy.add(new SelectOption('commdue', 'Commission Due Date'));
				dateFilterBy.add(new SelectOption('instaldue', 'Instalment Due Date'));
			}
			return dateFilterBy;
		}set;}

		public list<SelectOption> countryOptions {get;set;}
		public list<SelectOption> schGroupOptions {get;set;}
		public list<SelectOption> schoolOptions {get;set;}
		public list<SelectOption> campusOptions {get;set;}
		public list<SelectOption> agGroupOptions {get;set;}
		public list<SelectOption> agencyOptions {get;set;}
		public list<SelectOption> paymentOptions {get{
			if(paymentOptions==null){
				paymentOptions = new list<SelectOption>();
				paymentOptions.add(new SelectOption('all', '-- All --'));
				paymentOptions.add(new SelectOption('firstPayment', 'First Payment'));
				paymentOptions.add(new SelectOption('repayments', 'Repayments'));
			}
			return paymentOptions;
		}set;}

		@TestVisible
		private map<string,list<SelectOption>> allFilters {get;set;}

		private void checkFilters(){
			
			allFilters = ff.pdsFlowBOFilters(bo);

			countryOptions = allFilters.get('allCountries');
			if(countryOptions != null && countryOptions.size()>0){
				
				String pCountry = ApexPages.CurrentPage().getParameters().get('cn');
				String pGroup = ApexPages.CurrentPage().getParameters().get('pGroup');
				
				//Get Country
				if(pCountry != null && pCountry != '')
					selectedCountry = pCountry;
				else
					selectedCountry = countryOptions[0].getValue();

				//Get School Group
				schGroupOptions = allFilters.get(selectedCountry);
				
				selectedSchoolGP = pGroup;
				if((selectedSchoolGP == null || selectedSchoolGP == '') && schGroupOptions!= null && schGroupOptions.size()>0)
					selectedSchoolGP = schGroupOptions[0].getValue();

				//Get School 
				if(selectedSchoolGP != 'none' && selectedSchoolGP != 'all')
					schoolOptions = allFilters.get(selectedCountry+'-'+selectedSchoolGP);

				String pSchool = ApexPages.CurrentPage().getParameters().get('sch');

				if( pSchool != null && pSchool != ''){
					selectedSchool = pSchool;
				}
				else if(schoolOptions != null && schoolOptions.size()>0){
					selectedSchool = schoolOptions[0].getValue();
				}
			}
			else{
				schGroupOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
				schoolOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
				campusOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
			}
		}

		// public void changeCountry(){
		// 		schoolOptions = allFilters.get(selectedCountry);
		// 		selectedSchool = schoolOptions[0].getValue();
		// 		campusOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
		// }

		public void changeSchool(){
				if(selectedSchool != 'none' && selectedSchool != 'all'){
					campusOptions = allFilters.get(selectedCountry+'-'+selectedSchool);
				}
				else{
					campusOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
				}

				if(campusOptions!= null && campusOptions.size()>0)
					selectedCampus = campusOptions[0].getValue();
		}

		public void changeCountry(){
			schGroupOptions = allFilters.get(selectedCountry);
			if(schGroupOptions != null && schGroupOptions.size()>0)
				selectedSchoolGP = schGroupOptions[0].getValue();
			schoolOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
			campusOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
		}

		//Change School Group
		public void changeSchoolGP(){
			system.debug('selectedSchoolGP===>' + selectedSchoolGP);

			if(selectedSchoolGP != 'none' && selectedSchoolGP != 'all'){
			schoolOptions = allFilters.get(selectedCountry+'-'+selectedSchoolGP);
			}
			else{
			schoolOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
			}
			
			if(schoolOptions!= null && schoolOptions.size()>0)
				selectedSchool = schoolOptions[0].getValue();

			campusOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
			
		}

		


		private map<string, list<SelectOption>> agencieMap;
		public void changeagGroup(){
			if(selectedAgencyGroup!='all')
				agencyOptions = agencieMap.get(selectedAgencyGroup);
			else
				agencyOptions = new list<SelectOption>{new SelectOption('all', '-- All --')};
		}

		/** Current User **/
		private Id financeGlobalLink {get{
			if(financeGlobalLink==null){
				if(currentUser.Contact.Finance_Global_Link__c != null)
					financeGlobalLink = currentUser.Contact.Finance_Global_Link__c;
				else financeGlobalLink = currentUser.Contact.Account.Global_Link__c;
			}
			return financeGlobalLink;
		}set;}

		private User currentUser {get{
			if(currentUser==null) {
				currentUser = ff.currentUser;

			}
			return currentUser;
		}set;}


}