/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class mandrillSendEmail_test {

    static testMethod void myUnitTest() {
        
        mandrillSendEmail m = new mandrillSendEmail();
               
        mandrillSendEmail.messageJson mj = new mandrillSendEmail.messageJson(); 
        mj.from_email= 'batman@test.com.au';
        mj.from_name='Batman';
        mj.subject = 'Work Report';
        mj.html = '<p>Work Report</p>';
        mj.setTo('toemail@test.com.au', 'Robin');
        mj.setCc('justiceLeague@test.com.au', 'Justice League');
        mj.setBcc('justiceLeague@test.com.au', 'Justice League');
        Blob b = Blob.valueOf('Report Content');
       	mj.setAttachment('Att type', 'Report', b);
        mj.setImportant(true);
        
        m.sendMail(mj); 
        
        
        
    }
}