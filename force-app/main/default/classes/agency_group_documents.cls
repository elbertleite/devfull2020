public without sharing class agency_group_documents {

    string accoId;
    public agency_group_documents() {
        accoId = currentUser.contact.Account.ParentId;
    }

    public user currentUser{
        get{
            if(currentUser == null)
                currentUser = [Select contact.Account.id, contact.Account.ParentId from User where id =:UserInfo.getUserId() limit 1];
            return currentUser;
        }
        set;
    }

    public String bucketName {get{if(bucketName == null) bucketName = IPFunctions.s3bucket; return bucketName;}set;}
    public Account_Document_File__c doc{
        get{
            if(doc == null)
                doc = new Account_Document_File__c();
            return doc;
        }
        set;
     }

    private set<string> setDocs;
    public void addDoc(){
        if(!setDocs.contains(doc.Category__c) && doc.Category__c != null && doc.Category__c != ''){
            doc.Parent__c = accoId;
            doc.Is_Folder_New_School_Page__c = false;
            insert doc;
            doc = new Account_Document_File__c();
        }
    }

    public list<Account_Document_File__c> getdocList(){
        list<Account_Document_File__c> docfile = [Select id, Description__c, Category__c from Account_Document_File__c where Parent__c = :accoId];
        setDocs = new set<string>();
        for(Account_Document_File__c df:docfile)
            setDocs.add(df.Category__c);
        return docfile;
    }
}