public without sharing class AgenciesAgreementController {
	
	public School_Agreement__c periodIni {get;set;}
	public School_Agreement__c periodFin {get;set;}
		
	
	/* Get School Id */
	public string schoolId {get {
			if(schoolId == null){
				schoolId = [Select Account.ParentId, AccountId From Contact where ID = :[Select u.ContactId From User u where User.id = :UserInfo.getUserId()].ContactId].Account.ParentId;			
			}
			return schoolId;		
		}set;}
	public string orgId {get {
		if(orgId==null)
			orgId = UserInfo.getOrganizationId();
		return orgid;
	
	}set;}
	
	/* Get Agreement Documents */
	public list<String> documents {get{
		list<string> documents= new list<string>();
		String lDocuments = [Select document_link__c From School_Agreement__c Where Id = :agreement.id].document_link__c;
		if(lDocuments !=null && lDocuments  != '')
			documents= lDocuments.split(';');
		return documents;
	}set;}
	
	/* Get Url prefix */
	public string urlprefix {get{
		if(urlprefix==null)
			urlPrefix = 'https://s3.amazonaws.com/'+orgId+'/' + schoolId + '/Agreements/'+agreement.id+'/'+agreement.long__c+'/';
		return urlPrefix;
	}set;}
		
	
	public list<SelectOption> regionFilter {get;set;}
	public list<SelectOption> countryFilter {get;set;}
	public list<SelectOption> cityFilter {get;set;}
	public string sRegionFilter {get;set;}
	public string sCountryFilter {get;set;}
	public string sCityFilter {get;set;}
	public string sShow {get;set;}
	public string searchField {get;set;}
	public integer totGroups {get;set;}
	public integer totAgencies {get;set;}
	public List<string> countries {get;set;}	
	public transient map<string, agencyGroup> resultMap {get;set;}
	public School_Agreement__c agreement {get;set;}
	public string agencyID {get;set;}
	public xCourseSearchStyle agencyStyle {get;set;}
	
	public AgenciesAgreementController(){
		agencyStyle = new xCourseSearchStyle();
		periodIni = new School_Agreement__c();
		periodIni.Valid_till__c = Date.today();
		periodFin = new School_Agreement__c();
		periodFin.Valid_till__c = Date.today().addMonths(1);
		
	}
	/* SET UP SEARCH agencies page */
	public void setUpSearchPage(){
		sRegionFilter='all';
		sCountryFilter='all';
		sCityFilter='all';
		sShow = 'all';
		setNationalityGroupFilter();
		setCountryFilter();
		setCityFilter();	
		editAgree = false;
	}
	
	/* SET UP CREATE AGREEMENT page */
	public boolean editAgree {get;set;}
	public void setUpCreateAgreement(){	
	//Check if is Edit
		if(Apexpages.currentPage().getParameters().get('agriD')!=null){
			editAgree = true;
			agreement = [Select ID, Agency_Agreement__c, long__c, commission__c, deal__c, document_link__c, favourite__c, School_Manager__c, School_Account__c, Signed_on__c, Valid_till__c FROM School_Agreement__c WHERE ID = :Apexpages.currentPage().getParameters().get('agriD')];
		}else{
			editAgree = false;
			agreement = new School_Agreement__c();
		 	agencyID = Apexpages.currentPage().getParameters().get('ag');
		 	if(agencyID!=null){//School creating agreement
			 	agreement.Agency_Agreement__c = agencyId;
			 	agreement.School_Account__c = schoolId; //Get from user logged in
		 	}else{//Agency creating agreement
		 		agreement.Agency_Agreement__c = schoolId; //Get from user logged in (in this case schoolId is the agencyGroup ID)
			 	agreement.School_Account__c = Apexpages.currentPage().getParameters().get('sc');
		 	}
		}
	}
	
	/* SAVE AGREEMENT and let user attach a file*/
	public string filePath {get;set;}
	public void saveAgreement(){
		try{
			DateTime dt = DateTime.now();
			agreement.long__c = String.valueOf(dt.getTime());			
			insert agreement;
			editAgree = true;
		}catch(Exception e){
			editAgree = false;
			system.debug(e);
		}
	}
	
	/* UPDATE AGREEMENT */
	public void updateAgreement(){
		try{
			update agreement;			
		}catch(Exception e){			
			system.debug(e);
		}
	}
	
	public void getAgencies(){
		try{
			total_size =0;
			if(sShow.equals('expiredin')){
				if(periodIni.Valid_till__c == null || periodFin.Valid_till__c == null)
					ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL,'Please fill in the date range for your search.'));
				else
					searchAgencies();
			}else{
				searchAgencies();
			}
		}catch(Exception e){
			
		}
	}
	
	private static string FormatSqlDateTimeIni(date d){
		string day = string.valueOf(d.day());
		string month = string.valueOf(d.month());
		if (day.length() == 1)  
			day = '0'+day;
		if (month.length() == 1)    
			month = '0'+month;
		return d.year()+'-'+month+'-'+day;
	}
	
	/* Search Agencies*/
	private integer counter=0;  //keeps track of the offset
	private integer list_size=20; //sets the page size or number of rows
 	public integer total_size {get;set;} //used to show user the total size of the list
	private string wsCity {get;set;}
	private string wsCountry {get;set;}
	public void searchAgencies(){ 
		wsCity = null;
		wsCountry = null;
		totGroups=0;
		totAgencies = 0;
		resultMapOrdered = new list<string>();
		system.debug('School Id=='+ schoolId);
		String sql2 = 'SELECT ID, Valid_till__c, Agency_Agreement__c, Agency_Agreement__r.Name FROM School_Agreement__c WHERE School_Account__c = :schoolId  ';
		
		if(sShow=='valid'){ //Show agencies without agreements
			sql2 += ' and Valid_till__c >= TODAY ';
		}
		else if(sShow=='expired'){ //Show agencies without agreements
			sql2 += ' and Valid_till__c < TODAY ';
		}
		else if(sShow=='expiredin'){ //Show agencies without agreements
			sql2 += ' and Valid_till__c >= ' + FormatSqlDateTimeIni(periodIni.Valid_till__c) + ' and Valid_till__c <= ' + FormatSqlDateTimeIni(periodFin.Valid_till__c) ;
		}
		
		sql2 += ' ORDER BY Valid_till__c DESC ';
		
		list<School_Agreement__c> listAgreement = Database.Query(sql2);
		
		
		String sql= ' SELECT Parent.Name, ParentId, Parent.NumberOfEmployees, Parent.Agency_Type__c, Name, Agency_Type__c, BillingCity, BillingCountry, Website FROM Account  ' +
					' WHERE RecordType.Name= \'Agency\' ';
		String sqlWhere = '';
		String sqlWCount = '';
		
		set<string> groupIds = new set<string>();
		if(sShow=='agreement' ||sShow=='valid' || sShow=='expired' ||sShow=='expiredin'){ //Show agency with agreements
			for(School_Agreement__c a : listAgreement)
				groupIds.add(a.Agency_Agreement__c);
			sqlWhere += ' and parentId in :groupIds ';
			sqlWCount += ' and id in :groupIds ';
			system.debug('IDS===>' + groupIds);
		}else if(sShow=='without'){ //Show agency without agreements
			for(School_Agreement__c a : listAgreement)
				groupIds.add(a.Agency_Agreement__c);
			sqlWhere += ' and parentId not in :groupIds ';
			sqlWCount += ' and id not in :groupIds ';
		}
		else{ // Show All
			if(sRegionFilter!='all'){	 
		 		breakCountries();		
		 		sqlWhere+= ' AND BillingCountry IN :countries ';
				wsCountry = String.join(countries, ',');
		 	}else if(sCountryFilter!='all'){
		 		sqlWhere+= ' AND BillingCountry = \''+ sCountryFilter +'\' ';
				wsCountry = sCountryFilter;	
		 		if(sCityFilter!='all'){
		 			sqlWhere+= ' AND BillingCity = \''+ sCityFilter +'\' ';
		 			wsCity = sCityFilter;
		 		}	 		
		 	}
		}
	 	if(searchField!=null && searchField!='')
	 		sqlWhere+= ' AND (Name Like \'%'+ searchField +'%\' OR Parent.Name Like \'%'+ searchField +'%\' )';
	
	 	sql+= sqlWhere;
	 	system.debug('First SQL===>' + sql);
	 	
 	 	//LIMIT AND OffSet
	 	sql += ' order by Name limit '  + list_size + ' OFFSET '+counter;	 	

	 	/* Put values in AgencyGroup class then in the result map  */
	 	resultMap = new map <string, agencyGroup>();
	 	set<id>agencyIds = new set<id>();
	 	for(Account a : Database.query(sql)){
	 		
	 		if(!resultMap.containsKey(a.Parent.Name)){
	 			AgencyGroup g = new agencyGroup();
		 		g.gId = a.ParentId;
		 		g.gName = a.Parent.Name;
		 		g.gType = a.Parent.Agency_Type__c;
		 		g.gNumStaff = a.Parent.NumberOfEmployees;
		 		g.gAgencies = new list<agencies>();
		 		g.oldAgreements = new list<School_Agreement__c>();
		 		resultMapOrdered.add(g.gName);
		 		
		 		Agencies ag = new Agencies();
		 		ag.aCity = a.BillingCity;
		 		ag.aCountry = a.BillingCountry;
		 		ag.aName = a.Name;
		 		ag.aType = a.Agency_Type__c;
		 		ag.aWebsite = a.Website;
		 		g.gAgencies.add(ag);
		 		resultMap.put(a.Parent.Name, g);
		 		//totGroups++;
		 		totAgencies++;
		 		agencyIds.add(a.ParentId);
	 		}else{
 				AgencyGroup g = resultMap.get(a.Parent.Name);
 				Agencies ag = new Agencies();
		 		ag.aCity = a.BillingCity;
		 		ag.aCountry = a.BillingCountry;
		 		ag.aName = a.Name;
		 		ag.aWebsite = a.Website;
		 		g.gAgencies.add(ag);
		 		resultMap.put(a.Parent.Name, g);
		 		totAgencies++;
		 	}	 		
	 	}//end for
	 	
	 	if(!Test.isRunningTest()){
		 	//Get IP Agencies
		 	AgencyGroup g;
		 	Account infoPlanet = [Select id, name from account where name = 'Information Planet Group' and recordType.name = 'Agency Group' limit 1];
		 	if(sShow =='all' || (sShow=='without' && !groupIds.contains(infoPlanet.id)) || (sShow=='agreement' && groupIds.contains(infoPlanet.id))){
		 		if(!resultMap.containsKey(infoPlanet.Name)){
		 			g = new agencyGroup();
				 	g.gName = infoPlanet.name;
				 	g.gId = infoPlanet.id;
				 	g.gAgencies = new List<agencies>();
				 	resultMapOrdered.add(g.gName);
				 	for(WS_restCallouts.agencies  agency : WS_restCallouts.getIPAgencies(wsCountry, wsCity, searchField)){
				 		Agencies ag = new Agencies();
				 		ag.aName = agency.aName;
				   		ag.aType = agency.aType;
				   		ag.aCountry = agency.aCountry;
				   		ag.aCity = agency.aCity; 
				   		ag.aWebsite = agency.aWebsite;
			   			g.gAgencies.add(ag);
				 		totAgencies++;
				 	}
				 	if(g.gAgencies.size()>0){
				 		resultMap.put(infoPlanet.Name, g);
			   		}
		 		}else{
 					 g = resultMap.get(infoPlanet.Name);
 					 for(WS_restCallouts.agencies  agency : WS_restCallouts.getIPAgencies(wsCountry, wsCity, searchField)){
					 	Agencies ag = new Agencies();
					 	ag.aName = agency.aName;
					 	ag.aType = agency.aType;
					 	ag.aCountry = agency.aCountry;
					 	ag.aCity = agency.aCity; 
					 	ag.aWebsite = agency.aWebsite;
				   		g.gAgencies.add(ag);
					 	totAgencies++;
					 }
					 if(g.gAgencies.size()>0){
					 	resultMap.put(infoPlanet.Name, g);
					 }
 				}
		 	}
	 	}
	 	
 		if(sShow=='all' || sShow=='agreement' || sShow=='valid' || sShow=='expired' ||sShow=='expiredin'){
	 		for(School_Agreement__c a : listAgreement){
	 			agencyGroup ag = resultMap.get(a.Agency_Agreement__r.Name);
	 			if(ag !=null && (ag.gLastAgree==null || ag.gLastAgree=='')){
	 				ag.gLastAgree = a.Id;
	 				ag.gLastAgreeValid = a.Valid_till__c;
	 			}
	 			else if (ag !=null){
	 				ag.oldAgreements.add(a);	
	 			}
	 		}//end for
 		}
	 	total_size = resultMap.size();	 	
	 	resultMapOrdered.sort();
	}
	public List<String> resultMapOrdered {get;set;}
	
	/* INNER CLASS */
	public class agencyGroup{
		public string gId {get;set;}
		public string gName {get;set;}
		public string gType {get;set;}
		public integer gNumStaff {get;set;}
		public string gLastAgree {get;set;}
		public Date gLastAgreeValid {get;set;}
		public list<School_Agreement__c> oldAgreements {get;set;}
		public list<agencies> gAgencies {get;set;}
	}	
	public class agencies{
		public string aName {get;set;}
		public string aType {get;set;}
		public string aCountry {get;set;}
		public string aCity {get;set;}
		public string aWebsite {get;set;}
	}
	
	public void breakCountries(){
		countries = new List<string>();
		list<string> bCountry = sRegionFilter.split('\\(');
		bCountry[1] = bCountry[1].replace(')', '');
		for(String s : bCountry[1].split(',')){
			countries.add(s.trim());
		}
	}
	
	/* Set Nationlity Group Filter */
	public void setNationalityGroupFilter(){
		regionFilter = new list<SelectOption>();
		regionFilter.add(new SelectOption('all', 'All'));
		for(String n : IPFunctions.getNationalityGroups(schoolId)){
			regionFilter.add(new SelectOption(n, n));	
		}
	}
	
	/* Set Country Filter */
	public void setCountryFilter(){
		countryFilter = new list<SelectOption>();
		countryFilter.add(new SelectOption('all', 'All'));
		for(AggregateResult a : [SELECT BillingCountry FROM Account WHERE RecordType.Name='Agency' group by BillingCountry]){
			if((string)a.get('BillingCountry')!=null)
			countryFilter.add(new SelectOption((string)a.get('BillingCountry'),(string)a.get('BillingCountry')));
		}
	}
	
	/* Set City Filter */
	public void setCityFilter(){
		cityFilter = new list<SelectOption>();
		cityFilter.add(new SelectOption('all', 'All'));
		if(sCountryFilter!='all'){
			for(AggregateResult a : [SELECT BillingCity FROM Account WHERE RecordType.Name='Agency' and BillingCountry = :ScountryFilter group by BillingCity]){
				cityFilter.add(new SelectOption((string)a.get('BillingCity'),(string)a.get('BillingCity')));
			}
		}
	}
	
	/** DELETE DOC AMAZON S3 **/
    public string secret { get {return credentials.secret;} }
    public AWSKeys credentials {get;set;}
    public string key { get {return credentials.key;} set;}
    private String AWSCredentialName = IPFunctions.awsCredentialName;
    public S3.AmazonS3 as3 { get; private set; }
    public boolean deleteListObjects = false;
    public string fileToDelete {get;set;}
    
	public void deleteAgreeDocs(){
        
        try{
    	  	fileToDelete = Apexpages.currentPage().getParameters().get('FileName');
            credentials = new AWSKeys(AWSCredentialName);
            as3 = new S3.AmazonS3(credentials.key,credentials.secret);
            Datetime now = Datetime.now();        
            S3.Status deleteObjectReslt;
            String objectToDelete; 
            objectToDelete = orgId+'/'+schoolId+'/Agreements/'+agreement.id+'/'+agreement.long__c+'/'+fileToDelete;
            System.debug('about to delete S3 object with key: ' + objectToDelete);
            //This performs the Web Service call to Amazon S3 and create a new bucket.
            deleteObjectReslt= as3.DeleteObject(UserInfo.getOrganizationId(),objectToDelete,as3.key,now,as3.signature('DeleteObject',now), as3.secret);
            System.debug('Successfully deleted a Bucket: ' + deleteObjectReslt.Description);
            
            list<string> newDocuments = documents;
            //remove document
            for(integer i=0; i<newDocuments.size(); i++){
            	
            	if(newDocuments[i].equals(fileToDelete)){
            		newDocuments.remove(i);
					system.debug('Remove Document==>' + fileToDelete);            		
            	}
            }
            
            if(newDocuments.size()<1){
	            agreement.document_link__c = '';
	            update agreement;
	            system.debug('Sem documentos');
            }
            else{
            	system.debug('Com documentos');
	            String updateDocumentList;
	            for(string s : newDocuments){
	            	if(updateDocumentList==null || updateDocumentList=='')
	            		updateDocumentList = s;
            		else
            			updateDocumentList+= ';' + s;
	            }
              	agreement.document_link__c = updateDocumentList;
	            update agreement;
            }
            
            
                          
        } catch(System.CalloutException callout){
            System.debug('CALLOUT EXCEPTION: ' + callout);
            ApexPages.addMessages(callout);                
        }
        catch(Exception ex){
            System.debug(ex);
            ApexPages.addMessages(ex);
        }
    }
    
   //LIMITS AND OFFSETS
   public PageReference Beginning() { //user clicked beginning
   	counter = 0;
	searchAgencies();
    return null;
   }
 
   public PageReference Previous() { //user clicked previous button
   	counter -= list_size;
	searchAgencies();
    return null;
   }
 
   public PageReference Next() { //user clicked next button
    counter += list_size;
	searchAgencies();
	return null;
   }
 
   public PageReference End() { //user clicked end
   	counter = total_size - math.mod(total_size, list_size);
	searchAgencies();
    return null;
   }
 
   public Boolean getDisablePrevious() {
      //this will disable the previous and beginning buttons
      if (counter>0) return false; else return true;
   }
 
   public Boolean getDisableNext() { //this will disable the next and end buttons
      if (counter + list_size < total_size) return false; else return true;
   }
 
   public Integer getTotal_size() {
      return total_size;
   }
 
   public Integer getPageNumber() {
      return counter/list_size + 1;
   }
 
   public Integer getTotalPages() {
	   try{
      if (math.mod(total_size, list_size) > 0) {
         return total_size/list_size + 1;
      } else {
         return (total_size/list_size);
      }
	   }catch(Exception e){
			return 1;   
	   }
   }
}