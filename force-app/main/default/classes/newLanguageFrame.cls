public with sharing class newLanguageFrame {
	
	string disclaimerId;
	
	public newLanguageFrame(ApexPages.StandardController std){

		disclaimerId = std.getRecord().id;
		//init();
	}
	
	public Quotation_Disclaimer_Other_Nationality__c languageDisclaimer{
		get{
		if(languageDisclaimer == null){
			string langId = ApexPages.currentPage().getParameters().get('langId');
			if(langId == null)
				languageDisclaimer = new Quotation_Disclaimer_Other_Nationality__c(); 
			else languageDisclaimer = [Select Language__c, Disclaimer__c, Nationalities_Applicable__c, Quotation_Disclaimer__c from Quotation_Disclaimer_Other_Nationality__c where id = :langId];
			init(langId);
		}
		return languageDisclaimer;
	} 
		set;
	}
	
	public boolean checkData{get{if(checkData == null) checkData = true; return checkData;} set;}
	public pageReference saveNewLanguage(){
		try{
			checkData = true;
			if(languageDisclaimer.Language__c == null || languageDisclaimer.Language__c == ''){
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a Language');
				ApexPages.addMessage(msg);
				checkData = false;
			}
			if(selectedNames == null || selectedNames.isEmpty()){
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select nationality');
				ApexPages.addMessage(msg);
				checkData = false;
			}
			if(languageDisclaimer.Disclaimer__c == null || languageDisclaimer.Disclaimer__c == ''){
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill in the disclaimer');
				ApexPages.addMessage(msg);
				checkData = false;
			}
			if(!checkData)
				return null;
			
			
			if(languageDisclaimer.Quotation_Disclaimer__c == null)
				languageDisclaimer.Quotation_Disclaimer__c = disclaimerId;
			list<string> selCountries = new list<string>(selectedNames);
			languageDisclaimer.Nationalities_Applicable__c = String.join(selCountries, ',');
		
			upsert languageDisclaimer;
		}catch(Exception e){
			
		}
		return null;
	}
	
	public map<string,string> languages {
		get {
			languages = IPFunctions.getDisclaimerLanguages();
			languages.put(null,'');
			languages.put('en_US','English');
			return languages;
		}
		Set;
			
	}
	public string notSelectedLanguages{get; set;}
	public List<SelectOption> notSelectedLanguagesOptions { 
		get {
			map<string,string> languages = IPFunctions.getDisclaimerLanguages();
			List<SelectOption> options = new List<SelectOption>();
			
			for (String s: languages.keySet()) {
				options.add(new SelectOption(s, languages.get(s)));
			}
			
			options = IPFunctions.SortOptionList(options);
			options.add(0, new SelectOption('','--Select a Language--'));
			options.add(1, new SelectOption('en_US','English'));
			
			
			return options;
		}
	}
	
	
	private Set<String> unSelectedNames;
	private Set<String> selectedNames;
	private Set<String> nationalitiesToExclude;
    
	public List<String> selected { get; set; }
	public List<String> unselected { get; set; }
	

	
	private void init(string langId) {
		unSelectedNames = new Set<String>();
		
		
		if (langId == null){
			selectedNames = new Set<String>();
			
		}else{
			selectedNames = new set<string>(languageDisclaimer.Nationalities_Applicable__c.split(','));

		}
		if(nationalitiesToExclude == null)
			nationalitiesToExclude = new Set<String>();
		for(Quotation_Disclaimer_Other_Nationality__c cd:[Select Nationalities_Applicable__c from Quotation_Disclaimer_Other_Nationality__c where Quotation_Disclaimer__c = :disclaimerId])
		nationalitiesToExclude.addAll(cd.Nationalities_Applicable__c.split(','));
		
		for (SelectOption s:IPFunctions.getAllCountries())
			if(!selectedNames.contains(s.getValue()) && !nationalitiesToExclude.contains(s.getValue()))
				unSelectedNames.add(s.getValue());

	}
	
	public List<SelectOption> selectedOptions { 
		get {
		List<String> sorted = new List<String>(selectedNames);
		sorted.sort();
		List<SelectOption> options = new List<SelectOption>();
		for (String s: sorted) {
			options.add(new SelectOption(s, s));
		}
		return options;
	}
	}
    
	public List<SelectOption> unSelectedOptions { 
		get {
		List<String> sorted = new List<String>(unSelectedNames);
		sorted.sort();
		List<SelectOption> options = new List<SelectOption>();
		for (String s: sorted) {
			options.add(new SelectOption(s, s));
		}
		return options;
	}
	}


	public void doSelect() {
		for (String s: selected) {
			selectedNames.add(s);
			unselectedNames.remove(s);
		}
	}

	public void doUnSelect() {
		for (String s: unselected) {
			unSelectedNames.add(s);
			selectedNames.remove(s);
		}
	}

	
	
	
}