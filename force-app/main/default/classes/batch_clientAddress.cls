global class batch_clientAddress{
    global batch_clientAddress(){}
}
/*global class batch_clientAddress implements Database.Batchable<sObject> {   
	

	
	
	
	global batch_clientAddress(String batchType) {    
		

		
	}  
	
		
	global Database.QueryLocator start(Database.BatchableContext BC){
		
		String query = 'Select Contact__c, Address_Type__c, City__c, Comments__c, Country__c, Current_Address__c,  Location__Latitude__s, Location__c, Location__Longitude__s, Post_Code__c, Id, State__c, Street__c, Suburb__c  from Address__c WHERE Contact__r.recordType.name in ( \'client\', \'lead\') order by Contact__r.name, Current_Address__c desc';
				
		return Database.getQueryLocator(query);
	}   
	
	global void execute(Database.BatchableContext BC, List<Address__c> scope){    
		
		map<string,contact> checkAddress = new map<string,contact>();
		for(Address__c ad:scope){
			if(!checkAddress.containsKey(ad.Contact__c)){
				checkAddress.put(ad.Contact__c,
				new contact(id=ad.Contact__c,
				MailingCity=ad.City__c, 
				MailingCountry=ad.Country__c, 	
				MailingLatitude=ad.Location__Latitude__s, 
				MailingLongitude=ad.Location__Longitude__s, 
				MailingState=ad.State__c, 
				MailingStreet=ad.Street__c, 
				MailingPostalCode=ad.Post_Code__c));	
			}else{
				checkAddress.get(ad.Contact__c).OtherCity=ad.City__c; 
				checkAddress.get(ad.Contact__c).OtherCountry=ad.Country__c;
				checkAddress.get(ad.Contact__c).OtherLatitude=ad.Location__Latitude__s; 
				checkAddress.get(ad.Contact__c).OtherLongitude=ad.Location__Longitude__s; 
				checkAddress.get(ad.Contact__c).OtherState=ad.State__c; 
				checkAddress.get(ad.Contact__c).OtherStreet=ad.Street__c; 
				checkAddress.get(ad.Contact__c).OtherPostalCode=ad.Post_Code__c;	
			}
		}
		update checkAddress.values();

					
		
	}   
	
	global void finish(Database.BatchableContext BC){       
		System.debug('Batch Process Complete');   
	} 
	
	
	
/*open up the Debug Log and run this
	batch_clientAddress batch = new batch_clientAddress('Account');
	Id batchId = Database.executeBatch(batch);



	batch_clientAddress batch = new batch_clientAddress('Account_Document_File__c');
	Id batchId = Database.executeBatch(batch, 80); //Define batch size
*/

//}