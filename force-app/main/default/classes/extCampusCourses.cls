public class extCampusCourses{

	public Account acco {
		get{
		if(acco==null)
			acco = new Account();
		return acco;}
		set{acco = value;}
	}
	
	public List<Campus_Course__c> CampusCourses {
		get{
		if(CampusCourses==null)
			CampusCourses = new List<Campus_Course__c>();
		return CampusCourses;}
		set{CampusCourses = value;}
	}	
	
	public String unavailableReason{
		get{
			if(unavailableReason==null)
				unavailableReason = '';
			return unavailableReason;
		}
		set;
	}
	
	
	public Campus_Course__c editCampusCourse {get;set;}
	
	public extCampusCourses(ApexPages.StandardController controller) {
		//this.acco = [Select A.Id, A.Name, ( Select Name, Course__c from Campus_Courses__r) from Account A where id= :controller.getRecord().id];
		this.acco = [Select ID, ParentID from Account where id = :controller.getRecord().id];
		retrieveCampusCourses();
	}

	public PageReference edit(){
		string ccid = ApexPages.currentPage().getParameters().get('ccid');
		editCampusCourse = [Select C.Available_From__c, C.Course__r.Sub_Type__c, C.Course__r.Course_Type__c,C.Course__r.Course_Country__c, C.Course__r.Package__c, C.Course__r.Language__c,Unavailable_By__c, Unavailable_Date__c, 
		C.Course__r.Name, C.Course__r.Course_Unit_Type__c, C.Is_Available__c, C.Id, C.Course__r.Only_Sold_in_Blocks__c, Unavailable_Reason__c from Campus_Course__c C WHERE C.Id = :ccid];
		return null;
	}
	
	
	public PageReference cancel(){
		editCampusCourse = new Campus_Course__c();
		return null;
	}
	
	public PageReference save(){
		
		if(editCampusCourse.Is_Available__c){
			editCampusCourse.Unavailable_Reason__c = null;
			editCampusCourse.Unavailable_By__c = null;
			editCampusCourse.Unavailable_Date__c = null;
		} else if(unavailableReason != null && unavailableReason != '') {
			Account userAccount = [Select Name from Account where User__c = :UserInfo.getuserid() limit 1];
			editCampusCourse.Unavailable_Reason__c = unavailableReason;
			editCampusCourse.Unavailable_By__c = userAccount.Name;
			editCampusCourse.Unavailable_Date__c = System.today();
		}
		UPDATE editCampusCourse.course__r;
		UPDATE editCampusCourse;
		retrieveCampusCourses();
		unavailableReason = '';
		return cancel();
	}	
	
	public PageReference search(){
		retrieveCampusCourses();	
		return cancel();
	}
	
	public string courseName {get{if (courseName == null) courseName = ''; return courseName;} set;}
	private void retrieveCampusCourses(){
		string cpID = ApexPages.currentPage().getParameters().get('cpID');
		//ID   = SCHOOL ID
		//cpID = CAMPUS ID		
		string sql = 'Select C.Course__r.Only_Sold_in_Blocks__c, C.Available_From__c, C.Course__r.Name, C.Course__r.Course_Unit_Type__c, C.Course__r.Package__c, C.Is_Available__c, C.Id, Unavailable_Reason__c, '+
			' Unavailable_By__c, Unavailable_Date__c, campus__r.Name, C.Course__r.Sub_Type__c, C.Course__r.Course_Type__c, C.Course__r.Course_Qualification__c, C.Course__r.Language__c, C.Course__r.Type__c, '+
			'( Select Campus_Course__c, Name, CoursePriceID__c, CoursePriceIdFormula__c, From__c, Nationality__c,  '+
			'Price_per_week__c, Price_valid_from__c, Price_valid_until__c  from Course_Prices__r)  '+
			'from Campus_Course__c C ';
		if (cpID != null && cpID != '')
			sql += ' WHERE C.Campus__r.id = \'' + cpID + '\'';
		else sql += ' WHERE C.Campus__r.ParentID = \'' +acco.id + '\'';
		if (courseName != null && courseName != '')
			sql += ' and Course__r.Name like \'%'+ courseName + '%\'';
			
		if (filters.Course_Country__c != null)
			sql += ' and Course__r.Course_Country__c = \''+ filters.Course_Country__c + '\'';
		if (filters.Course_Type__c != null)
			sql += ' and Course__r.Course_Type__c = \''+ filters.Course_Type__c + '\'';
		if (filters.Type__c != null)
			sql += ' and Course__r.Type__c = \''+ filters.Type__c + '\'';
		if (filters.Sub_Type__c != null)
			sql += ' and Course__r.Sub_Type__c = \''+ filters.Sub_Type__c + '\'';
		if (filters.Course_Qualification__c != null)
			sql += ' and Course__r.Course_Qualification__c = \''+ filters.Course_Qualification__c + '\'';
		this.CampusCourses = Database.query(sql);
		
	}
	

	//Methods used in the course price
	private String Country;       
	public Course_Price__c newCoursePrice { 
		get{
		if(newCoursePrice==null)
			newCoursePrice = new Course_Price__c();
		return newCoursePrice;
	} 
		set;
	}  
	
/*	public PageReference addCoursePrice() {
		//	try {
		if (newCoursePrice.Nationality__c == null)
			return null;
		newCoursePrice.Campus_Course__c = 'a0vR0000000Ymr4IAC';
		if (newCoursePrice.Price_valid_until__c == null)
			newCoursePrice.Price_valid_until__c = date.newInstance(System.today().year(), 12, 31);
		if (newCoursePrice.Price_per_week__c <= 0){
			newCoursePrice.Price_per_week__c.addError('must be positive.');
			return null;
		}
		INSERT newCoursePrice;
		country = newCoursePrice.Nationality__c;
		//	ViewData();
		
		newCoursePrice = new Course_Price__c();
		newCoursePrice.Nationality__c = country;
		//	} catch (Exception e) {
		//			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'You cannot add duplicated values in the field From to one group of nationalities.');
		//		ApexPages.addMessage(msg);

		//	} 
		return null;
	}
	*/
	public List<SelectOption> getNationalityGroup() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('Published Price','Published Price'));
		for (String lst:IPFunctions.getNationalityGroups()){
			options.add(new SelectOption(lst,lst));
		}
		//nationality = 'Brazil';
		return options;
	}
	
	public Course__c filters {get{if (filters == null) filters = new Course__c(); return filters;} set;}
	
}