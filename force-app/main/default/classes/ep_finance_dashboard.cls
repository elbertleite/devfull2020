public with sharing class ep_finance_dashboard {
    
    public User currentUser{get; set;}
    
    string userContact;
    public ep_finance_dashboard(){
        currentUser = [Select Id, Name, Aditional_Agency_Managment__c, Contact.Account.ParentId, Contact.AccountId, Contact.Department__c, Contact.Account.Name from User where id = :UserInfo.getUserId() limit 1];
        
        selectedAgency = currentUser.Contact.AccountId;
        updateChart();
    }
    
    
    public void updateGroup(){
        selectedAgency='';
        returnAgencyOptions();
        //updateChart();
    }
    
    public string selectedAgencyName{get{if(selectedAgencyName == null) selectedAgencyName = ''; return selectedAgencyName;} set;}
    public void updateChart(){
        returnAgencyOptions();
        if(agencyName.containsKey(selectedAgency))
            selectedAgencyName = agencyName.get(selectedAgency);
        System.debug('==>selectedAgency: '+selectedAgency);
        if(selectedUser != 'all')
            userContact = [Select contactId from User where id = :selectedUser limit 1].contactId;
        
        retrieve_LastPaymentsReceived();
        retrieve_NextPaymentsToReceive();
        retrieve_TaskNext7Days();
        retrieve_TotalTasks();
        retrieve_TotalOverdueTasks();
        retrieve_TaskSubject();
        retrieve_TaskPriority();
        retrieve_TotLeads();
        retrieve_TotLeadsByType();
        retrieve_TotSales();
        retrieve_userSalesValue();
        retrieve_MostClientsEnrolledByUser();
    }
    
    private string returnPeriod(integer year, integer month){
        list<string> months = new list<string>{'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'};
        System.debug('==>month: '+month);
        return year +'/'+ months[month-1];
    }
    
    
    public string selectedAgencyGroup{
        get{
            if(selectedAgencyGroup == null)
                selectedAgencyGroup = currentUser.Contact.Account.ParentId;
            return selectedAgencyGroup;
        }  
        set;
    }
    
   public List<SelectOption> agencyGroupOptions {
        get{
            if(agencyGroupOptions == null){
                
                agencyGroupOptions = new List<SelectOption>();  
                    System.debug('==>Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible(): '+Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible());
                if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group  
                    for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:currentUser.Contact.AccountId order by Parent.Name]){
                        if(ag.Parent.Name != null && ag.ParentId != null)
                            agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
                        System.debug('==>ag: '+ag);
                    }
                }else{
                    for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
                        agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
                        System.debug('==>ag2: '+ag);
                    }
                }
            }
            System.debug('==>agencyGroupOptions: '+agencyGroupOptions);
            return agencyGroupOptions;
      }
      set;
    }
    
    public string selectedUser{
        get{
            if(selectedUser == null)
                selectedUser = UserInfo.getUserId();
            return selectedUser;
        }  
        set;
    }
     public List<SelectOption> userOptions {
        get{
            userOptions = new List<SelectOption>();
            if(selectedAgency != null ){
                /*if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
                    userOptions.add(new SelectOption(UserInfo.getUserId() , UserInfo.getName()));
                }else{*/
                userOptions.add(new SelectOption('all', '-- All --'));
                if(selectedDepartment == 'all') {
                    for(user u:[Select id, Name from User where Contact.AccountId =:selectedAgency and isActive =true]){
                        userOptions.add(new SelectOption(u.id, u.name));
                    }
                }else{
                    for(user u:[Select id, Name from User where Contact.AccountId =:selectedAgency and Contact.Department__c = :selectedDepartment and isActive =true]){
                        userOptions.add(new SelectOption(u.id, u.name));
                    }
                }
            }
            return userOptions;
        }
        set;
     }

    public map<string, string> agencyName{get; set;}
    public String selectedAgency {get;set;}
    public List<SelectOption> agencyOptions{get; set;}
    private List<SelectOption> returnAgencyOptions() {
        if(selectedAgencyGroup != null){
            
            agencyName = new map<string, string>();
            agencyOptions = new List<SelectOption>();
            if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
                for(Account a: [Select Id, Name from Account where id =:currentUser.Contact.AccountId order by name]){
                    if(selectedAgency=='')
                        selectedAgency = a.Id;                  
                    agencyOptions.add(new SelectOption(a.Id, a.Name));
                    if(currentUser.Aditional_Agency_Managment__c != null){
                        for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
                            agencyOptions.add(new SelectOption(ac.id, ac.name));
                            agencyName.put(ac.id, ac.name);
                        }
                    }
                    agencyName.put(a.Id, a.Name);
                }
            }else{
                for(Account a: [Select Id, Name from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
                    agencyOptions.add(new SelectOption(a.Id, a.Name));
                    agencyName.put(a.Id, a.Name);
                    if(selectedAgency=='')
                        selectedAgency = a.Id;  
                }
            }
            //updateChart();
        }
        return agencyOptions;
    }
    
    public string selectedDepartment {
        get{
            if(selectedDepartment == null){
                //if(currentUser.Contact.Department__c != null && currentUser.Contact.Department__c != '')
                    selectedDepartment = currentUser.Contact.Department__c;
                //else selectedDepartment = 'all'; 
            }
            return selectedDepartment;
        } 
        set;
    }
    
    
    public list<SelectOption> getDepartments(){
        List<SelectOption> result = new List<SelectOption>();
        //allDepartments = new set<Id>();

        if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){
            for(Department__c d: [Select Id, Name, Services__c, Show_Visits__c from Department__c where Agency__c = :selectedAgency and Inactive__c = false]){
                if(currentUser.Contact.Department__c == d.id)
                    result.add(new SelectOption(d.Id, d.Name));
            }
            if(result.size() == 0)
                result.add(new SelectOption('all', '-- All --'));
        }else{
            result.add(new SelectOption('all', '-- All --'));
            for(Department__c d: [Select Id, Name, Services__c, Show_Visits__c from Department__c where Agency__c = :selectedAgency and Inactive__c = false]){
                result.add(new SelectOption(d.Id, d.Name));
            }
        }

        return result;
    }
    
    public class gaugeValues{
        public String name { get; set; }
        public integer salesValue{get;set;}
        public gaugeValues(string name, integer salesValue){
            this.salesValue = salesValue;
            this.name = name;
        }
    }
    
    public decimal userSalesValue{get;set;}
    private decimal retrieve_userSalesValue(){
        userSalesValue = 0;
        if(selectedUser != 'all'){
            AggregateResult cc = [Select sum(Total_Course__c) tot from client_course__c where Enroled_by_User__c = :selectedUser];
            //list<gaugeValues> lg = new list<gaugeValues>();
            //lg.add(new gaugeValues(selectedUser,integer.valueOf((decimal)cc.get('tot'))));
            userSalesValue = (decimal)cc.get('tot');
        }
        return userSalesValue;
    }
    
    
    public string SelectedPeriod{get {if(SelectedPeriod == null) SelectedPeriod = 'TODAY'; return SelectedPeriod; } set;}
    private List<SelectOption> periods;
    public List<SelectOption> getPeriods() {
        if(periods == null){
            periods = new List<SelectOption>();
            periods.add(new SelectOption('TODAY','Today'));
            periods.add(new SelectOption('TOMORROW','Tomorrow'));
            periods.add(new SelectOption('THIS_WEEK','This Week'));
            periods.add(new SelectOption('LAST_WEEK','Last Week'));
            periods.add(new SelectOption('NEXT_WEEK','Next Week'));
            periods.add(new SelectOption('THIS_MONTH','This Month'));
            periods.add(new SelectOption('LAST_MONTH','Last Month'));
            periods.add(new SelectOption('NEXT_MONTH','Next Month'));
            
        }
        return periods;
    }
    
    public class enrollByUser{
        public string name{get;set;}
        public decimal nweeks{get; set;}
        public string userPhoto{get; set;}
        public enrollByUser(string name, decimal nweeks, string userPhoto){
            this.name = name;
            this.nweeks = nweeks;
            this.userPhoto = userPhoto;
        }
    }
    
    public class topSalesPerson{
        public string name{get; set;}
        public decimal salesTotal{get; set;}
        public string color{get; set;}
        public string personPic{get; set;}
        public topSalesPerson(string name, decimal salesTotal, string color, string personPic){
            this.name = name;
            this.salesTotal = salesTotal;
            this.color = color;
            this.personPic = personPic;
        }
    }
    
    public string MostClientsEnrolledByUser{get;set;}
    private string retrieve_MostClientsEnrolledByUser(){
        list<topSalesPerson> tpPerson = new list<topSalesPerson>();
        map<string, enrollByUser> enrolUser = new map<string, enrollByUser>();
        list<string> colors = new list<string>{'#7F8DA9','#FEC514','#DB4C3C','#DAF0FD','#7F8DA9'};
        for(AggregateResult ar:[Select client_course__r.client__r.Owner__c uid, client_course__r.client__r.Owner__r.name uname, sum(commission_value__c) tot from client_course_instalment__c where Received_By_Agency__c = :selectedAgency and Commission_Confirmed_On__c != null
             and DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) = THIS_MONTH
            group by client_course__r.client__r.Owner__c, client_course__r.client__r.Owner__r.name order by sum(Tuition_Value__c) desc limit 3]){
            enrolUser.put((string)ar.get('uid'), new enrollByUser((string)ar.get('uname'), (decimal)ar.get('tot'), ''));        
        }
        integer pos = 0;
        for(User u:[Select id, SmallPhotoURL from user where id in:enrolUser.keySet()]){
            //enrolUser.get(u.id).userPhoto = u.SmallPhotoURL;
            tpPerson.add(new topSalesPerson(enrolUser.get(u.id).name, enrolUser.get(u.id).nweeks, colors.get(pos), u.SmallPhotoURL));
            pos++;
        }
        MostClientsEnrolledByUser = JSON.serialize(tpPerson);
        return MostClientsEnrolledByUser;
    }
    
    public class sales{
        public date dt{get; set;}
        public decimal tot{get; set;}
        public sales(date dt, decimal tot){
            this.dt = dt;
            this.tot = tot;
        }
    }
    
    public string LastPaymentsReceived{get; set;}
    private string retrieve_LastPaymentsReceived(){
        list<sales> sl = new list<sales>();
        if(selectedUser == 'all' && selectedDepartment == 'all'){
            for(AggregateResult ar:[Select DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) date, sum(Tuition_Value__c) tot from client_course_instalment__c where client_course__r.client__r.Owner__r.Contact.AccountId = :selectedAgency and DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) = LAST_N_DAYS:60
                group by DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) order by DAY_ONLY(convertTimezone(Commission_Confirmed_On__c))])
                if((date)ar.get('date') != null)
                    sl.add(new sales((date)ar.get('date'),(decimal)ar.get('tot')));
        }else if(selectedUser != 'all'){
            for(AggregateResult ar:[Select DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) date, sum(Tuition_Value__c) tot from client_course_instalment__c where client_course__r.client__r.Owner__c = :selectedUser and DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) = LAST_N_DAYS:60
                group by DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) order by DAY_ONLY(convertTimezone(Commission_Confirmed_On__c))])
                if((date)ar.get('date') != null)
                    sl.add(new sales((date)ar.get('date'),(decimal)ar.get('tot')));
        }else if(selectedDepartment != null && selectedDepartment != ''){
            for(AggregateResult ar:[Select DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) date, sum(Tuition_Value__c) tot from client_course_instalment__c where client_course__r.client__r.Owner__r.contact.Department__c = :selectedDepartment and DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) = LAST_N_DAYS:60
                group by DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) order by DAY_ONLY(convertTimezone(Commission_Confirmed_On__c))])
                if((date)ar.get('date') != null)
                    sl.add(new sales((date)ar.get('date'),(decimal)ar.get('tot')));
        }
        LastPaymentsReceived = JSON.serialize(sl);
        return LastPaymentsReceived;
    }
    
    public string NextPaymentsToReceive{get;set;}
    private string retrieve_NextPaymentsToReceive(){
        list<sales> sl = new list<sales>();
        string sql = 'Select Due_Date__c date, sum(Tuition_Value__c) tot from client_course_instalment__c where ';
        if(selectedUser == 'all' && selectedDepartment == 'all'){
            sql += ' client_course__r.client__r.Owner__r.Contact.AccountId = \''+selectedAgency+'\' ';
        }else if(selectedUser != 'all')
            sql += ' client_course__r.client__r.Owner__c = \''+selectedUser+'\' ';
        else if(selectedDepartment != null && selectedDepartment != ''){
            sql += ' client_course__r.client__r.Owner__r.contact.Department__c = \''+selectedDepartment+'\' ';
        } 
        sql += ' and Due_Date__c = NEXT_N_DAYS:60 ';
        sql += ' group by Due_Date__c order by Due_Date__c ';
        for(AggregateResult ar:database.query(sql))
            if((date)ar.get('date') != null)
            sl.add(new sales((date)ar.get('date'),(decimal)ar.get('tot')));
        NextPaymentsToReceive = JSON.serialize(sl);
        return NextPaymentsToReceive;
    }
    
    
    public list<AggregateResult> TaskNext7Days{get;set;}
    private list<AggregateResult> retrieve_TaskNext7Days(){
        string sql = 'Select Due_Date__c dt, count(id) tot  from Custom_Note_Task__c where isNote__c = false and ';
        if(selectedUser == 'all' && selectedDepartment == 'all'){
            sql += ' Assign_To__r.Contact.AccountId = \''+selectedAgency+'\' ';
        }else if(selectedUser != 'all')
            sql += ' Assign_To__c = \''+selectedUser+'\' ';
        else if(selectedDepartment != null && selectedDepartment != ''){
            sql += ' Assign_To__r.Contact.Department__c = \''+selectedDepartment+'\' ';
        } 
        sql += ' and  Due_Date__c = NEXT_N_DAYS:7 group by Due_Date__c order by Due_Date__c';
        TaskNext7Days = database.Query(sql);
        return TaskNext7Days;
    }
    
    public integer TotalTasks{get;set;}
    private integer retrieve_TotalTasks(){  
        string sql = 'Select count() from Custom_Note_Task__c where isNote__c = false and ';
        if(selectedUser == 'all' && selectedDepartment == 'all'){
            sql += ' Assign_To__r.Contact.AccountId = \''+selectedAgency+'\' ';
        }else if(selectedUser != 'all')
            sql += ' Assign_To__c = \''+selectedUser+'\' ';
        else if(selectedDepartment != null && selectedDepartment != ''){
            sql += ' Assign_To__r.Contact.Department__c = \''+selectedDepartment+'\' ';
        }
        sql += ' and ( ';
        if(SelectedPeriod == 'THIS_WEEK')
            sql += ' Due_Date__c = THIS_WEEK ';
        else if(SelectedPeriod == 'LAST_WEEK')
            sql += ' Due_Date__c = LAST_WEEK ';
        else if(SelectedPeriod == 'THIS_MONTH')
            sql += ' Due_Date__c = THIS_MONTH ';
        else if(SelectedPeriod == 'LAST_MONTH')
            sql += ' Due_Date__c = LAST_MONTH ';
            
        else if(SelectedPeriod == 'TODAY')
            sql += ' Due_Date__c = TODAY ';
        else if(SelectedPeriod == 'TOMORROW')
            sql += ' Due_Date__c = TOMORROW ';
        else if(SelectedPeriod == 'NEXT_WEEK')
            sql += ' Due_Date__c = NEXT_WEEK ';
        else if(SelectedPeriod == 'NEXT_MONTH')
            sql += ' Due_Date__c = NEXT_MONTH ';
            
        sql += '  ) and Status__c != \'Completed\'';
        System.debug('==>sql: '+sql);
        TotalTasks = Database.countQuery(sql);
        return TotalTasks;
    }
    
    public integer TotalOverdueTasks{get;set;}
    private integer retrieve_TotalOverdueTasks(){   
        string sql = 'Select count() from Custom_Note_Task__c where isNote__c = false and ';
        if(selectedUser == 'all' && selectedDepartment == 'all'){
            sql += ' Assign_To__r.Contact.AccountId = \''+selectedAgency+'\' ';
        }else if(selectedUser != 'all')
            sql += ' Assign_To__c = \''+selectedUser+'\' ';
        else if(selectedDepartment != null && selectedDepartment != ''){
            sql += ' Assign_To__r.Contact.Department__c = \''+selectedDepartment+'\' ';
        }
        sql += ' and Due_Date__c < today ';
            
        sql += ' and Status__c != \'Completed\'';
        System.debug('==>sql: '+sql);
        TotalOverdueTasks = Database.countQuery(sql);
        return TotalOverdueTasks;
    }
    
    public list<AggregateResult> TaskSubject{get;set;}
    private list<AggregateResult> retrieve_TaskSubject(){   
        string sql = 'Select Subject__c sub, count(id) tot from Custom_Note_Task__c where isNote__c = false and ';
        if(selectedUser == 'all' && selectedDepartment == 'all'){
            sql += ' Assign_To__r.Contact.AccountId = \''+selectedAgency+'\' ';
        }else if(selectedUser != 'all')
            sql += ' Assign_To__c = \''+selectedUser+'\' ';
        else if(selectedDepartment != null && selectedDepartment != ''){
            sql += ' Assign_To__r.Contact.Department__c = \''+selectedDepartment+'\' ';
        }
        
        sql += ' and ( ';
        if(SelectedPeriod == 'THIS_WEEK')
            sql += ' Due_Date__c = THIS_WEEK ';
        else if(SelectedPeriod == 'LAST_WEEK')
            sql += ' Due_Date__c = LAST_WEEK ';
        else if(SelectedPeriod == 'THIS_MONTH')
            sql += ' Due_Date__c = THIS_MONTH ';
        else if(SelectedPeriod == 'LAST_MONTH')
            sql += ' Due_Date__c = LAST_MONTH ';
        
        else if(SelectedPeriod == 'TODAY')
            sql += ' Due_Date__c = TODAY ';
        else if(SelectedPeriod == 'TOMORROW')
            sql += ' Due_Date__c = TOMORROW ';
        else if(SelectedPeriod == 'NEXT_WEEK')
            sql += ' Due_Date__c = NEXT_WEEK ';
        else if(SelectedPeriod == 'NEXT_MONTH')
            sql += ' Due_Date__c = NEXT_MONTH ';
            
        sql += '  ) and Status__c != \'Completed\' group by Subject__c';
        System.debug('==>sql: '+sql);
        TaskSubject = Database.query(sql);
        return TaskSubject;
    }
    
    public list<AggregateResult> TaskPriority{get;set;}
    private list<AggregateResult> retrieve_TaskPriority(){
        string sql = 'Select Priority__c pri, count(id) tot from Custom_Note_Task__c where isNote__c = false and ';
        
        if(selectedUser == 'all' && selectedDepartment == 'all'){
            sql += ' Assign_To__r.Contact.AccountId = \''+selectedAgency+'\' and ( ';
        }else if(selectedUser != 'all')
            sql += ' Assign_To__c = \''+selectedUser+'\' and ( ';
        else if(selectedDepartment != null && selectedDepartment != ''){
            sql += ' Assign_To__r.Contact.Department__c = \''+selectedDepartment+'\' and ( ';
        }
        if(SelectedPeriod == 'THIS_WEEK')
            sql += ' Due_Date__c = THIS_WEEK ';
        else if(SelectedPeriod == 'LAST_WEEK')
            sql += ' Due_Date__c = LAST_WEEK ';
        else if(SelectedPeriod == 'THIS_MONTH')
            sql += ' Due_Date__c = THIS_MONTH ';
        else if(SelectedPeriod == 'LAST_MONTH')
            sql += ' Due_Date__c = LAST_MONTH ';
        else if(SelectedPeriod == 'TODAY')
            sql += ' Due_Date__c = TODAY ';
        else if(SelectedPeriod == 'TOMORROW')
            sql += ' Due_Date__c = TOMORROW ';
        else if(SelectedPeriod == 'NEXT_WEEK')
            sql += ' Due_Date__c = NEXT_WEEK ';
        else if(SelectedPeriod == 'NEXT_MONTH')
            sql += ' Due_Date__c = NEXT_MONTH ';
        sql += '  ) and Status__c != \'Completed\' group by Priority__c order by Priority__c desc';
        System.debug('==>sql: '+sql);
        TaskPriority = Database.query(sql);
        return TaskPriority;
        
    }
    
    
    public integer TotLeads{get;set;}
    private integer retrieve_TotLeads(){
        //return [Select count() from contact where recordType.name = 'lead' and Lead_Assignment_To__c = :userContact and Lead_Assignment_On__c = this_month];
        string sql = 'Select count() from contact where recordType.name = \'lead\' and ';
        if(selectedUser == 'all' && selectedDepartment == 'all'){
            sql += ' Lead_Assignment_To__r.AccountId = \''+selectedAgency+'\' and ';
        }else if(selectedUser != 'all')
            sql += ' Lead_Assignment_To__c = \''+userContact+'\' and ';
        else if(selectedDepartment != null && selectedDepartment != ''){
            sql += ' Lead_Assignment_To__r.Department__c = \''+selectedDepartment+'\' and ';
        }
        if(SelectedPeriod == 'THIS_WEEK')
            sql += ' Lead_Assignment_On__c = THIS_WEEK ';
        else if(SelectedPeriod == 'LAST_WEEK')
            sql += ' Lead_Assignment_On__c = LAST_WEEK ';
        else if(SelectedPeriod == 'THIS_MONTH')
            sql += ' Lead_Assignment_On__c = THIS_MONTH ';
        else if(SelectedPeriod == 'LAST_MONTH')
            sql += ' Lead_Assignment_On__c = LAST_MONTH ';
        else if(SelectedPeriod == 'TODAY')
            sql += ' Lead_Assignment_On__c = TODAY ';
        else if(SelectedPeriod == 'TOMORROW')
            sql += ' Lead_Assignment_On__c = TOMORROW ';
        else if(SelectedPeriod == 'NEXT_WEEK')
            sql += ' Lead_Assignment_On__c = NEXT_WEEK ';
        else if(SelectedPeriod == 'NEXT_MONTH')
            sql += ' Lead_Assignment_On__c = NEXT_MONTH ';
        System.debug('==>sql: '+sql);
        TotLeads = Database.countQuery(sql);
        return TotLeads;
    }
    
    
    public list<AggregateResult> TotLeadsByType{get;set;}
    private list<AggregateResult> retrieve_TotLeadsByType(){
        string sql = 'Select Lead_Level_of_Interest__c tp, count(id) tot from contact where recordType.name = \'lead\' and ';
        if(selectedUser == 'all' && selectedDepartment == 'all'){
            sql += ' Lead_Assignment_To__r.AccountId = \''+selectedAgency+'\' and ';
        }else if(selectedUser != 'all')
            sql += ' Lead_Assignment_To__c = \''+userContact+'\' and ';
        else if(selectedDepartment != null && selectedDepartment != ''){
            sql += ' Lead_Assignment_To__r.Department__c = \''+selectedDepartment+'\' and ';
        }
        if(SelectedPeriod == 'THIS_WEEK')
            sql += ' Lead_Assignment_On__c = THIS_WEEK ';
        else if(SelectedPeriod == 'LAST_WEEK')
            sql += ' Lead_Assignment_On__c = LAST_WEEK ';
        else if(SelectedPeriod == 'THIS_MONTH')
            sql += ' Lead_Assignment_On__c = THIS_MONTH ';
        else if(SelectedPeriod == 'LAST_MONTH')
            sql += ' Lead_Assignment_On__c = LAST_MONTH ';
        else if(SelectedPeriod == 'TODAY')
            sql += ' Lead_Assignment_On__c = TODAY ';
        else if(SelectedPeriod == 'TOMORROW')
            sql += ' Lead_Assignment_On__c = TOMORROW ';
        else if(SelectedPeriod == 'NEXT_WEEK')   
            sql += ' Lead_Assignment_On__c = NEXT_WEEK ';
        else if(SelectedPeriod == 'NEXT_MONTH')
            sql += ' Lead_Assignment_On__c = NEXT_MONTH ';
        sql += '  group by Lead_Level_of_Interest__c order by Lead_Level_of_Interest__c desc';
        System.debug('==>sql: '+sql);
        TotLeadsByType = Database.query(sql);
        return TotLeadsByType;
    }
    
    public AggregateResult TotSales{get;set;}
    private AggregateResult retrieve_TotSales(){
        string sql = 'Select sum(Tuition_Value__c) tot, sum(commission_value__c) com from client_course_instalment__c where ';
        sql += ' Received_By_Agency__c = \''+selectedAgency+'\' ';
        if(selectedUser != 'all')
            sql += ' and client_course__r.client__r.Owner__c = \''+selectedUser+'\' ';
        else if(selectedDepartment != null && selectedDepartment != 'all'){
            sql += ' and Received_by_Department__c = \''+selectedDepartment+'\' ';
        }
        sql += ' and Commission_Confirmed_On__c != null and ';
        /*if(SelectedPeriod == 'THIS_WEEK')
            sql += ' DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) = THIS_WEEK ';
        else if(SelectedPeriod == 'LAST_WEEK')
            sql += ' DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) = LAST_WEEK ';
        else if(SelectedPeriod == 'THIS_MONTH')
            sql += ' DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) = THIS_MONTH ';
        else if(SelectedPeriod == 'LAST_MONTH')
            sql += ' DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) = LAST_MONTH ';
        else if(SelectedPeriod == 'TODAY')
            sql += ' DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) = TODAY ';
        else if(SelectedPeriod == 'NEXT_WEEK')
            sql += ' DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) = NEXT_WEEK ';
        else if(SelectedPeriod == 'NEXT_MONTH')
            sql += ' DAY_ONLY(convertTimezone(Commission_Confirmed_On__c)) = NEXT_MONTH ';*/

        sql += ' (((isPds__c = false and isPcs__c = false and isPfs__c = false) and ( ';
            if(SelectedPeriod == 'THIS_WEEK')
                sql += ' Commission_Confirmed_On__c = THIS_WEEK ';
            else if(SelectedPeriod == 'LAST_WEEK')
                sql += ' Commission_Confirmed_On__c = LAST_WEEK ';
            else if(SelectedPeriod == 'NEXT_WEEK')
                sql += ' Commission_Confirmed_On__c = NEXT_WEEK ';
            else if(SelectedPeriod == 'THIS_MONTH')
                sql += ' Commission_Confirmed_On__c = THIS_MONTH ';
            else if(SelectedPeriod == 'LAST_MONTH')
                sql += ' Commission_Confirmed_On__c = LAST_MONTH ';
            else if(SelectedPeriod == 'NEXT_MONTH')
                sql += ' Commission_Confirmed_On__c = NEXT_MONTH';
            else if(SelectedPeriod == 'TODAY')
                sql += ' Commission_Confirmed_On__c = TODAY ';
            else if(SelectedPeriod == 'TOMORROW')
                sql += ' Commission_Confirmed_On__c = TOMORROW ';
            sql += ' )) or ';   
            sql += ' ((isPds__c = true or isPcs__c = true or isPfs__c = true) and ( ';
            if(SelectedPeriod == 'THIS_WEEK')
                sql += ' Commission_Paid_Date__c = THIS_WEEK ';
            else if(SelectedPeriod == 'LAST_WEEK')
                sql += ' Commission_Paid_Date__c = LAST_WEEK ';
            else if(SelectedPeriod == 'NEXT_WEEK')
                sql += ' Commission_Paid_Date__c = NEXT_WEEK ';
            else if(SelectedPeriod == 'THIS_MONTH')
                sql += ' Commission_Paid_Date__c = THIS_MONTH ';
            else if(SelectedPeriod == 'LAST_MONTH')
                sql += ' Commission_Paid_Date__c = LAST_MONTH ';
            else if(SelectedPeriod == 'NEXT_MONTH')
                sql += ' Commission_Paid_Date__c = NEXT_MONTH ';
            else if(SelectedPeriod == 'TODAY')
                sql += ' Commission_Paid_Date__c = TODAY ';
            else if(SelectedPeriod == 'TOMORROW')
                sql += ' Commission_Paid_Date__c = TOMORROW ';
            sql += ' ))) '; 

        System.debug('==>sql: '+sql);
        TotSales = Database.query(sql);
        return TotSales;
    }
}