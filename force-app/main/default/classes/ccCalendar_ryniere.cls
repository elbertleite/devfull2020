/****************************************************************
*	ccCalendar: controller for ccCalendar compnent				*
*	Renders a single or multiple YUI callendars according to	*
*	the properties set.											*
* 	Author: Celso de Souza	*	Created: 01/06/2010				*
****************************************************************/

public class ccCalendar_ryniere{
	
	
	
	/* A list of dates in comma spareted values in the format YYYY/MM/DD to render in the Calendar */
	public string  inStringDateListCSV {get{if(inStringDateListCSV==null)inStringDateListCSV='';return inStringDateListCSV;}set;}

	/* Enable/disable selection of past dates */
	public boolean SelectPastDates {get{if(SelectPastDates==null)SelectPastDates=false;return SelectPastDates;}set;}
	
	/* Calendar First date*/
	public date FromDate{
		get{
		if(FromDate==null)
		{
			FromDate = date.newInstance(System.today().year(), 1, 1);
		}
		return FromDate;}
		set;
	}
	
	
	/* Calendar Last date*/
	public date ToDate{
		get{if(ToDate==null)
			ToDate = date.newInstance(System.today().year(), 12, 31);
		return ToDate;}
		set;
	}	

	public void addYear()
	{	
		FromDate = date.newInstance(FromDate.year()+1, 1, 1);
	}
	
	public void removeYear()
	{	
		FromDate = date.newInstance(FromDate.year()-1, 1, 1);
	}

	public Course_Price__c auxDate{
		get {
		if(auxDate==null)
			auxDate = new Course_Price__c();
		return auxDate;}
		set;}
	
	public String inWeekday {get;set;}
	public String WeekFrequency {get;set;}
	public String inDateFrom {get;set;}
	public String inDateTo {get;set;}
	
	
	
	/* return the number of months between the start date and end date */
	public integer getNumberOfMonths()
	{return FromDate.monthsBetween(ToDate) + 1;}
	
	/* return the FromDate month index number */
	public integer getFromMonthIndex()
	{return FromDate.month()-1;}
	
	/* return the FromDate year*/
	public integer getFromYear()
	{return FromDate.year();}	
	
	
	/*	This function returns to the YUI Calendar the dates that are out of bound on the start of the calendars,
		E.g. FromDate is the 5th day of the month, so days from 1 to 4 are out of bound.
			 if SelectPastDates is true it will bring all dates from the 1st day of the month in
			 FromDate till yesterday as out of bound
		Below is how YUI expects the string. Teh date format used is YYYY/MM/DD
		YAHOO.CC.calendar.cal1.addRenderer("2009/12/01-2009/12/11", YAHOO.CC.calendar.cal1.renderOutOfBoundsDate); 	 */
	public string getFromDateOutOfBounds()
	{
		date nFromDate = FromDate;
		
		if(nFromDate < System.today() && !SelectPastDates)
			nFromDate = System.today();
		
		date f =  FromDate.toStartOfMonth();
	
		if(nFromDate==f)
			return '';
	
		String s =  f.year() + '/' + f.month() + '/' + f.day() + '-' + nFromDate.year() +'/'+ nFromDate.month() + '/' + (nFromDate.day()-1);
		return 'YAHOO.CC.calendar.cal1.addRenderer("' + s + '", YAHOO.CC.calendar.cal1.renderOutOfBoundsDate);';
	}

	/*	This function returns to the YUI Calendar the dates that are out of bound on the end of the calendars,
			E.g. ToDate is the 5th day of the month, so days from 6 to the last day of that month are out of bound.
		Below is how YUI expects the string. Teh date format used is YYYY/MM/DD
		YAHOO.CC.calendar.cal1.addRenderer("2009/12/01-2009/12/11", YAHOO.CC.calendar.cal1.renderOutOfBoundsDate); 	 */
	public string getToDateOutOfBounds()
	{
		date f =  ToDate.toStartOfMonth().addMonths(1).addDays(-1);
	
		if(ToDate==f)
			return '';
	
		String s =  ToDate.year() +'/'+ ToDate.month() + '/' + (ToDate.day()+1) + '-' + f.year() + '/' + f.month() + '/' + f.day();
		return 'YAHOO.CC.calendar.cal1.addRenderer("' + s + '", YAHOO.CC.calendar.cal1.renderOutOfBoundsDate);';
	}

	/* selectedDatesStrList is the property used by the component to render the selected dates as selected
		it uses the inStringDateListCSV property to generate the list
	*/	
	private boolean resetList = true;
	public List<String> selectedDatesStrList {
		get{			
		if(resetList){
			try {
				selectedDatesStrList=new List<String>(); 
				selectedDatesStrList.clear();
				if(inStringDateListCSV.length()>0){			
					for (date d:convertedToList())			
						selectedDatesStrList.add( d.year() + '/' + d.month() + '/' + d.day());
				}			
			} catch(Exception e){System.debug('=====> erro 1: ' + e);}
		}
		resetList = true;
		return selectedDatesStrList;
	}
		set;
	}
	

	/* Converts inStringDateListCSV into a List of Dates*/
	public List<Date> convertedToList()
	{
		List<Date> s = new List<Date>(); 
		
		if(inStringDateListCSV.length() < 1)
			return s;
				
		List<String> strDates = inStringDateListCSV.split(',');
		
		for(String str : strDates)
		{
			str = str.replace('/','-');
			Date d = date.valueOf(str + ' 00:00:00');
			s.add(d);
		}	
		
		return s;
	}
	
	
	public List<SelectOption> getWeekdayNamesSelectOption() {
		List<SelectOption> options = new List<SelectOption>();
		Date today = system.today().toStartOfWeek();
		DateTime dt = DateTime.newInstanceGmt(today.year(),today.month(),today.day());  
		list<String> ret = new list<String>();
		for(Integer i = 0; i < 7;i++) { 
			options.add(new SelectOption(String.valueOf(i),dt.formatgmt('EEE')));
			dt= dt.addDays(1);
		} 
		return options;
	}
	
	public List<SelectOption> getWeekFrequencySelectOption() {
		List<SelectOption> options = new List<SelectOption>();
		for(Integer i = 1; i < 5;i++) { 
			options.add(new SelectOption(String.valueOf(i),String.valueOf(i)));
		} 
		return options;
	}	
	
	
	public void getDaysAndRecur()
	{
		Integer d = integer.valueOf(inWeekday);
		Integer r = integer.valueOf(WeekFrequency);
		System.debug('CS ===> inWeekday: ' + inWeekday);
		System.debug('CS ===> WeekFrequency: ' + WeekFrequency);
		if(auxDate.Price_valid_from__c == null || auxDate.Price_valid_until__c == null)
			return;
	
		try{
			date dFrom = auxDate.Price_valid_from__c;//date.parse(inDateFrom);
			date dTo = auxDate.Price_valid_until__c; //date.parse(inDateTo);
			//System.debug('CS ===> dFrom: ' + dFrom);
			//System.debug('CS ===> dTo: ' + dTo);
			//System.debug('CS ===> before: ' + selectedDatesStrList.size());			
			selectedDatesStrList =  getXdays(d,r,dFrom, dTo);
			//System.debug('CS ===> after: ' + selectedDatesStrList.size());
			resetList = false;
		}
		catch (Exception e)
		{
			System.debug('CS ===> Error: ' + e);
			return;
		}
	}
	
	/*	@x = week day 0 for sunday, 1 for monday.... 6 for saturday
		@w = 1 for week, 2 fortnightly, 4 first of every month	
		@dFrom = start date
	*/
	public List<String> getXdays(Integer x, Integer w, Date dfrom, Date dto )
	{

		Map<string,string> mp = new Map<string,string>();
		for(string s:HolidayList)
			mp.put(s,s);
		
		List<String> dl = new List<String>();
		date FirstDay = getFindFirstXdayFrom(x,dfrom);
		System.debug('CS ===> FirstDay: ' + FirstDay);
		
		date endDate =  dto;

		date d = FirstDay;
		System.debug('CS ===> endDate: ' + endDate);
		
		
		while(d < endDate)
		{
			if(!mp.containsKey(d.year() + '/' + d.month() + '/' + d.day()))
				dl.add(d.year() + '/' + d.month() + '/' + d.day());
			d = d.addDays(7*w);
			System.debug('CS ===> d: ' + d);
		}
		return dl;
	}

	public date getFindFirstXdayFrom(Integer x, Date dfrom)	
	{
		date firstDate = dfrom; 
		date firstSunday = firstDate.toStartOfWeek();
		
		
		date firstXDayOftheCalendar = firstSunday.addDays(x);

		if(firstXDayOftheCalendar < firstDate)
			firstXDayOftheCalendar = firstXDayOftheCalendar.addDays(7);
		
		date firstXDay = firstXDayOftheCalendar;
		
		if(firstXDay.month() != dfrom.month())
			firstXDay = firstXDay.addDays(7);
		
		return firstXDay;
	}
	
	public string AccountID {get; set;}


	public List<String> HolidayList{
		get{
		if (HolidayList == null){
			Account ad = new Account();
			String country = '';
			String state = '';
			if(AccountID != null){
				
				try { 
					ad = [Select BillingCountry, BillingState from Account A where id = :AccountID limit 1];
					country = ad.BillingCountry;
					state = ad.BillingState;
				} catch (Exception e){
					/*City_Profile__c cp = [select Country__c, State__c from City_Profile__c where id = :AccountID limit 1];
					country = cp.Country__c;
					state = cp.State__c;*/
				}
			
				
				HolidayList = hl(country, state);
			}
		}
		return HolidayList;
	}
		set;
	}
	
	private List<String> hl(string country, string state){
		list<String> dl = new list<String>();
		for(Public_Holiday__c d:[select date__c from Public_Holiday__c where country__c = :country and state__c = :state])
		dl.add(d.date__c.year() + '/' + d.date__c.month() + '/' + d.date__c.day());
		return dl;
	}
	
}