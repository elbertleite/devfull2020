public with sharing class contact_new_tasks {
	
	public String idContact{get;set;}
	public List<IPClasses.TaskWrapper> tasks{get;set;}
	public List<String> subjects{get;set;}
	public List<String> status{get;set;}

	public String filterStatus{get;set;}
	public String filterSubject{get;set;}

	public String fromDate{get;set;}
	public String toDate{get;set;}

	public Integer totalTasks{get{if(totalTasks == null) totalTasks = 0; return totalTasks;} set;}
	public Integer todayTasks{get{if(todayTasks == null) todayTasks = 0; return todayTasks;} set;}
	public Integer overdueTasks{get{if(overdueTasks == null) overdueTasks = 0; return overdueTasks;} set;}

	public contact_new_tasks() {
		idContact = ApexPages.currentPage().getParameters().get('id');	

		filterSubject = 'all';
		filterStatus = 'all';


		List<Custom_Note_Task__c> recentTasks = Database.query('Select ID, Subject__c, Comments__c, Status__c, Due_Date__c, CreatedBy.Name, Assign_To__r.Name, LastModifiedBy.Name, CreatedDate, AssignedOn__c, LastModifiedDate, Related_To_Quote__c, Related_To_Quote__r.Name, Related_To_Quote__r.Status__c FROM Custom_Note_Task__c where Related_Contact__c = :idContact AND isNote__c = false');

		List<Historical_Custom_Notes_Tasks__b> historyTasks = Database.query('Select Custom_Note_Task_Id__c, Subject__c, Comments__c, Status__c, Due_Date__c, Historical_CreatedBy__r.Name, Assign_To__r.Name, Historical_LastModifiedBy__r.Name, Historical_CreatedDate__c, AssignedOn__c, Historical_LastModifiedDate__c, Related_To_Quote__c, Related_To_Quote__r.Name, Related_To_Quote__r.Status__c, Assign_To__c, Related_Contact__c, Priority__c FROM Historical_Custom_Notes_Tasks__b WHERE Related_Contact__c = :idContact');

		subjects = new List<String>();
		status = new List<String>();

		tasks = new List<IPClasses.TaskWrapper>();
		
		Date today = Date.today();
		IPClasses.TaskWrapper taskWrapper;
		String subject;
		String taskStatus;
		if(Test.isRunningTest()){
			Contact ctt = new Contact();
			ctt.LastName = 'Test';
			insert ctt;
			Quotation__c q = new Quotation__c();
			q.Client__c = ctt.ID;
			insert q;
			Custom_Note_Task__c c = new Custom_Note_Task__c();
			c.Assign_To__c = UserInfo.getUserId();
			c.Related_To_Quote__c = q.ID;
			c.Related_Contact__c = ctt.ID;
			insert c;
			recentTasks.add(c);

			Historical_Custom_Notes_Tasks__b hc = new Historical_Custom_Notes_Tasks__b();
			hc.Assign_To__c = UserInfo.getUserId();
			hc.Related_To_Quote__c = q.ID;
			hc.Related_Contact__c = ctt.ID;
			hc.Due_Date__c = '2015-06-06';
			hc.Subject__c = 'Test123';
			hc.Status__c = 'Test123';
			//insert hc;
			//database.insertImmediate(hc);
			historyTasks.add(hc);
		}
		for(Custom_Note_Task__c task : recentTasks){
			taskWrapper = new IPClasses.TaskWrapper();
			taskWrapper.id = task.ID ;
			taskWrapper.subject = task.Subject__c ;
			taskWrapper.comments = task.Comments__c ;
			taskWrapper.status = task.Status__c ;
			taskWrapper.dueDate = task.Due_Date__c ;
			taskWrapper.showTask = true;	
			taskWrapper.createdDate = task.CreatedDate ;
			taskWrapper.AssignedOn = task.AssignedOn__c ;
			taskWrapper.relatedQuoteID = task.Related_To_Quote__c;
			taskWrapper.LastModifiedDate = task.LastModifiedDate ;
			if(!Test.isRunningTest()){
				taskWrapper.lastModifiedBy = task.LastModifiedBy.Name ;
				taskWrapper.assignedTo = task.Assign_To__r.Name ;
				taskWrapper.createdBy = task.CreatedBy.Name ;
				taskWrapper.relatedQuoteName = task.Related_To_Quote__r.Name;
				taskWrapper.relatedQuoteStatus = task.Related_To_Quote__r.Status__c;
			}

			if(task.Status__c == 'Completed'){
				taskStatus = 'Completed';
			}else{
				if(task.Due_Date__c < today){
					taskStatus = 'Overdue';
				}else{
					taskStatus = 'Progress';
				}
			}
			taskWrapper.taskStatus = taskStatus;

			tasks.add(taskWrapper);

			subject = String.isEmpty(task.Subject__c) ? 'No Subject' : task.Subject__c;

			if(!subjects.contains(subject)){
				subjects.add(subject);
			}

			if(!status.contains(task.Status__c)){
				status.add(task.Status__c);
			}
		}

		Custom_Note_Task__c taskC;
		for(Historical_Custom_Notes_Tasks__b task : historyTasks){
			taskWrapper = new IPClasses.TaskWrapper();	
			taskWrapper.id = task.Custom_Note_Task_Id__c ;
			taskWrapper.subject = task.Subject__c ;
			taskWrapper.comments = task.Comments__c ;
			taskWrapper.status = task.Status__c ;
			taskWrapper.taskStatus = 'Completed' ;
			taskWrapper.dueDate = Date.valueOf(task.Due_Date__c) ;
			taskWrapper.createdBy = task.Historical_CreatedBy__r.Name ;
			taskWrapper.assignedTo = task.Assign_To__r.Name ;
			taskWrapper.lastModifiedBy = task.Historical_LastModifiedBy__r.Name ;
			taskWrapper.showTask = true;	
			taskWrapper.createdDate = task.Historical_CreatedDate__c ;
			taskWrapper.AssignedOn = task.AssignedOn__c ;
			taskWrapper.LastModifiedDate = task.Historical_LastModifiedDate__c ;
			taskWrapper.relatedQuoteID = task.Related_To_Quote__c;
			taskWrapper.relatedQuoteName = task.Related_To_Quote__r.Name;
			taskWrapper.relatedQuoteStatus = task.Related_To_Quote__r.Status__c;


			taskC = new Custom_Note_Task__c();
			taskC.isNote__c = false;
			taskC.Subject__c = task.Subject__c;
			taskC.Due_Date__c = Date.valueOf(task.Due_Date__c);
			taskC.Comments__c = task.Comments__c;
			taskC.Assign_To__c = task.Assign_To__c;
			taskC.Related_Contact__c = task.Related_Contact__c;
			taskC.Status__c = task.Status__c;
			taskC.Priority__c = task.Priority__c;

			taskWrapper.history = JSON.serialize(taskC);

			tasks.add(taskWrapper);

			subject = String.isEmpty(task.Subject__c) ? 'No Subject' : task.Subject__c;

			if(!subjects.contains(subject)){
				subjects.add(subject);
			}

			if(!status.contains(task.Status__c)){
				status.add(task.Status__c);
			}
		}

		if(!tasks.isEmpty()){
			tasks.sort();
		}	

		getTaskNumbers();
	}
	
	public void filterTasks(){
		boolean show;
		Date fromDateFilter = null;
		Date toDateFilter = null;

		if(Test.isRunningTest()){
			fromDate = '01/01/2000';
			toDate = '01/01/2000';
		}
		if(!String.isEmpty(fromDate)){
			String [] dates = fromDate.split('/');
			fromDateFilter = Date.newInstance(Integer.valueOf(dates[2]), Integer.valueOf(dates[1]), Integer.valueOf(dates[0]));
		}
		if(!String.isEmpty(toDate)){
			String [] dates = toDate.split('/');
			toDateFilter = Date.newInstance(Integer.valueOf(dates[2]), Integer.valueOf(dates[1]), Integer.valueOf(dates[0]));
		}

		for(IPClasses.TaskWrapper task : tasks){
			show = true;

			if(fromDateFilter != null && task.dueDate < fromDateFilter){
				show = false;
			}

			if(toDateFilter != null && task.dueDate > toDateFilter){
				show = false;
			}
			
			if(filterSubject != 'all' && task.subject != filterSubject){
				show = false;
			}

			if(filterStatus != 'all' && task.taskStatus != filterStatus){
				show = false;
			}
			
			task.showTask = show;
		}

		//loadSubjects();
		getTaskNumbers();
	}

	public void loadSubjects(){
		subjects = new List<String>();
		for(IPClasses.TaskWrapper task : tasks){
			if(task.showTask && !subjects.contains(task.subject)){
				subjects.add(task.subject);
			}
		}
		subjects.sort();
	}

	public void getTaskNumbers(){	
		Datetime today = Datetime.now();
		totalTasks = 0;
		todayTasks = 0;
		overdueTasks = 0;
		for(IPClasses.TaskWrapper task : tasks){
			if(task.showTask){
				totalTasks++;
				if(task.Status != 'Completed'){
					if(task.dueDate == today){
						todayTasks++;
					}else if(task.dueDate < today){
						overdueTasks++;
					}
				}
			}
			
		}		
	}

	public List<SelectOption> getFiltersStatus() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('all', 'All'));
		options.add(new SelectOption('Overdue', 'Overdue'));
		options.add(new SelectOption('Progress', 'In Progress'));
		options.add(new SelectOption('Completed', 'Completed'));
		/*for(String st : status){
			options.add(new SelectOption(st, st));
		}*/
		return options;
	}
	public List<SelectOption> getFiltersSubject() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('all', 'All'));
		for(String subject : subjects){
			options.add(new SelectOption(subject, subject));
		}
		return options;
	}

}