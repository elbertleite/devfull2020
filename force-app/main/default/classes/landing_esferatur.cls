public with sharing class landing_esferatur {
	 // Code we will invoke on page load.
    public PageReference forwardTologin_esferatur() {
        if(UserInfo.getUserType() == 'Guest'){
            return new PageReference('/login_esferatur');
        }
        else{
            return new Pagereference('/_ui/core/chatter/ui/ChatterPage');
        }
    }

    public landing_esferatur() {}
    
    

}