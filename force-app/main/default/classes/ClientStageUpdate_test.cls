/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ClientStageUpdate_test {
	
	 static testMethod void myUnitTest() {
 		TestFactory tf = new TestFactory();
		Account agency = tf.createAgency();
		Contact employee = tf.createEmployee(agency);
		User portalUser = tf.createPortalUser(employee);		
		tf.createChecklists(agency.parentID);
		
		Test.startTest();
		system.runAs(portalUser){
			Contact lead = tf.createLead(agency, employee);
			lead.Destination_Country__c = 'Australia';
			update lead;
			
			ClientStageUpdate csu = new ClientStageUpdate();
			csu.stage = 'Stage 1';
			csu.contactid = lead.id;
			csu.isLead = true;			
			Contact acco = csu.acco;
			boolean isStudyArea = csu.isStudyArea;
			List<String> selectedLeadProduct = csu.selectedLeadProduct;
			List<SelectOption> leadProductOptions = csu.leadProductOptions;
			List<String> selectedLeadStudyArea = csu.selectedLeadStudyArea;
			csu.getleadStudyAreaOptions();
			string stage = csu.stage;
			csu.getStageOptions();
			csu.refreshCycle();
			Destination_Tracking__c dTracking = csu.dTracking;
			List<ClientStageUpdate.ClientStatus> statusList = csu.statusList;
			csu.refreshStatusOptions();
			csu.saveClientUpdate();
			csu.saveClientStatus();
			csu.getCountries();
			csu.cancelClientStatus();	
			
			
			for(ClientStageUpdate.ClientStatus cs : csu.statusList)
				cs.selected = true;
			
			csu.saveClientStatus();
		}
		Test.stopTest(); 
	 }

}