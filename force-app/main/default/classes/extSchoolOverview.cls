public with sharing class extSchoolOverview{
    public Account acco {
        get{
        if(acco==null)
            acco = new Account();
        return acco;}
        set{acco = value;}
    }

    public List<Campus_Course__c> CampusCourses {
        get{
        if(CampusCourses==null)
            CampusCourses = new List<Campus_Course__c>();
        return CampusCourses;}
        set{CampusCourses = value;}
    }
    
    public boolean isModal {get; set;}
    
    public extSchoolOverview(ApexPages.StandardController controller) {
        //this.acco = [Select A.Id, A.Name, ( Select Name, Course__c from Campus_Courses__r) from Account A where id= :controller.getRecord().id];
        this.acco = [Select ID, ParentID from Account where id = :controller.getRecord().id];
        retrieveCampusCourses();
        
        String modal = ApexPages.currentPage().getParameters().get('modal');
        system.debug('@@@@@@@@@@@@@@@@@@@ modal: ' + modal);
        if(modal != null && modal.equals('true'))           
            isModal = true;
                
            
    }
    
    public string courseName {get{if (courseName == null) courseName = ''; return courseName;} set;}
    
    private void retrieveCampusCourses(){
        string cpID = ApexPages.currentPage().getParameters().get('cpID');
        //ID   = SCHOOL ID
        //cpID = CAMPUS ID      
        string sql = 'Select C.Available_From__c, C.Course__r.Name, C.Course__r.Course_Unit_Type__c, C.Is_Available__c, C.Id, '+
            'campus__r.Name, C.Course__r.Sub_Type__c, C.Course__r.Course_Type__c, C.Course__r.Language__c, C.Course__r.Type__c, '+
            '( Select Campus_Course__c, Name, CoursePriceID__c, CoursePriceIdFormula__c, From__c, Nationality__c,  '+
            'Price_per_week__c, Price_valid_from__c, Price_valid_until__c  from Course_Prices__r)  '+
            'from Campus_Course__c C ';
        sql += ' WHERE C.Campus__r.ParentID = \'' +acco.id + '\'';

        this.CampusCourses = Database.query(sql);
        
    }
        
}