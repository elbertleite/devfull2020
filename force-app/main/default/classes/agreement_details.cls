public without sharing class agreement_details {

	public String agree {get;set;}

	public String duration {get;set;}
	public String nationality {get;set;}
	public String enrolment {get;set;}
	public String coursePayments {get;set;}
	public String refund {get;set;}
	public String extraInfo {get;set;}
	public String aNotes {get;set;}
	public User userDetails {get;set;}

	public List<String> schoolsIDS{get;set;}
	public String schoolsIdsStr{get;set;}
	public agreement_details() {
		schoolsIDS = new List<String>();
		schoolsIdsStr = '';
		userDetails = [SELECT Contact.Account.Global_Link__c, Contact.Create_Agreement__c, Contact.View_Full_Agreement__c FROM User WHERE Id =:userInfo.getUserId()];

		String agreeId = ApexPages.currentPage().getParameters().get('id');

		String sql = 'SELECT Id, Renew_in_progress__c, Renew_upon_request__c, Expired_Notes__c, Signed_by_Agency__r.Name, Signed_by_User__c, Start_Date__c, Agreement_Type__c, End_Date__c, Status__c, Provider_Type__c, Until_Terminated__c, isAvailable__c, isCancelled__c, Expire_in_days__c, Preview_Link__c, Agreement_Name__c, Status_Formula__c, Contact_Details__c, Nationality_Conditions__c, Agreement_Duration__c, Course_Payments__c, Enrolment_Conditions__c, Extra_Information__c, Refund_and_Cancellation__c, Signed_Date__c, Agency_Regions__c, Agency_Exc_Countries__c, Include_Countries__c, Cancelled_by__r.Name, Cancelled_On__c, Cancel_Reason__c, Allow_Sub_Representatives__c, createdBy.Name, createdDate, Notes__c, School_not_Sign__c ';
		
		sql += ', (SELECT Account__r.Global_Link__c, School_Payment_Type__c, Commission_Includes_Tax__c, Tax_Claimable__c, Account__r.Name, Account__r.RecordType.Name, Account__r.Parent.Name, Account__r.Registration_name__c, Account__r.Trading_Name__c, Account__r.Type_of_Business_Number__c, Account__r.Type_of_Institution__c, Account__r.Business_Number__c, Account__r.Course_Types_Offered__c FROM Agreement_Accounts__r order by Account__r.Name) ';

		sql += ' FROM Agreement__c WHERE Id = :agreeId';

		Agreement__c ag;
		list<Agreement_Accounts__c> agencies = new list<Agreement_Accounts__c>();
		set<Id> aAccounts = new set<Id>();

		map<String, schoolDetails> schDetails  = new map<String, schoolDetails>();
				
		for(Agreement__c a : Database.query(sql)){
			ag = a;
			//Agreements Accounts
			for(Agreement_Accounts__c aa : a.Agreement_Accounts__r){
				if(aa.Account__r.RecordType.Name == 'Agency' && (userDetails.Contact.View_Full_Agreement__c || (!userDetails.Contact.View_Full_Agreement__c && aa.Account__r.Global_Link__c == userDetails.Contact.Account.Global_Link__c)))
					agencies.add(aa);

				//Schools
				else if(aa.Account__r.RecordType.Name == 'School'){
					schDetails.put(aa.Id, new schoolDetails(aa));
					aAccounts.add(aa.Id);
					schoolsIDS.add(aa.Account__c);
					schoolsIdsStr += aa.Account__c + ','; 		
				}
			}//end for
		}
		schoolsIdsStr = schoolsIdsStr.removeEnd(',');

		duration = ag.Agreement_Duration__c;
		nationality = ag.Nationality_Conditions__c;
		enrolment = ag.Enrolment_Conditions__c;
		coursePayments = ag.Course_Payments__c;
		refund = ag.Refund_and_Cancellation__c;
		extraInfo = ag.Extra_Information__c;

		//Agreement Notes
		if(ag.Notes__c != null){
			aNotes = loadNotes(ag.Notes__c);
		}else aNotes = 'null';
		//

		list<s3Docs> aDocs = new list<s3Docs>();	
		//Agreement Documents
		if(ag.Preview_Link__c!=null){
			for(String doc : ag.Preview_Link__c.split(FileUpload.SEPARATOR_FILE))
				aDocs.add(new S3Docs(doc.split(FileUpload.SEPARATOR_URL)));
		}
		
		//Get Schools Commissions
		sql = ' SELECT Agreement_Account__c, School_Control__c, Campus_Control__c, Campus_Name__c, Course_Type_Control__c, Country__c, School_Name__c, Campus_Id__r.name, Campus_Commission__c, Campus_Commission_Type__c, Campus_Course__c, Campus_Course__r.campus__c,CampusCourseTypeId__c, Campus_Id__c, Comments__c, Name, Course_Commission__c, Course_Commission_Type__c, Course_Name__c, Course_Type_Commission__c, Course_Type_Commission_Type__c, courseType__c, Id, School_Commission__c, School_Commission_Type__c, School_Id__c FROM Commission__c WHERE Agreement_Account__c in :aAccounts order by Agreement_Account__c, Country__c, School_Name__c, Campus_Name__c, Course_Type_Control__c, Course_Name__c';

		system.debug('sql comm. ==>' + sql);
		list<commissionDetails> otherC = new list<commissionDetails>();
		for(Commission__c c : Database.query(sql)){
			if(c.School_Id__c != null)
				schDetails.get(c.Agreement_Account__c).addSchoolComm(new commissionDetails(c.country__c, c.School_Name__c, c.Campus_Name__c, '', '', c.School_Commission__c, c.School_Commission_Type__c, c.Comments__c));
			else if(c.Campus_Id__c != null)
				schDetails.get(c.Agreement_Account__c).addOther(new commissionDetails(c.country__c, c.School_Name__c, c.Campus_Name__c, '', '', c.Campus_Commission__c, c.Campus_Commission_Type__c, c.Comments__c));
			else if(c.CampusCourseTypeId__c != null)
				schDetails.get(c.Agreement_Account__c).addOther(new commissionDetails(c.country__c, c.School_Name__c, c.Campus_Name__c, c.Course_Type_Control__c, '', c.Course_Type_Commission__c, c.Course_Type_Commission_Type__c, c.Comments__c));
			else if(c.Campus_Course__c != null)
				schDetails.get(c.Agreement_Account__c).addOther(new commissionDetails(c.country__c, c.School_Name__c, c.Campus_Name__c, c.Course_Type_Control__c, c.Course_Name__c, c.Course_Commission__c, c.Course_Commission_Type__c, c.Comments__c));
		}//end for


		list<schoolDetails> allSchools = new list<schoolDetails>();
		for(String aa : schDetails.keySet()){
			allSchools.add(schDetails.get(aa));
		}

		List<IPClasses.PMCSchoolDocuments> schoolDocuments = PMCHelper.retrieveSchoolDocumentsTreeFormat(schoolsIDS, true);

		//system.debug('THE RESPONSE '+JSON.serialize(schoolDocuments));

		if(schoolDocuments != null && !schoolDocuments.isEmpty()){
			for(IPClasses.PMCSchoolDocuments schoolDocs : schoolDocuments){
				for(schoolDetails scDetails : allSchools){
					if(scDetails.sAccount.Account__c == schoolDocs.schoolID){
						scDetails.documents = schoolDocs.folders;
					}
				}
			}
		}

		//Find logs on Amazon S3
		list<IPFunctions.LogHistory> logsList;
		String logs = agreements_new.findAgreementLog(string.valueOf(ag.Id));

		if(logs != null){
			logsList = (list<IPFunctions.LogHistory>) JSON.deserialize(logs, list<IPFunctions.LogHistory>.class);
			for(IPFunctions.LogHistory l : logsList){
				DateTime dt = Datetime.valueOfGmt(l.editDate);
				l.editDateCTuser = dt.format('yyyy-MM-dd HH:mm:ss');

				if(l.fieldName == 'Signed_Date__c'){
					if(l.oldValue != null)
						l.oldValue = (Date.valueOf(l.oldValue)).format();
					if(l.newValue != null)
						l.newValue = (Date.valueOf(l.newValue)).format();
				}
				else if(l.fieldName == 'Start_Date__c'){
					if(l.oldValue != null)
						l.oldValue = (Date.valueOf(l.oldValue)).format();
					if(l.newValue != null)
						l.newValue = (Date.valueOf(l.newValue)).format();
				}
				else if(l.fieldName == 'End_Date__c'){
					if(l.oldValue != null)
						l.oldValue = (Date.valueOf(l.oldValue)).format();
					if(l.newValue != null)
						l.newValue = (Date.valueOf(l.newValue)).format();
				}
			}//end for
		}

		agree = JSON.serialize(new agWrapper(ag, allSchools, agencies, aDocs, logsList));

		aDescription = JSON.serialize(new agreeDesc (duration, nationality, enrolment, coursePayments, refund, extraInfo));
	}

	public static String loadNotes(String dbNotes){
		list<noteWrapper> notes = (list<noteWrapper>) JSON.deserialize(dbNotes, list<noteWrapper>.class);
		for(noteWrapper n : notes){
			DateTime dt = Datetime.valueOfGmt(n.addedDate);
			n.addedDate = dt.format('yyyy-MM-dd HH:mm:ss');
		}//end for

		return JSON.serialize(notes);
	}

	public String aDescription {get;set;}
	private class agreeDesc{
		private String duration {get;set;}
		private String nationality {get;set;}
		private String enrolment {get;set;}
		private String coursePayments {get;set;}
		private String refund {get;set;}
		private String extraInfo {get;set;}

		public agreeDesc(String duration, String nationality, String enrolment, String coursePayments, String refund, String extraInfo){
 			this.duration = duration;
 			this.nationality = nationality;
 			this.enrolment = enrolment;
 			this.coursePayments = coursePayments;
 			this.refund = refund;
 			this.extraInfo = extraInfo;
		}
	}

	@remoteAction
	public static void cancelAgreement(String agreeId, String reason){
		Agreement__c ag = new Agreement__c(Id = agreeId, isCancelled__c = true, Cancel_Reason__c = reason, Cancelled_by__c = userInfo.getUserId(), Cancelled_On__c = system.now());
		update ag;
	}

	@remoteAction
	public static void deleteAgreement(String agreeId){
		map<String, Agreement_Accounts__c> delAgreeAcc = new map<String, Agreement_Accounts__c>([SELECT Id FROM Agreement_Accounts__c WHERE Agreement__c = :agreeId]);
		list<Commission__c> delComm = new list<Commission__c>([SELECT Id FROM Commission__c WHERE Agreement_Account__c IN :delAgreeAcc.keySet()]);
		Agreement__c ag = new Agreement__c(Id = agreeId);

		delete delComm;
		delete ag;
	}

	@remoteAction
	public static String saveNote(String agreementId, String note){
		list<noteWrapper> newNotes;
		
		User userDetails = [Select Id, Name, Contact.Account.Name From User Where Id = :userInfo.getUserId()];
		Datetime GMT = system.now();
		String editDate = GMT.format('yyyy-MM-dd HH:mm:ss', 'UTC');

		//Double check last agreement Note
		Agreement__c agreement = [Select Notes__c from Agreement__c where Id = :agreementId];
		
		if(agreement.Notes__c != null) // Get old notes
			newNotes = (list<noteWrapper>) JSON.deserialize(agreement.Notes__c, list<noteWrapper>.class);
		else
			newNotes = new list<noteWrapper>();

		newNotes.add(new noteWrapper(string.valueOf(newNotes.size() + 1), note, userDetails.Name, userDetails.Contact.Account.Name, editDate));

		String upNote = JSON.serialize(newNotes);

		agreement.Notes__c = upNote;

		update agreement;

		return 	(upNote);	
	}



	@remoteAction
	public static UserProfile getUserProfile(){
		String userid = 'U' + userInfo.getUserId();  //included a U in front of the Id 
													//to prevent a javascript error
		String sflocale = userInfo.getLocale();      //get user’s locale
		String anlocale = sflocale.toLowerCase();          
		anlocale = anlocale.replace('_','-');        // convert from locale format 
													//of en_US to en-us
		String money = userInfo.getDefaultCurrency();  // get user’s currency
		String language = userInfo.getLanguage();      // get user’s language
		TimeZone tz = userInfo.getTimezone();          // get the timezone
		String timezone = tz.toString();               // convert to user’s timezone

		UserProfile userp = new UserProfile(userid, sflocale, anlocale, 
											money, language, timezone);
		return userp;     
	}

	//helper class to return an object
	public class UserProfile {                  
		public String userid{get;set;}
		public String sflocale{get;set;}
		public String anlocale{get;set;}
		public String money{get;set;}
		public String language{get;set;}
		public String timezone{get;set;}

		public UserProfile (String userid, String sflocale, String anlocale, String money, 
							String language, String timezone) {
			this.userid = userid;
			this.sflocale = sflocale;
			this.anlocale = anlocale;
			this.money = money;
			this.language = language;   
			this.timezone = timezone;
		}
	}

	private class noteWrapper{
		private String Id {get;set;}
		private String note {get;set;}
		private String userName {get;set;}
		private String userAgency {get;set;}
		private String addedDate {get;set;}

		public noteWrapper(String Id, String note, String userName, String userAgency, String addedDate){
			this.Id = Id;
			this.note = note;
			this.userName = userName;
			this.userAgency = userAgency;
			this.addedDate = addedDate;
		}
	}
		
	private class agWrapper{
		private Agreement__c agreement {get;set;}
		private String createdDate {get;set;}
		private list<schoolDetails> schools {get;set;}
		private list<Agreement_Accounts__c> agencies {get;set;}
		private list<s3Docs> docs {get;set;}
		private list<IPFunctions.LogHistory> logs {get;set;}
		
		private agWrapper(Agreement__c agreement, list<schoolDetails> schools, list<Agreement_Accounts__c> agencies, list<s3Docs> docs, list<IPFunctions.LogHistory> logs){
			this.agreement = agreement;	
			this.schools = schools;
			this.agencies = agencies;	
			this.docs = docs;	
			this.logs = logs;	

			createdDate = agreement.createdDate.format('yyyy-MM-dd HH:mm:ss');
		}
	}

	private class schoolDetails{
		private Agreement_Accounts__c sAccount {get; set;}
		private commissionDetails schoolComm {get;set;}
		private list<commissionDetails> otherComm {get;set;}
		private List<IPClasses.PMCDocumentsFolder> documents{get;set;}
		
		private schoolDetails(Agreement_Accounts__c sAccount){
			this.sAccount = sAccount;
			schoolComm = new commissionDetails();
			otherComm = new list<commissionDetails>();
			documents = new List<IPClasses.PMCDocumentsFolder>();
		}
		
		private void addSchoolComm(commissionDetails schComm){
			this.schoolComm = schComm;
		}
		
		private void addOther(commissionDetails other){
			otherComm.add(other);
		}
	}

	public class commissionDetails{
		public string country{get; set;}
		public string school{get; set;}
		public string campus{get; set;}
		public string courseType{get; set;}
		public string course{get; set;}
		public decimal cValue{get; set;}
		// public decimal hifyCommission{get; set;}
		public decimal cType{get; set;}
		public string cComments{get; set;}

		public commissionDetails(){
			cValue = 0;
			cType = 0;
		}

		public commissionDetails(string country, string school, string campus, string courseType, string course, decimal cValue, decimal cType, string cComments){ //, decimal agCommission
			this.country = country;
			this.school = school;
			this.campus = campus;
			this.courseType = courseType;
			this.course = course;
			this.cValue = cValue;
			this.cType = cType;
			this.cComments = cComments;
			// this.hifyCommission = (value*agCommission)/100;
		}
	}

	public class s3Docs{
		public String docName {get;set;}
		public String docUrl {get;set;}

		public s3Docs(){}

		public s3Docs (List<String> docParts){
			this.docUrl = docParts[0];
			this.docName = docParts[1];
		}
	}
}