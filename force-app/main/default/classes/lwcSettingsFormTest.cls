@isTest
private class lwcSettingsFormTest {
    
    @isTest static void updatePasswordFormSettingsExceptionTest(){
        string r = lwcSettingsForm.updatePasswordFormSettings('1234','');
        System.assertNotEquals(r, 'success');
    }

    @isTest static void insertFormSettingsExceptionTest(){
        string r = lwcSettingsForm.insertFormSettings('objJson', '');
        System.assertNotEquals(r, 'success');
    }

    @isTest static void getSettingsFormPasswordExceptionTest(){
        string r = lwcSettingsForm.getSettingsFormPassword('');
        System.assertEquals(r, '');
    }

    @isTest static void getClientStageTest(){
        TestFactory tf = new TestFactory();        
        Account agency = tf.createAgency();           
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);

        Client_Stage__c csc = new Client_Stage__c();
        csc.Stage_description__c = 'LEAD - Not Interested';
        csc.Agency_Group__c = agency.ParentId;     
        insert csc;

        Test.startTest();
     	system.runAs(portalUser){            
            List<lwcSettingsForm.wrpGeneric> r = lwcSettingsForm.getClientStage();
            System.assertNotEquals(r, null);
        }        
    }

    @isTest static void getUserPicklistTest(){
        TestFactory tf = new TestFactory();        
        Account agency = tf.createAgency();         
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);

        Test.startTest();
     	system.runAs(portalUser){            
            List<lwcSettingsForm.wrpGeneric> r = lwcSettingsForm.getUserPicklist('');
            System.assertNotEquals(r, null);
        }        
    }

    @isTest static void getEmailTemplateTest(){
        TestFactory tf = new TestFactory();        
        Account agency = tf.createAgency();   

        Email_Template__c etp = new Email_Template__c();
        etp.Email_Subject__c = 'School Request';
        etp.Template__c = 'Hi How are you?';
        etp.Template_Description__c = 'Request LOO to School';
        etp.agency__c = agency.Id;
        insert etp;   
           
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);

        Test.startTest();
     	system.runAs(portalUser){            
            List<lwcSettingsForm.wrpGeneric> r = lwcSettingsForm.getEmailTemplate(agency.id);
            System.assertNotEquals(r, null);
        }         
    }
    
    @isTest static void updatePasswordFormSettingsTest(){ 
        
        TestFactory tf = new TestFactory();        
        Account agency = tf.createAgency();           
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);

        Test.startTest();
     	system.runAs(portalUser){            
            string r = lwcSettingsForm.updatePasswordFormSettings('1234', '');
            System.assertEquals(r, 'success');
        } 
    }
    
    @isTest static void insertFormSettingsTest(){ 
        TestFactory tf = new TestFactory();        
        Account agency = tf.createAgency();           
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);

        Test.startTest();
     	system.runAs(portalUser){            
            string r = lwcSettingsForm.insertFormSettings('objJson', agency.id);
            System.assertEquals(r, 'success');
        }        
    }
    
    @isTest static void getSettingsTest(){
        
        TestFactory tf = new TestFactory();        
        Account agency = tf.createAgency();   
        agency.Form_DeskSettings__c = '[{"tpclient":{"label":"New Lead","value":"New"},"user":{"label":"005O0000005VmJ0IAK","value":"Patricia Greco"},"emailsender":{"label":"005O0000005VmJ0IAK","value":"Pryx Nascimento"},"template":{"label":"005O0000005VmJ0IAK","value":"Request LOO to School"},"templateExist":{},"templateOffshore":{},"AssigningTo":{"label":"005O0000005AkNOIA0","value":"Pryx Nascimento"},"ClientStages":{},"DuoDate":"7","paises":["Brazil","Aruba"],"visas":["Student","Internship"]},{"tpclient":{"label":"Existing Contacts (not clients)","value":"Existing"},"user":{"label":"005O0000005AkNOIA0","value":"Pryx Nascimento"},"emailsender":{"label":"005O0000005VmJ0IAK","value":"Juliana Sousa"},"template":{},"templateExist":{"label":"005O0000005VmJ0IAK","value":"meu template refund"},"templateOffshore":{},"AssigningTo":{"label":"005O0000005AkNOIA0","value":"Pryx Nascimento"},"ClientStages":{},"DuoDate":"4","paises":["Brazil"],"visas":["Student"]},{"tpclient":{"label":"Offshore Arrival","value":"Offshore"},"user":{"label":"005O0000005Vs0mIAC","value":"Vitor Costa"},"emailsender":{"label":"005O0000005VmJ0IAK","value":"Vitor Costa"},"template":{},"templateExist":{},"templateOffshore":{"label":"005O0000005VmJ0IAK","value":"Request Prod Commission 2"},"AssigningTo":{},"ClientStages":{"label":"a19O00000037OdyIAE","value":"Test Show Lead Novo"},"DuoDate":"5","paises":["Brazil"],"visas":["Student"]},{"tpclient":{"label":"All","value":"All"},"user":{"label":"005O0000003nhNTIAY","value":"Juliana Sousa"},"emailsender":{"label":"005O0000005VmJ0IAK","value":"Patricia Greco"},"template":{"label":"005O0000005VmJ0IAK","value":"COE Test"},"templateExist":{"label":"a1dO00000022tn7IAA","value":"Letter of Offer Received"},"templateOffshore":{"label":"005O0000005VmJ0IAK","value":"Email COE 2"},"AssigningTo":"{}","ClientStages":{"label":"a19O0000002l4mxIAA","value":"CLIENT - Non-Active (DIFFERENT VISA)"},"DuoDate":"4","paises":["Brazil"],"visas":["Student"]}]';
        update agency;
     
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);

        Test.startTest();
     	system.runAs(portalUser){            
            List<Object> r = lwcSettingsForm.getSettings('');
            System.assertNotEquals(r, null);
        }  

    }
    
    @isTest static void getSettingsFormPasswordTest(){
        
        TestFactory tf = new TestFactory();        
        Account agency = tf.createAgency();   
        agency.Form_Password__c = '1234';
        update agency;
        
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);

        Test.startTest();
     	system.runAs(portalUser){            
            string r = lwcSettingsForm.getSettingsFormPassword('');
            System.assertEquals(r, '1234');
        }     
    }
}