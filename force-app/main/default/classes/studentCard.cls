public with sharing class studentCard {
    
    public studentCard(){
    	
    }
    
    public studentCard(ApexPages.StandardController ctr){
    	
    }
    
    public string getCardImage () { 
    	string tp = ApexPages.currentPage().getParameters().get('tp');
    	
	    document doc = [Select Id, name FROM DOCUMENT WHERE Name = :tp limit 1];
	    string imageid = doc.id;
	    imageid = imageid.substring(0,15); 
	    return '/servlet/servlet.FileDownload?file=' + imageid;
	}
	
	private string filePath(string fileName){
		document doc = [Select Id, name FROM DOCUMENT WHERE Name = :fileName limit 1];
	    string imageid = doc.id;
	    imageid = imageid.substring(0,15); 
	    
	    String urlstr = Url.getCurrentRequestUrl().toExternalForm();
	    String cut = urlstr.substring(0, urlstr.indexOf('/', 8) + 1 );
	    
		return cut + ipFunctions.getCommunityName() +'/servlet/servlet.FileDownload?file=' + imageid + '&oid='+UserInfo.getOrganizationId();
	}
	
	public string getCardImagePreviewFront() { 
	    return filePath('id-studentCard');
	}
	
	public string getCardImagePreviewBack() { 
	    return filePath('id-studentCard-back');
	}
	
	//https://c.cs6.content.force.com/servlet/servlet.ImageServer?id=015N0000000Pp3q&oid=00DN0000000ATlW
	
	public Contact getClientDetails(){
		string ctId = ApexPages.currentPage().getParameters().get('id');
		return [Select Name, PhotoClient__c, Nationality__c, Birthdate, Current_Agency__r.name FROM CONTACT WHERE Id = :ctId];
	}
	
	public PageReference generateIdCard(){
		string ctId = ApexPages.currentPage().getParameters().get('id');
		PageReference pr = Page.studentCard;
		pr.getParameters().put('id',ctId);
		pr.getParameters().put('tp','id-studentCard');
		pr.setRedirect(false);
		return pr;		
	}
	
	public PageReference generateIdCardBack(){
		string ctId = ApexPages.currentPage().getParameters().get('id');
		PageReference pr = Page.studentCard;
		pr.getParameters().put('id',ctId);
		pr.getParameters().put('tp','id-studentCard-back');
		pr.setRedirect(false);
		return pr;		
	}
	
}