public with sharing class Batch_Migrate_NotInterested_Stage0{
	public Batch_Migrate_NotInterested_Stage0(){
		system.debug('');
	}
}
/*global class Batch_Migrate_NotInterested_Stage0 implements Database.Batchable<sObject> {
	
	String agencyGroup;
	
	global Batch_Migrate_NotInterested_Stage0(String agencyGroup) {
		this.agencyGroup = agencyGroup;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		String query = 'SELECT ID, Stage_Item__c, Stage__c, Client__r.ID, Client__r.Lead_Stage__c, Client__r.Status__c FROM Client_Stage_Follow_Up__c WHERE Stage_Item__c = \'LEAD - Not Interested\' AND Agency_Group__c = :agencyGroup'; 
		
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<Client_Stage_Follow_Up__c> checklist) {
		List<Contact> contacts = new List<Contact>(); 
		Contact cttToUpdate;
		for(Client_Stage_Follow_Up__c check : checklist){
			check.Stage__c = 'Stage 0';
			if(check.Client__r.Status__c == 'LEAD - Not Interested'){
				cttToUpdate = new Contact(ID = check.Client__r.ID);
				cttToUpdate.Lead_Stage__c = 'Stage 0';
				contacts.add(cttToUpdate);
			}
		}
		update checklist;
		if(!contacts.isEmpty()){
			update contacts;
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}*/