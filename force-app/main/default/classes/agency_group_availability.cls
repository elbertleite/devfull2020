public without sharing class agency_group_availability {

	public Account agencyGroup {get;set;}
	public String agencyID {get;set;}
	public boolean foundSuppliers {get{if(foundSuppliers == null) foundSuppliers = false; return foundSuppliers;}set;}
	public String noSuppliersMSG {get;set;}
	public Map<String, Map<String, List<Supplier__c>>> suppliers {get;set;}
	public Map<String, Totals> countryTotals {get;set;}

	private final String AVAILABLE = 'available';
	private final String NOT_AVAILABLE = 'notavailable';
	private final String PREFERABLE = 'preferable';
	private final String NOT_PREFERABLE = 'notpreferable';

	public agency_group_availability(){
		setAgencyInfo();
	}


	private void setAgencyInfo(){

		User u = [select contact.accountid, contact.account.parentid from user where id = :UserInfo.getUserId()];
		agencyID = u.contact.accountid;
		String agencyGroupID = u.contact.account.parentid;
		try {
			agencyGroup = [SELECT Name, (SELECT Name, BillingCountry, BillingCity FROM ChildAccounts) FROM Account where id = :agencyGroupID];
		} catch (Exception e){
			agencyGroup = new Account();
		}

	}

	public String selectedAgencyCountry {get;set;}
	private List<SelectOption> agencyCountries;
	public List<SelectOption> getAgencyCountries(){
		if(agencyCountries == null){

			agencyCountries = new List<SelectOption>();
			agencyCountries.add(new SelectOption('all','All'));

			Set<String> countries = new Set<String>();

			for(Account agency : agencyGroup.ChildAccounts){
				if(!countries.contains(agency.BillingCountry)){
					countries.add(agency.BillingCountry);
					agencyCountries.add(new SelectOption(agency.BillingCountry, agency.BillingCountry));
				}
			}
		}

		return agencyCountries;
	}

	public void refreshAgencies(){
		agencies = null;
		getAgencies();
	}

	public String selectedAgency {
		get		{
			if(selectedAgency==null)
				selectedAgency = agencyID;
			return 	selectedAgency;
		}set;}

	private List<SelectOption> agencies;
	public List<SelectOption> getAgencies(){
		if(agencies == null){

			agencies = new List<SelectOption>();

			for(Account agency : agencyGroup.ChildAccounts){
				if(selectedAgencyCountry != null && selectedAgencyCountry != 'all') {

					if(selectedAgencyCountry.equals(agency.BillingCountry))
						agencies.add(new SelectOption(agency.id, agency.Name));

				} else
					agencies.add(new SelectOption(agency.id, agency.Name));

			}

			//if(agencyGroup.ChildAccounts != null && agencyGroup.ChildAccounts.size() > 0)
			//	selectedAgency = agencyGroup.ChildAccounts[0].id;
		}

		return agencies;
	}


	public String selectedDestinationCountry {get;set;}
	private List<SelectOption> destinationCountries;
	public List<SelectOption> getDestinationCountries(){
		if(destinationCountries == null){

			destinationCountries = new List<SelectOption>();
			destinationCountries.add(new SelectOption('all', 'All'));

			for(AggregateResult ar : [select Supplier__r.BillingCountry from Supplier__c where Record_Type__c = 'campus' and agency__c = :selectedAgency group by Supplier__r.BillingCountry order by Supplier__r.BillingCountry])
				destinationCountries.add(new SelectOption( (String) ar.get('BillingCountry') , (String) ar.get('BillingCountry') ) );



		}

		return destinationCountries;
	}


	public String selectedDestinationCity {get;set;}
	private List<SelectOption> destinationCities;
	public List<SelectOption> getDestinationCities(){
		if(destinationCities == null || (selectedDestinationCountry != null && selectedDestinationCountry != '') ){

			destinationCities = new List<SelectOption>();
			destinationCities.add(new SelectOption('all', 'All'));

			if(selectedDestinationCountry != 'all')
				for(AggregateResult ar : [select Supplier__r.BillingCity from Supplier__c where Record_Type__c = 'campus' and agency__c = :selectedAgency and Supplier__r.BillingCountry = :selectedDestinationCountry group by Supplier__r.BillingCity order by Supplier__r.BillingCity])
					destinationCities.add(new SelectOption( (String) ar.get('BillingCity') , (String) ar.get('BillingCity') ) );

		}

		return destinationCities;
	}

	public String selectedSchool {get;set;}
	private List<SelectOption> schools;
	public List<SelectOption> getSchools(){

		schools = new List<SelectOption>();
		schools.add(new SelectOption('all', 'All'));

		if(selectedAgency != null){
				String sql = 'select Supplier__r.ParentID,  Supplier__r.Parent.Name from Supplier__c where Record_Type__c = \'campus\' and agency__c = \'' + selectedAgency + '\' ';

				if(selectedDestinationCountry != null && selectedDestinationCountry != 'all')
					sql += ' and Supplier__r.BillingCountry = \'' + selectedDestinationCountry + '\' ';

				if(selectedDestinationCity != null && selectedDestinationCity != 'all')
					sql += ' and Supplier__r.BillingCity = \'' + selectedDestinationCity + '\' ';

				sql += ' group by Supplier__r.ParentID, Supplier__r.Parent.Name order by Supplier__r.Parent.Name';


				for(AggregateResult ar : Database.query(sql))
					schools.add(new SelectOption( (String) ar.get('ParentID') , (String) ar.get('Name') ) );
		}
		return schools;
	}


	public String selectedWebsiteAvailability {get;set;}
	public String selectedAvailability {get;set;}
	public List<SelectOption> getAvailabilityOptions(){

		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('all', 'All'));
		options.add(new SelectOption(AVAILABLE, 'Available'));
		options.add(new SelectOption(NOT_AVAILABLE, 'Not Available'));

		return options;
	}

	public String selectedPreferability {get;set;}
	public List<SelectOption> getPreferabilityOptions(){

		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('all', 'All'));
		options.add(new SelectOption(PREFERABLE, 'Favourite'));
		options.add(new SelectOption(NOT_PREFERABLE, 'No Preference'));

		return options;
	}


	public boolean webAgency {get;set;}
	private Map<String, Supplier__c> oldSuppliers;
	public void search(){
		oldSuppliers = new Map<String, Supplier__c>();
		foundSuppliers = false;
		suppliers = new Map<String, Map<String, List<Supplier__c>>>();
		countryTotals = new Map<String, Totals>();

		if(selectedAgency == null) return;

		webAgency = [select view_web_leads__c from account where id = :selectedAgency].view_web_leads__c;

		String sql = 'SELECT Agency__c, Supplier__r.BillingCountry, Supplier__r.Parent.Name, Supplier__r.Name, Available__c, Preferable__c, Web_Available__c, Supplier__r.ShowCaseOnly__c, Supplier__r.Disabled_Campus__c FROM Supplier__c where Record_Type__c = \'campus\' and agency__c = \'' + selectedAgency + '\' '; // and Supplier__r.Disabled_Campus__c=false 

		if(selectedDestinationCountry != null && selectedDestinationCountry != 'all')
			sql += ' and Supplier__r.BillingCountry = \'' + selectedDestinationCountry + '\' ';

		if(selectedDestinationCity != null && selectedDestinationCity != 'all')
			sql += ' and Supplier__r.BillingCity = \'' + selectedDestinationCity + '\' ';

		if(selectedSchool != null && selectedSchool != 'all')
			sql += ' and Supplier__r.ParentID = \'' + selectedSchool + '\' ';

		if(selectedAvailability != null && selectedAvailability != 'all'){
			if(selectedAvailability.equals(AVAILABLE))
				sql += ' and Available__c = true ';
			else
				sql += ' and Available__c = false ';
		}

		if(selectedPreferability != null && selectedPreferability != 'all'){
			if(selectedPreferability.equals(PREFERABLE))
				sql += ' and Preferable__c = true ';
			else
				sql += ' and Preferable__c = false ';
		}

		if(selectedWebsiteAvailability != null && selectedWebsiteAvailability != 'all'){
			if(selectedWebsiteAvailability.equals(AVAILABLE))
				sql += ' and Web_Available__c = true ';
			else
				sql += ' and Web_Available__c = false ';
		}




		sql += ' order by Supplier__r.BillingCountry, Supplier__r.Parent.Name, Supplier__r.Name ';

		for(Supplier__c sp : Database.query(sql)){
			oldSuppliers.put(sp.id, sp.clone(false,true,false,false));
			if(suppliers.containsKey(sp.Supplier__r.BillingCountry)){
				if(suppliers.get(sp.Supplier__r.BillingCountry).containsKey(sp.Supplier__r.Parent.Name)){
					suppliers.get(sp.Supplier__r.BillingCountry).get(sp.Supplier__r.Parent.Name).add(sp);
					countryTotals.get(sp.Supplier__r.BillingCountry).addCampus();

					if(sp.Available__c)
						countryTotals.get(sp.Supplier__r.BillingCountry).available();
					if(sp.Preferable__c)
						countryTotals.get(sp.Supplier__r.BillingCountry).favourite();
					if(sp.Web_Available__c)
						countryTotals.get(sp.Supplier__r.BillingCountry).webAvailable();

				} else {
					suppliers.get(sp.Supplier__r.BillingCountry).put(sp.Supplier__r.Parent.Name, new List<Supplier__c>{sp});
					countryTotals.get(sp.Supplier__r.BillingCountry).addSchool();
					countryTotals.get(sp.Supplier__r.BillingCountry).addCampus();

					if(sp.Available__c)
						countryTotals.get(sp.Supplier__r.BillingCountry).available();
					if(sp.Preferable__c)
						countryTotals.get(sp.Supplier__r.BillingCountry).favourite();
					if(sp.Web_Available__c)
						countryTotals.get(sp.Supplier__r.BillingCountry).webAvailable();

				}

			} else {
				suppliers.put(sp.Supplier__r.BillingCountry, new Map<String, List<Supplier__c>>{sp.Supplier__r.Parent.Name => new List<Supplier__c>{sp}});
				countryTotals.put(sp.Supplier__r.BillingCountry, new Totals());

				if(sp.Available__c)
						countryTotals.get(sp.Supplier__r.BillingCountry).available();
				if(sp.Preferable__c)
					countryTotals.get(sp.Supplier__r.BillingCountry).favourite();
				if(sp.Web_Available__c)
					countryTotals.get(sp.Supplier__r.BillingCountry).webAvailable();


			}

		}


		if(suppliers.isEmpty()){
			foundSuppliers = false;
			noSuppliersMSG = 'There are no schools available.';
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, noSuppliersMSG);
            ApexPages.addMessage(myMsg);
		} else {
			foundSuppliers = true;
		}

	}

	public void save(){
		List<Account> accounts = new List<Account>();
		List<Supplier__c> sups = new List<Supplier__c>();
		for(Map<String, List<Supplier__c>> m : suppliers.values()){
			for(List<Supplier__c> listSuppliers : m.values()){
				sups.addAll(listSuppliers);

				//check for differences in web availability
				for(Supplier__c sp : listSuppliers)
					if(oldSuppliers.get(sp.id).Web_Available__c != sp.Web_Available__c)
						accounts.add( new Account(id = sp.Supplier__c));


			}
		}

		update accounts;
		update sups;




		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Items saved successfully.');
        ApexPages.addMessage(myMsg);

	}

	private class Totals {

		public Integer schools {get {if(schools == null) schools = 1; return schools;} set;}
		public Integer campuses {get {if(campuses == null) campuses = 1; return campuses;} set;}

		public Integer available {get {if(available == null) available = 0; return available;} set;}
		public Integer favourite {get {if(favourite == null) favourite = 0; return favourite;} set;}
		public Integer webAvailable {get {if(webAvailable == null) webAvailable = 0; return webAvailable;} set;}

		public void addSchool(){
			this.schools++;
		}

		public void addCampus() {
			this.campuses++;
		}

		public void available() {
			this.available++;
		}

		public void favourite() {
			this.favourite++;
		}

		public void webAvailable() {
			this.webAvailable++;
		}

	}


}