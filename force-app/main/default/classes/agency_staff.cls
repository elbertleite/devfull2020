public with sharing class agency_staff {
	
	
	public Contact contact {get;set;}
	public Account agency {get;set;}
	public boolean edit {get;set;}
	private boolean isNewStaff = false;
	private IPFunctions.CampusAvailability ca; 
	
	public agency_staff(ApexPages.StandardController controller){
		
		ca = new IPFunctions.CampusAvailability();
		
		String agencyID = controller.getId();
		if(agencyID != null)
			agency = getDetails(agencyID);
		
		String contactID= Apexpages.currentPage().getParameters().get('contactID');
		
		if(contactID != null){
			contact = getContact(contactID);
			edit = false;
			
			if(contact.Preferable_School_Country__c != null)
				retrieveCities();
				
		} else {
			contact = new Contact();
			edit=true;
			isNewStaff = true;
		}
		
		
		
	}
	
	private Account getDetails(String id){
		Account acc = [Select Name, CreatedDate, CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name, Nickname__c, Agency_Type__c, Group_Type__c, Phone, Skype__c,
						BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry,  ParentId, Parent.Name, 
						Registration_name__c, Year_established__c, BusinessEmail__c, Website
				from Account where id = :id];

		
		return acc;		
	}
	
	
	private Contact getContact(String id){
		
		return [Select AccountID, id, FirstName, LastName, Email, Gender__c, Birthdate, Department, MobilePhone, Nationality__c, Preferable_Nationality__c, Preferable_School_City__c, Preferable_School_Country__c 
				from Contact where id = :id];
		
	}
	
	
	
	public void setEdit(){
		edit = true;
	}
	
	public PageReference cancel(){
		edit = false;
		if(isNewStaff)
			return new Pagereference('/apex/agency_staff_list?id=' + agency.id);
		else
			getContact(contact.id);
			
		return null;	
	}
	
	public PageReference save(){
		
		if(contact.AccountID == null){
			contact.AccountID = agency.id;
			
			RecordType rt = [select id from RecordType where Name = 'Employee' and isActive = true LIMIT 1];
			
			contact.RecordTypeID = rt.id;
			
		}
		
		try {
			upsert contact;
			edit = false;
			
			return new Pagereference('/apex/agency_staff_list?id=' + agency.id);
			
		} catch (Exception e){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);
		}
		
		return null;		
		
		 	
		
	}
	
	public List<SelectOption> nationalities {
		get {
			if(nationalities == null){
				nationalities = new List<SelectOption>();
				nationalities.add(new SelectOption('',''));
				
				Schema.DescribeFieldResult fieldResult = Schema.Contact.Nationality__c.getDescribe();
   				List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();        
			   	for( Schema.PicklistEntry f : ple){
			    	nationalities.add(new SelectOption(f.getLabel(), f.getValue()));
			   	}
				
			}
			return nationalities;
			
		}
		set;
	}
	

	public List<SelectOption> countries{
		get{
			if(countries == null) {
				//selectedCountry = null;
				countries = ca.getSchoolCountries(agency.id, true);
			}
			return countries;
		}
		set;
	}
	
	
	
	
	public List<SelectOption> Cities{get{if (cities == null) cities = new List<SelectOption>(); return cities;} set;}
	public void retrieveCities(){		
		cities.clear();
		system.debug('@@@ contact.Preferable_School_Country__c: ' + contact.Preferable_School_Country__c);
		system.debug('@@@ agency.id: ' + agency.id); 
		system.debug('@@@ ca: ' + ca);
		cities = ca.getSchoolCities(agency.id, contact.Preferable_School_Country__c, true);
	}
	
	


}