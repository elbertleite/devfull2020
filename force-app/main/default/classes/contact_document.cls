public with sharing class contact_document {

    public Contact client {get;set;}

    public String bucket { get{ return IPFUNCTIONS.s3bucket; } private set; }
    
    public contact_document(){
    }

    public contact_document(ApexPages.standardController controller){

        String cid = ApexPages.currentPage().getParameters().get('cid');
        if(cid != null & cid != '')
            client = [Select id, RecordType.Name from Contact where id = :cid];


        String category = ApexPages.currentPage().getParameters().get('cat');
        if(category != null){
            document.Document_Category__c = category;
            if(document.Document_Category__c == 'Visa')
                document.Document_Type__c = 'Visa';
            else if(document.Document_Category__c == 'Passport')
                document.Document_Type__c = 'Passport';
        }


        String docid = ApexPages.currentPage().getParameters().get('id');
        if(docid != null){
            document = [Select id, Name, C.Account_Name__c, C.Account_Number__c, C.Bank__c, C.Bank_Country__c, C.Branch_Address__c, C.Branch_Name__c, C.BSB__c, C.Client__c, C.Comment__c, Issuing_Authority__c ,
                                C.Country_of_Issue__c, C.Date_of_Issue__c, C.Document_Category__c, C.Document_Number__c, C.Document_Type__c, C.eTicket_Number__c, C.Expiry_Date__c,
                                C.Fax__c, C.IBAN__c, C.Notes__c, C.Phone__c, C.Swift_Code__c, Visa_conditions__c, Visa_country_applying_for__c, Visa_number__c, Visa_subclass__c, Visa_type__c,
                                TRN__c, Date_granted__c
                        from Client_Document__c C where id = :docid];



        }

        getFields();

        if(isFlightDetails)
                getFlightDetails(document.id);


    }

    private void getFlightDetails(String documentID){


        flightDetails = [Select C.Airline_Reference_Number__c, C.Arrival_Airline_Company__c, C.Arrival_Airport__c, C.Arrival_Country__c, C.Arrival_Date__c, C.Arrival_Flight_Number__c,
                                C.Arrival_Hour__c, C.Arrival_Minute__c, C.Arrival_Time__c, C.Client_Document__c, C.Departure_Airline_Company__c, C.Departure_Airport__c, C.Departure_Country__c,
                                C.Departure_Date__c, C.Departure_Flight_Number__c, C.Departure_Hour__c, C.Departure_Minute__c, C.Departure_Time__c, Custom_ID__c
                        from Client_Flight_Details__c C where Client_Document__c = :documentID];

    }

    public Client_Document__c document {
        get{
            if(Document == null){
                Document = new Client_Document__c();
                Document.client__c = client.id;
            }
            return document;
        }
        Set;
    }

    private Integer flightID = 0;

    public List<Client_Flight_Details__c> flightDetails {
        get{
            if(flightDetails == null){
                flightDetails = new List<Client_Flight_Details__c>();
                addNewFlight();
            }
            return flightDetails;
        }
        Set;
    }



    public void addNewFlight(){
        Client_Flight_Details__c cfd = new Client_Flight_Details__c();
        cfd.Custom_ID__c = flightID;
        flightID++;
        flightDetails.add(cfd);
    }

    public void deleteNewFlight(){
        Integer customFlightID = Integer.valueOf(ApexPages.currentPage().getParameters().get('flightID'));

        if(customFlightID != null){
            Integer count = 0;
            for(Client_Flight_Details__c cfd : flightDetails){
                if(cfd.Custom_ID__c == customFlightID){
                    flightDetails.remove(count);
                    break;
                }
                count++;
            }
        }
    }

    public void deleteClientDocs(){

    	String docid = ApexPages.currentPage().getParameters().get('docid');
    	Client_Document_File__c cdf = new Client_Document_File__c(id = docid);

    	delete cdf;

    }

    public PageReference deleteDocument(){

    	Pagereference pr = new PageReference('/apex/client_documents?id=' + document.Client__c);
    	pr.setRedirect(true);

    	IPFunctions.deleteDocumentFilesFromS3(document);

    	//delete document;

    	return pr;

    }


     public PageReference addNewDocument(){

    	Pagereference pr = new PageReference('/apex/contact_document?cid=' + document.Client__c);
    	pr.setRedirect(true);

    	//delete document;

    	return pr;

    }

    public void deleteFlight(){
        String flightID = ApexPages.currentPage().getParameters().get('flightID');
        Client_Flight_Details__c toDelete;
        if(flightID != null){
            for(Client_Flight_Details__c cfd : flightDetails){
                if(cfd.id == flightID){
                    toDelete = cfd;
                    break;
                }
            }
        }
        if(toDelete != null)
            delete toDelete;

        getFlightDetails(Document.id);

    }


    public list<Client_Document_File__c> getDocumentFiles(){

		List<Client_Document_File__c> doc = [Select id, File_Name__c, File_Size__c, Preview_Link__c, CreatedBy.Name, CreatedDate, ParentId__c  from Client_Document_File__c where client__c = :client.id and Parentid__c = :Document.id];
		if(doc == null)
			doc = new List<Client_Document_File__c>();
		return doc;
    }

    public List<SelectOption> countries {
        get {
            if(countries == null){
                countries = IPFunctions.getAllCountries();
                if(countries.size() > 0)
                    countries.add(0, new SelectOption('','--None--'));
            }
            return countries;
        }
        Set;
    }

    public Boolean isFlightDetails {
        get{
            if(isFlightDetails == null)
                isFlightDetails = false;
            return isFlightDetails;
        }
        Set;
    }

    public Boolean isVisaDetails {
        get{
            if(isVisaDetails == null)
                isVisaDetails = false;
            return isVisaDetails;
        }
        Set;
    }

    public Boolean isBankDetails {
        get{
            if(isBankDetails == null)
                isBankDetails = false;
            return isBankDetails;
        }
        Set;
    }

    public Boolean isDefaultDetails {
        get{
            if(isDefaultDetails == null)
                isDefaultDetails = false;
            return isDefaultDetails;
        }
        Set;
    }

    public PageReference clearFields(){

        Document.Document_type__c = '';
        isFlightDetails = false;
        isBankDetails = false;
        isDefaultDetails = false;
        isVisaDetails = false;
        return null;

    }

    public PageReference getFields(){


        if(Document.Document_type__c == 'Flight Ticket'){
            isFlightDetails = true;
            isBankDetails = false;
            isDefaultDetails = false;
            isVisaDetails = false;
        }else if(Document.Document_type__c == 'Bank Details'){
            isBankDetails = true;
            isFlightDetails = false;
            isDefaultDetails = false;
            isVisaDetails = false;
        }else if(Document.Document_type__c == 'Visa'){
            isBankDetails = false;
            isFlightDetails = false;
            isDefaultDetails = false;
            isVisaDetails = true;
        }else if( Document.Document_type__c == null || Document.Document_type__c == '' ){
            isDefaultDetails = false;
            isBankDetails = false;
            isFlightDetails = false;
            isVisaDetails = false;
        } else {
            isDefaultDetails = true;
            isBankDetails = false;
            isFlightDetails = false;
            isVisaDetails = false;
        }

        return null;

    }

    public PageReference save(){

        try {

            if(document.Document_Category__c == null || document.Document_Category__c == '' || document.Document_Type__c == null || document.Document_Type__c == ''){
                ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, 'Please select Document Category and Type.');
                ApexPages.addMessage(msg);
                return null;
            }



            if(isVisaDetails){

                if(document.Visa_Country_applying_for__c == null || document.Visa_Country_applying_for__c == ''){
                    ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, 'Please select \'Country applying for\'.');
                    ApexPages.addMessage(msg);
                    return null;
                }

                if(document.Client__c == null)
                    document.Client__c = client.id;
                upsert document;

            } else {
                if(document.Client__c == null)
                    document.Client__c = client.id;
                upsert Document;

                if(isFlightDetails){
                    for(Client_Flight_Details__c cfd : flightDetails)
                        if(cfd.Client_Document__c == null)
                            cfd.Client_Document__c = document.id;

                    upsert flightDetails;
                }
            }

            PageReference acctPage = new PageReference('/apex/contact_document?id=' + Document.id + '&cid=' + Document.client__c + '&detail=true');
            acctPage.setRedirect(true);
            return acctPage;

        } catch (Exception e){

            return null;

        }

    }

    public PageReference cancel(){
        String clientID = document.Client__c != null ? document.Client__c : client.id;
        PageReference acctPage = new PageReference('/apex/client_documents?id=' + clientID);
        acctPage.setRedirect(true);
        return acctPage;
    }

    public PageReference editDocument(){
        PageReference acctPage;
        acctPage = new PageReference('/apex/contact_document?id=' + Document.id + '&cid=' + Document.client__c + '&detail=false');
        acctPage.setRedirect(true);
        return acctPage;
    }


    public string getOrgId(){
        return  UserInfo.getOrganizationId();
    }


}