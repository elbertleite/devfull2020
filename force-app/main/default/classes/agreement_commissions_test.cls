@isTest
private class agreement_commissions_test{

	public static Account school {get;set;}
	public static Account agency {get;set;}
	public static Account campus {get;set;}
	public static Account campus2 {get;set;}
	public static Course__c course {get;set;}
	public static Course__c course2 {get;set;}
	public static Campus_Course__c campusCourse {get;set;}
	
	@testSetup static void setup() {
		TestFactory tf = new TestFactory();

		agency = tf.createAgency();
		school = tf.createSchool();
		campus = tf.createCampus(school, agency);
		campus2 = tf.createCampus(school, agency);
		course = tf.createCourse();
		course2 = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);
		campusCourse =  tf.createCampusCourse(campus2, course2);

		//Agreement Parties
		agreements_new.CallResult result = agreements_new.saveAgreement(null, 'Agreement Test', agency.Id, 'Test User', '2018-02-12', 'Direct', 'School', school.Id + ';' + agency.id, 'All Regions', null, null, 'true', 'false');

		//Agreement Details
		result = agreements_new.saveGeneralInfo(result.agreementId, '2018-02-12', null, true, 'School Contact Details', 'Nationality Exc.', 'Agree Duration', 'Enroll Conditions', 'Course Payment', 'Refund Cancel', 'Extra Info', false, false, false, false, '');

		

	}


	static testMethod void setCommission(){

		String agreementId = [Select Id From Agreement__c limit 1].Id;

		ApexPages.currentPage().getParameters().put('id', agreementId);
		agreement_commissions test = new agreement_commissions();

		test.coursesCommission[0].schSetup = '3;;true;;false';

		test.coursesCommission[0].SchoolCommissionObj.School_Commission__c = 10;
		test.coursesCommission[0].SchoolCommissionObj.School_Commission_Type__c = 0;

		test.coursesCommission[0].listCampus[0].campusCommissionObj.Campus_Commission__c = 20;
		test.coursesCommission[0].listCampus[0].campusCommissionObj.Campus_Commission_Type__c = 0;

		test.coursesCommission[0].listCampus[0].listCourseType[0].courseTypeCommissionObj.Course_Type_Commission__c = 30;
		test.coursesCommission[0].listCampus[0].listCourseType[0].courseTypeCommissionObj.Course_Type_Commission_Type__c = 0;

		test.coursesCommission[0].listCampus[0].listCourseType[0].listCourseCommission[0].Course_Commission__c = 200;
		test.coursesCommission[0].listCampus[0].listCourseType[0].listCourseCommission[0].Course_Commission_Type__c = 1;

		test.coursesCommission[0].listCampus[1].campusCommissionObj.Campus_Commission__c = 20;
		test.coursesCommission[0].listCampus[1].campusCommissionObj.Campus_Commission_Type__c = 0;

		test.coursesCommission[0].listCampus[1].listCourseType[0].courseTypeCommissionObj.Course_Type_Commission__c = 30;
		test.coursesCommission[0].listCampus[1].listCourseType[0].courseTypeCommissionObj.Course_Type_Commission_Type__c = 0;

		test.coursesCommission[0].listCampus[1].listCourseType[0].listCourseCommission[0].Course_Commission__c = 200;
		test.coursesCommission[0].listCampus[1].listCourseType[0].listCourseCommission[0].Course_Commission_Type__c = 1;

		ApexPages.currentPage().getParameters().put('isAvailable', 'true');
		test.saveCommission();
	}

	static testMethod void redirectWizard(){
		String agreementId = [Select Id From Agreement__c limit 1].Id;		
		
		ApexPages.currentPage().getParameters().put('id', agreementId);
		ApexPages.currentPage().getParameters().put('stepNum', '1');
		
		agreement_commissions test = new agreement_commissions();
		test.redirectWizard();
	}

	static testMethod void filters(){
		String agreementId = [Select Id From Agreement__c limit 1].Id;		
		agreement_commissions test = new agreement_commissions();

		integer fromUnit = test.fromUnit;
		decimal value = test.value;
		string details = test.details;
		
		List<SelectOption> itemsControl = test.getItemsControl();
		List<SelectOption> itemsAction = test.getItemsAction();
		List<SelectOption> commType = test.getItemCommissionType();
		map<string,string> mapActions = test.getmapAction();
		map<string,string> mapControl = test.getmapControl();
		test.refreshCommissions();
	}
	
	
}