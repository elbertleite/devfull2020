/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class client_course_instalment_pay_test {

    static testMethod void myUnitTest() {
       
       TestFactory tf = new TestFactory();
       
       	Account schGroup = tf.createSchoolGroup();

		Account school = tf.createSchool();
		school.ParentId = schGroup.Id;
		update school;

       Account agency = tf.createAgency();
       Contact emp = tf.createEmployee(agency);
       User portalUser = tf.createPortalUser(emp);
       Account campus = tf.createCampus(school, agency);
       Course__c course = tf.createCourse();
       Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
       
       
       
       Test.startTest();
       system.runAs(portalUser){
       	   Contact client = tf.createLead(agency, emp);
	       client_course__c booking = tf.createBooking(client);
	       client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       client_course__c courseCredit =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       courseCredit.isCourseCredit__c = true;
	       courseCredit.Credit_Available__c = 1000;
	       courseCredit.Credit_Valid_For_School__c = school.ParentId;
	       update courseCredit;
	       
	       client_course__c deposit =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       deposit.isDeposit__c = true;
	       deposit.Deposit_Total_Available__c = 1000;
	       deposit.Deposit_Total_Used__c = 0;
	       update deposit;
	       
	       List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
	       
	       client_course_instalment_pay testClass = new client_course_instalment_pay(new ApexPages.StandardController(instalments[0]));
	       
	       
	       client_course_instalment__c installment = testClass.installment;
	       list<client_course_instalment_payment__c> depositTest = testClass.deposit;
	       list<client_course_instalment_pay.feeInstallmentDetail> feesInst = testClass.getFeesInstallment();
	       
	       
	       
		   list<selectOption> depositListType =  testClass.getdepositListType();
		   
		   decimal totalpaid = testClass.totalPaid;
		   
		   
		   testClass.newPayDetails.Payment_Type__c = 'Deposit';
		   testClass.newPayDetails.Value__c = 0;
		   testClass.addPaymentValue();
		   testClass.newPayDetails.Value__c = 2250;
		   testClass.addPaymentValue();
		   testClass.newPayDetails.Value__c = 1500.0;
		   
		   
		   testClass.addPaymentValue();
		   testClass.newPayDetails.Value__c = 500;
		   testClass.newPayDetails.Date_Paid__c = Date.today().addDays(1);
		   testClass.addPaymentValue();
		   
		   testClass.newPayDetails.Date_Paid__c = Date.today();
		   testClass.addPaymentValue();
		   
		   
		   ApexPages.currentPage().getParameters().put('index', '0');
		   testClass.removePaymentValue();
		   
		   testClass.newPayDetails.Value__c = 500;
		   testClass.newPayDetails.Payment_Type__c = 'School Credit';
		   testClass.addPaymentValue();
		   ApexPages.currentPage().getParameters().remove('index');
		   ApexPages.currentPage().getParameters().put('index', '1');
		   testClass.removePaymentValue();
		   
		   testClass.newPayDetails.Value__c = 500;
		   testClass.newPayDetails.Payment_Type__c = 'Pds';
		   testClass.addPaymentValue();
		   ApexPages.currentPage().getParameters().put('index', '2');
		   testClass.removePaymentValue();
		   
		   testClass.newPayDetails.Value__c = 500;
		   testClass.newPayDetails.Payment_Type__c = 'offShore';
		   testClass.addPaymentValue();
		   ApexPages.currentPage().getParameters().put('index', '3');
		   testClass.removePaymentValue();
		   
		   testClass.newPayDetails.Value__c = 2230;
		   testClass.selectedUser = client.id;
		   testClass.selectedAgency = agency.id;
		   testClass.newPayDetails.Payment_Type__c = 'School Credit';
		   testClass.addPaymentValue();
		   testClass.savePayment();
		   
		   List<SelectOption> agencyGroupOptions = testClass.agencyGroupOptions;
		   testClass.changeAgencyGroup();
		   testClass.changeAgency();
		   List<SelectOption> agencyOptions = testClass.agencyOptions;
		   List<SelectOption> userOptions = testClass.userOptions;
		   
		   testClass.cancelPayment();
       }
       Test.stopTest();
    }
}