/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class xcourseSearch_AgencyCurrency_test {

    static testMethod void myUnitTest() {
       
       	TestFactory testFactory = new TestFactory();       
       
		Account agency = TestFactory.createAgency();
		
		Contact employee = TestFactory.createEmployee(agency);
		
		Currency_rate__c cr = new Currency_rate__c();
		cr.Agency__c = agency.id;
		cr.CurrencyCode__c = 'CAD';
		cr.Value__c = 1.5; 
		cr.High_Value__c = 1.8;
		insert cr;
		
		
		ApexPages.Standardcontroller controller = new Apexpages.Standardcontroller(agency);
		xcourseSearch_AgencyCurrency ac = new xcourseSearch_AgencyCurrency(controller);
		
		ac.refreshCurrency();
		ac.updateCurrencyRate();
		ac.changeAgencyCurrency();
		ac.getCurrencies();
		ac.getCurrenciesRate();
		
		ac.getAvailableCurrencies();
		
		ac.selectedAvailableCurrency = new List<String>{'BRL'};
		ac.addAgencyCurrency();
		
		ac.selectedCurrentCurrencies = new List<String>{'BRL'};
		ac.removeAgencyCurrency();
		
		List<SelectOption> optionalCurrency = ac.optionalCurrency;
		ac.getCurrenciesRate();
		ApexPages.currentPage().getParameters().put('lowSpread', '2');
		ac.copyCurrencyRate();
		ApexPages.currentPage().getParameters().put('highSpread', '3');
		ac.copyHighCurrencyRate();
		
		ac.agency.Optional_Currency__c = 'EUR';
		ac.saveCurrencies();
		
		ac.getCurrenciesRate();
		try {
			ac.getAgencyCurrencyConvertion();
			ac.getAgencyOptionalCurrencyConvertion();
		} catch(Exception e){
			 //callouts not supported on tests :( 
		}
		 
		/*ac.refreshCurrency();
		ac.getCurrenciesRate();
		ac.changeAgencyCurrency();
		
		ac.agency.Optional_Currency__c = 'COP';
		
		ac.saveCurrencies();
		
		//ac.getAgencyCurrencyConvertion();
		
		ac.getAvailableCurrencies();
		
		ac.addAgencyCurrency();
		
		
		
		
		ac.updateCurrencyRate();*/
		
		
		
		
    }
}