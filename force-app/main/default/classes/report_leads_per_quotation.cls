public with sharing class report_leads_per_quotation {

	public User user{get;set;}

	public List<SelectOption> agencies{get;set;}
	public List<SelectOption> employees{get;set;}
	public List<SelectOption> schools{get;set;}
	public List<SelectOption> campuses{get;set;}
	public List<SelectOption> countries{get;set;}

	public Set<String> idAgencies{get;set;}

	public String campusSelected{get;set;}
	public String schoolSelected{get;set;}
	public String countrySelected{get;set;}
	public String employeeSelected{get;set;}
	public String agencySelected{get;set;}
	public String groupSelected{get;set;}
	public String endDate{get;set;}
	public String beginDate{get;set;}

	public String graphResult{get;set;}
	public List<ReportResult> resultReport{get;set;}

	public boolean startingPage{get;set;}
	public boolean canExportToExcel{get;set;}
	public boolean resultTooBig{get;set;}
	public boolean isExcelExport{get;set;}

	public static final Integer SIZE_LIMIT = 500;

	public report_leads_per_quotation() {
		
		startingPage = true;
		
		user = [Select Id, Name, Contact.Export_Report_Permission__c, Contact.AccountId, Contact.Name, Contact.Account.Id, Contact.Account.Name, Contact.Account.Parent.RDStation_Campaigns__c, Contact.Account.Parent.Name, Contact.Account.Parent.Id, Aditional_Agency_Managment__c, Contact.Group_View_Permission__c, Contact.Agency_View_Permission__c, Contact.Account.Global_Link__c from User where Id = :UserInfo.getUserId()];

		canExportToExcel = user.Contact.Export_Report_Permission__c;
		agencySelected = user.Contact.Account.Id;
		groupSelected = user.Contact.Account.ParentId;
	
		if(!String.isEmpty(ApexPages.currentPage().getParameters().get('export')) && Boolean.valueOf(ApexPages.currentPage().getParameters().get('export'))){
			this.beginDate = String.isEmpty(ApexPages.currentPage().getParameters().get('begin')) ? '' : ApexPages.currentPage().getParameters().get('begin'); 
			this.endDate = String.isEmpty(ApexPages.currentPage().getParameters().get('end')) ? '' : ApexPages.currentPage().getParameters().get('end'); 
			this.groupSelected = String.isEmpty(ApexPages.currentPage().getParameters().get('group')) ? '' : ApexPages.currentPage().getParameters().get('group'); 
			this.agencySelected = String.isEmpty(ApexPages.currentPage().getParameters().get('agency')) ? '' : ApexPages.currentPage().getParameters().get('agency'); 
			this.employeeSelected = String.isEmpty(ApexPages.currentPage().getParameters().get('employee')) ? '' : ApexPages.currentPage().getParameters().get('employee'); 
			this.schoolSelected = String.isEmpty(ApexPages.currentPage().getParameters().get('school')) ? '' : ApexPages.currentPage().getParameters().get('school'); 
			this.campusSelected = String.isEmpty(ApexPages.currentPage().getParameters().get('campus')) ? '' : ApexPages.currentPage().getParameters().get('campus'); 
			isExcelExport = true;
			generateReport();
		}else{

			isExcelExport = false;
			this.endDate = Date.today().format();
			this.beginDate = Date.today().addMonths(-1).format();
			
			this.agencies = loadAgencies(user.Contact.AccountId, user.Contact.Account.Parent.Id);
			this.employees = loadEmployees(user.Contact.AccountId);
			this.employeeSelected = user.Contact.ID;
			//this.employeeSelected = '';
			
			//Map<String, List<SelectOption>> countriesSchools = new MethodsWithoutSharing().loadSchools(agencySelected, groupSelected);
			
			//this.countries = countriesSchools.get('countries');
			//this.schools = countriesSchools.get('schools');
			this.countries = new MethodsWithoutSharing().loadCountries(agencySelected, groupSelected);
			
			this.schools = new MethodsWithoutSharing().loadSchoolsByCountry(null, agencySelected, groupSelected);
			this.campuses = new MethodsWithoutSharing().loadCampuses(this.schoolSelected);
			
			this.countrySelected = '';
			this.schoolSelected = '';
			this.campusSelected = '';


		}
	}

	public void generateReport(){
		try{
			startingPage = false;
			graphResult = '';
			resultReport = new List<ReportResult>();

			checkAttributes();

			List<Quotation_Detail__c> result = new MethodsWithoutSharing().generateReport(beginDate, endDate, groupSelected, agencySelected, employeeSelected, schoolSelected, campusSelected, countrySelected, isExcelExport);

			resultTooBig = result.size() >= SIZE_LIMIT;
			
			ReportResult report;
			SchoolReport schoolRelated;
			CampusReport campusRelated;
			ContactGraph contactRelated;

			Integer index;
			if(result != null && !result.isEmpty()){
				Decimal courseDuration;
				Decimal extraDuration;
				Decimal freeDuration;
				Decimal totalCourse;
				for(Quotation_Detail__c detail : result){
					report = new ReportResult(detail.Campus_Country__c);		
					index = resultReport.indexOf(report);
					if(index > -1){
						report = resultReport.get(index);
					}else{
						report.totalPerCountry = 0;
						report.schools = new List<SchoolReport>();
						resultReport.add(report);
					}
					report.totalPerCountry += 1;

					schoolRelated = new SchoolReport(detail.Campus_Course__r.Campus__r.Parent.ID, detail.Campus_Course__r.Campus__r.Parent.Name);

					index = report.schools.indexOf(schoolRelated);
					if(index > -1){
						schoolRelated = report.schools.get(index);
					}else{
						schoolRelated.totalPerSchool = 0;
						schoolRelated.campuses = new List<CampusReport>();
						report.schools.add(schoolRelated);
					}
					schoolRelated.totalPerSchool += 1;

					campusRelated = new CampusReport(detail.Campus_Course__r.Campus__r.ID, detail.Campus_Course__r.Campus__r.Name);

					index = schoolRelated.campuses.indexOf(campusRelated);
					if(index > -1){
						campusRelated = schoolRelated.campuses.get(index);
					}else{
						campusRelated.totalPerCampus = 0;
						campusRelated.contacts = new List<ContactGraph>();
						schoolRelated.campuses.add(campusRelated);
					}
					campusRelated.totalPerCampus += 1;

					contactRelated = new ContactGraph();
					contactRelated.numberOfTimesOpened = detail.Quotation__r.Quotation_Total_Counter__c == null ? 0 : Integer.valueOf(detail.Quotation__r.Quotation_Total_Counter__c);
					contactRelated.contactID = detail.Quotation__r.Client__r.ID;
					contactRelated.contactName = detail.Quotation__r.Client__r.Name;
					contactRelated.contactEmail = detail.Quotation__r.Client__r.Email;
					contactRelated.contactOwner = detail.Quotation__r.Client__r.Owner__r.Name;
					contactRelated.contactAgency = detail.Quotation__r.Client__r.Owner__r.Account.Name;
					contactRelated.contactGroup = detail.Quotation__r.Agency__r.Parent.Name;
					contactRelated.campus = detail.Campus_Course__r.Campus__r.Name;
					contactRelated.course = detail.Campus_Course__r.Course__r.Name;
					contactRelated.quotation = detail.Quotation__r.Name;
					contactRelated.quotationID = detail.Quotation__r.ID;
					contactRelated.quotationCreatedBy = detail.Quotation__r.CreatedBy.Name;
					contactRelated.quotationCreatedAgency = detail.Quotation__r.CreatedBy.Account.Name;
					contactRelated.quotationCreatedDate = detail.Quotation__r.CreatedDate;

					if('Language'.equals(detail.Campus_Course__r.Course__r.Course_Category__c)){
							courseDuration = detail.Course_Duration__c != null ? detail.Course_Duration__c : 0;
							extraDuration = detail.Course_Number_Extra_Weeks__c != null ? detail.Course_Number_Extra_Weeks__c : 0;
							freeDuration = detail.Course_Number_Free_Weeks__c != null ? detail.Course_Number_Free_Weeks__c : 0;
							totalCourse = courseDuration + extraDuration + freeDuration;
							contactRelated.courseDuration = totalCourse.intValue();
							contactRelated.courseDurationUnit = detail.Course_Unit_Type__c;
							contactRelated.courseDurationStr = contactRelated.courseDuration + ' ' + contactRelated.courseDurationUnit;
					}else{
							contactRelated.courseDurationStr = '-';
					}


					campusRelated.contacts.add(contactRelated);

				}
			}
			/*resultReport.sort();
			if(!resultReport.isEmpty()){
				List<GraphResult> graph = new List<GraphResult>();
				GraphResult item;
				for(ReportGraphSchools schoolRep : resultReport){
					item = new GraphResult();
					item.schoolName = schoolRep.schoolName;
					item.totalQuotations = schoolRep.totalQuotations;
					graph.add(item);
				}
				graphResult = JSON.serialize(graph);
			}*/
		}catch(Exception e){
			system.debug('Error on generateReport() ===> ' + e.getLineNumber() + ' '+e.getMessage());	
		}
	}

	public void updateAgencies(){
		checkAttributes();
		
		String idGroup = ApexPages.currentPage().getParameters().get('idGroup');
		this.agencies = loadAgencies(user.Contact.AccountId, idGroup);
		
		agencySelected = '';
		employeeSelected = '';
		this.employees = loadEmployees(null);		
	}

	public void checkAttributes(){
		if(this.agencySelected == null){
			this.agencySelected = '';
		}
		if(this.employeeSelected == null){
			this.employeeSelected = '';
		}
		if(this.countrySelected == null){
			this.countrySelected = '';
		}
		if(this.schoolSelected == null){
			this.schoolSelected = '';
		}
		if(this.campusSelected == null){
			this.campusSelected = '';
		}
	}

	public void updateEmployees(){
		checkAttributes();
		employeeSelected = '';
		String idAgency = ApexPages.currentPage().getParameters().get('idAgency');
		this.employees = loadEmployees(idAgency);		
	}

	public void updateSchools(){
		checkAttributes();
		this.schoolSelected = '';
		this.campusSelected = '';
		String country = ApexPages.currentPage().getParameters().get('country');
		this.schools = new MethodsWithoutSharing().loadSchoolsByCountry(country, agencySelected, groupSelected);
		this.campuses = new MethodsWithoutSharing().loadCampuses(null);
	}
	
	public void updateCampuses(){
		checkAttributes();
		employeeSelected = '';
		String idSchool = ApexPages.currentPage().getParameters().get('idSchool');
		this.campuses = new MethodsWithoutSharing().loadCampuses(idSchool);
		this.campusSelected = '';	
	}

	public List<SelectOption> loadCampuses(String schoolID){
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('','-All Campuses-'));
		return options;
	}

	public List<SelectOption> loadAgencies(String agencyID, String groupID){		
		Set<String> idsAllAgencies = new Set<String>();
		for(SelectOption option : IPFunctions.getAgencyOptions(agencyID, groupID, Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible())){
			idsAllAgencies.add(option.getValue());
			//
		}
		List<SelectOption> options = new List<SelectOption>();
		for(Account option : [Select Id, Name from Account where id in :idsAllAgencies AND Inactive__c = false ORDER BY name]){
			options.add(new SelectOption(option.ID, option.Name));
		}

		if(options.size() > 1){
			idAgencies = new Set<String>();
			for(SelectOption option : options){
				idAgencies.add(option.getValue());
			}
			options.add(0, new SelectOption('', 'All Available Agencies'));
		}
		return options;
	}

	public List<SelectOption> loadEmployees(String agencyID){
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('','All Employees'));
		if(!String.isEmpty(agencyID) && agencyID != 'all'){
			for(SelectOption option : IPFunctions.getEmployeesByAgency(agencyID)){
				options.add(new SelectOption(option.getValue(), option.getLabel()));
			}
		}
		return options;
	}

	public List<SelectOption> getGroups(){
		Set<String> idsAllGroups = new Set<String>();
		for(SelectOption option : IPFunctions.getAgencyGroupsByPermission(user.Contact.AccountId, Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())){
			idsAllGroups.add(option.getValue());
			//
		}
		List<SelectOption> options = new List<SelectOption>();
		for(Account option : [Select Id, Name from Account where id in :idsAllGroups AND Inactive__c = false order by name]){
			options.add(new SelectOption(option.ID, option.Name));
		}
		
		return options;
	}

	public class GraphResult{
		public String schoolName{get;set;}
		public Integer totalQuotations{get;set;}
	}

	public class ReportResult{
		public String country{get;set;}
		public Integer totalPerCountry{get;set;}

		public List<SchoolReport> schools{get;set;}
	
		public ReportResult(){}
		public ReportResult(String country){
			this.country = country;
		}

		public Boolean equals(Object obj) {
			if (obj instanceof ReportResult) {
				ReportResult cs = (ReportResult)obj;
				if(country != null){
					return country.equals(cs.country);
				}
				return false;
			}
			return false;
		}
		public Integer hashCode() {
			return country.hashCode();
		}

		public Integer compareTo(Object compareTo) {
			ReportResult toCompare = (ReportResult)compareTo;
			return this.country.compareTo(toCompare.country);
		}
	}

	public class SchoolReport{
		public String schoolID{get;set;}
		public String schoolName{get;set;}
		public Integer totalPerSchool{get;set;}

		public List<CampusReport> campuses{get;set;}
		
		public SchoolReport(){}
		public SchoolReport(String schoolID, String schoolName){
			this.schoolID = schoolID;
			this.schoolName = schoolName;
		}

		public Boolean equals(Object obj) {
			if (obj instanceof SchoolReport) {
				SchoolReport cs = (SchoolReport)obj;
				if(schoolID != null){
					return schoolID.equals(cs.schoolID);
				}
				return false;
			}
			return false;
		}
		public Integer hashCode() {
			return schoolID.hashCode();
		}

		public Integer compareTo(Object compareTo) {
			SchoolReport toCompare = (SchoolReport)compareTo;

			if(this.totalPerSchool > toCompare.totalPerSchool){
				return -1;
			}else if(this.totalPerSchool < toCompare.totalPerSchool){
				return 1;
			}else{
			}
			return this.schoolName.compareTo(toCompare.schoolName);
		}

	}

	public class CampusReport{
		public String campusID{get;set;}
		public String campusName{get;set;}
		public Integer totalPerCampus{get;set;}

		public List<ContactGraph> contacts{get;set;}

		public CampusReport(){}
		public CampusReport(String campusID, String campusName){
			this.campusID = campusID;
			this.campusName = campusName;
		}

		public Boolean equals(Object obj) {
			if (obj instanceof CampusReport) {
				CampusReport cs = (CampusReport)obj;
				if(campusID != null){
					return campusID.equals(cs.campusID);
				}
				return false;
			}
			return false;
		}
		public Integer hashCode() {
			return campusID.hashCode();
		}

		public Integer compareTo(Object compareTo) {
			CampusReport toCompare = (CampusReport)compareTo;

			if(this.totalPerCampus > toCompare.totalPerCampus){
				return -1;
			}else if(this.totalPerCampus < toCompare.totalPerCampus){
				return 1;
			}else{
			}
			return this.campusName.compareTo(toCompare.campusName);
		}

	}

	public class ContactGraph{
		public String contactID{get;set;}
		public String contactName{get;set;}
		public String contactEmail{get;set;}
		public String contactOwner{get;set;}
		public String contactAgency{get;set;}
		public String contactGroup{get;set;}
		public String campus{get;set;}
		public String course{get;set;}
		public String quotationID{get;set;}
		public String quotation{get;set;}
		public String quotationCreatedBy{get;set;}
		public String quotationCreatedAgency{get;set;}
		public String courseDurationStr{get;set;}
		public Integer courseDuration{get;set;}
		public Integer numberOfTimesOpened{get;set;}
		public String courseDurationUnit{get;set;}
		public Datetime quotationCreatedDate{get;set;}

		public ContactGraph(){}
	}
	
	public without sharing class MethodsWithoutSharing{
		
		public List<Quotation_Detail__c> generateReport(String beginDate, String endDate, String groupSelected, String agencySelected, String employeeSelected, String schoolSelected, String campusSelected, String countrySelected, Boolean isExcelExport){
			Date fromDate = String.isEmpty(beginDate) ? null : Date.parse(beginDate);
			Date toDate = String.isEmpty(endDate) ? null : Date.parse(endDate);

			String query = 'SELECT Campus_Country__c, Quotation__r.Quotation_Total_Counter__c, Quotation__r.Name, Quotation__r.ID, Quotation__r.CreatedBy.Name, Quotation__r.CreatedBy.Account.Name, Quotation__r.CreatedDate, Quotation__r.Client__r.ID, Quotation__r.Client__r.Name, Quotation__r.Client__r.Email, Quotation__r.Client__r.Owner__r.Name, Quotation__r.Client__r.Owner__r.Account.Name, Campus_Course__r.Course__r.Name, Campus_Course__r.Campus__r.Name, Campus_Course__r.Course__r.Course_Category__c, Campus_Course__r.Campus__r.ID, Campus_Course__r.Campus__r.Parent.Name, Campus_Course__r.Campus__r.Parent.ID, Quotation__r.Agency__r.Parent.Name, Course_Duration__c, Course_Unit_Type__c, Course_Number_Extra_Weeks__c, Course_Number_Free_Weeks__c FROM Quotation_Detail__c WHERE Quotation__r.Agency__r.Parent.ID = :groupSelected AND Campus_Course__r.Campus__r.Parent.ID != null ';

			if(!String.isEmpty(agencySelected)){
				query = query + ' AND Quotation__r.Agency__r.ID = :agencySelected ';
			}
			if(!String.isEmpty(employeeSelected)){
				query = query + ' AND Quotation__r.Client__r.Owner__r.Contact.ID = :employeeSelected ';
			}
			if(!String.isEmpty(countrySelected)){
				query = query + ' AND Campus_Country__c = :countrySelected ';
			}
			if(!String.isEmpty(schoolSelected)){
				query = query + ' AND Campus_Course__r.Campus__r.Parent.ID = :schoolSelected ';
			}
			if(!String.isEmpty(campusSelected)){
				query = query + ' AND Campus_Course__r.Campus__r.ID = :campusSelected ';
			}
			if(fromDate != null){
				query = query + ' AND Quotation__r.CreatedDate >= :fromDate '; 
			}
			if(toDate != null){
				query = query + ' AND Quotation__r.CreatedDate <= :toDate ';
			}
			query = query + ' ORDER BY Campus_Course__r.Campus__r.Name, Quotation__r.Client__r.Name ';
			query = isExcelExport ? query : query + ' LIMIT '+report_leads_per_quotation.SIZE_LIMIT;

			return Database.query(query);
		}

		public List<SelectOption> loadSchoolsByCountry(String country, String agency, String agencyGroup){
			List<SelectOption> schoolsOptions = new List<SelectOption>();
			schoolsOptions.add(new SelectOption('','All Schools'));
			
			if(!String.isEmpty(country)){
				String query;
				if(!String.isEmpty(agency)){
					query = 'Select Campus_Course__r.Campus__c, Campus_Country__c FROM Quotation_Detail__c WHERE Campus_Country__c = :country AND Quotation__r.Agency__c = :agency';
				}else{
					query = 'Select Campus_Course__r.Campus__c, Campus_Country__c FROM Quotation_Detail__c WHERE Campus_Country__c = :country AND Quotation__r.Agency__r.Parent.ID = :agencyGroup';
				}
				List<Quotation_Detail__c> quotations = Database.query(query);
				Set<String> ids = new Set<String>();
				for(Quotation_Detail__c qd : quotations){
					ids.add(qd.Campus_Course__r.Campus__c);
				}
				
				
				List<Account> schools = [SELECT Parent.ID, Parent.Name FROM Account WHERE ID in :ids AND Parent.RecordType.Name = 'School' ORDER BY Parent.Name];
				ids = new Set<String>();
				for(Account school : schools){
					if(!String.isEmpty(school.Parent.ID) && !ids.contains(school.Parent.ID)){
						schoolsOptions.add(new SelectOption(school.Parent.ID, school.Parent.Name));
					}
					ids.add(school.Parent.ID);
				}
			}
			return schoolsOptions;
		}

		public List<SelectOption> loadCampuses(String school){
			List<SelectOption> options = new List<SelectOption>();
			options.add(new SelectOption('','All Campuses'));
			if(!String.isEmpty(school)){
				List<Account> campuses = [SELECT ID, Name FROM Account WHERE RecordType.Name = 'Campus' AND Parent.ID = :school ORDER BY Name ASC];
				for(Account campus : campuses){
					options.add(new SelectOption(campus.ID, campus.Name));
				}
			}
			return options;
		}
		public List<SelectOption> loadCountries(String agency, String agencyGroup){
			List<String> countries = new List<String>();
			String query = '';
			if(!String.isEmpty(agency)){
				query = 'SELECT ID, (Select Campus_Country__c FROM Quotation_Courses__r) From Quotation__c WHERE Agency__c = :agency';
			}else{
				query = 'SELECT ID, (Select Campus_Country__c FROM Quotation_Courses__r) From Quotation__c WHERE Agency__r.Parent.ID = :agencyGroup';
			}
			List<Quotation__c> quotations = Database.query(query);
			for(Quotation__c quote : quotations){
				for(Quotation_Detail__c qd : quote.Quotation_Courses__r){
					if(!String.isEmpty(qd.Campus_Country__c) && !countries.contains(qd.Campus_Country__c)){
						countries.add(qd.Campus_Country__c);
					}
				}
			}
			countries.sort();
			List<SelectOption> countriesOptions = new List<SelectOption>();
			countriesOptions.add(new SelectOption('','All Countries'));
			for(String country : countries){
				countriesOptions.add(new SelectOption(country, country));	
			}
			return countriesOptions;
		}

		/*public Map<String, List<SelectOption>> loadSchools(String agency, String agencyGroup){
			Map<String, List<SelectOption>> result = new Map<String, List<SelectOption>>();
			String query = '';
			if(!String.isEmpty(agency)){
				query = 'SELECT ID, (Select Campus_Course__r.Campus__c, Campus_Country__c FROM Quotation_Courses__r) From Quotation__c WHERE Agency__c = :agency';
			}else{
				query = 'SELECT ID, (Select Campus_Course__r.Campus__c, Campus_Country__c FROM Quotation_Courses__r) From Quotation__c WHERE Agency__r.Parent.ID = :agencyGroup';
			}
			List<Quotation__c> quotations = Database.query(query);
			Set<String> idsSchools = new Set<String>();
			List<String> countries = new List<String>();
			for(Quotation__c quote : quotations){
				for(Quotation_Detail__c qd : quote.Quotation_Courses__r){
					if(!String.isEmpty(qd.Campus_Course__r.Campus__c) && !idsSchools.contains(qd.Campus_Course__r.Campus__c)){
						idsSchools.add(qd.Campus_Course__r.Campus__c);
					}
					if(!String.isEmpty(qd.Campus_Country__c) && !countries.contains(qd.Campus_Country__c)){
						countries.add(qd.Campus_Country__c);
					}
				}
			}
			countries.sort();
			List<SelectOption> countriesOptions = new List<SelectOption>();
			countriesOptions.add(new SelectOption('','All Countries'));
			for(String country : countries){
				countriesOptions.add(new SelectOption(country, country));	
			}

			list<Account> schools = [SELECT Parent.ID, Parent.Name FROM Account WHERE ID in :idsSchools AND Parent.RecordType.Name = 'School' ORDER BY Parent.Name];
			idsSchools = new Set<String>();
			List<SelectOption> schoolsOptions = new List<SelectOption>();
			schoolsOptions.add(new SelectOption('','All Schools'));
			for(Account school : schools){
				if(!String.isEmpty(school.Parent.ID) && !idsSchools.contains(school.Parent.ID)){
					idsSchools.add(school.Parent.ID);
					schoolsOptions.add(new SelectOption(school.Parent.ID, school.Parent.Name));
				}
			}
			result.put('schools', schoolsOptions);
			result.put('countries', countriesOptions);
			return result;
		}*/
	}
}