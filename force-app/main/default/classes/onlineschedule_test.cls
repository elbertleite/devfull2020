@isTest
private class onlineschedule_test {
   
    static testMethod void testBooking() {
        TestFactory tf = new TestFactory();
        account agency = tf.createAgency();
        contact employee = tf.createEmployee(agency);
        Department__c dep = tf.createDepartment(agency);
        employee.Department__c = dep.id;
        update employee;
        User u = tf.createPortalUser(employee);

        dep.Allow_Booking__c = true;
        update dep;
        Client_Booking__c cb = new Client_Booking__c(Date_Booking__c = System.today(), Time_Booking__c = Time.newInstance(13,0,0,0));

            Test.startTest();
            ApexPages.currentPage().getParameters().put('ag',agency.id);
            onlineschedule book = new onlineschedule();
            book.selectedServices = new list<string>{'Job','Visa'};
             ApexPages.currentPage().getParameters().put('bookId',u.id+'#13:00');
            ApexPages.currentPage().getParameters().put('lstServices',JSON.Serialize(book.selectedServices));
            ApexPages.currentPage().getParameters().put('sendEmail','true');
            book.saveBooking();

            ApexPages.currentPage().getParameters().put('selAgency', agency.id);
            book.changeDepartment();

            ApexPages.currentPage().getParameters().put('bookId',u.id+'#13:00');
            book.retrieveBookDetails();
       
    }
}