/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class extrafees_maintenance_test {

    static testMethod void myUnitTest() {
    	
       
       	Map<String,String> recordTypes = new Map<String,String>();
  		for(RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
		
		
		Account school = new Account();
		school.recordtypeid = recordTypes.get('School');
		school.name = 'Test School';				
		insert school;
		
		Nationality_Group__c ng = new Nationality_Group__c();
		ng.Account__c = school.id;
		ng.Country__c = 'Brazil';
		ng.Name = 'Latin America';
		insert ng;
		
		Nationality_Group__c ng2 = new Nationality_Group__c();
		ng2.Account__c = school.id;
		ng2.Country__c = 'Czech Republic';
		ng2.Name = 'Central and Eastern Europe';
		insert ng2;
		
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;		
		insert p;
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Homestay Single';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'OSHC Single';
		p3.Product_Type__c = 'Insurance';
		p3.Supplier__c = school.id;
		insert p3;
		
		Product__c p4 = new Product__c();
		p4.Name__c = 'OSHC Dual Family';
		p4.Product_Type__c = 'Insurance';
		p4.Supplier__c = school.id;
		insert p4;
		
		
		Account campus = new Account();
		campus.RecordTypeId = recordTypes.get('Campus');
		campus.Name = 'Test Campus CBD';
		campus.BillingCountry = 'Australia';
		campus.BillingCity = 'Sydney';
		campus.ParentId = school.id;
		insert campus;
        
		
		Course__c course = new Course__c();
		course.Name = 'Certificate III in Business';        
		course.Type__c = 'Business';
		course.Sub_Type__c = 'Business';
		course.Course_Qualification__c = 'Certificate III';
		course.Course_Type__c = 'VET';
		course.Course_Unit_Type__c = 'Week';
		course.Only_Sold_in_Blocks__c = true;
		course.Period__c = 'Morning';
		insert course;
       
		Campus_Course__c cc = new Campus_Course__c();
		cc.Course__c = course.Id;
		cc.Campus__c = campus.Id;        
		cc.Is_Available__c = true;        
		insert cc;
		
		
		Account_Document_File__c folder = new Account_Document_File__c();
		folder.File_Name__c = 'The Folder';
		folder.Parent__c = campus.id;
		folder.Content_Type__c = 'Folder';
		insert folder;
		
		Account_Document_File__c adf = new Account_Document_File__c();
		adf.File_Name__c = 'Campus File';
		adf.Parent__c = campus.id;
		adf.Parent_Folder_Id__c = folder.id;
		insert adf;
		
		
		
		Course_Extra_Fee__c cef = new Course_Extra_Fee__c();
		cef.Campus_Course__c = cc.Id;
		cef.Nationality__c = 'Brazil';
		cef.From__c = 1;
		cef.Account_Document_File__c = adf.id;
		cef.Value__c = 100;		
		cef.Product__c = p.id;
		cef.date_paid_from__c = system.today();
		cef.date_paid_to__c = system.today().addDays(60);
		cef.date_start_from__c = system.today();
		cef.date_start_to__c = system.today().addDays(60);
		cef.Allow_change_units__c = true;
		cef.ExtraFeeInterval__c = 'Week';
		insert cef;
		
		Course_Extra_Fee_Dependent__c cefd = new Course_Extra_Fee_Dependent__c();
		cefd.Course_Extra_Fee__c = cef.id;
		cefd.Product__c = p2.id;
		cefd.Details__c = 'Testing';
		cefd.date_paid_from__c = system.today();
		cefd.date_paid_to__c = system.today().addDays(60);
		cefd.date_start_from__c = system.today();
		cefd.date_start_to__c = system.today().addDays(60);
		cefd.From__c = 1;
		cefd.Value__c = 200;
		cefd.Allow_Change_Units__c = true;
		cefd.Extra_Fee_Interval__c = 'Week';
		insert cefd;
		
		cef = new Course_Extra_Fee__c();
		cef.Campus__c = campus.Id;
		cef.Nationality__c = 'Czech Republic';
		cef.Value__c = 199;
		cef.From__c = 1;
		cef.Product__c = p3.id;		
		cef.Optional__c = false;
		cef.date_paid_from__c = system.today();
		cef.date_paid_to__c = system.today().addDays(60);
		cef.date_start_from__c = system.today();
		cef.date_start_to__c = system.today().addDays(60);
		insert cef;
		
		
		Test.startTest();
		
		
		Apexpages.Standardcontroller controller = new ApexPages.Standardcontroller(campus);
		extrafees_maintenance em = new extrafees_maintenance(controller);
		
		String str = em.feeAction;
		em.getFeeActionOptions();
		
		str = em.feeOption;
		em.getFeeOptions();
		
		boolean b = em.CourseAvailable;
		em.getCourseAvailableList();
		
		List<String> lstr = em.period;
		em.getPeriods();
		
		em.selectFeeOption();		
		em.selectCourseClone();		
		em.selectCourseAvailable();
		
		List<String> lstring = em.selectedCourseType;
		em.getselectedCourseTypeOptions();
		
		lstr = em.selectedNationalities;
		em.getNationalities();
		em.getnationalityGroupHint();
		
		em.selectedNationalities = new List<String>{'Brazil', 'Czech Republic'};
		
		lstr = em.courseClone;
		em.getCourseListClone();
		
		em.selectFeesOption();
		
		str = em.selectedCategory;
		em.getCategoryOptions();
		
		str = em.selectedExtraFee;
		em.getExtraFeeOptions();
		
		em.refreshFees();
		em.refreshRelatedFees();
		
		str = em.selectedRelatedExtraFee;
		em.getRelatedExtraFeeOptions();
		
		b = em.showRequired;
		b = em.showRequiredFees;
		
		lstr = em.filterOptional;
		
		em.getFilterOptionalFeesOptions();
		
		em.getCourseFees();
		
		Course_Extra_Fee__c newcef = em.newCourseExtraFee;
		Start_Date_Range__c newRange = em.newExtraFeeRange;
		Course_Extra_Fee_Dependent__c newRelated = em.newRelatedFee;
		
		em.getschoolFees();
		
		lstr = em.filterAvailability;
		em.getPriceAvailabilityList();
		
		em.newCourseExtraFee.Product__c = p.id;
		em.refreshAddFeeFields();
		
		str = em.comments;
		
		List<SelectOption> lso = em.Files;
		
		b = em.editMode;
		
		em.searchExtraFees();		
		em.retrieveExtraFees();
		em.setEditMode();
		em.dateFilter = '31/12/2016';
		em.filterDates();
		
		Set<String> sstr = em.errorUpdate;
		sstr = em.errorUpdateRange;
		sstr = em.errorUpdateRelated;
		
		/* no extra fee selected */
		em.addStartDateRange();
		em.addRelatedFees();
		/* no extra fee selected */
		
		for(extrafees_maintenance.CampusCourseExtraFee ccef : em.campusCourseFees){
			for(Course_Extra_Fee__c cefa : ccef.fees){
				cefa.selectedFee__c = true;
				cefa.date_paid_from__c = system.today().addDays(1);
				
				for(Course_Extra_Fee_Dependent__c rel : cefa.course_Extra_fees_dependent__r){
					rel.Date_Start_From__c = system.today().addDays(1);
					rel.Selected__c = true;				
				}
				for(Course_Extra_Fee_Start_Date_Range__c range : cefa.Extra_Fee_Start_Date_Prices__r){
					range.From_Date__c = system.today().addDays(1);
					range.selected__c = true;
				}
			}
		}
		
		em.saveExtraFeesUpdate();
		
		
	
		
		/*FORCING ERRORS*/
		em.selectedNationalities = null;
		em.addExtraFee();
		
		em.selectedNationalities = new List<String>{'Brazil', 'Czech Republic'};
		em.newCourseExtraFee.Product__c = null;
		em.addExtraFee();
		
		em.newCourseExtraFee.Product__c = p.id;
		em.newCourseExtraFee.Value__c = -10;
		em.newCourseExtraFee.Combine_fees__c = null;
		em.addExtraFee();
		
		
		em.newCourseExtraFee.Value__c = 150;
		em.newCourseExtraFee.From__c = null;
		em.addExtraFee();
		
		em.newCourseExtraFee.From__c = -1;
		em.addExtraFee();
		
		em.newCourseExtraFee.From__c = 1;
		em.newCourseExtraFee.Value__c = null;
		em.addExtraFee();
		
		em.newCourseExtraFee.Value__c = -10;
		em.addExtraFee();
		
		em.newCourseExtraFee.Value__c = 150;
		em.newCourseExtraFee.date_paid_from__c = null;
		em.addExtraFee();
		
		em.newCourseExtraFee.date_paid_from__c = system.today();
		em.newCourseExtraFee.date_paid_to__c = null;
		em.addExtraFee();
		
		em.newCourseExtraFee.date_paid_to__c = system.today().addDays(-60);
		em.addExtraFee();
		
		em.newCourseExtraFee.date_paid_to__c = system.today().addDays(60);		
		/*FORCING ERRORS*/
		
		em.newCourseExtraFee.Availability__c = 3.0;
		em.newCourseExtraFee.ExtraFeeInterval__c = 'Week';
		
		em.addExtraFee();
		
		
		
		for(Course_Extra_Fee__c acef : em.getCourseFees())
			acef.selectedFee__c = true;
		
		
		/*FORCING ERRORS*/
		em.newExtraFeeRange.From_Date__c = null;
		em.addStartDateRange();
		
		em.newExtraFeeRange.From_Date__c = system.today();
		em.newExtraFeeRange.To_Date__c = null;
		em.addStartDateRange();
		
		em.newExtraFeeRange.To_Date__c = system.today().addMonths(3);
		em.newExtraFeeRange.Value__c = null;
		em.addStartDateRange();
		
		em.newExtraFeeRange.Value__c = -200;
		em.addStartDateRange();
		
		em.newExtraFeeRange.To_Date__c = system.today().addMonths(-5);
		em.newExtraFeeRange.Value__c = 200;
		em.addStartDateRange();			
		/*FORCING ERRORS*/
		
		
		em.newExtraFeeRange.To_Date__c = system.today().addMonths(3);
		em.addStartDateRange();
		em.addStartDateRange(); //date error
		
		
		
		for(Course_Extra_Fee__c acef : em.getCourseFees())
			acef.selectedFee__c = true;
		
		
		
		/*FORCING ERRORS*/
		em.newRelatedFee.Product__c = null;
		em.addRelatedFees();
		
		em.newRelatedFee.Product__c = p.id;
		em.newRelatedFee.From__c = null;
		em.addRelatedFees();
		
		em.newRelatedFee.From__c = 1;
		em.newRelatedFee.Value__c = null;
		em.addRelatedFees();
		
		em.newRelatedFee.From__c = -1;
		em.newRelatedFee.Value__c = 100;
		em.addRelatedFees();
		
		em.newRelatedFee.From__c = 1;
		em.newRelatedFee.Value__c = -100;
		em.addRelatedFees();
		
		em.newRelatedFee.Value__c = 100;
		em.newRelatedFee.Date_Paid_From__c = null;
		em.addRelatedFees();
		
		em.newRelatedFee.Date_Paid_From__c = system.today();
		em.newRelatedFee.Date_Paid_To__c = null;		
		em.addRelatedFees();
		
		em.newRelatedFee.Date_Paid_To__c = system.today().addMonths(-3);
		em.addRelatedFees();
		
		em.newRelatedFee.Allow_Change_Units__c = true;
		em.newRelatedFee.Extra_Fee_Interval__c = null;
		em.newRelatedFee.Date_Paid_To__c = system.today().addMonths(3);
		em.addRelatedFees();
		/*FORCING ERRORS*/
		
		em.newRelatedFee.Allow_Change_Units__c = false;
		em.addRelatedFees();
		em.addRelatedFees();//force date error
		
		
		
		
		
				
		em.deleteExtraFees();		
		em.cancel();
		
		lstr = em.selectedExtraFeeName;
		em.getExtraFeeName();
		
		em.showGrouped = true;
		em.getGroupedCampusCourseExtraFees();
		
		str = em.listFeesView;
		em.getlistFeesViewOptions();
		
		
		
		em.listFeesView = '2';
		em.retrieveExtraFees();
		
		
		em.feeOption = '2';
		em.selectedNationalities = new List<String>{'Published Price'};
		em.courseClone = new List<String>{cc.id};
		em.addExtraFee();
		
		
		
		
		
		
		
		
		
		
		
		
		
		Test.stopTest();
		
    }
    
    
    
    
    
    
    static testMethod void myUnitTest2() {
    	
       
       	Map<String,String> recordTypes = new Map<String,String>();
  		for(RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
		
		
		Account school = new Account();
		school.recordtypeid = recordTypes.get('School');
		school.name = 'Test School';				
		insert school;
		
		Nationality_Group__c ng = new Nationality_Group__c();
		ng.Account__c = school.id;
		ng.Country__c = 'Brazil';
		ng.Name = 'Latin America';
		insert ng;
		
		Nationality_Group__c ng2 = new Nationality_Group__c();
		ng2.Account__c = school.id;
		ng2.Country__c = 'Czech Republic';
		ng2.Name = 'Europe';
		insert ng2;
		
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Homestay Single';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'OSHC Single';
		p3.Product_Type__c = 'Insurance';
		p3.Supplier__c = school.id;
		insert p3;
		
		Product__c p4 = new Product__c();
		p4.Name__c = 'OSHC Dual Family';
		p4.Product_Type__c = 'Insurance';
		p4.Supplier__c = school.id;
		insert p4;
		
		
		Account campus = new Account();
		campus.RecordTypeId = recordTypes.get('Campus');
		campus.Name = 'Test Campus CBD';
		campus.BillingCountry = 'Australia';
		campus.BillingCity = 'Sydney';
		campus.ParentId = school.id;
		insert campus;
        
		
		Course__c course = new Course__c();
		course.Name = 'Certificate III in Business';        
		course.Type__c = 'Business';
		course.Sub_Type__c = 'Business';
		course.Course_Qualification__c = 'Certificate III';
		course.Course_Type__c = 'VET';
		course.Course_Unit_Type__c = 'Week';
		course.Only_Sold_in_Blocks__c = true;
		course.Period__c = 'Morning';
		insert course;
       
		Campus_Course__c cc = new Campus_Course__c();
		cc.Course__c = course.Id;
		cc.Campus__c = campus.Id;        
		cc.Is_Available__c = true;        
		insert cc;
		
		
		Account_Document_File__c folder = new Account_Document_File__c();
		folder.File_Name__c = 'The Folder';
		folder.Parent__c = campus.id;
		folder.Content_Type__c = 'Folder';
		insert folder;
		
		Account_Document_File__c adf = new Account_Document_File__c();
		adf.File_Name__c = 'Campus File';
		adf.Parent__c = campus.id;
		adf.Parent_Folder_Id__c = folder.id;
		insert adf;
		
		
		
		Course_Extra_Fee__c cef = new Course_Extra_Fee__c();
		cef.Campus_Course__c = cc.Id;
		cef.Nationality__c = 'Latin America';
		cef.From__c = 1;
		cef.Account_Document_File__c = adf.id;
		cef.Value__c = 100;		
		cef.Product__c = p.id;
		cef.date_paid_from__c = system.today();
		cef.date_paid_to__c = system.today().addDays(60);
		cef.date_start_from__c = system.today();
		cef.date_start_to__c = system.today().addDays(60);
		cef.Allow_change_units__c = true;
		cef.ExtraFeeInterval__c = 'Week';
		insert cef;
		
		Course_Extra_Fee_Dependent__c cefd = new Course_Extra_Fee_Dependent__c();
		cefd.Course_Extra_Fee__c = cef.id;
		cefd.Product__c = p2.id;
		cefd.Details__c = 'Testing';
		cefd.date_paid_from__c = system.today();
		cefd.date_paid_to__c = system.today().addDays(60);
		cefd.date_start_from__c = system.today();
		cefd.date_start_to__c = system.today().addDays(60);
		cefd.From__c = 1;
		cefd.Value__c = 200;
		cefd.Allow_Change_Units__c = true;
		cefd.Extra_Fee_Interval__c = 'Week';
		insert cefd;
		
		cef = new Course_Extra_Fee__c();
		cef.Campus__c = campus.Id;
		cef.Nationality__c = 'Czech Republic';
		cef.Value__c = 199;
		cef.From__c = 1;
		cef.Product__c = p3.id;		
		cef.Optional__c = false;
		cef.date_paid_from__c = system.today();
		cef.date_paid_to__c = system.today().addDays(60);
		cef.date_start_from__c = system.today();
		cef.date_start_to__c = system.today().addDays(60);
		insert cef;
		
		TestFactory testFactory = new TestFactory();
		
		cef = testFactory.createCourseExtraFee(cc, 'Latin America', p3);
		cef = testFactory.createCourseExtraFeeCombined(cc, 'Published Price', p, p2);
		
		
		Test.startTest();
		
		
		Apexpages.Standardcontroller controller = new ApexPages.Standardcontroller(campus);
		extrafees_maintenance em = new extrafees_maintenance(controller);
		
		String str = em.feeAction;
		em.getFeeActionOptions();
		
		str = em.feeOption;
		em.getFeeOptions();
		
		boolean b = em.CourseAvailable;
		em.getCourseAvailableList();
		
		List<String> strList = em.period;
		em.getPeriods();
		
		em.selectFeeOption();		
		em.selectCourseClone();		
		em.selectCourseAvailable();
		
		List<String> lstr = em.selectedCourseType;
		em.getselectedCourseTypeOptions();
		
		lstr = em.selectedNationalities;
		em.getNationalities();
		em.getnationalityGroupHint();
		
		em.selectedNationalities = new List<String>{'Latin America', 'Europe', 'Published Price'};
		
		lstr = em.courseClone;
		em.period = new List<String>{course.Period__c};
		em.selectedRelatedExtraFee = cefd.Product__c;		
		em.getCourseListClone();
		
		
		em.selectedRelatedExtraFee = null;
		em.selectedExtraFee = cef.id;
		em.getCourseListClone();
		
		em.selectedRelatedExtraFee = null;
		em.selectedExtraFee = null;
		em.feeAction = 'new';
		em.feeOption = '2';
		em.selectedCategory = 'Other';
		em.getCourseListClone();
    	
    	
    	em.refreshGrouped();
    	em.refreshFeeNames();
    	
    	em.getschoolFees();
    	em.refreshAddRelatedFeeFields();
    	
    	em.saveExtraFeesUpdate();
    	
    	
    	em.showGrouped = true;
    	em.getGroupedCampusCourseExtraFees();
    	em.saveExtraFeesUpdate();
    	
    	
    	
    	Test.stopTest();
    
    }
    
    
    
    
    
    
    
    
    
}