public class extPageDispatcherEdit{
	
	private Map<String,String> accountRecordTypes; //Maps all record types, <Name,id>
	
	string ObjId;
	
	public extPageDispatcherEdit() {	
		
	}
	public extPageDispatcherEdit(ApexPages.StandardController controller) {		
		ObjId = controller.getId();

		accountRecordTypes = new Map<String,String>{};
		for(RecordType rt: [Select Name, Id From RecordType where sObjectType IN ('Account', 'Contact') and isActive=true])
		accountRecordTypes.put(rt.Name,rt.Id);
		
	}
 
	public PageReference getRedir() {
		
		PageReference newPage;
		
		String profileName = [Select Name from Profile where Id = :UserInfo.getProfileId() LIMIT 1].Name;
		Set<String> profiles = new Set<String>(new List<String>{'System Administrator', 'EHF System Administrator','EHF Support Officer'});
		if(profiles.contains(profileName)){	
			//e?id=003N000000IolJ2&nooverride=1&retURL=/003N000000IolJ2?id=003N000000IolJ2&nooverride=1	
			newPage = new PageReference('/' + ObjId+ '/e?id='+ObjId+ '&nooverride=1&retURL=/' + ObjId + '?id='+ ObjId + '&nooverride=1');
			if(!Test.isRunningTest()){
				return newPage.setRedirect(true);
			}
		}	
		
		if(Test.isRunningTest()){
			ObjId = ApexPages.currentPage().getParameters().get('id');
		}
 
 		string sub = String.valueOf(ObjId).substring(0, 3);
 		string recordTypeName;
 		
 		
 		if(sub == '001')
			recordTypeName = [Select id, recordtype.name From Account Where Id = :ObjId].recordtype.name;
		else recordTypeName = [Select id, recordtype.name From Contact Where Id = :ObjId].recordtype.name;
		
 
		if (recordTypeName == 'Agency'){
			newPage = Page.agency_details;
		} else if(recordTypeName == 'School'){
			ObjId = [Select id from Account where parentId = :ObjId order by name limit 1].id;
			newPage = Page.ShowCaseCampus;
		} else if(recordTypeName == 'Campus'){
			newPage = Page.ShowCaseCampus;		
		} else if(recordTypeName == 'Client'){
			newPage = Page.activity_log;
		} else if(recordTypeName == 'Lead'){
			newPage = Page.activity_log;
		} else if(recordTypeName == 'Employee'){
			newPage = Page.contact_overview;
		} else {
			newPage = new PageReference('/' + ObjId);
			newPage.getParameters().put('nooverride', '1');
		}
		
		newPage.getParameters().put('id', ObjId);
		return newPage.setRedirect(true);
 
	}
 
	


}