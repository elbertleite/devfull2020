public without sharing class sales_dashboard {
    
    public boolean hasAccess{get;set;}
    public sales_dashboard() {
        string contactId = [Select ContactId from user where id = :UserInfo.getUserId() limit 1].ContactId;
	    hasAccess = [select Sales_Dashboard_Permission__c from Contact where id = :contactId].Sales_Dashboard_Permission__c;
    }

    @ReadOnly
    @RemoteAction
    public static Map<String, Object> retrieveGroupSalesInfo(String fromDate, String toDate, string agencyType){
        User userLogged = [SELECT ID, Contact.AccountId, Contact.Account.ParentId, Contact.Account.Parent.Name FROM User WHERE ID = :UserInfo.getUserId()];
        
        SalesResultPerAgency ag;
        List<SalesResultPerAgency> result;
        Map<String, Object> response = new Map<String, Object>();
        Map<String, SalesResultPerAgency> itens = new Map<String, SalesResultPerAgency>();

        Set<String> idAgencies = new Set<String>(); 
        for(SelectOption option : IPFunctions.getAgencyOptions(userLogged.Contact.AccountId, userLogged.Contact.Account.ParentId, Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible())){
			idAgencies.add(option.getValue());
		}  
        
        for(Account agency : [SELECT ID, Name, Agency_Sales_Target__c FROM Account WHERE RecordType.Name = 'Agency' AND ID IN :idAgencies AND Inactive__c = false ORDER BY Name]){
            itens.put(agency.ID, new SalesResultPerAgency(agency.ID, agency.Name, agency.Agency_Sales_Target__c));
        }

        //Set<String> idAgencies = itens.keySet();
        Set<String> idContacts = new Set<String>();
        String idGroup = userLogged.Contact.Account.ParentId;

        Date fromDateQuery = IPFunctions.getDateFromString(fromDate);
        Date toDateQuery = IPFunctions.getDateFromString(toDate);
        string sql = 'SELECT ID, NAME, CreatedBy.UserType, Original_Agency__c, Current_Agency__c FROM Contact WHERE ';
        if(agencyType == 'original')
            sql += ' Original_Agency__c IN :idAgencies ';
        else sql += ' current_Agency__c IN :idAgencies ';
        sql += ' AND DAY_ONLY(convertTimezone(CreatedDate)) >= :fromDateQuery AND DAY_ONLY(convertTimezone(CreatedDate)) <= :toDateQuery';
        system.System.debug('==>sql: '+sql);
        system.System.debug('==>agencyType: '+agencyType);
        for(Contact ctt : Database.Query(sql)){
            idContacts.add(ctt.ID);
            if(agencyType == 'original')
                ag = itens.get(ctt.Original_Agency__c);
            else ag = itens.get(ctt.Current_Agency__c);
            if(ag != null){
                if(ctt.CreatedBy.UserType != 'CspLitePortal'){
                    ag.newLeadOnline += 1;
                }else{
                    ag.newLeadOffline += 1;
                }
                ag.totalSales += 1;
            }
        }

        Set<String> idContactsPerCycle = new Set<String>();
        string sqlCycle = 'SELECT ID, Client__c, Client__r.CreatedBy.UserType, Created_By_Agency__c, CreatedBy.Contact.Account.ID FROM Destination_Tracking__c WHERE Agency_Group__c = :idGroup AND Client__c NOT IN :idContacts ';
        if(agencyType == 'original')
            sqlCycle += '  AND Created_By_Agency__c IN :idAgencies ';
        else sqlCycle += '  AND Current_Agency__c IN :idAgencies ';
        sqlCycle += ' AND DAY_ONLY(convertTimezone(CreatedDate)) >= :fromDateQuery AND DAY_ONLY(convertTimezone(CreatedDate)) <= :toDateQuery';
        List<Destination_Tracking__c> cycles = Database.Query(sqlCycle);
        for(Destination_Tracking__c cycle : cycles){
            if(!idContactsPerCycle.contains(cycle.Client__c)){
                ag = itens.get(cycle.Created_By_Agency__c);
                if(ag != null){
                    ag.newCycles += 1;
                    ag.totalSales += 1;
                }
            }
            idContactsPerCycle.add(cycle.Client__c);
        }

        result = itens.values();
        result.sort();
        
        response.put('response', result);
        response.put('agencyGroup', userLogged.Contact.Account.Parent.Name);
        
        return response;
    }

    public class SalesResultPerAgency implements Comparable{
        public String id{get;set;}
        public String name{get;set;}
        public Long salesTarget{get;set;}
        public Long newLeadOnline{get;set;}
        public Long newLeadOffline{get;set;}
        public Long newCycles{get;set;}
        public Long totalSales{get;set;}

        public SalesResultPerAgency(String id, String name, Decimal salesTarget){
            this.id = id;
            this.name = name;
            this.salesTarget = salesTarget != null ? salesTarget.longValue() : 0;
            this.newLeadOnline = 0;
            this.newLeadOffline = 0;
            this.newCycles = 0;
            this.totalSales = 0;
        }

        public Integer compareTo(Object compareTo) {
			SalesResultPerAgency var2 = (SalesResultPerAgency)compareTo;
			return this.name.compareTo(var2.name);
		}
    }
}