/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class API_v1_schoolProducts_test {

    static testMethod void myUnitTest() {
        
        TestFactory tf = new TestFactory();
       
		Account agency = tf.createAgency();
		
		Account school = tf.createSchool();
        
        Account campus = tf.createCampus(school, agency);
		    
		Course__c course = tf.createCourse();
				
		Campus_Course__c cc = tf.createCampusCourse(campus, course);
		
		Product__c p = new Product__c();
		p.Name__c = 'Elbert\'s Homestay';
		p.Product_Type__c = 'Accommodation';
		p.Supplier__c = school.id;
		p.Website_Fee_Type__c = 'Homestay';
		insert p;
		
		Course_Extra_Fee__c cef2 = new Course_Extra_Fee__c();
		cef2.Campus_Course__c = cc.Id;
		cef2.Nationality__c = 'Published Price';
		cef2.Availability__c = 3;
		cef2.From__c = 1;
		cef2.Value__c = 275;		
		cef2.Optional__c = true;
		cef2.Product__c = p.id;
		cef2.date_paid_from__c = system.today();
		cef2.date_paid_to__c = system.today().addDays(+31);
		insert cef2;
        
        
        RestRequest req = new RestRequest();
   		req.addParameter('nationality', 'Brazil');
   		req.addParameter('campusCourses', cc.id);
   		req.addParameter('paymentDate', String.valueOf(system.today()));   		
   		req.addParameter('language', 'pt_BR');
   		RestContext.request = req;
   		
        API_v1_schoolProducts.doGet();
        
        
    }
}