public class scPageHeaderController {

	public String accoID {get;set;}
	public Account acco {
		get{
			if(accoID != null)
				acco = [Select Name, RecordType.Name from Account where id = :accoID];
				
			return acco;
		}
		set;
	}

}