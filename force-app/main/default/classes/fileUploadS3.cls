public class fileUploadS3{
    
    public String commUrlPathPrefix {
    	get{
    		
    		if(ConnectApi.Communities.getCommunities().communities.size() > 0)
    			return ConnectApi.Communities.getCommunities().communities[0].urlPathPrefix;
    		else return null;
    	}
    	private set;    	
    }
    
    public String commName {
    	get{
    		return IPFunctions.getCommunityName();
    	}
    	set;
    }

    
    public string getPageHost(){
        return  ApexPages.currentPage().getHeaders().get('Host');
    }
    
    public string getOrgId(){
        return  UserInfo.getOrganizationId();
    }
    
    public PageReference addDocToAgreement(){
        	system.debug('Entrou atualizacao');
        	schoolId = Apexpages.currentPage().getParameters().get('schId');
            agreementId = Apexpages.currentPage().getParameters().get('agrID');
            longD = Apexpages.currentPage().getParameters().get('longD');
            fromAgreement = Apexpages.currentPage().getParameters().get('fromAgreement');
        	fileName = Apexpages.currentPage().getParameters().get('filename');
               
        	School_Agreement__c a = new School_Agreement__c(id = Apexpages.currentPage().getParameters().get('agrID'));
        	a.document_link__c = 'https://s3.amazonaws.com/'+bucketName+'/'+schoolId+'/Agreements/'+agreementId+'/'+longD+'/'+fileName;
        	update a;
        	system.debug('Saiu Atualizacao'); 
        return null;
    }
    

    
    
    /** AMAZON S3 **/
    public string secret { get {return credentials.secret;} }
    public AWSKeys credentials {get;set;}
    public string key { get {return credentials.key;} set;}
    private String AWSCredentialName = IPFunctions.awsCredentialName;
    public S3.AmazonS3 as3 { get; private set; }
    
    /* Variables for Agreement */ 
    public String schoolId {get;set;}
    public String agreementId {get;set;}
    public String longD {get;set;}
    public String fromAgreement {get;set;}
    
    public PageReference constructor(){
        try{
            
            credentials = new AWSKeys(AWSCredentialName);
            as3 = new S3.AmazonS3(credentials.key,credentials.secret);
            bucketName = Apexpages.currentPage().getParameters().get('bucket');
               
            /* FOR AGREEMENT */
            schoolId = Apexpages.currentPage().getParameters().get('schId');
            agreementId = Apexpages.currentPage().getParameters().get('agrID');
            longD = Apexpages.currentPage().getParameters().get('longD');
            fromAgreement = Apexpages.currentPage().getParameters().get('fromAgreement');
            createBucket();
            
            if (Apexpages.currentPage().getParameters().get('filename') != null && Apexpages.currentPage().getParameters().get('filename') != 'fileName')
                addDocToAgreement();
    
        }catch(AWSKeys.AWSKeysException AWSEx){
            System.debug('Caught exception in extQuote: ' + AWSEx);
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, AWSEx.getMessage());
            ApexPages.addMessage(errorMsg);
        }   
        return null;    
    }
    
    
    //public String docType;
    public String bucketName { get { if(bucketName == null) bucketName = UserInfo.getOrganizationId(); return bucketName; } set; }
    public String access { get { if(access == null) access = 'public-read'; return access; } set; }
    public String contentType { get { if(contentType == null) contentType = ''; return contentType; } set; }
	public String contentDisposition { get { if(contentDisposition == null) contentDisposition = 'attachment'; return contentDisposition; } set; }
    public String fileName { get { if(fileName == null) fileName = 'fileName'; return fileName; } set; }
    public String fileSize { get { if(fileSize == null) fileSize = '0'; return fileSize; } set; }
    
    datetime expire = system.now().addDays(1);
    String formattedexpire = expire.formatGmt('yyyy-MM-dd')+'T'+
        expire.formatGmt('HH:mm:ss')+'.'+expire.formatGMT('SSS')+'Z';
    
    private String success_action_redirect() {
        String str;
        str = 'https://'+ApexPages.currentPage().getHeaders().get('Host') + '/' + commName + '/fileUploadDocuments?filename=' + fileName + '&schId='+schoolId +'&agrID='+agreementId+'"},';
        system.debug('Passou Aqui' + str);
        
        return str;   
    }
    
    
    string policy { get {return 
            '{ "expiration": "'+formattedexpire+'","conditions": [ {"bucket": "'+
            bucketName +'" } ,{ "acl": "'+
            access +'" },'+
            //  '{"success_action_status": "201" },'+
            '{"content-type":"'+ApexPages.currentPage().getParameters().get('contentType')+'"},'+
			'{"content-disposition":"'+contentDisposition+'"},'+
            '{"success_action_redirect": "'+success_action_redirect()+         
            '["starts-with", "$key", ""] ]}';   } } 
    
    public String getPolicy() {
    	return EncodingUtil.base64Encode(Blob.valueOf(policy));
    }
    
     public String getPolicyDesc() {
    	system.debug('Descricao Policy:'+policy );
    	return policy;
    }
    
    
    public String getSignedPolicy() {    
        return make_sig(EncodingUtil.base64Encode(Blob.valueOf(policy)));        
    }
    
    // tester
    public String getHexPolicy() {
        String p = getPolicy();
        return EncodingUtil.convertToHex(Blob.valueOf(p));
    }
    
    //method that will sign
    private String make_sig(string canonicalBuffer) {        
        String macUrl ;
        String signingKey = EncodingUtil.base64Encode(Blob.valueOf(secret));
        Blob mac = Crypto.generateMac('HMacSHA1', blob.valueof(canonicalBuffer),blob.valueof(Secret)); 
        macUrl = EncodingUtil.base64Encode(mac);                
        return macUrl;
    }
    
  	public void setParameters(){
        System.debug('**> fileName: '+fileName);
        contentType = ApexPages.currentPage().getParameters().get('contentType');
    }
 
    /*
       Method to create a bucket on AWS S3 
    
    */
    public String createBucketErrorMsg {get;set;}
    public PageReference createBucket(){
        try{     
            createBucketErrorMsg= null;
            Datetime now = Datetime.now();        
            System.debug('about to create S3 bucket called: ' + UserInfo.getOrganizationId());
           
           
            //This performs the Web Service call to Amazon S3 and create a new bucket.
            S3.CreateBucketResult createBucketReslt = as3.CreateBucket(UserInfo.getOrganizationId(),null,as3.key,now,as3.signature('CreateBucket',now));
            System.debug('Successfully created a Bucket with Name: ' + createBucketReslt.BucketName);
            createBucketErrorMsg='Success';
            return null;
        }
        catch(System.CalloutException callout){
            System.debug('CALLOUT EXCEPTION: ' + callout);
            ApexPages.addMessages(callout);
            createBucketErrorMsg = callout.getMessage();
            return null;    
        }
        catch(Exception ex){
            System.debug(ex);
            ApexPages.addMessages(ex);
            createBucketErrorMsg = ex.getMessage();
            return null;    
        }
       
    }

}