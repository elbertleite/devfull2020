/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class VisitsController_test {

    static testMethod void myUnitTest() {
        
		TestFactory tf = new TestFactory();
		Account agency = tf.createAgency();
		Contact employee = tf.createEmployee(agency);
		User portalUser = tf.createPortalUser(employee);		
		
		Department__c dept = [Select id, Services__c  from Department__c where id = :employee.Department__c];
		
		Test.startTest();
		system.runAs(portalUser){
			
			Contact ct = tf.createLead(agency, employee);
			
			ApexPages.currentPage().getParameters().put('ctId', ct.id);
			VisitsController createVisit = new VisitsController(new ApexPages.StandardController(new Visit__c()));
			String paramStaff = portalUser.id + '!#' + dept.id + '!#STC,'+portalUser.id + '!#' + dept.id + '!#General Services,'+portalUser.id + '!#' + dept.id + '!#Translations';
			ApexPages.currentPage().getParameters().put('paramStaff', paramStaff);
			createVisit.createVisit();
			
			VisitsController visits = new VisitsController();			
			visits.visitsPage();
			visits.changeAgencyGroup();
			visits.changeAgency();
			visits.findDepartmentsMobile();
			visits.refreshItems();
			
			ApexPages.currentPage().getParameters().put('clientId', ct.id);
			visits.createVisitMobile();

			
			visits.selectedNextService = portalUser.id + '!#' + dept.id + '!#STC,'+portalUser.id + '!#' + dept.id + '!#General Services';
			visits.completeNTransferVisit();
			
			
			Contact client = tf.createClient(agency);
			paramStaff = portalUser.id + '!#' + dept.id + '!#Translations,'+portalUser.id + '!#' + dept.id + '!#STC';
			
			Visit__c v = new Visit__c();
			v.Contact__c = client.id;
			v.Staff_Requested__c = paramStaff;
			v.status__c = 'Waiting';
			v.Department_Requested__c = dept.id;
			insert v;
			
			VisitsController pending = new VisitsController(new ApexPages.StandardController(new Visit__c()));
			
			VisitsController editVisit = new VisitsController(new ApexPages.StandardController(v));
			
			ApexPages.currentPage().getParameters().put('id', v.id);
			ApexPages.currentPage().getParameters().put('ac', 'cp');
			VisitsController anotherVisit = new VisitsController();
			
			anotherVisit.visitId = v.id;
			anotherVisit.acceptClient();
			
			anotherVisit.selectedServiceRadio = 'Translations';
			anotherVisit.completeVisit();
			anotherVisit.selectedNextService = portalUser.id + '!#' + dept.id + '!#STC';
			anotherVisit.completeNTransferVisit();
			
			//Visit__c v2 = [select id from Visit__c where Parent_Visit__c != null LIMIT 1];			
			anotherVisit.completeWithParent();
			
			anotherVisit.cancelSelected();
			anotherVisit.cancelVisit.Cancel_Reason__c = 'Foi embora';
			anotherVisit.completeNCancelVists();
			
			
			anotherVisit.cancelVisit();
			anotherVisit.visit.Cancel_Reason__c = 'Vazou';
			anotherVisit.selectedServices = new List<String>{'STC','Translations'};
			anotherVisit.cancelVisit();
			
			anotherVisit.selectedServices = new List<String>{'STC'};
			anotherVisit.cancelVisit();
			
			
			anotherVisit.findDepartments();
			
			/*
			
			ApexPages.currentPage().getParameters().put('ac', 'cn');
			vc = new VisitsController();
			vc.selectedServiceRadio = 'Translations';
			vc.selectedDepartment = dept.id;
			vc.visitId = v.id;
			
			vc.completeVisit();
			
			ApexPages.currentPage().getParameters().remove('id');
			vc = new VisitsController(new ApexPages.StandardController(new Visit__c()));
			
			
			v = new Visit__c();
			v.Contact__c = ct.id;
			paramStaff = portalUser.id + '!#' + dept.id + '!#Schools / Visas,' + portalUser.id + '!#' + dept.id + '!#Translations';
			v.Staff_Requested__c = paramStaff;
			v.status__c = 'Waiting';
			v.Department_Requested__c = dept.id;
			insert v;
			
			ApexPages.currentPage().getParameters().put('id', v.id);
			ApexPages.currentPage().getParameters().remove('ctId');
			
			vc.selectedDepartment = dept.id;
			vc.visitId = v.id;
			vc.acceptClient();
			
			vc.selectedServiceRadio = portalUser.id + '!#' + dept.id + '!#Schools / Visas';
			vc.completeVisit();
			
			
			*/
			
		}
		Test.stopTest(); 

    }
}