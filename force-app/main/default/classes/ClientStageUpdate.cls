public class ClientStageUpdate {
	
public String contactId {get;set;} 
public boolean isLead {get;set;} 


private User userDetails {
	get{
		if(userDetails == null){
			userDetails = [Select Contact.Account.ParentId, Contact.AccountId  from User where id = :UserInfo.getUserId() limit 1];
		}
		return userDetails;
		
	}set;
}
	
public Contact acco{
	get{
		system.debug('CONTACT ID SENT '+contactId);
		if(acco == null){
		try {
			acco = [Select id, Name, Current_Agency__c, Current_Agency__r.ParentId, Current_Agency__r.Hify_Agency__c, Account.Hify_Agency__c, Destination_Country__c, Lead_Product_Type__c,Lead_Study_Type__c, Expected_Travel_date__c, Lead_Stage__c, Lead_Status_Reason__c, Destination_City__c, Lead_Level_of_Interest__c, Status__c, recordType.Name, Visa_Expiry_Date__c, Lead_State__c, lastModifiedDate From Contact Where id =:contactId];
					
			if(acco.Lead_Product_Type__c != null && acco.Lead_Product_Type__c != '')
						selectedLeadProduct = acco.Lead_Product_Type__c.split(';');
						
			if(acco.Lead_Study_Type__c != null && acco.Lead_Study_Type__c != '')
					selectedLeadStudyArea = acco.Lead_Study_Type__c.split(';');
				
			if(acco.Lead_Product_Type__c != null && acco.Lead_Product_Type__c.contains('Study'))
				isStudyArea = true;
				
				
		} catch (Exception e){
			system.debug('Error===' + e.getMessage() + ' ' + e.getLineNumber());
		}
	}
		return acco;
	}
	set;
}

public Boolean isStudyArea {
	get{
		if(isStudyArea == null)
			isStudyArea = false;
		return isStudyArea;
	}
	Set;
}

public List<String> selectedLeadProduct {
		get{
			if(selectedLeadProduct == null)
				selectedLeadProduct = new List<String>();
			return selectedLeadProduct;
		}
		Set;
	}
	
public List<SelectOption> leadProductOptions {
	get{
		if(leadProductOptions == null){
			leadProductOptions = new List<SelectOption>();
			
			Schema.DescribeFieldResult fieldResult = Contact.Lead_Product_Type__c.getDescribe();
  			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
   			for( Schema.PicklistEntry f : ple)
				leadProductOptions.add(new SelectOption(f.getLabel(), f.getValue()));
			try {
				leadProductOptions.add(leadProductOptions.size()-1, new SelectOption('Not Sure','Not Sure'));
			} catch( Exception e ) {
				leadProductOptions.add(new SelectOption('Not Sure','Not Sure'));
			}	
		}
		return leadProductOptions;
	}
	Set;
}

public List<String> selectedLeadStudyArea {
	get{
		if(selectedLeadStudyArea == null)
			selectedLeadStudyArea = new List<String>();
		return selectedLeadStudyArea;
	}
	Set;
}
	
public List<SelectOption> getleadStudyAreaOptions() {
	List<SelectOption> options = new List<SelectOption>();
	Schema.DescribeFieldResult result = Contact.Lead_Study_Type__c.getDescribe();
	for (Schema.PicklistEntry ld:result.getPicklistValues())
		options.add(new SelectOption(ld.getLabel(),ld.getLabel()));
	return options;
}
		
	
//==========CLIENTS===========================
	/*public boolean hasVisa {get{if(hasVisa ==null) hasVisa = visa.id != null ? true : false; return hasVisa;}set;}
	public Client_Visa__c visa {
		get{
			if(visa == null){
				try {
					visa = [Select Expire_date__c from Client_Visa__c WHERE Account__c = :acco.id ORDER BY Expire_date__c DESC LIMIT 1];
					hasVisa = true;
				} catch (Exception e){ 
					visa = new Client_Visa__c();
					hasVisa = false; 
				}
			}
			return visa;
		}
		set;}
		*/
		
	public string stage{
		get{
			if(stage == null){
				if(acco.Lead_Stage__c != null){
					stage = acco.Lead_Stage__c;
					if(stage == 'Stage 0'){
						List<Client_Stage__c> checklist;
						if(acco.RecordType.Name == 'Lead'){
							checklist = [Select Selected__c, Stage__c, Stage_description__c, Destination__c, Agency_Group__r.name, Id  from Client_Stage__c where Stage__c = :stage and Agency_Group__c = :userDetails.Contact.Account.ParentId and Lead_Stage__c = true order by Agency_Group__r.name, Stage__c, ItemOrder__c, createdDate ];
						}else{
							checklist = [Select Selected__c, Stage__c, Stage_description__c, Destination__c, Agency_Group__r.name, Id  from Client_Stage__c where Stage__c = :stage and Agency_Group__c = :userDetails.Contact.Account.ParentId order by Agency_Group__r.name, Stage__c, ItemOrder__c, createdDate ];
						}	
						if(checklist == null || checklist.isEmpty()){
							stage = 'Stage 1';
						}					
					}
				}else{
					try{
						Client_Stage__c stageZero = [SELECT Stage__c FROM Client_Stage__c WHERE Stage__c = 'Stage 0' AND Lead_Stage__c = true AND Agency_Group__c = :userDetails.Contact.Account.ParentId LIMIT 1];
						if(stageZero != null)
							stage = 'Stage 0';
						else
							stage = 'Stage 1';
					}catch(QueryException qe){
						system.debug('No Stage Zero found: ' +userDetails.Contact.Account.ParentId);
						stage = 'Stage 1';
					}
				} 
			}
			system.debug('THE STAGE '+stage);
			system.debug('THE STAGE '+acco.Lead_Stage__c);
			return stage;
		} 
		set;
	}
	public List<SelectOption> getStageOptions() {
		List<SelectOption> options = new List<SelectOption>();
		if(acco.RecordType.Name == 'Lead'){
			for(AggregateResult ar : [SELECT Stage__c FROM CLIENT_STAGE__C WHERE Destination__c = :acco.Destination_Country__c and Agency_Group__c = :userDetails.Contact.Account.ParentId and Lead_Stage__c = true GROUP BY Stage__c  ORDER BY Stage__c]){
				if(dTracking != null && dTracking.Lead_Cycle__c && (String) ar.get('Stage__c') == 'Stage 1')
					continue;
				options.add(new SelectOption( (String) ar.get('Stage__c'), (String) ar.get('Stage__c') ));
			}
			try{
				Client_Stage__c stageZero = [SELECT Stage__c FROM Client_Stage__c WHERE Stage__c = 'Stage 0' AND Lead_Stage__c = true AND Agency_Group__c = :userDetails.Contact.Account.ParentId LIMIT 1];
				if(options.size() > 0){
					options.add(0, new SelectOption( stageZero.Stage__c, stageZero.Stage__c ));
				}else{
					options.add(new SelectOption( stageZero.Stage__c, stageZero.Stage__c ));
				}
			}catch(QueryException qe){
				system.debug('No Stage Zero found: ' +userDetails.Contact.Account.ParentId);
			}		
		}else{
			for(AggregateResult ar : [SELECT Stage__c FROM CLIENT_STAGE__C WHERE Destination__c = :acco.Destination_Country__c and Agency_Group__c = :userDetails.Contact.Account.ParentId GROUP BY Stage__c  ORDER BY Stage__c]){
				if(dTracking != null && dTracking.Lead_Cycle__c && (String) ar.get('Stage__c') == 'Stage 1')
					continue;
				options.add(new SelectOption( (String) ar.get('Stage__c'), (String) ar.get('Stage__c') ));
			}
			try{
				Client_Stage__c stageZero = [SELECT Stage__c FROM Client_Stage__c WHERE Stage__c = 'Stage 0' AND Agency_Group__c = :userDetails.Contact.Account.ParentId LIMIT 1];
				if(options.size() > 0){
					options.add(0, new SelectOption( stageZero.Stage__c, stageZero.Stage__c ));
				}else{
					options.add(new SelectOption( stageZero.Stage__c, stageZero.Stage__c ));
				}
			}catch(QueryException qe){
				system.debug('No Stage Zero found: ' +userDetails.Contact.Account.ParentId);
			}		
		}
		return options;
	}	
	
	public void refreshCycle(){
		acco = null;
		dTracking = null;
		stage = null;
		statusList = null;
	}
	
	public void updateList(){
		statusList = null;
		acco = null;
		stage = null;
	}
	
	public Destination_Tracking__c dTracking {
		get{
			if(dtracking == null){
				try {
					//dtracking = [Select id, Destination_Country__c, Destination_City__c, Expected_Travel_date__c, Arrival_date__c, Lead_Cycle__c  from Destination_Tracking__c where client__c = :acco.id and Destination_Country__c =:acco.Destination_Country__c and Destination_City__c = :acco.Destination_City__c order by LastModifiedDate desc limit 1];
					dTracking = [Select id, Destination_Country__c, Destination_City__c, Expected_Travel_date__c, Arrival_date__c, Lead_Cycle__c  from Destination_Tracking__c where Client__c = :acco.id and Destination_Country__c = :acco.Destination_Country__c AND Agency_Group__c = :userDetails.Contact.Account.ParentId order by CreatedDate desc LIMIT 1];
				} catch (Exception e){}
			}
			return dTracking;
		}
		set;
	}
	
	
	public class ClientStatus {
		public String id {get;Set;}
		public Boolean selected { get { if(selected == null) selected = false; return selected; } Set; }
		public String stage {get;Set;}
		public String status {get;Set;}		
	}
	
	
	
	public List<ClientStatus> statusList {
		get{
			system.debug('====> GETTING STATUS LIST <==== '+stage);
			if(statusList == null && dTracking != null){
				statusList = new List<ClientStatus>();
				List<Client_Stage__c> checklist;
				
				if(stage == 'Stage 0'){
					if(acco.RecordType.Name == 'Lead')
						checklist = [Select Selected__c, Stage__c, Stage_description__c, Destination__c, Agency_Group__r.name, Id  from Client_Stage__c where Stage__c = :stage and
											Agency_Group__c = :userDetails.Contact.Account.ParentId and Lead_Stage__c = true order by Agency_Group__r.name, Stage__c, ItemOrder__c, createdDate ];
					else
						checklist = [Select Selected__c, Stage__c, Stage_description__c, Destination__c, Agency_Group__r.name, Id  from Client_Stage__c where Stage__c = :stage and
											Agency_Group__c = :userDetails.Contact.Account.ParentId order by Agency_Group__r.name, Stage__c, ItemOrder__c, createdDate ];
				}else{

					if(acco.RecordType.Name == 'Lead')
						checklist = [Select Selected__c, Stage__c, Stage_description__c, Destination__c, Agency_Group__r.name, Id  from Client_Stage__c where Destination__c = :acco.Destination_Country__c and Stage__c = :stage and
											Agency_Group__c = :userDetails.Contact.Account.ParentId and Lead_Stage__c = true order by Agency_Group__r.name, Stage__c, ItemOrder__c, createdDate ];
					else
						checklist = [Select Selected__c, Stage__c, Stage_description__c, Destination__c, Agency_Group__r.name, Id  from Client_Stage__c where Destination__c = :acco.Destination_Country__c and Stage__c = :stage and
											Agency_Group__c = :userDetails.Contact.Account.ParentId order by Agency_Group__r.name, Stage__c, ItemOrder__c, createdDate ];
				}
				
				system.debug('====> GOT CHECKLIST <==== '+checklist);
				ClientStatus cStatus;
				for(Client_Stage__c cs : checklist){
					if(cs.Stage__c == 'Stage 1' && dTracking.Lead_Cycle__c) continue;
					cStatus = new Clientstatus();
					cStatus.id = cs.id;
					cStatus.stage = cs.Stage__c;
					cStatus.status = cs.Stage_description__c;
					statusList.add(cStatus);					
				}
				
				
				List<Client_Stage_Follow_Up__c> theFollowUP = [Select C.Agency__c, C.Agency_Group__c, C.Client__c, C.Destination__c, C.Destination_Tracking__c, C.Last_Saved_Date_Time__c, C.Stage__c, C.Stage_Item__c, C.Stage_Item_Id__c from Client_Stage_Follow_Up__c C
														where C.Client__c = :Acco.id and destination__c = :acco.Destination_Country__c and Destination_Tracking__c = :dTracking.id order by Last_Saved_Date_Time__c];
				
				for(Client_Stage_Follow_Up__c csfu : theFollowUP)					
					for(ClientStatus cs : statusList)
						if( csfu.Stage__c == cs.Stage && csfu.Stage_Item__c == cs.status )
							cs.selected = true;
								
				system.debug('====> GOT STATUS LIST <==== '+JSON.serialize(statusList));
			}	
			return statusList;
		}
		Set;
	}	
	
	public void refreshStatusOptions() {
		statusList = null;
	}	
	
	public boolean errorMessage {get;set;}
	public void verifyMandatoryFields(){
		errorMessage = false;
		system.debug('Reason====='+ acco.Lead_Status_Reason__c);
		
		if(statusList != null)
			for(ClientStatus s : statusList)
				if(s.status == 'LEAD - Not Interested' && s.selected && (acco.Lead_Status_Reason__c == null || acco.Lead_Status_Reason__c == '')){
					acco.Lead_Status_Reason__c.addError(IPFunctions.MESSAGE_FIELD_REQUIRED);
					errorMessage = true;
				}
	}
	
	public void saveClientUpdate(){
		acco.Lead_Product_Type__c = String.join(selectedLeadProduct, ';');
		acco.Lead_Study_Type__c = String.join(selectedLeadStudyArea, ';'); 
		update acco;
		
	}
	
	public PageReference saveClientStatus(){
		
		verifyMandatoryFields();

		Contact ct = [SELECT Id, LastModifiedDate FROM Contact WHERE id = : acco.id];
		boolean errorContact = false;
      	if(ct.LastModifiedDate!= acco.LastModifiedDate){
      		ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.FATAL,'This contact has been modified by another user, please refresh the page and try again.'));
			errorContact = true;
      	}

		else if(!errorMessage){
		
			/*if (hasVisa && visa.id != null)
				update visa;
			*/	
				
			if(dTracking != null && dTracking.Id != null){
			
				Set<String> previousStatus = new Set<String>();
				List<Client_Stage_Follow_Up__c> theFollowUP = [Select C.Agency__c, C.Agency_Group__c, C.Client__c, C.Destination__c, C.Destination_Tracking__c, C.Last_Saved_Date_Time__c, C.Stage__c, C.Stage_Item__c, C.Stage_Item_Id__c from Client_Stage_Follow_Up__c C
																where C.Client__c = :Acco.id and destination__c = :acco.Destination_Country__c and Destination_Tracking__c = :dTracking.id order by Last_Saved_Date_Time__c];
				for(Client_Stage_Follow_Up__c csfu : theFollowUP)
					previousStatus.add(csfu.Stage__c + '-' + csfu.Stage_Item__c);
					
				list<Client_Stage_Follow_Up__c> ckl = new list<Client_Stage_Follow_Up__c>();
				Client_Stage_Follow_Up__c it;
				
				
				Integer counter = 0;
		            
				Client_Stage_Follow_Up__c lastOne;
				try{
					lastOne = [Select Last_Saved_Date_Time__c from Client_Stage_Follow_Up__c where Client__c = :acco.Id and Destination_Tracking__c = :dTracking.id order by Last_Saved_Date_Time__c desc limit 1];
					if(lastOne != null){
						DateTime now = System.now();
						if(now.year() == lastOne.Last_Saved_Date_Time__c.year() && 
							now.month() == lastOne.Last_Saved_Date_Time__c.month() && 
							now.day() == lastOne.Last_Saved_Date_Time__c.day() && 
							now.hour() == lastOne.Last_Saved_Date_Time__c.hour() && 
						now.Minute() == lastOne.Last_Saved_Date_Time__c.minute()){
		                
							counter = lastOne.Last_Saved_Date_Time__c.second();
							counter++;
						}
					}
				} catch(Exception e){}
		        
				System.debug('==>counter: '+counter);
				
				for(ClientStatus cs : statusList ){
					if(cs.selected && !previousStatus.contains(cs.stage + '-' + cs.status)){
						it = new Client_Stage_Follow_Up__c();
						it.Client__c = acco.id;
						it.Agency_Group__c = userDetails.Contact.Account.ParentId;
						it.agency__c = userDetails.Contact.AccountId;
						it.Stage_Item__c = cs.status;
						it.Stage_Item_Id__c = cs.id;
						it.Stage__c = cs.stage;
						it.Checked_By__c = UserInfo.getUserId();
						it.Destination__c = acco.Destination_Country__c;
						it.Last_Saved_Date_Time__c = DateTime.newInstance(system.now().year(),system.now().month(),system.now().day(), system.now().hour(), system.now().minute(), counter);
						it.destination_tracking__c = dTracking.id;
						ckl.add(it);
						counter ++;
						if(counter == 60) counter = 0;
					}
				}
				list<Client_Stage_Follow_Up__c> orderedList = new list<Client_Stage_Follow_Up__c>();
		                                    
		            
				for (integer i = ckl.size() - 1; i >= 0; i--)               
					orderedList.add(ckl.get(i));
		            
				//upsert orderedList;
				insert orderedList;

				if(it != null){
					if(it.Stage_Item__c == 'LEAD - Not Interested'){
						acco.Last_Stage_Before_Not_Interested__c = acco.Status__c;
						acco.Lead_Status_CreatedBy__c = Userinfo.getUserId();
						acco.Lead_Status_Date_Time__c = Datetime.now();
					}
					dTracking.Lead_Last_Stage_Cycle__c = it.Stage__c;
					dTracking.Lead_Last_Status_Cycle__c = it.Stage_Item__c;
				}

				acco.Expected_Travel_date__c = dTracking.Expected_Travel_date__c; 
				update acco;

				update dTracking;
								
				try {
					if(!orderedList.isEmpty()){
						Client_Stage_Follow_Up__c lastSaved = orderedList.get(0);
						if(lastSaved != null){
							acco.Lead_Stage__c = lastSaved.Stage__c;
							acco.Status__c = lastSaved.Stage_Item__c;
							acco.Lead_Status_Date_Time__c = lastSaved.Last_Saved_Date_Time__c;
							acco.Lead_Status_CreatedBy__c = Userinfo.getUserId();
							update acco;
						}
					}
				} catch(Exception e){
					System.debug('$Error: ' + e.getMessage());
				}
				statusList = null;
			} else {
				
				update acco; //if tracking is null and there is change in level of interest
				
			}
		}
		
		String redirect = ApexPages.currentPage().getParameters().get('createTask');
		if(redirect != null && redirect == 'true' && !errorMessage && !errorContact){
			PageReference pr = new PageReference('/Client_Checklist?id='+acco.id+'&pop=pop&lead=1&editInfo=false');
			pr.setRedirect(true);
			return pr;
		} else 
			return null;
			
		
	}
	
	public List<SelectOption> getCountries(){
		List<SelectOption> options = IPFunctions.getAllCountries();
		options.add(0, new SelectOption('','-- Select --')); 
		return options;
	}
	
	
	public void cancelClientStatus(){
		statusList = null;
		acco = null;
	}
	

}