public with sharing class lwcSettingsForm {
    
    public class wrpGeneric {
        @AuraEnabled public String label {get; set;}
        @AuraEnabled public string value {get; set;}
        
        public wrpGeneric(String label, String value) {
            this.label = label;           
            this.value = value;            
        }       
    }

    // Method used on settings js
    @AuraEnabled(cacheable=true)
    public static List<wrpGeneric> getClientStage() { 

        List<wrpGeneric> wrapper = new List<wrpGeneric>();
        //try {                               
        String agencyGroupId = lwcFrontDesk.getAgencyGroupFromLoginUser(); 
        System.debug('<---getClientStage---> ' + agencyGroupId); 
        for(Client_Stage__c us :[SELECT id, Stage_description__c FROM Client_Stage__c WHERE Agency_Group__c = :agencyGroupId ORDER BY Stage_description__c]){                                                                    
            wrapper.add(new wrpGeneric(us.Id, us.Stage_description__c));                                                                         
        }                         
        
        // } catch (Exception ex) {
        //     System.debug('<---getClientStage error---> ' + ex.getMessage());
        //     throw new AuraHandledException('Error: ' + ex.getMessage());
        // }
        // System.debug('<---getClientStage---> ' + wrapper);
        
        return wrapper;    		
    }
    
    // Method used on settings js
    @AuraEnabled(cacheable=true)
    public static List<wrpGeneric> getUserPicklist(string agencyId) { 

        List<wrpGeneric> wrapper = new List<wrpGeneric>();
        // try {                               
            if (
                (String.isBlank(agencyId)) ||
                (String.isEmpty(agencyId)) ||
                (agencyId.isWhitespace())
            ) {
                agencyId = lwcFrontDesk.getUserContactAccountID(); 
            }
        //System.debug('<---getUserPiclist---> ' + agencyId);             
        for(User us :[SELECT id, name FROM User WHERE isActive = true AND Contact.AccountID = :agencyId ORDER BY name]){                                                                    
            wrapper.add(new wrpGeneric(us.Id, us.Name));                                                                         
        }                         
        
        return wrapper;    		
    }

    // Method used on settings js
    @AuraEnabled(cacheable=true)
    public static List<wrpGeneric> getEmailTemplate(string agencyId) { 

        List<wrpGeneric> pickListValuesList = new List<wrpGeneric>();       
        //String agencyId = lwcFrontDesk.getUserContactfAccountID(); 
        for(Email_Template__c te :[SELECT Id, Template_description__c FROM Email_Template__c WHERE Agency__c = :agencyId ORDER BY Template_description__c]){                                                                    
            pickListValuesList.add(new wrpGeneric(te.Id, te.Template_description__c));                                                                         
        }    

        return pickListValuesList;		
    }
   
    // Method used on settings js
    @AuraEnabled
    public static string updatePasswordFormSettings(string password, string agencyId) {
        System.debug('<---updatePasswordFormSettings---> '); 
        string res = 'success';

        try{
            if (
                (String.isBlank(agencyId)) ||
                (String.isEmpty(agencyId)) ||
                (agencyId.isWhitespace())
            ) {
                agencyId = lwcFrontDesk.getUserContactAccountID(); 
            }
            System.debug('agencyId---> ' + agencyId); 
            Account acc = [SELECT Id, Form_Password__c FROM Account WHERE Id = :agencyId LIMIT 1];                
            acc.Form_Password__c = password;
            update acc;               
            
        }catch(Exception ex){
            res = 'error';   
            System.debug('updatePasswordFormSettings Error---> ' + ex.getMessage());                
        }
        return res;
    } 

    // Method used on settings js
    @AuraEnabled
    public static string insertFormSettings(string objJson, String agencyId) {
        System.debug('<---insertFormSettings---> '); 
        string res = 'success';

        try{
            if (
                (String.isBlank(agencyId)) ||
                (String.isEmpty(agencyId)) ||
                (agencyId.isWhitespace())
            ) {
                agencyId = lwcFrontDesk.getUserContactAccountID(); 
            }

            System.debug('agencyId---> ' + agencyId); 
            Account acc = [SELECT Id, Form_DeskSettings__c FROM Account WHERE Id = :agencyId LIMIT 1];                
            acc.Form_DeskSettings__c = objJson;
            update acc;   
            
        }catch(Exception ex){
            res = 'error';   
            System.debug('insertFormSettings Error---> ' + ex.getMessage());                
        }
        return res;
    }    

    // Method used on settings js
    @AuraEnabled(cacheable=true) 
    public static List<Object> getSettings(String agencyId){
        System.debug('<---gettSettings---> ');
        List<Object> lst = new List<String>();
        if (
                (String.isBlank(agencyId)) ||
                (String.isEmpty(agencyId)) ||
                (agencyId.isWhitespace())
            ) {
                agencyId = lwcFrontDesk.getUserContactAccountID(); 
            }

        Account acc = [SELECT Id, Form_DeskSettings__c FROM Account WHERE Id = :agencyId LIMIT 1]; 
        if (acc != null){
            if ((!String.isBlank(acc.Form_DeskSettings__c)) &&
                (!String.isEmpty(acc.Form_DeskSettings__c)) &&
                (!acc.Form_DeskSettings__c.isWhitespace()) &&
                (acc.Form_DeskSettings__c != null) ) {

                String jsonInput = acc.Form_DeskSettings__c;                         
                                
                lst = (List<Object>) JSON.deserializeUntyped(jsonInput);
            }
        }
        System.debug('lst---> ' + lst);
        return lst;        
    }   

    // Method used on settings js and xCourseSearchNewClient class
    @AuraEnabled(cacheable=true)
    public static string getSettingsFormPassword(string agencyId) {
        
        string userPassword = '';

        try{     
            if (
                (String.isBlank(agencyId)) ||
                (String.isEmpty(agencyId)) ||
                (agencyId.isWhitespace())
            ) {
                agencyId = lwcFrontDesk.getUserContactAccountID(); 
            }
            System.debug('agencyId---> ' + agencyId); 
            string password = [SELECT Form_Password__c FROM Account WHERE Id = :agencyId LIMIT 1].Form_Password__c;                   
            if (
                (!String.isBlank(password)) &&
                (!String.isEmpty(password)) &&
                (!password.isWhitespace())
            ) {
                userPassword = password;
            }           

        }catch(Exception ex){
            userPassword = ''; 
            System.debug('getSettingsFormmPassword Error---> ' + ex.getMessage());                     
        }
        return userPassword;
    } 
}