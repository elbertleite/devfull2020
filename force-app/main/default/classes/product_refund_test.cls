@isTest
private class product_refund_test
{
	public static User portalUser {get{
	if (null == portalUser) {
	portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
	} return portalUser;} set;}

	@testSetup static void setup() {
		TestFactory tf = new TestFactory();
		tf.setupProdcutsTest();

		Contact client = [Select Id, Accountid From Contact limit 1];
	
		portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
		system.runAs(portalUser){
			
			//Add product to booking
			ApexPages.currentPage().getParameters().put('id', client.Id);
			products_search ps = new products_search();

			for(String av : ps.result.keySet())
				for(String cat : ps.result.get(av).keyset())
					for(products_search.products p : ps.result.get(av).get(cat)){
						p.product.Selected__c = true;

						for(Quotation_List_Products__c f : p.prodFees)
							f.isSelected__c = true;
					}

			ps.saveProduct();

			//Pay Product
			client_product_service__c cliProduct = [Select Id from client_product_service__c WHERE Related_to_Product__c = null limit 1];

			client_course_product_pay payProd = new client_course_product_pay(new ApexPages.StandardController(cliProduct));

			payProd.newPayDetails.Payment_Type__c = 'Creditcard';
			payProd.newPayDetails.Value__c = payProd.totalPay;
			payProd.addPaymentValue();
			payProd.paidByAgency.Paid_by_Agency__c = true;

			//Data manipulation
			for(client_product_service__c p : payProd.product){
				p.Paid_to_Provider_by_Agency__c = client.AccountId;
				p.Paid_to_Provider_on__c = system.today();
				p.isRefund_Requested__c = TRUE;
			}
			payProd.savePayment();
		}
	}

	static testMethod void requestRefund(){
		client_product_service__c prod = [SELECT ID FROM client_product_service__c limit 1];
		ApexPages.currentPage().getParameters().put('id', prod.id);

		product_refund p = new product_refund();
		list<SelectOption> agencyOptions = p.agencyOptions;
		Decimal totProds = p.totProds;
		p.saveRefund();

	}
}