@isTest
private class provider_new_test{

	public static User portalUser {get{
	if (null == portalUser) {
	portalUser = [Select Id, Name, Contact.AccountId from User where email = 'test12345@test.com' limit 1];
	} return portalUser;} set;}


	@testSetup static void setup() {
		TestFactory tf = new TestFactory();

		Account agencyGroup = tf.createAgencyGroup();
		Account agency = tf.createAgency();
		Contact employee = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(employee);
	}

	static testMethod void fitlers() {

		system.runAs(portalUser){

			provider_new p = new provider_new();

			list<SelectOption> opFees = p.opFees;
			list<SelectOption> claimCommOpt = p.claimCommOpt;
			list<SelectOption> unitsRange = p.unitsRange;
			list<SelectOption> currencies = p.currencies;
			list<SelectOption> commissionType = p.commissionType;
			list<SelectOption> availableForOpt = p.availableForOpt;

			Boolean hasFees = p.hasFees;

			p.cancelProduct();

		}
	}

	static testMethod void saveProvider() {

		system.runAs(portalUser){
			
			ApexPages.currentPage().getParameters().put('acco', portalUser.Contact.AccountId);
			provider_new p = new provider_new();
			p.acco.Business_Email__c ='test@bupaTest.com';
			p.acco.Business_Number__c ='000000000';
			p.acco.Business_Phone__c ='000000000';
			p.acco.Invoice_City__c ='Sydney';
			p.acco.Invoice_Country__c ='Australia';
			p.acco.Invoice_PostalCode__c ='2000';
			p.acco.Invoice_State__c ='NSW';
			p.acco.Invoice_Street__c ='Sydney Street';
			p.acco.Invoice_to_Email__c ='test@bupatest.com.au';
			p.acco.Provider_Name__c ='BUPA';
			p.acco.Provider_Type__c ='Insurance';
			p.acco.Registration_name__c ='BUPA';
			p.acco.Type_of_Business_Number__c ='ABN';
			p.acco.Website__c ='www.bupa.com.au';
			p.saveProvider();
			
			ApexPages.currentPage().getParameters().put('id', p.acco.Id);
			p = new provider_new();
			p.editMode();

			//Add Product
			p.product.Product_Name__c ='Test Product';
			p.product.Claim_Commission_Type__c = 'Net Commission';
			p.product.Description__c = 'Testing product input';
			p.product.Category__c = 'Test category';
			p.product.Currency__c = 'AUD';
			p.product.Commission_Type__c = 0;
			p.product.Commission__c = 10.5;
			p.selCountries = 'Australia;';

			//Add Product Price
			p.productItem.Payment_Start_Date__c = system.today();
			p.productItem.Payment_End_Date__c = system.today().addDays(30);
			p.productItem.Quantity__c = 1;
			p.productItem.Value__c = 50.0;
			p.addPrice();

			//Add Product Price
			p.productItem.Payment_Start_Date__c = system.today();
			p.productItem.Payment_End_Date__c = system.today().addDays(30);
			p.productItem.Quantity__c = 2;
			p.productItem.Value__c = 49.0;
			p.addPrice();

			//Cause validation error
			p.productItem.Payment_Start_Date__c = system.today().addDays(20);
			p.productItem.Payment_End_Date__c = p.productItem.Payment_Start_Date__c.addDays(30);
			p.addPrice();

			//Add Second Product Price
			p.productItem.Payment_Start_Date__c = system.today().addDays(31);
			p.productItem.Payment_End_Date__c = p.productItem.Payment_Start_Date__c.addDays(30);
			p.productItem.Quantity__c = 2;
			p.addPrice();
			p.saveproduct();

			String prodId = [SELECT ID FROM Quotation_Products_Services__c WHERE Provider__c != null limit 1].Id;

			ApexPages.currentPage().getParameters().put('prod', prodId);
			p.editProduct();


			

			//Add Start Date
			list<string> priceKeys = new list<string>(p.mapProductItem.keySet());
			ApexPages.currentPage().getParameters().put('group', priceKeys[0]);
			ApexPages.currentPage().getParameters().put('counter', '1');
			p.newDate.From_Date__c = system.today();
			p.newDate.To_Date__c = system.today().addDays(30);
			p.newDate.Value__c = 35.0;
			p.addStartDate();

			//Cause validation error
			ApexPages.currentPage().getParameters().put('group', priceKeys[0]);
			ApexPages.currentPage().getParameters().put('counter', '1');
			p.newDate.From_Date__c = system.today();
			p.newDate.To_Date__c = system.today().addDays(30);
			p.newDate.Value__c = 35.0;
			p.addStartDate();

			//Add Second Start Date
			ApexPages.currentPage().getParameters().put('group', priceKeys[0]);
			ApexPages.currentPage().getParameters().put('counter', '1');
			p.newDate.From_Date__c = system.today().addDays(31);
			p.newDate.To_Date__c = p.newDate.From_Date__c.addDays(30);
			p.newDate.Value__c = 40.0;
			p.addStartDate();
			p.selCountries = 'Australia;';

			//Remove Start Date
			list<String> dtRanges = new list<String>(p.mapDateRange.keySet());
			ApexPages.currentPage().getParameters().put('dateGroup', dtRanges[0]);
			ApexPages.currentPage().getParameters().put('counter', '1');
			p.removeStartDate();

			//Remove Price
			ApexPages.currentPage().getParameters().put('dateGroup', priceKeys[0]);
			ApexPages.currentPage().getParameters().put('counter', '2');
			p.removePrice();

			p.saveproduct();

			p.setupProducts();

			Integer hasItems = p.hasItems;

			//Setup add Related fee
			for(String c : p.products.keySet()){
				ApexPages.currentPage().getParameters().put('cat', c);
				for(provider_new.productDetails prodCat : p.products.get(c)){
					
					ApexPages.currentPage().getParameters().put('prod', prodCat.product.Id);

					for(provider_new.paymentRange pr : prodCat.lPayRange){
						ApexPages.currentPage().getParameters().put('range', pr.paymentRange);
						
						for(Quotation_list_products__c lp : pr.payItems){
							ApexPages.currentPage().getParameters().put('pay', lp.Id);
							break;
						}
						break;
					}
					break;
				}
				break;
			}
			p.setupAddRelated();

			//Create Product Fee
			p.productFee.Product_Name__c = 'Test Fee';
			p.addProdFee();

			//Add Related Fee
			p.productItem.Payment_Start_Date__c = system.today();
			p.productItem.Payment_End_Date__c = system.today().addDays(30);
			p.productItem.Quantity__c = 1;
			p.productItem.Value__c = 10.0;
			p.addRelatedFee();

			p.productItem.Payment_Start_Date__c = system.today();
			p.productItem.Payment_End_Date__c = system.today().addDays(30);
			p.productItem.Quantity__c = 2;
			p.productItem.Value__c = 9.99;
			p.addRelatedFee();

			//Add Start Date
			priceKeys = new list<string>(p.mRelatedFees.keySet());
			ApexPages.currentPage().getParameters().put('group', priceKeys[0]);
			ApexPages.currentPage().getParameters().put('counter', '1');
			p.newDate.From_Date__c = system.today();
			p.newDate.To_Date__c = system.today().addDays(30);
			p.newDate.Value__c = 35.0;
			p.addStartDate();

			//Cause validation error
			ApexPages.currentPage().getParameters().put('group', priceKeys[0]);
			ApexPages.currentPage().getParameters().put('counter', '1');
			p.newDate.From_Date__c = system.today();
			p.newDate.To_Date__c = system.today().addDays(30);
			p.newDate.Value__c = 35.0;
			p.addStartDate();

			//Add Second Start Date
			ApexPages.currentPage().getParameters().put('group', priceKeys[0]);
			ApexPages.currentPage().getParameters().put('counter', '1');
			p.newDate.From_Date__c = system.today().addDays(31);
			p.newDate.To_Date__c = p.newDate.From_Date__c.addDays(30);
			p.newDate.Value__c = 40.0;
			p.addStartDate();
			p.selCountries = 'Australia;';

			//Remove Start Date
			dtRanges = new list<String>(p.mapDateRange.keySet());
			ApexPages.currentPage().getParameters().put('dateGroup', dtRanges[0]);
			ApexPages.currentPage().getParameters().put('counter', '1');
			p.removeStartDate();

			//Remove Price
			ApexPages.currentPage().getParameters().put('dateGroup', priceKeys[0]);
			ApexPages.currentPage().getParameters().put('counter', '2');
			p.removePrice();


			//Save Related Fee to Product
			p.addRelatedProd();

			p.setupProducts();

			//Setup add Related fee ---> To load the prices and date ranges
			for(String c : p.products.keySet()){
				ApexPages.currentPage().getParameters().put('cat', c);
				for(provider_new.productDetails prodCat : p.products.get(c)){
					
					ApexPages.currentPage().getParameters().put('prod', prodCat.product.Id);

					for(provider_new.paymentRange pr : prodCat.lPayRange){
						ApexPages.currentPage().getParameters().put('range', pr.paymentRange);
						
						for(Quotation_list_products__c lp : pr.payItems){
							ApexPages.currentPage().getParameters().put('pay', lp.Id);
							break;
						}
						break;
					}
					break;
				}
				break;
			}
			p.setupAddRelated();


			ApexPages.currentPage().getParameters().put('cat', 'Test category');
			p.setChangeProviders();

			//Delete Products
			for(String c : p.products.keySet()){
				ApexPages.currentPage().getParameters().put('cat', c);
				for(provider_new.productDetails prodCat : p.products.get(c))
					prodCat.product.Selected__c = true;

				break;
			}
			p.deleteProds();
		}
	}
}