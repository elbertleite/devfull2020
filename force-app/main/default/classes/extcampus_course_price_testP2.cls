/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class extcampus_course_price_testP2 {

    static testMethod void myUnitTest() {
        
        Map<String,String> recordTypes = new Map<String,String>();
       
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
			
		TestFactory testFactory = new TestFactory();
		
		Account agency = TestFactory.createAgency();
		
		Currency_rate__c cr = new Currency_rate__c();
		cr.Agency__c = agency.id;
		cr.CurrencyCode__c = 'CAD';
		cr.Value__c = 1.5; 
		cr.High_Value__c = 1.8;
		insert cr;
		
		Contact employee = TestFactory.createEmployee(agency);
		
		Account school = TestFactory.createSchool();
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;
		insert p3;		
		
		Account campus = TestFactory.createCampus(school, agency);
		    
		Course__c course = TestFactory.createCourse();
		
		Campus_Course__c cc = TestFactory.createCampusCourse(campus, course);
		
		Course_Price__c cp = TestFactory.createCoursePrice(cc, 'Latin America');
		
		Course_Price__c cp2 = TestFactory.createCoursePrice(cc, 'Published Price');
		
		Test.startTest();
		
		Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(campus);
		extcampus_course_price eccp = new extcampus_course_price(controller);
		
		String cloneOption = eccp.cloneOption;
		eccp.getCloneOptions();
		
		
		List<String> lstr = eccp.selectedCourseType;
		for(Selectoption so : eccp.getselectedCourseTypeOptions())
			eccp.selectedCourseType.add(so.getValue());	
		
		eccp.selectCourseClone();
		
		lstr = eccp.selectedNationalities;
		for(Selectoption so : eccp.getNationalitiesEdit())
			eccp.selectedNationalities.add(so.getvalue());
			
		
		eccp.listCountry();
		
		//eccp.filterDates();
		
		eccp.getCoursePricesRange();
		
		
		Start_Date_Range__c sdr = eccp.newPriceRange;
		
		eccp.addStartDateRange();
		
		for(Campus_Course__c ccp : eccp.getCoursePricesRange())
			for(Course_Price__c cprice : ccp.Course_Prices__r)
				cprice.isSelected__c = true;
		
		
		eccp.addStartDateRange();
		
		eccp.newPriceRange.From_Date__c = system.today();
		eccp.addStartDateRange();
		
		eccp.newPriceRange.To_Date__c = system.today().addDays(-2);
		eccp.addStartDateRange();
		
		eccp.newPriceRange.Value__c = -200;
		eccp.addStartDateRange();
		
		eccp.newPriceRange.Value__c = 200;
		eccp.addStartDateRange();
		
		
		eccp.newPriceRange.To_Date__c = system.today().addDays(5);
		eccp.addStartDateRange();
		
		eccp.newPriceRange.From_Date__c = system.today().addMonths(5);
		eccp.newPriceRange.To_Date__c = system.today().addMonths(6);
		eccp.addStartDateRange();
		
		eccp.cancellstNRDates();
		
		
		extcampus_course_price.FormatSqlDate(system.today().format());
		
		for(Integer i = 1; i < 12; i++)
			eccp.getMonthDesc(i);
		
		
		eccp.month = String.valueOf(system.today().month());
		eccp.year = String.valueOf(system.today().year());
		eccp.retrieveCampusCoursesByYear();
		eccp.retrieveCampusCoursesByMonth();
		
		
		
		
		
		
		
		eccp.selectedNationalities = new List<String>{'Latin America', 'Published Price'};
		eccp.courseClone = new List<String>{cc.id};
		
		eccp.newCoursePrice.Price_valid_from__c = null;		
		eccp.addMultipleCoursePrice();
		
		eccp.newCoursePrice.Price_valid_from__c = system.today();
		eccp.newCoursePrice.Price_valid_until__c = 	null;
		eccp.addMultipleCoursePrice();
		
		eccp.newCoursePrice.Price_valid_until__c = 	system.today().addDays(-5);
		eccp.addMultipleCoursePrice();
		
		eccp.newCoursePrice.Price_per_week__c = 130;
		
		eccp.newCoursePrice.From__c = 0;
		eccp.addMultipleCoursePrice();
		
		
		eccp.newCoursePrice.From__c = 18;
		eccp.addMultipleCoursePrice();
		
		eccp.newCoursePrice.Price_valid_until__c = 	system.today().addDays(36);
		eccp.addMultipleCoursePrice();
		
		
		eccp.newCoursePrice.Price_valid_from__c = system.today();
		eccp.newCoursePrice.Price_per_week__c = 130;
		eccp.newCoursePrice.From__c = 18;
		eccp.newCoursePrice.Price_valid_until__c = 	system.today().addDays(36);
		eccp.addMultipleCoursePrice();
		
		
		eccp.selectedNationalities = new List<String>{'Latin America', 'Published Price', 'Western Europe'};
		eccp.getCourseListClone();
		
		eccp.selectedNationalities = new List<String>{'Latin America', 'Published Price'};
		
		List<Selectoption> sopt = eccp.campusCloneOptions;
		eccp.setCloneVar();
		
		
		
		eccp.courseClone = new List<String>{cc.id};
		eccp.getListNewTimeTable();
		
		for(Campus_Course__c cctt : eccp.ListNewTimeTable)
			cctt.course__r.important_information__c = 'some important information';
		
		eccp.saveCourseInfo();
		
		eccp.cloneOption = 'timetable';
		
		eccp.getListNewTimeTable();
		for(Campus_Course__c cctt : eccp.ListNewTimeTable)
			cctt.time_Table__c = 'timetable';
		eccp.saveCourseInfo();
		
		
		for(extcampus_course_price.priceDetails ccp : eccp.lstClone)
			for(Course_Price__c coursePrice : ccp.coursePrice){
				coursePrice.isSelected__c = true;
				coursePrice.Price_per_week__c += 50;
			}
		
		
		eccp.saveCoursePrice();
		
		for(extcampus_course_price.priceDetails ccp : eccp.lstClone)
			for(Course_Price__c coursePrice : ccp.coursePrice){
				coursePrice.isSelected__c = true;
				coursePrice.Price_per_week__c += 50;
			}		
		eccp.showGrouped = true;
		eccp.getgroupedCoursesPrice();
		
		eccp.saveCoursePrice();		
				
		eccp.deleteCoursePrice();
		
		Test.stopTest();
		
        
    }
}