/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class client_course_cancel_instalmentpaid_test {

  static testMethod void myUnitTest() {

  	TestFactory tf = new TestFactory();

   	Account school = tf.createSchool();
   	Account agency = tf.createAgency();
  	Contact emp = tf.createEmployee(agency);
   	Account campus = tf.createCampus(school, agency);
   	Course__c course = tf.createCourse();
   	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);


		Contact client = tf.createLead(agency, emp);
		client_course__c booking = tf.createBooking(client);
   	client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);

   	List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);



   	client_course_instalment_payment__c payment = new client_course_instalment_payment__c();

    client_Document__c receipt = new client_Document__c( client_course_instalment__c = instalments[0].id);
    insert receipt;

   	payment.Client__c = client.id;
		payment.Date_Paid__c = system.today();
		payment.Received_On__c = Datetime.now();
		payment.Received_By_Agency__c = agency.id;
		payment.Payment_Type__c = 'cash';
		payment.Value__c = 100;
		payment.client_course_instalment__c = instalments[0].id;
		insert payment;



		client_course_cancel_instalment_paid testClass = new client_course_cancel_instalment_paid(new ApexPages.StandardController(instalments[0]));

		testClass.cancelPayment();

		payment = new client_course_instalment_payment__c();
       	payment.Client__c = client.id;
		payment.Date_Paid__c = system.today();
		payment.Received_On__c = Datetime.now();
		payment.Received_By_Agency__c = agency.id;
		payment.Payment_Type__c = 'School Credit';
		payment.Value__c = 100;
		payment.client_course_instalment__c = instalments[1].id;
		insert payment;

		testClass = new client_course_cancel_instalment_paid(new ApexPages.StandardController(instalments[1]));

		testClass.cancelPayment();

    }
}