/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ClientCourseCOEController_test {

    static testMethod void myUnitTest() {
       TestFactory tf = new TestFactory();
       
       	Account school = tf.createSchool();
       	Account agency = tf.createAgency();
      	Contact emp = tf.createEmployee(agency);
       	User portalUser = tf.createPortalUser(emp);
       	Account campus = tf.createCampus(school, agency);
       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);
        Department__c department = tf.createDepartment(agency);

		Back_Office_Control__c bo = new Back_Office_Control__c(Agency__c = agency.id, Country__c = 'Australia', Backoffice__c = agency.id, Services__c = 'Admissions');
		insert bo;
       
       	Test.startTest();
       	system.runAs(portalUser){
			Contact client = tf.createLead(agency, emp);
			client_course__c booking = tf.createBooking(client);
	       	client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       	client_course__c cc2 =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       	client_course__c cc3Package =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);
	       	
	       	List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);
	       	instalments[0].Paid_To_School_By__c = UserInfo.getUserId();
			instalments[0].Paid_To_School_On__c = DateTime.now();
			instalments[0].Paid_to_School_Date__c = system.today();
			instalments[0].Received_By_Agency__c = agency.id;
			update instalments;
			
	       	cc2.isPackage__c = true;
	       	update cc2;
	       	
	       	List<client_course_instalment__c> instalments2 =  tf.createClientCourseInstalments(cc2);
	       	instalments2[0].Paid_To_School_By__c = UserInfo.getUserId();
			instalments2[0].Paid_To_School_On__c = DateTime.now();
			instalments2[0].Paid_to_School_Date__c = system.today();
			instalments2[0].Received_By_Agency__c = agency.id;
			update instalments2;
			
	       	cc3Package.course_package__c = cc2.id;
	       	update cc3Package;
			
			List<client_course_instalment__c> instalments3 =  tf.createClientCourseInstalments(cc3Package);
	       	instalments3[0].Paid_To_School_By__c = UserInfo.getUserId();
			instalments3[0].Paid_To_School_On__c = DateTime.now();
			instalments3[0].Paid_to_School_Date__c = system.today();
			instalments3[0].Received_By_Agency__c = agency.id;
			update instalments3;
			
	       	ClientCourseCOEController testClass = new ClientCourseCOEController();
	       	
	       	list<SelectOption> agencyGroupOptions = testClass.agencyGroupOptions;
	       	
	       	testClass.findToRequestCOE();
	       	
	       	ApexPages.currentPage().getParameters().put('ccId', cc.id);
	       	testClass.confirmCOE();
	       	
	       	ApexPages.currentPage().getParameters().put('ccId', cc2.id);
	       	testClass.confirmCOE();
	       	
	       	//testClass.getTotalToRequest();
	       	//testClass.getTotalToConfirm();
	       	
	       	testClass.findPendingCOE();

			ApexPages.currentPage().getParameters().put('courseid', cc.id);
			ApexPages.currentPage().getParameters().put('noteComments', 'asdadasdas');
			ApexPages.currentPage().getParameters().put('isUrgent', '');
			testClass.missingInformation();
	       	
	       	testClass.changeGroup();
	       	list<SelectOption> schoolOptions = testClass.schoolOptions;
	       	testClass.changeSchool();
	       	list<SelectOption> campusOptions = testClass.campusOptions;
       	}
        
    }
}