@isTest
private class contact_new_activities_products_test{
	static testMethod void myUnitTest() {

		TestFactory tf = new TestFactory();
		
		Account agency = tf.createAgency();

		Account agencyGroup = tf.createAgencyGroup();
		agencyGroup.RDStation_Campaigns__c = '["leading-page3","leading-australia","insercao-manual","leading-teste2"]';
		update agencyGroup;

		tf.createChecklists(agency.Parentid);

		Contact emp = tf.createEmployee(agency);
		
		Contact lead = tf.createLead(agency, emp);
		lead.Ownership_History_Field__c = '[{"toUserID":"005O0000004bBaSIAU","toUser":"Patricia Greco","toAgencyID":"0019000001MjqIIAAZ","toAgency":"IP Australia Sydney (HO)","fromUserID":null,"fromUser":null,"fromAgencyID":null,"fromAgency":null,"createdDate":"2018-09-18T04:17:12.024Z","createdByUserID":"005O0000004bBaSIAU","createdByUser":"Patricia Greco","action":"Taken"}]';
		lead.Lead_Stage__c = 'Stage 1';
		lead.Lead_Area_of_Study__c = 'Test 1; Test 2';
		update lead;

		Contact client = tf.createClient(agency);

		User portalUser = tf.createPortalUser(emp);

		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);

       	Course__c course = tf.createCourse();
       	Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);

		Quotation__c q1 = tf.createQuotation(lead, campusCourse);
		Quotation__c q2 = tf.createQuotation(lead, campusCourse);

		Destination_Tracking__c dt = new Destination_tracking__c();
		dt.Client__c = lead.id;
		dt.Arrival_Date__c = system.today().addMonths(3);
		dt.Destination_Country__c = 'Australia';
		dt.Destination_City__c = 'Sydney';
		dt.Expected_Travel_Date__c = system.today().addMonths(3);
		dt.lead_Cycle__c = false;
		insert dt;

		Agency_Checklist__c agc = new Agency_Checklist__c();
		agc.Checklist_Item__c = 'ITEM 1';
		agc.Checklist_stage__c = 'STAGE 1';
		agc.ItemOrder__c = 1;
		agc.Destination__c = 'Australia';
		agc.Agency_Group__c = agency.Parentid;

		insert agc;

		Client_Checklist__c checklist = new Client_Checklist__c();
		checklist.Agency__c = agency.id;
		checklist.Agency_Group__c = agency.Parentid;
		checklist.Destination__c = lead.Destination_Country__c;
		checklist.Checklist_Item__c = agc.Checklist_Item__c;
		checklist.Checklist_Item_Id__c = agc.ID;
		checklist.Client__c = lead.id;
		checklist.Destination_Tracking__c = dt.id;
		checklist.Last_Saved__c = true;
		insert checklist;

		agc = new Agency_Checklist__c();
		agc.Checklist_Item__c = 'ITEM 2';
		agc.Checklist_stage__c = 'STAGE 1';
		agc.ItemOrder__c = 1;
		agc.Destination__c = 'Australia';
		agc.Agency_Group__c = agency.Parentid;

		insert agc;

		checklist = new Client_Checklist__c();
		checklist.Agency__c = agency.id;
		checklist.Agency_Group__c = agency.Parentid;
		checklist.Destination__c = lead.Destination_Country__c;
		checklist.Checklist_Item__c = agc.Checklist_Item__c;
		checklist.Checklist_Item_Id__c = agc.ID;
		checklist.Client__c = lead.id;
		checklist.Destination_Tracking__c = dt.id;
		checklist.Last_Saved__c = true;
		insert checklist;
		
		Test.startTest();
     	system.runAs(portalUser){

			Apexpages.currentPage().getParameters().put('id', lead.id);
		
			contact_new_activities_products controller = new contact_new_activities_products();

			Apexpages.currentPage().getParameters().put('itemid', checklist.id);
			controller.uncheckFollowUp();

			controller.saveCheckList();
		}
	}
}