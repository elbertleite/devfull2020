public with sharing class client_course_cancel_product_paid {

	public list<client_product_service__c> product {get;set;}
	public client_course__c clientDeposit {get;set;}
	public boolean showError {get{if(showError ==  null) showError = false; return showError;}set;}
	public String errorMsg {get{if(errorMsg == null) errorMsg = ''; return errorMsg;}set;}
	private User currentUser = [Select Id, Name, Contact.AccountId, Contact.Department__c, Contact.Account.ParentId from User where id = :UserInfo.getUserId() limit 1];

	//Constructor
	public client_course_cancel_product_paid() {
		try{
			String prodId = ApexPages.CurrentPage().getParameters().get('id');
			product = [SELECT  Start_Date__c, End_Date__c, Products_Services__r.Provider__r.Provider_Name__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, Category__c, Client__c, Client__r.Name, Client__r.FirstName, Currency__c, Invoice__c, Quantity__c, Price_Total__c, Product_Name__c, Received_by__r.Name, Received_Date__c, Unit_Description__c, Paid_by_agency__c, Client__r.Owner__r.AccountId, Client__r.Owner__r.Contact.Department__c, isSelected__c, Covered_By_Agency__c, Provider_Payment__c, Paid_to_Provider_on__c, Request_Commission__c, Confirmed_Date__c, ( SELECT Bank_Deposit__c, Booking_Closed_On__c, client_product_service__c, client_course_instalment__c, Date_Paid__c, Deposit__c, Invoice__c, Transfered_from_Deposit__c, Paid_From_Deposit__c, Payment_Type__c, Received_By__c, Received_By_Agency__c, Received_On__c, Related_Credit__c, Value__c, Confirmed_By__c, Confirmed_By_Agency__c, Confirmed_Date__c FROM client_course_instalment_payments__r), (SELECT Product_Name__c, Currency__c, Received_by__r.Name, Received_Date__c, Unit_Description__c, Quantity__c, Start_Date__c, End_Date__c,Price_Total__c FROM paid_products__r) FROM client_product_service__c WHERE (Id = :prodId OR Related_to_Product__c = :prodId)  AND Paid_with_product__c = NULL order by Related_to_Product__c nulls first];

		}
		catch(Exception e ){
			product = new list<client_product_service__c>();
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
		}

		retrieveDeposit();
	}

	//Retrieve Client's Deposit
	private void retrieveDeposit(){
		try{
		 	clientDeposit = [SELECT Deposit_Description__c, Deposit_Total_Used__c, Deposit_Total_Value__c, Deposit_Total_Available__c, Deposit_Type__c, Deposit_Value_Track__c, Id, client__c, Client__r.name, CurrencyIsoCode__c FROM client_course__c WHERE client__c = :product[0].Client__c and isDeposit__c = true and Deposit_Total_Available__c > 0 and CurrencyIsoCode__c = :product[0].Currency__c limit 1];

		}catch(Exception e){
			clientDeposit = new client_course__c(Deposit_Reconciliated__c = true, isDeposit__c = true, client__c = product[0].Client__c, Deposit_Total_Value__c =0, Deposit_Total_Used__c=0,Deposit_Total_Available__c=0, CurrencyIsoCode__c = product[0].Currency__c);
		}
	}

	//C A N C E L 	P R O D U C T 	P A Y M E N T
	public void cancelPayment(){
		Savepoint sp;
		// errorMsg = '';
		showError = false;
		try{
			sp = Database.setSavepoint();
			list<client_course_instalment_payment__c> toDelete  = new list<client_course_instalment_payment__c>();//Used for 'Covered by Agency'

			//Get all payments from the selected products
			Decimal coveredAgency = 0;
			Decimal totalPrice = 0;

			list<client_course_instalment_payment__c> toDeposit = new list<client_course_instalment_payment__c>();
			list<client_product_service__c> toUpdateProd = new list<client_product_service__c>();

			for(client_product_service__c p : product){

				if(p.isSelected__c){

					if(p.Covered_By_Agency__c!= null){
						coveredAgency += p.Covered_By_Agency__c;
					}

					for(client_course_instalment_payment__c ccip : p.client_course_instalment_payments__r){
						
						if(ccip.Payment_Type__c.toLowerCase() != 'covered by agency') 
							totalPrice += ccip.Value__c; //Total amount to be deposited to the client (must exclude covered by agency)

						toDeposit.add(ccip);

					}//end for

					for(client_product_service__c pp : p.paid_products__r)
						toUpdateProd.add(pp);
						
						
					toUpdateProd.add(p);


				}
			}//end for
			


			if((coveredAgency == 0 || coveredAgency != totalPrice) && clientDeposit.id==null) //Create deposit is there is non available
				insert clientDeposit;

			for(client_course_instalment_payment__c ccip: toDeposit){

				if(ccip.Payment_Type__c != 'Deposit'){

					if(ccip.Payment_Type__c.toLowerCase() == 'covered by agency'){
						toDelete.add(ccip);//delete payment
						// product.Covered_By_Agency__c -= ccip.Value__c;
					}
					else{
						clientDeposit.Deposit_Total_Available__c += ccip.Value__c;
						clientDeposit.Deposit_Total_Value__c += ccip.Value__c;
						ccip.deposit__c = clientDeposit.id;
						ccip.client_product_service__c = null;
						ccip.CurrencyIsoCode__c = product[0].Currency__c;

						//Currency
						ccip.Agency_Currency__c = ccip.CurrencyIsoCode__c;
						ccip.Agency_Currency_Rate__c = 1;
						ccip.Agency_Currency_Value__c = ccip.Value__c;

						if(ccip.Confirmed_Date__c == null)
							clientDeposit.Deposit_Reconciliated__c = false;
					}
				}
				else{
					clientDeposit.Deposit_Total_Available__c += ccip.Value__c;
					if(ccip.Paid_From_Deposit__c == clientDeposit.id){
						clientDeposit.Deposit_Total_Used__c -= ccip.Value__c;
						toDelete.add(ccip);//delete deposit
					}else{
						ccip.Transfered_from_Deposit__c = ccip.Paid_From_Deposit__c;
						ccip.deposit__c = clientDeposit.id;
						ccip.CurrencyIsoCode__c = product[0].Currency__c;
						ccip.client_product_service__c = null;
						clientDeposit.Deposit_Total_Value__c += ccip.Value__c;
						ccip.Payment_Type__c = 'Transferred from Deposit';
						ccip.Confirmed_By__c = currentUser.id;
						ccip.Confirmed_Date__c = system.now();
						ccip.Confirmed_By_Agency__c = currentUser.Contact.AccountId;

						//Currency
						ccip.Agency_Currency__c = ccip.CurrencyIsoCode__c;
						ccip.Agency_Currency_Rate__c = 1;
						ccip.Agency_Currency_Value__c = ccip.Value__c;
					}
				}
			}//end for
			if(clientDeposit.id!=null)
				update clientDeposit;
		
			
			for(client_product_service__c p : toUpdateProd){

				p.Received_By_Agency__c = null;
				p.Received_by__c =  null;
				p.Received_by_Department__c =  null;
				p.Received_Date__c =  null;
				p.Creditcard_debit_date__c =  null;
				p.Confirmed_By__c = null;
				p.Confirmed_Date__c = null;
				p.isPaidOffShore__c = false;

				p.Agency_Currency__c = null;
				p.Agency_Currency_Value__c = 0;
				p.Agency_Currency_Rate__c = 0;
				p.Covered_By_Agency__c = 0;
				p.isSelected__c = false;
				p.Paid_with_product__c = null;


				p.Paid_to_Provider_by__c = null;
				p.Paid_to_Provider_by_Agency__c = null;
				p.Paid_to_Provider_on__c = null;

			}//end for

		
			update toDeposit;

			if(toDelete.size()>0) //Delete 'Client Scholarship' and 'Covered by Agency'
				delete toDelete;

			update toUpdateProd;

			showError = true;

		}catch(Exception e){
			String msg = 'Error===>' + e + ' Line:' + e.getLineNumber();
		   	system.debug('Error==>' + msg);
		   	ApexPages.addMessages(e);
			errorMsg += msg;
			Database.rollback(sp);
		}
	}
}