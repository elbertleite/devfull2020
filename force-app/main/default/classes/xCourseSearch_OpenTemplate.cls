public class xCourseSearch_OpenTemplate{
	
	public xCourseSearch_OpenTemplate(){
		getUserDetails();
		agencyShared = UserDetails.Agency_Id;
		openTemplates();
		
		
	}
	
	private xCourseSearchPersonDetails.UserDetails UserDetails;
	public xCourseSearchPersonDetails.UserDetails getUserDetails(){
		if(UserDetails == null){
			xCourseSearchPersonDetails spd = new xCourseSearchPersonDetails();
			UserDetails = spd.getAgencyDetails();
		}
		return UserDetails;
	}
	
	private string agencyShared;// = UserDetails.Agency_Id;

	public list<Search_Course_Template__c> Templates {get{if(Templates == null) Templates = new list<Search_Course_Template__c>(); return Templates;} set;}
	public list<Search_Course_Template__c> TemplatesShared {get{if(TemplatesShared == null) TemplatesShared = new list<Search_Course_Template__c>(); return TemplatesShared;} set;}
	
	
	public void openTemplates(){ 
		for(Search_Course_Template__c st:[Select Courses__c, Description__c, Name__c, Id, Shared_with_Agency__c, Courses_Details__c, isSelected__c, CreatedById, CreatedBy.Name from Search_Course_Template__c where CreatedById = :UserInfo.getUserId() or (CreatedById != :UserInfo.getUserId() and Shared_with_Agency__c = true and Agency_Shared__c = :agencyShared) order by CreatedBy.Name, Name__c]){
			if(st.CreatedById == UserInfo.getUserId())
				Templates.add(st);
			//else 
			if(st.Shared_with_Agency__c)
				TemplatesShared.add(st);
		}
	}
	
	public PageReference saveChanges(){
		for(Search_Course_Template__c st:Templates)
			st.isSelected__c = false;
		update Templates;
		return null;
	}
	
	
	public PageReference deleteSelected(){
		list<string> listDelete = new list<string>();
		for(Search_Course_Template__c t:Templates){
			if(t.isSelected__c)
				listDelete.add(t.id);
		}
		if(listDelete.size() > 0)
			delete [Select Id from Search_Course_Template__c where Id in :listDelete];
		
		Templates = null;
		TemplatesShared = null;
		openTemplates();
		
		return null;
	}
	
		
}