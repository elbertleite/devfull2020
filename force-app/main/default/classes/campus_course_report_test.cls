/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class campus_course_report_test {

    static testMethod void myUnitTest() {
        
        
        Map<String,String> recordTypes = new Map<String,String>();
       
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
		
		Account agency = new Account();
		agency.name = 'IP Sydney';
		agency.RecordTypeId = recordTypes.get('Agency');
		agency.BillingCountry = 'Australia';
		insert agency;
		
		
		Account school = new Account();
		school.recordtypeid = recordTypes.get('School');
		school.name = 'Test School';
		school.BillingCountry = 'Australia';
		school.BillingCity = 'Sydney';		
		insert school;
		
		Product__c p = new Product__c();
		p.Name__c = 'Enrolment Fee';
		p.Product_Type__c = 'Other';
		p.Supplier__c = school.id;
		insert p;
		
		
		Product__c p2 = new Product__c();
		p2.Name__c = 'Elberts Homestay';
		p2.Product_Type__c = 'Accommodation';
		p2.Supplier__c = school.id;
		insert p2;
		
		Product__c p3 = new Product__c();
		p3.Name__c = 'Accommodation Placement Fee';
		p3.Product_Type__c = 'Accommodation';
		p3.Supplier__c = school.id;
		insert p3;
		
		
		Nationality_Group__c ng = new Nationality_Group__c();
		ng.Account__c = school.id;
		ng.Country__c = 'Brazil';
		ng.Name = 'Latin America';
		insert ng;
       
		Account campus = new Account();
		campus.RecordTypeId = recordTypes.get('Campus');
		campus.Name = 'Test Campus CBD';
		campus.BillingCountry = 'Australia';
		campus.BillingCity = 'Sydney';
		campus.ParentId = school.id;
		insert campus;
		
		
		Course__c course = new Course__c();
		course.Name = 'Certificate III in Business';        
		course.Type__c = 'Business';
		course.Sub_Type__c = 'Business';
		course.Course_Qualification__c = 'Certificate III';
		course.Course_Type__c = 'English/ELICOS';
		course.Course_Unit_Type__c = 'Week';
		course.Only_Sold_in_Blocks__c = true;
		course.Period__c = 'Morning';
		course.Minimum_length__c = 1;
		course.Maximum_length__c = 52;
		course.School__c = school.id;
		insert course;
		
		Course__c course2 = new Course__c();
		course2.Name = 'Certificate I in Business';        
		course2.Type__c = 'Business';
		course2.Sub_Type__c = 'Business';
		course2.Course_Qualification__c = 'Certificate III';
		course2.Course_Type__c = 'English/ELICOS';
		course2.Course_Unit_Type__c = 'Week';
		course2.Only_Sold_in_Blocks__c = false;
		course2.Period__c = 'Evening';
		course2.Minimum_length__c = 1;
		course2.Maximum_length__c = 52;
		course2.School__c = school.id;
		insert course2;
       
		Campus_Course__c cc = new Campus_Course__c();
		cc.Course__c = course.Id;
		cc.Campus__c = campus.Id;        
		cc.Is_Available__c = true;
		cc.is_Selected__c = true;      
		insert cc;
		
		Campus_Course__c cc2 = new Campus_Course__c();
		cc2.Course__c = course2.Id;
		cc2.Campus__c = campus.Id;        
		cc2.Is_Available__c = true;
		cc2.is_Selected__c = true;      
		insert cc2;
		
		
		Course_Price__c cp = new Course_Price__c();
		
		cp.Nationality__c = 'Published Price';
		cp.From__c = 1;
		cp.Price_per_week__c = 150;
		cp.Availability__c = 3.0;
		cp.Campus_Course__c = cc.Id;
		cp.Price_valid_from__c = system.today();
		cp.Price_valid_until__c = system.today().addYears(1);        
		insert cp;
		
		Course_Price__c cp2 = new Course_Price__c();
		
		cp2.Nationality__c = 'Latin America';
		cp2.From__c = 5;
		cp2.Price_per_week__c = 150;
		cp2.Availability__c = 3.0;
		cp2.Campus_Course__c = cc2.Id;
		cp2.Price_valid_from__c = system.today();
		cp2.Price_valid_until__c = system.today().addYears(1);       
		insert cp2;
		
		
		Course_Intake_Date__c cid = new Course_Intake_Date__c();
		cid.Campus_Course__c = cc.id;
		cid.Intake_Date__c = System.today().addDays(365);
		insert cid;
		
		Course_Intake_Date__c cid2 = new Course_Intake_Date__c();
		cid2.Campus_Course__c = cc2.id;
		cid2.Intake_Date__c = System.today().addDays(365);
		insert cid2;
        
		Course_Extra_Fee__c cef = new Course_Extra_Fee__c();
		cef.Campus_Course__c = cc.Id;
		cef.Nationality__c = 'Latin America';
		cef.Availability__c = 3;
		cef.From__c = 1;
		cef.Value__c = 100;		
		cef.Product__c = p2.id;
		cef.date_paid_from__c = system.today();
		cef.date_paid_to__c = system.today().addDays(+30);
		insert cef;
		
		
		
		Course_Extra_Fee_Dependent__c cefd = new Course_Extra_Fee_Dependent__c();
		cefd.Course_Extra_Fee__c = cef.id;
		cefd.Product__c = p3.id;
		cefd.Details__c = 'Testing';
		cefd.Value__c = 150;
		insert cefd;
		
        
		Course_Extra_Fee__c cef2 = new Course_Extra_Fee__c();
		cef2.Campus__c = campus.Id;
		cef2.Nationality__c = 'Published Price';
		cef2.Availability__c = 3;
		cef2.From__c = 2;
		cef2.Value__c = 100;		
		cef2.Optional__c = false;
		cef2.Product__c = p.id;
		cef2.date_paid_from__c = system.today();
		cef2.date_paid_to__c = system.today().addDays(+31);
		insert cef2;
		
		Course_Extra_Fee__c combined = new Course_Extra_Fee__c();
		combined.Combine_fees__c = 'Tourism Passes';
		combined.Campus_Course__c = cc2.Id;
		combined.Nationality__c = 'Latin America';
		combined.Availability__c = 3;
		combined.From__c = 1;
		combined.Value__c = 100;		
		combined.Product__c = p.id;
		combined.date_paid_from__c = system.today();
		combined.date_paid_to__c = system.today().addDays(+32);
		combined.date_start_from__c = system.today();
		combined.date_start_to__c = system.today().addDays(+120);
		combined.date_start_value__c = 1500;
		insert combined;
		
		Course_Extra_Fee__c combined2 = new Course_Extra_Fee__c();
		combined2.Combine_fees__c = 'Tourism Passes';
		combined2.Campus_Course__c = cc2.Id;
		combined2.Nationality__c = 'Latin America';
		combined2.Availability__c = 3;
		combined2.From__c = 2;
		combined2.Value__c = 100;		
		combined2.Product__c = p2.id;
		combined2.date_paid_from__c = system.today();
		combined2.date_paid_to__c = system.today().addDays(+60);
		combined2.date_start_from__c = system.today();
		combined2.date_start_to__c = system.today().addDays(+60);
		combined2.date_start_value__c = 100;
		insert combined2;
		
		
		Partner_Relationship__c pr = new Partner_Relationship__c();
		pr.Country__c = 'Australia';
		pr.School__c = school.id;
		pr.Relationship__c = 3;
		pr.Priority__c = 3;
		pr.From__c = system.today().addDays(-30);
		pr.To__c  = system.today().addDays(350);
		insert pr;
		
		Deal__c d = new Deal__c();
		d.Availability__c = 3;
		d.Campus_Account__c = campus.id;
		d.From__c = 1;
		d.Extra_Fee__c = cef2.id;
		d.Promotion_Type__c = 3;
		d.From_Date__c = system.today();
		d.To_Date__c = system.today().addDays(60);
		d.Extra_Fee_Type__c = 'Cash';
		d.Extra_Fee_Value__c = 60;
		d.Product__c = p.id;
		d.Nationality_Group__c = 'Publish';
		insert d;
		
		
		Deal__c d2 = new Deal__c();
		d2.Availability__c = 3;
		d2.Campus_Course__c = cc2.id;
		d2.From__c = 1;		
		d2.Promotion_Type__c = 2;
		d2.From_Date__c = system.today();
		d2.To_Date__c = system.today().addDays(60);
		d2.Promotion_Weeks__c = 1;
		d2.Nationality_Group__c = 'Latin America';
		d2.Promotion_Name__c = '1+1';
		d2.Promo_Price__c = 50;		
		insert d2;
		
		
		
		Deal__c d3 = new Deal__c();
		d3.Availability__c = 3;
		d3.Campus_Course__c = cc.id;
		d3.From__c = 1;
		d3.Extra_Fee__c = cef.id;
		d3.Promotion_Type__c = 3;
		d3.From_Date__c = system.today();
		d3.To_Date__c = system.today().addDays(60);
		d3.Extra_Fee_Type__c = 'Cash';
		d3.Extra_Fee_Value__c = 50;
		d3.Nationality_Group__c = 'Latin America';
		insert d3;
		
		Deal__c d4 = new Deal__c();
		d4.Availability__c = 3;
		d4.Campus_Course__c = cc.id;
		d4.From__c = 1;		
		d4.Promotion_Type__c = 2;
		d4.Promotion_Name__c = '1+2';
		d4.Promo_Price__c = 50;		
		d4.From_Date__c = system.today();
		d4.To_Date__c = system.today().addDays(61);
		d4.Promotion_Weeks__c = 2;		
		d4.Nationality_Group__c = 'Published Price';
		insert d4;
		
		Deal__c d5 = new Deal__c();
		d5.Availability__c = 3;
		d5.Campus_Course__c = cc.id;
		d5.Extra_Fee_Dependent__c = cefd.id;
		d5.From__c = 1;		
		d5.Promotion_Type__c = 3;
		d5.From_Date__c = system.today();
		d5.To_Date__c = system.today().addDays(62);
		d3.Extra_Fee_Type__c = 'Cash';
		d3.Extra_Fee_Value__c = 50;
		d5.Nationality_Group__c = 'Latin America';
		d5.Product__c = p3.id;
		//insert d5;
		
		
		Web_Search__c ws = new Web_Search__c();
		ws.Email__c = 'iptest@ip.com';
		ws.Country__c = 'Australia';
		insert ws;
		
		Search_Courses__c sc = new Search_Courses__c();
		sc.Campus_Course__c = cc.id;
		sc.Web_Search__c = ws.id;
		sc.Number_of_Units__c = 10;
		insert sc;
		
        
        Test.startTest();
        
        campus_course_report ccr = new campus_course_report(new Apexpages.Standardcontroller(campus));
        ccr.getnationalityGroupHint();
        ccr.getCourseAvailableList();
        ccr.getselectedCourseTypeOptions();
        ccr.getNationalitiesEdit();
        ccr.selectedNationalities = new List<String>{'Published Price', 'Latin America'};
        ccr.getPeriods();
        ccr.getCourseListClone();        
        ccr.courseClone = new List<String>{cc.Id, cc2.id};
        
            
        
        ccr.getCourseDetails();
        
        
        
        
        
        Test.stopTest();
        
        
        
        
        
        
        
    }
}