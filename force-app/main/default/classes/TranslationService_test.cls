/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TranslationService_test {

    static testMethod void myUnitTest() {
        
        TestFactory tf = new TestFactory();
        Account agency = tf.createAgency();		
		Contact employee = tf.createEmployee(agency);
        Account school = tf.createSchool();
        Account campus = tf.createCampus(school, agency);		    
		
		List<Course__c> oldCourses = new list<Course__c>();
		List<Course__c> newCourses = new list<Course__c>();
		Course__c course = tf.createCourse();		
		Course__c course2 = tf.createLanguageCourse();				
		oldCourses.add(course);
		oldCourses.add(course2);
		course2.Course_Type__c = 'Agriculture';		
		newCourses.add(course);
		newCourses.add(course2);
		
		
		List<Campus_Course__c> oldCC = new list<Campus_Course__c>();
		List<Campus_Course__c> newCC = new list<Campus_Course__c>();
		Campus_Course__c cc = tf.createCampusCourse(campus, course);		
		Campus_Course__c cc2 = tf.createCampusCourse(campus, course2);
		oldcc.add(cc);
		oldCC.add(cc2);
		cc2.Language_Level_Required__c = 'Advanced';
		update cc2;
		newCC.add(cc);
		newCC.add(cc2);
		
		List<Translation__c> translations = new List<Translation__c>();
		translations.add(tf.populateCourseTranslation(cc));
		translations.add(tf.populateCourseTranslation(cc2));
		
		course2.name = 'Diploma in Cattle Management';
		update course2;
        
        Test.startTest();
		TranslationService ts = new TranslationService();
		TranslationService.updateWorkbenchTranslations(UserInfo.getSessionID(), translations);
		
		TranslationService.isCampusCourseTranslationRequired(oldCC, newCC);
		TranslationService.isCourseTranslationRequired(oldCourses, newCourses);
		
		Test.stopTest();
        
    }
}