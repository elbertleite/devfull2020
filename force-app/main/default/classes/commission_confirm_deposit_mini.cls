public with sharing class commission_confirm_deposit_mini {

	private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}
	private User currentUser {get{if(currentUser==null) currentUser = ff.currentUser; return currentUser;}set;}
	private Map<String, double> agencyCurrencies;


	public DateTime currencyLastModifiedDate {get;set;}
	public list<SelectOption> mainCurrencies {get;set;}
	public list<s3Docs> invDocs {get;set;}
	public list<IPFunctions.instalmentActivity> invActivities {get;set;}
	public Invoice__c invoice {get;set;}
	public Invoice__c invReason {get{if(invReason == null) invReason = new Invoice__c(); return invReason;}set;}
	public Boolean isConfirm {get{if(isConfirm == null) isConfirm = false; return isConfirm;}set;}

	//C O N S T R U C T O R
	public commission_confirm_deposit_mini() {
		mainCurrencies = ff.retrieveMainCurrencies();
		agencyCurrencies = ff.retrieveAgencyCurrencies();
		currencyLastModifiedDate = ff.currencyLastModifiedDate;

		String invoiceId = ApexPages.CurrentPage().getParameters().get('id');

		String sql = 'SELECT ID, Share_Commission_Number__c, Total_Value__c, Paid_On__c, Requested_by_Agency__r.Name, Requested_Commission_To_Agency__c, Requested_Commission_To_Agency__r.Name, Total_Instalments_Requested__c, Payment_Receipt__r.preview_link__c, Commission_Paid_Date__c, Invoice_Activities__c, Confirmed_Date__c, Paid_by__r.Name, Paid_by_Agency__r.Name, Paid_Date__c, Total_Emails_Sent__c, Sent_Email_To__c, Sent_On__c,CurrencyIsoCode__c, Country__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, Transfer_Send_Fee__c,  Transfer_Send_Fee_Currency__c, Transfer_Receive_Fee__c, Requested_Commission_for_Agency__r.Account_Currency_Iso_Code__c, Due_Date__c, Summary__c, Received_Currency__c, Received_Currency_Rate__c, Received_Currency_Value__c, Transfer_Receive_Fee_Currency__c FROM Invoice__c WHERE id = :invoiceId';

		invoice = Database.query(sql);

		if(invoice.Commission_Paid_Date__c == NULL){
			isConfirm = true;
			invoice.Received_Currency__c = currentUser.Contact.Account.account_currency_iso_code__c;
			invoice.Transfer_Receive_Fee_Currency__c = invoice.Received_Currency__c;
			invoice.Received_Currency_Rate__c = agencyCurrencies.get(invoice.CurrencyIsoCode__c);

			ff.convertCurrency(invoice, invoice.CurrencyIsoCode__c, invoice.Total_Value__c, null, 'Received_Currency__c', 'Received_Currency_Rate__c', 'Received_Currency_Value__c');
		}

		//Payment Documents
		if(invoice.Payment_Receipt__r.preview_link__c!=null){
			invDocs = new list<s3Docs>();	
			for(String doc : invoice.Payment_Receipt__r.preview_link__c.split(FileUpload.SEPARATOR_FILE))
				invDocs.add(new S3Docs(doc.split(FileUpload.SEPARATOR_URL)));
		}

		//Invoice Activities
		if(invoice.Invoice_Activities__c!=null && invoice.Invoice_Activities__c != ''){
			invActivities = new list<IPFunctions.instalmentActivity>();	
			List<String> allAc = invoice.Invoice_Activities__c.split('!#!');

			for(Integer ac = (allAc.size() - 1); ac >= 0; ac--){//activity order desc

				IPFunctions.instalmentActivity instA = new IPFunctions.instalmentActivity();
				List<String> a = allAc[ac].split(';;');

				instA.acType = a[0];
				instA.acTo = a[1];
				instA.acSubject = a[2];
				if(instA.acType=='SMS') // saved date in system mode
					instA.acDate = new Contact(Lead_Accepted_On__c = Datetime.valueOfGmt(a[3]));
				else
					instA.acDate = new Contact(Lead_Accepted_On__c = Datetime.valueOf(a[3]));
				instA.acStatus = a[4];
				instA.acError = a[5];
				instA.acFrom = a[6];

				system.debug('single activity==' + instA);

				invActivities.add(instA);
			}
		}
	}

	//C O N F I R M 	P A Y M E N T 
	public void confirmDeposit(){
		system.debug(' Received_Currency_Rate__c==>' + invoice.Received_Currency_Rate__c);
		system.debug(' Received_Currency_Value__c==>' + invoice.Received_Currency_Value__c );
		system.debug(' Transfer_Receive_Fee__c==>' +invoice.Transfer_Receive_Fee__c );
		system.debug(' Commission_Paid_Date__c==>' +invoice.Commission_Paid_Date__c );
		if(invoice.Received_Currency_Rate__c == 0){
			invoice.Received_Currency_Rate__c.addError('Has to be greater than 0');
		}
		else if(invoice.Commission_Paid_Date__c > system.today()){
			invoice.Commission_Paid_Date__c.addError('Cannot be future date');
		}
		else{
			ff.confirmShareCommDeposit(invoice);
		}
	}

	
	//U N C O N F I R M 	P A Y M E N T 
	public void unconfirmDeposit(){
		ff.unconfirmShareCommDeposit(invoice, invReason);
	}




	public class s3Docs{
		public String docName {get;set;}
		public String docUrl {get;set;}

		public s3Docs (List<String> docParts){
			this.docUrl = docParts[0];
			this.docName = docParts[1];
		}
    }
}