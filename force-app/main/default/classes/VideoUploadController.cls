public class VideoUploadController {
	
	public video_upload__c video {get;set;}
	public boolean edit {get;set;}	
	public boolean isInternalUser {get;set;}	
	public map<string, list<vimeoVideo>> listVideos {get{
		if(listVideos==null){
			listVideos = new map<string, list<vimeoVideo>>();
			for(Video_Upload__c video : [Select Id, Name, Category__c, Video_Key__c, Description__c, createdDate, LastModifiedDate, LastModifiedBy.Name From Video_Upload__c WHERE objectType__c = null order by name]){
				vimeoVideo v = new vimeoVideo();
				v.id = video.Id;
				v.name = video.Name;
				v.url = 'https://player.vimeo.com/video/'+video.Video_Key__c;
				v.description = video.Description__c;
				v.key = video.Video_Key__c;
				v.createdDate = video.createdDate;
				v.LastModifiedDate = video.LastModifiedDate;
				v.LastModifiedBy = video.LastModifiedBy.Name;
			
				if(!listVideos.containsKey(video.category__c))
					listVideos.put(video.category__c, new list<vimeoVideo>{v});
				else
					listVideos.get(video.category__c).add(v);
			}
		}
		return listVideos;	
	}set;}
	
	

	
	/**
		Constructor
	**/
	public VideoUploadController(){
		isInternalUser = IPFunctions.isInternalUser(UserInfo.getUserId());
		edit = false;
	}
	
	/**
		New 
	**/
	public void newVideo(){
		edit = true;
		video = new video_upload__c();
	}
	
	/**
		Edit 
	**/
	public void editVideo(){
		edit = true;
		video = [Select Name, Category__c, Video_Key__c, Description__c from Video_Upload__c where id =: ApexPages.currentPage().getParameters().get('idVideo')];
	}
	
	/**
		Delete 
	**/
	public void deleteVideo(){
		Database.delete(ApexPages.currentPage().getParameters().get('idVideo'));
		listVideos = null;
	}
	
		
	/**
		Get Thumbnails 
	**/
	public void videosThumbnails(){
		string mapKey = ApexPages.currentPage().getParameters().get('mapKey');
		system.debug('MapKey==>' + mapKey);
		list<vimeoVideo> upVideos = new list<vimeoVideo>();
		
		for(vimeoVideo v : listVideos.get(mapKey)){
			system.debug('==>' + v.name);
			Http h = new Http();
	        HttpRequest req = new HttpRequest();
	        // url that returns the XML in the response body
	        req.setEndpoint('http://vimeo.com/api/oembed.xml?url=http%3A//vimeo.com/'+v.key);
	        req.setMethod('GET');
	        
	        if(!Test.isRunningTest()){
		        HttpResponse res = h.send(req);
		        system.debug('Response==>' + res);
		        
		        if(res.getStatus()=='OK'){
			        Dom.Document doc = res.getBodyDocument();
			        
			        Dom.XMLNode oembed = doc.getRootElement();
			        system.debug('XML===>' + doc);
			        
			        String thumb = oembed.getChildElement('thumbnail_url', null).getText();
			      	v.urlThumbnail = thumb;
		        }
		      	upVideos.add(v);
	        }
	        
		}
				
      	listVideos.get(mapKey).clear();
      	upVideos.sort();
      	listVideos.get(mapKey).addAll(upVideos);
	}
	
	/**
		Save 
	**/
	public void save(){
		try{
			upsert video;
			edit=false;
			listVideos = null;
			video = new video_upload__c();
			
		}catch(Exception e){
			system.debug('Error===>' + e);	
		}
	}
		
	/**
		Cancel 
	**/
	public void cancel(){
		edit=false;
	}
		
	/**
		Inner Class
	**/
	public class vimeoVideo implements Comparable{
		public string id {get;set;}
		public string name {get;set;}
		public string key {get;set;}
		public string url {get;set;}
		public string urlThumbnail {get;set;}
		public string description {get;set;}
		public string shortDescription {get;set;}
		public DateTime createdDate {get;set;}
		public DateTime LastModifiedDate {get;set;}
		public string LastModifiedBy {get;set;}
		
	 	public Integer compareTo(Object compareTo){
	        vimeoVideo vimeoVideocompareTo = (vimeoVideo) compareTo;
	        if (name == vimeoVideocompareTo.name) return 0;
	        if (name > vimeoVideocompareTo.name) return 1;
	        return -1;        
		}
		
	}
	
}