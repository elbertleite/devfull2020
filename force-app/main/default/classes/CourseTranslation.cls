public class CourseTranslation {

	private final String TRANSLATION_TYPE_COURSE = 'course';

	//<CourseID, CourseWrapper>
	public Map<String, CourseWrapper> coursesMap {get;set;}

	//<CampusCourseID, <LangCode, Translation>>
	private Map<String, Map<String, Translation__c>> translationsMap;

	public String schoolName {get;set;}
	public String schoolID;
	public Set<ID> orderedCourses {get;set;}

	public CourseTranslation(){
		schoolID = ApexPages.currentPage().getParameters().get('id');
		getCourses( schoolID );

		String language = ApexPages.currentPage().getParameters().get('lang');

		if(language != null){
			selectedLanguage = language;
			changeLanguage();
		}
	}

	public boolean foundCourses {get{ if(foundCourses == null) foundCourses = false; return foundCourses; }set;}
	
	private void getCourses(String id){
		coursesMap = new Map<String, CourseWrapper>();
		translationsMap = new Map<String, Map<String, Translation__c>>();
		orderedCourses = new Set<ID>();
		foundCourses = false;
		
		schoolName = [select name from Account where id = :id limit 1].Name;
		
		for(Campus_Course__c cc : [select campus__r.Parent.Name, Course__r.Name, Course__r.Course_Category__c, Course__r.Type__c, course__r.Course_Type__c, course__r.Form_of_Delivery__c, Period__c, Language_Level_Required__c, Campus__r.Name,
											Course__r.School__r.Name,
											(Select course_name__c, Language__c, course_field__c, course_category__c, course_form_of_delivery__c, course_language_level_required__c, course_period__c, course_type__c, 
													Status__c, Updated_Fields__c
											 from translations__r)
				 					from Campus_Course__c where campus__r.ParentId = :id and Is_Available__c = true order by Course__r.Name]){

			orderedCourses.add(cc.Course__c);
			foundCourses = true;
			

			if(coursesMap.containsKey(cc.course__c)){
				CampusCourseWrapper ccw = new CampusCourseWrapper();
				ccw.campusCourse = cc;
				coursesMap.get(cc.course__c).campusCourses.add(ccw);
			} else {
				CourseWrapper c = new CourseWrapper();
				c.course = cc.Course__r;
				CampusCourseWrapper ccw = new CampusCourseWrapper();
				ccw.campusCourse = cc;
				c.campusCourses.add(ccw);
				coursesMap.put(cc.course__c, c);
			}


			for(Translation__c tr : cc.translations__r){
				if(translationsMap.containsKey(cc.id))
					translationsMap.get(cc.id).put(tr.Language__c, tr);
				else
					translationsMap.put(cc.id, new Map<String, Translation__c>{tr.Language__c => tr});
			}
		}
		
		if(!foundCourses)
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, 'There are no courses to be translated.'));

	}

	public void refreshLanguages(){
		getCourses(schoolID);
		changeLanguage();
	}

	public void changeLanguage(){

		for(CourseWrapper cw : coursesMap.values())
			for(CampusCourseWrapper ccw : cw.campusCourses)
				if(translationsMap.containsKey(ccw.campusCourse.id) && translationsMap.get(ccw.campusCourse.id).containsKey(selectedLanguage)){
					ccw.translation = translationsMap.get(ccw.campusCourse.id).get(selectedLanguage);
					cw.translatedName = translationsMap.get(ccw.campusCourse.id).get(selectedLanguage).course_name__c;
					cw.translatatedNameOriginal = translationsMap.get(ccw.campusCourse.id).get(selectedLanguage).course_name__c;
					cw.category = translationsMap.get(ccw.campusCourse.id).get(selectedLanguage).course_category__c;
					cw.type = translationsMap.get(ccw.campusCourse.id).get(selectedLanguage).course_type__c;
					cw.field = translationsMap.get(ccw.campusCourse.id).get(selectedLanguage).course_field__c;
					cw.formOfDelivery = translationsMap.get(ccw.campusCourse.id).get(selectedLanguage).course_form_of_delivery__c;
					cw.updateRequired = translationsMap.get(ccw.campusCourse.id).get(selectedLanguage).status__c == 'Update Required';
				} else
					ccw.translation = new Translation__c(Type__c = TRANSLATION_TYPE_COURSE, Campus_Course__c = ccw.campusCourse.id, Language__c = selectedLanguage);

	}


	private Set<ID> updatedTranslations;
	public void save(){

		List<Translation__c> translations = new List<Translation__c>();
		List<Translation__c> removedTranslations = new List<Translation__c>();

		for(CourseWrapper cw : coursesMap.values()){
			for(CampusCourseWrapper ccw : cw.campusCourses){
				if(cw.translatedName != null && cw.translatedName.trim() != '' && cw.translatedName != cw.translatatedNameOriginal){
					ccw.translation.course_name__c = cw.translatedName;
					ccw.translation.Status__c = 'Completed';
					ccw.translation.Progress__c = 100;
					ccw.translation.Updated_Fields__c = null;
					translations.add(ccw.translation);
				} else if( ccw.translation.id != null && (cw.translatedName == null || cw.translatedName.trim() == '') && cw.translatedName != cw.translatatedNameOriginal)
					removedTranslations.add(ccw.translation);
			}
		}

		delete removedTranslations;


		if(!translations.isEmpty()){
			updatedTranslations = new Set<ID>();
			Database.UpsertResult[] res = Database.upsert(translations, true);
			for(Database.UpsertResult ur : res)
				updatedTranslations.add(ur.getId());

		}

		ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM, 'Translation saved.'));

	}


	public void updateFromWorkbench(){
		if(updatedTranslations != null){
			List<Translation__c> trans = [Select id, Language__c, course_field__c, course_category__c, course_form_of_delivery__c, course_language_level_required__c, course_period__c, course_type__c,
												Campus_Course__r.Course__r.Course_Category__c, Campus_Course__r.Course__r.Course_Type__c, Campus_Course__r.Course__r.Type__c, Campus_Course__r.Course__r.Form_of_Delivery__c,
												Campus_Course__r.Period__c, Campus_Course__r.Language_Level_Required__c
												from translation__c where id in :updatedTranslations];

			TranslationService.updateWorkbenchTranslations(UserInfo.getSessionID(), trans);

			refreshLanguages();
		}
	}


	private Map<String, String> sysLanguages = new Map<String, String>();
	public String selectedLanguage {get;set;}
	private List<SelectOption> languages;
	public List<SelectOption> getLanguages(){

		if(languages == null){
			languages = new List<SelectOption>();
			languages.add(new SelectOption('', '-- Select Language --'));
			Schema.DescribeFieldResult fieldResult = Contact.Preferable_Language__c.getDescribe();
   			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
   			for(Schema.PicklistEntry f : ple){
      			languages.add(new SelectOption(f.getLabel(), IPFunctions.getLanguage(f.getLabel())));
      			sysLanguages.put(f.getLabel(), IPFunctions.getLanguage(f.getLabel()));
   			}
		}
		//languages.sort();
		return languages;
	}

	public void updateAllTranslations(){
		Database.executeBatch(new batch_translations(Userinfo.getSessionID()));
		ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.INFO, 'Batch queued successfully.'));
	}


	public class CourseWrapper {
		public Course__c course { get;set; }
		private String translatatedNameOriginal;
		public String translatedName {get;set;}
		public String category {get;set;}
		public String type {get;set;}
		public String field {get;set;}
		public String formOfDelivery {get;set;}
		public boolean updateRequired {get {if(updateRequired == null) updateRequired = false; return updateRequired; } set;}
		public List<CampusCourseWrapper> campusCourses {get{if(campusCourses == null) campusCourses = new List<CampusCourseWrapper>(); return campusCourses; }set;}
	}

	public class CampusCourseWrapper {
		public Campus_Course__c campusCourse {get;set;}
		public Translation__c translation {get;set;}
	}




}