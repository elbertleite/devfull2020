public with sharing class agency_Campus_Availability {
	public string selectedSchoolId{get; set;}
	public string selectedAgencyCountry{get; set;}
	public string selectedSchoolCountry{get; set;}
	public string selectedAgency{get; set;}
	
	private List<SelectOption> agencyCountries;
	public List<SelectOption> getagencyCountries() {
		if(agencyCountries == null){
			agencyCountries = new List<SelectOption>();
					
			agencyCountries.add(new SelectOption('','Select Country'));
			for (AggregateResult ar:[Select BillingCountry country from Account where recordType.name in ('Agency') AND BillingCountry != null group by BillingCountry order by BillingCountry])
				agencyCountries.add(new SelectOption((string) ar.get('country'),(string) ar.get('country')));
		}
		return agencyCountries;
	}
	
	public pageReference refreshAgency(){
		selectedSchoolCountry = '';
		return null;
	}
	
	public List<SelectOption> getagenciesPerCountry() {
		
		List<SelectOption> agenciesPerCountry = new List<SelectOption>();
		
		agenciesPerCountry.add(new SelectOption('','Select Agency'));
		for (Account age:[Select id, Name from Account where recordType.name in ('Agency') and BillingCountry = :selectedAgencyCountry order by Name])
			agenciesPerCountry.add(new SelectOption(age.id,age.Name));
		
		return agenciesPerCountry;
	}
	
	//SELECT OPTION  -- Select School :: list schools for one country  
  public list<SelectOption> getschoolsCountry(){    
    list<SelectOption> schoolsCountry = new List<SelectOption>();
    System.debug('==> selectedSchoolCountry: '+selectedSchoolCountry);
    
    schoolsCountry.add(new SelectOption('','Select School'));
    if(selectedSchoolCountry != null){
     
      for (AggregateResult sch:[Select parentid pid, parent.name pname from Account WHERE recordType.name = 'campus' and billingcountry = :selectedSchoolCountry GROUP BY parentid, parent.name order by parent.name])
        schoolsCountry.add(new SelectOption((string) sch.get('pid'), (string) sch.get('pname')));        
    }
    
    return schoolsCountry;
  }
  
  // SELECT OPTION 	-- Select Country (School filter)
	private List<SelectOption> schoolCountries;
	public List<SelectOption> getschoolCountries() {
		if(schoolCountries == null){
			schoolCountries = new List<SelectOption>();
					
			schoolCountries.add(new SelectOption('','Select Country'));
			for (AggregateResult ar:[Select BillingCountry country from Account where recordType.name = 'campus' and BillingCountry != null group by BillingCountry order by BillingCountry])
				schoolCountries.add(new SelectOption((string) ar.get('country'),(string) ar.get('country')));
		}
		return schoolCountries;
	}
	
	// SELECT OPTION   --  Select Campus (School filter) :: list campus for a one school of one country
  public list<SelectOption> getschoolCampus(){    
    list<SelectOption> schoolCampus = new List<SelectOption>();
    schoolCampus.add(new SelectOption('','Select Campus'));
    
    for(Account sp:[Select id, name from Account WHERE parentId = :selectedSchoolId and BillingCity != null order by name]){
      schoolCampus.add(new SelectOption(sp.id, sp.name));
    }
        
    return schoolCampus;
  }
	
	private map<string, Supplier__c> selectedCampusesSupplier;
	private map<string, Supplier__c> getselectedCampusesSupplier(){
		if(selectedCampusesSupplier == null){
			selectedCampusesSupplier = new map<string, Supplier__c>();
			for(Supplier__c sp:[Select Agency__c, Supplier__c, Supplier__r.BillingCountry from Supplier__c WHERE Supplier__r.BillingCountry = :selectedSchoolCountry AND Agency__c = :selectedAgency]){
				selectedCampusesSupplier.put(sp.Supplier__c, sp);
			}
		}
		return selectedCampusesSupplier;
	}
	
	public map<string, string> schoolName{get{if(schoolName == null) schoolName = new map<string, string>(); return schoolName;} set;}
	
	public map<string, list<Account>> mapSchoolCampuses{get; set;}
	
	public Integer mapSchoolCampusesNum{ get; set;}
	
	public boolean filterShowCaseOnly {get;set;}
	public boolean filterCompleted {get{ if(filterCompleted == null) filterCompleted = true; return filterCompleted;} set;}
    
    public pageReference searchListSchools(){
  		mapSchoolCampuses = null;
  		retrieveMapSchoolCampuses();
  		return null;
  	}
  
  /* SEARCH */
    public list<String> orderToShow {get;set;}
    public map<String,String> mapToMap {get;set;}
	private map<string, list<Account>> retrieveMapSchoolCampuses(){
		orderToShow = new List<String>(); 
		mapToMap = new map<string,string>();
		
		if(mapSchoolCampuses == null){
			mapSchoolCampuses = new map<string, list<Account>>();
			
			if(selectedSchoolCountry != null){
				selectedCampusesSupplier = null;
				schoolName = new map<string, string>();
				
				getselectedCampusesSupplier();
				
				String sql = 'Select id, Name, ParentId, Parent.name, BillingCity, isSelected__c, ShowCaseOnly__c from Account WHERE RecordType.name = \'campus\' and Disabled_Campus__c = false AND BillingCountry = :selectedSchoolCountry';
				
				if(String.isNotEmpty(selectedSchoolId)) 
					sql += ' and parentId = :selectedSchoolId';
				
				if(filterCompleted && !filterShowCaseOnly)
					sql += ' and ShowCaseOnly__c = false ';
					
				if(!filterCompleted && filterShowCaseOnly)
					sql += ' and ShowCaseOnly__c = true ';
				
				sql += ' order by Parent.name, name';
				
				for(Account acc: database.query(sql)){
					Account acco = acc.clone(true);
					acco.isSelected__c = selectedCampusesSupplier.containsKey(acco.id);
					if(!mapSchoolCampuses.containsKey(acco.ParentId)){
						mapSchoolCampuses.put(acco.ParentId, new list<Account>{acco});
						schoolName.put(acco.ParentId, acco.Parent.name);
						
						mapToMap.put(acco.Parent.name,acco.ParentId);
						orderToShow.add(acco.Parent.name);
						
					}else mapSchoolCampuses.get(acco.ParentId).add(acco);
					
				}
				
				orderToShow.sort();
				for(string k : mapSchoolCampuses.keySet()){
					list<Account> l = mapSchoolCampuses.get(k);
					l.sort();
					mapSchoolCampuses.put(k, l);
				}
				
				//mapSchoolCampusesEmpty = (mapSchoolCampuses.size()>0);
				mapSchoolCampusesNum = mapSchoolCampuses.size();
			}
		}
		
		return mapSchoolCampuses;
	}
	
	/* SAVE */
  
	public pageReference saveSupplier(){
		
		Savepoint sp = Database.setSavepoint() ;
		try{
						
			List<Supplier__c> newSuppliers = new List<Supplier__c>();
			List<String> delSuppliers = new List<String>();
			
			system.debug('mapSchoolCampuses: ' + mapSchoolCampuses);
			
			for(string sid:mapSchoolCampuses.keySet()){
				
				for(Account campus : mapSchoolCampuses.get(sid)){
					delSuppliers.add(campus.id);
					if(campus.isSelected__c){
						system.debug('campusisSelected__c: ' + campus);
						Supplier__c sup = new Supplier__c();
						sup.Record_Type__c = 'campus';
						sup.Agency__c = selectedAgency;
						sup.Supplier__c = campus.id;
						/*if(mapSuppliers.containsKey(campus.id)){
							sup.Available__c = mapSuppliers.get(campus.id).Available__c;
							sup.Preferable__c = mapSuppliers.get(campus.id).Preferable__c;
						}*/
						newSuppliers.add(sup);
					}
				}
			}
			
			
			
			List<Supplier__c> sps =  [Select id, agency__c, Available__c, Preferable__c, Supplier__c from Supplier__c where agency__c = :selectedAgency and Supplier__c in :delSuppliers];
      
			for(Supplier__c sup : sps)
				for(Supplier__c newSup : newSuppliers)		
					if(sup.agency__c == newSup.agency__c && sup.Supplier__c == newSup.Supplier__c){
						newSup.Available__c = sup.Available__c;
						newSup.Preferable__c = sup.Preferable__c;
						break;
					}
					
			delete sps;	
			
			
			
			insert newSuppliers;
			
		}catch(Exception e){
			Database.rollback(sp);
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
			ApexPages.addMessage(msg);
		}
		return null;
	}
	
	

}