/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class report_statusPipeline_test {
	
	static testMethod void myUnitTest() {
		
		TestFactory tf = new TestFactory();
		Account agency = tf.createAgency();		
		Contact employee = tf.createEmployee(agency);
		User portalUser = tf.createPortalUser(employee);
		employee.Global_Manager__c = true;
		update employee;
		
		Contact lead = tf.createLead(agency, employee);
		lead.Lead_Stage__c = 'Stage 1';
		lead.Status__c = 'NEW LEAD';
		update lead;
		
		Test.startTest();
		system.runAs(portalUser){
			report_statusPipeline lf = new report_statusPipeline();
			lf.range.arrival_date__c = system.today().addMonths(-6);
			lf.range.expected_travel_date__c = system.today().addMonths(1);
			lf.search();
			lf.checkUserAccess();
			lf.getAgencyGroupOptions();
			lf.getAgencyOptions();
			lf.refreshAgencies();
			
		}
		
		
	}
    
}