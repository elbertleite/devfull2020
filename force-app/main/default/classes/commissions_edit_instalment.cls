public with sharing class commissions_edit_instalment {

    public installmentDetails inst {get;set;}
    public commissions_edit_instalment() {
        String instId = ApexPages.currentPage().getParameters().get('id');
        String sql = sqlFields() + ' WHERE id = :instId limit 1';

        buildResult(Database.query(sql));
    }

    private String sqlFields(){

         return 'SELECT id, Name, Confirmed_Date__c, Confirmed_By__c, Instalment_Value__c, Commission_Due_Date__c, Related_Fees__c, Scholarship_Taken__c, Commission__c,  ' +
                                                'Commission_Value__c,Commission_Confirmed_On__c, Extra_Fee_Value__c, Split_Number__c, Received_By__r.Name, Received_Date__c, ' +
                                                'Confirmed_By__r.Name, Discount__c, Total_School_Payment__c, Number__c, Commission_Tax_Value__c,Commission_Tax_Rate__c, Balance__c, ' +
                                                'Paid_To_School_On__c, Tuition_Value__c, Client_Document__c, isPFS__c,isPDS__c, isPCS__c, instalment_activities__c, Kepp_Fee__c, ' +
                                                'status__c, Related_Promotions__c, PDS_Requested_By__c, PDS_Requested_On__c, Agent_Deal_Name__c, Agent_Deal_Value__c, ' +
                                                'Received_By_Agency__r.Name, Received_By_Agency__r.Parent.Name, Received_by_Department__r.Name,' +
                                                ' Client_Scholarship__c, Covered_By_Agency__c, Total_Paid_School_Credit__c, isSelected__c,  PDS_Note__c,    School_Request_Confirmation_Status__c,'+

                                                'isInstalment_Amendment__c, Amendment_Related_Fees__c, Original_Instalment__r.Instalment_Value__c, Original_Instalment__r.Tuition_Value__c, Original_Instalment__r.isAmended_BeforeSchoolPayment__c, Original_Instalment__r.Extra_Fee_Value__c, '+

                                                'client_course__r.client__c, ' +
                                                'client_course__r.client__r.Name, ' +
                                                'client_course__r.Course_Name__c, ' +
                                                'client_course__r.Campus_Name__c, ' +
                                                'client_course__r.client__r.Owner__r.Name, ' +
                                                'client_course__r.Campus_Id__c, ' +
                                                'client_course__r.School_Id__c, ' +
                                                'client_course__r.Commission_Tax_Rate__c, ' +
                                                'client_course__r.Commission_Type__c,' +
                                                'client_course__r.Commission_Tax_Name__c,' +
                                                'client_course__r.isPackage__c,' +
                                                'client_course__r.course_package__c,' +
                                                'client_course__r.Total_Agent_Deal_Taken__c,' +
                                                'client_course__r.Total_Balance__c,' +
                                                'client_course__r.Total_Commissions__c,' +
                                                'client_course__r.Total_Keep_Fee__c,' +
                                                'client_course__r.Total_Tax__c,' +
                                                'client_course__r.Total_Scholarship_Taken__c,' +
                                                'client_course__r.Total_School_Payment__c,' +
                                                'client_course__r.Total_Client_Scholarship__c,' +
                                                'client_course__r.Total_Covered_By_Agency__c,' +
                                                'client_course__r.School_Name__c ' +

                         'FROM client_course_instalment__c ';
    }

    public void buildResult(client_course_instalment__c cci){
            inst = new installmentDetails();

            if(cci.Client_Scholarship__c==null)
                cci.Client_Scholarship__c=0;

            if(cci.Covered_By_Agency__c==null)
                cci.Covered_By_Agency__c=0;

            inst.installment = cci;
            inst.previousCommission = cci.Commission__c;
            inst.previousScholarship = cci.Scholarship_Taken__c;
            inst.previousAgentDeal = cci.Agent_Deal_Value__c;
            inst.previousAgentDealName = cci.Agent_Deal_Name__c;
            inst.previousTax = cci.Commission_Tax_Rate__c;

            inst.previousAgent_Deal_Value = cci.Agent_Deal_Value__c;
            inst.previousBalance = cci.Balance__c;
            inst.previousCommission_Value = cci.Commission_Value__c;
            inst.previousKepp_Fee = cci.Kepp_Fee__c;
            inst.previousCommission_Tax_Value = cci.Commission_Tax_Value__c;
            inst.previousScholarship_Taken = cci.Scholarship_Taken__c;
            inst.previousClientScholarship = cci.Client_Scholarship__c;
            inst.previousTotal_School_Payment = cci.Total_School_Payment__c;
            inst.previousCoveredByAgency = cci.Covered_By_Agency__c;

            /** Notes **/
            // for(Custom_Note_Task__c n : cci.Custom_Notes_Tasks__r)
            //  inst.listNotes.add(n);

            /** Get extra fees from each instalment **/
            if(cci.Related_Fees__c != null && cci.Related_Fees__c != ''){
                for(string rf:cci.Related_Fees__c.split('!#')){
                            if(rf.split(';;').size() >= 7){ //It's amended
                                inst.feeInstall.add(new feeInstallmentDetail(rf.split(';;')[0], rf.split(';;')[1], decimal.valueOf(rf.split(';;')[2].trim()), rf.split(';;')[3], decimal.valueOf(rf.split(';;')[4].trim()),
                                                        decimal.valueOf(rf.split(';;')[5].trim()),decimal.valueOf(rf.split(';;')[6].trim()), decimal.valueOf(rf.split(';;')[7].trim())));
                            }
                            else //if(!cci.isInstalment_Amendment__c)
                                inst.feeInstall.add(new feeInstallmentDetail(rf.split(';;')[0], rf.split(';;')[1], decimal.valueOf(rf.split(';;')[2].trim()), rf.split(';;')[3], decimal.valueOf(rf.split(';;')[4].trim()),
                                            decimal.valueOf(rf.split(';;')[5].trim())));
                }//end for
            }

            if(cci.Amendment_Related_Fees__c != null && cci.Amendment_Related_Fees__c != ''){
                for(string rf:cci.Amendment_Related_Fees__c.split('!#')){
                    inst.amendmentFees.add(new feeInstallmentDetail(rf.split(';;')[0], rf.split(';;')[1], decimal.valueOf(rf.split(';;')[2].trim()), rf.split(';;')[3], decimal.valueOf(rf.split(';;')[4].trim()),
                                                decimal.valueOf(rf.split(';;')[5].trim())));
                }//end for
            }
            /** END -- Get extra fees from each instalment **/

            /** Instalment Activities /
            if(cci.instalment_activities__c!=null && cci.instalment_activities__c != ''){

                List<String> allAc = cci.instalment_activities__c.split('!#!');

                for(Integer i = (allAc.size() - 1); i >= 0; i--){//activity order desc

                    instalmentActivity instA = new instalmentActivity();
                    List<String> a = allAc[i].split(';;');

                    instA.acType = a[0];
                    instA.acTo = a[1];
                    instA.acSubject = a[2];
                    if(instA.acType=='SMS') // saved date in system mode
                        instA.acDate = new Contact(Lead_Accepted_On__c = Datetime.valueOfGmt(a[3]));
                    else
                        instA.acDate = new Contact(Lead_Accepted_On__c = Datetime.valueOf(a[3]));
                    instA.acStatus = a[4];
                    instA.acError = a[5];
                    instA.acFrom = a[6];

                    system.debug('single activity==' + instA);

                    inst.listActivities.add(instA);

                    if(inst.lastActivityType == null){
                        inst.lastActivityType= instA.acType;
                        inst.lastActivityStatus= instA.acStatus;
                        if(instA.acType=='SMS') // saved date in system mode
                            inst.lastActivityDate = new Contact(Lead_Accepted_On__c = Datetime.valueOfGmt(a[3]));
                        else
                            inst.lastActivityDate = new Contact(Lead_Accepted_On__c = Datetime.valueOf(a[3]));
                    }
                }
            }
            /** END -- Instalment Activities **/

    }

    //Save Instalment Changes
    public boolean showError {get{if(showError == null) showError = false; return showError;}set;}
    public void saveChanges(){

        /** update instalment and its extra fees **/
        boolean saveChanges  = true;
        list<string> keepFeeUpdate = new list<string>();
        decimal totalKeepFee = 0;
        decimal previousTotal = 0;
        String errorMsg = '';

        previousTotal = inst.installment.Kepp_Fee__c;

        for(feeInstallmentDetail fi: inst.feeInstall){

            if(fi.keepfee > fi.feeValue){
                if(errorMsg == '')
                    errorMsg = 'Keep fee value cannot be greater than fee value.';
                else
                    errorMsg += '<br/>Keep fee value cannot be greater than fee value.';

                saveChanges = false;
                break;
            }

            fi.originalKeepFee = fi.keepFee;
            keepFeeUpdate.add(fi.feeId + ';;' + fi.feeName + ';;' +fi.feeValue + ';;' +fi.feeType + ';;' +fi.originalValue + ';;' + fi.keepFee);
            totalKeepFee += fi.keepFee;
            System.debug('==>fi.keepFee: '+fi.keepFee);

        }//end for


        if(inst.installment.Total_Paid_School_Credit__c == null)
            inst.installment.Total_Paid_School_Credit__c = 0;

        decimal creditforTuition = inst.installment.Total_Paid_School_Credit__c;

        if(inst.installment.Total_Paid_School_Credit__c > 0 && inst.installment.Total_Paid_School_Credit__c - inst.installment.Tuition_Value__c > 0)
            creditforTuition = inst.installment.Tuition_Value__c;


        /** Calculate Commission **/
        if(inst.installment.client_course__r.Commission_Type__c == Decimal.valueOf(0))
            inst.installment.Commission_Value__c = (inst.installment.Tuition_Value__c - creditforTuition) * inst.installment.Commission__c/100;
        else
            inst.installment.Commission_Value__c = inst.installment.Commission__c;

        inst.installment.Commission_Tax_Value__c = inst.installment.Commission_Value__c * (inst.installment.Commission_Tax_Rate__c/100);

        decimal totalSchoolRaw = inst.installment.Tuition_Value__c + inst.installment.Extra_Fee_Value__c - inst.installment.Commission_Tax_Value__c - inst.installment.Commission_Value__c;
        totalSchoolRaw = totalSchoolRaw.setScale(2);

        if(inst.installment.Agent_Deal_Value__c==null)
            inst.installment.Agent_Deal_Value__c = 0;

        if(inst.installment.Scholarship_Taken__c==null)
            inst.installment.Scholarship_Taken__c = 0;

        if(inst.installment.client_scholarship__c==null)
            inst.installment.client_scholarship__c = 0;

        if(inst.installment.covered_by_agency__c==null)
            inst.installment.covered_by_agency__c = 0;

        if(inst.installment.Total_Paid_School_Credit__c==null)
            inst.installment.Total_Paid_School_Credit__c = 0;

        if( inst.installment.Scholarship_Taken__c + inst.installment.Agent_Deal_Value__c > totalSchoolRaw){
            errorMsg = 'Sum of Scholarship and Agent Deal cannot be greater than payment value.';
            saveChanges = false;
        }else{
            if(inst.installment.Total_School_Payment__c > 0 && inst.installment.Scholarship_Taken__c >  totalSchoolRaw){
                errorMsg = 'Scholarship cannot be greater than school payment value.';
                saveChanges = false;
            }

            if(inst.installment.Total_School_Payment__c > 0 && inst.installment.Agent_Deal_Value__c >  totalSchoolRaw){
                errorMsg = 'Agent deal cannot be greater than school payment value.';
                saveChanges = false;
            }
        }

        if(saveChanges){
            inst.installment.Kepp_Fee__c = totalKeepFee;

            /** Calculate Total School Payment **/
            if(inst.installment.isPFS__c)
                inst.installment.Total_School_Payment__c = inst.installment.Tuition_Value__c  + inst.installment.Extra_Fee_Value__c - inst.installment.Total_Paid_School_Credit__c - inst.installment.client_scholarship__c;
            else
                inst.installment.Total_School_Payment__c = inst.installment.Tuition_Value__c + inst.installment.Extra_Fee_Value__c - inst.installment.Commission_Tax_Value__c - inst.installment.Commission_Value__c - inst.installment.Kepp_Fee__c - inst.installment.Scholarship_Taken__c - inst.installment.Agent_Deal_Value__c - inst.installment.Total_Paid_School_Credit__c - inst.installment.client_scholarship__c;

            inst.installment.Total_School_Payment__c = inst.installment.Total_School_Payment__c.setScale(2);
            inst.installment.Related_Fees__c = string.join(keepFeeUpdate, '!#');



            inst.installment.Balance__c = inst.installment.Commission_Value__c + inst.installment.Agent_Deal_Value__c + inst.installment.Kepp_Fee__c + inst.installment.Scholarship_Taken__c - inst.installment.Discount__c - inst.installment.covered_by_agency__c;

            //inst.installment.Balance__c = inst.installment.Commission_Value__c + inst.installment.Commission_Tax_Value__c - inst.installment.Discount__c + inst.installment.Kepp_Fee__c;

            //CALCULATE CLIENT COURSE TOTALS
            System.debug('==> inst.installment.client_course__r.Total_Agent_Deal_Taken__c: '+inst.installment.client_course__r.Total_Agent_Deal_Taken__c);
            System.debug('==> inst.previousAgent_Deal_Value: '+inst.previousAgent_Deal_Value);
            System.debug('==> inst.installment.Agent_Deal_Value__c: '+inst.installment.Agent_Deal_Value__c);

            if(inst.installment.client_course__r.Total_Agent_Deal_Taken__c == null)
                inst.installment.client_course__r.Total_Agent_Deal_Taken__c = inst.installment.Agent_Deal_Value__c;
            else inst.installment.client_course__r.Total_Agent_Deal_Taken__c = (inst.installment.client_course__r.Total_Agent_Deal_Taken__c - inst.previousAgent_Deal_Value) + inst.installment.Agent_Deal_Value__c;

            inst.installment.client_course__r.Total_Agent_Deal_Taken__c = inst.installment.client_course__r.Total_Agent_Deal_Taken__c.setScale(2);


            if(inst.installment.client_course__r.Total_Balance__c == null)
                inst.installment.client_course__r.Total_Balance__c = inst.installment.Balance__c;
            else inst.installment.client_course__r.Total_Balance__c = (inst.installment.client_course__r.Total_Balance__c - inst.previousBalance) + inst.installment.Balance__c;

            inst.installment.client_course__r.Total_Balance__c = inst.installment.client_course__r.Total_Balance__c.setScale(2);

            if(inst.installment.client_course__r.Total_Commissions__c == null)
                inst.installment.client_course__r.Total_Commissions__c = inst.installment.Commission_Value__c;
            else inst.installment.client_course__r.Total_Commissions__c = (inst.installment.client_course__r.Total_Commissions__c - inst.previousCommission_Value) + inst.installment.Commission_Value__c;

            inst.installment.client_course__r.Total_Commissions__c = inst.installment.client_course__r.Total_Commissions__c.setScale(2);


            if(inst.installment.client_course__r.Total_Keep_Fee__c == null)
                inst.installment.client_course__r.Total_Keep_Fee__c = inst.installment.Kepp_Fee__c;
            else inst.installment.client_course__r.Total_Keep_Fee__c = (inst.installment.client_course__r.Total_Keep_Fee__c - inst.previousKepp_Fee) + inst.installment.Kepp_Fee__c;

            inst.installment.client_course__r.Total_Keep_Fee__c = inst.installment.client_course__r.Total_Keep_Fee__c.setScale(2);


            if(inst.installment.client_course__r.Total_Tax__c == null)
                inst.installment.client_course__r.Total_Tax__c = inst.installment.Commission_Tax_Value__c;
            else inst.installment.client_course__r.Total_Tax__c = (inst.installment.client_course__r.Total_Tax__c - inst.previousCommission_Tax_Value) + inst.installment.Commission_Tax_Value__c;

            inst.installment.client_course__r.Total_Tax__c = inst.installment.client_course__r.Total_Tax__c.setScale(2);


            if(inst.installment.client_course__r.Total_Scholarship_Taken__c == null)
                inst.installment.client_course__r.Total_Scholarship_Taken__c = inst.installment.Scholarship_Taken__c;
            else inst.installment.client_course__r.Total_Scholarship_Taken__c = (inst.installment.client_course__r.Total_Scholarship_Taken__c - inst.previousScholarship_Taken) + inst.installment.Scholarship_Taken__c;


            inst.installment.client_course__r.Total_Scholarship_Taken__c = inst.installment.client_course__r.Total_Scholarship_Taken__c.setScale(2);

            if(inst.installment.client_course__r.Total_School_Payment__c == null)
                inst.installment.client_course__r.Total_School_Payment__c = inst.installment.Total_School_Payment__c;
            else inst.installment.client_course__r.Total_School_Payment__c = (inst.installment.client_course__r.Total_School_Payment__c - inst.previousTotal_School_Payment) + inst.installment.Total_School_Payment__c;


            if(inst.installment.client_course__r.Total_Client_Scholarship__c == null || inst.installment.client_course__r.Total_Client_Scholarship__c == 0)
                inst.installment.client_course__r.Total_Client_Scholarship__c = inst.installment.Client_Scholarship__c;
            else inst.installment.client_course__r.Total_Client_Scholarship__c = (inst.installment.client_course__r.Total_Client_Scholarship__c - inst.previousClientScholarship) + inst.installment.Client_Scholarship__c;


            if(inst.installment.client_course__r.Total_Covered_By_Agency__c == null || inst.installment.client_course__r.Total_Covered_By_Agency__c == 0)
                inst.installment.client_course__r.Total_Covered_By_Agency__c = inst.installment.Covered_By_Agency__c;
            else inst.installment.client_course__r.Total_Covered_By_Agency__c = (inst.installment.client_course__r.Total_Covered_By_Agency__c - inst.previousCoveredByAgency) + inst.installment.Covered_By_Agency__c;

            inst.installment.client_course__r.Total_School_Payment__c = inst.installment.client_course__r.Total_School_Payment__c.setScale(2);

            update inst.installment;
            update inst.installment.client_course__r;

            inst.previousCommission = inst.installment.Commission__c;
            inst.previousScholarship = inst.installment.Scholarship_Taken__c;
            inst.previousAgentDeal = inst.installment.Agent_Deal_Value__c;
            inst.previousAgentDealName = inst.installment.Agent_Deal_Name__c;

            inst.previousAgent_Deal_Value = inst.installment.Agent_Deal_Value__c;
            inst.previousBalance = inst.installment.Balance__c;
            inst.previousCommission_Value = inst.installment.Commission_Value__c;
            inst.previousKepp_Fee = inst.installment.Kepp_Fee__c;
            inst.previousCommission_Tax_Value = inst.installment.Commission_Tax_Value__c;
            inst.previousScholarship_Taken = inst.installment.Scholarship_Taken__c;
            inst.previousTotal_School_Payment = inst.installment.Total_School_Payment__c;
            inst.previousClientScholarship = inst.installment.Client_Scholarship__c;
            inst.previousCoveredByAgency = inst.installment.Covered_By_Agency__c;
            showError = true;
        }
        else{
            showError = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error,errorMsg));
        }
    }

    //Cancel Instalment Changes
    public void cancelChanges(){

            for(feeInstallmentDetail fi: inst.feeInstall){
                fi.keepfee = fi.originalKeepFee;
            }//end for

            inst.installment.Commission__c = inst.previousCommission;
            inst.installment.Agent_Deal_Value__c = inst.previousAgentDeal;
            inst.installment.Agent_Deal_Name__c = inst.previousAgentDealName;
            inst.installment.Scholarship_Taken__c = inst.previousScholarship;
            inst.installment.Commission_Tax_Rate__c = inst.previousTax;
    }

/********************** Inner Classes **********************/

    //Instalment Details
    public class installmentDetails{
        public client_course_instalment__c installment{get; set;}
        public list<feeInstallmentDetail> feeInstall{get{if(feeInstall == null) feeInstall = new list<feeInstallmentDetail>(); return feeInstall;} set;}
        public list<feeInstallmentDetail> amendmentFees{get{if(amendmentFees == null) amendmentFees = new list<feeInstallmentDetail>(); return amendmentFees;} set;}
        // public list<instalmentActivity> listActivities{get{if(listActivities == null) listActivities = new list<instalmentActivity>(); return listActivities;} set;}
        // public list<Custom_Note_Task__c> listNotes{get{if(listNotes == null) listNotes = new list<Custom_Note_Task__c>(); return listNotes;} set;}
        // public String lastActivityType {get;set;}
        // public Contact lastActivityDate {get;set;}
        // public String lastActivityStatus {get;set;}
        public decimal previousCommission {get;set;}
        public decimal previousAgentDeal {get;set;}
        public decimal previousTax {get;set;}
        public String previousAgentDealName {get;set;}
        public decimal previousScholarship {get;set;}
        // public String emailURL {get;set;}
        // public list<SelectOption> instOptions {get;set;}

        private decimal previousAgent_Deal_Value {get;set;}
        private decimal previousBalance {get;set;}
        private decimal previousCommission_Value {get;set;}
        private decimal previousKepp_Fee {get;set;}
        private decimal previousCommission_Tax_Value {get;set;}
        private decimal previousScholarship_Taken {get;set;}
        private decimal previousTotal_School_Payment {get;set;}
        private decimal previousClientScholarship {get;set;}
        private decimal previousCoveredByAgency {get;set;}

        // public decimal totalPFSInvoice {get;set;}
    }

    //Fee Details
    public class feeInstallmentDetail{
        public string feeId{get; set;}
        public string feeName{get; set;}
        public decimal feeValue{get; set;}
        public string feeType{get; set;}
        public decimal originalValue{get; set;}
        public decimal keepFee{get; set;}
        public decimal feeAmendment{get; set;}
        public decimal keepFeeAmendment{get; set;}
        public decimal originalKeepFee {get;set;}

        public feeInstallmentDetail(string feeId, string feeName, decimal feeValue, string feeType, decimal originalValue, decimal keepFee, decimal feeAmendment, decimal keepFeeAmendment){
                this.feeId = feeId;
                this.feeName = feeName;
                this.feeValue = feeAmendment  + feeValue;
                this.feeType = feeType;
                this.originalValue = originalValue;
                this.keepFee = keepFee;
                this.feeAmendment = feeAmendment;
                this.keepFeeAmendment = keepFeeAmendment;
                this.originalValue = feeValue;
                this.originalKeepFee = keepFee;
        }

        public feeInstallmentDetail(string feeId, string feeName, decimal feeValue, string feeType, decimal originalValue, decimal keepFee){
                this.feeId = feeId;
                this.feeName = feeName;
                this.feeValue = feeValue;
                this.feeType = feeType;
                this.originalValue = originalValue;
                this.keepFee = keepFee;
        }
    }
}