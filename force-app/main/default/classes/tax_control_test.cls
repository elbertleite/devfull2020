/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class tax_control_test {

    static testMethod void myUnitTest() {
    	
    	TestFactory tf = new TestFactory();
    	Account country = tf.createCountryShowcase();
        Account city = tf.createCityShowcase(country);
        
        Account city2 = city;
        city2.Id = null;
        city2.Name = 'Cairns';
        insert city2;
        
        tax_control testClass = new tax_control();
        list<SelectOption> listTax = testClass.getlistTax;
        
        ApexPages.currentPage().getParameters().put('paramCountry', 'Australia');
        testClass.saveTax();
        
        city2.tax_rate__c = null;
        update city2;
        testClass.retrieveCountries();
        testClass.saveTax();
        
        city2.tax_rate__c = 30.0;
        city2.Tax_Name__c = null;
     	update city2;
        testClass.retrieveCountries();
        testClass.saveTax();
    }
}