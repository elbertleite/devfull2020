public with sharing class client_course_deposits_to_refund {
	
	//Currencies
	public DateTime currencyLastModifiedDate {get;set;}
	public list<SelectOption> mainCurrencies {get;set;}
	private Map<String, double> agencyCurrencies;
	public String bucket { get{ return IPFunctions.s3bucket; }set; }
	
	//

	private FinanceFunctions ff {get{if(ff==null) ff = new FinanceFunctions(); return ff;}set;}

	public boolean redirectNewContactPage {get{if(redirectNewContactPage == null) redirectNewContactPage = IPFunctions.redirectNewContactPage();return redirectNewContactPage; }set;}

	public string selectedDepartment {get;set;}
	public client_course_deposits_to_refund(){

		mainCurrencies = ff.retrieveMainCurrencies();
		agencyCurrencies = ff.retrieveAgencyCurrencies();
		currencyLastModifiedDate = ff.currencyLastModifiedDate;
		
		findDepositRefunds();
	}
	
	public list<client_course_instalment_payment__c> refDeposits {get;set;}
	public void findDepositRefunds(){
		selectedDepartment = currentUser.Contact.Department__c;
		
		if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible() && currentUser.Aditional_Agency_Managment__c == null){ //set the user to see only his own agency
			selectedAgency = currentUser.Contact.AccountId;
			selectedAgencies = new List<String>{currentUser.Contact.AccountId};
		}
		List<String> agenciesToSearch;
		if(selectedAgencies.size() == 1 && selectedAgencies.get(0) == 'all'){
			agenciesToSearch = new List<String>();
			for(SelectOption opt : getAgencies()){
				if(opt.getValue() != 'all'){
					agenciesToSearch.add(opt.getValue());		
				}
			}
		}else{
			agenciesToSearch = selectedAgencies;
		}
		
		// String sql ='SELECT Id, Value__c, Payment_Type__c, Date_Paid__c, Client__c, Client__r.Name, createdBy.Name, createdDate, Description__c, Payment_Receipt__c, CurrencyIsoCode__c, Deposit__r.Deposit_Total_Available__c, Deposit__r.Deposit_Total_Refund__c, Transfer_Send_Fee_Currency__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, Transfer_Send_Fee__c, Paid_to_Client_On__c FROM client_course_instalment_payment__c WHERE Refund_Requested_By_Agency__c = :selectedAgency';
		String sql ='SELECT Id, Value__c, Payment_Type__c, Date_Paid__c, Client__c, Client__r.Name, createdBy.Name, Refund_Requested_By_Agency__r.Name, createdDate, Description__c, Payment_Receipt__c, CurrencyIsoCode__c, Deposit__r.Deposit_Total_Available__c, Deposit__r.Deposit_Total_Refund__c, Transfer_Send_Fee_Currency__c, Agency_Currency__c, Agency_Currency_Rate__c, Agency_Currency_Value__c, Transfer_Send_Fee__c, Paid_to_Client_On__c, Refund_Requested_By_Agency__c FROM client_course_instalment_payment__c WHERE Refund_Requested_By_Agency__c IN :agenciesToSearch';
		
		// if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible())
		//  	sql+= ' AND Refund_Requested_By_Agency__r.ParentId = :selectedAgencyGroup ';
		
		sql+= ' AND isClient_Refund__c = true AND Not_Refunded__c = false AND (Paid_to_Client_On__c = null OR (Paid_to_Client_On__c != null AND Sent_Email_Receipt__c = false)) and Refund_Cancelled_On__c = null order by createdDate'; 
		
		system.debug('sql===' + sql);

		refDeposits =  new list<client_course_instalment_payment__c>();

		for(client_course_instalment_payment__c ccip : Database.query(sql)){

			//C U R R E N C Y
			ccip.Agency_Currency__c = currentUser.Contact.Account.account_currency_iso_code__c;
			ccip.Transfer_Send_Fee_Currency__c = ccip.Agency_Currency__c;
			ccip.Agency_Currency_Rate__c = agencyCurrencies.get(ccip.CurrencyIsoCode__c);
			ff.convertCurrency(ccip, ccip.CurrencyIsoCode__c, ccip.Value__c, null, 'Agency_Currency__c', 'Agency_Currency_Rate__c', 'Agency_Currency_Value__c');

			refDeposits.add(ccip);
		}//end for

	}
	
	
	/** C O N F I R M 		R E F U N D **/
	public PageReference confirmRefund(){
		
		string depositId = ApexPages.currentPage().getParameters().get('id');
		string agency = ApexPages.currentPage().getParameters().get('agency');
		system.debug('to confirm===>' + depositId);
		
		for(client_course_instalment_payment__c ccip : refDeposits)
			if(ccip.id == depositId){
				boolean hasError = false;
				
				
				if(ccip.Agency_Currency_Rate__c == null){
					ccip.Agency_Currency_Rate__c.addError('Please fill the rate.');
					hasError = true;
				}

				if(ccip.Agency_Currency_Value__c == null){
					ccip.Agency_Currency_Value__c.addError('Please fill the value.');
					hasError = true;
				}

				if(ccip.Transfer_Send_Fee__c == null){
					ccip.Transfer_Send_Fee__c.addError('Please fill the amount.');
					hasError = true;
				}
				
				if(ccip.Payment_Type__c == null || ccip.Payment_Type__c == 'none'){
					ccip.Description__c.addError('Please select the Payment Type.');
					hasError = true;
				}
				
				if(ccip.Date_Paid__c == null){
					ccip.Date_Paid__c.addError('Please select the Payment Date.');
					hasError = true;
				}else if(ccip.Date_Paid__c > system.today()){
					ccip.Date_Paid__c.addError('Payment Date cannot be a future date.');
					hasError = true;
				}
				
				if(hasError){
					return null;
				}
				else
					{
						// Update Refund Details	
						ccip.Paid_to_Client_By__c = currentUser.id;
						//ccip.Paid_to_Client_By_Agency__c = currentUser.Contact.AccountId;
						//ccip.Paid_to_Client_By_Agency__c = selectedagency;
						ccip.Paid_to_Client_By_Agency__c = agency;
						ccip.Paid_to_Client_By_Department__c = currentUser.Contact.Department__c;
						ccip.Paid_to_Client_On__c = Datetime.now();
						ccip.Booking_Closed_By__c = UserInfo.getUserId();
						ccip.Booking_Closed_On__c = Datetime.now();
						
						update ccip;
						break;
						
					}
			}
			
		findDepositRefunds();
		ApexPages.currentPage().getParameters().remove('id');
		return null;
	}
	
	
	/** C A N C E L 	R E F U N D **/
	public String cancelReason {get;set;}
	public boolean showError {get;set;}
	private String toCancelId {get;set;}
	private String toCancelAgency {get;set;}
	
	public void setIdCancel(){
		toCancelId = ApexPages.currentPage().getParameters().get('id');
		toCancelAgency = ApexPages.currentPage().getParameters().get('agency');
	}
	
	
	public void cancelRefund(){
		showError = false;
		system.debug('to cancel===>' + toCancelId);
		
		for(client_course_instalment_payment__c ccip : refDeposits)
			if(ccip.id == toCancelId){
				
				// Update Refund Details	
				ccip.Refund_Cancelled_By__c = currentUser.id;
				//ccip.Refund_Cancelled_By_Agency__c = currentUser.Contact.AccountId;
				//ccip.Refund_Cancelled_By_Agency__c = selectedagency;
				ccip.Refund_Cancelled_By_Agency__c = toCancelAgency;
				ccip.Refund_Cancelled_By_Department__c = currentUser.Contact.Department__c;
				ccip.Refund_Cancelled_On__c = Datetime.now();
				ccip.Refund_Cancelled_Description__c = cancelReason;
				
				update ccip;
				
				ccip.Deposit__r.Deposit_Total_Available__c += ccip.Value__c; 
				ccip.Deposit__r.Deposit_Total_Refund__c -= ccip.Value__c;
				update ccip.Deposit__r;
				
				break;
				//showError = true;
			}
	
		findDepositRefunds();
		ApexPages.currentPage().getParameters().remove('id');
		cancelReason = '';
		
	}

	public void keepRefund(){
		String depositId = ApexPages.currentPage().getParameters().get('id');

		client_course_instalment_payment__c toUpdate = new client_course_instalment_payment__c(Id = depositId, Not_Refunded__c = true);

		update toUpdate;
		findDepositRefunds();
	}
	
	
	
	
	
	private list<selectOption> refundActions;
	public list<selectOption> getrefundActions(){
		if(refundActions == null){
			refundActions = new list<selectOption>();
			refundActions.add(new SelectOption('none', '-- Action --'));
			refundActions.add(new SelectOption('confirm', 'Confirm Refund'));
			refundActions.add(new SelectOption('cancel', 'Cancel Refund'));
			refundActions.add(new SelectOption('kept', 'Non Refundable'));
		}
		return refundActions;
	}
	
	private list<selectOption> depositListType;
	public list<selectOption> getdepositListType(){
		if(depositListType == null){
			depositListType = new list<selectOption>();
			Schema.DescribeFieldResult fieldResult = client_course__c.Deposit_Type__c.getDescribe();
			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
			for( Schema.PicklistEntry f : ple)
				depositListType.add(new SelectOption(f.getLabel(), f.getValue()));
				
			depositListType.add(0,new SelectOption('none', '-- Payment Type --'));
		}
		return depositListType;
	}
	
	
		/********************** Filters **********************/	
	private User currentUser {get{if(currentUser==null) currentUser = ff.currentUser; return currentUser;}set;}
	
	public string selectedAgencyGroup{
    	get{
    		if(selectedAgencyGroup == null)
    			selectedAgencyGroup = currentUser.Contact.Account.ParentId;
    		return selectedAgencyGroup;
    	}  
    	set;
    }
    
	 public List<SelectOption> agencyGroupOptions {
  		get{
   			if(agencyGroupOptions == null){
   				
   				agencyGroupOptions = new List<SelectOption>();  
   				if(!Schema.sObjectType.User.fields.Finance_Access_All_Groups__c.isAccessible()){ //set the user to see only his own group  
	    			for(Account ag : [SELECT ParentId, Parent.Name FROM Account WHERE id =:currentUser.Contact.AccountId order by Parent.Name]){
		     			agencyGroupOptions.add(new SelectOption(ag.ParentId, ag.Parent.Name));
		    		}
   				}else{
   					for(Account ag : [SELECT id, name FROM Account WHERE RecordType.Name = 'Agency Group' order by Name ]){
		     			agencyGroupOptions.add(new SelectOption(ag.id, ag.Name));
		    		}
   				}
	   		}
	   		return agencyGroupOptions;
	  }
	  set;
 	}
	
	public string selectedAgency {get{if(selectedAgency==null) selectedAgency = currentUser.Contact.AccountId; return selectedAgency;}set;}
	public List<String> selectedAgencies {get{if(selectedAgencies==null) selectedAgencies = new List<String>{currentUser.Contact.AccountId}; return selectedAgencies;}set;}
		
	public list<SelectOption> getAgencies(){
		List<SelectOption> result = new List<SelectOption>();
		if(selectedAgencyGroup != null){
			if(!Schema.sObjectType.User.fields.Finance_Access_All_Agencies__c.isAccessible()){ //set the user to see only his own agency
				for(Account a: [Select Id, Name from Account where id =:currentUser.Contact.AccountId order by name]){
					if(selectedAgency=='')
						selectedAgency = a.Id;					
					result.add(new SelectOption(a.Id, a.Name));
					if(currentUser.Aditional_Agency_Managment__c != null){
						for(Account ac:ipFunctions.listAgencyManagment(currentUser.Aditional_Agency_Managment__c)){
							result.add(new SelectOption(ac.id, ac.name));
						}
					}
				}
			}else{
				for(Account a: [Select Id, Name from Account where ParentId = :selectedAgencyGroup and RecordType.Name = 'agency' order by name]){
					result.add(new SelectOption(a.Id, a.Name));
					if(selectedAgency=='')
						selectedAgency = a.Id;	
				}
			}
		}
		if(result.size() > 2){
			result.add(0, new SelectOption('all', '-- All Agencies --'));
		}
		return result;
	}
	
	public void changeGroup(){
		selectedAgency = '';
		getAgencies();
		
		changeAgency();
	}
		
	public void changeAgency(){
		selectedSchool ='all';
		schoolOptions = null;
		
		campusOptions = null;
		selectedCampus = 'all';
	}
	
	public IPFunctions.CampusAvailability ca {get{if(ca==null) ca = new IPFunctions.CampusAvailability(); return ca;}set;}
	
	public String selectedSchool {get{if(selectedSchool==null) selectedSchool = 'all'; return selectedSchool;}set;}
	public  List<SelectOption> schoolOptions {
		get{
			if(schoolOptions == null){
				schoolOptions = new List<SelectOption>();
		
				schoolOptions = ca.getAvlSchools(selectedAgency);
				
			}
			return schoolOptions;
		}set;}
		
	public void changeSchool(){
		campusOptions = null;
		selectedCampus = 'all';
	}		
			
	public String selectedCampus {get{if(selectedCampus==null) selectedCampus = 'all'; return selectedCampus;}set;}
	public List<SelectOption> campusOptions {
		get{
			if(campusOptions == null && selectedSchool != 'all'){
				
				campusOptions = new List<SelectOption>();				
				campusOptions = ca.getSchoolCampus(selectedSchool);				
			}
			
			if(campusOptions == null && selectedSchool == 'all'){
				campusOptions = new List<SelectOption>();	
				campusOptions.add(new SelectOption('all', '-- All --'));
			}
			
			return campusOptions;
		}
		set;
	}
    
}