public with sharing class school_Campus_Availability {
  public string selectedSchoolCountry{get; set;}
  public string selectedSchoolId{get; set;}
  public string selectedCampusId{get; set;}
  public string selectedAgencyCountry{get; set;}
  public string selectedAgencyCity{get; set;}
  public string typedNameAgency{get; set;}
  
  /* SCHOOLS */
  //school countries
  private List<SelectOption> schoolCountries;
  public List<SelectOption> getschoolCountries() {
    if(schoolCountries == null){
      schoolCountries = new List<SelectOption>();
          
      schoolCountries.add(new SelectOption('','Select Country'));
      for (AggregateResult ar:[Select BillingCountry country from Account where recordType.name = 'Campus' 
        and BillingCountry != null group by BillingCountry order by BillingCountry])
        schoolCountries.add(new SelectOption((string) ar.get('country'), (string) ar.get('country')));
    }
    return schoolCountries;
  }
  
  //list schools for one country  
  public list<SelectOption> getschoolsCountry(){  	
    list<SelectOption> schoolsCountry = new List<SelectOption>();
  	System.debug('==> selectedSchoolCountry: '+selectedSchoolCountry);
  	
  	schoolsCountry.add(new SelectOption('','Select School'));
  	if(selectedSchoolCountry != null){
  		
  	  for (AggregateResult sch:[Select parentid pid, parent.name pname from Account WHERE recordType.name='campus' and billingcountry = :selectedSchoolCountry GROUP BY parentid, parent.name order by parent.name])
        schoolsCountry.add(new SelectOption((string) sch.get('pid'), (string) sch.get('pname')));        
  	}
  	
  	return schoolsCountry;
  }
  
  //list campus for a one school of one country
  public list<SelectOption> getschoolCampus(){  	
  	list<SelectOption> schoolCampus = new List<SelectOption>();
  	schoolCampus.add(new SelectOption('','Select Campus'));
  	
  	for(Account sp:[Select id, name from Account WHERE  recordType.name='campus' and Disabled_Campus__c = false and parentId = :selectedSchoolId and BillingCity != null order by name]){
      schoolCampus.add(new SelectOption(sp.id, sp.name));
    }
  	  	
  	return schoolCampus;
  }
  
  /* AGENCY */
  //agency countries
  private List<SelectOption> agencyCountries;
  public List<SelectOption> getagencyCountries() {
    if(agencyCountries == null){
      agencyCountries = new List<SelectOption>();
          
      agencyCountries.add(new SelectOption('','Select Country'));
      for (AggregateResult ar:[Select BillingCountry country from Account where recordType.name = 'Agency' 
      AND BillingCountry != null group by BillingCountry order by BillingCountry])
        agencyCountries.add(new SelectOption((string) ar.get('country'), (string) ar.get('country')));
    }
    return agencyCountries;
  }
  
  //agency cities
  private List<String> cities;
  public List<SelectOption> getagencyCities() {  	
    List<SelectOption> agencyCities = new List<SelectOption>();
    cities = new List<String>();
  	agencyCities.add(new SelectOption('','Select City'));
  	
  	System.debug('==> selectedAgencyCountry'+selectedAgencyCountry);
  	for (AggregateResult ar:[Select BillingCity city from Account where recordType.name = 'Agency' 
  	  AND BillingCountry = :selectedAgencyCountry AND BillingCity != null group by BillingCity order by BillingCity]){
  	  agencyCities.add(new SelectOption((string) ar.get('city'), (string) ar.get('city')));//selectedAgencyCountry
      cities.add((string) ar.get('city'));  	  	
  	}
  	
  	return agencyCities;
  }  
  
  /* MAP SEARCH */
  
  public pageReference searchListAgency(){
    mapSchoolView = null;
    mapSchoolViewNum = 0;
    retrieveMapSchoolView();
    return null;
  }
    
  public map<String, list<Account>> cityAgency(string idcampus){//agencies per city (campus)
  	//map<string, list<Account>> agency = new map<string, list<Account>>();
  	map<String, list<Account>> mapcityAgencies = new map<String, list<Account>>();
  	
  	String sql= 'Select Agency__c from Supplier__c WHERE Supplier__c = :idcampus ';
  	String sql2 = 'Select id, name, BillingCity, isSelected__c from Account WHERE recordtype.name= \'agency\' ';
    if(selectedAgencyCountry != null && selectedAgencyCountry != ''){
      sql += 'AND Agency__r.BillingCountry = :selectedAgencyCountry ';
      sql2 += ' and BillingCountry = :selectedAgencyCountry ';
    }else{
      sql += 'AND Agency__r.BillingCountry != null ';
      sql2 += ' and BillingCountry != null ';

    }

  	if(String.isNotEmpty(selectedAgencyCity)){
  		sql += 'AND  Agency__r.BillingCity = :selectedAgencyCity ';
  		sql2 += 'AND  BillingCity = :selectedAgencyCity ';
  	}

    	sql += ' AND  Agency__r.Inactive__c = false ';
  		sql2 += ' AND  Inactive__c = false ';

  	if(String.isNotEmpty(typedNameAgency)) sql2 += ' AND name like \'%'+typedNameAgency+'%\'';
  	
  	set<string> supplierAgencies = new set<string>();
  	for(Supplier__c sp: database.query(sql))
      supplierAgencies.add(sp.Agency__c);
  	 
  	for(Account ac: database.query(sql2)){
      if(!mapcityAgencies.containsKey(ac.BillingCity)){
        ac.isSelected__c = supplierAgencies.contains(ac.id);
        mapcityAgencies.put(ac.BillingCity, new list<Account>{ac});
      }else{
  	 	  ac.isSelected__c = supplierAgencies.contains(ac.id);
        mapcityAgencies.get(ac.BillingCity).add(ac);
  	  }
  	}   
  	mapSchoolViewNum = mapcityAgencies.size();
  	return mapcityAgencies;
  }
    
  public Integer mapSchoolViewNum{ get; set;}
   
  public map<String, String> mapSchools{get{if(mapSchools == null) mapSchools = new map<string, string>(); return mapSchools;} set;} //<school id, school name>
  public map<String, String> mapCampusIdName{get{if(mapCampusIdName == null) mapCampusIdName = new map<string, string>(); return mapCampusIdName;} set;} //<campus id, campus name>
  
  //map<school, map<campus, map<city, list<agency>>>>
  public map<String, map<string, map<string, list<Account>>>> mapSchoolView{get;set;}
  private map<String, map<string, map<string, list<Account>>>> retrieveMapSchoolView(){
  	mapSchoolView = new map<String, map<string, map<string, list<Account>>>>();
  	
  	if(selectedSchoolId != null){
	  	map<String, list<Account>> mapcity = new map<String, list<Account>>();
	  	map<string, map<string, list<Account>>> mapcampus = new map<string, map<string, list<Account>>>();
  		
	  	Account ac = [Select id, name from Account WHERE id = :selectedSchoolId]; //school
	  	mapSchools.put(ac.id, ac.name);
	   
  		String sql = 'Select id, name, ShowCaseOnly__c from Account WHERE parentId = :selectedSchoolId and recordtype.Name = \'campus\' and Disabled_Campus__c = false and BillingCity != null';
  		if(String.isNotEmpty(selectedCampusId)) sql += ' AND id = :selectedCampusId';
  		sql += ' order by name';
	  	
	  	for(Account sp: database.query(sql)){//campus
	  		mapcity = cityAgency(sp.id);
	  		mapcampus.put(sp.id, mapcity);
	  		String campusName;
	  		if(sp.ShowCaseOnly__c)
	  			campusName = '<label style="color:red">' + sp.name + '&nbsp; <i>(Showcase Only)</i></label>';
	  		else 
	  			campusName = sp.name;
	  		mapCampusIdName.put(sp.id, campusName);
	  	}
	  	mapSchoolView.put(ac.id, mapcampus);
	  }
  	return mapSchoolView;
  }
  
  /* SAVE */
  
  public pageReference saveSupplier(){
    
    Savepoint sp = Database.setSavepoint() ;
    try{
    	
      List<Supplier__c> newSuppliers = new List<Supplier__c>();
      List<String> delSuppliers = new List<String>();
      list<string> agencyDel = new list<string>();
      
      for(string sid: mapSchoolView.keySet()){
        for(String campus : mapSchoolView.get(sid).keySet()){
        	delSuppliers.add(campus);
        	for(string city: mapSchoolView.get(sid).get(campus).keySet()){
        		for(Account agency: mapSchoolView.get(sid).get(campus).get(city)){
        			agencyDel.add(agency.id);
		          if(agency.isSelected__c){             
		            Supplier__c sup = new Supplier__c();
		            sup.Record_Type__c = 'campus';
		            sup.Agency__c = agency.id;
		            sup.Supplier__c = campus;
		            newSuppliers.add(sup);
		          }
        			
        		}
        	}
        }
      }
      
      	List<Supplier__c> sps =  [Select id, agency__c, Available__c, Preferable__c, Supplier__c from Supplier__c where agency__c in :agencyDel and Supplier__c in :delSuppliers];
      
		for(Supplier__c sup : sps)
			for(Supplier__c newSup : newSuppliers)		
				if(sup.agency__c == newSup.agency__c && sup.Supplier__c == newSup.Supplier__c){
					newSup.Available__c = sup.Available__c;
					newSup.Preferable__c = sup.Preferable__c;
					break;
				}
					
		delete sps;		
      
      insert newSuppliers;
    }catch(Exception e){
      Database.rollback(sp);
      ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
      ApexPages.addMessage(msg);
    }
    return null;
  }
  
}