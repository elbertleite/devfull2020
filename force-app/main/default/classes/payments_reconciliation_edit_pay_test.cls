@isTest
private class payments_reconciliation_edit_pay_test {

  @isTest static void test_method_one() {
    TestFactory tf = new TestFactory();

    Account school = tf.createSchool();
    Account agency = tf.createAgency();
    Contact emp = tf.createEmployee(agency);
    User portalUser = tf.createPortalUser(emp);
    Account campus = tf.createCampus(school, agency);
    Course__c course = tf.createCourse();
    Campus_Course__c campusCourse =  tf.createCampusCourse(campus, course);

    Test.startTest();
    system.runAs(portalUser){
      Contact client = tf.createLead(agency, emp);
      client_course__c booking = tf.createBooking(client);
      client_course__c cc =  tf.createClientCourse(client, school, campus, course, campusCourse, booking);


      List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(cc);

      client_course_instalment_pay instPay = new client_course_instalment_pay(new ApexPages.StandardController(instalments[0]));
      instPay.newPayDetails.Value__c = 2230;
      instPay.newPayDetails.Date_Paid__c = system.today();
      instPay.newPayDetails.Payment_Type__c = 'Cash';
      instPay.addPaymentValue();
      instPay.savePayment();


      client_product_service__c product =  tf.createCourseProduct(booking, agency);
      client_product_service__c product2 =  tf.createCourseProduct(booking, agency);
      client_product_service__c product3 =  tf.createCourseProduct(booking, agency);

      client_course_product_pay prodPay = new client_course_product_pay(new ApexPages.StandardController(product));
      prodPay.newPayDetails.Value__c = 50;
      prodPay.newPayDetails.Payment_Type__c = 'CreditCard';
      prodPay.newPayDetails.Date_Paid__c = system.today();
      prodPay.addPaymentValue();
      prodPay.savePayment();


      ApexPages.currentPage().getParameters().put('cs', instalments[3].id);
      ApexPages.currentPage().getParameters().put('pd', product2.id);
      ApexPages.currentPage().getParameters().put('ct', client.id);

      client_course_create_invoice invoicePay = new client_course_create_invoice();
      invoicePay.createInvoice();
      invoicePay.newPayDetails.Value__c = 1600;
      invoicePay.newPayDetails.Payment_Type__c = 'Creditcard';
      invoicePay.newPayDetails.Date_Paid__c = system.today();
      invoicePay.addPaymentValue();
      invoicePay.savePayment();

      String invoiceId = invoicePay.invoice.id;


      Apexpages.currentPage().getParameters().put('id', string.valueOf(instalments[0].id));
      payments_reconciliation_edit_payment testClass = new payments_reconciliation_edit_payment();

      testClass.payments[0].Value__c += -10;

      testClass.newPayment.Value__c = 10;
      testClass.newPayment.Payment_Type__c = 'Covered by Agency';
      testClass.newPayment.Date_Paid__c = system.today();
      testClass.addPayment();

      Apexpages.currentPage().getParameters().put('type', 'cash');
      Apexpages.currentPage().getParameters().put('pDate', testClass.payments[0].Date_Paid__c.format());
      Apexpages.currentPage().getParameters().put('value', string.valueOf(2230));
      testClass.deletePayment();
      testClass.saveChanges();


      Apexpages.currentPage().getParameters().put('id', string.valueOf(instalments[0].id));
      testClass = new payments_reconciliation_edit_payment();
      testClass.payments[0].Value__c += -10;

      testClass.newPayment.Value__c = 10;
      testClass.newPayment.Payment_Type__c = 'Covered by Agency';
      testClass.newPayment.Date_Paid__c = testClass.payments[0].Date_Paid__c;
      testClass.addPayment();

      Apexpages.currentPage().getParameters().put('type', 'Covered by Agency');
      Apexpages.currentPage().getParameters().put('pDate', testClass.payments[0].Date_Paid__c.format());
      Apexpages.currentPage().getParameters().put('value', string.valueOf(10));
      testClass.deletePayment();
      testClass.saveChanges();

      Apexpages.currentPage().getParameters().put('id', string.valueOf(product.id));
      testClass = new payments_reconciliation_edit_payment();

      testClass.payments[0].Value__c += -10;

      testClass.newPayment.Value__c = 10;
      testClass.newPayment.Payment_Type__c = 'Covered by Agency';
      testClass.newPayment.Date_Paid__c = system.today();
      testClass.addPayment();
      testClass.saveChanges();

      Apexpages.currentPage().getParameters().put('type', 'CreditCard');
      Apexpages.currentPage().getParameters().put('pDate', testClass.payments[0].Date_Paid__c.format());
      Apexpages.currentPage().getParameters().put('value', string.valueOf(50));
      testClass.deletePayment();



      list<selectOption> addPaymentsType = testClass.addPaymentsType;

      user u = testClass.currentUser;

      Apexpages.currentPage().getParameters().put('id', invoiceId);
      testClass = new payments_reconciliation_edit_payment();

      testClass.payments[0].Value__c += -10;

      testClass.newPayment.Value__c = 10;
      testClass.newPayment.Payment_Type__c = 'Covered by Agency';
      testClass.newPayment.Date_Paid__c = system.today();
      testClass.addPayment();
      testClass.saveChanges();

      Apexpages.currentPage().getParameters().put('type', 'CreditCard');
      Apexpages.currentPage().getParameters().put('pDate', testClass.payments[0].Date_Paid__c.format());
      Apexpages.currentPage().getParameters().put('value', string.valueOf(1600));
      testClass.deletePayment();
    }
  }
}