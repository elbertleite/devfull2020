@isTest
private class agency_agreements_test {
	
	public static Account school {get;set;}
	public static Account agency {get;set;}
	public static Account agency2 {get;set;}
	public static Account campus {get;set;}
	public static Course__c course {get;set;}
	public static Campus_Course__c campusCourse {get;set;}
	
	@testSetup static void setup() {
		TestFactory tf = new TestFactory();

		agency = tf.createAgency();
		agency2 = tf.createAgency();
		school = tf.createSchool();
		campus = tf.createCampus(school, agency);
		course = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);

		//Agreement 1
		agreements_new.CallResult result = agreements_new.saveAgreement(null, 'Agreement Test', agency.Id, 'Test User', '2018-02-12', 'Direct', 'School', school.Id + ';' + agency.id, 'All Regions', null, null, 'true', 'false');
		
		//Agreement 2
		result = agreements_new.saveAgreement(null, 'Agreement Test', agency2.Id, 'Test User', '2018-02-12', 'Direct', 'School', school.Id + ';' + agency2.id, 'All Regions', null, null, 'true', 'false');
	}

	static testMethod void openConstructor(){
		agency_agreements test = new agency_agreements();
	}

	static testMethod void searchAgencies(){
		String agency = agency_agreements.searchAgencies([Select Id From Account where RecordType.Name = 'Agency Group' limit 1].Id);
	}

	static testMethod void updateAgreement(){
		String agencyId = [Select Id From Account where RecordType.Name = 'Agency' limit 1].Id;
		String agreements = agency_agreements.findAgreements(agencyId);

		agency_agreements.agreeWrapper result = (agency_agreements.agreeWrapper) JSON.deserialize(agreements, agency_agreements.agreeWrapper.class);
		agency_agreements.updateAgreements(agencyId, new list<String>{result.agreements[1].Id}, new list<String>{result.agreements[0].Id});
	}


}