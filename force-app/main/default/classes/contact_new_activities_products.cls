public with sharing class contact_new_activities_products {
	
	public String idContact{get;set;}
	public List<IPClasses.ClientChecklist> clientChecklist{get;set;}
	public User user{get;set;}

	public Destination_Tracking__c currentDTracking{get;set;}
	public contact_new_activities_products() {
		user = [Select Contact.Account.ParentID, AccountId, Contact.Account.Parent.Name, Contact.Account.Parent.ID From User where User.id = :UserInfo.getUserId() limit 1];
		idContact = ApexPages.currentPage().getParameters().get('id');

		currentDTracking = new IPFunctions().getContactCurrentCycle(idContact, user.Contact.Account.Parent.ID);

		loadActivitiesProducts();
	}

	public void loadActivitiesProducts(){
		try{
			Contact contact = [SELECT ID, Destination_Country__c FROM Contact WHERE ID = :idContact];

			String destination = currentDTracking != null ? currentDTracking.Destination_Country__c : contact.Destination_Country__c;
			String dTrackingID = currentDTracking != null ? currentDTracking.ID : null;

			Set<ID> agencyGroups = new Set<ID>();
			Map<String, String> agencieGroupNames = new Map<String, String>();
			Map<String, Client_Checklist__c> checklistSavedItens = new Map<String, Client_Checklist__c>();

			for(Client_Checklist__c cls : [Select Last_Saved__c, Agency_Group__c, Checklist_Item__c, Checklist_Item_Id__c, Checklist_stage__c , Client__c, Id, CreatedBy.Name, CreatedDate, Agency_Group_Name__c from Client_Checklist__c where Client__c = :idContact and Destination_Tracking__c = :dTrackingID]){
				agencyGroups.add(cls.Agency_Group__c);
				agencieGroupNames.put(cls.Agency_Group__c, cls.Agency_Group_Name__c);
				checklistSavedItens.put(cls.Checklist_Item__c, cls);
			}

			system.debug('ITENS SAVED '+JSON.serialize(checklistSavedItens));


			agencyGroups.add(user.Contact.Account.ParentID);
			agencieGroupNames.put(user.Contact.Account.ParentID, user.Contact.Account.Parent.Name);

			list<Agency_Checklist__c> listAgency = new UserDetails.GroupSettings().getAgencyChecklist(contact, agencyGroups, user, destination);

			system.debug('LIST AGENCIES RETURNED '+JSON.serialize(listAgency));

			if(listAgency != null && !listAgency.isEmpty()){
				clientChecklist = new List<IPClasses.ClientChecklist>();
				
				IPClasses.ClientChecklist checklist = new IPClasses.ClientChecklist();
				IPClasses.AgencyChecklist agencyChecklist = null;
				

				checklist.destination = contact.Destination_Country__c;
				checklist.activeChecklist = true;
				clientChecklist.add(checklist);

				Integer indexLoop = 0;
				Integer indexCurrentGroup;
				for(String ID : agencyGroups){
					agencyChecklist = new IPClasses.AgencyChecklist();
					agencyChecklist.agencyId = ID;
					agencyChecklist.agencyName = agencieGroupNames.get(ID);
					agencyChecklist.blockedForEdition = ID == user.Contact.Account.ParentID ? false : true;

					if(ID == user.Contact.Account.ParentID){
						indexCurrentGroup = indexLoop;
					}
					
					checklist.agencyChecklist.add(agencyChecklist);

					indexLoop++;
				}

				if(indexCurrentGroup != null && indexCurrentGroup != 0){
					agencyChecklist = checklist.agencyChecklist.get(indexCurrentGroup);
					checklist.agencyChecklist.remove(indexCurrentGroup);	
					if(checklist.agencyChecklist.size() >= 1){
						checklist.agencyChecklist.add(0, agencyChecklist);	
					}else{
						checklist.agencyChecklist.add(agencyChecklist);
					}
				}

				Integer indexElement; 
				IPClasses.Stage checklistStage;
				IPClasses.StageChecklist checklistItem;
				Client_Checklist__c checkedItem = null;
				for(Agency_Checklist__c item : listAgency){ //IN CASE THEY WANT TO ENABLE MULTIPLE ACTIVITIES & PRODUCTS PER DESTINATION - YOU WILL HAVE TO INSERT A NEW OUTER LOOP HERE THROUGH THE OBJECT IPClasses.ClientChecklist checklist SEARCHING FOR EACH DESTINATION
					indexElement = checklist.agencyChecklist.indexOf(new IPClasses.AgencyChecklist(item.Agency_Group__c));
					if(indexElement > -1){
						agencyChecklist = checklist.agencyChecklist.get(indexElement);
						
						checklistStage = new IPClasses.Stage(item.Checklist_stage__c);
						indexElement = agencyChecklist.stages.indexOf(checklistStage);
						
						if(indexElement > -1){
							checklistStage = agencyChecklist.stages.get(indexElement);
						}else{
							checklistStage.index = agencyChecklist.stages.size();
							agencyChecklist.stages.add(checklistStage);
						}

						checklistItem = new IPClasses.StageChecklist(item.ID);
						checklistItem.description = item.Checklist_Item__c;
						checklistItem.isChecked = false;
						checklistItem.markedToCheck = false;
						checklistItem.itemorder = item.itemOrder__c != null ? item.itemOrder__c.intValue() : null;
						
						checkedItem = checklistSavedItens.get(item.Checklist_Item__c);
						if(checkedItem != null){
							checklistItem.isChecked = true;
							checklistItem.checkListFollowUpId = checkedItem.id;
							checklistItem.checkerName = checkedItem.CreatedBy.Name;
							checklistItem.checkingDate = checkedItem.createdDate;
							checklistSavedItens.remove(item.ID);
						}

						checklistStage.itensChecklist.add(checklistItem);
						checkedItem = null;
					}

				}

				system.debug('FINAL CHECKLIST ACTIVITIES & PRODUCTS '+JSON.serialize(clientChecklist));
			}
		}catch(Exception e){
			system.debug('Error on loadActivitiesProducts() ===> ' + e.getLineNumber() + ' <==== '+e.getMessage());	
		}
	}

	public void uncheckFollowUp(){
		String itemID = ApexPages.currentPage().getParameters().get('itemid');
		Client_Checklist__c checked = [select id, Last_Saved__c from Client_Checklist__c where ID = :itemID];
		boolean isLastSaved = checked.Last_Saved__c;
		delete checked;

		if(isLastSaved){
			checked = [select id, Last_Saved__c from Client_Checklist__c ORDER BY CreatedDate DESC LIMIT 1];
			checked.Last_Saved__c = isLastSaved;
			update checked;
		}
	}

	public void saveCheckList(){
		Contact contact = [SELECT ID, Destination_Country__c FROM Contact WHERE ID = :idContact];
		String dTrackingID = currentDTracking != null ? currentDTracking.ID : null;
		
		List<Client_Checklist__c> checkListToSave = new List<Client_Checklist__c>();
		Client_Checklist__c itemToSave = null;
		for(IPClasses.ClientChecklist checklist : clientChecklist){
			for(IPClasses.AgencyChecklist agencyChecklist : checklist.agencyChecklist){
				if(agencyChecklist.agencyId == user.Contact.Account.ParentID && !agencyChecklist.blockedForEdition){
					for(IPClasses.Stage stage : agencyChecklist.stages){
						for(IPClasses.StageChecklist item : stage.itensChecklist){
							if(Test.isRunningTest()){
								item.markedToCheck = true;
							}
							if(item != null && item.markedToCheck){
								itemToSave = new Client_Checklist__c();
								itemToSave.Client__c = idContact;
								itemToSave.Agency_Group__c = user.Contact.Account.ParentId;
								itemToSave.agency__c = user.Contact.AccountId;
								itemToSave.Checklist_Item__c = item.description;
								itemToSave.Checklist_Item_Id__c = item.checklistId;
								itemToSave.itemOrder__c = item.itemOrder;
								itemToSave.Checklist_stage__c = stage.stageName;
								itemToSave.Destination__c = contact.Destination_Country__c;
								itemToSave.destination_tracking__c = dTrackingID;
								itemToSave.Last_Saved__c = false;
								checkListToSave.add(itemToSave);
							}
						}
					}
				}
			}
		}
		if(!checkListToSave.isEmpty()){
			checkListToSave.get(checkListToSave.size() - 1).Last_Saved__c = true;

			List<Client_Checklist__c> removeLastSave = [select id from Client_Checklist__c where Client__c = :contact.Id and Last_Saved__c = true AND destination_tracking__c = :dTrackingID];
			for(Client_Checklist__c cc : removeLastSave)
				cc.Last_Saved__c = false;
			update removeLastSave;

			insert checkListToSave;
		}

	}
}