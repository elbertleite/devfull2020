/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class contact_controller_test2 {

    static testMethod void myUnitTest() {
        
        TestFactory tf = new TestFactory();
		Account agency = tf.createAgency();
		tf.createChecklists(agency.parentID);
		Contact employee = tf.createEmployee(agency);		
		
		User portalUser = tf.createPortalUser(employee);
		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);
  		Course__c course = tf.createCourse();
		Campus_Course__c campusCourse = tf.createCampusCourse(campus, course);	
		
		 
		
		
		system.runAs(portalUser){
			
			ApexPages.currentPage().getParameters().put('id', employee.id);

			Contact client = tf.createLead(agency, employee);
			
			tf.createQuotation(client, campusCourse);
		
			contact_controller cc = new contact_controller();		
			cc.ctid = client.id;		
			cc.getContact();
        
        	
        	
        	list<string> selectedDestinationsList = cc.selectedDestinationsList;
			list<String> otherDestination  = cc.otherDestination;
			list<string> selectedProductTypeList = cc.selectedProductTypeList;
			list<string> selectedStudyTypeList = cc.selectedStudyTypeList;
			List<SelectOption> listleadSource = cc.listLeadSource;
			
			cc.refreshlistLeadsSource();
			List<SelectOption> leadSourceSpecific = cc.leadSourceSpecific;
			
			cc.clearCheckListAgency();
			cc.clearCycle();
			
			
			String clientChecklistItem = [Select id from Client_Checklist__c where client__c = :client.id and Last_Saved__c = true limit 1].id;
			ApexPages.currentPage().getParameters().put('itemid', clientChecklistItem);
			cc.deleteCheckList();
			
			

			Test.startTest();
			
			
			
			
			cc.getCheckListAgency();
			
			
			List<SelectOption> stageOptions = cc.stageOptions;
			cc.getlastItemsChecked();
			
			cc.saveCheckList();
			
			list<contact_controller.TrackingDestination> stageTracking = cc.stageTracking;
			list<contact_controller.TrackingDestination> checklistTracking = cc.checklistTracking;
			
			Client_Course_Overview__c clientCourse = cc.clientCourse;
			cc.getSearchType();
			cc.getCourses();
			
			cc.selectCustomCourse();
			cc.clearDates();
			cc.getCampuses();
			cc.saveCourse();
			
			String courseid = [select id from Client_Course_Overview__c where client__c = :client.id limit 1].id;
			
			ApexPages.currentPage().getParameters().put('CourseId', courseid);
			cc.editCourse();
			cc.cancelEditCourse();
			
			cc.deleteCourse();
			
			cc.findDestinationCourses();
			cc.saveTracking();
			boolean b = cc.renderCycleButton;
			
			cc.startNewCycle();
			cc.saveStatusReason();
			String itemid = [Select id from Client_Stage_Follow_Up__c where client__c = :client.id limit 1].id;
			String fid = ApexPages.currentPage().getParameters().put('itemid', itemid);
			cc.deleteFollowUp();
			cc.getClientStageAgency();
			cc.getStages();
			cc.saveClientStage();
			cc.getDestinationsTracking();
			
			Test.stopTest();
        	
		}
        
		
		
		
		
    }
}