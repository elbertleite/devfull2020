/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class student_ranking_test {

    static testMethod void myUnitTest() {
        TestFactory tf = new TestFactory();    	
    	
		Map<String,String> recordTypes = new Map<String,String>();       
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);
    	    	
		Account agency = tf.createAgency();
		Account school = tf.createSchool();
		Account campus = tf.createCampus(school, agency);
		
		Account campus2 = new Account();
		campus2.account_currency_iso_code__c = 'AUD';
		campus2.RecordTypeId = recordTypes.get('Campus');
		campus2.Name = 'Test Campus CBD';
		campus2.BillingCountry = 'Australia';
		campus2.BillingCity = 'Melbourne';
		campus2.ParentId = school.id;		
		insert campus2;
		
		Supplier__c sp2 = new Supplier__c();
		sp2.Agency__c = agency.id;
		sp2.Supplier__c = campus2.id;
		sp2.Record_Type__c = 'campus';
		sp2.Available__c = true;
		sp2.preferable__c = true;
		sp2.web_available__c = true;
		insert sp2;
		
		
		Account school2 = new Account();
		school2.recordtypeid = recordTypes.get('School');
		school2.name = 'Test School2';
		insert school2;
		
		Account campus3 = new Account();
		campus3.account_currency_iso_code__c = 'AUD';
		campus3.RecordTypeId = recordTypes.get('Campus');
		campus3.Name = 'Test Cairns';
		campus3.BillingCountry = 'Australia';
		campus3.BillingCity = 'Cairns';
		campus3.ParentId = school2.id;		
		insert campus3;
		
		Supplier__c sp3 = new Supplier__c();
		sp3.Agency__c = agency.id;
		sp3.Supplier__c = campus3.id;
		sp3.Record_Type__c = 'campus';
		sp3.Available__c = true;
		sp3.preferable__c = true;
		sp3.web_available__c = true;
		sp3.Student_Rank__c = 4;
		insert sp3;
	
		Contact employee = tf.createEmployee(agency);
		User portalUser = tf.createPortalUser(employee);	
		
		System.RunAs(portalUser){
			
			student_ranking sc = new student_ranking();
			sc.getDestinationCountries();
			sc.selectedDestinationCountry = 'Australia';
			sc.getDestinationCities();
			sc.getRankingOptions();
			sc.getSchools();
			
			sc.search();
			sc.save();
			
			
			sc.selectedDestinationCity = 'Cairns';
			sc.getSchools();
			sc.selectedSchool = school2.id;
			sc.selectedRanking = '4';
			sc.search();
			
			sc.selectedDestinationCity = 'Perth';
			sc.search();	
		}
        
    }
}