public with sharing class OwnershipHistoryController {


	//public list<Ownership_History__c> resultList {get;set;}
	public List<IPClasses.OwnershipHistory> resultList{get;set;}

	public OwnershipHistoryController(ApexPages.StandardController controller){
		Contact contact = (Contact)controller.getRecord();
		Contact ctt = [SELECT ID, Ownership_History_Field__c FROM Contact WHERE ID = :contact.ID];
		resultList = new List<IPClasses.OwnershipHistory>();
		if(!String.isEmpty(ctt.Ownership_History_Field__c)){
			resultList = (List<IPClasses.OwnershipHistory>) JSON.deserialize(ctt.Ownership_History_Field__c, List<IPClasses.OwnershipHistory>.class);
			resultList.sort();
		}
		//resultList = new List<Ownership_History__c>();
	  	//resultList = [Select From_Agency_Name__c, From_User__r.Name, To_Agency_Name__c,	To_User__r.Name, Action__c, CreatedDate From Ownership_History__c where Contact__c = :contact.Id order by lastModifiedDate desc];
	}

}