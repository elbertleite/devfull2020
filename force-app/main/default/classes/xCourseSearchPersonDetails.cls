public class xCourseSearchPersonDetails{
	
	public class mostVistedCourses{
		public String Campus{get; Set;}	
		public String Course{get; Set;}	
		public String Country{get; Set;}	
		public String City{get; Set;}	
		public integer total{get; Set;}
		public mostVistedCourses(String campus,String course,String country,String city, integer total){
			this.Campus = Campus;
			this.Course = Course;
			this.Country = Country;
			this.City = city;
			this.Total = total;
		}
	}
	
	private list<mostVistedCourses> mostVisitedCourses;
	public list<mostVistedCourses> getMostVisitedCourses(string destination){
		if(mostVisitedCourses == null && destination != ''){
			mostVisitedCourses = new list<mostVistedCourses>();
			getAgencyDetails();
			if(agencyUsers == null)
				getAgencyUsers(agencyDetails.Agency_Id);
			
			system.debug('@#$ agencyUsers: ' + agencyUsers);
			if(!Schema.sObjectType.Contact.fields.allowSaveQuotation__c.isAccessible()){
				for(AggregateResult ag:[Select Campus_Course__r.Campus__r.name campus, Campus_Course__r.Course__r.name course, Campus_Course__r.Campus__r.BillingCountry country, Campus_Course__r.Campus__r.BillingCity city, COUNT(Id) tot from Search_Courses__c  
									WHERE isCustomCourse__c = false and Campus_Course__r.Campus__r.BillingCountry = :destination AND CreatedBy.ID in :agencyUsers										
									GROUP BY Campus_Course__r.Campus__r.name, Campus_Course__r.Course__r.name, Campus_Course__r.Campus__r.BillingCountry, Campus_Course__r.Campus__r.BillingCity 
									ORDER BY COUNT(Id) desc LIMIT 3])
				{
					mostVisitedCourses.add(new mostVistedCourses((String)ag.get('campus'),(String)ag.get('course'),(String)ag.get('country'),(String)ag.get('city'),(integer)ag.get('tot') ));						
				}
										
			}else{
				for(AggregateResult ag:[Select Course_Name__c course, Campus_Name__c campus, Campus_Country__c country, Campus_City__c city, COUNT(Id) tot
										From Quotation_Detail__c where Campus_Country__c = :destination AND CreatedBy.ID in :agencyUsers
										group by Course_Name__c, Campus_Name__c, Campus_Country__c, Campus_City__c ORDER BY COUNT(Id) desc LIMIT 3])					
				{
					mostVisitedCourses.add(new mostVistedCourses((String)ag.get('campus'),(String)ag.get('course'),(String)ag.get('country'),(String)ag.get('city'),(integer)ag.get('tot') ));
				}
			}
		}
		return mostVisitedCourses;
		
	}
	
	private List<String> agencyUsers;
	private List<String> getAgencyUsers(String agencyID){
		if(agencyUsers == null){
			agencyUsers = new List<String>();
			for(User acct : [Select Id  From User WHERE AccountId = :agencyID  AND isActive = true])
				agencyUsers.add(acct.id);
		}
					
		return agencyUsers;
	}
	
	
	
	public class userDetails{
		public string Name {get; set;}
		public string Agency_Name {get; set;}
		public string Agency_Id {get; set;}
		public boolean Hide_Timetable {get; set;}
		public string AgencyGroup_Id {get; set;}
		public boolean Destination_Group_Id {get; set;}
		public string Global_Link_Id {get; set;}
		public string Agency_Optional_Currency {get; set;}
		public string Agency_account_currency_iso_code {get; set;}
		public string Agency_BillingCountry {get; set;}
		public string Agency_Phone {get; set;}
		public string User_Email {get; set;}
		public string User_Name {get; set;}
		
		public string Preferable_nationality {get; set;}
		public string Preferable_School_Country {get; set;}
		public string Preferable_School_City {get; set;}
		
		public string courseSearch_InstalOption {get; set;}
		public string courseSearch_PriceOrder {get; set;}
		public string courseSearch_SchoolType {get; set;}
		public string courseSearch_TuitionType {get; set;}
		public boolean Finance_Enrolment_User {get; set;}
		public integer Quote_Expiration_In_Days {get; set;}
		public boolean show_instalment_average {get; set;}
		
		public string interestType {get; set;}
		public integer numberInstalments {get{if(numberInstalments == null) numberInstalments = 0; return numberInstalments;} set;}
		public double Deposit {get{if(Deposit == null) Deposit = 0; return Deposit;} set;}
		public double Surcharge {get; set;}
		public double interest {get{if(interest == null) interest = 0; return interest;} set;}
		public boolean hasAgencyInstalments {get{if(hasAgencyInstalments == null) hasAgencyInstalments = false; return hasAgencyInstalments;} set;}
	}
	
	/*private Account UserDetails;
	public Account getUserDetails(){
		if(UserDetails == null)
			UserDetails = xCourseSearchFunctions.agencyDetails();
		return UserDetails;
	}*/
	
	private userDetails agencyDetails;
	public userDetails getAgencyDetails(){
		if(agencyDetails == null){
			agencyDetails = new userDetails();
			
			if(Test.isRunningTest()){
					
				Account agency = [Select id, name, account_currency_iso_code__c, Optional_Currency__c,  BillingCountry, phone, ParentId from account where name = 'EH Sydney' limit 1];	
				
				agencyDetails.Agency_Id = agency.id;
				agencyDetails.AgencyGroup_Id = agency.ParentId;
				agencyDetails.Agency_Optional_Currency = agency.Optional_Currency__c;
				agencyDetails.Agency_account_currency_iso_code = agency.account_currency_iso_code__c;
				agencyDetails.Agency_BillingCountry = agency.BillingCountry;
				agencyDetails.Agency_Name = agency.Name;
				agencyDetails.Agency_Phone = agency.phone;
					
			} else {
				
				User u = [SELECT id, Contact.Name, Finance_Enrolment_User__c, Contact.Account.Name, Contact.AccountId, Contact.Account.ParentId, Contact.Account.Hide_Timetable__c, Contact.Account.Parent.Global_Link__c, Contact.Account.Parent.Destination_Group__c, Contact.Account.account_currency_iso_code__c , Contact.Account.Optional_Currency__c, Contact.Account.BillingCountry,
								 Contact.Preferable_School_Country__c, Contact.Preferable_School_City__c, Contact.Preferable_nationality__c,
								 Contact.Account.courseSearch_InstalOption__c, Contact.Account.courseSearch_PriceOrder__c, Contact.Account.courseSearch_SchoolType__c, Contact.Account.courseSearch_TuitionType__c,
								 contact.account.phone, contact.Email, Contact.Account.Parent.Quote_Expiration_In_Days__c, Contact.Account.Parent.show_instalment_average__c
						 	FROM User where id = :UserInfo.getUserId() limit 1];
				
				agencyDetails.Name = u.Contact.Name;
				agencyDetails.Agency_Id = u.Contact.AccountId;
				agencyDetails.AgencyGroup_Id = u.Contact.Account.ParentId;
				agencyDetails.Destination_Group_Id = u.Contact.Account.Parent.Destination_Group__c;
				agencyDetails.Global_Link_Id = u.Contact.Account.Parent.Global_Link__c;
				agencyDetails.Agency_Optional_Currency = u.Contact.Account.Optional_Currency__c;
				agencyDetails.Agency_account_currency_iso_code = u.Contact.Account.account_currency_iso_code__c;
				agencyDetails.Agency_BillingCountry = u.Contact.Account.BillingCountry;
				agencyDetails.Agency_Name = u.Contact.Account.Name;
				agencyDetails.Agency_Phone = u.Contact.Account.phone;
				agencyDetails.User_Email = u.Contact.Email;
				agencyDetails.User_Name = u.Contact.Name;
				agencyDetails.Preferable_School_Country =  u.Contact.Preferable_School_Country__c;
				agencyDetails.Preferable_School_City =  u.Contact.Preferable_School_City__c;
				agencyDetails.Preferable_nationality = u.Contact.Preferable_nationality__c;
				agencyDetails.Hide_Timetable = u.Contact.Account.Hide_Timetable__c;
				agencyDetails.Finance_Enrolment_User = u.Finance_Enrolment_User__c;
				agencyDetails.Quote_Expiration_In_Days = integer.valueOf(u.Contact.Account.Parent.Quote_Expiration_In_Days__c);
				agencyDetails.show_instalment_average = u.Contact.Account.Parent.show_instalment_average__c;
				
				//agencyDetails.courseSearch_InstalOption = u.Contact.Account.courseSearch_InstalOption__c;
				agencyDetails.courseSearch_PriceOrder   = u.Contact.Account.courseSearch_PriceOrder__c;  
				agencyDetails.courseSearch_SchoolType   = u.Contact.Account.courseSearch_SchoolType__c;  
				agencyDetails.courseSearch_TuitionType  = u.Contact.Account.courseSearch_TuitionType__c; 
				
				System.debug('==user: '+ agencyDetails);
			}
			
			/*Account acco = [Select Name, Agency__pr.Name, Agency__pc, Agency__pr.Optional_Currency__c, Agency__pr.account_currency_iso_code__c, Agency__pr.BillingCountry, Preferable_nationality__c,Preferable_School_Country__c, Preferable_School_City__c from Account WHERE User__c = :UserInfo.getUserId() limit 1];
			
			agencyDetails.Name = acco.Name;
			agencyDetails.Agency_Name = acco.Agency__pr.Name;
			agencyDetails.Agency_Id = acco.Agency__pc;
			agencyDetails.Agency_Optional_Currency = acco.Agency__pr.Optional_Currency__c;
			agencyDetails.Agency_account_currency_iso_code = acco.Agency__pr.account_currency_iso_code__c;
			agencyDetails.Agency_BillingCountry = acco.Agency__pr.BillingCountry;
			agencyDetails.Preferable_nationality = acco.Preferable_nationality__c;
			agencyDetails.Preferable_School_Country = acco.Preferable_School_Country__c;
			agencyDetails.Preferable_School_City = acco.Preferable_School_City__c;*/
		}
		return agencyDetails;		
	}
	
	public class clienDetailsWrapper{
		public string id {get; set;}
		public string Nationality {get; set;}
		public string Name {get; set;}
		public string Agency_Name {get; set;}
		public String owner {get;set;}
	}
	
	private clienDetailsWrapper clienDetails;
	public clienDetailsWrapper getclienDetails(string clientId){
		if (clienDetails == null){
			contact cont = [Select Id, Nationality__c, Name, Current_Agency__r.Name, Owner__c from Contact where Id = :clientId];
			clienDetails = new clienDetailsWrapper();
			clienDetails.id = cont.id;
			clienDetails.Nationality = cont.Nationality__c;
			clienDetails.Name = cont.name;
			clienDetails.Agency_Name = cont.Current_Agency__r.Name;
			clienDetails.owner=cont.Owner__c;
		}
		return clienDetails;
	}
	
	private clienDetailsWrapper objCartClienDetails;
	public clienDetailsWrapper getobjCartClienDetails(string wsid){
		if(objCartClienDetails == null){
			objCartClienDetails = new clienDetailsWrapper();
			Web_Search__c objCart = [Select id, Client__c, Client__r.Name, Client__r.Owner__c, Client__r.Nationality__c, Client__r.Current_Agency__r.Name from Web_Search__c where Id = :wsid];
			objCartClienDetails.id = objCart.Client__c;
			objCartClienDetails.Nationality = objCart.Client__r.Nationality__c;
			objCartClienDetails.Name = objCart.Client__r.Name;
			objCartClienDetails.Agency_Name = objCart.Client__r.Current_Agency__r.Name;
			objCartClienDetails.owner = objCart.Client__r.Owner__c;
		}
		return objCartClienDetails;
	}
	
	
	
	public List<SelectOption> getSchoolType(){
		List<SelectOption> SchoolType = new List<SelectOption>();
		SchoolType.add(new SelectOption('all','All Schools'));
		SchoolType.add(new SelectOption('selectedschools','Favourite Schools'));		
		return SchoolType;
	}
	
	public List<SelectOption> getSchoolTuitionOption(){
		List<SelectOption>SchoolTuitionOption = new List<SelectOption>();
		SchoolTuitionOption.add(new SelectOption('tuition','Tuition Only'));
		SchoolTuitionOption.add(new SelectOption('fullprice','Full Price'));
		return SchoolTuitionOption;
	}
	
	public List<SelectOption> getSchoolPaymentPrice(){
		List<SelectOption> schoolPaymentPrice = new List<SelectOption>();
		schoolPaymentPrice.add(new SelectOption('full','Without Instalments'));
		schoolPaymentPrice.add(new SelectOption('instal','With Instalments'));
		return schoolPaymentPrice;
	}
	
	public List<SelectOption> getSearchOrder(){
		List<SelectOption> SearchOrder = new List<SelectOption>();
		SearchOrder.add(new SelectOption('totpriceDown','Price(Low - High)'));
		SearchOrder.add(new SelectOption('totpriceUp','Price(High - Low)'));
		SearchOrder.add(new SelectOption('campusName', 'Group by Campus'));
		SearchOrder.add(new SelectOption('onlywithpromo','Deals Only'));
		return SearchOrder;
	}
	
	public string returnUrlEmailPage(string clientId, string quoteId){
			Contact ac = [Select RecordType.Name from Contact where id = :clientId limit 1];
			
			if(ac.RecordType.Name == 'Lead')
				return '/apex/contact_quotationEmail?Id='+clientId + '&quoteId='+quoteid;
			else			
				return '/apex/contact_quotationEmail?Id='+clientId + '&quoteId='+quoteid;
					
	}
	

}