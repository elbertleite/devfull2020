@isTest
private class commissions_pds_confirmation_test {

	public static Account school {get;set;}
	public static Account agency {get;set;}
	public static Contact emp {get;set;}
	public static Contact client {get;set;}
	public static User portalUser {get{
		if (null == portalUser) {
			portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
			portalUser.Finance_Backoffice_User__c = true;
		}
		return portalUser;
	}set;}
	public static Account campus {get;set;}
	public static Course__c course {get;set;}
	public static Campus_Course__c campusCourse {get;set;}
	public static client_course__c clientCourseBooking {get;set;}
	public static client_course__c clientCourse {get;set;}
	public static client_course__c clientCourse2 {get;set;}


	@testSetup static void setup() {
		TestFactory tf = new TestFactory();
		Account sg = tf.createSchoolGroup();
		school = tf.createSchool();
		school.ParentId = sg.Id;
		update school;

		agency = tf.createAgency();
		emp = tf.createEmployee(agency);
		portalUser = tf.createPortalUser(emp);
		campus = tf.createCampus(school, agency);
		course = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);

		list<Back_Office_Control__c> bo = new list<Back_Office_Control__c>();
		bo.add(new Back_Office_Control__c(Agency__c = agency.id, Country__c = 'Australia', Backoffice__c = agency.id, Services__c = 'PDS_PCS_PFS_Chase'));
		insert bo;

		system.runAs(portalUser){
			client = tf.createClient(agency);
			clientCourseBooking = tf.createBooking(client);
			clientCourse = tf.createClientCourse(client, school, campus, course, campusCourse, clientCourseBooking);
			tf.createClientCourseFees(clientCourse, false);
			List<client_course_instalment__c> instalments =  tf.createClientCourseInstalments(clientCourse);

			//TO REQUEST PDS
			instalments[0].Received_By_Agency__c = agency.id;
			instalments[0].Received_Date__c =  system.today();
			instalments[0].Paid_To_School_On__c =  system.today();
			instalments[0].isPDS__c = true;
			instalments[0].isMigrated__c = false;
			instalments[0].PDS_Confirmation_Invoice__c = null;
			instalments[0].PDS_Requested_On__c = null;
			instalments[0].Commission_Confirmed_On__c = null;
			instalments[0].Due_Date__c =  system.today();

			update instalments;
		}
	}

	static testMethod void noCommClaim(){
		system.runAs(portalUser){
			commissions_request_pds req = new commissions_request_pds();
			req.selectedCountry = 'Australia';
			req.selectedSchoolGP = 'all';
			req.selectedSchool = 'all';
			req.searchName = '';
			req.dates.Commission_Paid_Date__c = system.today().addYears(1);
			req.searchToRequest();

			ApexPages.currentPage().getParameters().put('cancelId', req.result[0].campuses[0].instalments[0].installment.Id);
			req.setIdCancel();
		}
	}

	static testMethod void invoiceAll(){
		system.runAs(portalUser){
			commissions_request_pds req = new commissions_request_pds();
			req.selectedCountry = 'Australia';
			req.changeCountry();
			system.debug('req.allFilters==='+ req.allFilters);
			system.debug('req.schGroupOptions==='+ req.schGroupOptions);
			req.selectedSchoolGP = req.schGroupOptions[2].getValue();
			req.selectedSchool = 'all';
			req.searchName = '';

			list<SelectOption> dateFilterBy = req.dateFilterBy;
			list<SelectOption> paymentOptions = req.paymentOptions;

			req.dates.Commission_Paid_Date__c = system.today().addYears(1);
			req.searchToRequest();

			ApexPages.CurrentPage().getParameters().put('isGroup', 'true');

			req.invoiceAll();

			req.changeSchoolGP();
			req.changeagGroup();
		}
	}

	static testMethod void requestConfirmation(){
		system.runAs(portalUser){
			commissions_request_pds req = new commissions_request_pds();
			req.changeBackOffice();
			req.selectedCountry = 'Australia';
			req.changeCountry();
			req.selectedSchoolGP = 'all';
			req.selectedSchool = 'all';
			req.searchName = '';
			req.dates.Commission_Paid_Date__c = system.today().addYears(1);
			req.searchToRequest();
			list<SelectOption> instOptions = req.instOptions;
			System.debug('==>req.result: '+req.result);
			req.result[0].campuses[0].instalments[0].installment.isSelected__c = true;
			ApexPages.currentPage().getParameters().put('schId', string.valueOf(req.result[0].schoolId));
			req.setToRequest();
			ApexPages.CurrentPage().getParameters().put('cn', 'Australia');
			ApexPages.currentPage().getParameters().put('type', 'requestConfirm');
			PaymentSchoolEmail invoice = new PaymentSchoolEmail();
			invoice.toAddress = 'test@educationhify.com';
			invoice.setupEmail();
			invoice.createInvoice();


			Invoice__c inv = [Select Id from Invoice__c limit 1];
			ApexPages.currentPage().getParameters().put('i', string.valueOf(inv.id));
			commissions_pds_confirmation toTest = new commissions_pds_confirmation();

			List<SelectOption> commissionStatus = toTest.commissionStatus;

			toTest.sendRequestForm();

			toTest.instResult[0].instalments[0].inst.School_Request_Confirmation_Status__c = 'Refused';
			toTest.saveAll();
			toTest.sendRequestForm();

			list<client_course_instalment__c> requestInstalments = [Select Id from client_course_instalment__c where PDS_Confirmation_Invoice__c != null];
			System.assertEquals(requestInstalments.size()>0, true);
		}
	}
}