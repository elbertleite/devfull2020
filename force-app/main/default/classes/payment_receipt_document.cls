public class payment_receipt_document {

	
	private String docId {get;set;}
	public String previewLink {get;set;}
	
	public payment_receipt_document(ApexPages.StandardController controller){
	   	
	   	docId = controller.getId();
	   	
	}
	
	
	public void confirmDocs(){
		
		if(previewLink!=null && previewLink !=''){
			Client_Document__c toUpdate = new Client_Document__c(Id= docId, Preview_Link__c = previewLink);
			update toUpdate;
		}
		
		else{//User deleted all documents
			
			list<client_course_instalment__c> instUpdate = new list<client_Course_instalment__c>();
			map<string, pfsTotals> coursePfs = new map<string, pfsTotals>();//To update PFS totals on the courses
			
			
			
			String invoiceId = [SELECT Invoice__c FROM Client_Document__c WHERE id=:docId].Invoice__c;
			
			Invoice__c invoice = [SELECT Id, (SELECT Id,Client_Course__c, isPFS__c, Commission_Value__c, Instalment_Value__c FROM client_course_instalments_sch_invoice__r ORDER BY Received_Date__c)
									FROM Invoice__c WHERE id=:invoiceId];
			
			invoice.Paid_On__c = null;
			invoice.Paid_Date__c = null;
			invoice.Paid_By__c = null;
			invoice.Paid_By_Agency__c = null;
			
			for(client_course_instalment__c cci : invoice.client_course_instalments_sch_invoice__r){
					
				cci.Paid_To_School_On__c = null;
				cci.Paid_to_School_Date__c = null;
				cci.Paid_to_School_By_Agency__c = null;
				cci.Commission_Confirmed_By__c = null;
				cci.Commission_Confirmed_On__c = null;
				cci.Commission_Confirmed_By_Agency__c = null;
				cci.Commission_Due_Date__c = null;
				
				if(cci.isPFS__c && !coursePfs.containskey(cci.Client_Course__c)){
					coursePfs.put(cci.Client_Course__c, new pfsTotals(cci.Commission_Value__c, cci.Instalment_Value__c));	
				}
				else if(cci.isPFS__c && coursePfs.containskey(cci.Client_Course__c)){
					coursePfs.get(cci.Client_Course__c).sumPfsTotals(cci.Commission_Value__c, cci.Instalment_Value__c);	
				}
				
				instUpdate.add(cci);
			}//end for
			
			update invoice;
			update instUpdate;
			
			list<client_course__c> allcourses = new list<client_course__c>();
			
			//It has courses to be updated w/ PFS values
			if(coursePfs.size()>0){
				
				for(Client_Course__c cc : [SELECT Id, Total_PFS_Commission_Pending__c, Total_Paid_PFS__c FROM client_course__c WHERE id in :coursePfs.keySet()]){
					
					cc.Total_PFS_Commission_Pending__c += coursePfs.get(cc.id).totCommPending;
					cc.Total_Paid_PFS__c += coursePfs.get(cc.id).totPaidPfs;
					
					allcourses.add(cc);
				}//end for
			}
			
			
			if(allCourses.size()>0) 
				update allCourses;
			
			
			Client_Document__c doc = new Client_Document__c(Id= docId, Preview_Link__c = previewLink);
			update doc;
		}
		
	}
	
	
	
	private class pfsTotals{
		private decimal totCommPending {get;set;}
		private decimal totPaidPfs {get;set;}
		
		private pfsTotals (decimal totCommPending, decimal totPaidPfs){
			this.totCommPending = totCommPending;
			this.totPaidPfs = totPaidPfs;
			system.debug('11111this.totCommPending==>' + this.totCommPending);
			system.debug('1111this.totPaidPfs==>' + this.totPaidPfs);
		}
		
		private void sumPfsTotals(decimal totCommPending, decimal totPaidPfs){
			
			system.debug('this.totCommPending==>' + this.totCommPending);
			system.debug('this.totPaidPfs==>' + this.totPaidPfs);
			this.totCommPending += totCommPending;
			this.totPaidPfs += totPaidPfs;
		}
		
	}
	
	
	
}