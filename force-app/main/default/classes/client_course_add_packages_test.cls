@isTest
private class client_course_add_packages_test {

	public static Account school {get;set;}
	public static Account agency {get;set;}
	public static Contact emp {get;set;}
	public static Contact client {get;set;}
	public static User portalUser {get{
		if (null == portalUser) {
			portalUser = [Select Id, Name from User where email = 'test12345@test.com' limit 1];
		}
		return portalUser;
	}set;}
	public static Account campus {get;set;}
	public static Account campus2 {get;set;}
	public static Course__c course {get;set;}
	public static Course__c course2 {get;set;}
	public static Campus_Course__c campusCourse {get;set;}
	public static Campus_Course__c campusCourse2 {get;set;}
	public static client_course__c clientCourseBooking {get;set;}
	public static client_course__c clientCourse {get;set;}


	@testSetup static void setup() {
		TestFactory tf = new TestFactory();

		Account schGroup = tf.createSchoolGroup(); 
		school = tf.createSchool();
		school.ParentId = schGroup.Id;
		update school;
		agency = tf.createAgency();
		emp = tf.createEmployee(agency);
		portalUser = tf.createPortalUser(emp);
		campus = tf.createCampus(school, agency);
		campus2 = tf.createCampus(school, agency);
		course = tf.createCourse();
		course2 = tf.createCourse();
		campusCourse =  tf.createCampusCourse(campus, course);
		campusCourse2 =  tf.createCampusCourse(campus2, course2);

		system.runAs(portalUser){
			client = tf.createClient(agency);
		}
	}

	static testMethod void addPkg() {
		Contact client = [Select Id From Contact where RecordType.Name = 'Client' limit 1];

		Map<String,String> recordTypes = new Map<String,String>();
		for( RecordType r :[select id, name from recordtype where isActive = true] )
			recordTypes.put(r.Name,r.Id);

		Account taxCountry = new Account(Tax_Name__c = 'GST', Tax_Rate__c =10, Name = 'Australia');
		insert taxCountry;

		Account taxCity = new Account(Tax_Name__c = 'GST', Tax_Rate__c =10, ParentId = taxCountry.Id, Name = 'Sydney', RecordTypeId = recordTypes.get('City'));
		insert taxCity;

		list<String> courses = new list<String>();
		for(Campus_Course__c c : [Select Id from Campus_Course__c])
			courses.add(c.Id);

		system.runAs(portalUser){
			ApexPages.currentPage().getParameters().put('cs', String.join(courses, ','));
			ApexPages.currentPage().getParameters().put('cid', client.Id);
			client_course_add_packages test = new client_course_add_packages();

			List<SelectOption> getNumberOfUnits = test.getNumberOfUnits();
			List<SelectOption> unitType = test.unitType ;
			List<SelectOption> clientTypeOpt = test.clientTypeOpt ;

			test.saveCourses();

		}

	}
	
}