trigger CreateAccountPicturePreviewLink on Account_Picture_File__c (before update, before delete) {
    
    if(Trigger.isUpdate){
        List<ID>  parentIDs = new List<ID>();
        List<ID> fileObjects = new List<ID>();
            
        for(Account_Picture_File__c file : Trigger.new){
            if(file.Content_Type__c != 'Folder')
                if( file.URL__c == null || file.URL__c == '' ){
                    parentIDs.add(file.Parent__c);
                    fileObjects.add(file.id);   
                }        
        }
        
        if(!parentIDs.isEmpty() && !fileObjects.isEmpty()){
            for(string url :cg.SDriveTools.getAttachmentURLs( parentIDs , fileObjects, (571138519))){
                for(Account_Picture_File__c af: Trigger.new){
                    if(url.contains(af.id)){
                        af.URL__c = url;
                        break;
                    }
                }
            }   
        }
        if(!parentIDs.isEmpty())
            update [select id, LastModifiedDate from Account where id in :parentIDs];   
            
    } else if(Trigger.isDelete){
        
        List<ID> parentIDs = new List<ID>();    
        for(Account_Picture_File__c file : Trigger.old)
            if(file.Content_Type__c != 'Folder')
                parentIDs.add(file.Parent__c);

        if(!parentIDs.isEmpty())
            update [select id, LastModifiedDate from Account where id in :parentIDs];               
        
    }
}