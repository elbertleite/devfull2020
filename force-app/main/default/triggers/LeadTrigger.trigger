trigger LeadTrigger on Lead (after insert) {
	
	List<Lead> leads = new List<Lead>();
	
	for(Lead lead : trigger.new)
		if(lead.Agency_Country__c == 'Netherlands' || lead.Agency_Country__c == 'Brazil' || lead.Agency_Country__c == 'Spain')
			leads.add(lead);
				
			
	if(!leads.isEmpty()){	
		List<Task> tasks = new List<Task>();			
		for(Lead l : leads){
									
			Task t = new Task();
			t.WhoID = l.id;
			t.Status = 'Completed';
			t.ActivityDate = system.today();		
			tasks.add(t);
		}
		insert tasks;
	}	
    	
}