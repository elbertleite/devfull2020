trigger ConvertLeadToContact on Task (after insert) {
    
    set<string> leads = new set<string>();
    string leadId = '';
    for(Task t:trigger.new){
        if(t.WhoID != null){
            leadId = t.WhoID;       
            if(leadId.startsWith('00Q')){// it's a lead
                leads.add(t.WhoID);
            }
        }
    }
    
    System.debug('==>leads: '+ leads);
 	
    Lead tLead = new Lead();
    if(leads.size() > 0){
        Map<String,Contact> mapEmailsLead = new map<String,Contact>();
        list<String> listAgencyGroup = new list<String>();
        list<String> listEmailLead = new list<String>();
        
        //Considering that object Tasks is not used in the system we are not expecting bulk, so it get one lead per time
        for(lead led:[Select id, Agency_Country__c, Email_Subscription__c, Form_URL__c, email, Residence_Country__c, Residence_State__c, Residence_City__c, Destinations__c, 
        					Message__c, Study_Area__c, Looking_for__c, Other_Destination__c, Other_Product_Type__c, skype__c,	
    						When_are_you_planning_to_travel__c, Original_Agency__c, Agency_Group__c, isLead__c, how_long_are_you_going_for__c, level_of_interest__c, 
    						firstname, lastname, Phone, MobilePhone, Residence_Street__c, Residence_PostCode__c, leadsource, gender__c, nationality__c, age_range__c    						
    						from Lead where id in:leads and LastActivityDate != null]){
            tLead = led;         	
         	break;   
        }        
        
        /** Check the Lead Account **/
      	String leadAccount;
  		String leadAgencyGroup;
  		
  		
        System.debug('==>tLead.Residence_Country__c: '+ tLead.Residence_Country__c);
        System.debug('==>tLead.Agency_Country__c: '+ tLead.Agency_Country__c);
        System.debug('==>tLead.Agency_Group__c: '+ tLead.Agency_Group__c);
        
        if(tLead.Original_Agency__c != null){
            leadAccount = tLead.Original_Agency__c;
            leadAgencyGroup = tLead.Agency_Group__c;
        }else if(tLead.Residence_Country__c!= null && tLead.Residence_Country__c != ''){            
                String globalLinkId = [Select Global_Link__c from Account where id = :tLead.Agency_Group__c limit 1].Global_Link__c;                
                 System.debug('==>globalLinkId: '+globalLinkId);
            try{
                Account agency = [select id, ParentId from Account where Global_Link__c =:globalLinkId and BillingCountry = :tLead.Residence_Country__c and View_Web_Leads__c = true and Inactive__c = false LIMIT 1];                    
               	leadAccount = agency.id; 
                leadAgencyGroup = agency.ParentId;
            }catch (Exception e){
                system.debug('Error====' + e);
                Account agency = [select id, ParentId from Account where Global_Link__c =:globalLinkId and BillingCountry = :tLead.Agency_Country__c and View_Web_Leads__c = true and Inactive__c = false LIMIT 1];                    
               	leadAccount = agency.id; 
               	leadAgencyGroup = agency.ParentId;
            }	
        }else{
            //Left without try catch block on purpose to easily identify groups without view web leads agencies                 
            Account agency = [select id, ParentId from Account where ParentID = :tLead.Agency_Group__c and View_Web_Leads__c = true and Inactive__c = false LIMIT 1];                    
            leadAccount= agency.id; 
            leadAgencyGroup = agency.ParentId;
        }
        /** END Check the Lead Account **/
       
       
        System.debug('==>leadAccount: '+ leadAccount);  
        System.debug('==>leadAgencyGroup: '+ leadAgencyGroup);
                   
        for(Contact acco:[Select id, Email_Subscription__c, Email, Owner__c,
        						(select detail__c, type__c from forms_of_contact__r where (type__c = 'Home Phone' or type__c = 'Mobile')),
        						(select detail__c, type__c from social_networks__r where type__c = 'Skype' LIMIT 1)
        				 	From Contact where Email = :tLead.email and Account.ParentId = :leadAgencyGroup order by LastModifiedDate]){
            mapEmailsLead.put(acco.Email,acco);
         	 System.debug('==>mapEmailsLead: '+ acco);   
        }
        
        
        
        
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        list<Database.LeadConvert> listLc = new list<Database.LeadConvert>();
        Database.LeadConvert lc;
        webFormInquiry__c wfe;
        Custom_Note_Task__c newTask;
        
        list<Custom_Note_Task__c> lTask = new list<Custom_Note_Task__c>();
        List<Contact> laccs=  new List<Contact>();
        list<webFormInquiry__c> wf = new list<webFormInquiry__c>();
		list<forms_of_contact__c> formsOfContact = new list<forms_of_contact__c>();
		list<social_network__c> sNetworks = new List<Social_Network__c>();
        system.debug('@> leads ' + leads);
        
        boolean overwrite_lead_info = false;
        try {
        	overwrite_lead_info = [select Overwrite_Lead_Info__c from account where id = :tlead.agency_group__c].overwrite_lead_info__c;        	
        } catch (Exception e){}
		
       	
        //for(lead l:[Select id, Agency_Country__c, Email_Subscription__c, Form_URL__c, email, Residence_Country__c, Residence_State__c, Residence_City__c, Destinations__c, LeadSource, How_long_are_you_going_for__c,  Message__c, Study_Area__c, Looking_for__c, Other_Destination__c, Other_Product_Type__c,When_are_you_planning_to_travel__c, Original_Agency__c, Agency_Group__c, isLead__c from Lead where id in:leads and Agency_Group__c in :listAgencyGroup  and LastActivityDate != null]){
        if(tLead.email != null && mapEmailsLead.containsKey(tLead.email)){
            Contact a = new Contact(id=mapEmailsLead.get(tLead.email).id, Last_Enquiry__c = System.now());
            
			system.debug('@@@ Overwrite_Lead_Info__c ? ' + overwrite_lead_info);
			
			boolean addToList = false;
			
			if(!mapEmailsLead.get(tLead.email).Email_Subscription__c && tLead.Email_Subscription__c){
                a.Email_Subscription__c = tLead.Email_Subscription__c;
                addToList = true;
            }
			
			if(overwrite_lead_info){
				
				if(!isEmpty(tlead.firstName))
					a.FirstName = tlead.firstName;
				if(!isEmpty(tlead.lastName))
					a.lastName = tlead.lastName;
				if(!isEmpty(tLead.Residence_Country__c)){
					a.MailingCountry = tLead.Residence_Country__c;
					a.MailingState = tLead.Residence_State__c;
					a.MailingCity = tLead.Residence_City__c;
					a.MailingStreet = tlead.Residence_Street__c;
					a.MailingPostalCode = tlead.Residence_PostCode__c;
				}
				
				
				
				if(!isEmpty(tlead.leadsource))
					a.leadsource = tlead.leadsource;
				if(!isEmpty(tlead.message__c))
					a.lead_message__c = tlead.message__c;
				if(!isEmpty(tlead.gender__c))
					a.gender__c = tlead.gender__c;
				if(!isEmpty(tlead.nationality__c))
					a.nationality__c = tlead.nationality__c;
				if(!isEmpty(tlead.how_long_are_you_going_for__c))
					a.travel_duration__c = tlead.how_long_are_you_going_for__c;
				if(!isEmpty(tlead.when_are_you_planning_to_Travel__c))
					a.When_are_you_planning_to_travel__c = tlead.when_are_you_planning_to_Travel__c;
				if(!isEmpty(tlead.level_of_interest__c))
					a.lead_level_of_interest__c = tlead.level_of_interest__c;
				if(!isEmpty(tlead.form_url__c))
					a.web_form_url__c = tlead.form_url__c;
				if(!isEmpty(tlead.destinations__c))
					a.lead_destinations__c = tlead.destinations__c;
				if(!isEmpty(tlead.study_area__c))
					a.lead_study_type__c = tlead.study_area__c;
				if(!isEmpty(tlead.looking_for__c))
					a.lead_product_type__c = tlead.looking_for__c;
				if(!isEmpty(tlead.age_range__c))
					a.age_range__c = tlead.age_range__c;
				
				
				if(tlead.Phone != null && tlead.Phone.trim() != ''){
					a.Phone = tlead.Phone;
					boolean found = false;
					if(mapEmailsLead.get(tLead.email).forms_of_contact__r != null && mapEmailsLead.get(tLead.email).forms_of_contact__r.size() > 0){					
						for(forms_of_contact__c foc : mapEmailsLead.get(tLead.email).forms_of_contact__r){
							if(foc.type__c == 'Home Phone'){
								foc.detail__c = a.phone;
								foc.country__c = a.MailingCountry;
								formsOfContact.add(foc);
								found = true;
								break;
							}
						}						
						
					} 
					if(!found){
						Forms_of_Contact__c foc = new Forms_of_Contact__c();
						foc.Contact__c = a.id;
						foc.country__c = a.MailingCountry;
						foc.detail__c = a.phone;
						foc.type__c = 'Home Phone';
						formsOfContact.add(foc);
					}
				}
				system.debug('@tlead.MobilePhone: ' + tlead.MobilePhone);
				if(tlead.MobilePhone != null && tlead.MobilePhone.trim() != ''){
					boolean found = false;
					a.MobilePhone = tlead.MobilePhone;
					if(mapEmailsLead.get(tLead.email).forms_of_contact__r != null && mapEmailsLead.get(tLead.email).forms_of_contact__r.size() > 0){
						for(forms_of_contact__c foc : mapEmailsLead.get(tLead.email).forms_of_contact__r){
							if(foc.type__c == 'Mobile'){
								foc.detail__c = a.mobilephone;
								foc.country__c = a.MailingCountry;
								formsOfContact.add(foc);
								found = true;
								break;
							}
						}				
						
					}
					if(!found){
						Forms_of_Contact__c foc = new Forms_of_Contact__c();
						foc.Contact__c = a.id;
						foc.country__c = a.MailingCountry;
						foc.detail__c = a.MobilePhone;
						foc.type__c = 'Mobile';
						formsOfContact.add(foc);
					}
				
				}
				
				
				if(tlead.skype__c != null && tlead.skype__c.trim() != ''){
					
					a.Lead_Skype__c = tlead.skype__c;
					if(mapEmailsLead.get(tLead.email).social_networks__r != null && mapEmailsLead.get(tLead.email).social_networks__r.size() > 0){
						for(social_network__c sn : mapEmailsLead.get(tLead.email).social_networks__r){							
								sn.detail__c = a.Lead_Skype__c;								
								sNetworks.add(sn);
								break;
							
						}				
						
					} else {
						Social_Network__c sn = new Social_network__c();
						sn.Contact__c = a.id;						
						sn.detail__c = a.Lead_Skype__c;
						sn.type__c = 'Skype';
						sNetworks.add(sn);
					}
				
				}
				
				addToList = true;
			}
            
            if(addToList)
            	laccs.add(a);
            
            
            
            wfe = new webFormInquiry__c();
            wfe.Account__c = mapEmailsLead.get(tLead.email).id;
            wfe.Country__c = tLead.Residence_Country__c;
            wfe.State__c = tLead.Residence_State__c;
            wfe.City__c = tLead.Residence_City__c;
            wfe.Destination__c = tLead.Destinations__c;
            wfe.How_did_you_hear_about_us__c = tLead.LeadSource;
            wfe.How_long_are_you_going_for__c = tLead.How_long_are_you_going_for__c;
            wfe.Message__c = tLead.Message__c;
            wfe.Study_Area__c = tLead.Study_Area__c;
            wfe.What_are_you_looking_for__c = tLead.Looking_for__c;
            wfe.Other_Destination__c = tLead.Other_Destination__c;
            wfe.Other_Product_Type__c = tLead.Other_Product_Type__c;
            wfe.When_are_you_planning_to_travel__c = tLead.When_are_you_planning_to_travel__c;
            wfe.Form_URL__c = tLead.Form_URL__c;
            wf.add(wfe);
            
            if(mapEmailsLead.get(tLead.email).owner__c != null){
                newTask = new Custom_Note_Task__c();
                newTask.Related_Contact__c = mapEmailsLead.get(tLead.email).Id;
                newTask.Due_Date__c = System.today();
                newTask.Assign_To__c = mapEmailsLead.get(tLead.email).owner__c;
                newTask.Status__c = 'In Progress';
                newTask.Subject__c = 'Web Enquiry';
                newTask.Comments__c = tLead.Message__c != null ? tLead.Message__c : '(No message)';
                //newTask.ReminderDateTime = System.now();
                //newTask.IsReminderSet = true;
                lTask.add(newTask);
            }           
            
        } else{                
            lc = new database.LeadConvert();
            lc.setConvertedStatus(convertStatus.MasterLabel);
            lc.setDoNotCreateOpportunity(true);
            lc.setLeadId(tLead.id);
         	lc.setAccountID(leadAccount);     
            listLc.add(lc);
            system.debug('lc=====' + lc);
        }
        
        Savepoint sp = Database.setSavepoint();
        try{
            if(listLc.size() > 0)
                Database.convertLead(listLc);
            if(wf.size() > 0)
                insert wf;
            if(lTask.size() > 0){
                insert lTask; 
            }  
            if(wf.size() > 0){
                delete [Select id from Lead where id in :leads];    
            }
            if(laccs.size() > 0)
                update laccs;
                
            if(formsOfContact.size() > 0)
            	upsert formsOfContact;
            	
            if(sNetworks.size() > 0)
            	upsert sNetworks;	
            	
        }catch(Exception e){
            System.debug('==> Lead Exception: '+e);
            Database.rollback(sp);
        }
    }
    
    public boolean isEmpty(String str){
    	return str != null && str.trim() != '' ? false : true;
    }
}