/**
    * CreateCampusLogoURL - <description>
    * Created by BrainEngine Cloud Studio
    * @author: Administrator
    * @version: 1.0
*/

trigger CreateCampusLogoURL on Account_Logo_File__c bulk (before update) {
    
    List<ID> parentIDs = new List<ID>();
    List<ID> fileObjects = new List<ID>();
        
    for(Account_Logo_File__c file : Trigger.new){
        if(file.Content_Type__c != 'Folder'){
            parentIDs.add(file.Parent__c);
            fileObjects.add(file.id);
        }
    }
    
    Map<String, String> accountLogos = new Map<String,String>();
    
    if(!parentIDs.isEmpty() && !fileObjects.isEmpty()){
        
        for(string url :cg.SDriveTools.getAttachmentURLs( parentIDs , fileObjects, (571138519))){
            for(Account_Logo_File__c af: Trigger.new){
                if(url.contains(af.id)){
                    accountLogos.put(af.Parent__c, url);
                    //af.Parent__r.Campus_Photo_URL__c = url;
                    break;
                }
            }
        }
        
        List<Account> campi = [Select id, ParentId, Campus_Photo_URL__c from Account where ParentId in :accountLogos.keySet() or id in :accountLogos.keySet()];
        for(Account campus : campi){
            campus.Campus_Photo_URL__c = accountLogos.get(campus.ParentId);
            if(accountLogos.containsKey(campus.id))
            	campus.Campus_Photo_URL__c = accountLogos.get(campus.id);
        }
        
        update campi;
        
    }

}