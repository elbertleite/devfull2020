trigger AccountTrigger on Account (after update, after insert) {
    
    if(Trigger.isAfter){
    	String schoolRecordType = '';
    	String campusRecordType = '';
		String countryRecordType = '';
		String cityRecordType = '';
		
		for(RecordType rt : [Select id, Name from RecordType where (Name = 'Campus' or name = 'Country' or name = 'City' or name = 'School') and isActive = true]){
			if(rt.name == 'Campus')
				campusRecordType = rt.id;
			else if(rt.name == 'Country')
				countryRecordType = rt.id;
			else if(rt.name == 'City')
				cityRecordType = rt.id;
			else if(rt.name == 'School')
				schoolRecordType = rt.id;
		}
		
		if(Trigger.isInsert){
			
			Set<ID> groups = new Set<ID>();
			List<AccountShare> shareList = new List<AccountShare>();
			
			
			for(Group g : [Select Id, RelatedId from Group where RelatedId in (select UserRoleId from USER WHERE API_User__c = true) AND Type = 'Role'])	
				groups.add(g.id);
			
			if(!groups.isEmpty()){
				for(Account acc : Trigger.new){				
					if(acc.RecordTypeID == campusRecordType || acc.RecordTypeID == countryRecordType || acc.RecordTypeID == cityRecordType || acc.RecordTypeID == schoolRecordType){
					
						for(String groupid : groups){
							AccountShare share = new AccountShare();
							share.AccountID = acc.id;
							share.UserOrGroupId = groupid;
							share.AccountAccessLevel = 'Read';
							share.OpportunityAccessLevel = 'None';
							shareList.add(share);						
						}
					}				
				}
			}
			if(!shareList.isEmpty())
				insert shareList;
		
		} else if(Trigger.isUpdate){
			
			
			
			
			
			Map<String, List<String>> updatedFields = new Map<String, List<String>>();
			List<Translation__c> updateTranslation = new List<Translation__c>();
	
			for(Account acc : Trigger.old){
				
				if(acc.RecordTypeID == campusRecordType){
	
					List<String> fields = new List<String>();
					
					if(acc.General_Description__c != trigger.newMap.get(acc.id).General_Description__c)
						fields.add(Account.General_Description__c.getDescribe().getName());				
					if(acc.Near_to__c != trigger.newMap.get(acc.id).Near_to__c)
						fields.add(Account.Near_to__c.getDescribe().getName());				
					if(acc.Why_choose_this_campus__c != trigger.newMap.get(acc.id).Why_choose_this_campus__c)
						fields.add(Account.Why_choose_this_campus__c.getDescribe().getName());
					if(acc.Features__c != trigger.newMap.get(acc.id).Features__c)
						fields.add(Account.Features__c.getDescribe().getName());
					if(acc.Extra_Activities__c != trigger.newMap.get(acc.id).Extra_Activities__c)
						fields.add(Account.Extra_Activities__c.getDescribe().getName());
					if(acc.Nationality_Mix__c != trigger.newMap.get(acc.id).Nationality_Mix__c)
						fields.add(Account.Nationality_Mix__c.getDescribe().getName());
					
					if(!fields.isEmpty())
						updatedFields.put(acc.id, fields);
	
				} else if(acc.RecordTypeID == countryRecordType || acc.RecordTypeID == cityRecordType){
					
					List<String> fields = new List<String>();
					
					if(acc.General_Description__c != trigger.newMap.get(acc.id).General_Description__c)
						fields.add(Account.General_Description__c.getDescribe().getName());
					if(acc.showcase_Cost_of_Living__c != trigger.newMap.get(acc.id).showcase_Cost_of_Living__c)
						fields.add(Account.showcase_Cost_of_Living__c.getDescribe().getName());
						
					if(!fields.isEmpty())
						updatedFields.put(acc.id, fields);	
				}
			}
			
			
			if(!updatedFields.keySet().isEmpty()){
				for(Translation__c tr : [select Account__c, Status__c, Updated_Fields__c from translation__c where account__c in :updatedFields.keySet()]){
	
					List<String> updated = updatedFields.get(tr.Account__c);
					if(tr.Updated_Fields__c == null || tr.Updated_Fields__c == '')
						tr.Updated_Fields__c = String.join(updated, ', ');
					else{
						for(String str : updated)
							if(!tr.Updated_Fields__c.contains(str))
								tr.Updated_Fields__c += ', ' + str;
					}			
					tr.status__c = 'Update Required';
					updateTranslation.add(tr);
				}
				update updateTranslation;
			}
		
		}
		
	}
    
}