trigger instalment_history on client_course_instalment__c (before update, after update) {

	map<Id, client_course_instalment__c> inst;
	map<Id, Account> acco;
	set<Id> allAcco = new set<Id>();
	client_course_instalment__c oldinstalment;

	if(trigger.isBefore && trigger.isUpdate){

		//Verify Pay to Agency
		for(client_course_instalment__c instalment : trigger.new){
			oldinstalment = trigger.oldmap.get(instalment.Id);
			if(oldinstalment.Received_Date__c == null && instalment.Received_Date__c != null)
				allAcco.add(instalment.Received_by_Agency__c);
		}//end for

		if(allAcco.size()>0){
			acco = new map<Id,Account>([SELECT Id, ParentId FROM Account  WHERE id in :allAcco]);

			Set<ID> ids = Trigger.newMap.keySet(); 
			inst = new map<Id, client_course_instalment__c>([SELECT client_course__r.Enroled_by_Agency__c, client_course__r.Enroled_by_Agency__r.ParentId, client_course__r.share_commission_request_agency__c, client_course__r.share_commission_request_agency__r.ParentId, client_course__r.Client__r.Current_Agency__c, client_course__r.Client__r.Current_Agency__r.ParentId FROM client_course_instalment__c WHERE id in :ids]);
		}
		//--- END Verify Pay to Agency 
		String currentAgency; 
		Account receivedAgency;
		client_course_instalment__c cci;
		for(client_course_instalment__c instalment : trigger.new) {
			oldinstalment = trigger.oldmap.get(instalment.Id);
			if(instalment.Discount__c != oldinstalment.Discount__c){
				instalment.Discounted_On__c = System.today();
				instalment.Discounted_By__c = Userinfo.getUserId();
			}
			
			//Pay to Agency
			if(oldinstalment.Received_Date__c == null && instalment.Received_Date__c != null ){
				receivedAgency = acco.get(instalment.Received_by_Agency__c);
				cci = inst.get(instalment.id);
				currentAgency = cci.client_course__r.Client__r.Current_Agency__c;
				// System.debug('==>receivedAgency.ParentId: '+receivedAgency.ParentId);
				// System.debug('==>cci.client_course__r.Client__r.Current_Agency__r.ParentId: '+cci.client_course__r.Client__r.Current_Agency__r.ParentId);
				// System.debug('==>currentAgency: '+currentAgency);
				// System.debug('==>cci.client_course__r.Enroled_by_Agency__c: '+cci.client_course__r.Enroled_by_Agency__c);
				// System.debug('==>cci.client_course__r.share_commission_request_agency__c: '+cci.client_course__r.share_commission_request_agency__c);
				if(!instalment.isMigrated__c && receivedAgency != null && receivedAgency.ParentId != cci.client_course__r.Client__r.Current_Agency__r.ParentId && currentAgency != cci.client_course__r.Enroled_by_Agency__c && currentAgency != cci.client_course__r.share_commission_request_agency__c){
					instalment.pay_to_agency__c = cci.client_course__r.Client__r.Current_Agency__c;
				}


				
			}
			else if(oldinstalment.Received_Date__c != null && instalment.Received_Date__c == null){
				instalment.pay_to_agency__c = null;
			}

			//Update backoffice commission for PCS/PDS instalments
			if( (oldinstalment.Received_Date__c == null && instalment.Received_Date__c != null && (instalment.isPds__c || instalment.isPcs__c)) || (instalment.isMigrated__c && !oldinstalment.PFS_Pending__c && instalment.PFS_Pending__c)){
				try{
					Date commissionDate;
					if(instalment.Due_Date__c <= instalment.Course_Start_Date__c){
						commissionDate = instalment.Course_Start_Date__c;
						commissionDate = commissionDate.addDays(7);
					}
					else{
						commissionDate = instalment.due_date__c;
						commissionDate = commissionDate.addDays(7);
					}
					instalment.Commission_Due_Date__c = commissionDate;
					instalment.PDS_Confirmation_Invoice__c = null;
					instalment.Request_Commission_Invoice__c = null;
					// instalment.Back_Office_Commission__c = [Select id from Back_Office_Control__c where Agency__c = :instalment.Received_By_Agency__c and Country__c = :instalment.client_course__r.Course_Country__c AND Services__c Includes ('PDS_PCS_PFS_Chase') limit 1].id;
				}catch (Exception e){
					system.debug('Error====' + e);
				}
			}


			//get back office from agency payging to school
			// if(oldinstalment.Paid_To_School_On__c == null && instalment.Paid_To_School_On__c != null && instalment.isPfs__c){
			// 	try{
			// 		instalment.Back_Office_Commission__c = [Select id from Back_Office_Control__c where Agency__c = :instalment.Paid_to_School_By_Agency__c and Country__c = :instalment.client_course__r.Course_Country__c  AND Services__c Includes ('PDS_PCS_PFS_Chase') limit 1].id;
			// 	}catch (Exception e){
			// 		system.debug('Error====' + e);
			// 	}
			// }


			//remove commission back office if is not PDS/PFS/PCS
			// if(!instalment.isPds__c && !instalment.isPcs__c && !instalment.isPfs__c){
			// 	instalment.Back_Office_Commission__c = null;
			// }

			instalment.isSelected__c = false;

		}
	}



	//Create Actvity for Enrolment Process
	if(trigger.isAfter && trigger.isUpdate){
		
		map<String, list<client_course_instalment__c>> courseMap = new map<String, list<client_course_instalment__c>>();
		client_course_instalment__c cci;

		Set<ID> ids = Trigger.newMap.keySet(); 
		inst = new map<Id, client_course_instalment__c>([SELECT Received_By__r.Name, Received_By__r.Contact.Account.Name, Received_Date__c, client_course__r.course_package__c, client_course__c, isFirstPayment__c, Required_for_COE__c, client_course__r.Course_Name__c, isPds__c, isPcs__c FROM client_course_instalment__c WHERE id in :ids]);
		
		for(client_course_instalment__c instalment : trigger.new){
			oldinstalment = trigger.oldmap.get(instalment.Id);
			cci = inst.get(instalment.id);
			
			if(oldinstalment.Received_Date__c == null && cci.Received_Date__c != null && (cci.isFirstPayment__c || cci.Required_for_COE__c)){
				
				String courseId = cci.client_course__r.course_package__c == NULL ? cci.client_course__c : cci.client_course__r.course_package__c;
			
				if(!courseMap.containsKey(courseId))
					courseMap.put(courseId, new list<client_course_instalment__c>{cci});
				else
					courseMap.get(courseId).add(cci);
			
			}

		}//end for

		list<client_course__c> updateCourses = [SELECT Enrolment_Activities__c FROM client_course__c WHERE Id in :courseMap.keySet()];

		for(client_course__c cc : updateCourses){
			String activity = cc.Enrolment_Activities__c != NULL ? cc.Enrolment_Activities__c : '';
			String payType;

			for(client_course_instalment__c ci : coursemap.get(cc.id)){

				payType = ci.isPds__c ? 'Instalment marked as PDS' : ci.isPcs__c ? 'Instalment marked as PCS' : 'Instalment Paid by Client';

				activity += EnrolmentManager.createActivity(payType, payType + ' - ' +  ci.client_course__r.Course_Name__c, ci.Received_Date__c, ci.Received_By__r.Name + ' ('+ ci.Received_By__r.Contact.Account.Name+')');

			}//end for - instalment

			cc.Enrolment_Activities__c = activity;

		}//end for - course

		update updateCourses;
	}

}