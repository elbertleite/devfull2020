trigger UpdateContactDetails on User (before insert, before update) {
   
   if(Trigger.isInsert){
   		for(User u : trigger.new){
   			u.UserPreferencesShowCityToExternalUsers = true;
			u.UserPreferencesShowCountryToExternalUsers = true;
			u.UserPreferencesShowEmailToExternalUsers = true;
			u.UserPreferencesShowFaxToExternalUsers = true;
			u.UserPreferencesShowPostalCodeToExternalUsers = true;
			u.UserPreferencesShowStateToExternalUsers = true;
			u.UserPreferencesShowStreetAddressToExternalUsers = true;
			u.UserPreferencesShowWorkPhoneToExternalUsers = true;
   		}
   } else if(Trigger.isUpdate){ 
    
	    Set<ID> ids = new Set<ID>();
	    for(User u : trigger.new)
	        ids.add(u.ContactID);
	    
	    IPFunctions.updateUserContact(ids);
	    
   }
}