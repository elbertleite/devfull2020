trigger AddressSetCurrent on Address__c (after delete, after insert, after update) {
    
    if (Trigger.isInsert || Trigger.isUpdate){
        
        Map<String, String> contactCurrentAddress = new Map<String, String>();

        for (Address__c ad : Trigger.new) {
            if (ad.Current_Address__c){
                Contact ct = new Contact(id = ad.Contact__c);

                ct.MailingCity       = ad.City__c;
                ct.MailingCountry    = ad.Country__c;
                ct.MailingState      = ad.State__c;
                ct.MailingStreet     = ad.Street__c;
                ct.MailingPostalCode = ad.Post_Code__c;
                ct.MailingLatitude   = ad.Location__Latitude__s;
                ct.MailingLongitude  = ad.Location__Longitude__s;                               
                update ct;
                
                contactCurrentAddress.put(ad.Contact__c, ad.id);
                
            }
        }
        
        
        List<Address__c> listAddresses = [select id, Current_Address__c from Address__c where Contact__c in :contactCurrentAddress.keySet() and id not in :contactCurrentAddress.values() and Current_Address__c = true];
        for(Address__c ad : listAddresses)
            ad.Current_Address__c = false;
        update listAddresses;
    }
    
    //This method ensure that at least one address is checked as "current address"
    if ( (Trigger.isAfter && Trigger.isUpdate) || Trigger.isDelete){
        Set<String> allContacts = new Set<String>();
        Set<String> contactsWithCurrentAddress = new Set<String>();
        
        List<Address__c> lad;
        if(Trigger.isDelete)
            lad = Trigger.old;
        else 
            lad = Trigger.new; 
        
        for (Address__c od : lad)
            allContacts.add(od.Contact__c);
        
        for(Address__c ad : [Select Contact__c from Address__c where Contact__c in :allContacts and Current_Address__c = true])
            contactsWithCurrentAddress.add(ad.Contact__c);
        
        allContacts.removeAll(contactsWithCurrentAddress);
        
        List<Address__c> newCurrentAddresses = new List<Address__c>();
        
        for(Contact c : [Select id, (Select Current_Address__c from Addresses__r order by LastModifiedDate desc limit 1) from Contact where id in :allContacts]){
            for(Address__c ad : c.addresses__r){
                ad.Current_Address__c = true;
                newCurrentAddresses.add(ad);
            }
        }
        
        update newCurrentAddresses;

            
    }
    
    
}