trigger ClientCourseTrigger on Client_Course__c (before insert, before update) {

	if(Trigger.isBefore){
		if(EnrolmentManager.runTrigger && (Trigger.isInsert || Trigger.isUpdate)){
			List<Client_Course__c> actualCourses = new List<Client_Course__c>();
			for (Client_Course__c so : Trigger.new)
				if(so.Enrolment_Start_Date__c != null && !so.isDeposit__c && !so.isBooking__c && !so.isCourseCredit__c)
					actualCourses.add(so);

			try {
				EnrolmentManager.setEnrolmentStatus(actualCourses);
			} catch (Exception e){
				system.debug(e);
			}
		}
	}


}