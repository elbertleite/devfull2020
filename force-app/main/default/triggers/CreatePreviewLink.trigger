/**
    * CreatePreviewLink - Generates the preview link for 20 years.
    * Created by BrainEngine Cloud Studio
    * @author: Administrator
    * @version: 1.0
*/

trigger CreatePreviewLink on Account_Document_File__c (before update) {

    List<ID>  parentIDs = new List<ID>();
    List<ID> fileObjects = new List<ID>();
        
    for(Account_Document_File__c file : Trigger.new){
        if(file.Content_Type__c != 'Folder')
            if( file.Preview_Link__c == null || file.Preview_Link__c == '' ){
                parentIDs.add(file.Parent__c);
                fileObjects.add(file.id);   
            }        
    }
    
    if(!parentIDs.isEmpty() && !fileObjects.isEmpty()){
        for(string url :cg.SDriveTools.getAttachmentURLs( parentIDs , fileObjects, (571138519))){
            for(Account_Document_File__c af: Trigger.new){
                if(url.contains(af.id)){
                    af.Preview_Link__c = url;
                    break;
                }
            }
        }   
    }
    

}