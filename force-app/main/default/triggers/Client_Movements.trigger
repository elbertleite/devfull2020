trigger Client_Movements on Contact (after insert,after update,after delete, before insert, before update) {
    
    system.debug('<===== TRIGGER SIZE '+trigger.size);
    boolean employeeFound = true;
    User employee;
//  Account userAgency;
//   Map<String,String> accountRecordTypes = new Map<String,String>{};
    boolean isAPI = false;
        
    try {
        
        employee = [Select Id, ContactId, Contact.AccountId, API_User__c, Contact.Account.ParentID, Contact.Account.BillingCountry, Contact.Account.Global_Link__c  from User where id =:UserInfo.getUserId() Limit 1];
        isAPI = employee.API_User__c;
        // for(RecordType rt: [Select Name, Id From RecordType where sObjectType='Contact' and isActive=true])
        //     accountRecordTypes.put(rt.Name,rt.Id);
        
        System.debug('---> employee: ' + UserInfo.getUserId() + ' - '+employee);
        
        if(employee != null)
            employeeFound = true;
        else
            employeeFound = false;
            
    } catch(Exception e){
        employeeFound = false;
    }
    
    // if(employeeFound) {
    //     try {
    //         userAgency = [Select ParentID, BillingCountry from Account where id = :employee.Contact.AccountId];
    //     } catch(Exception e){}
    // }
    
    if (Trigger.isAfter){
        
        /** Check if Contact were Merged **/
        if (Trigger.isDelete){
            
            set<Id> contactIds = new set<Id>();
            String winningContactId;
            
            
            List<Custom_Note_Task__c> updateTask = new List<Custom_Note_Task__c>();
            
            for(Contact ctOld : Trigger.old){
                
                if(ctOld.masterRecordID != null){ //This is not the winning contactId from the merge                    
                    system.debug('Contact Name === '+ ctOld.Name);                  
                    contactIds.add(ctOld.Id);    
                    
                    winningContactId = ctOld.masterRecordID;            
                }
                                
            }//end for
            
            system.debug('contactIds === '+ contactIds);
            system.debug('winningContactId === '+ winningContactId);
            List<Custom_Note_Task__c> listTasksNotes = new list <Custom_Note_Task__c>([Select Id, Related_Contact__c From Custom_Note_Task__c where Related_Contact__c in :contactIds]);
            system.debug('listTasksNotes === '+ listTasksNotes);                
            
            for(Custom_Note_Task__c t : listTasksNotes){
                t.Related_Contact__c = winningContactId;
                updateTask.add(t);
            }
            
            update updateTask;
                    
            
        }
        
        
        if (Trigger.isInsert){
            //========== Lead==================
            system.debug('INSERT DESTINATION TRACKING ');
            list<webFormInquiry__c> wf = new list<webFormInquiry__c>();
            list<Contact> newEnquiries = new list<Contact>();
            Forms_of_contact__c FormsOfContact;
            list<Forms_of_contact__c> listFormsOfContact = new list<Forms_of_contact__c>();
            Social_Network__c SocialNetwork;
            list<Social_Network__c> listSocialNetwork = new list<Social_Network__c>();
            
            Map<String, Contact> allContacts = new Map<String, Contact>(); 
            List<Destination_Tracking__c> lDestinationTrack = new List<Destination_Tracking__c>();
            String agencyGroup;
            String agency;
            for (Contact ac : Trigger.new){ 
                allContacts.put(ac.ID, ac);
                if(ac.isLead__c){                   
                    newEnquiries.add(ac);
                
                    //=========Add new Social Network=========================
                    if(ac.Lead_Skype__c != null && ac.Lead_Skype__c != ''){
                        SocialNetwork = new Social_Network__c();
                        SocialNetwork.Contact__c = ac.id; 
                        SocialNetwork.Detail__c = ac.Lead_Skype__c;
                        SocialNetwork.Type__c = 'Skype'; 
                        listSocialNetwork.add(SocialNetwork);               
                    }
                    //=========Add forms of contact=========================
                    if(ac.MobilePhone != null && ac.MobilePhone != ''){
                        FormsOfContact = new Forms_of_contact__c();
                        FormsOfContact.Contact__c = ac.id;
                        if(ac.Lead_Residence_Country__c != null)
                            FormsOfContact.Country__c = ac.Lead_Residence_Country__c;
                        else FormsOfContact.Country__c = ac.Lead_Agency_Country__c;
                        FormsOfContact.Detail__c = ac.MobilePhone;
                        FormsOfContact.Type__c = 'Mobile';
                        formsOfContact.from_Website__c = ac.Lead_From_Website__c;
                        listFormsOfContact.add(FormsOfContact);
                    }
                    if(ac.Phone != null && ac.Phone != ''){
                        FormsOfContact = new Forms_of_contact__c();
                        FormsOfContact.Contact__c = ac.id;
                        if(ac.Lead_Residence_Country__c != null)
                            FormsOfContact.Country__c = ac.Lead_Residence_Country__c;
                        else FormsOfContact.Country__c = ac.Lead_Agency_Country__c;
                        FormsOfContact.Detail__c = ac.Phone;
                        FormsOfContact.Type__c = 'Home Phone';
                        formsOfContact.from_Website__c = ac.Lead_From_Website__c;
                        listFormsOfContact.add(FormsOfContact);
                    }
                    
                    //Create Destination Tracking if contact selected only 1 destination                    
                   /* if(ac.Destination_Country__c != null && ac.Destination_Country__c != ''){   
                        Destination_Tracking__c newDT = new Destination_Tracking__c();
                        newDT.Destination_Country__c = ac.Destination_Country__c;                           
                        newDT.Client__c = ac.id;
                        newDT.Destination_City__c = ac.Destination_City__c;
                        newDT.Expected_Travel_date__c = ac.Expected_Travel_Date__c;
                        newDT.Arrival_Date__c = ac.Arrival_Date__c;
                        
                        lDestinationTrack.add(newDT);           
                    }*/
                    if(ac.Destination_Country__c != null && ac.Destination_Country__c != ''){   
                        Destination_Tracking__c newDT = new Destination_Tracking__c();
                        newDT.Destination_Country__c = ac.Destination_Country__c;                           
                        newDT.Client__c = ac.id;
                        newDT.Destination_City__c = ac.Destination_City__c;
                        newDT.Expected_Travel_date__c = ac.Expected_Travel_Date__c;
                        newDT.Arrival_Date__c = ac.Arrival_Date__c;
                        newDT.Current_Cycle__c = true;
                        newDT.Lead_Last_Change_Status_Cycle__c = Datetime.now();
                        newDT.Lead_Last_Stage_Cycle__c = 'Stage 0';
                        newDT.Lead_Last_Status_Cycle__c = 'NEW LEAD';
                        
                        if(!String.isEmpty(employee.Contact.Account.ParentID)){
                            agencyGroup = employee.Contact.Account.ParentID;
                            agency = employee.Contact.AccountId;
                        }else{
                            Contact ctt = [SELECT Original_Agency__c, Original_Agency__r.Parent.ID, Account.ID, Account.Parent.ID, Current_Agency__c, Current_Agency__r.Parent.ID FROM Contact WHERE ID = :ac.id];
                            if(!String.isEmpty(ctt.Original_Agency__r.Parent.ID)){
                                agencyGroup = ctt.Original_Agency__r.Parent.ID;
                                agency = ctt.Original_Agency__c;
                            }else if(!String.isEmpty(ctt.Account.Parent.ID)){
                                agencyGroup = ctt.Account.Parent.ID;
                                agency = ctt.Account.ID;
                            }else{
                                agencyGroup = ctt.Current_Agency__r.Parent.ID;
                                agency = ctt.Current_Agency__c;
                            }
                        }

                        newDT.Agency_Group__c = agencyGroup;
                        newDT.Current_Agency__c = agency;
                        newDT.Created_By_Agency__c = agency;
                        
                        if(!String.isEmpty(ac.RDStation_Last_Conversion__c)){
                            newDT.RDStation_Campaigns__c = ac.RDStation_Last_Conversion__c;
                        }
                        if(!String.isEmpty(ac.RDStation_Current_Campaign__c)){
                            newDT.RDStation_First_Conversion__c = ac.RDStation_Current_Campaign__c;
                        }

                        lDestinationTrack.add(newDT);           
                    }else{  // Save a Destination Tracking regardless of the destination chosen. 
                        Destination_Tracking__c newDT = new Destination_Tracking__c();
                        newDT.Client__c = ac.id;
                        newDT.Current_Cycle__c = true;
                        newDT.Lead_Last_Change_Status_Cycle__c = Datetime.now();
                        newDT.Lead_Last_Stage_Cycle__c = 'Stage 0';
                        newDT.Lead_Last_Status_Cycle__c = 'NEW LEAD';

                        if(!String.isEmpty(employee.Contact.Account.ParentID)){
                            agencyGroup = employee.Contact.Account.ParentID;
                            agency = employee.Contact.AccountId;
                        }else{
                            Contact ctt = [SELECT Original_Agency__c, Original_Agency__r.Parent.ID, Account.ID, Account.Parent.ID, Current_Agency__c, Current_Agency__r.Parent.ID FROM Contact WHERE ID = :ac.id];
                            if(!String.isEmpty(ctt.Original_Agency__r.Parent.ID)){
                                agencyGroup = ctt.Original_Agency__r.Parent.ID;
                                agency = ctt.Original_Agency__c;
                            }else if(!String.isEmpty(ctt.Account.Parent.ID)){
                                agencyGroup = ctt.Account.Parent.ID;
                                agency = ctt.Account.ID;
                            }else{
                                agencyGroup = ctt.Current_Agency__r.Parent.ID;
                                agency = ctt.Current_Agency__c;
                            }
                        }

                        newDT.Agency_Group__c = agencyGroup;
                        newDT.Current_Agency__c = agency;
                        newDT.Created_By_Agency__c = agency;
                        
                        if(!String.isEmpty(ac.RDStation_Last_Conversion__c)){
                            newDT.RDStation_Campaigns__c = ac.RDStation_Last_Conversion__c;
                        }
                        if(!String.isEmpty(ac.RDStation_Current_Campaign__c)){
                            newDT.RDStation_First_Conversion__c = ac.RDStation_Current_Campaign__c;
                        }
                        
                        lDestinationTrack.add(newDT);           
                    }       
                }
            }   
            
            insert lDestinationTrack;
                    
            if(listFormsOfContact.size() > 0)
                insert listFormsOfContact;      
            
            if(listSocialNetwork.size() > 0)
                insert listSocialNetwork;           
            
            if(newEnquiries.size() > 0){
                Set<String> groups = new Set<String>();
                Client_Stage_Follow_Up__c followUp = null;
                List<Client_Stage_Follow_Up__c> followUps = new List<Client_Stage_Follow_Up__c>();
                Map<String, Destination_Tracking__c> cyclesPerContact = new Map<String, Destination_Tracking__c>();
                Map<String, Client_Stage__c> newLeadStagesPerGroup = new Map<String, Client_Stage__c>();
                for(Destination_Tracking__c cycle : lDestinationTrack){
                    cyclesPerContact.put(cycle.Client__c, cycle);
                    groups.add(cycle.Agency_Group__c);
                }

                for(Client_Stage__c stage : [SELECT ID, Stage_description__c, Agency_Group__c FROM Client_Stage__c WHERE Agency_Group__c IN :groups AND Stage__c = 'Stage 0' AND Stage_description__c = 'NEW LEAD']){
                    newLeadStagesPerGroup.put(stage.Agency_Group__c, stage);
                }

                Client_Stage__c stageNewLead = null;
                Destination_Tracking__c cycle = null;
                for(Contact a : newEnquiries){
                    if(a.Lead_Assignment_To__c == null){
                        wf.add(addNewWebFormInquiry(a, a.id));
                    }
                    system.debug('INSERTING FOLLOW UP TO CONTACT '+a.ID);
                    cycle = cyclesPerContact.get(a.ID);
                    if(cycle != null){
                        //a.Status__c = 'NEW LEAD';
                        //a.Lead_Stage__c = 'Stage 0';
                        followUp = new Client_Stage_Follow_Up__c();
                        followUp.Checked_By__c = UserInfo.getUserId();
                        followUp.Client__c = a.ID;
                        followUp.Agency_Group__c = cycle.Agency_Group__c;
                        followUp.agency__c = a.AccountId;
                        followUp.Stage_Item__c = 'NEW LEAD';
                        followUp.Stage__c = 'Stage 0';
                        followUp.Destination__c = cycle.Destination_Country__c;
                        followUp.Destination_Tracking__c = cycle.ID;
                        followUp.Last_Saved_Date_Time__c = Datetime.now();
                        
                        stageNewLead = newLeadStagesPerGroup.get(cycle.Agency_Group__c);
                        if(stageNewLead != null){
                            followUp.Stage_Item_Id__c = stageNewLead.ID;
                        }

                        system.debug('FOLLOW UP SAVED    '+a.ID);

                        followUps.add(followUp);
                    }
                    /*if(!String.isEmpty(a.AccountId)){
                        try{
                            if(!String.isEmpty(groupID)){
                                system.debug('GROUP FOUND '+groupID);
                                if(stageNewLead == null || stageNewLead.Agency_Group__c != groupID){
                                    stageNewLead = ;
                                }
                                system.debug('NEW LEAD FOUND '+stageNewLead.id);
                                idCycle = [SELECT ID FROM Destination_Tracking__c WHERE Client__c = :a.ID ORDER BY CreatedDate desc LIMIT 1].ID;

                                followUp = new Client_Stage_Follow_Up__c();
                                followUp.Client__c = a.ID;
                                followUp.Agency_Group__c = groupID;
                                followUp.agency__c = a.AccountId;
                                followUp.Stage_Item__c = stageNewLead.Stage_description__c;
                                followUp.Stage_Item_Id__c = stageNewLead.ID;
                                followUp.Stage__c = 'Stage 0';
                                followUp.Destination__c = String.isEmpty(a.Destination_Country__c) ? '' : a.Destination_Country__c;
                                followUp.Destination_Tracking__c = idCycle;
                                followUp.Last_Saved_Date_Time__c = Datetime.now();

                                followUps.add(followUp);
                            }
                        }catch(Exception e){
                            system.debug('Error checking NEW LEAD on group ' +groupID+ ' ' + e.getLineNumber() + ' '+e.getMessage());
                        }
                    }*/
                }
                insert wf;
                insert followUps;
                //update newEnquiries;
                
            }

            //========== End Lead==================

            List<Contact> agencyList = new List<Contact>();
        
            for (Contact ac : Trigger.new) {
                System.debug('---> RecordTypeId: ' + ac.RecordType.Name + ' - '+ac.RecordType + ' ac: '+ac);
                //if(ac.RecordTypeID == accountRecordTypes.get('Client')){
                if(ac.RecordType.Name == 'Client'){
                    Contact editAgency = ac.clone(true);
                    if(employeeFound){  
                        editAgency.AccountId = employee.Contact.AccountId;
                        editAgency.Original_Agency__c = employee.Contact.AccountId;
                    }
                    agencyList.add(editAgency);
                }
            
            }
            if(agencyList.size()>0)
                update agencyList;
        }
        
        if(trigger.isUpdate){
            
            //------------- Ownership History ----------------------
            if(trigger.size == 1){
                for(Contact oldC : Trigger.old){
                    for(Contact newC : Trigger.new){
                        if(oldC.ID == newC.ID && oldC.email != newC.email){
                            Contact ctt = [SELECT Name, Current_Agency__r.Parent.RDStation_Emails_To_Send__c, LastModifiedBy.Name FROM Contact WHERE ID = :newC.ID];
                            User user = [SELECT ID, Contact.Account.Parent.Name FROM User WHERE ID = :UserInfo.getUserId()];
                            if(!String.isEmpty(ctt.Current_Agency__r.Parent.RDStation_Emails_To_Send__c)){
                                String currentEnvironment = IPFunctions.getCurrentEnvironment();
                                String url;
                                if(currentEnvironment == 'production')
                                    url = 'https://educationhify.force.com/ip/apex/contact_new_activity_log?id=';
                                else if(currentEnvironment == 'devpartial')
                                    url = 'https://devpartial-educationhify.cs5.force.com/ip/apex/contact_new_activity_log?id=';
                                else
                                    url = 'https://devfull-educationhify.cs73.force.com/ip/apex/contact_new_activity_log?id=';
                                boolean first = true; 
                                String emailTo;
                                String emailBody = 'The contact '+ctt.Name+' had the email changed in HiFy Platform. This info couldn’t be updated in RD Station.<br/<br/>Old Email: '+oldC.email+'<br/>New Email: '+newC.email+'<br/>URL: '+url+newC.ID+'<br/>Last Modified By: '+ctt.LastModifiedBy.Name;
                                Map<String, String> emailsCopy = new Map<String, String>();
                                for(String email : (List<String>) JSON.deserialize(ctt.Current_Agency__r.Parent.RDStation_Emails_To_Send__c, List<String>.class)){
                                    if(first){
                                        emailTo = email;
                                        first = false;
                                    }else{
                                        emailsCopy.put(email, email);
                                    }
                                }
                                String subject = 'RD Station Sync - Email Changed in HiFy - '+ctt.Name;
                                if(!String.isEmpty(user.Contact.Account.Parent.Name)){
                                    subject = subject + ' - '+user.Contact.Account.Parent.Name;
                                }
                                IPFunctions.sendEmail('Education Hify', 'alerts.rdstation@educationhify.com', emailTo, emailTo, emailsCopy, subject, emailBody);
                            }
                        }
                    }
                }
            }
            
            if(IPFunctions.firstRun){
                
                IPFunctions.firstRun=false;
                
                set<id> sIds = new set<id>();
                
                for (Contact ct : Trigger.new){ 
                    sIds.add(ct.Owner__c);
                }
                for (Contact ctOld : Trigger.old){
                    sIds.add(ctOld.Owner__c);
                }
                
                system.debug('sIds===' + sIds);
                    
                map<string, string> mapContacts = new map<string, string>();    
                List<User> owners = [Select id, Contact.AccountId From User Where id in :sIds];
                for(User o : owners)            
                    mapContacts.put(o.Id, o.Contact.AccountId);
                
                system.debug('MapContacts===' + mapContacts);
                list<Ownership_History__c> lOH = new list<Ownership_History__c>();
                for (Contact ct : Trigger.new) 
                    for (Contact ctOld : Trigger.old){
                        
                        if(ct.id == ctOld.id && ct.Owner__c != ctOld.Owner__c){                     
                            
                            
                            if(ct.Owner__c == employee.Id && ctOld.Owner__c!=employee.Id){
                                
                                Ownership_History__c o = new Ownership_History__c();                            
                                o.From_Agency__c = ctOld.Owner__c != null ? mapContacts.get(ctOld.Owner__c): null;
                                o.From_User__c = ctOld.Owner__c != null ? ctOld.Owner__c : null;
                                o.To_Agency__c = mapContacts.get(ct.Owner__c);
                                o.To_User__c = ct.Owner__c;
                                o.Action__c = 'Taken';
                                o.Contact__c = ct.ID;
                                lOH.add(o);
                            }
                            
                            if(ct.Owner__c != employee.Id && ctOld.Owner__c==employee.Id){
                                
                                Ownership_History__c o = new Ownership_History__c();                            
                                o.From_Agency__c = ctOld.Owner__c != null ? mapContacts.get(ctOld.Owner__c) : null;
                                o.From_User__c = ctOld.Owner__c != null ? ctOld.Owner__c : null;
                                o.To_Agency__c = mapContacts.get(ct.Owner__c);
                                o.To_User__c = ct.Owner__c;
                                o.Action__c = 'Given';
                                o.Contact__c = ct.ID;
                                lOH.add(o);
                            }
                            
                            if(ct.Owner__c != employee.Id && ctOld.Owner__c!=employee.Id){
                                
                                Ownership_History__c o = new Ownership_History__c();                            
                                o.From_Agency__c = ctOld.Owner__c != null ? mapContacts.get(ctOld.Owner__c) : null;
                                o.From_User__c = ctOld.Owner__c != null ? ctOld.Owner__c : null;
                                o.To_Agency__c = mapContacts.get(ct.Owner__c);
                                o.To_User__c = ct.Owner__c;
                                o.Action__c = 'Assigned';
                                o.Contact__c = ct.ID;
                                lOH.add(o);
                            }
                        }else if(ct.id == ctOld.id && ct.Owner__c == ctOld.Owner__c){ 
                            if(ct.Owner__c == employee.Id && ctOld.Owner__c== employee.Id && ct.AccountId != employee.Contact.AccountId && ctOld.AccountId != ct.AccountId){
                                Ownership_History__c o = new Ownership_History__c();                            
                                o.From_Agency__c = ctOld.Owner__c != null ? mapContacts.get(ctOld.Owner__c): null;
                                o.From_User__c = ctOld.Owner__c != null ? ctOld.Owner__c : null;
                                o.To_Agency__c = ct.AccountId;
                                o.To_User__c = ct.Owner__c;
                                o.Action__c = 'Shared Visibility';
                                o.Contact__c = ct.ID;
                                lOH.add(o);
                            } 
                        
                        }
                        
                            
                    }
                if(!lOH.isEmpty()){
                    //insert lOH;
                } 
            }
            //------------- END Ownership History ----------------------
            
        }
    }else if (Trigger.isBefore){
                
        if(trigger.isInsert){
            
            //========== Lead==================
            string leadRecordType = [Select id from RecordType where name = 'Lead' limit 1].id;
            list<Custom_Note_Task__c> lTask = new list<Custom_Note_Task__c>();
            list<webFormInquiry__c> wf = new list<webFormInquiry__c>();
            Map<String,Contact> mapEmailsLead = new map<String,Contact>();
            IPFunctions.Sharing IPFSharing = new IPFunctions.Sharing();
            
            for (Contact ac : Trigger.new){ 
                system.debug('ContactDetails==='  + ac);
                system.debug('ac.Lead_Study_Type__c: ' + ac.Lead_Study_Type__c);
                system.debug('ac.LeadSource: ' + ac.LeadSource);
                
                //Leads from DRUPAL
                if(ac.Lead_From_Website__c){
                    
                    ac.isLead__c = true;
                    //ac.allowSync__c = true;
                    ac.createEnquiry__c = false;
                    
                    ac.Email = ac.Lead_Email__c;
                    ac.Age_Range__c = ac.Lead_Age_Range__c;
                    ac.Gender__c = ac.Lead_Gender__c;
                    ac.Phone = ac.Lead_Phone__c; //forms of contact?
                    ac.MobilePhone = ac.Lead_Mobile__c; //forms of contact?
                    ac.When_are_you_planning_to_travel__c = ac.Lead_Plan_to_Travel__c;
                    ac.Travel_Duration__c = ac.Lead_Travel_Duration__c;
                    ac.Lead_Level_of_Interest__c = ac.Lead_How_Ready__c;
                    
                    
                    
                    /** Set the Lead Account **/
                    String leadAccount;
                    //String leadAgencyGroup;
                        
                    if(ac.Original_Agency__c != null){
                        
                        leadAccount = ac.Original_Agency__c;
                        //leadAgencyGroup = ac.Lead_Agency_Group__c;
                        
                    } else if(ac.Lead_Residence_Country__c!= null && ac.Lead_Residence_Country__c != ''){
                        // Still to be defined where should the contact go when theres agency in another country. (This would mean the contact has to be in a different drupal database)
                        
                        String globalLinkId = employee.Contact.Account.Global_Link__c; //[Select Global_Link__c from Account where id = :employee.Contact.Account.ParentID limit 1].Global_Link__c;                
                        System.debug('==>globalLinkId: '+globalLinkId);
                        try{
                            Account agency = [select id, ParentId from Account where Global_Link__c =:globalLinkId and BillingCountry = :ac.Lead_Residence_Country__c and View_Web_Leads__c = true and Inactive__c = false LIMIT 1];                    
                            leadAccount = agency.id; 
                            //leadAgencyGroup = agency.ParentId;
                        }catch (Exception e){
                            system.debug('Error====' + e);
                            Account agency = [select id, ParentId from Account where Global_Link__c =:globalLinkId and BillingCountry = :employee.Contact.Account.BillingCountry and View_Web_Leads__c = true and Inactive__c = false LIMIT 1];                    
                            leadAccount = agency.id; 
                            //leadAgencyGroup = agency.ParentId;
                        }
                            
                    } else {
                                        
                        Account agency = [select id, ParentId from Account where id = :employee.Contact.AccountId LIMIT 1];                    
                        leadAccount = agency.id; 
                        //leadAgencyGroup = agency.ParentId;
                    }
                    
                    
                    System.debug('==>leadAccount: '+ leadAccount);  
                    //System.debug('==>leadAgencyGroup: '+ leadAgencyGroup);
                    
                    
                    ac.Original_Agency__c = leadAccount;
                    ac.AccountID = leadAccount;
                    
                    /** END Set the Lead Account **/
                    
                    
                    
                    
                            
                    for(Contact acco : IPFSharing.getContactsByEmail(ac.Email) ){
                        mapEmailsLead.put(acco.Email,acco);
                        System.debug('==>mapEmailsLead: '+ acco);   
                    }
                    
                    webFormInquiry__c wfe;
                    Custom_Note_Task__c newTask;
                    
                    
                    //system.debug('@> leads ' + leads);
                    
                    if(ac.email != null && mapEmailsLead.containsKey(ac.email)){
                        
                        wfe = new webFormInquiry__c();
                        wfe.Account__c = mapEmailsLead.get(ac.email).id;
                        wfe.Country__c = ac.Lead_Residence_Country__c;
                        wfe.State__c = ac.Lead_Residence_State__c;
                        wfe.City__c = ac.Lead_Residence_City__c;
                        wfe.Destination__c = ac.Lead_Destinations__c;
                        wfe.How_did_you_hear_about_us__c = ac.LeadSource;
                        wfe.How_long_are_you_going_for__c = ac.Lead_Travel_Duration__c;
                        wfe.Message__c = ac.Lead_Message__c;
                        wfe.Study_Area__c = ac.Lead_Study_Type__c;
                        wfe.What_are_you_looking_for__c = ac.What_are_you_looking_for__c;
                        wfe.Other_Destination__c = ac.Lead_Other_Destination__c;
                        wfe.Other_Product_Type__c = ac.Lead_Other_Product__c;
                        wfe.When_are_you_planning_to_travel__c = ac.Lead_Plan_to_Travel__c;
                        wfe.Form_URL__c = ac.Web_Form_URL__c;
                        wf.add(wfe);
                        
                        if(mapEmailsLead.get(ac.email).owner__c != null){
                            newTask = new Custom_Note_Task__c();
                            newTask.Related_Contact__c = mapEmailsLead.get(ac.email).Id;
                            newTask.Due_Date__c = System.today();
                            newTask.Assign_To__c = mapEmailsLead.get(ac.email).owner__c;
                            newTask.Status__c = 'In Progress';
                            newTask.Subject__c = 'Web Enquiry';
                            newTask.Comments__c = ac.Lead_Message__c != null ? ac.Lead_Message__c : '(No message)';
                            //newTask.ReminderDateTime = System.now();
                            //newTask.IsReminderSet = true;
                            lTask.add(newTask);
                        }           
                        
                    }
                    
                }
                if(ac.isLead__c){
                    ac.Last_Enquiry__c = System.now();
                    ac.recordTypeId = leadRecordType;
                    ac.Email = ac.Email;
                    ac.Lead_Assignment__c = 'Received';
                    
                    if(ac.Lead_Level_of_Interest__c == null || ac.Lead_Level_of_Interest__c == '')
                        ac.Lead_Level_of_Interest__c = '0 - To Be Qualified';
                    
                    //ac.Main_Latitude__c =  ac.Lead_Latitude__c;
                    //ac.Location__Longitude__s = ac.Lead_Longitude__c;
                    ac.MailingCity = ac.Lead_Residence_City__c;
                    ac.MailingStreet = ac.Lead_Residence_Street__c;
                    ac.MailingCountry = ac.Lead_Residence_Country__c;
                    ac.MailingState = ac.Lead_Residence_State__c;
                    ac.MailingPostalCode =  ac.Lead_Residence_PostCode__c;

                    system.debug('Lead_Original_Agency__c====' + ac.Lead_Original_Agency__c);
                    system.debug('AccountId====' + ac.AccountId);
                    
                    
                    if(ac.Lead_Original_Agency__c!=null && ac.Lead_Original_Agency__c != ''){
                        
                        ac.AccountId = ac.Lead_Original_Agency__c ;
                        ac.Current_Agency__c = ac.Lead_Original_Agency__c;      
                    }else{
                        ac.Current_Agency__c = ac.AccountId;
                        ac.Lead_Original_Agency__c = ac.AccountId;
                    }

                    if(ac.Lead_State__c== null || ac.Lead_State__c ==''){
                        //ac.Lead_State__c = 'New';
                    }
                    ac.Lead_Stage__c = 'Stage 0';
                    ac.Lead_Status_CreatedBy__c = UserInfo.getUserId();
                    ac.Lead_Status_Date_Time__c = System.now();                 
                    if(ac.Status__c == null || ac.Status__c == ''){
                        ac.Status__c = 'NEW LEAD';
                    }

                    //ac.Lead_Product_Type__c = ac.What_are_you_looking_for__c;
                    if(ac.Lead_Assignment_To__c == null){
                        ac.Lead_First_Contact__c = 'Web Form';
                        ac.Lead_First_Contact_Date__c = System.today();
                    }
                    
                    system.debug('ac.Destination_Country__c: ' + ac.Destination_Country__c);
                    system.debug('ac.Lead_Destinations__c: ' + ac.Lead_Destinations__c);
                    system.debug('ac.Lead_Study_Type__c: ' + ac.Lead_Study_Type__c);
                    system.debug('ac.age_range__c: ' + ac.age_range__c);
                    system.debug('ac.lead_age_range__c: ' + ac.lead_age_range__c);
                    
                    if(ac.Destination_Country__c == null){
                        if(ac.Lead_Destinations__c != null && ac.Lead_Destinations__c != ''){
                            List<String> dests = ac.Lead_Destinations__c.split(';');    
                            
                            if(dests.size()==1 && dests[0]!= 'Other'){
                                ac.Destination_Country__c = dests.get(0);
                                ac.Lead_Destinations__c = null;         
                            }
                        }
                    }
                    
                    system.debug('ac.Destination_Country__c: ' + ac.Destination_Country__c);
                }
            }   
            if(wf.size() > 0)
                insert wf;
            //========== End Lead==================
            IpFunctions.Sharing f = new IpFunctions.Sharing();
            for (Contact ac : Trigger.new){ 
                System.Debug('==> ac.RDStation_createddate__c: '+ac.RDStation_createddate__c);
                System.Debug('==> ac.email: '+ac.email);
                if(ac.RDStation_createddate__c != null || ac.Lead_From_Website__c){
                    Account agency = null;
                    try{
                        agency = [Select id, parentId, parent.Destination_Group__c  from Account where id = :ac.original_agency__c limit 1];
                    }catch(QueryException qe){
                        system.debug('No Agency found: ' +ac.original_agency__c);
                    }
                    string sql = 'Select count() from Contact where email = \''+ac.email+ '\'';
                    if(agency != null && agency.parent != null && !agency.parent.Destination_Group__c)
                        sql += ' and account.ParentId = \''+agency.ParentId+ '\'';
                    System.Debug('==> sql: '+sql);
                    if(Database.countQuery(sql) > 0)
                        ac.email.addError('You cannot add a duplicate email, check if the client you are trying to add already exists!'); 
                }
                else{
                    if(f.verifyRepeatedEmail(ac))
                        ac.email.addError('You cannot add a duplicate email, check if the client you are trying to add already exists!');   
                }
                    
            }
            
        }else if(trigger.isUpdate){
            
            
            //Prevent the website from updating the Original Agency....... -.-
            if(isAPI)           
                for(Contact oldC : Trigger.old)
                    for(Contact newC : Trigger.new)
                        if(oldC.id == newC.id && oldC.Original_Agency__c != newC.Original_Agency__c)
                            newC.Original_Agency__c = oldC.Original_Agency__c;
                
            
            Map<String, ContactNumber> contactNumbers = new Map<String, ContactNumber>();
            
            //start drupal sync fields
            List<webFormInquiry__c> enquiries = new List<webFormInquiry__c>();
            List<Custom_Note_Task__c> tasks = new List<Custom_Note_Task__c>();
            Custom_Note_Task__c newTask;
            
            
            
            for (Contact ct : Trigger.new){
                system.debug('@@@ ct' + ct);
                system.debug('@@@ ct.Quote_URL__c: ' + ct.Quote_URL__c);
                system.debug('@@@ ct.CreateQuote__c: ' + ct.CreateQuote__c);
                
                if(isAPI){
                    if(ct.MailingCountry == null){
                        ct.MailingCountry = ct.Lead_Residence_Country__c;
                        ct.MailingCity = ct.Lead_Residence_City__c;
                        ct.MailingState = ct.Lead_Residence_State__c;
                        ct.MailingPostalCode = ct.Lead_Residence_PostCode__c;
                        ct.MailingStreet = ct.Lead_Residence_Street__c;
                    }
                    
                    ct.Phone = ct.Lead_Phone__c;
                    ct.MobilePhone = ct.Lead_Mobile__c;
                    contactNumbers.put(ct.id, new ContactNumber(ct.Lead_Phone__c, ct.Lead_Mobile__c, ct.Nationality__c));
                } else {
                    ct.Lead_Gender__c = ct.Gender__c; 
                    ct.Lead_Phone__c = ct.Phone;
                    ct.Lead_Mobile__c = ct.MobilePhone; 
                    ct.Lead_Residence_Country__c = ct.MailingCountry;
                    ct.Lead_Residence_City__c = ct.MailingCity;
                    ct.Lead_Residence_State__c = ct.MailingState;
                    ct.Lead_Residence_PostCode__c = ct.MailingPostalCode;
                    ct.Lead_Residence_Street__c = ct.MailingStreet;
                    ct.Lead_Age_Range__c = ct.Age_Range__c;
                    ct.Lead_Plan_to_Travel__c = ct.When_are_you_planning_to_travel__c;
                    ct.Lead_Travel_Duration__c = ct.Travel_Duration__c;
                    ct.Lead_How_Ready__c = ct.Lead_Level_of_Interest__c;
                    ct.Lead_Email__c = ct.Email;
                }
                
                
                
                
                
                system.debug('@@@ ct.CreateEnquiry__c: ' + ct.CreateEnquiry__c);
                
                if(ct.CreateEnquiry__c){

                    if(ct.CreateQuote__c)
                        ct.Lead_Message__c = 'Quote created in website';
                    
                    
                    if( (!ct.CreateQuote__c && ct.owner__c != null) || (ct.CreateQuote__c && !ct.DoNotCall)){
                        newTask = new Custom_Note_Task__c();
                        newTask.Related_Contact__c = ct.id;
                        newTask.Due_Date__c = System.today();
                        newTask.Assign_To__c = ct.owner__c;
                        newTask.Status__c = 'In Progress';
                        newTask.Subject__c = 'Web Enquiry';
                        newTask.Comments__c = ct.Lead_Message__c != null ? ct.Lead_Message__c : '(No message)';
                        if(ct.CreateQuote__c)
                            newTask.Comments__c += ' (' + ct.quote_url__c + ')';
                            
                        //newTask.ReminderDateTime = System.now();
                        //newTask.IsReminderSet = true;
                        tasks.add(newTask);
                    }
                    
                    ct.CreateEnquiry__c = false;
                    enquiries.add(addNewWebFormInquiry(ct, ct.id));
                    ct.CreateQuote__c = false;
                    ct.Quote_URL__c = null;
                    ct.Lead_Message__c = null;
                }
                
            }
            
            if(contactNumbers.size() > 0){
                
                List<Forms_of_Contact__c> focs = new List<Forms_of_Contact__c>();
                
                for(Contact ct : [select id, (Select Detail__c, type__c from forms_of_contact__r where type__c in ('Mobile', 'Home Phone') and From_Website__c = true) from Contact where id in :contactNumbers.keySet()]){
                    boolean foundPhone = false;
                    boolean foundMobile = false;
                    for(Forms_of_Contact__c foc : ct.forms_of_Contact__r){
                        if(foc.type__c == 'Mobile'){
                            foc.detail__c = contactNumbers.get(ct.id).mobile;
                            foundMobile = true;
                            focs.add(foc);
                        } else if(foc.Type__c == 'Home Phone'){
                            foc.detail__c = contactNumbers.get(ct.id).phone;
                            foundPhone = true;
                            focs.add(foc);
                        }
                    }
                    
                    if(!foundPhone && contactNumbers.get(ct.id).phone != null && contactNumbers.get(ct.id).phone != '')
                        focs.add(new Forms_of_Contact__c(Contact__c = ct.id, detail__c = contactNumbers.get(ct.id).phone, country__c = contactNumbers.get(ct.id).country, from_website__c = true, type__c = 'Mobile'));
                    
                    
                    if(!foundMobile && contactNumbers.get(ct.id).mobile != null && contactNumbers.get(ct.id).mobile != '')
                        focs.add(new Forms_of_Contact__c(Contact__c = ct.id, detail__c = contactNumbers.get(ct.id).mobile, country__c = contactNumbers.get(ct.id).country, from_website__c = true, type__c = 'Home Phone'));
                    
                }
                
                upsert focs;
            }
            
            insert enquiries;
            insert tasks;
            
            //end drupal sync fields
            
            if(IPFunctions.firstRunBeforeUpdate){
                IPFunctions.firstRunBeforeUpdate = false;
                IpFunctions.Sharing f = new IpFunctions.Sharing();
                for (Contact ac : Trigger.new){ 
                    for (Contact ao : Trigger.old)
                        if(ac.id == ao.id && ac.Email != ao.Email){
                            if(f.verifyRepeatedEmail(ac))
                                ac.email.addError('You cannot add a duplicate email, check if the client you are trying to add already exists!');
                        }   
                }
            }
        }
    }
    
    class ContactNumber {
        public String phone;
        public String mobile;
        public String country;
        public ContactNumber(String phone, String mobile, String country){
            this.phone = phone;
            this.mobile = mobile;
            this.country = country;
        }
    }

    public webFormInquiry__c addNewWebFormInquiry(Contact a, String AccoId){
        
        webFormInquiry__c wfe = new webFormInquiry__c();
        wfe.Account__c = AccoId;
        wfe.Country__c = a.MailingCountry;
        wfe.State__c = a.MailingState;
        wfe.City__c = a.MailingCity;
        wfe.Destination__c = a.Lead_Destinations__c;
        wfe.How_did_you_hear_about_us__c = a.LeadSource;
        wfe.How_long_are_you_going_for__c = a.Travel_Duration__c;
        wfe.Message__c = a.Lead_Message__c;
        wfe.Study_Area__c = a.Lead_Study_Type__c;
        wfe.What_are_you_looking_for__c = a.Lead_Product_Type__c;
        wfe.Other_Destination__c = a.Lead_Other_Destination__c;
        wfe.Other_Product_Type__c = a.Lead_Other_Product__c;
        wfe.When_are_you_planning_to_travel__c = a.When_are_you_planning_to_travel__c;
        wfe.Read__c = true;
        wfe.Form_URL__c = a.web_form_url__c;
        
        if(a.createQuote__c)
            wfe.quote_url__c = a.quote_url__c;
        
        System.debug('==> wfe: '+wfe);
        return wfe;
    }
}