trigger CourseTrigger on Course__c (after insert, after update) {
	
	if(Trigger.isAfter && Trigger.isUpdate)		
		TranslationService.updateTranslations(trigger.old, trigger.new);

}